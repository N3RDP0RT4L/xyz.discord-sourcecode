package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: WidgetHubDomains.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\t\b\u0087\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0010J \u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cR\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001d\u001a\u0004\b\u001e\u0010\bR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001f\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/widgets/hubs/HubDomainArgs;", "Landroid/os/Parcelable;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/widgets/hubs/DomainGuildInfo;", "component2", "()Ljava/util/List;", NotificationCompat.CATEGORY_EMAIL, "guildInfos", "copy", "(Ljava/lang/String;Ljava/util/List;)Lcom/discord/widgets/hubs/HubDomainArgs;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Ljava/util/List;", "getGuildInfos", "Ljava/lang/String;", "getEmail", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubDomainArgs implements Parcelable {
    public static final Parcelable.Creator<HubDomainArgs> CREATOR = new Creator();
    private final String email;
    private final List<DomainGuildInfo> guildInfos;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<HubDomainArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final HubDomainArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            String readString = parcel.readString();
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            while (readInt != 0) {
                arrayList.add(DomainGuildInfo.CREATOR.createFromParcel(parcel));
                readInt--;
            }
            return new HubDomainArgs(readString, arrayList);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final HubDomainArgs[] newArray(int i) {
            return new HubDomainArgs[i];
        }
    }

    public HubDomainArgs(String str, List<DomainGuildInfo> list) {
        m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
        m.checkNotNullParameter(list, "guildInfos");
        this.email = str;
        this.guildInfos = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ HubDomainArgs copy$default(HubDomainArgs hubDomainArgs, String str, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hubDomainArgs.email;
        }
        if ((i & 2) != 0) {
            list = hubDomainArgs.guildInfos;
        }
        return hubDomainArgs.copy(str, list);
    }

    public final String component1() {
        return this.email;
    }

    public final List<DomainGuildInfo> component2() {
        return this.guildInfos;
    }

    public final HubDomainArgs copy(String str, List<DomainGuildInfo> list) {
        m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
        m.checkNotNullParameter(list, "guildInfos");
        return new HubDomainArgs(str, list);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HubDomainArgs)) {
            return false;
        }
        HubDomainArgs hubDomainArgs = (HubDomainArgs) obj;
        return m.areEqual(this.email, hubDomainArgs.email) && m.areEqual(this.guildInfos, hubDomainArgs.guildInfos);
    }

    public final String getEmail() {
        return this.email;
    }

    public final List<DomainGuildInfo> getGuildInfos() {
        return this.guildInfos;
    }

    public int hashCode() {
        String str = this.email;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<DomainGuildInfo> list = this.guildInfos;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("HubDomainArgs(email=");
        R.append(this.email);
        R.append(", guildInfos=");
        return a.K(R, this.guildInfos, ")");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeString(this.email);
        List<DomainGuildInfo> list = this.guildInfos;
        parcel.writeInt(list.size());
        for (DomainGuildInfo domainGuildInfo : list) {
            domainGuildInfo.writeToParcel(parcel, 0);
        }
    }
}
