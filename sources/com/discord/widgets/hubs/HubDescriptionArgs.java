package com.discord.widgets.hubs;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubDescriptionViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u000e\b\u0087\b\u0018\u00002\u00020\u0001BQ\u0012\n\u0010\u0013\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\u0015\u001a\u00020\b\u0012\u0006\u0010\u0016\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u0017\u001a\u00020\b\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0010¢\u0006\u0004\b3\u00104J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012Jb\u0010\u001a\u001a\u00020\u00002\f\b\u0002\u0010\u0013\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0014\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\u0015\u001a\u00020\b2\b\b\u0002\u0010\u0016\u001a\u00020\u000b2\b\b\u0002\u0010\u0017\u001a\u00020\b2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0010HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\rJ\u0010\u0010\u001d\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010!\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010\u001fHÖ\u0003¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b#\u0010\u001eJ \u0010(\u001a\u00020'2\u0006\u0010%\u001a\u00020$2\u0006\u0010&\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b(\u0010)R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b+\u0010\rR\u0019\u0010\u0015\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010,\u001a\u0004\b\u0015\u0010\nR\u001d\u0010\u0014\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010-\u001a\u0004\b.\u0010\u0005R\u001d\u0010\u0013\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010-\u001a\u0004\b/\u0010\u0005R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00100\u001a\u0004\b1\u0010\u0012R\u0019\u0010\u0017\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010,\u001a\u0004\b\u0017\u0010\nR\u0019\u0010\u0016\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010*\u001a\u0004\b2\u0010\r¨\u00065"}, d2 = {"Lcom/discord/widgets/hubs/HubDescriptionArgs;", "Landroid/os/Parcelable;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "", "component3", "()Z", "", "component4", "()Ljava/lang/String;", "component5", "component6", "", "component7", "()Ljava/lang/Integer;", "guildId", "channelId", "isEditing", "hubName", "isNewGuild", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "primaryCategoryId", "copy", "(JJZLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;)Lcom/discord/widgets/hubs/HubDescriptionArgs;", "toString", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Ljava/lang/String;", "getDescription", "Z", "J", "getChannelId", "getGuildId", "Ljava/lang/Integer;", "getPrimaryCategoryId", "getHubName", HookHelper.constructorName, "(JJZLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubDescriptionArgs implements Parcelable {
    public static final Parcelable.Creator<HubDescriptionArgs> CREATOR = new Creator();
    private final long channelId;
    private final String description;
    private final long guildId;
    private final String hubName;
    private final boolean isEditing;
    private final boolean isNewGuild;
    private final Integer primaryCategoryId;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<HubDescriptionArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final HubDescriptionArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            return new HubDescriptionArgs(parcel.readLong(), parcel.readLong(), parcel.readInt() != 0, parcel.readString(), parcel.readInt() != 0, parcel.readString(), parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final HubDescriptionArgs[] newArray(int i) {
            return new HubDescriptionArgs[i];
        }
    }

    public HubDescriptionArgs(long j, long j2, boolean z2, String str, boolean z3, String str2, Integer num) {
        m.checkNotNullParameter(str, "hubName");
        this.guildId = j;
        this.channelId = j2;
        this.isEditing = z2;
        this.hubName = str;
        this.isNewGuild = z3;
        this.description = str2;
        this.primaryCategoryId = num;
    }

    public final long component1() {
        return this.guildId;
    }

    public final long component2() {
        return this.channelId;
    }

    public final boolean component3() {
        return this.isEditing;
    }

    public final String component4() {
        return this.hubName;
    }

    public final boolean component5() {
        return this.isNewGuild;
    }

    public final String component6() {
        return this.description;
    }

    public final Integer component7() {
        return this.primaryCategoryId;
    }

    public final HubDescriptionArgs copy(long j, long j2, boolean z2, String str, boolean z3, String str2, Integer num) {
        m.checkNotNullParameter(str, "hubName");
        return new HubDescriptionArgs(j, j2, z2, str, z3, str2, num);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HubDescriptionArgs)) {
            return false;
        }
        HubDescriptionArgs hubDescriptionArgs = (HubDescriptionArgs) obj;
        return this.guildId == hubDescriptionArgs.guildId && this.channelId == hubDescriptionArgs.channelId && this.isEditing == hubDescriptionArgs.isEditing && m.areEqual(this.hubName, hubDescriptionArgs.hubName) && this.isNewGuild == hubDescriptionArgs.isNewGuild && m.areEqual(this.description, hubDescriptionArgs.description) && m.areEqual(this.primaryCategoryId, hubDescriptionArgs.primaryCategoryId);
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final String getDescription() {
        return this.description;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final String getHubName() {
        return this.hubName;
    }

    public final Integer getPrimaryCategoryId() {
        return this.primaryCategoryId;
    }

    public int hashCode() {
        int a = (b.a(this.channelId) + (b.a(this.guildId) * 31)) * 31;
        boolean z2 = this.isEditing;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (a + i2) * 31;
        String str = this.hubName;
        int i5 = 0;
        int hashCode = (i4 + (str != null ? str.hashCode() : 0)) * 31;
        boolean z3 = this.isNewGuild;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        int i6 = (hashCode + i) * 31;
        String str2 = this.description;
        int hashCode2 = (i6 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num = this.primaryCategoryId;
        if (num != null) {
            i5 = num.hashCode();
        }
        return hashCode2 + i5;
    }

    public final boolean isEditing() {
        return this.isEditing;
    }

    public final boolean isNewGuild() {
        return this.isNewGuild;
    }

    public String toString() {
        StringBuilder R = a.R("HubDescriptionArgs(guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", isEditing=");
        R.append(this.isEditing);
        R.append(", hubName=");
        R.append(this.hubName);
        R.append(", isNewGuild=");
        R.append(this.isNewGuild);
        R.append(", description=");
        R.append(this.description);
        R.append(", primaryCategoryId=");
        return a.E(R, this.primaryCategoryId, ")");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeLong(this.guildId);
        parcel.writeLong(this.channelId);
        parcel.writeInt(this.isEditing ? 1 : 0);
        parcel.writeString(this.hubName);
        parcel.writeInt(this.isNewGuild ? 1 : 0);
        parcel.writeString(this.description);
        Integer num = this.primaryCategoryId;
        if (num != null) {
            parcel.writeInt(1);
            i2 = num.intValue();
        } else {
            i2 = 0;
        }
        parcel.writeInt(i2);
    }

    public /* synthetic */ HubDescriptionArgs(long j, long j2, boolean z2, String str, boolean z3, String str2, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, z2, str, (i & 16) != 0 ? false : z3, (i & 32) != 0 ? null : str2, (i & 64) != 0 ? null : num);
    }
}
