package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubDescriptionViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0011\u001a\u00020\b\u0012\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0004\b(\u0010)J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJB\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0011\u001a\u00020\b2\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0015\u0010\nJ\u0010\u0010\u0016\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001c\u001a\u0004\b\u001d\u0010\u000eR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\nR\u0015\u0010'\u001a\u0004\u0018\u00010$8F@\u0006¢\u0006\u0006\u001a\u0004\b%\u0010&¨\u0006*"}, d2 = {"Lcom/discord/widgets/hubs/HubDescriptionState;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Ljava/lang/Integer;", "", "component3", "()Ljava/lang/String;", "Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/directory/DirectoryEntryGuild;", "component4", "()Lcom/discord/stores/utilities/RestCallState;", "channel", "primaryCategoryId", "guildName", "addServerAsync", "copy", "(Lcom/discord/api/channel/Channel;Ljava/lang/Integer;Ljava/lang/String;Lcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/hubs/HubDescriptionState;", "toString", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/utilities/RestCallState;", "getAddServerAsync", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/lang/Integer;", "getPrimaryCategoryId", "Ljava/lang/String;", "getGuildName", "Lcom/discord/models/hubs/DirectoryEntryCategory;", "getSelectedCategory", "()Lcom/discord/models/hubs/DirectoryEntryCategory;", "selectedCategory", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/lang/Integer;Ljava/lang/String;Lcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubDescriptionState {
    private final RestCallState<DirectoryEntryGuild> addServerAsync;
    private final Channel channel;
    private final String guildName;
    private final Integer primaryCategoryId;

    public HubDescriptionState() {
        this(null, null, null, null, 15, null);
    }

    public HubDescriptionState(Channel channel, Integer num, String str, RestCallState<DirectoryEntryGuild> restCallState) {
        m.checkNotNullParameter(str, "guildName");
        m.checkNotNullParameter(restCallState, "addServerAsync");
        this.channel = channel;
        this.primaryCategoryId = num;
        this.guildName = str;
        this.addServerAsync = restCallState;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ HubDescriptionState copy$default(HubDescriptionState hubDescriptionState, Channel channel, Integer num, String str, RestCallState restCallState, int i, Object obj) {
        if ((i & 1) != 0) {
            channel = hubDescriptionState.channel;
        }
        if ((i & 2) != 0) {
            num = hubDescriptionState.primaryCategoryId;
        }
        if ((i & 4) != 0) {
            str = hubDescriptionState.guildName;
        }
        if ((i & 8) != 0) {
            restCallState = hubDescriptionState.addServerAsync;
        }
        return hubDescriptionState.copy(channel, num, str, restCallState);
    }

    public final Channel component1() {
        return this.channel;
    }

    public final Integer component2() {
        return this.primaryCategoryId;
    }

    public final String component3() {
        return this.guildName;
    }

    public final RestCallState<DirectoryEntryGuild> component4() {
        return this.addServerAsync;
    }

    public final HubDescriptionState copy(Channel channel, Integer num, String str, RestCallState<DirectoryEntryGuild> restCallState) {
        m.checkNotNullParameter(str, "guildName");
        m.checkNotNullParameter(restCallState, "addServerAsync");
        return new HubDescriptionState(channel, num, str, restCallState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HubDescriptionState)) {
            return false;
        }
        HubDescriptionState hubDescriptionState = (HubDescriptionState) obj;
        return m.areEqual(this.channel, hubDescriptionState.channel) && m.areEqual(this.primaryCategoryId, hubDescriptionState.primaryCategoryId) && m.areEqual(this.guildName, hubDescriptionState.guildName) && m.areEqual(this.addServerAsync, hubDescriptionState.addServerAsync);
    }

    public final RestCallState<DirectoryEntryGuild> getAddServerAsync() {
        return this.addServerAsync;
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final String getGuildName() {
        return this.guildName;
    }

    public final Integer getPrimaryCategoryId() {
        return this.primaryCategoryId;
    }

    public final DirectoryEntryCategory getSelectedCategory() {
        Integer num = this.primaryCategoryId;
        if (num == null) {
            return null;
        }
        int intValue = num.intValue();
        DirectoryEntryCategory.Companion companion = DirectoryEntryCategory.Companion;
        Channel channel = this.channel;
        boolean z2 = true;
        if (channel == null || !ChannelUtils.u(channel)) {
            z2 = false;
        }
        return companion.findByKey(intValue, z2);
    }

    public int hashCode() {
        Channel channel = this.channel;
        int i = 0;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        Integer num = this.primaryCategoryId;
        int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.guildName;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        RestCallState<DirectoryEntryGuild> restCallState = this.addServerAsync;
        if (restCallState != null) {
            i = restCallState.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("HubDescriptionState(channel=");
        R.append(this.channel);
        R.append(", primaryCategoryId=");
        R.append(this.primaryCategoryId);
        R.append(", guildName=");
        R.append(this.guildName);
        R.append(", addServerAsync=");
        R.append(this.addServerAsync);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ HubDescriptionState(Channel channel, Integer num, String str, RestCallState restCallState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : channel, (i & 2) != 0 ? null : num, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? Default.INSTANCE : restCallState);
    }
}
