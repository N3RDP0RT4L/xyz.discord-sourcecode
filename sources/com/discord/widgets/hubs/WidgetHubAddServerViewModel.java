package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreDirectories;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetHubAddServerViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001b2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001bB\u0017\u0012\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\r\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0010\u0010\u0011J%\u0010\u0015\u001a\u00020\u00042\n\u0010\u0013\u001a\u00060\u000bj\u0002`\u00122\n\u0010\u0014\u001a\u00060\u000bj\u0002`\f¢\u0006\u0004\b\u0015\u0010\u0016¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubAddServerViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/hubs/HubAddServerState;", "state", "", "handleStoreUpdate", "(Lcom/discord/widgets/hubs/HubAddServerState;)V", "", "index", "setIndex", "(I)V", "", "Lcom/discord/primitives/ChannelId;", "getChannelId", "()Ljava/lang/Long;", "", "getHubName", "()Ljava/lang/String;", "Lcom/discord/primitives/GuildId;", ModelAuditLogEntry.CHANGE_KEY_ID, "directoryChannelId", "removeGuild", "(JJ)V", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(Lrx/Observable;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAddServerViewModel extends AppViewModel<HubAddServerState> {
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetHubAddServerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/hubs/HubAddServerState;", "p1", "", "invoke", "(Lcom/discord/widgets/hubs/HubAddServerState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.hubs.WidgetHubAddServerViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<HubAddServerState, Unit> {
        public AnonymousClass1(WidgetHubAddServerViewModel widgetHubAddServerViewModel) {
            super(1, widgetHubAddServerViewModel, WidgetHubAddServerViewModel.class, "handleStoreUpdate", "handleStoreUpdate(Lcom/discord/widgets/hubs/HubAddServerState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(HubAddServerState hubAddServerState) {
            invoke2(hubAddServerState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(HubAddServerState hubAddServerState) {
            m.checkNotNullParameter(hubAddServerState, "p1");
            ((WidgetHubAddServerViewModel) this.receiver).handleStoreUpdate(hubAddServerState);
        }
    }

    /* compiled from: WidgetHubAddServerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015JM\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubAddServerViewModel$Companion;", "", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuildSelected;", "guildsSelected", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannelsSelected;", "channelsSelectedStore", "Lcom/discord/stores/StoreDirectories;", "directoriesStore", "Lrx/Observable;", "Lcom/discord/widgets/hubs/HubAddServerState;", "observeStores", "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreDirectories;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<HubAddServerState> observeStores(ObservationDeck observationDeck, StoreGuilds storeGuilds, StoreGuildSelected storeGuildSelected, StorePermissions storePermissions, StoreChannels storeChannels, StoreChannelsSelected storeChannelsSelected, StoreDirectories storeDirectories) {
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeGuilds, storeGuildSelected, storePermissions, storeChannels, storeChannelsSelected, storeDirectories}, false, null, null, new WidgetHubAddServerViewModel$Companion$observeStores$1(storeGuilds, storeGuildSelected, storePermissions, storeChannelsSelected, storeChannels, storeDirectories), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetHubAddServerViewModel() {
        this(null, 1, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetHubAddServerViewModel(rx.Observable r9, int r10, kotlin.jvm.internal.DefaultConstructorMarker r11) {
        /*
            r8 = this;
            r10 = r10 & 1
            if (r10 == 0) goto L28
            com.discord.widgets.hubs.WidgetHubAddServerViewModel$Companion r0 = com.discord.widgets.hubs.WidgetHubAddServerViewModel.Companion
            com.discord.stores.updates.ObservationDeck r1 = com.discord.stores.updates.ObservationDeckProvider.get()
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r2 = r9.getGuilds()
            com.discord.stores.StoreGuildSelected r3 = r9.getGuildSelected()
            com.discord.stores.StorePermissions r4 = r9.getPermissions()
            com.discord.stores.StoreChannels r5 = r9.getChannels()
            com.discord.stores.StoreChannelsSelected r6 = r9.getChannelsSelected()
            com.discord.stores.StoreDirectories r7 = r9.getDirectories()
            rx.Observable r9 = com.discord.widgets.hubs.WidgetHubAddServerViewModel.Companion.access$observeStores(r0, r1, r2, r3, r4, r5, r6, r7)
        L28:
            r8.<init>(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.hubs.WidgetHubAddServerViewModel.<init>(rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreUpdate(HubAddServerState hubAddServerState) {
        updateViewState(HubAddServerState.copy$default(requireViewState(), hubAddServerState.getHubName(), hubAddServerState.getDirectoryChannelId(), 0, hubAddServerState.getSelectableGuilds(), hubAddServerState.getAddedGuilds(), hubAddServerState.getAddedDirectories(), 4, null));
    }

    public final Long getChannelId() {
        HubAddServerState viewState = getViewState();
        if (viewState != null) {
            return viewState.getDirectoryChannelId();
        }
        return null;
    }

    public final String getHubName() {
        HubAddServerState viewState = getViewState();
        String hubName = viewState != null ? viewState.getHubName() : null;
        return hubName != null ? hubName : "";
    }

    public final void removeGuild(long j, long j2) {
        StoreStream.Companion.getDirectories().removeServerFromDirectory(j2, j);
    }

    public final void setIndex(int i) {
        updateViewState(HubAddServerState.copy$default(requireViewState(), null, null, i, null, null, null, 59, null));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubAddServerViewModel(Observable<HubAddServerState> observable) {
        super(new HubAddServerState(null, null, 0, null, null, null, 63, null));
        m.checkNotNullParameter(observable, "storeObservable");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetHubAddServerViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
