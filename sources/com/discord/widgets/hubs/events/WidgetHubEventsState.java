package com.discord.widgets.hubs.events;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.directory.DirectoryEntryEvent;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubEventsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002\u0012\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0014\b\u0002\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00050\t¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001c\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00050\tHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ@\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0014\b\u0002\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00050\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0019\u001a\u00020\u00022\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\bR%\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00050\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\f¨\u0006#"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventsState;", "", "", "component1", "()Z", "", "Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;", "component2", "()Ljava/util/List;", "Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/directory/DirectoryEntryEvent;", "component3", "()Lcom/discord/stores/utilities/RestCallState;", "showHeader", "eventsData", "eventsAsync", "copy", "(ZLjava/util/List;Lcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/hubs/events/WidgetHubEventsState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowHeader", "Ljava/util/List;", "getEventsData", "Lcom/discord/stores/utilities/RestCallState;", "getEventsAsync", HookHelper.constructorName, "(ZLjava/util/List;Lcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventsState {
    private final RestCallState<List<DirectoryEntryEvent>> eventsAsync;
    private final List<HubGuildScheduledEventData> eventsData;
    private final boolean showHeader;

    public WidgetHubEventsState() {
        this(false, null, null, 7, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public WidgetHubEventsState(boolean z2, List<HubGuildScheduledEventData> list, RestCallState<? extends List<DirectoryEntryEvent>> restCallState) {
        m.checkNotNullParameter(list, "eventsData");
        m.checkNotNullParameter(restCallState, "eventsAsync");
        this.showHeader = z2;
        this.eventsData = list;
        this.eventsAsync = restCallState;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WidgetHubEventsState copy$default(WidgetHubEventsState widgetHubEventsState, boolean z2, List list, RestCallState restCallState, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = widgetHubEventsState.showHeader;
        }
        if ((i & 2) != 0) {
            list = widgetHubEventsState.eventsData;
        }
        if ((i & 4) != 0) {
            restCallState = widgetHubEventsState.eventsAsync;
        }
        return widgetHubEventsState.copy(z2, list, restCallState);
    }

    public final boolean component1() {
        return this.showHeader;
    }

    public final List<HubGuildScheduledEventData> component2() {
        return this.eventsData;
    }

    public final RestCallState<List<DirectoryEntryEvent>> component3() {
        return this.eventsAsync;
    }

    public final WidgetHubEventsState copy(boolean z2, List<HubGuildScheduledEventData> list, RestCallState<? extends List<DirectoryEntryEvent>> restCallState) {
        m.checkNotNullParameter(list, "eventsData");
        m.checkNotNullParameter(restCallState, "eventsAsync");
        return new WidgetHubEventsState(z2, list, restCallState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetHubEventsState)) {
            return false;
        }
        WidgetHubEventsState widgetHubEventsState = (WidgetHubEventsState) obj;
        return this.showHeader == widgetHubEventsState.showHeader && m.areEqual(this.eventsData, widgetHubEventsState.eventsData) && m.areEqual(this.eventsAsync, widgetHubEventsState.eventsAsync);
    }

    public final RestCallState<List<DirectoryEntryEvent>> getEventsAsync() {
        return this.eventsAsync;
    }

    public final List<HubGuildScheduledEventData> getEventsData() {
        return this.eventsData;
    }

    public final boolean getShowHeader() {
        return this.showHeader;
    }

    public int hashCode() {
        boolean z2 = this.showHeader;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        int i3 = i * 31;
        List<HubGuildScheduledEventData> list = this.eventsData;
        int i4 = 0;
        int hashCode = (i3 + (list != null ? list.hashCode() : 0)) * 31;
        RestCallState<List<DirectoryEntryEvent>> restCallState = this.eventsAsync;
        if (restCallState != null) {
            i4 = restCallState.hashCode();
        }
        return hashCode + i4;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetHubEventsState(showHeader=");
        R.append(this.showHeader);
        R.append(", eventsData=");
        R.append(this.eventsData);
        R.append(", eventsAsync=");
        R.append(this.eventsAsync);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ WidgetHubEventsState(boolean z2, List list, RestCallState restCallState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? true : z2, (i & 2) != 0 ? n.emptyList() : list, (i & 4) != 0 ? Default.INSTANCE : restCallState);
    }
}
