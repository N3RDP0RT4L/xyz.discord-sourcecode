package com.discord.widgets.hubs.events;

import androidx.fragment.app.FragmentManager;
import com.discord.analytics.utils.hubs.HubGuildScheduledEventClickType;
import com.discord.stores.StoreStream;
import com.discord.utilities.directories.DirectoryUtils;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetHubEventsPage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\n\u0010\tJ\u0017\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u000b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\f\u0010\t¨\u0006\r"}, d2 = {"com/discord/widgets/hubs/events/WidgetHubEventsPage$listener$1", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "", "dismissHeader", "()V", "fetchGuildScheduledEvents", "Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;", "eventData", "onSecondaryButtonClicked", "(Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;)V", "onPrimaryButtonClicked", "onCardClicked", "onShareClicked", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventsPage$listener$1 implements HubEventsEventListener {
    public final /* synthetic */ WidgetHubEventsPage this$0;

    public WidgetHubEventsPage$listener$1(WidgetHubEventsPage widgetHubEventsPage) {
        this.this$0 = widgetHubEventsPage;
    }

    @Override // com.discord.widgets.hubs.events.HubEventsEventListener
    public void dismissHeader() {
        this.this$0.getViewModel().dismissHeader();
    }

    @Override // com.discord.widgets.hubs.events.HubEventsEventListener
    public void fetchGuildScheduledEvents() {
        this.this$0.getViewModel().fetchGuildScheduledEvents();
    }

    @Override // com.discord.widgets.hubs.events.HubEventsEventListener
    public void onCardClicked(HubGuildScheduledEventData hubGuildScheduledEventData) {
        m.checkNotNullParameter(hubGuildScheduledEventData, "eventData");
        DirectoryUtils.INSTANCE.logGuildScheduledEventClickAction(hubGuildScheduledEventData.getEvent().i(), hubGuildScheduledEventData.getEvent().h(), this.this$0.getArgs().getGuildId(), HubGuildScheduledEventClickType.JoinServer);
        WidgetGuildScheduledEventDetailsBottomSheet.Companion companion = WidgetGuildScheduledEventDetailsBottomSheet.Companion;
        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.showForDirectory(parentFragmentManager, hubGuildScheduledEventData.getEvent().i(), this.this$0.getArgs().getGuildId(), this.this$0.getArgs().getDirectoryChannelId());
    }

    @Override // com.discord.widgets.hubs.events.HubEventsEventListener
    public void onPrimaryButtonClicked(HubGuildScheduledEventData hubGuildScheduledEventData) {
        m.checkNotNullParameter(hubGuildScheduledEventData, "eventData");
        DirectoryUtils.INSTANCE.maybeJoinAndGoToGuild(this.this$0, hubGuildScheduledEventData.getEvent(), hubGuildScheduledEventData.isInGuild(), this.this$0.getArgs().getGuildId(), this.this$0.getArgs().getDirectoryChannelId(), !hubGuildScheduledEventData.isRsvped() && GuildScheduledEventUtilitiesKt.canRsvp(hubGuildScheduledEventData.getEvent()), new WidgetHubEventsPage$listener$1$onPrimaryButtonClicked$1(this, hubGuildScheduledEventData));
    }

    @Override // com.discord.widgets.hubs.events.HubEventsEventListener
    public void onSecondaryButtonClicked(HubGuildScheduledEventData hubGuildScheduledEventData) {
        m.checkNotNullParameter(hubGuildScheduledEventData, "eventData");
        this.this$0.getViewModel().toggleRsvp(hubGuildScheduledEventData.getEvent());
    }

    @Override // com.discord.widgets.hubs.events.HubEventsEventListener
    public void onShareClicked(HubGuildScheduledEventData hubGuildScheduledEventData) {
        boolean canShareEvent;
        m.checkNotNullParameter(hubGuildScheduledEventData, "eventData");
        long h = hubGuildScheduledEventData.getEvent().h();
        long i = hubGuildScheduledEventData.getEvent().i();
        Long b2 = hubGuildScheduledEventData.getEvent().b();
        GuildScheduledEventUtilities.Companion companion = GuildScheduledEventUtilities.Companion;
        canShareEvent = companion.canShareEvent(b2, h, (r17 & 4) != 0 ? StoreStream.Companion.getChannels() : null, (r17 & 8) != 0 ? StoreStream.Companion.getGuilds() : null, (r17 & 16) != 0 ? StoreStream.Companion.getUsers() : null, (r17 & 32) != 0 ? StoreStream.Companion.getPermissions() : null);
        companion.launchInvite(canShareEvent, this.this$0, h, hubGuildScheduledEventData.getChannel(), i);
    }
}
