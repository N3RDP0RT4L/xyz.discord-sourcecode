package com.discord.widgets.hubs.events;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubEventsPageViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005\u0082\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventsPageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View;", "view", HookHelper.constructorName, "(Landroid/view/View;)V", "Lcom/discord/widgets/hubs/events/WidgetHubEventViewHolder;", "Lcom/discord/widgets/hubs/events/WidgetHubEventsPageHeaderViewHolder;", "Lcom/discord/widgets/hubs/events/WidgetHubEventsPageFooterViewHolder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class WidgetHubEventsPageViewHolder extends RecyclerView.ViewHolder {
    private WidgetHubEventsPageViewHolder(View view) {
        super(view);
    }

    public /* synthetic */ WidgetHubEventsPageViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
