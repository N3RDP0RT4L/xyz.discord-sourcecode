package com.discord.widgets.hubs.events;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.directory.DirectoryEntryEvent;
import com.discord.app.AppBottomSheet;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetHubEventsPageBinding;
import com.discord.stores.utilities.RestCallState;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.stores.utilities.Success;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.hubs.events.HubEventsPage;
import d0.g;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetHubEventsPage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 32\u00020\u0001:\u00013B\u0007¢\u0006\u0004\b2\u0010\u0011J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ!\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0017\u001a\u00020\u00128F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001d\u001a\u00020\u00188F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001d\u0010\"\u001a\u00020\u001e8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010\u001a\u001a\u0004\b \u0010!R\u001d\u0010'\u001a\u00020#8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b$\u0010\u001a\u001a\u0004\b%\u0010&R\u001c\u0010)\u001a\u00020(8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u0019\u0010.\u001a\u00020-8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101¨\u00064"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventsPage;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/hubs/events/WidgetHubEventsState;", "state", "", "configUI", "(Lcom/discord/widgets/hubs/events/WidgetHubEventsState;)V", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "()V", "Lcom/discord/databinding/WidgetHubEventsPageBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetHubEventsPageBinding;", "binding", "Lcom/discord/widgets/hubs/events/HubEventsArgs;", "args$delegate", "Lkotlin/Lazy;", "getArgs", "()Lcom/discord/widgets/hubs/events/HubEventsArgs;", "args", "Lcom/discord/widgets/hubs/events/WidgetHubEventsViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/hubs/events/WidgetHubEventsViewModel;", "viewModel", "Lcom/discord/widgets/hubs/events/WidgetHubEventsPageAdapter;", "adapter$delegate", "getAdapter", "()Lcom/discord/widgets/hubs/events/WidgetHubEventsPageAdapter;", "adapter", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "listener", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "getListener", "()Lcom/discord/widgets/hubs/events/HubEventsEventListener;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventsPage extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetHubEventsPage.class, "binding", "getBinding()Lcom/discord/databinding/WidgetHubEventsPageBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final Lazy viewModel$delegate;
    private final Lazy args$delegate = g.lazy(new WidgetHubEventsPage$$special$$inlined$args$1(this, "intent_args_key"));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetHubEventsPage$binding$2.INSTANCE, null, 2, null);
    private final HubEventsEventListener listener = new WidgetHubEventsPage$listener$1(this);
    private final Lazy adapter$delegate = g.lazy(new WidgetHubEventsPage$adapter$2(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, new WidgetHubEventsPage$loggingConfig$1(this), 3);

    /* compiled from: WidgetHubEventsPage.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ-\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u0007¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventsPage$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "directoryChannelId", "", "show", "(Landroidx/fragment/app/FragmentManager;JJ)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, long j, long j2) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetHubEventsPage widgetHubEventsPage = new WidgetHubEventsPage();
            widgetHubEventsPage.setArguments(d.e2(new HubEventsArgs(j, j2)));
            widgetHubEventsPage.show(fragmentManager, WidgetHubEventsPage.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetHubEventsPage() {
        super(false, 1, null);
        WidgetHubEventsPage$viewModel$2 widgetHubEventsPage$viewModel$2 = new WidgetHubEventsPage$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetHubEventsViewModel.class), new WidgetHubEventsPage$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetHubEventsPage$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configUI(WidgetHubEventsState widgetHubEventsState) {
        if (!widgetHubEventsState.getEventsData().isEmpty() || (widgetHubEventsState.getEventsAsync() instanceof Success)) {
            WidgetHubEventsPageAdapter adapter = getAdapter();
            HubEventsPage.Header header = HubEventsPage.Header.INSTANCE;
            CharSequence charSequence = null;
            if (!widgetHubEventsState.getShowHeader()) {
                header = null;
            }
            List listOfNotNull = n.listOfNotNull(header);
            List<HubGuildScheduledEventData> eventsData = widgetHubEventsState.getEventsData();
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(eventsData, 10));
            for (HubGuildScheduledEventData hubGuildScheduledEventData : eventsData) {
                arrayList.add(new HubEventsPage.Event(hubGuildScheduledEventData));
            }
            adapter.setItems(u.plus((Collection<? extends HubEventsPage.Footer>) u.plus((Collection) listOfNotNull, (Iterable) arrayList), new HubEventsPage.Footer(widgetHubEventsState.getEventsAsync())));
            TextView textView = getBinding().c;
            m.checkNotNullExpressionValue(textView, "binding.title");
            Context context = getContext();
            if (context != null) {
                charSequence = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_events_plural_number, widgetHubEventsState.getEventsData().size(), Integer.valueOf(widgetHubEventsState.getEventsData().size()));
            }
            textView.setText(charSequence);
            RestCallState<List<DirectoryEntryEvent>> eventsAsync = widgetHubEventsState.getEventsAsync();
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            RestCallStateKt.handleResponse$default(eventsAsync, requireContext, null, null, new WidgetHubEventsPage$configUI$3(this), 6, null);
        }
    }

    public final WidgetHubEventsPageAdapter getAdapter() {
        return (WidgetHubEventsPageAdapter) this.adapter$delegate.getValue();
    }

    public final HubEventsArgs getArgs() {
        return (HubEventsArgs) this.args$delegate.getValue();
    }

    public final WidgetHubEventsPageBinding getBinding() {
        return (WidgetHubEventsPageBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_hub_events_page;
    }

    public final HubEventsEventListener getListener() {
        return this.listener;
    }

    @Override // com.discord.app.AppBottomSheet, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    public final WidgetHubEventsViewModel getViewModel() {
        return (WidgetHubEventsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetHubEventsPage.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHubEventsPage$onResume$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        RecyclerView recyclerView = getBinding().f2454b;
        recyclerView.setAdapter(getAdapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
    }
}
