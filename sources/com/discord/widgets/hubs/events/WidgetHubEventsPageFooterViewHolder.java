package com.discord.widgets.hubs.events;

import andhook.lib.HookHelper;
import android.view.View;
import com.discord.databinding.WidgetHubEventsFooterBinding;
import com.discord.stores.utilities.Loading;
import com.discord.views.LoadingButton;
import com.discord.widgets.hubs.events.HubEventsPage;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetHubEventsPageViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventsPageFooterViewHolder;", "Lcom/discord/widgets/hubs/events/WidgetHubEventsPageViewHolder;", "Lcom/discord/widgets/hubs/events/HubEventsPage$Footer;", "footer", "", "bind", "(Lcom/discord/widgets/hubs/events/HubEventsPage$Footer;)V", "Lcom/discord/databinding/WidgetHubEventsFooterBinding;", "binding", "Lcom/discord/databinding/WidgetHubEventsFooterBinding;", "getBinding", "()Lcom/discord/databinding/WidgetHubEventsFooterBinding;", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "listener", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "getListener", "()Lcom/discord/widgets/hubs/events/HubEventsEventListener;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetHubEventsFooterBinding;Lcom/discord/widgets/hubs/events/HubEventsEventListener;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventsPageFooterViewHolder extends WidgetHubEventsPageViewHolder {
    private final WidgetHubEventsFooterBinding binding;
    private final HubEventsEventListener listener;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public WidgetHubEventsPageFooterViewHolder(com.discord.databinding.WidgetHubEventsFooterBinding r3, com.discord.widgets.hubs.events.HubEventsEventListener r4) {
        /*
            r2 = this;
            java.lang.String r0 = "binding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "listener"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            android.widget.LinearLayout r0 = r3.a
            java.lang.String r1 = "binding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r1 = 0
            r2.<init>(r0, r1)
            r2.binding = r3
            r2.listener = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.hubs.events.WidgetHubEventsPageFooterViewHolder.<init>(com.discord.databinding.WidgetHubEventsFooterBinding, com.discord.widgets.hubs.events.HubEventsEventListener):void");
    }

    public final void bind(final HubEventsPage.Footer footer) {
        m.checkNotNullParameter(footer, "footer");
        LoadingButton loadingButton = this.binding.f2452b;
        loadingButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.events.WidgetHubEventsPageFooterViewHolder$bind$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetHubEventsPageFooterViewHolder.this.getListener().fetchGuildScheduledEvents();
            }
        });
        loadingButton.setIsLoading(footer.getEventsAsync() instanceof Loading);
    }

    public final WidgetHubEventsFooterBinding getBinding() {
        return this.binding;
    }

    public final HubEventsEventListener getListener() {
        return this.listener;
    }
}
