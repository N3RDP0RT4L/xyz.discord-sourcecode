package com.discord.widgets.hubs.events;

import com.discord.stores.StoreChannels;
import com.discord.stores.StoreDirectories;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreVoiceChannelSelected;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetHubEventsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventsState;", "invoke", "()Lcom/discord/widgets/hubs/events/WidgetHubEventsState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventsViewModel$getObservableFromStores$1 extends o implements Function0<WidgetHubEventsState> {
    public final /* synthetic */ StoreChannels $channelsStore;
    public final /* synthetic */ StoreDirectories $directoriesStore;
    public final /* synthetic */ long $directoryChannelId;
    public final /* synthetic */ StoreGuildScheduledEvents $guildScheduledEventsStore;
    public final /* synthetic */ StoreGuilds $guildsStore;
    public final /* synthetic */ StorePermissions $permissionsStore;
    public final /* synthetic */ StoreVoiceChannelSelected $voiceChannelSelectedStore;
    public final /* synthetic */ WidgetHubEventsViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubEventsViewModel$getObservableFromStores$1(WidgetHubEventsViewModel widgetHubEventsViewModel, StoreDirectories storeDirectories, long j, StoreChannels storeChannels, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreVoiceChannelSelected storeVoiceChannelSelected, StorePermissions storePermissions, StoreGuilds storeGuilds) {
        super(0);
        this.this$0 = widgetHubEventsViewModel;
        this.$directoriesStore = storeDirectories;
        this.$directoryChannelId = j;
        this.$channelsStore = storeChannels;
        this.$guildScheduledEventsStore = storeGuildScheduledEvents;
        this.$voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.$permissionsStore = storePermissions;
        this.$guildsStore = storeGuilds;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    /* JADX WARN: Removed duplicated region for block: B:27:0x00ca  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00e6  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00f4  */
    @Override // kotlin.jvm.functions.Function0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.hubs.events.WidgetHubEventsState invoke() {
        /*
            Method dump skipped, instructions count: 306
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.hubs.events.WidgetHubEventsViewModel$getObservableFromStores$1.invoke():com.discord.widgets.hubs.events.WidgetHubEventsState");
    }
}
