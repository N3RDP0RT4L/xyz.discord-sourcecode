package com.discord.widgets.hubs.events;

import andhook.lib.HookHelper;
import android.view.View;
import com.discord.databinding.WidgetHubEventBinding;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetHubEventsPageViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventViewHolder;", "Lcom/discord/widgets/hubs/events/WidgetHubEventsPageViewHolder;", "Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;", "eventData", "", "bind", "(Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;)V", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "listener", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "getListener", "()Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "Lcom/discord/databinding/WidgetHubEventBinding;", "binding", "Lcom/discord/databinding/WidgetHubEventBinding;", "getBinding", "()Lcom/discord/databinding/WidgetHubEventBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetHubEventBinding;Lcom/discord/widgets/hubs/events/HubEventsEventListener;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventViewHolder extends WidgetHubEventsPageViewHolder {
    private final WidgetHubEventBinding binding;
    private final HubEventsEventListener listener;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public WidgetHubEventViewHolder(com.discord.databinding.WidgetHubEventBinding r3, com.discord.widgets.hubs.events.HubEventsEventListener r4) {
        /*
            r2 = this;
            java.lang.String r0 = "binding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "listener"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            com.discord.widgets.guildscheduledevent.GuildScheduledEventItemView r0 = r3.a
            java.lang.String r1 = "binding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r1 = 0
            r2.<init>(r0, r1)
            r2.binding = r3
            r2.listener = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.hubs.events.WidgetHubEventViewHolder.<init>(com.discord.databinding.WidgetHubEventBinding, com.discord.widgets.hubs.events.HubEventsEventListener):void");
    }

    public final void bind(final HubGuildScheduledEventData hubGuildScheduledEventData) {
        m.checkNotNullParameter(hubGuildScheduledEventData, "eventData");
        this.binding.f2451b.configureInDirectoryEventList(hubGuildScheduledEventData, new View.OnClickListener() { // from class: com.discord.widgets.hubs.events.WidgetHubEventViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetHubEventViewHolder.this.getListener().onCardClicked(hubGuildScheduledEventData);
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.hubs.events.WidgetHubEventViewHolder$bind$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetHubEventViewHolder.this.getListener().onSecondaryButtonClicked(hubGuildScheduledEventData);
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.hubs.events.WidgetHubEventViewHolder$bind$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetHubEventViewHolder.this.getListener().onPrimaryButtonClicked(hubGuildScheduledEventData);
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.hubs.events.WidgetHubEventViewHolder$bind$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetHubEventViewHolder.this.getListener().onShareClicked(hubGuildScheduledEventData);
            }
        });
    }

    public final WidgetHubEventBinding getBinding() {
        return this.binding;
    }

    public final HubEventsEventListener getListener() {
        return this.listener;
    }
}
