package com.discord.widgets.hubs.events;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.WidgetHubEventBinding;
import com.discord.databinding.WidgetHubEventsFooterBinding;
import com.discord.databinding.WidgetHubEventsHeaderBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.views.LoadingButton;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventItemView;
import com.discord.widgets.hubs.events.HubEventsPage;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetHubEventsPageAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0017\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b!\u0010\"J\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R6\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00190\u00182\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00190\u00188\u0006@GX\u0086\u000e¢\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 ¨\u0006#"}, d2 = {"Lcom/discord/widgets/hubs/events/WidgetHubEventsPageAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/hubs/events/WidgetHubEventsPageViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/hubs/events/WidgetHubEventsPageViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/hubs/events/WidgetHubEventsPageViewHolder;I)V", "getItemViewType", "(I)I", "getItemCount", "()I", "Landroid/content/Context;", "context", "Landroid/content/Context;", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "eventListener", "Lcom/discord/widgets/hubs/events/HubEventsEventListener;", "", "Lcom/discord/widgets/hubs/events/HubEventsPage;", "value", "items", "Ljava/util/List;", "getItems", "()Ljava/util/List;", "setItems", "(Ljava/util/List;)V", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/widgets/hubs/events/HubEventsEventListener;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventsPageAdapter extends RecyclerView.Adapter<WidgetHubEventsPageViewHolder> {
    private final Context context;
    private final HubEventsEventListener eventListener;
    private List<? extends HubEventsPage> items = n.emptyList();

    public WidgetHubEventsPageAdapter(Context context, HubEventsEventListener hubEventsEventListener) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(hubEventsEventListener, "eventListener");
        this.context = context;
        this.eventListener = hubEventsEventListener;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.items.get(i).getViewType();
    }

    public final List<HubEventsPage> getItems() {
        return this.items;
    }

    @SuppressLint({"NotifyDataSetChanged"})
    public final void setItems(List<? extends HubEventsPage> list) {
        m.checkNotNullParameter(list, "value");
        this.items = list;
        notifyDataSetChanged();
    }

    public void onBindViewHolder(WidgetHubEventsPageViewHolder widgetHubEventsPageViewHolder, int i) {
        m.checkNotNullParameter(widgetHubEventsPageViewHolder, "holder");
        if (widgetHubEventsPageViewHolder instanceof WidgetHubEventsPageHeaderViewHolder) {
            ((WidgetHubEventsPageHeaderViewHolder) widgetHubEventsPageViewHolder).bind();
        } else if (widgetHubEventsPageViewHolder instanceof WidgetHubEventsPageFooterViewHolder) {
            HubEventsPage hubEventsPage = this.items.get(i);
            Objects.requireNonNull(hubEventsPage, "null cannot be cast to non-null type com.discord.widgets.hubs.events.HubEventsPage.Footer");
            ((WidgetHubEventsPageFooterViewHolder) widgetHubEventsPageViewHolder).bind((HubEventsPage.Footer) hubEventsPage);
        } else if (widgetHubEventsPageViewHolder instanceof WidgetHubEventViewHolder) {
            HubEventsPage hubEventsPage2 = this.items.get(i);
            Objects.requireNonNull(hubEventsPage2, "null cannot be cast to non-null type com.discord.widgets.hubs.events.HubEventsPage.Event");
            ((WidgetHubEventViewHolder) widgetHubEventsPageViewHolder).bind(((HubEventsPage.Event) hubEventsPage2).getGuildScheduledEventData());
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public WidgetHubEventsPageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(this.context);
        if (i == 0) {
            View inflate = from.inflate(R.layout.widget_hub_events_header, viewGroup, false);
            int i2 = R.id.close;
            ImageView imageView = (ImageView) inflate.findViewById(R.id.close);
            if (imageView != null) {
                i2 = R.id.description;
                TextView textView = (TextView) inflate.findViewById(R.id.description);
                if (textView != null) {
                    i2 = R.id.image;
                    ImageView imageView2 = (ImageView) inflate.findViewById(R.id.image);
                    if (imageView2 != null) {
                        i2 = R.id.title;
                        TextView textView2 = (TextView) inflate.findViewById(R.id.title);
                        if (textView2 != null) {
                            WidgetHubEventsHeaderBinding widgetHubEventsHeaderBinding = new WidgetHubEventsHeaderBinding((ConstraintLayout) inflate, imageView, textView, imageView2, textView2);
                            m.checkNotNullExpressionValue(widgetHubEventsHeaderBinding, "WidgetHubEventsHeaderBin…(inflater, parent, false)");
                            return new WidgetHubEventsPageHeaderViewHolder(widgetHubEventsHeaderBinding, this.eventListener);
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
        } else if (i == 1) {
            View inflate2 = from.inflate(R.layout.widget_hub_events_footer, viewGroup, false);
            LoadingButton loadingButton = (LoadingButton) inflate2.findViewById(R.id.button);
            if (loadingButton != null) {
                WidgetHubEventsFooterBinding widgetHubEventsFooterBinding = new WidgetHubEventsFooterBinding((LinearLayout) inflate2, loadingButton);
                m.checkNotNullExpressionValue(widgetHubEventsFooterBinding, "WidgetHubEventsFooterBin…(inflater, parent, false)");
                return new WidgetHubEventsPageFooterViewHolder(widgetHubEventsFooterBinding, this.eventListener);
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(R.id.button)));
        } else if (i == 2) {
            View inflate3 = from.inflate(R.layout.widget_hub_event, viewGroup, false);
            Objects.requireNonNull(inflate3, "rootView");
            GuildScheduledEventItemView guildScheduledEventItemView = (GuildScheduledEventItemView) inflate3;
            WidgetHubEventBinding widgetHubEventBinding = new WidgetHubEventBinding(guildScheduledEventItemView, guildScheduledEventItemView);
            m.checkNotNullExpressionValue(widgetHubEventBinding, "WidgetHubEventBinding.in…(inflater, parent, false)");
            return new WidgetHubEventViewHolder(widgetHubEventBinding, this.eventListener);
        } else {
            throw new IllegalStateException(a.p("Invalid view type: ", i));
        }
    }
}
