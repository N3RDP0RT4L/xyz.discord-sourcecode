package com.discord.widgets.hubs.events;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetHubEventsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001BU\u0012\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0015\u001a\u00020\u0006\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u0017\u001a\u00020\f\u0012\u0006\u0010\u0018\u001a\u00020\f\u0012\u0006\u0010\u0019\u001a\u00020\f\u0012\u0006\u0010\u001a\u001a\u00020\f\u0012\u0006\u0010\u001b\u001a\u00020\f\u0012\u0006\u0010\u001c\u001a\u00020\f¢\u0006\u0004\b2\u00103J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000eJ\u0010\u0010\u0011\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000eJ\u0010\u0010\u0012\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000eJ\u0010\u0010\u0013\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000eJp\u0010\u001d\u001a\u00020\u00002\f\b\u0002\u0010\u0014\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0015\u001a\u00020\u00062\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u0017\u001a\u00020\f2\b\b\u0002\u0010\u0018\u001a\u00020\f2\b\b\u0002\u0010\u0019\u001a\u00020\f2\b\b\u0002\u0010\u001a\u001a\u00020\f2\b\b\u0002\u0010\u001b\u001a\u00020\f2\b\b\u0002\u0010\u001c\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010&\u001a\u00020\f2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b&\u0010'R\u0019\u0010\u0019\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010(\u001a\u0004\b)\u0010\u000eR\u0019\u0010\u001a\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010(\u001a\u0004\b*\u0010\u000eR\u001b\u0010\u0016\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010+\u001a\u0004\b,\u0010\u000bR\u0019\u0010\u0018\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b-\u0010\u000eR\u0019\u0010\u0017\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010(\u001a\u0004\b\u0017\u0010\u000eR\u0019\u0010\u001b\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010(\u001a\u0004\b\u001b\u0010\u000eR\u001d\u0010\u0014\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010.\u001a\u0004\b/\u0010\u0005R\u0019\u0010\u001c\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010(\u001a\u0004\b\u001c\u0010\u000eR\u0019\u0010\u0015\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u00100\u001a\u0004\b1\u0010\b¨\u00064"}, d2 = {"Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component2", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "", "component4", "()Z", "component5", "component6", "component7", "component8", "component9", "directoryChannelId", "event", "channel", "isRsvped", "canShare", "canStartEvent", "canConnect", "isConnected", "isInGuild", "copy", "(JLcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;ZZZZZZ)Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanStartEvent", "getCanConnect", "Lcom/discord/api/channel/Channel;", "getChannel", "getCanShare", "J", "getDirectoryChannelId", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getEvent", HookHelper.constructorName, "(JLcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;ZZZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubGuildScheduledEventData {
    private final boolean canConnect;
    private final boolean canShare;
    private final boolean canStartEvent;
    private final Channel channel;
    private final long directoryChannelId;
    private final GuildScheduledEvent event;
    private final boolean isConnected;
    private final boolean isInGuild;
    private final boolean isRsvped;

    public HubGuildScheduledEventData(long j, GuildScheduledEvent guildScheduledEvent, Channel channel, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        m.checkNotNullParameter(guildScheduledEvent, "event");
        this.directoryChannelId = j;
        this.event = guildScheduledEvent;
        this.channel = channel;
        this.isRsvped = z2;
        this.canShare = z3;
        this.canStartEvent = z4;
        this.canConnect = z5;
        this.isConnected = z6;
        this.isInGuild = z7;
    }

    public final long component1() {
        return this.directoryChannelId;
    }

    public final GuildScheduledEvent component2() {
        return this.event;
    }

    public final Channel component3() {
        return this.channel;
    }

    public final boolean component4() {
        return this.isRsvped;
    }

    public final boolean component5() {
        return this.canShare;
    }

    public final boolean component6() {
        return this.canStartEvent;
    }

    public final boolean component7() {
        return this.canConnect;
    }

    public final boolean component8() {
        return this.isConnected;
    }

    public final boolean component9() {
        return this.isInGuild;
    }

    public final HubGuildScheduledEventData copy(long j, GuildScheduledEvent guildScheduledEvent, Channel channel, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        m.checkNotNullParameter(guildScheduledEvent, "event");
        return new HubGuildScheduledEventData(j, guildScheduledEvent, channel, z2, z3, z4, z5, z6, z7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HubGuildScheduledEventData)) {
            return false;
        }
        HubGuildScheduledEventData hubGuildScheduledEventData = (HubGuildScheduledEventData) obj;
        return this.directoryChannelId == hubGuildScheduledEventData.directoryChannelId && m.areEqual(this.event, hubGuildScheduledEventData.event) && m.areEqual(this.channel, hubGuildScheduledEventData.channel) && this.isRsvped == hubGuildScheduledEventData.isRsvped && this.canShare == hubGuildScheduledEventData.canShare && this.canStartEvent == hubGuildScheduledEventData.canStartEvent && this.canConnect == hubGuildScheduledEventData.canConnect && this.isConnected == hubGuildScheduledEventData.isConnected && this.isInGuild == hubGuildScheduledEventData.isInGuild;
    }

    public final boolean getCanConnect() {
        return this.canConnect;
    }

    public final boolean getCanShare() {
        return this.canShare;
    }

    public final boolean getCanStartEvent() {
        return this.canStartEvent;
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final long getDirectoryChannelId() {
        return this.directoryChannelId;
    }

    public final GuildScheduledEvent getEvent() {
        return this.event;
    }

    public int hashCode() {
        int a = b.a(this.directoryChannelId) * 31;
        GuildScheduledEvent guildScheduledEvent = this.event;
        int i = 0;
        int hashCode = (a + (guildScheduledEvent != null ? guildScheduledEvent.hashCode() : 0)) * 31;
        Channel channel = this.channel;
        if (channel != null) {
            i = channel.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z2 = this.isRsvped;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z3 = this.canShare;
        if (z3) {
            z3 = true;
        }
        int i7 = z3 ? 1 : 0;
        int i8 = z3 ? 1 : 0;
        int i9 = (i6 + i7) * 31;
        boolean z4 = this.canStartEvent;
        if (z4) {
            z4 = true;
        }
        int i10 = z4 ? 1 : 0;
        int i11 = z4 ? 1 : 0;
        int i12 = (i9 + i10) * 31;
        boolean z5 = this.canConnect;
        if (z5) {
            z5 = true;
        }
        int i13 = z5 ? 1 : 0;
        int i14 = z5 ? 1 : 0;
        int i15 = (i12 + i13) * 31;
        boolean z6 = this.isConnected;
        if (z6) {
            z6 = true;
        }
        int i16 = z6 ? 1 : 0;
        int i17 = z6 ? 1 : 0;
        int i18 = (i15 + i16) * 31;
        boolean z7 = this.isInGuild;
        if (!z7) {
            i3 = z7 ? 1 : 0;
        }
        return i18 + i3;
    }

    public final boolean isConnected() {
        return this.isConnected;
    }

    public final boolean isInGuild() {
        return this.isInGuild;
    }

    public final boolean isRsvped() {
        return this.isRsvped;
    }

    public String toString() {
        StringBuilder R = a.R("HubGuildScheduledEventData(directoryChannelId=");
        R.append(this.directoryChannelId);
        R.append(", event=");
        R.append(this.event);
        R.append(", channel=");
        R.append(this.channel);
        R.append(", isRsvped=");
        R.append(this.isRsvped);
        R.append(", canShare=");
        R.append(this.canShare);
        R.append(", canStartEvent=");
        R.append(this.canStartEvent);
        R.append(", canConnect=");
        R.append(this.canConnect);
        R.append(", isConnected=");
        R.append(this.isConnected);
        R.append(", isInGuild=");
        return a.M(R, this.isInGuild, ")");
    }
}
