package com.discord.widgets.hubs.events;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetHubEventsPage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEventsPage$listener$1$onPrimaryButtonClicked$1 extends o implements Function0<Unit> {
    public final /* synthetic */ HubGuildScheduledEventData $eventData;
    public final /* synthetic */ WidgetHubEventsPage$listener$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubEventsPage$listener$1$onPrimaryButtonClicked$1(WidgetHubEventsPage$listener$1 widgetHubEventsPage$listener$1, HubGuildScheduledEventData hubGuildScheduledEventData) {
        super(0);
        this.this$0 = widgetHubEventsPage$listener$1;
        this.$eventData = hubGuildScheduledEventData;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.this$0.getViewModel().toggleRsvp(this.$eventData.getEvent());
    }
}
