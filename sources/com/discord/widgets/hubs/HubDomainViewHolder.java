package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.TextView;
import com.discord.databinding.DiscordHubAddServerListItemBinding;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubDomains.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR%\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/hubs/HubDomainViewHolder;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;", "Lcom/discord/widgets/hubs/DomainGuildInfo;", "data", "", "bind", "(Lcom/discord/widgets/hubs/DomainGuildInfo;)V", "Lcom/discord/databinding/DiscordHubAddServerListItemBinding;", "binding", "Lcom/discord/databinding/DiscordHubAddServerListItemBinding;", "getBinding", "()Lcom/discord/databinding/DiscordHubAddServerListItemBinding;", "Lkotlin/Function1;", "onClickListener", "Lkotlin/jvm/functions/Function1;", "getOnClickListener", "()Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lcom/discord/databinding/DiscordHubAddServerListItemBinding;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubDomainViewHolder extends SimpleRecyclerAdapter.ViewHolder<DomainGuildInfo> {
    private final DiscordHubAddServerListItemBinding binding;
    private final Function1<DomainGuildInfo, Unit> onClickListener;

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public HubDomainViewHolder(com.discord.databinding.DiscordHubAddServerListItemBinding r3, kotlin.jvm.functions.Function1<? super com.discord.widgets.hubs.DomainGuildInfo, kotlin.Unit> r4) {
        /*
            r2 = this;
            java.lang.String r0 = "binding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "onClickListener"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            android.widget.FrameLayout r0 = r3.a
            java.lang.String r1 = "binding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.binding = r3
            r2.onClickListener = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.hubs.HubDomainViewHolder.<init>(com.discord.databinding.DiscordHubAddServerListItemBinding, kotlin.jvm.functions.Function1):void");
    }

    public final DiscordHubAddServerListItemBinding getBinding() {
        return this.binding;
    }

    public final Function1<DomainGuildInfo, Unit> getOnClickListener() {
        return this.onClickListener;
    }

    public void bind(final DomainGuildInfo domainGuildInfo) {
        m.checkNotNullParameter(domainGuildInfo, "data");
        this.binding.f2086b.b();
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.discordUAddServerListItemText");
        textView.setText(domainGuildInfo.getName());
        this.binding.f2086b.a(domainGuildInfo.getIcon(), GuildUtilsKt.computeShortName(domainGuildInfo.getName()));
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.HubDomainViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                HubDomainViewHolder.this.getOnClickListener().invoke(domainGuildInfo);
            }
        });
    }
}
