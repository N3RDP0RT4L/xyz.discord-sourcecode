package com.discord.widgets.hubs;

import android.content.Context;
import b.a.d.j;
import com.discord.stores.utilities.Success;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubDomains.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/utilities/Success;", "", "it", "", "invoke", "(Lcom/discord/stores/utilities/Success;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubDomains$updateView$1 extends o implements Function1<Success<? extends Object>, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ DomainsState $state;
    public final /* synthetic */ WidgetHubDomains this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubDomains$updateView$1(WidgetHubDomains widgetHubDomains, Context context, DomainsState domainsState) {
        super(1);
        this.this$0 = widgetHubDomains;
        this.$context = context;
        this.$state = domainsState;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Success<? extends Object> success) {
        invoke2(success);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Success<? extends Object> success) {
        m.checkNotNullParameter(success, "it");
        j.g(j.g, this.this$0.getParentFragmentManager(), this.$context, WidgetHubAuthentication.class, 0, true, null, new HubAuthenticationArgs(this.this$0.getArgs().getEmail(), this.$state.getSelectedGuildId()), 40);
        this.this$0.getViewModel().reset();
    }
}
