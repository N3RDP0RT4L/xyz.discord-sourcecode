package com.discord.widgets.hubs;

import com.discord.api.hubs.EmailVerification;
import com.discord.stores.utilities.RestCallState;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubAuthenticationlViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/hubs/EmailVerification;", "it", "", "invoke", "(Lcom/discord/stores/utilities/RestCallState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAuthenticationViewModel$resendEmail$2 extends o implements Function1<RestCallState<? extends EmailVerification>, Unit> {
    public final /* synthetic */ WidgetHubAuthenticationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubAuthenticationViewModel$resendEmail$2(WidgetHubAuthenticationViewModel widgetHubAuthenticationViewModel) {
        super(1);
        this.this$0 = widgetHubAuthenticationViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestCallState<? extends EmailVerification> restCallState) {
        invoke2((RestCallState<EmailVerification>) restCallState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestCallState<EmailVerification> restCallState) {
        HubAuthenticationState requireViewState;
        m.checkNotNullParameter(restCallState, "it");
        WidgetHubAuthenticationViewModel widgetHubAuthenticationViewModel = this.this$0;
        requireViewState = widgetHubAuthenticationViewModel.requireViewState();
        widgetHubAuthenticationViewModel.updateViewState(HubAuthenticationState.copy$default(requireViewState, null, restCallState, null, 5, null));
    }
}
