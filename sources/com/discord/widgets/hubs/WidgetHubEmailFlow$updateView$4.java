package com.discord.widgets.hubs;

import android.content.Context;
import android.content.Intent;
import androidx.activity.result.ActivityResultLauncher;
import b.a.d.j;
import com.discord.api.hubs.EmailVerification;
import com.discord.api.hubs.GuildInfo;
import com.discord.stores.StoreStream;
import com.discord.stores.utilities.Success;
import com.discord.utilities.features.GrowthTeamFeatures;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubEmailFlow.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/utilities/Success;", "Lcom/discord/api/hubs/EmailVerification;", "it", "", "invoke", "(Lcom/discord/stores/utilities/Success;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEmailFlow$updateView$4 extends o implements Function1<Success<? extends EmailVerification>, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ String $email;
    public final /* synthetic */ HubEmailState $state;
    public final /* synthetic */ WidgetHubEmailFlow this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubEmailFlow$updateView$4(WidgetHubEmailFlow widgetHubEmailFlow, HubEmailState hubEmailState, Context context, String str) {
        super(1);
        this.this$0 = widgetHubEmailFlow;
        this.$state = hubEmailState;
        this.$context = context;
        this.$email = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Success<? extends EmailVerification> success) {
        invoke2((Success<EmailVerification>) success);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Success<EmailVerification> success) {
        ActivityResultLauncher<Intent> activityResultLauncher;
        WidgetHubEmailViewModel viewModel;
        ActivityResultLauncher<Intent> activityResultLauncher2;
        WidgetHubEmailViewModel viewModel2;
        m.checkNotNullParameter(success, "it");
        StoreStream.Companion.getDirectories().markDiscordHubClicked();
        if (this.$state.getHasMultipleDomains()) {
            j jVar = j.g;
            Context context = this.$context;
            activityResultLauncher2 = this.this$0.activityResultHandler;
            String str = this.$email;
            List<GuildInfo> a = success.invoke().a();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(a, 10));
            for (GuildInfo guildInfo : a) {
                arrayList.add(DomainGuildInfo.Companion.from(guildInfo));
            }
            jVar.f(context, activityResultLauncher2, WidgetHubDomains.class, new HubDomainArgs(str, arrayList));
            viewModel2 = this.this$0.getViewModel();
            viewModel2.reset();
            return;
        }
        EmailVerification invoke = this.$state.getVerifyEmailAsync().invoke();
        if (!(invoke == null || invoke.b() || !GrowthTeamFeatures.INSTANCE.isMultiDomainEnabled())) {
            j jVar2 = j.g;
            Context context2 = this.$context;
            activityResultLauncher = this.this$0.activityResultHandler;
            jVar2.f(context2, activityResultLauncher, WidgetHubWaitlist.class, new HubWaitlistArgs(this.$email));
            viewModel = this.this$0.getViewModel();
            viewModel.reset();
        }
    }
}
