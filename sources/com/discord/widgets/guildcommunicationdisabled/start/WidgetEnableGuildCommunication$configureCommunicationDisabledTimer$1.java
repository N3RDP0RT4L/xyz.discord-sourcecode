package com.discord.widgets.guildcommunicationdisabled.start;

import android.content.Context;
import android.widget.TextView;
import b.a.k.b;
import com.discord.databinding.WidgetEnableGuildCommunicationBinding;
import com.discord.utilities.duration.DurationUtilsKt;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetEnableGuildCommunication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/lang/Long;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEnableGuildCommunication$configureCommunicationDisabledTimer$1 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ long $communicationDisabledTimestampMs;
    public final /* synthetic */ CharSequence $username;
    public final /* synthetic */ WidgetEnableGuildCommunication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEnableGuildCommunication$configureCommunicationDisabledTimer$1(WidgetEnableGuildCommunication widgetEnableGuildCommunication, long j, CharSequence charSequence) {
        super(1);
        this.this$0 = widgetEnableGuildCommunication;
        this.$communicationDisabledTimestampMs = j;
        this.$username = charSequence;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke2(l);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Long l) {
        Clock clock;
        WidgetEnableGuildCommunicationBinding binding;
        long j = this.$communicationDisabledTimestampMs;
        clock = this.this$0.clock;
        long max = Math.max(j - clock.currentTimeMillis(), 0L);
        binding = this.this$0.getBinding();
        TextView textView = binding.d;
        m.checkNotNullExpressionValue(textView, "binding.enableGuildCommunicationBody");
        Context requireContext = this.this$0.requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        b.m(textView, R.string.enable_guild_communication_body, new Object[]{this.$username, DurationUtilsKt.humanizeCountdownDuration(requireContext, max)}, (r4 & 4) != 0 ? b.g.j : null);
    }
}
