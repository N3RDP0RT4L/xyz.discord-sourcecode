package com.discord.widgets.guildcommunicationdisabled.start;

import com.discord.analytics.generated.events.network_action.TrackNetworkActionUserCommunicationDisabledUpdate;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: DisableGuildCommunicationViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Ljava/lang/Void;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DisableGuildCommunicationViewModel$onDisableCommunicationConfirm$1 extends o implements Function1<Void, TrackNetworkMetadataReceiver> {
    public final /* synthetic */ long $disabledUntilTimestamp;
    public final /* synthetic */ long $durationS;
    public final /* synthetic */ String $reason;
    public final /* synthetic */ DisableGuildCommunicationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DisableGuildCommunicationViewModel$onDisableCommunicationConfirm$1(DisableGuildCommunicationViewModel disableGuildCommunicationViewModel, long j, long j2, String str) {
        super(1);
        this.this$0 = disableGuildCommunicationViewModel;
        this.$durationS = j;
        this.$disabledUntilTimestamp = j2;
        this.$reason = str;
    }

    public final TrackNetworkMetadataReceiver invoke(Void r7) {
        long j;
        long j2;
        j = this.this$0.guildId;
        Long valueOf = Long.valueOf(j);
        j2 = this.this$0.userId;
        return new TrackNetworkActionUserCommunicationDisabledUpdate(valueOf, Long.valueOf(j2), Float.valueOf((float) this.$durationS), this.$reason, Long.valueOf(this.$disabledUntilTimestamp));
    }
}
