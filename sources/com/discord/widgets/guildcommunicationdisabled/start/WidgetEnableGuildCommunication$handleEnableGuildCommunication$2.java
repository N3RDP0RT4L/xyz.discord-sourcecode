package com.discord.widgets.guildcommunicationdisabled.start;

import android.content.Context;
import b.a.d.m;
import b.a.k.b;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetEnableGuildCommunication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEnableGuildCommunication$handleEnableGuildCommunication$2 extends o implements Function1<Void, Unit> {
    public final /* synthetic */ WidgetEnableGuildCommunication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEnableGuildCommunication$handleEnableGuildCommunication$2(WidgetEnableGuildCommunication widgetEnableGuildCommunication) {
        super(1);
        this.this$0 = widgetEnableGuildCommunication;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
        invoke2(r1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Void r7) {
        CharSequence charSequence;
        this.this$0.dismiss();
        Context context = this.this$0.getContext();
        Context context2 = this.this$0.getContext();
        if (context2 != null) {
            charSequence = b.b(context2, R.string.guild_enable_communication_success, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        } else {
            charSequence = null;
        }
        m.h(context, charSequence, 0, null, 12);
    }
}
