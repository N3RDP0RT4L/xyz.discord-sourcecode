package com.discord.widgets.guildcommunicationdisabled.start;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetDisableGuildCommunicationBinding;
import com.discord.models.user.User;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.guildcommunicationdisabled.start.DisableGuildCommunicationViewModel;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.ref.WeakReference;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetDisableGuildCommunication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 %2\u00020\u0001:\u0001%B\u0007¢\u0006\u0004\b$\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0015\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001b\u001a\u00020\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010 \u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u0012\u001a\u0004\b\u001e\u0010\u001fR\u001d\u0010#\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\u0012\u001a\u0004\b\"\u0010\u0014¨\u0006&"}, d2 = {"Lcom/discord/widgets/guildcommunicationdisabled/start/WidgetDisableGuildCommunication;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guildcommunicationdisabled/start/DisableGuildCommunicationViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/guildcommunicationdisabled/start/DisableGuildCommunicationViewModel$ViewState;)V", "Lcom/discord/widgets/guildcommunicationdisabled/start/DisableGuildCommunicationViewModel$ViewState$Valid;", "Lcom/discord/views/CheckedSetting;", "setting", "Lcom/discord/widgets/guildcommunicationdisabled/start/TimeDurationDisabledCommunication;", "settingValue", "configureDurationOption", "(Lcom/discord/widgets/guildcommunicationdisabled/start/DisableGuildCommunicationViewModel$ViewState$Valid;Lcom/discord/views/CheckedSetting;Lcom/discord/widgets/guildcommunicationdisabled/start/TimeDurationDisabledCommunication;)V", "onResume", "()V", "", "userId$delegate", "Lkotlin/Lazy;", "getUserId", "()J", "userId", "Lcom/discord/databinding/WidgetDisableGuildCommunicationBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetDisableGuildCommunicationBinding;", "binding", "Lcom/discord/widgets/guildcommunicationdisabled/start/DisableGuildCommunicationViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/guildcommunicationdisabled/start/DisableGuildCommunicationViewModel;", "viewModel", "guildId$delegate", "getGuildId", "guildId", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDisableGuildCommunication extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetDisableGuildCommunication.class, "binding", "getBinding()Lcom/discord/databinding/WidgetDisableGuildCommunicationBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String INTENT_EXTRA_USER_ID = "INTENT_EXTRA_USER_ID";
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetDisableGuildCommunication$binding$2.INSTANCE, null, 2, null);
    private final Lazy userId$delegate = g.lazy(new WidgetDisableGuildCommunication$userId$2(this));
    private final Lazy guildId$delegate = g.lazy(new WidgetDisableGuildCommunication$guildId$2(this));

    /* compiled from: WidgetDisableGuildCommunication.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ'\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/guildcommunicationdisabled/start/WidgetDisableGuildCommunication$Companion;", "", "", "userId", "guildId", "Landroid/content/Context;", "context", "", "launch", "(JJLandroid/content/Context;)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", WidgetDisableGuildCommunication.INTENT_EXTRA_USER_ID, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, long j2, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra("INTENT_EXTRA_GUILD_ID", j2);
            intent.putExtra(WidgetDisableGuildCommunication.INTENT_EXTRA_USER_ID, j);
            j.d(context, WidgetDisableGuildCommunication.class, intent);
            AnalyticsTracker.INSTANCE.viewedDisableCommunicationModal(j2, j);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetDisableGuildCommunication() {
        super(R.layout.widget_disable_guild_communication);
        WidgetDisableGuildCommunication$viewModel$2 widgetDisableGuildCommunication$viewModel$2 = new WidgetDisableGuildCommunication$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(DisableGuildCommunicationViewModel.class), new WidgetDisableGuildCommunication$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetDisableGuildCommunication$viewModel$2));
    }

    private final void configureDurationOption(DisableGuildCommunicationViewModel.ViewState.Valid valid, CheckedSetting checkedSetting, final TimeDurationDisabledCommunication timeDurationDisabledCommunication) {
        checkedSetting.setChecked(valid.getSelectedDurationOption() == timeDurationDisabledCommunication);
        checkedSetting.setText(GuildCommunicationDisabledDateUtils.INSTANCE.getFriendlyDurationString(getContext(), timeDurationDisabledCommunication));
        checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.guildcommunicationdisabled.start.WidgetDisableGuildCommunication$configureDurationOption$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetDisableGuildCommunicationBinding binding;
                DisableGuildCommunicationViewModel viewModel;
                binding = WidgetDisableGuildCommunication.this.getBinding();
                binding.d.clearFocus();
                AppFragment.hideKeyboard$default(WidgetDisableGuildCommunication.this, null, 1, null);
                viewModel = WidgetDisableGuildCommunication.this.getViewModel();
                viewModel.onDurationLengthSelected(timeDurationDisabledCommunication);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final DisableGuildCommunicationViewModel.ViewState viewState) {
        CharSequence charSequence;
        if (viewState instanceof DisableGuildCommunicationViewModel.ViewState.Valid) {
            DisableGuildCommunicationViewModel.ViewState.Valid valid = (DisableGuildCommunicationViewModel.ViewState.Valid) viewState;
            CheckedSetting checkedSetting = getBinding().k;
            m.checkNotNullExpressionValue(checkedSetting, "binding.timeUnit60Seconds");
            configureDurationOption(valid, checkedSetting, TimeDurationDisabledCommunication.SECONDS_60);
            CheckedSetting checkedSetting2 = getBinding().j;
            m.checkNotNullExpressionValue(checkedSetting2, "binding.timeUnit5Minutes");
            configureDurationOption(valid, checkedSetting2, TimeDurationDisabledCommunication.MINUTES_5);
            CheckedSetting checkedSetting3 = getBinding().f;
            m.checkNotNullExpressionValue(checkedSetting3, "binding.timeUnit10Minutes");
            configureDurationOption(valid, checkedSetting3, TimeDurationDisabledCommunication.MINUTES_10);
            CheckedSetting checkedSetting4 = getBinding().h;
            m.checkNotNullExpressionValue(checkedSetting4, "binding.timeUnit1Hour");
            configureDurationOption(valid, checkedSetting4, TimeDurationDisabledCommunication.HOUR_1);
            CheckedSetting checkedSetting5 = getBinding().g;
            m.checkNotNullExpressionValue(checkedSetting5, "binding.timeUnit1Day");
            configureDurationOption(valid, checkedSetting5, TimeDurationDisabledCommunication.DAY_1);
            CheckedSetting checkedSetting6 = getBinding().i;
            m.checkNotNullExpressionValue(checkedSetting6, "binding.timeUnit1Week");
            configureDurationOption(valid, checkedSetting6, TimeDurationDisabledCommunication.WEEK_1);
            Context context = getContext();
            if (context != null) {
                charSequence = b.b(context, R.string.disable_guild_communication_body_header, new Object[]{f.a.a(4413305239191L, null)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            } else {
                charSequence = null;
            }
            TextView textView = getBinding().c;
            m.checkNotNullExpressionValue(textView, "binding.disableGuildCommunicationBody");
            textView.setText(charSequence);
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildcommunicationdisabled.start.WidgetDisableGuildCommunication$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetDisableGuildCommunicationBinding binding;
                    UriHandler uriHandler = UriHandler.INSTANCE;
                    binding = WidgetDisableGuildCommunication.this.getBinding();
                    TextView textView2 = binding.c;
                    m.checkNotNullExpressionValue(textView2, "binding.disableGuildCommunicationBody");
                    Context context2 = textView2.getContext();
                    m.checkNotNullExpressionValue(context2, "binding.disableGuildCommunicationBody.context");
                    UriHandler.handle$default(uriHandler, context2, f.a.a(4413305239191L, null), null, 4, null);
                }
            });
            TextView textView2 = getBinding().e;
            m.checkNotNullExpressionValue(textView2, "binding.disableGuildCommunicationSubtitle");
            User user = valid.getUser();
            b.o(textView2, user != null ? UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user, null, null, 3, null) : null, new Object[0], null, 4);
            getBinding().f2347b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildcommunicationdisabled.start.WidgetDisableGuildCommunication$configureUI$2

                /* compiled from: WidgetDisableGuildCommunication.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.guildcommunicationdisabled.start.WidgetDisableGuildCommunication$configureUI$2$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function0<Unit> {
                    public AnonymousClass1() {
                        super(0);
                    }

                    @Override // kotlin.jvm.functions.Function0
                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2() {
                        CharSequence charSequence;
                        Context context = WidgetDisableGuildCommunication.this.getContext();
                        Context context2 = WidgetDisableGuildCommunication.this.getContext();
                        if (context2 != null) {
                            Object[] objArr = new Object[2];
                            User user = ((DisableGuildCommunicationViewModel.ViewState.Valid) viewState).getUser();
                            objArr[0] = user != null ? UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user, null, null, 3, null) : null;
                            objArr[1] = GuildCommunicationDisabledDateUtils.INSTANCE.getFriendlyDurationString(WidgetDisableGuildCommunication.this.getContext(), ((DisableGuildCommunicationViewModel.ViewState.Valid) viewState).getSelectedDurationOption());
                            charSequence = b.b(context2, R.string.disable_guild_communication_confirmed, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
                        } else {
                            charSequence = null;
                        }
                        b.a.d.m.h(context, charSequence, 0, null, 12);
                        AppActivity appActivity = WidgetDisableGuildCommunication.this.getAppActivity();
                        if (appActivity != null) {
                            appActivity.finish();
                        }
                    }
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    DisableGuildCommunicationViewModel viewModel;
                    WidgetDisableGuildCommunicationBinding binding;
                    WeakReference<Context> weakReference = new WeakReference<>(WidgetDisableGuildCommunication.this.requireContext());
                    viewModel = WidgetDisableGuildCommunication.this.getViewModel();
                    binding = WidgetDisableGuildCommunication.this.getBinding();
                    TextInputLayout textInputLayout = binding.d;
                    m.checkNotNullExpressionValue(textInputLayout, "binding.disableGuildCommunicationReason");
                    viewModel.onDisableCommunicationConfirm(weakReference, ViewExtensions.getTextOrEmpty(textInputLayout), new AnonymousClass1());
                }
            });
            TextInputLayout textInputLayout = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout, "binding.disableGuildCommunicationReason");
            EditText editText = textInputLayout.getEditText();
            if (editText != null) {
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: com.discord.widgets.guildcommunicationdisabled.start.WidgetDisableGuildCommunication$configureUI$$inlined$apply$lambda$1
                    @Override // android.view.View.OnFocusChangeListener
                    public final void onFocusChange(View view, boolean z2) {
                        WidgetDisableGuildCommunicationBinding binding;
                        WidgetDisableGuildCommunicationBinding binding2;
                        if (z2) {
                            binding2 = WidgetDisableGuildCommunication.this.getBinding();
                            TextInputLayout textInputLayout2 = binding2.d;
                            m.checkNotNullExpressionValue(textInputLayout2, "binding.disableGuildCommunicationReason");
                            textInputLayout2.setHint("");
                            return;
                        }
                        binding = WidgetDisableGuildCommunication.this.getBinding();
                        binding.d.setHint(R.string.guild_communication_disabled_reason_hint_android);
                    }
                });
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetDisableGuildCommunicationBinding getBinding() {
        return (WidgetDisableGuildCommunicationBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getUserId() {
        return ((Number) this.userId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final DisableGuildCommunicationViewModel getViewModel() {
        return (DisableGuildCommunicationViewModel) this.viewModel$delegate.getValue();
    }

    public static final void launch(long j, long j2, Context context) {
        Companion.launch(j, j2, context);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetDisableGuildCommunication.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDisableGuildCommunication$onResume$1(this));
    }
}
