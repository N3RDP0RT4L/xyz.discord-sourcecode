package com.discord.widgets.guildcommunicationdisabled.start;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.f;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetEnableGuildCommunicationBinding;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.duration.DurationUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetEnableGuildCommunication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0007¢\u0006\u0004\b!\u0010\u0014J!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ'\u0010\r\u001a\u00020\u00062\n\u0010\n\u001a\u00060\u0002j\u0002`\t2\n\u0010\f\u001a\u00060\u0002j\u0002`\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0018\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u001d\u0010 \u001a\u00020\u001b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lcom/discord/widgets/guildcommunicationdisabled/start/WidgetEnableGuildCommunication;", "Lcom/discord/app/AppDialog;", "", "communicationDisabledTimestampMs", "", "username", "", "configureCommunicationDisabledTimer", "(JLjava/lang/CharSequence;)V", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "handleEnableGuildCommunication", "(JJ)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onDestroy", "()V", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lrx/Subscription;", "communicationDisabledCountdownSubscription", "Lrx/Subscription;", "Lcom/discord/databinding/WidgetEnableGuildCommunicationBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetEnableGuildCommunicationBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEnableGuildCommunication extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetEnableGuildCommunication.class, "binding", "getBinding()Lcom/discord/databinding/WidgetEnableGuildCommunicationBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetEnableGuildCommunication$binding$2.INSTANCE, null, 2, null);
    private final Clock clock = ClockFactory.get();
    private Subscription communicationDisabledCountdownSubscription;

    /* compiled from: WidgetEnableGuildCommunication.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ/\u0010\n\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/guildcommunicationdisabled/start/WidgetEnableGuildCommunication$Companion;", "", "", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/primitives/GuildId;", "guildId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "launch", "(JJLandroidx/fragment/app/FragmentManager;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, long j2, FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetEnableGuildCommunication widgetEnableGuildCommunication = new WidgetEnableGuildCommunication();
            Bundle I = a.I("com.discord.intent.extra.EXTRA_USER_ID", j);
            I.putLong("com.discord.intent.extra.EXTRA_GUILD_ID", j2);
            widgetEnableGuildCommunication.setArguments(I);
            widgetEnableGuildCommunication.show(fragmentManager, WidgetEnableGuildCommunication.class.getSimpleName());
            AnalyticsTracker.INSTANCE.viewedEnableCommunicationModal(j2, j);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetEnableGuildCommunication() {
        super(R.layout.widget_enable_guild_communication);
    }

    private final void configureCommunicationDisabledTimer(long j, CharSequence charSequence) {
        if (this.communicationDisabledCountdownSubscription == null) {
            Observable<Long> D = Observable.D(0L, 1L, TimeUnit.SECONDS);
            m.checkNotNullExpressionValue(D, "Observable\n        .inte…0L, 1L, TimeUnit.SECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(D, this, null, 2, null), WidgetEnableGuildCommunication.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetEnableGuildCommunication$configureCommunicationDisabledTimer$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEnableGuildCommunication$configureCommunicationDisabledTimer$1(this, j, charSequence));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetEnableGuildCommunicationBinding getBinding() {
        return (WidgetEnableGuildCommunicationBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEnableGuildCommunication(long j, long j2) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApiSerializeNulls().disableGuildCommunication(j, j2, new RestAPIParams.DisableGuildCommunication(null), null), false, 1, null), new WidgetEnableGuildCommunication$handleEnableGuildCommunication$1(j, j2)), this, null, 2, null), WidgetEnableGuildCommunication.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEnableGuildCommunication$handleEnableGuildCommunication$2(this));
    }

    public static final void launch(long j, long j2, FragmentManager fragmentManager) {
        Companion.launch(j, j2, fragmentManager);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        Subscription subscription = this.communicationDisabledCountdownSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        final long j = getArgumentsOrDefault().getLong("com.discord.intent.extra.EXTRA_GUILD_ID", -1L);
        final long j2 = getArgumentsOrDefault().getLong("com.discord.intent.extra.EXTRA_USER_ID", -1L);
        StoreStream.Companion companion = StoreStream.Companion;
        StoreUser users = companion.getUsers();
        GuildMember member = companion.getGuilds().getMember(j, j2);
        User user = users.getUsers().get(Long.valueOf(j2));
        if (member != null && user != null && member.isCommunicationDisabled()) {
            CharSequence userNameWithDiscriminator$default = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user, null, null, 3, null);
            UtcDateTime communicationDisabledUntil = member.getCommunicationDisabledUntil();
            long g = communicationDisabledUntil != null ? communicationDisabledUntil.g() : 0L;
            configureCommunicationDisabledTimer(g, userNameWithDiscriminator$default);
            long max = Math.max(g - this.clock.currentTimeMillis(), 0L);
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.enableGuildCommunicationBody");
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            Object[] objArr = {userNameWithDiscriminator$default, DurationUtilsKt.humanizeCountdownDuration(requireContext, max)};
            CharSequence charSequence = null;
            b.m(textView, R.string.enable_guild_communication_body, objArr, (r4 & 4) != 0 ? b.g.j : null);
            Context context = getContext();
            if (context != null) {
                charSequence = b.b(context, R.string.enable_guild_communication_body_help_text, new Object[]{f.a.a(4413305239191L, null)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            }
            TextView textView2 = getBinding().e;
            m.checkNotNullExpressionValue(textView2, "binding.enableGuildCommunicationBodyHelpText");
            textView2.setText(charSequence);
            getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildcommunicationdisabled.start.WidgetEnableGuildCommunication$onViewBound$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WidgetEnableGuildCommunicationBinding binding;
                    UriHandler uriHandler = UriHandler.INSTANCE;
                    binding = WidgetEnableGuildCommunication.this.getBinding();
                    TextView textView3 = binding.e;
                    m.checkNotNullExpressionValue(textView3, "binding.enableGuildCommunicationBodyHelpText");
                    Context context2 = textView3.getContext();
                    m.checkNotNullExpressionValue(context2, "binding.enableGuildCommu…ationBodyHelpText.context");
                    UriHandler.handle$default(uriHandler, context2, f.a.a(4413305239191L, null), null, 4, null);
                }
            });
            getBinding().f2360b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildcommunicationdisabled.start.WidgetEnableGuildCommunication$onViewBound$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WidgetEnableGuildCommunication.this.dismiss();
                }
            });
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildcommunicationdisabled.start.WidgetEnableGuildCommunication$onViewBound$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WidgetEnableGuildCommunication.this.handleEnableGuildCommunication(j, j2);
                }
            });
        }
    }
}
