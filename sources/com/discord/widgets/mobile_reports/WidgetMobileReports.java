package com.discord.widgets.mobile_reports;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetMobileReportsBinding;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.mobile_reports.MobileReportArgs;
import com.discord.widgets.mobile_reports.MobileReportsViewModel;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetMobileReports.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0007¢\u0006\u0004\b!\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0015\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001b\u001a\u00020\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010 \u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u0012\u001a\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lcom/discord/widgets/mobile_reports/WidgetMobileReports;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Menu;", "viewState", "Lcom/discord/widgets/mobile_reports/ReportsMenuNode;", "createNodeView", "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Menu;)Lcom/discord/widgets/mobile_reports/ReportsMenuNode;", "", "configureUI", "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Menu;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/mobile_reports/MobileReportArgs;", "args$delegate", "Lkotlin/Lazy;", "getArgs", "()Lcom/discord/widgets/mobile_reports/MobileReportArgs;", "args", "Lcom/discord/databinding/WidgetMobileReportsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetMobileReportsBinding;", "binding", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMobileReports extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetMobileReports.class, "binding", "getBinding()Lcom/discord/databinding/WidgetMobileReportsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final Lazy args$delegate = g.lazy(new WidgetMobileReports$$special$$inlined$args$1(this, "intent_args_key"));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetMobileReports$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetMobileReports.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J-\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u0007¢\u0006\u0004\b\n\u0010\u000bJ!\u0010\f\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\b\u001a\u00060\u0004j\u0002`\u0007¢\u0006\u0004\b\f\u0010\rJ9\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u000f\u001a\u00060\u0004j\u0002`\u000e2\n\u0010\u0010\u001a\u00060\u0004j\u0002`\u000e2\n\u0010\b\u001a\u00060\u0004j\u0002`\u0007¢\u0006\u0004\b\u0011\u0010\u0012J-\u0010\u0015\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u000f\u001a\u00060\u0004j\u0002`\u000e2\n\u0010\u0014\u001a\u00060\u0004j\u0002`\u0013¢\u0006\u0004\b\u0015\u0010\u000b¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/mobile_reports/WidgetMobileReports$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/MessageId;", "messageId", "Lcom/discord/primitives/ChannelId;", "channelId", "", "launchMessageReport", "(Landroid/content/Context;JJ)V", "launchStageChannelReport", "(Landroid/content/Context;J)V", "Lcom/discord/primitives/GuildId;", "guildId", "hubId", "launchDirectoryServerReport", "(Landroid/content/Context;JJJ)V", "Lcom/discord/primitives/GuildScheduledEventId;", "eventId", "launchGuildScheduledEventReport", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launchDirectoryServerReport(Context context, long j, long j2, long j3) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetMobileReports.class, new MobileReportArgs.DirectoryServer(j, j2, j3));
        }

        public final void launchGuildScheduledEventReport(Context context, long j, long j2) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetMobileReports.class, new MobileReportArgs.GuildScheduledEvent(j, j2));
        }

        public final void launchMessageReport(Context context, long j, long j2) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetMobileReports.class, new MobileReportArgs.Message(j, j2));
        }

        public final void launchStageChannelReport(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetMobileReports.class, new MobileReportArgs.StageChannel(j));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetMobileReports() {
        super(R.layout.widget_mobile_reports);
        WidgetMobileReports$viewModel$2 widgetMobileReports$viewModel$2 = new WidgetMobileReports$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(MobileReportsViewModel.class), new WidgetMobileReports$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetMobileReports$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(MobileReportsViewModel.ViewState.Menu menu) {
        setActionBarDisplayHomeAsUpEnabled(!menu.shouldHideBackArrow());
        ProgressBar progressBar = getBinding().c;
        m.checkNotNullExpressionValue(progressBar, "binding.mobileReportsProgressBar");
        progressBar.setVisibility(8);
        ReportsMenuNode reportsMenuNode = (ReportsMenuNode) getBinding().f2473b.getChildAt(0);
        MobileReportsViewModel.NodeState genNodeState = menu.genNodeState();
        if (reportsMenuNode == null) {
            ReportsMenuNode createNodeView = createNodeView(menu);
            createNodeView.setVisibility(8);
            getBinding().f2473b.addView(createNodeView);
            ViewExtensions.fadeIn$default(createNodeView, 0L, null, null, new WidgetMobileReports$configureUI$1(this, menu), 7, null);
            return;
        }
        MobileReportsViewModel.NodeState viewState = reportsMenuNode.getViewState();
        if (!m.areEqual(viewState != null ? viewState.getNode() : null, genNodeState.getNode())) {
            ReportsMenuNode createNodeView2 = createNodeView(menu);
            createNodeView2.setVisibility(8);
            ViewExtensions.fadeOut$default(reportsMenuNode, 0L, null, new WidgetMobileReports$configureUI$2(this, createNodeView2, menu), 3, null);
            return;
        }
        reportsMenuNode.setup(genNodeState);
        setActionBarDisplayHomeAsUpEnabled(!menu.shouldHideBackArrow());
    }

    private final ReportsMenuNode createNodeView(MobileReportsViewModel.ViewState.Menu menu) {
        ReportsMenuNode reportsMenuNode = new ReportsMenuNode(requireContext());
        reportsMenuNode.setHandleSelectChild(new WidgetMobileReports$createNodeView$1(this));
        reportsMenuNode.setHandleBlock(new WidgetMobileReports$createNodeView$2(this));
        reportsMenuNode.setHandleCancel(new WidgetMobileReports$createNodeView$3(this));
        reportsMenuNode.setHandleSubmit(new WidgetMobileReports$createNodeView$4(this));
        reportsMenuNode.setup(menu.genNodeState());
        return reportsMenuNode;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final MobileReportArgs getArgs() {
        return (MobileReportArgs) this.args$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetMobileReportsBinding getBinding() {
        return (WidgetMobileReportsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final MobileReportsViewModel getViewModel() {
        return (MobileReportsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarDisplayHomeAsUpEnabled(false);
        final WidgetMobileReports$onViewBound$1 widgetMobileReports$onViewBound$1 = new WidgetMobileReports$onViewBound$1(getViewModel());
        AppFragment.setOnBackPressed$default(this, new Func0() { // from class: com.discord.widgets.mobile_reports.WidgetMobileReports$sam$rx_functions_Func0$0
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final /* synthetic */ Object call() {
                return Function0.this.invoke();
            }
        }, 0, 2, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetMobileReports.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMobileReports$onViewBoundOrOnResume$1(this));
    }
}
