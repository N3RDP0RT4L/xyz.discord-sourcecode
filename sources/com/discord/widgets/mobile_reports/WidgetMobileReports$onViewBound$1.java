package com.discord.widgets.mobile_reports;

import d0.z.d.k;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetMobileReports.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetMobileReports$onViewBound$1 extends k implements Function0<Boolean> {
    public WidgetMobileReports$onViewBound$1(MobileReportsViewModel mobileReportsViewModel) {
        super(0, mobileReportsViewModel, MobileReportsViewModel.class, "handleBack", "handleBack()Z", 0);
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [boolean, java.lang.Boolean] */
    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2() {
        return ((MobileReportsViewModel) this.receiver).handleBack();
    }
}
