package com.discord.widgets.mobile_reports;

import com.discord.widgets.mobile_reports.MobileReportsViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetMobileReports.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMobileReports$configureUI$1 extends o implements Function0<Unit> {
    public final /* synthetic */ MobileReportsViewModel.ViewState.Menu $viewState;
    public final /* synthetic */ WidgetMobileReports this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetMobileReports$configureUI$1(WidgetMobileReports widgetMobileReports, MobileReportsViewModel.ViewState.Menu menu) {
        super(0);
        this.this$0 = widgetMobileReports;
        this.$viewState = menu;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.setActionBarDisplayHomeAsUpEnabled(!this.$viewState.shouldHideBackArrow());
    }
}
