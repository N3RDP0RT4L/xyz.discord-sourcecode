package com.discord.widgets.mobile_reports;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.report.MenuAPIResponse;
import com.discord.api.report.NodeElementResult;
import com.discord.api.report.NodeResult;
import com.discord.api.report.ReportNode;
import com.discord.api.report.ReportNodeBottomButton;
import com.discord.api.report.ReportNodeChild;
import com.discord.api.report.ReportNodeElement;
import com.discord.api.report.ReportNodeElementData;
import com.discord.api.report.ReportSubmissionBody;
import com.discord.api.stageinstance.StageInstance;
import com.discord.app.AppLog;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.User;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMessages;
import com.discord.stores.StoreStream;
import com.discord.stores.utilities.RestCallState;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.error.Error;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.message.MessageUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.DiscordParser;
import com.discord.utilities.textprocessing.MessagePreprocessor;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.widgets.mobile_reports.MobileReportArgs;
import com.discord.widgets.mobile_reports.MobileReportsViewModel;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.o;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.Regex;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func8;
import xyz.discord.R;
/* compiled from: MobileReportsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 /2\b\u0012\u0004\u0012\u00020\u00020\u0001:\f012/3456789:BA\u0012\f\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$\u0012\u0006\u0010)\u001a\u00020(\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u001e\u0012\b\b\u0002\u0010\"\u001a\u00020!\u0012\u000e\b\u0002\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00030+¢\u0006\u0004\b-\u0010.J\u0019\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0019\u0010\t\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\r\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\r\u0010\u001b\u001a\u00020\u0018¢\u0006\u0004\b\u001b\u0010\u001cJ\r\u0010\u001d\u001a\u00020\u0018¢\u0006\u0004\b\u001d\u0010\u001cR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u001c\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*¨\u0006;"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;", "storeState", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "parseMessagePreview", "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "parseChannelPreview", "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "parseDirectoryServerPreview", "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "parseEventPreview", "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "", "handleBack", "()Z", "Lcom/discord/api/report/ReportNodeChild;", "destination", "Lcom/discord/api/report/NodeElementResult;", "elementResult", "", "handleNext", "(Lcom/discord/api/report/ReportNodeChild;Lcom/discord/api/report/NodeElementResult;)V", "handleSubmit", "()V", "handleBlockUser", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Ljava/lang/ref/WeakReference;", "Landroid/content/Context;", "context", "Ljava/lang/ref/WeakReference;", "Lcom/discord/widgets/mobile_reports/MobileReportArgs;", "args", "Lcom/discord/widgets/mobile_reports/MobileReportArgs;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Ljava/lang/ref/WeakReference;Lcom/discord/widgets/mobile_reports/MobileReportArgs;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/time/Clock;Lrx/Observable;)V", "Companion", "BlockUserElement", "ChannelPreview", "CheckboxElement", "DirectoryServerPreview", "GuildScheduledEventPreview", "MessagePreview", "NodeNavigationType", "NodeState", "StoreState", "SubmitState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MobileReportsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final MobileReportArgs args;
    private final Clock clock;
    private final WeakReference<Context> context;
    private final RestAPI restAPI;

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001aB\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003 \u0001* \u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00050\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "Lcom/discord/api/report/MenuAPIResponse;", "menuAPI", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;Lcom/discord/api/report/MenuAPIResponse;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.mobile_reports.MobileReportsViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T1, T2, R> implements Func2<StoreState, MenuAPIResponse, Pair<? extends StoreState, ? extends MenuAPIResponse>> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Pair<StoreState, MenuAPIResponse> call(StoreState storeState, MenuAPIResponse menuAPIResponse) {
            return o.to(storeState, menuAPIResponse);
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052F\u0010\u0004\u001aB\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003 \u0002* \u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lkotlin/Pair;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;", "kotlin.jvm.PlatformType", "Lcom/discord/api/report/MenuAPIResponse;", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.mobile_reports.MobileReportsViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends d0.z.d.o implements Function1<Pair<? extends StoreState, ? extends MenuAPIResponse>, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends StoreState, ? extends MenuAPIResponse> pair) {
            invoke2((Pair<StoreState, MenuAPIResponse>) pair);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Pair<StoreState, MenuAPIResponse> pair) {
            ReportNode reportNode;
            StoreState component1 = pair.component1();
            MenuAPIResponse component2 = pair.component2();
            BlockUserElement blockUserElement = null;
            if (component2 != null) {
                reportNode = component2.c().get(Integer.valueOf(component2.d()));
            } else {
                reportNode = null;
            }
            if (component2 == null || reportNode == null) {
                MobileReportsViewModel.this.updateViewState(ViewState.Invalid.INSTANCE);
                return;
            }
            MobileReportsViewModel mobileReportsViewModel = MobileReportsViewModel.this;
            m.checkNotNullExpressionValue(component1, "storeState");
            MessagePreview parseMessagePreview = mobileReportsViewModel.parseMessagePreview(component1);
            MobileReportsViewModel mobileReportsViewModel2 = MobileReportsViewModel.this;
            ViewState access$getViewState$p = MobileReportsViewModel.access$getViewState$p(mobileReportsViewModel2);
            if (access$getViewState$p == null) {
                ChannelPreview parseChannelPreview = MobileReportsViewModel.this.parseChannelPreview(component1);
                NodeNavigationType.Initial initial = new NodeNavigationType.Initial(reportNode);
                List emptyList = n.emptyList();
                if (parseMessagePreview != null) {
                    blockUserElement = new BlockUserElement(component1.getBlockedUsers().containsKey(Long.valueOf(parseMessagePreview.getAuthor().getId())), parseMessagePreview.getAuthor());
                }
                access$getViewState$p = new ViewState.Menu(component2, parseMessagePreview, parseChannelPreview, MobileReportsViewModel.this.parseDirectoryServerPreview(component1), MobileReportsViewModel.this.parseEventPreview(component1), initial, null, emptyList, blockUserElement, 64, null);
            }
            mobileReportsViewModel2.updateViewState(access$getViewState$p);
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.mobile_reports.MobileReportsViewModel$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends d0.z.d.o implements Function1<Error, Unit> {
        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            m.checkNotNullParameter(error, "it");
            Logger.e$default(AppLog.g, "Can't parse report message? Closing report screen.", null, null, 6, null);
            MobileReportsViewModel.this.updateViewState(ViewState.Invalid.INSTANCE);
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00022\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\b\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "", "", "component1", "()Z", "Lcom/discord/models/user/User;", "component2", "()Lcom/discord/models/user/User;", "isBlocked", "user", "copy", "(ZLcom/discord/models/user/User;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/user/User;", "getUser", "Z", HookHelper.constructorName, "(ZLcom/discord/models/user/User;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BlockUserElement {
        private final boolean isBlocked;
        private final User user;

        public BlockUserElement(boolean z2, User user) {
            m.checkNotNullParameter(user, "user");
            this.isBlocked = z2;
            this.user = user;
        }

        public static /* synthetic */ BlockUserElement copy$default(BlockUserElement blockUserElement, boolean z2, User user, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = blockUserElement.isBlocked;
            }
            if ((i & 2) != 0) {
                user = blockUserElement.user;
            }
            return blockUserElement.copy(z2, user);
        }

        public final boolean component1() {
            return this.isBlocked;
        }

        public final User component2() {
            return this.user;
        }

        public final BlockUserElement copy(boolean z2, User user) {
            m.checkNotNullParameter(user, "user");
            return new BlockUserElement(z2, user);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BlockUserElement)) {
                return false;
            }
            BlockUserElement blockUserElement = (BlockUserElement) obj;
            return this.isBlocked == blockUserElement.isBlocked && m.areEqual(this.user, blockUserElement.user);
        }

        public final User getUser() {
            return this.user;
        }

        public int hashCode() {
            boolean z2 = this.isBlocked;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            int i3 = i * 31;
            User user = this.user;
            return i3 + (user != null ? user.hashCode() : 0);
        }

        public final boolean isBlocked() {
            return this.isBlocked;
        }

        public String toString() {
            StringBuilder R = a.R("BlockUserElement(isBlocked=");
            R.append(this.isBlocked);
            R.append(", user=");
            R.append(this.user);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/stageinstance/StageInstance;", "component2", "()Lcom/discord/api/stageinstance/StageInstance;", "guild", "stageInstance", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/stageinstance/StageInstance;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/stageinstance/StageInstance;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelPreview {
        private final Guild guild;
        private final StageInstance stageInstance;

        public ChannelPreview(Guild guild, StageInstance stageInstance) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(stageInstance, "stageInstance");
            this.guild = guild;
            this.stageInstance = stageInstance;
        }

        public static /* synthetic */ ChannelPreview copy$default(ChannelPreview channelPreview, Guild guild, StageInstance stageInstance, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = channelPreview.guild;
            }
            if ((i & 2) != 0) {
                stageInstance = channelPreview.stageInstance;
            }
            return channelPreview.copy(guild, stageInstance);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final StageInstance component2() {
            return this.stageInstance;
        }

        public final ChannelPreview copy(Guild guild, StageInstance stageInstance) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(stageInstance, "stageInstance");
            return new ChannelPreview(guild, stageInstance);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChannelPreview)) {
                return false;
            }
            ChannelPreview channelPreview = (ChannelPreview) obj;
            return m.areEqual(this.guild, channelPreview.guild) && m.areEqual(this.stageInstance, channelPreview.stageInstance);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final StageInstance getStageInstance() {
            return this.stageInstance;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            StageInstance stageInstance = this.stageInstance;
            if (stageInstance != null) {
                i = stageInstance.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelPreview(guild=");
            R.append(this.guild);
            R.append(", stageInstance=");
            R.append(this.stageInstance);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u00060\tj\b\u0012\u0004\u0012\u00020\u0006`\n¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ \u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\u00060\tj\b\u0012\u0004\u0012\u00020\u0006`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJD\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0018\b\u0002\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u00060\tj\b\u0012\u0004\u0012\u00020\u0006`\nHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u001b\u0010\bR)\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u00060\tj\b\u0012\u0004\u0012\u00020\u0006`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\fR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004¨\u0006\""}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$CheckboxElement;", "", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/api/report/ReportNodeElementData;", "component2", "()Ljava/util/List;", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "component3", "()Ljava/util/HashSet;", ModelAuditLogEntry.CHANGE_KEY_NAME, "data", "selections", "copy", "(Ljava/lang/String;Ljava/util/List;Ljava/util/HashSet;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$CheckboxElement;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getData", "Ljava/util/HashSet;", "getSelections", "Ljava/lang/String;", "getName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;Ljava/util/HashSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CheckboxElement {
        private final List<ReportNodeElementData> data;
        private final String name;
        private final HashSet<ReportNodeElementData> selections;

        public CheckboxElement(String str, List<ReportNodeElementData> list, HashSet<ReportNodeElementData> hashSet) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(list, "data");
            m.checkNotNullParameter(hashSet, "selections");
            this.name = str;
            this.data = list;
            this.selections = hashSet;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ CheckboxElement copy$default(CheckboxElement checkboxElement, String str, List list, HashSet hashSet, int i, Object obj) {
            if ((i & 1) != 0) {
                str = checkboxElement.name;
            }
            if ((i & 2) != 0) {
                list = checkboxElement.data;
            }
            if ((i & 4) != 0) {
                hashSet = checkboxElement.selections;
            }
            return checkboxElement.copy(str, list, hashSet);
        }

        public final String component1() {
            return this.name;
        }

        public final List<ReportNodeElementData> component2() {
            return this.data;
        }

        public final HashSet<ReportNodeElementData> component3() {
            return this.selections;
        }

        public final CheckboxElement copy(String str, List<ReportNodeElementData> list, HashSet<ReportNodeElementData> hashSet) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(list, "data");
            m.checkNotNullParameter(hashSet, "selections");
            return new CheckboxElement(str, list, hashSet);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CheckboxElement)) {
                return false;
            }
            CheckboxElement checkboxElement = (CheckboxElement) obj;
            return m.areEqual(this.name, checkboxElement.name) && m.areEqual(this.data, checkboxElement.data) && m.areEqual(this.selections, checkboxElement.selections);
        }

        public final List<ReportNodeElementData> getData() {
            return this.data;
        }

        public final String getName() {
            return this.name;
        }

        public final HashSet<ReportNodeElementData> getSelections() {
            return this.selections;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            List<ReportNodeElementData> list = this.data;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            HashSet<ReportNodeElementData> hashSet = this.selections;
            if (hashSet != null) {
                i = hashSet.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("CheckboxElement(name=");
            R.append(this.name);
            R.append(", data=");
            R.append(this.data);
            R.append(", selections=");
            R.append(this.selections);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001d\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$Companion;", "", "Lcom/discord/api/report/ReportNode;", "node", "", "getLocation", "(Lcom/discord/api/report/ReportNode;)Ljava/lang/String;", "Lcom/discord/widgets/mobile_reports/MobileReportArgs;", "args", "Lrx/Observable;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;", "getStoreState", "(Lcom/discord/widgets/mobile_reports/MobileReportArgs;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final String getLocation(ReportNode reportNode) {
            return "REPORT_MENU_NODE_" + reportNode + ".id";
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> getStoreState(final MobileReportArgs mobileReportArgs) {
            StoreStream.Companion companion = StoreStream.Companion;
            StoreMessages messages = companion.getMessages();
            long channelId = mobileReportArgs.getChannelId();
            MobileReportArgs.GuildScheduledEvent guildScheduledEvent = null;
            MobileReportArgs.Message message = (MobileReportArgs.Message) (!(mobileReportArgs instanceof MobileReportArgs.Message) ? null : mobileReportArgs);
            long j = -1;
            Observable<Message> observeMessagesForChannel = messages.observeMessagesForChannel(channelId, message != null ? message.getMessageId() : -1L);
            Observable<Channel> observeChannel = companion.getChannels().observeChannel(mobileReportArgs.getChannelId());
            Observable<Guild> observeFromChannelId = companion.getGuilds().observeFromChannelId(mobileReportArgs.getChannelId());
            Observable<StageInstance> observeStageInstanceForChannel = companion.getStageInstances().observeStageInstanceForChannel(mobileReportArgs.getChannelId());
            Observable<Map<Long, Integer>> observeForType = companion.getUserRelationships().observeForType(2);
            Observable<RestCallState<List<DirectoryEntryGuild>>> observeDirectoriesForChannel = companion.getDirectories().observeDirectoriesForChannel(mobileReportArgs.getChannelId());
            StoreGuildScheduledEvents guildScheduledEvents = companion.getGuildScheduledEvents();
            boolean z2 = mobileReportArgs instanceof MobileReportArgs.GuildScheduledEvent;
            MobileReportArgs.GuildScheduledEvent guildScheduledEvent2 = (MobileReportArgs.GuildScheduledEvent) (!z2 ? null : mobileReportArgs);
            Long valueOf = guildScheduledEvent2 != null ? Long.valueOf(guildScheduledEvent2.getEventId()) : null;
            MobileReportArgs.GuildScheduledEvent guildScheduledEvent3 = (MobileReportArgs.GuildScheduledEvent) (!z2 ? null : mobileReportArgs);
            Observable<GuildScheduledEvent> observeGuildScheduledEvent = guildScheduledEvents.observeGuildScheduledEvent(valueOf, guildScheduledEvent3 != null ? Long.valueOf(guildScheduledEvent3.getGuildId()) : null);
            StoreGuilds guilds = companion.getGuilds();
            if (z2) {
                guildScheduledEvent = mobileReportArgs;
            }
            MobileReportArgs.GuildScheduledEvent guildScheduledEvent4 = guildScheduledEvent;
            if (guildScheduledEvent4 != null) {
                j = guildScheduledEvent4.getGuildId();
            }
            Observable<StoreState> d = Observable.d(observeMessagesForChannel, observeChannel, observeFromChannelId, observeStageInstanceForChannel, observeForType, observeDirectoriesForChannel, observeGuildScheduledEvent, guilds.observeGuild(j), new Func8<Message, Channel, Guild, StageInstance, Map<Long, ? extends Integer>, RestCallState<? extends List<? extends DirectoryEntryGuild>>, GuildScheduledEvent, Guild, StoreState>() { // from class: com.discord.widgets.mobile_reports.MobileReportsViewModel$Companion$getStoreState$1
                @Override // rx.functions.Func8
                public /* bridge */ /* synthetic */ MobileReportsViewModel.StoreState call(Message message2, Channel channel, Guild guild, StageInstance stageInstance, Map<Long, ? extends Integer> map, RestCallState<? extends List<? extends DirectoryEntryGuild>> restCallState, GuildScheduledEvent guildScheduledEvent5, Guild guild2) {
                    return call2(message2, channel, guild, stageInstance, (Map<Long, Integer>) map, (RestCallState<? extends List<DirectoryEntryGuild>>) restCallState, guildScheduledEvent5, guild2);
                }

                /* JADX WARN: Multi-variable type inference failed */
                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final MobileReportsViewModel.StoreState call2(Message message2, Channel channel, Guild guild, StageInstance stageInstance, Map<Long, Integer> map, RestCallState<? extends List<DirectoryEntryGuild>> restCallState, GuildScheduledEvent guildScheduledEvent5, Guild guild2) {
                    List<DirectoryEntryGuild> invoke;
                    boolean z3;
                    MobileReportArgs mobileReportArgs2 = MobileReportArgs.this;
                    DirectoryEntryGuild directoryEntryGuild = null;
                    if (!(mobileReportArgs2 instanceof MobileReportArgs.DirectoryServer)) {
                        mobileReportArgs2 = null;
                    }
                    MobileReportArgs.DirectoryServer directoryServer = (MobileReportArgs.DirectoryServer) mobileReportArgs2;
                    Long valueOf2 = directoryServer != null ? Long.valueOf(directoryServer.getGuildId()) : null;
                    Guild guild3 = guild != null ? guild : guild2;
                    m.checkNotNullExpressionValue(map, "blockedUsers");
                    if (!(restCallState == null || (invoke = restCallState.invoke()) == null)) {
                        Iterator<T> it = invoke.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Object next = it.next();
                            long h = ((DirectoryEntryGuild) next).e().h();
                            if (valueOf2 != null && h == valueOf2.longValue()) {
                                z3 = true;
                                continue;
                            } else {
                                z3 = false;
                                continue;
                            }
                            if (z3) {
                                directoryEntryGuild = next;
                                break;
                            }
                        }
                        directoryEntryGuild = directoryEntryGuild;
                    }
                    return new MobileReportsViewModel.StoreState(message2, channel, guild3, directoryEntryGuild, stageInstance, map, guildScheduledEvent5);
                }
            });
            m.checkNotNullExpressionValue(d, "Observable.combineLatest…nt = event,\n      )\n    }");
            return d;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/directory/DirectoryEntryGuild;", "component2", "()Lcom/discord/api/directory/DirectoryEntryGuild;", "hub", "directoryEntry", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/directory/DirectoryEntryGuild;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/directory/DirectoryEntryGuild;", "getDirectoryEntry", "Lcom/discord/models/guild/Guild;", "getHub", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/directory/DirectoryEntryGuild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DirectoryServerPreview {
        private final DirectoryEntryGuild directoryEntry;
        private final Guild hub;

        public DirectoryServerPreview(Guild guild, DirectoryEntryGuild directoryEntryGuild) {
            m.checkNotNullParameter(guild, "hub");
            m.checkNotNullParameter(directoryEntryGuild, "directoryEntry");
            this.hub = guild;
            this.directoryEntry = directoryEntryGuild;
        }

        public static /* synthetic */ DirectoryServerPreview copy$default(DirectoryServerPreview directoryServerPreview, Guild guild, DirectoryEntryGuild directoryEntryGuild, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = directoryServerPreview.hub;
            }
            if ((i & 2) != 0) {
                directoryEntryGuild = directoryServerPreview.directoryEntry;
            }
            return directoryServerPreview.copy(guild, directoryEntryGuild);
        }

        public final Guild component1() {
            return this.hub;
        }

        public final DirectoryEntryGuild component2() {
            return this.directoryEntry;
        }

        public final DirectoryServerPreview copy(Guild guild, DirectoryEntryGuild directoryEntryGuild) {
            m.checkNotNullParameter(guild, "hub");
            m.checkNotNullParameter(directoryEntryGuild, "directoryEntry");
            return new DirectoryServerPreview(guild, directoryEntryGuild);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DirectoryServerPreview)) {
                return false;
            }
            DirectoryServerPreview directoryServerPreview = (DirectoryServerPreview) obj;
            return m.areEqual(this.hub, directoryServerPreview.hub) && m.areEqual(this.directoryEntry, directoryServerPreview.directoryEntry);
        }

        public final DirectoryEntryGuild getDirectoryEntry() {
            return this.directoryEntry;
        }

        public final Guild getHub() {
            return this.hub;
        }

        public int hashCode() {
            Guild guild = this.hub;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            DirectoryEntryGuild directoryEntryGuild = this.directoryEntry;
            if (directoryEntryGuild != null) {
                i = directoryEntryGuild.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("DirectoryServerPreview(hub=");
            R.append(this.hub);
            R.append(", directoryEntry=");
            R.append(this.directoryEntry);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component2", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guild", "event", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getEvent", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GuildScheduledEventPreview {
        private final GuildScheduledEvent event;
        private final Guild guild;

        public GuildScheduledEventPreview(Guild guild, GuildScheduledEvent guildScheduledEvent) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(guildScheduledEvent, "event");
            this.guild = guild;
            this.event = guildScheduledEvent;
        }

        public static /* synthetic */ GuildScheduledEventPreview copy$default(GuildScheduledEventPreview guildScheduledEventPreview, Guild guild, GuildScheduledEvent guildScheduledEvent, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = guildScheduledEventPreview.guild;
            }
            if ((i & 2) != 0) {
                guildScheduledEvent = guildScheduledEventPreview.event;
            }
            return guildScheduledEventPreview.copy(guild, guildScheduledEvent);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final GuildScheduledEvent component2() {
            return this.event;
        }

        public final GuildScheduledEventPreview copy(Guild guild, GuildScheduledEvent guildScheduledEvent) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(guildScheduledEvent, "event");
            return new GuildScheduledEventPreview(guild, guildScheduledEvent);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GuildScheduledEventPreview)) {
                return false;
            }
            GuildScheduledEventPreview guildScheduledEventPreview = (GuildScheduledEventPreview) obj;
            return m.areEqual(this.guild, guildScheduledEventPreview.guild) && m.areEqual(this.event, guildScheduledEventPreview.event);
        }

        public final GuildScheduledEvent getEvent() {
            return this.event;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            GuildScheduledEvent guildScheduledEvent = this.event;
            if (guildScheduledEvent != null) {
                i = guildScheduledEvent.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("GuildScheduledEventPreview(guild=");
            R.append(this.guild);
            R.append(", event=");
            R.append(this.event);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001b\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0005\u0012\u0006\u0010\u0013\u001a\u00020\b\u0012\u0006\u0010\u0014\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u000e¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JB\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00052\b\b\u0002\u0010\u0013\u001a\u00020\b2\b\b\u0002\u0010\u0014\u001a\u00020\u000b2\b\b\u0002\u0010\u0015\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0007J\u0010\u0010\u0019\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0019\u0010\nJ\u001a\u0010\u001b\u001a\u00020\u000e2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u0012\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R\u0019\u0010\u0013\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001f\u001a\u0004\b \u0010\nR\u0019\u0010\u0014\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b\"\u0010\rR\u0019\u0010\u0015\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010#\u001a\u0004\b$\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b&\u0010\u0004¨\u0006)"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "component1", "()Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "", "component2", "()Ljava/lang/String;", "", "component3", "()I", "Lcom/discord/models/user/User;", "component4", "()Lcom/discord/models/user/User;", "", "component5", "()Z", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "authorName", "authorNameColor", "author", "hasEmbeds", "copy", "(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;ILcom/discord/models/user/User;Z)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getAuthorName", "I", "getAuthorNameColor", "Lcom/discord/models/user/User;", "getAuthor", "Z", "getHasEmbeds", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "getText", HookHelper.constructorName, "(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;ILcom/discord/models/user/User;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MessagePreview {
        private final User author;
        private final String authorName;
        private final int authorNameColor;
        private final boolean hasEmbeds;
        private final DraweeSpanStringBuilder text;

        public MessagePreview(DraweeSpanStringBuilder draweeSpanStringBuilder, String str, int i, User user, boolean z2) {
            m.checkNotNullParameter(draweeSpanStringBuilder, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(str, "authorName");
            m.checkNotNullParameter(user, "author");
            this.text = draweeSpanStringBuilder;
            this.authorName = str;
            this.authorNameColor = i;
            this.author = user;
            this.hasEmbeds = z2;
        }

        public static /* synthetic */ MessagePreview copy$default(MessagePreview messagePreview, DraweeSpanStringBuilder draweeSpanStringBuilder, String str, int i, User user, boolean z2, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                draweeSpanStringBuilder = messagePreview.text;
            }
            if ((i2 & 2) != 0) {
                str = messagePreview.authorName;
            }
            String str2 = str;
            if ((i2 & 4) != 0) {
                i = messagePreview.authorNameColor;
            }
            int i3 = i;
            if ((i2 & 8) != 0) {
                user = messagePreview.author;
            }
            User user2 = user;
            if ((i2 & 16) != 0) {
                z2 = messagePreview.hasEmbeds;
            }
            return messagePreview.copy(draweeSpanStringBuilder, str2, i3, user2, z2);
        }

        public final DraweeSpanStringBuilder component1() {
            return this.text;
        }

        public final String component2() {
            return this.authorName;
        }

        public final int component3() {
            return this.authorNameColor;
        }

        public final User component4() {
            return this.author;
        }

        public final boolean component5() {
            return this.hasEmbeds;
        }

        public final MessagePreview copy(DraweeSpanStringBuilder draweeSpanStringBuilder, String str, int i, User user, boolean z2) {
            m.checkNotNullParameter(draweeSpanStringBuilder, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(str, "authorName");
            m.checkNotNullParameter(user, "author");
            return new MessagePreview(draweeSpanStringBuilder, str, i, user, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MessagePreview)) {
                return false;
            }
            MessagePreview messagePreview = (MessagePreview) obj;
            return m.areEqual(this.text, messagePreview.text) && m.areEqual(this.authorName, messagePreview.authorName) && this.authorNameColor == messagePreview.authorNameColor && m.areEqual(this.author, messagePreview.author) && this.hasEmbeds == messagePreview.hasEmbeds;
        }

        public final User getAuthor() {
            return this.author;
        }

        public final String getAuthorName() {
            return this.authorName;
        }

        public final int getAuthorNameColor() {
            return this.authorNameColor;
        }

        public final boolean getHasEmbeds() {
            return this.hasEmbeds;
        }

        public final DraweeSpanStringBuilder getText() {
            return this.text;
        }

        public int hashCode() {
            DraweeSpanStringBuilder draweeSpanStringBuilder = this.text;
            int i = 0;
            int hashCode = (draweeSpanStringBuilder != null ? draweeSpanStringBuilder.hashCode() : 0) * 31;
            String str = this.authorName;
            int hashCode2 = (((hashCode + (str != null ? str.hashCode() : 0)) * 31) + this.authorNameColor) * 31;
            User user = this.author;
            if (user != null) {
                i = user.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.hasEmbeds;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("MessagePreview(text=");
            R.append((Object) this.text);
            R.append(", authorName=");
            R.append(this.authorName);
            R.append(", authorNameColor=");
            R.append(this.authorNameColor);
            R.append(", author=");
            R.append(this.author);
            R.append(", hasEmbeds=");
            return a.M(R, this.hasEmbeds, ")");
        }

        public /* synthetic */ MessagePreview(DraweeSpanStringBuilder draweeSpanStringBuilder, String str, int i, User user, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(draweeSpanStringBuilder, str, i, user, (i2 & 16) != 0 ? false : z2);
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;", "", "Lcom/discord/api/report/ReportNode;", "node", "Lcom/discord/api/report/ReportNode;", "getNode", "()Lcom/discord/api/report/ReportNode;", HookHelper.constructorName, "(Lcom/discord/api/report/ReportNode;)V", "Back", "Initial", "Next", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Next;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Back;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Initial;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class NodeNavigationType {
        private final ReportNode node;

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Back;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;", "Lcom/discord/api/report/ReportNode;", "component1", "()Lcom/discord/api/report/ReportNode;", "prevNode", "copy", "(Lcom/discord/api/report/ReportNode;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Back;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/report/ReportNode;", HookHelper.constructorName, "(Lcom/discord/api/report/ReportNode;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Back extends NodeNavigationType {
            private final ReportNode prevNode;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Back(ReportNode reportNode) {
                super(reportNode, null);
                m.checkNotNullParameter(reportNode, "prevNode");
                this.prevNode = reportNode;
            }

            private final ReportNode component1() {
                return this.prevNode;
            }

            public static /* synthetic */ Back copy$default(Back back, ReportNode reportNode, int i, Object obj) {
                if ((i & 1) != 0) {
                    reportNode = back.prevNode;
                }
                return back.copy(reportNode);
            }

            public final Back copy(ReportNode reportNode) {
                m.checkNotNullParameter(reportNode, "prevNode");
                return new Back(reportNode);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Back) && m.areEqual(this.prevNode, ((Back) obj).prevNode);
                }
                return true;
            }

            public int hashCode() {
                ReportNode reportNode = this.prevNode;
                if (reportNode != null) {
                    return reportNode.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Back(prevNode=");
                R.append(this.prevNode);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Initial;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;", "Lcom/discord/api/report/ReportNode;", "component1", "()Lcom/discord/api/report/ReportNode;", "initialNode", "copy", "(Lcom/discord/api/report/ReportNode;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Initial;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/report/ReportNode;", HookHelper.constructorName, "(Lcom/discord/api/report/ReportNode;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Initial extends NodeNavigationType {
            private final ReportNode initialNode;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Initial(ReportNode reportNode) {
                super(reportNode, null);
                m.checkNotNullParameter(reportNode, "initialNode");
                this.initialNode = reportNode;
            }

            private final ReportNode component1() {
                return this.initialNode;
            }

            public static /* synthetic */ Initial copy$default(Initial initial, ReportNode reportNode, int i, Object obj) {
                if ((i & 1) != 0) {
                    reportNode = initial.initialNode;
                }
                return initial.copy(reportNode);
            }

            public final Initial copy(ReportNode reportNode) {
                m.checkNotNullParameter(reportNode, "initialNode");
                return new Initial(reportNode);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Initial) && m.areEqual(this.initialNode, ((Initial) obj).initialNode);
                }
                return true;
            }

            public int hashCode() {
                ReportNode reportNode = this.initialNode;
                if (reportNode != null) {
                    return reportNode.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Initial(initialNode=");
                R.append(this.initialNode);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Next;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;", "Lcom/discord/api/report/ReportNode;", "component1", "()Lcom/discord/api/report/ReportNode;", "nextNode", "copy", "(Lcom/discord/api/report/ReportNode;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType$Next;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/report/ReportNode;", HookHelper.constructorName, "(Lcom/discord/api/report/ReportNode;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Next extends NodeNavigationType {
            private final ReportNode nextNode;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Next(ReportNode reportNode) {
                super(reportNode, null);
                m.checkNotNullParameter(reportNode, "nextNode");
                this.nextNode = reportNode;
            }

            private final ReportNode component1() {
                return this.nextNode;
            }

            public static /* synthetic */ Next copy$default(Next next, ReportNode reportNode, int i, Object obj) {
                if ((i & 1) != 0) {
                    reportNode = next.nextNode;
                }
                return next.copy(reportNode);
            }

            public final Next copy(ReportNode reportNode) {
                m.checkNotNullParameter(reportNode, "nextNode");
                return new Next(reportNode);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Next) && m.areEqual(this.nextNode, ((Next) obj).nextNode);
                }
                return true;
            }

            public int hashCode() {
                ReportNode reportNode = this.nextNode;
                if (reportNode != null) {
                    return reportNode.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Next(nextNode=");
                R.append(this.nextNode);
                R.append(")");
                return R.toString();
            }
        }

        private NodeNavigationType(ReportNode reportNode) {
            this.node = reportNode;
        }

        public final ReportNode getNode() {
            return this.node;
        }

        public /* synthetic */ NodeNavigationType(ReportNode reportNode, DefaultConstructorMarker defaultConstructorMarker) {
            this(reportNode);
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u001e\b\u0086\b\u0018\u00002\u00020\u0001Bw\u0012\u0006\u0010$\u001a\u00020\u0002\u0012\b\u0010%\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010&\u001a\u0004\u0018\u00010\b\u0012\b\u0010'\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010(\u001a\u0004\u0018\u00010\u000e\u0012\b\u0010)\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010*\u001a\u0004\u0018\u00010\u0014\u0012\u000e\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017\u0012\u0006\u0010,\u001a\u00020\u001b\u0012\b\u0010-\u001a\u0004\u0018\u00010\u001e\u0012\b\u0010.\u001a\u0004\u0018\u00010!¢\u0006\u0004\bP\u0010QJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0018\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0012\u0010\"\u001a\u0004\u0018\u00010!HÆ\u0003¢\u0006\u0004\b\"\u0010#J\u0096\u0001\u0010/\u001a\u00020\u00002\b\b\u0002\u0010$\u001a\u00020\u00022\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00142\u0010\b\u0002\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00172\b\b\u0002\u0010,\u001a\u00020\u001b2\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010.\u001a\u0004\u0018\u00010!HÆ\u0001¢\u0006\u0004\b/\u00100J\u0010\u00102\u001a\u000201HÖ\u0001¢\u0006\u0004\b2\u00103J\u0010\u00105\u001a\u000204HÖ\u0001¢\u0006\u0004\b5\u00106J\u001a\u00108\u001a\u00020\u001b2\b\u00107\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b8\u00109R\u0019\u0010,\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010:\u001a\u0004\b;\u0010\u001dR\u001b\u0010&\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010<\u001a\u0004\b=\u0010\nR\u001b\u0010'\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010>\u001a\u0004\b?\u0010\rR\u001b\u0010)\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010@\u001a\u0004\bA\u0010\u0013R!\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010B\u001a\u0004\bC\u0010\u001aR\u001b\u0010%\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010D\u001a\u0004\bE\u0010\u0007R\u001b\u0010.\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010F\u001a\u0004\bG\u0010#R\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010H\u001a\u0004\bI\u0010\u0004R\u001b\u0010-\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010J\u001a\u0004\bK\u0010 R\u001b\u0010*\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010L\u001a\u0004\bM\u0010\u0016R\u001b\u0010(\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010N\u001a\u0004\bO\u0010\u0010¨\u0006R"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "", "Lcom/discord/api/report/ReportNode;", "component1", "()Lcom/discord/api/report/ReportNode;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$CheckboxElement;", "component2", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$CheckboxElement;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "component3", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "component4", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "component5", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "component6", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "component7", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "", "Lcom/discord/api/report/NodeResult;", "component8", "()Ljava/util/List;", "", "component9", "()Z", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", "component10", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", "Lcom/discord/api/report/ReportNodeBottomButton;", "component11", "()Lcom/discord/api/report/ReportNodeBottomButton;", "node", "checkboxElement", "messagePreviewElement", "channelPreviewElement", "directoryServerPreviewElement", "eventPreviewElement", "blockUserElement", "breadcrumbsElement", "successElement", "submitState", "bottomButton", "copy", "(Lcom/discord/api/report/ReportNode;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$CheckboxElement;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;Ljava/util/List;ZLcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;Lcom/discord/api/report/ReportNodeBottomButton;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getSuccessElement", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "getMessagePreviewElement", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "getChannelPreviewElement", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "getEventPreviewElement", "Ljava/util/List;", "getBreadcrumbsElement", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$CheckboxElement;", "getCheckboxElement", "Lcom/discord/api/report/ReportNodeBottomButton;", "getBottomButton", "Lcom/discord/api/report/ReportNode;", "getNode", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", "getSubmitState", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "getBlockUserElement", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "getDirectoryServerPreviewElement", HookHelper.constructorName, "(Lcom/discord/api/report/ReportNode;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$CheckboxElement;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;Ljava/util/List;ZLcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;Lcom/discord/api/report/ReportNodeBottomButton;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NodeState {
        private final BlockUserElement blockUserElement;
        private final ReportNodeBottomButton bottomButton;
        private final List<NodeResult> breadcrumbsElement;
        private final ChannelPreview channelPreviewElement;
        private final CheckboxElement checkboxElement;
        private final DirectoryServerPreview directoryServerPreviewElement;
        private final GuildScheduledEventPreview eventPreviewElement;
        private final MessagePreview messagePreviewElement;
        private final ReportNode node;
        private final SubmitState submitState;
        private final boolean successElement;

        public NodeState(ReportNode reportNode, CheckboxElement checkboxElement, MessagePreview messagePreview, ChannelPreview channelPreview, DirectoryServerPreview directoryServerPreview, GuildScheduledEventPreview guildScheduledEventPreview, BlockUserElement blockUserElement, List<NodeResult> list, boolean z2, SubmitState submitState, ReportNodeBottomButton reportNodeBottomButton) {
            m.checkNotNullParameter(reportNode, "node");
            this.node = reportNode;
            this.checkboxElement = checkboxElement;
            this.messagePreviewElement = messagePreview;
            this.channelPreviewElement = channelPreview;
            this.directoryServerPreviewElement = directoryServerPreview;
            this.eventPreviewElement = guildScheduledEventPreview;
            this.blockUserElement = blockUserElement;
            this.breadcrumbsElement = list;
            this.successElement = z2;
            this.submitState = submitState;
            this.bottomButton = reportNodeBottomButton;
        }

        public final ReportNode component1() {
            return this.node;
        }

        public final SubmitState component10() {
            return this.submitState;
        }

        public final ReportNodeBottomButton component11() {
            return this.bottomButton;
        }

        public final CheckboxElement component2() {
            return this.checkboxElement;
        }

        public final MessagePreview component3() {
            return this.messagePreviewElement;
        }

        public final ChannelPreview component4() {
            return this.channelPreviewElement;
        }

        public final DirectoryServerPreview component5() {
            return this.directoryServerPreviewElement;
        }

        public final GuildScheduledEventPreview component6() {
            return this.eventPreviewElement;
        }

        public final BlockUserElement component7() {
            return this.blockUserElement;
        }

        public final List<NodeResult> component8() {
            return this.breadcrumbsElement;
        }

        public final boolean component9() {
            return this.successElement;
        }

        public final NodeState copy(ReportNode reportNode, CheckboxElement checkboxElement, MessagePreview messagePreview, ChannelPreview channelPreview, DirectoryServerPreview directoryServerPreview, GuildScheduledEventPreview guildScheduledEventPreview, BlockUserElement blockUserElement, List<NodeResult> list, boolean z2, SubmitState submitState, ReportNodeBottomButton reportNodeBottomButton) {
            m.checkNotNullParameter(reportNode, "node");
            return new NodeState(reportNode, checkboxElement, messagePreview, channelPreview, directoryServerPreview, guildScheduledEventPreview, blockUserElement, list, z2, submitState, reportNodeBottomButton);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof NodeState)) {
                return false;
            }
            NodeState nodeState = (NodeState) obj;
            return m.areEqual(this.node, nodeState.node) && m.areEqual(this.checkboxElement, nodeState.checkboxElement) && m.areEqual(this.messagePreviewElement, nodeState.messagePreviewElement) && m.areEqual(this.channelPreviewElement, nodeState.channelPreviewElement) && m.areEqual(this.directoryServerPreviewElement, nodeState.directoryServerPreviewElement) && m.areEqual(this.eventPreviewElement, nodeState.eventPreviewElement) && m.areEqual(this.blockUserElement, nodeState.blockUserElement) && m.areEqual(this.breadcrumbsElement, nodeState.breadcrumbsElement) && this.successElement == nodeState.successElement && m.areEqual(this.submitState, nodeState.submitState) && m.areEqual(this.bottomButton, nodeState.bottomButton);
        }

        public final BlockUserElement getBlockUserElement() {
            return this.blockUserElement;
        }

        public final ReportNodeBottomButton getBottomButton() {
            return this.bottomButton;
        }

        public final List<NodeResult> getBreadcrumbsElement() {
            return this.breadcrumbsElement;
        }

        public final ChannelPreview getChannelPreviewElement() {
            return this.channelPreviewElement;
        }

        public final CheckboxElement getCheckboxElement() {
            return this.checkboxElement;
        }

        public final DirectoryServerPreview getDirectoryServerPreviewElement() {
            return this.directoryServerPreviewElement;
        }

        public final GuildScheduledEventPreview getEventPreviewElement() {
            return this.eventPreviewElement;
        }

        public final MessagePreview getMessagePreviewElement() {
            return this.messagePreviewElement;
        }

        public final ReportNode getNode() {
            return this.node;
        }

        public final SubmitState getSubmitState() {
            return this.submitState;
        }

        public final boolean getSuccessElement() {
            return this.successElement;
        }

        public int hashCode() {
            ReportNode reportNode = this.node;
            int i = 0;
            int hashCode = (reportNode != null ? reportNode.hashCode() : 0) * 31;
            CheckboxElement checkboxElement = this.checkboxElement;
            int hashCode2 = (hashCode + (checkboxElement != null ? checkboxElement.hashCode() : 0)) * 31;
            MessagePreview messagePreview = this.messagePreviewElement;
            int hashCode3 = (hashCode2 + (messagePreview != null ? messagePreview.hashCode() : 0)) * 31;
            ChannelPreview channelPreview = this.channelPreviewElement;
            int hashCode4 = (hashCode3 + (channelPreview != null ? channelPreview.hashCode() : 0)) * 31;
            DirectoryServerPreview directoryServerPreview = this.directoryServerPreviewElement;
            int hashCode5 = (hashCode4 + (directoryServerPreview != null ? directoryServerPreview.hashCode() : 0)) * 31;
            GuildScheduledEventPreview guildScheduledEventPreview = this.eventPreviewElement;
            int hashCode6 = (hashCode5 + (guildScheduledEventPreview != null ? guildScheduledEventPreview.hashCode() : 0)) * 31;
            BlockUserElement blockUserElement = this.blockUserElement;
            int hashCode7 = (hashCode6 + (blockUserElement != null ? blockUserElement.hashCode() : 0)) * 31;
            List<NodeResult> list = this.breadcrumbsElement;
            int hashCode8 = (hashCode7 + (list != null ? list.hashCode() : 0)) * 31;
            boolean z2 = this.successElement;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode8 + i2) * 31;
            SubmitState submitState = this.submitState;
            int hashCode9 = (i4 + (submitState != null ? submitState.hashCode() : 0)) * 31;
            ReportNodeBottomButton reportNodeBottomButton = this.bottomButton;
            if (reportNodeBottomButton != null) {
                i = reportNodeBottomButton.hashCode();
            }
            return hashCode9 + i;
        }

        public String toString() {
            StringBuilder R = a.R("NodeState(node=");
            R.append(this.node);
            R.append(", checkboxElement=");
            R.append(this.checkboxElement);
            R.append(", messagePreviewElement=");
            R.append(this.messagePreviewElement);
            R.append(", channelPreviewElement=");
            R.append(this.channelPreviewElement);
            R.append(", directoryServerPreviewElement=");
            R.append(this.directoryServerPreviewElement);
            R.append(", eventPreviewElement=");
            R.append(this.eventPreviewElement);
            R.append(", blockUserElement=");
            R.append(this.blockUserElement);
            R.append(", breadcrumbsElement=");
            R.append(this.breadcrumbsElement);
            R.append(", successElement=");
            R.append(this.successElement);
            R.append(", submitState=");
            R.append(this.submitState);
            R.append(", bottomButton=");
            R.append(this.bottomButton);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u000e\u0012\u001a\u0010 \u001a\u0016\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u0011\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0018¢\u0006\u0004\b;\u0010<J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J$\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u0011HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJv\u0010\"\u001a\u00020\u00002\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u000e2\u001c\b\u0002\u0010 \u001a\u0016\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u00112\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0018HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b'\u0010(J\u001a\u0010+\u001a\u00020*2\b\u0010)\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b+\u0010,R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010-\u001a\u0004\b.\u0010\u0004R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010/\u001a\u0004\b0\u0010\u0007R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b2\u0010\u0010R\u001b\u0010!\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b!\u00103\u001a\u0004\b4\u0010\u001aR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00105\u001a\u0004\b6\u0010\rR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00107\u001a\u0004\b8\u0010\nR-\u0010 \u001a\u0016\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u00118\u0006@\u0006¢\u0006\f\n\u0004\b \u00109\u001a\u0004\b:\u0010\u0017¨\u0006="}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;", "", "Lcom/discord/models/message/Message;", "component1", "()Lcom/discord/models/message/Message;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/directory/DirectoryEntryGuild;", "component4", "()Lcom/discord/api/directory/DirectoryEntryGuild;", "Lcom/discord/api/stageinstance/StageInstance;", "component5", "()Lcom/discord/api/stageinstance/StageInstance;", "", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "component6", "()Ljava/util/Map;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component7", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "message", "channel", "guild", "directoryEntry", "stageInstance", "blockedUsers", "event", "copy", "(Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/api/directory/DirectoryEntryGuild;Lcom/discord/api/stageinstance/StageInstance;Ljava/util/Map;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/message/Message;", "getMessage", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getEvent", "Lcom/discord/api/directory/DirectoryEntryGuild;", "getDirectoryEntry", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/util/Map;", "getBlockedUsers", HookHelper.constructorName, "(Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/api/directory/DirectoryEntryGuild;Lcom/discord/api/stageinstance/StageInstance;Ljava/util/Map;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, Integer> blockedUsers;
        private final Channel channel;
        private final DirectoryEntryGuild directoryEntry;
        private final GuildScheduledEvent event;
        private final Guild guild;
        private final Message message;
        private final StageInstance stageInstance;

        public StoreState(Message message, Channel channel, Guild guild, DirectoryEntryGuild directoryEntryGuild, StageInstance stageInstance, Map<Long, Integer> map, GuildScheduledEvent guildScheduledEvent) {
            m.checkNotNullParameter(map, "blockedUsers");
            this.message = message;
            this.channel = channel;
            this.guild = guild;
            this.directoryEntry = directoryEntryGuild;
            this.stageInstance = stageInstance;
            this.blockedUsers = map;
            this.event = guildScheduledEvent;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Message message, Channel channel, Guild guild, DirectoryEntryGuild directoryEntryGuild, StageInstance stageInstance, Map map, GuildScheduledEvent guildScheduledEvent, int i, Object obj) {
            if ((i & 1) != 0) {
                message = storeState.message;
            }
            if ((i & 2) != 0) {
                channel = storeState.channel;
            }
            Channel channel2 = channel;
            if ((i & 4) != 0) {
                guild = storeState.guild;
            }
            Guild guild2 = guild;
            if ((i & 8) != 0) {
                directoryEntryGuild = storeState.directoryEntry;
            }
            DirectoryEntryGuild directoryEntryGuild2 = directoryEntryGuild;
            if ((i & 16) != 0) {
                stageInstance = storeState.stageInstance;
            }
            StageInstance stageInstance2 = stageInstance;
            Map<Long, Integer> map2 = map;
            if ((i & 32) != 0) {
                map2 = storeState.blockedUsers;
            }
            Map map3 = map2;
            if ((i & 64) != 0) {
                guildScheduledEvent = storeState.event;
            }
            return storeState.copy(message, channel2, guild2, directoryEntryGuild2, stageInstance2, map3, guildScheduledEvent);
        }

        public final Message component1() {
            return this.message;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final Guild component3() {
            return this.guild;
        }

        public final DirectoryEntryGuild component4() {
            return this.directoryEntry;
        }

        public final StageInstance component5() {
            return this.stageInstance;
        }

        public final Map<Long, Integer> component6() {
            return this.blockedUsers;
        }

        public final GuildScheduledEvent component7() {
            return this.event;
        }

        public final StoreState copy(Message message, Channel channel, Guild guild, DirectoryEntryGuild directoryEntryGuild, StageInstance stageInstance, Map<Long, Integer> map, GuildScheduledEvent guildScheduledEvent) {
            m.checkNotNullParameter(map, "blockedUsers");
            return new StoreState(message, channel, guild, directoryEntryGuild, stageInstance, map, guildScheduledEvent);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.message, storeState.message) && m.areEqual(this.channel, storeState.channel) && m.areEqual(this.guild, storeState.guild) && m.areEqual(this.directoryEntry, storeState.directoryEntry) && m.areEqual(this.stageInstance, storeState.stageInstance) && m.areEqual(this.blockedUsers, storeState.blockedUsers) && m.areEqual(this.event, storeState.event);
        }

        public final Map<Long, Integer> getBlockedUsers() {
            return this.blockedUsers;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final DirectoryEntryGuild getDirectoryEntry() {
            return this.directoryEntry;
        }

        public final GuildScheduledEvent getEvent() {
            return this.event;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Message getMessage() {
            return this.message;
        }

        public final StageInstance getStageInstance() {
            return this.stageInstance;
        }

        public int hashCode() {
            Message message = this.message;
            int i = 0;
            int hashCode = (message != null ? message.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            int hashCode3 = (hashCode2 + (guild != null ? guild.hashCode() : 0)) * 31;
            DirectoryEntryGuild directoryEntryGuild = this.directoryEntry;
            int hashCode4 = (hashCode3 + (directoryEntryGuild != null ? directoryEntryGuild.hashCode() : 0)) * 31;
            StageInstance stageInstance = this.stageInstance;
            int hashCode5 = (hashCode4 + (stageInstance != null ? stageInstance.hashCode() : 0)) * 31;
            Map<Long, Integer> map = this.blockedUsers;
            int hashCode6 = (hashCode5 + (map != null ? map.hashCode() : 0)) * 31;
            GuildScheduledEvent guildScheduledEvent = this.event;
            if (guildScheduledEvent != null) {
                i = guildScheduledEvent.hashCode();
            }
            return hashCode6 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(message=");
            R.append(this.message);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", directoryEntry=");
            R.append(this.directoryEntry);
            R.append(", stageInstance=");
            R.append(this.stageInstance);
            R.append(", blockedUsers=");
            R.append(this.blockedUsers);
            R.append(", event=");
            R.append(this.event);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", "", HookHelper.constructorName, "()V", "Error", "Loading", "None", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState$None;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState$Loading;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class SubmitState {

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState$Error;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends SubmitState {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState$Loading;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends SubmitState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState$None;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class None extends SubmitState {
            public static final None INSTANCE = new None();

            private None() {
                super(null);
            }
        }

        private SubmitState() {
        }

        public /* synthetic */ SubmitState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: MobileReportsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Menu", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Menu;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: MobileReportsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u0001Ba\u0012\u0006\u0010$\u001a\u00020\b\u0012\b\u0010%\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010&\u001a\u0004\u0018\u00010\u000e\u0012\b\u0010'\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010(\u001a\u0004\u0018\u00010\u0014\u0012\u0006\u0010)\u001a\u00020\u0017\u0012\b\b\u0002\u0010*\u001a\u00020\u001a\u0012\f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d\u0012\b\u0010,\u001a\u0004\u0018\u00010!¢\u0006\u0004\bK\u0010LJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0016\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001dHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0012\u0010\"\u001a\u0004\u0018\u00010!HÆ\u0003¢\u0006\u0004\b\"\u0010#Jz\u0010-\u001a\u00020\u00002\b\b\u0002\u0010$\u001a\u00020\b2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010)\u001a\u00020\u00172\b\b\u0002\u0010*\u001a\u00020\u001a2\u000e\b\u0002\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d2\n\b\u0002\u0010,\u001a\u0004\u0018\u00010!HÆ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u00100\u001a\u00020/HÖ\u0001¢\u0006\u0004\b0\u00101J\u0010\u00103\u001a\u000202HÖ\u0001¢\u0006\u0004\b3\u00104J\u001a\u00107\u001a\u00020\u00022\b\u00106\u001a\u0004\u0018\u000105HÖ\u0003¢\u0006\u0004\b7\u00108R\u001b\u0010,\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b,\u00109\u001a\u0004\b:\u0010#R\u001f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010;\u001a\u0004\b<\u0010 R\u001b\u0010'\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010=\u001a\u0004\b>\u0010\u0013R\u001b\u0010(\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010?\u001a\u0004\b@\u0010\u0016R\u0019\u0010*\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010A\u001a\u0004\bB\u0010\u001cR\u001b\u0010%\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010C\u001a\u0004\bD\u0010\rR\u001b\u0010&\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010E\u001a\u0004\bF\u0010\u0010R\u0019\u0010)\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010G\u001a\u0004\bH\u0010\u0019R\u0019\u0010$\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010I\u001a\u0004\bJ\u0010\n¨\u0006M"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Menu;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState;", "", "shouldHideBackArrow", "()Z", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "genNodeState", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "Lcom/discord/api/report/MenuAPIResponse;", "component1", "()Lcom/discord/api/report/MenuAPIResponse;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "component2", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "component3", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "component4", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "component5", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;", "component6", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", "component7", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", "", "Lcom/discord/api/report/NodeResult;", "component8", "()Ljava/util/List;", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "component9", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "menu", "messagePreview", "channelPreview", "directoryServerPreview", "eventPreview", "nodeNavigationType", "submitState", "history", "blockUserElement", "copy", "(Lcom/discord/api/report/MenuAPIResponse;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;Ljava/util/List;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;)Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ViewState$Menu;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;", "getBlockUserElement", "Ljava/util/List;", "getHistory", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;", "getDirectoryServerPreview", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;", "getEventPreview", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;", "getSubmitState", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;", "getMessagePreview", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;", "getChannelPreview", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;", "getNodeNavigationType", "Lcom/discord/api/report/MenuAPIResponse;", "getMenu", HookHelper.constructorName, "(Lcom/discord/api/report/MenuAPIResponse;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$MessagePreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$ChannelPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$DirectoryServerPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$GuildScheduledEventPreview;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeNavigationType;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$SubmitState;Ljava/util/List;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$BlockUserElement;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Menu extends ViewState {
            private final BlockUserElement blockUserElement;
            private final ChannelPreview channelPreview;
            private final DirectoryServerPreview directoryServerPreview;
            private final GuildScheduledEventPreview eventPreview;
            private final List<NodeResult> history;
            private final MenuAPIResponse menu;
            private final MessagePreview messagePreview;
            private final NodeNavigationType nodeNavigationType;
            private final SubmitState submitState;

            public /* synthetic */ Menu(MenuAPIResponse menuAPIResponse, MessagePreview messagePreview, ChannelPreview channelPreview, DirectoryServerPreview directoryServerPreview, GuildScheduledEventPreview guildScheduledEventPreview, NodeNavigationType nodeNavigationType, SubmitState submitState, List list, BlockUserElement blockUserElement, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(menuAPIResponse, messagePreview, channelPreview, directoryServerPreview, guildScheduledEventPreview, nodeNavigationType, (i & 64) != 0 ? SubmitState.None.INSTANCE : submitState, list, blockUserElement);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Menu copy$default(Menu menu, MenuAPIResponse menuAPIResponse, MessagePreview messagePreview, ChannelPreview channelPreview, DirectoryServerPreview directoryServerPreview, GuildScheduledEventPreview guildScheduledEventPreview, NodeNavigationType nodeNavigationType, SubmitState submitState, List list, BlockUserElement blockUserElement, int i, Object obj) {
                return menu.copy((i & 1) != 0 ? menu.menu : menuAPIResponse, (i & 2) != 0 ? menu.messagePreview : messagePreview, (i & 4) != 0 ? menu.channelPreview : channelPreview, (i & 8) != 0 ? menu.directoryServerPreview : directoryServerPreview, (i & 16) != 0 ? menu.eventPreview : guildScheduledEventPreview, (i & 32) != 0 ? menu.nodeNavigationType : nodeNavigationType, (i & 64) != 0 ? menu.submitState : submitState, (i & 128) != 0 ? menu.history : list, (i & 256) != 0 ? menu.blockUserElement : blockUserElement);
            }

            public final MenuAPIResponse component1() {
                return this.menu;
            }

            public final MessagePreview component2() {
                return this.messagePreview;
            }

            public final ChannelPreview component3() {
                return this.channelPreview;
            }

            public final DirectoryServerPreview component4() {
                return this.directoryServerPreview;
            }

            public final GuildScheduledEventPreview component5() {
                return this.eventPreview;
            }

            public final NodeNavigationType component6() {
                return this.nodeNavigationType;
            }

            public final SubmitState component7() {
                return this.submitState;
            }

            public final List<NodeResult> component8() {
                return this.history;
            }

            public final BlockUserElement component9() {
                return this.blockUserElement;
            }

            public final Menu copy(MenuAPIResponse menuAPIResponse, MessagePreview messagePreview, ChannelPreview channelPreview, DirectoryServerPreview directoryServerPreview, GuildScheduledEventPreview guildScheduledEventPreview, NodeNavigationType nodeNavigationType, SubmitState submitState, List<NodeResult> list, BlockUserElement blockUserElement) {
                m.checkNotNullParameter(menuAPIResponse, "menu");
                m.checkNotNullParameter(nodeNavigationType, "nodeNavigationType");
                m.checkNotNullParameter(submitState, "submitState");
                m.checkNotNullParameter(list, "history");
                return new Menu(menuAPIResponse, messagePreview, channelPreview, directoryServerPreview, guildScheduledEventPreview, nodeNavigationType, submitState, list, blockUserElement);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Menu)) {
                    return false;
                }
                Menu menu = (Menu) obj;
                return m.areEqual(this.menu, menu.menu) && m.areEqual(this.messagePreview, menu.messagePreview) && m.areEqual(this.channelPreview, menu.channelPreview) && m.areEqual(this.directoryServerPreview, menu.directoryServerPreview) && m.areEqual(this.eventPreview, menu.eventPreview) && m.areEqual(this.nodeNavigationType, menu.nodeNavigationType) && m.areEqual(this.submitState, menu.submitState) && m.areEqual(this.history, menu.history) && m.areEqual(this.blockUserElement, menu.blockUserElement);
            }

            public final NodeState genNodeState() {
                Object obj;
                Object obj2;
                CheckboxElement checkboxElement;
                Object obj3;
                Object obj4;
                Object obj5;
                Object obj6;
                Object obj7;
                Object obj8;
                ReportNode node = this.nodeNavigationType.getNode();
                List<ReportNodeElement> c = node.c();
                ReportNodeElement.Companion companion = ReportNodeElement.Companion;
                Iterator<T> it = c.iterator();
                while (true) {
                    obj = null;
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    ReportNodeElement reportNodeElement = (ReportNodeElement) obj2;
                    Objects.requireNonNull(companion);
                    m.checkNotNullParameter(reportNodeElement, "element");
                    if (m.areEqual(reportNodeElement.c(), "checkbox")) {
                        break;
                    }
                }
                ReportNodeElement reportNodeElement2 = (ReportNodeElement) obj2;
                if (reportNodeElement2 != null) {
                    List<ReportNodeElementData> a = reportNodeElement2.a();
                    checkboxElement = a == null ? null : new CheckboxElement(reportNodeElement2.b(), a, new HashSet());
                } else {
                    checkboxElement = null;
                }
                List<ReportNodeElement> c2 = node.c();
                ReportNodeElement.Companion companion2 = ReportNodeElement.Companion;
                Iterator<T> it2 = c2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        obj3 = null;
                        break;
                    }
                    obj3 = it2.next();
                    ReportNodeElement reportNodeElement3 = (ReportNodeElement) obj3;
                    Objects.requireNonNull(companion2);
                    m.checkNotNullParameter(reportNodeElement3, "element");
                    if (m.areEqual(reportNodeElement3.c(), "message_preview")) {
                        break;
                    }
                }
                MessagePreview messagePreview = ((ReportNodeElement) obj3) != null ? this.messagePreview : null;
                List<ReportNodeElement> c3 = node.c();
                ReportNodeElement.Companion companion3 = ReportNodeElement.Companion;
                Iterator<T> it3 = c3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        obj4 = null;
                        break;
                    }
                    obj4 = it3.next();
                    ReportNodeElement reportNodeElement4 = (ReportNodeElement) obj4;
                    Objects.requireNonNull(companion3);
                    m.checkNotNullParameter(reportNodeElement4, "element");
                    if (m.areEqual(reportNodeElement4.c(), "channel_preview")) {
                        break;
                    }
                }
                ChannelPreview channelPreview = ((ReportNodeElement) obj4) != null ? this.channelPreview : null;
                List<ReportNodeElement> c4 = node.c();
                ReportNodeElement.Companion companion4 = ReportNodeElement.Companion;
                Iterator<T> it4 = c4.iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        obj5 = null;
                        break;
                    }
                    obj5 = it4.next();
                    ReportNodeElement reportNodeElement5 = (ReportNodeElement) obj5;
                    Objects.requireNonNull(companion4);
                    m.checkNotNullParameter(reportNodeElement5, "element");
                    if (m.areEqual(reportNodeElement5.c(), "guild_directory_entry_preview")) {
                        break;
                    }
                }
                DirectoryServerPreview directoryServerPreview = ((ReportNodeElement) obj5) != null ? this.directoryServerPreview : null;
                List<ReportNodeElement> c5 = node.c();
                ReportNodeElement.Companion companion5 = ReportNodeElement.Companion;
                Iterator<T> it5 = c5.iterator();
                while (true) {
                    if (!it5.hasNext()) {
                        obj6 = null;
                        break;
                    }
                    obj6 = it5.next();
                    ReportNodeElement reportNodeElement6 = (ReportNodeElement) obj6;
                    Objects.requireNonNull(companion5);
                    m.checkNotNullParameter(reportNodeElement6, "element");
                    if (m.areEqual(reportNodeElement6.c(), "guild_scheduled_event_preview")) {
                        break;
                    }
                }
                GuildScheduledEventPreview guildScheduledEventPreview = ((ReportNodeElement) obj6) != null ? this.eventPreview : null;
                List<ReportNodeElement> c6 = node.c();
                ReportNodeElement.Companion companion6 = ReportNodeElement.Companion;
                Iterator<T> it6 = c6.iterator();
                while (true) {
                    if (!it6.hasNext()) {
                        obj7 = null;
                        break;
                    }
                    obj7 = it6.next();
                    ReportNodeElement reportNodeElement7 = (ReportNodeElement) obj7;
                    Objects.requireNonNull(companion6);
                    m.checkNotNullParameter(reportNodeElement7, "element");
                    if (m.areEqual(reportNodeElement7.c(), "block_users")) {
                        break;
                    }
                }
                BlockUserElement blockUserElement = ((ReportNodeElement) obj7) != null ? this.blockUserElement : null;
                List<ReportNodeElement> c7 = node.c();
                ReportNodeElement.Companion companion7 = ReportNodeElement.Companion;
                Iterator<T> it7 = c7.iterator();
                while (true) {
                    if (!it7.hasNext()) {
                        obj8 = null;
                        break;
                    }
                    obj8 = it7.next();
                    ReportNodeElement reportNodeElement8 = (ReportNodeElement) obj8;
                    Objects.requireNonNull(companion7);
                    m.checkNotNullParameter(reportNodeElement8, "element");
                    if (m.areEqual(reportNodeElement8.c(), "breadcrumbs")) {
                        break;
                    }
                }
                List<NodeResult> list = ((ReportNodeElement) obj8) != null ? this.history : null;
                List<ReportNodeElement> c8 = node.c();
                ReportNodeElement.Companion companion8 = ReportNodeElement.Companion;
                Iterator<T> it8 = c8.iterator();
                while (true) {
                    if (!it8.hasNext()) {
                        break;
                    }
                    Object next = it8.next();
                    ReportNodeElement reportNodeElement9 = (ReportNodeElement) next;
                    Objects.requireNonNull(companion8);
                    m.checkNotNullParameter(reportNodeElement9, "element");
                    if (m.areEqual(reportNodeElement9.c(), "success")) {
                        obj = next;
                        break;
                    }
                }
                return new NodeState(node, checkboxElement, messagePreview, channelPreview, directoryServerPreview, guildScheduledEventPreview, blockUserElement, list, obj != null, this.submitState, node.a());
            }

            public final BlockUserElement getBlockUserElement() {
                return this.blockUserElement;
            }

            public final ChannelPreview getChannelPreview() {
                return this.channelPreview;
            }

            public final DirectoryServerPreview getDirectoryServerPreview() {
                return this.directoryServerPreview;
            }

            public final GuildScheduledEventPreview getEventPreview() {
                return this.eventPreview;
            }

            public final List<NodeResult> getHistory() {
                return this.history;
            }

            public final MenuAPIResponse getMenu() {
                return this.menu;
            }

            public final MessagePreview getMessagePreview() {
                return this.messagePreview;
            }

            public final NodeNavigationType getNodeNavigationType() {
                return this.nodeNavigationType;
            }

            public final SubmitState getSubmitState() {
                return this.submitState;
            }

            public int hashCode() {
                MenuAPIResponse menuAPIResponse = this.menu;
                int i = 0;
                int hashCode = (menuAPIResponse != null ? menuAPIResponse.hashCode() : 0) * 31;
                MessagePreview messagePreview = this.messagePreview;
                int hashCode2 = (hashCode + (messagePreview != null ? messagePreview.hashCode() : 0)) * 31;
                ChannelPreview channelPreview = this.channelPreview;
                int hashCode3 = (hashCode2 + (channelPreview != null ? channelPreview.hashCode() : 0)) * 31;
                DirectoryServerPreview directoryServerPreview = this.directoryServerPreview;
                int hashCode4 = (hashCode3 + (directoryServerPreview != null ? directoryServerPreview.hashCode() : 0)) * 31;
                GuildScheduledEventPreview guildScheduledEventPreview = this.eventPreview;
                int hashCode5 = (hashCode4 + (guildScheduledEventPreview != null ? guildScheduledEventPreview.hashCode() : 0)) * 31;
                NodeNavigationType nodeNavigationType = this.nodeNavigationType;
                int hashCode6 = (hashCode5 + (nodeNavigationType != null ? nodeNavigationType.hashCode() : 0)) * 31;
                SubmitState submitState = this.submitState;
                int hashCode7 = (hashCode6 + (submitState != null ? submitState.hashCode() : 0)) * 31;
                List<NodeResult> list = this.history;
                int hashCode8 = (hashCode7 + (list != null ? list.hashCode() : 0)) * 31;
                BlockUserElement blockUserElement = this.blockUserElement;
                if (blockUserElement != null) {
                    i = blockUserElement.hashCode();
                }
                return hashCode8 + i;
            }

            public final boolean shouldHideBackArrow() {
                return (this.submitState instanceof SubmitState.Loading) || this.nodeNavigationType.getNode().e() == this.menu.e() || this.nodeNavigationType.getNode().e() == this.menu.d();
            }

            public String toString() {
                StringBuilder R = a.R("Menu(menu=");
                R.append(this.menu);
                R.append(", messagePreview=");
                R.append(this.messagePreview);
                R.append(", channelPreview=");
                R.append(this.channelPreview);
                R.append(", directoryServerPreview=");
                R.append(this.directoryServerPreview);
                R.append(", eventPreview=");
                R.append(this.eventPreview);
                R.append(", nodeNavigationType=");
                R.append(this.nodeNavigationType);
                R.append(", submitState=");
                R.append(this.submitState);
                R.append(", history=");
                R.append(this.history);
                R.append(", blockUserElement=");
                R.append(this.blockUserElement);
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Menu(MenuAPIResponse menuAPIResponse, MessagePreview messagePreview, ChannelPreview channelPreview, DirectoryServerPreview directoryServerPreview, GuildScheduledEventPreview guildScheduledEventPreview, NodeNavigationType nodeNavigationType, SubmitState submitState, List<NodeResult> list, BlockUserElement blockUserElement) {
                super(null);
                m.checkNotNullParameter(menuAPIResponse, "menu");
                m.checkNotNullParameter(nodeNavigationType, "nodeNavigationType");
                m.checkNotNullParameter(submitState, "submitState");
                m.checkNotNullParameter(list, "history");
                this.menu = menuAPIResponse;
                this.messagePreview = messagePreview;
                this.channelPreview = channelPreview;
                this.directoryServerPreview = directoryServerPreview;
                this.eventPreview = guildScheduledEventPreview;
                this.nodeNavigationType = nodeNavigationType;
                this.submitState = submitState;
                this.history = list;
                this.blockUserElement = blockUserElement;
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ MobileReportsViewModel(WeakReference weakReference, MobileReportArgs mobileReportArgs, RestAPI restAPI, Clock clock, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(weakReference, mobileReportArgs, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 8) != 0 ? ClockFactory.get() : clock, (i & 16) != 0 ? Companion.getStoreState(mobileReportArgs) : observable);
    }

    public static final /* synthetic */ ViewState access$getViewState$p(MobileReportsViewModel mobileReportsViewModel) {
        return mobileReportsViewModel.getViewState();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ChannelPreview parseChannelPreview(StoreState storeState) {
        StageInstance stageInstance;
        Guild guild = storeState.getGuild();
        if (guild == null || (stageInstance = storeState.getStageInstance()) == null) {
            return null;
        }
        return new ChannelPreview(guild, stageInstance);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final DirectoryServerPreview parseDirectoryServerPreview(StoreState storeState) {
        DirectoryEntryGuild directoryEntry;
        Guild guild = storeState.getGuild();
        if (guild == null || (directoryEntry = storeState.getDirectoryEntry()) == null) {
            return null;
        }
        return new DirectoryServerPreview(guild, directoryEntry);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildScheduledEventPreview parseEventPreview(StoreState storeState) {
        Guild guild;
        GuildScheduledEvent event = storeState.getEvent();
        if (event == null || (guild = storeState.getGuild()) == null) {
            return null;
        }
        return new GuildScheduledEventPreview(guild, event);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final MessagePreview parseMessagePreview(StoreState storeState) {
        Message message;
        Map<Long, String> map;
        Long l;
        Context context = this.context.get();
        Long l2 = null;
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context.get() ?: return null");
            Channel channel = storeState.getChannel();
            if (!(channel == null || (message = storeState.getMessage()) == null)) {
                StoreStream.Companion companion = StoreStream.Companion;
                Map<Long, User> users = companion.getUsers().getUsers();
                com.discord.api.user.User author = message.getAuthor();
                User user = users.get(author != null ? Long.valueOf(author.i()) : null);
                if (user == null || (getViewState() instanceof ViewState.Invalid)) {
                    return null;
                }
                long id2 = companion.getUsers().getMe().getId();
                Map map2 = (Map) a.u0(channel, companion.getGuilds().getMembers());
                if (map2 == null) {
                    map2 = new HashMap();
                }
                Map map3 = map2;
                Map map4 = (Map) a.u0(channel, companion.getGuilds().getRoles());
                Map<Long, String> channelNames = companion.getChannels().getChannelNames();
                Map<Long, String> nickOrUsernames = MessageUtils.getNickOrUsernames(message, channel, map3, channel.n());
                String content = message.getContent();
                DraweeSpanStringBuilder parseChannelMessage = DiscordParser.parseChannelMessage(context, content != null ? new Regex("\n").replace(content, " ") : null, new MessageRenderContext(context, id2, false, nickOrUsernames, channelNames, map4, 0, null, null, 0, 0, null, null, null, 16320, null), new MessagePreprocessor(id2, null, null, false, 50, 6, null), DiscordParser.ParserOptions.REPLY, false);
                com.discord.api.user.User author2 = message.getAuthor();
                if (author2 != null) {
                    l = Long.valueOf(author2.i());
                    map = nickOrUsernames;
                } else {
                    map = nickOrUsernames;
                    l = null;
                }
                String str = map.get(l);
                if (str == null) {
                    com.discord.api.user.User author3 = message.getAuthor();
                    str = author3 != null ? author3.r() : null;
                }
                if (str == null) {
                    str = "";
                }
                String str2 = str;
                GuildMember.Companion companion2 = GuildMember.Companion;
                com.discord.api.user.User author4 = message.getAuthor();
                if (author4 != null) {
                    l2 = Long.valueOf(author4.i());
                }
                return new MessagePreview(parseChannelMessage, str2, companion2.getColor((GuildMember) map3.get(l2), ColorCompat.getThemedColor(context, (int) R.attr.colorHeaderPrimary)), user, message.hasEmbeds() || message.hasAttachments());
            }
        }
        return null;
    }

    public final boolean handleBack() {
        ViewState viewState = getViewState();
        ReportNode reportNode = null;
        if (!(viewState instanceof ViewState.Menu)) {
            viewState = null;
        }
        ViewState.Menu menu = (ViewState.Menu) viewState;
        if (menu == null) {
            return false;
        }
        NodeResult nodeResult = (NodeResult) u.lastOrNull((List<? extends Object>) menu.getHistory());
        if (nodeResult != null) {
            reportNode = nodeResult.c();
        }
        ReportNode node = menu.getNodeNavigationType().getNode();
        MenuAPIResponse menu2 = menu.getMenu();
        if (menu2.e() != node.e() && !(menu.getSubmitState() instanceof SubmitState.Loading)) {
            if (reportNode == null || menu2.d() == node.e()) {
                return false;
            }
            updateViewState(ViewState.Menu.copy$default(menu, null, null, null, null, null, new NodeNavigationType.Back(reportNode), null, u.dropLast(menu.getHistory(), 1), null, 351, null));
        }
        return true;
    }

    public final void handleBlockUser() {
        Observable addRelationship;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Menu)) {
            viewState = null;
        }
        ViewState.Menu menu = (ViewState.Menu) viewState;
        if (menu != null) {
            ReportNode node = menu.getNodeNavigationType().getNode();
            BlockUserElement blockUserElement = menu.getBlockUserElement();
            if (blockUserElement != null) {
                updateViewState(ViewState.Menu.copy$default(menu, null, null, null, null, null, null, null, null, BlockUserElement.copy$default(blockUserElement, true, null, 2, null), 255, null));
                addRelationship = this.restAPI.addRelationship(Companion.getLocation(node), blockUserElement.getUser().getId(), (r16 & 4) != 0 ? null : 2, (r16 & 8) != 0 ? null : null, (r16 & 16) != 0 ? null : null);
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(addRelationship, false, 1, null), this, null, 2, null), MobileReportsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, MobileReportsViewModel$handleBlockUser$1.INSTANCE);
            }
        }
    }

    public final void handleNext(ReportNodeChild reportNodeChild, NodeElementResult nodeElementResult) {
        ReportNode reportNode;
        m.checkNotNullParameter(reportNodeChild, "destination");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Menu)) {
            viewState = null;
        }
        ViewState.Menu menu = (ViewState.Menu) viewState;
        if (menu != null && (reportNode = menu.getMenu().c().get(Integer.valueOf(reportNodeChild.b()))) != null) {
            updateViewState(ViewState.Menu.copy$default(menu, null, null, null, null, null, new NodeNavigationType.Next(reportNode), SubmitState.None.INSTANCE, u.plus((Collection<? extends NodeResult>) menu.getHistory(), new NodeResult(menu.getNodeNavigationType().getNode(), reportNodeChild, nodeElementResult)), null, 287, null));
        }
    }

    public final void handleSubmit() {
        ReportSubmissionBody reportSubmissionBody;
        Guild guild;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Menu)) {
            viewState = null;
        }
        ViewState.Menu menu = (ViewState.Menu) viewState;
        if (menu != null && !(menu.getSubmitState() instanceof SubmitState.Loading)) {
            updateViewState(ViewState.Menu.copy$default(menu, null, null, null, null, null, null, SubmitState.Loading.INSTANCE, null, null, 447, null));
            MobileReportArgs mobileReportArgs = this.args;
            if (mobileReportArgs instanceof MobileReportArgs.Message) {
                ReportSubmissionBody.Companion companion = ReportSubmissionBody.Companion;
                long currentTimeMillis = (this.clock.currentTimeMillis() - SnowflakeUtils.DISCORD_EPOCH) << 22;
                long channelId = this.args.getChannelId();
                long messageId = ((MobileReportArgs.Message) this.args).getMessageId();
                MenuAPIResponse menu2 = menu.getMenu();
                List<NodeResult> history = menu.getHistory();
                Objects.requireNonNull(companion);
                m.checkNotNullParameter(menu2, "menu");
                m.checkNotNullParameter(history, "results");
                Pair<List<Integer>, Map<String, List<String>>> a = companion.a(history);
                List<Integer> component1 = a.component1();
                Map<String, List<String>> component2 = a.component2();
                Long valueOf = Long.valueOf(messageId);
                Long valueOf2 = Long.valueOf(channelId);
                String b2 = menu2.b();
                String a2 = menu2.a();
                reportSubmissionBody = new ReportSubmissionBody(currentTimeMillis, valueOf, valueOf2, null, null, null, a2 != null ? a2 : "en", menu2.f(), b2, menu2.g(), component1, component2, 56);
            } else if (mobileReportArgs instanceof MobileReportArgs.StageChannel) {
                ChannelPreview channelPreview = menu.getChannelPreview();
                if (channelPreview == null || (guild = channelPreview.getGuild()) == null) {
                    Logger.e$default(AppLog.g, "Tried to send report for stage channel, but without guild id?", null, null, 6, null);
                    return;
                }
                long id2 = guild.getId();
                ReportSubmissionBody.Companion companion2 = ReportSubmissionBody.Companion;
                long currentTimeMillis2 = (this.clock.currentTimeMillis() - SnowflakeUtils.DISCORD_EPOCH) << 22;
                long channelId2 = this.args.getChannelId();
                MenuAPIResponse menu3 = menu.getMenu();
                List<NodeResult> history2 = menu.getHistory();
                Objects.requireNonNull(companion2);
                m.checkNotNullParameter(menu3, "menu");
                m.checkNotNullParameter(history2, "results");
                Pair<List<Integer>, Map<String, List<String>>> a3 = companion2.a(history2);
                List<Integer> component12 = a3.component1();
                Map<String, List<String>> component22 = a3.component2();
                Long valueOf3 = Long.valueOf(id2);
                Long valueOf4 = Long.valueOf(channelId2);
                String b3 = menu3.b();
                String a4 = menu3.a();
                reportSubmissionBody = new ReportSubmissionBody(currentTimeMillis2, null, valueOf4, valueOf3, null, null, a4 != null ? a4 : "en", menu3.f(), b3, menu3.g(), component12, component22, 50);
            } else if (mobileReportArgs instanceof MobileReportArgs.DirectoryServer) {
                ReportSubmissionBody.Companion companion3 = ReportSubmissionBody.Companion;
                long currentTimeMillis3 = (this.clock.currentTimeMillis() - SnowflakeUtils.DISCORD_EPOCH) << 22;
                long channelId3 = this.args.getChannelId();
                long guildId = ((MobileReportArgs.DirectoryServer) this.args).getGuildId();
                long hubId = ((MobileReportArgs.DirectoryServer) this.args).getHubId();
                MenuAPIResponse menu4 = menu.getMenu();
                List<NodeResult> history3 = menu.getHistory();
                Objects.requireNonNull(companion3);
                m.checkNotNullParameter(menu4, "menu");
                m.checkNotNullParameter(history3, "results");
                Pair<List<Integer>, Map<String, List<String>>> a5 = companion3.a(history3);
                List<Integer> component13 = a5.component1();
                Map<String, List<String>> component23 = a5.component2();
                Long valueOf5 = Long.valueOf(guildId);
                Long valueOf6 = Long.valueOf(channelId3);
                Long valueOf7 = Long.valueOf(hubId);
                String b4 = menu4.b();
                String a6 = menu4.a();
                reportSubmissionBody = new ReportSubmissionBody(currentTimeMillis3, null, valueOf6, valueOf5, valueOf7, null, a6 != null ? a6 : "en", menu4.f(), b4, menu4.g(), component13, component23, 34);
            } else if (mobileReportArgs instanceof MobileReportArgs.GuildScheduledEvent) {
                ReportSubmissionBody.Companion companion4 = ReportSubmissionBody.Companion;
                long currentTimeMillis4 = (this.clock.currentTimeMillis() - SnowflakeUtils.DISCORD_EPOCH) << 22;
                long guildId2 = ((MobileReportArgs.GuildScheduledEvent) this.args).getGuildId();
                long eventId = ((MobileReportArgs.GuildScheduledEvent) this.args).getEventId();
                MenuAPIResponse menu5 = menu.getMenu();
                List<NodeResult> history4 = menu.getHistory();
                Objects.requireNonNull(companion4);
                m.checkNotNullParameter(menu5, "menu");
                m.checkNotNullParameter(history4, "results");
                Pair<List<Integer>, Map<String, List<String>>> a7 = companion4.a(history4);
                List<Integer> component14 = a7.component1();
                Map<String, List<String>> component24 = a7.component2();
                Long valueOf8 = Long.valueOf(guildId2);
                Long valueOf9 = Long.valueOf(eventId);
                String b5 = menu5.b();
                String a8 = menu5.a();
                reportSubmissionBody = new ReportSubmissionBody(currentTimeMillis4, null, null, valueOf8, null, valueOf9, a8 != null ? a8 : "en", menu5.f(), b5, menu5.g(), component14, component24, 22);
            } else {
                throw new NoWhenBranchMatchedException();
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.submitReport(this.args.getReportType().getPathValue(), reportSubmissionBody), false, 1, null), this, null, 2, null), MobileReportsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new MobileReportsViewModel$handleSubmit$2(this, menu), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MobileReportsViewModel$handleSubmit$1(this, menu));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MobileReportsViewModel(WeakReference<Context> weakReference, MobileReportArgs mobileReportArgs, RestAPI restAPI, Clock clock, Observable<StoreState> observable) {
        super(null);
        m.checkNotNullParameter(weakReference, "context");
        m.checkNotNullParameter(mobileReportArgs, "args");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.context = weakReference;
        this.args = mobileReportArgs;
        this.restAPI = restAPI;
        this.clock = clock;
        Observable j = Observable.j(ObservableExtensionsKt.computationLatest(observable), ObservableExtensionsKt.restSubscribeOn$default(restAPI.getReportMenu(mobileReportArgs.getReportType().getPathValue()), false, 1, null), AnonymousClass1.INSTANCE);
        m.checkNotNullExpressionValue(j, "Observable\n        .comb…> storeState to menuAPI }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(j, this, null, 2, null), MobileReportsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass3(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
