package com.discord.widgets.mobile_reports;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetMobileReportsBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetMobileReports.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetMobileReportsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetMobileReportsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetMobileReports$binding$2 extends k implements Function1<View, WidgetMobileReportsBinding> {
    public static final WidgetMobileReports$binding$2 INSTANCE = new WidgetMobileReports$binding$2();

    public WidgetMobileReports$binding$2() {
        super(1, WidgetMobileReportsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetMobileReportsBinding;", 0);
    }

    public final WidgetMobileReportsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.menu_reports_node_view_holder;
        FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.menu_reports_node_view_holder);
        if (frameLayout != null) {
            i = R.id.mobile_reports_progress_bar;
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.mobile_reports_progress_bar);
            if (progressBar != null) {
                return new WidgetMobileReportsBinding((CoordinatorLayout) view, frameLayout, progressBar);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
