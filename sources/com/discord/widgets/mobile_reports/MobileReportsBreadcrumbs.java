package com.discord.widgets.mobile_reports;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.discord.databinding.ViewMobileReportsBreadcrumbsBinding;
import com.discord.databinding.ViewMobileReportsBreadcrumbsItemBinding;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: MobileReportsBreadcrumbs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u001d\u0010\t\u001a\u00020\b2\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/mobile_reports/MobileReportsBreadcrumbs;", "Landroid/widget/LinearLayout;", "Lcom/discord/databinding/ViewMobileReportsBreadcrumbsItemBinding;", "getBoundBreadcrumbItem", "()Lcom/discord/databinding/ViewMobileReportsBreadcrumbsItemBinding;", "", "Lcom/discord/api/report/NodeResult;", "history", "", "setup", "(Ljava/util/List;)V", "Lcom/discord/databinding/ViewMobileReportsBreadcrumbsBinding;", "binding", "Lcom/discord/databinding/ViewMobileReportsBreadcrumbsBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MobileReportsBreadcrumbs extends LinearLayout {
    private final ViewMobileReportsBreadcrumbsBinding binding;

    public MobileReportsBreadcrumbs(Context context) {
        this(context, null, 0, 6, null);
    }

    public MobileReportsBreadcrumbs(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ MobileReportsBreadcrumbs(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    private final ViewMobileReportsBreadcrumbsItemBinding getBoundBreadcrumbItem() {
        LayoutInflater from = LayoutInflater.from(getContext());
        View view = this.binding.a;
        Objects.requireNonNull(view, "null cannot be cast to non-null type android.view.ViewGroup");
        ViewGroup viewGroup = (ViewGroup) view;
        View inflate = from.inflate(R.layout.view_mobile_reports_breadcrumbs_item, viewGroup, false);
        viewGroup.addView(inflate);
        int i = R.id.reports_breadcrumbs_item_dot;
        View findViewById = inflate.findViewById(R.id.reports_breadcrumbs_item_dot);
        if (findViewById != null) {
            i = R.id.reports_breadcrumbs_item_title;
            TextView textView = (TextView) inflate.findViewById(R.id.reports_breadcrumbs_item_title);
            if (textView != null) {
                ViewMobileReportsBreadcrumbsItemBinding viewMobileReportsBreadcrumbsItemBinding = new ViewMobileReportsBreadcrumbsItemBinding((ConstraintLayout) inflate, findViewById, textView);
                m.checkNotNullExpressionValue(viewMobileReportsBreadcrumbsItemBinding, "ViewMobileReportsBreadcr…ach to parent */ true\n  )");
                return viewMobileReportsBreadcrumbsItemBinding;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    /* JADX WARN: Removed duplicated region for block: B:54:0x00a4 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:57:0x0085 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void setup(java.util.List<com.discord.api.report.NodeResult> r18) {
        /*
            r17 = this;
            r0 = r17
            com.discord.databinding.ViewMobileReportsBreadcrumbsBinding r1 = r0.binding
            android.view.View r1 = r1.a
            boolean r2 = r1 instanceof android.widget.LinearLayout
            r3 = 0
            if (r2 != 0) goto Lc
            r1 = r3
        Lc:
            android.widget.LinearLayout r1 = (android.widget.LinearLayout) r1
            if (r1 == 0) goto Lc7
            int r2 = r1.getChildCount()
            r4 = 1
            if (r2 <= r4) goto L1f
            int r2 = r1.getChildCount()
            int r2 = r2 - r4
            r1.removeViewsInLayout(r4, r2)
        L1f:
            if (r18 == 0) goto Lc7
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r2 = r18.iterator()
        L2a:
            boolean r5 = r2.hasNext()
            r6 = 0
            if (r5 == 0) goto L7c
            java.lang.Object r5 = r2.next()
            com.discord.api.report.NodeResult r5 = (com.discord.api.report.NodeResult) r5
            com.discord.api.report.NodeElementResult r7 = r5.b()
            if (r7 == 0) goto L4c
            java.util.List r7 = r7.a()
            if (r7 == 0) goto L4c
            boolean r8 = r7.isEmpty()
            if (r8 == 0) goto L4a
            r7 = r3
        L4a:
            r8 = r7
            goto L4d
        L4c:
            r8 = r3
        L4d:
            r7 = 2
            java.lang.String[] r7 = new java.lang.String[r7]
            if (r8 == 0) goto L63
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            com.discord.widgets.mobile_reports.MobileReportsBreadcrumbs$setup$1$1 r14 = com.discord.widgets.mobile_reports.MobileReportsBreadcrumbs$setup$1$1.INSTANCE
            r15 = 30
            r16 = 0
            java.lang.String r9 = ", "
            java.lang.String r8 = d0.t.u.joinToString$default(r8, r9, r10, r11, r12, r13, r14, r15, r16)
            goto L64
        L63:
            r8 = r3
        L64:
            r7[r6] = r8
            com.discord.api.report.ReportNodeChild r5 = r5.a()
            if (r5 == 0) goto L71
            java.lang.String r5 = r5.a()
            goto L72
        L71:
            r5 = r3
        L72:
            r7[r4] = r5
            java.util.List r5 = d0.t.n.listOf(r7)
            d0.t.r.addAll(r1, r5)
            goto L2a
        L7c:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r1 = r1.iterator()
        L85:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto La8
            java.lang.Object r3 = r1.next()
            r5 = r3
            java.lang.String r5 = (java.lang.String) r5
            if (r5 == 0) goto La1
            int r5 = r5.length()
            if (r5 <= 0) goto L9c
            r5 = 1
            goto L9d
        L9c:
            r5 = 0
        L9d:
            if (r5 == 0) goto La1
            r5 = 1
            goto La2
        La1:
            r5 = 0
        La2:
            if (r5 == 0) goto L85
            r2.add(r3)
            goto L85
        La8:
            java.util.Iterator r1 = r2.iterator()
        Lac:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto Lc7
            java.lang.Object r2 = r1.next()
            java.lang.String r2 = (java.lang.String) r2
            com.discord.databinding.ViewMobileReportsBreadcrumbsItemBinding r3 = r17.getBoundBreadcrumbItem()
            android.widget.TextView r3 = r3.f2187b
            java.lang.String r4 = "getBoundBreadcrumbItem()…portsBreadcrumbsItemTitle"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
            r3.setText(r2)
            goto Lac
        Lc7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.mobile_reports.MobileReportsBreadcrumbs.setup(java.util.List):void");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MobileReportsBreadcrumbs(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.view_mobile_reports_breadcrumbs, this);
        ViewMobileReportsBreadcrumbsBinding viewMobileReportsBreadcrumbsBinding = new ViewMobileReportsBreadcrumbsBinding(this);
        m.checkNotNullExpressionValue(viewMobileReportsBreadcrumbsBinding, "ViewMobileReportsBreadcr…ater.from(context), this)");
        this.binding = viewMobileReportsBreadcrumbsBinding;
    }
}
