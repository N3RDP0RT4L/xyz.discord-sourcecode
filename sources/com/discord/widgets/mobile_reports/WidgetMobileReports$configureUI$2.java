package com.discord.widgets.mobile_reports;

import com.discord.databinding.WidgetMobileReportsBinding;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.mobile_reports.MobileReportsViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetMobileReports.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMobileReports$configureUI$2 extends o implements Function0<Unit> {
    public final /* synthetic */ ReportsMenuNode $nextNodeView;
    public final /* synthetic */ MobileReportsViewModel.ViewState.Menu $viewState;
    public final /* synthetic */ WidgetMobileReports this$0;

    /* compiled from: WidgetMobileReports.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.mobile_reports.WidgetMobileReports$configureUI$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public AnonymousClass1() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            WidgetMobileReports$configureUI$2 widgetMobileReports$configureUI$2 = WidgetMobileReports$configureUI$2.this;
            widgetMobileReports$configureUI$2.this$0.setActionBarDisplayHomeAsUpEnabled(!widgetMobileReports$configureUI$2.$viewState.shouldHideBackArrow());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetMobileReports$configureUI$2(WidgetMobileReports widgetMobileReports, ReportsMenuNode reportsMenuNode, MobileReportsViewModel.ViewState.Menu menu) {
        super(0);
        this.this$0 = widgetMobileReports;
        this.$nextNodeView = reportsMenuNode;
        this.$viewState = menu;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetMobileReportsBinding binding;
        WidgetMobileReportsBinding binding2;
        binding = this.this$0.getBinding();
        binding.f2473b.removeAllViewsInLayout();
        binding2 = this.this$0.getBinding();
        binding2.f2473b.addView(this.$nextNodeView);
        ViewExtensions.fadeIn$default(this.$nextNodeView, 0L, null, null, new AnonymousClass1(), 7, null);
    }
}
