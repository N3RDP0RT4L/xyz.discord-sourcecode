package com.discord.widgets.mobile_reports;

import com.discord.api.report.NodeElementResult;
import com.discord.api.report.ReportNodeChild;
import com.discord.widgets.mobile_reports.MobileReportsViewModel;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetMobileReports.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/report/ReportNodeChild;", "destination", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "nodeState", "", "invoke", "(Lcom/discord/api/report/ReportNodeChild;Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMobileReports$createNodeView$1 extends o implements Function2<ReportNodeChild, MobileReportsViewModel.NodeState, Unit> {
    public final /* synthetic */ WidgetMobileReports this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetMobileReports$createNodeView$1(WidgetMobileReports widgetMobileReports) {
        super(2);
        this.this$0 = widgetMobileReports;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(ReportNodeChild reportNodeChild, MobileReportsViewModel.NodeState nodeState) {
        invoke2(reportNodeChild, nodeState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ReportNodeChild reportNodeChild, MobileReportsViewModel.NodeState nodeState) {
        MobileReportsViewModel viewModel;
        m.checkNotNullParameter(reportNodeChild, "destination");
        m.checkNotNullParameter(nodeState, "nodeState");
        viewModel = this.this$0.getViewModel();
        MobileReportsViewModel.CheckboxElement checkboxElement = nodeState.getCheckboxElement();
        viewModel.handleNext(reportNodeChild, checkboxElement != null ? new NodeElementResult(checkboxElement.getName(), u.toList(checkboxElement.getSelections())) : null);
    }
}
