package com.discord.widgets.mobile_reports;

import com.discord.api.report.ReportNodeElementData;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: MobileReportsBreadcrumbs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/report/ReportNodeElementData;", "data", "", "invoke", "(Lcom/discord/api/report/ReportNodeElementData;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MobileReportsBreadcrumbs$setup$1$1 extends o implements Function1<ReportNodeElementData, CharSequence> {
    public static final MobileReportsBreadcrumbs$setup$1$1 INSTANCE = new MobileReportsBreadcrumbs$setup$1$1();

    public MobileReportsBreadcrumbs$setup$1$1() {
        super(1);
    }

    public final CharSequence invoke(ReportNodeElementData reportNodeElementData) {
        m.checkNotNullParameter(reportNodeElementData, "data");
        return reportNodeElementData.b();
    }
}
