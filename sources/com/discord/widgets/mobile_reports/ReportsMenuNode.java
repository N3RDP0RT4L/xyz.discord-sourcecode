package com.discord.widgets.mobile_reports;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.view.ViewGroupKt;
import b.a.i.k2;
import b.a.i.l2;
import b.a.i.m2;
import b.a.i.n2;
import b.a.i.u4;
import b.a.k.b;
import com.discord.api.report.NodeResult;
import com.discord.api.report.ReportNode;
import com.discord.api.report.ReportNodeBottomButton;
import com.discord.api.report.ReportNodeChild;
import com.discord.api.report.ReportNodeElementData;
import com.discord.databinding.ViewReportsMenuNodeBinding;
import com.discord.models.user.User;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.CheckedSetting;
import com.discord.widgets.mobile_reports.MobileReportsViewModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: ReportsMenuNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010B\u001a\u00020A¢\u0006\u0004\bC\u0010DB\u001d\b\u0016\u0012\u0006\u0010B\u001a\u00020A\u0012\n\b\u0002\u0010F\u001a\u0004\u0018\u00010E¢\u0006\u0004\bC\u0010GB'\b\u0016\u0012\u0006\u0010B\u001a\u00020A\u0012\n\b\u0002\u0010F\u001a\u0004\u0018\u00010E\u0012\b\b\u0002\u0010I\u001a\u00020H¢\u0006\u0004\bC\u0010JJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0015\u0010\u0010J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0016\u0010\u0010J\u0017\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0017\u0010\u0010J\u0017\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0018\u0010\u0010J\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0019\u0010\u0010J\u0017\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u001a\u0010\u0010J\u0017\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u001b\u0010\u0010J\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u001c\u0010\u0010J\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u001d\u0010\u0010J\u000f\u0010\u001e\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b\u001e\u0010\u001fJ\u0015\u0010 \u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b \u0010\u0010R*\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010!8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%\"\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R*\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010!8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b+\u0010#\u001a\u0004\b,\u0010%\"\u0004\b-\u0010'R*\u0010.\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010!8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b.\u0010#\u001a\u0004\b/\u0010%\"\u0004\b0\u0010'R\u0018\u00101\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R0\u00104\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0004\u0018\u0001038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b4\u00105\u001a\u0004\b6\u00107\"\u0004\b8\u00109R6\u0010;\u001a\u0016\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0004\u0018\u00010:8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>\"\u0004\b?\u0010@¨\u0006K"}, d2 = {"Lcom/discord/widgets/mobile_reports/ReportsMenuNode;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lcom/discord/api/report/ReportNodeChild;", "destination", "", "childClickListener", "(Lcom/discord/api/report/ReportNodeChild;)V", "Lcom/discord/api/report/ReportNodeBottomButton;", "button", "bottomButtonClickListener", "(Lcom/discord/api/report/ReportNodeBottomButton;)V", "blockUserClickListener", "()V", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "viewState", "setupBlockUser", "(Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;)V", "", "showSuccess", "setupSuccess", "(Z)V", "setupTextElements", "setupChildren", "setupBottomButton", "setupBreadCrumbs", "setupCheckbox", "setupMessagePreview", "setupChannelPreview", "setupEventPreview", "setupDirectoryServerPreview", "getViewState", "()Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "setup", "Lkotlin/Function0;", "handleCancel", "Lkotlin/jvm/functions/Function0;", "getHandleCancel", "()Lkotlin/jvm/functions/Function0;", "setHandleCancel", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/databinding/ViewReportsMenuNodeBinding;", "binding", "Lcom/discord/databinding/ViewReportsMenuNodeBinding;", "handleBlock", "getHandleBlock", "setHandleBlock", "handleSubmit", "getHandleSubmit", "setHandleSubmit", "prevViewState", "Lcom/discord/widgets/mobile_reports/MobileReportsViewModel$NodeState;", "Lkotlin/Function1;", "handlePressBottomButton", "Lkotlin/jvm/functions/Function1;", "getHandlePressBottomButton", "()Lkotlin/jvm/functions/Function1;", "setHandlePressBottomButton", "(Lkotlin/jvm/functions/Function1;)V", "Lkotlin/Function2;", "handleSelectChild", "Lkotlin/jvm/functions/Function2;", "getHandleSelectChild", "()Lkotlin/jvm/functions/Function2;", "setHandleSelectChild", "(Lkotlin/jvm/functions/Function2;)V", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ReportsMenuNode extends ConstraintLayout {
    private final ViewReportsMenuNodeBinding binding;
    private Function0<Unit> handleBlock;
    private Function0<Unit> handleCancel;
    private Function1<? super ReportNodeBottomButton, Unit> handlePressBottomButton;
    private Function2<? super ReportNodeChild, ? super MobileReportsViewModel.NodeState, Unit> handleSelectChild;
    private Function0<Unit> handleSubmit;
    private MobileReportsViewModel.NodeState prevViewState;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReportsMenuNode(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        ViewReportsMenuNodeBinding a = ViewReportsMenuNodeBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "ViewReportsMenuNodeBindi…rom(context), this, true)");
        this.binding = a;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void blockUserClickListener() {
        Function0<Unit> function0 = this.handleBlock;
        if (function0 != null) {
            function0.invoke();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void bottomButtonClickListener(ReportNodeBottomButton reportNodeBottomButton) {
        Function0<Unit> function0;
        Function2<? super ReportNodeChild, ? super MobileReportsViewModel.NodeState, Unit> function2;
        if ((reportNodeBottomButton instanceof ReportNodeBottomButton.Done) || (reportNodeBottomButton instanceof ReportNodeBottomButton.Cancel)) {
            Function0<Unit> function02 = this.handleCancel;
            if (function02 != null) {
                function02.invoke();
            }
        } else if (reportNodeBottomButton instanceof ReportNodeBottomButton.Next) {
            ReportNodeChild reportNodeChild = new ReportNodeChild("", ((ReportNodeBottomButton.Next) reportNodeBottomButton).b());
            MobileReportsViewModel.NodeState nodeState = this.prevViewState;
            if (nodeState != null && (function2 = this.handleSelectChild) != null) {
                function2.invoke(reportNodeChild, nodeState);
            }
        } else if ((reportNodeBottomButton instanceof ReportNodeBottomButton.Submit) && (function0 = this.handleSubmit) != null) {
            function0.invoke();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void childClickListener(ReportNodeChild reportNodeChild) {
        Function2<? super ReportNodeChild, ? super MobileReportsViewModel.NodeState, Unit> function2;
        MobileReportsViewModel.NodeState nodeState = this.prevViewState;
        if (nodeState != null && (function2 = this.handleSelectChild) != null) {
            function2.invoke(reportNodeChild, nodeState);
        }
    }

    private final void setupBlockUser(MobileReportsViewModel.NodeState nodeState) {
        Context context;
        int i;
        MobileReportsViewModel.BlockUserElement blockUserElement = nodeState.getBlockUserElement();
        k2 k2Var = this.binding.f;
        m.checkNotNullExpressionValue(k2Var, "binding.mobileReportsNodeBlockUser");
        LinearLayout linearLayout = k2Var.a;
        m.checkNotNullExpressionValue(linearLayout, "binding.mobileReportsNodeBlockUser.root");
        int i2 = 0;
        if (!(blockUserElement != null)) {
            i2 = 8;
        }
        linearLayout.setVisibility(i2);
        if (blockUserElement != null) {
            User user = blockUserElement.getUser();
            boolean isBlocked = blockUserElement.isBlocked();
            TextView textView = this.binding.f.d;
            m.checkNotNullExpressionValue(textView, "binding.mobileReportsNod…obileReportsBlockUserName");
            textView.setText(UserUtils.INSTANCE.getUserNameWithDiscriminator(user, Integer.valueOf(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorHeaderSecondary)), Float.valueOf(0.8f)));
            SimpleDraweeView simpleDraweeView = this.binding.f.f143b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.mobileReportsNod…ileReportsBlockUserAvatar");
            IconUtils.setIcon$default(simpleDraweeView, user, R.dimen.avatar_size_standard, null, null, null, 56, null);
            this.binding.f.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.mobile_reports.ReportsMenuNode$setupBlockUser$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ReportsMenuNode.this.blockUserClickListener();
                }
            });
            MaterialButton materialButton = this.binding.f.c;
            m.checkNotNullExpressionValue(materialButton, "binding.mobileReportsNod…ileReportsBlockUserButton");
            ViewExtensions.setEnabledAndAlpha(materialButton, !isBlocked, 0.5f);
            MaterialButton materialButton2 = this.binding.f.c;
            m.checkNotNullExpressionValue(materialButton2, "binding.mobileReportsNod…ileReportsBlockUserButton");
            if (isBlocked) {
                context = getContext();
                i = R.string.blocked;
            } else {
                context = getContext();
                i = R.string.block;
            }
            materialButton2.setText(context.getString(i));
        }
    }

    private final void setupBottomButton(MobileReportsViewModel.NodeState nodeState) {
        ReportNodeBottomButton bottomButton = nodeState.getBottomButton();
        MobileReportsBottomButton mobileReportsBottomButton = this.binding.g;
        m.checkNotNullExpressionValue(mobileReportsBottomButton, "binding.mobileReportsNodeBottomButton");
        int i = 0;
        if (!(bottomButton != null)) {
            i = 8;
        }
        mobileReportsBottomButton.setVisibility(i);
        this.binding.g.setup(bottomButton, nodeState.getSubmitState(), new ReportsMenuNode$setupBottomButton$1(this));
    }

    private final void setupBreadCrumbs(MobileReportsViewModel.NodeState nodeState) {
        MobileReportsBreadcrumbs mobileReportsBreadcrumbs = this.binding.h;
        m.checkNotNullExpressionValue(mobileReportsBreadcrumbs, "binding.mobileReportsNodeBreadcrumbs");
        int i = 0;
        if (!(nodeState.getBreadcrumbsElement() != null)) {
            i = 8;
        }
        mobileReportsBreadcrumbs.setVisibility(i);
        List<NodeResult> breadcrumbsElement = nodeState.getBreadcrumbsElement();
        MobileReportsViewModel.NodeState nodeState2 = this.prevViewState;
        if (!m.areEqual(breadcrumbsElement, nodeState2 != null ? nodeState2.getBreadcrumbsElement() : null)) {
            this.binding.h.setup(nodeState.getBreadcrumbsElement());
        }
    }

    private final void setupChannelPreview(MobileReportsViewModel.NodeState nodeState) {
        MobileReportsViewModel.ChannelPreview channelPreviewElement = nodeState.getChannelPreviewElement();
        ViewReportsMenuNodeBinding viewReportsMenuNodeBinding = this.binding;
        l2 l2Var = viewReportsMenuNodeBinding.f2190b;
        if (channelPreviewElement != null) {
            TextView textView = l2Var.c;
            m.checkNotNullExpressionValue(textView, "kicker");
            textView.setText(channelPreviewElement.getStageInstance().f());
            LinkifiedTextView linkifiedTextView = l2Var.d;
            m.checkNotNullExpressionValue(linkifiedTextView, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            linkifiedTextView.setText(channelPreviewElement.getGuild().getName());
            l2Var.f150b.a(IconUtils.getForGuild$default(channelPreviewElement.getGuild(), null, false, null, 14, null), channelPreviewElement.getGuild().getShortName());
            MaterialCardView materialCardView = this.binding.i;
            m.checkNotNullExpressionValue(materialCardView, "binding.mobileReportsNodeChannelPreview");
            materialCardView.setVisibility(0);
            return;
        }
        MaterialCardView materialCardView2 = viewReportsMenuNodeBinding.i;
        m.checkNotNullExpressionValue(materialCardView2, "binding.mobileReportsNodeChannelPreview");
        materialCardView2.setVisibility(8);
    }

    private final void setupCheckbox(MobileReportsViewModel.NodeState nodeState) {
        List<ReportNodeElementData> data;
        final MobileReportsViewModel.CheckboxElement checkboxElement = nodeState.getCheckboxElement();
        LinearLayout linearLayout = this.binding.e;
        m.checkNotNullExpressionValue(linearLayout, "binding.mobileReportsMultiselect");
        linearLayout.setVisibility(checkboxElement != null ? 0 : 8);
        MobileReportsViewModel.CheckboxElement checkboxElement2 = nodeState.getCheckboxElement();
        MobileReportsViewModel.NodeState nodeState2 = this.prevViewState;
        if (!m.areEqual(checkboxElement2, nodeState2 != null ? nodeState2.getCheckboxElement() : null)) {
            this.binding.e.removeAllViewsInLayout();
            if (!(checkboxElement == null || (data = checkboxElement.getData()) == null)) {
                for (final ReportNodeElementData reportNodeElementData : data) {
                    LayoutInflater from = LayoutInflater.from(getContext());
                    LinearLayout linearLayout2 = this.binding.e;
                    View inflate = from.inflate(R.layout.view_mobile_reports_multicheck_item, (ViewGroup) linearLayout2, false);
                    linearLayout2.addView(inflate);
                    Objects.requireNonNull(inflate, "rootView");
                    CheckedSetting checkedSetting = (CheckedSetting) inflate;
                    m.checkNotNullExpressionValue(new n2(checkedSetting), "checkbox");
                    checkedSetting.setText(reportNodeElementData.b());
                    m.checkNotNullExpressionValue(checkedSetting, "checkbox.root");
                    checkedSetting.setChecked(checkboxElement.getSelections().contains(reportNodeElementData));
                    checkedSetting.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.mobile_reports.ReportsMenuNode$setupCheckbox$$inlined$forEach$lambda$1
                        public final void call(Boolean bool) {
                            m.checkNotNullExpressionValue(bool, "isChecked");
                            if (bool.booleanValue()) {
                                checkboxElement.getSelections().add(ReportNodeElementData.this);
                            } else {
                                checkboxElement.getSelections().remove(ReportNodeElementData.this);
                            }
                        }
                    });
                }
            }
        }
    }

    private final void setupChildren(MobileReportsViewModel.NodeState nodeState) {
        ReportNode node;
        MobileReportsViewModel.NodeState nodeState2 = this.prevViewState;
        if (!m.areEqual((nodeState2 == null || (node = nodeState2.getNode()) == null) ? null : node.b(), nodeState.getNode().b())) {
            LinearLayout linearLayout = this.binding.j;
            m.checkNotNullExpressionValue(linearLayout, "binding.mobileReportsNodeChildList");
            for (View view : ViewGroupKt.getChildren(linearLayout)) {
                view.setOnClickListener(null);
            }
            this.binding.j.removeAllViewsInLayout();
            for (final ReportNodeChild reportNodeChild : nodeState.getNode().b()) {
                LayoutInflater from = LayoutInflater.from(getContext());
                LinearLayout linearLayout2 = this.binding.j;
                View inflate = from.inflate(R.layout.view_mobile_reports_child, (ViewGroup) linearLayout2, false);
                linearLayout2.addView(inflate);
                int i = R.id.mobile_reports_child;
                CardView cardView = (CardView) inflate.findViewById(R.id.mobile_reports_child);
                if (cardView != null) {
                    i = R.id.mobile_reports_child_menu_title;
                    TextView textView = (TextView) inflate.findViewById(R.id.mobile_reports_child_menu_title);
                    if (textView != null) {
                        FrameLayout frameLayout = (FrameLayout) inflate;
                        m.checkNotNullExpressionValue(new m2(frameLayout, cardView, textView), "childView");
                        frameLayout.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.mobile_reports.ReportsMenuNode$setupChildren$$inlined$forEach$lambda$1
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view2) {
                                this.childClickListener(ReportNodeChild.this);
                            }
                        });
                        m.checkNotNullExpressionValue(textView, "childView.mobileReportsChildMenuTitle");
                        textView.setText(reportNodeChild.a());
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
            }
        }
    }

    private final void setupDirectoryServerPreview(MobileReportsViewModel.NodeState nodeState) {
        MaterialCardView materialCardView = this.binding.k;
        m.checkNotNullExpressionValue(materialCardView, "binding.mobileReportsNodeDirectoryChannelPreview");
        boolean z2 = true;
        int i = 0;
        materialCardView.setVisibility(nodeState.getDirectoryServerPreviewElement() != null ? 0 : 8);
        TextView textView = this.binding.l;
        m.checkNotNullExpressionValue(textView, "binding.mobileReportsNod…ectoryChannelPreviewTitle");
        if (nodeState.getDirectoryServerPreviewElement() == null) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        textView.setVisibility(i);
        MobileReportsViewModel.DirectoryServerPreview directoryServerPreviewElement = nodeState.getDirectoryServerPreviewElement();
        if (directoryServerPreviewElement != null) {
            l2 l2Var = this.binding.c;
            LinkifiedTextView linkifiedTextView = l2Var.d;
            m.checkNotNullExpressionValue(linkifiedTextView, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            linkifiedTextView.setText(directoryServerPreviewElement.getDirectoryEntry().e().i());
            TextView textView2 = l2Var.c;
            m.checkNotNullExpressionValue(textView2, "kicker");
            textView2.setText(directoryServerPreviewElement.getHub().getName());
            l2Var.f150b.a(IconUtils.getForGuild$default(Long.valueOf(directoryServerPreviewElement.getDirectoryEntry().e().h()), directoryServerPreviewElement.getDirectoryEntry().e().g(), null, false, null, 28, null), GuildUtilsKt.computeShortName(directoryServerPreviewElement.getDirectoryEntry().e().i()));
        }
    }

    private final void setupEventPreview(MobileReportsViewModel.NodeState nodeState) {
        MobileReportsViewModel.GuildScheduledEventPreview eventPreviewElement = nodeState.getEventPreviewElement();
        ViewReportsMenuNodeBinding viewReportsMenuNodeBinding = this.binding;
        l2 l2Var = viewReportsMenuNodeBinding.f2190b;
        if (eventPreviewElement != null) {
            TextView textView = l2Var.c;
            m.checkNotNullExpressionValue(textView, "kicker");
            textView.setText(eventPreviewElement.getGuild().getName());
            LinkifiedTextView linkifiedTextView = l2Var.d;
            m.checkNotNullExpressionValue(linkifiedTextView, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            linkifiedTextView.setText(eventPreviewElement.getEvent().j());
            l2Var.f150b.a(IconUtils.getForGuild$default(eventPreviewElement.getGuild(), null, false, null, 14, null), eventPreviewElement.getGuild().getShortName());
            MaterialCardView materialCardView = this.binding.i;
            m.checkNotNullExpressionValue(materialCardView, "binding.mobileReportsNodeChannelPreview");
            materialCardView.setVisibility(0);
            return;
        }
        MaterialCardView materialCardView2 = viewReportsMenuNodeBinding.i;
        m.checkNotNullExpressionValue(materialCardView2, "binding.mobileReportsNodeChannelPreview");
        materialCardView2.setVisibility(8);
    }

    private final void setupMessagePreview(MobileReportsViewModel.NodeState nodeState) {
        MobileReportsViewModel.MessagePreview messagePreviewElement = nodeState.getMessagePreviewElement();
        ViewReportsMenuNodeBinding viewReportsMenuNodeBinding = this.binding;
        int i = 8;
        if (messagePreviewElement != null) {
            TextView textView = viewReportsMenuNodeBinding.d.e;
            m.checkNotNullExpressionValue(textView, "mobileReportsMessagePrev…atListAdapterItemTextName");
            textView.setText(messagePreviewElement.getAuthorName());
            viewReportsMenuNodeBinding.d.e.setTextColor(messagePreviewElement.getAuthorNameColor());
            SimpleDraweeView simpleDraweeView = viewReportsMenuNodeBinding.d.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "mobileReportsMessagePrev…ListAdapterItemTextAvatar");
            IconUtils.setIcon$default(simpleDraweeView, messagePreviewElement.getAuthor(), R.dimen.avatar_size_small, null, null, null, 56, null);
            viewReportsMenuNodeBinding.d.c.setDraweeSpanStringBuilder(messagePreviewElement.getText());
            ImageView imageView = viewReportsMenuNodeBinding.d.f207b;
            m.checkNotNullExpressionValue(imageView, "mobileReportsMessagePrev…terItemChatAttachmentIcon");
            if (messagePreviewElement.getHasEmbeds()) {
                i = 0;
            }
            imageView.setVisibility(i);
            u4 u4Var = viewReportsMenuNodeBinding.d;
            m.checkNotNullExpressionValue(u4Var, "mobileReportsMessagePreview");
            ConstraintLayout constraintLayout = u4Var.a;
            m.checkNotNullExpressionValue(constraintLayout, "mobileReportsMessagePreview.root");
            constraintLayout.setVisibility(0);
            return;
        }
        u4 u4Var2 = viewReportsMenuNodeBinding.d;
        m.checkNotNullExpressionValue(u4Var2, "mobileReportsMessagePreview");
        ConstraintLayout constraintLayout2 = u4Var2.a;
        m.checkNotNullExpressionValue(constraintLayout2, "mobileReportsMessagePreview.root");
        constraintLayout2.setVisibility(8);
    }

    private final void setupSuccess(boolean z2) {
        ImageView imageView = this.binding.q;
        m.checkNotNullExpressionValue(imageView, "binding.mobileReportsNodeSuccessShield");
        imageView.setVisibility(z2 ? 0 : 8);
    }

    private final void setupTextElements(MobileReportsViewModel.NodeState nodeState) {
        CharSequence g;
        TextView textView = this.binding.m;
        m.checkNotNullExpressionValue(textView, "binding.mobileReportsNodeHeader");
        g = b.g(nodeState.getNode().d(), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
        textView.setText(g);
        String g2 = nodeState.getNode().g();
        LinkifiedTextView linkifiedTextView = this.binding.p;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.mobileReportsNodeSubheader");
        boolean z2 = true;
        int i = 8;
        linkifiedTextView.setVisibility(g2 != null ? 0 : 8);
        LinkifiedTextView linkifiedTextView2 = this.binding.p;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.mobileReportsNodeSubheader");
        b.o(linkifiedTextView2, g2, new Object[0], null, 4);
        String f = nodeState.getNode().f();
        MaterialCardView materialCardView = this.binding.n;
        m.checkNotNullExpressionValue(materialCardView, "binding.mobileReportsNodeInfoBox");
        if (f == null) {
            z2 = false;
        }
        if (z2) {
            i = 0;
        }
        materialCardView.setVisibility(i);
        TextView textView2 = this.binding.o;
        m.checkNotNullExpressionValue(textView2, "binding.mobileReportsNodeInfoText");
        b.o(textView2, f, new Object[0], null, 4);
    }

    public final Function0<Unit> getHandleBlock() {
        return this.handleBlock;
    }

    public final Function0<Unit> getHandleCancel() {
        return this.handleCancel;
    }

    public final Function1<ReportNodeBottomButton, Unit> getHandlePressBottomButton() {
        return this.handlePressBottomButton;
    }

    public final Function2<ReportNodeChild, MobileReportsViewModel.NodeState, Unit> getHandleSelectChild() {
        return this.handleSelectChild;
    }

    public final Function0<Unit> getHandleSubmit() {
        return this.handleSubmit;
    }

    public final MobileReportsViewModel.NodeState getViewState() {
        return this.prevViewState;
    }

    public final void setHandleBlock(Function0<Unit> function0) {
        this.handleBlock = function0;
    }

    public final void setHandleCancel(Function0<Unit> function0) {
        this.handleCancel = function0;
    }

    public final void setHandlePressBottomButton(Function1<? super ReportNodeBottomButton, Unit> function1) {
        this.handlePressBottomButton = function1;
    }

    public final void setHandleSelectChild(Function2<? super ReportNodeChild, ? super MobileReportsViewModel.NodeState, Unit> function2) {
        this.handleSelectChild = function2;
    }

    public final void setHandleSubmit(Function0<Unit> function0) {
        this.handleSubmit = function0;
    }

    public final void setup(MobileReportsViewModel.NodeState nodeState) {
        m.checkNotNullParameter(nodeState, "viewState");
        setupSuccess(nodeState.getSuccessElement());
        setupMessagePreview(nodeState);
        setupChannelPreview(nodeState);
        setupDirectoryServerPreview(nodeState);
        setupEventPreview(nodeState);
        setupBreadCrumbs(nodeState);
        setupTextElements(nodeState);
        setupChildren(nodeState);
        setupCheckbox(nodeState);
        setupBottomButton(nodeState);
        setupBlockUser(nodeState);
        this.prevViewState = nodeState;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReportsMenuNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        ViewReportsMenuNodeBinding a = ViewReportsMenuNodeBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "ViewReportsMenuNodeBindi…rom(context), this, true)");
        this.binding = a;
    }

    public /* synthetic */ ReportsMenuNode(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    public /* synthetic */ ReportsMenuNode(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReportsMenuNode(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        ViewReportsMenuNodeBinding a = ViewReportsMenuNodeBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "ViewReportsMenuNodeBindi…rom(context), this, true)");
        this.binding = a;
    }
}
