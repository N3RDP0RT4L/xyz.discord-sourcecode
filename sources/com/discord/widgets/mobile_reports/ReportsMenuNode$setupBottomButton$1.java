package com.discord.widgets.mobile_reports;

import com.discord.api.report.ReportNodeBottomButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ReportsMenuNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/report/ReportNodeBottomButton;", "p1", "", "invoke", "(Lcom/discord/api/report/ReportNodeBottomButton;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class ReportsMenuNode$setupBottomButton$1 extends k implements Function1<ReportNodeBottomButton, Unit> {
    public ReportsMenuNode$setupBottomButton$1(ReportsMenuNode reportsMenuNode) {
        super(1, reportsMenuNode, ReportsMenuNode.class, "bottomButtonClickListener", "bottomButtonClickListener(Lcom/discord/api/report/ReportNodeBottomButton;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ReportNodeBottomButton reportNodeBottomButton) {
        invoke2(reportNodeBottomButton);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ReportNodeBottomButton reportNodeBottomButton) {
        m.checkNotNullParameter(reportNodeBottomButton, "p1");
        ((ReportsMenuNode) this.receiver).bottomButtonClickListener(reportNodeBottomButton);
    }
}
