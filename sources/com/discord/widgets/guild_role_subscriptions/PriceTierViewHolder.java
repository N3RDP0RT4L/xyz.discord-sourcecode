package com.discord.widgets.guild_role_subscriptions;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.DialogSimpleSelectorItemBinding;
import com.discord.utilities.billing.PremiumUtilsKt;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetPriceTierPickerBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ)\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/PriceTierViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "", "priceTier", "Lkotlin/Function1;", "", "onItemClickListener", "configureUI", "(ILkotlin/jvm/functions/Function1;)V", "Lcom/discord/databinding/DialogSimpleSelectorItemBinding;", "binding", "Lcom/discord/databinding/DialogSimpleSelectorItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/DialogSimpleSelectorItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PriceTierViewHolder extends RecyclerView.ViewHolder {
    private final DialogSimpleSelectorItemBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PriceTierViewHolder(DialogSimpleSelectorItemBinding dialogSimpleSelectorItemBinding) {
        super(dialogSimpleSelectorItemBinding.a);
        m.checkNotNullParameter(dialogSimpleSelectorItemBinding, "binding");
        this.binding = dialogSimpleSelectorItemBinding;
    }

    public final void configureUI(final int i, final Function1<? super Integer, Unit> function1) {
        m.checkNotNullParameter(function1, "onItemClickListener");
        TextView textView = this.binding.a;
        m.checkNotNullExpressionValue(textView, "binding.root");
        TextView textView2 = this.binding.a;
        m.checkNotNullExpressionValue(textView2, "binding.root");
        Context context = textView2.getContext();
        m.checkNotNullExpressionValue(context, "binding.root.context");
        textView.setText(PremiumUtilsKt.getFormattedPriceUsd(i, context));
        this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guild_role_subscriptions.PriceTierViewHolder$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function1.this.invoke(Integer.valueOf(i));
            }
        });
    }
}
