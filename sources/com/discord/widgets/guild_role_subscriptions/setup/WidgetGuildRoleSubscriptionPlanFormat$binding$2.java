package com.discord.widgets.guild_role_subscriptions.setup;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetGuildRoleSubscriptionPlanFormatBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildRoleSubscriptionPlanFormat.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildRoleSubscriptionPlanFormatBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildRoleSubscriptionPlanFormatBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildRoleSubscriptionPlanFormat$binding$2 extends k implements Function1<View, WidgetGuildRoleSubscriptionPlanFormatBinding> {
    public static final WidgetGuildRoleSubscriptionPlanFormat$binding$2 INSTANCE = new WidgetGuildRoleSubscriptionPlanFormat$binding$2();

    public WidgetGuildRoleSubscriptionPlanFormat$binding$2() {
        super(1, WidgetGuildRoleSubscriptionPlanFormatBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildRoleSubscriptionPlanFormatBinding;", 0);
    }

    public final WidgetGuildRoleSubscriptionPlanFormatBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_role_subscription_format_divider;
        View findViewById = view.findViewById(R.id.guild_role_subscription_format_divider);
        if (findViewById != null) {
            i = R.id.guild_role_subscription_format_subtitle;
            TextView textView = (TextView) view.findViewById(R.id.guild_role_subscription_format_subtitle);
            if (textView != null) {
                i = R.id.guild_role_subscription_format_title;
                TextView textView2 = (TextView) view.findViewById(R.id.guild_role_subscription_format_title);
                if (textView2 != null) {
                    i = R.id.guild_role_subscription_plan_entire_server_format;
                    GuildRoleSubscriptionPlanFormatRadioButton guildRoleSubscriptionPlanFormatRadioButton = (GuildRoleSubscriptionPlanFormatRadioButton) view.findViewById(R.id.guild_role_subscription_plan_entire_server_format);
                    if (guildRoleSubscriptionPlanFormatRadioButton != null) {
                        i = R.id.guild_role_subscription_plan_some_channels_format;
                        GuildRoleSubscriptionPlanFormatRadioButton guildRoleSubscriptionPlanFormatRadioButton2 = (GuildRoleSubscriptionPlanFormatRadioButton) view.findViewById(R.id.guild_role_subscription_plan_some_channels_format);
                        if (guildRoleSubscriptionPlanFormatRadioButton2 != null) {
                            return new WidgetGuildRoleSubscriptionPlanFormatBinding((LinearLayout) view, findViewById, textView, textView2, guildRoleSubscriptionPlanFormatRadioButton, guildRoleSubscriptionPlanFormatRadioButton2);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
