package com.discord.widgets.guild_role_subscriptions.setup;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.api.guildrolesubscription.ImageAsset;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildRoleSubscriptionPlanDetailsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0006\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\t\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b\t\u0010\u0007J\u0015\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/setup/GuildRoleSubscriptionPlanDetailsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guild_role_subscriptions/setup/GuildRoleSubscriptionPlanDetailsViewModel$ViewState;", "", "coverImage", "", "updateCoverImage", "(Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "updateDescription", "", "isFullServerGate", "updateFullServerGate", "(Z)V", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "subscriptionGroupListing", "fullServerGatingOverwrite", "setGroupPlanDetails", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;Ljava/lang/Boolean;)V", HookHelper.constructorName, "()V", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionPlanDetailsViewModel extends AppViewModel<ViewState> {

    /* compiled from: GuildRoleSubscriptionPlanDetailsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001BG\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u000f\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u000b¢\u0006\u0004\b#\u0010$J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJP\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u000f\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0004J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001a\u001a\u00020\u000b2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001c\u001a\u0004\b\u0012\u0010\rR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b \u0010\u0004R!\u0010\u000f\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b!\u0010\bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001f\u001a\u0004\b\"\u0010\u0004¨\u0006%"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/setup/GuildRoleSubscriptionPlanDetailsViewModel$ViewState;", "", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/ApplicationId;", "component2", "()Ljava/lang/Long;", "component3", "component4", "", "component5", "()Z", "coverImage", "applicationId", "coverImageAssetId", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "isFullServerGate", "copy", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Z)Lcom/discord/widgets/guild_role_subscriptions/setup/GuildRoleSubscriptionPlanDetailsViewModel$ViewState;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/Long;", "getCoverImageAssetId", "Ljava/lang/String;", "getCoverImage", "getApplicationId", "getDescription", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Long applicationId;
        private final String coverImage;
        private final Long coverImageAssetId;
        private final String description;
        private final boolean isFullServerGate;

        public ViewState() {
            this(null, null, null, null, false, 31, null);
        }

        public ViewState(String str, Long l, Long l2, String str2, boolean z2) {
            this.coverImage = str;
            this.applicationId = l;
            this.coverImageAssetId = l2;
            this.description = str2;
            this.isFullServerGate = z2;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, String str, Long l, Long l2, String str2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = viewState.coverImage;
            }
            if ((i & 2) != 0) {
                l = viewState.applicationId;
            }
            Long l3 = l;
            if ((i & 4) != 0) {
                l2 = viewState.coverImageAssetId;
            }
            Long l4 = l2;
            if ((i & 8) != 0) {
                str2 = viewState.description;
            }
            String str3 = str2;
            if ((i & 16) != 0) {
                z2 = viewState.isFullServerGate;
            }
            return viewState.copy(str, l3, l4, str3, z2);
        }

        public final String component1() {
            return this.coverImage;
        }

        public final Long component2() {
            return this.applicationId;
        }

        public final Long component3() {
            return this.coverImageAssetId;
        }

        public final String component4() {
            return this.description;
        }

        public final boolean component5() {
            return this.isFullServerGate;
        }

        public final ViewState copy(String str, Long l, Long l2, String str2, boolean z2) {
            return new ViewState(str, l, l2, str2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.coverImage, viewState.coverImage) && m.areEqual(this.applicationId, viewState.applicationId) && m.areEqual(this.coverImageAssetId, viewState.coverImageAssetId) && m.areEqual(this.description, viewState.description) && this.isFullServerGate == viewState.isFullServerGate;
        }

        public final Long getApplicationId() {
            return this.applicationId;
        }

        public final String getCoverImage() {
            return this.coverImage;
        }

        public final Long getCoverImageAssetId() {
            return this.coverImageAssetId;
        }

        public final String getDescription() {
            return this.description;
        }

        public int hashCode() {
            String str = this.coverImage;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Long l = this.applicationId;
            int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
            Long l2 = this.coverImageAssetId;
            int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
            String str2 = this.description;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i2 = (hashCode3 + i) * 31;
            boolean z2 = this.isFullServerGate;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isFullServerGate() {
            return this.isFullServerGate;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(coverImage=");
            R.append(this.coverImage);
            R.append(", applicationId=");
            R.append(this.applicationId);
            R.append(", coverImageAssetId=");
            R.append(this.coverImageAssetId);
            R.append(", description=");
            R.append(this.description);
            R.append(", isFullServerGate=");
            return a.M(R, this.isFullServerGate, ")");
        }

        public /* synthetic */ ViewState(String str, Long l, Long l2, String str2, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : l2, (i & 8) == 0 ? str2 : null, (i & 16) != 0 ? false : z2);
        }
    }

    public GuildRoleSubscriptionPlanDetailsViewModel() {
        super(new ViewState(null, null, null, null, false, 31, null));
    }

    public final void setGroupPlanDetails(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing, Boolean bool) {
        m.checkNotNullParameter(guildRoleSubscriptionGroupListing, "subscriptionGroupListing");
        Long valueOf = Long.valueOf(guildRoleSubscriptionGroupListing.b());
        ImageAsset g = guildRoleSubscriptionGroupListing.g();
        updateViewState(new ViewState(null, valueOf, g != null ? Long.valueOf(g.a()) : null, guildRoleSubscriptionGroupListing.c(), bool != null ? bool.booleanValue() : guildRoleSubscriptionGroupListing.d(), 1, null));
    }

    public final void updateCoverImage(String str) {
        updateViewState(ViewState.copy$default(requireViewState(), str, null, null, null, false, 30, null));
    }

    public final void updateDescription(String str) {
        updateViewState(ViewState.copy$default(requireViewState(), null, null, null, str, false, 23, null));
    }

    public final void updateFullServerGate(boolean z2) {
        updateViewState(ViewState.copy$default(requireViewState(), null, null, null, null, z2, 15, null));
    }
}
