package com.discord.widgets.guild_role_subscriptions.setup;

import android.view.View;
import com.discord.databinding.WidgetGuildSubscriptionPlanSetupBinding;
import com.discord.views.steps.StepsView;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildRoleSubscriptionPlanSetup.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildSubscriptionPlanSetupBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildSubscriptionPlanSetupBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildRoleSubscriptionPlanSetup$binding$2 extends k implements Function1<View, WidgetGuildSubscriptionPlanSetupBinding> {
    public static final WidgetGuildRoleSubscriptionPlanSetup$binding$2 INSTANCE = new WidgetGuildRoleSubscriptionPlanSetup$binding$2();

    public WidgetGuildRoleSubscriptionPlanSetup$binding$2() {
        super(1, WidgetGuildSubscriptionPlanSetupBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildSubscriptionPlanSetupBinding;", 0);
    }

    public final WidgetGuildSubscriptionPlanSetupBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        Objects.requireNonNull(view, "rootView");
        StepsView stepsView = (StepsView) view;
        return new WidgetGuildSubscriptionPlanSetupBinding(stepsView, stepsView);
    }
}
