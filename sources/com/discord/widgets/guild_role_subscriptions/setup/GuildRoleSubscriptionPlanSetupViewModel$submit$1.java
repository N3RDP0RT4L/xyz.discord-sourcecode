package com.discord.widgets.guild_role_subscriptions.setup;

import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.widgets.guild_role_subscriptions.setup.GuildRoleSubscriptionPlanSetupViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.objectweb.asm.Opcodes;
/* compiled from: GuildRoleSubscriptionPlanSetupViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "it", "", "invoke", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionPlanSetupViewModel$submit$1 extends o implements Function1<GuildRoleSubscriptionTierListing, Unit> {
    public final /* synthetic */ GuildRoleSubscriptionPlanSetupViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionPlanSetupViewModel$submit$1(GuildRoleSubscriptionPlanSetupViewModel guildRoleSubscriptionPlanSetupViewModel) {
        super(1);
        this.this$0 = guildRoleSubscriptionPlanSetupViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
        invoke2(guildRoleSubscriptionTierListing);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
        GuildRoleSubscriptionPlanSetupViewModel.ViewState requireViewState;
        m.checkNotNullParameter(guildRoleSubscriptionTierListing, "it");
        GuildRoleSubscriptionPlanSetupViewModel guildRoleSubscriptionPlanSetupViewModel = this.this$0;
        requireViewState = guildRoleSubscriptionPlanSetupViewModel.requireViewState();
        guildRoleSubscriptionPlanSetupViewModel.updateViewState(GuildRoleSubscriptionPlanSetupViewModel.ViewState.copy$default(requireViewState, 0, false, false, null, null, null, null, Opcodes.LUSHR, null));
        this.this$0.emitEvent(GuildRoleSubscriptionPlanSetupViewModel.Event.SubmitSuccess.INSTANCE);
    }
}
