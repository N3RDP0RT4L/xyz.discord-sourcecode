package com.discord.widgets.guild_role_subscriptions;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.DialogSimpleSelectorItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetPriceTierPickerBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001b\u0012\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b0\u0014¢\u0006\u0004\b\u0019\u0010\u001aJ\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u001b\u0010\u0012\u001a\u00020\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\u0010¢\u0006\u0004\b\u0012\u0010\u0013R\"\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b0\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00050\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/PriceTierPickerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/guild_role_subscriptions/PriceTierViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/guild_role_subscriptions/PriceTierViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/guild_role_subscriptions/PriceTierViewHolder;I)V", "getItemCount", "()I", "", "newItems", "setItems", "(Ljava/util/List;)V", "Lkotlin/Function1;", "itemClickListener", "Lkotlin/jvm/functions/Function1;", "items", "Ljava/util/List;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PriceTierPickerAdapter extends RecyclerView.Adapter<PriceTierViewHolder> {
    private final Function1<Integer, Unit> itemClickListener;
    private List<Integer> items = n.emptyList();

    /* JADX WARN: Multi-variable type inference failed */
    public PriceTierPickerAdapter(Function1<? super Integer, Unit> function1) {
        m.checkNotNullParameter(function1, "itemClickListener");
        this.itemClickListener = function1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    public final void setItems(List<Integer> list) {
        m.checkNotNullParameter(list, "newItems");
        this.items = list;
        notifyDataSetChanged();
    }

    public void onBindViewHolder(PriceTierViewHolder priceTierViewHolder, int i) {
        m.checkNotNullParameter(priceTierViewHolder, "holder");
        priceTierViewHolder.configureUI(this.items.get(i).intValue(), this.itemClickListener);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public PriceTierViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        DialogSimpleSelectorItemBinding a = DialogSimpleSelectorItemBinding.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        m.checkNotNullExpressionValue(a, "DialogSimpleSelectorItem…rent,\n        false\n    )");
        return new PriceTierViewHolder(a);
    }
}
