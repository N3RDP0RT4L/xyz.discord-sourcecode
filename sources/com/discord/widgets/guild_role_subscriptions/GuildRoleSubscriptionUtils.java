package com.discord.widgets.guild_role_subscriptions;

import andhook.lib.HookHelper;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: GuildRoleSubscriptionUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b/\u00100JI\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0095\u0001\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\t2\b\u0010\u0014\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0015\u001a\u00020\u00072\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\t2\u0006\u0010\b\u001a\u00020\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0018\u001a\u00020\u00172\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00192\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019¢\u0006\u0004\b\u001d\u0010\u001eJÃ\u0001\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u00042\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00192\u0010\b\u0002\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00192\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b!\u0010\"JA\u0010$\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010#0\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0004¢\u0006\u0004\b$\u0010%J«\u0001\u0010*\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\u0010&\u001a\u0004\u0018\u00010\t2\b\u0010'\u001a\u0004\u0018\u00010\t2\b\u0010(\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0013\u001a\u00020\t2\b\u0010\u0014\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0015\u001a\u00020\u00072\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\t2\u0006\u0010\b\u001a\u00020\u00072\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0018\u001a\u00020\u00172\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00192\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019¢\u0006\u0004\b*\u0010+JU\u0010-\u001a\b\u0012\u0004\u0012\u00020,0\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0012\u001a\u00020\u00042\b\u0010&\u001a\u0004\u0018\u00010\t2\b\u0010'\u001a\u0004\u0018\u00010\t2\b\u0010(\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b-\u0010.¨\u00061"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionUtils;", "", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "memberColor", "", "memberIcon", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "guildRoleSubscriptionTierListing", "Lrx/Observable;", "updateGuildRoleSubscriptionDesign", "(Lcom/discord/utilities/rest/RestAPI;JLjava/lang/Integer;Ljava/lang/String;Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;)Lrx/Observable;", "Lcom/discord/stores/StoreGuildRoleSubscriptions;", "storeGuildRoleSubscriptions", "groupListingId", "tierName", "tierDescription", "priceTier", "tierImage", "", "canAccessAllChannels", "", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "channelBenefits", "intangibleBenefits", "createGuildRoleSubscriptionTierListing", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildRoleSubscriptions;JJLjava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;ZLjava/util/List;Ljava/util/List;)Lrx/Observable;", "tierListingId", "published", "updateGuildRoleSubscriptionTierListing", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildRoleSubscriptions;JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;)Lrx/Observable;", "Ljava/lang/Void;", "deleteGuildRoleSubscriptionTierListing", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildRoleSubscriptions;JJJ)Lrx/Observable;", "coverImage", "planDescription", "isFullServerGating", "memberBadge", "createGuildRoleSubscriptionGroupListing", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildRoleSubscriptions;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;ZLjava/util/List;Ljava/util/List;)Lrx/Observable;", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "updateGuildRoleSubscriptionGroupListing", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildRoleSubscriptions;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionUtils {
    public static final GuildRoleSubscriptionUtils INSTANCE = new GuildRoleSubscriptionUtils();

    private GuildRoleSubscriptionUtils() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Observable<GuildRoleSubscriptionTierListing> updateGuildRoleSubscriptionDesign(RestAPI restAPI, long j, Integer num, String str, final GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
        Observable F = restAPI.updateRole(j, guildRoleSubscriptionTierListing.h(), new RestAPIParams.Role(null, null, null, num, null, null, guildRoleSubscriptionTierListing.h(), str, 55, null)).F(new b<Void, GuildRoleSubscriptionTierListing>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$updateGuildRoleSubscriptionDesign$1
            public final GuildRoleSubscriptionTierListing call(Void r1) {
                return GuildRoleSubscriptionTierListing.this;
            }
        });
        m.checkNotNullExpressionValue(F, "restApi\n          .updat…onTierListing\n          }");
        return F;
    }

    public final Observable<GuildRoleSubscriptionTierListing> createGuildRoleSubscriptionGroupListing(final RestAPI restAPI, final StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, final long j, String str, String str2, Boolean bool, final String str3, final String str4, final int i, final String str5, final int i2, final String str6, final boolean z2, final List<? extends Benefit> list, final List<? extends Benefit> list2) {
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(storeGuildRoleSubscriptions, "storeGuildRoleSubscriptions");
        m.checkNotNullParameter(str3, "tierName");
        m.checkNotNullParameter(list, "channelBenefits");
        m.checkNotNullParameter(list2, "intangibleBenefits");
        Observable<R> z3 = restAPI.createGuildRoleSubscriptionGroupListing(j, new RestAPIParams.CreateGuildRoleSubscriptionGroupListing(str, str2, bool)).z(new b<GuildRoleSubscriptionGroupListing, Observable<? extends GuildRoleSubscriptionTierListing>>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$createGuildRoleSubscriptionGroupListing$1
            public final Observable<? extends GuildRoleSubscriptionTierListing> call(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing) {
                StoreGuildRoleSubscriptions storeGuildRoleSubscriptions2 = StoreGuildRoleSubscriptions.this;
                long j2 = j;
                m.checkNotNullExpressionValue(guildRoleSubscriptionGroupListing, "subscriptionGroupListing");
                storeGuildRoleSubscriptions2.handleGuildRoleSubscriptionGroupUpdate(j2, guildRoleSubscriptionGroupListing);
                return GuildRoleSubscriptionUtils.INSTANCE.createGuildRoleSubscriptionTierListing(restAPI, StoreGuildRoleSubscriptions.this, j, guildRoleSubscriptionGroupListing.f(), str3, str4, i, str5, i2, str6, z2, list, list2);
            }
        });
        m.checkNotNullExpressionValue(z3, "restApi\n        .createG…ts,\n          )\n        }");
        return ObservableExtensionsKt.restSubscribeOn$default(z3, false, 1, null);
    }

    public final Observable<GuildRoleSubscriptionTierListing> createGuildRoleSubscriptionTierListing(final RestAPI restAPI, final StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, final long j, final long j2, String str, String str2, int i, String str3, final int i2, final String str4, boolean z2, List<? extends Benefit> list, List<? extends Benefit> list2) {
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(storeGuildRoleSubscriptions, "storeGuildRoleSubscriptions");
        m.checkNotNullParameter(str, "tierName");
        m.checkNotNullParameter(list, "channelBenefits");
        m.checkNotNullParameter(list2, "intangibleBenefits");
        List<Benefit> plus = u.plus((Collection) list, (Iterable) list2);
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(plus, 10));
        for (Benefit benefit : plus) {
            arrayList.add(benefit.toGuildRoleSubscriptionBenefit());
        }
        Observable<R> z3 = restAPI.createGuildRoleSubscriptionTier(j, j2, new RestAPIParams.CreateGuildRoleSubscriptionTierListing(str, str3, str2, arrayList, i, z2)).z(new b<GuildRoleSubscriptionTierListing, Observable<? extends GuildRoleSubscriptionTierListing>>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$createGuildRoleSubscriptionTierListing$1
            public final Observable<? extends GuildRoleSubscriptionTierListing> call(GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
                Observable<? extends GuildRoleSubscriptionTierListing> updateGuildRoleSubscriptionDesign;
                GuildRoleSubscriptionUtils guildRoleSubscriptionUtils = GuildRoleSubscriptionUtils.INSTANCE;
                RestAPI restAPI2 = RestAPI.this;
                long j3 = j;
                Integer valueOf = Integer.valueOf(i2);
                String str5 = str4;
                m.checkNotNullExpressionValue(guildRoleSubscriptionTierListing, "guildRoleSubscriptionTierListing");
                updateGuildRoleSubscriptionDesign = guildRoleSubscriptionUtils.updateGuildRoleSubscriptionDesign(restAPI2, j3, valueOf, str5, guildRoleSubscriptionTierListing);
                return updateGuildRoleSubscriptionDesign;
            }
        });
        m.checkNotNullExpressionValue(z3, "restApi\n        .createG…ng,\n          )\n        }");
        Observable<GuildRoleSubscriptionTierListing> t = ObservableExtensionsKt.restSubscribeOn$default(z3, false, 1, null).t(new Action1<GuildRoleSubscriptionTierListing>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$createGuildRoleSubscriptionTierListing$2
            public final void call(GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
                StoreGuildRoleSubscriptions storeGuildRoleSubscriptions2 = StoreGuildRoleSubscriptions.this;
                long j3 = j;
                long j4 = j2;
                m.checkNotNullExpressionValue(guildRoleSubscriptionTierListing, "tierListing");
                storeGuildRoleSubscriptions2.handleGuildRoleSubscriptionTierListingCreate(j3, j4, guildRoleSubscriptionTierListing);
            }
        });
        m.checkNotNullExpressionValue(t, "restApi\n        .createG…ing\n          )\n        }");
        return t;
    }

    public final Observable<Void> deleteGuildRoleSubscriptionTierListing(RestAPI restAPI, final StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, final long j, final long j2, final long j3) {
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(storeGuildRoleSubscriptions, "storeGuildRoleSubscriptions");
        Observable<Void> t = ObservableExtensionsKt.restSubscribeOn$default(restAPI.deleteGuildRoleSubscriptionTierListing(j, j2, j3), false, 1, null).t(new Action1<Void>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$deleteGuildRoleSubscriptionTierListing$1
            public final void call(Void r8) {
                StoreGuildRoleSubscriptions.this.handleGuildRoleSubscriptionTierListingDelete(j, j2, j3);
            }
        });
        m.checkNotNullExpressionValue(t, "restApi\n        .deleteG…Id,\n          )\n        }");
        return t;
    }

    public final Observable<GuildRoleSubscriptionGroupListing> updateGuildRoleSubscriptionGroupListing(RestAPI restAPI, final StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, final long j, long j2, String str, String str2, Boolean bool) {
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(storeGuildRoleSubscriptions, "storeGuildRoleSubscriptions");
        Observable<GuildRoleSubscriptionGroupListing> t = ObservableExtensionsKt.restSubscribeOn$default(restAPI.updateGuildRoleSubscriptionGroupListing(j, j2, new RestAPIParams.UpdateGuildRoleSubscriptionGroupListing(str, str2, bool)), false, 1, null).t(new Action1<GuildRoleSubscriptionGroupListing>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$updateGuildRoleSubscriptionGroupListing$1
            public final void call(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing) {
                StoreGuildRoleSubscriptions storeGuildRoleSubscriptions2 = StoreGuildRoleSubscriptions.this;
                long j3 = j;
                m.checkNotNullExpressionValue(guildRoleSubscriptionGroupListing, "groupListing");
                storeGuildRoleSubscriptions2.handleGuildRoleSubscriptionGroupUpdate(j3, guildRoleSubscriptionGroupListing);
            }
        });
        m.checkNotNullExpressionValue(t, "restApi\n        .updateG…, groupListing)\n        }");
        return t;
    }

    public final Observable<GuildRoleSubscriptionTierListing> updateGuildRoleSubscriptionTierListing(final RestAPI restAPI, final StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, final long j, long j2, long j3, String str, String str2, String str3, Integer num, final Integer num2, final String str4, Boolean bool, List<? extends Benefit> list, List<? extends Benefit> list2, Boolean bool2) {
        List<? extends Benefit> list3;
        ArrayList arrayList;
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(storeGuildRoleSubscriptions, "storeGuildRoleSubscriptions");
        if (list == null || list2 == null) {
            list3 = list != null ? list : list2;
        } else {
            list3 = u.plus((Collection) list, (Iterable) list2);
        }
        if (list3 != null) {
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(list3, 10));
            for (Benefit benefit : list3) {
                arrayList2.add(benefit.toGuildRoleSubscriptionBenefit());
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        Observable<R> z2 = restAPI.updateGuildRoleSubscriptionTierListing(j, j2, j3, new RestAPIParams.UpdateGuildRoleSubscriptionTierListing(str, str3, str2, num, arrayList, bool2, bool)).z(new b<GuildRoleSubscriptionTierListing, Observable<? extends GuildRoleSubscriptionTierListing>>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$updateGuildRoleSubscriptionTierListing$2
            public final Observable<? extends GuildRoleSubscriptionTierListing> call(GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
                Observable<? extends GuildRoleSubscriptionTierListing> updateGuildRoleSubscriptionDesign;
                GuildRoleSubscriptionUtils guildRoleSubscriptionUtils = GuildRoleSubscriptionUtils.INSTANCE;
                RestAPI restAPI2 = RestAPI.this;
                long j4 = j;
                Integer num3 = num2;
                String str5 = str4;
                m.checkNotNullExpressionValue(guildRoleSubscriptionTierListing, "guildRoleSubscriptionTierListing");
                updateGuildRoleSubscriptionDesign = guildRoleSubscriptionUtils.updateGuildRoleSubscriptionDesign(restAPI2, j4, num3, str5, guildRoleSubscriptionTierListing);
                return updateGuildRoleSubscriptionDesign;
            }
        });
        m.checkNotNullExpressionValue(z2, "restApi\n        .updateG…ng,\n          )\n        }");
        Observable<GuildRoleSubscriptionTierListing> t = ObservableExtensionsKt.restSubscribeOn$default(z2, false, 1, null).t(new Action1<GuildRoleSubscriptionTierListing>() { // from class: com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils$updateGuildRoleSubscriptionTierListing$3
            public final void call(GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
                StoreGuildRoleSubscriptions storeGuildRoleSubscriptions2 = StoreGuildRoleSubscriptions.this;
                long j4 = j;
                m.checkNotNullExpressionValue(guildRoleSubscriptionTierListing, "it");
                storeGuildRoleSubscriptions2.handleGuildRoleSubscriptionTierListingUpdate(j4, guildRoleSubscriptionTierListing);
            }
        });
        m.checkNotNullExpressionValue(t, "restApi\n        .updateG…te(guildId, it)\n        }");
        return t;
    }
}
