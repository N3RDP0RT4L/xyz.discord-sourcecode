package com.discord.widgets.guild_role_subscriptions;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.f.g.e.r;
import b.f.g.e.v;
import b.f.g.f.c;
import com.discord.api.role.GuildRole;
import com.discord.databinding.ViewGuildRoleSubscriptionImageUploadBinding;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildSubscriptionRoleImageUploadView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 02\u00020\u0001:\u00010B\u0011\b\u0016\u0012\u0006\u0010*\u001a\u00020)¢\u0006\u0004\b+\u0010,B\u001d\b\u0016\u0012\u0006\u0010*\u001a\u00020)\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b+\u0010-B'\b\u0016\u0012\u0006\u0010*\u001a\u00020)\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010.\u001a\u00020%¢\u0006\u0004\b+\u0010/J\u001b\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\u000b\u001a\u00020\u00042\u000e\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\u0006\u0010\n\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\u000b\u001a\u00020\u00042\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u000b\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J)\u0010\u0017\u001a\u00020\u00042\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00040\u00142\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00040\u0014¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u000b\u001a\u00020\u00042\b\u0010\u0019\u001a\u0004\u0018\u00010\r2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\b\u000b\u0010\u001cJ\u0015\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u000b\u0010\u001fR\u001e\u0010 \u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u001e\u0010(\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010!¨\u00061"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/GuildSubscriptionRoleImageUploadView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Landroid/util/AttributeSet;", "attrs", "", "initialize", "(Landroid/util/AttributeSet;)V", "", "Lcom/discord/primitives/ApplicationId;", "applicationId", "imageAssetId", "updateImage", "(Ljava/lang/Long;J)V", "", "imageUrl", "(Ljava/lang/String;)V", "", "hasImage", "updateImageVisibility", "(Z)V", "Lkotlin/Function0;", "onImageRemoved", "onImageChooserClicked", "configureUI", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "memberIcon", "Lcom/discord/api/role/GuildRole;", "guildRole", "(Ljava/lang/String;Lcom/discord/api/role/GuildRole;)V", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "guildSubscriptionTier", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;)V", "onImageChooserClickListener", "Lkotlin/jvm/functions/Function0;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionImageUploadBinding;", "binding", "Lcom/discord/databinding/ViewGuildRoleSubscriptionImageUploadBinding;", "", "avatarSize", "I", "onImageRemovedListener", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildSubscriptionRoleImageUploadView extends ConstraintLayout {
    private static final int CIRCLE_IMAGE_STYLE = 0;
    public static final Companion Companion = new Companion(null);
    private static final int ROUNDED_IMAGE_STYLE = 1;
    private int avatarSize;
    private final ViewGuildRoleSubscriptionImageUploadBinding binding;
    private Function0<Unit> onImageChooserClickListener;
    private Function0<Unit> onImageRemovedListener;

    /* compiled from: GuildSubscriptionRoleImageUploadView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/GuildSubscriptionRoleImageUploadView$Companion;", "", "", "CIRCLE_IMAGE_STYLE", "I", "ROUNDED_IMAGE_STYLE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildSubscriptionRoleImageUploadView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionImageUploadBinding a = ViewGuildRoleSubscriptionImageUploadBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size_huge);
        initialize$default(this, null, 1, null);
    }

    private final void initialize(AttributeSet attributeSet) {
        ScalingUtils$ScaleType scalingUtils$ScaleType;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        int[] iArr = com.discord.R.a.GuildSubscriptionRoleImageUploadView;
        m.checkNotNullExpressionValue(iArr, "R.styleable.GuildSubscriptionRoleImageUploadView");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        if (obtainStyledAttributes.getBoolean(4, false)) {
            ScalingUtils$ScaleType scalingUtils$ScaleType2 = ScalingUtils$ScaleType.a;
            scalingUtils$ScaleType = r.l;
        } else {
            ScalingUtils$ScaleType scalingUtils$ScaleType3 = ScalingUtils$ScaleType.a;
            scalingUtils$ScaleType = v.l;
        }
        SimpleDraweeView simpleDraweeView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildSubscriptionImage");
        GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
        m.checkNotNullExpressionValue(hierarchy, "binding.guildSubscriptionImage.hierarchy");
        hierarchy.n(scalingUtils$ScaleType);
        int i = obtainStyledAttributes.getInt(2, 0);
        if (i == 0) {
            this.binding.d.setBackgroundResource(DrawableCompat.getThemedDrawableRes(this, (int) R.attr.primary_700_circle, 0));
        } else if (i == 1) {
            SimpleDraweeView simpleDraweeView2 = this.binding.d;
            simpleDraweeView2.setBackgroundResource(R.drawable.drawable_rect_rounded_bg_tertiary);
            GenericDraweeHierarchy hierarchy2 = simpleDraweeView2.getHierarchy();
            m.checkNotNullExpressionValue(hierarchy2, "hierarchy");
            c cVar = new c();
            cVar.f519b = false;
            hierarchy2.s(cVar);
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildSubscriptio…            }\n          }");
        }
        String string = obtainStyledAttributes.getString(3);
        if (string == null) {
            string = getContext().getString(R.string.guild_role_subscription_tier_detail_custom_image_label);
        }
        m.checkNotNullExpressionValue(string, "it.getString(R.styleable…etail_custom_image_label)");
        TextView textView = this.binding.f2178b;
        m.checkNotNullExpressionValue(textView, "binding.guildSubscriptionCustomImageTitle");
        textView.setText(string);
        String string2 = obtainStyledAttributes.getString(0);
        if (string2 == null) {
            string2 = getContext().getString(R.string.guild_role_subscription_tier_detail_custom_image_description);
        }
        m.checkNotNullExpressionValue(string2, "it.getString(R.styleable…image_description\n      )");
        TextView textView2 = this.binding.f;
        m.checkNotNullExpressionValue(textView2, "binding.guildSubscriptionUploadImageLabel");
        textView2.setText(string2);
        this.avatarSize = obtainStyledAttributes.getDimensionPixelSize(1, getResources().getDimensionPixelSize(R.dimen.avatar_size_huge));
        obtainStyledAttributes.recycle();
        this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guild_role_subscriptions.GuildSubscriptionRoleImageUploadView$initialize$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0 function0;
                function0 = GuildSubscriptionRoleImageUploadView.this.onImageChooserClickListener;
                if (function0 != null) {
                    Unit unit = (Unit) function0.invoke();
                }
            }
        });
        this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guild_role_subscriptions.GuildSubscriptionRoleImageUploadView$initialize$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0 function0;
                function0 = GuildSubscriptionRoleImageUploadView.this.onImageChooserClickListener;
                if (function0 != null) {
                    Unit unit = (Unit) function0.invoke();
                }
            }
        });
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guild_role_subscriptions.GuildSubscriptionRoleImageUploadView$initialize$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0 function0;
                function0 = GuildSubscriptionRoleImageUploadView.this.onImageChooserClickListener;
                if (function0 != null) {
                    Unit unit = (Unit) function0.invoke();
                }
            }
        });
    }

    public static /* synthetic */ void initialize$default(GuildSubscriptionRoleImageUploadView guildSubscriptionRoleImageUploadView, AttributeSet attributeSet, int i, Object obj) {
        if ((i & 1) != 0) {
            attributeSet = null;
        }
        guildSubscriptionRoleImageUploadView.initialize(attributeSet);
    }

    private final void updateImageVisibility(boolean z2) {
        ImageView imageView = this.binding.c;
        m.checkNotNullExpressionValue(imageView, "binding.guildSubscriptionEditImage");
        imageView.setVisibility(z2 ? 0 : 8);
        if (z2) {
            this.binding.e.setText(R.string.avatar_upload_edit_media);
        } else {
            this.binding.e.setText(R.string.guild_role_subscription_tier_detail_custom_image_button);
        }
    }

    public final void configureUI(Function0<Unit> function0, Function0<Unit> function02) {
        m.checkNotNullParameter(function0, "onImageRemoved");
        m.checkNotNullParameter(function02, "onImageChooserClicked");
        this.onImageRemovedListener = function0;
        this.onImageChooserClickListener = function02;
    }

    public final void updateImage(String str, GuildRole guildRole) {
        boolean z2 = false;
        if (!(str == null || t.isBlank(str))) {
            updateImage(str);
            return;
        }
        if (guildRole != null) {
            String d = guildRole.d();
            if (d == null || t.isBlank(d)) {
                z2 = true;
            }
            if (!z2) {
                SimpleDraweeView simpleDraweeView = this.binding.d;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildSubscriptionImage");
                IconUtils.setIcon$default(simpleDraweeView, guildRole, (int) R.dimen.avatar_size_huge, (MGImages.ChangeDetector) null, 8, (Object) null);
                updateImageVisibility(true);
                return;
            }
        }
        updateImage((String) null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildSubscriptionRoleImageUploadView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionImageUploadBinding a = ViewGuildRoleSubscriptionImageUploadBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size_huge);
        initialize(attributeSet);
    }

    public final void updateImage(GuildRoleSubscriptionTier guildRoleSubscriptionTier) {
        m.checkNotNullParameter(guildRoleSubscriptionTier, "guildSubscriptionTier");
        String image = guildRoleSubscriptionTier.getImage();
        if (!(image == null || t.isBlank(image)) || guildRoleSubscriptionTier.getImageAssetId() == null) {
            updateImage(guildRoleSubscriptionTier.getImage());
        } else {
            updateImage(guildRoleSubscriptionTier.getApplicationId(), guildRoleSubscriptionTier.getImageAssetId().longValue());
        }
    }

    public /* synthetic */ GuildSubscriptionRoleImageUploadView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildSubscriptionRoleImageUploadView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionImageUploadBinding a = ViewGuildRoleSubscriptionImageUploadBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size_huge);
        initialize(attributeSet);
    }

    private final void updateImage(Long l, long j) {
        updateImage(IconUtils.INSTANCE.getStoreAssetImage(l, String.valueOf(j), this.avatarSize));
    }

    private final void updateImage(String str) {
        SimpleDraweeView simpleDraweeView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildSubscriptionImage");
        int i = this.avatarSize;
        IconUtils.setIcon$default(simpleDraweeView, str, i, i, false, null, null, 112, null);
        updateImageVisibility(!(str == null || t.isBlank(str)));
    }

    public /* synthetic */ GuildSubscriptionRoleImageUploadView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
