package com.discord.widgets.guild_role_subscriptions.entrypoint;

import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.models.domain.ModelSubscription;
import com.discord.stores.StoreConnectivity;
import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreSubscriptions;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtilsKt;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionsFeatureFlag;
import com.discord.widgets.guild_role_subscriptions.entrypoint.WidgetGuildRoleSubscriptionEntryPointViewModel;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildRoleSubscriptionEntryPointViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/entrypoint/WidgetGuildRoleSubscriptionEntryPointViewModel$StoreState;", "invoke", "()Lcom/discord/widgets/guild_role_subscriptions/entrypoint/WidgetGuildRoleSubscriptionEntryPointViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildRoleSubscriptionEntryPointViewModel$observeStoreState$1 extends o implements Function0<WidgetGuildRoleSubscriptionEntryPointViewModel.StoreState> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ WidgetGuildRoleSubscriptionEntryPointViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildRoleSubscriptionEntryPointViewModel$observeStoreState$1(WidgetGuildRoleSubscriptionEntryPointViewModel widgetGuildRoleSubscriptionEntryPointViewModel, long j) {
        super(0);
        this.this$0 = widgetGuildRoleSubscriptionEntryPointViewModel;
        this.$guildId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetGuildRoleSubscriptionEntryPointViewModel.StoreState invoke() {
        StoreGuildRoleSubscriptions storeGuildRoleSubscriptions;
        StoreSubscriptions storeSubscriptions;
        StoreConnectivity storeConnectivity;
        StoreGuilds storeGuilds;
        storeGuildRoleSubscriptions = this.this$0.guildRoleSubscriptionsStore;
        StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionState = storeGuildRoleSubscriptions.getGuildRoleSubscriptionState(this.$guildId);
        Boolean bool = null;
        if (!(guildRoleSubscriptionState instanceof StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded)) {
            guildRoleSubscriptionState = null;
        }
        StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded loaded = (StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded) guildRoleSubscriptionState;
        GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing = loaded != null ? loaded.getGuildRoleSubscriptionGroupListing() : null;
        storeSubscriptions = this.this$0.subscriptionsStore;
        StoreSubscriptions.SubscriptionsState subscriptions = storeSubscriptions.getSubscriptions();
        if (!(subscriptions instanceof StoreSubscriptions.SubscriptionsState.Loaded)) {
            subscriptions = null;
        }
        StoreSubscriptions.SubscriptionsState.Loaded loaded2 = (StoreSubscriptions.SubscriptionsState.Loaded) subscriptions;
        List<ModelSubscription> subscriptions2 = loaded2 != null ? loaded2.getSubscriptions() : null;
        Long valueOf = guildRoleSubscriptionGroupListing != null ? Long.valueOf(guildRoleSubscriptionGroupListing.f()) : null;
        if (!(subscriptions2 == null || guildRoleSubscriptionGroupListing == null)) {
            bool = Boolean.valueOf(GuildRoleSubscriptionUtilsKt.hasUserActiveSubscriptionFor(guildRoleSubscriptionGroupListing, subscriptions2));
        }
        Boolean bool2 = bool;
        storeConnectivity = this.this$0.connectivityStore;
        boolean isConnected = storeConnectivity.isConnected();
        boolean canGuildSeePremiumMemberships = GuildRoleSubscriptionsFeatureFlag.Companion.getINSTANCE().canGuildSeePremiumMemberships(this.$guildId);
        storeGuilds = this.this$0.guildsStore;
        return new WidgetGuildRoleSubscriptionEntryPointViewModel.StoreState(isConnected, canGuildSeePremiumMemberships, storeGuilds.getGuild(this.$guildId), bool2, valueOf);
    }
}
