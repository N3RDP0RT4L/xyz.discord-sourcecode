package com.discord.widgets.guild_role_subscriptions;

import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.api.guildrolesubscription.PayoutStatus;
import com.discord.api.permission.Permission;
import com.discord.api.premium.SubscriptionPlan;
import com.discord.api.role.GuildRole;
import com.discord.models.domain.ModelSubscription;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.widgets.servers.guild_role_subscription.model.PayoutStatusMedia;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0011\u0010\u0002\u001a\u00020\u0001*\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u001a-\u0010\u000b\u001a\u0004\u0018\u00010\n*\u00020\u00042\u0018\u0010\t\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u0005¢\u0006\u0004\b\u000b\u0010\f\u001a\u001f\u0010\u0010\u001a\u00020\n*\u00020\u00042\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\u0004\b\u0010\u0010\u0011\u001a\u0011\u0010\u0014\u001a\u00020\u0013*\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015¨\u0006\u0016"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "", "getPrice", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;)I", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "", "getFullServerGatingOverwrite", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;Ljava/util/Map;)Ljava/lang/Boolean;", "", "Lcom/discord/models/domain/ModelSubscription;", "userSubscriptions", "hasUserActiveSubscriptionFor", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;Ljava/util/List;)Z", "Lcom/discord/api/guildrolesubscription/PayoutStatus;", "Lcom/discord/widgets/servers/guild_role_subscription/model/PayoutStatusMedia;", "getStatusMedia", "(Lcom/discord/api/guildrolesubscription/PayoutStatus;)Lcom/discord/widgets/servers/guild_role_subscription/model/PayoutStatusMedia;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionUtilsKt {

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            PayoutStatus.values();
            int[] iArr = new int[14];
            $EnumSwitchMapping$0 = iArr;
            iArr[PayoutStatus.MANUAL.ordinal()] = 1;
            iArr[PayoutStatus.OPEN.ordinal()] = 2;
            iArr[PayoutStatus.PENDING.ordinal()] = 3;
            iArr[PayoutStatus.PROCESSING.ordinal()] = 4;
            iArr[PayoutStatus.SUBMITTED.ordinal()] = 5;
            iArr[PayoutStatus.PENDING_FUNDS.ordinal()] = 6;
            iArr[PayoutStatus.PAID.ordinal()] = 7;
            iArr[PayoutStatus.CANCELED.ordinal()] = 8;
            iArr[PayoutStatus.DEFERRED.ordinal()] = 9;
            iArr[PayoutStatus.DEFERRED_INTERNAL.ordinal()] = 10;
            iArr[PayoutStatus.ERROR.ordinal()] = 11;
            iArr[PayoutStatus.UNKNOWN.ordinal()] = 12;
            iArr[PayoutStatus.REJECTED.ordinal()] = 13;
            iArr[PayoutStatus.RISK_REVIEW.ordinal()] = 14;
        }
    }

    public static final Boolean getFullServerGatingOverwrite(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing, Map<Long, GuildRole> map) {
        Boolean bool;
        m.checkNotNullParameter(guildRoleSubscriptionGroupListing, "$this$getFullServerGatingOverwrite");
        List<GuildRoleSubscriptionTierListing> h = guildRoleSubscriptionGroupListing.h();
        if (h != null) {
            boolean z2 = false;
            if (!h.isEmpty()) {
                Iterator<T> it = h.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((GuildRoleSubscriptionTierListing) it.next()).f()) {
                            z2 = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            bool = Boolean.valueOf(z2);
        } else {
            bool = null;
        }
        if (bool == null || !bool.booleanValue()) {
            return null;
        }
        return Boolean.valueOf(!PermissionUtils.INSTANCE.canRole(Permission.VIEW_CHANNEL, map != null ? map.get(Long.valueOf(guildRoleSubscriptionGroupListing.e())) : null, null));
    }

    public static final int getPrice(GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
        m.checkNotNullParameter(guildRoleSubscriptionTierListing, "$this$getPrice");
        SubscriptionPlan subscriptionPlan = (SubscriptionPlan) u.getOrNull(guildRoleSubscriptionTierListing.i(), 0);
        if (subscriptionPlan != null) {
            return subscriptionPlan.d();
        }
        return 0;
    }

    public static final PayoutStatusMedia getStatusMedia(PayoutStatus payoutStatus) {
        m.checkNotNullParameter(payoutStatus, "$this$getStatusMedia");
        switch (payoutStatus.ordinal()) {
            case 0:
            case 9:
                return new PayoutStatusMedia(R.drawable.ic_close, R.string.guild_role_subscription_earnings_table_status_error);
            case 1:
            case 3:
            case 4:
            case 8:
            case 12:
            case 13:
                return new PayoutStatusMedia(R.drawable.ic_pending, R.string.guild_role_subscription_earnings_table_status_scheduled);
            case 2:
                return new PayoutStatusMedia(R.drawable.ic_paid, R.string.guild_role_subscription_earnings_table_status_paid);
            case 5:
                return new PayoutStatusMedia(R.drawable.ic_close, R.string.guild_role_subscription_earnings_table_status_cancelled);
            case 6:
                return new PayoutStatusMedia(R.drawable.ic_close, R.string.guild_role_subscription_earnings_table_status_cancelled);
            case 7:
                return new PayoutStatusMedia(R.drawable.ic_close, R.string.guild_role_subscription_earnings_table_status_deferred);
            case 10:
                return new PayoutStatusMedia(R.drawable.ic_close, R.string.guild_role_subscription_earnings_table_status_rejected);
            case 11:
                return new PayoutStatusMedia(R.drawable.ic_close, R.string.guild_role_subscription_earnings_table_status_risk_review);
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public static final boolean hasUserActiveSubscriptionFor(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing, List<ModelSubscription> list) {
        m.checkNotNullParameter(guildRoleSubscriptionGroupListing, "$this$hasUserActiveSubscriptionFor");
        m.checkNotNullParameter(list, "userSubscriptions");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            Long l = null;
            if (!it.hasNext()) {
                break;
            }
            ModelSubscription.SubscriptionItem subscriptionItem = (ModelSubscription.SubscriptionItem) u.getOrNull(((ModelSubscription) it.next()).getItems(), 0);
            if (subscriptionItem != null) {
                l = Long.valueOf(subscriptionItem.getPlanId());
            }
            if (l != null) {
                arrayList.add(l);
            }
        }
        Set set = u.toSet(arrayList);
        List<GuildRoleSubscriptionTierListing> h = guildRoleSubscriptionGroupListing.h();
        if (h != null) {
            for (GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing : h) {
                SubscriptionPlan subscriptionPlan = (SubscriptionPlan) u.getOrNull(guildRoleSubscriptionTierListing.i(), 0);
                Long valueOf = subscriptionPlan != null ? Long.valueOf(subscriptionPlan.a()) : null;
                if (valueOf != null && set.contains(valueOf)) {
                    return true;
                }
            }
        }
        return false;
    }
}
