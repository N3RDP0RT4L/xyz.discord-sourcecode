package com.discord.widgets.guild_role_subscriptions.tier;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.ViewGuildRoleSubscriptionPlanDetailsReviewBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionTierHeaderBinding;
import com.discord.utilities.billing.PremiumUtilsKt;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionBenefitItemView;
import com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierAdapterItem;
import com.discord.widgets.guild_role_subscriptions.tier.create.GuildRoleSubscriptionMemberPreview;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionTierViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u000b\f\r\u000e\u000fB\u0011\b\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006\u0082\u0001\u0005\u0010\u0011\u0012\u0013\u0014¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;", "item", "", "bind", "(Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;)V", "Landroid/view/View;", "view", HookHelper.constructorName, "(Landroid/view/View;)V", "BenefitViewHolder", "MemberPreviewViewHolder", "PlanDetailsViewHolder", "SectionHeaderViewHolder", "TierHeaderViewHolder", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$TierHeaderViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$MemberPreviewViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$SectionHeaderViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$BenefitViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$PlanDetailsViewHolder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildRoleSubscriptionTierViewHolder extends RecyclerView.ViewHolder {

    /* compiled from: GuildRoleSubscriptionTierViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\n\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$BenefitViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionBenefitItemView;", "component1", "()Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionBenefitItemView;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;", "item", "", "bind", "(Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;)V", "view", "copy", "(Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionBenefitItemView;)Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$BenefitViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionBenefitItemView;", HookHelper.constructorName, "(Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionBenefitItemView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BenefitViewHolder extends GuildRoleSubscriptionTierViewHolder {
        private final GuildRoleSubscriptionBenefitItemView view;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public BenefitViewHolder(GuildRoleSubscriptionBenefitItemView guildRoleSubscriptionBenefitItemView) {
            super(guildRoleSubscriptionBenefitItemView, null);
            m.checkNotNullParameter(guildRoleSubscriptionBenefitItemView, "view");
            this.view = guildRoleSubscriptionBenefitItemView;
            guildRoleSubscriptionBenefitItemView.setPadding(0, 0, 0, 0);
            guildRoleSubscriptionBenefitItemView.setBackground(null);
        }

        private final GuildRoleSubscriptionBenefitItemView component1() {
            return this.view;
        }

        public static /* synthetic */ BenefitViewHolder copy$default(BenefitViewHolder benefitViewHolder, GuildRoleSubscriptionBenefitItemView guildRoleSubscriptionBenefitItemView, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionBenefitItemView = benefitViewHolder.view;
            }
            return benefitViewHolder.copy(guildRoleSubscriptionBenefitItemView);
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder
        public void bind(GuildRoleSubscriptionTierAdapterItem guildRoleSubscriptionTierAdapterItem) {
            m.checkNotNullParameter(guildRoleSubscriptionTierAdapterItem, "item");
            if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.BenefitItem) {
                this.view.configureUI(((GuildRoleSubscriptionTierAdapterItem.BenefitItem) guildRoleSubscriptionTierAdapterItem).getBenefit());
            } else if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.AllChannelsAccessBenefitItem) {
                GuildRoleSubscriptionBenefitItemView guildRoleSubscriptionBenefitItemView = this.view;
                String string = guildRoleSubscriptionBenefitItemView.getContext().getString(R.string.guild_role_subscription_tier_review_entire_server_access_benefit);
                m.checkNotNullExpressionValue(string, "view.context.getString(R…re_server_access_benefit)");
                guildRoleSubscriptionBenefitItemView.configureUI(string, R.drawable.ic_key_24dp, (String) null);
            }
        }

        public final BenefitViewHolder copy(GuildRoleSubscriptionBenefitItemView guildRoleSubscriptionBenefitItemView) {
            m.checkNotNullParameter(guildRoleSubscriptionBenefitItemView, "view");
            return new BenefitViewHolder(guildRoleSubscriptionBenefitItemView);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof BenefitViewHolder) && m.areEqual(this.view, ((BenefitViewHolder) obj).view);
            }
            return true;
        }

        public int hashCode() {
            GuildRoleSubscriptionBenefitItemView guildRoleSubscriptionBenefitItemView = this.view;
            if (guildRoleSubscriptionBenefitItemView != null) {
                return guildRoleSubscriptionBenefitItemView.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("BenefitViewHolder(view=");
            R.append(this.view);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionTierViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\n\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$MemberPreviewViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionMemberPreview;", "component1", "()Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionMemberPreview;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;", "item", "", "bind", "(Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;)V", "memberPreview", "copy", "(Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionMemberPreview;)Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$MemberPreviewViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionMemberPreview;", HookHelper.constructorName, "(Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionMemberPreview;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MemberPreviewViewHolder extends GuildRoleSubscriptionTierViewHolder {
        private final GuildRoleSubscriptionMemberPreview memberPreview;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public MemberPreviewViewHolder(GuildRoleSubscriptionMemberPreview guildRoleSubscriptionMemberPreview) {
            super(guildRoleSubscriptionMemberPreview, null);
            m.checkNotNullParameter(guildRoleSubscriptionMemberPreview, "memberPreview");
            this.memberPreview = guildRoleSubscriptionMemberPreview;
            guildRoleSubscriptionMemberPreview.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            guildRoleSubscriptionMemberPreview.setBackgroundColor(ColorCompat.getThemedColor(guildRoleSubscriptionMemberPreview, (int) R.attr.colorBackgroundSecondary));
        }

        private final GuildRoleSubscriptionMemberPreview component1() {
            return this.memberPreview;
        }

        public static /* synthetic */ MemberPreviewViewHolder copy$default(MemberPreviewViewHolder memberPreviewViewHolder, GuildRoleSubscriptionMemberPreview guildRoleSubscriptionMemberPreview, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionMemberPreview = memberPreviewViewHolder.memberPreview;
            }
            return memberPreviewViewHolder.copy(guildRoleSubscriptionMemberPreview);
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder
        public void bind(GuildRoleSubscriptionTierAdapterItem guildRoleSubscriptionTierAdapterItem) {
            m.checkNotNullParameter(guildRoleSubscriptionTierAdapterItem, "item");
            GuildRoleSubscriptionTierAdapterItem.MemberPreview memberPreview = (GuildRoleSubscriptionTierAdapterItem.MemberPreview) guildRoleSubscriptionTierAdapterItem;
            GuildRoleSubscriptionMemberPreview.setMemberDesign$default(this.memberPreview, memberPreview.getMemberColor(), memberPreview.getMemberIcon(), null, 4, null);
        }

        public final MemberPreviewViewHolder copy(GuildRoleSubscriptionMemberPreview guildRoleSubscriptionMemberPreview) {
            m.checkNotNullParameter(guildRoleSubscriptionMemberPreview, "memberPreview");
            return new MemberPreviewViewHolder(guildRoleSubscriptionMemberPreview);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof MemberPreviewViewHolder) && m.areEqual(this.memberPreview, ((MemberPreviewViewHolder) obj).memberPreview);
            }
            return true;
        }

        public int hashCode() {
            GuildRoleSubscriptionMemberPreview guildRoleSubscriptionMemberPreview = this.memberPreview;
            if (guildRoleSubscriptionMemberPreview != null) {
                return guildRoleSubscriptionMemberPreview.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("MemberPreviewViewHolder(memberPreview=");
            R.append(this.memberPreview);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionTierViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\n\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$PlanDetailsViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionPlanDetailsReviewBinding;", "component1", "()Lcom/discord/databinding/ViewGuildRoleSubscriptionPlanDetailsReviewBinding;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;", "item", "", "bind", "(Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;)V", "binding", "copy", "(Lcom/discord/databinding/ViewGuildRoleSubscriptionPlanDetailsReviewBinding;)Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$PlanDetailsViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/databinding/ViewGuildRoleSubscriptionPlanDetailsReviewBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionPlanDetailsReviewBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PlanDetailsViewHolder extends GuildRoleSubscriptionTierViewHolder {
        private final ViewGuildRoleSubscriptionPlanDetailsReviewBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public PlanDetailsViewHolder(com.discord.databinding.ViewGuildRoleSubscriptionPlanDetailsReviewBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.LinearLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder.PlanDetailsViewHolder.<init>(com.discord.databinding.ViewGuildRoleSubscriptionPlanDetailsReviewBinding):void");
        }

        private final ViewGuildRoleSubscriptionPlanDetailsReviewBinding component1() {
            return this.binding;
        }

        public static /* synthetic */ PlanDetailsViewHolder copy$default(PlanDetailsViewHolder planDetailsViewHolder, ViewGuildRoleSubscriptionPlanDetailsReviewBinding viewGuildRoleSubscriptionPlanDetailsReviewBinding, int i, Object obj) {
            if ((i & 1) != 0) {
                viewGuildRoleSubscriptionPlanDetailsReviewBinding = planDetailsViewHolder.binding;
            }
            return planDetailsViewHolder.copy(viewGuildRoleSubscriptionPlanDetailsReviewBinding);
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder
        public void bind(GuildRoleSubscriptionTierAdapterItem guildRoleSubscriptionTierAdapterItem) {
            m.checkNotNullParameter(guildRoleSubscriptionTierAdapterItem, "item");
            GuildRoleSubscriptionTierAdapterItem.PlanDetails planDetails = (GuildRoleSubscriptionTierAdapterItem.PlanDetails) guildRoleSubscriptionTierAdapterItem;
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.guildRoleSubscriptionPlanReviewDescription");
            textView.setText(planDetails.getDescription());
            SimpleDraweeView simpleDraweeView = this.binding.f2179b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildRoleSubscriptionPlanReviewCoverImage");
            MGImages.setImage$default(simpleDraweeView, planDetails.getCoverImage(), 0, 0, false, null, null, 124, null);
        }

        public final PlanDetailsViewHolder copy(ViewGuildRoleSubscriptionPlanDetailsReviewBinding viewGuildRoleSubscriptionPlanDetailsReviewBinding) {
            m.checkNotNullParameter(viewGuildRoleSubscriptionPlanDetailsReviewBinding, "binding");
            return new PlanDetailsViewHolder(viewGuildRoleSubscriptionPlanDetailsReviewBinding);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof PlanDetailsViewHolder) && m.areEqual(this.binding, ((PlanDetailsViewHolder) obj).binding);
            }
            return true;
        }

        public int hashCode() {
            ViewGuildRoleSubscriptionPlanDetailsReviewBinding viewGuildRoleSubscriptionPlanDetailsReviewBinding = this.binding;
            if (viewGuildRoleSubscriptionPlanDetailsReviewBinding != null) {
                return viewGuildRoleSubscriptionPlanDetailsReviewBinding.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("PlanDetailsViewHolder(binding=");
            R.append(this.binding);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionTierViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\n\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$SectionHeaderViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;", "component1", "()Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;", "item", "", "bind", "(Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;)V", "binding", "copy", "(Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;)Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$SectionHeaderViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SectionHeaderViewHolder extends GuildRoleSubscriptionTierViewHolder {
        private final ViewGuildRoleSubscriptionSectionHeaderItemBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public SectionHeaderViewHolder(com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.LinearLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                android.view.View r3 = r2.itemView
                r0 = 0
                r3.setPadding(r0, r0, r0, r0)
                r3.setBackground(r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder.SectionHeaderViewHolder.<init>(com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding):void");
        }

        private final ViewGuildRoleSubscriptionSectionHeaderItemBinding component1() {
            return this.binding;
        }

        public static /* synthetic */ SectionHeaderViewHolder copy$default(SectionHeaderViewHolder sectionHeaderViewHolder, ViewGuildRoleSubscriptionSectionHeaderItemBinding viewGuildRoleSubscriptionSectionHeaderItemBinding, int i, Object obj) {
            if ((i & 1) != 0) {
                viewGuildRoleSubscriptionSectionHeaderItemBinding = sectionHeaderViewHolder.binding;
            }
            return sectionHeaderViewHolder.copy(viewGuildRoleSubscriptionSectionHeaderItemBinding);
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder
        public void bind(GuildRoleSubscriptionTierAdapterItem guildRoleSubscriptionTierAdapterItem) {
            CharSequence charSequence;
            m.checkNotNullParameter(guildRoleSubscriptionTierAdapterItem, "item");
            GuildRoleSubscriptionTierAdapterItem.SectionHeader sectionHeader = (GuildRoleSubscriptionTierAdapterItem.SectionHeader) guildRoleSubscriptionTierAdapterItem;
            if (sectionHeader.getTitleResId() != null) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                charSequence = view.getContext().getString(sectionHeader.getTitleResId().intValue());
            } else {
                charSequence = (sectionHeader.getTitlePluralResId() == null || sectionHeader.getFormatArgument() == null) ? null : StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), sectionHeader.getTitlePluralResId().intValue(), sectionHeader.getFormatArgument().intValue(), sectionHeader.getFormatArgument());
            }
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.guildRoleSubscriptionSectionHeader");
            textView.setText(charSequence);
        }

        public final SectionHeaderViewHolder copy(ViewGuildRoleSubscriptionSectionHeaderItemBinding viewGuildRoleSubscriptionSectionHeaderItemBinding) {
            m.checkNotNullParameter(viewGuildRoleSubscriptionSectionHeaderItemBinding, "binding");
            return new SectionHeaderViewHolder(viewGuildRoleSubscriptionSectionHeaderItemBinding);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof SectionHeaderViewHolder) && m.areEqual(this.binding, ((SectionHeaderViewHolder) obj).binding);
            }
            return true;
        }

        public int hashCode() {
            ViewGuildRoleSubscriptionSectionHeaderItemBinding viewGuildRoleSubscriptionSectionHeaderItemBinding = this.binding;
            if (viewGuildRoleSubscriptionSectionHeaderItemBinding != null) {
                return viewGuildRoleSubscriptionSectionHeaderItemBinding.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("SectionHeaderViewHolder(binding=");
            R.append(this.binding);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionTierViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\n\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$TierHeaderViewHolder;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionTierHeaderBinding;", "component1", "()Lcom/discord/databinding/ViewGuildRoleSubscriptionTierHeaderBinding;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;", "item", "", "bind", "(Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;)V", "binding", "copy", "(Lcom/discord/databinding/ViewGuildRoleSubscriptionTierHeaderBinding;)Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder$TierHeaderViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/databinding/ViewGuildRoleSubscriptionTierHeaderBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionTierHeaderBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TierHeaderViewHolder extends GuildRoleSubscriptionTierViewHolder {
        private final ViewGuildRoleSubscriptionTierHeaderBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public TierHeaderViewHolder(com.discord.databinding.ViewGuildRoleSubscriptionTierHeaderBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.LinearLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder.TierHeaderViewHolder.<init>(com.discord.databinding.ViewGuildRoleSubscriptionTierHeaderBinding):void");
        }

        private final ViewGuildRoleSubscriptionTierHeaderBinding component1() {
            return this.binding;
        }

        public static /* synthetic */ TierHeaderViewHolder copy$default(TierHeaderViewHolder tierHeaderViewHolder, ViewGuildRoleSubscriptionTierHeaderBinding viewGuildRoleSubscriptionTierHeaderBinding, int i, Object obj) {
            if ((i & 1) != 0) {
                viewGuildRoleSubscriptionTierHeaderBinding = tierHeaderViewHolder.binding;
            }
            return tierHeaderViewHolder.copy(viewGuildRoleSubscriptionTierHeaderBinding);
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder
        public void bind(GuildRoleSubscriptionTierAdapterItem guildRoleSubscriptionTierAdapterItem) {
            CharSequence charSequence;
            m.checkNotNullParameter(guildRoleSubscriptionTierAdapterItem, "item");
            GuildRoleSubscriptionTierAdapterItem.Header header = (GuildRoleSubscriptionTierAdapterItem.Header) guildRoleSubscriptionTierAdapterItem;
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.guildRoleSubscriptionTierHeaderName");
            textView.setText(header.getName());
            TextView textView2 = this.binding.f2184b;
            m.checkNotNullExpressionValue(textView2, "binding.guildRoleSubscriptionTierHeaderDescription");
            textView2.setText(header.getDescription());
            if (header.getPrice() != null) {
                int intValue = header.getPrice().intValue();
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "itemView.context");
                charSequence = PremiumUtilsKt.getFormattedPriceUsd(intValue, context);
            } else {
                charSequence = null;
            }
            TextView textView3 = this.binding.e;
            m.checkNotNullExpressionValue(textView3, "binding.guildRoleSubscriptionTierHeaderPrice");
            ViewExtensions.setTextAndVisibilityBy(textView3, charSequence);
            SimpleDraweeView simpleDraweeView = this.binding.c;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildRoleSubscriptionTierHeaderImage");
            IconUtils.setIcon$default(simpleDraweeView, header.getImage(), (int) R.dimen.avatar_size_xxlarge, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        }

        public final TierHeaderViewHolder copy(ViewGuildRoleSubscriptionTierHeaderBinding viewGuildRoleSubscriptionTierHeaderBinding) {
            m.checkNotNullParameter(viewGuildRoleSubscriptionTierHeaderBinding, "binding");
            return new TierHeaderViewHolder(viewGuildRoleSubscriptionTierHeaderBinding);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof TierHeaderViewHolder) && m.areEqual(this.binding, ((TierHeaderViewHolder) obj).binding);
            }
            return true;
        }

        public int hashCode() {
            ViewGuildRoleSubscriptionTierHeaderBinding viewGuildRoleSubscriptionTierHeaderBinding = this.binding;
            if (viewGuildRoleSubscriptionTierHeaderBinding != null) {
                return viewGuildRoleSubscriptionTierHeaderBinding.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("TierHeaderViewHolder(binding=");
            R.append(this.binding);
            R.append(")");
            return R.toString();
        }
    }

    private GuildRoleSubscriptionTierViewHolder(View view) {
        super(view);
    }

    public abstract void bind(GuildRoleSubscriptionTierAdapterItem guildRoleSubscriptionTierAdapterItem);

    public /* synthetic */ GuildRoleSubscriptionTierViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
