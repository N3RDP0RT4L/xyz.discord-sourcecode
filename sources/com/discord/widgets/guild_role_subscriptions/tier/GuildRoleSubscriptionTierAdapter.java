package com.discord.widgets.guild_role_subscriptions.tier;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.databinding.ViewGuildRoleSubscriptionPlanDetailsReviewBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionTierHeaderBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.recycler.DiffCreator;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionBenefitItemView;
import com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierAdapterItem;
import com.discord.widgets.guild_role_subscriptions.tier.GuildRoleSubscriptionTierViewHolder;
import com.discord.widgets.guild_role_subscriptions.tier.create.GuildRoleSubscriptionMemberPreview;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionTierAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000  2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001 B+\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u001a\b\u0002\u0010\u0018\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u0017¢\u0006\u0004\b\u001e\u0010\u001fJ\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0010\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0015\u001a\u00020\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012¢\u0006\u0004\b\u0015\u0010\u0016R(\u0010\u0018\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006!"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierViewHolder;I)V", "getItemCount", "()I", "getItemViewType", "(I)I", "", "Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapterItem;", "newItems", "setItems", "(Ljava/util/List;)V", "Lcom/discord/utilities/recycler/DiffCreator;", "diffCreator", "Lcom/discord/utilities/recycler/DiffCreator;", "items", "Ljava/util/List;", "Lcom/discord/app/AppComponent;", "appComponent", HookHelper.constructorName, "(Lcom/discord/app/AppComponent;Lcom/discord/utilities/recycler/DiffCreator;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionTierAdapter extends RecyclerView.Adapter<GuildRoleSubscriptionTierViewHolder> {
    public static final Companion Companion = new Companion(null);
    public static final int VIEW_TYPE_ALL_CHANNELS_ACCESS_BENEFIT = 5;
    public static final int VIEW_TYPE_PLAN_DETAILS = 4;
    public static final int VIEW_TYPE_TIER_BENEFIT = 3;
    public static final int VIEW_TYPE_TIER_HEADER = 0;
    public static final int VIEW_TYPE_TIER_MEMBER_PREVIEW = 1;
    public static final int VIEW_TYPE_TIER_SECTION_HEADER = 2;
    private final DiffCreator<List<GuildRoleSubscriptionTierAdapterItem>, GuildRoleSubscriptionTierViewHolder> diffCreator;
    private List<? extends GuildRoleSubscriptionTierAdapterItem> items;

    /* compiled from: GuildRoleSubscriptionTierAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/GuildRoleSubscriptionTierAdapter$Companion;", "", "", "VIEW_TYPE_ALL_CHANNELS_ACCESS_BENEFIT", "I", "VIEW_TYPE_PLAN_DETAILS", "VIEW_TYPE_TIER_BENEFIT", "VIEW_TYPE_TIER_HEADER", "VIEW_TYPE_TIER_MEMBER_PREVIEW", "VIEW_TYPE_TIER_SECTION_HEADER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ GuildRoleSubscriptionTierAdapter(AppComponent appComponent, DiffCreator diffCreator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(appComponent, (i & 2) != 0 ? new DiffCreator(appComponent) : diffCreator);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        GuildRoleSubscriptionTierAdapterItem guildRoleSubscriptionTierAdapterItem = this.items.get(i);
        if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.Header) {
            return 0;
        }
        if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.MemberPreview) {
            return 1;
        }
        if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.SectionHeader) {
            return 2;
        }
        if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.BenefitItem) {
            return 3;
        }
        if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.PlanDetails) {
            return 4;
        }
        if (guildRoleSubscriptionTierAdapterItem instanceof GuildRoleSubscriptionTierAdapterItem.AllChannelsAccessBenefitItem) {
            return 5;
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void setItems(List<? extends GuildRoleSubscriptionTierAdapterItem> list) {
        m.checkNotNullParameter(list, "newItems");
        this.diffCreator.dispatchDiffUpdatesAsync(this, new GuildRoleSubscriptionTierAdapter$setItems$1(this), this.items, list);
    }

    public GuildRoleSubscriptionTierAdapter(AppComponent appComponent, DiffCreator<List<GuildRoleSubscriptionTierAdapterItem>, GuildRoleSubscriptionTierViewHolder> diffCreator) {
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(diffCreator, "diffCreator");
        this.diffCreator = diffCreator;
        this.items = n.emptyList();
    }

    public void onBindViewHolder(GuildRoleSubscriptionTierViewHolder guildRoleSubscriptionTierViewHolder, int i) {
        m.checkNotNullParameter(guildRoleSubscriptionTierViewHolder, "holder");
        guildRoleSubscriptionTierViewHolder.bind(this.items.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public GuildRoleSubscriptionTierViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_guild_role_subscription_tier_header, viewGroup, false);
            int i2 = R.id.guild_role_subscription_tier_header_description;
            TextView textView = (TextView) inflate.findViewById(R.id.guild_role_subscription_tier_header_description);
            if (textView != null) {
                i2 = R.id.guild_role_subscription_tier_header_image;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.guild_role_subscription_tier_header_image);
                if (simpleDraweeView != null) {
                    i2 = R.id.guild_role_subscription_tier_header_name;
                    TextView textView2 = (TextView) inflate.findViewById(R.id.guild_role_subscription_tier_header_name);
                    if (textView2 != null) {
                        i2 = R.id.guild_role_subscription_tier_header_price;
                        TextView textView3 = (TextView) inflate.findViewById(R.id.guild_role_subscription_tier_header_price);
                        if (textView3 != null) {
                            i2 = R.id.guild_role_subscription_tier_header_price_interval;
                            TextView textView4 = (TextView) inflate.findViewById(R.id.guild_role_subscription_tier_header_price_interval);
                            if (textView4 != null) {
                                i2 = R.id.guild_role_subscription_tier_header_subscribe;
                                Button button = (Button) inflate.findViewById(R.id.guild_role_subscription_tier_header_subscribe);
                                if (button != null) {
                                    ViewGuildRoleSubscriptionTierHeaderBinding viewGuildRoleSubscriptionTierHeaderBinding = new ViewGuildRoleSubscriptionTierHeaderBinding((LinearLayout) inflate, textView, simpleDraweeView, textView2, textView3, textView4, button);
                                    m.checkNotNullExpressionValue(viewGuildRoleSubscriptionTierHeaderBinding, "ViewGuildRoleSubscriptio…rent, false\n            )");
                                    return new GuildRoleSubscriptionTierViewHolder.TierHeaderViewHolder(viewGuildRoleSubscriptionTierHeaderBinding);
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
        } else if (i == 1) {
            Context context = viewGroup.getContext();
            m.checkNotNullExpressionValue(context, "parent.context");
            return new GuildRoleSubscriptionTierViewHolder.MemberPreviewViewHolder(new GuildRoleSubscriptionMemberPreview(context));
        } else if (i == 2) {
            ViewGuildRoleSubscriptionSectionHeaderItemBinding a = ViewGuildRoleSubscriptionSectionHeaderItemBinding.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
            m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…rent, false\n            )");
            return new GuildRoleSubscriptionTierViewHolder.SectionHeaderViewHolder(a);
        } else if (i == 3) {
            Context context2 = viewGroup.getContext();
            m.checkNotNullExpressionValue(context2, "parent.context");
            return new GuildRoleSubscriptionTierViewHolder.BenefitViewHolder(new GuildRoleSubscriptionBenefitItemView(context2));
        } else if (i == 4) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_guild_role_subscription_plan_details_review, viewGroup, false);
            int i3 = R.id.guild_role_subscription_plan_review_cover_image;
            SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) inflate2.findViewById(R.id.guild_role_subscription_plan_review_cover_image);
            if (simpleDraweeView2 != null) {
                i3 = R.id.guild_role_subscription_plan_review_description;
                TextView textView5 = (TextView) inflate2.findViewById(R.id.guild_role_subscription_plan_review_description);
                if (textView5 != null) {
                    ViewGuildRoleSubscriptionPlanDetailsReviewBinding viewGuildRoleSubscriptionPlanDetailsReviewBinding = new ViewGuildRoleSubscriptionPlanDetailsReviewBinding((LinearLayout) inflate2, simpleDraweeView2, textView5);
                    m.checkNotNullExpressionValue(viewGuildRoleSubscriptionPlanDetailsReviewBinding, "ViewGuildRoleSubscriptio…rent, false\n            )");
                    return new GuildRoleSubscriptionTierViewHolder.PlanDetailsViewHolder(viewGuildRoleSubscriptionPlanDetailsReviewBinding);
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i3)));
        } else if (i == 5) {
            Context context3 = viewGroup.getContext();
            m.checkNotNullExpressionValue(context3, "parent.context");
            return new GuildRoleSubscriptionTierViewHolder.BenefitViewHolder(new GuildRoleSubscriptionBenefitItemView(context3));
        } else {
            StringBuilder S = a.S("invalid viewType ", i, " for ");
            S.append(GuildRoleSubscriptionTierAdapter.class.getSimpleName());
            throw new IllegalArgumentException(S.toString());
        }
    }
}
