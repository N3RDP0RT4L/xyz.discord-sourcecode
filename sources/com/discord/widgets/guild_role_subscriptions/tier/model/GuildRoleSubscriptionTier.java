package com.discord.widgets.guild_role_subscriptions.tier.model;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildRoleSubscriptionTier.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b*\b\u0086\b\u0018\u00002\u00020\u0001B±\u0001\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\b\u0012\u0010\b\u0002\u0010\"\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0014\u0012\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0002\u0012\u000e\b\u0002\u0010,\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b\u0012\u000e\b\u0002\u0010-\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001b¢\u0006\u0004\bG\u0010HJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0006\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0018\u0010\r\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u0013\u0010\nJ\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u0017\u0010\nJ\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u0018\u0010\u000eJ\u0012\u0010\u0019\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u0019\u0010\nJ\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0004J\u0016\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ\u0016\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001f0\u001bHÆ\u0003¢\u0006\u0004\b \u0010\u001eJº\u0001\u0010.\u001a\u00020\u00002\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\b2\u0010\b\u0002\u0010\"\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010,\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b2\u000e\b\u0002\u0010-\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001bHÆ\u0001¢\u0006\u0004\b.\u0010/J\u0010\u00100\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b0\u0010\nJ\u0010\u00101\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b1\u00102J\u001a\u00104\u001a\u00020\u00022\b\u00103\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b4\u00105R\u001b\u0010(\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u00106\u001a\u0004\b7\u0010\u000eR\u001b\u0010)\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u00108\u001a\u0004\b9\u0010\nR\u001b\u0010*\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010:\u001a\u0004\b*\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010;\u001a\u0004\b<\u0010\u0016R\u001b\u0010!\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00108\u001a\u0004\b=\u0010\nR\u001b\u0010#\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010>\u001a\u0004\b?\u0010\u0011R\u001b\u0010$\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010>\u001a\u0004\b@\u0010\u0011R!\u0010\"\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00106\u001a\u0004\bA\u0010\u000eR\u001b\u0010%\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u00108\u001a\u0004\bB\u0010\nR\u001b\u0010'\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u00108\u001a\u0004\bC\u0010\nR\u001f\u0010,\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010D\u001a\u0004\bE\u0010\u001eR\u001f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010D\u001a\u0004\bF\u0010\u001eR\u0018\u0010+\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010:¨\u0006I"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "", "", "component11", "()Ljava/lang/Boolean;", "isFullServerGating", "canAccessAllChannelsOrDefault", "(Ljava/lang/Boolean;)Z", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/ApplicationId;", "component2", "()Ljava/lang/Long;", "", "component3", "()Ljava/lang/Integer;", "component4", "component5", "Lcom/discord/api/role/GuildRole;", "component6", "()Lcom/discord/api/role/GuildRole;", "component7", "component8", "component9", "component10", "", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$ChannelBenefit;", "component12", "()Ljava/util/List;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$IntangibleBenefit;", "component13", ModelAuditLogEntry.CHANGE_KEY_NAME, "applicationId", "priceTier", "memberColor", "memberIcon", "guildRole", "image", "imageAssetId", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "isPublished", "canAccessAllChannels", "channelBenefits", "intangibleBenefits", "copy", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/discord/api/role/GuildRole;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getImageAssetId", "Ljava/lang/String;", "getDescription", "Ljava/lang/Boolean;", "Lcom/discord/api/role/GuildRole;", "getGuildRole", "getName", "Ljava/lang/Integer;", "getPriceTier", "getMemberColor", "getApplicationId", "getMemberIcon", "getImage", "Ljava/util/List;", "getChannelBenefits", "getIntangibleBenefits", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/discord/api/role/GuildRole;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionTier {
    private final Long applicationId;
    private final Boolean canAccessAllChannels;
    private final List<Benefit.ChannelBenefit> channelBenefits;
    private final String description;
    private final GuildRole guildRole;
    private final String image;
    private final Long imageAssetId;
    private final List<Benefit.IntangibleBenefit> intangibleBenefits;
    private final Boolean isPublished;
    private final Integer memberColor;
    private final String memberIcon;
    private final String name;
    private final Integer priceTier;

    public GuildRoleSubscriptionTier() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, 8191, null);
    }

    public GuildRoleSubscriptionTier(String str, Long l, Integer num, Integer num2, String str2, GuildRole guildRole, String str3, Long l2, String str4, Boolean bool, Boolean bool2, List<Benefit.ChannelBenefit> list, List<Benefit.IntangibleBenefit> list2) {
        m.checkNotNullParameter(list, "channelBenefits");
        m.checkNotNullParameter(list2, "intangibleBenefits");
        this.name = str;
        this.applicationId = l;
        this.priceTier = num;
        this.memberColor = num2;
        this.memberIcon = str2;
        this.guildRole = guildRole;
        this.image = str3;
        this.imageAssetId = l2;
        this.description = str4;
        this.isPublished = bool;
        this.canAccessAllChannels = bool2;
        this.channelBenefits = list;
        this.intangibleBenefits = list2;
    }

    private final Boolean component11() {
        return this.canAccessAllChannels;
    }

    public final boolean canAccessAllChannelsOrDefault(Boolean bool) {
        Boolean bool2 = Boolean.TRUE;
        return m.areEqual(bool, bool2) && (m.areEqual(this.canAccessAllChannels, bool2) || this.canAccessAllChannels == null);
    }

    public final String component1() {
        return this.name;
    }

    public final Boolean component10() {
        return this.isPublished;
    }

    public final List<Benefit.ChannelBenefit> component12() {
        return this.channelBenefits;
    }

    public final List<Benefit.IntangibleBenefit> component13() {
        return this.intangibleBenefits;
    }

    public final Long component2() {
        return this.applicationId;
    }

    public final Integer component3() {
        return this.priceTier;
    }

    public final Integer component4() {
        return this.memberColor;
    }

    public final String component5() {
        return this.memberIcon;
    }

    public final GuildRole component6() {
        return this.guildRole;
    }

    public final String component7() {
        return this.image;
    }

    public final Long component8() {
        return this.imageAssetId;
    }

    public final String component9() {
        return this.description;
    }

    public final GuildRoleSubscriptionTier copy(String str, Long l, Integer num, Integer num2, String str2, GuildRole guildRole, String str3, Long l2, String str4, Boolean bool, Boolean bool2, List<Benefit.ChannelBenefit> list, List<Benefit.IntangibleBenefit> list2) {
        m.checkNotNullParameter(list, "channelBenefits");
        m.checkNotNullParameter(list2, "intangibleBenefits");
        return new GuildRoleSubscriptionTier(str, l, num, num2, str2, guildRole, str3, l2, str4, bool, bool2, list, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildRoleSubscriptionTier)) {
            return false;
        }
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = (GuildRoleSubscriptionTier) obj;
        return m.areEqual(this.name, guildRoleSubscriptionTier.name) && m.areEqual(this.applicationId, guildRoleSubscriptionTier.applicationId) && m.areEqual(this.priceTier, guildRoleSubscriptionTier.priceTier) && m.areEqual(this.memberColor, guildRoleSubscriptionTier.memberColor) && m.areEqual(this.memberIcon, guildRoleSubscriptionTier.memberIcon) && m.areEqual(this.guildRole, guildRoleSubscriptionTier.guildRole) && m.areEqual(this.image, guildRoleSubscriptionTier.image) && m.areEqual(this.imageAssetId, guildRoleSubscriptionTier.imageAssetId) && m.areEqual(this.description, guildRoleSubscriptionTier.description) && m.areEqual(this.isPublished, guildRoleSubscriptionTier.isPublished) && m.areEqual(this.canAccessAllChannels, guildRoleSubscriptionTier.canAccessAllChannels) && m.areEqual(this.channelBenefits, guildRoleSubscriptionTier.channelBenefits) && m.areEqual(this.intangibleBenefits, guildRoleSubscriptionTier.intangibleBenefits);
    }

    public final Long getApplicationId() {
        return this.applicationId;
    }

    public final List<Benefit.ChannelBenefit> getChannelBenefits() {
        return this.channelBenefits;
    }

    public final String getDescription() {
        return this.description;
    }

    public final GuildRole getGuildRole() {
        return this.guildRole;
    }

    public final String getImage() {
        return this.image;
    }

    public final Long getImageAssetId() {
        return this.imageAssetId;
    }

    public final List<Benefit.IntangibleBenefit> getIntangibleBenefits() {
        return this.intangibleBenefits;
    }

    public final Integer getMemberColor() {
        return this.memberColor;
    }

    public final String getMemberIcon() {
        return this.memberIcon;
    }

    public final String getName() {
        return this.name;
    }

    public final Integer getPriceTier() {
        return this.priceTier;
    }

    public int hashCode() {
        String str = this.name;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Long l = this.applicationId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Integer num = this.priceTier;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.memberColor;
        int hashCode4 = (hashCode3 + (num2 != null ? num2.hashCode() : 0)) * 31;
        String str2 = this.memberIcon;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        GuildRole guildRole = this.guildRole;
        int hashCode6 = (hashCode5 + (guildRole != null ? guildRole.hashCode() : 0)) * 31;
        String str3 = this.image;
        int hashCode7 = (hashCode6 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Long l2 = this.imageAssetId;
        int hashCode8 = (hashCode7 + (l2 != null ? l2.hashCode() : 0)) * 31;
        String str4 = this.description;
        int hashCode9 = (hashCode8 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Boolean bool = this.isPublished;
        int hashCode10 = (hashCode9 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.canAccessAllChannels;
        int hashCode11 = (hashCode10 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        List<Benefit.ChannelBenefit> list = this.channelBenefits;
        int hashCode12 = (hashCode11 + (list != null ? list.hashCode() : 0)) * 31;
        List<Benefit.IntangibleBenefit> list2 = this.intangibleBenefits;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode12 + i;
    }

    public final Boolean isPublished() {
        return this.isPublished;
    }

    public String toString() {
        StringBuilder R = a.R("GuildRoleSubscriptionTier(name=");
        R.append(this.name);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", priceTier=");
        R.append(this.priceTier);
        R.append(", memberColor=");
        R.append(this.memberColor);
        R.append(", memberIcon=");
        R.append(this.memberIcon);
        R.append(", guildRole=");
        R.append(this.guildRole);
        R.append(", image=");
        R.append(this.image);
        R.append(", imageAssetId=");
        R.append(this.imageAssetId);
        R.append(", description=");
        R.append(this.description);
        R.append(", isPublished=");
        R.append(this.isPublished);
        R.append(", canAccessAllChannels=");
        R.append(this.canAccessAllChannels);
        R.append(", channelBenefits=");
        R.append(this.channelBenefits);
        R.append(", intangibleBenefits=");
        return a.K(R, this.intangibleBenefits, ")");
    }

    public /* synthetic */ GuildRoleSubscriptionTier(String str, Long l, Integer num, Integer num2, String str2, GuildRole guildRole, String str3, Long l2, String str4, Boolean bool, Boolean bool2, List list, List list2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : num, (i & 8) != 0 ? null : num2, (i & 16) != 0 ? null : str2, (i & 32) != 0 ? null : guildRole, (i & 64) != 0 ? null : str3, (i & 128) != 0 ? null : l2, (i & 256) != 0 ? null : str4, (i & 512) != 0 ? null : bool, (i & 1024) == 0 ? bool2 : null, (i & 2048) != 0 ? n.emptyList() : list, (i & 4096) != 0 ? n.emptyList() : list2);
    }
}
