package com.discord.widgets.guild_role_subscriptions.tier.create;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import com.discord.api.role.GuildRole;
import com.discord.databinding.WidgetChatListAdapterItemTextBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import d0.g;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionMemberPreview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016B\u001d\b\u0016\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b\u0015\u0010\u0019B'\b\u0016\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u001bJ1\u0010\t\u001a\u00020\b2\n\b\u0001\u0010\u0003\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\t\u0010\nR\u001d\u0010\u000f\u001a\u00020\u00028B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionMemberPreview;", "Landroid/widget/FrameLayout;", "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "", "icon", "Lcom/discord/api/role/GuildRole;", "guildRole", "", "setMemberDesign", "(Ljava/lang/Integer;Ljava/lang/String;Lcom/discord/api/role/GuildRole;)V", "defaultColor$delegate", "Lkotlin/Lazy;", "getDefaultColor", "()I", "defaultColor", "Lcom/discord/databinding/WidgetChatListAdapterItemTextBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemTextBinding;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionMemberPreview extends FrameLayout {
    private final WidgetChatListAdapterItemTextBinding binding;
    private final Lazy defaultColor$delegate;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionMemberPreview(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        WidgetChatListAdapterItemTextBinding b2 = WidgetChatListAdapterItemTextBinding.b(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(b2, "WidgetChatListAdapterIte…rom(context), this, true)");
        this.binding = b2;
        this.defaultColor$delegate = g.lazy(new GuildRoleSubscriptionMemberPreview$defaultColor$2(this));
        if (isInEditMode()) {
            b2.c.setImageResource(R.drawable.img_guild_role_subscription_default_avatar_40dp);
        } else {
            b2.c.setActualImageResource(R.drawable.img_guild_role_subscription_default_avatar_40dp);
        }
        TextView textView = b2.f;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemTextTag");
        textView.setVisibility(8);
        b2.d.setText(R.string.wumpus);
        b2.g.setText(R.string.sample_time_pm);
        b2.f2319b.setText(R.string.guild_role_subscription_tier_design_member_preview_placeholder_message);
        setBackgroundColor(ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundPrimary));
        b2.h.setPadding(0, 0, 0, 0);
        b2.e.setRole((GuildRole) null, (Long) null);
    }

    private final int getDefaultColor() {
        return ((Number) this.defaultColor$delegate.getValue()).intValue();
    }

    public static /* synthetic */ void setMemberDesign$default(GuildRoleSubscriptionMemberPreview guildRoleSubscriptionMemberPreview, Integer num, String str, GuildRole guildRole, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            guildRole = null;
        }
        guildRoleSubscriptionMemberPreview.setMemberDesign(num, str, guildRole);
    }

    public final void setMemberDesign(@ColorInt Integer num, String str, GuildRole guildRole) {
        this.binding.d.setTextColor(num != null ? num.intValue() : getDefaultColor());
        if (!(str == null || t.isBlank(str))) {
            this.binding.e.setRoleIconPreview(str);
        } else {
            this.binding.e.setRoleIconPreview(guildRole);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionMemberPreview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        WidgetChatListAdapterItemTextBinding b2 = WidgetChatListAdapterItemTextBinding.b(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(b2, "WidgetChatListAdapterIte…rom(context), this, true)");
        this.binding = b2;
        this.defaultColor$delegate = g.lazy(new GuildRoleSubscriptionMemberPreview$defaultColor$2(this));
        if (isInEditMode()) {
            b2.c.setImageResource(R.drawable.img_guild_role_subscription_default_avatar_40dp);
        } else {
            b2.c.setActualImageResource(R.drawable.img_guild_role_subscription_default_avatar_40dp);
        }
        TextView textView = b2.f;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemTextTag");
        textView.setVisibility(8);
        b2.d.setText(R.string.wumpus);
        b2.g.setText(R.string.sample_time_pm);
        b2.f2319b.setText(R.string.guild_role_subscription_tier_design_member_preview_placeholder_message);
        setBackgroundColor(ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundPrimary));
        b2.h.setPadding(0, 0, 0, 0);
        b2.e.setRole((GuildRole) null, (Long) null);
    }

    public /* synthetic */ GuildRoleSubscriptionMemberPreview(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionMemberPreview(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        WidgetChatListAdapterItemTextBinding b2 = WidgetChatListAdapterItemTextBinding.b(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(b2, "WidgetChatListAdapterIte…rom(context), this, true)");
        this.binding = b2;
        this.defaultColor$delegate = g.lazy(new GuildRoleSubscriptionMemberPreview$defaultColor$2(this));
        if (isInEditMode()) {
            b2.c.setImageResource(R.drawable.img_guild_role_subscription_default_avatar_40dp);
        } else {
            b2.c.setActualImageResource(R.drawable.img_guild_role_subscription_default_avatar_40dp);
        }
        TextView textView = b2.f;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemTextTag");
        textView.setVisibility(8);
        b2.d.setText(R.string.wumpus);
        b2.g.setText(R.string.sample_time_pm);
        b2.f2319b.setText(R.string.guild_role_subscription_tier_design_member_preview_placeholder_message);
        setBackgroundColor(ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundPrimary));
        b2.h.setPadding(0, 0, 0, 0);
        b2.e.setRole((GuildRole) null, (Long) null);
    }

    public /* synthetic */ GuildRoleSubscriptionMemberPreview(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
