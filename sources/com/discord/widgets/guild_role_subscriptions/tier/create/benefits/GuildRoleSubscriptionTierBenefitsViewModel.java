package com.discord.widgets.guild_role_subscriptions.tier.create.benefits;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildRoleSubscriptionTierBenefitsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B\u001b\u0012\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u001a\u0010\u000f\u001a\u00060\rj\u0002`\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitsViewModel$ViewState;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "guildRoleSubscriptionTier", "", "isFullServerGating", "", "updateBenefits", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;Ljava/lang/Boolean;)V", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitListType;", "benefitListType", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitListType;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", HookHelper.constructorName, "(JLcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitListType;)V", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionTierBenefitsViewModel extends AppViewModel<ViewState> {
    private final GuildRoleSubscriptionTierBenefitListType benefitListType;
    private final long guildId;

    /* compiled from: GuildRoleSubscriptionTierBenefitsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ6\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0017\u001a\u00020\u00022\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\bR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\n¨\u0006!"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitsViewModel$ViewState;", "", "", "component1", "()Z", "", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem;", "component2", "()Ljava/util/List;", "component3", "()Ljava/lang/Boolean;", "canChangeChannelSettings", "items", "canAccessAllChannels", "copy", "(ZLjava/util/List;Ljava/lang/Boolean;)Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitsViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanChangeChannelSettings", "Ljava/util/List;", "getItems", "Ljava/lang/Boolean;", "getCanAccessAllChannels", HookHelper.constructorName, "(ZLjava/util/List;Ljava/lang/Boolean;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Boolean canAccessAllChannels;
        private final boolean canChangeChannelSettings;
        private final List<GuildRoleSubscriptionBenefitAdapterItem> items;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(boolean z2, List<? extends GuildRoleSubscriptionBenefitAdapterItem> list, Boolean bool) {
            m.checkNotNullParameter(list, "items");
            this.canChangeChannelSettings = z2;
            this.items = list;
            this.canAccessAllChannels = bool;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, boolean z2, List list, Boolean bool, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = viewState.canChangeChannelSettings;
            }
            if ((i & 2) != 0) {
                list = viewState.items;
            }
            if ((i & 4) != 0) {
                bool = viewState.canAccessAllChannels;
            }
            return viewState.copy(z2, list, bool);
        }

        public final boolean component1() {
            return this.canChangeChannelSettings;
        }

        public final List<GuildRoleSubscriptionBenefitAdapterItem> component2() {
            return this.items;
        }

        public final Boolean component3() {
            return this.canAccessAllChannels;
        }

        public final ViewState copy(boolean z2, List<? extends GuildRoleSubscriptionBenefitAdapterItem> list, Boolean bool) {
            m.checkNotNullParameter(list, "items");
            return new ViewState(z2, list, bool);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return this.canChangeChannelSettings == viewState.canChangeChannelSettings && m.areEqual(this.items, viewState.items) && m.areEqual(this.canAccessAllChannels, viewState.canAccessAllChannels);
        }

        public final Boolean getCanAccessAllChannels() {
            return this.canAccessAllChannels;
        }

        public final boolean getCanChangeChannelSettings() {
            return this.canChangeChannelSettings;
        }

        public final List<GuildRoleSubscriptionBenefitAdapterItem> getItems() {
            return this.items;
        }

        public int hashCode() {
            boolean z2 = this.canChangeChannelSettings;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            int i3 = i * 31;
            List<GuildRoleSubscriptionBenefitAdapterItem> list = this.items;
            int i4 = 0;
            int hashCode = (i3 + (list != null ? list.hashCode() : 0)) * 31;
            Boolean bool = this.canAccessAllChannels;
            if (bool != null) {
                i4 = bool.hashCode();
            }
            return hashCode + i4;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(canChangeChannelSettings=");
            R.append(this.canChangeChannelSettings);
            R.append(", items=");
            R.append(this.items);
            R.append(", canAccessAllChannels=");
            return a.C(R, this.canAccessAllChannels, ")");
        }

        public /* synthetic */ ViewState(boolean z2, List list, Boolean bool, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(z2, (i & 2) != 0 ? n.emptyList() : list, (i & 4) != 0 ? null : bool);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionTierBenefitsViewModel(long j, GuildRoleSubscriptionTierBenefitListType guildRoleSubscriptionTierBenefitListType) {
        super(null, 1, null);
        m.checkNotNullParameter(guildRoleSubscriptionTierBenefitListType, "benefitListType");
        this.guildId = j;
        this.benefitListType = guildRoleSubscriptionTierBenefitListType;
    }

    public final void updateBenefits(GuildRoleSubscriptionTier guildRoleSubscriptionTier, Boolean bool) {
        m.checkNotNullParameter(guildRoleSubscriptionTier, "guildRoleSubscriptionTier");
        updateViewState(new ViewState(m.areEqual(bool, Boolean.TRUE) && this.benefitListType != GuildRoleSubscriptionTierBenefitListType.INTANGIBLE, GuildRoleSubscriptionBenefitListItemGeneratorKt.generateGuildRoleSubscriptionBenefitListItems(guildRoleSubscriptionTier.getChannelBenefits(), guildRoleSubscriptionTier.getIntangibleBenefits(), this.benefitListType, this.guildId, guildRoleSubscriptionTier.getName(), Boolean.valueOf(guildRoleSubscriptionTier.canAccessAllChannelsOrDefault(bool))), Boolean.valueOf(guildRoleSubscriptionTier.canAccessAllChannelsOrDefault(bool))));
    }
}
