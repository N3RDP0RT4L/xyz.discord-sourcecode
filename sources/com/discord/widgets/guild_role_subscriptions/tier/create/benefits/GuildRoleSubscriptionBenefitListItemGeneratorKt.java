package com.discord.widgets.guild_role_subscriptions.tier.create.benefits;

import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType;
import com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionBenefitAdapterItem;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildRoleSubscriptionBenefitListItemGenerator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001aW\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00002\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u00002\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00002\u0006\u0010\u0006\u001a\u00020\u00052\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\u0010\r\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0011"}, d2 = {"", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$ChannelBenefit;", "channelBenefits", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$IntangibleBenefit;", "intangibleBenefits", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitListType;", "benefitListType", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "tierName", "", "canAccessAllChannels", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem;", "generateGuildRoleSubscriptionBenefitListItems", "(Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitListType;JLjava/lang/String;Ljava/lang/Boolean;)Ljava/util/List;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionBenefitListItemGeneratorKt {
    public static final List<GuildRoleSubscriptionBenefitAdapterItem> generateGuildRoleSubscriptionBenefitListItems(List<Benefit.ChannelBenefit> list, List<Benefit.IntangibleBenefit> list2, GuildRoleSubscriptionTierBenefitListType guildRoleSubscriptionTierBenefitListType, long j, String str, Boolean bool) {
        m.checkNotNullParameter(list, "channelBenefits");
        m.checkNotNullParameter(list2, "intangibleBenefits");
        m.checkNotNullParameter(guildRoleSubscriptionTierBenefitListType, "benefitListType");
        ArrayList arrayList = new ArrayList();
        if ((!m.areEqual(bool, Boolean.TRUE)) && (guildRoleSubscriptionTierBenefitListType == GuildRoleSubscriptionTierBenefitListType.ALL || guildRoleSubscriptionTierBenefitListType == GuildRoleSubscriptionTierBenefitListType.CHANNEL)) {
            arrayList.add(new GuildRoleSubscriptionBenefitAdapterItem.Header(GuildRoleSubscriptionBenefitType.CHANNEL));
            for (Benefit.ChannelBenefit channelBenefit : list) {
                arrayList.add(new GuildRoleSubscriptionBenefitAdapterItem.BenefitItem(GuildRoleSubscriptionBenefitType.CHANNEL, channelBenefit, j, str));
            }
            arrayList.add(new GuildRoleSubscriptionBenefitAdapterItem.AddBenefitItem(GuildRoleSubscriptionBenefitType.CHANNEL, j, str));
        }
        if (guildRoleSubscriptionTierBenefitListType == GuildRoleSubscriptionTierBenefitListType.ALL || guildRoleSubscriptionTierBenefitListType == GuildRoleSubscriptionTierBenefitListType.INTANGIBLE) {
            arrayList.add(new GuildRoleSubscriptionBenefitAdapterItem.Header(GuildRoleSubscriptionBenefitType.INTANGIBLE));
            for (Benefit.IntangibleBenefit intangibleBenefit : list2) {
                arrayList.add(new GuildRoleSubscriptionBenefitAdapterItem.BenefitItem(GuildRoleSubscriptionBenefitType.INTANGIBLE, intangibleBenefit, j, str));
            }
            arrayList.add(new GuildRoleSubscriptionBenefitAdapterItem.AddBenefitItem(GuildRoleSubscriptionBenefitType.INTANGIBLE, j, str));
        }
        return arrayList;
    }
}
