package com.discord.widgets.guild_role_subscriptions.tier.create.benefits;

import andhook.lib.HookHelper;
import androidx.annotation.DrawableRes;
import b.d.b.a.a;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: GuildRoleSubscriptionTierBenefitViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002./B%\u0012\u0006\u0010(\u001a\u00020'\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010*¢\u0006\u0004\b,\u0010-J\u0013\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\n\u0010\u000bJ+\u0010\u0013\u001a\u00020\t2\n\u0010\u000e\u001a\u00060\fj\u0002`\r2\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u000f¢\u0006\u0004\b\u0016\u0010\u0017J\u0015\u0010\u0019\u001a\u00020\t2\u0006\u0010\u0018\u001a\u00020\u000f¢\u0006\u0004\b\u0019\u0010\u0017J\u0017\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001a\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u001b\u0010\u001cJ\r\u0010\u001d\u001a\u00020\t¢\u0006\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R:\u0010%\u001a&\u0012\f\u0012\n $*\u0004\u0018\u00010\u00040\u0004 $*\u0012\u0012\f\u0012\n $*\u0004\u0018\u00010\u00040\u0004\u0018\u00010#0#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)¨\u00060"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$ViewState;", "Lrx/Observable;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/models/domain/emoji/Emoji;", "emoji", "", "updateEmoji", "(Lcom/discord/models/domain/emoji/Emoji;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "channelName", "", "channelIconResId", "updateChannel", "(JLjava/lang/String;Ljava/lang/Integer;)V", ModelAuditLogEntry.CHANGE_KEY_NAME, "updateName", "(Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "updateDescription", "viewState", "updateViewState", "(Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$ViewState;)V", "submitResult", "()V", "tierName", "Ljava/lang/String;", "getTierName", "()Ljava/lang/String;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "benefitType", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "benefit", HookHelper.constructorName, "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;Ljava/lang/String;Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;)V", "Event", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionTierBenefitViewModel extends AppViewModel<ViewState> {
    private final GuildRoleSubscriptionBenefitType benefitType;
    private final PublishSubject<Event> eventSubject;
    private final String tierName;

    /* compiled from: GuildRoleSubscriptionTierBenefitViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$Event;", "", HookHelper.constructorName, "()V", "SendResult", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$Event$SendResult;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: GuildRoleSubscriptionTierBenefitViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$Event$SendResult;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$Event;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "component1", "()Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "benefit", "copy", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;)Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$Event$SendResult;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "getBenefit", HookHelper.constructorName, "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SendResult extends Event {
            private final Benefit benefit;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SendResult(Benefit benefit) {
                super(null);
                m.checkNotNullParameter(benefit, "benefit");
                this.benefit = benefit;
            }

            public static /* synthetic */ SendResult copy$default(SendResult sendResult, Benefit benefit, int i, Object obj) {
                if ((i & 1) != 0) {
                    benefit = sendResult.benefit;
                }
                return sendResult.copy(benefit);
            }

            public final Benefit component1() {
                return this.benefit;
            }

            public final SendResult copy(Benefit benefit) {
                m.checkNotNullParameter(benefit, "benefit");
                return new SendResult(benefit);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SendResult) && m.areEqual(this.benefit, ((SendResult) obj).benefit);
                }
                return true;
            }

            public final Benefit getBenefit() {
                return this.benefit;
            }

            public int hashCode() {
                Benefit benefit = this.benefit;
                if (benefit != null) {
                    return benefit.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("SendResult(benefit=");
                R.append(this.benefit);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildRoleSubscriptionTierBenefitViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b \b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0003\u0010\u0017\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0005\u0012\u0010\b\u0002\u0010\u001a\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u0011¢\u0006\u0004\b/\u00100J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0007J\u0018\u0010\u0012\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013Jh\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u0017\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00052\u0010\b\u0002\u0010\u001a\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u0011HÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001d\u0010\u0007J\u0010\u0010\u001e\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010!\u001a\u00020\u00022\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010#\u001a\u0004\b$\u0010\u000bR!\u0010\u001a\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010%\u001a\u0004\b&\u0010\u0013R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010'\u001a\u0004\b(\u0010\u0007R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b*\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010+\u001a\u0004\b,\u0010\u000eR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010'\u001a\u0004\b-\u0010\u0007R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010'\u001a\u0004\b.\u0010\u0007¨\u00061"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$ViewState;", "", "", "component1", "()Z", "", "component2", "()Ljava/lang/String;", "component3", "", "component4", "()Ljava/lang/Integer;", "Lcom/discord/models/domain/emoji/Emoji;", "component5", "()Lcom/discord/models/domain/emoji/Emoji;", "component6", "", "Lcom/discord/primitives/ChannelId;", "component7", "()Ljava/lang/Long;", "canSubmitResult", ModelAuditLogEntry.CHANGE_KEY_NAME, "tierName", "leadingNameIconResId", "emoji", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "channelId", "copy", "(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;Ljava/lang/Long;)Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitViewModel$ViewState;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getLeadingNameIconResId", "Ljava/lang/Long;", "getChannelId", "Ljava/lang/String;", "getName", "Z", "getCanSubmitResult", "Lcom/discord/models/domain/emoji/Emoji;", "getEmoji", "getDescription", "getTierName", HookHelper.constructorName, "(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;Ljava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean canSubmitResult;
        private final Long channelId;
        private final String description;
        private final Emoji emoji;
        private final Integer leadingNameIconResId;
        private final String name;
        private final String tierName;

        public ViewState() {
            this(false, null, null, null, null, null, null, Opcodes.LAND, null);
        }

        public ViewState(boolean z2, String str, String str2, @DrawableRes Integer num, Emoji emoji, String str3, Long l) {
            this.canSubmitResult = z2;
            this.name = str;
            this.tierName = str2;
            this.leadingNameIconResId = num;
            this.emoji = emoji;
            this.description = str3;
            this.channelId = l;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, boolean z2, String str, String str2, Integer num, Emoji emoji, String str3, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = viewState.canSubmitResult;
            }
            if ((i & 2) != 0) {
                str = viewState.name;
            }
            String str4 = str;
            if ((i & 4) != 0) {
                str2 = viewState.tierName;
            }
            String str5 = str2;
            if ((i & 8) != 0) {
                num = viewState.leadingNameIconResId;
            }
            Integer num2 = num;
            if ((i & 16) != 0) {
                emoji = viewState.emoji;
            }
            Emoji emoji2 = emoji;
            if ((i & 32) != 0) {
                str3 = viewState.description;
            }
            String str6 = str3;
            if ((i & 64) != 0) {
                l = viewState.channelId;
            }
            return viewState.copy(z2, str4, str5, num2, emoji2, str6, l);
        }

        public final boolean component1() {
            return this.canSubmitResult;
        }

        public final String component2() {
            return this.name;
        }

        public final String component3() {
            return this.tierName;
        }

        public final Integer component4() {
            return this.leadingNameIconResId;
        }

        public final Emoji component5() {
            return this.emoji;
        }

        public final String component6() {
            return this.description;
        }

        public final Long component7() {
            return this.channelId;
        }

        public final ViewState copy(boolean z2, String str, String str2, @DrawableRes Integer num, Emoji emoji, String str3, Long l) {
            return new ViewState(z2, str, str2, num, emoji, str3, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return this.canSubmitResult == viewState.canSubmitResult && m.areEqual(this.name, viewState.name) && m.areEqual(this.tierName, viewState.tierName) && m.areEqual(this.leadingNameIconResId, viewState.leadingNameIconResId) && m.areEqual(this.emoji, viewState.emoji) && m.areEqual(this.description, viewState.description) && m.areEqual(this.channelId, viewState.channelId);
        }

        public final boolean getCanSubmitResult() {
            return this.canSubmitResult;
        }

        public final Long getChannelId() {
            return this.channelId;
        }

        public final String getDescription() {
            return this.description;
        }

        public final Emoji getEmoji() {
            return this.emoji;
        }

        public final Integer getLeadingNameIconResId() {
            return this.leadingNameIconResId;
        }

        public final String getName() {
            return this.name;
        }

        public final String getTierName() {
            return this.tierName;
        }

        public int hashCode() {
            boolean z2 = this.canSubmitResult;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            int i3 = i * 31;
            String str = this.name;
            int i4 = 0;
            int hashCode = (i3 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.tierName;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Integer num = this.leadingNameIconResId;
            int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
            Emoji emoji = this.emoji;
            int hashCode4 = (hashCode3 + (emoji != null ? emoji.hashCode() : 0)) * 31;
            String str3 = this.description;
            int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
            Long l = this.channelId;
            if (l != null) {
                i4 = l.hashCode();
            }
            return hashCode5 + i4;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(canSubmitResult=");
            R.append(this.canSubmitResult);
            R.append(", name=");
            R.append(this.name);
            R.append(", tierName=");
            R.append(this.tierName);
            R.append(", leadingNameIconResId=");
            R.append(this.leadingNameIconResId);
            R.append(", emoji=");
            R.append(this.emoji);
            R.append(", description=");
            R.append(this.description);
            R.append(", channelId=");
            return a.F(R, this.channelId, ")");
        }

        public /* synthetic */ ViewState(boolean z2, String str, String str2, Integer num, Emoji emoji, String str3, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : num, (i & 16) != 0 ? null : emoji, (i & 32) != 0 ? null : str3, (i & 64) == 0 ? l : null);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            GuildRoleSubscriptionBenefitType.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType = GuildRoleSubscriptionBenefitType.CHANNEL;
            iArr[guildRoleSubscriptionBenefitType.ordinal()] = 1;
            GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType2 = GuildRoleSubscriptionBenefitType.INTANGIBLE;
            iArr[guildRoleSubscriptionBenefitType2.ordinal()] = 2;
            GuildRoleSubscriptionBenefitType.values();
            int[] iArr2 = new int[3];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[guildRoleSubscriptionBenefitType.ordinal()] = 1;
            iArr2[guildRoleSubscriptionBenefitType2.ordinal()] = 2;
        }
    }

    public /* synthetic */ GuildRoleSubscriptionTierBenefitViewModel(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, String str, Benefit benefit, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(guildRoleSubscriptionBenefitType, str, (i & 4) != 0 ? null : benefit);
    }

    public final String getTierName() {
        return this.tierName;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x006d  */
    /* JADX WARN: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void submitResult() {
        /*
            r10 = this;
            java.lang.Object r0 = r10.requireViewState()
            com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$ViewState r0 = (com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel.ViewState) r0
            com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType r1 = r10.benefitType
            int r1 = r1.ordinal()
            r2 = 1
            java.lang.String r3 = "Required value was null."
            if (r1 == r2) goto L41
            r2 = 2
            if (r1 == r2) goto L16
            r0 = 0
            goto L6b
        L16:
            com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$IntangibleBenefit r1 = new com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$IntangibleBenefit
            java.lang.String r2 = r0.getName()
            if (r2 == 0) goto L37
            com.discord.models.domain.emoji.Emoji r4 = r0.getEmoji()
            if (r4 == 0) goto L2d
            java.lang.String r0 = r0.getDescription()
            r1.<init>(r2, r4, r0)
        L2b:
            r0 = r1
            goto L6b
        L2d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = r3.toString()
            r0.<init>(r1)
            throw r0
        L37:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = r3.toString()
            r0.<init>(r1)
            throw r0
        L41:
            com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$ChannelBenefit r1 = new com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$ChannelBenefit
            java.lang.String r4 = r0.getName()
            if (r4 == 0) goto L8e
            com.discord.models.domain.emoji.Emoji r5 = r0.getEmoji()
            if (r5 == 0) goto L84
            java.lang.String r6 = r0.getDescription()
            java.lang.Integer r7 = r0.getLeadingNameIconResId()
            java.lang.Long r0 = r0.getChannelId()
            if (r0 == 0) goto L7a
            long r8 = r0.longValue()
            r2 = r1
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r2.<init>(r3, r4, r5, r6, r7)
            goto L2b
        L6b:
            if (r0 == 0) goto L79
            rx.subjects.PublishSubject<com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$Event> r1 = r10.eventSubject
            com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$Event$SendResult r2 = new com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$Event$SendResult
            r2.<init>(r0)
            rx.subjects.PublishSubject$b<T> r0 = r1.k
            r0.onNext(r2)
        L79:
            return
        L7a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = r3.toString()
            r0.<init>(r1)
            throw r0
        L84:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = r3.toString()
            r0.<init>(r1)
            throw r0
        L8e:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = r3.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel.submitResult():void");
    }

    public final void updateChannel(long j, String str, Integer num) {
        m.checkNotNullParameter(str, "channelName");
        updateViewState(ViewState.copy$default(requireViewState(), false, str, null, num, null, null, Long.valueOf(j), 53, null));
    }

    public final void updateDescription(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        updateViewState(ViewState.copy$default(requireViewState(), false, null, null, null, null, str, null, 95, null));
    }

    public final void updateEmoji(Emoji emoji) {
        updateViewState(ViewState.copy$default(requireViewState(), false, null, null, null, emoji, null, null, 111, null));
    }

    public final void updateName(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        updateViewState(ViewState.copy$default(requireViewState(), false, str, null, null, null, null, null, Opcodes.LUSHR, null));
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public GuildRoleSubscriptionTierBenefitViewModel(com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType r10, java.lang.String r11, com.discord.widgets.guild_role_subscriptions.tier.model.Benefit r12) {
        /*
            r9 = this;
            java.lang.String r0 = "benefitType"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$ViewState r0 = new com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$ViewState
            if (r12 == 0) goto Lc
            r1 = 1
            r2 = 1
            goto Le
        Lc:
            r1 = 0
            r2 = 0
        Le:
            r1 = 0
            if (r12 == 0) goto L16
            java.lang.String r3 = r12.getName()
            goto L17
        L16:
            r3 = r1
        L17:
            boolean r4 = r12 instanceof com.discord.widgets.guild_role_subscriptions.tier.model.Benefit.ChannelBenefit
            if (r4 != 0) goto L1d
            r5 = r1
            goto L1e
        L1d:
            r5 = r12
        L1e:
            com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$ChannelBenefit r5 = (com.discord.widgets.guild_role_subscriptions.tier.model.Benefit.ChannelBenefit) r5
            if (r5 == 0) goto L2c
            long r5 = r5.getChannelId()
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r8 = r5
            goto L2d
        L2c:
            r8 = r1
        L2d:
            if (r4 != 0) goto L31
            r4 = r1
            goto L32
        L31:
            r4 = r12
        L32:
            com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$ChannelBenefit r4 = (com.discord.widgets.guild_role_subscriptions.tier.model.Benefit.ChannelBenefit) r4
            if (r4 == 0) goto L3c
            java.lang.Integer r4 = r4.getChannelIconResId()
            r5 = r4
            goto L3d
        L3c:
            r5 = r1
        L3d:
            if (r12 == 0) goto L45
            com.discord.models.domain.emoji.Emoji r4 = r12.getEmoji()
            r6 = r4
            goto L46
        L45:
            r6 = r1
        L46:
            if (r12 == 0) goto L4e
            java.lang.String r12 = r12.getDescription()
            r7 = r12
            goto L4f
        L4e:
            r7 = r1
        L4f:
            r1 = r0
            r4 = r11
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            r9.<init>(r0)
            r9.benefitType = r10
            r9.tierName = r11
            rx.subjects.PublishSubject r10 = rx.subjects.PublishSubject.k0()
            r9.eventSubject = r10
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel.<init>(com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType, java.lang.String, com.discord.widgets.guild_role_subscriptions.tier.model.Benefit):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x002a, code lost:
        if (r14.getEmoji() != null) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x002c, code lost:
        r4 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x004a, code lost:
        if (r14.getChannelId() != null) goto L16;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void updateViewState(com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel.ViewState r14) {
        /*
            r13 = this;
            java.lang.String r0 = "viewState"
            d0.z.d.m.checkNotNullParameter(r14, r0)
            com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType r0 = r13.benefitType
            int r0 = r0.ordinal()
            r1 = 0
            r2 = 1
            if (r0 == r2) goto L2e
            r3 = 2
            if (r0 == r3) goto L14
        L12:
            r4 = 0
            goto L4d
        L14:
            java.lang.String r0 = r14.getName()
            if (r0 == 0) goto L23
            boolean r0 = d0.g0.t.isBlank(r0)
            if (r0 == 0) goto L21
            goto L23
        L21:
            r0 = 0
            goto L24
        L23:
            r0 = 1
        L24:
            if (r0 != 0) goto L12
            com.discord.models.domain.emoji.Emoji r0 = r14.getEmoji()
            if (r0 == 0) goto L12
        L2c:
            r4 = 1
            goto L4d
        L2e:
            java.lang.String r0 = r14.getName()
            if (r0 == 0) goto L3d
            boolean r0 = d0.g0.t.isBlank(r0)
            if (r0 == 0) goto L3b
            goto L3d
        L3b:
            r0 = 0
            goto L3e
        L3d:
            r0 = 1
        L3e:
            if (r0 != 0) goto L12
            com.discord.models.domain.emoji.Emoji r0 = r14.getEmoji()
            if (r0 == 0) goto L12
            java.lang.Long r0 = r14.getChannelId()
            if (r0 == 0) goto L12
            goto L2c
        L4d:
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 126(0x7e, float:1.77E-43)
            r12 = 0
            r3 = r14
            com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$ViewState r14 = com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel.ViewState.copy$default(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            super.updateViewState(r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel.updateViewState(com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionTierBenefitViewModel$ViewState):void");
    }
}
