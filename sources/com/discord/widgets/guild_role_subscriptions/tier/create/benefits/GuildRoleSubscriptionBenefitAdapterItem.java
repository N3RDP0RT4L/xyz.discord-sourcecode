package com.discord.widgets.guild_role_subscriptions.tier.create.benefits;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.recycler.DiffKeyProvider;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildRoleSubscriptionBenefitAdapterItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem;", "Lcom/discord/utilities/recycler/DiffKeyProvider;", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "type", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "getType", "()Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", HookHelper.constructorName, "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;)V", "AddBenefitItem", "BenefitItem", Traits.Location.Section.HEADER, "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$BenefitItem;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$Header;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$AddBenefitItem;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildRoleSubscriptionBenefitAdapterItem implements DiffKeyProvider {
    private final GuildRoleSubscriptionBenefitType type;

    /* compiled from: GuildRoleSubscriptionBenefitAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\n\u0010\r\u001a\u00060\u0005j\u0002`\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b\"\u0010#J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ4\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\f\b\u0002\u0010\r\u001a\u00060\u0005j\u0002`\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0011\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001d\u0010\r\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\bR\u001c\u0010\u001c\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u000bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001f\u0010\u000bR\u001c\u0010\f\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\f\u0010 \u001a\u0004\b!\u0010\u0004¨\u0006$"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$AddBenefitItem;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem;", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "component1", "()Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "", "Lcom/discord/primitives/GuildId;", "component2", "()J", "", "component3", "()Ljava/lang/String;", "type", "guildId", "tierName", "copy", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;JLjava/lang/String;)Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$AddBenefitItem;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "key", "Ljava/lang/String;", "getKey", "getTierName", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "getType", HookHelper.constructorName, "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;JLjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AddBenefitItem extends GuildRoleSubscriptionBenefitAdapterItem {
        private final long guildId;
        private final String key;
        private final String tierName;
        private final GuildRoleSubscriptionBenefitType type;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AddBenefitItem(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, long j, String str) {
            super(guildRoleSubscriptionBenefitType, null);
            m.checkNotNullParameter(guildRoleSubscriptionBenefitType, "type");
            this.type = guildRoleSubscriptionBenefitType;
            this.guildId = j;
            this.tierName = str;
            StringBuilder R = a.R("AddBenefitItem");
            R.append(getType());
            this.key = R.toString();
        }

        public static /* synthetic */ AddBenefitItem copy$default(AddBenefitItem addBenefitItem, GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, long j, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionBenefitType = addBenefitItem.getType();
            }
            if ((i & 2) != 0) {
                j = addBenefitItem.guildId;
            }
            if ((i & 4) != 0) {
                str = addBenefitItem.tierName;
            }
            return addBenefitItem.copy(guildRoleSubscriptionBenefitType, j, str);
        }

        public final GuildRoleSubscriptionBenefitType component1() {
            return getType();
        }

        public final long component2() {
            return this.guildId;
        }

        public final String component3() {
            return this.tierName;
        }

        public final AddBenefitItem copy(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, long j, String str) {
            m.checkNotNullParameter(guildRoleSubscriptionBenefitType, "type");
            return new AddBenefitItem(guildRoleSubscriptionBenefitType, j, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AddBenefitItem)) {
                return false;
            }
            AddBenefitItem addBenefitItem = (AddBenefitItem) obj;
            return m.areEqual(getType(), addBenefitItem.getType()) && this.guildId == addBenefitItem.guildId && m.areEqual(this.tierName, addBenefitItem.tierName);
        }

        public final long getGuildId() {
            return this.guildId;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final String getTierName() {
            return this.tierName;
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionBenefitAdapterItem
        public GuildRoleSubscriptionBenefitType getType() {
            return this.type;
        }

        public int hashCode() {
            GuildRoleSubscriptionBenefitType type = getType();
            int i = 0;
            int a = (b.a(this.guildId) + ((type != null ? type.hashCode() : 0) * 31)) * 31;
            String str = this.tierName;
            if (str != null) {
                i = str.hashCode();
            }
            return a + i;
        }

        public String toString() {
            StringBuilder R = a.R("AddBenefitItem(type=");
            R.append(getType());
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", tierName=");
            return a.H(R, this.tierName, ")");
        }
    }

    /* compiled from: GuildRoleSubscriptionBenefitAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\n\u0010\u0011\u001a\u00060\bj\u0002`\t\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b(\u0010)J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0014\u0010\n\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ>\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\f\b\u0002\u0010\u0011\u001a\u00060\bj\u0002`\t2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\u0015\u0010\u000eJ\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u001f\u0010\u000eR\u001c\u0010\u000f\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\u0007R\u001d\u0010\u0011\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b%\u0010\u000bR\u001c\u0010&\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010\u001e\u001a\u0004\b'\u0010\u000e¨\u0006*"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$BenefitItem;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem;", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "component1", "()Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "component2", "()Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "", "Lcom/discord/primitives/GuildId;", "component3", "()J", "", "component4", "()Ljava/lang/String;", "type", "benefit", "guildId", "tierName", "copy", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;JLjava/lang/String;)Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$BenefitItem;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getTierName", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "getType", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "getBenefit", "J", "getGuildId", "key", "getKey", HookHelper.constructorName, "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;JLjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BenefitItem extends GuildRoleSubscriptionBenefitAdapterItem {
        private final Benefit benefit;
        private final long guildId;
        private final String key;
        private final String tierName;
        private final GuildRoleSubscriptionBenefitType type;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public BenefitItem(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, Benefit benefit, long j, String str) {
            super(guildRoleSubscriptionBenefitType, null);
            m.checkNotNullParameter(guildRoleSubscriptionBenefitType, "type");
            m.checkNotNullParameter(benefit, "benefit");
            this.type = guildRoleSubscriptionBenefitType;
            this.benefit = benefit;
            this.guildId = j;
            this.tierName = str;
            this.key = String.valueOf(benefit.hashCode());
        }

        public static /* synthetic */ BenefitItem copy$default(BenefitItem benefitItem, GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, Benefit benefit, long j, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionBenefitType = benefitItem.getType();
            }
            if ((i & 2) != 0) {
                benefit = benefitItem.benefit;
            }
            Benefit benefit2 = benefit;
            if ((i & 4) != 0) {
                j = benefitItem.guildId;
            }
            long j2 = j;
            if ((i & 8) != 0) {
                str = benefitItem.tierName;
            }
            return benefitItem.copy(guildRoleSubscriptionBenefitType, benefit2, j2, str);
        }

        public final GuildRoleSubscriptionBenefitType component1() {
            return getType();
        }

        public final Benefit component2() {
            return this.benefit;
        }

        public final long component3() {
            return this.guildId;
        }

        public final String component4() {
            return this.tierName;
        }

        public final BenefitItem copy(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, Benefit benefit, long j, String str) {
            m.checkNotNullParameter(guildRoleSubscriptionBenefitType, "type");
            m.checkNotNullParameter(benefit, "benefit");
            return new BenefitItem(guildRoleSubscriptionBenefitType, benefit, j, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BenefitItem)) {
                return false;
            }
            BenefitItem benefitItem = (BenefitItem) obj;
            return m.areEqual(getType(), benefitItem.getType()) && m.areEqual(this.benefit, benefitItem.benefit) && this.guildId == benefitItem.guildId && m.areEqual(this.tierName, benefitItem.tierName);
        }

        public final Benefit getBenefit() {
            return this.benefit;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final String getTierName() {
            return this.tierName;
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionBenefitAdapterItem
        public GuildRoleSubscriptionBenefitType getType() {
            return this.type;
        }

        public int hashCode() {
            GuildRoleSubscriptionBenefitType type = getType();
            int i = 0;
            int hashCode = (type != null ? type.hashCode() : 0) * 31;
            Benefit benefit = this.benefit;
            int a = (b.a(this.guildId) + ((hashCode + (benefit != null ? benefit.hashCode() : 0)) * 31)) * 31;
            String str = this.tierName;
            if (str != null) {
                i = str.hashCode();
            }
            return a + i;
        }

        public String toString() {
            StringBuilder R = a.R("BenefitItem(type=");
            R.append(getType());
            R.append(", benefit=");
            R.append(this.benefit);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", tierName=");
            return a.H(R, this.tierName, ")");
        }
    }

    /* compiled from: GuildRoleSubscriptionBenefitAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0005\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001c\u0010\u0015\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\n¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$Header;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem;", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "component1", "()Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "type", "copy", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;)Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionBenefitAdapterItem$Header;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "getType", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Header extends GuildRoleSubscriptionBenefitAdapterItem {
        private final String key;
        private final GuildRoleSubscriptionBenefitType type;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Header(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType) {
            super(guildRoleSubscriptionBenefitType, null);
            m.checkNotNullParameter(guildRoleSubscriptionBenefitType, "type");
            this.type = guildRoleSubscriptionBenefitType;
            StringBuilder R = a.R("BenefitHeader");
            R.append(getType());
            this.key = R.toString();
        }

        public static /* synthetic */ Header copy$default(Header header, GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionBenefitType = header.getType();
            }
            return header.copy(guildRoleSubscriptionBenefitType);
        }

        public final GuildRoleSubscriptionBenefitType component1() {
            return getType();
        }

        public final Header copy(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType) {
            m.checkNotNullParameter(guildRoleSubscriptionBenefitType, "type");
            return new Header(guildRoleSubscriptionBenefitType);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Header) && m.areEqual(getType(), ((Header) obj).getType());
            }
            return true;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.widgets.guild_role_subscriptions.tier.create.benefits.GuildRoleSubscriptionBenefitAdapterItem
        public GuildRoleSubscriptionBenefitType getType() {
            return this.type;
        }

        public int hashCode() {
            GuildRoleSubscriptionBenefitType type = getType();
            if (type != null) {
                return type.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("Header(type=");
            R.append(getType());
            R.append(")");
            return R.toString();
        }
    }

    private GuildRoleSubscriptionBenefitAdapterItem(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType) {
        this.type = guildRoleSubscriptionBenefitType;
    }

    public GuildRoleSubscriptionBenefitType getType() {
        return this.type;
    }

    public /* synthetic */ GuildRoleSubscriptionBenefitAdapterItem(GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, DefaultConstructorMarker defaultConstructorMarker) {
        this(guildRoleSubscriptionBenefitType);
    }
}
