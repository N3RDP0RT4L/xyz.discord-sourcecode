package com.discord.widgets.guild_role_subscriptions.tier.create.benefits;

import android.content.Context;
import androidx.activity.result.ActivityResultLauncher;
import com.discord.widgets.channels.WidgetCreateChannel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildRoleSubscriptionTierBenefit.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildRoleSubscriptionTierBenefit$configureUIForChannelBenefit$2 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetGuildRoleSubscriptionTierBenefit this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildRoleSubscriptionTierBenefit$configureUIForChannelBenefit$2(WidgetGuildRoleSubscriptionTierBenefit widgetGuildRoleSubscriptionTierBenefit) {
        super(0);
        this.this$0 = widgetGuildRoleSubscriptionTierBenefit;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ActivityResultLauncher activityResultLauncher;
        long guildId;
        WidgetCreateChannel.Companion companion = WidgetCreateChannel.Companion;
        Context requireContext = this.this$0.requireContext();
        activityResultLauncher = this.this$0.createChannelLauncher;
        guildId = this.this$0.getGuildId();
        companion.launch(requireContext, activityResultLauncher, guildId, (r25 & 8) != 0 ? 0 : 0, (r25 & 16) != 0 ? null : null, (r25 & 32) != 0 ? false : true, (r25 & 64) != 0 ? false : true, (r25 & 128) != 0 ? false : true, (r25 & 256) != 0 ? false : true);
    }
}
