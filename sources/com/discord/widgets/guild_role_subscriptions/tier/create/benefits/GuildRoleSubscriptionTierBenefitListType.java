package com.discord.widgets.guild_role_subscriptions.tier.create.benefits;

import andhook.lib.HookHelper;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import kotlin.Metadata;
/* compiled from: GuildRoleSubscriptionTierBenefitListType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/benefits/GuildRoleSubscriptionTierBenefitListType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", AutocompleteSelectionTypes.CHANNEL, "INTANGIBLE", "ALL", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public enum GuildRoleSubscriptionTierBenefitListType {
    CHANNEL,
    INTANGIBLE,
    ALL
}
