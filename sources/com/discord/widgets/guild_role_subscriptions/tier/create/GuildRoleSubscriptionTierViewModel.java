package com.discord.widgets.guild_role_subscriptions.tier.create;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildRoleSubscriptionTierViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001<B\u0007¢\u0006\u0004\b:\u0010;J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\n\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\f\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\f\u0010\u0007J\u0017\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0015\u0010\u0010J\u0015\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001b\u001a\u00020\u00052\u0006\u0010\u001a\u001a\u00020\u0016¢\u0006\u0004\b\u001b\u0010\u0019J\u0019\u0010\u001d\u001a\u00020\u00052\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0016¢\u0006\u0004\b\u001d\u0010\u0019J\u0015\u0010 \u001a\u00020\u00052\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b \u0010!J\u0019\u0010#\u001a\u00020\u00052\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u0016¢\u0006\u0004\b#\u0010\u0019J\u0017\u0010%\u001a\u00020\u00052\b\u0010$\u001a\u0004\u0018\u00010\u001e¢\u0006\u0004\b%\u0010&J!\u0010'\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00032\b\u0010\t\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b'\u0010\u000bJ\u0015\u0010*\u001a\u00020\u00052\u0006\u0010)\u001a\u00020(¢\u0006\u0004\b*\u0010+J\u0015\u0010.\u001a\u00020\u00052\u0006\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010/J\u0015\u00101\u001a\u00020\u00052\u0006\u00100\u001a\u00020\u001e¢\u0006\u0004\b1\u0010!J!\u00105\u001a\u00020\u00052\b\u00103\u001a\u0004\u0018\u0001022\b\u00104\u001a\u0004\u0018\u000102¢\u0006\u0004\b5\u00106J!\u00107\u001a\u00020\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\r2\b\u0010\u0012\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b7\u0010\u0014J\u0015\u00109\u001a\u00020\u00052\u0006\u00108\u001a\u00020,¢\u0006\u0004\b9\u0010/¨\u0006="}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionTierViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionTierViewModel$ViewState;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$IntangibleBenefit;", "intangibleBenefit", "", "addIntangibleBenefit", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$IntangibleBenefit;)V", "existingIntangibleBenefit", "newIntangibleBenefit", "replaceIntangibleBenefit", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$IntangibleBenefit;Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$IntangibleBenefit;)V", "removeIntangibleBenefit", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$ChannelBenefit;", "channelBenefit", "addChannelBenefit", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$ChannelBenefit;)V", "existingChannelBenefit", "newChannelBenefit", "replaceChannelBenefit", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$ChannelBenefit;Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$ChannelBenefit;)V", "removeChannelBenefit", "", "tierName", "updateTierName", "(Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "updateDescription", "image", "updateImage", "", "priceTier", "updatePriceTier", "(I)V", "memberIcon", "updateMemberIcon", "memberColor", "updateMemberColor", "(Ljava/lang/Integer;)V", "updateIntangibleBenefit", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "guildRoleSubscriptionTier", "setTier", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;)V", "", "isFullServerGating", "setFullServerGating", "(Z)V", "defaultMemberColor", "setDefaultMemberColor", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "existingBenefit", "newBenefit", "updateBenefit", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;)V", "updateChannelBenefit", "canAccessAllChannels", "setCanAccessAllChannels", HookHelper.constructorName, "()V", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionTierViewModel extends AppViewModel<ViewState> {

    /* compiled from: GuildRoleSubscriptionTierViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\b\b\u0002\u0010\b\u001a\u00020\u0002\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\t\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionTierViewModel$ViewState;", "", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "component1", "()Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "", "component2", "()Ljava/lang/Boolean;", "guildRoleSubscriptionTier", "isFullServerGating", "copy", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;Ljava/lang/Boolean;)Lcom/discord/widgets/guild_role_subscriptions/tier/create/GuildRoleSubscriptionTierViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Boolean;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "getGuildRoleSubscriptionTier", HookHelper.constructorName, "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;Ljava/lang/Boolean;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final GuildRoleSubscriptionTier guildRoleSubscriptionTier;
        private final Boolean isFullServerGating;

        public ViewState() {
            this(null, null, 3, null);
        }

        public ViewState(GuildRoleSubscriptionTier guildRoleSubscriptionTier, Boolean bool) {
            m.checkNotNullParameter(guildRoleSubscriptionTier, "guildRoleSubscriptionTier");
            this.guildRoleSubscriptionTier = guildRoleSubscriptionTier;
            this.isFullServerGating = bool;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, GuildRoleSubscriptionTier guildRoleSubscriptionTier, Boolean bool, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionTier = viewState.guildRoleSubscriptionTier;
            }
            if ((i & 2) != 0) {
                bool = viewState.isFullServerGating;
            }
            return viewState.copy(guildRoleSubscriptionTier, bool);
        }

        public final GuildRoleSubscriptionTier component1() {
            return this.guildRoleSubscriptionTier;
        }

        public final Boolean component2() {
            return this.isFullServerGating;
        }

        public final ViewState copy(GuildRoleSubscriptionTier guildRoleSubscriptionTier, Boolean bool) {
            m.checkNotNullParameter(guildRoleSubscriptionTier, "guildRoleSubscriptionTier");
            return new ViewState(guildRoleSubscriptionTier, bool);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.guildRoleSubscriptionTier, viewState.guildRoleSubscriptionTier) && m.areEqual(this.isFullServerGating, viewState.isFullServerGating);
        }

        public final GuildRoleSubscriptionTier getGuildRoleSubscriptionTier() {
            return this.guildRoleSubscriptionTier;
        }

        public int hashCode() {
            GuildRoleSubscriptionTier guildRoleSubscriptionTier = this.guildRoleSubscriptionTier;
            int i = 0;
            int hashCode = (guildRoleSubscriptionTier != null ? guildRoleSubscriptionTier.hashCode() : 0) * 31;
            Boolean bool = this.isFullServerGating;
            if (bool != null) {
                i = bool.hashCode();
            }
            return hashCode + i;
        }

        public final Boolean isFullServerGating() {
            return this.isFullServerGating;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(guildRoleSubscriptionTier=");
            R.append(this.guildRoleSubscriptionTier);
            R.append(", isFullServerGating=");
            return a.C(R, this.isFullServerGating, ")");
        }

        public /* synthetic */ ViewState(GuildRoleSubscriptionTier guildRoleSubscriptionTier, Boolean bool, int i, DefaultConstructorMarker defaultConstructorMarker) {
            Boolean bool2;
            ViewState viewState;
            GuildRoleSubscriptionTier guildRoleSubscriptionTier2 = (i & 1) != 0 ? new GuildRoleSubscriptionTier(null, null, null, null, null, null, null, null, null, null, null, null, null, 8191, null) : guildRoleSubscriptionTier;
            if ((i & 2) != 0) {
                bool2 = null;
                viewState = this;
            } else {
                viewState = this;
                bool2 = bool;
            }
            new ViewState(guildRoleSubscriptionTier2, bool2);
        }
    }

    public GuildRoleSubscriptionTierViewModel() {
        super(new ViewState(null, null, 3, null));
    }

    private final void addChannelBenefit(Benefit.ChannelBenefit channelBenefit) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : null, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : null, (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : u.plus((Collection<? extends Benefit.ChannelBenefit>) guildRoleSubscriptionTier.getChannelBenefits(), channelBenefit), (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    private final void addIntangibleBenefit(Benefit.IntangibleBenefit intangibleBenefit) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : null, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : null, (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : null, (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : u.plus((Collection<? extends Benefit.IntangibleBenefit>) guildRoleSubscriptionTier.getIntangibleBenefits(), intangibleBenefit));
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    private final void removeChannelBenefit(Benefit.ChannelBenefit channelBenefit) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : null, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : null, (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : u.minus(guildRoleSubscriptionTier.getChannelBenefits(), channelBenefit), (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    private final void removeIntangibleBenefit(Benefit.IntangibleBenefit intangibleBenefit) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : null, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : null, (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : null, (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : u.minus(guildRoleSubscriptionTier.getIntangibleBenefits(), intangibleBenefit));
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    private final void replaceChannelBenefit(Benefit.ChannelBenefit channelBenefit, Benefit.ChannelBenefit channelBenefit2) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        List mutableList = u.toMutableList((Collection) guildRoleSubscriptionTier.getChannelBenefits());
        int indexOf = mutableList.indexOf(channelBenefit);
        if (indexOf != -1) {
            mutableList.set(indexOf, channelBenefit2);
            copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : null, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : null, (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : mutableList, (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : null);
            updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
        }
    }

    private final void replaceIntangibleBenefit(Benefit.IntangibleBenefit intangibleBenefit, Benefit.IntangibleBenefit intangibleBenefit2) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        List mutableList = u.toMutableList((Collection) guildRoleSubscriptionTier.getIntangibleBenefits());
        int indexOf = mutableList.indexOf(intangibleBenefit);
        if (indexOf != -1) {
            mutableList.set(indexOf, intangibleBenefit2);
            copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : null, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : null, (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : null, (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : mutableList);
            updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
        }
    }

    public static /* synthetic */ void updateImage$default(GuildRoleSubscriptionTierViewModel guildRoleSubscriptionTierViewModel, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        guildRoleSubscriptionTierViewModel.updateImage(str);
    }

    public static /* synthetic */ void updateMemberIcon$default(GuildRoleSubscriptionTierViewModel guildRoleSubscriptionTierViewModel, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        guildRoleSubscriptionTierViewModel.updateMemberIcon(str);
    }

    public final void setCanAccessAllChannels(boolean z2) {
        GuildRoleSubscriptionTier copy;
        copy = r1.copy((r28 & 1) != 0 ? r1.name : null, (r28 & 2) != 0 ? r1.applicationId : null, (r28 & 4) != 0 ? r1.priceTier : null, (r28 & 8) != 0 ? r1.memberColor : null, (r28 & 16) != 0 ? r1.memberIcon : null, (r28 & 32) != 0 ? r1.guildRole : null, (r28 & 64) != 0 ? r1.image : null, (r28 & 128) != 0 ? r1.imageAssetId : null, (r28 & 256) != 0 ? r1.description : null, (r28 & 512) != 0 ? r1.isPublished : null, (r28 & 1024) != 0 ? r1.canAccessAllChannels : Boolean.valueOf(z2), (r28 & 2048) != 0 ? r1.channelBenefits : null, (r28 & 4096) != 0 ? requireViewState().getGuildRoleSubscriptionTier().intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    public final void setDefaultMemberColor(int i) {
        if (requireViewState().getGuildRoleSubscriptionTier().getMemberColor() == null) {
            updateMemberColor(Integer.valueOf(i));
        }
    }

    public final void setFullServerGating(boolean z2) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        Boolean valueOf = Boolean.valueOf(z2);
        copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : null, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : Boolean.valueOf(guildRoleSubscriptionTier.canAccessAllChannelsOrDefault(Boolean.valueOf(z2))), (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : null, (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : null);
        updateViewState(requireViewState().copy(copy, valueOf));
    }

    public final void setTier(GuildRoleSubscriptionTier guildRoleSubscriptionTier) {
        m.checkNotNullParameter(guildRoleSubscriptionTier, "guildRoleSubscriptionTier");
        updateViewState(ViewState.copy$default(requireViewState(), guildRoleSubscriptionTier, null, 2, null));
    }

    public final void updateBenefit(Benefit benefit, Benefit benefit2) {
        boolean z2 = benefit instanceof Benefit.ChannelBenefit;
        if (z2 || (benefit2 instanceof Benefit.ChannelBenefit)) {
            if (!z2) {
                benefit = null;
            }
            Benefit.ChannelBenefit channelBenefit = (Benefit.ChannelBenefit) benefit;
            if (!(benefit2 instanceof Benefit.ChannelBenefit)) {
                benefit2 = null;
            }
            updateChannelBenefit(channelBenefit, (Benefit.ChannelBenefit) benefit2);
            return;
        }
        if (!(benefit instanceof Benefit.IntangibleBenefit)) {
            benefit = null;
        }
        Benefit.IntangibleBenefit intangibleBenefit = (Benefit.IntangibleBenefit) benefit;
        if (!(benefit2 instanceof Benefit.IntangibleBenefit)) {
            benefit2 = null;
        }
        updateIntangibleBenefit(intangibleBenefit, (Benefit.IntangibleBenefit) benefit2);
    }

    public final void updateChannelBenefit(Benefit.ChannelBenefit channelBenefit, Benefit.ChannelBenefit channelBenefit2) {
        if (channelBenefit == null && channelBenefit2 != null) {
            addChannelBenefit(channelBenefit2);
        } else if (channelBenefit != null && channelBenefit2 == null) {
            removeChannelBenefit(channelBenefit);
        } else if (channelBenefit != null && channelBenefit2 != null) {
            replaceChannelBenefit(channelBenefit, channelBenefit2);
        }
    }

    public final void updateDescription(String str) {
        GuildRoleSubscriptionTier copy;
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        copy = r1.copy((r28 & 1) != 0 ? r1.name : null, (r28 & 2) != 0 ? r1.applicationId : null, (r28 & 4) != 0 ? r1.priceTier : null, (r28 & 8) != 0 ? r1.memberColor : null, (r28 & 16) != 0 ? r1.memberIcon : null, (r28 & 32) != 0 ? r1.guildRole : null, (r28 & 64) != 0 ? r1.image : null, (r28 & 128) != 0 ? r1.imageAssetId : null, (r28 & 256) != 0 ? r1.description : str, (r28 & 512) != 0 ? r1.isPublished : null, (r28 & 1024) != 0 ? r1.canAccessAllChannels : null, (r28 & 2048) != 0 ? r1.channelBenefits : null, (r28 & 4096) != 0 ? requireViewState().getGuildRoleSubscriptionTier().intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    public final void updateImage(String str) {
        GuildRoleSubscriptionTier copy;
        GuildRoleSubscriptionTier guildRoleSubscriptionTier = requireViewState().getGuildRoleSubscriptionTier();
        requireViewState();
        copy = guildRoleSubscriptionTier.copy((r28 & 1) != 0 ? guildRoleSubscriptionTier.name : null, (r28 & 2) != 0 ? guildRoleSubscriptionTier.applicationId : null, (r28 & 4) != 0 ? guildRoleSubscriptionTier.priceTier : null, (r28 & 8) != 0 ? guildRoleSubscriptionTier.memberColor : null, (r28 & 16) != 0 ? guildRoleSubscriptionTier.memberIcon : null, (r28 & 32) != 0 ? guildRoleSubscriptionTier.guildRole : null, (r28 & 64) != 0 ? guildRoleSubscriptionTier.image : str, (r28 & 128) != 0 ? guildRoleSubscriptionTier.imageAssetId : null, (r28 & 256) != 0 ? guildRoleSubscriptionTier.description : null, (r28 & 512) != 0 ? guildRoleSubscriptionTier.isPublished : null, (r28 & 1024) != 0 ? guildRoleSubscriptionTier.canAccessAllChannels : null, (r28 & 2048) != 0 ? guildRoleSubscriptionTier.channelBenefits : null, (r28 & 4096) != 0 ? guildRoleSubscriptionTier.intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    public final void updateIntangibleBenefit(Benefit.IntangibleBenefit intangibleBenefit, Benefit.IntangibleBenefit intangibleBenefit2) {
        if (intangibleBenefit == null && intangibleBenefit2 != null) {
            addIntangibleBenefit(intangibleBenefit2);
        } else if (intangibleBenefit != null && intangibleBenefit2 == null) {
            removeIntangibleBenefit(intangibleBenefit);
        } else if (intangibleBenefit != null && intangibleBenefit2 != null) {
            replaceIntangibleBenefit(intangibleBenefit, intangibleBenefit2);
        }
    }

    public final void updateMemberColor(Integer num) {
        GuildRoleSubscriptionTier copy;
        copy = r1.copy((r28 & 1) != 0 ? r1.name : null, (r28 & 2) != 0 ? r1.applicationId : null, (r28 & 4) != 0 ? r1.priceTier : null, (r28 & 8) != 0 ? r1.memberColor : num, (r28 & 16) != 0 ? r1.memberIcon : null, (r28 & 32) != 0 ? r1.guildRole : null, (r28 & 64) != 0 ? r1.image : null, (r28 & 128) != 0 ? r1.imageAssetId : null, (r28 & 256) != 0 ? r1.description : null, (r28 & 512) != 0 ? r1.isPublished : null, (r28 & 1024) != 0 ? r1.canAccessAllChannels : null, (r28 & 2048) != 0 ? r1.channelBenefits : null, (r28 & 4096) != 0 ? requireViewState().getGuildRoleSubscriptionTier().intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    public final void updateMemberIcon(String str) {
        GuildRoleSubscriptionTier copy;
        copy = r1.copy((r28 & 1) != 0 ? r1.name : null, (r28 & 2) != 0 ? r1.applicationId : null, (r28 & 4) != 0 ? r1.priceTier : null, (r28 & 8) != 0 ? r1.memberColor : null, (r28 & 16) != 0 ? r1.memberIcon : str, (r28 & 32) != 0 ? r1.guildRole : null, (r28 & 64) != 0 ? r1.image : null, (r28 & 128) != 0 ? r1.imageAssetId : null, (r28 & 256) != 0 ? r1.description : null, (r28 & 512) != 0 ? r1.isPublished : null, (r28 & 1024) != 0 ? r1.canAccessAllChannels : null, (r28 & 2048) != 0 ? r1.channelBenefits : null, (r28 & 4096) != 0 ? requireViewState().getGuildRoleSubscriptionTier().intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    public final void updatePriceTier(int i) {
        GuildRoleSubscriptionTier copy;
        copy = r1.copy((r28 & 1) != 0 ? r1.name : null, (r28 & 2) != 0 ? r1.applicationId : null, (r28 & 4) != 0 ? r1.priceTier : Integer.valueOf(i), (r28 & 8) != 0 ? r1.memberColor : null, (r28 & 16) != 0 ? r1.memberIcon : null, (r28 & 32) != 0 ? r1.guildRole : null, (r28 & 64) != 0 ? r1.image : null, (r28 & 128) != 0 ? r1.imageAssetId : null, (r28 & 256) != 0 ? r1.description : null, (r28 & 512) != 0 ? r1.isPublished : null, (r28 & 1024) != 0 ? r1.canAccessAllChannels : null, (r28 & 2048) != 0 ? r1.channelBenefits : null, (r28 & 4096) != 0 ? requireViewState().getGuildRoleSubscriptionTier().intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }

    public final void updateTierName(String str) {
        GuildRoleSubscriptionTier copy;
        m.checkNotNullParameter(str, "tierName");
        copy = r1.copy((r28 & 1) != 0 ? r1.name : str, (r28 & 2) != 0 ? r1.applicationId : null, (r28 & 4) != 0 ? r1.priceTier : null, (r28 & 8) != 0 ? r1.memberColor : null, (r28 & 16) != 0 ? r1.memberIcon : null, (r28 & 32) != 0 ? r1.guildRole : null, (r28 & 64) != 0 ? r1.image : null, (r28 & 128) != 0 ? r1.imageAssetId : null, (r28 & 256) != 0 ? r1.description : null, (r28 & 512) != 0 ? r1.isPublished : null, (r28 & 1024) != 0 ? r1.canAccessAllChannels : null, (r28 & 2048) != 0 ? r1.channelBenefits : null, (r28 & 4096) != 0 ? requireViewState().getGuildRoleSubscriptionTier().intangibleBenefits : null);
        updateViewState(ViewState.copy$default(requireViewState(), copy, null, 2, null));
    }
}
