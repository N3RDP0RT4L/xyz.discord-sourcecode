package com.discord.widgets.guild_role_subscriptions.tier.create;

import com.discord.utilities.color.ColorCompat;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionMemberPreview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionMemberPreview$defaultColor$2 extends o implements Function0<Integer> {
    public final /* synthetic */ GuildRoleSubscriptionMemberPreview this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionMemberPreview$defaultColor$2(GuildRoleSubscriptionMemberPreview guildRoleSubscriptionMemberPreview) {
        super(0);
        this.this$0 = guildRoleSubscriptionMemberPreview;
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [int, java.lang.Integer] */
    @Override // kotlin.jvm.functions.Function0
    public final Integer invoke() {
        return ColorCompat.getColor(this.this$0, (int) R.color.primary_300);
    }
}
