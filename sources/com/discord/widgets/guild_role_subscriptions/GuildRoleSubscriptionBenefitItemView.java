package com.discord.widgets.guild_role_subscriptions;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import com.discord.databinding.ViewGuildRoleSubscriptionBenefitItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.d0.f;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionBenefitItemView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 $2\u00020\u0001:\u0001$B\u0011\b\u0016\u0012\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 B\u001d\b\u0016\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001f\u0010!B'\b\u0016\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\"\u001a\u00020\u0015¢\u0006\u0004\b\u001f\u0010#J\u001b\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J!\u0010\n\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\b\u0010\t\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ'\u0010\u0010\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\f2\b\u0010\t\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0010\u0010\u0014J)\u0010\u0010\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\b\b\u0001\u0010\u0016\u001a\u00020\u00152\b\u0010\t\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\u0010\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001c¨\u0006%"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionBenefitItemView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Landroid/util/AttributeSet;", "attrs", "", "initialize", "(Landroid/util/AttributeSet;)V", "", "title", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "configureTitleAndDescription", "(Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/models/domain/emoji/Emoji;", "emoji", "configureEmoji", "(Lcom/discord/models/domain/emoji/Emoji;)V", "configureUI", "(Ljava/lang/String;Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;)V", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;", "benefit", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit;)V", "", "imageRes", "(Ljava/lang/String;ILjava/lang/String;)V", "emojiSizePx", "I", "Lcom/discord/databinding/ViewGuildRoleSubscriptionBenefitItemBinding;", "binding", "Lcom/discord/databinding/ViewGuildRoleSubscriptionBenefitItemBinding;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionBenefitItemView extends ConstraintLayout {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_EMOJI_SIZE = 64;
    private final ViewGuildRoleSubscriptionBenefitItemBinding binding;
    private final int emojiSizePx;

    /* compiled from: GuildRoleSubscriptionBenefitItemView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guild_role_subscriptions/GuildRoleSubscriptionBenefitItemView$Companion;", "", "", "MAX_EMOJI_SIZE", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionBenefitItemView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionBenefitItemBinding a = ViewGuildRoleSubscriptionBenefitItemBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        this.emojiSizePx = getResources().getDimensionPixelSize(R.dimen.guild_role_subscription_benefit_emoji_size);
        initialize$default(this, null, 1, null);
    }

    private final void configureEmoji(Emoji emoji) {
        String imageUri = emoji.getImageUri(false, f.coerceAtMost(IconUtils.getMediaProxySize(this.emojiSizePx), 64), getContext());
        SimpleDraweeView simpleDraweeView = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildRoleSubscriptionBenefitImage");
        MGImages.setImage$default(simpleDraweeView, imageUri, 0, 0, true, null, null, 108, null);
        SimpleDraweeView simpleDraweeView2 = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildRoleSubscriptionBenefitImage");
        simpleDraweeView2.setContentDescription(emoji.getFirstName());
    }

    private final void configureTitleAndDescription(String str, String str2) {
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.guildRoleSubscriptionBenefitName");
        textView.setText(str);
        TextView textView2 = this.binding.f2174b;
        m.checkNotNullExpressionValue(textView2, "binding.guildRoleSubscriptionBenefitDescription");
        ViewExtensions.setTextAndVisibilityBy(textView2, str2);
    }

    private final void initialize(AttributeSet attributeSet) {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.guild_role_subscription_setup_default_padding);
        setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ripple_rounded_bg_secondary));
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        int[] iArr = com.discord.R.a.GuildRoleSubscriptionBenefitItemView;
        m.checkNotNullExpressionValue(iArr, "R.styleable.GuildRoleSubscriptionBenefitItemView");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        String string = obtainStyledAttributes.getString(2);
        boolean z2 = false;
        String string2 = obtainStyledAttributes.getString(0);
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        if (string == null || t.isBlank(string)) {
            z2 = true;
        }
        if (!z2 && resourceId != -1) {
            configureUI(string, resourceId, string2);
        }
        obtainStyledAttributes.recycle();
    }

    public static /* synthetic */ void initialize$default(GuildRoleSubscriptionBenefitItemView guildRoleSubscriptionBenefitItemView, AttributeSet attributeSet, int i, Object obj) {
        if ((i & 1) != 0) {
            attributeSet = null;
        }
        guildRoleSubscriptionBenefitItemView.initialize(attributeSet);
    }

    public final void configureUI(String str, Emoji emoji, String str2) {
        m.checkNotNullParameter(str, "title");
        m.checkNotNullParameter(emoji, "emoji");
        configureTitleAndDescription(str, str2);
        configureEmoji(emoji);
    }

    public final void configureUI(Benefit benefit) {
        Integer channelIconResId;
        m.checkNotNullParameter(benefit, "benefit");
        configureUI(benefit.getName(), benefit.getEmoji(), benefit.getDescription());
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.guildRoleSubscriptionBenefitName");
        if (!(benefit instanceof Benefit.ChannelBenefit)) {
            benefit = null;
        }
        Benefit.ChannelBenefit channelBenefit = (Benefit.ChannelBenefit) benefit;
        ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView, (channelBenefit == null || (channelIconResId = channelBenefit.getChannelIconResId()) == null) ? 0 : channelIconResId.intValue(), 0, 0, 0, 14, null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionBenefitItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionBenefitItemBinding a = ViewGuildRoleSubscriptionBenefitItemBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        this.emojiSizePx = getResources().getDimensionPixelSize(R.dimen.guild_role_subscription_benefit_emoji_size);
        initialize(attributeSet);
    }

    public final void configureUI(String str, @DrawableRes int i, String str2) {
        m.checkNotNullParameter(str, "title");
        configureTitleAndDescription(str, str2);
        MGImages mGImages = MGImages.INSTANCE;
        SimpleDraweeView simpleDraweeView = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildRoleSubscriptionBenefitImage");
        ScalingUtils$ScaleType scalingUtils$ScaleType = ScalingUtils$ScaleType.a;
        ScalingUtils$ScaleType scalingUtils$ScaleType2 = b.f.g.e.t.l;
        m.checkNotNullExpressionValue(scalingUtils$ScaleType2, "ScalingUtils.ScaleType.CENTER_INSIDE");
        MGImages.setImage$default(mGImages, simpleDraweeView, i, scalingUtils$ScaleType2, null, 8, null);
    }

    public /* synthetic */ GuildRoleSubscriptionBenefitItemView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionBenefitItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionBenefitItemBinding a = ViewGuildRoleSubscriptionBenefitItemBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        this.emojiSizePx = getResources().getDimensionPixelSize(R.dimen.guild_role_subscription_benefit_emoji_size);
        initialize(attributeSet);
    }

    public /* synthetic */ GuildRoleSubscriptionBenefitItemView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
