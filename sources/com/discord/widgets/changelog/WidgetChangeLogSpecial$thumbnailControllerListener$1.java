package com.discord.widgets.changelog;

import android.graphics.drawable.Animatable;
import android.view.View;
import b.f.g.c.c;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetChangeLogSpecialBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.logging.Logger;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetChangeLogSpecial.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J-\u0010\t\u001a\u00020\b2\b\u0010\u0004\u001a\u0004\u0018\u00010\u00032\b\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u0016¢\u0006\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"com/discord/widgets/changelog/WidgetChangeLogSpecial$thumbnailControllerListener$1", "Lb/f/g/c/c;", "", "", ModelAuditLogEntry.CHANGE_KEY_ID, "imageInfo", "Landroid/graphics/drawable/Animatable;", "animatable", "", "onFinalImageSet", "(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChangeLogSpecial$thumbnailControllerListener$1 extends c<Object> {
    public final /* synthetic */ WidgetChangeLogSpecial this$0;

    public WidgetChangeLogSpecial$thumbnailControllerListener$1(WidgetChangeLogSpecial widgetChangeLogSpecial) {
        this.this$0 = widgetChangeLogSpecial;
    }

    @Override // b.f.g.c.c, com.facebook.drawee.controller.ControllerListener
    public void onFinalImageSet(String str, Object obj, Animatable animatable) {
        WidgetChangeLogSpecialBinding binding;
        WidgetChangeLogSpecialBinding binding2;
        super.onFinalImageSet(str, obj, animatable);
        try {
            binding = this.this$0.getBinding();
            SimpleDraweeView simpleDraweeView = binding.j;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.changeLogVideoOverlay");
            int i = 0;
            if (!(animatable != null)) {
                i = 8;
            }
            simpleDraweeView.setVisibility(i);
            binding2 = this.this$0.getBinding();
            binding2.j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$thumbnailControllerListener$1$onFinalImageSet$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChangeLogSpecialBinding binding3;
                    Animatable c;
                    WidgetChangeLogSpecial$thumbnailControllerListener$1.this.this$0.hideVideoOverlay();
                    binding3 = WidgetChangeLogSpecial$thumbnailControllerListener$1.this.this$0.getBinding();
                    SimpleDraweeView simpleDraweeView2 = binding3.h;
                    m.checkNotNullExpressionValue(simpleDraweeView2, "binding.changeLogThumbnail");
                    DraweeController controller = simpleDraweeView2.getController();
                    if (controller != null && (c = controller.c()) != null) {
                        c.start();
                    }
                }
            });
        } catch (Exception e) {
            Logger.e$default(AppLog.g, "Failed to set changelog thumbnail image.", e, null, 4, null);
        }
    }
}
