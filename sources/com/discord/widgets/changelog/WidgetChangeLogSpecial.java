package com.discord.widgets.changelog;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import b.a.d.f;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import b.f.g.a.a.b;
import b.f.g.a.a.d;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChangeLogSpecialBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.t.h0;
import d0.z.d.m;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import org.webrtc.MediaStreamTrack;
import xyz.discord.R;
/* compiled from: WidgetChangeLogSpecial.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\b\u0006*\u0001.\u0018\u0000 22\u00020\u0001:\u00012B\u0007¢\u0006\u0004\b1\u0010\u0015J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ7\u0010\u0012\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u00022\u0014\b\u0002\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000e0\r2\b\b\u0002\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0016\u0010\u0015J\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001bH\u0016¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001f\u0010\u0015J\u000f\u0010 \u001a\u00020\u0004H\u0016¢\u0006\u0004\b \u0010\u0015R\u001d\u0010\u0018\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R$\u0010&\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e\u0018\u00010%8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100¨\u00063"}, d2 = {"Lcom/discord/widgets/changelog/WidgetChangeLogSpecial;", "Lcom/discord/app/AppFragment;", "", "videoUrl", "", "configureMedia", "(Ljava/lang/String;)V", "Landroid/content/Context;", "context", "", "getDateString", "(Landroid/content/Context;)Ljava/lang/CharSequence;", "event", "", "", "properties", "", "includeStats", "track", "(Ljava/lang/String;Ljava/util/Map;Z)V", "showVideoOverlay", "()V", "hideVideoOverlay", "Lcom/discord/databinding/WidgetChangeLogSpecialBinding;", "binding", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetChangeLogSpecialBinding;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onPause", "onDestroy", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChangeLogSpecialBinding;", "Lcom/facebook/drawee/controller/AbstractDraweeController;", "thumbnailDraweeController", "Lcom/facebook/drawee/controller/AbstractDraweeController;", "", "maxScrolledPercent", "I", "", "openedTimestamp", "J", "com/discord/widgets/changelog/WidgetChangeLogSpecial$thumbnailControllerListener$1", "thumbnailControllerListener", "Lcom/discord/widgets/changelog/WidgetChangeLogSpecial$thumbnailControllerListener$1;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChangeLogSpecial extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChangeLogSpecial.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChangeLogSpecialBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_BODY = "INTENT_EXTRA_BODY";
    private static final String INTENT_EXTRA_EXIT_STYLE = "INTENT_EXTRA_EXIT_STYLE";
    private static final String INTENT_EXTRA_HIDE_VIDEO = "INTENT_EXTRA_HIDE_VIDEO";
    private static final String INTENT_EXTRA_REVISION = "INTENT_EXTRA_REVISION";
    private static final String INTENT_EXTRA_VERSION = "INTENT_EXTRA_VERSION";
    private static final String INTENT_EXTRA_VIDEO = "INTENT_EXTRA_VIDEO";
    private int maxScrolledPercent;
    private long openedTimestamp;
    private AbstractDraweeController<Object, Object> thumbnailDraweeController;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetChangeLogSpecial$binding$2.INSTANCE, new WidgetChangeLogSpecial$binding$3(this));
    private final WidgetChangeLogSpecial$thumbnailControllerListener$1 thumbnailControllerListener = new WidgetChangeLogSpecial$thumbnailControllerListener$1(this);

    /* compiled from: WidgetChangeLogSpecial.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0019B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018JK\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00042\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u000bH\u0007¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0011R\u0016\u0010\u0014\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0011R\u0016\u0010\u0015\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0011R\u0016\u0010\u0016\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0011¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/changelog/WidgetChangeLogSpecial$Companion;", "", "Landroid/content/Context;", "context", "", "version", "revision", MediaStreamTrack.VIDEO_TRACK_KIND, "body", "Lcom/discord/widgets/changelog/WidgetChangeLogSpecial$Companion$ExitStyle;", "exitStyle", "", "hideVideo", "", "launch", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/widgets/changelog/WidgetChangeLogSpecial$Companion$ExitStyle;Z)V", WidgetChangeLogSpecial.INTENT_EXTRA_BODY, "Ljava/lang/String;", WidgetChangeLogSpecial.INTENT_EXTRA_EXIT_STYLE, WidgetChangeLogSpecial.INTENT_EXTRA_HIDE_VIDEO, WidgetChangeLogSpecial.INTENT_EXTRA_REVISION, WidgetChangeLogSpecial.INTENT_EXTRA_VERSION, WidgetChangeLogSpecial.INTENT_EXTRA_VIDEO, HookHelper.constructorName, "()V", "ExitStyle", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetChangeLogSpecial.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/changelog/WidgetChangeLogSpecial$Companion$ExitStyle;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "BACK", "CLOSE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum ExitStyle {
            BACK,
            CLOSE
        }

        private Companion() {
        }

        public final void launch(Context context, String str, String str2, String str3, String str4, ExitStyle exitStyle, boolean z2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "version");
            m.checkNotNullParameter(str2, "revision");
            m.checkNotNullParameter(str3, MediaStreamTrack.VIDEO_TRACK_KIND);
            m.checkNotNullParameter(str4, "body");
            m.checkNotNullParameter(exitStyle, "exitStyle");
            Bundle bundle = new Bundle();
            bundle.putSerializable(WidgetChangeLogSpecial.INTENT_EXTRA_EXIT_STYLE, exitStyle);
            bundle.putString(WidgetChangeLogSpecial.INTENT_EXTRA_VERSION, str);
            bundle.putString(WidgetChangeLogSpecial.INTENT_EXTRA_REVISION, str2);
            bundle.putString(WidgetChangeLogSpecial.INTENT_EXTRA_VIDEO, str3);
            bundle.putString(WidgetChangeLogSpecial.INTENT_EXTRA_BODY, str4);
            bundle.putBoolean(WidgetChangeLogSpecial.INTENT_EXTRA_HIDE_VIDEO, z2);
            j.d(context, WidgetChangeLogSpecial.class, new Intent().putExtras(bundle));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChangeLogSpecial() {
        super(R.layout.widget_change_log_special);
    }

    private final void configureMedia(String str) {
        boolean endsWith$default = t.endsWith$default(str, ".mp4", false, 2, null);
        VideoView videoView = getBinding().i;
        m.checkNotNullExpressionValue(videoView, "binding.changeLogVideo");
        int i = 8;
        videoView.setVisibility(endsWith$default ? 0 : 8);
        SimpleDraweeView simpleDraweeView = getBinding().j;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.changeLogVideoOverlay");
        if (endsWith$default) {
            i = 0;
        }
        simpleDraweeView.setVisibility(i);
        if (!endsWith$default) {
            SimpleDraweeView simpleDraweeView2 = getBinding().h;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.changeLogThumbnail");
            simpleDraweeView2.setVisibility(0);
            d a = b.a();
            SimpleDraweeView simpleDraweeView3 = getBinding().h;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.changeLogThumbnail");
            a.n = simpleDraweeView3.getController();
            d g = a.g(str);
            g.m = false;
            g.k = this.thumbnailControllerListener;
            this.thumbnailDraweeController = g.a();
            SimpleDraweeView simpleDraweeView4 = getBinding().h;
            m.checkNotNullExpressionValue(simpleDraweeView4, "binding.changeLogThumbnail");
            simpleDraweeView4.setController(this.thumbnailDraweeController);
            getBinding().h.requestLayout();
            return;
        }
        getBinding().i.setVideoPath(str);
        getBinding().i.setOnCompletionListener(new MediaPlayer.OnCompletionListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$configureMedia$1
            @Override // android.media.MediaPlayer.OnCompletionListener
            public final void onCompletion(MediaPlayer mediaPlayer) {
                WidgetChangeLogSpecial.this.showVideoOverlay();
            }
        });
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$configureMedia$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChangeLogSpecialBinding binding;
                WidgetChangeLogSpecialBinding binding2;
                WidgetChangeLogSpecialBinding binding3;
                binding = WidgetChangeLogSpecial.this.getBinding();
                VideoView videoView2 = binding.i;
                m.checkNotNullExpressionValue(videoView2, "binding.changeLogVideo");
                if (videoView2.isPlaying()) {
                    WidgetChangeLogSpecial.this.showVideoOverlay();
                    binding3 = WidgetChangeLogSpecial.this.getBinding();
                    binding3.i.pause();
                } else {
                    WidgetChangeLogSpecial.this.hideVideoOverlay();
                    binding2 = WidgetChangeLogSpecial.this.getBinding();
                    binding2.i.start();
                }
                WidgetChangeLogSpecial.track$default(WidgetChangeLogSpecial.this, "change_log_video_interacted", null, false, 2, null);
            }
        });
        getBinding().i.setOnPreparedListener(new MediaPlayer.OnPreparedListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$configureMedia$3
            @Override // android.media.MediaPlayer.OnPreparedListener
            public final void onPrepared(MediaPlayer mediaPlayer) {
                WidgetChangeLogSpecialBinding binding;
                WidgetChangeLogSpecialBinding binding2;
                m.checkNotNullParameter(mediaPlayer, "mp");
                mediaPlayer.start();
                mediaPlayer.pause();
                binding = WidgetChangeLogSpecial.this.getBinding();
                VideoView videoView2 = binding.i;
                m.checkNotNullExpressionValue(videoView2, "binding.changeLogVideo");
                videoView2.getLayoutParams().height = -2;
                binding2 = WidgetChangeLogSpecial.this.getBinding();
                binding2.i.requestLayout();
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChangeLogSpecialBinding getBinding() {
        return (WidgetChangeLogSpecialBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final CharSequence getDateString(Context context) {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VERSION);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_date);
        }
        m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…tring.change_log_md_date)");
        try {
            Date parse = new SimpleDateFormat(TimeUtils.UTCFormat.SHORT).parse(stringExtra);
            if (parse == null) {
                parse = new Date();
            }
            return " " + DateFormat.getMediumDateFormat(context).format(parse) + " ";
        } catch (ParseException unused) {
            return stringExtra;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void hideVideoOverlay() {
        ViewExtensions.fadeOut$default(getBinding().j, 200L, WidgetChangeLogSpecial$hideVideoOverlay$1.INSTANCE, null, 4, null);
    }

    public static final void launch(Context context, String str, String str2, String str3, String str4, Companion.ExitStyle exitStyle, boolean z2) {
        Companion.launch(context, str, str2, str3, str4, exitStyle, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onViewBindingDestroy(WidgetChangeLogSpecialBinding widgetChangeLogSpecialBinding) {
        widgetChangeLogSpecialBinding.i.setOnPreparedListener(null);
        widgetChangeLogSpecialBinding.i.setOnCompletionListener(null);
        AbstractDraweeController<Object, Object> abstractDraweeController = this.thumbnailDraweeController;
        if (abstractDraweeController != null) {
            abstractDraweeController.A(this.thumbnailControllerListener);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showVideoOverlay() {
        ViewExtensions.fadeIn$default(getBinding().j, 200L, WidgetChangeLogSpecial$showVideoOverlay$1.INSTANCE, WidgetChangeLogSpecial$showVideoOverlay$2.INSTANCE, null, 8, null);
    }

    private final void track(String str, Map<String, ? extends Object> map, boolean z2) {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VERSION);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_date);
        }
        m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…tring.change_log_md_date)");
        String stringExtra2 = getMostRecentIntent().getStringExtra(INTENT_EXTRA_REVISION);
        if (stringExtra2 == null) {
            stringExtra2 = getString(R.string.change_log_md_revision);
        }
        m.checkNotNullExpressionValue(stringExtra2, "mostRecentIntent.getStri…g.change_log_md_revision)");
        HashMap hashMap = new HashMap();
        if (z2) {
            hashMap.put("seconds_open", Long.valueOf((ClockFactory.get().currentTimeMillis() - this.openedTimestamp) / 1000));
            hashMap.put("max_scrolled_percentage", Integer.valueOf(this.maxScrolledPercent));
        }
        AnalyticsTracker.INSTANCE.changeLogEvent(str, stringExtra, stringExtra2, h0.plus(map, hashMap));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void track$default(WidgetChangeLogSpecial widgetChangeLogSpecial, String str, Map map, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            map = new HashMap();
        }
        if ((i & 4) != 0) {
            z2 = true;
        }
        widgetChangeLogSpecial.track(str, map, z2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VERSION);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_date);
        }
        m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…tring.change_log_md_date)");
        StoreStream.Companion.getChangeLog().markSeen(stringExtra);
        track$default(this, "change_log_closed", null, false, 6, null);
        super.onDestroy();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        VideoView videoView = getBinding().i;
        m.checkNotNullExpressionValue(videoView, "binding.changeLogVideo");
        if (videoView.isPlaying()) {
            getBinding().i.pause();
        }
        super.onPause();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        this.openedTimestamp = ClockFactory.get().currentTimeMillis();
        track$default(this, "change_log_opened", null, false, 2, null);
        setActionBarSubtitle(getDateString(requireContext()));
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_BODY);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_body);
        }
        m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…tring.change_log_md_body)");
        if (getMostRecentIntent().getSerializableExtra(INTENT_EXTRA_EXIT_STYLE) == Companion.ExitStyle.BACK) {
            ImageButton imageButton = getBinding().e;
            m.checkNotNullExpressionValue(imageButton, "binding.changeLogSpecialClose");
            imageButton.setVisibility(8);
            ImageButton imageButton2 = getBinding().c;
            m.checkNotNullExpressionValue(imageButton2, "binding.changeLogSpecialBack");
            imageButton2.setVisibility(0);
        }
        TextView textView = getBinding().f;
        m.checkNotNullExpressionValue(textView, "binding.changeLogSpecialDate");
        textView.setText(getDateString(requireContext()));
        getBinding().f2237b.setDraweeSpanStringBuilder(ChangeLogParser.INSTANCE.parse(requireContext(), stringExtra, true, new WidgetChangeLogSpecial$onViewBound$1(this)));
        String stringExtra2 = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VIDEO);
        if (stringExtra2 == null) {
            stringExtra2 = getString(R.string.change_log_md_video);
        }
        m.checkNotNullExpressionValue(stringExtra2, "mostRecentIntent.getStri…ring.change_log_md_video)");
        configureMedia(stringExtra2);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChangeLogSpecial.this.requireActivity().finish();
            }
        });
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChangeLogSpecial.this.requireActivity().finish();
            }
        });
        LinkifiedTextView linkifiedTextView = getBinding().f2237b;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.changeLogBody");
        b.a.k.b.m(linkifiedTextView, R.string.changelog_stickers_cta_body, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        LinkifiedTextView linkifiedTextView2 = getBinding().f2237b;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.changeLogBody");
        linkifiedTextView2.setMovementMethod(LinkMovementMethod.getInstance());
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                UriHandler.handle$default(UriHandler.INSTANCE, WidgetChangeLogSpecial.this.requireContext(), f.a.a(360056891113L, null), null, 4, null);
            }
        });
        if (getMostRecentIntent().getBooleanExtra(INTENT_EXTRA_HIDE_VIDEO, false)) {
            VideoView videoView = getBinding().i;
            m.checkNotNullExpressionValue(videoView, "binding.changeLogVideo");
            videoView.setVisibility(8);
            CardView cardView = getBinding().k;
            m.checkNotNullExpressionValue(cardView, "binding.changelogSpecialHeaderContainer");
            ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) layoutParams;
            layoutParams2.setMargins(layoutParams2.leftMargin, layoutParams2.topMargin, layoutParams2.rightMargin, DimenUtils.dpToPixels(16));
        }
        getBinding().g.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() { // from class: com.discord.widgets.changelog.WidgetChangeLogSpecial$onViewBound$5
            @Override // androidx.core.widget.NestedScrollView.OnScrollChangeListener
            public final void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                int i5;
                WidgetChangeLogSpecialBinding binding;
                WidgetChangeLogSpecialBinding binding2;
                WidgetChangeLogSpecial widgetChangeLogSpecial = WidgetChangeLogSpecial.this;
                i5 = widgetChangeLogSpecial.maxScrolledPercent;
                binding = WidgetChangeLogSpecial.this.getBinding();
                NestedScrollView nestedScrollView2 = binding.g;
                m.checkNotNullExpressionValue(nestedScrollView2, "binding.changeLogSpecialScrollview");
                int height = ViewExtensions.getContentView(nestedScrollView2).getHeight();
                binding2 = WidgetChangeLogSpecial.this.getBinding();
                NestedScrollView nestedScrollView3 = binding2.g;
                m.checkNotNullExpressionValue(nestedScrollView3, "binding.changeLogSpecialScrollview");
                widgetChangeLogSpecial.maxScrolledPercent = d0.d0.f.coerceAtLeast(i5, (i2 * 100) / d0.d0.f.coerceAtLeast(height - nestedScrollView3.getHeight(), 1));
            }
        });
    }
}
