package com.discord.widgets.changelog;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.VideoView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.d.b.a.a;
import b.f.g.a.a.b;
import b.f.g.a.a.d;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChangeLogBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.d0.f;
import d0.g0.t;
import d0.t.h0;
import d0.z.d.m;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import org.webrtc.MediaStreamTrack;
import xyz.discord.R;
/* compiled from: WidgetChangeLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005*\u0001\"\u0018\u0000 32\u00020\u0001:\u00013B\u0007¢\u0006\u0004\b2\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\f\u0010\rJ7\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00022\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00100\u000f2\b\b\u0002\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0016\u0010\bJ\u000f\u0010\u0017\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0017\u0010\bJ\u0017\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001cH\u0016¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\u0004H\u0016¢\u0006\u0004\b \u0010\bJ\u000f\u0010!\u001a\u00020\u0004H\u0016¢\u0006\u0004\b!\u0010\bR\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u001d\u0010\u0019\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R$\u00100\u001a\u0010\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0010\u0018\u00010/8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101¨\u00064"}, d2 = {"Lcom/discord/widgets/changelog/WidgetChangeLog;", "Lcom/discord/app/AppFragment;", "", "videoUrl", "", "configureMedia", "(Ljava/lang/String;)V", "configureFooter", "()V", "Landroid/content/Context;", "context", "", "getDateString", "(Landroid/content/Context;)Ljava/lang/CharSequence;", "event", "", "", "properties", "", "includeStats", "track", "(Ljava/lang/String;Ljava/util/Map;Z)V", "showVideoOverlay", "hideVideoOverlay", "Lcom/discord/databinding/WidgetChangeLogBinding;", "binding", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetChangeLogBinding;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onPause", "onDestroy", "com/discord/widgets/changelog/WidgetChangeLog$thumbnailControllerListener$1", "thumbnailControllerListener", "Lcom/discord/widgets/changelog/WidgetChangeLog$thumbnailControllerListener$1;", "", "maxScrolledPercent", "I", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChangeLogBinding;", "", "openedTimestamp", "J", "Lcom/facebook/drawee/controller/AbstractDraweeController;", "thumbnailDraweeController", "Lcom/facebook/drawee/controller/AbstractDraweeController;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChangeLog extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChangeLog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChangeLogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_BODY = "INTENT_EXTRA_BODY";
    private static final String INTENT_EXTRA_REVISION = "INTENT_EXTRA_REVISION";
    private static final String INTENT_EXTRA_VERSION = "INTENT_EXTRA_VERSION";
    private static final String INTENT_EXTRA_VIDEO = "INTENT_EXTRA_VIDEO";
    private int maxScrolledPercent;
    private long openedTimestamp;
    private AbstractDraweeController<Object, Object> thumbnailDraweeController;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetChangeLog$binding$2.INSTANCE, new WidgetChangeLog$binding$3(this));
    private final WidgetChangeLog$thumbnailControllerListener$1 thumbnailControllerListener = new WidgetChangeLog$thumbnailControllerListener$1(this);

    /* compiled from: WidgetChangeLog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012J7\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\rR\u0016\u0010\u000f\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\rR\u0016\u0010\u0010\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\r¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/changelog/WidgetChangeLog$Companion;", "", "Landroid/content/Context;", "context", "", "version", "revision", MediaStreamTrack.VIDEO_TRACK_KIND, "body", "", "launch", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", WidgetChangeLog.INTENT_EXTRA_BODY, "Ljava/lang/String;", WidgetChangeLog.INTENT_EXTRA_REVISION, WidgetChangeLog.INTENT_EXTRA_VERSION, WidgetChangeLog.INTENT_EXTRA_VIDEO, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, String str, String str2, String str3, String str4) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "version");
            m.checkNotNullParameter(str2, "revision");
            m.checkNotNullParameter(str3, MediaStreamTrack.VIDEO_TRACK_KIND);
            m.checkNotNullParameter(str4, "body");
            Bundle bundle = new Bundle();
            bundle.putString(WidgetChangeLog.INTENT_EXTRA_VERSION, str);
            bundle.putString(WidgetChangeLog.INTENT_EXTRA_REVISION, str2);
            bundle.putString(WidgetChangeLog.INTENT_EXTRA_VIDEO, str3);
            bundle.putString(WidgetChangeLog.INTENT_EXTRA_BODY, str4);
            j.d(context, WidgetChangeLog.class, new Intent().putExtras(bundle));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChangeLog() {
        super(R.layout.widget_change_log);
    }

    private final void configureFooter() {
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLog$configureFooter$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UriHandler.handle$default(UriHandler.INSTANCE, WidgetChangeLog.this.requireContext(), WidgetChangeLog.this.getString(R.string.twitter_page_url), null, 4, null);
            }
        });
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLog$configureFooter$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UriHandler.handle$default(UriHandler.INSTANCE, WidgetChangeLog.this.requireContext(), "https://www.facebook.com/discordapp", null, 4, null);
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLog$configureFooter$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UriHandler.handle$default(UriHandler.INSTANCE, WidgetChangeLog.this.requireContext(), "https://www.instagram.com/discord", null, 4, null);
            }
        });
    }

    private final void configureMedia(String str) {
        boolean endsWith$default = t.endsWith$default(str, ".mp4", false, 2, null);
        VideoView videoView = getBinding().h;
        m.checkNotNullExpressionValue(videoView, "binding.changeLogVideo");
        int i = 8;
        videoView.setVisibility(endsWith$default ? 0 : 8);
        SimpleDraweeView simpleDraweeView = getBinding().i;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.changeLogVideoOverlay");
        if (endsWith$default) {
            i = 0;
        }
        simpleDraweeView.setVisibility(i);
        if (!endsWith$default) {
            SimpleDraweeView simpleDraweeView2 = getBinding().f;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.changeLogThumbnail");
            simpleDraweeView2.setVisibility(0);
            d a = b.a();
            SimpleDraweeView simpleDraweeView3 = getBinding().f;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.changeLogThumbnail");
            a.n = simpleDraweeView3.getController();
            d g = a.g(str);
            g.m = false;
            g.k = this.thumbnailControllerListener;
            this.thumbnailDraweeController = g.a();
            SimpleDraweeView simpleDraweeView4 = getBinding().f;
            m.checkNotNullExpressionValue(simpleDraweeView4, "binding.changeLogThumbnail");
            simpleDraweeView4.setController(this.thumbnailDraweeController);
            getBinding().f.requestLayout();
            return;
        }
        getBinding().h.setVideoPath(str);
        getBinding().h.setOnCompletionListener(new MediaPlayer.OnCompletionListener() { // from class: com.discord.widgets.changelog.WidgetChangeLog$configureMedia$1
            @Override // android.media.MediaPlayer.OnCompletionListener
            public final void onCompletion(MediaPlayer mediaPlayer) {
                WidgetChangeLog.this.showVideoOverlay();
            }
        });
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.changelog.WidgetChangeLog$configureMedia$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChangeLogBinding binding;
                WidgetChangeLogBinding binding2;
                WidgetChangeLogBinding binding3;
                binding = WidgetChangeLog.this.getBinding();
                VideoView videoView2 = binding.h;
                m.checkNotNullExpressionValue(videoView2, "binding.changeLogVideo");
                if (videoView2.isPlaying()) {
                    WidgetChangeLog.this.showVideoOverlay();
                    binding3 = WidgetChangeLog.this.getBinding();
                    binding3.h.pause();
                } else {
                    WidgetChangeLog.this.hideVideoOverlay();
                    binding2 = WidgetChangeLog.this.getBinding();
                    binding2.h.start();
                }
                WidgetChangeLog.track$default(WidgetChangeLog.this, "change_log_video_interacted", null, false, 2, null);
            }
        });
        getBinding().h.setOnPreparedListener(new MediaPlayer.OnPreparedListener() { // from class: com.discord.widgets.changelog.WidgetChangeLog$configureMedia$3
            @Override // android.media.MediaPlayer.OnPreparedListener
            public final void onPrepared(MediaPlayer mediaPlayer) {
                WidgetChangeLogBinding binding;
                WidgetChangeLogBinding binding2;
                m.checkNotNullParameter(mediaPlayer, "mp");
                mediaPlayer.start();
                mediaPlayer.pause();
                binding = WidgetChangeLog.this.getBinding();
                VideoView videoView2 = binding.h;
                m.checkNotNullExpressionValue(videoView2, "binding.changeLogVideo");
                videoView2.getLayoutParams().height = -2;
                binding2 = WidgetChangeLog.this.getBinding();
                binding2.h.requestLayout();
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChangeLogBinding getBinding() {
        return (WidgetChangeLogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final CharSequence getDateString(Context context) {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VERSION);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_date);
        }
        m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…tring.change_log_md_date)");
        try {
            Date parse = new SimpleDateFormat(TimeUtils.UTCFormat.SHORT).parse(stringExtra);
            if (parse == null) {
                parse = new Date();
            }
            String format = DateFormat.getMediumDateFormat(context).format(parse);
            m.checkNotNullExpressionValue(format, "DateFormat.getMediumDate…mat(context).format(date)");
            return format;
        } catch (ParseException unused) {
            return stringExtra;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void hideVideoOverlay() {
        ViewExtensions.fadeOut$default(getBinding().i, 200L, WidgetChangeLog$hideVideoOverlay$1.INSTANCE, null, 4, null);
    }

    public static final void launch(Context context, String str, String str2, String str3, String str4) {
        Companion.launch(context, str, str2, str3, str4);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onViewBindingDestroy(WidgetChangeLogBinding widgetChangeLogBinding) {
        widgetChangeLogBinding.h.setOnPreparedListener(null);
        widgetChangeLogBinding.h.setOnCompletionListener(null);
        AbstractDraweeController<Object, Object> abstractDraweeController = this.thumbnailDraweeController;
        if (abstractDraweeController != null) {
            abstractDraweeController.A(this.thumbnailControllerListener);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showVideoOverlay() {
        ViewExtensions.fadeIn$default(getBinding().i, 200L, WidgetChangeLog$showVideoOverlay$1.INSTANCE, WidgetChangeLog$showVideoOverlay$2.INSTANCE, null, 8, null);
    }

    private final void track(String str, Map<String, ? extends Object> map, boolean z2) {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VERSION);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_date);
        }
        m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…tring.change_log_md_date)");
        String stringExtra2 = getMostRecentIntent().getStringExtra(INTENT_EXTRA_REVISION);
        if (stringExtra2 == null) {
            stringExtra2 = getString(R.string.change_log_md_revision);
        }
        m.checkNotNullExpressionValue(stringExtra2, "mostRecentIntent.getStri…g.change_log_md_revision)");
        HashMap hashMap = new HashMap();
        if (z2) {
            hashMap.put("seconds_open", Long.valueOf((ClockFactory.get().currentTimeMillis() - this.openedTimestamp) / 1000));
            hashMap.put("max_scrolled_percentage", Integer.valueOf(this.maxScrolledPercent));
        }
        AnalyticsTracker.INSTANCE.changeLogEvent(str, stringExtra, stringExtra2, h0.plus(map, hashMap));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void track$default(WidgetChangeLog widgetChangeLog, String str, Map map, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            map = new HashMap();
        }
        if ((i & 4) != 0) {
            z2 = true;
        }
        widgetChangeLog.track(str, map, z2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VERSION);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_date);
        }
        m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…tring.change_log_md_date)");
        StoreStream.Companion.getChangeLog().markSeen(stringExtra);
        track$default(this, "change_log_closed", null, false, 6, null);
        super.onDestroy();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        VideoView videoView = getBinding().h;
        m.checkNotNullExpressionValue(videoView, "binding.changeLogVideo");
        if (videoView.isPlaying()) {
            getBinding().h.pause();
        }
        super.onPause();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        this.openedTimestamp = ClockFactory.get().currentTimeMillis();
        track$default(this, "change_log_opened", null, false, 2, null);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.whats_new);
        setActionBarSubtitle(getDateString(requireContext()));
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_BODY);
        if (stringExtra == null) {
            stringExtra = getString(R.string.change_log_md_body);
        }
        String str = stringExtra;
        m.checkNotNullExpressionValue(str, "mostRecentIntent.getStri…tring.change_log_md_body)");
        getBinding().f2236b.setDraweeSpanStringBuilder(ChangeLogParser.parse$default(ChangeLogParser.INSTANCE, requireContext(), str, false, new WidgetChangeLog$onViewBound$1(this), 4, null));
        String stringExtra2 = getMostRecentIntent().getStringExtra(INTENT_EXTRA_VIDEO);
        if (stringExtra2 == null) {
            stringExtra2 = getString(R.string.change_log_md_video);
        }
        m.checkNotNullExpressionValue(stringExtra2, "mostRecentIntent.getStri…ring.change_log_md_video)");
        configureMedia(stringExtra2);
        configureFooter();
        getBinding().e.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() { // from class: com.discord.widgets.changelog.WidgetChangeLog$onViewBound$2
            @Override // androidx.core.widget.NestedScrollView.OnScrollChangeListener
            public final void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                int i5;
                WidgetChangeLogBinding binding;
                WidgetChangeLogBinding binding2;
                WidgetChangeLog widgetChangeLog = WidgetChangeLog.this;
                i5 = widgetChangeLog.maxScrolledPercent;
                binding = WidgetChangeLog.this.getBinding();
                NestedScrollView nestedScrollView2 = binding.e;
                m.checkNotNullExpressionValue(nestedScrollView2, "binding.changeLogScrollview");
                int height = ViewExtensions.getContentView(nestedScrollView2).getHeight();
                binding2 = WidgetChangeLog.this.getBinding();
                NestedScrollView nestedScrollView3 = binding2.e;
                m.checkNotNullExpressionValue(nestedScrollView3, "binding.changeLogScrollview");
                widgetChangeLog.maxScrolledPercent = f.coerceAtLeast(i5, (i2 * 100) / f.coerceAtLeast(height - nestedScrollView3.getHeight(), 1));
            }
        });
    }
}
