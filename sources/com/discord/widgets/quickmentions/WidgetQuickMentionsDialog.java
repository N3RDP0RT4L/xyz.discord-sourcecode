package com.discord.widgets.quickmentions;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import androidx.core.os.BundleKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetQuickMentionsDialogBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.recycler.ViewPager2ExtensionsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.user.WidgetUserMentions;
import com.discord.widgets.user.search.WidgetGlobalSearch;
import com.discord.widgets.user.search.WidgetGlobalSearchDismissModel;
import com.google.android.material.tabs.TabLayout;
import d0.o;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetQuickMentionsDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00182\u00020\u0001:\u0002\u0018\u0019B\u0007¢\u0006\u0004\b\u0017\u0010\tJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\n\u0010\tR\u001d\u0010\u0010\u001a\u00020\u000b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0016\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/quickmentions/WidgetQuickMentionsDialog;", "Lcom/discord/app/AppDialog;", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/app/Dialog;", "onCreateDialog", "(Landroid/os/Bundle;)Landroid/app/Dialog;", "", "onStart", "()V", "onViewBoundOrOnResume", "Lcom/discord/widgets/user/search/WidgetGlobalSearchDismissModel;", "dismissViewModel$delegate", "Lkotlin/Lazy;", "getDismissViewModel", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchDismissModel;", "dismissViewModel", "Lcom/discord/databinding/WidgetQuickMentionsDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetQuickMentionsDialogBinding;", "binding", HookHelper.constructorName, "Companion", "TabsAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetQuickMentionsDialog extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetQuickMentionsDialog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetQuickMentionsDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int POSITION_MENTIONS = 1;
    private static final int POSITION_QUICK_SWITCHER = 0;
    private final Lazy dismissViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGlobalSearchDismissModel.class), new WidgetQuickMentionsDialog$$special$$inlined$activityViewModels$1(this), new WidgetQuickMentionsDialog$$special$$inlined$activityViewModels$2(this));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetQuickMentionsDialog$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetQuickMentionsDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\r¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/quickmentions/WidgetQuickMentionsDialog$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "searchText", "", "show", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V", "dismiss", "(Landroidx/fragment/app/FragmentManager;)V", "", "POSITION_MENTIONS", "I", "POSITION_QUICK_SWITCHER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, String str, int i, Object obj) {
            if ((i & 2) != 0) {
                str = null;
            }
            companion.show(fragmentManager, str);
        }

        public final void dismiss(FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            Fragment findFragmentByTag = fragmentManager.findFragmentByTag("javaClass");
            if (findFragmentByTag instanceof WidgetQuickMentionsDialog) {
                ((WidgetQuickMentionsDialog) findFragmentByTag).dismiss();
            }
        }

        public final void show(FragmentManager fragmentManager, String str) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            dismiss(fragmentManager);
            WidgetQuickMentionsDialog widgetQuickMentionsDialog = new WidgetQuickMentionsDialog();
            widgetQuickMentionsDialog.setArguments(BundleKt.bundleOf(o.to(WidgetGlobalSearch.EXTRA_SEARCH_TEXT, str)));
            widgetQuickMentionsDialog.show(fragmentManager, "javaClass");
            AnalyticsTracker.INSTANCE.quickSwitcherOpen();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetQuickMentionsDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\t\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR\u0018\u0010\f\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/quickmentions/WidgetQuickMentionsDialog$TabsAdapter;", "Landroidx/viewpager2/adapter/FragmentStateAdapter;", "", "getItemCount", "()I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Landroidx/fragment/app/Fragment;", "createFragment", "(I)Landroidx/fragment/app/Fragment;", "getItemTitleResId", "(I)I", "", "searchText", "Ljava/lang/String;", "Lcom/discord/widgets/quickmentions/WidgetQuickMentionsDialog;", "fragment", HookHelper.constructorName, "(Lcom/discord/widgets/quickmentions/WidgetQuickMentionsDialog;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TabsAdapter extends FragmentStateAdapter {
        private final String searchText;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public TabsAdapter(WidgetQuickMentionsDialog widgetQuickMentionsDialog, String str) {
            super(widgetQuickMentionsDialog);
            m.checkNotNullParameter(widgetQuickMentionsDialog, "fragment");
            this.searchText = str;
        }

        @Override // androidx.viewpager2.adapter.FragmentStateAdapter
        public Fragment createFragment(int i) {
            if (i == 0) {
                return WidgetGlobalSearch.Companion.create(this.searchText);
            }
            if (i == 1) {
                return WidgetUserMentions.Companion.create(true);
            }
            throw new UnsupportedOperationException();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return 2;
        }

        public final int getItemTitleResId(int i) {
            if (i == 0) {
                return R.string.search;
            }
            if (i == 1) {
                return R.string.mentions;
            }
            throw new UnsupportedOperationException();
        }
    }

    public WidgetQuickMentionsDialog() {
        super(R.layout.widget_quick_mentions_dialog);
    }

    private final WidgetQuickMentionsDialogBinding getBinding() {
        return (WidgetQuickMentionsDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final WidgetGlobalSearchDismissModel getDismissViewModel() {
        return (WidgetGlobalSearchDismissModel) this.dismissViewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        setStyle(0, R.style.AppTheme_GlobalSearchDialog);
        return super.onCreateDialog(bundle);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && (window = dialog.getWindow()) != null) {
            if (!AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
                window.getAttributes().windowAnimations = R.style.UiKit_QuickSwitcher_Animation;
            }
            window.setLayout(-1, -1);
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setSoftInputMode(53);
        }
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getDismissViewModel().getDismissEvents(), this, null, 2, null), WidgetQuickMentionsDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetQuickMentionsDialog$onViewBoundOrOnResume$1(this));
        TabsAdapter tabsAdapter = new TabsAdapter(this, requireArguments().getString(WidgetGlobalSearch.EXTRA_SEARCH_TEXT));
        ViewPager2 viewPager2 = getBinding().f2493b;
        m.checkNotNullExpressionValue(viewPager2, "binding.tabContents");
        viewPager2.setAdapter(tabsAdapter);
        TabLayout tabLayout = getBinding().c;
        m.checkNotNullExpressionValue(tabLayout, "binding.tabs");
        ViewPager2 viewPager22 = getBinding().f2493b;
        m.checkNotNullExpressionValue(viewPager22, "binding.tabContents");
        ViewPager2ExtensionsKt.setUpWithViewPager2(tabLayout, viewPager22, new WidgetQuickMentionsDialog$onViewBoundOrOnResume$2(this, tabsAdapter));
    }
}
