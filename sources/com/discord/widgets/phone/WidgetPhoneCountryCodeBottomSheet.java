package com.discord.widgets.phone;

import andhook.lib.HookHelper;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetPhoneCountryCodeBottomSheetBinding;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetPhoneCountryCodeBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0007¢\u0006\u0004\b\u0018\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\n\u0010\u000bR\u001c\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u001d\u0010\u0014\u001a\u00020\u000f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/phone/WidgetPhoneCountryCodeBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "", "filter", "", "configureUI", "(Ljava/lang/String;)V", "", "getContentViewResId", "()I", "onResume", "()V", "Lrx/subjects/BehaviorSubject;", "nameFilterSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/databinding/WidgetPhoneCountryCodeBottomSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetPhoneCountryCodeBottomSheetBinding;", "binding", "Lcom/discord/widgets/phone/PhoneCountryCodeAdapter;", "adapter", "Lcom/discord/widgets/phone/PhoneCountryCodeAdapter;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPhoneCountryCodeBottomSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetPhoneCountryCodeBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetPhoneCountryCodeBottomSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final BehaviorSubject<String> nameFilterSubject;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetPhoneCountryCodeBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final PhoneCountryCodeAdapter adapter = new PhoneCountryCodeAdapter();

    /* compiled from: WidgetPhoneCountryCodeBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/phone/WidgetPhoneCountryCodeBottomSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "context", "", "show", "(Landroidx/fragment/app/FragmentManager;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "context");
            new WidgetPhoneCountryCodeBottomSheet().show(fragmentManager, WidgetPhoneCountryCodeBottomSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetPhoneCountryCodeBottomSheet() {
        super(false, 1, null);
        BehaviorSubject<String> l0 = BehaviorSubject.l0("");
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(\"\")");
        this.nameFilterSubject = l0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x003f, code lost:
        if (d0.g0.w.contains((java.lang.CharSequence) r6.getTranslatedStringForCountry(r5, r8), (java.lang.CharSequence) r11, true) != false) goto L9;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureUI(java.lang.String r11) {
        /*
            r10 = this;
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePhone r0 = r0.getPhone()
            java.util.List r1 = r0.getCountryCodes()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r1 = r1.iterator()
        L13:
            boolean r3 = r1.hasNext()
            r4 = 0
            if (r3 == 0) goto L48
            java.lang.Object r3 = r1.next()
            r5 = r3
            com.discord.models.phone.PhoneCountryCode r5 = (com.discord.models.phone.PhoneCountryCode) r5
            java.lang.String r6 = r5.getName()
            r7 = 1
            boolean r6 = d0.g0.w.contains(r6, r11, r7)
            if (r6 != 0) goto L41
            com.discord.utilities.phone.PhoneUtils r6 = com.discord.utilities.phone.PhoneUtils.INSTANCE
            android.content.Context r8 = r10.requireContext()
            java.lang.String r9 = "requireContext()"
            d0.z.d.m.checkNotNullExpressionValue(r8, r9)
            java.lang.String r5 = r6.getTranslatedStringForCountry(r5, r8)
            boolean r5 = d0.g0.w.contains(r5, r11, r7)
            if (r5 == 0) goto L42
        L41:
            r4 = 1
        L42:
            if (r4 == 0) goto L13
            r2.add(r3)
            goto L13
        L48:
            com.discord.databinding.WidgetPhoneCountryCodeBottomSheetBinding r11 = r10.getBinding()
            androidx.recyclerview.widget.RecyclerView r11 = r11.f2486b
            r11.setHasFixedSize(r4)
            com.discord.databinding.WidgetPhoneCountryCodeBottomSheetBinding r11 = r10.getBinding()
            androidx.recyclerview.widget.RecyclerView r11 = r11.f2486b
            java.lang.String r1 = "binding.phoneCountryCodeRecycler"
            d0.z.d.m.checkNotNullExpressionValue(r11, r1)
            com.discord.widgets.phone.PhoneCountryCodeAdapter r1 = r10.adapter
            r11.setAdapter(r1)
            com.discord.widgets.phone.PhoneCountryCodeAdapter r11 = r10.adapter
            com.discord.widgets.phone.WidgetPhoneCountryCodeBottomSheet$configureUI$1 r1 = new com.discord.widgets.phone.WidgetPhoneCountryCodeBottomSheet$configureUI$1
            r1.<init>()
            r11.configure(r2, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.phone.WidgetPhoneCountryCodeBottomSheet.configureUI(java.lang.String):void");
    }

    private final WidgetPhoneCountryCodeBottomSheetBinding getBinding() {
        return (WidgetPhoneCountryCodeBottomSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_phone_country_code_bottom_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneCountryCodeSearch");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetPhoneCountryCodeBottomSheet$onResume$1(this));
        Observable<String> o = this.nameFilterSubject.o(200L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "nameFilterSubject.deboun…0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(o, this, null, 2, null), WidgetPhoneCountryCodeBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPhoneCountryCodeBottomSheet$onResume$2(this));
        configureUI("");
    }
}
