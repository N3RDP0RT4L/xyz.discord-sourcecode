package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetDirectoriesSearchBinding;
import com.discord.stores.utilities.Loading;
import com.discord.utilities.hubs.HubUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.directories.DirectoryChannelItem;
import com.discord.widgets.directories.WidgetDirectoriesSearchViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.g0.t;
import d0.t.o;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetDirectoriesSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b-\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\fR\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001c\u001a\u00020\u00178F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010!\u001a\u00020\u001d8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u0019\u001a\u0004\b\u001f\u0010 R$\u0010%\u001a\u0010\u0012\f\u0012\n $*\u0004\u0018\u00010#0#0\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u001d\u0010,\u001a\u00020'8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+¨\u0006."}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesSearch;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "state", "", "configureUI", "(Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;)V", "", "showList", "toggleList", "(Z)V", "searchForDirectories", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/directories/WidgetDirectoryChannelAdapter;", "adapter", "Lcom/discord/widgets/directories/WidgetDirectoryChannelAdapter;", "getAdapter", "()Lcom/discord/widgets/directories/WidgetDirectoryChannelAdapter;", "Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel;", "viewModel", "Lcom/discord/widgets/directories/DirectoriesSearchArgs;", "args$delegate", "getArgs", "()Lcom/discord/widgets/directories/DirectoriesSearchArgs;", "args", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "activityResult", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/databinding/WidgetDirectoriesSearchBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetDirectoriesSearchBinding;", "binding", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesSearch extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetDirectoriesSearch.class, "binding", "getBinding()Lcom/discord/databinding/WidgetDirectoriesSearchBinding;", 0)};
    private final Lazy viewModel$delegate;
    private final Lazy args$delegate = g.lazy(new WidgetDirectoriesSearch$$special$$inlined$args$1(this, "intent_args_key"));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetDirectoriesSearch$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> activityResult = HubUtilsKt.getAddServerActivityResultHandler(this);
    private final WidgetDirectoryChannelAdapter adapter = new WidgetDirectoryChannelAdapter(new WidgetDirectoriesSearch$adapter$1(this));

    public WidgetDirectoriesSearch() {
        super(R.layout.widget_directories_search);
        WidgetDirectoriesSearch$viewModel$2 widgetDirectoriesSearch$viewModel$2 = new WidgetDirectoriesSearch$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetDirectoriesSearchViewModel.class), new WidgetDirectoriesSearch$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetDirectoriesSearch$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetDirectoriesSearchViewModel.ViewState viewState) {
        CharSequence charSequence;
        WidgetDirectoryChannelAdapter widgetDirectoryChannelAdapter = this.adapter;
        List<DirectoryEntryData> directoryEntryData = viewState.getDirectoryEntryData();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(directoryEntryData, 10));
        for (DirectoryEntryData directoryEntryData2 : directoryEntryData) {
            arrayList.add(new DirectoryChannelItem.DirectoryItem(directoryEntryData2));
        }
        widgetDirectoryChannelAdapter.setDirectoryChannelItems(arrayList);
        toggleList(!viewState.getDirectories().isEmpty());
        FloatingActionButton floatingActionButton = getBinding().f;
        m.checkNotNullExpressionValue(floatingActionButton, "binding.search");
        WidgetDirectoriesSearchViewModel viewModel = getViewModel();
        TextInputEditText textInputEditText = getBinding().h;
        m.checkNotNullExpressionValue(textInputEditText, "binding.searchBarText");
        boolean z2 = !viewModel.shouldNotSearch(String.valueOf(textInputEditText.getText()));
        int i = 8;
        floatingActionButton.setVisibility(z2 ? 0 : 8);
        if (viewState.getDirectories().isEmpty() && (!m.areEqual(viewState.getDirectoriesState(), Loading.INSTANCE))) {
            LinkifiedTextView linkifiedTextView = getBinding().f2343b;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.emptyDescription");
            TextInputEditText textInputEditText2 = getBinding().h;
            m.checkNotNullExpressionValue(textInputEditText2, "binding.searchBarText");
            Editable text = textInputEditText2.getText();
            if (text == null || t.isBlank(text)) {
                charSequence = b.e(this, R.string.guild_discovery_search_protip, new Object[0], new WidgetDirectoriesSearch$configureUI$2(this));
            } else if (!viewState.getHasAddGuildPermissions()) {
                charSequence = b.e(this, R.string.directory_search_no_results_no_add, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            } else {
                charSequence = b.e(this, R.string.directory_search_no_results_subtitle, new Object[0], new WidgetDirectoriesSearch$configureUI$3(this));
            }
            linkifiedTextView.setText(charSequence);
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.emptyTitle");
            TextInputEditText textInputEditText3 = getBinding().h;
            m.checkNotNullExpressionValue(textInputEditText3, "binding.searchBarText");
            Editable text2 = textInputEditText3.getText();
            if (!(text2 == null || t.isBlank(text2))) {
                i = 0;
            }
            textView.setVisibility(i);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void searchForDirectories() {
        WidgetDirectoriesSearchViewModel viewModel = getViewModel();
        long channelId = getArgs().getChannelId();
        TextInputEditText textInputEditText = getBinding().h;
        m.checkNotNullExpressionValue(textInputEditText, "binding.searchBarText");
        viewModel.searchForDirectories(channelId, String.valueOf(textInputEditText.getText()));
        hideKeyboard(getView());
    }

    private final void toggleList(boolean z2) {
        RecyclerView recyclerView = getBinding().e;
        m.checkNotNullExpressionValue(recyclerView, "binding.recyclerView");
        int i = 0;
        recyclerView.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout = getBinding().c;
        m.checkNotNullExpressionValue(linearLayout, "binding.emptyState");
        if (!(!z2)) {
            i = 8;
        }
        linearLayout.setVisibility(i);
    }

    public final WidgetDirectoryChannelAdapter getAdapter() {
        return this.adapter;
    }

    public final DirectoriesSearchArgs getArgs() {
        return (DirectoriesSearchArgs) this.args$delegate.getValue();
    }

    public final WidgetDirectoriesSearchBinding getBinding() {
        return (WidgetDirectoriesSearchBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final WidgetDirectoriesSearchViewModel getViewModel() {
        return (WidgetDirectoriesSearchViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(final View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RecyclerView recyclerView = getBinding().e;
        recyclerView.setAdapter(this.adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        getBinding().g.setStartIconOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.WidgetDirectoriesSearch$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                FragmentActivity activity = WidgetDirectoriesSearch.this.e();
                if (activity != null) {
                    activity.onBackPressed();
                }
                WidgetDirectoriesSearch.this.hideKeyboard(view);
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.WidgetDirectoriesSearch$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetDirectoriesSearch.this.searchForDirectories();
            }
        });
        TextInputLayout textInputLayout = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout, "binding.searchBar");
        ViewExtensions.setOnImeActionDone$default(textInputLayout, false, new WidgetDirectoriesSearch$onViewBound$4(this), 1, null);
        TextInputEditText textInputEditText = getBinding().h;
        textInputEditText.addTextChangedListener(new TextWatcher() { // from class: com.discord.widgets.directories.WidgetDirectoriesSearch$onViewBound$$inlined$apply$lambda$1
            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
            }

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                FloatingActionButton floatingActionButton = WidgetDirectoriesSearch.this.getBinding().f;
                m.checkNotNullExpressionValue(floatingActionButton, "binding.search");
                WidgetDirectoriesSearchViewModel viewModel = WidgetDirectoriesSearch.this.getViewModel();
                if (charSequence == null) {
                    charSequence = "";
                }
                floatingActionButton.setVisibility(viewModel.shouldNotSearch(charSequence) ^ true ? 0 : 8);
            }
        });
        m.checkNotNullExpressionValue(textInputEditText, "this");
        showKeyboard(textInputEditText);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(ObservableExtensionsKt.ui(getViewModel().observeViewState()), this, null, 2, null), WidgetDirectoriesSearch.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDirectoriesSearch$onViewBoundOrOnResume$1(this));
    }
}
