package com.discord.widgets.directories;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.widgets.directories.WidgetDirectoriesViewModel;
import d0.t.g0;
import d0.t.h0;
import d0.t.i0;
import d0.t.m;
import d0.t.u;
import d0.z.d.o;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetDirectoriesViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lkotlin/Pair;", "Lcom/discord/models/hubs/DirectoryEntryCategory;", "", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesViewModel$ViewState$mappedTabs$2 extends o implements Function0<List<? extends Pair<? extends DirectoryEntryCategory, ? extends Integer>>> {
    public final /* synthetic */ WidgetDirectoriesViewModel.ViewState this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesViewModel$ViewState$mappedTabs$2(WidgetDirectoriesViewModel.ViewState viewState) {
        super(0);
        this.this$0 = viewState;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends Pair<? extends DirectoryEntryCategory, ? extends Integer>> invoke() {
        Map map;
        List listOf = m.listOf(d0.o.to(DirectoryEntryCategory.Home, 0));
        Map<Integer, Integer> invoke = this.this$0.getTabs().invoke();
        if (invoke != null) {
            map = new LinkedHashMap(g0.mapCapacity(invoke.size()));
            Iterator<T> it = invoke.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                DirectoryEntryCategory.Companion companion = DirectoryEntryCategory.Companion;
                int intValue = ((Number) entry.getKey()).intValue();
                Channel channel = this.this$0.getChannel();
                boolean z2 = true;
                if (channel == null || !ChannelUtils.u(channel)) {
                    z2 = false;
                }
                map.put(companion.findByKey(intValue, z2), entry.getValue());
            }
        } else {
            map = null;
        }
        if (map == null) {
            map = h0.emptyMap();
        }
        return u.plus((Collection) listOf, (Iterable) i0.toList(map));
    }
}
