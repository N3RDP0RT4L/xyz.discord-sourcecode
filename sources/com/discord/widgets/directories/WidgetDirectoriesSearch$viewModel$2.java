package com.discord.widgets.directories;

import com.discord.app.AppViewModel;
import com.discord.widgets.directories.WidgetDirectoriesSearchViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetDirectoriesSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesSearch$viewModel$2 extends o implements Function0<AppViewModel<WidgetDirectoriesSearchViewModel.ViewState>> {
    public final /* synthetic */ WidgetDirectoriesSearch this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesSearch$viewModel$2(WidgetDirectoriesSearch widgetDirectoriesSearch) {
        super(0);
        this.this$0 = widgetDirectoriesSearch;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetDirectoriesSearchViewModel.ViewState> invoke() {
        return new WidgetDirectoriesSearchViewModel(this.this$0.getArgs().getChannelId(), null, null, null, 14, null);
    }
}
