package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.content.Intent;
import android.view.View;
import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.e0;
import b.d.b.a.a;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetDirectoryCategoryBinding;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.utilities.directories.DirectoryUtilsKt;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.utilities.hubs.HubUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.directories.DirectoryChannelItem;
import com.discord.widgets.directories.WidgetDirectoriesViewModel;
import d0.g;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetDirectoryCategory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b3\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\b*\b\u0012\u0004\u0012\u00020\t0\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\u0006J!\u0010\u0010\u001a\u00020\u0004*\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017R$\u0010\u001b\u001a\u0010\u0012\f\u0012\n \u001a*\u0004\u0018\u00010\u00190\u00190\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0019\u0010\u001e\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u001d\u0010'\u001a\u00020\"8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001d\u0010,\u001a\u00020(8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b)\u0010$\u001a\u0004\b*\u0010+R\u001d\u00102\u001a\u00020-8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101¨\u00064"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoryCategory;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "state", "", "configureUi", "(Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;)V", "configureHomeUi", "", "Lcom/discord/widgets/directories/DirectoryEntryData;", "Lcom/discord/widgets/directories/DirectoryChannelItem$DirectoryItem;", "toDirectoryItems", "(Ljava/util/List;)Ljava/util/List;", "configureCategoryUi", "", "Lcom/discord/widgets/directories/DirectoryChannelItem;", "maybeAddServerRow", "(Ljava/util/List;Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;)V", "onViewBoundOrOnResume", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "activityResult", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/widgets/directories/WidgetDirectoryChannelAdapter;", "adapter", "Lcom/discord/widgets/directories/WidgetDirectoryChannelAdapter;", "getAdapter", "()Lcom/discord/widgets/directories/WidgetDirectoryChannelAdapter;", "Lcom/discord/widgets/directories/WidgetDirectoriesViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/directories/WidgetDirectoriesViewModel;", "viewModel", "Lcom/discord/widgets/directories/DirectoryCategoryArgs;", "args$delegate", "getArgs", "()Lcom/discord/widgets/directories/DirectoryCategoryArgs;", "args", "Lcom/discord/databinding/WidgetDirectoryCategoryBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetDirectoryCategoryBinding;", "binding", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoryCategory extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetDirectoryCategory.class, "binding", "getBinding()Lcom/discord/databinding/WidgetDirectoryCategoryBinding;", 0)};
    private final Lazy args$delegate = g.lazy(new WidgetDirectoryCategory$$special$$inlined$args$1(this, "intent_args_key"));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetDirectoryCategory$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetDirectoriesViewModel.class), new WidgetDirectoryCategory$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetDirectoryCategory$viewModel$2.INSTANCE));
    private final ActivityResultLauncher<Intent> activityResult = HubUtilsKt.getAddServerActivityResultHandler(this);
    private final WidgetDirectoryChannelAdapter adapter = new WidgetDirectoryChannelAdapter(new WidgetDirectoryCategory$adapter$1(this));

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            DirectoryEntryCategory.values();
            int[] iArr = new int[11];
            $EnumSwitchMapping$0 = iArr;
            iArr[DirectoryEntryCategory.Home.ordinal()] = 1;
        }
    }

    public WidgetDirectoryCategory() {
        super(R.layout.widget_directory_category);
    }

    private final void configureCategoryUi(WidgetDirectoriesViewModel.ViewState viewState) {
        List<DirectoryEntryData> list = viewState.getDirectoryEntryData().get(Integer.valueOf(getArgs().getDirectoryCategory().getKey()));
        if (list == null) {
            list = n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (DirectoryEntryData directoryEntryData : list) {
            arrayList.add(new DirectoryChannelItem.DirectoryItem(directoryEntryData));
        }
        List<DirectoryChannelItem> mutableList = u.toMutableList((Collection) arrayList);
        maybeAddServerRow(mutableList, viewState);
        this.adapter.setDirectoryChannelItems(mutableList);
    }

    private final void configureHomeUi(WidgetDirectoriesViewModel.ViewState viewState) {
        List<DirectoryChannelItem.DirectoryItem> directoryItems;
        List<DirectoryChannelItem.DirectoryItem> directoryItems2;
        List<DirectoryChannelItem.DirectoryItem> directoryItems3;
        if (viewState.getChannel() != null) {
            boolean u = ChannelUtils.u(viewState.getChannel());
            GrowthTeamFeatures growthTeamFeatures = GrowthTeamFeatures.INSTANCE;
            boolean hubUnreadsEnabled = growthTeamFeatures.hubUnreadsEnabled(!u);
            boolean hubUnreadsRecommendations = growthTeamFeatures.hubUnreadsRecommendations(!u);
            if (u || (!hubUnreadsRecommendations && !hubUnreadsEnabled)) {
                WidgetDirectoryChannelAdapter widgetDirectoryChannelAdapter = this.adapter;
                List<DirectoryChannelItem> mutableList = u.toMutableList((Collection) toDirectoryItems(viewState.getAllDirectoryEntryData()));
                maybeAddServerRow(mutableList, viewState);
                widgetDirectoryChannelAdapter.setDirectoryChannelItems(mutableList);
                return;
            }
            ArrayList arrayList = new ArrayList();
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            ArrayList arrayList2 = null;
            if (hubUnreadsEnabled) {
                List<DirectoryEntryData> sortByAdded = DirectoryUtilsKt.sortByAdded(viewState.getAllDirectoryEntryData());
                if (!(!sortByAdded.isEmpty())) {
                    sortByAdded = null;
                }
                if (!(sortByAdded == null || (directoryItems3 = toDirectoryItems(sortByAdded)) == null)) {
                    arrayList.add(new DirectoryChannelItem.SectionHeader(R.string.search_newest));
                    arrayList.addAll(directoryItems3);
                    for (DirectoryChannelItem.DirectoryItem directoryItem : directoryItems3) {
                        linkedHashSet.add(Long.valueOf(directoryItem.getDirectoryEntryData().getEntry().e().h()));
                    }
                }
            }
            if (hubUnreadsRecommendations) {
                ArrayList<DirectoryEntryData> arrayList3 = new ArrayList();
                for (DirectoryEntryData directoryEntryData : viewState.getAllDirectoryEntryData()) {
                    if (arrayList3.size() >= 2) {
                        break;
                    } else if (!linkedHashSet.contains(Long.valueOf(directoryEntryData.getEntry().e().h()))) {
                        arrayList3.add(directoryEntryData);
                        linkedHashSet.add(Long.valueOf(directoryEntryData.getEntry().e().h()));
                    }
                }
                List<DirectoryEntryData> allDirectoryEntryData = viewState.getAllDirectoryEntryData();
                ArrayList arrayList4 = new ArrayList();
                for (Object obj : allDirectoryEntryData) {
                    if (!linkedHashSet.contains(Long.valueOf(((DirectoryEntryData) obj).getEntry().e().h()))) {
                        arrayList4.add(obj);
                    }
                }
                List<DirectoryEntryData> sortByRanking = DirectoryUtilsKt.sortByRanking(arrayList4, 5 - arrayList3.size());
                if (!sortByRanking.isEmpty()) {
                    arrayList2 = sortByRanking;
                }
                if (!(arrayList2 == null || (directoryItems2 = toDirectoryItems(arrayList2)) == null)) {
                    arrayList.add(new DirectoryChannelItem.SectionHeader(R.string.directory_category_recommended));
                    for (DirectoryEntryData directoryEntryData2 : arrayList3) {
                        arrayList.add(new DirectoryChannelItem.DirectoryItem(directoryEntryData2));
                    }
                    arrayList.addAll(directoryItems2);
                }
            } else {
                List<DirectoryEntryData> allDirectoryEntryData2 = viewState.getAllDirectoryEntryData();
                ArrayList arrayList5 = new ArrayList();
                for (Object obj2 : allDirectoryEntryData2) {
                    if (!linkedHashSet.contains(Long.valueOf(((DirectoryEntryData) obj2).getEntry().e().h()))) {
                        arrayList5.add(obj2);
                    }
                }
                if (!arrayList5.isEmpty()) {
                    arrayList2 = arrayList5;
                }
                if (!(arrayList2 == null || (directoryItems = toDirectoryItems(arrayList2)) == null)) {
                    arrayList.add(new DirectoryChannelItem.SectionHeader(R.string.all_servers));
                    arrayList.addAll(directoryItems);
                }
            }
            this.adapter.setDirectoryChannelItems(arrayList);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(WidgetDirectoriesViewModel.ViewState viewState) {
        if (getArgs().getDirectoryCategory().ordinal() != 0) {
            configureCategoryUi(viewState);
        } else {
            configureHomeUi(viewState);
        }
    }

    private final void maybeAddServerRow(List<DirectoryChannelItem> list, WidgetDirectoriesViewModel.ViewState viewState) {
        if (viewState.getHasAddGuildPermissions()) {
            list.add(DirectoryChannelItem.AddServer.INSTANCE);
        }
    }

    private final List<DirectoryChannelItem.DirectoryItem> toDirectoryItems(List<DirectoryEntryData> list) {
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (DirectoryEntryData directoryEntryData : list) {
            arrayList.add(new DirectoryChannelItem.DirectoryItem(directoryEntryData));
        }
        return arrayList;
    }

    public final WidgetDirectoryChannelAdapter getAdapter() {
        return this.adapter;
    }

    public final DirectoryCategoryArgs getArgs() {
        return (DirectoryCategoryArgs) this.args$delegate.getValue();
    }

    public final WidgetDirectoryCategoryBinding getBinding() {
        return (WidgetDirectoryCategoryBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final WidgetDirectoriesViewModel getViewModel() {
        return (WidgetDirectoriesViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RecyclerView recyclerView = getBinding().f2344b;
        recyclerView.setAdapter(this.adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<WidgetDirectoriesViewModel.ViewState> x2 = getViewModel().observeViewState().x(WidgetDirectoryCategory$onViewBoundOrOnResume$1.INSTANCE);
        m.checkNotNullExpressionValue(x2, "viewModel\n        .obser…   .filter { it != null }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(x2, this, null, 2, null), WidgetDirectoryCategory.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDirectoryCategory$onViewBoundOrOnResume$2(this));
    }
}
