package com.discord.widgets.directories;

import b.d.b.a.a;
import com.discord.api.permission.Permission;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.widgets.directories.WidgetDirectoriesSearchViewModel;
import d0.t.u;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetDirectoriesSearchViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "invoke", "()Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesSearchViewModel$Companion$observeStores$1 extends o implements Function0<WidgetDirectoriesSearchViewModel.ViewState> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreGuildSelected $guildSelectedStore;
    public final /* synthetic */ StoreGuilds $guildsStore;
    public final /* synthetic */ StorePermissions $permissionsStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesSearchViewModel$Companion$observeStores$1(StorePermissions storePermissions, StoreGuilds storeGuilds, StoreGuildSelected storeGuildSelected, long j) {
        super(0);
        this.$permissionsStore = storePermissions;
        this.$guildsStore = storeGuilds;
        this.$guildSelectedStore = storeGuildSelected;
        this.$channelId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetDirectoriesSearchViewModel.ViewState invoke() {
        Map<Long, Long> guildPermissions = this.$permissionsStore.getGuildPermissions();
        Guild guild = this.$guildsStore.getGuild(this.$guildSelectedStore.getSelectedGuildId());
        Map<Long, Long> permissionsByChannel = this.$permissionsStore.getPermissionsByChannel();
        Set<Long> keySet = this.$guildsStore.getGuilds().keySet();
        Collection<Guild> values = this.$guildsStore.getGuilds().values();
        ArrayList<Guild> arrayList = new ArrayList();
        for (Object obj : values) {
            if (PermissionUtils.can(8L, (Long) a.d((Guild) obj, guildPermissions))) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        for (Guild guild2 : arrayList) {
            arrayList2.add(Long.valueOf(guild2.getId()));
        }
        return new WidgetDirectoriesSearchViewModel.ViewState(guild, null, null, keySet, u.toSet(arrayList2), PermissionUtils.can(Permission.SEND_MESSAGES, permissionsByChannel.get(Long.valueOf(this.$channelId))), null, 70, null);
    }
}
