package com.discord.widgets.directories;

import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.stores.utilities.RestCallState;
import com.discord.widgets.directories.WidgetDirectoriesSearchViewModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetDirectoriesSearchViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/stores/utilities/RestCallState;", "", "Lcom/discord/api/directory/DirectoryEntryGuild;", "entriesState", "", "invoke", "(Lcom/discord/stores/utilities/RestCallState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesSearchViewModel$searchForDirectories$1 extends o implements Function1<RestCallState<? extends List<? extends DirectoryEntryGuild>>, Unit> {
    public final /* synthetic */ CharSequence $query;
    public final /* synthetic */ WidgetDirectoriesSearchViewModel.ViewState $viewState;
    public final /* synthetic */ WidgetDirectoriesSearchViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesSearchViewModel$searchForDirectories$1(WidgetDirectoriesSearchViewModel widgetDirectoriesSearchViewModel, WidgetDirectoriesSearchViewModel.ViewState viewState, CharSequence charSequence) {
        super(1);
        this.this$0 = widgetDirectoriesSearchViewModel;
        this.$viewState = viewState;
        this.$query = charSequence;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestCallState<? extends List<? extends DirectoryEntryGuild>> restCallState) {
        invoke2((RestCallState<? extends List<DirectoryEntryGuild>>) restCallState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestCallState<? extends List<DirectoryEntryGuild>> restCallState) {
        m.checkNotNullParameter(restCallState, "entriesState");
        WidgetDirectoriesSearchViewModel widgetDirectoriesSearchViewModel = this.this$0;
        WidgetDirectoriesSearchViewModel.ViewState viewState = this.$viewState;
        String obj = this.$query.toString();
        List<DirectoryEntryGuild> invoke = restCallState.invoke();
        if (invoke == null) {
            invoke = this.$viewState.getDirectories();
        }
        widgetDirectoriesSearchViewModel.updateViewState(WidgetDirectoriesSearchViewModel.ViewState.copy$default(viewState, null, obj, invoke, null, null, false, restCallState, 57, null));
    }
}
