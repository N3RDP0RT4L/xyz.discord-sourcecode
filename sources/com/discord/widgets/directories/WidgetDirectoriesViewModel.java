package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreDirectories;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreReadStates;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import com.discord.utilities.directories.DirectoryUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.widgets.guilds.join.GuildJoinHelperKt;
import d0.g;
import d0.t.n0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
/* compiled from: WidgetDirectoriesViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 $2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002$%BI\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u001a\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u001c\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u001e\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0015\u0012\u000e\b\u0002\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020 ¢\u0006\u0004\b\"\u0010#J\r\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0004\u0010\u0005J)\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u00062\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010\u000b\u001a\u00020\b¢\u0006\u0004\b\r\u0010\u000eJ%\u0010\u0010\u001a\u00020\f2\n\u0010\n\u001a\u00060\bj\u0002`\t2\n\u0010\u000b\u001a\u00060\bj\u0002`\u000f¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0016\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019¨\u0006&"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "", "getHubName", "()Ljava/lang/String;", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", ModelAuditLogEntry.CHANGE_KEY_ID, "directoryChannelId", "", "joinGuild", "(Landroid/content/Context;JJ)V", "Lcom/discord/primitives/ChannelId;", "removeGuild", "(JJ)V", "Lcom/discord/stores/StoreDirectories;", "directoriesStore", "Lcom/discord/stores/StoreDirectories;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "getRestAPI", "()Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreChannelsSelected;", "channelsSelectedStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreReadStates;", "readStatesStore", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreDirectories;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreReadStates;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V", "Companion", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final StoreDirectories directoriesStore;
    private final RestAPI restAPI;

    /* compiled from: WidgetDirectoriesViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.directories.WidgetDirectoriesViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T, R> implements b<ViewState, Boolean> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Boolean call(ViewState viewState) {
            Channel channel = viewState.getChannel();
            boolean z2 = true;
            if (channel == null || !ChannelUtils.o(channel)) {
                z2 = false;
            }
            return Boolean.valueOf(z2);
        }
    }

    /* compiled from: WidgetDirectoriesViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "p1", "", "invoke", "(Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.directories.WidgetDirectoriesViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<ViewState, Unit> {
        public AnonymousClass2(WidgetDirectoriesViewModel widgetDirectoriesViewModel) {
            super(1, widgetDirectoriesViewModel, WidgetDirectoriesViewModel.class, "updateViewState", "updateViewState(Ljava/lang/Object;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ViewState viewState) {
            invoke2(viewState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ViewState viewState) {
            m.checkNotNullParameter(viewState, "p1");
            ((WidgetDirectoriesViewModel) this.receiver).updateViewState(viewState);
        }
    }

    /* compiled from: WidgetDirectoriesViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.directories.WidgetDirectoriesViewModel$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3<T, R> implements b<Channel, Boolean> {
        public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

        public final Boolean call(Channel channel) {
            boolean z2 = true;
            if (channel == null || !ChannelUtils.o(channel)) {
                z2 = false;
            }
            return Boolean.valueOf(z2);
        }
    }

    /* compiled from: WidgetDirectoriesViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.directories.WidgetDirectoriesViewModel$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass4 extends o implements Function1<Channel, Unit> {
        public AnonymousClass4() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
            invoke2(channel);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Channel channel) {
            WidgetDirectoriesViewModel.this.directoriesStore.fetchDirectoriesForChannel(channel.h());
            WidgetDirectoriesViewModel.this.directoriesStore.fetchEntryCountsForChannel(channel.h());
            WidgetDirectoriesViewModel.this.directoriesStore.fetchGuildScheduledEventsForChannel(channel.f(), channel.h());
        }
    }

    /* compiled from: WidgetDirectoriesViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0018\u0010\u0003\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/lang/Long;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.directories.WidgetDirectoriesViewModel$6  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass6 extends o implements Function1<Long, Unit> {
        public final /* synthetic */ StoreReadStates $readStatesStore;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass6(StoreReadStates storeReadStates) {
            super(1);
            this.$readStatesStore = storeReadStates;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Long l) {
            invoke2(l);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Long l) {
            this.$readStatesStore.markAsRead(l);
        }
    }

    /* compiled from: WidgetDirectoriesViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013JE\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$Companion;", "", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuildSelected;", "guildSelectedStore", "Lcom/discord/stores/StoreChannelsSelected;", "channelsSelectedStore", "Lcom/discord/stores/StoreDirectories;", "directoriesStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lrx/Observable;", "Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "observeStores", "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreDirectories;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<ViewState> observeStores(ObservationDeck observationDeck, StoreGuilds storeGuilds, StoreGuildSelected storeGuildSelected, StoreChannelsSelected storeChannelsSelected, StoreDirectories storeDirectories, StorePermissions storePermissions) {
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeGuilds, storeGuildSelected, storeDirectories, storeChannelsSelected, storePermissions}, false, null, null, new WidgetDirectoriesViewModel$Companion$observeStores$1(storeChannelsSelected, storeGuildSelected, storeGuilds, storePermissions, storeDirectories), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetDirectoriesViewModel() {
        this(null, null, null, null, null, null, 63, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetDirectoriesViewModel(com.discord.stores.StoreDirectories r13, com.discord.stores.StoreChannelsSelected r14, com.discord.stores.StoreChannels r15, com.discord.stores.StoreReadStates r16, com.discord.utilities.rest.RestAPI r17, rx.Observable r18, int r19, kotlin.jvm.internal.DefaultConstructorMarker r20) {
        /*
            r12 = this;
            r0 = r19 & 1
            if (r0 == 0) goto Lb
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreDirectories r0 = r0.getDirectories()
            goto Lc
        Lb:
            r0 = r13
        Lc:
            r1 = r19 & 2
            if (r1 == 0) goto L17
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannelsSelected r1 = r1.getChannelsSelected()
            goto L18
        L17:
            r1 = r14
        L18:
            r2 = r19 & 4
            if (r2 == 0) goto L23
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r2 = r2.getChannels()
            goto L24
        L23:
            r2 = r15
        L24:
            r3 = r19 & 8
            if (r3 == 0) goto L2f
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreReadStates r3 = r3.getReadStates()
            goto L31
        L2f:
            r3 = r16
        L31:
            r4 = r19 & 16
            if (r4 == 0) goto L3c
            com.discord.utilities.rest.RestAPI$Companion r4 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r4 = r4.getApi()
            goto L3e
        L3c:
            r4 = r17
        L3e:
            r5 = r19 & 32
            if (r5 == 0) goto L6e
            com.discord.widgets.directories.WidgetDirectoriesViewModel$Companion r5 = com.discord.widgets.directories.WidgetDirectoriesViewModel.Companion
            com.discord.stores.updates.ObservationDeck r6 = com.discord.stores.updates.ObservationDeckProvider.get()
            com.discord.stores.StoreStream$Companion r7 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r8 = r7.getGuilds()
            com.discord.stores.StoreGuildSelected r9 = r7.getGuildSelected()
            com.discord.stores.StoreChannelsSelected r10 = r7.getChannelsSelected()
            com.discord.stores.StoreDirectories r11 = r7.getDirectories()
            com.discord.stores.StorePermissions r7 = r7.getPermissions()
            r13 = r5
            r14 = r6
            r15 = r8
            r16 = r9
            r17 = r10
            r18 = r11
            r19 = r7
            rx.Observable r5 = com.discord.widgets.directories.WidgetDirectoriesViewModel.Companion.access$observeStores(r13, r14, r15, r16, r17, r18, r19)
            goto L70
        L6e:
            r5 = r18
        L70:
            r13 = r12
            r14 = r0
            r15 = r1
            r16 = r2
            r17 = r3
            r18 = r4
            r19 = r5
            r13.<init>(r14, r15, r16, r17, r18, r19)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.directories.WidgetDirectoriesViewModel.<init>(com.discord.stores.StoreDirectories, com.discord.stores.StoreChannelsSelected, com.discord.stores.StoreChannels, com.discord.stores.StoreReadStates, com.discord.utilities.rest.RestAPI, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final String getHubName() {
        Guild guild;
        ViewState viewState = getViewState();
        String name = (viewState == null || (guild = viewState.getGuild()) == null) ? null : guild.getName();
        return name != null ? name : "";
    }

    public final RestAPI getRestAPI() {
        return this.restAPI;
    }

    public final void joinGuild(Context context, long j, long j2) {
        m.checkNotNullParameter(context, "context");
        GuildJoinHelperKt.joinGuild(context, j, false, (r27 & 8) != 0 ? null : null, (r27 & 16) != 0 ? null : Long.valueOf(j2), (r27 & 32) != 0 ? null : this.restAPI.jsonObjectOf(d0.o.to("source", DirectoryUtils.JOIN_GUILD_SOURCE)), WidgetDirectoriesViewModel.class, (r27 & 128) != 0 ? null : null, (r27 & 256) != 0 ? null : null, (r27 & 512) != 0 ? null : null, WidgetDirectoriesViewModel$joinGuild$1.INSTANCE);
    }

    public final void removeGuild(long j, long j2) {
        this.directoriesStore.removeServerFromDirectory(j2, j);
    }

    /* compiled from: WidgetDirectoriesViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0083\u0001\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\b\u0012\u0012\b\u0002\u0010\u001c\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\b\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u000e\u0012\u0014\b\u0002\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011\u0012\u001a\b\u0002\u0010\u001f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00170\u00160\u0011¢\u0006\u0004\bE\u0010FJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\u000b\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u001a\u0010\r\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\bHÆ\u0003¢\u0006\u0004\b\r\u0010\fJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001c\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\"\u0010\u0018\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00170\u00160\u0011HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0015J\u008c\u0001\u0010 \u001a\u00020\u00002\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\b2\u0012\b\u0002\u0010\u001c\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\b2\b\b\u0002\u0010\u001d\u001a\u00020\u000e2\u0014\b\u0002\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u00112\u001a\b\u0002\u0010\u001f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00170\u00160\u0011HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b%\u0010&J\u001a\u0010(\u001a\u00020\u000e2\b\u0010'\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b(\u0010)R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b+\u0010\u0004R/\u00102\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u00020\u00170,0\u00128F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00103\u001a\u0004\b4\u0010\fR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00105\u001a\u0004\b6\u0010\u0007R+\u0010\u001f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00170\u00160\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00107\u001a\u0004\b8\u0010\u0015R%\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00107\u001a\u0004\b9\u0010\u0015R#\u0010\u001c\u001a\f\u0012\b\u0012\u00060\tj\u0002`\n0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00103\u001a\u0004\b:\u0010\fR\u0019\u0010\u001d\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010;\u001a\u0004\b<\u0010\u0010R/\u0010A\u001a\u0014\u0012\u0004\u0012\u00020\u0017\u0012\n\u0012\b\u0012\u0004\u0012\u00020=0\u00120\u00168F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b>\u0010/\u001a\u0004\b?\u0010@R#\u0010D\u001a\b\u0012\u0004\u0012\u00020=0\u00128F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\bB\u0010/\u001a\u0004\bC\u00101¨\u0006G"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/GuildId;", "component3", "()Ljava/util/Set;", "component4", "", "component5", "()Z", "Lcom/discord/stores/utilities/RestCallState;", "", "Lcom/discord/api/directory/DirectoryEntryGuild;", "component6", "()Lcom/discord/stores/utilities/RestCallState;", "", "", "component7", "guild", "channel", "joinedGuildIds", "adminGuildIds", "hasAddGuildPermissions", "directories", "tabs", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Set;Ljava/util/Set;ZLcom/discord/stores/utilities/RestCallState;Lcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lkotlin/Pair;", "Lcom/discord/models/hubs/DirectoryEntryCategory;", "mappedTabs$delegate", "Lkotlin/Lazy;", "getMappedTabs", "()Ljava/util/List;", "mappedTabs", "Ljava/util/Set;", "getJoinedGuildIds", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/stores/utilities/RestCallState;", "getTabs", "getDirectories", "getAdminGuildIds", "Z", "getHasAddGuildPermissions", "Lcom/discord/widgets/directories/DirectoryEntryData;", "directoryEntryData$delegate", "getDirectoryEntryData", "()Ljava/util/Map;", "directoryEntryData", "allDirectoryEntryData$delegate", "getAllDirectoryEntryData", "allDirectoryEntryData", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Set;Ljava/util/Set;ZLcom/discord/stores/utilities/RestCallState;Lcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Set<Long> adminGuildIds;
        private final Lazy allDirectoryEntryData$delegate;
        private final Channel channel;
        private final RestCallState<List<DirectoryEntryGuild>> directories;
        private final Lazy directoryEntryData$delegate;
        private final Guild guild;
        private final boolean hasAddGuildPermissions;
        private final Set<Long> joinedGuildIds;
        private final Lazy mappedTabs$delegate;
        private final RestCallState<Map<Integer, Integer>> tabs;

        public ViewState() {
            this(null, null, null, null, false, null, null, Opcodes.LAND, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(Guild guild, Channel channel, Set<Long> set, Set<Long> set2, boolean z2, RestCallState<? extends List<DirectoryEntryGuild>> restCallState, RestCallState<? extends Map<Integer, Integer>> restCallState2) {
            m.checkNotNullParameter(set, "joinedGuildIds");
            m.checkNotNullParameter(set2, "adminGuildIds");
            m.checkNotNullParameter(restCallState, "directories");
            m.checkNotNullParameter(restCallState2, "tabs");
            this.guild = guild;
            this.channel = channel;
            this.joinedGuildIds = set;
            this.adminGuildIds = set2;
            this.hasAddGuildPermissions = z2;
            this.directories = restCallState;
            this.tabs = restCallState2;
            this.allDirectoryEntryData$delegate = g.lazy(new WidgetDirectoriesViewModel$ViewState$allDirectoryEntryData$2(this));
            this.directoryEntryData$delegate = g.lazy(new WidgetDirectoriesViewModel$ViewState$directoryEntryData$2(this));
            this.mappedTabs$delegate = g.lazy(new WidgetDirectoriesViewModel$ViewState$mappedTabs$2(this));
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, Guild guild, Channel channel, Set set, Set set2, boolean z2, RestCallState restCallState, RestCallState restCallState2, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = viewState.guild;
            }
            if ((i & 2) != 0) {
                channel = viewState.channel;
            }
            Channel channel2 = channel;
            Set<Long> set3 = set;
            if ((i & 4) != 0) {
                set3 = viewState.joinedGuildIds;
            }
            Set set4 = set3;
            Set<Long> set5 = set2;
            if ((i & 8) != 0) {
                set5 = viewState.adminGuildIds;
            }
            Set set6 = set5;
            if ((i & 16) != 0) {
                z2 = viewState.hasAddGuildPermissions;
            }
            boolean z3 = z2;
            RestCallState<List<DirectoryEntryGuild>> restCallState3 = restCallState;
            if ((i & 32) != 0) {
                restCallState3 = viewState.directories;
            }
            RestCallState restCallState4 = restCallState3;
            RestCallState<Map<Integer, Integer>> restCallState5 = restCallState2;
            if ((i & 64) != 0) {
                restCallState5 = viewState.tabs;
            }
            return viewState.copy(guild, channel2, set4, set6, z3, restCallState4, restCallState5);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final Set<Long> component3() {
            return this.joinedGuildIds;
        }

        public final Set<Long> component4() {
            return this.adminGuildIds;
        }

        public final boolean component5() {
            return this.hasAddGuildPermissions;
        }

        public final RestCallState<List<DirectoryEntryGuild>> component6() {
            return this.directories;
        }

        public final RestCallState<Map<Integer, Integer>> component7() {
            return this.tabs;
        }

        public final ViewState copy(Guild guild, Channel channel, Set<Long> set, Set<Long> set2, boolean z2, RestCallState<? extends List<DirectoryEntryGuild>> restCallState, RestCallState<? extends Map<Integer, Integer>> restCallState2) {
            m.checkNotNullParameter(set, "joinedGuildIds");
            m.checkNotNullParameter(set2, "adminGuildIds");
            m.checkNotNullParameter(restCallState, "directories");
            m.checkNotNullParameter(restCallState2, "tabs");
            return new ViewState(guild, channel, set, set2, z2, restCallState, restCallState2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.guild, viewState.guild) && m.areEqual(this.channel, viewState.channel) && m.areEqual(this.joinedGuildIds, viewState.joinedGuildIds) && m.areEqual(this.adminGuildIds, viewState.adminGuildIds) && this.hasAddGuildPermissions == viewState.hasAddGuildPermissions && m.areEqual(this.directories, viewState.directories) && m.areEqual(this.tabs, viewState.tabs);
        }

        public final Set<Long> getAdminGuildIds() {
            return this.adminGuildIds;
        }

        public final List<DirectoryEntryData> getAllDirectoryEntryData() {
            return (List) this.allDirectoryEntryData$delegate.getValue();
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final RestCallState<List<DirectoryEntryGuild>> getDirectories() {
            return this.directories;
        }

        public final Map<Integer, List<DirectoryEntryData>> getDirectoryEntryData() {
            return (Map) this.directoryEntryData$delegate.getValue();
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final boolean getHasAddGuildPermissions() {
            return this.hasAddGuildPermissions;
        }

        public final Set<Long> getJoinedGuildIds() {
            return this.joinedGuildIds;
        }

        public final List<Pair<DirectoryEntryCategory, Integer>> getMappedTabs() {
            return (List) this.mappedTabs$delegate.getValue();
        }

        public final RestCallState<Map<Integer, Integer>> getTabs() {
            return this.tabs;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            Set<Long> set = this.joinedGuildIds;
            int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
            Set<Long> set2 = this.adminGuildIds;
            int hashCode4 = (hashCode3 + (set2 != null ? set2.hashCode() : 0)) * 31;
            boolean z2 = this.hasAddGuildPermissions;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode4 + i2) * 31;
            RestCallState<List<DirectoryEntryGuild>> restCallState = this.directories;
            int hashCode5 = (i4 + (restCallState != null ? restCallState.hashCode() : 0)) * 31;
            RestCallState<Map<Integer, Integer>> restCallState2 = this.tabs;
            if (restCallState2 != null) {
                i = restCallState2.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(guild=");
            R.append(this.guild);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", joinedGuildIds=");
            R.append(this.joinedGuildIds);
            R.append(", adminGuildIds=");
            R.append(this.adminGuildIds);
            R.append(", hasAddGuildPermissions=");
            R.append(this.hasAddGuildPermissions);
            R.append(", directories=");
            R.append(this.directories);
            R.append(", tabs=");
            R.append(this.tabs);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ ViewState(Guild guild, Channel channel, Set set, Set set2, boolean z2, RestCallState restCallState, RestCallState restCallState2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : guild, (i & 2) == 0 ? channel : null, (i & 4) != 0 ? n0.emptySet() : set, (i & 8) != 0 ? n0.emptySet() : set2, (i & 16) != 0 ? true : z2, (i & 32) != 0 ? Default.INSTANCE : restCallState, (i & 64) != 0 ? Default.INSTANCE : restCallState2);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesViewModel(StoreDirectories storeDirectories, StoreChannelsSelected storeChannelsSelected, final StoreChannels storeChannels, StoreReadStates storeReadStates, RestAPI restAPI, Observable<ViewState> observable) {
        super(null);
        m.checkNotNullParameter(storeDirectories, "directoriesStore");
        m.checkNotNullParameter(storeChannelsSelected, "channelsSelectedStore");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeReadStates, "readStatesStore");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observable, "storeObservable");
        this.directoriesStore = storeDirectories;
        this.restAPI = restAPI;
        Observable x2 = ObservableExtensionsKt.computationLatest(observable).x(AnonymousClass1.INSTANCE);
        m.checkNotNullExpressionValue(x2, "storeObservable\n        …?.isDirectory() == true }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(x2, this, null, 2, null), WidgetDirectoriesViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(this));
        Observable<Channel> q = storeChannelsSelected.observeSelectedChannel().q();
        m.checkNotNullExpressionValue(q, "channelsSelectedStore\n  …  .distinctUntilChanged()");
        Observable<R> F = q.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable x3 = F.x(AnonymousClass3.INSTANCE);
        m.checkNotNullExpressionValue(x3, "channelsSelectedStore\n  …?.isDirectory() == true }");
        ObservableExtensionsKt.appSubscribe(x3, WidgetDirectoriesViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass4());
        Observable<Long> x4 = storeChannelsSelected.observePreviousId().q().x(new b<Long, Boolean>() { // from class: com.discord.widgets.directories.WidgetDirectoriesViewModel.5
            public final Boolean call(Long l) {
                StoreChannels storeChannels2 = StoreChannels.this;
                m.checkNotNullExpressionValue(l, ModelAuditLogEntry.CHANGE_KEY_ID);
                Channel findChannelById = storeChannels2.findChannelById(l.longValue());
                boolean z2 = true;
                if (findChannelById == null || !ChannelUtils.o(findChannelById)) {
                    z2 = false;
                }
                return Boolean.valueOf(z2);
            }
        });
        m.checkNotNullExpressionValue(x4, "channelsSelectedStore.ob…?.isDirectory() == true }");
        ObservableExtensionsKt.appSubscribe(x4, WidgetDirectoriesViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass6(storeReadStates));
    }
}
