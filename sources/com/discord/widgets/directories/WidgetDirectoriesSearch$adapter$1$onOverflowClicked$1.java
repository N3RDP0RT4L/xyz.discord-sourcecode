package com.discord.widgets.directories;

import com.discord.api.directory.DirectoryEntryGuild;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetDirectoriesSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesSearch$adapter$1$onOverflowClicked$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ DirectoryEntryGuild $directoryEntry;
    public final /* synthetic */ WidgetDirectoriesSearch$adapter$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesSearch$adapter$1$onOverflowClicked$1(WidgetDirectoriesSearch$adapter$1 widgetDirectoriesSearch$adapter$1, DirectoryEntryGuild directoryEntryGuild, long j) {
        super(0);
        this.this$0 = widgetDirectoriesSearch$adapter$1;
        this.$directoryEntry = directoryEntryGuild;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.this$0.getViewModel().removeGuild(this.$directoryEntry.e().h(), this.$channelId);
    }
}
