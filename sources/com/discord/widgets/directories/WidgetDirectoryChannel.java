package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.viewpager2.widget.ViewPager2;
import b.a.d.e0;
import b.a.d.j;
import b.a.i.y4;
import b.a.k.b;
import b.a.o.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetDirectoryChannelBinding;
import com.discord.models.guild.Guild;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.stores.StoreStream;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.hubs.HubUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.view.recycler.ViewPager2ExtensionsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.directories.ServerDiscoveryHeader;
import com.discord.widgets.directories.WidgetDirectoriesViewModel;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShare;
import com.discord.widgets.hubs.WidgetHubAddServer;
import com.google.android.material.tabs.TabLayout;
import d0.g;
import d0.t.o;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetDirectoryChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 ,2\u00020\u0001:\u0001,B\u0007¢\u0006\u0004\b+\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0011\u0010\u0010J!\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u00072\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b\u0013\u0010\u0014R$\u0010\u0018\u001a\u0010\u0012\f\u0012\n \u0017*\u0004\u0018\u00010\u00160\u00160\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u001d\u0010\u001f\u001a\u00020\u001a8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001d\u0010$\u001a\u00020 8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\u001c\u001a\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u001d\u0010\f\u001a\u00020\u000b8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*¨\u0006-"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoryChannel;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "state", "", "configureUi", "(Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;)V", "", "showTabLayout", "toggleHeaderMargins", "(Z)V", "Lcom/discord/databinding/WidgetDirectoryChannelBinding;", "binding", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetDirectoryChannelBinding;)V", "onViewBoundOrOnResume", "()V", "initializeUI", "bind", "bindGestureObservers", "(ZLcom/discord/databinding/WidgetDirectoryChannelBinding;)V", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "activityResult", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/widgets/directories/WidgetDirectoryChannelViewPagerAdapter;", "adapter$delegate", "Lkotlin/Lazy;", "getAdapter", "()Lcom/discord/widgets/directories/WidgetDirectoryChannelViewPagerAdapter;", "adapter", "Lcom/discord/widgets/directories/WidgetDirectoriesViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/directories/WidgetDirectoriesViewModel;", "viewModel", "hasBound", "Z", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetDirectoryChannelBinding;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoryChannel extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetDirectoryChannel.class, "binding", "getBinding()Lcom/discord/databinding/WidgetDirectoryChannelBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int TAB_LAYOUT_MARGINS = DimenUtils.dpToPixels(48);
    private boolean hasBound;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetDirectoryChannel$binding$2.INSTANCE, new WidgetDirectoryChannel$binding$3(this));
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetDirectoriesViewModel.class), new WidgetDirectoryChannel$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetDirectoryChannel$viewModel$2.INSTANCE));
    private final Lazy adapter$delegate = g.lazy(new WidgetDirectoryChannel$adapter$2(this));
    private final ActivityResultLauncher<Intent> activityResult = HubUtilsKt.getAddServerActivityResultHandler(this);

    /* compiled from: WidgetDirectoryChannel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoryChannel$Companion;", "", "", "TAB_LAYOUT_MARGINS", "I", "getTAB_LAYOUT_MARGINS", "()I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final int getTAB_LAYOUT_MARGINS() {
            return WidgetDirectoryChannel.TAB_LAYOUT_MARGINS;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetDirectoryChannel() {
        super(R.layout.widget_directory_channel);
    }

    public static /* synthetic */ void bindGestureObservers$default(WidgetDirectoryChannel widgetDirectoryChannel, boolean z2, WidgetDirectoryChannelBinding widgetDirectoryChannelBinding, int i, Object obj) {
        if ((i & 2) != 0) {
            widgetDirectoryChannelBinding = null;
        }
        widgetDirectoryChannel.bindGestureObservers(z2, widgetDirectoryChannelBinding);
    }

    public final void configureUi(final WidgetDirectoriesViewModel.ViewState viewState) {
        CharSequence e;
        final Guild guild = viewState.getGuild();
        int i = 0;
        if (guild != null) {
            getBinding().f.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.WidgetDirectoryChannel$configureUi$$inlined$let$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetGuildInviteShare.Companion companion = WidgetGuildInviteShare.Companion;
                    Context x2 = a.x(view, "it", "it.context");
                    FragmentManager childFragmentManager = this.getChildFragmentManager();
                    m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                    companion.launch(x2, childFragmentManager, Guild.this.getId(), (r22 & 8) != 0 ? null : null, (r22 & 16) != 0 ? false : false, (r22 & 32) != 0 ? null : null, (r22 & 64) != 0 ? null : null, "Guild Header");
                }
            });
            TextView textView = getBinding().f.f231b;
            m.checkNotNullExpressionValue(textView, "binding.widgetDirectoryC….itemDirectoryEmptyHeader");
            e = b.e(this, R.string.hub_directory_channel_empty_title, new Object[]{guild.getName()}, (r4 & 4) != 0 ? b.a.j : null);
            textView.setText(e);
        }
        final ServerDiscoveryHeader serverDiscoveryHeader = getBinding().c;
        serverDiscoveryHeader.setButtonOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.WidgetDirectoryChannel$configureUi$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Channel channel = viewState.getChannel();
                if (channel != null) {
                    long h = channel.h();
                    Context context = ServerDiscoveryHeader.this.getContext();
                    m.checkNotNullExpressionValue(context, "context");
                    j.d(context, WidgetDirectoriesSearch.class, new DirectoriesSearchArgs(h));
                }
            }
        });
        List<DirectoryEntryGuild> invoke = viewState.getDirectories().invoke();
        boolean z2 = invoke != null && (invoke.isEmpty() ^ true);
        ServerDiscoveryHeader serverDiscoveryHeader2 = getBinding().c;
        m.checkNotNullExpressionValue(serverDiscoveryHeader2, "binding.header");
        serverDiscoveryHeader2.setVisibility(z2 ? 0 : 8);
        ViewPager2 viewPager2 = getBinding().e;
        m.checkNotNullExpressionValue(viewPager2, "binding.viewPager");
        viewPager2.setVisibility(z2 ? 0 : 8);
        y4 y4Var = getBinding().f;
        m.checkNotNullExpressionValue(y4Var, "binding.widgetDirectoryChannelEmpty");
        LinearLayout linearLayout = y4Var.a;
        m.checkNotNullExpressionValue(linearLayout, "binding.widgetDirectoryChannelEmpty.root");
        linearLayout.setVisibility(z2 ^ true ? 0 : 8);
        Map<Integer, Integer> invoke2 = viewState.getTabs().invoke();
        boolean z3 = !(invoke2 == null || invoke2.isEmpty());
        toggleHeaderMargins(z3);
        TabLayout tabLayout = getBinding().d;
        m.checkNotNullExpressionValue(tabLayout, "binding.tabs");
        if (!z3) {
            i = 8;
        }
        tabLayout.setVisibility(i);
        WidgetDirectoryChannelViewPagerAdapter adapter = getAdapter();
        List<Pair<DirectoryEntryCategory, Integer>> mappedTabs = viewState.getMappedTabs();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(mappedTabs, 10));
        Iterator<T> it = mappedTabs.iterator();
        while (it.hasNext()) {
            arrayList.add((DirectoryEntryCategory) ((Pair) it.next()).getFirst());
        }
        adapter.setTabs(u.toList(arrayList));
    }

    public final void onViewBindingDestroy(WidgetDirectoryChannelBinding widgetDirectoryChannelBinding) {
        bindGestureObservers(false, widgetDirectoryChannelBinding);
    }

    private final void toggleHeaderMargins(boolean z2) {
        int i = z2 ? TAB_LAYOUT_MARGINS : 0;
        ServerDiscoveryHeader serverDiscoveryHeader = getBinding().c;
        m.checkNotNullExpressionValue(serverDiscoveryHeader, "binding.header");
        ViewGroup.LayoutParams layoutParams = serverDiscoveryHeader.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.bottomMargin = i;
        serverDiscoveryHeader.setLayoutParams(marginLayoutParams);
        Toolbar toolbar = getBinding().f2345b;
        m.checkNotNullExpressionValue(toolbar, "binding.actionBarToolbar");
        ViewGroup.LayoutParams layoutParams2 = toolbar.getLayoutParams();
        Objects.requireNonNull(layoutParams2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) layoutParams2;
        marginLayoutParams2.bottomMargin = i;
        toolbar.setLayoutParams(marginLayoutParams2);
    }

    public final void bindGestureObservers(boolean z2, WidgetDirectoryChannelBinding widgetDirectoryChannelBinding) {
        if (widgetDirectoryChannelBinding == null) {
            widgetDirectoryChannelBinding = getBinding();
        }
        if (!this.hasBound && z2) {
            b.a.o.b a = b.C0038b.a();
            TabLayout tabLayout = widgetDirectoryChannelBinding.d;
            m.checkNotNullExpressionValue(tabLayout, "nonNullBinding.tabs");
            a.b(tabLayout);
            b.a.o.b a2 = b.C0038b.a();
            ViewPager2 viewPager2 = widgetDirectoryChannelBinding.e;
            m.checkNotNullExpressionValue(viewPager2, "nonNullBinding.viewPager");
            a2.b(viewPager2);
            this.hasBound = true;
        } else if (!z2) {
            b.a.o.b a3 = b.C0038b.a();
            TabLayout tabLayout2 = widgetDirectoryChannelBinding.d;
            m.checkNotNullExpressionValue(tabLayout2, "nonNullBinding.tabs");
            a3.c(tabLayout2);
            b.a.o.b a4 = b.C0038b.a();
            ViewPager2 viewPager22 = widgetDirectoryChannelBinding.e;
            m.checkNotNullExpressionValue(viewPager22, "nonNullBinding.viewPager");
            a4.c(viewPager22);
            this.hasBound = false;
        }
    }

    public final WidgetDirectoryChannelViewPagerAdapter getAdapter() {
        return (WidgetDirectoryChannelViewPagerAdapter) this.adapter$delegate.getValue();
    }

    public final WidgetDirectoryChannelBinding getBinding() {
        return (WidgetDirectoryChannelBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final WidgetDirectoriesViewModel getViewModel() {
        return (WidgetDirectoriesViewModel) this.viewModel$delegate.getValue();
    }

    @SuppressLint({"SetTextI18n"})
    public final void initializeUI() {
        getBinding().f.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.WidgetDirectoryChannel$initializeUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ActivityResultLauncher<Intent> activityResultLauncher;
                j jVar = j.g;
                Context requireContext = WidgetDirectoryChannel.this.requireContext();
                activityResultLauncher = WidgetDirectoryChannel.this.activityResult;
                jVar.f(requireContext, activityResultLauncher, WidgetHubAddServer.class, null);
            }
        });
        ServerDiscoveryHeader serverDiscoveryHeader = getBinding().c;
        serverDiscoveryHeader.setTitle(getString(R.string.hub_directory_search_title));
        serverDiscoveryHeader.setDescription(getString(R.string.hub_directory_search_subtitle));
        getBinding().e.setAdapter(getAdapter());
        TabLayout tabLayout = getBinding().d;
        ViewPager2 viewPager2 = getBinding().e;
        m.checkNotNullExpressionValue(viewPager2, "binding.viewPager");
        ViewPager2ExtensionsKt.setUpWithViewPager2(tabLayout, viewPager2, new WidgetDirectoryChannel$initializeUI$$inlined$apply$lambda$1(tabLayout, this));
        bindGestureObservers$default(this, true, null, 2, null);
        Observable<Long> q = StoreStream.Companion.getGuildSelected().observeSelectedGuildId().q();
        m.checkNotNullExpressionValue(q, "StoreStream\n        .get…  .distinctUntilChanged()");
        Observable<R> F = q.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(F), WidgetDirectoryChannel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDirectoryChannel$initializeUI$5(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<WidgetDirectoriesViewModel.ViewState> y2 = getViewModel().observeViewState().x(WidgetDirectoryChannel$onViewBoundOrOnResume$1.INSTANCE).y();
        m.checkNotNullExpressionValue(y2, "viewModel\n        .obser…= null }\n        .first()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(y2, this, null, 2, null), WidgetDirectoryChannel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDirectoryChannel$onViewBoundOrOnResume$2(this));
        Observable<WidgetDirectoriesViewModel.ViewState> x2 = getViewModel().observeViewState().x(WidgetDirectoryChannel$onViewBoundOrOnResume$3.INSTANCE);
        m.checkNotNullExpressionValue(x2, "viewModel\n        .obser…   .filter { it != null }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(x2, this, null, 2, null), WidgetDirectoryChannel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDirectoryChannel$onViewBoundOrOnResume$4(this));
    }
}
