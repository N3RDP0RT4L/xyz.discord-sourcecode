package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import com.discord.models.hubs.DirectoryEntryCategory;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetDirectoryCategory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0087\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u0013\u0010\rJ \u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/directories/DirectoryCategoryArgs;", "Landroid/os/Parcelable;", "Lcom/discord/models/hubs/DirectoryEntryCategory;", "component1", "()Lcom/discord/models/hubs/DirectoryEntryCategory;", "directoryCategory", "copy", "(Lcom/discord/models/hubs/DirectoryEntryCategory;)Lcom/discord/widgets/directories/DirectoryCategoryArgs;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Lcom/discord/models/hubs/DirectoryEntryCategory;", "getDirectoryCategory", HookHelper.constructorName, "(Lcom/discord/models/hubs/DirectoryEntryCategory;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DirectoryCategoryArgs implements Parcelable {
    public static final Parcelable.Creator<DirectoryCategoryArgs> CREATOR = new Creator();
    private final DirectoryEntryCategory directoryCategory;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<DirectoryCategoryArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final DirectoryCategoryArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            return new DirectoryCategoryArgs((DirectoryEntryCategory) Enum.valueOf(DirectoryEntryCategory.class, parcel.readString()));
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final DirectoryCategoryArgs[] newArray(int i) {
            return new DirectoryCategoryArgs[i];
        }
    }

    public DirectoryCategoryArgs(DirectoryEntryCategory directoryEntryCategory) {
        m.checkNotNullParameter(directoryEntryCategory, "directoryCategory");
        this.directoryCategory = directoryEntryCategory;
    }

    public static /* synthetic */ DirectoryCategoryArgs copy$default(DirectoryCategoryArgs directoryCategoryArgs, DirectoryEntryCategory directoryEntryCategory, int i, Object obj) {
        if ((i & 1) != 0) {
            directoryEntryCategory = directoryCategoryArgs.directoryCategory;
        }
        return directoryCategoryArgs.copy(directoryEntryCategory);
    }

    public final DirectoryEntryCategory component1() {
        return this.directoryCategory;
    }

    public final DirectoryCategoryArgs copy(DirectoryEntryCategory directoryEntryCategory) {
        m.checkNotNullParameter(directoryEntryCategory, "directoryCategory");
        return new DirectoryCategoryArgs(directoryEntryCategory);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof DirectoryCategoryArgs) && m.areEqual(this.directoryCategory, ((DirectoryCategoryArgs) obj).directoryCategory);
        }
        return true;
    }

    public final DirectoryEntryCategory getDirectoryCategory() {
        return this.directoryCategory;
    }

    public int hashCode() {
        DirectoryEntryCategory directoryEntryCategory = this.directoryCategory;
        if (directoryEntryCategory != null) {
            return directoryEntryCategory.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder R = a.R("DirectoryCategoryArgs(directoryCategory=");
        R.append(this.directoryCategory);
        R.append(")");
        return R.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeString(this.directoryCategory.name());
    }
}
