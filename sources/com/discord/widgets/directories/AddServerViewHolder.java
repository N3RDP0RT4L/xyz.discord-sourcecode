package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.view.View;
import com.discord.databinding.DirectoryAddServerListItemBinding;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetDirectoryEntryViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\n\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/directories/AddServerViewHolder;", "Lcom/discord/widgets/directories/DirectoryChannelViewHolder;", "Lcom/discord/widgets/directories/DirectoryChannelItem;", "item", "Lcom/discord/widgets/directories/DirectoryChannelItemClickInterface;", "listener", "", "bind", "(Lcom/discord/widgets/directories/DirectoryChannelItem;Lcom/discord/widgets/directories/DirectoryChannelItemClickInterface;)V", "Lcom/discord/databinding/DirectoryAddServerListItemBinding;", "viewBinding", "Lcom/discord/databinding/DirectoryAddServerListItemBinding;", "getViewBinding", "()Lcom/discord/databinding/DirectoryAddServerListItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/DirectoryAddServerListItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AddServerViewHolder extends DirectoryChannelViewHolder {
    private final DirectoryAddServerListItemBinding viewBinding;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public AddServerViewHolder(com.discord.databinding.DirectoryAddServerListItemBinding r3) {
        /*
            r2 = this;
            java.lang.String r0 = "viewBinding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            android.widget.TextView r0 = r3.a
            java.lang.String r1 = "viewBinding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.viewBinding = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.directories.AddServerViewHolder.<init>(com.discord.databinding.DirectoryAddServerListItemBinding):void");
    }

    @Override // com.discord.widgets.directories.DirectoryChannelViewHolder
    public void bind(DirectoryChannelItem directoryChannelItem, final DirectoryChannelItemClickInterface directoryChannelItemClickInterface) {
        m.checkNotNullParameter(directoryChannelItem, "item");
        m.checkNotNullParameter(directoryChannelItemClickInterface, "listener");
        this.viewBinding.f2083b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.AddServerViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                DirectoryChannelItemClickInterface.this.onAddServerClicked();
            }
        });
    }

    public final DirectoryAddServerListItemBinding getViewBinding() {
        return this.viewBinding;
    }
}
