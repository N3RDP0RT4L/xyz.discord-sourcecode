package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreDirectories;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.directories.DirectoryUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guilds.join.GuildJoinHelperKt;
import d0.g;
import d0.g0.t;
import d0.o;
import d0.t.n;
import d0.t.n0;
import d0.z.d.k;
import d0.z.d.m;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
/* compiled from: WidgetDirectoriesSearchViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 /2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002/0B7\u0012\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010\u0012\b\b\u0002\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010\"\u001a\u00020!\u0012\u000e\b\u0002\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00020+¢\u0006\u0004\b-\u0010.J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\u0013\u001a\u00020\u00042\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\u0006\u0010\u0012\u001a\u00020\n¢\u0006\u0004\b\u0013\u0010\u0014J)\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00152\n\u0010\u0018\u001a\u00060\u000fj\u0002`\u00172\u0006\u0010\u0019\u001a\u00020\u000f¢\u0006\u0004\b\u001a\u0010\u001bJ!\u0010\u001c\u001a\u00020\u00042\n\u0010\u0018\u001a\u00060\u000fj\u0002`\u00172\u0006\u0010\u0019\u001a\u00020\u000f¢\u0006\u0004\b\u001c\u0010\u001dR\u001d\u0010\u0011\u001a\u00060\u000fj\u0002`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u0019\u0010'\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*¨\u00061"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "state", "", "handleNewState", "(Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;)V", "", "getHubName", "()Ljava/lang/String;", "", "searchTerm", "", "shouldNotSearch", "(Ljava/lang/CharSequence;)Z", "", "Lcom/discord/primitives/ChannelId;", "channelId", "query", "searchForDirectories", "(JLjava/lang/CharSequence;)V", "Landroid/content/Context;", "context", "Lcom/discord/primitives/GuildId;", ModelAuditLogEntry.CHANGE_KEY_ID, "directoryChannelId", "joinGuild", "(Landroid/content/Context;JJ)V", "removeGuild", "(JJ)V", "J", "getChannelId", "()J", "Lcom/discord/stores/StoreDirectories;", "directoriesStore", "Lcom/discord/stores/StoreDirectories;", "getDirectoriesStore", "()Lcom/discord/stores/StoreDirectories;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "getRestAPI", "()Lcom/discord/utilities/rest/RestAPI;", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(JLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreDirectories;Lrx/Observable;)V", "Companion", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesSearchViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final long channelId;
    private final StoreDirectories directoriesStore;
    private final RestAPI restAPI;

    /* compiled from: WidgetDirectoriesSearchViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "p1", "", "invoke", "(Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.directories.WidgetDirectoriesSearchViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<ViewState, Unit> {
        public AnonymousClass1(WidgetDirectoriesSearchViewModel widgetDirectoriesSearchViewModel) {
            super(1, widgetDirectoriesSearchViewModel, WidgetDirectoriesSearchViewModel.class, "handleNewState", "handleNewState(Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ViewState viewState) {
            invoke2(viewState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ViewState viewState) {
            m.checkNotNullParameter(viewState, "p1");
            ((WidgetDirectoriesSearchViewModel) this.receiver).handleNewState(viewState);
        }
    }

    /* compiled from: WidgetDirectoriesSearchViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014JI\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuildSelected;", "guildSelectedStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lrx/Observable;", "Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "observeStores", "(JLcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<ViewState> observeStores(long j, ObservationDeck observationDeck, StoreGuilds storeGuilds, StoreGuildSelected storeGuildSelected, StoreChannels storeChannels, StorePermissions storePermissions) {
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeGuilds, storeGuildSelected, storeChannels, storePermissions}, false, null, null, new WidgetDirectoriesSearchViewModel$Companion$observeStores$1(storePermissions, storeGuilds, storeGuildSelected, j), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetDirectoriesSearchViewModel(long r15, com.discord.utilities.rest.RestAPI r17, com.discord.stores.StoreDirectories r18, rx.Observable r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r14 = this;
            r0 = r20 & 2
            if (r0 == 0) goto Lc
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            r4 = r0
            goto Le
        Lc:
            r4 = r17
        Le:
            r0 = r20 & 4
            if (r0 == 0) goto L1a
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreDirectories r0 = r0.getDirectories()
            r5 = r0
            goto L1c
        L1a:
            r5 = r18
        L1c:
            r0 = r20 & 8
            if (r0 == 0) goto L3f
            com.discord.widgets.directories.WidgetDirectoriesSearchViewModel$Companion r6 = com.discord.widgets.directories.WidgetDirectoriesSearchViewModel.Companion
            com.discord.stores.updates.ObservationDeck r9 = com.discord.stores.updates.ObservationDeckProvider.get()
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r10 = r0.getGuilds()
            com.discord.stores.StoreGuildSelected r11 = r0.getGuildSelected()
            com.discord.stores.StoreChannels r12 = r0.getChannels()
            com.discord.stores.StorePermissions r13 = r0.getPermissions()
            r7 = r15
            rx.Observable r0 = com.discord.widgets.directories.WidgetDirectoriesSearchViewModel.Companion.access$observeStores(r6, r7, r9, r10, r11, r12, r13)
            r6 = r0
            goto L41
        L3f:
            r6 = r19
        L41:
            r1 = r14
            r2 = r15
            r1.<init>(r2, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.directories.WidgetDirectoriesSearchViewModel.<init>(long, com.discord.utilities.rest.RestAPI, com.discord.stores.StoreDirectories, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleNewState(ViewState viewState) {
        ViewState viewState2 = getViewState();
        if (viewState2 != null) {
            updateViewState(ViewState.copy$default(viewState2, viewState.getGuild(), null, null, viewState.getJoinedGuildIds(), viewState.getAdminGuildIds(), viewState.getHasAddGuildPermissions(), null, 70, null));
        }
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final StoreDirectories getDirectoriesStore() {
        return this.directoriesStore;
    }

    public final String getHubName() {
        Guild guild;
        ViewState viewState = getViewState();
        String name = (viewState == null || (guild = viewState.getGuild()) == null) ? null : guild.getName();
        return name != null ? name : "";
    }

    public final RestAPI getRestAPI() {
        return this.restAPI;
    }

    public final void joinGuild(Context context, long j, long j2) {
        m.checkNotNullParameter(context, "context");
        GuildJoinHelperKt.joinGuild(context, j, false, (r27 & 8) != 0 ? null : null, (r27 & 16) != 0 ? null : Long.valueOf(j2), (r27 & 32) != 0 ? null : this.restAPI.jsonObjectOf(o.to("source", DirectoryUtils.JOIN_GUILD_SOURCE)), WidgetDirectoriesSearchViewModel.class, (r27 & 128) != 0 ? null : null, (r27 & 256) != 0 ? null : null, (r27 & 512) != 0 ? null : null, WidgetDirectoriesSearchViewModel$joinGuild$1.INSTANCE);
    }

    public final void removeGuild(long j, long j2) {
        this.directoriesStore.removeServerFromDirectory(j2, j);
    }

    public final void searchForDirectories(long j, CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "query");
        ViewState viewState = getViewState();
        if (viewState != null && !shouldNotSearch(charSequence)) {
            RestCallStateKt.executeRequest(this.restAPI.searchServers(j, charSequence.toString()), new WidgetDirectoriesSearchViewModel$searchForDirectories$1(this, viewState, charSequence));
        }
    }

    public final boolean shouldNotSearch(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "searchTerm");
        if (!t.isBlank(charSequence)) {
            ViewState viewState = getViewState();
            if (!m.areEqual(viewState != null ? viewState.getCurrentSearchTerm() : null, charSequence.toString())) {
                return false;
            }
        }
        return true;
    }

    /* compiled from: WidgetDirectoriesSearchViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001Bu\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f\u0012\u0012\b\u0002\u0010\u001c\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u0012\u0012\u0014\b\u0002\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0015¢\u0006\u0004\b:\u0010;J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0011\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u001c\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J~\u0010\u001f\u001a\u00020\u00002\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0019\u001a\u00020\u00052\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f2\u0012\b\u0002\u0010\u001c\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f2\b\b\u0002\u0010\u001d\u001a\u00020\u00122\u0014\b\u0002\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0015HÆ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b!\u0010\u0007J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010&\u001a\u00020\u00122\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b&\u0010'R#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010(\u001a\u0004\b)\u0010\u0010R\u0019\u0010\u0019\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b+\u0010\u0007R\u001f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b-\u0010\u000bR#\u00102\u001a\b\u0012\u0004\u0012\u00020.0\b8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u000bR%\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00103\u001a\u0004\b4\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00105\u001a\u0004\b6\u0010\u0004R#\u0010\u001c\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010(\u001a\u0004\b7\u0010\u0010R\u0019\u0010\u001d\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00108\u001a\u0004\b9\u0010\u0014¨\u0006<"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "component2", "()Ljava/lang/String;", "", "Lcom/discord/api/directory/DirectoryEntryGuild;", "component3", "()Ljava/util/List;", "", "", "Lcom/discord/primitives/GuildId;", "component4", "()Ljava/util/Set;", "component5", "", "component6", "()Z", "Lcom/discord/stores/utilities/RestCallState;", "component7", "()Lcom/discord/stores/utilities/RestCallState;", "guild", "currentSearchTerm", "directories", "joinedGuildIds", "adminGuildIds", "hasAddGuildPermissions", "directoriesState", "copy", "(Lcom/discord/models/guild/Guild;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;ZLcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/directories/WidgetDirectoriesSearchViewModel$ViewState;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getJoinedGuildIds", "Ljava/lang/String;", "getCurrentSearchTerm", "Ljava/util/List;", "getDirectories", "Lcom/discord/widgets/directories/DirectoryEntryData;", "directoryEntryData$delegate", "Lkotlin/Lazy;", "getDirectoryEntryData", "directoryEntryData", "Lcom/discord/stores/utilities/RestCallState;", "getDirectoriesState", "Lcom/discord/models/guild/Guild;", "getGuild", "getAdminGuildIds", "Z", "getHasAddGuildPermissions", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;ZLcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Set<Long> adminGuildIds;
        private final String currentSearchTerm;
        private final List<DirectoryEntryGuild> directories;
        private final RestCallState<List<DirectoryEntryGuild>> directoriesState;
        private final Lazy directoryEntryData$delegate;
        private final Guild guild;
        private final boolean hasAddGuildPermissions;
        private final Set<Long> joinedGuildIds;

        public ViewState() {
            this(null, null, null, null, null, false, null, Opcodes.LAND, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(Guild guild, String str, List<DirectoryEntryGuild> list, Set<Long> set, Set<Long> set2, boolean z2, RestCallState<? extends List<DirectoryEntryGuild>> restCallState) {
            m.checkNotNullParameter(str, "currentSearchTerm");
            m.checkNotNullParameter(list, "directories");
            m.checkNotNullParameter(set, "joinedGuildIds");
            m.checkNotNullParameter(set2, "adminGuildIds");
            m.checkNotNullParameter(restCallState, "directoriesState");
            this.guild = guild;
            this.currentSearchTerm = str;
            this.directories = list;
            this.joinedGuildIds = set;
            this.adminGuildIds = set2;
            this.hasAddGuildPermissions = z2;
            this.directoriesState = restCallState;
            this.directoryEntryData$delegate = g.lazy(new WidgetDirectoriesSearchViewModel$ViewState$directoryEntryData$2(this));
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, Guild guild, String str, List list, Set set, Set set2, boolean z2, RestCallState restCallState, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = viewState.guild;
            }
            if ((i & 2) != 0) {
                str = viewState.currentSearchTerm;
            }
            String str2 = str;
            List<DirectoryEntryGuild> list2 = list;
            if ((i & 4) != 0) {
                list2 = viewState.directories;
            }
            List list3 = list2;
            Set<Long> set3 = set;
            if ((i & 8) != 0) {
                set3 = viewState.joinedGuildIds;
            }
            Set set4 = set3;
            Set<Long> set5 = set2;
            if ((i & 16) != 0) {
                set5 = viewState.adminGuildIds;
            }
            Set set6 = set5;
            if ((i & 32) != 0) {
                z2 = viewState.hasAddGuildPermissions;
            }
            boolean z3 = z2;
            RestCallState<List<DirectoryEntryGuild>> restCallState2 = restCallState;
            if ((i & 64) != 0) {
                restCallState2 = viewState.directoriesState;
            }
            return viewState.copy(guild, str2, list3, set4, set6, z3, restCallState2);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final String component2() {
            return this.currentSearchTerm;
        }

        public final List<DirectoryEntryGuild> component3() {
            return this.directories;
        }

        public final Set<Long> component4() {
            return this.joinedGuildIds;
        }

        public final Set<Long> component5() {
            return this.adminGuildIds;
        }

        public final boolean component6() {
            return this.hasAddGuildPermissions;
        }

        public final RestCallState<List<DirectoryEntryGuild>> component7() {
            return this.directoriesState;
        }

        public final ViewState copy(Guild guild, String str, List<DirectoryEntryGuild> list, Set<Long> set, Set<Long> set2, boolean z2, RestCallState<? extends List<DirectoryEntryGuild>> restCallState) {
            m.checkNotNullParameter(str, "currentSearchTerm");
            m.checkNotNullParameter(list, "directories");
            m.checkNotNullParameter(set, "joinedGuildIds");
            m.checkNotNullParameter(set2, "adminGuildIds");
            m.checkNotNullParameter(restCallState, "directoriesState");
            return new ViewState(guild, str, list, set, set2, z2, restCallState);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.guild, viewState.guild) && m.areEqual(this.currentSearchTerm, viewState.currentSearchTerm) && m.areEqual(this.directories, viewState.directories) && m.areEqual(this.joinedGuildIds, viewState.joinedGuildIds) && m.areEqual(this.adminGuildIds, viewState.adminGuildIds) && this.hasAddGuildPermissions == viewState.hasAddGuildPermissions && m.areEqual(this.directoriesState, viewState.directoriesState);
        }

        public final Set<Long> getAdminGuildIds() {
            return this.adminGuildIds;
        }

        public final String getCurrentSearchTerm() {
            return this.currentSearchTerm;
        }

        public final List<DirectoryEntryGuild> getDirectories() {
            return this.directories;
        }

        public final RestCallState<List<DirectoryEntryGuild>> getDirectoriesState() {
            return this.directoriesState;
        }

        public final List<DirectoryEntryData> getDirectoryEntryData() {
            return (List) this.directoryEntryData$delegate.getValue();
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final boolean getHasAddGuildPermissions() {
            return this.hasAddGuildPermissions;
        }

        public final Set<Long> getJoinedGuildIds() {
            return this.joinedGuildIds;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            String str = this.currentSearchTerm;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            List<DirectoryEntryGuild> list = this.directories;
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            Set<Long> set = this.joinedGuildIds;
            int hashCode4 = (hashCode3 + (set != null ? set.hashCode() : 0)) * 31;
            Set<Long> set2 = this.adminGuildIds;
            int hashCode5 = (hashCode4 + (set2 != null ? set2.hashCode() : 0)) * 31;
            boolean z2 = this.hasAddGuildPermissions;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode5 + i2) * 31;
            RestCallState<List<DirectoryEntryGuild>> restCallState = this.directoriesState;
            if (restCallState != null) {
                i = restCallState.hashCode();
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(guild=");
            R.append(this.guild);
            R.append(", currentSearchTerm=");
            R.append(this.currentSearchTerm);
            R.append(", directories=");
            R.append(this.directories);
            R.append(", joinedGuildIds=");
            R.append(this.joinedGuildIds);
            R.append(", adminGuildIds=");
            R.append(this.adminGuildIds);
            R.append(", hasAddGuildPermissions=");
            R.append(this.hasAddGuildPermissions);
            R.append(", directoriesState=");
            R.append(this.directoriesState);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ ViewState(Guild guild, String str, List list, Set set, Set set2, boolean z2, RestCallState restCallState, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : guild, (i & 2) != 0 ? "" : str, (i & 4) != 0 ? n.emptyList() : list, (i & 8) != 0 ? n0.emptySet() : set, (i & 16) != 0 ? n0.emptySet() : set2, (i & 32) != 0 ? true : z2, (i & 64) != 0 ? Default.INSTANCE : restCallState);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesSearchViewModel(long j, RestAPI restAPI, StoreDirectories storeDirectories, Observable<ViewState> observable) {
        super(new ViewState(null, null, null, null, null, false, null, Opcodes.LAND, null));
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(storeDirectories, "directoriesStore");
        m.checkNotNullParameter(observable, "storeObservable");
        this.channelId = j;
        this.restAPI = restAPI;
        this.directoriesStore = storeDirectories;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetDirectoriesSearchViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
