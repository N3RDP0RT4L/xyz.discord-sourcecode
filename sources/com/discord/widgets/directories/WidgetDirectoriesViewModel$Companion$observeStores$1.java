package com.discord.widgets.directories;

import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreDirectories;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.widgets.directories.WidgetDirectoriesViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetDirectoriesViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "invoke", "()Lcom/discord/widgets/directories/WidgetDirectoriesViewModel$ViewState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesViewModel$Companion$observeStores$1 extends o implements Function0<WidgetDirectoriesViewModel.ViewState> {
    public final /* synthetic */ StoreChannelsSelected $channelsSelectedStore;
    public final /* synthetic */ StoreDirectories $directoriesStore;
    public final /* synthetic */ StoreGuildSelected $guildSelectedStore;
    public final /* synthetic */ StoreGuilds $guildsStore;
    public final /* synthetic */ StorePermissions $permissionsStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesViewModel$Companion$observeStores$1(StoreChannelsSelected storeChannelsSelected, StoreGuildSelected storeGuildSelected, StoreGuilds storeGuilds, StorePermissions storePermissions, StoreDirectories storeDirectories) {
        super(0);
        this.$channelsSelectedStore = storeChannelsSelected;
        this.$guildSelectedStore = storeGuildSelected;
        this.$guildsStore = storeGuilds;
        this.$permissionsStore = storePermissions;
        this.$directoriesStore = storeDirectories;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x00a9, code lost:
        if (r0 != null) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x00bb, code lost:
        if (r0 != null) goto L26;
     */
    @Override // kotlin.jvm.functions.Function0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.directories.WidgetDirectoriesViewModel.ViewState invoke() {
        /*
            r11 = this;
            com.discord.stores.StoreChannelsSelected r0 = r11.$channelsSelectedStore
            com.discord.api.channel.Channel r3 = r0.getSelectedChannel()
            com.discord.stores.StoreGuildSelected r0 = r11.$guildSelectedStore
            long r0 = r0.getSelectedGuildId()
            com.discord.stores.StoreGuilds r2 = r11.$guildsStore
            com.discord.models.guild.Guild r2 = r2.getGuild(r0)
            com.discord.stores.StorePermissions r0 = r11.$permissionsStore
            java.util.Map r0 = r0.getGuildPermissions()
            com.discord.stores.StorePermissions r1 = r11.$permissionsStore
            java.util.Map r1 = r1.getPermissionsByChannel()
            if (r3 == 0) goto L29
            long r4 = r3.h()
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            goto L2a
        L29:
            r4 = 0
        L2a:
            java.lang.Object r1 = r1.get(r4)
            java.lang.Long r1 = (java.lang.Long) r1
            r4 = 2048(0x800, double:1.0118E-320)
            boolean r6 = com.discord.utilities.permissions.PermissionUtils.can(r4, r1)
            com.discord.stores.StoreGuilds r1 = r11.$guildsStore
            java.util.Map r1 = r1.getGuilds()
            java.util.Set r4 = r1.keySet()
            com.discord.stores.StoreGuilds r1 = r11.$guildsStore
            java.util.Map r1 = r1.getGuilds()
            java.util.Collection r1 = r1.values()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Iterator r1 = r1.iterator()
        L53:
            boolean r7 = r1.hasNext()
            if (r7 == 0) goto L72
            java.lang.Object r7 = r1.next()
            r8 = r7
            com.discord.models.guild.Guild r8 = (com.discord.models.guild.Guild) r8
            r9 = 8
            java.lang.Object r8 = b.d.b.a.a.d(r8, r0)
            java.lang.Long r8 = (java.lang.Long) r8
            boolean r8 = com.discord.utilities.permissions.PermissionUtils.can(r9, r8)
            if (r8 == 0) goto L53
            r5.add(r7)
            goto L53
        L72:
            java.util.ArrayList r0 = new java.util.ArrayList
            r1 = 10
            int r1 = d0.t.o.collectionSizeOrDefault(r5, r1)
            r0.<init>(r1)
            java.util.Iterator r1 = r5.iterator()
        L81:
            boolean r5 = r1.hasNext()
            if (r5 == 0) goto L99
            java.lang.Object r5 = r1.next()
            com.discord.models.guild.Guild r5 = (com.discord.models.guild.Guild) r5
            long r7 = r5.getId()
            java.lang.Long r5 = java.lang.Long.valueOf(r7)
            r0.add(r5)
            goto L81
        L99:
            java.util.Set r5 = d0.t.u.toSet(r0)
            if (r3 == 0) goto Lac
            long r0 = r3.h()
            com.discord.stores.StoreDirectories r7 = r11.$directoriesStore
            com.discord.stores.utilities.RestCallState r0 = r7.getDirectoriesForChannel(r0)
            if (r0 == 0) goto Lac
            goto Lae
        Lac:
            com.discord.stores.utilities.Default r0 = com.discord.stores.utilities.Default.INSTANCE
        Lae:
            r7 = r0
            if (r3 == 0) goto Lbe
            long r0 = r3.h()
            com.discord.stores.StoreDirectories r8 = r11.$directoriesStore
            com.discord.stores.utilities.RestCallState r0 = r8.getEntryCountsForChannel(r0)
            if (r0 == 0) goto Lbe
            goto Lc0
        Lbe:
            com.discord.stores.utilities.Default r0 = com.discord.stores.utilities.Default.INSTANCE
        Lc0:
            r8 = r0
            com.discord.widgets.directories.WidgetDirectoriesViewModel$ViewState r0 = new com.discord.widgets.directories.WidgetDirectoriesViewModel$ViewState
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.directories.WidgetDirectoriesViewModel$Companion$observeStores$1.invoke():com.discord.widgets.directories.WidgetDirectoriesViewModel$ViewState");
    }
}
