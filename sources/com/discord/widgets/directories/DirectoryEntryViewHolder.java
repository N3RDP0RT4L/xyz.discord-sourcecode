package com.discord.widgets.directories;

import andhook.lib.HookHelper;
import android.view.View;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.databinding.DirectoryEntryListItemBinding;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.views.directories.ServerDiscoveryItem;
import com.discord.widgets.directories.DirectoryChannelItem;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetDirectoryEntryViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\n\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/directories/DirectoryEntryViewHolder;", "Lcom/discord/widgets/directories/DirectoryChannelViewHolder;", "Lcom/discord/widgets/directories/DirectoryChannelItem;", "item", "Lcom/discord/widgets/directories/DirectoryChannelItemClickInterface;", "listener", "", "bind", "(Lcom/discord/widgets/directories/DirectoryChannelItem;Lcom/discord/widgets/directories/DirectoryChannelItemClickInterface;)V", "Lcom/discord/databinding/DirectoryEntryListItemBinding;", "viewBinding", "Lcom/discord/databinding/DirectoryEntryListItemBinding;", "getViewBinding", "()Lcom/discord/databinding/DirectoryEntryListItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/DirectoryEntryListItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DirectoryEntryViewHolder extends DirectoryChannelViewHolder {
    private final DirectoryEntryListItemBinding viewBinding;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public DirectoryEntryViewHolder(com.discord.databinding.DirectoryEntryListItemBinding r3) {
        /*
            r2 = this;
            java.lang.String r0 = "viewBinding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            com.discord.views.directories.ServerDiscoveryItem r0 = r3.a
            java.lang.String r1 = "viewBinding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.viewBinding = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.directories.DirectoryEntryViewHolder.<init>(com.discord.databinding.DirectoryEntryListItemBinding):void");
    }

    @Override // com.discord.widgets.directories.DirectoryChannelViewHolder
    public void bind(DirectoryChannelItem directoryChannelItem, final DirectoryChannelItemClickInterface directoryChannelItemClickInterface) {
        final DirectoryEntryData directoryEntryData;
        m.checkNotNullParameter(directoryChannelItem, "item");
        m.checkNotNullParameter(directoryChannelItemClickInterface, "listener");
        if (!(directoryChannelItem instanceof DirectoryChannelItem.DirectoryItem)) {
            directoryChannelItem = null;
        }
        DirectoryChannelItem.DirectoryItem directoryItem = (DirectoryChannelItem.DirectoryItem) directoryChannelItem;
        if (directoryItem != null && (directoryEntryData = directoryItem.getDirectoryEntryData()) != null) {
            final DirectoryEntryGuild entry = directoryEntryData.getEntry();
            ServerDiscoveryItem serverDiscoveryItem = this.viewBinding.f2084b;
            serverDiscoveryItem.setTitle(entry.e().i());
            serverDiscoveryItem.setDescription(entry.b());
            Integer a = entry.e().a();
            int i = 0;
            serverDiscoveryItem.setMembers(a != null ? a.intValue() : 0);
            Integer b2 = entry.e().b();
            if (b2 != null) {
                i = b2.intValue();
            }
            serverDiscoveryItem.setOnline(i);
            String forGuild$default = IconUtils.getForGuild$default(Long.valueOf(entry.e().h()), entry.e().g(), null, false, null, 28, null);
            String computeShortName = GuildUtilsKt.computeShortName(entry.e().i());
            m.checkNotNullParameter(computeShortName, "fallbackText");
            serverDiscoveryItem.j.f.a(forGuild$default, computeShortName);
            if (!directoryEntryData.getHasJoinedGuild()) {
                serverDiscoveryItem.setJoinButtonOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.DirectoryEntryViewHolder$bind$$inlined$apply$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        directoryChannelItemClickInterface.onEntryClicked(DirectoryEntryGuild.this.e().h(), DirectoryEntryGuild.this.c());
                    }
                });
                serverDiscoveryItem.setJoinedButtonOnClickListener(null);
            } else {
                serverDiscoveryItem.setJoinButtonOnClickListener(null);
                serverDiscoveryItem.setJoinedButtonOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.directories.DirectoryEntryViewHolder$bind$$inlined$apply$lambda$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        directoryChannelItemClickInterface.onGoToGuildClicked(DirectoryEntryGuild.this.e().h());
                    }
                });
            }
            serverDiscoveryItem.setOnLongClickListener(new View.OnLongClickListener() { // from class: com.discord.widgets.directories.DirectoryEntryViewHolder$bind$$inlined$apply$lambda$3
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    DirectoryChannelItemClickInterface directoryChannelItemClickInterface2 = directoryChannelItemClickInterface;
                    DirectoryEntryGuild directoryEntryGuild = DirectoryEntryGuild.this;
                    directoryChannelItemClickInterface2.onOverflowClicked(directoryEntryGuild, directoryEntryGuild.c(), directoryEntryData.getHasEditPermissions());
                    return true;
                }
            });
            serverDiscoveryItem.setJoinedGuild(directoryEntryData.getHasJoinedGuild());
        }
    }

    public final DirectoryEntryListItemBinding getViewBinding() {
        return this.viewBinding;
    }
}
