package com.discord.widgets.directories;

import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.utilities.directories.DirectoryUtilsKt;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.widgets.directories.WidgetDirectoriesViewModel;
import d0.t.g0;
import d0.t.n;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetDirectoriesViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "", "Lcom/discord/widgets/directories/DirectoryEntryData;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoriesViewModel$ViewState$directoryEntryData$2 extends o implements Function0<Map<Integer, ? extends List<? extends DirectoryEntryData>>> {
    public final /* synthetic */ WidgetDirectoriesViewModel.ViewState this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoriesViewModel$ViewState$directoryEntryData$2(WidgetDirectoriesViewModel.ViewState viewState) {
        super(0);
        this.this$0 = viewState;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Integer, ? extends List<? extends DirectoryEntryData>> invoke() {
        List<DirectoryEntryGuild> invoke = this.this$0.getDirectories().invoke();
        if (invoke == null) {
            invoke = n.emptyList();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (DirectoryEntryGuild directoryEntryGuild : invoke) {
            Integer valueOf = Integer.valueOf(directoryEntryGuild.f());
            Object obj = linkedHashMap.get(valueOf);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(valueOf, obj);
            }
            ((List) obj).add(new DirectoryEntryData(directoryEntryGuild, this.this$0.getJoinedGuildIds().contains(Long.valueOf(directoryEntryGuild.e().h())), this.this$0.getAdminGuildIds().contains(Long.valueOf(directoryEntryGuild.e().h()))));
        }
        if (!GrowthTeamFeatures.INSTANCE.hubRankingsEnabled()) {
            return linkedHashMap;
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(linkedHashMap.size()));
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            linkedHashMap2.put(entry.getKey(), DirectoryUtilsKt.rank((List) entry.getValue(), Integer.valueOf(DirectoryEntryCategory.Companion.findByKey(((Number) entry.getKey()).intValue(), false).getIdealSize())));
        }
        return linkedHashMap2;
    }
}
