package com.discord.widgets.directories;

import android.content.Context;
import b.a.d.j;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreStream;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.widgets.hubs.HubAddNameArgs;
import com.discord.widgets.hubs.WidgetHubAddName;
import d0.g0.t;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetDirectoryChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/GuildId;", "guildId", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDirectoryChannel$initializeUI$5 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ WidgetDirectoryChannel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDirectoryChannel$initializeUI$5(WidgetDirectoryChannel widgetDirectoryChannel) {
        super(1);
        this.this$0 = widgetDirectoryChannel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        Guild guild;
        Context context;
        StoreStream.Companion companion = StoreStream.Companion;
        long selectedGuildId = companion.getGuildSelected().getSelectedGuildId();
        GuildMember member = companion.getGuilds().getMember(selectedGuildId, companion.getUsers().getMe().getId());
        if (member != null && (guild = companion.getGuilds().getGuild(selectedGuildId)) != null && (context = this.this$0.getContext()) != null) {
            m.checkNotNullExpressionValue(context, "context ?: return@appSubscribe");
            if (this.this$0.isVisible() && selectedGuildId == j && guild.isHub()) {
                String nick = member.getNick();
                if ((nick == null || t.isBlank(nick)) && !companion.getDirectories().getAndSetSeenNamePrompt(selectedGuildId) && !GrowthTeamFeatures.INSTANCE.isHubNameKillSwitchEnabled()) {
                    j.d(context, WidgetHubAddName.class, new HubAddNameArgs(guild.getId()));
                }
            }
        }
    }
}
