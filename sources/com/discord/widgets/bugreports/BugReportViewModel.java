package com.discord.widgets.bugreports;

import andhook.lib.HookHelper;
import android.content.Context;
import android.net.Uri;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.bugreport.BugReportConfig;
import com.discord.api.bugreport.Feature;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreStream;
import com.discord.stores.utilities.Loading;
import com.discord.stores.utilities.RestCallState;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.error.Error;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import d0.c0.c;
import d0.d0.f;
import d0.g0.w;
import d0.t.m;
import d0.t.n;
import d0.t.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: BugReportViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 f2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003fghB#\u0012\u0006\u0010U\u001a\u00020T\u0012\b\u00109\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010Z\u001a\u00020Y¢\u0006\u0004\bd\u0010eJ\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0013\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0014\u0010\u0012J\u0017\u0010\u0017\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001a\u001a\u00020\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0019\u0010\u001e\u001a\u00020\t2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\u0019\u0010\"\u001a\u00020\t2\b\u0010!\u001a\u0004\u0018\u00010 H\u0007¢\u0006\u0004\b\"\u0010#J\r\u0010$\u001a\u00020\t¢\u0006\u0004\b$\u0010\u000bJ\r\u0010%\u001a\u00020\t¢\u0006\u0004\b%\u0010\u000bJ\u0017\u0010'\u001a\u00020\t2\b\u0010&\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b'\u0010(J\u0015\u0010+\u001a\u00020\t2\u0006\u0010*\u001a\u00020)¢\u0006\u0004\b+\u0010,J\u0015\u0010\u0007\u001a\u00020\t2\u0006\u0010-\u001a\u00020\u0003¢\u0006\u0004\b\u0007\u0010\u0012R$\u0010.\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b.\u0010/\u001a\u0004\b0\u00101\"\u0004\b2\u0010\u0012R\"\u00103\u001a\u00020\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b3\u00104\u001a\u0004\b5\u00106\"\u0004\b7\u0010\u001bR\u0018\u00108\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u0010/R\u001b\u00109\u001a\u0004\u0018\u00010\u00038\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010/\u001a\u0004\b:\u00101R$\u0010;\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b;\u0010/\u001a\u0004\b<\u00101\"\u0004\b=\u0010\u0012R\u0019\u0010?\u001a\u00020>8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010BR\u0019\u0010C\u001a\u00020>8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010@\u001a\u0004\bD\u0010BR\u0016\u0010E\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u0010FR$\u0010&\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b&\u0010G\u001a\u0004\bH\u0010I\"\u0004\bJ\u0010(R$\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010K\u001a\u0004\bL\u0010M\"\u0004\bN\u0010OR\u001e\u0010R\u001a\n\u0012\u0004\u0012\u00020Q\u0018\u00010P8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bR\u0010SR\u0019\u0010U\u001a\u00020T8\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010V\u001a\u0004\bW\u0010XR\u0019\u0010Z\u001a\u00020Y8\u0006@\u0006¢\u0006\f\n\u0004\bZ\u0010[\u001a\u0004\b\\\u0010]R\"\u0010\u001a\u001a\u00020\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001a\u00104\u001a\u0004\b^\u00106\"\u0004\b_\u0010\u001bR:\u0010b\u001a&\u0012\f\u0012\n a*\u0004\u0018\u00010\r0\r a*\u0012\u0012\f\u0012\n a*\u0004\u0018\u00010\r0\r\u0018\u00010`0`8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bb\u0010c¨\u0006i"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "", "filter", "", "Lcom/discord/api/bugreport/Feature;", "filterFeatures", "(Ljava/lang/String;)Ljava/util/List;", "", "dismissAfterDelay", "()V", "Lrx/Observable;", "Lcom/discord/widgets/bugreports/BugReportViewModel$Event;", "observeEvents", "()Lrx/Observable;", ModelAuditLogEntry.CHANGE_KEY_NAME, "updateReportName", "(Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "updateReportDescription", "", "priority", "updatePriority", "(I)V", "", "useScreenshot", "(Z)V", "Lcom/discord/utilities/error/Error;", "error", "showReport", "(Lcom/discord/utilities/error/Error;)V", "Lokhttp3/RequestBody;", "screenshotBody", "sendReport", "(Lokhttp3/RequestBody;)V", "showFeatureAreas", "loadFeatures", "feature", "selectFeatureArea", "(Lcom/discord/api/bugreport/Feature;)V", "Landroid/content/Context;", "context", "prefetchStickers", "(Landroid/content/Context;)V", "it", "reportDescription", "Ljava/lang/String;", "getReportDescription", "()Ljava/lang/String;", "setReportDescription", "stickersPrefetched", "Z", "getStickersPrefetched", "()Z", "setStickersPrefetched", "featureSearchQuery", "screenshotName", "getScreenshotName", "reportName", "getReportName", "setReportName", "Lcom/discord/api/sticker/Sticker;", "successSticker", "Lcom/discord/api/sticker/Sticker;", "getSuccessSticker", "()Lcom/discord/api/sticker/Sticker;", "sendingSticker", "getSendingSticker", "stickerCharacterIndex", "I", "Lcom/discord/api/bugreport/Feature;", "getFeature", "()Lcom/discord/api/bugreport/Feature;", "setFeature", "Ljava/lang/Integer;", "getPriority", "()Ljava/lang/Integer;", "setPriority", "(Ljava/lang/Integer;)V", "Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/bugreport/BugReportConfig;", "bugReportConfig", "Lcom/discord/stores/utilities/RestCallState;", "Landroid/net/Uri;", "screenshotUri", "Landroid/net/Uri;", "getScreenshotUri", "()Landroid/net/Uri;", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/utilities/rest/RestAPI;", "getRestApi", "()Lcom/discord/utilities/rest/RestAPI;", "getUseScreenshot", "setUseScreenshot", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "(Landroid/net/Uri;Ljava/lang/String;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "Event", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BugReportViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private static final List<Feature> genericFeatureAreas = m.listOf(new Feature(NotificationClient.NOTIF_GENERAL, "Android", null));
    private static final List<Sticker> sendingStickers;
    private static final List<Sticker> successStickers;
    private RestCallState<BugReportConfig> bugReportConfig;
    private final PublishSubject<Event> eventSubject;
    private Feature feature;
    private String featureSearchQuery;
    private Integer priority;
    private String reportDescription;
    private String reportName;
    private final RestAPI restApi;
    private final String screenshotName;
    private final Uri screenshotUri;
    private final Sticker sendingSticker;
    private final int stickerCharacterIndex;
    private boolean stickersPrefetched;
    private final Sticker successSticker;
    private boolean useScreenshot;

    /* compiled from: BugReportViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0005\u001a\u0004\b\n\u0010\u0007R\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0005\u001a\u0004\b\f\u0010\u0007¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$Companion;", "", "", "Lcom/discord/api/bugreport/Feature;", "genericFeatureAreas", "Ljava/util/List;", "getGenericFeatureAreas", "()Ljava/util/List;", "Lcom/discord/api/sticker/Sticker;", "successStickers", "getSuccessStickers", "sendingStickers", "getSendingStickers", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final List<Feature> getGenericFeatureAreas() {
            return BugReportViewModel.genericFeatureAreas;
        }

        public final List<Sticker> getSendingStickers() {
            return BugReportViewModel.sendingStickers;
        }

        public final List<Sticker> getSuccessStickers() {
            return BugReportViewModel.successStickers;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: BugReportViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$Event;", "", HookHelper.constructorName, "()V", "CloseReport", "Lcom/discord/widgets/bugreports/BugReportViewModel$Event$CloseReport;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: BugReportViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$Event$CloseReport;", "Lcom/discord/widgets/bugreports/BugReportViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CloseReport extends Event {
            public static final CloseReport INSTANCE = new CloseReport();

            private CloseReport() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: BugReportViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Report", "SelectFeature", "Sending", "Success", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Report;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Sending;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Success;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$SelectFeature;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: BugReportViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\t\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\f\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u000f\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b2\u00103J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J`\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u001a\u001a\u00020\u000f2\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0012HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0007J\u0010\u0010\u001f\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u001a\u0010#\u001a\u00020\u000f2\b\u0010\"\u001a\u0004\u0018\u00010!HÖ\u0003¢\u0006\u0004\b#\u0010$R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010%\u001a\u0004\b&\u0010\u000bR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010'\u001a\u0004\b(\u0010\u000eR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010)\u001a\u0004\b*\u0010\u0014R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010+\u001a\u0004\b,\u0010\u0004R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010-\u001a\u0004\b.\u0010\u0007R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010-\u001a\u0004\b/\u0010\u0007R\u0019\u0010\u001a\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00100\u001a\u0004\b1\u0010\u0011¨\u00064"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Report;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "Landroid/net/Uri;", "component1", "()Landroid/net/Uri;", "", "component2", "()Ljava/lang/String;", "component3", "", "component4", "()Ljava/lang/Integer;", "Lcom/discord/api/bugreport/Feature;", "component5", "()Lcom/discord/api/bugreport/Feature;", "", "component6", "()Z", "Lcom/discord/utilities/error/Error;", "component7", "()Lcom/discord/utilities/error/Error;", "screenshotUri", "reportName", "reportDescription", "priority", "feature", "useScreenshot", "error", "copy", "(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/api/bugreport/Feature;ZLcom/discord/utilities/error/Error;)Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Report;", "toString", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getPriority", "Lcom/discord/api/bugreport/Feature;", "getFeature", "Lcom/discord/utilities/error/Error;", "getError", "Landroid/net/Uri;", "getScreenshotUri", "Ljava/lang/String;", "getReportDescription", "getReportName", "Z", "getUseScreenshot", HookHelper.constructorName, "(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/api/bugreport/Feature;ZLcom/discord/utilities/error/Error;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Report extends ViewState {
            private final Error error;
            private final Feature feature;
            private final Integer priority;
            private final String reportDescription;
            private final String reportName;
            private final Uri screenshotUri;
            private final boolean useScreenshot;

            public /* synthetic */ Report(Uri uri, String str, String str2, Integer num, Feature feature, boolean z2, Error error, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(uri, str, str2, num, feature, (i & 32) != 0 ? true : z2, (i & 64) != 0 ? null : error);
            }

            public static /* synthetic */ Report copy$default(Report report, Uri uri, String str, String str2, Integer num, Feature feature, boolean z2, Error error, int i, Object obj) {
                if ((i & 1) != 0) {
                    uri = report.screenshotUri;
                }
                if ((i & 2) != 0) {
                    str = report.reportName;
                }
                String str3 = str;
                if ((i & 4) != 0) {
                    str2 = report.reportDescription;
                }
                String str4 = str2;
                if ((i & 8) != 0) {
                    num = report.priority;
                }
                Integer num2 = num;
                if ((i & 16) != 0) {
                    feature = report.feature;
                }
                Feature feature2 = feature;
                if ((i & 32) != 0) {
                    z2 = report.useScreenshot;
                }
                boolean z3 = z2;
                if ((i & 64) != 0) {
                    error = report.error;
                }
                return report.copy(uri, str3, str4, num2, feature2, z3, error);
            }

            public final Uri component1() {
                return this.screenshotUri;
            }

            public final String component2() {
                return this.reportName;
            }

            public final String component3() {
                return this.reportDescription;
            }

            public final Integer component4() {
                return this.priority;
            }

            public final Feature component5() {
                return this.feature;
            }

            public final boolean component6() {
                return this.useScreenshot;
            }

            public final Error component7() {
                return this.error;
            }

            public final Report copy(Uri uri, String str, String str2, Integer num, Feature feature, boolean z2, Error error) {
                d0.z.d.m.checkNotNullParameter(uri, "screenshotUri");
                return new Report(uri, str, str2, num, feature, z2, error);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Report)) {
                    return false;
                }
                Report report = (Report) obj;
                return d0.z.d.m.areEqual(this.screenshotUri, report.screenshotUri) && d0.z.d.m.areEqual(this.reportName, report.reportName) && d0.z.d.m.areEqual(this.reportDescription, report.reportDescription) && d0.z.d.m.areEqual(this.priority, report.priority) && d0.z.d.m.areEqual(this.feature, report.feature) && this.useScreenshot == report.useScreenshot && d0.z.d.m.areEqual(this.error, report.error);
            }

            public final Error getError() {
                return this.error;
            }

            public final Feature getFeature() {
                return this.feature;
            }

            public final Integer getPriority() {
                return this.priority;
            }

            public final String getReportDescription() {
                return this.reportDescription;
            }

            public final String getReportName() {
                return this.reportName;
            }

            public final Uri getScreenshotUri() {
                return this.screenshotUri;
            }

            public final boolean getUseScreenshot() {
                return this.useScreenshot;
            }

            public int hashCode() {
                Uri uri = this.screenshotUri;
                int i = 0;
                int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
                String str = this.reportName;
                int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.reportDescription;
                int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
                Integer num = this.priority;
                int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
                Feature feature = this.feature;
                int hashCode5 = (hashCode4 + (feature != null ? feature.hashCode() : 0)) * 31;
                boolean z2 = this.useScreenshot;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode5 + i2) * 31;
                Error error = this.error;
                if (error != null) {
                    i = error.hashCode();
                }
                return i4 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Report(screenshotUri=");
                R.append(this.screenshotUri);
                R.append(", reportName=");
                R.append(this.reportName);
                R.append(", reportDescription=");
                R.append(this.reportDescription);
                R.append(", priority=");
                R.append(this.priority);
                R.append(", feature=");
                R.append(this.feature);
                R.append(", useScreenshot=");
                R.append(this.useScreenshot);
                R.append(", error=");
                R.append(this.error);
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Report(Uri uri, String str, String str2, Integer num, Feature feature, boolean z2, Error error) {
                super(null);
                d0.z.d.m.checkNotNullParameter(uri, "screenshotUri");
                this.screenshotUri = uri;
                this.reportName = str;
                this.reportDescription = str2;
                this.priority = num;
                this.feature = feature;
                this.useScreenshot = z2;
                this.error = error;
            }
        }

        /* compiled from: BugReportViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\r\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\u001f\u0010 J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ6\u0010\u000f\u001a\u00020\u00002\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u000b¨\u0006!"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$SelectFeature;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "", "component1", "()Ljava/lang/String;", "", "component2", "()Z", "", "Lcom/discord/api/bugreport/Feature;", "component3", "()Ljava/util/List;", "query", "loadingFeatures", "features", "copy", "(Ljava/lang/String;ZLjava/util/List;)Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$SelectFeature;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getQuery", "Z", "getLoadingFeatures", "Ljava/util/List;", "getFeatures", HookHelper.constructorName, "(Ljava/lang/String;ZLjava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SelectFeature extends ViewState {
            private final List<Feature> features;
            private final boolean loadingFeatures;
            private final String query;

            public SelectFeature() {
                this(null, false, null, 7, null);
            }

            public /* synthetic */ SelectFeature(String str, boolean z2, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? null : str, (i & 2) != 0 ? true : z2, (i & 4) != 0 ? n.emptyList() : list);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ SelectFeature copy$default(SelectFeature selectFeature, String str, boolean z2, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = selectFeature.query;
                }
                if ((i & 2) != 0) {
                    z2 = selectFeature.loadingFeatures;
                }
                if ((i & 4) != 0) {
                    list = selectFeature.features;
                }
                return selectFeature.copy(str, z2, list);
            }

            public final String component1() {
                return this.query;
            }

            public final boolean component2() {
                return this.loadingFeatures;
            }

            public final List<Feature> component3() {
                return this.features;
            }

            public final SelectFeature copy(String str, boolean z2, List<Feature> list) {
                d0.z.d.m.checkNotNullParameter(list, "features");
                return new SelectFeature(str, z2, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SelectFeature)) {
                    return false;
                }
                SelectFeature selectFeature = (SelectFeature) obj;
                return d0.z.d.m.areEqual(this.query, selectFeature.query) && this.loadingFeatures == selectFeature.loadingFeatures && d0.z.d.m.areEqual(this.features, selectFeature.features);
            }

            public final List<Feature> getFeatures() {
                return this.features;
            }

            public final boolean getLoadingFeatures() {
                return this.loadingFeatures;
            }

            public final String getQuery() {
                return this.query;
            }

            public int hashCode() {
                String str = this.query;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                boolean z2 = this.loadingFeatures;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode + i2) * 31;
                List<Feature> list = this.features;
                if (list != null) {
                    i = list.hashCode();
                }
                return i4 + i;
            }

            public String toString() {
                StringBuilder R = a.R("SelectFeature(query=");
                R.append(this.query);
                R.append(", loadingFeatures=");
                R.append(this.loadingFeatures);
                R.append(", features=");
                return a.K(R, this.features, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SelectFeature(String str, boolean z2, List<Feature> list) {
                super(null);
                d0.z.d.m.checkNotNullParameter(list, "features");
                this.query = str;
                this.loadingFeatures = z2;
                this.features = list;
            }
        }

        /* compiled from: BugReportViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Sending;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "Lcom/discord/api/sticker/Sticker;", "component1", "()Lcom/discord/api/sticker/Sticker;", "sendingSticker", "copy", "(Lcom/discord/api/sticker/Sticker;)Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Sending;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/sticker/Sticker;", "getSendingSticker", HookHelper.constructorName, "(Lcom/discord/api/sticker/Sticker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Sending extends ViewState {
            private final Sticker sendingSticker;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Sending(Sticker sticker) {
                super(null);
                d0.z.d.m.checkNotNullParameter(sticker, "sendingSticker");
                this.sendingSticker = sticker;
            }

            public static /* synthetic */ Sending copy$default(Sending sending, Sticker sticker, int i, Object obj) {
                if ((i & 1) != 0) {
                    sticker = sending.sendingSticker;
                }
                return sending.copy(sticker);
            }

            public final Sticker component1() {
                return this.sendingSticker;
            }

            public final Sending copy(Sticker sticker) {
                d0.z.d.m.checkNotNullParameter(sticker, "sendingSticker");
                return new Sending(sticker);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Sending) && d0.z.d.m.areEqual(this.sendingSticker, ((Sending) obj).sendingSticker);
                }
                return true;
            }

            public final Sticker getSendingSticker() {
                return this.sendingSticker;
            }

            public int hashCode() {
                Sticker sticker = this.sendingSticker;
                if (sticker != null) {
                    return sticker.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Sending(sendingSticker=");
                R.append(this.sendingSticker);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: BugReportViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Success;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "Lcom/discord/api/sticker/Sticker;", "component1", "()Lcom/discord/api/sticker/Sticker;", "successSticker", "copy", "(Lcom/discord/api/sticker/Sticker;)Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState$Success;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/sticker/Sticker;", "getSuccessSticker", HookHelper.constructorName, "(Lcom/discord/api/sticker/Sticker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Success extends ViewState {
            private final Sticker successSticker;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Success(Sticker sticker) {
                super(null);
                d0.z.d.m.checkNotNullParameter(sticker, "successSticker");
                this.successSticker = sticker;
            }

            public static /* synthetic */ Success copy$default(Success success, Sticker sticker, int i, Object obj) {
                if ((i & 1) != 0) {
                    sticker = success.successSticker;
                }
                return success.copy(sticker);
            }

            public final Sticker component1() {
                return this.successSticker;
            }

            public final Success copy(Sticker sticker) {
                d0.z.d.m.checkNotNullParameter(sticker, "successSticker");
                return new Success(sticker);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Success) && d0.z.d.m.areEqual(this.successSticker, ((Success) obj).successSticker);
                }
                return true;
            }

            public final Sticker getSuccessSticker() {
                return this.successSticker;
            }

            public int hashCode() {
                Sticker sticker = this.successSticker;
                if (sticker != null) {
                    return sticker.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Success(successSticker=");
                R.append(this.successSticker);
                R.append(")");
                return R.toString();
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    static {
        StoreStream.Companion companion = StoreStream.Companion;
        sendingStickers = n.listOfNotNull((Object[]) new Sticker[]{companion.getStickers().getStickers().get(754107634172297306L), companion.getStickers().getStickers().get(751606719611928586L), companion.getStickers().getStickers().get(749049128012742676L), companion.getStickers().getStickers().get(761114619137359892L), companion.getStickers().getStickers().get(783788733987815434L)});
        successStickers = n.listOfNotNull((Object[]) new Sticker[]{companion.getStickers().getStickers().get(754109076933443614L), companion.getStickers().getStickers().get(751606065447305216L), companion.getStickers().getStickers().get(749053689419006003L), companion.getStickers().getStickers().get(761108384010862602L), companion.getStickers().getStickers().get(783794979704537108L)});
    }

    public /* synthetic */ BugReportViewModel(Uri uri, String str, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(uri, str, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void dismissAfterDelay() {
        Observable<Long> d02 = Observable.d0(1500L, TimeUnit.MILLISECONDS);
        d0.z.d.m.checkNotNullExpressionValue(d02, "Observable.timer(1500, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), BugReportViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new BugReportViewModel$dismissAfterDelay$1(this));
    }

    private final List<Feature> filterFeatures(String str) {
        BugReportConfig invoke;
        RestCallState<BugReportConfig> restCallState = this.bugReportConfig;
        List<Feature> a = (restCallState == null || (invoke = restCallState.invoke()) == null) ? null : invoke.a();
        if (a == null) {
            a = n.emptyList();
        }
        List<Feature> plus = u.plus((Collection) a, (Iterable) genericFeatureAreas);
        if (str == null) {
            return plus;
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : plus) {
            Feature feature = (Feature) obj;
            boolean z2 = true;
            if (!w.contains((CharSequence) feature.b(), (CharSequence) str, true) && !w.contains((CharSequence) feature.c(), (CharSequence) str, true)) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public static /* synthetic */ void showReport$default(BugReportViewModel bugReportViewModel, Error error, int i, Object obj) {
        if ((i & 1) != 0) {
            error = null;
        }
        bugReportViewModel.showReport(error);
    }

    public final Feature getFeature() {
        return this.feature;
    }

    public final Integer getPriority() {
        return this.priority;
    }

    public final String getReportDescription() {
        return this.reportDescription;
    }

    public final String getReportName() {
        return this.reportName;
    }

    public final RestAPI getRestApi() {
        return this.restApi;
    }

    public final String getScreenshotName() {
        return this.screenshotName;
    }

    public final Uri getScreenshotUri() {
        return this.screenshotUri;
    }

    public final Sticker getSendingSticker() {
        return this.sendingSticker;
    }

    public final boolean getStickersPrefetched() {
        return this.stickersPrefetched;
    }

    public final Sticker getSuccessSticker() {
        return this.successSticker;
    }

    public final boolean getUseScreenshot() {
        return this.useScreenshot;
    }

    public final void loadFeatures() {
        RestCallStateKt.executeRequest(this.restApi.getBugReportConfig(), new BugReportViewModel$loadFeatures$1(this));
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        d0.z.d.m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void prefetchStickers(Context context) {
        d0.z.d.m.checkNotNullParameter(context, "context");
        if (!this.stickersPrefetched) {
            StickerUtils stickerUtils = StickerUtils.INSTANCE;
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(stickerUtils.fetchSticker(context, this.sendingSticker), false, 1, null), BugReportViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, BugReportViewModel$prefetchStickers$1.INSTANCE);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(stickerUtils.fetchSticker(context, this.successSticker), false, 1, null), BugReportViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, BugReportViewModel$prefetchStickers$2.INSTANCE);
            this.stickersPrefetched = true;
        }
    }

    public final void selectFeatureArea(Feature feature) {
        this.feature = feature;
        showReport$default(this, null, 1, null);
    }

    @MainThread
    public final void sendReport(RequestBody requestBody) {
        Integer num;
        String str = this.reportName;
        if (str != null && (num = this.priority) != null) {
            int intValue = num.intValue();
            MultipartBody.Part b2 = requestBody != null ? MultipartBody.Part.b("screenshot", this.screenshotName, requestBody) : null;
            updateViewState(new ViewState.Sending(this.sendingSticker));
            RestAPI restAPI = this.restApi;
            String str2 = this.reportDescription;
            Feature feature = this.feature;
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(restAPI.sendBugReport(str, str2, intValue, feature != null ? feature.a() : null, b2), false, 1, null)), BugReportViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new BugReportViewModel$sendReport$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new BugReportViewModel$sendReport$1(this));
        }
    }

    public final void setFeature(Feature feature) {
        this.feature = feature;
    }

    public final void setPriority(Integer num) {
        this.priority = num;
    }

    public final void setReportDescription(String str) {
        this.reportDescription = str;
    }

    public final void setReportName(String str) {
        this.reportName = str;
    }

    public final void setStickersPrefetched(boolean z2) {
        this.stickersPrefetched = z2;
    }

    public final void setUseScreenshot(boolean z2) {
        this.useScreenshot = z2;
    }

    public final void showFeatureAreas() {
        String str = this.featureSearchQuery;
        updateViewState(new ViewState.SelectFeature(str, this.bugReportConfig instanceof Loading, u.sortedWith(filterFeatures(str), new Comparator() { // from class: com.discord.widgets.bugreports.BugReportViewModel$showFeatureAreas$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                Feature feature = (Feature) t;
                Feature feature2 = (Feature) t2;
                return d0.u.a.compareValues(feature.c() + feature.b(), feature2.c() + feature2.b());
            }
        })));
    }

    public final void showReport(Error error) {
        updateViewState(new ViewState.Report(this.screenshotUri, this.reportName, this.reportDescription, this.priority, this.feature, this.useScreenshot, error));
    }

    @MainThread
    public final void updatePriority(int i) {
        this.priority = Integer.valueOf(i);
        showReport$default(this, null, 1, null);
    }

    @MainThread
    public final void updateReportDescription(String str) {
        d0.z.d.m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        this.reportDescription = str;
        showReport$default(this, null, 1, null);
    }

    @MainThread
    public final void updateReportName(String str) {
        d0.z.d.m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.reportName = str;
        showReport$default(this, null, 1, null);
    }

    @MainThread
    public final void useScreenshot(boolean z2) {
        this.useScreenshot = z2;
        showReport$default(this, null, 1, null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BugReportViewModel(Uri uri, String str, RestAPI restAPI) {
        super(null, 1, null);
        d0.z.d.m.checkNotNullParameter(uri, "screenshotUri");
        d0.z.d.m.checkNotNullParameter(restAPI, "restApi");
        this.screenshotUri = uri;
        this.screenshotName = str;
        this.restApi = restAPI;
        List<Sticker> list = sendingStickers;
        int random = f.random(n.getIndices(list), c.k);
        this.stickerCharacterIndex = random;
        this.sendingSticker = list.get(random);
        this.successSticker = successStickers.get(random);
        this.useScreenshot = true;
        this.eventSubject = PublishSubject.k0();
        loadFeatures();
    }

    /* renamed from: filterFeatures  reason: collision with other method in class */
    public final void m17filterFeatures(String str) {
        d0.z.d.m.checkNotNullParameter(str, "it");
        this.featureSearchQuery = str;
        showFeatureAreas();
    }
}
