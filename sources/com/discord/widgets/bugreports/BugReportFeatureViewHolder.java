package com.discord.widgets.bugreports;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.bugreport.Feature;
import com.discord.databinding.WidgetBugReportFeatureListItemBinding;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetBugReport.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR%\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/bugreports/BugReportFeatureViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/api/bugreport/Feature;", "data", "", "bind", "(Lcom/discord/api/bugreport/Feature;)V", "Lcom/discord/databinding/WidgetBugReportFeatureListItemBinding;", "binding", "Lcom/discord/databinding/WidgetBugReportFeatureListItemBinding;", "getBinding", "()Lcom/discord/databinding/WidgetBugReportFeatureListItemBinding;", "Lkotlin/Function1;", "onClickListener", "Lkotlin/jvm/functions/Function1;", "getOnClickListener", "()Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetBugReportFeatureListItemBinding;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BugReportFeatureViewHolder extends RecyclerView.ViewHolder {
    private final WidgetBugReportFeatureListItemBinding binding;
    private final Function1<Feature, Unit> onClickListener;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public BugReportFeatureViewHolder(WidgetBugReportFeatureListItemBinding widgetBugReportFeatureListItemBinding, Function1<? super Feature, Unit> function1) {
        super(widgetBugReportFeatureListItemBinding.a);
        m.checkNotNullParameter(widgetBugReportFeatureListItemBinding, "binding");
        m.checkNotNullParameter(function1, "onClickListener");
        this.binding = widgetBugReportFeatureListItemBinding;
        this.onClickListener = function1;
    }

    public final void bind(final Feature feature) {
        m.checkNotNullParameter(feature, "data");
        TextView textView = this.binding.f2227b;
        m.checkNotNullExpressionValue(textView, "binding.featureListItemFeature");
        textView.setText(feature.b());
        TextView textView2 = this.binding.c;
        m.checkNotNullExpressionValue(textView2, "binding.featureListItemSquad");
        textView2.setText(feature.c());
        this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.bugreports.BugReportFeatureViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BugReportFeatureViewHolder.this.getOnClickListener().invoke(feature);
            }
        });
    }

    public final WidgetBugReportFeatureListItemBinding getBinding() {
        return this.binding;
    }

    public final Function1<Feature, Unit> getOnClickListener() {
        return this.onClickListener;
    }
}
