package com.discord.widgets.bugreports;

import com.discord.widgets.bugreports.BugReportViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: BugReportViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "it", "invoke", "(Lkotlin/Unit;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BugReportViewModel$sendReport$1 extends o implements Function1<Unit, Unit> {
    public final /* synthetic */ BugReportViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BugReportViewModel$sendReport$1(BugReportViewModel bugReportViewModel) {
        super(1);
        this.this$0 = bugReportViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Unit unit) {
        invoke2(unit);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Unit unit) {
        BugReportViewModel bugReportViewModel = this.this$0;
        bugReportViewModel.updateViewState(new BugReportViewModel.ViewState.Success(bugReportViewModel.getSuccessSticker()));
        this.this$0.dismissAfterDelay();
    }
}
