package com.discord.widgets.bugreports;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.y.c0;
import b.a.y.d0;
import b.d.b.a.a;
import com.discord.api.bugreport.Feature;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetBugReportBinding;
import com.discord.screenshot_detection.ScreenshotDetector;
import com.discord.utilities.error.Error;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.SearchInputView;
import com.discord.views.sticker.StickerView;
import com.discord.widgets.bugreports.BugReportViewModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import d0.g;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.io.InputStream;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetBugReport.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 =2\u00020\u0001:\u0001=B\u0007¢\u0006\u0004\b<\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\u000b\u001a\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\r\u0010\bJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001b\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R#\u0010(\u001a\b\u0012\u0004\u0012\u00020#0\"8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001d\u0010.\u001a\u00020)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u001d\u00103\u001a\u00020/8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b0\u0010%\u001a\u0004\b1\u00102R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b5\u00106R\u001d\u0010;\u001a\u0002078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b8\u0010%\u001a\u0004\b9\u0010:¨\u0006>"}, d2 = {"Lcom/discord/widgets/bugreports/WidgetBugReport;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/bugreports/BugReportViewModel$Event;", "event", "", "handleEvent", "(Lcom/discord/widgets/bugreports/BugReportViewModel$Event;)V", "submit", "()V", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "onViewBoundOrOnResume", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "viewState", "updateView", "(Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;)V", "Lcom/discord/api/bugreport/Feature;", "feature", "onFeatureClickListener", "(Lcom/discord/api/bugreport/Feature;)V", "Lcom/discord/widgets/bugreports/BugReportFeatureAdapter;", "featuresAdapter", "Lcom/discord/widgets/bugreports/BugReportFeatureAdapter;", "getFeaturesAdapter", "()Lcom/discord/widgets/bugreports/BugReportFeatureAdapter;", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "imagesChangeDetector", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "", "Lb/a/y/d0;", "items$delegate", "Lkotlin/Lazy;", "getItems", "()Ljava/util/List;", "items", "Lcom/discord/databinding/WidgetBugReportBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetBugReportBinding;", "binding", "Lcom/discord/widgets/bugreports/BugReportViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/bugreports/BugReportViewModel;", "viewModel", "", "backHandlerAdded", "Z", "Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager$delegate", "getValidationManager", "()Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetBugReport extends AppFragment {
    public static final String INTENT_EXTRA_SCREENSHOT_FILENAME = "extra_screenshot_filename";
    public static final String INTENT_EXTRA_SCREENSHOT_URI = "extra_screenshot_uri";
    public static final int VIEW_INDEX_FEATURE_AREA = 2;
    public static final int VIEW_INDEX_REPORT = 0;
    public static final int VIEW_INDEX_STATUS = 1;
    private boolean backHandlerAdded;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetBugReport$binding$2.INSTANCE, null, 2, null);
    private final BugReportFeatureAdapter featuresAdapter = new BugReportFeatureAdapter(new WidgetBugReport$featuresAdapter$1(this));
    private final MGImages.DistinctChangeDetector imagesChangeDetector = new MGImages.DistinctChangeDetector();
    private final Lazy items$delegate = g.lazy(new WidgetBugReport$items$2(this));
    private final Lazy validationManager$delegate = g.lazy(new WidgetBugReport$validationManager$2(this));
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetBugReport.class, "binding", "getBinding()Lcom/discord/databinding/WidgetBugReportBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final List<Priority> priorityLevels = n.listOf((Object[]) new Priority[]{new Priority(R.string.bug_report_priority_critical_title, R.string.bug_report_priority_critical_description, 801497159479722084L), new Priority(R.string.bug_report_priority_high_title, R.string.bug_report_priority_high_description, 410336837563973632L), new Priority(R.string.bug_report_priority_low_title, R.string.bug_report_priority_low_description, 841420679643529296L), new Priority(R.string.bug_report_priority_very_low_title, R.string.bug_report_priority_very_low_description, 827645852352512021L)});

    /* compiled from: WidgetBugReport.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u000f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011R\u0016\u0010\u0014\u001a\u00020\u00138\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00138\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00138\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0015¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/bugreports/WidgetBugReport$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/screenshot_detection/ScreenshotDetector$Screenshot;", "screenshot", "", "launch", "(Landroid/content/Context;Lcom/discord/screenshot_detection/ScreenshotDetector$Screenshot;)V", "", "Lcom/discord/widgets/bugreports/Priority;", "priorityLevels", "Ljava/util/List;", "getPriorityLevels", "()Ljava/util/List;", "", "INTENT_EXTRA_SCREENSHOT_FILENAME", "Ljava/lang/String;", "INTENT_EXTRA_SCREENSHOT_URI", "", "VIEW_INDEX_FEATURE_AREA", "I", "VIEW_INDEX_REPORT", "VIEW_INDEX_STATUS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final List<Priority> getPriorityLevels() {
            return WidgetBugReport.priorityLevels;
        }

        public final void launch(Context context, ScreenshotDetector.Screenshot screenshot) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(screenshot, "screenshot");
            Intent intent = new Intent();
            intent.putExtra(WidgetBugReport.INTENT_EXTRA_SCREENSHOT_URI, screenshot.a.toString());
            intent.putExtra(WidgetBugReport.INTENT_EXTRA_SCREENSHOT_FILENAME, screenshot.f2777b);
            j.d(context, WidgetBugReport.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetBugReport() {
        super(R.layout.widget_bug_report);
        WidgetBugReport$viewModel$2 widgetBugReport$viewModel$2 = new WidgetBugReport$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(BugReportViewModel.class), new WidgetBugReport$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetBugReport$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetBugReportBinding getBinding() {
        return (WidgetBugReportBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final ValidationManager getValidationManager() {
        return (ValidationManager) this.validationManager$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final BugReportViewModel getViewModel() {
        return (BugReportViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(BugReportViewModel.Event event) {
        AppActivity appActivity;
        if (m.areEqual(event, BugReportViewModel.Event.CloseReport.INSTANCE) && (appActivity = getAppActivity()) != null) {
            appActivity.finish();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void submit() {
        InputStream openInputStream;
        byte[] readBytes;
        RequestBody requestBody = null;
        MediaType mediaType = null;
        requestBody = null;
        requestBody = null;
        requestBody = null;
        if (ValidationManager.validate$default(getValidationManager(), false, 1, null)) {
            Uri screenshotUri = getViewModel().getScreenshotUri();
            if (!(!getViewModel().getUseScreenshot() || screenshotUri.getPath() == null || (openInputStream = requireContext().getContentResolver().openInputStream(screenshotUri)) == null || (readBytes = d0.y.a.readBytes(openInputStream)) == null)) {
                RequestBody.Companion companion = RequestBody.Companion;
                String type = requireContext().getContentResolver().getType(screenshotUri);
                if (type != null) {
                    MediaType.a aVar = MediaType.c;
                    mediaType = MediaType.a.a(type);
                }
                requestBody = RequestBody.Companion.d(companion, readBytes, mediaType, 0, 0, 6);
            }
            getViewModel().sendReport(requestBody);
        }
    }

    public final BugReportFeatureAdapter getFeaturesAdapter() {
        return this.featuresAdapter;
    }

    public final List<d0> getItems() {
        return (List) this.items$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getViewModel().prefetchStickers(requireContext());
    }

    public final void onFeatureClickListener(Feature feature) {
        m.checkNotNullParameter(feature, "feature");
        getViewModel().selectFeatureArea(feature);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarTitle(R.string.submit_bug);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        Uri parse = Uri.parse(getMostRecentIntent().getStringExtra(INTENT_EXTRA_SCREENSHOT_URI));
        RecyclerView recyclerView = getBinding().f;
        m.checkNotNullExpressionValue(recyclerView, "binding.bugReportFeatureRecycler");
        recyclerView.setAdapter(this.featuresAdapter);
        getBinding().l.setImageURI(parse);
        getBinding().f2224b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.bugreports.WidgetBugReport$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BugReportViewModel viewModel;
                viewModel = WidgetBugReport.this.getViewModel();
                viewModel.useScreenshot(false);
            }
        });
        TextInputLayout textInputLayout = getBinding().h;
        m.checkNotNullExpressionValue(textInputLayout, "binding.bugReportName");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetBugReport$onViewBound$2(this));
        TextInputLayout textInputLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.bugReportDescription");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, new WidgetBugReport$onViewBound$3(this));
        getBinding().g.a(this, new WidgetBugReport$onViewBound$4(this));
        getBinding().q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.bugreports.WidgetBugReport$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BugReportViewModel viewModel;
                viewModel = WidgetBugReport.this.getViewModel();
                viewModel.useScreenshot(true);
            }
        });
        getBinding().t.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.bugreports.WidgetBugReport$onViewBound$6

            /* compiled from: WidgetBugReport.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "priority", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.bugreports.WidgetBugReport$onViewBound$6$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Integer, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                    invoke(num.intValue());
                    return Unit.a;
                }

                public final void invoke(int i) {
                    BugReportViewModel viewModel;
                    viewModel = WidgetBugReport.this.getViewModel();
                    viewModel.updatePriority(i);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AppFragment.hideKeyboard$default(WidgetBugReport.this, null, 1, null);
                c0.a aVar = c0.k;
                FragmentManager childFragmentManager = WidgetBugReport.this.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                String string = WidgetBugReport.this.getString(R.string.bug_report_priority);
                m.checkNotNullExpressionValue(string, "getString(R.string.bug_report_priority)");
                c0.a.b(aVar, childFragmentManager, string, WidgetBugReport.this.getItems(), false, new AnonymousClass1(), 8);
            }
        });
        getBinding().f2225s.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.bugreports.WidgetBugReport$onViewBound$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BugReportViewModel viewModel;
                viewModel = WidgetBugReport.this.getViewModel();
                viewModel.showFeatureAreas();
            }
        });
        getBinding().o.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.bugreports.WidgetBugReport$onViewBound$8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetBugReport.this.submit();
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetBugReport.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetBugReport$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetBugReport.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetBugReport$onViewBoundOrOnResume$2(this));
    }

    public final void updateView(BugReportViewModel.ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        String str = "";
        int i = 8;
        if (viewState instanceof BugReportViewModel.ViewState.Report) {
            setActionBarTitle(R.string.submit_bug);
            setActionBarSubtitle(str);
            AppViewFlipper appViewFlipper = getBinding().p;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.bugReportSwitchFlipper");
            appViewFlipper.setDisplayedChild(0);
            MaterialTextView materialTextView = getBinding().r;
            m.checkNotNullExpressionValue(materialTextView, "binding.submitReportError");
            BugReportViewModel.ViewState.Report report = (BugReportViewModel.ViewState.Report) viewState;
            materialTextView.setVisibility(report.getError() != null ? 0 : 8);
            if (report.getError() != null) {
                MaterialTextView materialTextView2 = getBinding().r;
                m.checkNotNullExpressionValue(materialTextView2, "binding.submitReportError");
                Error.Response response = report.getError().getResponse();
                m.checkNotNullExpressionValue(response, "viewState.error.response");
                materialTextView2.setText(response.getMessage());
            }
            TextInputLayout textInputLayout = getBinding().h;
            m.checkNotNullExpressionValue(textInputLayout, "binding.bugReportName");
            ViewExtensions.setTextIfDifferent(textInputLayout, report.getReportName());
            TextInputLayout textInputLayout2 = getBinding().c;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.bugReportDescription");
            ViewExtensions.setTextIfDifferent(textInputLayout2, report.getReportDescription());
            AppCompatImageView appCompatImageView = getBinding().l;
            m.checkNotNullExpressionValue(appCompatImageView, "binding.bugReportScreenshot");
            appCompatImageView.setVisibility(report.getUseScreenshot() ^ true ? 4 : 0);
            AppCompatImageView appCompatImageView2 = getBinding().f2224b;
            m.checkNotNullExpressionValue(appCompatImageView2, "binding.bugReportClearScreenshot");
            appCompatImageView2.setVisibility(report.getUseScreenshot() ? 0 : 8);
            MaterialTextView materialTextView3 = getBinding().i;
            m.checkNotNullExpressionValue(materialTextView3, "binding.bugReportNoScreenshotLabel");
            materialTextView3.setVisibility(report.getUseScreenshot() ^ true ? 0 : 8);
            MaterialButton materialButton = getBinding().q;
            m.checkNotNullExpressionValue(materialButton, "binding.bugReportUndoScreenshotRemove");
            if (!report.getUseScreenshot()) {
                i = 0;
            }
            materialButton.setVisibility(i);
            Feature feature = report.getFeature();
            if (feature != null) {
                TextInputLayout textInputLayout3 = getBinding().d;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.bugReportFeatureArea");
                ViewExtensions.setTextIfDifferent(textInputLayout3, feature.b());
            }
            Integer priority = report.getPriority();
            if (priority != null) {
                int intValue = priority.intValue();
                TextInputLayout textInputLayout4 = getBinding().j;
                m.checkNotNullExpressionValue(textInputLayout4, "binding.bugReportPriority");
                ViewExtensions.setText(textInputLayout4, priorityLevels.get(intValue).getTitle());
                SimpleDraweeView simpleDraweeView = getBinding().k;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.bugReportPriorityIcon");
                MGImages.setImage$default(simpleDraweeView, getItems().get(intValue).d(), R.dimen.emoji_size, R.dimen.emoji_size, true, null, this.imagesChangeDetector, 32, null);
            }
        } else if (viewState instanceof BugReportViewModel.ViewState.Sending) {
            AppViewFlipper appViewFlipper2 = getBinding().p;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.bugReportSwitchFlipper");
            appViewFlipper2.setDisplayedChild(1);
            StickerView.e(getBinding().m, getViewModel().getSendingSticker(), null, 2);
            getBinding().n.setText(R.string.bug_report_status_sending);
        } else if (viewState instanceof BugReportViewModel.ViewState.Success) {
            AppViewFlipper appViewFlipper3 = getBinding().p;
            m.checkNotNullExpressionValue(appViewFlipper3, "binding.bugReportSwitchFlipper");
            appViewFlipper3.setDisplayedChild(1);
            StickerView.e(getBinding().m, getViewModel().getSuccessSticker(), null, 2);
            getBinding().n.setText(R.string.bug_report_status_sent);
        } else if (viewState instanceof BugReportViewModel.ViewState.SelectFeature) {
            setActionBarTitle(R.string.bug_report_select_feature_area);
            setActionBarSubtitle(R.string.submit_bug);
            AppViewFlipper appViewFlipper4 = getBinding().p;
            m.checkNotNullExpressionValue(appViewFlipper4, "binding.bugReportSwitchFlipper");
            appViewFlipper4.setDisplayedChild(2);
            ProgressBar progressBar = getBinding().e;
            m.checkNotNullExpressionValue(progressBar, "binding.bugReportFeatureLoader");
            BugReportViewModel.ViewState.SelectFeature selectFeature = (BugReportViewModel.ViewState.SelectFeature) viewState;
            progressBar.setVisibility(selectFeature.getLoadingFeatures() ? 0 : 8);
            RecyclerView recyclerView = getBinding().f;
            m.checkNotNullExpressionValue(recyclerView, "binding.bugReportFeatureRecycler");
            if (!selectFeature.getLoadingFeatures()) {
                i = 0;
            }
            recyclerView.setVisibility(i);
            View editText = getBinding().g.getEditText();
            if (!(editText instanceof TextInputLayout)) {
                editText = null;
            }
            TextInputLayout textInputLayout5 = (TextInputLayout) editText;
            if (textInputLayout5 != null) {
                EditText editText2 = textInputLayout5.getEditText();
                String valueOf = String.valueOf(editText2 != null ? editText2.getText() : null);
                String query = selectFeature.getQuery();
                if (query == null) {
                    query = str;
                }
                if (!m.areEqual(valueOf, query)) {
                    SearchInputView searchInputView = getBinding().g;
                    String query2 = selectFeature.getQuery();
                    if (query2 != null) {
                        str = query2;
                    }
                    searchInputView.setText(str);
                }
            }
            this.featuresAdapter.setData(selectFeature.getFeatures());
            if (!this.backHandlerAdded) {
                this.backHandlerAdded = true;
                AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.bugreports.WidgetBugReport$updateView$4

                    /* compiled from: WidgetBugReport.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "()Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.bugreports.WidgetBugReport$updateView$4$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1<R> implements Func0<Boolean> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        @Override // rx.functions.Func0, java.util.concurrent.Callable
                        public final Boolean call() {
                            return Boolean.FALSE;
                        }
                    }

                    @Override // rx.functions.Func0, java.util.concurrent.Callable
                    public final Boolean call() {
                        BugReportViewModel viewModel;
                        WidgetBugReport.this.backHandlerAdded = false;
                        viewModel = WidgetBugReport.this.getViewModel();
                        BugReportViewModel.showReport$default(viewModel, null, 1, null);
                        AppFragment.setOnBackPressed$default(WidgetBugReport.this, AnonymousClass1.INSTANCE, 0, 2, null);
                        return Boolean.TRUE;
                    }
                }, 0, 2, null);
            }
        }
    }
}
