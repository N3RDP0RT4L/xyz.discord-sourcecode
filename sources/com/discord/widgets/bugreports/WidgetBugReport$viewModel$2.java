package com.discord.widgets.bugreports;

import android.net.Uri;
import com.discord.app.AppViewModel;
import com.discord.widgets.bugreports.BugReportViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetBugReport.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/bugreports/BugReportViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetBugReport$viewModel$2 extends o implements Function0<AppViewModel<BugReportViewModel.ViewState>> {
    public final /* synthetic */ WidgetBugReport this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetBugReport$viewModel$2(WidgetBugReport widgetBugReport) {
        super(0);
        this.this$0 = widgetBugReport;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<BugReportViewModel.ViewState> invoke() {
        Uri parse = Uri.parse(this.this$0.getMostRecentIntent().getStringExtra(WidgetBugReport.INTENT_EXTRA_SCREENSHOT_URI));
        String stringExtra = this.this$0.getMostRecentIntent().getStringExtra(WidgetBugReport.INTENT_EXTRA_SCREENSHOT_FILENAME);
        if (stringExtra == null) {
            stringExtra = "";
        }
        m.checkNotNullExpressionValue(parse, "screenshotUri");
        return new BugReportViewModel(parse, stringExtra, null, 4, null);
    }
}
