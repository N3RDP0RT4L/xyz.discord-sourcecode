package com.discord.widgets.guilds;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetGuildFolderSettingsBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildFolderSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildFolderSettingsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildFolderSettingsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildFolderSettings$binding$2 extends k implements Function1<View, WidgetGuildFolderSettingsBinding> {
    public static final WidgetGuildFolderSettings$binding$2 INSTANCE = new WidgetGuildFolderSettings$binding$2();

    public WidgetGuildFolderSettings$binding$2() {
        super(1, WidgetGuildFolderSettingsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildFolderSettingsBinding;", 0);
    }

    public final WidgetGuildFolderSettingsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.edit_guild_folder_name;
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.edit_guild_folder_name);
        if (textInputLayout != null) {
            i = R.id.guild_folder_color_name;
            TextView textView = (TextView) view.findViewById(R.id.guild_folder_color_name);
            if (textView != null) {
                i = R.id.guild_folder_settings_color;
                RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.guild_folder_settings_color);
                if (relativeLayout != null) {
                    i = R.id.guild_folder_settings_color_display;
                    View findViewById = view.findViewById(R.id.guild_folder_settings_color_display);
                    if (findViewById != null) {
                        i = R.id.guild_folder_settings_save;
                        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.guild_folder_settings_save);
                        if (floatingActionButton != null) {
                            return new WidgetGuildFolderSettingsBinding((CoordinatorLayout) view, textInputLayout, textView, relativeLayout, findViewById, floatingActionButton);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
