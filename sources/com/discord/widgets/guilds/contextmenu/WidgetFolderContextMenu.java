package com.discord.widgets.guilds.contextmenu;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetFolderContextMenuBinding;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guilds.WidgetGuildFolderSettings;
import com.discord.widgets.guilds.contextmenu.FolderContextMenuViewModel;
import com.discord.widgets.guilds.contextmenu.WidgetFolderContextMenu;
import d0.d0.f;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetFolderContextMenu.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 &2\u00020\u0001:\u0002'&B\u0007¢\u0006\u0004\b%\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0010\u0010\u000fJ\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\u000fJ\u000f\u0010\u0012\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\u000fR\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001b\u001a\u00020\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010!\u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0018\u0010#\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006("}, d2 = {"Lcom/discord/widgets/guilds/contextmenu/WidgetFolderContextMenu;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel$ViewState;)V", "Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel$Event;)V", "Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel$ViewState$Valid;", "configureValidUI", "(Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel$ViewState$Valid;)V", "doCircularReveal", "()V", "doCircularRemove", "onResume", "onDestroy", "Landroid/animation/Animator;", "animator", "Landroid/animation/Animator;", "Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/contextmenu/FolderContextMenuViewModel;", "viewModel", "Lcom/discord/databinding/WidgetFolderContextMenuBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetFolderContextMenuBinding;", "binding", "Lcom/discord/widgets/guilds/contextmenu/WidgetFolderContextMenu$AnimationState;", "animationState", "Lcom/discord/widgets/guilds/contextmenu/WidgetFolderContextMenu$AnimationState;", HookHelper.constructorName, "Companion", "AnimationState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFolderContextMenu extends AppFragment {
    private static final String FRAGMENT_TAG = "WidgetFolderContextMenu";
    private static final String VIEW_CONTAINER_TAG = "WidgetFolderContextMenuViewContainer";
    private static boolean isShowingContextMenu;
    private AnimationState animationState;
    private Animator animator;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetFolderContextMenu$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetFolderContextMenu.class, "binding", "getBinding()Lcom/discord/databinding/WidgetFolderContextMenuBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int SCREEN_BOTTOM_BUFFER = DimenUtils.dpToPixels(96);

    /* compiled from: WidgetFolderContextMenu.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0082\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/contextmenu/WidgetFolderContextMenu$AnimationState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ANIMATING_IN", "ANIMATING_OUT", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum AnimationState {
        ANIMATING_IN,
        ANIMATING_OUT
    }

    /* compiled from: WidgetFolderContextMenu.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b \u0010!J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ)\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u001d\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001aR\u0016\u0010\u001e\u001a\u00020\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001f¨\u0006\""}, d2 = {"Lcom/discord/widgets/guilds/contextmenu/WidgetFolderContextMenu$Companion;", "", "", "Lcom/discord/primitives/FolderId;", "folderId", "Lcom/discord/widgets/guilds/contextmenu/WidgetFolderContextMenu;", "newInstance", "(J)Lcom/discord/widgets/guilds/contextmenu/WidgetFolderContextMenu;", "Landroid/content/Context;", "context", "", "computeMaxContextMenuHeight", "(Landroid/content/Context;)I", "Landroidx/fragment/app/FragmentActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Landroid/graphics/PointF;", "absolutePosition", "", "show", "(Landroidx/fragment/app/FragmentActivity;Landroid/graphics/PointF;J)V", "", "animate", "hide", "(Landroidx/fragment/app/FragmentActivity;Z)V", "", "FRAGMENT_TAG", "Ljava/lang/String;", "SCREEN_BOTTOM_BUFFER", "I", "VIEW_CONTAINER_TAG", "isShowingContextMenu", "Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final int computeMaxContextMenuHeight(Context context) {
            WidgetFolderContextMenuBinding a = WidgetFolderContextMenuBinding.a(LayoutInflater.from(context).inflate(R.layout.widget_folder_context_menu, (ViewGroup) null, false));
            m.checkNotNullExpressionValue(a, "WidgetFolderContextMenuB…om(context), null, false)");
            CardView cardView = a.a;
            m.checkNotNullExpressionValue(cardView, "WidgetFolderContextMenuB…ntext), null, false).root");
            cardView.measure(0, 0);
            return cardView.getMeasuredHeight();
        }

        private final WidgetFolderContextMenu newInstance(long j) {
            Bundle I = a.I("com.discord.intent.extra.EXTRA_GUILD_FOLDER_ID", j);
            WidgetFolderContextMenu widgetFolderContextMenu = new WidgetFolderContextMenu();
            widgetFolderContextMenu.setArguments(I);
            return widgetFolderContextMenu;
        }

        public final void hide(FragmentActivity fragmentActivity, boolean z2) {
            m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            Fragment findFragmentByTag = fragmentActivity.getSupportFragmentManager().findFragmentByTag(WidgetFolderContextMenu.FRAGMENT_TAG);
            if (!(findFragmentByTag instanceof WidgetFolderContextMenu)) {
                findFragmentByTag = null;
            }
            WidgetFolderContextMenu widgetFolderContextMenu = (WidgetFolderContextMenu) findFragmentByTag;
            if (widgetFolderContextMenu == null) {
                return;
            }
            if (z2) {
                widgetFolderContextMenu.doCircularRemove();
                return;
            }
            fragmentActivity.getSupportFragmentManager().beginTransaction().remove(widgetFolderContextMenu).commitAllowingStateLoss();
            Window window = fragmentActivity.getWindow();
            m.checkNotNullExpressionValue(window, "activity.window");
            View decorView = window.getDecorView();
            m.checkNotNullExpressionValue(decorView, "activity.window.decorView");
            View rootView = decorView.getRootView();
            Objects.requireNonNull(rootView, "null cannot be cast to non-null type android.view.ViewGroup");
            ViewGroup viewGroup = (ViewGroup) rootView;
            Window window2 = fragmentActivity.getWindow();
            m.checkNotNullExpressionValue(window2, "activity.window");
            View decorView2 = window2.getDecorView();
            m.checkNotNullExpressionValue(decorView2, "activity.window.decorView");
            FrameLayout frameLayout = (FrameLayout) decorView2.getRootView().findViewWithTag(WidgetFolderContextMenu.VIEW_CONTAINER_TAG);
            if (frameLayout != null) {
                viewGroup.removeView(frameLayout);
            }
            WidgetFolderContextMenu.isShowingContextMenu = false;
        }

        public final void show(final FragmentActivity fragmentActivity, PointF pointF, long j) {
            m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            m.checkNotNullParameter(pointF, "absolutePosition");
            if (!WidgetFolderContextMenu.isShowingContextMenu) {
                WidgetFolderContextMenu.isShowingContextMenu = true;
                computeMaxContextMenuHeight(fragmentActivity);
                hide(fragmentActivity, false);
                FrameLayout frameLayout = new FrameLayout(fragmentActivity);
                frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                frameLayout.setTag(WidgetFolderContextMenu.VIEW_CONTAINER_TAG);
                Window window = fragmentActivity.getWindow();
                m.checkNotNullExpressionValue(window, "activity.window");
                View decorView = window.getDecorView();
                m.checkNotNullExpressionValue(decorView, "activity.window.decorView");
                View rootView = decorView.getRootView();
                Objects.requireNonNull(rootView, "null cannot be cast to non-null type android.view.ViewGroup");
                ViewGroup viewGroup = (ViewGroup) rootView;
                viewGroup.addView(frameLayout);
                frameLayout.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetFolderContextMenu$Companion$show$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetFolderContextMenu.Companion.hide(FragmentActivity.this, true);
                    }
                });
                FrameLayout frameLayout2 = new FrameLayout(fragmentActivity);
                frameLayout2.setId(View.generateViewId());
                frameLayout2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
                frameLayout.addView(frameLayout2);
                frameLayout2.setX(pointF.x);
                frameLayout2.setY(f.coerceAtMost(pointF.y, (viewGroup.getHeight() - computeMaxContextMenuHeight(fragmentActivity)) - WidgetFolderContextMenu.SCREEN_BOTTOM_BUFFER));
                fragmentActivity.getSupportFragmentManager().beginTransaction().add(frameLayout2.getId(), newInstance(j), WidgetFolderContextMenu.FRAGMENT_TAG).commit();
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetFolderContextMenu() {
        super(R.layout.widget_folder_context_menu);
        WidgetFolderContextMenu$viewModel$2 widgetFolderContextMenu$viewModel$2 = new WidgetFolderContextMenu$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(FolderContextMenuViewModel.class), new WidgetFolderContextMenu$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetFolderContextMenu$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(FolderContextMenuViewModel.ViewState viewState) {
        if (viewState instanceof FolderContextMenuViewModel.ViewState.Valid) {
            configureValidUI((FolderContextMenuViewModel.ViewState.Valid) viewState);
        } else if (m.areEqual(viewState, FolderContextMenuViewModel.ViewState.Invalid.INSTANCE)) {
            Companion companion = Companion;
            FragmentActivity requireActivity = requireActivity();
            m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            companion.hide(requireActivity, false);
        }
    }

    private final void configureValidUI(final FolderContextMenuViewModel.ViewState.Valid valid) {
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.folderContextMenuHeader");
        CharSequence name = valid.getFolder().getName();
        boolean z2 = false;
        if (name == null) {
            name = b.e(this, R.string.guild_folder_unnamed, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        }
        textView.setText(name);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetFolderContextMenu$configureValidUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                FolderContextMenuViewModel viewModel;
                WidgetFolderContextMenu.this.doCircularRemove();
                viewModel = WidgetFolderContextMenu.this.getViewModel();
                viewModel.onMarkAsReadClicked();
            }
        });
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetFolderContextMenu$configureValidUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                FolderContextMenuViewModel viewModel;
                WidgetFolderContextMenuBinding binding;
                WidgetFolderContextMenu.this.doCircularRemove();
                Long id2 = valid.getFolder().getId();
                if (id2 != null) {
                    WidgetGuildFolderSettings.Companion companion = WidgetGuildFolderSettings.Companion;
                    binding = WidgetFolderContextMenu.this.getBinding();
                    TextView textView2 = binding.e;
                    m.checkNotNullExpressionValue(textView2, "binding.folderContextMenuSettings");
                    Context context = textView2.getContext();
                    m.checkNotNullExpressionValue(context, "binding.folderContextMenuSettings.context");
                    companion.create(context, id2.longValue());
                }
                viewModel = WidgetFolderContextMenu.this.getViewModel();
                viewModel.onSettingsClicked();
            }
        });
        TextView textView2 = getBinding().d;
        m.checkNotNullExpressionValue(textView2, "binding.folderContextMenuMarkAsRead");
        textView2.setVisibility(valid.getShowMarkAsRead() ? 0 : 8);
        getBinding().f2367b.setContentPadding(0, 0, 0, 0);
        CardView cardView = getBinding().f2367b;
        m.checkNotNullExpressionValue(cardView, "binding.folderContextMenuCard");
        if (cardView.getVisibility() == 0) {
            z2 = true;
        }
        if (!z2) {
            doCircularReveal();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void doCircularRemove() {
        AnimationState animationState = this.animationState;
        if (animationState == AnimationState.ANIMATING_IN) {
            Animator animator = this.animator;
            if (animator != null) {
                animator.cancel();
            }
            Companion companion = Companion;
            FragmentActivity requireActivity = requireActivity();
            m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            companion.hide(requireActivity, false);
            return;
        }
        AnimationState animationState2 = AnimationState.ANIMATING_OUT;
        if (animationState != animationState2) {
            CardView cardView = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView, "binding.folderContextMenuCard");
            int height = cardView.getHeight() / 2;
            CardView cardView2 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView2, "binding.folderContextMenuCard");
            int width = cardView2.getWidth();
            CardView cardView3 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView3, "binding.folderContextMenuCard");
            float hypot = (float) Math.hypot(width, cardView3.getHeight() / 2);
            CardView cardView4 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView4, "binding.folderContextMenuCard");
            if (cardView4.isAttachedToWindow()) {
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(getBinding().f2367b, 0, height, hypot, 0.0f);
                this.animator = createCircularReveal;
                this.animationState = animationState2;
                m.checkNotNullExpressionValue(createCircularReveal, "animator");
                createCircularReveal.setDuration(200L);
                createCircularReveal.addListener(new AnimatorListenerAdapter() { // from class: com.discord.widgets.guilds.contextmenu.WidgetFolderContextMenu$doCircularRemove$1
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator2) {
                        super.onAnimationEnd(animator2);
                        FragmentActivity activity = WidgetFolderContextMenu.this.e();
                        if (activity != null) {
                            WidgetFolderContextMenu.Companion companion2 = WidgetFolderContextMenu.Companion;
                            m.checkNotNullExpressionValue(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                            companion2.hide(activity, false);
                        }
                    }
                });
                createCircularReveal.start();
                return;
            }
            CardView cardView5 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView5, "binding.folderContextMenuCard");
            cardView5.setVisibility(8);
        }
    }

    private final void doCircularReveal() {
        CardView cardView = getBinding().f2367b;
        m.checkNotNullExpressionValue(cardView, "binding.folderContextMenuCard");
        if (cardView.isAttachedToWindow()) {
            CardView cardView2 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView2, "binding.folderContextMenuCard");
            CardView cardView3 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView3, "binding.folderContextMenuCard");
            int width = cardView3.getWidth();
            CardView cardView4 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView4, "binding.folderContextMenuCard");
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(getBinding().f2367b, 0, cardView2.getHeight() / 2, 0.0f, (float) Math.hypot(width, cardView4.getHeight() / 2));
            this.animator = createCircularReveal;
            this.animationState = AnimationState.ANIMATING_IN;
            m.checkNotNullExpressionValue(createCircularReveal, "animator");
            createCircularReveal.setDuration(200L);
            createCircularReveal.addListener(new AnimatorListenerAdapter() { // from class: com.discord.widgets.guilds.contextmenu.WidgetFolderContextMenu$doCircularReveal$1
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    super.onAnimationEnd(animator);
                    WidgetFolderContextMenu.this.animationState = null;
                }
            });
            CardView cardView5 = getBinding().f2367b;
            m.checkNotNullExpressionValue(cardView5, "binding.folderContextMenuCard");
            cardView5.setVisibility(0);
            createCircularReveal.start();
            return;
        }
        CardView cardView6 = getBinding().f2367b;
        m.checkNotNullExpressionValue(cardView6, "binding.folderContextMenuCard");
        cardView6.setVisibility(0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetFolderContextMenuBinding getBinding() {
        return (WidgetFolderContextMenuBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final FolderContextMenuViewModel getViewModel() {
        return (FolderContextMenuViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(FolderContextMenuViewModel.Event event) {
        if (m.areEqual(event, FolderContextMenuViewModel.Event.Dismiss.INSTANCE)) {
            doCircularRemove();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        isShowingContextMenu = false;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetFolderContextMenu.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFolderContextMenu$onResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetFolderContextMenu.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFolderContextMenu$onResume$2(this));
    }
}
