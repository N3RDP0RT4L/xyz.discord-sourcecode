package com.discord.widgets.guilds.contextmenu;

import com.discord.widgets.guilds.contextmenu.FolderContextMenuViewModel;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: FolderContextMenuViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u001e\u0010\u0004\u001a\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u0001 \u0003*\f\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0018\u00010\u00020\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Ljava/lang/Void;", "", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FolderContextMenuViewModel$onMarkAsReadClicked$1 extends o implements Function1<List<Void>, Unit> {
    public final /* synthetic */ FolderContextMenuViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FolderContextMenuViewModel$onMarkAsReadClicked$1(FolderContextMenuViewModel folderContextMenuViewModel) {
        super(1);
        this.this$0 = folderContextMenuViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<Void> list) {
        invoke2(list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Void> list) {
        PublishSubject publishSubject;
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(FolderContextMenuViewModel.Event.Dismiss.INSTANCE);
    }
}
