package com.discord.widgets.guilds.contextmenu;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGuildContextMenuBinding;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guilds.contextmenu.GuildContextMenuViewModel;
import com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu;
import com.discord.widgets.guilds.leave.WidgetLeaveGuildDialog;
import com.discord.widgets.guilds.profile.WidgetGuildProfileSheet;
import com.discord.widgets.servers.WidgetServerNotifications;
import d0.d0.f;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetGuildContextMenu.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 &2\u00020\u0001:\u0002'&B\u0007¢\u0006\u0004\b%\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0010\u0010\u000fJ\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\u000fJ\u000f\u0010\u0012\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\u000fR\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u001d\u0010!\u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0018\u0010#\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006("}, d2 = {"Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;)V", "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V", "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;", "configureValidUI", "(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V", "doCircularReveal", "()V", "doCircularRemove", "onResume", "onDestroy", "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;", "viewModel", "Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;", "animationState", "Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;", "Lcom/discord/databinding/WidgetGuildContextMenuBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildContextMenuBinding;", "binding", "Landroid/animation/Animator;", "animator", "Landroid/animation/Animator;", HookHelper.constructorName, "Companion", "AnimationState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildContextMenu extends AppFragment {
    private static final String FRAGMENT_TAG = "WidgetGuildContextMenu";
    private static final String VIEW_CONTAINER_TAG = "WidgetGuildContextMenuViewContainer";
    private static boolean isShowingContextMenu;
    private AnimationState animationState;
    private Animator animator;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildContextMenu$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildContextMenu.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildContextMenuBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int SCREEN_BOTTOM_BUFFER = DimenUtils.dpToPixels(96);

    /* compiled from: WidgetGuildContextMenu.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0082\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ANIMATING_IN", "ANIMATING_OUT", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum AnimationState {
        ANIMATING_IN,
        ANIMATING_OUT
    }

    /* compiled from: WidgetGuildContextMenu.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b \u0010!J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ)\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u001d\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001aR\u0016\u0010\u001e\u001a\u00020\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001f¨\u0006\""}, d2 = {"Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;", "newInstance", "(J)Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;", "Landroid/content/Context;", "context", "", "computeMaxContextMenuHeight", "(Landroid/content/Context;)I", "Landroidx/fragment/app/FragmentActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Landroid/graphics/PointF;", "absolutePosition", "", "show", "(Landroidx/fragment/app/FragmentActivity;Landroid/graphics/PointF;J)V", "", "animate", "hide", "(Landroidx/fragment/app/FragmentActivity;Z)V", "", "FRAGMENT_TAG", "Ljava/lang/String;", "SCREEN_BOTTOM_BUFFER", "I", "VIEW_CONTAINER_TAG", "isShowingContextMenu", "Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final int computeMaxContextMenuHeight(Context context) {
            WidgetGuildContextMenuBinding a = WidgetGuildContextMenuBinding.a(LayoutInflater.from(context).inflate(R.layout.widget_guild_context_menu, (ViewGroup) null, false));
            m.checkNotNullExpressionValue(a, "WidgetGuildContextMenuBi…om(context), null, false)");
            CardView cardView = a.a;
            m.checkNotNullExpressionValue(cardView, "WidgetGuildContextMenuBi…ntext), null, false).root");
            cardView.measure(0, 0);
            return cardView.getMeasuredHeight();
        }

        private final WidgetGuildContextMenu newInstance(long j) {
            Bundle I = a.I("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            WidgetGuildContextMenu widgetGuildContextMenu = new WidgetGuildContextMenu();
            widgetGuildContextMenu.setArguments(I);
            return widgetGuildContextMenu;
        }

        public final void hide(FragmentActivity fragmentActivity, boolean z2) {
            m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            Fragment findFragmentByTag = fragmentActivity.getSupportFragmentManager().findFragmentByTag(WidgetGuildContextMenu.FRAGMENT_TAG);
            if (!(findFragmentByTag instanceof WidgetGuildContextMenu)) {
                findFragmentByTag = null;
            }
            WidgetGuildContextMenu widgetGuildContextMenu = (WidgetGuildContextMenu) findFragmentByTag;
            if (widgetGuildContextMenu == null) {
                return;
            }
            if (z2) {
                widgetGuildContextMenu.doCircularRemove();
                return;
            }
            fragmentActivity.getSupportFragmentManager().beginTransaction().remove(widgetGuildContextMenu).commitAllowingStateLoss();
            Window window = fragmentActivity.getWindow();
            m.checkNotNullExpressionValue(window, "activity.window");
            View decorView = window.getDecorView();
            m.checkNotNullExpressionValue(decorView, "activity.window.decorView");
            View rootView = decorView.getRootView();
            Objects.requireNonNull(rootView, "null cannot be cast to non-null type android.view.ViewGroup");
            ViewGroup viewGroup = (ViewGroup) rootView;
            Window window2 = fragmentActivity.getWindow();
            m.checkNotNullExpressionValue(window2, "activity.window");
            View decorView2 = window2.getDecorView();
            m.checkNotNullExpressionValue(decorView2, "activity.window.decorView");
            FrameLayout frameLayout = (FrameLayout) decorView2.getRootView().findViewWithTag(WidgetGuildContextMenu.VIEW_CONTAINER_TAG);
            if (frameLayout != null) {
                viewGroup.removeView(frameLayout);
            }
            WidgetGuildContextMenu.isShowingContextMenu = false;
        }

        public final void show(final FragmentActivity fragmentActivity, PointF pointF, long j) {
            m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            m.checkNotNullParameter(pointF, "absolutePosition");
            if (!WidgetGuildContextMenu.isShowingContextMenu) {
                WidgetGuildContextMenu.isShowingContextMenu = true;
                computeMaxContextMenuHeight(fragmentActivity);
                hide(fragmentActivity, false);
                FrameLayout frameLayout = new FrameLayout(fragmentActivity);
                frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                frameLayout.setTag(WidgetGuildContextMenu.VIEW_CONTAINER_TAG);
                Window window = fragmentActivity.getWindow();
                m.checkNotNullExpressionValue(window, "activity.window");
                View decorView = window.getDecorView();
                m.checkNotNullExpressionValue(decorView, "activity.window.decorView");
                View rootView = decorView.getRootView();
                Objects.requireNonNull(rootView, "null cannot be cast to non-null type android.view.ViewGroup");
                ViewGroup viewGroup = (ViewGroup) rootView;
                viewGroup.addView(frameLayout);
                frameLayout.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu$Companion$show$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetGuildContextMenu.Companion.hide(FragmentActivity.this, true);
                    }
                });
                FrameLayout frameLayout2 = new FrameLayout(fragmentActivity);
                frameLayout2.setId(View.generateViewId());
                frameLayout2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
                frameLayout.addView(frameLayout2);
                frameLayout2.setX(pointF.x);
                frameLayout2.setY(f.coerceAtMost(pointF.y, (viewGroup.getHeight() - computeMaxContextMenuHeight(fragmentActivity)) - WidgetGuildContextMenu.SCREEN_BOTTOM_BUFFER));
                fragmentActivity.getSupportFragmentManager().beginTransaction().add(frameLayout2.getId(), newInstance(j), WidgetGuildContextMenu.FRAGMENT_TAG).commit();
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildContextMenu() {
        super(R.layout.widget_guild_context_menu);
        WidgetGuildContextMenu$viewModel$2 widgetGuildContextMenu$viewModel$2 = new WidgetGuildContextMenu$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildContextMenuViewModel.class), new WidgetGuildContextMenu$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildContextMenu$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(GuildContextMenuViewModel.ViewState viewState) {
        if (viewState instanceof GuildContextMenuViewModel.ViewState.Valid) {
            configureValidUI((GuildContextMenuViewModel.ViewState.Valid) viewState);
        } else if (m.areEqual(viewState, GuildContextMenuViewModel.ViewState.Invalid.INSTANCE)) {
            Companion companion = Companion;
            FragmentActivity requireActivity = requireActivity();
            m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            companion.hide(requireActivity, false);
        }
    }

    private final void configureValidUI(final GuildContextMenuViewModel.ViewState.Valid valid) {
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.guildContextMenuHeader");
        textView.setText(valid.getGuild().getName());
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu$configureValidUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildContextMenuViewModel viewModel;
                WidgetGuildContextMenu.this.doCircularRemove();
                viewModel = WidgetGuildContextMenu.this.getViewModel();
                viewModel.onMarkAsReadClicked();
            }
        });
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu$configureValidUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildContextMenu.this.doCircularRemove();
                WidgetServerNotifications.Companion companion = WidgetServerNotifications.Companion;
                long id2 = valid.getGuild().getId();
                FragmentActivity requireActivity = WidgetGuildContextMenu.this.requireActivity();
                m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                companion.launch(id2, requireActivity);
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu$configureValidUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildContextMenu.this.doCircularRemove();
                WidgetLeaveGuildDialog.Companion companion = WidgetLeaveGuildDialog.Companion;
                FragmentManager parentFragmentManager = WidgetGuildContextMenu.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                companion.show(parentFragmentManager, valid.getGuild().getId());
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu$configureValidUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildContextMenu.this.doCircularRemove();
                WidgetGuildProfileSheet.Companion companion = WidgetGuildProfileSheet.Companion;
                FragmentManager parentFragmentManager = WidgetGuildContextMenu.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                companion.show(parentFragmentManager, valid.isGuildSelected(), valid.getGuild().getId(), (r18 & 8) != 0 ? 0L : 0L, (r18 & 16) != 0 ? false : false);
            }
        });
        TextView textView2 = getBinding().d;
        m.checkNotNullExpressionValue(textView2, "binding.guildContextMenuLeaveGuild");
        int i = 8;
        boolean z2 = false;
        textView2.setVisibility(valid.getShowLeaveGuild() ? 0 : 8);
        TextView textView3 = getBinding().e;
        m.checkNotNullExpressionValue(textView3, "binding.guildContextMenuMarkAsRead");
        if (valid.getShowMarkAsRead()) {
            i = 0;
        }
        textView3.setVisibility(i);
        getBinding().f2389b.setContentPadding(0, 0, 0, 0);
        CardView cardView = getBinding().f2389b;
        m.checkNotNullExpressionValue(cardView, "binding.guildContextMenuCard");
        if (cardView.getVisibility() == 0) {
            z2 = true;
        }
        if (!z2) {
            doCircularReveal();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void doCircularRemove() {
        AnimationState animationState = this.animationState;
        if (animationState == AnimationState.ANIMATING_IN) {
            Animator animator = this.animator;
            if (animator != null) {
                animator.cancel();
            }
            Companion companion = Companion;
            FragmentActivity requireActivity = requireActivity();
            m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            companion.hide(requireActivity, false);
            return;
        }
        AnimationState animationState2 = AnimationState.ANIMATING_OUT;
        if (animationState != animationState2) {
            CardView cardView = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView, "binding.guildContextMenuCard");
            int height = cardView.getHeight() / 2;
            CardView cardView2 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView2, "binding.guildContextMenuCard");
            int width = cardView2.getWidth();
            CardView cardView3 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView3, "binding.guildContextMenuCard");
            float hypot = (float) Math.hypot(width, cardView3.getHeight() / 2);
            CardView cardView4 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView4, "binding.guildContextMenuCard");
            if (cardView4.isAttachedToWindow()) {
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(getBinding().f2389b, 0, height, hypot, 0.0f);
                this.animator = createCircularReveal;
                this.animationState = animationState2;
                m.checkNotNullExpressionValue(createCircularReveal, "animator");
                createCircularReveal.setDuration(200L);
                createCircularReveal.addListener(new AnimatorListenerAdapter() { // from class: com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu$doCircularRemove$1
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator2) {
                        super.onAnimationEnd(animator2);
                        FragmentActivity activity = WidgetGuildContextMenu.this.e();
                        if (activity != null) {
                            WidgetGuildContextMenu.Companion companion2 = WidgetGuildContextMenu.Companion;
                            m.checkNotNullExpressionValue(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                            companion2.hide(activity, false);
                        }
                    }
                });
                createCircularReveal.start();
                return;
            }
            CardView cardView5 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView5, "binding.guildContextMenuCard");
            cardView5.setVisibility(8);
        }
    }

    private final void doCircularReveal() {
        CardView cardView = getBinding().f2389b;
        m.checkNotNullExpressionValue(cardView, "binding.guildContextMenuCard");
        if (cardView.isAttachedToWindow()) {
            CardView cardView2 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView2, "binding.guildContextMenuCard");
            CardView cardView3 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView3, "binding.guildContextMenuCard");
            int width = cardView3.getWidth();
            CardView cardView4 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView4, "binding.guildContextMenuCard");
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(getBinding().f2389b, 0, cardView2.getHeight() / 2, 0.0f, (float) Math.hypot(width, cardView4.getHeight() / 2));
            this.animator = createCircularReveal;
            this.animationState = AnimationState.ANIMATING_IN;
            m.checkNotNullExpressionValue(createCircularReveal, "animator");
            createCircularReveal.setDuration(200L);
            createCircularReveal.addListener(new AnimatorListenerAdapter() { // from class: com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu$doCircularReveal$1
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    super.onAnimationEnd(animator);
                    WidgetGuildContextMenu.this.animationState = null;
                }
            });
            CardView cardView5 = getBinding().f2389b;
            m.checkNotNullExpressionValue(cardView5, "binding.guildContextMenuCard");
            cardView5.setVisibility(0);
            createCircularReveal.start();
            return;
        }
        CardView cardView6 = getBinding().f2389b;
        m.checkNotNullExpressionValue(cardView6, "binding.guildContextMenuCard");
        cardView6.setVisibility(0);
    }

    private final WidgetGuildContextMenuBinding getBinding() {
        return (WidgetGuildContextMenuBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildContextMenuViewModel getViewModel() {
        return (GuildContextMenuViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(GuildContextMenuViewModel.Event event) {
        if (m.areEqual(event, GuildContextMenuViewModel.Event.Dismiss.INSTANCE)) {
            doCircularRemove();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        isShowingContextMenu = false;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildContextMenu.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildContextMenu$onResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetGuildContextMenu.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildContextMenu$onResume$2(this));
    }
}
