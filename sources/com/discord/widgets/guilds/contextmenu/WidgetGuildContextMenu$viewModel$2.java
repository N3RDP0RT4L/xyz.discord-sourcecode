package com.discord.widgets.guilds.contextmenu;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.guilds.contextmenu.GuildContextMenuViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildContextMenu.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildContextMenu$viewModel$2 extends o implements Function0<AppViewModel<GuildContextMenuViewModel.ViewState>> {
    public final /* synthetic */ WidgetGuildContextMenu this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildContextMenu$viewModel$2(WidgetGuildContextMenu widgetGuildContextMenu) {
        super(0);
        this.this$0 = widgetGuildContextMenu;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<GuildContextMenuViewModel.ViewState> invoke() {
        Bundle arguments = this.this$0.getArguments();
        if (arguments != null) {
            return new GuildContextMenuViewModel(arguments.getLong("com.discord.intent.extra.EXTRA_GUILD_ID"), null, null, 6, null);
        }
        throw new IllegalStateException("missing guild id");
    }
}
