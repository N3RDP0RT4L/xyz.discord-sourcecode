package com.discord.widgets.guilds;

import android.os.Bundle;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0004\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "kotlin.jvm.PlatformType", "invoke", "()Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildSelector$requestKey$2 extends o implements Function0<String> {
    public final /* synthetic */ WidgetGuildSelector this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildSelector$requestKey$2(WidgetGuildSelector widgetGuildSelector) {
        super(0);
        this.this$0 = widgetGuildSelector;
    }

    @Override // kotlin.jvm.functions.Function0
    public final String invoke() {
        Bundle argumentsOrDefault;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        return argumentsOrDefault.getString("INTENT_EXTRA_REQUEST_CODE", "");
    }
}
