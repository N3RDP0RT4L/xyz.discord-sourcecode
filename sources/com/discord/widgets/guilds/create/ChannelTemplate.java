package com.discord.widgets.guilds.create;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: StockGuildTemplate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u000f\u0010B!\b\u0002\u0012\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\r\u0010\u000eR!\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u0019\u0010\t\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\f\u0082\u0001\u0002\u0011\u0012¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/guilds/create/ChannelTemplate;", "", "", "Lcom/discord/primitives/ChannelId;", ModelAuditLogEntry.CHANGE_KEY_ID, "Ljava/lang/Long;", "getId", "()Ljava/lang/Long;", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "getName", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/String;)V", "NormalChannel", "SystemChannel", "Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;", "Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class ChannelTemplate {

    /* renamed from: id  reason: collision with root package name */
    private final Long f2827id;
    private final String name;

    /* compiled from: StockGuildTemplate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;", "Lcom/discord/widgets/guilds/create/ChannelTemplate;", "", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NormalChannel extends ChannelTemplate {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public NormalChannel(String str) {
            super(null, str, null);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        }
    }

    /* compiled from: StockGuildTemplate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;", "Lcom/discord/widgets/guilds/create/ChannelTemplate;", "", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SystemChannel extends ChannelTemplate {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public SystemChannel(String str) {
            super(11L, str, null);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        }
    }

    private ChannelTemplate(Long l, String str) {
        this.f2827id = l;
        this.name = str;
    }

    public final Long getId() {
        return this.f2827id;
    }

    public final String getName() {
        return this.name;
    }

    public /* synthetic */ ChannelTemplate(Long l, String str, DefaultConstructorMarker defaultConstructorMarker) {
        this(l, str);
    }
}
