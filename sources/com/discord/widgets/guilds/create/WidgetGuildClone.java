package com.discord.widgets.guilds.create;

import andhook.lib.HookHelper;
import andhook.lib.xposed.callbacks.XCallback;
import android.content.Context;
import android.content.Intent;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.Guild;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetGuildCloneBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildTemplate;
import com.discord.stores.StoreGuildTemplates;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guilds.create.GuildCreateCloneViews;
import com.discord.widgets.guilds.create.WidgetGuildCreate;
import com.discord.widgets.guilds.create.WidgetGuildCreateViewModel;
import com.discord.widgets.roles.RolesListView;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetGuildClone.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0007¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\r\u001a\u00020\n8T@\u0014X\u0094\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001d\u0010\u0013\u001a\u00020\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guilds/create/WidgetGuildClone;", "Lcom/discord/widgets/guilds/create/WidgetGuildCreate;", "Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;", "createViewModelFactory", "()Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;", "Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;)V", "Lcom/discord/widgets/guilds/create/GuildCreateCloneViews;", "getViews", "()Lcom/discord/widgets/guilds/create/GuildCreateCloneViews;", "views", "Lcom/discord/databinding/WidgetGuildCloneBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildCloneBinding;", "binding", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildClone extends WidgetGuildCreate {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildClone.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildCloneBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_GUILD_TEMPLATE_CODE = "guild_template_code";
    private static final int VIEW_INDEX_INVALID_TEMPLATE = 1;
    private static final int VIEW_INDEX_LOADING_TEMPLATE = 0;
    private static final int VIEW_INDEX_READY = 2;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildClone$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetGuildClone.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012J)\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0006\u001a\u00020\u0004¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000e¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;", "", "Landroid/content/Context;", "context", "", "guildTemplateCode", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "", "show", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V", "INTENT_GUILD_TEMPLATE_CODE", "Ljava/lang/String;", "", "VIEW_INDEX_INVALID_TEMPLATE", "I", "VIEW_INDEX_LOADING_TEMPLATE", "VIEW_INDEX_READY", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, Context context, String str, String str2, int i, Object obj) {
            if ((i & 2) != 0) {
                str = null;
            }
            companion.show(context, str, str2);
        }

        public final void show(Context context, String str, String str2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            Intent putExtra = new Intent().putExtra(WidgetGuildCreate.EXTRA_OPTIONS, new WidgetGuildCreate.Options(str2, null, false, null, false, 30, null));
            m.checkNotNullExpressionValue(putExtra, "Intent().putExtra(EXTRA_OPTIONS, options)");
            if (str != null) {
                putExtra.putExtra(WidgetGuildClone.INTENT_GUILD_TEMPLATE_CODE, str);
            }
            j.d(context, WidgetGuildClone.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildClone() {
        super(R.layout.widget_guild_clone);
    }

    private final WidgetGuildCloneBinding getBinding() {
        return (WidgetGuildCloneBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.widgets.guilds.create.WidgetGuildCreate
    public void configureUI(WidgetGuildCreateViewModel.ViewState viewState) {
        List<Channel> list;
        m.checkNotNullParameter(viewState, "viewState");
        super.configureUI(viewState);
        StoreStream.Companion.getGuildTemplates().clearDynamicLinkGuildTemplateCode();
        int i = 0;
        if (m.areEqual(viewState, WidgetGuildCreateViewModel.ViewState.Uninitialized.INSTANCE)) {
            AppViewFlipper appViewFlipper = getBinding().c;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.guildCreateFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (viewState instanceof WidgetGuildCreateViewModel.ViewState.Initialized) {
            WidgetGuildCreateViewModel.ViewState.Initialized initialized = (WidgetGuildCreateViewModel.ViewState.Initialized) viewState;
            StoreGuildTemplates.GuildTemplateState guildTemplate = initialized.getGuildTemplate();
            if (m.areEqual(guildTemplate, StoreGuildTemplates.GuildTemplateState.None.INSTANCE) || m.areEqual(guildTemplate, StoreGuildTemplates.GuildTemplateState.Invalid.INSTANCE) || m.areEqual(guildTemplate, StoreGuildTemplates.GuildTemplateState.LoadFailed.INSTANCE)) {
                AppViewFlipper appViewFlipper2 = getBinding().c;
                m.checkNotNullExpressionValue(appViewFlipper2, "binding.guildCreateFlipper");
                appViewFlipper2.setDisplayedChild(1);
            } else if (m.areEqual(guildTemplate, StoreGuildTemplates.GuildTemplateState.Loading.INSTANCE)) {
                AppViewFlipper appViewFlipper3 = getBinding().c;
                m.checkNotNullExpressionValue(appViewFlipper3, "binding.guildCreateFlipper");
                appViewFlipper3.setDisplayedChild(0);
            } else if (guildTemplate instanceof StoreGuildTemplates.GuildTemplateState.Resolved) {
                ModelGuildTemplate guildTemplate2 = ((StoreGuildTemplates.GuildTemplateState.Resolved) initialized.getGuildTemplate()).getGuildTemplate();
                Guild serializedSourceGuild = guildTemplate2.getSerializedSourceGuild();
                if (serializedSourceGuild == null) {
                    AppViewFlipper appViewFlipper4 = getBinding().c;
                    m.checkNotNullExpressionValue(appViewFlipper4, "binding.guildCreateFlipper");
                    appViewFlipper4.setDisplayedChild(1);
                    return;
                }
                AppViewFlipper appViewFlipper5 = getBinding().c;
                m.checkNotNullExpressionValue(appViewFlipper5, "binding.guildCreateFlipper");
                appViewFlipper5.setDisplayedChild(2);
                TextView textView = getBinding().g;
                m.checkNotNullExpressionValue(textView, "binding.guildTemplateName");
                int themedColor = ColorCompat.getThemedColor(textView, (int) R.attr.colorHeaderSecondary);
                TextView textView2 = getBinding().g;
                m.checkNotNullExpressionValue(textView2, "binding.guildTemplateName");
                ColorCompatKt.setDrawableColor(textView2, themedColor);
                TextView textView3 = getBinding().g;
                m.checkNotNullExpressionValue(textView3, "binding.guildTemplateName");
                textView3.setText(guildTemplate2.getName());
                GuildTemplateChannelsView guildTemplateChannelsView = getBinding().h;
                List<Channel> g = serializedSourceGuild.g();
                if (g == null || (list = u.sortedWith(g, new Comparator() { // from class: com.discord.widgets.guilds.create.WidgetGuildClone$configureUI$$inlined$sortedBy$1
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        Channel channel = (Channel) t;
                        Channel channel2 = (Channel) t2;
                        return d0.u.a.compareValues(Long.valueOf(channel.r() == 0 ? channel.h() * ((long) XCallback.PRIORITY_HIGHEST) : (channel.r() * ((long) XCallback.PRIORITY_HIGHEST)) + channel.h()), Long.valueOf(channel2.r() == 0 ? channel2.h() * ((long) XCallback.PRIORITY_HIGHEST) : (channel2.r() * ((long) XCallback.PRIORITY_HIGHEST)) + channel2.h()));
                    }
                })) == null) {
                    list = n.emptyList();
                }
                guildTemplateChannelsView.updateView(list);
                List<GuildRole> G = serializedSourceGuild.G();
                if (G == null) {
                    G = n.emptyList();
                }
                List sortedWith = u.sortedWith(G, new Comparator() { // from class: com.discord.widgets.guilds.create.WidgetGuildClone$configureUI$$inlined$sortedBy$2
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return d0.u.a.compareValues(Long.valueOf(-((GuildRole) t).getId()), Long.valueOf(-((GuildRole) t2).getId()));
                    }
                });
                ArrayList arrayList = new ArrayList();
                for (Object obj : sortedWith) {
                    if (!m.areEqual(((GuildRole) obj).g(), "@everyone")) {
                        arrayList.add(obj);
                    }
                }
                LinearLayout linearLayout = getBinding().j;
                m.checkNotNullExpressionValue(linearLayout, "binding.guildTemplatePreviewRolesLayout");
                if (!(!arrayList.isEmpty())) {
                    i = 8;
                }
                linearLayout.setVisibility(i);
                RolesListView rolesListView = getBinding().i;
                RolesListView rolesListView2 = getBinding().i;
                m.checkNotNullExpressionValue(rolesListView2, "binding.guildTemplatePreviewRoles");
                rolesListView.updateView(arrayList, ColorCompat.getThemedColor(rolesListView2.getContext(), (int) R.attr.primary_300), serializedSourceGuild.r());
            }
        }
    }

    @Override // com.discord.widgets.guilds.create.WidgetGuildCreate
    public WidgetGuildCreateViewModel createViewModelFactory() {
        return new WidgetGuildCreateViewModel(R.string.create_server_default_server_name_format, null, getMostRecentIntent().getStringExtra(INTENT_GUILD_TEMPLATE_CODE), false, getArgs().getAnalyticsLocation(), null, false, null, null, null, 962, null);
    }

    @Override // com.discord.widgets.guilds.create.WidgetGuildCreate
    public GuildCreateCloneViews getViews() {
        GuildCreateCloneViews.Companion companion = GuildCreateCloneViews.Companion;
        WidgetGuildCloneBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        return companion.from(binding);
    }
}
