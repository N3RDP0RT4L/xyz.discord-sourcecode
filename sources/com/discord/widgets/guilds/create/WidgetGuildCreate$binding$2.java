package com.discord.widgets.guilds.create;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import b.a.i.p0;
import com.discord.databinding.WidgetGuildCreateBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.LoadingButton;
import com.discord.views.ScreenTitleView;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildCreate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildCreateBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildCreateBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildCreate$binding$2 extends k implements Function1<View, WidgetGuildCreateBinding> {
    public static final WidgetGuildCreate$binding$2 INSTANCE = new WidgetGuildCreate$binding$2();

    public WidgetGuildCreate$binding$2() {
        super(1, WidgetGuildCreateBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildCreateBinding;", 0);
    }

    public final WidgetGuildCreateBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_create_button;
        LoadingButton loadingButton = (LoadingButton) view.findViewById(R.id.guild_create_button);
        if (loadingButton != null) {
            i = R.id.guild_create_guidelines;
            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.guild_create_guidelines);
            if (linkifiedTextView != null) {
                i = R.id.guild_create_icon_uploader;
                View findViewById = view.findViewById(R.id.guild_create_icon_uploader);
                if (findViewById != null) {
                    p0 a = p0.a(findViewById);
                    i = R.id.guild_create_name;
                    TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.guild_create_name);
                    if (textInputLayout != null) {
                        i = R.id.guild_create_screen_title;
                        ScreenTitleView screenTitleView = (ScreenTitleView) view.findViewById(R.id.guild_create_screen_title);
                        if (screenTitleView != null) {
                            return new WidgetGuildCreateBinding((CoordinatorLayout) view, loadingButton, linkifiedTextView, a, textInputLayout, screenTitleView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
