package com.discord.widgets.guilds.create;

import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import com.discord.widgets.guilds.create.WidgetGuildCreate;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetCreationIntent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\t\b\u0087\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\"\u0010#J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0011J \u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010 \u001a\u0004\b!\u0010\u0004¨\u0006$"}, d2 = {"Lcom/discord/widgets/guilds/create/CreationIntentArgs;", "Landroid/os/Parcelable;", "Lcom/discord/widgets/guilds/create/CreateGuildTrigger;", "component1", "()Lcom/discord/widgets/guilds/create/CreateGuildTrigger;", "Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Options;", "component2", "()Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Options;", "trigger", "createGuildOptions", "copy", "(Lcom/discord/widgets/guilds/create/CreateGuildTrigger;Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Options;)Lcom/discord/widgets/guilds/create/CreationIntentArgs;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Options;", "getCreateGuildOptions", "Lcom/discord/widgets/guilds/create/CreateGuildTrigger;", "getTrigger", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/create/CreateGuildTrigger;Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Options;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CreationIntentArgs implements Parcelable {
    public static final Parcelable.Creator<CreationIntentArgs> CREATOR = new Creator();
    private final WidgetGuildCreate.Options createGuildOptions;
    private final CreateGuildTrigger trigger;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<CreationIntentArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final CreationIntentArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            return new CreationIntentArgs((CreateGuildTrigger) Enum.valueOf(CreateGuildTrigger.class, parcel.readString()), WidgetGuildCreate.Options.CREATOR.createFromParcel(parcel));
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final CreationIntentArgs[] newArray(int i) {
            return new CreationIntentArgs[i];
        }
    }

    public CreationIntentArgs(CreateGuildTrigger createGuildTrigger, WidgetGuildCreate.Options options) {
        m.checkNotNullParameter(createGuildTrigger, "trigger");
        m.checkNotNullParameter(options, "createGuildOptions");
        this.trigger = createGuildTrigger;
        this.createGuildOptions = options;
    }

    public static /* synthetic */ CreationIntentArgs copy$default(CreationIntentArgs creationIntentArgs, CreateGuildTrigger createGuildTrigger, WidgetGuildCreate.Options options, int i, Object obj) {
        if ((i & 1) != 0) {
            createGuildTrigger = creationIntentArgs.trigger;
        }
        if ((i & 2) != 0) {
            options = creationIntentArgs.createGuildOptions;
        }
        return creationIntentArgs.copy(createGuildTrigger, options);
    }

    public final CreateGuildTrigger component1() {
        return this.trigger;
    }

    public final WidgetGuildCreate.Options component2() {
        return this.createGuildOptions;
    }

    public final CreationIntentArgs copy(CreateGuildTrigger createGuildTrigger, WidgetGuildCreate.Options options) {
        m.checkNotNullParameter(createGuildTrigger, "trigger");
        m.checkNotNullParameter(options, "createGuildOptions");
        return new CreationIntentArgs(createGuildTrigger, options);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CreationIntentArgs)) {
            return false;
        }
        CreationIntentArgs creationIntentArgs = (CreationIntentArgs) obj;
        return m.areEqual(this.trigger, creationIntentArgs.trigger) && m.areEqual(this.createGuildOptions, creationIntentArgs.createGuildOptions);
    }

    public final WidgetGuildCreate.Options getCreateGuildOptions() {
        return this.createGuildOptions;
    }

    public final CreateGuildTrigger getTrigger() {
        return this.trigger;
    }

    public int hashCode() {
        CreateGuildTrigger createGuildTrigger = this.trigger;
        int i = 0;
        int hashCode = (createGuildTrigger != null ? createGuildTrigger.hashCode() : 0) * 31;
        WidgetGuildCreate.Options options = this.createGuildOptions;
        if (options != null) {
            i = options.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("CreationIntentArgs(trigger=");
        R.append(this.trigger);
        R.append(", createGuildOptions=");
        R.append(this.createGuildOptions);
        R.append(")");
        return R.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeString(this.trigger.name());
        this.createGuildOptions.writeToParcel(parcel, 0);
    }
}
