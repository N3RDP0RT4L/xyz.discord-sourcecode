package com.discord.widgets.guilds.create;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.databinding.GuildTemplateChannelsItemViewBinding;
import com.discord.databinding.GuildTemplateChannelsViewBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildTemplateChannelsView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0003\u0014\u0015\u0016B\u0017\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView;", "Landroid/widget/LinearLayout;", "", "Lcom/discord/api/channel/Channel;", "channels", "", "updateView", "(Ljava/util/List;)V", "Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelsAdapter;", "channelsAdapter", "Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelsAdapter;", "Lcom/discord/databinding/GuildTemplateChannelsViewBinding;", "binding", "Lcom/discord/databinding/GuildTemplateChannelsViewBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "ChannelDataPayload", "ChannelViewHolder", "ChannelsAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildTemplateChannelsView extends LinearLayout {
    private final GuildTemplateChannelsViewBinding binding;
    private final ChannelsAdapter channelsAdapter;

    /* compiled from: GuildTemplateChannelsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0082\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\rR\u001c\u0010\u0016\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelDataPayload;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "channel", "copy", "(Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelDataPayload;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelDataPayload implements MGRecyclerDataPayload {
        private final Channel channel;
        private final String key = String.valueOf(hashCode());
        private final int type;

        public ChannelDataPayload(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            this.channel = channel;
        }

        public static /* synthetic */ ChannelDataPayload copy$default(ChannelDataPayload channelDataPayload, Channel channel, int i, Object obj) {
            if ((i & 1) != 0) {
                channel = channelDataPayload.channel;
            }
            return channelDataPayload.copy(channel);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final ChannelDataPayload copy(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            return new ChannelDataPayload(channel);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ChannelDataPayload) && m.areEqual(this.channel, ((ChannelDataPayload) obj).channel);
            }
            return true;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            Channel channel = this.channel;
            if (channel != null) {
                return channel.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelDataPayload(channel=");
            R.append(this.channel);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildTemplateChannelsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelsAdapter;", "Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelDataPayload;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelDataPayload;)V", "Lcom/discord/databinding/GuildTemplateChannelsItemViewBinding;", "binding", "Lcom/discord/databinding/GuildTemplateChannelsItemViewBinding;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelsAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelViewHolder extends MGRecyclerViewHolder<ChannelsAdapter, ChannelDataPayload> {
        private final GuildTemplateChannelsItemViewBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ChannelViewHolder(@LayoutRes int i, ChannelsAdapter channelsAdapter) {
            super(i, channelsAdapter);
            m.checkNotNullParameter(channelsAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.guild_template_channels_item_image;
            ImageView imageView = (ImageView) view.findViewById(R.id.guild_template_channels_item_image);
            if (imageView != null) {
                i2 = R.id.guild_template_channels_item_name;
                TextView textView = (TextView) view.findViewById(R.id.guild_template_channels_item_name);
                if (textView != null) {
                    GuildTemplateChannelsItemViewBinding guildTemplateChannelsItemViewBinding = new GuildTemplateChannelsItemViewBinding((LinearLayout) view, imageView, textView);
                    m.checkNotNullExpressionValue(guildTemplateChannelsItemViewBinding, "GuildTemplateChannelsIte…iewBinding.bind(itemView)");
                    this.binding = guildTemplateChannelsItemViewBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public void onConfigure(int i, ChannelDataPayload channelDataPayload) {
            m.checkNotNullParameter(channelDataPayload, "data");
            super.onConfigure(i, (int) channelDataPayload);
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.guildTemplateChannelsItemName");
            textView.setText(ChannelUtils.c(channelDataPayload.getChannel()));
            if (ChannelUtils.k(channelDataPayload.getChannel())) {
                this.binding.f2107b.setImageResource(R.drawable.ic_chevron_down_grey_12dp);
            } else if (ChannelUtils.E(channelDataPayload.getChannel())) {
                this.binding.f2107b.setImageResource(R.drawable.ic_channel_voice_16dp);
            } else {
                this.binding.f2107b.setImageResource(R.drawable.ic_channel_text_16dp);
            }
            if (channelDataPayload.getChannel().r() > 0) {
                ImageView imageView = this.binding.f2107b;
                m.checkNotNullExpressionValue(imageView, "binding.guildTemplateChannelsItemImage");
                ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                Resources resources = ((ChannelsAdapter) this.adapter).getContext().getResources();
                m.checkNotNullExpressionValue(resources, "adapter.context.resources");
                ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin = (int) TypedValue.applyDimension(1, 24.0f, resources.getDisplayMetrics());
            }
        }
    }

    /* compiled from: GuildTemplateChannelsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rJ+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\t¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelsAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView$ChannelDataPayload;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelsAdapter extends MGRecyclerAdapterSimple<ChannelDataPayload> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ChannelsAdapter(RecyclerView recyclerView) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recyclerView");
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public MGRecyclerViewHolder<ChannelsAdapter, ChannelDataPayload> onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            return new ChannelViewHolder(R.layout.guild_template_channels_item_view, this);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildTemplateChannelsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        View inflate = LayoutInflater.from(context).inflate(R.layout.guild_template_channels_view, (ViewGroup) this, false);
        addView(inflate);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.guild_template_channels_recycler_view);
        if (recyclerView != null) {
            GuildTemplateChannelsViewBinding guildTemplateChannelsViewBinding = new GuildTemplateChannelsViewBinding((LinearLayout) inflate, recyclerView);
            m.checkNotNullExpressionValue(guildTemplateChannelsViewBinding, "GuildTemplateChannelsVie…rom(context), this, true)");
            this.binding = guildTemplateChannelsViewBinding;
            LinearLayout.inflate(context, R.layout.guild_template_channels_view, this);
            MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
            m.checkNotNullExpressionValue(recyclerView, "binding.guildTemplateChannelsRecyclerView");
            this.channelsAdapter = (ChannelsAdapter) companion.configure(new ChannelsAdapter(recyclerView));
            recyclerView.setHasFixedSize(false);
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(R.id.guild_template_channels_recycler_view)));
    }

    public final void updateView(List<Channel> list) {
        m.checkNotNullParameter(list, "channels");
        ChannelsAdapter channelsAdapter = this.channelsAdapter;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (Channel channel : list) {
            arrayList.add(new ChannelDataPayload(channel));
        }
        channelsAdapter.setData(arrayList);
    }
}
