package com.discord.widgets.guilds.create;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import androidx.exifinterface.media.ExifInterface;
import b.a.k.b;
import com.discord.restapi.RestAPIParams;
import com.discord.widgets.guilds.create.ChannelTemplate;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import xyz.discord.R;
/* compiled from: StockGuildTemplate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0012\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u0011\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001b\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\n\u0010\u000bj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018j\u0002\b\u0019j\u0002\b\u001a¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guilds/create/StockGuildTemplate;", "", "", "Lcom/discord/primitives/ChannelId;", "getSystemChannelId", "()J", "Landroid/content/res/Resources;", "resources", "", "Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;", "getChannels", "(Landroid/content/res/Resources;)Ljava/util/List;", HookHelper.constructorName, "(Ljava/lang/String;I)V", "FRIEND_GROUP", "STUDY_GROUP", "GAMING_GROUP", "CONTENT_CREATOR", "CLUB", "LOCAL_COMMUNITY", "HUB_SCHOOL_CLUB", "HUB_STUDY_GROUP", "CLASS", "SOCIAL", "MAJOR", "DORM", "CREATE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public enum StockGuildTemplate {
    FRIEND_GROUP,
    STUDY_GROUP,
    GAMING_GROUP,
    CONTENT_CREATOR,
    CLUB,
    LOCAL_COMMUNITY,
    HUB_SCHOOL_CLUB,
    HUB_STUDY_GROUP,
    CLASS,
    SOCIAL,
    MAJOR,
    DORM,
    CREATE;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StockGuildTemplate.values();
            int[] iArr = new int[13];
            $EnumSwitchMapping$0 = iArr;
            iArr[StockGuildTemplate.GAMING_GROUP.ordinal()] = 1;
            iArr[StockGuildTemplate.FRIEND_GROUP.ordinal()] = 2;
            iArr[StockGuildTemplate.STUDY_GROUP.ordinal()] = 3;
            iArr[StockGuildTemplate.CLUB.ordinal()] = 4;
            iArr[StockGuildTemplate.CONTENT_CREATOR.ordinal()] = 5;
            iArr[StockGuildTemplate.LOCAL_COMMUNITY.ordinal()] = 6;
            iArr[StockGuildTemplate.HUB_SCHOOL_CLUB.ordinal()] = 7;
            iArr[StockGuildTemplate.HUB_STUDY_GROUP.ordinal()] = 8;
            iArr[StockGuildTemplate.CLASS.ordinal()] = 9;
            iArr[StockGuildTemplate.SOCIAL.ordinal()] = 10;
            iArr[StockGuildTemplate.MAJOR.ordinal()] = 11;
            iArr[StockGuildTemplate.DORM.ordinal()] = 12;
            iArr[StockGuildTemplate.CREATE.ordinal()] = 13;
        }
    }

    public final List<RestAPIParams.CreateGuildChannel> getChannels(Resources resources) {
        List createCategorySection;
        List createCategorySection2;
        List createCategorySection3;
        List createCategorySection4;
        List createCategorySection5;
        List createCategorySection6;
        CharSequence c;
        CharSequence c2;
        List createCategorySection7;
        List createCategorySection8;
        List createCategorySection9;
        List createCategorySection10;
        List createCategorySection11;
        List createCategorySection12;
        List createCategorySection13;
        List createCategorySection14;
        List createCategorySection15;
        List createCategorySection16;
        List createCategorySection17;
        List createCategorySection18;
        List createCategorySection19;
        List createCategorySection20;
        List createCategorySection21;
        CharSequence c3;
        CharSequence c4;
        List createCategorySection22;
        List createCategorySection23;
        List createCategorySection24;
        CharSequence c5;
        CharSequence c6;
        List createCategorySection25;
        List createCategorySection26;
        List createCategorySection27;
        List createCategorySection28;
        List createCategorySection29;
        CharSequence c7;
        CharSequence c8;
        List createCategorySection30;
        List createCategorySection31;
        List createCategorySection32;
        List createCategorySection33;
        m.checkNotNullParameter(resources, "resources");
        switch (ordinal()) {
            case 0:
                String string = resources.getString(R.string.guild_template_name_category_text);
                m.checkNotNullExpressionValue(string, "resources.getString(R.st…plate_name_category_text)");
                String string2 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string2, "resources.getString(R.st…ld_template_name_general)");
                String string3 = resources.getString(R.string.guild_template_name_game);
                m.checkNotNullExpressionValue(string3, "resources.getString(R.st…guild_template_name_game)");
                String string4 = resources.getString(R.string.guild_template_name_music);
                m.checkNotNullExpressionValue(string4, "resources.getString(R.st…uild_template_name_music)");
                createCategorySection3 = StockGuildTemplateKt.createCategorySection(string, 100L, 0, new ChannelTemplate.SystemChannel(string2), new ChannelTemplate.NormalChannel(string3), new ChannelTemplate.NormalChannel(string4));
                String string5 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string5, "resources.getString(R.st…late_name_category_voice)");
                String string6 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string6, "resources.getString(R.st…mplate_name_voice_lounge)");
                String string7 = resources.getString(R.string.guild_template_name_voice_stream_room);
                m.checkNotNullExpressionValue(string7, "resources.getString(R.st…e_name_voice_stream_room)");
                createCategorySection4 = StockGuildTemplateKt.createCategorySection(string5, 200L, 2, new ChannelTemplate.NormalChannel(string6), new ChannelTemplate.NormalChannel(string7));
                return u.plus((Collection) createCategorySection3, (Iterable) createCategorySection4);
            case 1:
                String string8 = resources.getString(R.string.guild_template_name_category_information);
                m.checkNotNullExpressionValue(string8, "resources.getString(R.st…ame_category_information)");
                String string9 = resources.getString(R.string.guild_template_name_welcome_and_rules);
                m.checkNotNullExpressionValue(string9, "resources.getString(R.st…e_name_welcome_and_rules)");
                String string10 = resources.getString(R.string.guild_template_name_notes_resources);
                m.checkNotNullExpressionValue(string10, "resources.getString(R.st…ate_name_notes_resources)");
                createCategorySection5 = StockGuildTemplateKt.createCategorySection(string8, 100L, 0, new ChannelTemplate.NormalChannel(string9), new ChannelTemplate.NormalChannel(string10));
                String string11 = resources.getString(R.string.guild_template_name_category_text);
                m.checkNotNullExpressionValue(string11, "resources.getString(R.st…plate_name_category_text)");
                String string12 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string12, "resources.getString(R.st…ld_template_name_general)");
                String string13 = resources.getString(R.string.guild_template_name_homework_help);
                m.checkNotNullExpressionValue(string13, "resources.getString(R.st…plate_name_homework_help)");
                String string14 = resources.getString(R.string.guild_template_name_session_planning);
                m.checkNotNullExpressionValue(string14, "resources.getString(R.st…te_name_session_planning)");
                String string15 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string15, "resources.getString(R.st…_template_name_off_topic)");
                createCategorySection6 = StockGuildTemplateKt.createCategorySection(string11, 200L, 0, new ChannelTemplate.SystemChannel(string12), new ChannelTemplate.NormalChannel(string13), new ChannelTemplate.NormalChannel(string14), new ChannelTemplate.NormalChannel(string15));
                List plus = u.plus((Collection) createCategorySection5, (Iterable) createCategorySection6);
                String string16 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string16, "resources.getString(R.st…late_name_category_voice)");
                String string17 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string17, "resources.getString(R.st…mplate_name_voice_lounge)");
                c = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{"1"}, (r4 & 4) != 0 ? b.d.j : null);
                c2 = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{ExifInterface.GPS_MEASUREMENT_2D}, (r4 & 4) != 0 ? b.d.j : null);
                createCategorySection7 = StockGuildTemplateKt.createCategorySection(string16, 300L, 2, new ChannelTemplate.NormalChannel(string17), new ChannelTemplate.NormalChannel(c.toString()), new ChannelTemplate.NormalChannel(c2.toString()));
                return u.plus((Collection) plus, (Iterable) createCategorySection7);
            case 2:
                String string18 = resources.getString(R.string.guild_template_name_category_text);
                m.checkNotNullExpressionValue(string18, "resources.getString(R.st…plate_name_category_text)");
                String string19 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string19, "resources.getString(R.st…ld_template_name_general)");
                String string20 = resources.getString(R.string.guild_template_name_clips_and_highlights);
                m.checkNotNullExpressionValue(string20, "resources.getString(R.st…ame_clips_and_highlights)");
                createCategorySection = StockGuildTemplateKt.createCategorySection(string18, 100L, 0, new ChannelTemplate.SystemChannel(string19), new ChannelTemplate.NormalChannel(string20));
                String string21 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string21, "resources.getString(R.st…late_name_category_voice)");
                String string22 = resources.getString(R.string.guild_template_name_voice_lobby);
                m.checkNotNullExpressionValue(string22, "resources.getString(R.st…emplate_name_voice_lobby)");
                String string23 = resources.getString(R.string.guild_template_name_voice_gaming);
                m.checkNotNullExpressionValue(string23, "resources.getString(R.st…mplate_name_voice_gaming)");
                createCategorySection2 = StockGuildTemplateKt.createCategorySection(string21, 200L, 2, new ChannelTemplate.NormalChannel(string22), new ChannelTemplate.NormalChannel(string23));
                return u.plus((Collection) createCategorySection, (Iterable) createCategorySection2);
            case 3:
                String string24 = resources.getString(R.string.guild_template_name_category_information);
                m.checkNotNullExpressionValue(string24, "resources.getString(R.st…ame_category_information)");
                String string25 = resources.getString(R.string.guild_template_name_welcome_and_rules);
                m.checkNotNullExpressionValue(string25, "resources.getString(R.st…e_name_welcome_and_rules)");
                String string26 = resources.getString(R.string.guild_template_name_announcements);
                m.checkNotNullExpressionValue(string26, "resources.getString(R.st…plate_name_announcements)");
                createCategorySection11 = StockGuildTemplateKt.createCategorySection(string24, 100L, 0, new ChannelTemplate.NormalChannel(string25), new ChannelTemplate.NormalChannel(string26));
                String string27 = resources.getString(R.string.guild_template_name_category_text);
                m.checkNotNullExpressionValue(string27, "resources.getString(R.st…plate_name_category_text)");
                String string28 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string28, "resources.getString(R.st…ld_template_name_general)");
                String string29 = resources.getString(R.string.guild_template_name_events);
                m.checkNotNullExpressionValue(string29, "resources.getString(R.st…ild_template_name_events)");
                String string30 = resources.getString(R.string.guild_template_name_ideas_and_feedback);
                m.checkNotNullExpressionValue(string30, "resources.getString(R.st…_name_ideas_and_feedback)");
                createCategorySection12 = StockGuildTemplateKt.createCategorySection(string27, 200L, 0, new ChannelTemplate.SystemChannel(string28), new ChannelTemplate.NormalChannel(string29), new ChannelTemplate.NormalChannel(string30));
                List plus2 = u.plus((Collection) createCategorySection11, (Iterable) createCategorySection12);
                String string31 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string31, "resources.getString(R.st…late_name_category_voice)");
                String string32 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string32, "resources.getString(R.st…mplate_name_voice_lounge)");
                String string33 = resources.getString(R.string.guild_template_name_voice_community_hangout);
                m.checkNotNullExpressionValue(string33, "resources.getString(R.st…_voice_community_hangout)");
                String string34 = resources.getString(R.string.guild_template_name_voice_stream_room);
                m.checkNotNullExpressionValue(string34, "resources.getString(R.st…e_name_voice_stream_room)");
                createCategorySection13 = StockGuildTemplateKt.createCategorySection(string31, 300L, 2, new ChannelTemplate.NormalChannel(string32), new ChannelTemplate.NormalChannel(string33), new ChannelTemplate.NormalChannel(string34));
                return u.plus((Collection) plus2, (Iterable) createCategorySection13);
            case 4:
                String string35 = resources.getString(R.string.guild_template_name_category_information);
                m.checkNotNullExpressionValue(string35, "resources.getString(R.st…ame_category_information)");
                String string36 = resources.getString(R.string.guild_template_name_welcome_and_rules);
                m.checkNotNullExpressionValue(string36, "resources.getString(R.st…e_name_welcome_and_rules)");
                String string37 = resources.getString(R.string.guild_template_name_announcements);
                m.checkNotNullExpressionValue(string37, "resources.getString(R.st…plate_name_announcements)");
                createCategorySection8 = StockGuildTemplateKt.createCategorySection(string35, 100L, 0, new ChannelTemplate.NormalChannel(string36), new ChannelTemplate.NormalChannel(string37));
                String string38 = resources.getString(R.string.guild_template_name_category_text);
                m.checkNotNullExpressionValue(string38, "resources.getString(R.st…plate_name_category_text)");
                String string39 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string39, "resources.getString(R.st…ld_template_name_general)");
                String string40 = resources.getString(R.string.guild_template_name_meeting_plans);
                m.checkNotNullExpressionValue(string40, "resources.getString(R.st…plate_name_meeting_plans)");
                createCategorySection9 = StockGuildTemplateKt.createCategorySection(string38, 200L, 0, new ChannelTemplate.SystemChannel(string39), new ChannelTemplate.NormalChannel(string40));
                List plus3 = u.plus((Collection) createCategorySection8, (Iterable) createCategorySection9);
                String string41 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string41, "resources.getString(R.st…late_name_category_voice)");
                String string42 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string42, "resources.getString(R.st…mplate_name_voice_lounge)");
                String string43 = resources.getString(R.string.guild_template_name_voice_meeting_room);
                m.checkNotNullExpressionValue(string43, "resources.getString(R.st…_name_voice_meeting_room)");
                createCategorySection10 = StockGuildTemplateKt.createCategorySection(string41, 300L, 2, new ChannelTemplate.NormalChannel(string42), new ChannelTemplate.NormalChannel(string43));
                return u.plus((Collection) plus3, (Iterable) createCategorySection10);
            case 5:
                String string44 = resources.getString(R.string.guild_template_name_category_information);
                m.checkNotNullExpressionValue(string44, "resources.getString(R.st…ame_category_information)");
                String string45 = resources.getString(R.string.guild_template_name_welcome_and_rules);
                m.checkNotNullExpressionValue(string45, "resources.getString(R.st…e_name_welcome_and_rules)");
                String string46 = resources.getString(R.string.guild_template_name_announcements);
                m.checkNotNullExpressionValue(string46, "resources.getString(R.st…plate_name_announcements)");
                String string47 = resources.getString(R.string.guild_template_name_resources);
                m.checkNotNullExpressionValue(string47, "resources.getString(R.st…_template_name_resources)");
                createCategorySection14 = StockGuildTemplateKt.createCategorySection(string44, 100L, 0, new ChannelTemplate.NormalChannel(string45), new ChannelTemplate.NormalChannel(string46), new ChannelTemplate.NormalChannel(string47));
                String string48 = resources.getString(R.string.guild_template_name_category_text);
                m.checkNotNullExpressionValue(string48, "resources.getString(R.st…plate_name_category_text)");
                String string49 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string49, "resources.getString(R.st…ld_template_name_general)");
                String string50 = resources.getString(R.string.guild_template_name_meeting_plans);
                m.checkNotNullExpressionValue(string50, "resources.getString(R.st…plate_name_meeting_plans)");
                String string51 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string51, "resources.getString(R.st…_template_name_off_topic)");
                createCategorySection15 = StockGuildTemplateKt.createCategorySection(string48, 200L, 0, new ChannelTemplate.SystemChannel(string49), new ChannelTemplate.NormalChannel(string50), new ChannelTemplate.NormalChannel(string51));
                List plus4 = u.plus((Collection) createCategorySection14, (Iterable) createCategorySection15);
                String string52 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string52, "resources.getString(R.st…late_name_category_voice)");
                String string53 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string53, "resources.getString(R.st…mplate_name_voice_lounge)");
                String string54 = resources.getString(R.string.guild_template_name_voice_meeting_room);
                m.checkNotNullExpressionValue(string54, "resources.getString(R.st…_name_voice_meeting_room)");
                createCategorySection16 = StockGuildTemplateKt.createCategorySection(string52, 300L, 2, new ChannelTemplate.NormalChannel(string53), new ChannelTemplate.NormalChannel(string54));
                return u.plus((Collection) plus4, (Iterable) createCategorySection16);
            case 6:
                String string55 = resources.getString(R.string.guild_template_name_category_information);
                m.checkNotNullExpressionValue(string55, "resources.getString(R.st…ame_category_information)");
                String string56 = resources.getString(R.string.guild_template_name_welcome_and_rules);
                m.checkNotNullExpressionValue(string56, "resources.getString(R.st…e_name_welcome_and_rules)");
                String string57 = resources.getString(R.string.guild_template_name_announcements);
                m.checkNotNullExpressionValue(string57, "resources.getString(R.st…plate_name_announcements)");
                String string58 = resources.getString(R.string.guild_template_name_resources);
                m.checkNotNullExpressionValue(string58, "resources.getString(R.st…_template_name_resources)");
                createCategorySection17 = StockGuildTemplateKt.createCategorySection(string55, 100L, 0, new ChannelTemplate.NormalChannel(string56), new ChannelTemplate.NormalChannel(string57), new ChannelTemplate.NormalChannel(string58));
                String string59 = resources.getString(R.string.guild_template_name_category_text);
                m.checkNotNullExpressionValue(string59, "resources.getString(R.st…plate_name_category_text)");
                String string60 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string60, "resources.getString(R.st…ld_template_name_general)");
                String string61 = resources.getString(R.string.guild_template_name_introductions);
                m.checkNotNullExpressionValue(string61, "resources.getString(R.st…plate_name_introductions)");
                String string62 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string62, "resources.getString(R.st…_template_name_off_topic)");
                createCategorySection18 = StockGuildTemplateKt.createCategorySection(string59, 200L, 0, new ChannelTemplate.SystemChannel(string60), new ChannelTemplate.NormalChannel(string61), new ChannelTemplate.NormalChannel(string62));
                List plus5 = u.plus((Collection) createCategorySection17, (Iterable) createCategorySection18);
                String string63 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string63, "resources.getString(R.st…late_name_category_voice)");
                String string64 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string64, "resources.getString(R.st…mplate_name_voice_lounge)");
                String string65 = resources.getString(R.string.guild_template_name_voice_meeting_room_1);
                m.checkNotNullExpressionValue(string65, "resources.getString(R.st…ame_voice_meeting_room_1)");
                String string66 = resources.getString(R.string.guild_template_name_voice_meeting_room_2);
                m.checkNotNullExpressionValue(string66, "resources.getString(R.st…ame_voice_meeting_room_2)");
                createCategorySection19 = StockGuildTemplateKt.createCategorySection(string63, 300L, 2, new ChannelTemplate.NormalChannel(string64), new ChannelTemplate.NormalChannel(string65), new ChannelTemplate.NormalChannel(string66));
                return u.plus((Collection) plus5, (Iterable) createCategorySection19);
            case 7:
                String string67 = resources.getString(R.string.guild_template_name_category_info);
                m.checkNotNullExpressionValue(string67, "resources.getString(R.st…plate_name_category_info)");
                String string68 = resources.getString(R.string.guild_template_name_notes_resources);
                m.checkNotNullExpressionValue(string68, "resources.getString(R.st…ate_name_notes_resources)");
                String string69 = resources.getString(R.string.guild_template_name_help_questions);
                m.checkNotNullExpressionValue(string69, "resources.getString(R.st…late_name_help_questions)");
                createCategorySection20 = StockGuildTemplateKt.createCategorySection(string67, 100L, 0, new ChannelTemplate.NormalChannel(string68), new ChannelTemplate.NormalChannel(string69));
                String string70 = resources.getString(R.string.guild_template_name_category_chat);
                m.checkNotNullExpressionValue(string70, "resources.getString(R.st…plate_name_category_chat)");
                String string71 = resources.getString(R.string.guild_template_name_introductions);
                m.checkNotNullExpressionValue(string71, "resources.getString(R.st…plate_name_introductions)");
                String string72 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string72, "resources.getString(R.st…ld_template_name_general)");
                String string73 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string73, "resources.getString(R.st…_template_name_off_topic)");
                createCategorySection21 = StockGuildTemplateKt.createCategorySection(string70, 200L, 0, new ChannelTemplate.NormalChannel(string71), new ChannelTemplate.SystemChannel(string72), new ChannelTemplate.NormalChannel(string73));
                List plus6 = u.plus((Collection) createCategorySection20, (Iterable) createCategorySection21);
                String string74 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string74, "resources.getString(R.st…late_name_category_voice)");
                String string75 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string75, "resources.getString(R.st…mplate_name_voice_lounge)");
                c3 = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{1}, (r4 & 4) != 0 ? b.d.j : null);
                c4 = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{2}, (r4 & 4) != 0 ? b.d.j : null);
                createCategorySection22 = StockGuildTemplateKt.createCategorySection(string74, 300L, 2, new ChannelTemplate.NormalChannel(string75), new ChannelTemplate.NormalChannel(c3.toString()), new ChannelTemplate.NormalChannel(c4.toString()));
                return u.plus((Collection) plus6, (Iterable) createCategorySection22);
            case 8:
                String string76 = resources.getString(R.string.guild_template_name_category_info);
                m.checkNotNullExpressionValue(string76, "resources.getString(R.st…plate_name_category_info)");
                String string77 = resources.getString(R.string.guild_template_name_assignments);
                m.checkNotNullExpressionValue(string77, "resources.getString(R.st…emplate_name_assignments)");
                String string78 = resources.getString(R.string.guild_template_name_help_questions);
                m.checkNotNullExpressionValue(string78, "resources.getString(R.st…late_name_help_questions)");
                String string79 = resources.getString(R.string.guild_template_name_resources);
                m.checkNotNullExpressionValue(string79, "resources.getString(R.st…_template_name_resources)");
                createCategorySection23 = StockGuildTemplateKt.createCategorySection(string76, 100L, 0, new ChannelTemplate.NormalChannel(string77), new ChannelTemplate.NormalChannel(string78), new ChannelTemplate.NormalChannel(string79));
                String string80 = resources.getString(R.string.guild_template_name_category_chat);
                m.checkNotNullExpressionValue(string80, "resources.getString(R.st…plate_name_category_chat)");
                String string81 = resources.getString(R.string.guild_template_name_introductions);
                m.checkNotNullExpressionValue(string81, "resources.getString(R.st…plate_name_introductions)");
                String string82 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string82, "resources.getString(R.st…ld_template_name_general)");
                String string83 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string83, "resources.getString(R.st…_template_name_off_topic)");
                createCategorySection24 = StockGuildTemplateKt.createCategorySection(string80, 200L, 0, new ChannelTemplate.NormalChannel(string81), new ChannelTemplate.SystemChannel(string82), new ChannelTemplate.NormalChannel(string83));
                List plus7 = u.plus((Collection) createCategorySection23, (Iterable) createCategorySection24);
                String string84 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string84, "resources.getString(R.st…late_name_category_voice)");
                String string85 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string85, "resources.getString(R.st…mplate_name_voice_lounge)");
                c5 = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{1}, (r4 & 4) != 0 ? b.d.j : null);
                c6 = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{2}, (r4 & 4) != 0 ? b.d.j : null);
                createCategorySection25 = StockGuildTemplateKt.createCategorySection(string84, 300L, 2, new ChannelTemplate.NormalChannel(string85), new ChannelTemplate.NormalChannel(c5.toString()), new ChannelTemplate.NormalChannel(c6.toString()));
                return u.plus((Collection) plus7, (Iterable) createCategorySection25);
            case 9:
                String string86 = resources.getString(R.string.guild_template_name_category_chat);
                m.checkNotNullExpressionValue(string86, "resources.getString(R.st…plate_name_category_chat)");
                String string87 = resources.getString(R.string.guild_template_name_introductions);
                m.checkNotNullExpressionValue(string87, "resources.getString(R.st…plate_name_introductions)");
                String string88 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string88, "resources.getString(R.st…ld_template_name_general)");
                String string89 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string89, "resources.getString(R.st…_template_name_off_topic)");
                String string90 = resources.getString(R.string.guild_template_name_meetups);
                m.checkNotNullExpressionValue(string90, "resources.getString(R.st…ld_template_name_meetups)");
                createCategorySection26 = StockGuildTemplateKt.createCategorySection(string86, 200L, 0, new ChannelTemplate.NormalChannel(string87), new ChannelTemplate.SystemChannel(string88), new ChannelTemplate.NormalChannel(string89), new ChannelTemplate.NormalChannel(string90));
                String string91 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string91, "resources.getString(R.st…late_name_category_voice)");
                String string92 = resources.getString(R.string.guild_template_name_voice_couches);
                m.checkNotNullExpressionValue(string92, "resources.getString(R.st…plate_name_voice_couches)");
                String string93 = resources.getString(R.string.guild_template_name_voice_movie_room);
                m.checkNotNullExpressionValue(string93, "resources.getString(R.st…te_name_voice_movie_room)");
                String string94 = resources.getString(R.string.guild_template_name_voice_unnumbered_study_room);
                m.checkNotNullExpressionValue(string94, "resources.getString(R.st…ce_unnumbered_study_room)");
                createCategorySection27 = StockGuildTemplateKt.createCategorySection(string91, 300L, 2, new ChannelTemplate.NormalChannel(string92), new ChannelTemplate.NormalChannel(string93), new ChannelTemplate.NormalChannel(string94));
                return u.plus((Collection) createCategorySection26, (Iterable) createCategorySection27);
            case 10:
                String string95 = resources.getString(R.string.guild_template_name_category_info);
                m.checkNotNullExpressionValue(string95, "resources.getString(R.st…plate_name_category_info)");
                String string96 = resources.getString(R.string.guild_template_name_resources);
                m.checkNotNullExpressionValue(string96, "resources.getString(R.st…_template_name_resources)");
                String string97 = resources.getString(R.string.guild_template_name_help_questions);
                m.checkNotNullExpressionValue(string97, "resources.getString(R.st…late_name_help_questions)");
                createCategorySection28 = StockGuildTemplateKt.createCategorySection(string95, 100L, 0, new ChannelTemplate.NormalChannel(string96), new ChannelTemplate.NormalChannel(string97));
                String string98 = resources.getString(R.string.guild_template_name_category_chat);
                m.checkNotNullExpressionValue(string98, "resources.getString(R.st…plate_name_category_chat)");
                String string99 = resources.getString(R.string.guild_template_name_introductions);
                m.checkNotNullExpressionValue(string99, "resources.getString(R.st…plate_name_introductions)");
                String string100 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string100, "resources.getString(R.st…ld_template_name_general)");
                String string101 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string101, "resources.getString(R.st…_template_name_off_topic)");
                createCategorySection29 = StockGuildTemplateKt.createCategorySection(string98, 200L, 0, new ChannelTemplate.NormalChannel(string99), new ChannelTemplate.SystemChannel(string100), new ChannelTemplate.NormalChannel(string101));
                List plus8 = u.plus((Collection) createCategorySection28, (Iterable) createCategorySection29);
                String string102 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string102, "resources.getString(R.st…late_name_category_voice)");
                String string103 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string103, "resources.getString(R.st…mplate_name_voice_lounge)");
                c7 = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{1}, (r4 & 4) != 0 ? b.d.j : null);
                c8 = b.c(resources, R.string.guild_template_name_voice_study_room, new Object[]{2}, (r4 & 4) != 0 ? b.d.j : null);
                createCategorySection30 = StockGuildTemplateKt.createCategorySection(string102, 300L, 2, new ChannelTemplate.NormalChannel(string103), new ChannelTemplate.NormalChannel(c7.toString()), new ChannelTemplate.NormalChannel(c8.toString()));
                return u.plus((Collection) plus8, (Iterable) createCategorySection30);
            case 11:
                String string104 = resources.getString(R.string.guild_template_name_category_info);
                m.checkNotNullExpressionValue(string104, "resources.getString(R.st…plate_name_category_info)");
                String string105 = resources.getString(R.string.guild_template_name_dorm_news);
                m.checkNotNullExpressionValue(string105, "resources.getString(R.st…_template_name_dorm_news)");
                createCategorySection31 = StockGuildTemplateKt.createCategorySection(string104, 100L, 0, new ChannelTemplate.NormalChannel(string105));
                String string106 = resources.getString(R.string.guild_template_name_category_chat);
                m.checkNotNullExpressionValue(string106, "resources.getString(R.st…plate_name_category_chat)");
                String string107 = resources.getString(R.string.guild_template_name_introductions);
                m.checkNotNullExpressionValue(string107, "resources.getString(R.st…plate_name_introductions)");
                String string108 = resources.getString(R.string.guild_template_name_general);
                m.checkNotNullExpressionValue(string108, "resources.getString(R.st…ld_template_name_general)");
                String string109 = resources.getString(R.string.guild_template_name_off_topic);
                m.checkNotNullExpressionValue(string109, "resources.getString(R.st…_template_name_off_topic)");
                createCategorySection32 = StockGuildTemplateKt.createCategorySection(string106, 200L, 0, new ChannelTemplate.NormalChannel(string107), new ChannelTemplate.SystemChannel(string108), new ChannelTemplate.NormalChannel(string109));
                List plus9 = u.plus((Collection) createCategorySection31, (Iterable) createCategorySection32);
                String string110 = resources.getString(R.string.guild_template_name_category_voice);
                m.checkNotNullExpressionValue(string110, "resources.getString(R.st…late_name_category_voice)");
                String string111 = resources.getString(R.string.guild_template_name_voice_lounge);
                m.checkNotNullExpressionValue(string111, "resources.getString(R.st…mplate_name_voice_lounge)");
                String string112 = resources.getString(R.string.guild_template_name_voice_movie_room);
                m.checkNotNullExpressionValue(string112, "resources.getString(R.st…te_name_voice_movie_room)");
                String string113 = resources.getString(R.string.guild_template_name_voice_couches);
                m.checkNotNullExpressionValue(string113, "resources.getString(R.st…plate_name_voice_couches)");
                createCategorySection33 = StockGuildTemplateKt.createCategorySection(string110, 300L, 2, new ChannelTemplate.NormalChannel(string111), new ChannelTemplate.NormalChannel(string112), new ChannelTemplate.NormalChannel(string113));
                return u.plus((Collection) plus9, (Iterable) createCategorySection33);
            case 12:
                return n.emptyList();
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public final long getSystemChannelId() {
        return 11L;
    }
}
