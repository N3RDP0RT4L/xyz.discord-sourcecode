package com.discord.widgets.guilds;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Selection;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.d.b.a.a;
import b.k.a.a.f;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGuildFolderSettingsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.guilds.WidgetGuildFolderSettingsViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import d0.z.d.a0;
import d0.z.d.m;
import f0.e0.c;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGuildFolderSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 ,2\u00020\u0001:\u0001,B\u0007¢\u0006\u0004\b+\u0010\u001eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\u000e\u001a\u00020\r2\b\b\u0001\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u000b2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u0019H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001d\u0010\u001eR\u001d\u0010$\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001d\u0010*\u001a\u00020%8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)¨\u0006-"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState;", "state", "", "configureUI", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState;)V", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event;)V", "", "currentColor", "", "getColorsToDisplay", "(I)[I", ModelAuditLogEntry.CHANGE_KEY_COLOR, "rgbColorToARGB", "(Ljava/lang/Integer;)Ljava/lang/Integer;", "argbColorToRGB", "(I)I", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState$Loaded;", "data", "launchColorPicker", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState$Loaded;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetGuildFolderSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildFolderSettingsBinding;", "binding", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildFolderSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildFolderSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildFolderSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String DIALOG_TAG_COLOR_PICKER = "DIALOG_TAG_COLOR_PICKER";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildFolderSettings$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetGuildFolderSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettings$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/FolderId;", "folderId", "", "create", "(Landroid/content/Context;J)V", "", WidgetGuildFolderSettings.DIALOG_TAG_COLOR_PICKER, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetGuildFolderSettings.class, new Intent().putExtra("com.discord.intent.extra.EXTRA_GUILD_FOLDER_ID", j));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildFolderSettings() {
        super(R.layout.widget_guild_folder_settings);
        WidgetGuildFolderSettings$viewModel$2 widgetGuildFolderSettings$viewModel$2 = new WidgetGuildFolderSettings$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGuildFolderSettingsViewModel.class), new WidgetGuildFolderSettings$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildFolderSettings$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final int argbColorToRGB(int i) {
        return Color.argb(0, Color.red(i), Color.green(i), Color.blue(i));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetGuildFolderSettingsViewModel.ViewState viewState) {
        int i;
        if (viewState instanceof WidgetGuildFolderSettingsViewModel.ViewState.Loaded) {
            View view = getBinding().e;
            m.checkNotNullExpressionValue(view, "binding.guildFolderSettingsColorDisplay");
            Drawable drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.drawable_circle_white_1);
            WidgetGuildFolderSettingsViewModel.ViewState.Loaded loaded = (WidgetGuildFolderSettingsViewModel.ViewState.Loaded) viewState;
            Integer rgbColorToARGB = rgbColorToARGB(loaded.getFormState().getColor());
            if (rgbColorToARGB != null) {
                i = rgbColorToARGB.intValue();
            } else {
                View view2 = getBinding().e;
                m.checkNotNullExpressionValue(view2, "binding.guildFolderSettingsColorDisplay");
                i = ColorCompat.getThemedColor(view2, (int) R.attr.color_brand);
            }
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_ATOP));
                View view3 = getBinding().e;
                m.checkNotNullExpressionValue(view3, "binding.guildFolderSettingsColorDisplay");
                view3.setBackground(drawable);
            }
            TextInputLayout textInputLayout = getBinding().f2391b;
            m.checkNotNullExpressionValue(textInputLayout, "binding.editGuildFolderName");
            ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetGuildFolderSettings$configureUI$1(this));
            TextInputLayout textInputLayout2 = getBinding().f2391b;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.editGuildFolderName");
            String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout2);
            boolean z2 = true;
            int i2 = 0;
            if (!m.areEqual(loaded.getFormState().getName(), textOrEmpty)) {
                TextInputLayout textInputLayout3 = getBinding().f2391b;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.editGuildFolderName");
                ViewExtensions.setText(textInputLayout3, loaded.getFormState().getName());
                if (textOrEmpty.length() != 0) {
                    z2 = false;
                }
                if (z2) {
                    TextInputLayout textInputLayout4 = getBinding().f2391b;
                    m.checkNotNullExpressionValue(textInputLayout4, "binding.editGuildFolderName");
                    EditText editText = textInputLayout4.getEditText();
                    Editable text = editText != null ? editText.getText() : null;
                    TextInputLayout textInputLayout5 = getBinding().f2391b;
                    m.checkNotNullExpressionValue(textInputLayout5, "binding.editGuildFolderName");
                    Selection.setSelection(text, ViewExtensions.getTextOrEmpty(textInputLayout5).length());
                }
            }
            TextView textView = getBinding().c;
            m.checkNotNullExpressionValue(textView, "binding.guildFolderColorName");
            StringBuilder sb = new StringBuilder();
            sb.append(MentionUtilsKt.CHANNELS_CHAR);
            String x2 = c.x(i);
            Objects.requireNonNull(x2, "null cannot be cast to non-null type java.lang.String");
            String substring = x2.substring(2);
            m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            Objects.requireNonNull(substring, "null cannot be cast to non-null type java.lang.String");
            String upperCase = substring.toUpperCase(locale);
            m.checkNotNullExpressionValue(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            sb.append(upperCase);
            textView.setText(sb.toString());
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.WidgetGuildFolderSettings$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view4) {
                    WidgetGuildFolderSettings.this.launchColorPicker((WidgetGuildFolderSettingsViewModel.ViewState.Loaded) viewState);
                }
            });
            FloatingActionButton floatingActionButton = getBinding().f;
            m.checkNotNullExpressionValue(floatingActionButton, "binding.guildFolderSettingsSave");
            if (!loaded.getShowSave()) {
                i2 = 8;
            }
            floatingActionButton.setVisibility(i2);
            getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.WidgetGuildFolderSettings$configureUI$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view4) {
                    WidgetGuildFolderSettingsViewModel viewModel;
                    viewModel = WidgetGuildFolderSettings.this.getViewModel();
                    viewModel.saveFolder();
                }
            });
        }
    }

    private final WidgetGuildFolderSettingsBinding getBinding() {
        return (WidgetGuildFolderSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @ColorInt
    private final int[] getColorsToDisplay(@ColorInt int i) {
        int[] intArray = getResources().getIntArray(R.array.color_picker_palette);
        m.checkNotNullExpressionValue(intArray, "resources.getIntArray(R.…ray.color_picker_palette)");
        ArrayList arrayList = new ArrayList();
        boolean z2 = false;
        for (int i2 : intArray) {
            arrayList.add(Integer.valueOf(i2));
            if (i2 == i) {
                z2 = true;
            }
        }
        if (!z2) {
            arrayList.add(Integer.valueOf(i));
        }
        int[] iArr = new int[arrayList.size()];
        int size = arrayList.size();
        for (int i3 = 0; i3 < size; i3++) {
            iArr[i3] = ((Number) arrayList.get(i3)).intValue();
        }
        return iArr;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGuildFolderSettingsViewModel getViewModel() {
        return (WidgetGuildFolderSettingsViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(WidgetGuildFolderSettingsViewModel.Event event) {
        if (event instanceof WidgetGuildFolderSettingsViewModel.Event.UpdateFolderSettingsSuccess) {
            b.a.d.m.i(this, ((WidgetGuildFolderSettingsViewModel.Event.UpdateFolderSettingsSuccess) event).getSuccessMessageStringRes(), 0, 4);
            AppFragment.hideKeyboard$default(this, null, 1, null);
            FragmentActivity activity = e();
            if (activity != null) {
                activity.onBackPressed();
            }
        } else if (event instanceof WidgetGuildFolderSettingsViewModel.Event.UpdateFolderSettingsFailure) {
            b.a.d.m.g(getContext(), ((WidgetGuildFolderSettingsViewModel.Event.UpdateFolderSettingsFailure) event).getFailureMessageStringRes(), 0, null, 12);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchColorPicker(WidgetGuildFolderSettingsViewModel.ViewState.Loaded loaded) {
        Integer rgbColorToARGB = rgbColorToARGB(loaded.getFormState().getColor());
        int intValue = rgbColorToARGB != null ? rgbColorToARGB.intValue() : ColorCompat.getThemedColor(getContext(), (int) R.attr.color_brand);
        ColorPickerDialog.k kVar = new ColorPickerDialog.k();
        kVar.h = intValue;
        kVar.f3120s = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundPrimary);
        kVar.i = false;
        kVar.g = getColorsToDisplay(intValue);
        kVar.a = R.string.guild_folder_color;
        kVar.r = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorHeaderPrimary);
        FontUtils fontUtils = FontUtils.INSTANCE;
        kVar.f3121x = fontUtils.getThemedFontResId(getContext(), R.attr.font_display_bold);
        kVar.o = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundAccent);
        kVar.c = R.string.color_picker_custom;
        kVar.v = ColorCompat.getColor(getContext(), (int) R.color.white);
        kVar.f3119b = R.string.color_picker_presets;
        kVar.p = ColorCompat.getThemedColor(getContext(), (int) R.attr.color_brand);
        kVar.d = R.string.select;
        kVar.l = true;
        kVar.e = R.string.reset;
        kVar.w = ColorCompat.getColor(getContext(), (int) R.color.white);
        kVar.f3122y = fontUtils.getThemedFontResId(getContext(), R.attr.font_primary_semibold);
        kVar.q = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundModifierAccent);
        kVar.t = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorTextMuted);
        kVar.u = R.drawable.drawable_cpv_edit_text_background;
        kVar.f3123z = fontUtils.getThemedFontResId(getContext(), R.attr.font_primary_normal);
        ColorPickerDialog a = kVar.a();
        a.k = new f() { // from class: com.discord.widgets.guilds.WidgetGuildFolderSettings$launchColorPicker$1
            @Override // b.k.a.a.f
            public void onColorReset(int i) {
                WidgetGuildFolderSettingsViewModel viewModel;
                int argbColorToRGB;
                int themedColor = ColorCompat.getThemedColor(WidgetGuildFolderSettings.this.getContext(), (int) R.attr.color_brand);
                viewModel = WidgetGuildFolderSettings.this.getViewModel();
                argbColorToRGB = WidgetGuildFolderSettings.this.argbColorToRGB(themedColor);
                viewModel.setColor(Integer.valueOf(argbColorToRGB));
            }

            @Override // b.k.a.a.f
            public void onColorSelected(int i, int i2) {
                WidgetGuildFolderSettingsViewModel viewModel;
                int argbColorToRGB;
                viewModel = WidgetGuildFolderSettings.this.getViewModel();
                argbColorToRGB = WidgetGuildFolderSettings.this.argbColorToRGB(i2);
                viewModel.setColor(Integer.valueOf(argbColorToRGB));
            }

            @Override // b.k.a.a.f
            public void onDialogDismissed(int i) {
            }
        };
        AppFragment.hideKeyboard$default(this, null, 1, null);
        a.show(getParentFragmentManager(), DIALOG_TAG_COLOR_PICKER);
    }

    private final Integer rgbColorToARGB(Integer num) {
        if (num != null) {
            return Integer.valueOf((int) (num.intValue() + 4278190080L));
        }
        return null;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.server_folder_settings);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable q = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "viewModel.observeViewSta…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetGuildFolderSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildFolderSettings$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetGuildFolderSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildFolderSettings$onViewBoundOrOnResume$2(this));
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Fragment findFragmentByTag = parentFragmentManager.findFragmentByTag(DIALOG_TAG_COLOR_PICKER);
        if (findFragmentByTag != null) {
            parentFragmentManager.beginTransaction().remove(findFragmentByTag).commit();
        }
    }
}
