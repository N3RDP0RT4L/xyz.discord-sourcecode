package com.discord.widgets.guilds.list;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.api.guildjoinrequest.GuildJoinRequest;
import com.discord.api.permission.Permission;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGuildsSorted;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.utilities.guilds.MemberVerificationUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.streams.StreamContextService;
import com.discord.utilities.time.Clock;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.guilds.list.GuildListItem;
import com.discord.widgets.guilds.list.WidgetGuildListAdapter;
import d0.c0.c;
import d0.f0.q;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.sequences.Sequence;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetGuildsListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000æ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u001e\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 x2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004xyz{B!\u0012\b\b\u0002\u0010i\u001a\u00020h\u0012\u000e\b\u0002\u0010u\u001a\b\u0012\u0004\u0012\u00020\u00180T¢\u0006\u0004\bv\u0010wJ/\u0010\n\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00032\u000e\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ9\u0010\u0012\u001a\u00020\t2\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\r0\fj\b\u0012\u0004\u0012\u00020\r`\u000e2\u0010\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J'\u0010\u0014\u001a\u00020\t2\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\r0\fj\b\u0012\u0004\u0012\u00020\r`\u000eH\u0002¢\u0006\u0004\b\u0014\u0010\u0015J7\u0010\u0016\u001a\u00020\t2\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\r0\fj\b\u0012\u0004\u0012\u00020\r`\u000e2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJµ\u0002\u00109\u001a\u0002082\u0006\u0010\u001d\u001a\u00020\u001c2\n\u0010\u001f\u001a\u00060\u0006j\u0002`\u001e2\n\u0010!\u001a\u00060\u0006j\u0002` 2\u0010\u0010\"\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u0016\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u001e\u0012\u0004\u0012\u00020$0#2\u0006\u0010&\u001a\u00020\u00032 \u0010(\u001a\u001c\u0012\b\u0012\u00060\u0006j\u0002`\u001e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0006j\u0002` 0'0#2\u0016\u0010*\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002` \u0012\u0004\u0012\u00020)0#2\u0010\u0010+\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u0010\u0010,\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u0010\u0010-\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u000e\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\b\u0010/\u001a\u0004\u0018\u00010.2\u0018\u00102\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`0\u0012\u0004\u0012\u000201\u0018\u00010#2\u001a\u00104\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002` \u0012\b\u0012\u00060\u0006j\u0002`30#2\b\u00106\u001a\u0004\u0018\u0001052\b\b\u0002\u00107\u001a\u00020.H\u0002¢\u0006\u0004\b9\u0010:JO\u0010@\u001a\b\u0012\u0004\u0012\u00020?0>2\u0016\u0010;\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002` \u0012\u0004\u0012\u00020)0#2\u0016\u0010<\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002` \u0012\u0004\u0012\u00020\u00030#2\b\u0010=\u001a\u0004\u0018\u00010$H\u0002¢\u0006\u0004\b@\u0010AJµ\u0002\u0010F\u001a\b\u0012\u0004\u0012\u00020\r0'2\f\u0010B\u001a\b\u0012\u0004\u0012\u00020\u001c0'2\u0016\u0010D\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u001e\u0012\u0004\u0012\u00020C0#2\u0010\u0010E\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u00102\n\u0010\u001f\u001a\u00060\u0006j\u0002`\u001e2\n\u0010!\u001a\u00060\u0006j\u0002` 2\u0010\u0010\"\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u0016\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u001e\u0012\u0004\u0012\u00020$0#2 \u0010(\u001a\u001c\u0012\b\u0012\u00060\u0006j\u0002`\u001e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0006j\u0002` 0'0#2\u0016\u0010*\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002` \u0012\u0004\u0012\u00020)0#2\u0010\u0010+\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u0010\u0010,\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u0010\u0010-\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u001e0\u00102\u0018\u00102\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`0\u0012\u0004\u0012\u000201\u0018\u00010#2\u001a\u00104\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002` \u0012\b\u0012\u00060\u0006j\u0002`30#H\u0002¢\u0006\u0004\bF\u0010GJU\u0010K\u001a\u00020\u00032\n\u0010H\u001a\u00060\u0006j\u0002`\u001e2 \u0010J\u001a\u001c\u0012\b\u0012\u00060\u0006j\u0002`\u001e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0006j\u0002` 0I0#2\u0016\u0010<\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002` \u0012\u0004\u0012\u00020\u00030#H\u0002¢\u0006\u0004\bK\u0010LJ[\u0010P\u001a\u00020.2\n\u0010H\u001a\u00060\u0006j\u0002`\u001e2\b\u0010M\u001a\u0004\u0018\u00010$2\u0018\u0010N\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`0\u0012\u0004\u0012\u000201\u0018\u00010#2\u001a\u0010O\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002` \u0012\b\u0012\u00060\u0006j\u0002`30#H\u0002¢\u0006\u0004\bP\u0010QJ\u0013\u0010R\u001a\u00020.*\u00020\u001cH\u0002¢\u0006\u0004\bR\u0010SJ\u0013\u0010V\u001a\b\u0012\u0004\u0012\u00020U0T¢\u0006\u0004\bV\u0010WJ%\u0010]\u001a\u00020\t2\u0006\u0010X\u001a\u00020\r2\u0006\u0010Z\u001a\u00020Y2\u0006\u0010\\\u001a\u00020[¢\u0006\u0004\b]\u0010^J\u0015\u0010_\u001a\u00020\t2\u0006\u0010X\u001a\u00020\r¢\u0006\u0004\b_\u0010`J\r\u0010a\u001a\u00020.¢\u0006\u0004\ba\u0010bJ\u001d\u0010c\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0004\bc\u0010dJ\u001d\u0010f\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010e\u001a\u00020\u0003¢\u0006\u0004\bf\u0010dJ\u001d\u0010g\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010e\u001a\u00020\u0003¢\u0006\u0004\bg\u0010dR\u0016\u0010i\u001a\u00020h8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010jR\u0016\u0010k\u001a\u00020.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bk\u0010lR\u0016\u0010m\u001a\u00020.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bm\u0010lR:\u0010p\u001a&\u0012\f\u0012\n o*\u0004\u0018\u00010U0U o*\u0012\u0012\f\u0012\n o*\u0004\u0018\u00010U0U\u0018\u00010n0n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bp\u0010qR\u0018\u0010s\u001a\u0004\u0018\u00010r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bs\u0010t¨\u0006|"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;", "", "fromPosition", "toPosition", "", "Lcom/discord/primitives/FolderId;", "folderId", "", "move", "(IILjava/lang/Long;)V", "Ljava/util/ArrayList;", "Lcom/discord/widgets/guilds/list/GuildListItem;", "Lkotlin/collections/ArrayList;", "editingList", "", "changedFolderIds", "rebuildFolders", "(Ljava/util/ArrayList;Ljava/util/Set;)V", "untargetCurrentTarget", "(Ljava/util/ArrayList;)V", "performTargetOperation", "(Ljava/util/ArrayList;II)V", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;)V", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/primitives/GuildId;", "selectedGuildId", "Lcom/discord/primitives/ChannelId;", "selectedVoiceChannelId", "unreadGuildIds", "", "Lcom/discord/models/domain/ModelNotificationSettings;", "guildSettings", "numMentions", "", "channelIds", "Lcom/discord/api/channel/Channel;", "channels", "lurkingGuildIds", "guildIdsWithActiveStageEvents", "guildIdsWithActiveScheduledEvents", "", "isLastGuildInFolder", "Lcom/discord/primitives/UserId;", "Lcom/discord/utilities/streams/StreamContext;", "allApplicationStreamContexts", "Lcom/discord/api/permission/PermissionBit;", "allChannelPermissions", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "applicationStatus", "isPendingGuild", "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;", "createGuildItem", "(Lcom/discord/models/guild/Guild;JJLjava/util/Set;Ljava/util/Map;ILjava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/util/Map;Ljava/util/Map;Lcom/discord/api/guildjoinrequest/ApplicationStatus;Z)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;", "privateChannels", "mentionCounts", "dmSettings", "Lkotlin/sequences/Sequence;", "Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;", "createDirectMessageItems", "(Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/domain/ModelNotificationSettings;)Lkotlin/sequences/Sequence;", "pendingGuilds", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "guildJoinRequests", "openFolderIds", "createPendingGuildsFolder", "(Ljava/util/List;Ljava/util/Map;Ljava/util/Set;JJLjava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;", "guildId", "", "guildChannels", "sumMentionCountsForGuild", "(JLjava/util/Map;Ljava/util/Map;)I", "guildNotificationSettings", "streamContexts", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "shouldDisplayVideoIconOnGuild", "(JLcom/discord/models/domain/ModelNotificationSettings;Ljava/util/Map;Ljava/util/Map;)Z", "shouldShowUnread", "(Lcom/discord/models/guild/Guild;)Z", "Lrx/Observable;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", "listenForEvents", "()Lrx/Observable;", "item", "Landroid/content/Context;", "context", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "onItemClicked", "(Lcom/discord/widgets/guilds/list/GuildListItem;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V", "onItemLongPressed", "(Lcom/discord/widgets/guilds/list/GuildListItem;)V", "onDrop", "()Z", "target", "(II)V", "targetPosition", "moveAbove", "moveBelow", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "wasLeftPanelOpened", "Z", "wasOnHomeTab", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;", "currentTargetOperation", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildsListViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final Clock clock;
    private WidgetGuildListAdapter.Operation.TargetOperation currentTargetOperation;
    private final PublishSubject<Event> eventSubject;
    private boolean wasLeftPanelOpened;
    private boolean wasOnHomeTab;

    /* compiled from: WidgetGuildsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.list.WidgetGuildsListViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            WidgetGuildsListViewModel widgetGuildsListViewModel = WidgetGuildsListViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            widgetGuildsListViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetGuildsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0002\n\u000bB\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\f"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Companion;", "", "Lcom/discord/utilities/time/Clock;", "clock", "Lrx/Observable;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;", "observeStores", "(Lcom/discord/utilities/time/Clock;)Lrx/Observable;", HookHelper.constructorName, "()V", "Chunk", "SecondChunk", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0082\b\u0018\u00002\u00020\u0001B·\u0001\u0012\n\u0010\u0019\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u001a\u001a\u00060\u0002j\u0002`\u0006\u0012\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\b\u0012\u0010\u0010\u001c\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0016\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\b\u0012 \u0010\u001e\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\b\u0012\u0010\u0010\u001f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0016\u0010 \u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b\u0012\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00160\u0011¢\u0006\u0004\b:\u0010;J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u001c\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\r\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ \u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\bHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000bJ*\u0010\u0012\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000bJ\u001a\u0010\u0013\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000eJ \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\bHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u000bJ\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0011HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018JÒ\u0001\u0010\"\u001a\u00020\u00002\f\b\u0002\u0010\u0019\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u001a\u001a\u00060\u0002j\u0002`\u00062\u0014\b\u0002\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\b2\u0012\b\u0002\u0010\u001c\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0018\b\u0002\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\b2\"\b\u0002\u0010\u001e\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\b2\u0012\b\u0002\u0010\u001f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0018\b\u0002\u0010 \u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b2\u000e\b\u0002\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00160\u0011HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b'\u0010(J\u001a\u0010+\u001a\u00020*2\b\u0010)\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b+\u0010,R\u001d\u0010\u001a\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010-\u001a\u0004\b.\u0010\u0005R3\u0010\u001e\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010/\u001a\u0004\b0\u0010\u000bR\u001f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00160\u00118\u0006@\u0006¢\u0006\f\n\u0004\b!\u00101\u001a\u0004\b2\u0010\u0018R\u001d\u0010\u0019\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010-\u001a\u0004\b3\u0010\u0005R#\u0010\u001c\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00104\u001a\u0004\b5\u0010\u000eR)\u0010 \u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010/\u001a\u0004\b6\u0010\u000bR#\u0010\u001f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00104\u001a\u0004\b7\u0010\u000eR%\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010/\u001a\u0004\b8\u0010\u000bR)\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010/\u001a\u0004\b9\u0010\u000b¨\u0006<"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Companion$Chunk;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "", "Lcom/discord/models/domain/ModelNotificationSettings;", "component3", "()Ljava/util/Map;", "", "component4", "()Ljava/util/Set;", "", "component5", "", "component6", "component7", "Lcom/discord/api/channel/Channel;", "component8", "Lcom/discord/stores/StoreGuildsSorted$Entry;", "component9", "()Ljava/util/List;", "selectedGuildId", "selectedVoiceChannelId", "guildSettings", "unreadGuildIds", "mentionCounts", "channelIds", "unavailableGuilds", "privateChannels", "sortedGuilds", "copy", "(JJLjava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/List;)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Companion$Chunk;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getSelectedVoiceChannelId", "Ljava/util/Map;", "getChannelIds", "Ljava/util/List;", "getSortedGuilds", "getSelectedGuildId", "Ljava/util/Set;", "getUnreadGuildIds", "getPrivateChannels", "getUnavailableGuilds", "getGuildSettings", "getMentionCounts", HookHelper.constructorName, "(JJLjava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Chunk {
            private final Map<Long, List<Long>> channelIds;
            private final Map<Long, ModelNotificationSettings> guildSettings;
            private final Map<Long, Integer> mentionCounts;
            private final Map<Long, Channel> privateChannels;
            private final long selectedGuildId;
            private final long selectedVoiceChannelId;
            private final List<StoreGuildsSorted.Entry> sortedGuilds;
            private final Set<Long> unavailableGuilds;
            private final Set<Long> unreadGuildIds;

            /* JADX WARN: Multi-variable type inference failed */
            public Chunk(long j, long j2, Map<Long, ? extends ModelNotificationSettings> map, Set<Long> set, Map<Long, Integer> map2, Map<Long, ? extends List<Long>> map3, Set<Long> set2, Map<Long, Channel> map4, List<? extends StoreGuildsSorted.Entry> list) {
                m.checkNotNullParameter(map, "guildSettings");
                m.checkNotNullParameter(set, "unreadGuildIds");
                m.checkNotNullParameter(map2, "mentionCounts");
                m.checkNotNullParameter(map3, "channelIds");
                m.checkNotNullParameter(set2, "unavailableGuilds");
                m.checkNotNullParameter(map4, "privateChannels");
                m.checkNotNullParameter(list, "sortedGuilds");
                this.selectedGuildId = j;
                this.selectedVoiceChannelId = j2;
                this.guildSettings = map;
                this.unreadGuildIds = set;
                this.mentionCounts = map2;
                this.channelIds = map3;
                this.unavailableGuilds = set2;
                this.privateChannels = map4;
                this.sortedGuilds = list;
            }

            public final long component1() {
                return this.selectedGuildId;
            }

            public final long component2() {
                return this.selectedVoiceChannelId;
            }

            public final Map<Long, ModelNotificationSettings> component3() {
                return this.guildSettings;
            }

            public final Set<Long> component4() {
                return this.unreadGuildIds;
            }

            public final Map<Long, Integer> component5() {
                return this.mentionCounts;
            }

            public final Map<Long, List<Long>> component6() {
                return this.channelIds;
            }

            public final Set<Long> component7() {
                return this.unavailableGuilds;
            }

            public final Map<Long, Channel> component8() {
                return this.privateChannels;
            }

            public final List<StoreGuildsSorted.Entry> component9() {
                return this.sortedGuilds;
            }

            public final Chunk copy(long j, long j2, Map<Long, ? extends ModelNotificationSettings> map, Set<Long> set, Map<Long, Integer> map2, Map<Long, ? extends List<Long>> map3, Set<Long> set2, Map<Long, Channel> map4, List<? extends StoreGuildsSorted.Entry> list) {
                m.checkNotNullParameter(map, "guildSettings");
                m.checkNotNullParameter(set, "unreadGuildIds");
                m.checkNotNullParameter(map2, "mentionCounts");
                m.checkNotNullParameter(map3, "channelIds");
                m.checkNotNullParameter(set2, "unavailableGuilds");
                m.checkNotNullParameter(map4, "privateChannels");
                m.checkNotNullParameter(list, "sortedGuilds");
                return new Chunk(j, j2, map, set, map2, map3, set2, map4, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Chunk)) {
                    return false;
                }
                Chunk chunk = (Chunk) obj;
                return this.selectedGuildId == chunk.selectedGuildId && this.selectedVoiceChannelId == chunk.selectedVoiceChannelId && m.areEqual(this.guildSettings, chunk.guildSettings) && m.areEqual(this.unreadGuildIds, chunk.unreadGuildIds) && m.areEqual(this.mentionCounts, chunk.mentionCounts) && m.areEqual(this.channelIds, chunk.channelIds) && m.areEqual(this.unavailableGuilds, chunk.unavailableGuilds) && m.areEqual(this.privateChannels, chunk.privateChannels) && m.areEqual(this.sortedGuilds, chunk.sortedGuilds);
            }

            public final Map<Long, List<Long>> getChannelIds() {
                return this.channelIds;
            }

            public final Map<Long, ModelNotificationSettings> getGuildSettings() {
                return this.guildSettings;
            }

            public final Map<Long, Integer> getMentionCounts() {
                return this.mentionCounts;
            }

            public final Map<Long, Channel> getPrivateChannels() {
                return this.privateChannels;
            }

            public final long getSelectedGuildId() {
                return this.selectedGuildId;
            }

            public final long getSelectedVoiceChannelId() {
                return this.selectedVoiceChannelId;
            }

            public final List<StoreGuildsSorted.Entry> getSortedGuilds() {
                return this.sortedGuilds;
            }

            public final Set<Long> getUnavailableGuilds() {
                return this.unavailableGuilds;
            }

            public final Set<Long> getUnreadGuildIds() {
                return this.unreadGuildIds;
            }

            public int hashCode() {
                int a = (b.a(this.selectedVoiceChannelId) + (b.a(this.selectedGuildId) * 31)) * 31;
                Map<Long, ModelNotificationSettings> map = this.guildSettings;
                int i = 0;
                int hashCode = (a + (map != null ? map.hashCode() : 0)) * 31;
                Set<Long> set = this.unreadGuildIds;
                int hashCode2 = (hashCode + (set != null ? set.hashCode() : 0)) * 31;
                Map<Long, Integer> map2 = this.mentionCounts;
                int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
                Map<Long, List<Long>> map3 = this.channelIds;
                int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
                Set<Long> set2 = this.unavailableGuilds;
                int hashCode5 = (hashCode4 + (set2 != null ? set2.hashCode() : 0)) * 31;
                Map<Long, Channel> map4 = this.privateChannels;
                int hashCode6 = (hashCode5 + (map4 != null ? map4.hashCode() : 0)) * 31;
                List<StoreGuildsSorted.Entry> list = this.sortedGuilds;
                if (list != null) {
                    i = list.hashCode();
                }
                return hashCode6 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Chunk(selectedGuildId=");
                R.append(this.selectedGuildId);
                R.append(", selectedVoiceChannelId=");
                R.append(this.selectedVoiceChannelId);
                R.append(", guildSettings=");
                R.append(this.guildSettings);
                R.append(", unreadGuildIds=");
                R.append(this.unreadGuildIds);
                R.append(", mentionCounts=");
                R.append(this.mentionCounts);
                R.append(", channelIds=");
                R.append(this.channelIds);
                R.append(", unavailableGuilds=");
                R.append(this.unavailableGuilds);
                R.append(", privateChannels=");
                R.append(this.privateChannels);
                R.append(", sortedGuilds=");
                return a.K(R, this.sortedGuilds, ")");
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\b\u0082\b\u0018\u00002\u00020\u0001B\u0083\u0001\u0012\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0010\u0010\u0019\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f\u0012\u0010\u0010\u001a\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f\u0012\u0010\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0014¢\u0006\u0004\b4\u00105J \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\r\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u000f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u001a\u0010\u0010\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000eJ \u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u0002HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0007J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u009a\u0001\u0010\u001e\u001a\u00020\u00002\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0012\b\u0002\u0010\u0019\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f2\u0012\b\u0002\u0010\u001a\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f2\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f2\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u00022\b\b\u0002\u0010\u001d\u001a\u00020\u0014HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010'\u001a\u00020\u00142\b\u0010&\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R#\u0010\u001a\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b*\u0010\u000eR)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010+\u001a\u0004\b,\u0010\u0007R\u0019\u0010\u001d\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010-\u001a\u0004\b.\u0010\u0016R)\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010+\u001a\u0004\b/\u0010\u0007R#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010)\u001a\u0004\b0\u0010\u000eR#\u0010\u0019\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010)\u001a\u0004\b1\u0010\u000eR\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00102\u001a\u0004\b3\u0010\u000b¨\u00066"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Companion$SecondChunk;", "", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "component1", "()Ljava/util/Map;", "", "Lcom/discord/models/guild/Guild;", "component2", "()Ljava/util/List;", "", "component3", "()Ljava/util/Set;", "component4", "component5", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component6", "", "component7", "()Z", "guildJoinRequests", "pendingGuilds", "guildIds", "guildIdsWithActiveStageEvents", "guildIdsWithActiveScheduledEvents", "channels", "showHubSparkle", "copy", "(Ljava/util/Map;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Z)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Companion$SecondChunk;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getGuildIdsWithActiveStageEvents", "Ljava/util/Map;", "getChannels", "Z", "getShowHubSparkle", "getGuildJoinRequests", "getGuildIdsWithActiveScheduledEvents", "getGuildIds", "Ljava/util/List;", "getPendingGuilds", HookHelper.constructorName, "(Ljava/util/Map;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SecondChunk {
            private final Map<Long, Channel> channels;
            private final Set<Long> guildIds;
            private final Set<Long> guildIdsWithActiveScheduledEvents;
            private final Set<Long> guildIdsWithActiveStageEvents;
            private final Map<Long, GuildJoinRequest> guildJoinRequests;
            private final List<Guild> pendingGuilds;
            private final boolean showHubSparkle;

            public SecondChunk(Map<Long, GuildJoinRequest> map, List<Guild> list, Set<Long> set, Set<Long> set2, Set<Long> set3, Map<Long, Channel> map2, boolean z2) {
                m.checkNotNullParameter(map, "guildJoinRequests");
                m.checkNotNullParameter(list, "pendingGuilds");
                m.checkNotNullParameter(set, "guildIds");
                m.checkNotNullParameter(set2, "guildIdsWithActiveStageEvents");
                m.checkNotNullParameter(set3, "guildIdsWithActiveScheduledEvents");
                m.checkNotNullParameter(map2, "channels");
                this.guildJoinRequests = map;
                this.pendingGuilds = list;
                this.guildIds = set;
                this.guildIdsWithActiveStageEvents = set2;
                this.guildIdsWithActiveScheduledEvents = set3;
                this.channels = map2;
                this.showHubSparkle = z2;
            }

            public static /* synthetic */ SecondChunk copy$default(SecondChunk secondChunk, Map map, List list, Set set, Set set2, Set set3, Map map2, boolean z2, int i, Object obj) {
                Map<Long, GuildJoinRequest> map3 = map;
                if ((i & 1) != 0) {
                    map3 = secondChunk.guildJoinRequests;
                }
                List<Guild> list2 = list;
                if ((i & 2) != 0) {
                    list2 = secondChunk.pendingGuilds;
                }
                List list3 = list2;
                Set<Long> set4 = set;
                if ((i & 4) != 0) {
                    set4 = secondChunk.guildIds;
                }
                Set set5 = set4;
                Set<Long> set6 = set2;
                if ((i & 8) != 0) {
                    set6 = secondChunk.guildIdsWithActiveStageEvents;
                }
                Set set7 = set6;
                Set<Long> set8 = set3;
                if ((i & 16) != 0) {
                    set8 = secondChunk.guildIdsWithActiveScheduledEvents;
                }
                Set set9 = set8;
                Map<Long, Channel> map4 = map2;
                if ((i & 32) != 0) {
                    map4 = secondChunk.channels;
                }
                Map map5 = map4;
                if ((i & 64) != 0) {
                    z2 = secondChunk.showHubSparkle;
                }
                return secondChunk.copy(map3, list3, set5, set7, set9, map5, z2);
            }

            public final Map<Long, GuildJoinRequest> component1() {
                return this.guildJoinRequests;
            }

            public final List<Guild> component2() {
                return this.pendingGuilds;
            }

            public final Set<Long> component3() {
                return this.guildIds;
            }

            public final Set<Long> component4() {
                return this.guildIdsWithActiveStageEvents;
            }

            public final Set<Long> component5() {
                return this.guildIdsWithActiveScheduledEvents;
            }

            public final Map<Long, Channel> component6() {
                return this.channels;
            }

            public final boolean component7() {
                return this.showHubSparkle;
            }

            public final SecondChunk copy(Map<Long, GuildJoinRequest> map, List<Guild> list, Set<Long> set, Set<Long> set2, Set<Long> set3, Map<Long, Channel> map2, boolean z2) {
                m.checkNotNullParameter(map, "guildJoinRequests");
                m.checkNotNullParameter(list, "pendingGuilds");
                m.checkNotNullParameter(set, "guildIds");
                m.checkNotNullParameter(set2, "guildIdsWithActiveStageEvents");
                m.checkNotNullParameter(set3, "guildIdsWithActiveScheduledEvents");
                m.checkNotNullParameter(map2, "channels");
                return new SecondChunk(map, list, set, set2, set3, map2, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SecondChunk)) {
                    return false;
                }
                SecondChunk secondChunk = (SecondChunk) obj;
                return m.areEqual(this.guildJoinRequests, secondChunk.guildJoinRequests) && m.areEqual(this.pendingGuilds, secondChunk.pendingGuilds) && m.areEqual(this.guildIds, secondChunk.guildIds) && m.areEqual(this.guildIdsWithActiveStageEvents, secondChunk.guildIdsWithActiveStageEvents) && m.areEqual(this.guildIdsWithActiveScheduledEvents, secondChunk.guildIdsWithActiveScheduledEvents) && m.areEqual(this.channels, secondChunk.channels) && this.showHubSparkle == secondChunk.showHubSparkle;
            }

            public final Map<Long, Channel> getChannels() {
                return this.channels;
            }

            public final Set<Long> getGuildIds() {
                return this.guildIds;
            }

            public final Set<Long> getGuildIdsWithActiveScheduledEvents() {
                return this.guildIdsWithActiveScheduledEvents;
            }

            public final Set<Long> getGuildIdsWithActiveStageEvents() {
                return this.guildIdsWithActiveStageEvents;
            }

            public final Map<Long, GuildJoinRequest> getGuildJoinRequests() {
                return this.guildJoinRequests;
            }

            public final List<Guild> getPendingGuilds() {
                return this.pendingGuilds;
            }

            public final boolean getShowHubSparkle() {
                return this.showHubSparkle;
            }

            public int hashCode() {
                Map<Long, GuildJoinRequest> map = this.guildJoinRequests;
                int i = 0;
                int hashCode = (map != null ? map.hashCode() : 0) * 31;
                List<Guild> list = this.pendingGuilds;
                int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
                Set<Long> set = this.guildIds;
                int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
                Set<Long> set2 = this.guildIdsWithActiveStageEvents;
                int hashCode4 = (hashCode3 + (set2 != null ? set2.hashCode() : 0)) * 31;
                Set<Long> set3 = this.guildIdsWithActiveScheduledEvents;
                int hashCode5 = (hashCode4 + (set3 != null ? set3.hashCode() : 0)) * 31;
                Map<Long, Channel> map2 = this.channels;
                if (map2 != null) {
                    i = map2.hashCode();
                }
                int i2 = (hashCode5 + i) * 31;
                boolean z2 = this.showHubSparkle;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("SecondChunk(guildJoinRequests=");
                R.append(this.guildJoinRequests);
                R.append(", pendingGuilds=");
                R.append(this.pendingGuilds);
                R.append(", guildIds=");
                R.append(this.guildIds);
                R.append(", guildIdsWithActiveStageEvents=");
                R.append(this.guildIdsWithActiveStageEvents);
                R.append(", guildIdsWithActiveScheduledEvents=");
                R.append(this.guildIdsWithActiveScheduledEvents);
                R.append(", channels=");
                R.append(this.channels);
                R.append(", showHubSparkle=");
                return a.M(R, this.showHubSparkle, ")");
            }
        }

        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(final Clock clock) {
            WidgetGuildsListViewModel$Companion$observeStores$1 widgetGuildsListViewModel$Companion$observeStores$1 = WidgetGuildsListViewModel$Companion$observeStores$1.INSTANCE;
            WidgetGuildsListViewModel$Companion$observeStores$2 widgetGuildsListViewModel$Companion$observeStores$2 = WidgetGuildsListViewModel$Companion$observeStores$2.INSTANCE;
            Observable<Chunk> invoke = widgetGuildsListViewModel$Companion$observeStores$1.invoke();
            Observable<SecondChunk> invoke2 = widgetGuildsListViewModel$Companion$observeStores$2.invoke();
            StoreStream.Companion companion = StoreStream.Companion;
            Observable c = Observable.c(invoke, invoke2, companion.getLurking().getLurkingGuildIds(), companion.getExpandedGuildFolders().observeOpenFolderIds(), StoreUser.observeMe$default(companion.getUsers(), false, 1, null).q().F(new j0.k.b<MeUser, Boolean>() { // from class: com.discord.widgets.guilds.list.WidgetGuildsListViewModel$Companion$observeStores$3
                public final Boolean call(MeUser meUser) {
                    UserUtils userUtils = UserUtils.INSTANCE;
                    m.checkNotNullExpressionValue(meUser, "meUser");
                    return Boolean.valueOf(userUtils.getAgeMs(meUser, Clock.this) < 1209600000);
                }
            }), new StreamContextService(null, null, null, null, null, null, null, null, 255, null).getForAllStreamingUsers(), companion.getPermissions().observePermissionsForAllChannels(), companion.getNavigation().observeLeftPanelState().F(WidgetGuildsListViewModel$Companion$observeStores$4.INSTANCE), companion.getTabsNavigation().observeSelectedTab().F(WidgetGuildsListViewModel$Companion$observeStores$5.INSTANCE), WidgetGuildsListViewModel$Companion$observeStores$6.INSTANCE);
            m.checkNotNullExpressionValue(c, "Observable.combineLatest…Sparkle\n        )\n      }");
            return ObservableExtensionsKt.leadingEdgeThrottle(c, 100L, TimeUnit.MILLISECONDS);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0007\u0004\u0005\u0006\u0007\b\t\nB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0007\u000b\f\r\u000e\u000f\u0010\u0011¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", "", HookHelper.constructorName, "()V", "AnnounceFolderToggleForAccessibility", "FocusFirstElement", "ShowChannelActions", "ShowCreateGuild", "ShowHelp", "ShowHubVerification", "ShowUnavailableGuilds", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowChannelActions;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowUnavailableGuilds;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowCreateGuild;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowHubVerification;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowHelp;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$FocusFirstElement;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", "Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;", "component1", "()Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;", "item", "copy", "(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;", "getItem", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AnnounceFolderToggleForAccessibility extends Event {
            private final GuildListItem.FolderItem item;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnnounceFolderToggleForAccessibility(GuildListItem.FolderItem folderItem) {
                super(null);
                m.checkNotNullParameter(folderItem, "item");
                this.item = folderItem;
            }

            public static /* synthetic */ AnnounceFolderToggleForAccessibility copy$default(AnnounceFolderToggleForAccessibility announceFolderToggleForAccessibility, GuildListItem.FolderItem folderItem, int i, Object obj) {
                if ((i & 1) != 0) {
                    folderItem = announceFolderToggleForAccessibility.item;
                }
                return announceFolderToggleForAccessibility.copy(folderItem);
            }

            public final GuildListItem.FolderItem component1() {
                return this.item;
            }

            public final AnnounceFolderToggleForAccessibility copy(GuildListItem.FolderItem folderItem) {
                m.checkNotNullParameter(folderItem, "item");
                return new AnnounceFolderToggleForAccessibility(folderItem);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof AnnounceFolderToggleForAccessibility) && m.areEqual(this.item, ((AnnounceFolderToggleForAccessibility) obj).item);
                }
                return true;
            }

            public final GuildListItem.FolderItem getItem() {
                return this.item;
            }

            public int hashCode() {
                GuildListItem.FolderItem folderItem = this.item;
                if (folderItem != null) {
                    return folderItem.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("AnnounceFolderToggleForAccessibility(item=");
                R.append(this.item);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$FocusFirstElement;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FocusFirstElement extends Event {
            public static final FocusFirstElement INSTANCE = new FocusFirstElement();

            private FocusFirstElement() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowChannelActions;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "channelId", "copy", "(J)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowChannelActions;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowChannelActions extends Event {
            private final long channelId;

            public ShowChannelActions(long j) {
                super(null);
                this.channelId = j;
            }

            public static /* synthetic */ ShowChannelActions copy$default(ShowChannelActions showChannelActions, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = showChannelActions.channelId;
                }
                return showChannelActions.copy(j);
            }

            public final long component1() {
                return this.channelId;
            }

            public final ShowChannelActions copy(long j) {
                return new ShowChannelActions(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowChannelActions) && this.channelId == ((ShowChannelActions) obj).channelId;
                }
                return true;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                return b.a(this.channelId);
            }

            public String toString() {
                return a.B(a.R("ShowChannelActions(channelId="), this.channelId, ")");
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowCreateGuild;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowCreateGuild extends Event {
            public static final ShowCreateGuild INSTANCE = new ShowCreateGuild();

            private ShowCreateGuild() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowHelp;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowHelp extends Event {
            public static final ShowHelp INSTANCE = new ShowHelp();

            private ShowHelp() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowHubVerification;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowHubVerification extends Event {
            public static final ShowHubVerification INSTANCE = new ShowHubVerification();

            private ShowHubVerification() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowUnavailableGuilds;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", "", "component1", "()I", "unavailableGuildCount", "copy", "(I)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowUnavailableGuilds;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getUnavailableGuildCount", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowUnavailableGuilds extends Event {
            private final int unavailableGuildCount;

            public ShowUnavailableGuilds(int i) {
                super(null);
                this.unavailableGuildCount = i;
            }

            public static /* synthetic */ ShowUnavailableGuilds copy$default(ShowUnavailableGuilds showUnavailableGuilds, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showUnavailableGuilds.unavailableGuildCount;
                }
                return showUnavailableGuilds.copy(i);
            }

            public final int component1() {
                return this.unavailableGuildCount;
            }

            public final ShowUnavailableGuilds copy(int i) {
                return new ShowUnavailableGuilds(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowUnavailableGuilds) && this.unavailableGuildCount == ((ShowUnavailableGuilds) obj).unavailableGuildCount;
                }
                return true;
            }

            public final int getUnavailableGuildCount() {
                return this.unavailableGuildCount;
            }

            public int hashCode() {
                return this.unavailableGuildCount;
            }

            public String toString() {
                return a.A(a.R("ShowUnavailableGuilds(unavailableGuildCount="), this.unavailableGuildCount, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0010\u000e\n\u0002\b#\b\u0086\b\u0018\u00002\u00020\u0001B£\u0003\u0012\n\u0010/\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u00100\u001a\u00060\u0002j\u0002`\u0006\u0012\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\b\u0012\u0010\u00102\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0016\u00103\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\b\u0012 \u00104\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\b\u0012\u0010\u00105\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0016\u00106\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b\u0012\f\u00107\u001a\b\u0012\u0004\u0012\u00020\u00160\u0011\u0012\u0016\u00108\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00190\b\u0012\f\u00109\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0011\u0012\u0010\u0010:\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0010\u0010;\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0010\u0010<\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0010\u0010=\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0016\u0010>\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b\u0012\u0010\u0010?\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\"0\f\u0012\u0006\u0010@\u001a\u00020$\u0012\u0016\u0010A\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`'\u0012\u0004\u0012\u00020(0\b\u0012\u001a\u0010B\u001a\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\b\u0012\u00060\u0002j\u0002`*0\b\u0012\u0006\u0010C\u001a\u00020$\u0012\u0006\u0010D\u001a\u00020$\u0012\u0006\u0010E\u001a\u00020$¢\u0006\u0004\bi\u0010jJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u001c\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\r\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ \u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\bHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000bJ*\u0010\u0012\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000bJ\u001a\u0010\u0013\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000eJ \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\bHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u000bJ\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0011HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J \u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00190\bHÆ\u0003¢\u0006\u0004\b\u001a\u0010\u000bJ\u0016\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0011HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0018J\u001a\u0010\u001d\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u000eJ\u001a\u0010\u001e\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u000eJ\u001a\u0010\u001f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\u001f\u0010\u000eJ\u001a\u0010 \u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b \u0010\u000eJ \u0010!\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\bHÆ\u0003¢\u0006\u0004\b!\u0010\u000bJ\u001a\u0010#\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\"0\fHÆ\u0003¢\u0006\u0004\b#\u0010\u000eJ\u0010\u0010%\u001a\u00020$HÆ\u0003¢\u0006\u0004\b%\u0010&J \u0010)\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`'\u0012\u0004\u0012\u00020(0\bHÆ\u0003¢\u0006\u0004\b)\u0010\u000bJ$\u0010+\u001a\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\b\u0012\u00060\u0002j\u0002`*0\bHÆ\u0003¢\u0006\u0004\b+\u0010\u000bJ\u0010\u0010,\u001a\u00020$HÆ\u0003¢\u0006\u0004\b,\u0010&J\u0010\u0010-\u001a\u00020$HÆ\u0003¢\u0006\u0004\b-\u0010&J\u0010\u0010.\u001a\u00020$HÆ\u0003¢\u0006\u0004\b.\u0010&JÚ\u0003\u0010F\u001a\u00020\u00002\f\b\u0002\u0010/\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u00100\u001a\u00060\u0002j\u0002`\u00062\u0014\b\u0002\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\b2\u0012\b\u0002\u00102\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0018\b\u0002\u00103\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\b2\"\b\u0002\u00104\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\b2\u0012\b\u0002\u00105\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0018\b\u0002\u00106\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b2\u000e\b\u0002\u00107\u001a\b\u0012\u0004\u0012\u00020\u00160\u00112\u0018\b\u0002\u00108\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00190\b2\u000e\b\u0002\u00109\u001a\b\u0012\u0004\u0012\u00020\u001b0\u00112\u0012\b\u0002\u0010:\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0012\b\u0002\u0010;\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0012\b\u0002\u0010<\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0012\b\u0002\u0010=\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\u0018\b\u0002\u0010>\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b2\u0012\b\u0002\u0010?\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\"0\f2\b\b\u0002\u0010@\u001a\u00020$2\u0018\b\u0002\u0010A\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`'\u0012\u0004\u0012\u00020(0\b2\u001c\b\u0002\u0010B\u001a\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\b\u0012\u00060\u0002j\u0002`*0\b2\b\b\u0002\u0010C\u001a\u00020$2\b\b\u0002\u0010D\u001a\u00020$2\b\b\u0002\u0010E\u001a\u00020$HÆ\u0001¢\u0006\u0004\bF\u0010GJ\u0010\u0010I\u001a\u00020HHÖ\u0001¢\u0006\u0004\bI\u0010JJ\u0010\u0010K\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\bK\u0010LJ\u001a\u0010N\u001a\u00020$2\b\u0010M\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\bN\u0010OR\u0019\u0010@\u001a\u00020$8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010P\u001a\u0004\b@\u0010&R\u0019\u0010C\u001a\u00020$8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010P\u001a\u0004\bC\u0010&R%\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010Q\u001a\u0004\bR\u0010\u000bR)\u00108\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00190\b8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010Q\u001a\u0004\bS\u0010\u000bR#\u00105\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010T\u001a\u0004\bU\u0010\u000eR\u001d\u00100\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010V\u001a\u0004\bW\u0010\u0005R#\u0010?\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\"0\f8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010T\u001a\u0004\bX\u0010\u000eR)\u00103\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\b8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010Q\u001a\u0004\bY\u0010\u000bR3\u00104\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00110\b8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010Q\u001a\u0004\bZ\u0010\u000bR)\u0010A\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`'\u0012\u0004\u0012\u00020(0\b8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010Q\u001a\u0004\b[\u0010\u000bR\u001f\u00109\u001a\b\u0012\u0004\u0012\u00020\u001b0\u00118\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\\\u001a\u0004\b]\u0010\u0018R\u001d\u0010/\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010V\u001a\u0004\b^\u0010\u0005R#\u00102\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010T\u001a\u0004\b_\u0010\u000eR)\u00106\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010Q\u001a\u0004\b`\u0010\u000bR#\u0010=\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010T\u001a\u0004\ba\u0010\u000eR-\u0010B\u001a\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\b\u0012\u00060\u0002j\u0002`*0\b8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010Q\u001a\u0004\bb\u0010\u000bR#\u0010;\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010T\u001a\u0004\bc\u0010\u000eR\u0019\u0010E\u001a\u00020$8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010P\u001a\u0004\bd\u0010&R#\u0010<\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010T\u001a\u0004\be\u0010\u000eR\u0019\u0010D\u001a\u00020$8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010P\u001a\u0004\bD\u0010&R\u001f\u00107\u001a\b\u0012\u0004\u0012\u00020\u00160\u00118\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\\\u001a\u0004\bf\u0010\u0018R#\u0010:\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010T\u001a\u0004\bg\u0010\u000eR)\u0010>\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00140\b8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010Q\u001a\u0004\bh\u0010\u000b¨\u0006k"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "", "Lcom/discord/models/domain/ModelNotificationSettings;", "component3", "()Ljava/util/Map;", "", "component4", "()Ljava/util/Set;", "", "component5", "", "component6", "component7", "Lcom/discord/api/channel/Channel;", "component8", "Lcom/discord/stores/StoreGuildsSorted$Entry;", "component9", "()Ljava/util/List;", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "component10", "Lcom/discord/models/guild/Guild;", "component11", "component12", "component13", "component14", "component15", "component16", "Lcom/discord/primitives/FolderId;", "component17", "", "component18", "()Z", "Lcom/discord/primitives/UserId;", "Lcom/discord/utilities/streams/StreamContext;", "component19", "Lcom/discord/api/permission/PermissionBit;", "component20", "component21", "component22", "component23", "selectedGuildId", "selectedVoiceChannelId", "guildSettings", "unreadGuildIds", "mentionCounts", "channelIds", "unavailableGuilds", "privateChannels", "sortedGuilds", "guildJoinRequests", "pendingGuilds", "guildIds", "lurkingGuildIds", "guildIdsWithActiveStageEvents", "guildIdsWithActiveScheduledEvents", "channels", "openFolderIds", "isNewUser", "allApplicationStreamContexts", "allChannelPermissions", "isLeftPanelOpened", "isOnHomeTab", "showHubSparkle", "copy", "(JJLjava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;ZLjava/util/Map;Ljava/util/Map;ZZZ)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/Map;", "getGuildSettings", "getGuildJoinRequests", "Ljava/util/Set;", "getUnavailableGuilds", "J", "getSelectedVoiceChannelId", "getOpenFolderIds", "getMentionCounts", "getChannelIds", "getAllApplicationStreamContexts", "Ljava/util/List;", "getPendingGuilds", "getSelectedGuildId", "getUnreadGuildIds", "getPrivateChannels", "getGuildIdsWithActiveScheduledEvents", "getAllChannelPermissions", "getLurkingGuildIds", "getShowHubSparkle", "getGuildIdsWithActiveStageEvents", "getSortedGuilds", "getGuildIds", "getChannels", HookHelper.constructorName, "(JJLjava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;ZLjava/util/Map;Ljava/util/Map;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, StreamContext> allApplicationStreamContexts;
        private final Map<Long, Long> allChannelPermissions;
        private final Map<Long, List<Long>> channelIds;
        private final Map<Long, Channel> channels;
        private final Set<Long> guildIds;
        private final Set<Long> guildIdsWithActiveScheduledEvents;
        private final Set<Long> guildIdsWithActiveStageEvents;
        private final Map<Long, GuildJoinRequest> guildJoinRequests;
        private final Map<Long, ModelNotificationSettings> guildSettings;
        private final boolean isLeftPanelOpened;
        private final boolean isNewUser;
        private final boolean isOnHomeTab;
        private final Set<Long> lurkingGuildIds;
        private final Map<Long, Integer> mentionCounts;
        private final Set<Long> openFolderIds;
        private final List<Guild> pendingGuilds;
        private final Map<Long, Channel> privateChannels;
        private final long selectedGuildId;
        private final long selectedVoiceChannelId;
        private final boolean showHubSparkle;
        private final List<StoreGuildsSorted.Entry> sortedGuilds;
        private final Set<Long> unavailableGuilds;
        private final Set<Long> unreadGuildIds;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(long j, long j2, Map<Long, ? extends ModelNotificationSettings> map, Set<Long> set, Map<Long, Integer> map2, Map<Long, ? extends List<Long>> map3, Set<Long> set2, Map<Long, Channel> map4, List<? extends StoreGuildsSorted.Entry> list, Map<Long, GuildJoinRequest> map5, List<Guild> list2, Set<Long> set3, Set<Long> set4, Set<Long> set5, Set<Long> set6, Map<Long, Channel> map6, Set<Long> set7, boolean z2, Map<Long, StreamContext> map7, Map<Long, Long> map8, boolean z3, boolean z4, boolean z5) {
            m.checkNotNullParameter(map, "guildSettings");
            m.checkNotNullParameter(set, "unreadGuildIds");
            m.checkNotNullParameter(map2, "mentionCounts");
            m.checkNotNullParameter(map3, "channelIds");
            m.checkNotNullParameter(set2, "unavailableGuilds");
            m.checkNotNullParameter(map4, "privateChannels");
            m.checkNotNullParameter(list, "sortedGuilds");
            m.checkNotNullParameter(map5, "guildJoinRequests");
            m.checkNotNullParameter(list2, "pendingGuilds");
            m.checkNotNullParameter(set3, "guildIds");
            m.checkNotNullParameter(set4, "lurkingGuildIds");
            m.checkNotNullParameter(set5, "guildIdsWithActiveStageEvents");
            m.checkNotNullParameter(set6, "guildIdsWithActiveScheduledEvents");
            m.checkNotNullParameter(map6, "channels");
            m.checkNotNullParameter(set7, "openFolderIds");
            m.checkNotNullParameter(map7, "allApplicationStreamContexts");
            m.checkNotNullParameter(map8, "allChannelPermissions");
            this.selectedGuildId = j;
            this.selectedVoiceChannelId = j2;
            this.guildSettings = map;
            this.unreadGuildIds = set;
            this.mentionCounts = map2;
            this.channelIds = map3;
            this.unavailableGuilds = set2;
            this.privateChannels = map4;
            this.sortedGuilds = list;
            this.guildJoinRequests = map5;
            this.pendingGuilds = list2;
            this.guildIds = set3;
            this.lurkingGuildIds = set4;
            this.guildIdsWithActiveStageEvents = set5;
            this.guildIdsWithActiveScheduledEvents = set6;
            this.channels = map6;
            this.openFolderIds = set7;
            this.isNewUser = z2;
            this.allApplicationStreamContexts = map7;
            this.allChannelPermissions = map8;
            this.isLeftPanelOpened = z3;
            this.isOnHomeTab = z4;
            this.showHubSparkle = z5;
        }

        public final long component1() {
            return this.selectedGuildId;
        }

        public final Map<Long, GuildJoinRequest> component10() {
            return this.guildJoinRequests;
        }

        public final List<Guild> component11() {
            return this.pendingGuilds;
        }

        public final Set<Long> component12() {
            return this.guildIds;
        }

        public final Set<Long> component13() {
            return this.lurkingGuildIds;
        }

        public final Set<Long> component14() {
            return this.guildIdsWithActiveStageEvents;
        }

        public final Set<Long> component15() {
            return this.guildIdsWithActiveScheduledEvents;
        }

        public final Map<Long, Channel> component16() {
            return this.channels;
        }

        public final Set<Long> component17() {
            return this.openFolderIds;
        }

        public final boolean component18() {
            return this.isNewUser;
        }

        public final Map<Long, StreamContext> component19() {
            return this.allApplicationStreamContexts;
        }

        public final long component2() {
            return this.selectedVoiceChannelId;
        }

        public final Map<Long, Long> component20() {
            return this.allChannelPermissions;
        }

        public final boolean component21() {
            return this.isLeftPanelOpened;
        }

        public final boolean component22() {
            return this.isOnHomeTab;
        }

        public final boolean component23() {
            return this.showHubSparkle;
        }

        public final Map<Long, ModelNotificationSettings> component3() {
            return this.guildSettings;
        }

        public final Set<Long> component4() {
            return this.unreadGuildIds;
        }

        public final Map<Long, Integer> component5() {
            return this.mentionCounts;
        }

        public final Map<Long, List<Long>> component6() {
            return this.channelIds;
        }

        public final Set<Long> component7() {
            return this.unavailableGuilds;
        }

        public final Map<Long, Channel> component8() {
            return this.privateChannels;
        }

        public final List<StoreGuildsSorted.Entry> component9() {
            return this.sortedGuilds;
        }

        public final StoreState copy(long j, long j2, Map<Long, ? extends ModelNotificationSettings> map, Set<Long> set, Map<Long, Integer> map2, Map<Long, ? extends List<Long>> map3, Set<Long> set2, Map<Long, Channel> map4, List<? extends StoreGuildsSorted.Entry> list, Map<Long, GuildJoinRequest> map5, List<Guild> list2, Set<Long> set3, Set<Long> set4, Set<Long> set5, Set<Long> set6, Map<Long, Channel> map6, Set<Long> set7, boolean z2, Map<Long, StreamContext> map7, Map<Long, Long> map8, boolean z3, boolean z4, boolean z5) {
            m.checkNotNullParameter(map, "guildSettings");
            m.checkNotNullParameter(set, "unreadGuildIds");
            m.checkNotNullParameter(map2, "mentionCounts");
            m.checkNotNullParameter(map3, "channelIds");
            m.checkNotNullParameter(set2, "unavailableGuilds");
            m.checkNotNullParameter(map4, "privateChannels");
            m.checkNotNullParameter(list, "sortedGuilds");
            m.checkNotNullParameter(map5, "guildJoinRequests");
            m.checkNotNullParameter(list2, "pendingGuilds");
            m.checkNotNullParameter(set3, "guildIds");
            m.checkNotNullParameter(set4, "lurkingGuildIds");
            m.checkNotNullParameter(set5, "guildIdsWithActiveStageEvents");
            m.checkNotNullParameter(set6, "guildIdsWithActiveScheduledEvents");
            m.checkNotNullParameter(map6, "channels");
            m.checkNotNullParameter(set7, "openFolderIds");
            m.checkNotNullParameter(map7, "allApplicationStreamContexts");
            m.checkNotNullParameter(map8, "allChannelPermissions");
            return new StoreState(j, j2, map, set, map2, map3, set2, map4, list, map5, list2, set3, set4, set5, set6, map6, set7, z2, map7, map8, z3, z4, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return this.selectedGuildId == storeState.selectedGuildId && this.selectedVoiceChannelId == storeState.selectedVoiceChannelId && m.areEqual(this.guildSettings, storeState.guildSettings) && m.areEqual(this.unreadGuildIds, storeState.unreadGuildIds) && m.areEqual(this.mentionCounts, storeState.mentionCounts) && m.areEqual(this.channelIds, storeState.channelIds) && m.areEqual(this.unavailableGuilds, storeState.unavailableGuilds) && m.areEqual(this.privateChannels, storeState.privateChannels) && m.areEqual(this.sortedGuilds, storeState.sortedGuilds) && m.areEqual(this.guildJoinRequests, storeState.guildJoinRequests) && m.areEqual(this.pendingGuilds, storeState.pendingGuilds) && m.areEqual(this.guildIds, storeState.guildIds) && m.areEqual(this.lurkingGuildIds, storeState.lurkingGuildIds) && m.areEqual(this.guildIdsWithActiveStageEvents, storeState.guildIdsWithActiveStageEvents) && m.areEqual(this.guildIdsWithActiveScheduledEvents, storeState.guildIdsWithActiveScheduledEvents) && m.areEqual(this.channels, storeState.channels) && m.areEqual(this.openFolderIds, storeState.openFolderIds) && this.isNewUser == storeState.isNewUser && m.areEqual(this.allApplicationStreamContexts, storeState.allApplicationStreamContexts) && m.areEqual(this.allChannelPermissions, storeState.allChannelPermissions) && this.isLeftPanelOpened == storeState.isLeftPanelOpened && this.isOnHomeTab == storeState.isOnHomeTab && this.showHubSparkle == storeState.showHubSparkle;
        }

        public final Map<Long, StreamContext> getAllApplicationStreamContexts() {
            return this.allApplicationStreamContexts;
        }

        public final Map<Long, Long> getAllChannelPermissions() {
            return this.allChannelPermissions;
        }

        public final Map<Long, List<Long>> getChannelIds() {
            return this.channelIds;
        }

        public final Map<Long, Channel> getChannels() {
            return this.channels;
        }

        public final Set<Long> getGuildIds() {
            return this.guildIds;
        }

        public final Set<Long> getGuildIdsWithActiveScheduledEvents() {
            return this.guildIdsWithActiveScheduledEvents;
        }

        public final Set<Long> getGuildIdsWithActiveStageEvents() {
            return this.guildIdsWithActiveStageEvents;
        }

        public final Map<Long, GuildJoinRequest> getGuildJoinRequests() {
            return this.guildJoinRequests;
        }

        public final Map<Long, ModelNotificationSettings> getGuildSettings() {
            return this.guildSettings;
        }

        public final Set<Long> getLurkingGuildIds() {
            return this.lurkingGuildIds;
        }

        public final Map<Long, Integer> getMentionCounts() {
            return this.mentionCounts;
        }

        public final Set<Long> getOpenFolderIds() {
            return this.openFolderIds;
        }

        public final List<Guild> getPendingGuilds() {
            return this.pendingGuilds;
        }

        public final Map<Long, Channel> getPrivateChannels() {
            return this.privateChannels;
        }

        public final long getSelectedGuildId() {
            return this.selectedGuildId;
        }

        public final long getSelectedVoiceChannelId() {
            return this.selectedVoiceChannelId;
        }

        public final boolean getShowHubSparkle() {
            return this.showHubSparkle;
        }

        public final List<StoreGuildsSorted.Entry> getSortedGuilds() {
            return this.sortedGuilds;
        }

        public final Set<Long> getUnavailableGuilds() {
            return this.unavailableGuilds;
        }

        public final Set<Long> getUnreadGuildIds() {
            return this.unreadGuildIds;
        }

        public int hashCode() {
            int a = (b.a(this.selectedVoiceChannelId) + (b.a(this.selectedGuildId) * 31)) * 31;
            Map<Long, ModelNotificationSettings> map = this.guildSettings;
            int i = 0;
            int hashCode = (a + (map != null ? map.hashCode() : 0)) * 31;
            Set<Long> set = this.unreadGuildIds;
            int hashCode2 = (hashCode + (set != null ? set.hashCode() : 0)) * 31;
            Map<Long, Integer> map2 = this.mentionCounts;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, List<Long>> map3 = this.channelIds;
            int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Set<Long> set2 = this.unavailableGuilds;
            int hashCode5 = (hashCode4 + (set2 != null ? set2.hashCode() : 0)) * 31;
            Map<Long, Channel> map4 = this.privateChannels;
            int hashCode6 = (hashCode5 + (map4 != null ? map4.hashCode() : 0)) * 31;
            List<StoreGuildsSorted.Entry> list = this.sortedGuilds;
            int hashCode7 = (hashCode6 + (list != null ? list.hashCode() : 0)) * 31;
            Map<Long, GuildJoinRequest> map5 = this.guildJoinRequests;
            int hashCode8 = (hashCode7 + (map5 != null ? map5.hashCode() : 0)) * 31;
            List<Guild> list2 = this.pendingGuilds;
            int hashCode9 = (hashCode8 + (list2 != null ? list2.hashCode() : 0)) * 31;
            Set<Long> set3 = this.guildIds;
            int hashCode10 = (hashCode9 + (set3 != null ? set3.hashCode() : 0)) * 31;
            Set<Long> set4 = this.lurkingGuildIds;
            int hashCode11 = (hashCode10 + (set4 != null ? set4.hashCode() : 0)) * 31;
            Set<Long> set5 = this.guildIdsWithActiveStageEvents;
            int hashCode12 = (hashCode11 + (set5 != null ? set5.hashCode() : 0)) * 31;
            Set<Long> set6 = this.guildIdsWithActiveScheduledEvents;
            int hashCode13 = (hashCode12 + (set6 != null ? set6.hashCode() : 0)) * 31;
            Map<Long, Channel> map6 = this.channels;
            int hashCode14 = (hashCode13 + (map6 != null ? map6.hashCode() : 0)) * 31;
            Set<Long> set7 = this.openFolderIds;
            int hashCode15 = (hashCode14 + (set7 != null ? set7.hashCode() : 0)) * 31;
            boolean z2 = this.isNewUser;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode15 + i3) * 31;
            Map<Long, StreamContext> map7 = this.allApplicationStreamContexts;
            int hashCode16 = (i5 + (map7 != null ? map7.hashCode() : 0)) * 31;
            Map<Long, Long> map8 = this.allChannelPermissions;
            if (map8 != null) {
                i = map8.hashCode();
            }
            int i6 = (hashCode16 + i) * 31;
            boolean z3 = this.isLeftPanelOpened;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.isOnHomeTab;
            if (z4) {
                z4 = true;
            }
            int i10 = z4 ? 1 : 0;
            int i11 = z4 ? 1 : 0;
            int i12 = (i9 + i10) * 31;
            boolean z5 = this.showHubSparkle;
            if (!z5) {
                i2 = z5 ? 1 : 0;
            }
            return i12 + i2;
        }

        public final boolean isLeftPanelOpened() {
            return this.isLeftPanelOpened;
        }

        public final boolean isNewUser() {
            return this.isNewUser;
        }

        public final boolean isOnHomeTab() {
            return this.isOnHomeTab;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(selectedGuildId=");
            R.append(this.selectedGuildId);
            R.append(", selectedVoiceChannelId=");
            R.append(this.selectedVoiceChannelId);
            R.append(", guildSettings=");
            R.append(this.guildSettings);
            R.append(", unreadGuildIds=");
            R.append(this.unreadGuildIds);
            R.append(", mentionCounts=");
            R.append(this.mentionCounts);
            R.append(", channelIds=");
            R.append(this.channelIds);
            R.append(", unavailableGuilds=");
            R.append(this.unavailableGuilds);
            R.append(", privateChannels=");
            R.append(this.privateChannels);
            R.append(", sortedGuilds=");
            R.append(this.sortedGuilds);
            R.append(", guildJoinRequests=");
            R.append(this.guildJoinRequests);
            R.append(", pendingGuilds=");
            R.append(this.pendingGuilds);
            R.append(", guildIds=");
            R.append(this.guildIds);
            R.append(", lurkingGuildIds=");
            R.append(this.lurkingGuildIds);
            R.append(", guildIdsWithActiveStageEvents=");
            R.append(this.guildIdsWithActiveStageEvents);
            R.append(", guildIdsWithActiveScheduledEvents=");
            R.append(this.guildIdsWithActiveScheduledEvents);
            R.append(", channels=");
            R.append(this.channels);
            R.append(", openFolderIds=");
            R.append(this.openFolderIds);
            R.append(", isNewUser=");
            R.append(this.isNewUser);
            R.append(", allApplicationStreamContexts=");
            R.append(this.allApplicationStreamContexts);
            R.append(", allChannelPermissions=");
            R.append(this.allChannelPermissions);
            R.append(", isLeftPanelOpened=");
            R.append(this.isLeftPanelOpened);
            R.append(", isOnHomeTab=");
            R.append(this.isOnHomeTab);
            R.append(", showHubSparkle=");
            return a.M(R, this.showHubSparkle, ")");
        }
    }

    /* compiled from: WidgetGuildsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0006\u0012\u0006\u0010\f\u001a\u00020\u0006¢\u0006\u0004\b\u001e\u0010\u001fJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ4\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00062\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\bR\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001d\u0010\b¨\u0006 "}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;", "", "Lcom/discord/widgets/guilds/list/GuildListItem;", "component1", "()Ljava/util/List;", "", "component2", "()Z", "component3", "items", "hasChannels", "wasDragResult", "copy", "(Ljava/util/List;ZZ)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", "Z", "getHasChannels", "getWasDragResult", HookHelper.constructorName, "(Ljava/util/List;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean hasChannels;
            private final List<GuildListItem> items;
            private final boolean wasDragResult;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(List<? extends GuildListItem> list, boolean z2, boolean z3) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.items = list;
                this.hasChannels = z2;
                this.wasDragResult = z3;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.items;
                }
                if ((i & 2) != 0) {
                    z2 = loaded.hasChannels;
                }
                if ((i & 4) != 0) {
                    z3 = loaded.wasDragResult;
                }
                return loaded.copy(list, z2, z3);
            }

            public final List<GuildListItem> component1() {
                return this.items;
            }

            public final boolean component2() {
                return this.hasChannels;
            }

            public final boolean component3() {
                return this.wasDragResult;
            }

            public final Loaded copy(List<? extends GuildListItem> list, boolean z2, boolean z3) {
                m.checkNotNullParameter(list, "items");
                return new Loaded(list, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.items, loaded.items) && this.hasChannels == loaded.hasChannels && this.wasDragResult == loaded.wasDragResult;
            }

            public final boolean getHasChannels() {
                return this.hasChannels;
            }

            public final List<GuildListItem> getItems() {
                return this.items;
            }

            public final boolean getWasDragResult() {
                return this.wasDragResult;
            }

            public int hashCode() {
                List<GuildListItem> list = this.items;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                boolean z2 = this.hasChannels;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode + i2) * 31;
                boolean z3 = this.wasDragResult;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                return i4 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(items=");
                R.append(this.items);
                R.append(", hasChannels=");
                R.append(this.hasChannels);
                R.append(", wasDragResult=");
                return a.M(R, this.wasDragResult, ")");
            }
        }

        /* compiled from: WidgetGuildsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildsListViewModel() {
        this(null, null, 3, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetGuildsListViewModel(com.discord.utilities.time.Clock r1, rx.Observable r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
        /*
            r0 = this;
            r4 = r3 & 1
            if (r4 == 0) goto L8
            com.discord.utilities.time.Clock r1 = com.discord.utilities.time.ClockFactory.get()
        L8:
            r3 = r3 & 2
            if (r3 == 0) goto L12
            com.discord.widgets.guilds.list.WidgetGuildsListViewModel$Companion r2 = com.discord.widgets.guilds.list.WidgetGuildsListViewModel.Companion
            rx.Observable r2 = com.discord.widgets.guilds.list.WidgetGuildsListViewModel.Companion.access$observeStores(r2, r1)
        L12:
            r0.<init>(r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.list.WidgetGuildsListViewModel.<init>(com.discord.utilities.time.Clock, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final Sequence<GuildListItem.PrivateChannelItem> createDirectMessageItems(Map<Long, Channel> map, Map<Long, Integer> map2, ModelNotificationSettings modelNotificationSettings) {
        return q.map(q.sortedWith(q.filter(q.filterNotNull(u.asSequence(map.values())), new WidgetGuildsListViewModel$createDirectMessageItems$1(modelNotificationSettings, map2)), ChannelUtils.h(Channel.Companion)), new WidgetGuildsListViewModel$createDirectMessageItems$2(map2));
    }

    private final GuildListItem.GuildItem createGuildItem(Guild guild, long j, long j2, Set<Long> set, Map<Long, ? extends ModelNotificationSettings> map, int i, Map<Long, ? extends List<Long>> map2, Map<Long, Channel> map3, Set<Long> set2, Set<Long> set3, Set<Long> set4, Long l, Boolean bool, Map<Long, StreamContext> map4, Map<Long, Long> map5, ApplicationStatus applicationStatus, boolean z2) {
        boolean z3;
        boolean z4;
        long id2 = guild.getId();
        List<Long> list = map2.get(Long.valueOf(id2));
        ModelNotificationSettings modelNotificationSettings = map.get(Long.valueOf(id2));
        boolean contains = (modelNotificationSettings == null || !modelNotificationSettings.isMuted()) ? set.contains(Long.valueOf(id2)) : false;
        boolean z5 = id2 == j;
        if (j2 > 0 && list != null && !list.isEmpty()) {
            for (Number number : list) {
                if (j2 == number.longValue()) {
                    z4 = true;
                    continue;
                } else {
                    z4 = false;
                    continue;
                }
                if (z4) {
                    z3 = true;
                    break;
                }
            }
        }
        z3 = false;
        boolean shouldDisplayVideoIconOnGuild = shouldDisplayVideoIconOnGuild(id2, modelNotificationSettings, map4, map5);
        boolean contains2 = set2.contains(Long.valueOf(guild.getId()));
        boolean z6 = shouldShowUnread(guild) && contains;
        boolean contains3 = set3.contains(Long.valueOf(guild.getId()));
        Channel channel = map3.get(Long.valueOf(j2));
        return new GuildListItem.GuildItem(guild, i, contains2, z6, z5, l, z3, shouldDisplayVideoIconOnGuild, false, bool, applicationStatus, z2, contains3, channel != null && ChannelUtils.z(channel), set4.contains(Long.valueOf(guild.getId())), 256, null);
    }

    public static /* synthetic */ GuildListItem.GuildItem createGuildItem$default(WidgetGuildsListViewModel widgetGuildsListViewModel, Guild guild, long j, long j2, Set set, Map map, int i, Map map2, Map map3, Set set2, Set set3, Set set4, Long l, Boolean bool, Map map4, Map map5, ApplicationStatus applicationStatus, boolean z2, int i2, Object obj) {
        return widgetGuildsListViewModel.createGuildItem(guild, j, j2, set, map, i, map2, map3, set2, set3, set4, l, bool, map4, map5, applicationStatus, (i2 & 65536) != 0 ? false : z2);
    }

    private final List<GuildListItem> createPendingGuildsFolder(List<Guild> list, Map<Long, GuildJoinRequest> map, Set<Long> set, long j, long j2, Set<Long> set2, Map<Long, ? extends ModelNotificationSettings> map2, Map<Long, ? extends List<Long>> map3, Map<Long, Channel> map4, Set<Long> set3, Set<Long> set4, Set<Long> set5, Map<Long, StreamContext> map5, Map<Long, Long> map6) {
        ArrayList arrayList = new ArrayList();
        GuildListItem.FolderItem folderItem = new GuildListItem.FolderItem(-7L, null, null, set.contains(-7L), list, false, false, false, 0, false, false);
        arrayList.add(folderItem);
        if (folderItem.isOpen()) {
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
            int i = 0;
            for (Object obj : list) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                Guild guild = (Guild) obj;
                Long valueOf = Long.valueOf(folderItem.getFolderId());
                Boolean valueOf2 = Boolean.valueOf(n.getLastIndex(list) == i);
                GuildJoinRequest guildJoinRequest = (GuildJoinRequest) a.d(guild, map);
                arrayList2.add(createGuildItem(guild, j, j2, set2, map2, 0, map3, map4, set3, set4, set5, valueOf, valueOf2, map5, map6, guildJoinRequest != null ? guildJoinRequest.a() : null, true));
            }
            arrayList.addAll(arrayList2);
        }
        return arrayList;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x0247, code lost:
        if (com.discord.api.channel.ChannelUtils.z(r6) == true) goto L59;
     */
    /* JADX WARN: Removed duplicated region for block: B:158:0x0295 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:52:0x022e  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x024d  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0253  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0293  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.guilds.list.WidgetGuildsListViewModel.StoreState r54) {
        /*
            Method dump skipped, instructions count: 1305
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.list.WidgetGuildsListViewModel.handleStoreState(com.discord.widgets.guilds.list.WidgetGuildsListViewModel$StoreState):void");
    }

    private final void move(int i, int i2, Long l) {
        ArrayList<GuildListItem> arrayList;
        GuildListItem.GuildItem copy;
        int i3 = i2;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            List<GuildListItem> items = loaded.getItems();
            GuildListItem guildListItem = items.get(i);
            if (this.currentTargetOperation != null || (i > i3 || Math.abs(i - i3) >= 2) || ((guildListItem instanceof GuildListItem.GuildItem) && (m.areEqual(((GuildListItem.GuildItem) guildListItem).getFolderId(), l) ^ true))) {
                ArrayList<GuildListItem> arrayList2 = new ArrayList<>(items);
                untargetCurrentTarget(arrayList2);
                if (i < i3) {
                    i3--;
                }
                int i4 = i3;
                if (guildListItem instanceof GuildListItem.FolderItem) {
                    arrayList2.remove(i);
                    arrayList2.add(i4, guildListItem);
                } else if (guildListItem instanceof GuildListItem.GuildItem) {
                    arrayList2.remove(i);
                    GuildListItem.GuildItem guildItem = (GuildListItem.GuildItem) guildListItem;
                    copy = guildItem.copy((r32 & 1) != 0 ? guildItem.guild : null, (r32 & 2) != 0 ? guildItem.getMentionCount() : 0, (r32 & 4) != 0 ? guildItem.isLurkingGuild : false, (r32 & 8) != 0 ? guildItem.isUnread() : false, (r32 & 16) != 0 ? guildItem.isSelected : false, (r32 & 32) != 0 ? guildItem.folderId : l, (r32 & 64) != 0 ? guildItem.isConnectedToVoice : false, (r32 & 128) != 0 ? guildItem.hasOngoingApplicationStream : false, (r32 & 256) != 0 ? guildItem.isTargetedForFolderCreation : false, (r32 & 512) != 0 ? guildItem.isLastGuildInFolder : null, (r32 & 1024) != 0 ? guildItem.applicationStatus : null, (r32 & 2048) != 0 ? guildItem.isPendingGuild : false, (r32 & 4096) != 0 ? guildItem.hasActiveStageChannel : false, (r32 & 8192) != 0 ? guildItem.isConnectedToStageChannel : false, (r32 & 16384) != 0 ? guildItem.hasActiveScheduledEvent : false);
                    arrayList = arrayList2;
                    arrayList.add(i4, copy);
                    rebuildFolders(arrayList, u.toSet(n.listOfNotNull((Object[]) new Long[]{guildItem.getFolderId(), l})));
                    updateViewState(ViewState.Loaded.copy$default(loaded, arrayList, false, true, 2, null));
                }
                arrayList = arrayList2;
                updateViewState(ViewState.Loaded.copy$default(loaded, arrayList, false, true, 2, null));
            }
        }
    }

    private final void performTargetOperation(ArrayList<GuildListItem> arrayList, int i, int i2) {
        GuildListItem.FolderItem copy;
        GuildListItem guildListItem = arrayList.get(i);
        m.checkNotNullExpressionValue(guildListItem, "editingList[fromPosition]");
        GuildListItem guildListItem2 = guildListItem;
        GuildListItem guildListItem3 = arrayList.get(i2);
        m.checkNotNullExpressionValue(guildListItem3, "editingList[toPosition]");
        GuildListItem guildListItem4 = guildListItem3;
        boolean z2 = guildListItem2 instanceof GuildListItem.GuildItem;
        if (z2 && (guildListItem4 instanceof GuildListItem.GuildItem)) {
            GuildListItem.GuildItem guildItem = (GuildListItem.GuildItem) guildListItem4;
            GuildListItem.GuildItem guildItem2 = (GuildListItem.GuildItem) guildListItem2;
            arrayList.set(i2, new GuildListItem.FolderItem(c.k.nextLong(), null, null, false, n.listOf((Object[]) new Guild[]{guildItem.getGuild(), guildItem2.getGuild()}), guildItem.isSelected() || guildItem2.isSelected(), guildItem.isConnectedToVoice() || guildItem2.isConnectedToVoice(), guildItem.isConnectedToStageChannel() || guildItem2.isConnectedToStageChannel(), guildItem2.getMentionCount() + guildItem.getMentionCount(), guildItem.isUnread() || guildItem2.isUnread(), false));
            m.checkNotNullExpressionValue(arrayList.remove(i), "editingList.removeAt(fromPosition)");
        } else if (z2 && (guildListItem4 instanceof GuildListItem.FolderItem)) {
            GuildListItem.FolderItem folderItem = (GuildListItem.FolderItem) guildListItem4;
            GuildListItem.GuildItem guildItem3 = (GuildListItem.GuildItem) guildListItem2;
            copy = folderItem.copy((r26 & 1) != 0 ? folderItem.folderId : 0L, (r26 & 2) != 0 ? folderItem.color : null, (r26 & 4) != 0 ? folderItem.name : null, (r26 & 8) != 0 ? folderItem.isOpen : false, (r26 & 16) != 0 ? folderItem.guilds : u.plus((Collection<? extends Guild>) u.toMutableList((Collection) folderItem.getGuilds()), guildItem3.getGuild()), (r26 & 32) != 0 ? folderItem.isAnyGuildSelected : folderItem.isAnyGuildSelected() || guildItem3.isSelected(), (r26 & 64) != 0 ? folderItem.isAnyGuildConnectedToVoice : folderItem.isAnyGuildConnectedToVoice() || guildItem3.isConnectedToVoice(), (r26 & 128) != 0 ? folderItem.isAnyGuildConnectedToStageChannel : false, (r26 & 256) != 0 ? folderItem.getMentionCount() : 0, (r26 & 512) != 0 ? folderItem.isUnread() : folderItem.isUnread() || guildItem3.isUnread(), (r26 & 1024) != 0 ? folderItem.isTargetedForFolderAddition : false);
            arrayList.set(i2, copy);
            arrayList.remove(i);
        }
    }

    private final void rebuildFolders(ArrayList<GuildListItem> arrayList, Set<Long> set) {
        GuildListItem.FolderItem copy;
        if (!set.isEmpty()) {
            HashMap hashMap = new HashMap();
            ArrayList<GuildListItem.GuildItem> arrayList2 = new ArrayList();
            for (Object obj : arrayList) {
                if (obj instanceof GuildListItem.GuildItem) {
                    arrayList2.add(obj);
                }
            }
            for (GuildListItem.GuildItem guildItem : arrayList2) {
                Long folderId = guildItem.getFolderId();
                if (folderId != null) {
                    long longValue = folderId.longValue();
                    ArrayList arrayList3 = (ArrayList) hashMap.get(Long.valueOf(longValue));
                    if (arrayList3 == null) {
                        arrayList3 = new ArrayList();
                    }
                    arrayList3.add(guildItem.getGuild());
                    hashMap.put(Long.valueOf(longValue), arrayList3);
                }
            }
            for (Number number : set) {
                long longValue2 = number.longValue();
                Iterator<GuildListItem> it = arrayList.iterator();
                int i = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i = -1;
                        break;
                    }
                    GuildListItem next = it.next();
                    if ((next instanceof GuildListItem.FolderItem) && ((GuildListItem.FolderItem) next).getFolderId() == longValue2) {
                        break;
                    }
                    i++;
                }
                GuildListItem guildListItem = arrayList.get(i);
                Objects.requireNonNull(guildListItem, "null cannot be cast to non-null type com.discord.widgets.guilds.list.GuildListItem.FolderItem");
                GuildListItem.FolderItem folderItem = (GuildListItem.FolderItem) guildListItem;
                ArrayList arrayList4 = (ArrayList) hashMap.get(Long.valueOf(longValue2));
                if (arrayList4 == null) {
                    arrayList4 = new ArrayList();
                }
                copy = folderItem.copy((r26 & 1) != 0 ? folderItem.folderId : 0L, (r26 & 2) != 0 ? folderItem.color : null, (r26 & 4) != 0 ? folderItem.name : null, (r26 & 8) != 0 ? folderItem.isOpen : false, (r26 & 16) != 0 ? folderItem.guilds : arrayList4, (r26 & 32) != 0 ? folderItem.isAnyGuildSelected : false, (r26 & 64) != 0 ? folderItem.isAnyGuildConnectedToVoice : false, (r26 & 128) != 0 ? folderItem.isAnyGuildConnectedToStageChannel : false, (r26 & 256) != 0 ? folderItem.getMentionCount() : 0, (r26 & 512) != 0 ? folderItem.isUnread() : false, (r26 & 1024) != 0 ? folderItem.isTargetedForFolderAddition : false);
                arrayList.set(i, copy);
            }
        }
    }

    private final boolean shouldDisplayVideoIconOnGuild(long j, ModelNotificationSettings modelNotificationSettings, Map<Long, StreamContext> map, Map<Long, Long> map2) {
        boolean z2;
        if (map == null || map.isEmpty()) {
            return false;
        }
        if (modelNotificationSettings != null && modelNotificationSettings.isMuted()) {
            return false;
        }
        Collection<StreamContext> values = map.values();
        if (!(values instanceof Collection) || !values.isEmpty()) {
            for (StreamContext streamContext : values) {
                Guild guild = streamContext.getGuild();
                if (guild == null || guild.getId() != j || !PermissionUtils.can(Permission.VIEW_CHANNEL, map2.get(Long.valueOf(streamContext.getStream().getChannelId())))) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    private final boolean shouldShowUnread(Guild guild) {
        return !guild.isHub() || GrowthTeamFeatures.hubUnreadsEnabled$default(GrowthTeamFeatures.INSTANCE, false, 1, null);
    }

    private final int sumMentionCountsForGuild(long j, Map<Long, ? extends Collection<Long>> map, Map<Long, Integer> map2) {
        Collection<Long> collection = map.get(Long.valueOf(j));
        if (collection == null) {
            return 0;
        }
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(collection, 10));
        for (Number number : collection) {
            Integer num = map2.get(Long.valueOf(number.longValue()));
            arrayList.add(Integer.valueOf(num != null ? num.intValue() : 0));
        }
        return u.sumOfInt(arrayList);
    }

    private final void untargetCurrentTarget(ArrayList<GuildListItem> arrayList) {
        GuildListItem guildListItem;
        WidgetGuildListAdapter.Operation.TargetOperation targetOperation = this.currentTargetOperation;
        if (targetOperation != null) {
            int component2 = targetOperation.component2();
            GuildListItem guildListItem2 = arrayList.get(component2);
            m.checkNotNullExpressionValue(guildListItem2, "editingList[toPosition]");
            GuildListItem guildListItem3 = guildListItem2;
            if (guildListItem3 instanceof GuildListItem.GuildItem) {
                guildListItem = r5.copy((r32 & 1) != 0 ? r5.guild : null, (r32 & 2) != 0 ? r5.getMentionCount() : 0, (r32 & 4) != 0 ? r5.isLurkingGuild : false, (r32 & 8) != 0 ? r5.isUnread() : false, (r32 & 16) != 0 ? r5.isSelected : false, (r32 & 32) != 0 ? r5.folderId : null, (r32 & 64) != 0 ? r5.isConnectedToVoice : false, (r32 & 128) != 0 ? r5.hasOngoingApplicationStream : false, (r32 & 256) != 0 ? r5.isTargetedForFolderCreation : false, (r32 & 512) != 0 ? r5.isLastGuildInFolder : null, (r32 & 1024) != 0 ? r5.applicationStatus : null, (r32 & 2048) != 0 ? r5.isPendingGuild : false, (r32 & 4096) != 0 ? r5.hasActiveStageChannel : false, (r32 & 8192) != 0 ? r5.isConnectedToStageChannel : false, (r32 & 16384) != 0 ? ((GuildListItem.GuildItem) guildListItem3).hasActiveScheduledEvent : false);
            } else if (guildListItem3 instanceof GuildListItem.FolderItem) {
                guildListItem = r5.copy((r26 & 1) != 0 ? r5.folderId : 0L, (r26 & 2) != 0 ? r5.color : null, (r26 & 4) != 0 ? r5.name : null, (r26 & 8) != 0 ? r5.isOpen : false, (r26 & 16) != 0 ? r5.guilds : null, (r26 & 32) != 0 ? r5.isAnyGuildSelected : false, (r26 & 64) != 0 ? r5.isAnyGuildConnectedToVoice : false, (r26 & 128) != 0 ? r5.isAnyGuildConnectedToStageChannel : false, (r26 & 256) != 0 ? r5.getMentionCount() : 0, (r26 & 512) != 0 ? r5.isUnread() : false, (r26 & 1024) != 0 ? ((GuildListItem.FolderItem) guildListItem3).isTargetedForFolderAddition : false);
            }
            arrayList.set(component2, guildListItem);
        }
        this.currentTargetOperation = null;
    }

    public final Observable<Event> listenForEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void moveAbove(int i, int i2) {
        ViewState viewState = getViewState();
        Long l = null;
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            GuildListItem guildListItem = loaded.getItems().get(i2);
            if (guildListItem instanceof GuildListItem.GuildItem) {
                l = ((GuildListItem.GuildItem) guildListItem).getFolderId();
            } else if (!(guildListItem instanceof GuildListItem.FolderItem) && !(guildListItem instanceof GuildListItem.HelpItem) && !(guildListItem instanceof GuildListItem.CreateItem)) {
                throw new IllegalStateException("invalid target");
            }
            move(i, i2, l);
        }
    }

    public final void moveBelow(int i, int i2) {
        ViewState viewState = getViewState();
        Long l = null;
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            GuildListItem guildListItem = loaded.getItems().get(i2);
            if (guildListItem instanceof GuildListItem.GuildItem) {
                l = ((GuildListItem.GuildItem) guildListItem).getFolderId();
            } else if (guildListItem instanceof GuildListItem.FolderItem) {
                GuildListItem.FolderItem folderItem = (GuildListItem.FolderItem) guildListItem;
                if (folderItem.isOpen()) {
                    l = Long.valueOf(folderItem.getFolderId());
                }
            } else {
                throw new IllegalStateException("invalid target");
            }
            move(i, i2 + 1, l);
        }
    }

    public final boolean onDrop() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded == null) {
            return false;
        }
        List<GuildListItem> items = loaded.getItems();
        ArrayList<StoreGuildsSorted.Entry> arrayList = new ArrayList();
        WidgetGuildListAdapter.Operation.TargetOperation targetOperation = this.currentTargetOperation;
        if (targetOperation != null) {
            ArrayList<GuildListItem> arrayList2 = new ArrayList<>(items);
            untargetCurrentTarget(arrayList2);
            performTargetOperation(arrayList2, targetOperation.getFromPosition(), targetOperation.getTargetPosition());
            items = arrayList2;
        }
        for (GuildListItem guildListItem : items) {
            if (guildListItem instanceof GuildListItem.FolderItem) {
                GuildListItem.FolderItem folderItem = (GuildListItem.FolderItem) guildListItem;
                arrayList.add(new StoreGuildsSorted.Entry.Folder(folderItem.getFolderId(), folderItem.getGuilds(), folderItem.getColor(), folderItem.getName()));
            } else if (guildListItem instanceof GuildListItem.GuildItem) {
                GuildListItem.GuildItem guildItem = (GuildListItem.GuildItem) guildListItem;
                if (guildItem.getFolderId() == null) {
                    arrayList.add(new StoreGuildsSorted.Entry.SingletonGuild(guildItem.getGuild()));
                }
            }
        }
        StoreStream.Companion.getGuildsSorted().setPositions(arrayList);
        ArrayList arrayList3 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        for (StoreGuildsSorted.Entry entry : arrayList) {
            arrayList3.add(entry.asModelGuildFolder());
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateUserSettings(RestAPIParams.UserSettings.Companion.createWithGuildFolders(arrayList3)), false, 1, null), WidgetGuildsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetGuildsListViewModel$onDrop$2.INSTANCE);
        return targetOperation != null;
    }

    public final void onItemClicked(GuildListItem guildListItem, Context context, FragmentManager fragmentManager) {
        m.checkNotNullParameter(guildListItem, "item");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        boolean z2 = !GrowthTeamFeatures.INSTANCE.isHubEnabled();
        if (guildListItem instanceof GuildListItem.GuildItem) {
            GuildListItem.GuildItem guildItem = (GuildListItem.GuildItem) guildListItem;
            if (guildItem.isPendingGuild()) {
                MemberVerificationUtils.maybeShowVerificationGate$default(MemberVerificationUtils.INSTANCE, context, fragmentManager, guildItem.getGuild().getId(), "Guilds List", null, null, WidgetGuildsListViewModel$onItemClicked$1.INSTANCE, 48, null);
            } else if (guildItem.isSelected()) {
                StoreNavigation.setNavigationPanelAction$default(StoreStream.Companion.getNavigation(), StoreNavigation.PanelAction.CLOSE, null, 2, null);
            } else if (!guildItem.getGuild().isHub() || !z2) {
                StoreStream.Companion.getGuildSelected().set(guildItem.getGuild().getId());
            } else {
                b.a.d.m.g(context, R.string.discord_u_coming_soon_to_mobile, 0, null, 12);
            }
        } else if (guildListItem instanceof GuildListItem.PrivateChannelItem) {
            ChannelSelector.Companion.getInstance().selectChannel(0L, ((GuildListItem.PrivateChannelItem) guildListItem).getChannel().h(), (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null);
        } else if (guildListItem instanceof GuildListItem.UnavailableItem) {
            this.eventSubject.k.onNext(new Event.ShowUnavailableGuilds(((GuildListItem.UnavailableItem) guildListItem).getUnavailableGuildCount()));
        } else if (guildListItem instanceof GuildListItem.FriendsItem) {
            if (((GuildListItem.FriendsItem) guildListItem).isSelected()) {
                StoreNavigation.setNavigationPanelAction$default(StoreStream.Companion.getNavigation(), StoreNavigation.PanelAction.CLOSE, null, 2, null);
            } else {
                StoreStream.Companion.getGuildSelected().set(0L);
            }
        } else if (m.areEqual(guildListItem, GuildListItem.CreateItem.INSTANCE)) {
            this.eventSubject.k.onNext(Event.ShowCreateGuild.INSTANCE);
        } else if (guildListItem instanceof GuildListItem.HubItem) {
            StoreStream.Companion.getDirectories().markDiscordHubClicked();
            this.eventSubject.k.onNext(Event.ShowHubVerification.INSTANCE);
        } else if (m.areEqual(guildListItem, GuildListItem.HelpItem.INSTANCE)) {
            this.eventSubject.k.onNext(Event.ShowHelp.INSTANCE);
        } else if (guildListItem instanceof GuildListItem.FolderItem) {
            GuildListItem.FolderItem folderItem = (GuildListItem.FolderItem) guildListItem;
            if (folderItem.isOpen()) {
                StoreStream.Companion.getExpandedGuildFolders().closeFolder(guildListItem.getItemId());
            } else {
                StoreStream.Companion.getExpandedGuildFolders().openFolder(guildListItem.getItemId());
            }
            this.eventSubject.k.onNext(new Event.AnnounceFolderToggleForAccessibility(folderItem));
        }
    }

    public final void onItemLongPressed(GuildListItem guildListItem) {
        m.checkNotNullParameter(guildListItem, "item");
        if (guildListItem instanceof GuildListItem.PrivateChannelItem) {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new Event.ShowChannelActions(((GuildListItem.PrivateChannelItem) guildListItem).getChannel().h()));
        }
    }

    public final void target(int i, int i2) {
        GuildListItem guildListItem;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            ArrayList<GuildListItem> arrayList = new ArrayList<>(loaded.getItems());
            untargetCurrentTarget(arrayList);
            GuildListItem guildListItem2 = arrayList.get(i2);
            if (guildListItem2 instanceof GuildListItem.GuildItem) {
                guildListItem = r6.copy((r32 & 1) != 0 ? r6.guild : null, (r32 & 2) != 0 ? r6.getMentionCount() : 0, (r32 & 4) != 0 ? r6.isLurkingGuild : false, (r32 & 8) != 0 ? r6.isUnread() : false, (r32 & 16) != 0 ? r6.isSelected : false, (r32 & 32) != 0 ? r6.folderId : null, (r32 & 64) != 0 ? r6.isConnectedToVoice : false, (r32 & 128) != 0 ? r6.hasOngoingApplicationStream : false, (r32 & 256) != 0 ? r6.isTargetedForFolderCreation : true, (r32 & 512) != 0 ? r6.isLastGuildInFolder : null, (r32 & 1024) != 0 ? r6.applicationStatus : null, (r32 & 2048) != 0 ? r6.isPendingGuild : false, (r32 & 4096) != 0 ? r6.hasActiveStageChannel : false, (r32 & 8192) != 0 ? r6.isConnectedToStageChannel : false, (r32 & 16384) != 0 ? ((GuildListItem.GuildItem) guildListItem2).hasActiveScheduledEvent : false);
            } else if (guildListItem2 instanceof GuildListItem.FolderItem) {
                guildListItem = r6.copy((r26 & 1) != 0 ? r6.folderId : 0L, (r26 & 2) != 0 ? r6.color : null, (r26 & 4) != 0 ? r6.name : null, (r26 & 8) != 0 ? r6.isOpen : false, (r26 & 16) != 0 ? r6.guilds : null, (r26 & 32) != 0 ? r6.isAnyGuildSelected : false, (r26 & 64) != 0 ? r6.isAnyGuildConnectedToVoice : false, (r26 & 128) != 0 ? r6.isAnyGuildConnectedToStageChannel : false, (r26 & 256) != 0 ? r6.getMentionCount() : 0, (r26 & 512) != 0 ? r6.isUnread() : false, (r26 & 1024) != 0 ? ((GuildListItem.FolderItem) guildListItem2).isTargetedForFolderAddition : true);
            } else {
                throw new IllegalStateException("invalid target item: " + guildListItem2);
            }
            arrayList.set(i2, guildListItem);
            this.currentTargetOperation = new WidgetGuildListAdapter.Operation.TargetOperation(i, i2);
            updateViewState(ViewState.Loaded.copy$default(loaded, arrayList, false, true, 2, null));
            return;
        }
        throw new IllegalStateException("targeting with no items");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildsListViewModel(Clock clock, Observable<StoreState> observable) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(observable, "storeObservable");
        this.clock = clock;
        this.eventSubject = PublishSubject.k0();
        Observable q = ObservableExtensionsKt.computationLatest(observable).q();
        m.checkNotNullExpressionValue(q, "storeObservable\n        …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), WidgetGuildsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
