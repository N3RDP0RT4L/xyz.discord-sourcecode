package com.discord.widgets.guilds.list;

import andhook.lib.HookHelper;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.widgets.guilds.list.GuildListItem;
import com.discord.widgets.guilds.list.GuildListViewHolder;
import com.discord.widgets.guilds.list.GuildsDragAndDropCallback;
import com.discord.widgets.guilds.list.WidgetGuildListAdapter;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetGuildListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000{\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\n*\u0001\r\u0018\u0000 N2\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0004NOPQB\u0017\u0012\u0006\u0010I\u001a\u00020H\u0012\u0006\u0010A\u001a\u00020@¢\u0006\u0004\bL\u0010MJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ+\u0010\u000e\u001a\u00020\r2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J'\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ7\u0010#\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u001d\u001a\u00020\u00102\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00100\u001e2\u0006\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020 H\u0016¢\u0006\u0004\b#\u0010$J#\u0010&\u001a\u00020\u00122\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0006\u0010%\u001a\u00020\u0007¢\u0006\u0004\b&\u0010'J\u001f\u0010+\u001a\u00020\u00022\u0006\u0010)\u001a\u00020(2\u0006\u0010*\u001a\u00020 H\u0016¢\u0006\u0004\b+\u0010,J\u001f\u0010/\u001a\u00020\u00122\u0006\u0010-\u001a\u00020\u00022\u0006\u0010.\u001a\u00020 H\u0016¢\u0006\u0004\b/\u00100J\u000f\u00101\u001a\u00020 H\u0016¢\u0006\u0004\b1\u00102J\u0017\u00104\u001a\u0002032\u0006\u0010.\u001a\u00020 H\u0016¢\u0006\u0004\b4\u00105J\u0017\u00106\u001a\u00020 2\u0006\u0010.\u001a\u00020 H\u0016¢\u0006\u0004\b6\u00107J\u0015\u00109\u001a\u00020\u00122\u0006\u00108\u001a\u00020 ¢\u0006\u0004\b9\u0010:R\u0016\u0010;\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010C\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010<R\u001c\u0010D\u001a\b\u0012\u0004\u0012\u00020\u00040\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010ER\u0018\u0010F\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR\u0016\u0010K\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bK\u0010<¨\u0006R"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Lcom/discord/widgets/guilds/list/GuildsDragAndDropCallback$Controller;", "Lcom/discord/widgets/guilds/list/GuildListItem;", "sourceItem", "targetItem", "", "isPendingGuildOperation", "(Lcom/discord/widgets/guilds/list/GuildListItem;Lcom/discord/widgets/guilds/list/GuildListItem;)Z", "", "oldItems", "newItems", "com/discord/widgets/guilds/list/WidgetGuildListAdapter$createDiffUtilCallback$1", "createDiffUtilCallback", "(Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$createDiffUtilCallback$1;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "viewHolder", "", "onDragStarted", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V", "onDrop", "()V", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "source", "target", "onMove", "(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z", "selected", "", "dropTargets", "", "curX", "curY", "chooseDropTarget", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Ljava/util/List;II)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "calculateDiff", "setItems", "(Ljava/util/List;Z)V", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onBindViewHolder", "(Lcom/discord/widgets/guilds/list/GuildListViewHolder;I)V", "getItemCount", "()I", "", "getItemId", "(I)J", "getItemViewType", "(I)I", "height", "handleBottomNavHeight", "(I)V", "targetCenterY", "I", "Landroid/graphics/Rect;", "boundingBoxRect", "Landroid/graphics/Rect;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$InteractionListener;", "interactionListener", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$InteractionListener;", "selectedCenterY", "items", "Ljava/util/List;", "draggingItem", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Landroidx/recyclerview/widget/LinearLayoutManager;", "layoutManager", "Landroidx/recyclerview/widget/LinearLayoutManager;", "bottomNavHeight", HookHelper.constructorName, "(Landroidx/recyclerview/widget/LinearLayoutManager;Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$InteractionListener;)V", "Companion", "InteractionListener", "Operation", "TargetPosition", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildListAdapter extends RecyclerView.Adapter<GuildListViewHolder> implements GuildsDragAndDropCallback.Controller {
    public static final Companion Companion = new Companion(null);
    private static final float NEAR_CENTER_PERCENTAGE = 0.2f;
    private int bottomNavHeight;
    private GuildListViewHolder draggingItem;
    private final InteractionListener interactionListener;
    private final LinearLayoutManager layoutManager;
    private int selectedCenterY;
    private int targetCenterY;
    private List<? extends GuildListItem> items = n.emptyList();
    private final Rect boundingBoxRect = new Rect();

    /* compiled from: WidgetGuildListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0007\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Companion;", "", "", "NEAR_CENTER_PERCENTAGE", "F", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H&¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H&¢\u0006\u0004\b\t\u0010\bJ\u0017\u0010\f\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\nH&¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0006H&¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0011\u001a\u00020\u0010H&¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$InteractionListener;", "", "Landroid/view/View;", "view", "Lcom/discord/widgets/guilds/list/GuildListItem;", "item", "", "onItemClicked", "(Landroid/view/View;Lcom/discord/widgets/guilds/list/GuildListItem;)V", "onItemLongPressed", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;", "operation", "onOperation", "(Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;)V", "onItemMoved", "()V", "", "onDrop", "()Z", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface InteractionListener {
        boolean onDrop();

        void onItemClicked(View view, GuildListItem guildListItem);

        void onItemLongPressed(View view, GuildListItem guildListItem);

        void onItemMoved();

        void onOperation(Operation operation);
    }

    /* compiled from: WidgetGuildListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;", "", HookHelper.constructorName, "()V", "MoveAbove", "MoveBelow", "TargetOperation", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveAbove;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Operation {

        /* compiled from: WidgetGuildListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveAbove;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;", "", "component1", "()I", "component2", "fromPosition", "targetPosition", "copy", "(II)Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveAbove;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getFromPosition", "getTargetPosition", HookHelper.constructorName, "(II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MoveAbove extends Operation {
            private final int fromPosition;
            private final int targetPosition;

            public MoveAbove(int i, int i2) {
                super(null);
                this.fromPosition = i;
                this.targetPosition = i2;
            }

            public static /* synthetic */ MoveAbove copy$default(MoveAbove moveAbove, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = moveAbove.fromPosition;
                }
                if ((i3 & 2) != 0) {
                    i2 = moveAbove.targetPosition;
                }
                return moveAbove.copy(i, i2);
            }

            public final int component1() {
                return this.fromPosition;
            }

            public final int component2() {
                return this.targetPosition;
            }

            public final MoveAbove copy(int i, int i2) {
                return new MoveAbove(i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MoveAbove)) {
                    return false;
                }
                MoveAbove moveAbove = (MoveAbove) obj;
                return this.fromPosition == moveAbove.fromPosition && this.targetPosition == moveAbove.targetPosition;
            }

            public final int getFromPosition() {
                return this.fromPosition;
            }

            public final int getTargetPosition() {
                return this.targetPosition;
            }

            public int hashCode() {
                return (this.fromPosition * 31) + this.targetPosition;
            }

            public String toString() {
                StringBuilder R = a.R("MoveAbove(fromPosition=");
                R.append(this.fromPosition);
                R.append(", targetPosition=");
                return a.A(R, this.targetPosition, ")");
            }
        }

        /* compiled from: WidgetGuildListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;", "", "component1", "()I", "component2", "fromPosition", "targetPosition", "copy", "(II)Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getTargetPosition", "getFromPosition", HookHelper.constructorName, "(II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MoveBelow extends Operation {
            private final int fromPosition;
            private final int targetPosition;

            public MoveBelow(int i, int i2) {
                super(null);
                this.fromPosition = i;
                this.targetPosition = i2;
            }

            public static /* synthetic */ MoveBelow copy$default(MoveBelow moveBelow, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = moveBelow.fromPosition;
                }
                if ((i3 & 2) != 0) {
                    i2 = moveBelow.targetPosition;
                }
                return moveBelow.copy(i, i2);
            }

            public final int component1() {
                return this.fromPosition;
            }

            public final int component2() {
                return this.targetPosition;
            }

            public final MoveBelow copy(int i, int i2) {
                return new MoveBelow(i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MoveBelow)) {
                    return false;
                }
                MoveBelow moveBelow = (MoveBelow) obj;
                return this.fromPosition == moveBelow.fromPosition && this.targetPosition == moveBelow.targetPosition;
            }

            public final int getFromPosition() {
                return this.fromPosition;
            }

            public final int getTargetPosition() {
                return this.targetPosition;
            }

            public int hashCode() {
                return (this.fromPosition * 31) + this.targetPosition;
            }

            public String toString() {
                StringBuilder R = a.R("MoveBelow(fromPosition=");
                R.append(this.fromPosition);
                R.append(", targetPosition=");
                return a.A(R, this.targetPosition, ")");
            }
        }

        /* compiled from: WidgetGuildListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;", "", "component1", "()I", "component2", "fromPosition", "targetPosition", "copy", "(II)Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getTargetPosition", "getFromPosition", HookHelper.constructorName, "(II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class TargetOperation extends Operation {
            private final int fromPosition;
            private final int targetPosition;

            public TargetOperation(int i, int i2) {
                super(null);
                this.fromPosition = i;
                this.targetPosition = i2;
            }

            public static /* synthetic */ TargetOperation copy$default(TargetOperation targetOperation, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = targetOperation.fromPosition;
                }
                if ((i3 & 2) != 0) {
                    i2 = targetOperation.targetPosition;
                }
                return targetOperation.copy(i, i2);
            }

            public final int component1() {
                return this.fromPosition;
            }

            public final int component2() {
                return this.targetPosition;
            }

            public final TargetOperation copy(int i, int i2) {
                return new TargetOperation(i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof TargetOperation)) {
                    return false;
                }
                TargetOperation targetOperation = (TargetOperation) obj;
                return this.fromPosition == targetOperation.fromPosition && this.targetPosition == targetOperation.targetPosition;
            }

            public final int getFromPosition() {
                return this.fromPosition;
            }

            public final int getTargetPosition() {
                return this.targetPosition;
            }

            public int hashCode() {
                return (this.fromPosition * 31) + this.targetPosition;
            }

            public String toString() {
                StringBuilder R = a.R("TargetOperation(fromPosition=");
                R.append(this.fromPosition);
                R.append(", targetPosition=");
                return a.A(R, this.targetPosition, ")");
            }
        }

        private Operation() {
        }

        public /* synthetic */ Operation(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0082\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$TargetPosition;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "TOP", "CENTER", "BOTTOM", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum TargetPosition {
        TOP,
        CENTER,
        BOTTOM
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            TargetPosition.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[TargetPosition.TOP.ordinal()] = 1;
            iArr[TargetPosition.CENTER.ordinal()] = 2;
            iArr[TargetPosition.BOTTOM.ordinal()] = 3;
        }
    }

    public WidgetGuildListAdapter(LinearLayoutManager linearLayoutManager, InteractionListener interactionListener) {
        m.checkNotNullParameter(linearLayoutManager, "layoutManager");
        m.checkNotNullParameter(interactionListener, "interactionListener");
        this.layoutManager = linearLayoutManager;
        this.interactionListener = interactionListener;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.discord.widgets.guilds.list.WidgetGuildListAdapter$createDiffUtilCallback$1] */
    private final WidgetGuildListAdapter$createDiffUtilCallback$1 createDiffUtilCallback(final List<? extends GuildListItem> list, final List<? extends GuildListItem> list2) {
        return new DiffUtil.Callback() { // from class: com.discord.widgets.guilds.list.WidgetGuildListAdapter$createDiffUtilCallback$1
            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public boolean areContentsTheSame(int i, int i2) {
                return m.areEqual((GuildListItem) list.get(i), (GuildListItem) list2.get(i2));
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public boolean areItemsTheSame(int i, int i2) {
                return ((GuildListItem) list.get(i)).getItemId() == ((GuildListItem) list2.get(i2)).getItemId();
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public int getNewListSize() {
                return list2.size();
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public int getOldListSize() {
                return list.size();
            }
        };
    }

    private final boolean isPendingGuildOperation(GuildListItem guildListItem, GuildListItem guildListItem2) {
        return ((guildListItem instanceof GuildListItem.GuildItem) && ((GuildListItem.GuildItem) guildListItem).isPendingGuild()) || ((guildListItem instanceof GuildListItem.FolderItem) && ((GuildListItem.FolderItem) guildListItem).isPendingGuildsFolder()) || (((guildListItem2 instanceof GuildListItem.GuildItem) && ((GuildListItem.GuildItem) guildListItem2).isPendingGuild()) || ((guildListItem2 instanceof GuildListItem.FolderItem) && ((GuildListItem.FolderItem) guildListItem2).isPendingGuildsFolder()));
    }

    @Override // com.discord.widgets.guilds.list.GuildsDragAndDropCallback.Controller
    public RecyclerView.ViewHolder chooseDropTarget(RecyclerView.ViewHolder viewHolder, List<RecyclerView.ViewHolder> list, int i, int i2) {
        m.checkNotNullParameter(viewHolder, "selected");
        m.checkNotNullParameter(list, "dropTargets");
        this.layoutManager.getTransformedBoundingBox(viewHolder.itemView, false, this.boundingBoxRect);
        this.selectedCenterY = this.boundingBoxRect.centerY();
        int i3 = Integer.MAX_VALUE;
        RecyclerView.ViewHolder viewHolder2 = null;
        for (RecyclerView.ViewHolder viewHolder3 : list) {
            this.layoutManager.getTransformedBoundingBox(viewHolder3.itemView, false, this.boundingBoxRect);
            int centerY = this.boundingBoxRect.centerY();
            int abs = Math.abs(this.selectedCenterY - centerY);
            if (abs < i3) {
                this.targetCenterY = centerY;
                viewHolder2 = viewHolder3;
                i3 = abs;
            }
        }
        return viewHolder2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.items.get(i).getItemId();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        GuildListItem guildListItem = this.items.get(i);
        if (guildListItem instanceof GuildListItem.GuildItem) {
            return 3;
        }
        if (guildListItem instanceof GuildListItem.PrivateChannelItem) {
            return 2;
        }
        if (guildListItem instanceof GuildListItem.UnavailableItem) {
            return 4;
        }
        if (guildListItem instanceof GuildListItem.FriendsItem) {
            return 0;
        }
        if (m.areEqual(guildListItem, GuildListItem.DividerItem.INSTANCE)) {
            return 1;
        }
        if (m.areEqual(guildListItem, GuildListItem.CreateItem.INSTANCE)) {
            return 5;
        }
        if (guildListItem instanceof GuildListItem.HubItem) {
            return 9;
        }
        if (m.areEqual(guildListItem, GuildListItem.HelpItem.INSTANCE)) {
            return 7;
        }
        if (guildListItem instanceof GuildListItem.FolderItem) {
            return 6;
        }
        if (m.areEqual(guildListItem, GuildListItem.SpaceItem.INSTANCE)) {
            return 8;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final void handleBottomNavHeight(int i) {
        this.bottomNavHeight = i;
        notifyItemChanged(this.items.lastIndexOf(GuildListItem.SpaceItem.INSTANCE));
    }

    @Override // com.discord.widgets.guilds.list.GuildsDragAndDropCallback.Controller
    public void onDragStarted(RecyclerView.ViewHolder viewHolder) {
        m.checkNotNullParameter(viewHolder, "viewHolder");
        GuildListViewHolder guildListViewHolder = (GuildListViewHolder) viewHolder;
        guildListViewHolder.onDragStarted();
        this.draggingItem = guildListViewHolder;
    }

    @Override // com.discord.widgets.guilds.list.GuildsDragAndDropCallback.Controller
    public void onDrop() {
        boolean onDrop = this.interactionListener.onDrop();
        GuildListViewHolder guildListViewHolder = this.draggingItem;
        if (guildListViewHolder != null) {
            guildListViewHolder.onDragEnded(onDrop);
        }
        this.draggingItem = null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:57:0x00ec, code lost:
        if (r7 != false) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x012d, code lost:
        if (r6 != false) goto L84;
     */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00e8  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x012b  */
    @Override // com.discord.widgets.guilds.list.GuildsDragAndDropCallback.Controller
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean onMove(androidx.recyclerview.widget.RecyclerView r18, androidx.recyclerview.widget.RecyclerView.ViewHolder r19, androidx.recyclerview.widget.RecyclerView.ViewHolder r20) {
        /*
            Method dump skipped, instructions count: 358
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.list.WidgetGuildListAdapter.onMove(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$ViewHolder, androidx.recyclerview.widget.RecyclerView$ViewHolder):boolean");
    }

    public final void setItems(List<? extends GuildListItem> list, boolean z2) {
        m.checkNotNullParameter(list, "newItems");
        if (z2) {
            DiffUtil.DiffResult calculateDiff = DiffUtil.calculateDiff(createDiffUtilCallback(this.items, list), true);
            m.checkNotNullExpressionValue(calculateDiff, "DiffUtil.calculateDiff(callback, true)");
            this.items = list;
            calculateDiff.dispatchUpdatesTo(this);
            return;
        }
        this.items = list;
        notifyDataSetChanged();
    }

    public void onBindViewHolder(final GuildListViewHolder guildListViewHolder, int i) {
        m.checkNotNullParameter(guildListViewHolder, "holder");
        final GuildListItem guildListItem = this.items.get(i);
        if (guildListItem instanceof GuildListItem.GuildItem) {
            ((GuildListViewHolder.GuildViewHolder) guildListViewHolder).configure((GuildListItem.GuildItem) guildListItem);
        } else if (guildListItem instanceof GuildListItem.FriendsItem) {
            ((GuildListViewHolder.FriendsViewHolder) guildListViewHolder).configure((GuildListItem.FriendsItem) guildListItem);
        } else if (guildListItem instanceof GuildListItem.PrivateChannelItem) {
            ((GuildListViewHolder.PrivateChannelViewHolder) guildListViewHolder).configure((GuildListItem.PrivateChannelItem) guildListItem);
        } else if (guildListItem instanceof GuildListItem.FolderItem) {
            ((GuildListViewHolder.FolderViewHolder) guildListViewHolder).configure((GuildListItem.FolderItem) guildListItem);
        } else if ((guildListItem instanceof GuildListItem.UnavailableItem) || (guildListItem instanceof GuildListItem.CreateItem) || (guildListItem instanceof GuildListItem.HelpItem)) {
            guildListViewHolder.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.list.WidgetGuildListAdapter$onBindViewHolder$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetGuildListAdapter.InteractionListener interactionListener;
                    interactionListener = WidgetGuildListAdapter.this.interactionListener;
                    View view2 = guildListViewHolder.itemView;
                    m.checkNotNullExpressionValue(view2, "holder.itemView");
                    interactionListener.onItemClicked(view2, guildListItem);
                }
            });
        } else if (guildListItem instanceof GuildListItem.HubItem) {
            guildListViewHolder.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.list.WidgetGuildListAdapter$onBindViewHolder$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetGuildListAdapter.InteractionListener interactionListener;
                    interactionListener = WidgetGuildListAdapter.this.interactionListener;
                    View view2 = guildListViewHolder.itemView;
                    m.checkNotNullExpressionValue(view2, "holder.itemView");
                    interactionListener.onItemClicked(view2, guildListItem);
                }
            });
            ((GuildListViewHolder.DiscordHubViewHolder) guildListViewHolder).configure((GuildListItem.HubItem) guildListItem);
        } else if (guildListItem instanceof GuildListItem.SpaceItem) {
            ((GuildListViewHolder.SpaceViewHolder) guildListViewHolder).configure(this.bottomNavHeight);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public GuildListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        WidgetGuildListAdapter$onCreateViewHolder$1 widgetGuildListAdapter$onCreateViewHolder$1 = new WidgetGuildListAdapter$onCreateViewHolder$1(viewGroup);
        switch (i) {
            case 0:
                View invoke = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_profile);
                m.checkNotNullExpressionValue(invoke, "itemView");
                return new GuildListViewHolder.FriendsViewHolder(invoke, new WidgetGuildListAdapter$onCreateViewHolder$8(this, invoke));
            case 1:
                View invoke2 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_divider);
                m.checkNotNullExpressionValue(invoke2, "itemView");
                return new GuildListViewHolder.SimpleViewHolder(invoke2);
            case 2:
                View invoke3 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_dm_vertical);
                m.checkNotNullExpressionValue(invoke3, "itemView");
                return new GuildListViewHolder.PrivateChannelViewHolder(invoke3, new WidgetGuildListAdapter$onCreateViewHolder$6(this, invoke3), new WidgetGuildListAdapter$onCreateViewHolder$7(this, invoke3));
            case 3:
                View invoke4 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_guild_vertical);
                int themedColor = ColorCompat.getThemedColor(viewGroup.getContext(), (int) R.attr.colorBackgroundTertiary);
                int themedColor2 = ColorCompat.getThemedColor(viewGroup.getContext(), (int) R.attr.primary_600);
                m.checkNotNullExpressionValue(invoke4, "itemView");
                return new GuildListViewHolder.GuildViewHolder(invoke4, themedColor, themedColor2, new WidgetGuildListAdapter$onCreateViewHolder$2(this, invoke4), new WidgetGuildListAdapter$onCreateViewHolder$3(this, invoke4));
            case 4:
                View invoke5 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_guild_unavailable);
                m.checkNotNullExpressionValue(invoke5, "itemView");
                return new GuildListViewHolder.SimpleViewHolder(invoke5);
            case 5:
                View invoke6 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_guild_new);
                m.checkNotNullExpressionValue(invoke6, "itemView");
                return new GuildListViewHolder.SimpleViewHolder(invoke6);
            case 6:
                View invoke7 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_folder);
                m.checkNotNullExpressionValue(invoke7, "itemView");
                return new GuildListViewHolder.FolderViewHolder(invoke7, new WidgetGuildListAdapter$onCreateViewHolder$4(this, invoke7), new WidgetGuildListAdapter$onCreateViewHolder$5(this, invoke7));
            case 7:
                View invoke8 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_guild_nux);
                m.checkNotNullExpressionValue(invoke8, "itemView");
                return new GuildListViewHolder.SimpleViewHolder(invoke8);
            case 8:
                View invoke9 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.recycler_item_bottom_nav_space);
                m.checkNotNullExpressionValue(invoke9, "itemView");
                return new GuildListViewHolder.SpaceViewHolder(invoke9);
            case 9:
                View invoke10 = widgetGuildListAdapter$onCreateViewHolder$1.invoke(R.layout.widget_guilds_list_item_hub_verification);
                m.checkNotNullExpressionValue(invoke10, "itemView");
                return new GuildListViewHolder.DiscordHubViewHolder(invoke10);
            default:
                throw new IllegalStateException(a.p("invalid view type: ", i));
        }
    }
}
