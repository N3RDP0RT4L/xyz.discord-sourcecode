package com.discord.widgets.guilds.list;

import com.discord.app.AppViewModel;
import com.discord.widgets.guilds.list.WidgetGuildsListViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildsList$viewModel$2 extends o implements Function0<AppViewModel<WidgetGuildsListViewModel.ViewState>> {
    public static final WidgetGuildsList$viewModel$2 INSTANCE = new WidgetGuildsList$viewModel$2();

    public WidgetGuildsList$viewModel$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetGuildsListViewModel.ViewState> invoke() {
        return new WidgetGuildsListViewModel(null, null, 3, null);
    }
}
