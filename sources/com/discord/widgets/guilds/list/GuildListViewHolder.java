package com.discord.widgets.guilds.list;

import andhook.lib.HookHelper;
import android.content.res.ColorStateList;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.d3;
import b.a.k.b;
import b.d.b.a.a;
import b.f.g.e.f;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetGuildsListItemDmBinding;
import com.discord.databinding.WidgetGuildsListItemFolderBinding;
import com.discord.databinding.WidgetGuildsListItemGuildBinding;
import com.discord.databinding.WidgetGuildsListItemGuildVerticalBinding;
import com.discord.databinding.WidgetGuildsListItemHubVerificationBinding;
import com.discord.databinding.WidgetGuildsListItemProfileBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.tooltips.SparkleView;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.GuildView;
import com.discord.views.ServerFolderView;
import com.discord.widgets.guilds.list.GuildListItem;
import com.discord.widgets.guilds.list.GuildListViewHolder;
import com.discord.widgets.guilds.list.GuildsDragAndDropCallback;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.o;
import d0.t.g0;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildListViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0007\u0013\u0014\u0015\u0016\u0017\u0018\u0019B\u0011\b\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0004¢\u0006\u0004\b\r\u0010\u000e\u0082\u0001\u0007\u001a\u001b\u001c\u001d\u001e\u001f ¨\u0006!"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "", "onDragStarted", "()V", "", "wasMerge", "onDragEnded", "(Z)V", "Landroid/widget/TextView;", "textView", "", "count", "configureMentionsCount", "(Landroid/widget/TextView;I)V", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;)V", "DiscordHubViewHolder", "FolderViewHolder", "FriendsViewHolder", "GuildViewHolder", "PrivateChannelViewHolder", "SimpleViewHolder", "SpaceViewHolder", "Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder$FriendsViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder$PrivateChannelViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder$FolderViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder$SpaceViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder$SimpleViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder$DiscordHubViewHolder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildListViewHolder extends RecyclerView.ViewHolder {

    /* compiled from: GuildListViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$DiscordHubViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListItem$HubItem;", "item", "", "configure", "(Lcom/discord/widgets/guilds/list/GuildListItem$HubItem;)V", "Lcom/discord/databinding/WidgetGuildsListItemHubVerificationBinding;", "binding", "Lcom/discord/databinding/WidgetGuildsListItemHubVerificationBinding;", "getBinding", "()Lcom/discord/databinding/WidgetGuildsListItemHubVerificationBinding;", "Landroid/view/View;", "view", HookHelper.constructorName, "(Landroid/view/View;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DiscordHubViewHolder extends GuildListViewHolder {
        private final WidgetGuildsListItemHubVerificationBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public DiscordHubViewHolder(View view) {
            super(view, null);
            m.checkNotNullParameter(view, "view");
            SparkleView sparkleView = (SparkleView) view.findViewById(R.id.guild_item_sparkle);
            if (sparkleView != null) {
                WidgetGuildsListItemHubVerificationBinding widgetGuildsListItemHubVerificationBinding = new WidgetGuildsListItemHubVerificationBinding((FrameLayout) view, sparkleView);
                m.checkNotNullExpressionValue(widgetGuildsListItemHubVerificationBinding, "WidgetGuildsListItemHubV…icationBinding.bind(view)");
                this.binding = widgetGuildsListItemHubVerificationBinding;
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.guild_item_sparkle)));
        }

        public final void configure(GuildListItem.HubItem hubItem) {
            m.checkNotNullParameter(hubItem, "item");
            SparkleView sparkleView = this.binding.f2440b;
            if (!hubItem.getShowSparkle()) {
                sparkleView.b();
            }
            sparkleView.setVisibility(hubItem.getShowSparkle() ? 0 : 8);
        }

        public final WidgetGuildsListItemHubVerificationBinding getBinding() {
            return this.binding;
        }
    }

    /* compiled from: GuildListViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B7\u0012\u0006\u0010'\u001a\u00020&\u0012\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\t0\u0015\u0012\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\t0\u0015¢\u0006\u0004\b(\u0010)J\u000f\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0004\u0010\u0005J\r\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\r\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u000f\u001a\u00020\u0003¢\u0006\u0004\b\u000f\u0010\u0005J\u0015\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013R\u0018\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0014R\"\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\t0\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\"\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\t0\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u0017R\u0016\u0010\u001e\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR$\u0010 \u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e¢\u0006\u0012\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%¨\u0006*"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$FolderViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Lcom/discord/widgets/guilds/list/GuildsDragAndDropCallback$DraggableViewHolder;", "", "canDrag", "()Z", "", "getNumChildren", "()I", "", "onDragStarted", "()V", "wasMerge", "onDragEnded", "(Z)V", "shouldDrawDecoration", "Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;", "data", "configure", "(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)V", "Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;", "Lkotlin/Function1;", "onLongPressed", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/databinding/WidgetGuildsListItemFolderBinding;", "binding", "Lcom/discord/databinding/WidgetGuildsListItemFolderBinding;", "isDragging", "Z", "onClicked", "numChildren", "I", ModelAuditLogEntry.CHANGE_KEY_COLOR, "Ljava/lang/Integer;", "getColor", "()Ljava/lang/Integer;", "setColor", "(Ljava/lang/Integer;)V", "Landroid/view/View;", "view", HookHelper.constructorName, "(Landroid/view/View;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class FolderViewHolder extends GuildListViewHolder implements GuildsDragAndDropCallback.DraggableViewHolder {
        private final WidgetGuildsListItemFolderBinding binding;
        @ColorInt
        private Integer color;
        private GuildListItem.FolderItem data;
        private boolean isDragging;
        private int numChildren;
        private final Function1<GuildListItem.FolderItem, Unit> onClicked;
        private final Function1<GuildListItem.FolderItem, Unit> onLongPressed;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public FolderViewHolder(View view, Function1<? super GuildListItem.FolderItem, Unit> function1, Function1<? super GuildListItem.FolderItem, Unit> function12) {
            super(view, null);
            m.checkNotNullParameter(view, "view");
            m.checkNotNullParameter(function1, "onClicked");
            m.checkNotNullParameter(function12, "onLongPressed");
            this.onClicked = function1;
            this.onLongPressed = function12;
            int i = R.id.guilds_item_folder;
            ServerFolderView serverFolderView = (ServerFolderView) view.findViewById(R.id.guilds_item_folder);
            if (serverFolderView != null) {
                i = R.id.guilds_item_folder_container;
                FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.guilds_item_folder_container);
                if (frameLayout != null) {
                    i = R.id.guilds_item_highlight;
                    View findViewById = view.findViewById(R.id.guilds_item_highlight);
                    if (findViewById != null) {
                        i = R.id.guilds_item_mentions;
                        TextView textView = (TextView) view.findViewById(R.id.guilds_item_mentions);
                        if (textView != null) {
                            i = R.id.guilds_item_selected;
                            View findViewById2 = view.findViewById(R.id.guilds_item_selected);
                            if (findViewById2 != null) {
                                i = R.id.guilds_item_unread;
                                ImageView imageView = (ImageView) view.findViewById(R.id.guilds_item_unread);
                                if (imageView != null) {
                                    i = R.id.guilds_item_voice;
                                    ImageView imageView2 = (ImageView) view.findViewById(R.id.guilds_item_voice);
                                    if (imageView2 != null) {
                                        WidgetGuildsListItemFolderBinding widgetGuildsListItemFolderBinding = new WidgetGuildsListItemFolderBinding((RelativeLayout) view, serverFolderView, frameLayout, findViewById, textView, findViewById2, imageView, imageView2);
                                        m.checkNotNullExpressionValue(widgetGuildsListItemFolderBinding, "WidgetGuildsListItemFolderBinding.bind(view)");
                                        this.binding = widgetGuildsListItemFolderBinding;
                                        m.checkNotNullExpressionValue(frameLayout, "binding.guildsItemFolderContainer");
                                        frameLayout.setClipToOutline(true);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        @Override // com.discord.widgets.guilds.list.GuildsDragAndDropCallback.DraggableViewHolder
        public boolean canDrag() {
            GuildListItem.FolderItem folderItem;
            GuildListItem.FolderItem folderItem2 = this.data;
            return folderItem2 != null && !folderItem2.isOpen() && ((folderItem = this.data) == null || !folderItem.isPendingGuildsFolder());
        }

        public final void configure(final GuildListItem.FolderItem folderItem) {
            CharSequence d;
            CharSequence d2;
            m.checkNotNullParameter(folderItem, "data");
            this.data = folderItem;
            Integer color = folderItem.getColor();
            this.color = color != null ? Integer.valueOf((int) (color.intValue() + 4278190080L)) : null;
            int i = 0;
            this.numChildren = folderItem.isOpen() ? folderItem.getGuilds().size() : 0;
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.list.GuildListViewHolder$FolderViewHolder$configure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1 function1;
                    function1 = GuildListViewHolder.FolderViewHolder.this.onClicked;
                    function1.invoke(folderItem);
                }
            });
            ServerFolderView serverFolderView = this.binding.f2437b;
            boolean isPendingGuildsFolder = folderItem.isPendingGuildsFolder();
            long folderId = folderItem.getFolderId();
            boolean isOpen = folderItem.isOpen();
            List<Guild> guilds = folderItem.getGuilds();
            Integer color2 = folderItem.getColor();
            Objects.requireNonNull(serverFolderView);
            m.checkNotNullParameter(guilds, "guilds");
            if (isPendingGuildsFolder) {
                serverFolderView.l = Long.valueOf(folderId);
                serverFolderView.m = isOpen;
                GridLayout gridLayout = serverFolderView.k.g;
                m.checkNotNullExpressionValue(gridLayout, "binding.guildViews");
                gridLayout.setVisibility(isOpen ? 0 : 8);
                serverFolderView.k.f97b.setImageDrawable(ContextCompat.getDrawable(serverFolderView.getContext(), R.drawable.ic_guild_list_pending_folder));
            } else {
                Long l = serverFolderView.l;
                boolean z2 = (l == null || l.longValue() != folderId || serverFolderView.m == isOpen) ? false : true;
                serverFolderView.l = Long.valueOf(folderId);
                if (isOpen) {
                    serverFolderView.m = true;
                    if (z2) {
                        serverFolderView.b(R.anim.anim_folder_slide_in_down);
                    } else {
                        serverFolderView.a();
                    }
                    ImageViewCompat.setImageTintList(serverFolderView.k.f97b, ColorStateList.valueOf((int) ((color2 != null ? color2.intValue() : ColorCompat.getThemedColor(serverFolderView.getContext(), (int) R.attr.color_brand_500)) + 4278190080L)));
                } else {
                    serverFolderView.m = false;
                    if (z2) {
                        serverFolderView.b(R.anim.anim_folder_slide_out_up);
                    } else {
                        serverFolderView.a();
                    }
                    d3 d3Var = serverFolderView.k;
                    int i2 = 0;
                    for (Object obj : n.listOf((Object[]) new GuildView[]{d3Var.c, d3Var.d, d3Var.e, d3Var.f})) {
                        i2++;
                        if (i2 < 0) {
                            n.throwIndexOverflow();
                        }
                        GuildView guildView = (GuildView) obj;
                        if (n.getLastIndex(guilds) >= i2) {
                            Guild guild = guilds.get(i2);
                            String forGuild$default = guild.hasIcon() ? IconUtils.getForGuild$default(guild, IconUtils.DEFAULT_ICON, false, null, 12, null) : null;
                            m.checkNotNullExpressionValue(guildView, "guildView");
                            guildView.setVisibility(0);
                            guildView.a(forGuild$default, guild.getShortName());
                        } else {
                            m.checkNotNullExpressionValue(guildView, "guildView");
                            guildView.setVisibility(8);
                        }
                    }
                }
            }
            int i3 = 8;
            View view = this.binding.f;
            m.checkNotNullExpressionValue(view, "binding.guildsItemSelected");
            view.setVisibility(!folderItem.isOpen() && folderItem.isAnyGuildSelected() ? 0 : 8);
            ImageView imageView = this.binding.g;
            m.checkNotNullExpressionValue(imageView, "binding.guildsItemUnread");
            imageView.setVisibility(!folderItem.isOpen() && folderItem.isUnread() ? 0 : 8);
            ImageView imageView2 = this.binding.h;
            imageView2.setActivated(folderItem.isAnyGuildConnectedToVoice());
            imageView2.setVisibility(!folderItem.isOpen() && folderItem.isAnyGuildConnectedToVoice() ? 0 : 8);
            imageView2.setImageResource(folderItem.isAnyGuildConnectedToStageChannel() ? R.drawable.ic_channel_stage_24dp_white : R.drawable.ic_volume_up_white_24dp);
            View view2 = this.binding.d;
            m.checkNotNullExpressionValue(view2, "binding.guildsItemHighlight");
            if (folderItem.isTargetedForFolderAddition()) {
                i3 = 0;
            }
            view2.setVisibility(i3);
            int mentionCount = folderItem.getMentionCount();
            String i18nPluralString = mentionCount > 0 ? StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.guild_folder_tooltip_a11y_label_mentions, mentionCount, Integer.valueOf(mentionCount)) : "";
            View view3 = this.itemView;
            m.checkNotNullExpressionValue(view3, "itemView");
            d = b.d(view3, folderItem.isOpen() ? R.string.expanded : R.string.collapsed, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            View view4 = this.itemView;
            m.checkNotNullExpressionValue(view4, "itemView");
            View view5 = this.itemView;
            m.checkNotNullExpressionValue(view5, "itemView");
            d2 = b.d(view5, R.string.guild_folder_tooltip_a11y_label_with_expanded_state, new Object[]{folderItem.getName(), i18nPluralString, d}, (r4 & 4) != 0 ? b.c.j : null);
            view4.setContentDescription(d2);
            View view6 = this.itemView;
            m.checkNotNullExpressionValue(view6, "itemView");
            ViewExtensions.setOnLongClickListenerConsumeClick(view6, new GuildListViewHolder$FolderViewHolder$configure$4(this, folderItem));
            TextView textView = this.binding.e;
            m.checkNotNullExpressionValue(textView, "binding.guildsItemMentions");
            if (!folderItem.isOpen()) {
                i = folderItem.getMentionCount();
            }
            configureMentionsCount(textView, i);
        }

        public final Integer getColor() {
            return this.color;
        }

        public final int getNumChildren() {
            return this.numChildren;
        }

        @Override // com.discord.widgets.guilds.list.GuildListViewHolder
        public void onDragEnded(boolean z2) {
            this.isDragging = false;
            ServerFolderView serverFolderView = this.binding.f2437b;
            m.checkNotNullExpressionValue(serverFolderView, "binding.guildsItemFolder");
            serverFolderView.setBackground(null);
        }

        @Override // com.discord.widgets.guilds.list.GuildListViewHolder
        public void onDragStarted() {
            this.isDragging = true;
            ServerFolderView serverFolderView = this.binding.f2437b;
            m.checkNotNullExpressionValue(serverFolderView, "binding.guildsItemFolder");
            ServerFolderView serverFolderView2 = this.binding.f2437b;
            m.checkNotNullExpressionValue(serverFolderView2, "binding.guildsItemFolder");
            serverFolderView.setBackground(ContextCompat.getDrawable(serverFolderView2.getContext(), R.drawable.drawable_squircle_primary_600));
        }

        public final void setColor(Integer num) {
            this.color = num;
        }

        public final boolean shouldDrawDecoration() {
            return !this.isDragging;
        }
    }

    /* compiled from: GuildListViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\n¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\"\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$FriendsViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListItem$FriendsItem;", "data", "", "configure", "(Lcom/discord/widgets/guilds/list/GuildListItem$FriendsItem;)V", "Lcom/discord/databinding/WidgetGuildsListItemProfileBinding;", "binding", "Lcom/discord/databinding/WidgetGuildsListItemProfileBinding;", "Lkotlin/Function1;", "onClicked", "Lkotlin/jvm/functions/Function1;", "Landroid/view/View;", "view", HookHelper.constructorName, "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class FriendsViewHolder extends GuildListViewHolder {
        private final WidgetGuildsListItemProfileBinding binding;
        private final Function1<GuildListItem.FriendsItem, Unit> onClicked;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public FriendsViewHolder(View view, Function1<? super GuildListItem.FriendsItem, Unit> function1) {
            super(view, null);
            m.checkNotNullParameter(view, "view");
            m.checkNotNullParameter(function1, "onClicked");
            this.onClicked = function1;
            int i = R.id.guilds_item_profile_avatar;
            ImageView imageView = (ImageView) view.findViewById(R.id.guilds_item_profile_avatar);
            if (imageView != null) {
                i = R.id.guilds_item_profile_avatar_wrap;
                FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.guilds_item_profile_avatar_wrap);
                if (frameLayout != null) {
                    i = R.id.guilds_item_profile_selected;
                    ImageView imageView2 = (ImageView) view.findViewById(R.id.guilds_item_profile_selected);
                    if (imageView2 != null) {
                        WidgetGuildsListItemProfileBinding widgetGuildsListItemProfileBinding = new WidgetGuildsListItemProfileBinding((RelativeLayout) view, imageView, frameLayout, imageView2);
                        m.checkNotNullExpressionValue(widgetGuildsListItemProfileBinding, "WidgetGuildsListItemProfileBinding.bind(view)");
                        this.binding = widgetGuildsListItemProfileBinding;
                        return;
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public final void configure(final GuildListItem.FriendsItem friendsItem) {
            m.checkNotNullParameter(friendsItem, "data");
            ImageView imageView = this.binding.d;
            m.checkNotNullExpressionValue(imageView, "binding.guildsItemProfileSelected");
            imageView.setVisibility(friendsItem.isSelected() ? 0 : 8);
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.list.GuildListViewHolder$FriendsViewHolder$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1 function1;
                    function1 = GuildListViewHolder.FriendsViewHolder.this.onClicked;
                    function1.invoke(friendsItem);
                }
            });
            if (friendsItem.isSelected()) {
                this.binding.c.setBackgroundResource(R.drawable.drawable_squircle_brand_500);
                FrameLayout frameLayout = this.binding.c;
                m.checkNotNullExpressionValue(frameLayout, "binding.guildsItemProfileAvatarWrap");
                frameLayout.setBackgroundTintList(null);
            } else {
                this.binding.c.setBackgroundResource(R.drawable.drawable_circle_black);
                FrameLayout frameLayout2 = this.binding.c;
                m.checkNotNullExpressionValue(frameLayout2, "binding.guildsItemProfileAvatarWrap");
                int themedColor = ColorCompat.getThemedColor(frameLayout2, (int) R.attr.colorBackgroundSecondary);
                FrameLayout frameLayout3 = this.binding.c;
                m.checkNotNullExpressionValue(frameLayout3, "binding.guildsItemProfileAvatarWrap");
                frameLayout3.setBackgroundTintList(ColorStateList.valueOf(themedColor));
            }
            if (friendsItem.isSelected()) {
                ImageView imageView2 = this.binding.f2441b;
                m.checkNotNullExpressionValue(imageView2, "binding.guildsItemProfileAvatar");
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                imageView2.setImageTintList(ColorStateList.valueOf(ColorCompat.getColor(view, (int) R.color.white)));
                return;
            }
            ImageView imageView3 = this.binding.f2441b;
            m.checkNotNullExpressionValue(imageView3, "binding.guildsItemProfileAvatar");
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            imageView3.setImageTintList(ColorStateList.valueOf(ColorCompat.getThemedColor(view2, (int) R.attr.primary_300)));
        }
    }

    /* compiled from: GuildListViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 @2\u00020\u00012\u00020\u0002:\u0001@BK\u0012\u0006\u0010=\u001a\u00020<\u0012\b\b\u0001\u0010;\u001a\u00020'\u0012\b\b\u0001\u00107\u001a\u00020'\u0012\u0012\u00105\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\b0*\u0012\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\b0*¢\u0006\u0004\b>\u0010?J)\u0010\t\u001a\u00020\b2\b\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ'\u0010\u000f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0015\u0010\u001a\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u0015\u0010 \u001a\n\u0018\u00010\u001ej\u0004\u0018\u0001`\u001f¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\"\u0010#J\r\u0010\u0015\u001a\u00020\u0005¢\u0006\u0004\b\u0015\u0010\u001dJ\u000f\u0010$\u001a\u00020\bH\u0016¢\u0006\u0004\b$\u0010\fJ\u0017\u0010&\u001a\u00020\b2\u0006\u0010%\u001a\u00020\u0005H\u0016¢\u0006\u0004\b&\u0010\u0017R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\"\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\b0*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u0016\u0010.\u001a\u00020-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00103\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u0010)R\u0016\u00104\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u0010)R\"\u00105\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\b0*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u0010,R\u0018\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u00106R\u0016\u00107\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u0010)R\u0016\u00108\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u0010)R\u0016\u00109\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b9\u0010:R\u0016\u0010;\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010)¨\u0006A"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Lcom/discord/widgets/guilds/list/GuildsDragAndDropCallback$DraggableViewHolder;", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "applicationStatus", "", "isInFolder", "hasMentions", "", "configureApplicationStatus", "(Lcom/discord/api/guildjoinrequest/ApplicationStatus;ZZ)V", "configureDraggingAlpha", "()V", "isSelected", "guildHasIcon", "configureGuildIconBackground", "(ZZZ)V", "Lcom/discord/models/guild/Guild;", "guild", "configureGuildIconImage", "(Lcom/discord/models/guild/Guild;Z)V", "isTargetedForFolderCreation", "configureGuildIconPositioning", "(Z)V", "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;", "data", "configure", "(Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;)V", "canDrag", "()Z", "", "Lcom/discord/primitives/FolderId;", "getFolderId", "()Ljava/lang/Long;", "isLastGuildInFolder", "()Ljava/lang/Boolean;", "onDragStarted", "wasMerge", "onDragEnded", "", "defaultAvatarSize", "I", "Lkotlin/Function1;", "onLongPressed", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/databinding/WidgetGuildsListItemGuildVerticalBinding;", "binding", "Lcom/discord/databinding/WidgetGuildsListItemGuildVerticalBinding;", "Lcom/discord/databinding/WidgetGuildsListItemGuildBinding;", "bindingGuild", "Lcom/discord/databinding/WidgetGuildsListItemGuildBinding;", "targetedAvatarSize", "imageRequestSize", "onClicked", "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;", "overlayColorInFolder", "targetedAvatarMargin", "isDragging", "Z", "overlayColor", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GuildViewHolder extends GuildListViewHolder implements GuildsDragAndDropCallback.DraggableViewHolder {
        public static final Companion Companion = new Companion(null);
        private static final float DEFAULT_AVATAR_TEXT_SIZE_SP = 14.0f;
        private static final float TARGETED_AVATAR_TEXT_SIZE_DP = 8.0f;
        private final WidgetGuildsListItemGuildVerticalBinding binding;
        private final WidgetGuildsListItemGuildBinding bindingGuild;
        private GuildListItem.GuildItem data;
        private final int defaultAvatarSize;
        private final int imageRequestSize;
        private boolean isDragging;
        private final Function1<GuildListItem.GuildItem, Unit> onClicked;
        private final Function1<GuildListItem.GuildItem, Unit> onLongPressed;
        private final int overlayColor;
        private final int overlayColorInFolder;
        private final int targetedAvatarMargin;
        private final int targetedAvatarSize;

        /* compiled from: GuildListViewHolder.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0007\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$Companion;", "", "", "DEFAULT_AVATAR_TEXT_SIZE_SP", "F", "TARGETED_AVATAR_TEXT_SIZE_DP", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                ApplicationStatus.values();
                int[] iArr = new int[5];
                $EnumSwitchMapping$0 = iArr;
                iArr[ApplicationStatus.APPROVED.ordinal()] = 1;
                iArr[ApplicationStatus.REJECTED.ordinal()] = 2;
                iArr[ApplicationStatus.PENDING.ordinal()] = 3;
                iArr[ApplicationStatus.STARTED.ordinal()] = 4;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public GuildViewHolder(View view, @ColorInt int i, @ColorInt int i2, Function1<? super GuildListItem.GuildItem, Unit> function1, Function1<? super GuildListItem.GuildItem, Unit> function12) {
            super(view, null);
            m.checkNotNullParameter(view, "itemView");
            m.checkNotNullParameter(function1, "onClicked");
            m.checkNotNullParameter(function12, "onLongPressed");
            this.overlayColor = i;
            this.overlayColorInFolder = i2;
            this.onClicked = function1;
            this.onLongPressed = function12;
            int i3 = R.id.guilds_item_selected;
            ImageView imageView = (ImageView) view.findViewById(R.id.guilds_item_selected);
            if (imageView != null) {
                i3 = R.id.guilds_item_unread;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.guilds_item_unread);
                if (imageView2 != null) {
                    WidgetGuildsListItemGuildVerticalBinding widgetGuildsListItemGuildVerticalBinding = new WidgetGuildsListItemGuildVerticalBinding((RelativeLayout) view, imageView, imageView2);
                    m.checkNotNullExpressionValue(widgetGuildsListItemGuildVerticalBinding, "WidgetGuildsListItemGuil…calBinding.bind(itemView)");
                    this.binding = widgetGuildsListItemGuildVerticalBinding;
                    WidgetGuildsListItemGuildBinding a = WidgetGuildsListItemGuildBinding.a(view);
                    m.checkNotNullExpressionValue(a, "WidgetGuildsListItemGuildBinding.bind(itemView)");
                    this.bindingGuild = a;
                    this.defaultAvatarSize = view.getResources().getDimensionPixelSize(R.dimen.avatar_size_large);
                    this.targetedAvatarSize = view.getResources().getDimensionPixelSize(R.dimen.folder_guild_size);
                    this.targetedAvatarMargin = view.getResources().getDimensionPixelSize(R.dimen.folder_guild_outer_margin);
                    FrameLayout frameLayout = a.f;
                    m.checkNotNullExpressionValue(frameLayout, "bindingGuild.guildsItemAvatarWrap");
                    frameLayout.setClipToOutline(true);
                    SimpleDraweeView simpleDraweeView = a.d;
                    m.checkNotNullExpressionValue(simpleDraweeView, "bindingGuild.guildsItemAvatar");
                    this.imageRequestSize = IconUtils.getMediaProxySize(simpleDraweeView.getLayoutParams().height);
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i3)));
        }

        private final void configureApplicationStatus(ApplicationStatus applicationStatus, boolean z2, boolean z3) {
            if (applicationStatus == null || z3) {
                ImageView imageView = this.bindingGuild.f2438b;
                m.checkNotNullExpressionValue(imageView, "bindingGuild.guildsItemApplicationStatus");
                imageView.setVisibility(8);
                return;
            }
            int i = z2 ? R.attr.colorBackgroundSecondary : R.attr.colorBackgroundTertiary;
            ImageView imageView2 = this.bindingGuild.f2438b;
            m.checkNotNullExpressionValue(imageView2, "bindingGuild.guildsItemApplicationStatus");
            int themedColor = ColorCompat.getThemedColor(imageView2, i);
            ImageView imageView3 = this.bindingGuild.f2438b;
            m.checkNotNullExpressionValue(imageView3, "bindingGuild.guildsItemApplicationStatus");
            imageView3.setBackgroundTintList(ColorStateList.valueOf(themedColor));
            ImageView imageView4 = this.bindingGuild.f2438b;
            m.checkNotNullExpressionValue(imageView4, "bindingGuild.guildsItemApplicationStatus");
            imageView4.setVisibility(0);
            int ordinal = applicationStatus.ordinal();
            if (ordinal == 0) {
                this.bindingGuild.f2438b.setImageResource(R.drawable.ic_application_status_started);
            } else if (ordinal == 1) {
                this.bindingGuild.f2438b.setImageResource(R.drawable.ic_application_status_pending);
            } else if (ordinal == 2) {
                this.bindingGuild.f2438b.setImageResource(R.drawable.ic_application_status_rejected);
            } else if (ordinal != 3) {
                ImageView imageView5 = this.bindingGuild.f2438b;
                m.checkNotNullExpressionValue(imageView5, "bindingGuild.guildsItemApplicationStatus");
                imageView5.setVisibility(8);
            } else {
                this.bindingGuild.f2438b.setImageResource(R.drawable.ic_application_status_approved);
            }
        }

        private final void configureDraggingAlpha() {
            if (this.isDragging) {
                FrameLayout frameLayout = this.bindingGuild.f;
                m.checkNotNullExpressionValue(frameLayout, "bindingGuild.guildsItemAvatarWrap");
                frameLayout.setAlpha(0.5f);
                return;
            }
            FrameLayout frameLayout2 = this.bindingGuild.f;
            m.checkNotNullExpressionValue(frameLayout2, "bindingGuild.guildsItemAvatarWrap");
            frameLayout2.setAlpha(1.0f);
        }

        private final void configureGuildIconBackground(boolean z2, boolean z3, boolean z4) {
            int i;
            SimpleDraweeView simpleDraweeView = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "bindingGuild.guildsItemAvatar");
            float dimensionPixelSize = simpleDraweeView.getResources().getDimensionPixelSize(R.dimen.guild_icon_radius);
            SimpleDraweeView simpleDraweeView2 = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView2, "bindingGuild.guildsItemAvatar");
            boolean z5 = !z2;
            if (z3) {
                i = this.overlayColorInFolder;
            } else {
                i = this.overlayColor;
            }
            MGImages.setRoundingParams(simpleDraweeView2, dimensionPixelSize, z5, (r13 & 8) != 0 ? null : Integer.valueOf(i), (r13 & 16) != 0 ? null : null, (r13 & 32) != 0 ? null : null);
            if (z4) {
                if (z2) {
                    this.bindingGuild.f.setBackgroundResource(R.drawable.drawable_squircle_transparent);
                } else {
                    this.bindingGuild.f.setBackgroundResource(R.drawable.drawable_circle_transparent);
                }
                FrameLayout frameLayout = this.bindingGuild.f;
                m.checkNotNullExpressionValue(frameLayout, "bindingGuild.guildsItemAvatarWrap");
                frameLayout.setBackgroundTintList(null);
            } else if (z2) {
                this.bindingGuild.f.setBackgroundResource(R.drawable.drawable_squircle_brand_500);
                FrameLayout frameLayout2 = this.bindingGuild.f;
                m.checkNotNullExpressionValue(frameLayout2, "bindingGuild.guildsItemAvatarWrap");
                frameLayout2.setBackgroundTintList(null);
            } else {
                this.bindingGuild.f.setBackgroundResource(R.drawable.drawable_circle_black);
                int i2 = z3 ? R.attr.colorBackgroundSecondary : R.attr.colorBackgroundPrimary;
                FrameLayout frameLayout3 = this.bindingGuild.f;
                m.checkNotNullExpressionValue(frameLayout3, "bindingGuild.guildsItemAvatarWrap");
                int themedColor = ColorCompat.getThemedColor(frameLayout3, i2);
                FrameLayout frameLayout4 = this.bindingGuild.f;
                m.checkNotNullExpressionValue(frameLayout4, "bindingGuild.guildsItemAvatarWrap");
                frameLayout4.setBackgroundTintList(ColorStateList.valueOf(themedColor));
            }
        }

        private final void configureGuildIconImage(Guild guild, boolean z2) {
            boolean z3;
            String icon;
            try {
                z3 = guild.getFeatures().contains(GuildFeature.ANIMATED_ICON);
            } catch (Exception e) {
                AppLog.g.e("Guild is missing feature set", e, g0.mapOf(o.to(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID, String.valueOf(guild.getId()))));
                z3 = false;
            }
            if (!z3 || (icon = guild.getIcon()) == null || !t.startsWith$default(icon, "a", false, 2, null)) {
                SimpleDraweeView simpleDraweeView = this.bindingGuild.d;
                m.checkNotNullExpressionValue(simpleDraweeView, "bindingGuild.guildsItemAvatar");
                simpleDraweeView.getHierarchy().p(R.drawable.asset_default_avatar_64dp);
                SimpleDraweeView simpleDraweeView2 = this.bindingGuild.d;
                m.checkNotNullExpressionValue(simpleDraweeView2, "bindingGuild.guildsItemAvatar");
                GenericDraweeHierarchy hierarchy = simpleDraweeView2.getHierarchy();
                m.checkNotNullExpressionValue(hierarchy, "bindingGuild.guildsItemAvatar.hierarchy");
                f fVar = hierarchy.e;
                fVar.w = 50;
                if (fVar.v == 1) {
                    fVar.v = 0;
                }
            } else {
                SimpleDraweeView simpleDraweeView3 = this.bindingGuild.d;
                m.checkNotNullExpressionValue(simpleDraweeView3, "bindingGuild.guildsItemAvatar");
                simpleDraweeView3.getHierarchy().o(1, null);
                SimpleDraweeView simpleDraweeView4 = this.bindingGuild.d;
                m.checkNotNullExpressionValue(simpleDraweeView4, "bindingGuild.guildsItemAvatar");
                GenericDraweeHierarchy hierarchy2 = simpleDraweeView4.getHierarchy();
                m.checkNotNullExpressionValue(hierarchy2, "bindingGuild.guildsItemAvatar.hierarchy");
                f fVar2 = hierarchy2.e;
                fVar2.w = 0;
                if (fVar2.v == 1) {
                    fVar2.v = 0;
                }
            }
            if (guild.hasIcon()) {
                String forGuild$default = IconUtils.getForGuild$default(guild, null, z2, null, 10, null);
                StringBuilder R = a.R("?size=");
                R.append(this.imageRequestSize);
                String stringPlus = m.stringPlus(forGuild$default, R.toString());
                SimpleDraweeView simpleDraweeView5 = this.bindingGuild.d;
                m.checkNotNullExpressionValue(simpleDraweeView5, "bindingGuild.guildsItemAvatar");
                int i = this.imageRequestSize;
                MGImages.setImage$default(simpleDraweeView5, stringPlus, i, i, false, null, null, 112, null);
                SimpleDraweeView simpleDraweeView6 = this.bindingGuild.d;
                m.checkNotNullExpressionValue(simpleDraweeView6, "bindingGuild.guildsItemAvatar");
                simpleDraweeView6.setVisibility(0);
                return;
            }
            SimpleDraweeView simpleDraweeView7 = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView7, "bindingGuild.guildsItemAvatar");
            simpleDraweeView7.setVisibility(8);
        }

        private final void configureGuildIconPositioning(boolean z2) {
            SimpleDraweeView simpleDraweeView = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "bindingGuild.guildsItemAvatar");
            ViewGroup.LayoutParams layoutParams = simpleDraweeView.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
            SimpleDraweeView simpleDraweeView2 = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView2, "bindingGuild.guildsItemAvatar");
            ViewGroup.LayoutParams layoutParams3 = simpleDraweeView2.getLayoutParams();
            Objects.requireNonNull(layoutParams3, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
            FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) layoutParams3;
            if (z2) {
                int i = this.targetedAvatarMargin;
                layoutParams2.setMargins(i, i, 0, 0);
                int i2 = this.targetedAvatarSize;
                layoutParams2.height = i2;
                layoutParams2.width = i2;
            } else {
                layoutParams2.setMargins(0, 0, 0, 0);
                int i3 = this.defaultAvatarSize;
                layoutParams2.height = i3;
                layoutParams2.width = i3;
            }
            if (z2) {
                this.bindingGuild.e.setTextSize(1, TARGETED_AVATAR_TEXT_SIZE_DP);
                int i4 = this.targetedAvatarMargin;
                layoutParams4.setMargins(i4, i4, 0, 0);
                int i5 = this.targetedAvatarSize;
                layoutParams4.height = i5;
                layoutParams4.width = i5;
            } else {
                this.bindingGuild.e.setTextSize(2, DEFAULT_AVATAR_TEXT_SIZE_SP);
                layoutParams4.setMargins(0, 0, 0, 0);
                layoutParams4.height = -1;
                layoutParams4.width = -1;
            }
            SimpleDraweeView simpleDraweeView3 = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView3, "bindingGuild.guildsItemAvatar");
            simpleDraweeView3.setLayoutParams(layoutParams2);
            TextView textView = this.bindingGuild.e;
            m.checkNotNullExpressionValue(textView, "bindingGuild.guildsItemAvatarText");
            textView.setLayoutParams(layoutParams4);
        }

        @Override // com.discord.widgets.guilds.list.GuildsDragAndDropCallback.DraggableViewHolder
        public boolean canDrag() {
            GuildListItem.GuildItem guildItem;
            GuildListItem.GuildItem guildItem2 = this.data;
            return guildItem2 != null && !guildItem2.isLurkingGuild() && ((guildItem = this.data) == null || !guildItem.isPendingGuild());
        }

        public final void configure(final GuildListItem.GuildItem guildItem) {
            CharSequence d;
            int i;
            GuildListItem.GuildItem guildItem2;
            Guild guild;
            m.checkNotNullParameter(guildItem, "data");
            GuildListItem.GuildItem guildItem3 = this.data;
            String icon = (guildItem3 == null || (guild = guildItem3.getGuild()) == null) ? null : guild.getIcon();
            GuildListItem.GuildItem guildItem4 = this.data;
            boolean z2 = true;
            boolean z3 = guildItem4 == null || guildItem4.isSelected() != guildItem.isSelected();
            boolean z4 = icon == null || (m.areEqual(icon, guildItem.getGuild().getIcon()) ^ true);
            GuildListItem.GuildItem guildItem5 = this.data;
            boolean z5 = (guildItem5 != null ? Boolean.valueOf(guildItem5.isTargetedForFolderCreation()) : null) != null && ((guildItem2 = this.data) == null || guildItem2.isTargetedForFolderCreation() != guildItem.isTargetedForFolderCreation());
            this.data = guildItem;
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            view.setVisibility(0);
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            view2.setSelected(guildItem.isSelected());
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.list.GuildListViewHolder$GuildViewHolder$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    Function1 function1;
                    function1 = GuildListViewHolder.GuildViewHolder.this.onClicked;
                    function1.invoke(guildItem);
                }
            });
            View view3 = this.itemView;
            m.checkNotNullExpressionValue(view3, "itemView");
            ViewExtensions.setOnLongClickListenerConsumeClick(view3, new GuildListViewHolder$GuildViewHolder$configure$2(this, guildItem));
            boolean z6 = guildItem.getFolderId() != null || guildItem.isTargetedForFolderCreation();
            configureGuildIconBackground(guildItem.isSelected(), z6, guildItem.getGuild().hasIcon());
            if (z5) {
                configureGuildIconPositioning(guildItem.isTargetedForFolderCreation());
            }
            if (z4 || z3) {
                configureGuildIconImage(guildItem.getGuild(), guildItem.isSelected());
            }
            configureDraggingAlpha();
            TextView textView = this.bindingGuild.e;
            m.checkNotNullExpressionValue(textView, "bindingGuild.guildsItemAvatarText");
            textView.setText(guildItem.getGuild().hasIcon() ? null : guildItem.getGuild().getShortName());
            if (z3) {
                if (guildItem.isSelected()) {
                    TextView textView2 = this.bindingGuild.e;
                    m.checkNotNullExpressionValue(textView2, "bindingGuild.guildsItemAvatarText");
                    i = ColorCompat.getColor(textView2.getContext(), (int) R.color.white);
                } else {
                    TextView textView3 = this.bindingGuild.e;
                    m.checkNotNullExpressionValue(textView3, "bindingGuild.guildsItemAvatarText");
                    i = ColorCompat.getThemedColor(textView3.getContext(), (int) R.attr.colorHeaderPrimary);
                }
                this.bindingGuild.e.setTextColor(i);
            }
            TextView textView4 = this.bindingGuild.h;
            m.checkNotNullExpressionValue(textView4, "bindingGuild.guildsItemMentions");
            configureMentionsCount(textView4, guildItem.getMentionCount());
            int mentionCount = guildItem.getMentionCount();
            String i18nPluralString = mentionCount > 0 ? StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.guild_tooltip_a11y_label_mentions, mentionCount, Integer.valueOf(mentionCount)) : "";
            View view4 = this.itemView;
            m.checkNotNullExpressionValue(view4, "itemView");
            View view5 = this.itemView;
            m.checkNotNullExpressionValue(view5, "itemView");
            d = b.d(view5, R.string.guild_tooltip_a11y_label, new Object[]{i18nPluralString, guildItem.getGuild().getName()}, (r4 & 4) != 0 ? b.c.j : null);
            view4.setContentDescription(d);
            ImageView imageView = this.bindingGuild.i;
            int i2 = 8;
            imageView.setVisibility(guildItem.isConnectedToVoice() || (guildItem.getHasActiveStageChannel() && !guildItem.getHasActiveScheduledEvent()) ? 0 : 8);
            imageView.setActivated(guildItem.isConnectedToVoice());
            imageView.setImageResource((!guildItem.isConnectedToVoice() || guildItem.isConnectedToStageChannel()) ? R.drawable.ic_channel_stage_24dp_white : R.drawable.ic_volume_up_white_24dp);
            ImageView imageView2 = this.bindingGuild.g;
            m.checkNotNullExpressionValue(imageView2, "bindingGuild.guildsItemEventStatus");
            imageView2.setVisibility(!guildItem.isConnectedToVoice() && guildItem.getHasActiveScheduledEvent() ? 0 : 8);
            ImageView imageView3 = this.bindingGuild.c;
            m.checkNotNullExpressionValue(imageView3, "bindingGuild.guildsItemApplicationStream");
            imageView3.setVisibility(!guildItem.isConnectedToVoice() && guildItem.getHasOngoingApplicationStream() ? 0 : 8);
            ImageView imageView4 = this.binding.c;
            m.checkNotNullExpressionValue(imageView4, "binding.guildsItemUnread");
            imageView4.setVisibility(guildItem.isUnread() ? 0 : 8);
            ImageView imageView5 = this.binding.f2439b;
            m.checkNotNullExpressionValue(imageView5, "binding.guildsItemSelected");
            if (guildItem.isSelected()) {
                i2 = 0;
            }
            imageView5.setVisibility(i2);
            if (mentionCount <= 0) {
                z2 = false;
            }
            configureApplicationStatus(null, z6, z2);
        }

        public final Long getFolderId() {
            GuildListItem.GuildItem guildItem = this.data;
            if (guildItem != null) {
                return guildItem.getFolderId();
            }
            return null;
        }

        public final Boolean isLastGuildInFolder() {
            GuildListItem.GuildItem guildItem = this.data;
            if (guildItem != null) {
                return guildItem.isLastGuildInFolder();
            }
            return null;
        }

        public final boolean isTargetedForFolderCreation() {
            GuildListItem.GuildItem guildItem = this.data;
            return guildItem != null && guildItem.isTargetedForFolderCreation();
        }

        @Override // com.discord.widgets.guilds.list.GuildListViewHolder
        public void onDragEnded(boolean z2) {
            GuildListViewHolder.super.onDragEnded(z2);
            if (z2) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                view.setVisibility(8);
            }
            this.isDragging = false;
            configureDraggingAlpha();
        }

        @Override // com.discord.widgets.guilds.list.GuildListViewHolder
        public void onDragStarted() {
            GuildListViewHolder.super.onDragStarted();
            this.isDragging = true;
            configureDraggingAlpha();
        }
    }

    /* compiled from: GuildListViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0007\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0007¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\"\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\"\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\tR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$PrivateChannelViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;", "data", "", "configure", "(Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;)V", "Lkotlin/Function1;", "onClicked", "Lkotlin/jvm/functions/Function1;", "onLongPressed", "Lcom/discord/databinding/WidgetGuildsListItemDmBinding;", "binding", "Lcom/discord/databinding/WidgetGuildsListItemDmBinding;", "Landroid/view/View;", "view", HookHelper.constructorName, "(Landroid/view/View;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PrivateChannelViewHolder extends GuildListViewHolder {
        private final WidgetGuildsListItemDmBinding binding;
        private final Function1<GuildListItem.PrivateChannelItem, Unit> onClicked;
        private final Function1<GuildListItem.PrivateChannelItem, Unit> onLongPressed;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public PrivateChannelViewHolder(View view, Function1<? super GuildListItem.PrivateChannelItem, Unit> function1, Function1<? super GuildListItem.PrivateChannelItem, Unit> function12) {
            super(view, null);
            m.checkNotNullParameter(view, "view");
            m.checkNotNullParameter(function1, "onClicked");
            m.checkNotNullParameter(function12, "onLongPressed");
            this.onClicked = function1;
            this.onLongPressed = function12;
            WidgetGuildsListItemDmBinding a = WidgetGuildsListItemDmBinding.a(view);
            m.checkNotNullExpressionValue(a, "WidgetGuildsListItemDmBinding.bind(view)");
            this.binding = a;
        }

        public final void configure(final GuildListItem.PrivateChannelItem privateChannelItem) {
            CharSequence d;
            m.checkNotNullParameter(privateChannelItem, "data");
            int mentionCount = privateChannelItem.getMentionCount();
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.guildsItemDmCount");
            configureMentionsCount(textView, mentionCount);
            String i18nPluralString = mentionCount > 0 ? StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.dm_tooltip_a11y_label_mentions, mentionCount, Integer.valueOf(mentionCount)) : "";
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            d = b.d(view2, R.string.dm_tooltip_a11y_label, new Object[]{ChannelUtils.c(privateChannelItem.getChannel()), i18nPluralString}, (r4 & 4) != 0 ? b.c.j : null);
            view.setContentDescription(d);
            SimpleDraweeView simpleDraweeView = this.binding.f2436b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildsItemDmAvatar");
            IconUtils.setIcon$default(simpleDraweeView, privateChannelItem.getChannel(), (int) R.dimen.avatar_size_large, (MGImages.ChangeDetector) null, 8, (Object) null);
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.list.GuildListViewHolder$PrivateChannelViewHolder$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    Function1 function1;
                    function1 = GuildListViewHolder.PrivateChannelViewHolder.this.onClicked;
                    function1.invoke(privateChannelItem);
                }
            });
            View view3 = this.itemView;
            m.checkNotNullExpressionValue(view3, "itemView");
            ViewExtensions.setOnLongClickListenerConsumeClick(view3, new GuildListViewHolder$PrivateChannelViewHolder$configure$2(this, privateChannelItem));
        }
    }

    /* compiled from: GuildListViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$SimpleViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "Landroid/view/View;", "view", HookHelper.constructorName, "(Landroid/view/View;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SimpleViewHolder extends GuildListViewHolder {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public SimpleViewHolder(View view) {
            super(view, null);
            m.checkNotNullParameter(view, "view");
        }
    }

    /* compiled from: GuildListViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\f\u0010\rJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/guilds/list/GuildListViewHolder$SpaceViewHolder;", "Lcom/discord/widgets/guilds/list/GuildListViewHolder;", "", "height", "", "configure", "(I)V", "Landroid/view/View;", "view", "Landroid/view/View;", "getView", "()Landroid/view/View;", HookHelper.constructorName, "(Landroid/view/View;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SpaceViewHolder extends GuildListViewHolder {
        private final View view;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public SpaceViewHolder(View view) {
            super(view, null);
            m.checkNotNullParameter(view, "view");
            this.view = view;
        }

        public final void configure(int i) {
            ViewGroup.LayoutParams layoutParams = this.view.getLayoutParams();
            layoutParams.height = i;
            this.view.setLayoutParams(layoutParams);
        }

        public final View getView() {
            return this.view;
        }
    }

    public /* synthetic */ GuildListViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }

    public final void configureMentionsCount(TextView textView, int i) {
        m.checkNotNullParameter(textView, "textView");
        if (i < 1) {
            textView.setVisibility(8);
            return;
        }
        textView.setVisibility(0);
        textView.setText(String.valueOf(i));
    }

    public void onDragEnded(boolean z2) {
    }

    public void onDragStarted() {
    }

    private GuildListViewHolder(View view) {
        super(view);
    }
}
