package com.discord.widgets.guilds.list;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewStub;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGuildsListBinding;
import com.discord.stores.StoreConnectionOpen;
import com.discord.stores.StoreStream;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.list.WidgetChannelListUnreads;
import com.discord.widgets.channels.list.WidgetChannelsListItemChannelActions;
import com.discord.widgets.guilds.contextmenu.WidgetFolderContextMenu;
import com.discord.widgets.guilds.contextmenu.WidgetGuildContextMenu;
import com.discord.widgets.guilds.create.CreateGuildTrigger;
import com.discord.widgets.guilds.list.GuildListItem;
import com.discord.widgets.guilds.list.WidgetGuildListAdapter;
import com.discord.widgets.guilds.list.WidgetGuildsListViewModel;
import com.discord.widgets.home.WidgetHome;
import com.discord.widgets.hubs.HubEmailArgs;
import com.discord.widgets.hubs.WidgetHubEmailFlow;
import com.discord.widgets.nux.WidgetGuildTemplates;
import com.discord.widgets.nux.WidgetNavigationHelp;
import com.discord.widgets.tabs.BottomNavViewObserver;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGuildsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u0002:\u0001PB\u0007¢\u0006\u0004\bO\u0010\tJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000e\u0010\tJ\u000f\u0010\u000f\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000f\u0010\tJ\u000f\u0010\u0010\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0010\u0010\tJ\u000f\u0010\u0011\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0011\u0010\tJ\u000f\u0010\u0012\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0012\u0010\tJ\u0017\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u001b\u0010\u001a\u001a\u00020\u00052\n\u0010\u0019\u001a\u00060\u0017j\u0002`\u0018H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u001cH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010\"\u001a\u00020\u00052\u0006\u0010!\u001a\u00020 H\u0002¢\u0006\u0004\b\"\u0010#J\u0017\u0010&\u001a\u00020\u00052\u0006\u0010%\u001a\u00020$H\u0002¢\u0006\u0004\b&\u0010'J\u0017\u0010*\u001a\u00020\u00052\u0006\u0010)\u001a\u00020(H\u0016¢\u0006\u0004\b*\u0010+J\u000f\u0010,\u001a\u00020\u0005H\u0016¢\u0006\u0004\b,\u0010\tJ\u001f\u0010.\u001a\u00020\u00052\u0006\u0010)\u001a\u00020(2\u0006\u0010\u0014\u001a\u00020-H\u0016¢\u0006\u0004\b.\u0010/J\u001f\u00100\u001a\u00020\u00052\u0006\u0010)\u001a\u00020(2\u0006\u0010\u0014\u001a\u00020-H\u0016¢\u0006\u0004\b0\u0010/J\u000f\u00101\u001a\u00020\u0005H\u0016¢\u0006\u0004\b1\u0010\tJ\u0017\u00104\u001a\u00020\u00052\u0006\u00103\u001a\u000202H\u0016¢\u0006\u0004\b4\u00105J\u000f\u00106\u001a\u00020$H\u0016¢\u0006\u0004\b6\u00107R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:R\u001d\u0010@\u001a\u00020;8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010?R\u0018\u0010B\u001a\u0004\u0018\u00010A8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010CR\u0016\u0010E\u001a\u00020D8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bE\u0010FR\u0018\u0010G\u001a\u0004\u0018\u00010(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bG\u0010HR\u001d\u0010N\u001a\u00020I8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bJ\u0010K\u001a\u0004\bL\u0010M¨\u0006Q"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsList;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$InteractionListener;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;)V", "configureBottomNavSpace", "()V", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;)V", "setupRecycler", "showCreateGuild", "showHubVerification", "showHelp", "focusFirstElement", "Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;", "item", "announceFolderToggle", "(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "showChannelActions", "(J)V", "", "unavailableGuildCount", "showUnavailableGuildsToast", "(I)V", "Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint;", "addGuildHint", "configureAddGuildHint", "(Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint;)V", "", "wasAcknowledged", "dismissAddGuildHint", "(Z)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "Lcom/discord/widgets/guilds/list/GuildListItem;", "onItemClicked", "(Landroid/view/View;Lcom/discord/widgets/guilds/list/GuildListItem;)V", "onItemLongPressed", "onItemMoved", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;", "operation", "onOperation", "(Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;)V", "onDrop", "()Z", "Lcom/discord/widgets/tabs/BottomNavViewObserver;", "bottomNavViewObserver", "Lcom/discord/widgets/tabs/BottomNavViewObserver;", "Lcom/discord/databinding/WidgetGuildsListBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildsListBinding;", "binding", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;", "guildListUnreads", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter;", "adapter", "Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter;", "guildListAddHint", "Landroid/view/View;", "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;", "viewModel", HookHelper.constructorName, "AddGuildHint", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildsList extends AppFragment implements WidgetGuildListAdapter.InteractionListener {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildsList.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildsListBinding;", 0)};
    private WidgetGuildListAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildsList$binding$2.INSTANCE, null, 2, null);
    private final BottomNavViewObserver bottomNavViewObserver = BottomNavViewObserver.Companion.getINSTANCE();
    private View guildListAddHint;
    private WidgetChannelListUnreads guildListUnreads;
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetGuildsList.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\b\u0082\b\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0019\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0011\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0006\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0007\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint;", "", "", "component1", "()Z", "component2", "isEligible", "isAddGuildHint", "copy", "(ZZ)Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(ZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AddGuildHint {
        public static final Companion Companion = new Companion(null);
        private final boolean isAddGuildHint;
        private final boolean isEligible;

        /* compiled from: WidgetGuildsList.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002¢\u0006\u0004\b\u0007\u0010\u0005¨\u0006\n"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint;", "get", "()Lrx/Observable;", "", "getDismissAction", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<AddGuildHint> get() {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable x2 = Observable.i(companion.getNux().getNuxState(), StoreConnectionOpen.observeConnectionOpen$default(companion.getConnectionOpen(), false, 1, null), companion.getChannels().observeGuildAndPrivateChannels(), WidgetGuildsList$AddGuildHint$Companion$get$1.INSTANCE).x(WidgetGuildsList$AddGuildHint$Companion$get$2.INSTANCE);
                m.checkNotNullExpressionValue(x2, "Observable\n             ….filter { it.isEligible }");
                Observable<AddGuildHint> L = ObservableExtensionsKt.takeSingleUntilTimeout$default(x2, 0L, false, 3, null).L(WidgetGuildsList$AddGuildHint$Companion$get$3.INSTANCE);
                m.checkNotNullExpressionValue(L, "Observable\n             … = false)\n              }");
                return L;
            }

            public final Observable<Boolean> getDismissAction() {
                Observable F = StoreStream.Companion.getNavigation().observeLeftPanelState().F(WidgetGuildsList$AddGuildHint$Companion$getDismissAction$1.INSTANCE);
                m.checkNotNullExpressionValue(F, "StoreStream\n            …te == PanelState.Closed }");
                return F;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public AddGuildHint(boolean z2, boolean z3) {
            this.isEligible = z2;
            this.isAddGuildHint = z3;
        }

        public static /* synthetic */ AddGuildHint copy$default(AddGuildHint addGuildHint, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = addGuildHint.isEligible;
            }
            if ((i & 2) != 0) {
                z3 = addGuildHint.isAddGuildHint;
            }
            return addGuildHint.copy(z2, z3);
        }

        public final boolean component1() {
            return this.isEligible;
        }

        public final boolean component2() {
            return this.isAddGuildHint;
        }

        public final AddGuildHint copy(boolean z2, boolean z3) {
            return new AddGuildHint(z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AddGuildHint)) {
                return false;
            }
            AddGuildHint addGuildHint = (AddGuildHint) obj;
            return this.isEligible == addGuildHint.isEligible && this.isAddGuildHint == addGuildHint.isAddGuildHint;
        }

        public int hashCode() {
            boolean z2 = this.isEligible;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.isAddGuildHint;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return i4 + i;
        }

        public final boolean isAddGuildHint() {
            return this.isAddGuildHint;
        }

        public final boolean isEligible() {
            return this.isEligible;
        }

        public String toString() {
            StringBuilder R = a.R("AddGuildHint(isEligible=");
            R.append(this.isEligible);
            R.append(", isAddGuildHint=");
            return a.M(R, this.isAddGuildHint, ")");
        }

        public /* synthetic */ AddGuildHint(boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(z2, (i & 2) != 0 ? false : z3);
        }
    }

    public WidgetGuildsList() {
        super(R.layout.widget_guilds_list);
        WidgetGuildsList$viewModel$2 widgetGuildsList$viewModel$2 = WidgetGuildsList$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGuildsListViewModel.class), new WidgetGuildsList$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildsList$viewModel$2));
    }

    public static final /* synthetic */ WidgetGuildListAdapter access$getAdapter$p(WidgetGuildsList widgetGuildsList) {
        WidgetGuildListAdapter widgetGuildListAdapter = widgetGuildsList.adapter;
        if (widgetGuildListAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        return widgetGuildListAdapter;
    }

    private final void announceFolderToggle(GuildListItem.FolderItem folderItem) {
        CharSequence e;
        Object[] objArr = new Object[2];
        e = b.e(this, folderItem.isOpen() ? R.string.collapsed : R.string.expanded, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        objArr[0] = e;
        String name = folderItem.getName();
        if (name == null) {
            name = "";
        }
        objArr[1] = name;
        AccessibilityUtils.INSTANCE.sendAnnouncement(requireContext(), a.N(objArr, 2, "%s, %s", "java.lang.String.format(this, *args)"));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureAddGuildHint(AddGuildHint addGuildHint) {
        if (addGuildHint.isAddGuildHint()) {
            StoreStream.Companion.getNux().updateNux(WidgetGuildsList$configureAddGuildHint$1.INSTANCE);
        }
        if (addGuildHint.isEligible()) {
            AnalyticsTracker.INSTANCE.showFirstServerTipTutorial();
            View view = this.guildListAddHint;
            if (view != null) {
                ViewExtensions.fadeIn$default(view, 0L, null, null, null, 15, null);
            }
        }
    }

    private final void configureBottomNavSpace() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(this.bottomNavViewObserver.observeHeight(), this, null, 2, null), WidgetGuildsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildsList$configureBottomNavSpace$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetGuildsListViewModel.ViewState viewState) {
        View view;
        if (viewState instanceof WidgetGuildsListViewModel.ViewState.Loaded) {
            WidgetGuildListAdapter widgetGuildListAdapter = this.adapter;
            if (widgetGuildListAdapter == null) {
                m.throwUninitializedPropertyAccessException("adapter");
            }
            WidgetGuildsListViewModel.ViewState.Loaded loaded = (WidgetGuildsListViewModel.ViewState.Loaded) viewState;
            widgetGuildListAdapter.setItems(loaded.getItems(), !loaded.getWasDragResult());
            WidgetChannelListUnreads widgetChannelListUnreads = this.guildListUnreads;
            if (widgetChannelListUnreads != null) {
                widgetChannelListUnreads.onDatasetChanged(loaded.getItems());
            }
            if (loaded.getHasChannels() && (view = this.guildListAddHint) != null && view.getVisibility() == 0) {
                dismissAddGuildHint(false);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void dismissAddGuildHint(boolean z2) {
        if (z2) {
            View view = this.guildListAddHint;
            if (view != null) {
                ViewExtensions.fadeOut$default(view, 0L, null, null, 7, null);
            }
            AnalyticsTracker.INSTANCE.closeFirstServerTipTutorial(true);
            return;
        }
        View view2 = this.guildListAddHint;
        if (view2 != null && view2.getVisibility() == 0) {
            View view3 = this.guildListAddHint;
            if (view3 != null) {
                view3.setVisibility(8);
            }
            AnalyticsTracker.INSTANCE.closeFirstServerTipTutorial(false);
        }
    }

    private final void focusFirstElement() {
        View childAt = getBinding().f2435b.getChildAt(0);
        if (childAt != null) {
            childAt.sendAccessibilityEvent(8);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGuildsListBinding getBinding() {
        return (WidgetGuildsListBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final WidgetGuildsListViewModel getViewModel() {
        return (WidgetGuildsListViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(WidgetGuildsListViewModel.Event event) {
        if (event instanceof WidgetGuildsListViewModel.Event.ShowChannelActions) {
            showChannelActions(((WidgetGuildsListViewModel.Event.ShowChannelActions) event).getChannelId());
        } else if (event instanceof WidgetGuildsListViewModel.Event.ShowUnavailableGuilds) {
            showUnavailableGuildsToast(((WidgetGuildsListViewModel.Event.ShowUnavailableGuilds) event).getUnavailableGuildCount());
        } else if (event instanceof WidgetGuildsListViewModel.Event.AnnounceFolderToggleForAccessibility) {
            announceFolderToggle(((WidgetGuildsListViewModel.Event.AnnounceFolderToggleForAccessibility) event).getItem());
        } else if (m.areEqual(event, WidgetGuildsListViewModel.Event.ShowCreateGuild.INSTANCE)) {
            showCreateGuild();
        } else if (m.areEqual(event, WidgetGuildsListViewModel.Event.ShowHubVerification.INSTANCE)) {
            showHubVerification();
        } else if (m.areEqual(event, WidgetGuildsListViewModel.Event.ShowHelp.INSTANCE)) {
            showHelp();
        } else if (m.areEqual(event, WidgetGuildsListViewModel.Event.FocusFirstElement.INSTANCE)) {
            focusFirstElement();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void setupRecycler() {
        RecyclerView recyclerView = getBinding().f2435b;
        m.checkNotNullExpressionValue(recyclerView, "binding.guildList");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext(), 1, false);
        WidgetGuildListAdapter widgetGuildListAdapter = new WidgetGuildListAdapter(linearLayoutManager, this);
        this.adapter = widgetGuildListAdapter;
        if (widgetGuildListAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetGuildListAdapter.setHasStableIds(true);
        RecyclerView recyclerView2 = getBinding().f2435b;
        m.checkNotNullExpressionValue(recyclerView2, "binding.guildList");
        recyclerView2.setItemAnimator(null);
        RecyclerView recyclerView3 = getBinding().f2435b;
        m.checkNotNullExpressionValue(recyclerView3, "binding.guildList");
        recyclerView3.setLayoutManager(linearLayoutManager);
        RecyclerView recyclerView4 = getBinding().f2435b;
        m.checkNotNullExpressionValue(recyclerView4, "binding.guildList");
        WidgetGuildListAdapter widgetGuildListAdapter2 = this.adapter;
        if (widgetGuildListAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        recyclerView4.setAdapter(widgetGuildListAdapter2);
        WidgetGuildListAdapter widgetGuildListAdapter3 = this.adapter;
        if (widgetGuildListAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        new ItemTouchHelper(new GuildsDragAndDropCallback(widgetGuildListAdapter3)).attachToRecyclerView(getBinding().f2435b);
        RecyclerView recyclerView5 = getBinding().f2435b;
        Drawable drawable = ContextCompat.getDrawable(requireContext(), DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.bg_folder_no_children, 0, 2, (Object) null));
        m.checkNotNull(drawable);
        m.checkNotNullExpressionValue(drawable, "ContextCompat.getDrawabl…children)\n            )!!");
        Drawable drawable2 = ContextCompat.getDrawable(requireContext(), DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.bg_folder_tintable_no_children, 0, 2, (Object) null));
        m.checkNotNull(drawable2);
        m.checkNotNullExpressionValue(drawable2, "ContextCompat.getDrawabl…children)\n            )!!");
        Drawable drawable3 = ContextCompat.getDrawable(requireContext(), DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.bg_folder_with_children, 0, 2, (Object) null));
        m.checkNotNull(drawable3);
        m.checkNotNullExpressionValue(drawable3, "ContextCompat.getDrawabl…children)\n            )!!");
        recyclerView5.addItemDecoration(new FolderItemDecoration(drawable, drawable2, drawable3, requireContext().getResources().getDimensionPixelSize(R.dimen.avatar_size_large)));
    }

    private final void showChannelActions(long j) {
        WidgetChannelsListItemChannelActions.Companion companion = WidgetChannelsListItemChannelActions.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager, j);
    }

    private final void showCreateGuild() {
        dismissAddGuildHint(true);
        WidgetGuildTemplates.Companion.launch(requireContext(), CreateGuildTrigger.IN_APP, false);
    }

    private final void showHelp() {
        dismissAddGuildHint(true);
        WidgetNavigationHelp.Companion companion = WidgetNavigationHelp.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager);
    }

    private final void showHubVerification() {
        j.d(requireContext(), WidgetHubEmailFlow.class, new HubEmailArgs(null, 0, null, 7, null));
    }

    private final void showUnavailableGuildsToast(int i) {
        CharSequence g;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        g = b.g(StringResourceUtilsKt.getQuantityString(resources, requireContext(), (int) R.plurals.partial_outage_count, i, Integer.valueOf(i)), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
        b.a.d.m.h(getContext(), g, 0, null, 12);
    }

    @Override // com.discord.widgets.guilds.list.WidgetGuildListAdapter.InteractionListener
    public boolean onDrop() {
        return getViewModel().onDrop();
    }

    @Override // com.discord.widgets.guilds.list.WidgetGuildListAdapter.InteractionListener
    public void onItemClicked(View view, GuildListItem guildListItem) {
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(guildListItem, "item");
        WidgetGuildsListViewModel viewModel = getViewModel();
        Context requireContext = requireContext();
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        viewModel.onItemClicked(guildListItem, requireContext, parentFragmentManager);
    }

    @Override // com.discord.widgets.guilds.list.WidgetGuildListAdapter.InteractionListener
    public void onItemLongPressed(View view, GuildListItem guildListItem) {
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(guildListItem, "item");
        boolean z2 = guildListItem instanceof GuildListItem.GuildItem;
        if (z2 || (guildListItem instanceof GuildListItem.FolderItem)) {
            int[] iArr = new int[2];
            view.getLocationInWindow(iArr);
            PointF pointF = new PointF(iArr[0] + DimenUtils.dpToPixels(64), iArr[1]);
            if (z2) {
                WidgetGuildContextMenu.Companion companion = WidgetGuildContextMenu.Companion;
                FragmentActivity requireActivity = requireActivity();
                m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                companion.show(requireActivity, pointF, ((GuildListItem.GuildItem) guildListItem).getGuild().getId());
            } else if (guildListItem instanceof GuildListItem.FolderItem) {
                WidgetFolderContextMenu.Companion companion2 = WidgetFolderContextMenu.Companion;
                FragmentActivity requireActivity2 = requireActivity();
                m.checkNotNullExpressionValue(requireActivity2, "requireActivity()");
                companion2.show(requireActivity2, pointF, ((GuildListItem.FolderItem) guildListItem).getFolderId());
            }
        } else {
            getViewModel().onItemLongPressed(guildListItem);
        }
    }

    @Override // com.discord.widgets.guilds.list.WidgetGuildListAdapter.InteractionListener
    public void onItemMoved() {
        WidgetGuildContextMenu.Companion companion = WidgetGuildContextMenu.Companion;
        FragmentActivity requireActivity = requireActivity();
        m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        companion.hide(requireActivity, true);
        WidgetFolderContextMenu.Companion companion2 = WidgetFolderContextMenu.Companion;
        FragmentActivity requireActivity2 = requireActivity();
        m.checkNotNullExpressionValue(requireActivity2, "requireActivity()");
        companion2.hide(requireActivity2, true);
    }

    @Override // com.discord.widgets.guilds.list.WidgetGuildListAdapter.InteractionListener
    public void onOperation(WidgetGuildListAdapter.Operation operation) {
        m.checkNotNullParameter(operation, "operation");
        if (operation instanceof WidgetGuildListAdapter.Operation.MoveAbove) {
            WidgetGuildListAdapter.Operation.MoveAbove moveAbove = (WidgetGuildListAdapter.Operation.MoveAbove) operation;
            getViewModel().moveAbove(moveAbove.getFromPosition(), moveAbove.getTargetPosition());
        } else if (operation instanceof WidgetGuildListAdapter.Operation.MoveBelow) {
            WidgetGuildListAdapter.Operation.MoveBelow moveBelow = (WidgetGuildListAdapter.Operation.MoveBelow) operation;
            getViewModel().moveBelow(moveBelow.getFromPosition(), moveBelow.getTargetPosition());
        } else if (operation instanceof WidgetGuildListAdapter.Operation.TargetOperation) {
            WidgetGuildListAdapter.Operation.TargetOperation targetOperation = (WidgetGuildListAdapter.Operation.TargetOperation) operation;
            getViewModel().target(targetOperation.getFromPosition(), targetOperation.getTargetPosition());
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildsList$onResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().listenForEvents(), this, null, 2, null), WidgetGuildsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildsList$onResume$2(this));
        AddGuildHint.Companion companion = AddGuildHint.Companion;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.get(), this, null, 2, null), WidgetGuildsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildsList$onResume$3(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.getDismissAction(), this, null, 2, null), WidgetGuildsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildsList$onResume$4(this));
        configureBottomNavSpace();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setupRecycler();
        ViewStub viewStub = getBinding().c;
        m.checkNotNullExpressionValue(viewStub, "binding.guildListUnreadsStub");
        RecyclerView recyclerView = getBinding().f2435b;
        m.checkNotNullExpressionValue(recyclerView, "binding.guildList");
        WidgetChannelListUnreads widgetChannelListUnreads = new WidgetChannelListUnreads(viewStub, recyclerView, null, new WidgetGuildsList$onViewBound$1(this), 0, 0, false, 116, null);
        this.guildListUnreads = widgetChannelListUnreads;
        if (widgetChannelListUnreads != null) {
            widgetChannelListUnreads.setMentionResId(R.string._new);
        }
        WidgetChannelListUnreads widgetChannelListUnreads2 = this.guildListUnreads;
        if (widgetChannelListUnreads2 != null) {
            widgetChannelListUnreads2.setUnreadsEnabled(false);
        }
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof WidgetHome)) {
            parentFragment = null;
        }
        WidgetHome widgetHome = (WidgetHome) parentFragment;
        if (widgetHome != null) {
            widgetHome.setOnGuildListAddHintCreate(new WidgetGuildsList$onViewBound$2(this));
        }
    }
}
