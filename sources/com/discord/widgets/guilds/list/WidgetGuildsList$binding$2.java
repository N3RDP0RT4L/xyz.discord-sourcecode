package com.discord.widgets.guilds.list;

import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetGuildsListBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildsListBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildsListBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildsList$binding$2 extends k implements Function1<View, WidgetGuildsListBinding> {
    public static final WidgetGuildsList$binding$2 INSTANCE = new WidgetGuildsList$binding$2();

    public WidgetGuildsList$binding$2() {
        super(1, WidgetGuildsListBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildsListBinding;", 0);
    }

    public final WidgetGuildsListBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_list;
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.guild_list);
        if (recyclerView != null) {
            i = R.id.guild_list_unreads_stub;
            ViewStub viewStub = (ViewStub) view.findViewById(R.id.guild_list_unreads_stub);
            if (viewStub != null) {
                return new WidgetGuildsListBinding((RelativeLayout) view, recyclerView, viewStub);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
