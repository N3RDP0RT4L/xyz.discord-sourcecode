package com.discord.widgets.guilds.list;

import com.discord.stores.StoreGuildsSorted;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: _Sequences.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0006\b\u0000\u0010\u0000\u0018\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"R", "", "it", "", "invoke", "(Ljava/lang/Object;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildsListViewModel$handleStoreState$$inlined$filterIsInstance$1 extends o implements Function1<Object, Boolean> {
    public static final WidgetGuildsListViewModel$handleStoreState$$inlined$filterIsInstance$1 INSTANCE = new WidgetGuildsListViewModel$handleStoreState$$inlined$filterIsInstance$1();

    public WidgetGuildsListViewModel$handleStoreState$$inlined$filterIsInstance$1() {
        super(1);
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [boolean, java.lang.Boolean] */
    @Override // kotlin.jvm.functions.Function1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2(Object obj) {
        return obj instanceof StoreGuildsSorted.Entry.SingletonGuild;
    }
}
