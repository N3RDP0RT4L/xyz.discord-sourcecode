package com.discord.widgets.guilds.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "layoutRes", "Landroid/view/View;", "kotlin.jvm.PlatformType", "invoke", "(I)Landroid/view/View;", "inflate"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildListAdapter$onCreateViewHolder$1 extends o implements Function1<Integer, View> {
    public final /* synthetic */ ViewGroup $parent;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildListAdapter$onCreateViewHolder$1(ViewGroup viewGroup) {
        super(1);
        this.$parent = viewGroup;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ View invoke(Integer num) {
        return invoke(num.intValue());
    }

    public final View invoke(int i) {
        return LayoutInflater.from(this.$parent.getContext()).inflate(i, this.$parent, false);
    }
}
