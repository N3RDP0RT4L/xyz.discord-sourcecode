package com.discord.widgets.guilds.list;

import andhook.lib.HookHelper;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.widgets.guilds.list.GuildListViewHolder;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: FolderItemDecoration.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B'\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0014\u001a\u00020\u000f\u0012\u0006\u0010\u0015\u001a\u00020\u000f\u0012\u0006\u0010\u0016\u001a\u00020\u0006¢\u0006\u0004\b\u0017\u0010\u0018J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ'\u0010\r\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0011R\u0016\u0010\u0015\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0011¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/guilds/list/FolderItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "Landroid/graphics/Canvas;", "canvas", "Landroidx/recyclerview/widget/RecyclerView;", "parent", "", "drawBackgroundForInitialFolder", "(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;)I", "c", "Landroidx/recyclerview/widget/RecyclerView$State;", "state", "", "onDraw", "(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V", "Landroid/graphics/drawable/Drawable;", "drawableNoChildren", "Landroid/graphics/drawable/Drawable;", "halfSize", "I", "tintableDrawableNoChildren", "drawableWithChildren", "sizePx", HookHelper.constructorName, "(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FolderItemDecoration extends RecyclerView.ItemDecoration {
    public static final Companion Companion = new Companion(null);
    private static final int EXTRA_PADDING_PX = 200;
    private final Drawable drawableNoChildren;
    private final Drawable drawableWithChildren;
    private final int halfSize;
    private final Drawable tintableDrawableNoChildren;

    /* compiled from: FolderItemDecoration.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guilds/list/FolderItemDecoration$Companion;", "", "", "EXTRA_PADDING_PX", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public FolderItemDecoration(Drawable drawable, Drawable drawable2, Drawable drawable3, int i) {
        m.checkNotNullParameter(drawable, "drawableNoChildren");
        m.checkNotNullParameter(drawable2, "tintableDrawableNoChildren");
        m.checkNotNullParameter(drawable3, "drawableWithChildren");
        this.drawableNoChildren = drawable;
        this.tintableDrawableNoChildren = drawable2;
        this.drawableWithChildren = drawable3;
        this.halfSize = i / 2;
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0045  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final int drawBackgroundForInitialFolder(android.graphics.Canvas r9, androidx.recyclerview.widget.RecyclerView r10) {
        /*
            r8 = this;
            r0 = 0
            android.view.View r1 = r10.getChildAt(r0)
            androidx.recyclerview.widget.RecyclerView$ViewHolder r2 = r10.getChildViewHolder(r1)
            boolean r3 = r2 instanceof com.discord.widgets.guilds.list.GuildListViewHolder.GuildViewHolder
            r4 = 1
            if (r3 == 0) goto L42
            com.discord.widgets.guilds.list.GuildListViewHolder$GuildViewHolder r2 = (com.discord.widgets.guilds.list.GuildListViewHolder.GuildViewHolder) r2
            java.lang.Long r3 = r2.getFolderId()
            if (r3 == 0) goto L42
            java.lang.Long r2 = r2.getFolderId()
            r3 = 1
            r5 = 1
        L1c:
            if (r3 == 0) goto L43
            int r3 = r10.getChildCount()
            if (r5 >= r3) goto L43
            android.view.View r3 = r10.getChildAt(r5)
            androidx.recyclerview.widget.RecyclerView$ViewHolder r3 = r10.getChildViewHolder(r3)
            boolean r6 = r3 instanceof com.discord.widgets.guilds.list.GuildListViewHolder.GuildViewHolder
            if (r6 == 0) goto L40
            com.discord.widgets.guilds.list.GuildListViewHolder$GuildViewHolder r3 = (com.discord.widgets.guilds.list.GuildListViewHolder.GuildViewHolder) r3
            java.lang.Long r3 = r3.getFolderId()
            boolean r3 = d0.z.d.m.areEqual(r3, r2)
            if (r3 == 0) goto L40
            int r5 = r5 + 1
            r3 = 1
            goto L1c
        L40:
            r3 = 0
            goto L1c
        L42:
            r5 = 0
        L43:
            if (r5 <= 0) goto Lac
            java.lang.String r2 = "firstView"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
            int r2 = r1.getRight()
            int r3 = r1.getLeft()
            int r3 = r3 + r2
            int r3 = r3 / 2
            int r2 = r1.getBottom()
            int r6 = r1.getTop()
            int r6 = r6 + r2
            int r6 = r6 / 2
            int r2 = r10.getChildCount()
            if (r2 != r5) goto L68
            r2 = 1
            goto L69
        L68:
            r2 = 0
        L69:
            int r7 = r10.getChildCount()
            int r7 = r7 - r4
            android.view.View r7 = r10.getChildAt(r7)
            androidx.recyclerview.widget.RecyclerView$ViewHolder r10 = r10.getChildViewHolder(r7)
            if (r2 == 0) goto L8b
            boolean r2 = r10 instanceof com.discord.widgets.guilds.list.GuildListViewHolder.GuildViewHolder
            if (r2 == 0) goto L8b
            com.discord.widgets.guilds.list.GuildListViewHolder$GuildViewHolder r10 = (com.discord.widgets.guilds.list.GuildListViewHolder.GuildViewHolder) r10
            java.lang.Boolean r10 = r10.isLastGuildInFolder()
            java.lang.Boolean r2 = java.lang.Boolean.FALSE
            boolean r10 = d0.z.d.m.areEqual(r10, r2)
            if (r10 == 0) goto L8b
            r0 = 1
        L8b:
            int r10 = r1.getHeight()
            int r1 = r8.halfSize
            int r2 = r6 + r1
            int r4 = r5 + (-1)
            int r4 = r4 * r10
            int r4 = r4 + r2
            if (r0 == 0) goto L9c
            int r4 = r4 + 200
        L9c:
            android.graphics.drawable.Drawable r10 = r8.drawableWithChildren
            int r0 = r3 - r1
            int r6 = r6 - r1
            int r6 = r6 + (-200)
            int r3 = r3 + r1
            r10.setBounds(r0, r6, r3, r4)
            android.graphics.drawable.Drawable r10 = r8.drawableWithChildren
            r10.draw(r9)
        Lac:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.list.FolderItemDecoration.drawBackgroundForInitialFolder(android.graphics.Canvas, androidx.recyclerview.widget.RecyclerView):int");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        Drawable drawable;
        m.checkNotNullParameter(canvas, "c");
        m.checkNotNullParameter(recyclerView, "parent");
        m.checkNotNullParameter(state, "state");
        if (recyclerView.getChildCount() >= 1) {
            int childCount = recyclerView.getChildCount();
            for (int drawBackgroundForInitialFolder = drawBackgroundForInitialFolder(canvas, recyclerView); drawBackgroundForInitialFolder < childCount; drawBackgroundForInitialFolder++) {
                View childAt = recyclerView.getChildAt(drawBackgroundForInitialFolder);
                RecyclerView.ViewHolder childViewHolder = recyclerView.getChildViewHolder(childAt);
                if (childViewHolder instanceof GuildListViewHolder.FolderViewHolder) {
                    GuildListViewHolder.FolderViewHolder folderViewHolder = (GuildListViewHolder.FolderViewHolder) childViewHolder;
                    if (folderViewHolder.shouldDrawDecoration()) {
                        m.checkNotNullExpressionValue(childAt, "view");
                        int left = (childAt.getLeft() + childAt.getRight()) / 2;
                        int top = (childAt.getTop() + childAt.getBottom()) / 2;
                        int numChildren = folderViewHolder.getNumChildren();
                        if (numChildren == 0) {
                            Integer color = folderViewHolder.getColor();
                            if (color != null) {
                                DrawableCompat.setTint(this.tintableDrawableNoChildren, color.intValue());
                                drawable = this.tintableDrawableNoChildren;
                            } else {
                                drawable = this.drawableNoChildren;
                            }
                            int i = this.halfSize;
                            drawable.setBounds(left - i, top - i, left + i, top + i);
                            drawable.draw(canvas);
                        } else {
                            int height = (childAt.getHeight() * numChildren) + this.halfSize + top;
                            Drawable drawable2 = this.drawableWithChildren;
                            int i2 = this.halfSize;
                            drawable2.setBounds(left - i2, top - i2, left + i2, height);
                            this.drawableWithChildren.draw(canvas);
                        }
                    }
                }
                if ((childViewHolder instanceof GuildListViewHolder.GuildViewHolder) && ((GuildListViewHolder.GuildViewHolder) childViewHolder).isTargetedForFolderCreation()) {
                    m.checkNotNullExpressionValue(childAt, "view");
                    int left2 = (childAt.getLeft() + childAt.getRight()) / 2;
                    int top2 = (childAt.getTop() + childAt.getBottom()) / 2;
                    Drawable drawable3 = this.drawableNoChildren;
                    int i3 = this.halfSize;
                    drawable3.setBounds(left2 - i3, top2 - i3, left2 + i3, top2 + i3);
                    this.drawableNoChildren.draw(canvas);
                }
            }
        }
    }
}
