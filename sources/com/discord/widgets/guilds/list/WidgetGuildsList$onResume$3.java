package com.discord.widgets.guilds.list;

import com.discord.widgets.guilds.list.WidgetGuildsList;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint;", "it", "", "invoke", "(Lcom/discord/widgets/guilds/list/WidgetGuildsList$AddGuildHint;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildsList$onResume$3 extends o implements Function1<WidgetGuildsList.AddGuildHint, Unit> {
    public final /* synthetic */ WidgetGuildsList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildsList$onResume$3(WidgetGuildsList widgetGuildsList) {
        super(1);
        this.this$0 = widgetGuildsList;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetGuildsList.AddGuildHint addGuildHint) {
        invoke2(addGuildHint);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetGuildsList.AddGuildHint addGuildHint) {
        m.checkNotNullParameter(addGuildHint, "it");
        this.this$0.configureAddGuildHint(addGuildHint);
    }
}
