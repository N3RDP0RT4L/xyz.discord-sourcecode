package com.discord.widgets.guilds.list;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.widgets.guilds.list.GuildListItem;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildsListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "it", "Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;", "invoke", "(Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildsListViewModel$createDirectMessageItems$2 extends o implements Function1<Channel, GuildListItem.PrivateChannelItem> {
    public final /* synthetic */ Map $mentionCounts;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildsListViewModel$createDirectMessageItems$2(Map map) {
        super(1);
        this.$mentionCounts = map;
    }

    public final GuildListItem.PrivateChannelItem invoke(Channel channel) {
        m.checkNotNullParameter(channel, "it");
        Integer num = (Integer) a.c(channel, this.$mentionCounts);
        return new GuildListItem.PrivateChannelItem(channel, num != null ? num.intValue() : 0);
    }
}
