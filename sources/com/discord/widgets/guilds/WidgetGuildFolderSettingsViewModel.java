package com.discord.widgets.guilds;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildFolder;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGuildsSorted;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guilds.WidgetGuildFolderSettingsViewModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetGuildFolderSettingsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 82\b\u0012\u0004\u0012\u00020\u00020\u0001:\u000589:;<B'\u0012\n\u00100\u001a\u00060.j\u0002`/\u0012\b\b\u0002\u0010*\u001a\u00020)\u0012\b\b\u0002\u00105\u001a\u000204¢\u0006\u0004\b6\u00107J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000e\u0010\rJ\u001f\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0013\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\u0015\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\u00052\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ\r\u0010\u001f\u001a\u00020\u0005¢\u0006\u0004\b\u001f\u0010\rR$\u0010 \u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010\u0007R:\u0010'\u001a&\u0012\f\u0012\n &*\u0004\u0018\u00010\u00140\u0014 &*\u0012\u0012\f\u0012\n &*\u0004\u0018\u00010\u00140\u0014\u0018\u00010%0%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0019\u0010*\u001a\u00020)8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u001d\u00100\u001a\u00060.j\u0002`/8\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103¨\u0006="}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;)V", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;", "formState", "updateFormState", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;)V", "emitUpdateSuccessEvent", "()V", "emitUpdateFailureEvent", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState$Valid;", "", "shouldShowSave", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState$Valid;Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;)Z", "Lrx/Observable;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event;", "observeEvents", "()Lrx/Observable;", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "setName", "(Ljava/lang/String;)V", "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "setColor", "(Ljava/lang/Integer;)V", "saveFolder", "previousStoreState", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", "getPreviousStoreState", "()Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", "setPreviousStoreState", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUserSettings;", "getStoreUserSettings", "()Lcom/discord/stores/StoreUserSettings;", "", "Lcom/discord/primitives/FolderId;", "folderId", "J", "getFolderId", "()J", "Lcom/discord/stores/StoreGuildsSorted;", "storeGuildsSorted", HookHelper.constructorName, "(JLcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreGuildsSorted;)V", "Companion", "Event", "FormState", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildFolderSettingsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final long folderId;
    private StoreState previousStoreState;
    private final StoreUserSettings storeUserSettings;

    /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.WidgetGuildFolderSettingsViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetGuildFolderSettingsViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ1\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Companion;", "", "", "Lcom/discord/primitives/FolderId;", "folderId", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreGuildsSorted;", "storeGuildsSorted", "Lrx/Observable;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreGuildsSorted;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(final long j, StoreUserSettings storeUserSettings, StoreGuildsSorted storeGuildsSorted) {
            Observable<StoreState> j2 = Observable.j(storeUserSettings.observeGuildFolders(), storeGuildsSorted.observeEntries(), new Func2<List<? extends ModelGuildFolder>, List<? extends StoreGuildsSorted.Entry>, StoreState>() { // from class: com.discord.widgets.guilds.WidgetGuildFolderSettingsViewModel$Companion$observeStoreState$1
                @Override // rx.functions.Func2
                public /* bridge */ /* synthetic */ WidgetGuildFolderSettingsViewModel.StoreState call(List<? extends ModelGuildFolder> list, List<? extends StoreGuildsSorted.Entry> list2) {
                    return call2((List<ModelGuildFolder>) list, list2);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetGuildFolderSettingsViewModel.StoreState call2(List<ModelGuildFolder> list, List<? extends StoreGuildsSorted.Entry> list2) {
                    Object obj;
                    boolean z2;
                    m.checkNotNullExpressionValue(list, "guildFolders");
                    Iterator<T> it = list.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it.next();
                        Long id2 = ((ModelGuildFolder) obj).getId();
                        if (id2 != null && id2.longValue() == j) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            break;
                        }
                    }
                    ModelGuildFolder modelGuildFolder = (ModelGuildFolder) obj;
                    if (modelGuildFolder == null) {
                        return WidgetGuildFolderSettingsViewModel.StoreState.Invalid.INSTANCE;
                    }
                    m.checkNotNullExpressionValue(list2, "sortedGuilds");
                    return new WidgetGuildFolderSettingsViewModel.StoreState.Valid(modelGuildFolder, list2);
                }
            });
            m.checkNotNullExpressionValue(j2, "Observable.combineLatest…Guilds)\n        }\n      }");
            return j2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event;", "", HookHelper.constructorName, "()V", "UpdateFolderSettingsFailure", "UpdateFolderSettingsSuccess", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event$UpdateFolderSettingsSuccess;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event$UpdateFolderSettingsFailure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event$UpdateFolderSettingsFailure;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event;", "", "component1", "()I", "failureMessageStringRes", "copy", "(I)Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event$UpdateFolderSettingsFailure;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getFailureMessageStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UpdateFolderSettingsFailure extends Event {
            private final int failureMessageStringRes;

            public UpdateFolderSettingsFailure(int i) {
                super(null);
                this.failureMessageStringRes = i;
            }

            public static /* synthetic */ UpdateFolderSettingsFailure copy$default(UpdateFolderSettingsFailure updateFolderSettingsFailure, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = updateFolderSettingsFailure.failureMessageStringRes;
                }
                return updateFolderSettingsFailure.copy(i);
            }

            public final int component1() {
                return this.failureMessageStringRes;
            }

            public final UpdateFolderSettingsFailure copy(int i) {
                return new UpdateFolderSettingsFailure(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof UpdateFolderSettingsFailure) && this.failureMessageStringRes == ((UpdateFolderSettingsFailure) obj).failureMessageStringRes;
                }
                return true;
            }

            public final int getFailureMessageStringRes() {
                return this.failureMessageStringRes;
            }

            public int hashCode() {
                return this.failureMessageStringRes;
            }

            public String toString() {
                return a.A(a.R("UpdateFolderSettingsFailure(failureMessageStringRes="), this.failureMessageStringRes, ")");
            }
        }

        /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event$UpdateFolderSettingsSuccess;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event;", "", "component1", "()I", "successMessageStringRes", "copy", "(I)Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$Event$UpdateFolderSettingsSuccess;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getSuccessMessageStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UpdateFolderSettingsSuccess extends Event {
            private final int successMessageStringRes;

            public UpdateFolderSettingsSuccess(int i) {
                super(null);
                this.successMessageStringRes = i;
            }

            public static /* synthetic */ UpdateFolderSettingsSuccess copy$default(UpdateFolderSettingsSuccess updateFolderSettingsSuccess, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = updateFolderSettingsSuccess.successMessageStringRes;
                }
                return updateFolderSettingsSuccess.copy(i);
            }

            public final int component1() {
                return this.successMessageStringRes;
            }

            public final UpdateFolderSettingsSuccess copy(int i) {
                return new UpdateFolderSettingsSuccess(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof UpdateFolderSettingsSuccess) && this.successMessageStringRes == ((UpdateFolderSettingsSuccess) obj).successMessageStringRes;
                }
                return true;
            }

            public final int getSuccessMessageStringRes() {
                return this.successMessageStringRes;
            }

            public int hashCode() {
                return this.successMessageStringRes;
            }

            public String toString() {
                return a.A(a.R("UpdateFolderSettingsSuccess(successMessageStringRes="), this.successMessageStringRes, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J(\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0013\u001a\u0004\b\u0014\u0010\u0007R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_COLOR, "copy", "(Ljava/lang/String;Ljava/lang/Integer;)Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;", "toString", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getColor", "Ljava/lang/String;", "getName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class FormState {
        private final Integer color;
        private final String name;

        public FormState(String str, Integer num) {
            this.name = str;
            this.color = num;
        }

        public static /* synthetic */ FormState copy$default(FormState formState, String str, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                str = formState.name;
            }
            if ((i & 2) != 0) {
                num = formState.color;
            }
            return formState.copy(str, num);
        }

        public final String component1() {
            return this.name;
        }

        public final Integer component2() {
            return this.color;
        }

        public final FormState copy(String str, Integer num) {
            return new FormState(str, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FormState)) {
                return false;
            }
            FormState formState = (FormState) obj;
            return m.areEqual(this.name, formState.name) && m.areEqual(this.color, formState.color);
        }

        public final Integer getColor() {
            return this.color;
        }

        public final String getName() {
            return this.name;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Integer num = this.color;
            if (num != null) {
                i = num.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("FormState(name=");
            R.append(this.name);
            R.append(", color=");
            return a.E(R, this.color, ")");
        }
    }

    /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState$Valid;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState$Invalid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState$Invalid;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends StoreState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState$Valid;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState;", "Lcom/discord/models/domain/ModelGuildFolder;", "component1", "()Lcom/discord/models/domain/ModelGuildFolder;", "", "Lcom/discord/stores/StoreGuildsSorted$Entry;", "component2", "()Ljava/util/List;", "folder", "sortedGuilds", "copy", "(Lcom/discord/models/domain/ModelGuildFolder;Ljava/util/List;)Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$StoreState$Valid;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getSortedGuilds", "Lcom/discord/models/domain/ModelGuildFolder;", "getFolder", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGuildFolder;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends StoreState {
            private final ModelGuildFolder folder;
            private final List<StoreGuildsSorted.Entry> sortedGuilds;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Valid(ModelGuildFolder modelGuildFolder, List<? extends StoreGuildsSorted.Entry> list) {
                super(null);
                m.checkNotNullParameter(modelGuildFolder, "folder");
                m.checkNotNullParameter(list, "sortedGuilds");
                this.folder = modelGuildFolder;
                this.sortedGuilds = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Valid copy$default(Valid valid, ModelGuildFolder modelGuildFolder, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGuildFolder = valid.folder;
                }
                if ((i & 2) != 0) {
                    list = valid.sortedGuilds;
                }
                return valid.copy(modelGuildFolder, list);
            }

            public final ModelGuildFolder component1() {
                return this.folder;
            }

            public final List<StoreGuildsSorted.Entry> component2() {
                return this.sortedGuilds;
            }

            public final Valid copy(ModelGuildFolder modelGuildFolder, List<? extends StoreGuildsSorted.Entry> list) {
                m.checkNotNullParameter(modelGuildFolder, "folder");
                m.checkNotNullParameter(list, "sortedGuilds");
                return new Valid(modelGuildFolder, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.folder, valid.folder) && m.areEqual(this.sortedGuilds, valid.sortedGuilds);
            }

            public final ModelGuildFolder getFolder() {
                return this.folder;
            }

            public final List<StoreGuildsSorted.Entry> getSortedGuilds() {
                return this.sortedGuilds;
            }

            public int hashCode() {
                ModelGuildFolder modelGuildFolder = this.folder;
                int i = 0;
                int hashCode = (modelGuildFolder != null ? modelGuildFolder.hashCode() : 0) * 31;
                List<StoreGuildsSorted.Entry> list = this.sortedGuilds;
                if (list != null) {
                    i = list.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(folder=");
                R.append(this.folder);
                R.append(", sortedGuilds=");
                return a.K(R, this.sortedGuilds, ")");
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState$Uninitialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;", "component1", "()Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;", "", "component2", "()Z", "formState", "showSave", "copy", "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;Z)Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;", "getFormState", "Z", "getShowSave", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$FormState;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final FormState formState;
            private final boolean showSave;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(FormState formState, boolean z2) {
                super(null);
                m.checkNotNullParameter(formState, "formState");
                this.formState = formState;
                this.showSave = z2;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, FormState formState, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    formState = loaded.formState;
                }
                if ((i & 2) != 0) {
                    z2 = loaded.showSave;
                }
                return loaded.copy(formState, z2);
            }

            public final FormState component1() {
                return this.formState;
            }

            public final boolean component2() {
                return this.showSave;
            }

            public final Loaded copy(FormState formState, boolean z2) {
                m.checkNotNullParameter(formState, "formState");
                return new Loaded(formState, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.formState, loaded.formState) && this.showSave == loaded.showSave;
            }

            public final FormState getFormState() {
                return this.formState;
            }

            public final boolean getShowSave() {
                return this.showSave;
            }

            public int hashCode() {
                FormState formState = this.formState;
                int hashCode = (formState != null ? formState.hashCode() : 0) * 31;
                boolean z2 = this.showSave;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(formState=");
                R.append(this.formState);
                R.append(", showSave=");
                return a.M(R, this.showSave, ")");
            }
        }

        /* compiled from: WidgetGuildFolderSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetGuildFolderSettingsViewModel(long j, StoreUserSettings storeUserSettings, StoreGuildsSorted storeGuildsSorted, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i & 2) != 0 ? StoreStream.Companion.getUserSettings() : storeUserSettings, (i & 4) != 0 ? StoreStream.Companion.getGuildsSorted() : storeGuildsSorted);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitUpdateFailureEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.UpdateFolderSettingsFailure(R.string.default_failure_to_perform_action_message));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitUpdateSuccessEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.UpdateFolderSettingsSuccess(R.string.guild_folder_updated_success));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        this.previousStoreState = storeState;
        if (storeState instanceof StoreState.Valid) {
            StoreState.Valid valid = (StoreState.Valid) storeState;
            FormState formState = new FormState(valid.getFolder().getName(), valid.getFolder().getColor());
            updateViewState(new ViewState.Loaded(formState, shouldShowSave(valid, formState)));
        }
    }

    private final boolean shouldShowSave(StoreState.Valid valid, FormState formState) {
        return (m.areEqual(valid.getFolder().getColor(), formState.getColor()) ^ true) || (m.areEqual(valid.getFolder().getName(), formState.getName()) ^ true);
    }

    private final void updateFormState(FormState formState) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            StoreState storeState = this.previousStoreState;
            updateViewState(loaded.copy(formState, storeState instanceof StoreState.Valid ? shouldShowSave((StoreState.Valid) storeState, formState) : false));
        }
    }

    public final long getFolderId() {
        return this.folderId;
    }

    public final StoreState getPreviousStoreState() {
        return this.previousStoreState;
    }

    public final StoreUserSettings getStoreUserSettings() {
        return this.storeUserSettings;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void saveFolder() {
        ModelGuildFolder modelGuildFolder;
        StoreState storeState = this.previousStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null) {
            ViewState viewState = getViewState();
            if (!(viewState instanceof ViewState.Loaded)) {
                viewState = null;
            }
            ViewState.Loaded loaded = (ViewState.Loaded) viewState;
            if (loaded != null) {
                FormState formState = loaded.getFormState();
                List<StoreGuildsSorted.Entry> sortedGuilds = valid.getSortedGuilds();
                ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(sortedGuilds, 10));
                for (StoreGuildsSorted.Entry entry : sortedGuilds) {
                    if (entry instanceof StoreGuildsSorted.Entry.SingletonGuild) {
                        modelGuildFolder = entry.asModelGuildFolder();
                    } else if (entry instanceof StoreGuildsSorted.Entry.Folder) {
                        StoreGuildsSorted.Entry.Folder folder = (StoreGuildsSorted.Entry.Folder) entry;
                        if (folder.getId() == this.folderId) {
                            modelGuildFolder = StoreGuildsSorted.Entry.Folder.copy$default(folder, 0L, null, formState.getColor(), formState.getName(), 3, null).asModelGuildFolder();
                        } else {
                            modelGuildFolder = entry.asModelGuildFolder();
                        }
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                    arrayList.add(modelGuildFolder);
                }
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateUserSettings(RestAPIParams.UserSettings.Companion.createWithGuildFolders(arrayList)), false, 1, null), this, null, 2, null), WidgetGuildFolderSettingsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetGuildFolderSettingsViewModel$saveFolder$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildFolderSettingsViewModel$saveFolder$1(this));
            }
        }
    }

    public final void setColor(Integer num) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateFormState(FormState.copy$default(loaded.getFormState(), null, num, 1, null));
        }
    }

    public final void setName(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateFormState(FormState.copy$default(loaded.getFormState(), str, null, 2, null));
        }
    }

    public final void setPreviousStoreState(StoreState storeState) {
        this.previousStoreState = storeState;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildFolderSettingsViewModel(long j, StoreUserSettings storeUserSettings, StoreGuildsSorted storeGuildsSorted) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(storeUserSettings, "storeUserSettings");
        m.checkNotNullParameter(storeGuildsSorted, "storeGuildsSorted");
        this.folderId = j;
        this.storeUserSettings = storeUserSettings;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Companion.observeStoreState(j, storeUserSettings, storeGuildsSorted)), this, null, 2, null), WidgetGuildFolderSettingsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        this.eventSubject = PublishSubject.k0();
    }
}
