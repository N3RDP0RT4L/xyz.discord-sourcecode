package com.discord.widgets.guilds.join;

import android.content.Context;
import com.discord.utilities.error.Error;
import com.discord.utilities.guilds.GuildCaptchaUtilsKt;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildJoinHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildJoinHelperKt$joinGuild$1 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ Long $directoryChannelId;
    public final /* synthetic */ Class $errorClass;
    public final /* synthetic */ Function1 $errorHandler;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ boolean $isLurker;
    public final /* synthetic */ Function1 $onNext;
    public final /* synthetic */ String $sessionId;
    public final /* synthetic */ Function1 $subscriptionHandler;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildJoinHelperKt$joinGuild$1(Context context, long j, boolean z2, String str, Long l, Class cls, Function1 function1, Function1 function12, Function1 function13) {
        super(1);
        this.$context = context;
        this.$guildId = j;
        this.$isLurker = z2;
        this.$sessionId = str;
        this.$directoryChannelId = l;
        this.$errorClass = cls;
        this.$subscriptionHandler = function1;
        this.$errorHandler = function12;
        this.$onNext = function13;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        m.checkNotNullParameter(error, "it");
        GuildCaptchaUtilsKt.handleHttpException(error, this.$context, new JoinArgs(this.$guildId, this.$isLurker, this.$sessionId, this.$directoryChannelId, this.$errorClass, this.$subscriptionHandler, this.$errorHandler, this.$onNext));
    }
}
