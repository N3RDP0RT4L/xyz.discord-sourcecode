package com.discord.widgets.guilds.join;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.TextView;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.welcome.GuildWelcomeChannel;
import com.discord.databinding.WidgetGuildWelcomeChannelBinding;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.stores.StoreStream;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.widgets.guilds.join.ChannelItem;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetGuildWelcomeSheetChannelAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/guilds/join/ChannelViewHolder;", "Lcom/discord/widgets/guilds/join/BaseChannelViewHolder;", "Lcom/discord/widgets/guilds/join/ChannelItem;", "data", "", "bind", "(Lcom/discord/widgets/guilds/join/ChannelItem;)V", "Lcom/discord/databinding/WidgetGuildWelcomeChannelBinding;", "binding", "Lcom/discord/databinding/WidgetGuildWelcomeChannelBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetGuildWelcomeChannelBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelViewHolder extends BaseChannelViewHolder {
    private final WidgetGuildWelcomeChannelBinding binding;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public ChannelViewHolder(com.discord.databinding.WidgetGuildWelcomeChannelBinding r3) {
        /*
            r2 = this;
            java.lang.String r0 = "binding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            androidx.cardview.widget.CardView r0 = r3.a
            java.lang.String r1 = "binding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.binding = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.join.ChannelViewHolder.<init>(com.discord.databinding.WidgetGuildWelcomeChannelBinding):void");
    }

    @Override // com.discord.widgets.guilds.join.BaseChannelViewHolder
    public void bind(final ChannelItem channelItem) {
        m.checkNotNullParameter(channelItem, "data");
        super.bind(channelItem);
        ChannelItem.ChannelData channelData = (ChannelItem.ChannelData) channelItem;
        SimpleDraweeView simpleDraweeView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildWelcomeChannelEmoji");
        int mediaProxySize = IconUtils.getMediaProxySize(simpleDraweeView.getLayoutParams().width);
        GuildWelcomeChannel welcomeChannel = channelData.getWelcomeChannel();
        m.checkNotNullParameter(welcomeChannel, "$this$getEmojiUri");
        Long c = welcomeChannel.c();
        String str = null;
        String imageUri = c != null ? ModelEmojiCustom.getImageUri(c.longValue(), false, mediaProxySize) : null;
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.guildWelcomeChannelDescription");
        textView.setText(channelData.getWelcomeChannel().b());
        TextView textView2 = this.binding.e;
        m.checkNotNullExpressionValue(textView2, "binding.guildWelcomeChannelName");
        Channel findChannelById = StoreStream.Companion.getChannels().findChannelById(channelData.getWelcomeChannel().a());
        if (findChannelById != null) {
            str = ChannelUtils.c(findChannelById);
        }
        textView2.setText(String.valueOf(str));
        this.binding.f2433b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.join.ChannelViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ((ChannelItem.ChannelData) ChannelItem.this).getGoToChannel().invoke(Long.valueOf(((ChannelItem.ChannelData) ChannelItem.this).getWelcomeChannel().a()), Integer.valueOf(((ChannelItem.ChannelData) ChannelItem.this).getIndex()));
                ((ChannelItem.ChannelData) ChannelItem.this).getDismissSheet().invoke();
            }
        });
        if (imageUri != null) {
            SimpleDraweeView simpleDraweeView2 = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildWelcomeChannelEmoji");
            MGImages.setImage$default(simpleDraweeView2, imageUri, 0, 0, false, null, null, 124, null);
            SimpleDraweeView simpleDraweeView3 = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.guildWelcomeChannelEmoji");
            simpleDraweeView3.setVisibility(0);
            TextView textView3 = this.binding.f;
            m.checkNotNullExpressionValue(textView3, "binding.guildWelcomeChannelUnicodeEmoji");
            textView3.setVisibility(8);
        } else if (channelData.getWelcomeChannel().d() != null) {
            SimpleDraweeView simpleDraweeView4 = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView4, "binding.guildWelcomeChannelEmoji");
            simpleDraweeView4.setVisibility(8);
            TextView textView4 = this.binding.f;
            m.checkNotNullExpressionValue(textView4, "binding.guildWelcomeChannelUnicodeEmoji");
            textView4.setVisibility(0);
            TextView textView5 = this.binding.f;
            m.checkNotNullExpressionValue(textView5, "binding.guildWelcomeChannelUnicodeEmoji");
            textView5.setText(channelData.getWelcomeChannel().d());
        }
    }
}
