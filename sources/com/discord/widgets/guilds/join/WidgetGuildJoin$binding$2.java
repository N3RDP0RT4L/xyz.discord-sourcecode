package com.discord.widgets.guilds.join;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetGuildJoinBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildJoin.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildJoinBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildJoinBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildJoin$binding$2 extends k implements Function1<View, WidgetGuildJoinBinding> {
    public static final WidgetGuildJoin$binding$2 INSTANCE = new WidgetGuildJoin$binding$2();

    public WidgetGuildJoin$binding$2() {
        super(1, WidgetGuildJoinBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildJoinBinding;", 0);
    }

    public final WidgetGuildJoinBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_join_action_btn;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.guild_join_action_btn);
        if (materialButton != null) {
            i = R.id.guild_join_invite;
            TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.guild_join_invite);
            if (textInputLayout != null) {
                return new WidgetGuildJoinBinding((CoordinatorLayout) view, materialButton, textInputLayout);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
