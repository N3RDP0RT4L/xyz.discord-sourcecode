package com.discord.widgets.guilds.join;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.welcome.GuildWelcomeChannel;
import com.discord.api.guild.welcome.GuildWelcomeScreen;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreGuildWelcomeScreens;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000  2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003 !\"B#\u0012\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010\u0012\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00030\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\f\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u000e\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000e\u0010\rJ;\u0010\u0019\u001a\u00020\u00052\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\n\u0010\u0013\u001a\u00060\u000fj\u0002`\u00122\u0006\u0010\u0015\u001a\u00020\u00142\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016¢\u0006\u0004\b\u0019\u0010\u001aR\u001a\u0010\u0011\u001a\u00060\u000fj\u0002`\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u001b¨\u0006#"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;)V", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "welcomeScreen", "handleLoadedGuild", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/guild/welcome/GuildWelcomeScreen;)V", "handleLoadedWelcomeScreen", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "", "index", "", "Lcom/discord/api/guild/welcome/GuildWelcomeChannel;", "welcomeChannels", "onClickChannel", "(JJILjava/util/List;)V", "J", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(JLrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildWelcomeSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final long guildId;

    /* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.join.WidgetGuildWelcomeSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetGuildWelcomeSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;", "observeStores", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<StoreState> observeStores(long j) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<StoreState> j2 = Observable.j(companion.getGuilds().observeGuild(j), companion.getGuildWelcomeScreens().observeGuildWelcomeScreen(j), WidgetGuildWelcomeSheetViewModel$Companion$observeStores$1.INSTANCE);
            m.checkNotNullExpressionValue(j2, "Observable\n            .…          )\n            }");
            return j2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J(\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "component2", "()Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "guild", "guildWelcomeScreen", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildWelcomeScreens$State;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "getGuildWelcomeScreen", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildWelcomeScreens$State;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Guild guild;
        private final StoreGuildWelcomeScreens.State guildWelcomeScreen;

        public StoreState(Guild guild, StoreGuildWelcomeScreens.State state) {
            this.guild = guild;
            this.guildWelcomeScreen = state;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Guild guild, StoreGuildWelcomeScreens.State state, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = storeState.guild;
            }
            if ((i & 2) != 0) {
                state = storeState.guildWelcomeScreen;
            }
            return storeState.copy(guild, state);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final StoreGuildWelcomeScreens.State component2() {
            return this.guildWelcomeScreen;
        }

        public final StoreState copy(Guild guild, StoreGuildWelcomeScreens.State state) {
            return new StoreState(guild, state);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guild, storeState.guild) && m.areEqual(this.guildWelcomeScreen, storeState.guildWelcomeScreen);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final StoreGuildWelcomeScreens.State getGuildWelcomeScreen() {
            return this.guildWelcomeScreen;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            StoreGuildWelcomeScreens.State state = this.guildWelcomeScreen;
            if (state != null) {
                i = state.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guild=");
            R.append(this.guild);
            R.append(", guildWelcomeScreen=");
            R.append(this.guildWelcomeScreen);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Loaded", "Loading", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001BG\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0006\u0012\u0006\u0010\u0012\u001a\u00020\u0006\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u0012\u000e\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f¢\u0006\u0004\b*\u0010+J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\bJ\u0018\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\\\u0010\u0016\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0012\u001a\u00020\u00062\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00062\u0010\b\u0002\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0018\u0010\bJ\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001f\u001a\u00020\u001e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001f\u0010 R!\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010!\u001a\u0004\b\"\u0010\u000fR\u0019\u0010\u0012\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\bR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010%\u001a\u0004\b&\u0010\u0005R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b'\u0010\bR\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b(\u0010\bR\u0019\u0010\u0011\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b)\u0010\b¨\u0006,"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "component4", "component5", "", "Lcom/discord/api/guild/welcome/GuildWelcomeChannel;", "component6", "()Ljava/util/List;", "guildId", "guildName", "guildShortName", "guildIcon", "guildDescription", "welcomeChannelsData", "copy", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getWelcomeChannelsData", "Ljava/lang/String;", "getGuildShortName", "J", "getGuildId", "getGuildIcon", "getGuildDescription", "getGuildName", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final String guildDescription;
            private final String guildIcon;
            private final long guildId;
            private final String guildName;
            private final String guildShortName;
            private final List<GuildWelcomeChannel> welcomeChannelsData;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(long j, String str, String str2, String str3, String str4, List<GuildWelcomeChannel> list) {
                super(null);
                m.checkNotNullParameter(str, "guildName");
                m.checkNotNullParameter(str2, "guildShortName");
                this.guildId = j;
                this.guildName = str;
                this.guildShortName = str2;
                this.guildIcon = str3;
                this.guildDescription = str4;
                this.welcomeChannelsData = list;
            }

            public final long component1() {
                return this.guildId;
            }

            public final String component2() {
                return this.guildName;
            }

            public final String component3() {
                return this.guildShortName;
            }

            public final String component4() {
                return this.guildIcon;
            }

            public final String component5() {
                return this.guildDescription;
            }

            public final List<GuildWelcomeChannel> component6() {
                return this.welcomeChannelsData;
            }

            public final Loaded copy(long j, String str, String str2, String str3, String str4, List<GuildWelcomeChannel> list) {
                m.checkNotNullParameter(str, "guildName");
                m.checkNotNullParameter(str2, "guildShortName");
                return new Loaded(j, str, str2, str3, str4, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.guildId == loaded.guildId && m.areEqual(this.guildName, loaded.guildName) && m.areEqual(this.guildShortName, loaded.guildShortName) && m.areEqual(this.guildIcon, loaded.guildIcon) && m.areEqual(this.guildDescription, loaded.guildDescription) && m.areEqual(this.welcomeChannelsData, loaded.welcomeChannelsData);
            }

            public final String getGuildDescription() {
                return this.guildDescription;
            }

            public final String getGuildIcon() {
                return this.guildIcon;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final String getGuildName() {
                return this.guildName;
            }

            public final String getGuildShortName() {
                return this.guildShortName;
            }

            public final List<GuildWelcomeChannel> getWelcomeChannelsData() {
                return this.welcomeChannelsData;
            }

            public int hashCode() {
                int a = b.a(this.guildId) * 31;
                String str = this.guildName;
                int i = 0;
                int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.guildShortName;
                int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
                String str3 = this.guildIcon;
                int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
                String str4 = this.guildDescription;
                int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
                List<GuildWelcomeChannel> list = this.welcomeChannelsData;
                if (list != null) {
                    i = list.hashCode();
                }
                return hashCode4 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(guildId=");
                R.append(this.guildId);
                R.append(", guildName=");
                R.append(this.guildName);
                R.append(", guildShortName=");
                R.append(this.guildShortName);
                R.append(", guildIcon=");
                R.append(this.guildIcon);
                R.append(", guildDescription=");
                R.append(this.guildDescription);
                R.append(", welcomeChannelsData=");
                return a.K(R, this.welcomeChannelsData, ")");
            }
        }

        /* compiled from: WidgetGuildWelcomeSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetGuildWelcomeSheetViewModel(long j, Observable<StoreState> observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i & 2) != 0 ? Companion.observeStores(j) : observable);
    }

    private final void handleLoadedGuild(Guild guild, GuildWelcomeScreen guildWelcomeScreen) {
        long id2 = guild.getId();
        String name = guild.getName();
        String shortName = guild.getShortName();
        String icon = guild.getIcon();
        List<GuildWelcomeChannel> list = null;
        String a = guildWelcomeScreen != null ? guildWelcomeScreen.a() : null;
        if (guildWelcomeScreen != null) {
            list = guildWelcomeScreen.b();
        }
        updateViewState(new ViewState.Loaded(id2, name, shortName, icon, a, list));
    }

    private final void handleLoadedWelcomeScreen(Guild guild, GuildWelcomeScreen guildWelcomeScreen) {
        updateViewState(new ViewState.Loaded(guild.getId(), guild.getName(), guild.getShortName(), guild.getIcon(), guildWelcomeScreen.a(), guildWelcomeScreen.b()));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Guild component1 = storeState.component1();
        StoreGuildWelcomeScreens.State component2 = storeState.component2();
        GuildWelcomeScreen guildWelcomeScreen = null;
        StoreGuildWelcomeScreens.State.Loaded loaded = (StoreGuildWelcomeScreens.State.Loaded) (!(component2 instanceof StoreGuildWelcomeScreens.State.Loaded) ? null : component2);
        if (component1 != null && component2 == StoreGuildWelcomeScreens.State.Failure.INSTANCE) {
            updateViewState(ViewState.Invalid.INSTANCE);
        } else if (component2 == StoreGuildWelcomeScreens.State.Fetching.INSTANCE) {
            updateViewState(ViewState.Loading.INSTANCE);
        } else {
            if (component1 != null) {
                if ((loaded != null ? loaded.getData() : null) != null) {
                    handleLoadedWelcomeScreen(component1, loaded.getData());
                    return;
                }
            }
            if (component1 != null) {
                if (loaded != null) {
                    guildWelcomeScreen = loaded.getData();
                }
                handleLoadedGuild(component1, guildWelcomeScreen);
                return;
            }
            if (loaded != null) {
                guildWelcomeScreen = loaded.getData();
            }
            if (guildWelcomeScreen == null) {
                updateViewState(ViewState.Invalid.INSTANCE);
            } else {
                updateViewState(ViewState.Loading.INSTANCE);
            }
        }
    }

    public final void onClickChannel(long j, long j2, int i, List<GuildWelcomeChannel> list) {
        m.checkNotNullParameter(list, "welcomeChannels");
        ChannelSelector.Companion.getInstance().selectChannel(j, j2, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null);
        String b2 = list.get(i).b();
        boolean z2 = list.get(i).c() != null;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (GuildWelcomeChannel guildWelcomeChannel : list) {
            arrayList2.add(Long.valueOf(guildWelcomeChannel.a()));
            arrayList.add(guildWelcomeChannel.b());
        }
        AnalyticsTracker.welcomeScreenChannelSelected(i, j, arrayList, arrayList2, b2, z2);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildWelcomeSheetViewModel(long j, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(observable, "storeObservable");
        this.guildId = j;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetGuildWelcomeSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getGuildWelcomeScreens().fetchIfNonexisting(j);
        companion.getGuildWelcomeScreens().markWelcomeScreenShown(j);
        AnalyticsTracker.openModal("Guild Welcome Screen", "", Long.valueOf(j));
    }
}
