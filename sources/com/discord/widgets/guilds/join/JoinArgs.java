package com.discord.widgets.guilds.join;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.utilities.error.Error;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Subscription;
/* compiled from: WidgetGuildJoinCaptchaBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B\u0085\u0001\u0012\n\u0010\u001b\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u001c\u001a\u00020\u0006\u0012\u000e\u0010\u001d\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0002\u0012\n\u0010\u001f\u001a\u0006\u0012\u0002\b\u00030\u000f\u0012\u0016\b\u0002\u0010 \u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0012\u0012\u0016\b\u0002\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0012\u0012\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00140\u0012¢\u0006\u0004\b:\u0010;J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0018\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\r\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0014\u0010\u0010\u001a\u0006\u0012\u0002\b\u00030\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u001e\u0010\u0015\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u001e\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0016J\u001c\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00140\u0012HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0016J\u009a\u0001\u0010#\u001a\u00020\u00002\f\b\u0002\u0010\u001b\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001c\u001a\u00020\u00062\u0010\b\u0002\u0010\u001d\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00022\f\b\u0002\u0010\u001f\u001a\u0006\u0012\u0002\b\u00030\u000f2\u0016\b\u0002\u0010 \u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00122\u0016\b\u0002\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00122\u0014\b\u0002\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00140\u0012HÆ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b%\u0010\fJ\u0010\u0010'\u001a\u00020&HÖ\u0001¢\u0006\u0004\b'\u0010(J\u001a\u0010+\u001a\u00020\u00062\b\u0010*\u001a\u0004\u0018\u00010)HÖ\u0003¢\u0006\u0004\b+\u0010,R'\u0010 \u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b \u0010-\u001a\u0004\b.\u0010\u0016R%\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00140\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010-\u001a\u0004\b/\u0010\u0016R\u001d\u0010\u001b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00100\u001a\u0004\b1\u0010\u0005R!\u0010\u001d\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00102\u001a\u0004\b3\u0010\fR\u001d\u0010\u001f\u001a\u0006\u0012\u0002\b\u00030\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00104\u001a\u0004\b5\u0010\u0011R'\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010-\u001a\u0004\b6\u0010\u0016R\u0019\u0010\u001c\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00107\u001a\u0004\b\u001c\u0010\bR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00108\u001a\u0004\b9\u0010\u000e¨\u0006<"}, d2 = {"Lcom/discord/widgets/guilds/join/JoinArgs;", "Lcom/discord/widgets/guilds/join/CaptchaArgs;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Z", "", "Lcom/discord/primitives/SessionId;", "component3", "()Ljava/lang/String;", "component4", "()Ljava/lang/Long;", "Ljava/lang/Class;", "component5", "()Ljava/lang/Class;", "Lkotlin/Function1;", "Lrx/Subscription;", "", "component6", "()Lkotlin/jvm/functions/Function1;", "Lcom/discord/utilities/error/Error;", "component7", "Lcom/discord/api/guild/Guild;", "component8", "guildId", "isLurker", "sessionId", "directoryChannelId", "errorClass", "subscriptionHandler", "errorHandler", "onNext", "copy", "(JZLjava/lang/String;Ljava/lang/Long;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/guilds/join/JoinArgs;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/jvm/functions/Function1;", "getSubscriptionHandler", "getOnNext", "J", "getGuildId", "Ljava/lang/String;", "getSessionId", "Ljava/lang/Class;", "getErrorClass", "getErrorHandler", "Z", "Ljava/lang/Long;", "getDirectoryChannelId", HookHelper.constructorName, "(JZLjava/lang/String;Ljava/lang/Long;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class JoinArgs extends CaptchaArgs {
    private final Long directoryChannelId;
    private final Class<?> errorClass;
    private final Function1<Error, Unit> errorHandler;
    private final long guildId;
    private final boolean isLurker;
    private final Function1<Guild, Unit> onNext;
    private final String sessionId;
    private final Function1<Subscription, Unit> subscriptionHandler;

    public /* synthetic */ JoinArgs(long j, boolean z2, String str, Long l, Class cls, Function1 function1, Function1 function12, Function1 function13, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, z2, str, l, cls, (i & 32) != 0 ? null : function1, (i & 64) != 0 ? null : function12, function13);
    }

    public final long component1() {
        return this.guildId;
    }

    public final boolean component2() {
        return this.isLurker;
    }

    public final String component3() {
        return this.sessionId;
    }

    public final Long component4() {
        return this.directoryChannelId;
    }

    public final Class<?> component5() {
        return this.errorClass;
    }

    public final Function1<Subscription, Unit> component6() {
        return this.subscriptionHandler;
    }

    public final Function1<Error, Unit> component7() {
        return this.errorHandler;
    }

    public final Function1<Guild, Unit> component8() {
        return this.onNext;
    }

    public final JoinArgs copy(long j, boolean z2, String str, Long l, Class<?> cls, Function1<? super Subscription, Unit> function1, Function1<? super Error, Unit> function12, Function1<? super Guild, Unit> function13) {
        m.checkNotNullParameter(cls, "errorClass");
        m.checkNotNullParameter(function13, "onNext");
        return new JoinArgs(j, z2, str, l, cls, function1, function12, function13);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof JoinArgs)) {
            return false;
        }
        JoinArgs joinArgs = (JoinArgs) obj;
        return this.guildId == joinArgs.guildId && this.isLurker == joinArgs.isLurker && m.areEqual(this.sessionId, joinArgs.sessionId) && m.areEqual(this.directoryChannelId, joinArgs.directoryChannelId) && m.areEqual(this.errorClass, joinArgs.errorClass) && m.areEqual(this.subscriptionHandler, joinArgs.subscriptionHandler) && m.areEqual(this.errorHandler, joinArgs.errorHandler) && m.areEqual(this.onNext, joinArgs.onNext);
    }

    public final Long getDirectoryChannelId() {
        return this.directoryChannelId;
    }

    public final Class<?> getErrorClass() {
        return this.errorClass;
    }

    public final Function1<Error, Unit> getErrorHandler() {
        return this.errorHandler;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final Function1<Guild, Unit> getOnNext() {
        return this.onNext;
    }

    public final String getSessionId() {
        return this.sessionId;
    }

    public final Function1<Subscription, Unit> getSubscriptionHandler() {
        return this.subscriptionHandler;
    }

    public int hashCode() {
        int a = b.a(this.guildId) * 31;
        boolean z2 = this.isLurker;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        int i3 = (a + i) * 31;
        String str = this.sessionId;
        int i4 = 0;
        int hashCode = (i3 + (str != null ? str.hashCode() : 0)) * 31;
        Long l = this.directoryChannelId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Class<?> cls = this.errorClass;
        int hashCode3 = (hashCode2 + (cls != null ? cls.hashCode() : 0)) * 31;
        Function1<Subscription, Unit> function1 = this.subscriptionHandler;
        int hashCode4 = (hashCode3 + (function1 != null ? function1.hashCode() : 0)) * 31;
        Function1<Error, Unit> function12 = this.errorHandler;
        int hashCode5 = (hashCode4 + (function12 != null ? function12.hashCode() : 0)) * 31;
        Function1<Guild, Unit> function13 = this.onNext;
        if (function13 != null) {
            i4 = function13.hashCode();
        }
        return hashCode5 + i4;
    }

    public final boolean isLurker() {
        return this.isLurker;
    }

    public String toString() {
        StringBuilder R = a.R("JoinArgs(guildId=");
        R.append(this.guildId);
        R.append(", isLurker=");
        R.append(this.isLurker);
        R.append(", sessionId=");
        R.append(this.sessionId);
        R.append(", directoryChannelId=");
        R.append(this.directoryChannelId);
        R.append(", errorClass=");
        R.append(this.errorClass);
        R.append(", subscriptionHandler=");
        R.append(this.subscriptionHandler);
        R.append(", errorHandler=");
        R.append(this.errorHandler);
        R.append(", onNext=");
        R.append(this.onNext);
        R.append(")");
        return R.toString();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public JoinArgs(long j, boolean z2, String str, Long l, Class<?> cls, Function1<? super Subscription, Unit> function1, Function1<? super Error, Unit> function12, Function1<? super Guild, Unit> function13) {
        super(null);
        m.checkNotNullParameter(cls, "errorClass");
        m.checkNotNullParameter(function13, "onNext");
        this.guildId = j;
        this.isLurker = z2;
        this.sessionId = str;
        this.directoryChannelId = l;
        this.errorClass = cls;
        this.subscriptionHandler = function1;
        this.errorHandler = function12;
        this.onNext = function13;
    }
}
