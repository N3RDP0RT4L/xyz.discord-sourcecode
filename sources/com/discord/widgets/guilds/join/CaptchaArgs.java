package com.discord.widgets.guilds.join;

import andhook.lib.HookHelper;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetGuildJoinCaptchaBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0004\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/join/CaptchaArgs;", "Ljava/io/Serializable;", HookHelper.constructorName, "()V", "Lcom/discord/widgets/guilds/join/InviteArgs;", "Lcom/discord/widgets/guilds/join/JoinArgs;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class CaptchaArgs implements Serializable {
    private CaptchaArgs() {
    }

    public /* synthetic */ CaptchaArgs(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
