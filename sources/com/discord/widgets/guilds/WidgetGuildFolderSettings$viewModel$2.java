package com.discord.widgets.guilds;

import com.discord.app.AppViewModel;
import com.discord.widgets.guilds.WidgetGuildFolderSettingsViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildFolderSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/WidgetGuildFolderSettingsViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildFolderSettings$viewModel$2 extends o implements Function0<AppViewModel<WidgetGuildFolderSettingsViewModel.ViewState>> {
    public final /* synthetic */ WidgetGuildFolderSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildFolderSettings$viewModel$2(WidgetGuildFolderSettings widgetGuildFolderSettings) {
        super(0);
        this.this$0 = widgetGuildFolderSettings;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetGuildFolderSettingsViewModel.ViewState> invoke() {
        return new WidgetGuildFolderSettingsViewModel(this.this$0.getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_GUILD_FOLDER_ID", -1L), null, null, 6, null);
    }
}
