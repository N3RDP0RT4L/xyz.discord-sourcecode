package com.discord.widgets.guilds.profile;

import android.view.View;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildProfileSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "com/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$3$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$3 extends o implements Function1<View, Unit> {
    public final /* synthetic */ Long $channelId$inlined;
    public final /* synthetic */ long $guildId$inlined;
    public final /* synthetic */ boolean $showViewServer$inlined;
    public final /* synthetic */ WidgetGuildProfileSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$3(WidgetGuildProfileSheet widgetGuildProfileSheet, boolean z2, long j, Long l) {
        super(1);
        this.this$0 = widgetGuildProfileSheet;
        this.$showViewServer$inlined = z2;
        this.$guildId$inlined = j;
        this.$channelId$inlined = l;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        WidgetGuildProfileSheetViewModel viewModel;
        m.checkNotNullParameter(view, "it");
        viewModel = this.this$0.getViewModel();
        viewModel.onClickViewServer(this.$guildId$inlined, this.$channelId$inlined);
    }
}
