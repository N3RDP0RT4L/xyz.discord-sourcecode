package com.discord.widgets.guilds.profile;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.api.emoji.GuildEmoji;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.preview.GuildPreview;
import com.discord.app.AppActivity;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreEmoji;
import com.discord.stores.StoreGuildProfiles;
import com.discord.stores.StoreLurking;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserGuildSettings;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetGuildProfileSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Æ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 a2\b\u0012\u0004\u0012\u00020\u00020\u0001:\tbcdaefghiBg\u0012\b\b\u0002\u0010P\u001a\u00020O\u0012\b\b\u0002\u0010J\u001a\u00020I\u0012\b\b\u0002\u0010M\u001a\u00020L\u0012\u0006\u0010E\u001a\u00020\u0016\u0012\b\b\u0002\u0010S\u001a\u00020R\u0012\b\b\u0002\u0010V\u001a\u00020U\u0012\b\b\u0002\u0010]\u001a\u00020\\\u0012\n\u0010'\u001a\u00060\u0013j\u0002`\u0014\u0012\u000e\b\u0002\u0010^\u001a\b\u0012\u0004\u0012\u00020\u00030!¢\u0006\u0004\b_\u0010`J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007Js\u0010\u001c\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0010\u0010\u0015\u001a\f\u0012\b\u0012\u00060\u0013j\u0002`\u00140\u00122\u0006\u0010\u0017\u001a\u00020\u00162\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u00122\u0006\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u001f\u0010\u001f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u001e\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u001f\u0010 J\u0013\u0010#\u001a\b\u0012\u0004\u0012\u00020\"0!¢\u0006\u0004\b#\u0010$J#\u0010(\u001a\u00020\u00052\b\u0010&\u001a\u0004\u0018\u00010%2\n\u0010'\u001a\u00060\u0013j\u0002`\u0014¢\u0006\u0004\b(\u0010)J/\u0010.\u001a\u00020\u00052\n\u0010'\u001a\u00060\u0013j\u0002`\u00142\u0006\u0010+\u001a\u00020*2\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00050,¢\u0006\u0004\b.\u0010/J'\u00100\u001a\u00020\u00052\n\u0010'\u001a\u00060\u0013j\u0002`\u00142\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00050,¢\u0006\u0004\b0\u00101J'\u00102\u001a\u00020\u00052\n\u0010'\u001a\u00060\u0013j\u0002`\u00142\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00050,¢\u0006\u0004\b2\u00101J+\u00106\u001a\u00020\u00052\b\u00104\u001a\u0004\u0018\u0001032\n\u0010'\u001a\u00060\u0013j\u0002`\u00142\u0006\u00105\u001a\u00020\u0016¢\u0006\u0004\b6\u00107J!\u00109\u001a\u00020\u00052\n\u0010'\u001a\u00060\u0013j\u0002`\u00142\u0006\u00108\u001a\u00020\u0016¢\u0006\u0004\b9\u0010:J\r\u0010;\u001a\u00020\u0005¢\u0006\u0004\b;\u0010<J!\u0010?\u001a\u00020\u00052\n\u0010'\u001a\u00060\u0013j\u0002`\u00142\u0006\u0010>\u001a\u00020=¢\u0006\u0004\b?\u0010@J)\u0010C\u001a\u00020\u00052\n\u0010'\u001a\u00060\u0013j\u0002`\u00142\u000e\u0010B\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`A¢\u0006\u0004\bC\u0010DR\u0016\u0010E\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u0010FR\u0016\u0010G\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bG\u0010FR\u001a\u0010'\u001a\u00060\u0013j\u0002`\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010HR\u0016\u0010J\u001a\u00020I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010KR\u0016\u0010M\u001a\u00020L8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bM\u0010NR\u0016\u0010P\u001a\u00020O8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bP\u0010QR\u0016\u0010S\u001a\u00020R8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010TR\u0016\u0010V\u001a\u00020U8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010WR:\u0010Z\u001a&\u0012\f\u0012\n Y*\u0004\u0018\u00010\"0\" Y*\u0012\u0012\f\u0012\n Y*\u0004\u0018\u00010\"0\"\u0018\u00010X0X8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[¨\u0006j"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/guild/preview/GuildPreview;", "guildPreview", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "guildChannelsInfo", "Lcom/discord/models/user/MeUser;", "me", "Lcom/discord/models/member/GuildMember;", "computedMe", "", "", "Lcom/discord/primitives/GuildId;", "restrictedGuildIds", "", "isDeveloper", "Lcom/discord/models/domain/emoji/Emoji;", "emojis", "isLurking", "isUnread", "handleLoadedGuild", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/guild/preview/GuildPreview;Lcom/discord/utilities/channel/GuildChannelsInfo;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Ljava/util/List;ZLjava/util/List;ZZ)V", "meUser", "handleLoadedGuildPreview", "(Lcom/discord/api/guild/preview/GuildPreview;Lcom/discord/models/user/MeUser;)V", "Lrx/Observable;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Landroid/content/Context;", "context", "guildId", "onClickMarkAsRead", "(Landroid/content/Context;J)V", "", ModelAuditLogEntry.CHANGE_KEY_NICK, "Lkotlin/Function0;", "onSuccess", "onClickSaveNickname", "(JLjava/lang/String;Lkotlin/jvm/functions/Function0;)V", "onClickResetNickname", "(JLkotlin/jvm/functions/Function0;)V", "onClickLeaveServer", "Lcom/discord/app/AppActivity;", "appActivity", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED, "setAllowDM", "(Lcom/discord/app/AppActivity;JZ)V", "hide", "setHideMutedChannels", "(JZ)V", "onClickEmoji", "()V", "Landroidx/fragment/app/Fragment;", "fragment", "onClickJoinServer", "(JLandroidx/fragment/app/Fragment;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "onClickViewServer", "(JLjava/lang/Long;)V", "viewingGuild", "Z", "isEmojiSectionExpanded", "J", "Lcom/discord/stores/StoreUserGuildSettings;", "storeUserGuildSettings", "Lcom/discord/stores/StoreUserGuildSettings;", "Lcom/discord/stores/StoreMessageAck;", "messageAck", "Lcom/discord/stores/StoreMessageAck;", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUserSettings;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreLurking;", "storeLurking", "Lcom/discord/stores/StoreLurking;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "storeObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreMessageAck;ZLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreAnalytics;JLrx/Observable;)V", "Companion", "Actions", "Banner", "BottomActions", "EmojisData", "Event", "StoreState", "TabItems", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildProfileSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final long guildId;
    private boolean isEmojiSectionExpanded;
    private final StoreMessageAck messageAck;
    private final RestAPI restAPI;
    private final StoreLurking storeLurking;
    private final StoreUserGuildSettings storeUserGuildSettings;
    private final StoreUserSettings storeUserSettings;
    private final boolean viewingGuild;

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetGuildProfileSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0016\n\u0002\u0010\b\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001Bc\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0002\u0012\u0006\u0010\u001b\u001a\u00020\b¢\u0006\u0004\b1\u00102J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0010\u0010\nJ\u0082\u0001\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00022\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00022\b\b\u0002\u0010\u001b\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001e\u0010\nJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010#\u001a\u00020\u00022\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b\u0011\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010%\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010%\u001a\u0004\b'\u0010\u0004R\u0019\u0010(\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010%\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010%\u001a\u0004\b+\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010%\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010%\u001a\u0004\b,\u0010\u0004R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010-\u001a\u0004\b.\u0010\nR\u0019\u0010\u001b\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010-\u001a\u0004\b/\u0010\nR\u001b\u0010\u0015\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010-\u001a\u0004\b0\u0010\n¨\u00063"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;", "", "", "component1", "()Z", "component2", "component3", "component4", "", "component5", "()Ljava/lang/String;", "component6", "component7", "component8", "component9", "component10", "component11", "isUnread", "canManageChannels", "canManageEvents", "canChangeNickname", ModelAuditLogEntry.CHANGE_KEY_NICK, "guildAvatar", "isAllowDMChecked", "hideMutedChannels", "canLeaveGuild", "isDeveloper", "username", "copy", "(ZZZZLjava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanChangeNickname", "getCanManageChannels", "displayGuildIdentityRow", "getDisplayGuildIdentityRow", "getCanManageEvents", "getCanLeaveGuild", "getHideMutedChannels", "Ljava/lang/String;", "getGuildAvatar", "getUsername", "getNick", HookHelper.constructorName, "(ZZZZLjava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Actions {
        private final boolean canChangeNickname;
        private final boolean canLeaveGuild;
        private final boolean canManageChannels;
        private final boolean canManageEvents;
        private final boolean displayGuildIdentityRow;
        private final String guildAvatar;
        private final boolean hideMutedChannels;
        private final boolean isAllowDMChecked;
        private final boolean isDeveloper;
        private final boolean isUnread;
        private final String nick;
        private final String username;

        /* JADX WARN: Code restructure failed: missing block: B:14:0x003a, code lost:
            if ((r7 == null || r7.length() == 0) == false) goto L15;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public Actions(boolean r2, boolean r3, boolean r4, boolean r5, java.lang.String r6, java.lang.String r7, boolean r8, boolean r9, boolean r10, boolean r11, java.lang.String r12) {
            /*
                r1 = this;
                java.lang.String r0 = "username"
                d0.z.d.m.checkNotNullParameter(r12, r0)
                r1.<init>()
                r1.isUnread = r2
                r1.canManageChannels = r3
                r1.canManageEvents = r4
                r1.canChangeNickname = r5
                r1.nick = r6
                r1.guildAvatar = r7
                r1.isAllowDMChecked = r8
                r1.hideMutedChannels = r9
                r1.canLeaveGuild = r10
                r1.isDeveloper = r11
                r1.username = r12
                r2 = 0
                r3 = 1
                if (r6 == 0) goto L2b
                int r4 = r6.length()
                if (r4 != 0) goto L29
                goto L2b
            L29:
                r4 = 0
                goto L2c
            L2b:
                r4 = 1
            L2c:
                if (r4 == 0) goto L3c
                if (r7 == 0) goto L39
                int r4 = r7.length()
                if (r4 != 0) goto L37
                goto L39
            L37:
                r4 = 0
                goto L3a
            L39:
                r4 = 1
            L3a:
                if (r4 != 0) goto L3d
            L3c:
                r2 = 1
            L3d:
                r1.displayGuildIdentityRow = r2
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel.Actions.<init>(boolean, boolean, boolean, boolean, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, java.lang.String):void");
        }

        public final boolean component1() {
            return this.isUnread;
        }

        public final boolean component10() {
            return this.isDeveloper;
        }

        public final String component11() {
            return this.username;
        }

        public final boolean component2() {
            return this.canManageChannels;
        }

        public final boolean component3() {
            return this.canManageEvents;
        }

        public final boolean component4() {
            return this.canChangeNickname;
        }

        public final String component5() {
            return this.nick;
        }

        public final String component6() {
            return this.guildAvatar;
        }

        public final boolean component7() {
            return this.isAllowDMChecked;
        }

        public final boolean component8() {
            return this.hideMutedChannels;
        }

        public final boolean component9() {
            return this.canLeaveGuild;
        }

        public final Actions copy(boolean z2, boolean z3, boolean z4, boolean z5, String str, String str2, boolean z6, boolean z7, boolean z8, boolean z9, String str3) {
            m.checkNotNullParameter(str3, "username");
            return new Actions(z2, z3, z4, z5, str, str2, z6, z7, z8, z9, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Actions)) {
                return false;
            }
            Actions actions = (Actions) obj;
            return this.isUnread == actions.isUnread && this.canManageChannels == actions.canManageChannels && this.canManageEvents == actions.canManageEvents && this.canChangeNickname == actions.canChangeNickname && m.areEqual(this.nick, actions.nick) && m.areEqual(this.guildAvatar, actions.guildAvatar) && this.isAllowDMChecked == actions.isAllowDMChecked && this.hideMutedChannels == actions.hideMutedChannels && this.canLeaveGuild == actions.canLeaveGuild && this.isDeveloper == actions.isDeveloper && m.areEqual(this.username, actions.username);
        }

        public final boolean getCanChangeNickname() {
            return this.canChangeNickname;
        }

        public final boolean getCanLeaveGuild() {
            return this.canLeaveGuild;
        }

        public final boolean getCanManageChannels() {
            return this.canManageChannels;
        }

        public final boolean getCanManageEvents() {
            return this.canManageEvents;
        }

        public final boolean getDisplayGuildIdentityRow() {
            return this.displayGuildIdentityRow;
        }

        public final String getGuildAvatar() {
            return this.guildAvatar;
        }

        public final boolean getHideMutedChannels() {
            return this.hideMutedChannels;
        }

        public final String getNick() {
            return this.nick;
        }

        public final String getUsername() {
            return this.username;
        }

        public int hashCode() {
            boolean z2 = this.isUnread;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.canManageChannels;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.canManageEvents;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.canChangeNickname;
            if (z5) {
                z5 = true;
            }
            int i11 = z5 ? 1 : 0;
            int i12 = z5 ? 1 : 0;
            int i13 = (i10 + i11) * 31;
            String str = this.nick;
            int i14 = 0;
            int hashCode = (i13 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.guildAvatar;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            boolean z6 = this.isAllowDMChecked;
            if (z6) {
                z6 = true;
            }
            int i15 = z6 ? 1 : 0;
            int i16 = z6 ? 1 : 0;
            int i17 = (hashCode2 + i15) * 31;
            boolean z7 = this.hideMutedChannels;
            if (z7) {
                z7 = true;
            }
            int i18 = z7 ? 1 : 0;
            int i19 = z7 ? 1 : 0;
            int i20 = (i17 + i18) * 31;
            boolean z8 = this.canLeaveGuild;
            if (z8) {
                z8 = true;
            }
            int i21 = z8 ? 1 : 0;
            int i22 = z8 ? 1 : 0;
            int i23 = (i20 + i21) * 31;
            boolean z9 = this.isDeveloper;
            if (!z9) {
                i = z9 ? 1 : 0;
            }
            int i24 = (i23 + i) * 31;
            String str3 = this.username;
            if (str3 != null) {
                i14 = str3.hashCode();
            }
            return i24 + i14;
        }

        public final boolean isAllowDMChecked() {
            return this.isAllowDMChecked;
        }

        public final boolean isDeveloper() {
            return this.isDeveloper;
        }

        public final boolean isUnread() {
            return this.isUnread;
        }

        public String toString() {
            StringBuilder R = a.R("Actions(isUnread=");
            R.append(this.isUnread);
            R.append(", canManageChannels=");
            R.append(this.canManageChannels);
            R.append(", canManageEvents=");
            R.append(this.canManageEvents);
            R.append(", canChangeNickname=");
            R.append(this.canChangeNickname);
            R.append(", nick=");
            R.append(this.nick);
            R.append(", guildAvatar=");
            R.append(this.guildAvatar);
            R.append(", isAllowDMChecked=");
            R.append(this.isAllowDMChecked);
            R.append(", hideMutedChannels=");
            R.append(this.hideMutedChannels);
            R.append(", canLeaveGuild=");
            R.append(this.canLeaveGuild);
            R.append(", isDeveloper=");
            R.append(this.isDeveloper);
            R.append(", username=");
            return a.H(R, this.username, ")");
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0001!B%\u0012\n\u0010\f\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\u001f\u0010 J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ4\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\f\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\bJ\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001a\u0010\u000bR\u001d\u0010\f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\b¨\u0006\""}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;", "component3", "()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;", "guildId", "hash", "type", "copy", "(JLjava/lang/String;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;", "getType", "J", "getGuildId", "Ljava/lang/String;", "getHash", HookHelper.constructorName, "(JLjava/lang/String;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;)V", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Banner {
        private final long guildId;
        private final String hash;
        private final Type type;

        /* compiled from: WidgetGuildProfileSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "BANNER", "SPLASH", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum Type {
            BANNER,
            SPLASH
        }

        public Banner(long j, String str, Type type) {
            m.checkNotNullParameter(type, "type");
            this.guildId = j;
            this.hash = str;
            this.type = type;
        }

        public static /* synthetic */ Banner copy$default(Banner banner, long j, String str, Type type, int i, Object obj) {
            if ((i & 1) != 0) {
                j = banner.guildId;
            }
            if ((i & 2) != 0) {
                str = banner.hash;
            }
            if ((i & 4) != 0) {
                type = banner.type;
            }
            return banner.copy(j, str, type);
        }

        public final long component1() {
            return this.guildId;
        }

        public final String component2() {
            return this.hash;
        }

        public final Type component3() {
            return this.type;
        }

        public final Banner copy(long j, String str, Type type) {
            m.checkNotNullParameter(type, "type");
            return new Banner(j, str, type);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Banner)) {
                return false;
            }
            Banner banner = (Banner) obj;
            return this.guildId == banner.guildId && m.areEqual(this.hash, banner.hash) && m.areEqual(this.type, banner.type);
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final String getHash() {
            return this.hash;
        }

        public final Type getType() {
            return this.type;
        }

        public int hashCode() {
            int a = b.a(this.guildId) * 31;
            String str = this.hash;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            Type type = this.type;
            if (type != null) {
                i = type.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("Banner(guildId=");
            R.append(this.guildId);
            R.append(", hash=");
            R.append(this.hash);
            R.append(", type=");
            R.append(this.type);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J.\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00022\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;", "", "", "component1", "()Z", "component2", "component3", "showUploadEmoji", "showJoinServer", "showViewServer", "copy", "(ZZZ)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowUploadEmoji", "getShowJoinServer", "getShowViewServer", HookHelper.constructorName, "(ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BottomActions {
        private final boolean showJoinServer;
        private final boolean showUploadEmoji;
        private final boolean showViewServer;

        public BottomActions(boolean z2, boolean z3, boolean z4) {
            this.showUploadEmoji = z2;
            this.showJoinServer = z3;
            this.showViewServer = z4;
        }

        public static /* synthetic */ BottomActions copy$default(BottomActions bottomActions, boolean z2, boolean z3, boolean z4, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = bottomActions.showUploadEmoji;
            }
            if ((i & 2) != 0) {
                z3 = bottomActions.showJoinServer;
            }
            if ((i & 4) != 0) {
                z4 = bottomActions.showViewServer;
            }
            return bottomActions.copy(z2, z3, z4);
        }

        public final boolean component1() {
            return this.showUploadEmoji;
        }

        public final boolean component2() {
            return this.showJoinServer;
        }

        public final boolean component3() {
            return this.showViewServer;
        }

        public final BottomActions copy(boolean z2, boolean z3, boolean z4) {
            return new BottomActions(z2, z3, z4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BottomActions)) {
                return false;
            }
            BottomActions bottomActions = (BottomActions) obj;
            return this.showUploadEmoji == bottomActions.showUploadEmoji && this.showJoinServer == bottomActions.showJoinServer && this.showViewServer == bottomActions.showViewServer;
        }

        public final boolean getShowJoinServer() {
            return this.showJoinServer;
        }

        public final boolean getShowUploadEmoji() {
            return this.showUploadEmoji;
        }

        public final boolean getShowViewServer() {
            return this.showViewServer;
        }

        public int hashCode() {
            boolean z2 = this.showUploadEmoji;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.showJoinServer;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.showViewServer;
            if (!z4) {
                i = z4 ? 1 : 0;
            }
            return i7 + i;
        }

        public String toString() {
            StringBuilder R = a.R("BottomActions(showUploadEmoji=");
            R.append(this.showUploadEmoji);
            R.append(", showJoinServer=");
            R.append(this.showJoinServer);
            R.append(", showViewServer=");
            return a.M(R, this.showViewServer, ")");
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;", "observeStores", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<StoreState> observeStores(final long j) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<StoreState> c = Observable.c(companion.getGuilds().observeGuild(j), companion.getGuildProfiles().observeGuildProfile(j), companion.getReadStates().getIsUnread(j), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), StoreUser.observeMe$default(companion.getUsers(), false, 1, null).Y(new j0.k.b<MeUser, Observable<? extends GuildMember>>() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel$Companion$observeStores$1
                public final Observable<? extends GuildMember> call(final MeUser meUser) {
                    return (Observable<R>) StoreStream.Companion.getGuilds().observeComputed(j, d0.t.m.listOf(Long.valueOf(meUser.getId()))).F(new j0.k.b<Map<Long, ? extends GuildMember>, GuildMember>() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel$Companion$observeStores$1.1
                        @Override // j0.k.b
                        public /* bridge */ /* synthetic */ GuildMember call(Map<Long, ? extends GuildMember> map) {
                            return call2((Map<Long, GuildMember>) map);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final GuildMember call2(Map<Long, GuildMember> map) {
                            return map.get(Long.valueOf(MeUser.this.getId()));
                        }
                    });
                }
            }), companion.getUserSettings().observeRestrictedGuildIds(), companion.getEmojis().getEmojiSet(new StoreEmoji.EmojiContext.GuildProfile(j), true, false).F(new j0.k.b<EmojiSet, List<Emoji>>() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel$Companion$observeStores$2
                public final List<Emoji> call(EmojiSet emojiSet) {
                    return emojiSet.customEmojis.get(Long.valueOf(j));
                }
            }), companion.getLurking().isLurkingObs(j), GuildChannelsInfo.Companion.get(j), WidgetGuildProfileSheetViewModel$Companion$observeStores$3.INSTANCE);
            m.checkNotNullExpressionValue(c, "Observable.combineLatest…ead\n          )\n        }");
            return c;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ4\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0016\u001a\u00020\u00022\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\n\u0010\u0004R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\tR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u000b\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;", "", "", "component1", "()Z", "component2", "", "Lcom/discord/models/domain/emoji/Emoji;", "component3", "()Ljava/util/List;", "isPremium", "isExpanded", "emojis", "copy", "(ZZLjava/util/List;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/List;", "getEmojis", HookHelper.constructorName, "(ZZLjava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EmojisData {
        private final List<Emoji> emojis;
        private final boolean isExpanded;
        private final boolean isPremium;

        /* JADX WARN: Multi-variable type inference failed */
        public EmojisData(boolean z2, boolean z3, List<? extends Emoji> list) {
            m.checkNotNullParameter(list, "emojis");
            this.isPremium = z2;
            this.isExpanded = z3;
            this.emojis = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ EmojisData copy$default(EmojisData emojisData, boolean z2, boolean z3, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = emojisData.isPremium;
            }
            if ((i & 2) != 0) {
                z3 = emojisData.isExpanded;
            }
            if ((i & 4) != 0) {
                list = emojisData.emojis;
            }
            return emojisData.copy(z2, z3, list);
        }

        public final boolean component1() {
            return this.isPremium;
        }

        public final boolean component2() {
            return this.isExpanded;
        }

        public final List<Emoji> component3() {
            return this.emojis;
        }

        public final EmojisData copy(boolean z2, boolean z3, List<? extends Emoji> list) {
            m.checkNotNullParameter(list, "emojis");
            return new EmojisData(z2, z3, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EmojisData)) {
                return false;
            }
            EmojisData emojisData = (EmojisData) obj;
            return this.isPremium == emojisData.isPremium && this.isExpanded == emojisData.isExpanded && m.areEqual(this.emojis, emojisData.emojis);
        }

        public final List<Emoji> getEmojis() {
            return this.emojis;
        }

        public int hashCode() {
            boolean z2 = this.isPremium;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.isExpanded;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            int i5 = (i4 + i) * 31;
            List<Emoji> list = this.emojis;
            return i5 + (list != null ? list.hashCode() : 0);
        }

        public final boolean isExpanded() {
            return this.isExpanded;
        }

        public final boolean isPremium() {
            return this.isPremium;
        }

        public String toString() {
            StringBuilder R = a.R("EmojisData(isPremium=");
            R.append(this.isPremium);
            R.append(", isExpanded=");
            R.append(this.isExpanded);
            R.append(", emojis=");
            return a.K(R, this.emojis, ")");
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "DismissAndShowToast", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetGuildProfileSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;", "", "component1", "()I", "stringRes", "copy", "(I)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DismissAndShowToast extends Event {
            private final int stringRes;

            public DismissAndShowToast(@StringRes int i) {
                super(null);
                this.stringRes = i;
            }

            public static /* synthetic */ DismissAndShowToast copy$default(DismissAndShowToast dismissAndShowToast, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = dismissAndShowToast.stringRes;
                }
                return dismissAndShowToast.copy(i);
            }

            public final int component1() {
                return this.stringRes;
            }

            public final DismissAndShowToast copy(@StringRes int i) {
                return new DismissAndShowToast(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof DismissAndShowToast) && this.stringRes == ((DismissAndShowToast) obj).stringRes;
                }
                return true;
            }

            public final int getStringRes() {
                return this.stringRes;
            }

            public int hashCode() {
                return this.stringRes;
            }

            public String toString() {
                return a.A(a.R("DismissAndShowToast(stringRes="), this.stringRes, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001Bm\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u001f\u001a\u00020\b\u0012\u0006\u0010 \u001a\u00020\u000b\u0012\b\u0010!\u001a\u0004\u0018\u00010\u000e\u0012\u0010\u0010\"\u001a\f\u0012\b\u0012\u00060\u0012j\u0002`\u00130\u0011\u0012\u0006\u0010#\u001a\u00020\u0016\u0012\f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00190\u0011\u0012\u0006\u0010%\u001a\u00020\u0016\u0012\u0006\u0010&\u001a\u00020\u0016¢\u0006\u0004\b@\u0010AJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0014\u001a\f\u0012\b\u0012\u00060\u0012j\u0002`\u00130\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0016\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00190\u0011HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0015J\u0010\u0010\u001b\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0018J\u0010\u0010\u001c\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0018J\u008a\u0001\u0010'\u001a\u00020\u00002\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u001f\u001a\u00020\b2\b\b\u0002\u0010 \u001a\u00020\u000b2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u000e2\u0012\b\u0002\u0010\"\u001a\f\u0012\b\u0012\u00060\u0012j\u0002`\u00130\u00112\b\b\u0002\u0010#\u001a\u00020\u00162\u000e\b\u0002\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00190\u00112\b\b\u0002\u0010%\u001a\u00020\u00162\b\b\u0002\u0010&\u001a\u00020\u0016HÆ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010*\u001a\u00020)HÖ\u0001¢\u0006\u0004\b*\u0010+J\u0010\u0010-\u001a\u00020,HÖ\u0001¢\u0006\u0004\b-\u0010.J\u001a\u00100\u001a\u00020\u00162\b\u0010/\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b0\u00101R#\u0010\"\u001a\f\u0012\b\u0012\u00060\u0012j\u0002`\u00130\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00102\u001a\u0004\b3\u0010\u0015R\u0019\u0010#\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b#\u0010\u0018R\u0019\u0010 \u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u00105\u001a\u0004\b6\u0010\rR\u0019\u0010&\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b&\u00104\u001a\u0004\b&\u0010\u0018R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00107\u001a\u0004\b8\u0010\u0004R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00109\u001a\u0004\b:\u0010\u0010R\u0019\u0010%\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b%\u00104\u001a\u0004\b%\u0010\u0018R\u001f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00190\u00118\u0006@\u0006¢\u0006\f\n\u0004\b$\u00102\u001a\u0004\b;\u0010\u0015R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010<\u001a\u0004\b=\u0010\u0007R\u0019\u0010\u001f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010>\u001a\u0004\b?\u0010\n¨\u0006B"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "component2", "()Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "component3", "()Lcom/discord/utilities/channel/GuildChannelsInfo;", "Lcom/discord/models/user/MeUser;", "component4", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/member/GuildMember;", "component5", "()Lcom/discord/models/member/GuildMember;", "", "", "Lcom/discord/primitives/GuildId;", "component6", "()Ljava/util/List;", "", "component7", "()Z", "Lcom/discord/models/domain/emoji/Emoji;", "component8", "component9", "component10", "guild", "guildProfile", "guildChannelsInfo", "me", "computedMe", "restrictedGuildIds", "isDeveloper", "emojis", "isLurking", "isUnread", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;Lcom/discord/utilities/channel/GuildChannelsInfo;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Ljava/util/List;ZLjava/util/List;ZZ)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getRestrictedGuildIds", "Z", "Lcom/discord/models/user/MeUser;", "getMe", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/member/GuildMember;", "getComputedMe", "getEmojis", "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "getGuildProfile", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "getGuildChannelsInfo", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;Lcom/discord/utilities/channel/GuildChannelsInfo;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Ljava/util/List;ZLjava/util/List;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final GuildMember computedMe;
        private final List<Emoji> emojis;
        private final Guild guild;
        private final GuildChannelsInfo guildChannelsInfo;
        private final StoreGuildProfiles.GuildProfileData guildProfile;
        private final boolean isDeveloper;
        private final boolean isLurking;
        private final boolean isUnread;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2830me;
        private final List<Long> restrictedGuildIds;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(Guild guild, StoreGuildProfiles.GuildProfileData guildProfileData, GuildChannelsInfo guildChannelsInfo, MeUser meUser, GuildMember guildMember, List<Long> list, boolean z2, List<? extends Emoji> list2, boolean z3, boolean z4) {
            m.checkNotNullParameter(guildChannelsInfo, "guildChannelsInfo");
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(list, "restrictedGuildIds");
            m.checkNotNullParameter(list2, "emojis");
            this.guild = guild;
            this.guildProfile = guildProfileData;
            this.guildChannelsInfo = guildChannelsInfo;
            this.f2830me = meUser;
            this.computedMe = guildMember;
            this.restrictedGuildIds = list;
            this.isDeveloper = z2;
            this.emojis = list2;
            this.isLurking = z3;
            this.isUnread = z4;
        }

        public final Guild component1() {
            return this.guild;
        }

        public final boolean component10() {
            return this.isUnread;
        }

        public final StoreGuildProfiles.GuildProfileData component2() {
            return this.guildProfile;
        }

        public final GuildChannelsInfo component3() {
            return this.guildChannelsInfo;
        }

        public final MeUser component4() {
            return this.f2830me;
        }

        public final GuildMember component5() {
            return this.computedMe;
        }

        public final List<Long> component6() {
            return this.restrictedGuildIds;
        }

        public final boolean component7() {
            return this.isDeveloper;
        }

        public final List<Emoji> component8() {
            return this.emojis;
        }

        public final boolean component9() {
            return this.isLurking;
        }

        public final StoreState copy(Guild guild, StoreGuildProfiles.GuildProfileData guildProfileData, GuildChannelsInfo guildChannelsInfo, MeUser meUser, GuildMember guildMember, List<Long> list, boolean z2, List<? extends Emoji> list2, boolean z3, boolean z4) {
            m.checkNotNullParameter(guildChannelsInfo, "guildChannelsInfo");
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(list, "restrictedGuildIds");
            m.checkNotNullParameter(list2, "emojis");
            return new StoreState(guild, guildProfileData, guildChannelsInfo, meUser, guildMember, list, z2, list2, z3, z4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guild, storeState.guild) && m.areEqual(this.guildProfile, storeState.guildProfile) && m.areEqual(this.guildChannelsInfo, storeState.guildChannelsInfo) && m.areEqual(this.f2830me, storeState.f2830me) && m.areEqual(this.computedMe, storeState.computedMe) && m.areEqual(this.restrictedGuildIds, storeState.restrictedGuildIds) && this.isDeveloper == storeState.isDeveloper && m.areEqual(this.emojis, storeState.emojis) && this.isLurking == storeState.isLurking && this.isUnread == storeState.isUnread;
        }

        public final GuildMember getComputedMe() {
            return this.computedMe;
        }

        public final List<Emoji> getEmojis() {
            return this.emojis;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final GuildChannelsInfo getGuildChannelsInfo() {
            return this.guildChannelsInfo;
        }

        public final StoreGuildProfiles.GuildProfileData getGuildProfile() {
            return this.guildProfile;
        }

        public final MeUser getMe() {
            return this.f2830me;
        }

        public final List<Long> getRestrictedGuildIds() {
            return this.restrictedGuildIds;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            StoreGuildProfiles.GuildProfileData guildProfileData = this.guildProfile;
            int hashCode2 = (hashCode + (guildProfileData != null ? guildProfileData.hashCode() : 0)) * 31;
            GuildChannelsInfo guildChannelsInfo = this.guildChannelsInfo;
            int hashCode3 = (hashCode2 + (guildChannelsInfo != null ? guildChannelsInfo.hashCode() : 0)) * 31;
            MeUser meUser = this.f2830me;
            int hashCode4 = (hashCode3 + (meUser != null ? meUser.hashCode() : 0)) * 31;
            GuildMember guildMember = this.computedMe;
            int hashCode5 = (hashCode4 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
            List<Long> list = this.restrictedGuildIds;
            int hashCode6 = (hashCode5 + (list != null ? list.hashCode() : 0)) * 31;
            boolean z2 = this.isDeveloper;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode6 + i3) * 31;
            List<Emoji> list2 = this.emojis;
            if (list2 != null) {
                i = list2.hashCode();
            }
            int i6 = (i5 + i) * 31;
            boolean z3 = this.isLurking;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.isUnread;
            if (!z4) {
                i2 = z4 ? 1 : 0;
            }
            return i9 + i2;
        }

        public final boolean isDeveloper() {
            return this.isDeveloper;
        }

        public final boolean isLurking() {
            return this.isLurking;
        }

        public final boolean isUnread() {
            return this.isUnread;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guild=");
            R.append(this.guild);
            R.append(", guildProfile=");
            R.append(this.guildProfile);
            R.append(", guildChannelsInfo=");
            R.append(this.guildChannelsInfo);
            R.append(", me=");
            R.append(this.f2830me);
            R.append(", computedMe=");
            R.append(this.computedMe);
            R.append(", restrictedGuildIds=");
            R.append(this.restrictedGuildIds);
            R.append(", isDeveloper=");
            R.append(this.isDeveloper);
            R.append(", emojis=");
            R.append(this.emojis);
            R.append(", isLurking=");
            R.append(this.isLurking);
            R.append(", isUnread=");
            return a.M(R, this.isUnread, ")");
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\bJ\u001a\u0010\u0013\u001a\u00020\u00022\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;", "", "", "component1", "()Z", "component2", "", "component3", "()I", "canAccessSettings", "ableToInstantInvite", "premiumSubscriptionCount", "copy", "(ZZI)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanAccessSettings", "getAbleToInstantInvite", "I", "getPremiumSubscriptionCount", HookHelper.constructorName, "(ZZI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TabItems {
        private final boolean ableToInstantInvite;
        private final boolean canAccessSettings;
        private final int premiumSubscriptionCount;

        public TabItems(boolean z2, boolean z3, int i) {
            this.canAccessSettings = z2;
            this.ableToInstantInvite = z3;
            this.premiumSubscriptionCount = i;
        }

        public static /* synthetic */ TabItems copy$default(TabItems tabItems, boolean z2, boolean z3, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                z2 = tabItems.canAccessSettings;
            }
            if ((i2 & 2) != 0) {
                z3 = tabItems.ableToInstantInvite;
            }
            if ((i2 & 4) != 0) {
                i = tabItems.premiumSubscriptionCount;
            }
            return tabItems.copy(z2, z3, i);
        }

        public final boolean component1() {
            return this.canAccessSettings;
        }

        public final boolean component2() {
            return this.ableToInstantInvite;
        }

        public final int component3() {
            return this.premiumSubscriptionCount;
        }

        public final TabItems copy(boolean z2, boolean z3, int i) {
            return new TabItems(z2, z3, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TabItems)) {
                return false;
            }
            TabItems tabItems = (TabItems) obj;
            return this.canAccessSettings == tabItems.canAccessSettings && this.ableToInstantInvite == tabItems.ableToInstantInvite && this.premiumSubscriptionCount == tabItems.premiumSubscriptionCount;
        }

        public final boolean getAbleToInstantInvite() {
            return this.ableToInstantInvite;
        }

        public final boolean getCanAccessSettings() {
            return this.canAccessSettings;
        }

        public final int getPremiumSubscriptionCount() {
            return this.premiumSubscriptionCount;
        }

        public int hashCode() {
            boolean z2 = this.canAccessSettings;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.ableToInstantInvite;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return ((i4 + i) * 31) + this.premiumSubscriptionCount;
        }

        public String toString() {
            StringBuilder R = a.R("TabItems(canAccessSettings=");
            R.append(this.canAccessSettings);
            R.append(", ableToInstantInvite=");
            R.append(this.ableToInstantInvite);
            R.append(", premiumSubscriptionCount=");
            return a.A(R, this.premiumSubscriptionCount, ")");
        }
    }

    /* compiled from: WidgetGuildProfileSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Loaded", "Loading", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetGuildProfileSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildProfileSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u0000\n\u0002\b\u001e\b\u0086\b\u0018\u00002\u00020\u0001B\u0091\u0001\u0012\n\u0010&\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010'\u001a\u00020\u0006\u0012\u0006\u0010(\u001a\u00020\u0006\u0012\b\u0010)\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010*\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010+\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010,\u001a\u00020\u000f\u0012\b\u0010-\u001a\u0004\u0018\u00010\f\u0012\b\u0010.\u001a\u0004\u0018\u00010\f\u0012\b\u0010/\u001a\u0004\u0018\u00010\u0014\u0012\b\u00100\u001a\u0004\u0018\u00010\u0017\u0012\u0006\u00101\u001a\u00020\u001a\u0012\u0006\u00102\u001a\u00020\u001d\u0012\u0006\u00103\u001a\u00020 \u0012\u0006\u00104\u001a\u00020#¢\u0006\u0004\bV\u0010WJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000eJ\u0012\u0010\u0013\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000eJ\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÆ\u0003¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÆ\u0003¢\u0006\u0004\b$\u0010%J¸\u0001\u00105\u001a\u00020\u00002\f\b\u0002\u0010&\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010'\u001a\u00020\u00062\b\b\u0002\u0010(\u001a\u00020\u00062\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010,\u001a\u00020\u000f2\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010.\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u00100\u001a\u0004\u0018\u00010\u00172\b\b\u0002\u00101\u001a\u00020\u001a2\b\b\u0002\u00102\u001a\u00020\u001d2\b\b\u0002\u00103\u001a\u00020 2\b\b\u0002\u00104\u001a\u00020#HÆ\u0001¢\u0006\u0004\b5\u00106J\u0010\u00107\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b7\u0010\bJ\u0010\u00108\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b8\u00109J\u001a\u0010<\u001a\u00020 2\b\u0010;\u001a\u0004\u0018\u00010:HÖ\u0003¢\u0006\u0004\b<\u0010=R\u001d\u0010&\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010>\u001a\u0004\b?\u0010\u0005R\u0019\u00103\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010@\u001a\u0004\b3\u0010\"R\u001b\u0010*\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010A\u001a\u0004\bB\u0010\bR\u001b\u0010-\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010C\u001a\u0004\bD\u0010\u000eR\u0019\u00102\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010E\u001a\u0004\bF\u0010\u001fR\u0019\u0010(\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010A\u001a\u0004\bG\u0010\bR\u0019\u0010,\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010H\u001a\u0004\bI\u0010\u0011R\u001b\u0010/\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010J\u001a\u0004\bK\u0010\u0016R\u0019\u0010'\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010A\u001a\u0004\bL\u0010\bR\u0019\u00104\u001a\u00020#8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010M\u001a\u0004\bN\u0010%R\u001b\u0010)\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010A\u001a\u0004\bO\u0010\bR\u001b\u0010.\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010C\u001a\u0004\bP\u0010\u000eR\u001b\u0010+\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010C\u001a\u0004\bQ\u0010\u000eR\u0019\u00101\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010R\u001a\u0004\bS\u0010\u001cR\u001b\u00100\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010T\u001a\u0004\bU\u0010\u0019¨\u0006X"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "component4", "component5", "", "component6", "()Ljava/lang/Integer;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;", "component7", "()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;", "component8", "component9", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;", "component10", "()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;", "component11", "()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;", "component12", "()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;", "component13", "()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;", "", "component14", "()Z", "Lcom/discord/models/user/MeUser;", "component15", "()Lcom/discord/models/user/MeUser;", "guildId", "guildName", "guildShortName", "guildIcon", "guildDescription", "verifiedPartneredIconRes", "banner", "onlineCount", "memberCount", "tabItems", "actions", "emojisData", "bottomActions", "isGuildHub", "meUser", "copy", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;ZLcom/discord/models/user/MeUser;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;", "toString", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "Z", "Ljava/lang/String;", "getGuildDescription", "Ljava/lang/Integer;", "getOnlineCount", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;", "getBottomActions", "getGuildShortName", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;", "getBanner", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;", "getTabItems", "getGuildName", "Lcom/discord/models/user/MeUser;", "getMeUser", "getGuildIcon", "getMemberCount", "getVerifiedPartneredIconRes", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;", "getEmojisData", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;", "getActions", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;ZLcom/discord/models/user/MeUser;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final Actions actions;
            private final Banner banner;
            private final BottomActions bottomActions;
            private final EmojisData emojisData;
            private final String guildDescription;
            private final String guildIcon;
            private final long guildId;
            private final String guildName;
            private final String guildShortName;
            private final boolean isGuildHub;
            private final MeUser meUser;
            private final Integer memberCount;
            private final Integer onlineCount;
            private final TabItems tabItems;
            private final Integer verifiedPartneredIconRes;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(long j, String str, String str2, String str3, String str4, Integer num, Banner banner, Integer num2, Integer num3, TabItems tabItems, Actions actions, EmojisData emojisData, BottomActions bottomActions, boolean z2, MeUser meUser) {
                super(null);
                m.checkNotNullParameter(str, "guildName");
                m.checkNotNullParameter(str2, "guildShortName");
                m.checkNotNullParameter(banner, "banner");
                m.checkNotNullParameter(emojisData, "emojisData");
                m.checkNotNullParameter(bottomActions, "bottomActions");
                m.checkNotNullParameter(meUser, "meUser");
                this.guildId = j;
                this.guildName = str;
                this.guildShortName = str2;
                this.guildIcon = str3;
                this.guildDescription = str4;
                this.verifiedPartneredIconRes = num;
                this.banner = banner;
                this.onlineCount = num2;
                this.memberCount = num3;
                this.tabItems = tabItems;
                this.actions = actions;
                this.emojisData = emojisData;
                this.bottomActions = bottomActions;
                this.isGuildHub = z2;
                this.meUser = meUser;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, long j, String str, String str2, String str3, String str4, Integer num, Banner banner, Integer num2, Integer num3, TabItems tabItems, Actions actions, EmojisData emojisData, BottomActions bottomActions, boolean z2, MeUser meUser, int i, Object obj) {
                return loaded.copy((i & 1) != 0 ? loaded.guildId : j, (i & 2) != 0 ? loaded.guildName : str, (i & 4) != 0 ? loaded.guildShortName : str2, (i & 8) != 0 ? loaded.guildIcon : str3, (i & 16) != 0 ? loaded.guildDescription : str4, (i & 32) != 0 ? loaded.verifiedPartneredIconRes : num, (i & 64) != 0 ? loaded.banner : banner, (i & 128) != 0 ? loaded.onlineCount : num2, (i & 256) != 0 ? loaded.memberCount : num3, (i & 512) != 0 ? loaded.tabItems : tabItems, (i & 1024) != 0 ? loaded.actions : actions, (i & 2048) != 0 ? loaded.emojisData : emojisData, (i & 4096) != 0 ? loaded.bottomActions : bottomActions, (i & 8192) != 0 ? loaded.isGuildHub : z2, (i & 16384) != 0 ? loaded.meUser : meUser);
            }

            public final long component1() {
                return this.guildId;
            }

            public final TabItems component10() {
                return this.tabItems;
            }

            public final Actions component11() {
                return this.actions;
            }

            public final EmojisData component12() {
                return this.emojisData;
            }

            public final BottomActions component13() {
                return this.bottomActions;
            }

            public final boolean component14() {
                return this.isGuildHub;
            }

            public final MeUser component15() {
                return this.meUser;
            }

            public final String component2() {
                return this.guildName;
            }

            public final String component3() {
                return this.guildShortName;
            }

            public final String component4() {
                return this.guildIcon;
            }

            public final String component5() {
                return this.guildDescription;
            }

            public final Integer component6() {
                return this.verifiedPartneredIconRes;
            }

            public final Banner component7() {
                return this.banner;
            }

            public final Integer component8() {
                return this.onlineCount;
            }

            public final Integer component9() {
                return this.memberCount;
            }

            public final Loaded copy(long j, String str, String str2, String str3, String str4, Integer num, Banner banner, Integer num2, Integer num3, TabItems tabItems, Actions actions, EmojisData emojisData, BottomActions bottomActions, boolean z2, MeUser meUser) {
                m.checkNotNullParameter(str, "guildName");
                m.checkNotNullParameter(str2, "guildShortName");
                m.checkNotNullParameter(banner, "banner");
                m.checkNotNullParameter(emojisData, "emojisData");
                m.checkNotNullParameter(bottomActions, "bottomActions");
                m.checkNotNullParameter(meUser, "meUser");
                return new Loaded(j, str, str2, str3, str4, num, banner, num2, num3, tabItems, actions, emojisData, bottomActions, z2, meUser);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.guildId == loaded.guildId && m.areEqual(this.guildName, loaded.guildName) && m.areEqual(this.guildShortName, loaded.guildShortName) && m.areEqual(this.guildIcon, loaded.guildIcon) && m.areEqual(this.guildDescription, loaded.guildDescription) && m.areEqual(this.verifiedPartneredIconRes, loaded.verifiedPartneredIconRes) && m.areEqual(this.banner, loaded.banner) && m.areEqual(this.onlineCount, loaded.onlineCount) && m.areEqual(this.memberCount, loaded.memberCount) && m.areEqual(this.tabItems, loaded.tabItems) && m.areEqual(this.actions, loaded.actions) && m.areEqual(this.emojisData, loaded.emojisData) && m.areEqual(this.bottomActions, loaded.bottomActions) && this.isGuildHub == loaded.isGuildHub && m.areEqual(this.meUser, loaded.meUser);
            }

            public final Actions getActions() {
                return this.actions;
            }

            public final Banner getBanner() {
                return this.banner;
            }

            public final BottomActions getBottomActions() {
                return this.bottomActions;
            }

            public final EmojisData getEmojisData() {
                return this.emojisData;
            }

            public final String getGuildDescription() {
                return this.guildDescription;
            }

            public final String getGuildIcon() {
                return this.guildIcon;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final String getGuildName() {
                return this.guildName;
            }

            public final String getGuildShortName() {
                return this.guildShortName;
            }

            public final MeUser getMeUser() {
                return this.meUser;
            }

            public final Integer getMemberCount() {
                return this.memberCount;
            }

            public final Integer getOnlineCount() {
                return this.onlineCount;
            }

            public final TabItems getTabItems() {
                return this.tabItems;
            }

            public final Integer getVerifiedPartneredIconRes() {
                return this.verifiedPartneredIconRes;
            }

            public int hashCode() {
                int a = b.a(this.guildId) * 31;
                String str = this.guildName;
                int i = 0;
                int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.guildShortName;
                int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
                String str3 = this.guildIcon;
                int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
                String str4 = this.guildDescription;
                int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
                Integer num = this.verifiedPartneredIconRes;
                int hashCode5 = (hashCode4 + (num != null ? num.hashCode() : 0)) * 31;
                Banner banner = this.banner;
                int hashCode6 = (hashCode5 + (banner != null ? banner.hashCode() : 0)) * 31;
                Integer num2 = this.onlineCount;
                int hashCode7 = (hashCode6 + (num2 != null ? num2.hashCode() : 0)) * 31;
                Integer num3 = this.memberCount;
                int hashCode8 = (hashCode7 + (num3 != null ? num3.hashCode() : 0)) * 31;
                TabItems tabItems = this.tabItems;
                int hashCode9 = (hashCode8 + (tabItems != null ? tabItems.hashCode() : 0)) * 31;
                Actions actions = this.actions;
                int hashCode10 = (hashCode9 + (actions != null ? actions.hashCode() : 0)) * 31;
                EmojisData emojisData = this.emojisData;
                int hashCode11 = (hashCode10 + (emojisData != null ? emojisData.hashCode() : 0)) * 31;
                BottomActions bottomActions = this.bottomActions;
                int hashCode12 = (hashCode11 + (bottomActions != null ? bottomActions.hashCode() : 0)) * 31;
                boolean z2 = this.isGuildHub;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode12 + i2) * 31;
                MeUser meUser = this.meUser;
                if (meUser != null) {
                    i = meUser.hashCode();
                }
                return i4 + i;
            }

            public final boolean isGuildHub() {
                return this.isGuildHub;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(guildId=");
                R.append(this.guildId);
                R.append(", guildName=");
                R.append(this.guildName);
                R.append(", guildShortName=");
                R.append(this.guildShortName);
                R.append(", guildIcon=");
                R.append(this.guildIcon);
                R.append(", guildDescription=");
                R.append(this.guildDescription);
                R.append(", verifiedPartneredIconRes=");
                R.append(this.verifiedPartneredIconRes);
                R.append(", banner=");
                R.append(this.banner);
                R.append(", onlineCount=");
                R.append(this.onlineCount);
                R.append(", memberCount=");
                R.append(this.memberCount);
                R.append(", tabItems=");
                R.append(this.tabItems);
                R.append(", actions=");
                R.append(this.actions);
                R.append(", emojisData=");
                R.append(this.emojisData);
                R.append(", bottomActions=");
                R.append(this.bottomActions);
                R.append(", isGuildHub=");
                R.append(this.isGuildHub);
                R.append(", meUser=");
                R.append(this.meUser);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetGuildProfileSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetGuildProfileSheetViewModel(StoreUserSettings storeUserSettings, StoreUserGuildSettings storeUserGuildSettings, StoreMessageAck storeMessageAck, boolean z2, RestAPI restAPI, StoreLurking storeLurking, StoreAnalytics storeAnalytics, long j, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getUserSettings() : storeUserSettings, (i & 2) != 0 ? StoreStream.Companion.getUserGuildSettings() : storeUserGuildSettings, (i & 4) != 0 ? StoreStream.Companion.getMessageAck() : storeMessageAck, z2, (i & 16) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 32) != 0 ? StoreStream.Companion.getLurking() : storeLurking, (i & 64) != 0 ? StoreStream.Companion.getAnalytics() : storeAnalytics, j, (i & 256) != 0 ? Companion.observeStores(j) : observable);
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0057  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x005b  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00ad  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x00b3  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0112  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0119  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x011d  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x0132  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void handleLoadedGuild(com.discord.models.guild.Guild r29, com.discord.api.guild.preview.GuildPreview r30, com.discord.utilities.channel.GuildChannelsInfo r31, com.discord.models.user.MeUser r32, com.discord.models.member.GuildMember r33, java.util.List<java.lang.Long> r34, boolean r35, java.util.List<? extends com.discord.models.domain.emoji.Emoji> r36, boolean r37, boolean r38) {
        /*
            Method dump skipped, instructions count: 344
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel.handleLoadedGuild(com.discord.models.guild.Guild, com.discord.api.guild.preview.GuildPreview, com.discord.utilities.channel.GuildChannelsInfo, com.discord.models.user.MeUser, com.discord.models.member.GuildMember, java.util.List, boolean, java.util.List, boolean, boolean):void");
    }

    private final void handleLoadedGuildPreview(GuildPreview guildPreview, MeUser meUser) {
        Integer valueOf;
        if (guildPreview.f().contains(GuildFeature.VERIFIED)) {
            valueOf = Integer.valueOf((int) R.drawable.ic_verified_badge);
        } else {
            valueOf = guildPreview.f().contains(GuildFeature.PARTNERED) ? Integer.valueOf((int) R.drawable.ic_partnered_badge) : null;
        }
        Integer num = valueOf;
        Banner banner = new Banner(guildPreview.h(), guildPreview.j(), Banner.Type.SPLASH);
        BottomActions bottomActions = new BottomActions(false, true, !this.viewingGuild);
        long h = guildPreview.h();
        String i = guildPreview.i();
        String computeShortName = GuildUtilsKt.computeShortName(guildPreview.i());
        String g = guildPreview.g();
        String c = guildPreview.c();
        Integer b2 = guildPreview.b();
        Integer a = guildPreview.a();
        boolean isPremium = UserUtils.INSTANCE.isPremium(meUser);
        boolean z2 = this.isEmojiSectionExpanded;
        List<GuildEmoji> d = guildPreview.d();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(d, 10));
        Iterator it = d.iterator();
        while (it.hasNext()) {
            it = it;
            b2 = b2;
            a = a;
            arrayList.add(new ModelEmojiCustom((GuildEmoji) it.next(), guildPreview.h()));
        }
        updateViewState(new ViewState.Loaded(h, i, computeShortName, g, c, num, banner, b2, a, null, null, new EmojisData(isPremium, z2, arrayList), bottomActions, guildPreview.f().contains(GuildFeature.HUB), meUser));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Guild component1 = storeState.component1();
        StoreGuildProfiles.GuildProfileData component2 = storeState.component2();
        GuildChannelsInfo component3 = storeState.component3();
        MeUser component4 = storeState.component4();
        GuildMember component5 = storeState.component5();
        List<Long> component6 = storeState.component6();
        boolean component7 = storeState.component7();
        List<Emoji> component8 = storeState.component8();
        boolean component9 = storeState.component9();
        boolean component10 = storeState.component10();
        StoreGuildProfiles.FetchStates fetchStates = null;
        GuildPreview guildPreview = null;
        if (component1 != null) {
            if (component2 != null) {
                guildPreview = component2.getData();
            }
            handleLoadedGuild(component1, guildPreview, component3, component4, component5, component6, component7, component8, component9, component10);
            return;
        }
        StoreGuildProfiles.FetchStates fetchState = component2 != null ? component2.getFetchState() : null;
        StoreGuildProfiles.FetchStates fetchStates2 = StoreGuildProfiles.FetchStates.SUCCEEDED;
        if (fetchState != fetchStates2 || component2.getData() == null) {
            if ((component2 != null ? component2.getFetchState() : null) != StoreGuildProfiles.FetchStates.FAILED) {
                if (component2 != null) {
                    fetchStates = component2.getFetchState();
                }
                if (!(fetchStates == fetchStates2 && component2.getData() == null)) {
                    updateViewState(ViewState.Loading.INSTANCE);
                    return;
                }
            }
            updateViewState(ViewState.Invalid.INSTANCE);
            return;
        }
        handleLoadedGuildPreview(component2.getData(), component4);
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void onClickEmoji() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && !this.isEmojiSectionExpanded) {
            this.isEmojiSectionExpanded = true;
            updateViewState(ViewState.Loaded.copy$default(loaded, 0L, null, null, null, null, null, null, null, null, null, null, EmojisData.copy$default(loaded.getEmojisData(), false, this.isEmojiSectionExpanded, null, 5, null), null, false, null, 30719, null));
        }
    }

    public final void onClickJoinServer(long j, Fragment fragment) {
        m.checkNotNullParameter(fragment, "fragment");
        Context context = fragment.getContext();
        if (context != null) {
            StoreLurking storeLurking = this.storeLurking;
            m.checkNotNullExpressionValue(context, "it");
            storeLurking.postJoinGuildAsMember(j, context);
        }
    }

    public final void onClickLeaveServer(long j, Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.leaveGuild(j), false, 1, null), this, null, 2, null), WidgetGuildProfileSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildProfileSheetViewModel$onClickLeaveServer$1(function0));
    }

    public final void onClickMarkAsRead(Context context, long j) {
        this.messageAck.ackGuild(context, j, new WidgetGuildProfileSheetViewModel$onClickMarkAsRead$1(this));
    }

    public final void onClickResetNickname(long j, Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.changeGuildNickname(j, new RestAPIParams.Nick("")), false, 1, null), this, null, 2, null), WidgetGuildProfileSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildProfileSheetViewModel$onClickResetNickname$1(function0));
    }

    public final void onClickSaveNickname(long j, String str, Function0<Unit> function0) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NICK);
        m.checkNotNullParameter(function0, "onSuccess");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.changeGuildNickname(j, new RestAPIParams.Nick(str)), false, 1, null), this, null, 2, null), WidgetGuildProfileSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildProfileSheetViewModel$onClickSaveNickname$1(function0));
    }

    public final void onClickViewServer(long j, Long l) {
        StoreLurking.startLurkingAndNavigate$default(this.storeLurking, j, l, null, 4, null);
    }

    public final void setAllowDM(AppActivity appActivity, long j, boolean z2) {
        this.storeUserSettings.setRestrictedGuildId(appActivity, j, !z2);
    }

    public final void setHideMutedChannels(long j, boolean z2) {
        this.storeUserGuildSettings.setHideMutedChannels(j, z2);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildProfileSheetViewModel(StoreUserSettings storeUserSettings, StoreUserGuildSettings storeUserGuildSettings, StoreMessageAck storeMessageAck, boolean z2, RestAPI restAPI, StoreLurking storeLurking, StoreAnalytics storeAnalytics, long j, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(storeUserSettings, "storeUserSettings");
        m.checkNotNullParameter(storeUserGuildSettings, "storeUserGuildSettings");
        m.checkNotNullParameter(storeMessageAck, "messageAck");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(storeLurking, "storeLurking");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(observable, "storeObservable");
        this.storeUserSettings = storeUserSettings;
        this.storeUserGuildSettings = storeUserGuildSettings;
        this.messageAck = storeMessageAck;
        this.viewingGuild = z2;
        this.restAPI = restAPI;
        this.storeLurking = storeLurking;
        this.guildId = j;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetGuildProfileSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        storeAnalytics.trackGuildProfileOpened(j);
    }
}
