package com.discord.widgets.guilds.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.text.Editable;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.b.d;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.a.y.c0;
import b.a.y.d0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.ViewDialogConfirmationBinding;
import com.discord.databinding.WidgetChangeGuildIdentityBinding;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guilds.profile.WidgetChangeGuildIdentityViewModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.t;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetChangeGuildIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 92\u00020\u0001:\u00019B\u0007¢\u0006\u0004\b8\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\r\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\fJ\u0017\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000e\u0010\fJ\u000f\u0010\u000f\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0019\u0010\u0013\u001a\u00020\b2\b\b\u0001\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0013\u0010\u0016J\u001f\u0010\u001a\u001a\u00020\b2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0015H\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ\u001f\u0010\u001c\u001a\u00020\b2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0015H\u0016¢\u0006\u0004\b\u001c\u0010\u001bJ\u0017\u0010\u001f\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\bH\u0016¢\u0006\u0004\b!\u0010\u0010R\"\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\b0\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\"\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\b0\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010$R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u001d\u00101\u001a\u00020,8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R\u001d\u00107\u001a\u0002028B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b3\u00104\u001a\u0004\b5\u00106¨\u0006:"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentity;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loaded;", "viewState", "", "handleBackPressed", "(Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loaded;)Z", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState;", "", "configureUI", "(Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState;)V", "configureNickname", "(Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loaded;)V", "configureUpsell", "configureAvatar", "navigateToUpsellModal", "()V", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "showToast", "(I)V", "", "(Ljava/lang/String;)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "mimeType", "onImageChosen", "(Landroid/net/Uri;Ljava/lang/String;)V", "onImageCropped", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lkotlin/Function1;", "avatarSelectedResult", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "imagesChangeDetector", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "imageSelectedResult", "Ljava/util/concurrent/atomic/AtomicBoolean;", "discardConfirmed", "Ljava/util/concurrent/atomic/AtomicBoolean;", "Lcom/discord/databinding/WidgetChangeGuildIdentityBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChangeGuildIdentityBinding;", "binding", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChangeGuildIdentity extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChangeGuildIdentity.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChangeGuildIdentityBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final String USER_GUILD_PROFILE_VIEWED_CACHE_KEY = "USER_GUILD_PROFILE_VIEWED_CACHE_KEY";
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChangeGuildIdentity$binding$2.INSTANCE, null, 2, null);
    private Function1<? super String, Unit> imageSelectedResult = WidgetChangeGuildIdentity$imageSelectedResult$1.INSTANCE;
    private Function1<? super String, Unit> avatarSelectedResult = WidgetChangeGuildIdentity$avatarSelectedResult$1.INSTANCE;
    private final MGImages.DistinctChangeDetector imagesChangeDetector = new MGImages.DistinctChangeDetector();
    private final AtomicBoolean discardConfirmed = new AtomicBoolean(false);

    /* compiled from: WidgetChangeGuildIdentity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ)\u0010\n\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentity$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "source", "Landroid/content/Context;", "context", "", "launch", "(JLjava/lang/String;Landroid/content/Context;)V", WidgetChangeGuildIdentity.USER_GUILD_PROFILE_VIEWED_CACHE_KEY, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, String str, Context context) {
            m.checkNotNullParameter(str, "source");
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", j).putExtra("com.discord.intent.extra.EXTRA_SOURCE", str);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…nts.EXTRA_SOURCE, source)");
            j.d(context, WidgetChangeGuildIdentity.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChangeGuildIdentity() {
        super(R.layout.widget_change_guild_identity);
        WidgetChangeGuildIdentity$viewModel$2 widgetChangeGuildIdentity$viewModel$2 = new WidgetChangeGuildIdentity$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetChangeGuildIdentityViewModel.class), new WidgetChangeGuildIdentity$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetChangeGuildIdentity$viewModel$2));
    }

    private final void configureAvatar(final WidgetChangeGuildIdentityViewModel.ViewState.Loaded loaded) {
        SimpleDraweeView simpleDraweeView = getBinding().d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildAvatar");
        IconUtils.setIcon$default(simpleDraweeView, loaded.getDisplayedAvatarURL(), 0, (Function1) null, this.imagesChangeDetector, 12, (Object) null);
        this.avatarSelectedResult = new WidgetChangeGuildIdentity$configureAvatar$1(this);
        final List mutableListOf = n.mutableListOf(new d0(getString(R.string.change_guild_member_avatar), null, null, null, null, null, null, 116));
        if (loaded.getDisplayingGuildAvatar()) {
            mutableListOf.add(new d0(getString(R.string.change_identity_modal_reset_primary_avatar), null, null, null, null, Integer.valueOf(ColorCompat.getColor(requireContext(), (int) R.color.status_red_500)), null, 84));
        }
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$configureAvatar$2

            /* compiled from: WidgetChangeGuildIdentity.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "selectedItemPosition", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$configureAvatar$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Integer, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                    invoke(num.intValue());
                    return Unit.a;
                }

                public final void invoke(int i) {
                    Function1 function1;
                    WidgetChangeGuildIdentityViewModel viewModel;
                    if (i == 0) {
                        WidgetChangeGuildIdentity widgetChangeGuildIdentity = WidgetChangeGuildIdentity.this;
                        function1 = widgetChangeGuildIdentity.avatarSelectedResult;
                        widgetChangeGuildIdentity.imageSelectedResult = function1;
                        WidgetChangeGuildIdentity.this.openMediaChooser();
                    } else if (i == 1) {
                        viewModel = WidgetChangeGuildIdentity.this.getViewModel();
                        viewModel.updateAvatar(null);
                    }
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (loaded.getShouldUpsell()) {
                    WidgetChangeGuildIdentity.this.navigateToUpsellModal();
                    return;
                }
                c0.a aVar = c0.k;
                FragmentManager childFragmentManager = WidgetChangeGuildIdentity.this.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                aVar.a(childFragmentManager, "", mutableListOf, false, new AnonymousClass1());
            }
        });
    }

    private final void configureNickname(final WidgetChangeGuildIdentityViewModel.ViewState.Loaded loaded) {
        String str;
        TextInputLayout textInputLayout = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout, "textInputLayout");
        String currentNickname = loaded.getCurrentNickname();
        boolean z2 = false;
        textInputLayout.setEndIconVisible(!(currentNickname == null || currentNickname.length() == 0));
        Editable editable = null;
        ViewExtensions.setEnabledAndAlpha$default(textInputLayout, loaded.getCanChangeNickname(), 0.0f, 2, null);
        if (loaded.getCanChangeNickname()) {
            str = getString(R.string.nickname);
        } else {
            str = getString(R.string.change_identity_modal_change_nickname_disabled);
        }
        textInputLayout.setHint(str);
        textInputLayout.setPlaceholderText(loaded.getMeUser().getUsername());
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetChangeGuildIdentity$configureNickname$$inlined$also$lambda$1(this, loaded));
        textInputLayout.setEndIconOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$configureNickname$$inlined$also$lambda$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChangeGuildIdentityViewModel viewModel;
                viewModel = WidgetChangeGuildIdentity.this.getViewModel();
                viewModel.updateNickname("");
            }
        });
        TextInputLayout textInputLayout2 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.setNicknameText");
        String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout2);
        if (!m.areEqual(loaded.getCurrentNickname(), textOrEmpty)) {
            ViewExtensions.setText(textInputLayout, loaded.getCurrentNickname());
            if (textOrEmpty.length() == 0) {
                z2 = true;
            }
            if (z2) {
                EditText editText = textInputLayout.getEditText();
                if (editText != null) {
                    editable = editText.getText();
                }
                Selection.setSelection(editable, ViewExtensions.getTextOrEmpty(textInputLayout).length());
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetChangeGuildIdentityViewModel.ViewState viewState) {
        if (viewState instanceof WidgetChangeGuildIdentityViewModel.ViewState.Loaded) {
            if (!getViewModel().getTrackedModalOpen()) {
                AnalyticsTracker.INSTANCE.openModal("Change Server Identity", new Traits.Source(Traits.Location.Page.GUILD_CHANNEL, getViewModel().getSourceSection(), null, null, null, 28, null));
                getViewModel().setTrackedModalOpen(true);
            }
            WidgetChangeGuildIdentityViewModel.ViewState.Loaded loaded = (WidgetChangeGuildIdentityViewModel.ViewState.Loaded) viewState;
            configureNickname(loaded);
            configureAvatar(loaded);
            configureUpsell(loaded);
            Func0<Boolean> widgetChangeGuildIdentity$configureUI$1 = new Func0<Boolean>() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$configureUI$1
                @Override // rx.functions.Func0, java.util.concurrent.Callable
                public final Boolean call() {
                    boolean handleBackPressed;
                    handleBackPressed = WidgetChangeGuildIdentity.this.handleBackPressed((WidgetChangeGuildIdentityViewModel.ViewState.Loaded) viewState);
                    return Boolean.valueOf(handleBackPressed);
                }
            };
            int i = 0;
            AppFragment.setOnBackPressed$default(this, widgetChangeGuildIdentity$configureUI$1, 0, 2, null);
            DimmerView.setDimmed$default(getBinding().f2235b, loaded.getDimmed(), false, 2, null);
            FloatingActionButton floatingActionButton = getBinding().f;
            m.checkNotNullExpressionValue(floatingActionButton, "binding.saveFab");
            if (!loaded.getShowSaveFab()) {
                i = 8;
            }
            floatingActionButton.setVisibility(i);
        }
    }

    private final void configureUpsell(WidgetChangeGuildIdentityViewModel.ViewState.Loaded loaded) {
        LinearLayout linearLayout = getBinding().h;
        m.checkNotNullExpressionValue(linearLayout, "binding.upsellSection");
        linearLayout.setVisibility(loaded.getShouldUpsell() ? 0 : 8);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$configureUpsell$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChangeGuildIdentity.this.navigateToUpsellModal();
            }
        });
        if (!getViewModel().getTrackedUpsell() && loaded.getShouldUpsell()) {
            AnalyticsTracker.premiumUpsellViewed$default(AnalyticsTracker.INSTANCE, AnalyticsTracker.PremiumUpsellType.PerGuildIdentityInline, new Traits.Location(Traits.Location.Page.GUILD_CHANNEL, "Change Per Server Identity Modal", "Nitro upsell button", null, null, 24, null), null, null, 12, null);
            getViewModel().setTrackedUpsell(true);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChangeGuildIdentityBinding getBinding() {
        return (WidgetChangeGuildIdentityBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChangeGuildIdentityViewModel getViewModel() {
        return (WidgetChangeGuildIdentityViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean handleBackPressed(WidgetChangeGuildIdentityViewModel.ViewState.Loaded loaded) {
        hideKeyboard(getBinding().g);
        if (!loaded.isDirty() || this.discardConfirmed.get()) {
            return false;
        }
        ViewDialogConfirmationBinding b2 = ViewDialogConfirmationBinding.b(LayoutInflater.from(e()));
        m.checkNotNullExpressionValue(b2, "ViewDialogConfirmationBi…tInflater.from(activity))");
        final AlertDialog create = new AlertDialog.Builder(requireContext()).setView(b2.a).create();
        m.checkNotNullExpressionValue(create, "AlertDialog.Builder(requ…logBinding.root).create()");
        b2.d.setText(R.string.discard_changes);
        b2.e.setText(R.string.discard_changes_description);
        b2.f2167b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$handleBackPressed$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AlertDialog.this.dismiss();
            }
        });
        b2.c.setText(R.string.okay);
        b2.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$handleBackPressed$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AtomicBoolean atomicBoolean;
                atomicBoolean = WidgetChangeGuildIdentity.this.discardConfirmed;
                atomicBoolean.set(true);
                create.dismiss();
                FragmentActivity activity = WidgetChangeGuildIdentity.this.e();
                if (activity != null) {
                    activity.onBackPressed();
                }
            }
        });
        create.show();
        return true;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void navigateToUpsellModal() {
        CharSequence c;
        CharSequence c2;
        d.b bVar = d.k;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        AnalyticsTracker.PremiumUpsellType premiumUpsellType = AnalyticsTracker.PremiumUpsellType.PerGuildIdentityUpsellModal;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        c = b.c(resources, R.string.guild_member_avatar_upsell_title, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        String obj = c.toString();
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        c2 = b.c(resources2, R.string.guild_member_avatar_upsell_body, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        bVar.a(parentFragmentManager, premiumUpsellType, R.drawable.per_guild_identity_modal_image, obj, c2.toString(), Traits.Location.Page.GUILD_CHANNEL, "Change Per Server Identity Modal", "Nitro upsell button");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showToast(@StringRes int i) {
        b.a.d.m.i(this, i, 0, 4);
    }

    @Override // com.discord.app.AppFragment
    public void onImageChosen(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageChosen(uri, str);
        if (!t.startsWith$default(str, "image", false, 2, null)) {
            b.a.d.m.g(getContext(), R.string.user_settings_image_upload_filetype_error, 0, null, 12);
        } else if (m.areEqual(str, "image/gif")) {
            Context context = getContext();
            Function1<? super String, Unit> function1 = this.imageSelectedResult;
            Object obj = function1;
            if (function1 != null) {
                obj = new WidgetChangeGuildIdentity$sam$rx_functions_Action1$0(function1);
            }
            MGImages.requestDataUrl(context, uri, str, (Action1) obj);
        } else {
            MGImages.requestImageCrop(requireContext(), this, uri, 1.0f, 1.0f, 1024);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onImageCropped(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageCropped(uri, str);
        Context context = getContext();
        Function1<? super String, Unit> function1 = this.imageSelectedResult;
        Object obj = function1;
        if (function1 != null) {
            obj = new WidgetChangeGuildIdentity$sam$rx_functions_Action1$0(function1);
        }
        MGImages.requestDataUrl(context, uri, str, (Action1) obj);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(final View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        SharedPreferences.Editor edit = SharedPreferencesProvider.INSTANCE.get().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean(USER_GUILD_PROFILE_VIEWED_CACHE_KEY, true);
        edit.apply();
        setActionBarTitle(R.string.change_identity);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentity$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChangeGuildIdentityViewModel viewModel;
                WidgetChangeGuildIdentityBinding binding;
                viewModel = WidgetChangeGuildIdentity.this.getViewModel();
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "view.context");
                viewModel.saveMemberChanges(context);
                binding = WidgetChangeGuildIdentity.this.getBinding();
                DimmerView.setDimmed$default(binding.f2235b, true, false, 2, null);
            }
        });
        TextInputLayout textInputLayout = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout, "binding.setNicknameText");
        showKeyboard(textInputLayout);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetChangeGuildIdentity.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChangeGuildIdentity$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetChangeGuildIdentity.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChangeGuildIdentity$onViewBoundOrOnResume$2(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showToast(String str) {
        b.a.d.m.j(this, str, 0, 4);
    }
}
