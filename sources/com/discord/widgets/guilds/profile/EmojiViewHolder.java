package com.discord.widgets.guilds.profile;

import andhook.lib.HookHelper;
import android.view.View;
import com.discord.databinding.WidgetGuildProfileEmojiItemBinding;
import com.discord.models.domain.emoji.Emoji;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.widgets.guilds.profile.EmojiItem;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetGuildProfileSheetEmojisAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/guilds/profile/EmojiViewHolder;", "Lcom/discord/widgets/guilds/profile/BaseEmojiViewHolder;", "Lcom/discord/widgets/guilds/profile/EmojiItem;", "data", "", "bind", "(Lcom/discord/widgets/guilds/profile/EmojiItem;)V", "Lcom/discord/databinding/WidgetGuildProfileEmojiItemBinding;", "binding", "Lcom/discord/databinding/WidgetGuildProfileEmojiItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetGuildProfileEmojiItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmojiViewHolder extends BaseEmojiViewHolder {
    private final WidgetGuildProfileEmojiItemBinding binding;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public EmojiViewHolder(com.discord.databinding.WidgetGuildProfileEmojiItemBinding r3) {
        /*
            r2 = this;
            java.lang.String r0 = "binding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            com.facebook.drawee.view.SimpleDraweeView r0 = r3.a
            java.lang.String r1 = "binding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.binding = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.profile.EmojiViewHolder.<init>(com.discord.databinding.WidgetGuildProfileEmojiItemBinding):void");
    }

    @Override // com.discord.widgets.guilds.profile.BaseEmojiViewHolder
    public void bind(EmojiItem emojiItem) {
        m.checkNotNullParameter(emojiItem, "data");
        super.bind(emojiItem);
        SimpleDraweeView simpleDraweeView = this.binding.f2408b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildProfileSheetEmojiImageview");
        int mediaProxySize = IconUtils.getMediaProxySize(simpleDraweeView.getLayoutParams().width);
        SimpleDraweeView simpleDraweeView2 = this.binding.f2408b;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildProfileSheetEmojiImageview");
        Emoji emoji = ((EmojiItem.EmojiData) emojiItem).getEmoji();
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        MGImages.setImage$default(simpleDraweeView2, emoji.getImageUri(true, mediaProxySize, view.getContext()), 0, 0, false, null, null, 124, null);
    }
}
