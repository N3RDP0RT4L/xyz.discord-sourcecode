package com.discord.widgets.guilds.profile;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.WidgetGuildProfileEmojiExtraBinding;
import com.discord.databinding.WidgetGuildProfileEmojiItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.widgets.guilds.profile.EmojiItem;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetGuildProfileSheetEmojisAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b$\u0010%J+\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\f\u001a\u00020\u000b2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u001f\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0019\u0010\u001aR(\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u000b0\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u001c\u0010\"\u001a\b\u0012\u0004\u0012\u00020\b0\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#¨\u0006&"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/guilds/profile/BaseEmojiViewHolder;", "", "Lcom/discord/models/domain/emoji/Emoji;", "emojis", "", "maxEmojisToShow", "Lcom/discord/widgets/guilds/profile/EmojiItem;", "getEmojiItems", "(Ljava/util/List;I)Ljava/util/List;", "", "setData", "(Ljava/util/List;I)V", "getItemCount", "()I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "getItemViewType", "(I)I", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/guilds/profile/BaseEmojiViewHolder;", "holder", "onBindViewHolder", "(Lcom/discord/widgets/guilds/profile/BaseEmojiViewHolder;I)V", "Lkotlin/Function0;", "onClickEmoji", "Lkotlin/jvm/functions/Function0;", "getOnClickEmoji", "()Lkotlin/jvm/functions/Function0;", "setOnClickEmoji", "(Lkotlin/jvm/functions/Function0;)V", "data", "Ljava/util/List;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildProfileSheetEmojisAdapter extends RecyclerView.Adapter<BaseEmojiViewHolder> {
    private List<? extends EmojiItem> data = n.emptyList();
    private Function0<Unit> onClickEmoji = WidgetGuildProfileSheetEmojisAdapter$onClickEmoji$1.INSTANCE;

    private final List<EmojiItem> getEmojiItems(List<? extends Emoji> list, int i) {
        List<Emoji> take = u.take(list, i);
        ArrayList arrayList = new ArrayList();
        for (Emoji emoji : take) {
            arrayList.add(new EmojiItem.EmojiData(emoji));
        }
        int size = list.size() - arrayList.size();
        if (size > 0) {
            if (arrayList.size() == i) {
                arrayList.remove(n.getLastIndex(arrayList));
                size++;
            }
            arrayList.add(new EmojiItem.MoreEmoji(Math.min(size, 99)));
        }
        return arrayList;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.data.get(i).getType();
    }

    public final Function0<Unit> getOnClickEmoji() {
        return this.onClickEmoji;
    }

    public final void setData(List<? extends Emoji> list, int i) {
        m.checkNotNullParameter(list, "emojis");
        this.data = getEmojiItems(list, i);
        notifyDataSetChanged();
    }

    public final void setOnClickEmoji(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onClickEmoji = function0;
    }

    public void onBindViewHolder(BaseEmojiViewHolder baseEmojiViewHolder, int i) {
        m.checkNotNullParameter(baseEmojiViewHolder, "holder");
        baseEmojiViewHolder.bind(this.data.get(i));
        baseEmojiViewHolder.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheetEmojisAdapter$onBindViewHolder$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildProfileSheetEmojisAdapter.this.getOnClickEmoji().invoke();
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public BaseEmojiViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.widget_guild_profile_emoji_item, viewGroup, false);
            Objects.requireNonNull(inflate, "rootView");
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate;
            WidgetGuildProfileEmojiItemBinding widgetGuildProfileEmojiItemBinding = new WidgetGuildProfileEmojiItemBinding(simpleDraweeView, simpleDraweeView);
            m.checkNotNullExpressionValue(widgetGuildProfileEmojiItemBinding, "WidgetGuildProfileEmojiI….context), parent, false)");
            return new EmojiViewHolder(widgetGuildProfileEmojiItemBinding);
        } else if (i == 1) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.widget_guild_profile_emoji_extra, viewGroup, false);
            Objects.requireNonNull(inflate2, "rootView");
            WidgetGuildProfileEmojiExtraBinding widgetGuildProfileEmojiExtraBinding = new WidgetGuildProfileEmojiExtraBinding((TextView) inflate2);
            m.checkNotNullExpressionValue(widgetGuildProfileEmojiExtraBinding, "WidgetGuildProfileEmojiE….context), parent, false)");
            return new MoreEmojiViewHolder(widgetGuildProfileEmojiExtraBinding);
        } else {
            throw new IllegalArgumentException(a.p("invalid view type: ", i));
        }
    }
}
