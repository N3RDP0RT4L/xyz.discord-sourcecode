package com.discord.widgets.guilds.profile;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import b.a.k.b;
import com.discord.databinding.WidgetGuildProfileEmojiExtraBinding;
import com.discord.widgets.guilds.profile.EmojiItem;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetGuildProfileSheetEmojisAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/guilds/profile/MoreEmojiViewHolder;", "Lcom/discord/widgets/guilds/profile/BaseEmojiViewHolder;", "Lcom/discord/widgets/guilds/profile/EmojiItem;", "data", "", "bind", "(Lcom/discord/widgets/guilds/profile/EmojiItem;)V", "Lcom/discord/databinding/WidgetGuildProfileEmojiExtraBinding;", "binding", "Lcom/discord/databinding/WidgetGuildProfileEmojiExtraBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetGuildProfileEmojiExtraBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MoreEmojiViewHolder extends BaseEmojiViewHolder {
    private final WidgetGuildProfileEmojiExtraBinding binding;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public MoreEmojiViewHolder(com.discord.databinding.WidgetGuildProfileEmojiExtraBinding r3) {
        /*
            r2 = this;
            java.lang.String r0 = "binding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            android.widget.TextView r0 = r3.a
            java.lang.String r1 = "binding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.binding = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.profile.MoreEmojiViewHolder.<init>(com.discord.databinding.WidgetGuildProfileEmojiExtraBinding):void");
    }

    @Override // com.discord.widgets.guilds.profile.BaseEmojiViewHolder
    public void bind(EmojiItem emojiItem) {
        CharSequence c;
        m.checkNotNullParameter(emojiItem, "data");
        super.bind(emojiItem);
        TextView textView = this.binding.a;
        m.checkNotNullExpressionValue(textView, "binding.root");
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        Resources resources = view.getResources();
        m.checkNotNullExpressionValue(resources, "itemView.resources");
        c = b.c(resources, R.string.extra_emoji_count, new Object[]{String.valueOf(((EmojiItem.MoreEmoji) emojiItem).getExtraEmojiCount())}, (r4 & 4) != 0 ? b.d.j : null);
        textView.setText(c);
    }
}
