package com.discord.widgets.guilds.profile;

import com.discord.utilities.error.Error;
import com.discord.widgets.guilds.profile.WidgetChangeGuildIdentityViewModel;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: WidgetChangeGuildIdentityViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChangeGuildIdentityViewModel$saveMemberChanges$1 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ WidgetChangeGuildIdentityViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ WidgetChangeGuildIdentityViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChangeGuildIdentityViewModel$saveMemberChanges$1(WidgetChangeGuildIdentityViewModel widgetChangeGuildIdentityViewModel, WidgetChangeGuildIdentityViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = widgetChangeGuildIdentityViewModel;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        String str;
        PublishSubject publishSubject;
        m.checkNotNullParameter(error, "it");
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "it.response");
        if (!response.getMessages().values().isEmpty()) {
            Error.Response response2 = error.getResponse();
            m.checkNotNullExpressionValue(response2, "it.response");
            Object first = u.first(response2.getMessages().values());
            m.checkNotNullExpressionValue(first, "it.response.messages.values.first()");
            if (!((Collection) first).isEmpty()) {
                Error.Response response3 = error.getResponse();
                m.checkNotNullExpressionValue(response3, "it.response");
                Object first2 = u.first(response3.getMessages().values());
                m.checkNotNullExpressionValue(first2, "it.response.messages.values.first()");
                str = (String) u.first((List<? extends Object>) first2);
                publishSubject = this.this$0.eventSubject;
                publishSubject.k.onNext(new WidgetChangeGuildIdentityViewModel.Event.MemberUpdateFailed(str));
                this.this$0.updateViewState(WidgetChangeGuildIdentityViewModel.ViewState.Loaded.copy$default(this.$viewState, null, null, null, false, null, null, false, 63, null));
            }
        }
        Error.Response response4 = error.getResponse();
        m.checkNotNullExpressionValue(response4, "it.response");
        str = response4.getMessage();
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(new WidgetChangeGuildIdentityViewModel.Event.MemberUpdateFailed(str));
        this.this$0.updateViewState(WidgetChangeGuildIdentityViewModel.ViewState.Loaded.copy$default(this.$viewState, null, null, null, false, null, null, false, 63, null));
    }
}
