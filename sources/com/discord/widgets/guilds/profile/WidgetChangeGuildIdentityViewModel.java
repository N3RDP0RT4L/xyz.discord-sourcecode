package com.discord.widgets.guilds.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.guildmember.PatchGuildMemberBody;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.nullserializable.NullSerializable;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: WidgetChangeGuildIdentityViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 42\b\u0012\u0004\u0012\u00020\u00020\u0001:\u00044567B%\u0012\n\u0010+\u001a\u00060)j\u0002`*\u0012\u0006\u0010\u0018\u001a\u00020\f\u0012\b\b\u0002\u0010'\u001a\u00020&¢\u0006\u0004\b2\u00103J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0011\u001a\u00020\u00052\b\u0010\u0010\u001a\u0004\u0018\u00010\fH\u0007¢\u0006\u0004\b\u0011\u0010\u000fJ\u000f\u0010\u0012\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0014H\u0007¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\u0018\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\"\u0010\u001d\u001a\u00020\u001c8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\"\u0010#\u001a\u00020\u001c8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b#\u0010\u001e\u001a\u0004\b$\u0010 \"\u0004\b%\u0010\"R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001d\u0010+\u001a\u00060)j\u0002`*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u001c\u00100\u001a\b\u0012\u0004\u0012\u00020\t0/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101¨\u00068"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$StoreState;)V", "Lrx/Observable;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event;", "observeEvents", "()Lrx/Observable;", "", "nickname", "updateNickname", "(Ljava/lang/String;)V", "dataUrl", "updateAvatar", "clearStatus", "()V", "Landroid/content/Context;", "context", "saveMemberChanges", "(Landroid/content/Context;)V", "sourceSection", "Ljava/lang/String;", "getSourceSection", "()Ljava/lang/String;", "", "trackedUpsell", "Z", "getTrackedUpsell", "()Z", "setTrackedUpsell", "(Z)V", "trackedModalOpen", "getTrackedModalOpen", "setTrackedModalOpen", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "getGuildId", "()J", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "(JLjava/lang/String;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChangeGuildIdentityViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final long guildId;
    private final RestAPI restAPI;
    private final String sourceSection;
    private boolean trackedModalOpen;
    private boolean trackedUpsell;

    /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentityViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetChangeGuildIdentityViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ5\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreUser;", "storeUser", "Lrx/Observable;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStoreState(final long j, StoreGuilds storeGuilds, StoreUser storeUser) {
            Observable<StoreState> h = Observable.h(GuildChannelsInfo.Companion.get(j), storeGuilds.observeGuild(j), StoreUser.observeMe$default(storeUser, false, 1, null), StoreUser.observeMe$default(storeUser, false, 1, null).Y(new b<MeUser, Observable<? extends GuildMember>>() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentityViewModel$Companion$observeStoreState$1
                public final Observable<? extends GuildMember> call(final MeUser meUser) {
                    return (Observable<R>) StoreStream.Companion.getGuilds().observeComputed(j, d0.t.m.listOf(Long.valueOf(meUser.getId()))).F(new b<Map<Long, ? extends GuildMember>, GuildMember>() { // from class: com.discord.widgets.guilds.profile.WidgetChangeGuildIdentityViewModel$Companion$observeStoreState$1.1
                        @Override // j0.k.b
                        public /* bridge */ /* synthetic */ GuildMember call(Map<Long, ? extends GuildMember> map) {
                            return call2((Map<Long, GuildMember>) map);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final GuildMember call2(Map<Long, GuildMember> map) {
                            return map.get(Long.valueOf(MeUser.this.getId()));
                        }
                    });
                }
            }), WidgetChangeGuildIdentityViewModel$Companion$observeStoreState$2.INSTANCE);
            m.checkNotNullExpressionValue(h, "Observable.combineLatest…uildChannelsInfo)\n      }");
            return h;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, long j, StoreGuilds storeGuilds, StoreUser storeUser, int i, Object obj) {
            if ((i & 2) != 0) {
                storeGuilds = StoreStream.Companion.getGuilds();
            }
            if ((i & 4) != 0) {
                storeUser = StoreStream.Companion.getUsers();
            }
            return companion.observeStoreState(j, storeGuilds, storeUser);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event;", "", HookHelper.constructorName, "()V", "MemberUpdateFailed", "MemberUpdateSucceeded", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event$MemberUpdateFailed;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event$MemberUpdateSucceeded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event$MemberUpdateFailed;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event;", "", "component1", "()Ljava/lang/String;", "errorMessage", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event$MemberUpdateFailed;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getErrorMessage", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MemberUpdateFailed extends Event {
            private final String errorMessage;

            public MemberUpdateFailed(String str) {
                super(null);
                this.errorMessage = str;
            }

            public static /* synthetic */ MemberUpdateFailed copy$default(MemberUpdateFailed memberUpdateFailed, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = memberUpdateFailed.errorMessage;
                }
                return memberUpdateFailed.copy(str);
            }

            public final String component1() {
                return this.errorMessage;
            }

            public final MemberUpdateFailed copy(String str) {
                return new MemberUpdateFailed(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof MemberUpdateFailed) && m.areEqual(this.errorMessage, ((MemberUpdateFailed) obj).errorMessage);
                }
                return true;
            }

            public final String getErrorMessage() {
                return this.errorMessage;
            }

            public int hashCode() {
                String str = this.errorMessage;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("MemberUpdateFailed(errorMessage="), this.errorMessage, ")");
            }
        }

        /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event$MemberUpdateSucceeded;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MemberUpdateSucceeded extends Event {
            public static final MemberUpdateSucceeded INSTANCE = new MemberUpdateSucceeded();

            private MemberUpdateSucceeded() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b&\u0010'J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ<\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\nR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\"\u001a\u0004\b#\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b%\u0010\r¨\u0006("}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$StoreState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "component4", "()Lcom/discord/utilities/channel/GuildChannelsInfo;", "guild", "meUser", "member", "guildChannelsInfo", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Lcom/discord/utilities/channel/GuildChannelsInfo;)Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/user/MeUser;", "getMeUser", "Lcom/discord/models/member/GuildMember;", "getMember", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "getGuildChannelsInfo", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Lcom/discord/utilities/channel/GuildChannelsInfo;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Guild guild;
        private final GuildChannelsInfo guildChannelsInfo;
        private final MeUser meUser;
        private final GuildMember member;

        public StoreState(Guild guild, MeUser meUser, GuildMember guildMember, GuildChannelsInfo guildChannelsInfo) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(guildChannelsInfo, "guildChannelsInfo");
            this.guild = guild;
            this.meUser = meUser;
            this.member = guildMember;
            this.guildChannelsInfo = guildChannelsInfo;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Guild guild, MeUser meUser, GuildMember guildMember, GuildChannelsInfo guildChannelsInfo, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = storeState.guild;
            }
            if ((i & 2) != 0) {
                meUser = storeState.meUser;
            }
            if ((i & 4) != 0) {
                guildMember = storeState.member;
            }
            if ((i & 8) != 0) {
                guildChannelsInfo = storeState.guildChannelsInfo;
            }
            return storeState.copy(guild, meUser, guildMember, guildChannelsInfo);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final MeUser component2() {
            return this.meUser;
        }

        public final GuildMember component3() {
            return this.member;
        }

        public final GuildChannelsInfo component4() {
            return this.guildChannelsInfo;
        }

        public final StoreState copy(Guild guild, MeUser meUser, GuildMember guildMember, GuildChannelsInfo guildChannelsInfo) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(guildChannelsInfo, "guildChannelsInfo");
            return new StoreState(guild, meUser, guildMember, guildChannelsInfo);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guild, storeState.guild) && m.areEqual(this.meUser, storeState.meUser) && m.areEqual(this.member, storeState.member) && m.areEqual(this.guildChannelsInfo, storeState.guildChannelsInfo);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final GuildChannelsInfo getGuildChannelsInfo() {
            return this.guildChannelsInfo;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final GuildMember getMember() {
            return this.member;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            MeUser meUser = this.meUser;
            int hashCode2 = (hashCode + (meUser != null ? meUser.hashCode() : 0)) * 31;
            GuildMember guildMember = this.member;
            int hashCode3 = (hashCode2 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
            GuildChannelsInfo guildChannelsInfo = this.guildChannelsInfo;
            if (guildChannelsInfo != null) {
                i = guildChannelsInfo.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guild=");
            R.append(this.guild);
            R.append(", meUser=");
            R.append(this.meUser);
            R.append(", member=");
            R.append(this.member);
            R.append(", guildChannelsInfo=");
            R.append(this.guildChannelsInfo);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Loading", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loading;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u001c\b\u0086\b\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\b\u0012\u0006\u0010\u0018\u001a\u00020\u000b\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u000e\u0012\u0010\b\u0002\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0011\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u000b¢\u0006\u0004\b<\u0010=J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0018\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\rJ`\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00052\b\b\u0002\u0010\u0017\u001a\u00020\b2\b\b\u0002\u0010\u0018\u001a\u00020\u000b2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u000e2\u0010\b\u0002\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u00112\b\b\u0002\u0010\u001b\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0010J\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010$\u001a\u00020\u000b2\b\u0010#\u001a\u0004\u0018\u00010\"HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0018\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010&\u001a\u0004\b'\u0010\rR\u0019\u0010\u001b\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010&\u001a\u0004\b(\u0010\rR\u0019\u0010)\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010&\u001a\u0004\b)\u0010\rR\u001b\u0010*\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010\u0010R\u0019\u0010-\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010&\u001a\u0004\b.\u0010\rR\u0019\u0010/\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010&\u001a\u0004\b0\u0010\rR\u0019\u0010\u0016\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u00101\u001a\u0004\b2\u0010\u0007R\u0019\u0010\u0017\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u00103\u001a\u0004\b4\u0010\nR!\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00105\u001a\u0004\b6\u0010\u0013R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u00107\u001a\u0004\b8\u0010\u0004R\u0019\u00109\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010&\u001a\u0004\b:\u0010\rR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010+\u001a\u0004\b;\u0010\u0010¨\u0006>"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "", "component4", "()Z", "", "component5", "()Ljava/lang/String;", "Lcom/discord/nullserializable/NullSerializable;", "component6", "()Lcom/discord/nullserializable/NullSerializable;", "component7", "guild", "meUser", "member", "canChangeNickname", "currentNickname", "currentAvatar", "dimmed", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;ZLjava/lang/String;Lcom/discord/nullserializable/NullSerializable;Z)Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loaded;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanChangeNickname", "getDimmed", "isDirty", "displayedAvatarURL", "Ljava/lang/String;", "getDisplayedAvatarURL", "shouldUpsell", "getShouldUpsell", "showSaveFab", "getShowSaveFab", "Lcom/discord/models/user/MeUser;", "getMeUser", "Lcom/discord/models/member/GuildMember;", "getMember", "Lcom/discord/nullserializable/NullSerializable;", "getCurrentAvatar", "Lcom/discord/models/guild/Guild;", "getGuild", "displayingGuildAvatar", "getDisplayingGuildAvatar", "getCurrentNickname", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;ZLjava/lang/String;Lcom/discord/nullserializable/NullSerializable;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean canChangeNickname;
            private final NullSerializable<String> currentAvatar;
            private final String currentNickname;
            private final boolean dimmed;
            private final String displayedAvatarURL;
            private final boolean displayingGuildAvatar;
            private final Guild guild;
            private final boolean isDirty;
            private final MeUser meUser;
            private final GuildMember member;
            private final boolean shouldUpsell;
            private final boolean showSaveFab;

            public /* synthetic */ Loaded(Guild guild, MeUser meUser, GuildMember guildMember, boolean z2, String str, NullSerializable nullSerializable, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(guild, meUser, guildMember, z2, (i & 16) != 0 ? null : str, (i & 32) != 0 ? null : nullSerializable, (i & 64) != 0 ? false : z3);
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, Guild guild, MeUser meUser, GuildMember guildMember, boolean z2, String str, NullSerializable nullSerializable, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    guild = loaded.guild;
                }
                if ((i & 2) != 0) {
                    meUser = loaded.meUser;
                }
                MeUser meUser2 = meUser;
                if ((i & 4) != 0) {
                    guildMember = loaded.member;
                }
                GuildMember guildMember2 = guildMember;
                if ((i & 8) != 0) {
                    z2 = loaded.canChangeNickname;
                }
                boolean z4 = z2;
                if ((i & 16) != 0) {
                    str = loaded.currentNickname;
                }
                String str2 = str;
                NullSerializable<String> nullSerializable2 = nullSerializable;
                if ((i & 32) != 0) {
                    nullSerializable2 = loaded.currentAvatar;
                }
                NullSerializable nullSerializable3 = nullSerializable2;
                if ((i & 64) != 0) {
                    z3 = loaded.dimmed;
                }
                return loaded.copy(guild, meUser2, guildMember2, z4, str2, nullSerializable3, z3);
            }

            public final Guild component1() {
                return this.guild;
            }

            public final MeUser component2() {
                return this.meUser;
            }

            public final GuildMember component3() {
                return this.member;
            }

            public final boolean component4() {
                return this.canChangeNickname;
            }

            public final String component5() {
                return this.currentNickname;
            }

            public final NullSerializable<String> component6() {
                return this.currentAvatar;
            }

            public final boolean component7() {
                return this.dimmed;
            }

            public final Loaded copy(Guild guild, MeUser meUser, GuildMember guildMember, boolean z2, String str, NullSerializable<String> nullSerializable, boolean z3) {
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(meUser, "meUser");
                m.checkNotNullParameter(guildMember, "member");
                return new Loaded(guild, meUser, guildMember, z2, str, nullSerializable, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.guild, loaded.guild) && m.areEqual(this.meUser, loaded.meUser) && m.areEqual(this.member, loaded.member) && this.canChangeNickname == loaded.canChangeNickname && m.areEqual(this.currentNickname, loaded.currentNickname) && m.areEqual(this.currentAvatar, loaded.currentAvatar) && this.dimmed == loaded.dimmed;
            }

            public final boolean getCanChangeNickname() {
                return this.canChangeNickname;
            }

            public final NullSerializable<String> getCurrentAvatar() {
                return this.currentAvatar;
            }

            public final String getCurrentNickname() {
                return this.currentNickname;
            }

            public final boolean getDimmed() {
                return this.dimmed;
            }

            public final String getDisplayedAvatarURL() {
                return this.displayedAvatarURL;
            }

            public final boolean getDisplayingGuildAvatar() {
                return this.displayingGuildAvatar;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final MeUser getMeUser() {
                return this.meUser;
            }

            public final GuildMember getMember() {
                return this.member;
            }

            public final boolean getShouldUpsell() {
                return this.shouldUpsell;
            }

            public final boolean getShowSaveFab() {
                return this.showSaveFab;
            }

            public int hashCode() {
                Guild guild = this.guild;
                int i = 0;
                int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
                MeUser meUser = this.meUser;
                int hashCode2 = (hashCode + (meUser != null ? meUser.hashCode() : 0)) * 31;
                GuildMember guildMember = this.member;
                int hashCode3 = (hashCode2 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
                boolean z2 = this.canChangeNickname;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode3 + i3) * 31;
                String str = this.currentNickname;
                int hashCode4 = (i5 + (str != null ? str.hashCode() : 0)) * 31;
                NullSerializable<String> nullSerializable = this.currentAvatar;
                if (nullSerializable != null) {
                    i = nullSerializable.hashCode();
                }
                int i6 = (hashCode4 + i) * 31;
                boolean z3 = this.dimmed;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                return i6 + i2;
            }

            public final boolean isDirty() {
                return this.isDirty;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(guild=");
                R.append(this.guild);
                R.append(", meUser=");
                R.append(this.meUser);
                R.append(", member=");
                R.append(this.member);
                R.append(", canChangeNickname=");
                R.append(this.canChangeNickname);
                R.append(", currentNickname=");
                R.append(this.currentNickname);
                R.append(", currentAvatar=");
                R.append(this.currentAvatar);
                R.append(", dimmed=");
                return a.M(R, this.dimmed, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(Guild guild, MeUser meUser, GuildMember guildMember, boolean z2, String str, NullSerializable<String> nullSerializable, boolean z3) {
                super(null);
                String str2;
                boolean z4;
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(meUser, "meUser");
                m.checkNotNullParameter(guildMember, "member");
                this.guild = guild;
                this.meUser = meUser;
                this.member = guildMember;
                this.canChangeNickname = z2;
                this.currentNickname = str;
                this.currentAvatar = nullSerializable;
                this.dimmed = z3;
                String nick = guildMember.getNick();
                boolean z5 = (m.areEqual(nick == null ? "" : nick, str) ^ true) || nullSerializable != null;
                this.isDirty = z5;
                this.showSaveFab = z5;
                if (nullSerializable instanceof NullSerializable) {
                    if (nullSerializable instanceof NullSerializable.b) {
                        str2 = (String) ((NullSerializable.b) nullSerializable).a();
                    } else if (nullSerializable instanceof NullSerializable.a) {
                        str2 = IconUtils.getForUser$default(meUser, true, null, 4, null);
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                } else if (guildMember.hasAvatar()) {
                    str2 = IconUtils.getForGuildMember$default(IconUtils.INSTANCE, guildMember, null, true, 2, null);
                } else {
                    str2 = IconUtils.getForUser$default(meUser, true, null, 4, null);
                }
                this.displayedAvatarURL = str2;
                if (nullSerializable instanceof NullSerializable) {
                    z4 = nullSerializable instanceof NullSerializable.b;
                } else {
                    z4 = guildMember.hasAvatar();
                }
                this.displayingGuildAvatar = z4;
                this.shouldUpsell = !UserUtils.INSTANCE.isPremiumTier2(meUser);
            }
        }

        /* compiled from: WidgetChangeGuildIdentityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState$Loading;", "Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetChangeGuildIdentityViewModel(long j, String str, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, str, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        String str;
        NullSerializable<String> currentAvatar;
        Guild guild = storeState.getGuild();
        GuildMember member = storeState.getMember();
        MeUser meUser = storeState.getMeUser();
        GuildChannelsInfo guildChannelsInfo = storeState.getGuildChannelsInfo();
        if (guild != null && member != null) {
            ViewState viewState = getViewState();
            if (!(viewState instanceof ViewState.Loaded)) {
                viewState = null;
            }
            ViewState.Loaded loaded = (ViewState.Loaded) viewState;
            if (loaded == null || (str = loaded.getCurrentNickname()) == null) {
                str = member.getNick();
            }
            String str2 = str;
            ViewState viewState2 = getViewState();
            if (!(viewState2 instanceof ViewState.Loaded)) {
                viewState2 = null;
            }
            ViewState.Loaded loaded2 = (ViewState.Loaded) viewState2;
            updateViewState(new ViewState.Loaded(guild, meUser, member, guildChannelsInfo.getCanChangeNickname(), str2, (loaded2 == null || (currentAvatar = loaded2.getCurrentAvatar()) == null) ? null : currentAvatar, false, 64, null));
        }
    }

    @MainThread
    public final void clearStatus() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, false, "", null, false, 111, null));
        }
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final String getSourceSection() {
        return this.sourceSection;
    }

    public final boolean getTrackedModalOpen() {
        return this.trackedModalOpen;
    }

    public final boolean getTrackedUpsell() {
        return this.trackedUpsell;
    }

    public final Observable<Event> observeEvents() {
        return this.eventSubject;
    }

    @MainThread
    public final void saveMemberChanges(Context context) {
        m.checkNotNullParameter(context, "context");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, false, null, null, true, 63, null));
            RestAPI restAPI = this.restAPI;
            long id2 = loaded.getGuild().getId();
            String currentNickname = loaded.getCurrentNickname();
            String nick = loaded.getMember().getNick();
            if (nick == null) {
                nick = "";
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(restAPI.updateMeGuildMember(id2, new PatchGuildMemberBody(m.areEqual(currentNickname, nick) ^ true ? loaded.getCurrentNickname() : null, loaded.getCurrentAvatar(), null, null, 12)), false, 1, null), this, null, 2, null), WidgetChangeGuildIdentityViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetChangeGuildIdentityViewModel$saveMemberChanges$1(this, loaded), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChangeGuildIdentityViewModel$saveMemberChanges$2(this, loaded));
        }
    }

    public final void setTrackedModalOpen(boolean z2) {
        this.trackedModalOpen = z2;
    }

    public final void setTrackedUpsell(boolean z2) {
        this.trackedUpsell = z2;
    }

    @MainThread
    public final void updateAvatar(String str) {
        NullSerializable.b bVar;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            if (str != null) {
                bVar = new NullSerializable.b(str);
            } else {
                bVar = new NullSerializable.a(null, 1);
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, false, null, bVar, false, 95, null));
        }
    }

    @MainThread
    public final void updateNickname(String str) {
        m.checkNotNullParameter(str, "nickname");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (!m.areEqual(str, loaded.getCurrentNickname()))) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, false, str, null, false, 111, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChangeGuildIdentityViewModel(long j, String str, RestAPI restAPI) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(str, "sourceSection");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.guildId = j;
        this.sourceSection = str;
        this.restAPI = restAPI;
        PublishSubject<Event> k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.eventSubject = k0;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Companion.observeStoreState$default(Companion, j, null, null, 6, null)), this, null, 2, null), WidgetChangeGuildIdentityViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
