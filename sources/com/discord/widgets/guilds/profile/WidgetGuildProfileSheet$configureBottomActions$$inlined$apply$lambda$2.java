package com.discord.widgets.guilds.profile;

import android.view.View;
import androidx.fragment.app.ViewKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildProfileSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "com/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$2$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$2 extends o implements Function1<View, Unit> {
    public final /* synthetic */ long $guildId$inlined;
    public final /* synthetic */ boolean $showJoinServer$inlined;
    public final /* synthetic */ MaterialButton $this_apply;
    public final /* synthetic */ WidgetGuildProfileSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$2(MaterialButton materialButton, WidgetGuildProfileSheet widgetGuildProfileSheet, boolean z2, long j) {
        super(1);
        this.$this_apply = materialButton;
        this.this$0 = widgetGuildProfileSheet;
        this.$showJoinServer$inlined = z2;
        this.$guildId$inlined = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        WidgetGuildProfileSheetViewModel viewModel;
        m.checkNotNullParameter(view, "it");
        viewModel = this.this$0.getViewModel();
        long j = this.$guildId$inlined;
        MaterialButton materialButton = this.$this_apply;
        m.checkNotNullExpressionValue(materialButton, "this");
        viewModel.onClickJoinServer(j, ViewKt.findFragment(materialButton));
    }
}
