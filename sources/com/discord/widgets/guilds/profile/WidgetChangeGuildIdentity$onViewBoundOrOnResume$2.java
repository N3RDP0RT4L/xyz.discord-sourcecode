package com.discord.widgets.guilds.profile;

import androidx.fragment.app.FragmentActivity;
import com.discord.widgets.guilds.profile.WidgetChangeGuildIdentityViewModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChangeGuildIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event;", "event", "", "invoke", "(Lcom/discord/widgets/guilds/profile/WidgetChangeGuildIdentityViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChangeGuildIdentity$onViewBoundOrOnResume$2 extends o implements Function1<WidgetChangeGuildIdentityViewModel.Event, Unit> {
    public final /* synthetic */ WidgetChangeGuildIdentity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChangeGuildIdentity$onViewBoundOrOnResume$2(WidgetChangeGuildIdentity widgetChangeGuildIdentity) {
        super(1);
        this.this$0 = widgetChangeGuildIdentity;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetChangeGuildIdentityViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetChangeGuildIdentityViewModel.Event event) {
        AtomicBoolean atomicBoolean;
        m.checkNotNullParameter(event, "event");
        boolean z2 = true;
        if (event instanceof WidgetChangeGuildIdentityViewModel.Event.MemberUpdateFailed) {
            WidgetChangeGuildIdentityViewModel.Event.MemberUpdateFailed memberUpdateFailed = (WidgetChangeGuildIdentityViewModel.Event.MemberUpdateFailed) event;
            String errorMessage = memberUpdateFailed.getErrorMessage();
            if (!(errorMessage == null || errorMessage.length() == 0)) {
                z2 = false;
            }
            if (z2) {
                this.this$0.showToast((int) R.string.change_identity_modal_unknown_error);
            } else {
                this.this$0.showToast(memberUpdateFailed.getErrorMessage());
            }
        } else if (event instanceof WidgetChangeGuildIdentityViewModel.Event.MemberUpdateSucceeded) {
            this.this$0.showToast((int) R.string.per_guild_identity_saved);
            atomicBoolean = this.this$0.discardConfirmed;
            atomicBoolean.set(true);
            FragmentActivity activity = this.this$0.e();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }
}
