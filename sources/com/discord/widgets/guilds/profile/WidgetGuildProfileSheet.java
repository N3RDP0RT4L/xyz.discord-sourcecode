package com.discord.widgets.guilds.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetGuildHubProfileActionsBinding;
import com.discord.databinding.WidgetGuildProfileActionsBinding;
import com.discord.databinding.WidgetGuildProfileSheetBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShare;
import com.discord.widgets.guilds.leave.WidgetLeaveGuildDialog;
import com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetGuildProfileSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000À\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 o2\u00020\u0001:\u0001oB\u0007¢\u0006\u0004\bn\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0013\u0010\u0012J-\u0010\u001a\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0019\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001cH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ+\u0010$\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u00172\b\u0010!\u001a\u0004\u0018\u00010\u00172\b\u0010#\u001a\u0004\u0018\u00010\"H\u0002¢\u0006\u0004\b$\u0010%J#\u0010(\u001a\u00020\u00042\b\u0010&\u001a\u0004\u0018\u00010\"2\b\u0010'\u001a\u0004\u0018\u00010\"H\u0002¢\u0006\u0004\b(\u0010)J-\u0010.\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\b\u0010+\u001a\u0004\u0018\u00010*2\u0006\u0010-\u001a\u00020,H\u0002¢\u0006\u0004\b.\u0010/J1\u00104\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\n\u00101\u001a\u00060\u0014j\u0002`02\b\u00103\u001a\u0004\u0018\u000102H\u0002¢\u0006\u0004\b4\u00105J1\u00106\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\n\u00101\u001a\u00060\u0014j\u0002`02\b\u00103\u001a\u0004\u0018\u000102H\u0002¢\u0006\u0004\b6\u00105J\u0017\u00109\u001a\u00020\u00042\u0006\u00108\u001a\u000207H\u0002¢\u0006\u0004\b9\u0010:J3\u0010?\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\u000e\u0010<\u001a\n\u0018\u00010\u0014j\u0004\u0018\u0001`;2\u0006\u0010>\u001a\u00020=H\u0002¢\u0006\u0004\b?\u0010@J\u001b\u0010A\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u0015H\u0002¢\u0006\u0004\bA\u0010BJ\u0011\u0010C\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\bC\u0010DJ\u000f\u0010E\u001a\u00020\u0004H\u0002¢\u0006\u0004\bE\u0010\u000fJ\u000f\u0010F\u001a\u00020\u0004H\u0002¢\u0006\u0004\bF\u0010\u000fJ\u000f\u0010G\u001a\u00020\"H\u0002¢\u0006\u0004\bG\u0010HJ\u001b\u0010I\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u0015H\u0002¢\u0006\u0004\bI\u0010BJ\u000f\u0010J\u001a\u00020\"H\u0016¢\u0006\u0004\bJ\u0010HJ!\u0010O\u001a\u00020\u00042\u0006\u0010L\u001a\u00020K2\b\u0010N\u001a\u0004\u0018\u00010MH\u0016¢\u0006\u0004\bO\u0010PR\u0016\u0010R\u001a\u00020Q8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bR\u0010SR\u001d\u0010Y\u001a\u00020T8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bU\u0010V\u001a\u0004\bW\u0010XR\u0016\u0010[\u001a\u00020Z8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b[\u0010\\R\u001d\u0010b\u001a\u00020]8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b^\u0010_\u001a\u0004\b`\u0010aR\u0016\u0010d\u001a\u00020c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010eR\u0018\u0010g\u001a\u0004\u0018\u00010f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bg\u0010hR\u001e\u0010<\u001a\n\u0018\u00010\u0014j\u0004\u0018\u0001`;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u0010iR\u001d\u0010m\u001a\u00020,8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bj\u0010V\u001a\u0004\bk\u0010l¨\u0006p"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;", "event", "", "handleEvent", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;)V", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;", "handleDismissAndShowToast", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;)V", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;", "viewState", "handleViewState", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;)V", "showLoadingView", "()V", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;", "updateView", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;)V", "configureUI", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "iconHash", "shortName", "configureGuildIcon", "(JLjava/lang/String;Ljava/lang/String;)V", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;", "guildBanner", "configureGuildBanner", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;)V", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "", "verifiedPartneredIconRes", "configureGuildContent", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V", "approximatePresenceCount", "approximateMemberCount", "configureMemberCount", "(Ljava/lang/Integer;Ljava/lang/Integer;)V", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;", "tabItems", "", "isGuildHub", "configureTabItems", "(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Z)V", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;", "actions", "configureGuildHubActions", "(JJLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;)V", "configureGuildActions", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;", "emojisData", "configureEmojis", "(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;", "bottomActions", "configureBottomActions", "(JLjava/lang/Long;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)V", "showLeaveServerDialog", "(J)V", "dismissAlert", "()Lkotlin/Unit;", "constrainIconToBanner", "constrainIconToParent", "maxEmojisPerRow", "()I", "launchInvite", "getContentViewResId", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Lcom/discord/databinding/WidgetGuildHubProfileActionsBinding;", "guildHubActionBinding", "Lcom/discord/databinding/WidgetGuildHubProfileActionsBinding;", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;", "viewModel", "Lcom/discord/databinding/WidgetGuildProfileActionsBinding;", "guildActionBinding", "Lcom/discord/databinding/WidgetGuildProfileActionsBinding;", "Lcom/discord/databinding/WidgetGuildProfileSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildProfileSheetBinding;", "binding", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;", "emojisAdapter", "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;", "Landroidx/appcompat/app/AlertDialog;", "dialog", "Landroidx/appcompat/app/AlertDialog;", "Ljava/lang/Long;", "fromGuildEventUpsell$delegate", "getFromGuildEventUpsell", "()Z", "fromGuildEventUpsell", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildProfileSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildProfileSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildProfileSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_FROM_UPSELL = "EXTRA_FROM_UPSELL";
    private static final int LOADED_VIEW_INDEX = 1;
    private static final int LOADING_VIEW_INDEX = 0;
    private static final int NUM_ROWS_EMOJIS = 2;
    private Long channelId;
    private AlertDialog dialog;
    private WidgetGuildProfileActionsBinding guildActionBinding;
    private WidgetGuildHubProfileActionsBinding guildHubActionBinding;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildProfileSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy fromGuildEventUpsell$delegate = g.lazy(new WidgetGuildProfileSheet$fromGuildEventUpsell$2(this));
    private final WidgetGuildProfileSheetEmojisAdapter emojisAdapter = new WidgetGuildProfileSheetEmojisAdapter();

    /* compiled from: WidgetGuildProfileSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018JC\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u0010\n\u001a\u00060\u0006j\u0002`\t2\b\b\u0002\u0010\u000b\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u00128\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00128\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0014¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "viewingGuild", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "fromGuildEventUpsell", "", "show", "(Landroidx/fragment/app/FragmentManager;ZJJZ)V", "", WidgetGuildProfileSheet.EXTRA_FROM_UPSELL, "Ljava/lang/String;", "", "LOADED_VIEW_INDEX", "I", "LOADING_VIEW_INDEX", "NUM_ROWS_EMOJIS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, boolean z2, long j, long j2, boolean z3, int i, Object obj) {
            companion.show(fragmentManager, z2, j, (i & 8) != 0 ? 0L : j2, (i & 16) != 0 ? false : z3);
        }

        public final void show(FragmentManager fragmentManager, boolean z2, long j, long j2, boolean z3) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetGuildProfileSheet widgetGuildProfileSheet = new WidgetGuildProfileSheet();
            Bundle I = a.I("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            I.putLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", j2);
            I.putBoolean("com.discord.intent.extra.EXTRA_VIEWING_GUILD", z2);
            I.putBoolean(WidgetGuildProfileSheet.EXTRA_FROM_UPSELL, z3);
            widgetGuildProfileSheet.setArguments(I);
            widgetGuildProfileSheet.show(fragmentManager, WidgetGuildProfileSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            WidgetGuildProfileSheetViewModel.Banner.Type.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[WidgetGuildProfileSheetViewModel.Banner.Type.BANNER.ordinal()] = 1;
            iArr[WidgetGuildProfileSheetViewModel.Banner.Type.SPLASH.ordinal()] = 2;
        }
    }

    public WidgetGuildProfileSheet() {
        super(false, 1, null);
        WidgetGuildProfileSheet$viewModel$2 widgetGuildProfileSheet$viewModel$2 = new WidgetGuildProfileSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGuildProfileSheetViewModel.class), new WidgetGuildProfileSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildProfileSheet$viewModel$2));
    }

    public static final /* synthetic */ WidgetGuildProfileActionsBinding access$getGuildActionBinding$p(WidgetGuildProfileSheet widgetGuildProfileSheet) {
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding = widgetGuildProfileSheet.guildActionBinding;
        if (widgetGuildProfileActionsBinding == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        return widgetGuildProfileActionsBinding;
    }

    public static final /* synthetic */ WidgetGuildHubProfileActionsBinding access$getGuildHubActionBinding$p(WidgetGuildProfileSheet widgetGuildProfileSheet) {
        WidgetGuildHubProfileActionsBinding widgetGuildHubProfileActionsBinding = widgetGuildProfileSheet.guildHubActionBinding;
        if (widgetGuildHubProfileActionsBinding == null) {
            m.throwUninitializedPropertyAccessException("guildHubActionBinding");
        }
        return widgetGuildHubProfileActionsBinding;
    }

    private final void configureBottomActions(long j, Long l, WidgetGuildProfileSheetViewModel.BottomActions bottomActions) {
        boolean showUploadEmoji = bottomActions.getShowUploadEmoji();
        boolean showJoinServer = bottomActions.getShowJoinServer();
        boolean showViewServer = bottomActions.getShowViewServer();
        int i = 8;
        if (showUploadEmoji || showJoinServer || showViewServer) {
            WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding = this.guildActionBinding;
            if (widgetGuildProfileActionsBinding == null) {
                m.throwUninitializedPropertyAccessException("guildActionBinding");
            }
            LinearLayout linearLayout = widgetGuildProfileActionsBinding.e;
            m.checkNotNullExpressionValue(linearLayout, "guildActionBinding.guildProfileSheetBottomActions");
            linearLayout.setVisibility(0);
            WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding2 = this.guildActionBinding;
            if (widgetGuildProfileActionsBinding2 == null) {
                m.throwUninitializedPropertyAccessException("guildActionBinding");
            }
            View view = widgetGuildProfileActionsBinding2.f2407z;
            view.setVisibility(showUploadEmoji ? 0 : 8);
            setOnClickAndDismissListener(view, new WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$1(this, showUploadEmoji, j));
            WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding3 = this.guildActionBinding;
            if (widgetGuildProfileActionsBinding3 == null) {
                m.throwUninitializedPropertyAccessException("guildActionBinding");
            }
            MaterialButton materialButton = widgetGuildProfileActionsBinding3.r;
            materialButton.setVisibility(showJoinServer ? 0 : 8);
            setOnClickAndDismissListener(materialButton, new WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$2(materialButton, this, showJoinServer, j));
            WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding4 = this.guildActionBinding;
            if (widgetGuildProfileActionsBinding4 == null) {
                m.throwUninitializedPropertyAccessException("guildActionBinding");
            }
            View view2 = widgetGuildProfileActionsBinding4.A;
            if (showViewServer) {
                i = 0;
            }
            view2.setVisibility(i);
            setOnClickAndDismissListener(view2, new WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$3(this, showViewServer, j, l));
            m.checkNotNullExpressionValue(view2, "guildActionBinding.guild…dId, channelId) }\n      }");
            return;
        }
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding5 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding5 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        LinearLayout linearLayout2 = widgetGuildProfileActionsBinding5.e;
        m.checkNotNullExpressionValue(linearLayout2, "guildActionBinding.guildProfileSheetBottomActions");
        linearLayout2.setVisibility(8);
    }

    private final void configureEmojis(WidgetGuildProfileSheetViewModel.EmojisData emojisData) {
        boolean isPremium = emojisData.isPremium();
        boolean isExpanded = emojisData.isExpanded();
        List<Emoji> emojis = emojisData.getEmojis();
        int size = emojis.size();
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        TextView textView = widgetGuildProfileActionsBinding.o;
        m.checkNotNullExpressionValue(textView, "guildActionBinding.guildProfileSheetEmojisCount");
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        textView.setText(StringResourceUtilsKt.getQuantityString(resources, (int) R.plurals.emojis_title_count, (int) R.string.no_emoji_title, size, Integer.valueOf(size)));
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding2 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding2 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        ImageView imageView = widgetGuildProfileActionsBinding2.w;
        m.checkNotNullExpressionValue(imageView, "guildActionBinding.guild…PremiumUpsellDotSeparator");
        imageView.setVisibility(isPremium ^ true ? 0 : 8);
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding3 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding3 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        TextView textView2 = widgetGuildProfileActionsBinding3.f2405x;
        m.checkNotNullExpressionValue(textView2, "guildActionBinding.guild…ileSheetPremiumUpsellText");
        textView2.setVisibility(isPremium ^ true ? 0 : 8);
        if (size != 0) {
            WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding4 = this.guildActionBinding;
            if (widgetGuildProfileActionsBinding4 == null) {
                m.throwUninitializedPropertyAccessException("guildActionBinding");
            }
            CardView cardView = widgetGuildProfileActionsBinding4.n;
            m.checkNotNullExpressionValue(cardView, "guildActionBinding.guildProfileSheetEmojisCard");
            cardView.setVisibility(0);
            int maxEmojisPerRow = maxEmojisPerRow();
            WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding5 = this.guildActionBinding;
            if (widgetGuildProfileActionsBinding5 == null) {
                m.throwUninitializedPropertyAccessException("guildActionBinding");
            }
            RecyclerView recyclerView = widgetGuildProfileActionsBinding5.m;
            m.checkNotNullExpressionValue(recyclerView, "guildActionBinding.guildProfileSheetEmojis");
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            Objects.requireNonNull(layoutManager, "null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager");
            ((GridLayoutManager) layoutManager).setSpanCount(maxEmojisPerRow);
            this.emojisAdapter.setData(emojis, isExpanded ? emojis.size() : maxEmojisPerRow * 2);
            return;
        }
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding6 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding6 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CardView cardView2 = widgetGuildProfileActionsBinding6.n;
        m.checkNotNullExpressionValue(cardView2, "guildActionBinding.guildProfileSheetEmojisCard");
        cardView2.setVisibility(8);
    }

    private final void configureGuildActions(final long j, long j2, final WidgetGuildProfileSheetViewModel.Actions actions) {
        if (actions == null) {
            WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding = this.guildActionBinding;
            if (widgetGuildProfileActionsBinding == null) {
                m.throwUninitializedPropertyAccessException("guildActionBinding");
            }
            LinearLayout linearLayout = widgetGuildProfileActionsBinding.c;
            m.checkNotNullExpressionValue(linearLayout, "guildActionBinding.guildProfileSheetActions");
            linearLayout.setVisibility(8);
            return;
        }
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding2 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding2 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        LinearLayout linearLayout2 = widgetGuildProfileActionsBinding2.c;
        m.checkNotNullExpressionValue(linearLayout2, "guildActionBinding.guildProfileSheetActions");
        linearLayout2.setVisibility(0);
        boolean isUnread = actions.isUnread();
        boolean canManageChannels = actions.getCanManageChannels();
        boolean canManageEvents = actions.getCanManageEvents();
        String nick = actions.getNick();
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding3 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding3 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CardView cardView = widgetGuildProfileActionsBinding3.u;
        m.checkNotNullExpressionValue(cardView, "guildActionBinding.guild…fileSheetMarkAsReadAction");
        cardView.setVisibility(isUnread ? 0 : 8);
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding4 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding4 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        widgetGuildProfileActionsBinding4.t.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheet$configureGuildActions$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildProfileSheetViewModel viewModel;
                viewModel = WidgetGuildProfileSheet.this.getViewModel();
                viewModel.onClickMarkAsRead(WidgetGuildProfileSheet.this.getContext(), j);
            }
        });
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding5 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding5 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CardView cardView2 = widgetGuildProfileActionsBinding5.f2406y;
        m.checkNotNullExpressionValue(cardView2, "guildActionBinding.guildProfileSheetPrimaryActions");
        cardView2.setVisibility(canManageChannels || (canManageEvents && StageEventsGuildsFeatureFlag.Companion.getINSTANCE().canGuildAccessStageEvents(j)) ? 0 : 8);
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding6 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding6 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        TextView textView = widgetGuildProfileActionsBinding6.h;
        textView.setVisibility(canManageChannels ? 0 : 8);
        setOnClickAndDismissListener(textView, new WidgetGuildProfileSheet$configureGuildActions$$inlined$apply$lambda$1(textView, this, canManageChannels, j));
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding7 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding7 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        TextView textView2 = widgetGuildProfileActionsBinding7.i;
        textView2.setVisibility(canManageChannels ? 0 : 8);
        setOnClickAndDismissListener(textView2, new WidgetGuildProfileSheet$configureGuildActions$$inlined$apply$lambda$2(textView2, this, canManageChannels, j));
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding8 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding8 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        ConstraintLayout constraintLayout = widgetGuildProfileActionsBinding8.j;
        constraintLayout.setVisibility(canManageEvents && StageEventsGuildsFeatureFlag.Companion.getINSTANCE().canGuildAccessStageEvents(j) ? 0 : 8);
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding9 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding9 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        TextView textView3 = widgetGuildProfileActionsBinding9.k;
        m.checkNotNullExpressionValue(textView3, "guildActionBinding.guildProfileSheetCreateEventNew");
        textView3.setVisibility(getFromGuildEventUpsell() ? 0 : 8);
        setOnClickAndDismissListener(constraintLayout, new WidgetGuildProfileSheet$configureGuildActions$$inlined$apply$lambda$3(constraintLayout, this, canManageEvents, j));
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding10 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding10 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        setOnClickAndDismissListener(widgetGuildProfileActionsBinding10.f2403b, new WidgetGuildProfileSheet$configureGuildActions$$inlined$apply$lambda$4(this, j));
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding11 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding11 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        TextView textView4 = widgetGuildProfileActionsBinding11.v;
        textView4.setVisibility(actions.getDisplayGuildIdentityRow() ? 0 : 8);
        if (nick == null) {
            nick = actions.getUsername();
        }
        textView4.setText(nick);
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding12 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding12 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        SimpleDraweeView simpleDraweeView = widgetGuildProfileActionsBinding12.p;
        String guildAvatar = actions.getGuildAvatar();
        simpleDraweeView.setVisibility((guildAvatar == null || guildAvatar.length() == 0) ^ true ? 0 : 8);
        simpleDraweeView.setImageURI(IconUtils.INSTANCE.getForGuildMember(actions.getGuildAvatar(), j, j2, Integer.valueOf(DimenUtils.dpToPixels(16)), false));
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding13 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding13 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        LinearLayout linearLayout3 = widgetGuildProfileActionsBinding13.B;
        m.checkNotNullExpressionValue(linearLayout3, "guildActionBinding.perGuildIdentityContainer");
        linearLayout3.setVisibility(actions.getDisplayGuildIdentityRow() ? 0 : 8);
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding14 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding14 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CheckedSetting checkedSetting = widgetGuildProfileActionsBinding14.d;
        checkedSetting.setChecked(actions.isAllowDMChecked());
        checkedSetting.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheet$configureGuildActions$$inlined$apply$lambda$5
            public final void call(Boolean bool) {
                WidgetGuildProfileSheetViewModel viewModel;
                viewModel = WidgetGuildProfileSheet.this.getViewModel();
                AppActivity appActivity = WidgetGuildProfileSheet.this.getAppActivity();
                long j3 = j;
                m.checkNotNullExpressionValue(bool, "checked");
                viewModel.setAllowDM(appActivity, j3, bool.booleanValue());
            }
        });
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding15 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding15 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CheckedSetting checkedSetting2 = widgetGuildProfileActionsBinding15.q;
        checkedSetting2.setChecked(actions.getHideMutedChannels());
        checkedSetting2.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheet$configureGuildActions$$inlined$apply$lambda$6
            public final void call(Boolean bool) {
                WidgetGuildProfileSheetViewModel viewModel;
                viewModel = WidgetGuildProfileSheet.this.getViewModel();
                long j3 = j;
                m.checkNotNullExpressionValue(bool, "checked");
                viewModel.setHideMutedChannels(j3, bool.booleanValue());
            }
        });
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding16 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding16 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        View view = widgetGuildProfileActionsBinding16.f2404s;
        view.setVisibility(actions.getCanLeaveGuild() ? 0 : 8);
        setOnClickAndDismissListener(view, new WidgetGuildProfileSheet$configureGuildActions$$inlined$apply$lambda$7(this, actions, j));
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding17 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding17 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CardView cardView3 = widgetGuildProfileActionsBinding17.l;
        m.checkNotNullExpressionValue(cardView3, "guildActionBinding.guild…fileSheetDeveloperActions");
        cardView3.setVisibility(actions.isDeveloper() ? 0 : 8);
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding18 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding18 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        widgetGuildProfileActionsBinding18.g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheet$configureGuildActions$11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Context requireContext = WidgetGuildProfileSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                b.a.d.m.c(requireContext, String.valueOf(j), 0, 4);
            }
        });
    }

    private final void configureGuildBanner(WidgetGuildProfileSheetViewModel.Banner banner) {
        String str;
        if (banner.getHash() != null) {
            int ordinal = banner.getType().ordinal();
            if (ordinal == 0) {
                str = IconUtils.getBannerForGuild$default(IconUtils.INSTANCE, Long.valueOf(banner.getGuildId()), banner.getHash(), Integer.valueOf(getResources().getDimensionPixelSize(R.dimen.nav_panel_width)), false, 8, null);
            } else if (ordinal == 1) {
                str = IconUtils.INSTANCE.getGuildSplashUrl(banner.getGuildId(), banner.getHash(), Integer.valueOf(getResources().getDimensionPixelSize(R.dimen.nav_panel_width)));
            } else {
                throw new NoWhenBranchMatchedException();
            }
            SimpleDraweeView simpleDraweeView = getBinding().f2409b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildProfileSheetBanner");
            MGImages.setImage$default(simpleDraweeView, str, 0, 0, false, null, null, 124, null);
            SimpleDraweeView simpleDraweeView2 = getBinding().f2409b;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildProfileSheetBanner");
            simpleDraweeView2.setVisibility(0);
            constrainIconToBanner();
            return;
        }
        SimpleDraweeView simpleDraweeView3 = getBinding().f2409b;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.guildProfileSheetBanner");
        simpleDraweeView3.setVisibility(8);
        constrainIconToParent();
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x004a  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0064  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureGuildContent(java.lang.String r5, java.lang.String r6, java.lang.Integer r7) {
        /*
            r4 = this;
            com.discord.databinding.WidgetGuildProfileSheetBinding r0 = r4.getBinding()
            android.widget.TextView r0 = r0.o
            java.lang.String r1 = "binding.guildProfileSheetName"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r0.setText(r5)
            r5 = 8
            r0 = 0
            java.lang.String r1 = "binding.guildProfileSheetDescription"
            if (r6 == 0) goto L3a
            int r2 = r6.length()
            r3 = 1
            if (r2 <= 0) goto L1e
            r2 = 1
            goto L1f
        L1e:
            r2 = 0
        L1f:
            if (r2 != r3) goto L3a
            com.discord.databinding.WidgetGuildProfileSheetBinding r2 = r4.getBinding()
            android.widget.TextView r2 = r2.f
            d0.z.d.m.checkNotNullExpressionValue(r2, r1)
            r2.setText(r6)
            com.discord.databinding.WidgetGuildProfileSheetBinding r6 = r4.getBinding()
            android.widget.TextView r6 = r6.f
            d0.z.d.m.checkNotNullExpressionValue(r6, r1)
            r6.setVisibility(r0)
            goto L46
        L3a:
            com.discord.databinding.WidgetGuildProfileSheetBinding r6 = r4.getBinding()
            android.widget.TextView r6 = r6.f
            d0.z.d.m.checkNotNullExpressionValue(r6, r1)
            r6.setVisibility(r5)
        L46:
            java.lang.String r6 = "binding.guildProfileSheetGuildVerifiedPremiumIcon"
            if (r7 == 0) goto L64
            com.discord.databinding.WidgetGuildProfileSheetBinding r5 = r4.getBinding()
            android.widget.ImageView r5 = r5.h
            int r7 = r7.intValue()
            r5.setImageResource(r7)
            com.discord.databinding.WidgetGuildProfileSheetBinding r5 = r4.getBinding()
            android.widget.ImageView r5 = r5.h
            d0.z.d.m.checkNotNullExpressionValue(r5, r6)
            r5.setVisibility(r0)
            goto L70
        L64:
            com.discord.databinding.WidgetGuildProfileSheetBinding r7 = r4.getBinding()
            android.widget.ImageView r7 = r7.h
            d0.z.d.m.checkNotNullExpressionValue(r7, r6)
            r7.setVisibility(r5)
        L70:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.profile.WidgetGuildProfileSheet.configureGuildContent(java.lang.String, java.lang.String, java.lang.Integer):void");
    }

    private final void configureGuildHubActions(final long j, long j2, WidgetGuildProfileSheetViewModel.Actions actions) {
        if (actions != null) {
            WidgetGuildHubProfileActionsBinding widgetGuildHubProfileActionsBinding = this.guildHubActionBinding;
            if (widgetGuildHubProfileActionsBinding == null) {
                m.throwUninitializedPropertyAccessException("guildHubActionBinding");
            }
            widgetGuildHubProfileActionsBinding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.profile.WidgetGuildProfileSheet$configureGuildHubActions$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetGuildProfileSheet.this.launchInvite(j);
                }
            });
            WidgetGuildHubProfileActionsBinding widgetGuildHubProfileActionsBinding2 = this.guildHubActionBinding;
            if (widgetGuildHubProfileActionsBinding2 == null) {
                m.throwUninitializedPropertyAccessException("guildHubActionBinding");
            }
            View view = widgetGuildHubProfileActionsBinding2.f2392b;
            m.checkNotNullExpressionValue(view, "guildHubActionBinding.gu…rofileSheetChangeNickname");
            setOnClickAndDismissListener(view, new WidgetGuildProfileSheet$configureGuildHubActions$2(j));
            WidgetGuildHubProfileActionsBinding widgetGuildHubProfileActionsBinding3 = this.guildHubActionBinding;
            if (widgetGuildHubProfileActionsBinding3 == null) {
                m.throwUninitializedPropertyAccessException("guildHubActionBinding");
            }
            TextView textView = widgetGuildHubProfileActionsBinding3.f;
            textView.setVisibility(actions.getDisplayGuildIdentityRow() ? 0 : 8);
            String nick = actions.getNick();
            if (nick == null) {
                nick = actions.getUsername();
            }
            textView.setText(nick);
            WidgetGuildHubProfileActionsBinding widgetGuildHubProfileActionsBinding4 = this.guildHubActionBinding;
            if (widgetGuildHubProfileActionsBinding4 == null) {
                m.throwUninitializedPropertyAccessException("guildHubActionBinding");
            }
            SimpleDraweeView simpleDraweeView = widgetGuildHubProfileActionsBinding4.c;
            String guildAvatar = actions.getGuildAvatar();
            simpleDraweeView.setVisibility((guildAvatar == null || guildAvatar.length() == 0) ^ true ? 0 : 8);
            simpleDraweeView.setImageURI(IconUtils.INSTANCE.getForGuildMember(actions.getGuildAvatar(), j, j2, Integer.valueOf(DimenUtils.dpToPixels(16)), false));
            WidgetGuildHubProfileActionsBinding widgetGuildHubProfileActionsBinding5 = this.guildHubActionBinding;
            if (widgetGuildHubProfileActionsBinding5 == null) {
                m.throwUninitializedPropertyAccessException("guildHubActionBinding");
            }
            View view2 = widgetGuildHubProfileActionsBinding5.e;
            view2.setVisibility(actions.getCanLeaveGuild() ? 0 : 8);
            setOnClickAndDismissListener(view2, new WidgetGuildProfileSheet$configureGuildHubActions$$inlined$apply$lambda$1(this, actions, j));
            WidgetGuildHubProfileActionsBinding widgetGuildHubProfileActionsBinding6 = this.guildHubActionBinding;
            if (widgetGuildHubProfileActionsBinding6 == null) {
                m.throwUninitializedPropertyAccessException("guildHubActionBinding");
            }
            TextView textView2 = widgetGuildHubProfileActionsBinding6.g;
            textView2.setVisibility(GrowthTeamFeatures.hubUnreadsEnabled$default(GrowthTeamFeatures.INSTANCE, false, 1, null) ? 0 : 8);
            setOnClickAndDismissListener(textView2, new WidgetGuildProfileSheet$configureGuildHubActions$$inlined$apply$lambda$2(textView2, this, j));
        }
    }

    private final void configureGuildIcon(long j, String str, String str2) {
        String forGuild$default = IconUtils.getForGuild$default(Long.valueOf(j), str, null, true, Integer.valueOf(IconUtils.getMediaProxySize(getResources().getDimensionPixelSize(R.dimen.avatar_size_xxlarge))), 4, null);
        if (forGuild$default != null) {
            TextView textView = getBinding().k;
            m.checkNotNullExpressionValue(textView, "binding.guildProfileSheetIconName");
            textView.setVisibility(8);
            SimpleDraweeView simpleDraweeView = getBinding().i;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildProfileSheetIcon");
            IconUtils.setIcon$default(simpleDraweeView, forGuild$default, 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
            return;
        }
        SimpleDraweeView simpleDraweeView2 = getBinding().i;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildProfileSheetIcon");
        IconUtils.setIcon$default(simpleDraweeView2, IconUtils.DEFAULT_ICON_BLURPLE, 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
        TextView textView2 = getBinding().k;
        m.checkNotNullExpressionValue(textView2, "binding.guildProfileSheetIconName");
        textView2.setVisibility(0);
        TextView textView3 = getBinding().k;
        m.checkNotNullExpressionValue(textView3, "binding.guildProfileSheetIconName");
        textView3.setText(str2);
    }

    private final void configureMemberCount(Integer num, Integer num2) {
        if (num != null) {
            LinearLayout linearLayout = getBinding().q;
            m.checkNotNullExpressionValue(linearLayout, "binding.guildProfileSheetOnlineCount");
            linearLayout.setVisibility(0);
            TextView textView = getBinding().r;
            m.checkNotNullExpressionValue(textView, "binding.guildProfileSheetOnlineCountText");
            int intValue = num.intValue();
            LinearLayout linearLayout2 = getBinding().q;
            m.checkNotNullExpressionValue(linearLayout2, "binding.guildProfileSheetOnlineCount");
            Context context = linearLayout2.getContext();
            m.checkNotNullExpressionValue(context, "binding.guildProfileSheetOnlineCount.context");
            b.m(textView, R.string.instant_invite_guild_members_online, new Object[]{StringUtilsKt.format(intValue, context)}, (r4 & 4) != 0 ? b.g.j : null);
        } else {
            LinearLayout linearLayout3 = getBinding().q;
            m.checkNotNullExpressionValue(linearLayout3, "binding.guildProfileSheetOnlineCount");
            linearLayout3.setVisibility(8);
        }
        if (num2 != null) {
            LinearLayout linearLayout4 = getBinding().m;
            m.checkNotNullExpressionValue(linearLayout4, "binding.guildProfileSheetMemberCount");
            linearLayout4.setVisibility(0);
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources, requireContext, (int) R.plurals.instant_invite_guild_members_total_count, num2.intValue(), num2);
            TextView textView2 = getBinding().n;
            m.checkNotNullExpressionValue(textView2, "binding.guildProfileSheetMemberCountText");
            b.m(textView2, R.string.instant_invite_guild_members_total, new Object[]{quantityString}, (r4 & 4) != 0 ? b.g.j : null);
            return;
        }
        LinearLayout linearLayout5 = getBinding().m;
        m.checkNotNullExpressionValue(linearLayout5, "binding.guildProfileSheetMemberCount");
        linearLayout5.setVisibility(8);
    }

    private final void configureTabItems(long j, WidgetGuildProfileSheetViewModel.TabItems tabItems, boolean z2) {
        int i = 8;
        if (tabItems == null || z2) {
            LinearLayout linearLayout = getBinding().t;
            m.checkNotNullExpressionValue(linearLayout, "binding.guildProfileSheetTabItems");
            linearLayout.setVisibility(8);
            View view = getBinding().e;
            m.checkNotNullExpressionValue(view, "binding.guildProfileShee…entContainerBottomDivider");
            view.setVisibility(8);
            return;
        }
        boolean canAccessSettings = tabItems.getCanAccessSettings();
        boolean ableToInstantInvite = tabItems.getAbleToInstantInvite();
        int premiumSubscriptionCount = tabItems.getPremiumSubscriptionCount();
        LinearLayout linearLayout2 = getBinding().t;
        m.checkNotNullExpressionValue(linearLayout2, "binding.guildProfileSheetTabItems");
        linearLayout2.setVisibility(0);
        View view2 = getBinding().e;
        m.checkNotNullExpressionValue(view2, "binding.guildProfileShee…entContainerBottomDivider");
        view2.setVisibility(0);
        int themedColor = ColorCompat.getThemedColor(this, (int) R.attr.colorInteractiveNormal);
        MaterialButton materialButton = getBinding().c;
        Resources resources = materialButton.getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        materialButton.setText(StringResourceUtilsKt.getQuantityString(resources, (int) R.plurals.premium_guild_perks_modal_header_subscription_count_subscriptions, (int) R.string.premium_guild_subscription, premiumSubscriptionCount, Integer.valueOf(premiumSubscriptionCount)));
        materialButton.setTextColor(themedColor);
        setOnClickAndDismissListener(materialButton, new WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$1(this, premiumSubscriptionCount, themedColor, j));
        MaterialButton materialButton2 = getBinding().p;
        ColorCompatKt.setDrawableColor(materialButton2, themedColor);
        materialButton2.setTextColor(themedColor);
        setOnClickAndDismissListener(materialButton2, new WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$2(this, themedColor, j));
        MaterialButton materialButton3 = getBinding().f2410s;
        materialButton3.setVisibility(canAccessSettings ? 0 : 8);
        ColorCompatKt.setDrawableColor(materialButton3, themedColor);
        materialButton3.setTextColor(themedColor);
        setOnClickAndDismissListener(materialButton3, new WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$3(this, canAccessSettings, themedColor, j));
        MaterialButton materialButton4 = getBinding().l;
        if (ableToInstantInvite) {
            i = 0;
        }
        materialButton4.setVisibility(i);
        ColorCompatKt.setDrawableColor(materialButton4, themedColor);
        materialButton4.setTextColor(themedColor);
        setOnClickAndDismissListener(materialButton4, new WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$4(this, ableToInstantInvite, themedColor, j));
        m.checkNotNullExpressionValue(materialButton4, "binding.guildProfileShee…Invite(guildId) }\n      }");
    }

    /* JADX WARN: Removed duplicated region for block: B:127:0x0347  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x0354  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureUI(com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel.ViewState.Loaded r59) {
        /*
            Method dump skipped, instructions count: 877
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.profile.WidgetGuildProfileSheet.configureUI(com.discord.widgets.guilds.profile.WidgetGuildProfileSheetViewModel$ViewState$Loaded):void");
    }

    private final void constrainIconToBanner() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(getBinding().d);
        CardView cardView = getBinding().j;
        m.checkNotNullExpressionValue(cardView, "binding.guildProfileSheetIconCard");
        constraintSet.clear(cardView.getId(), 3);
        CardView cardView2 = getBinding().j;
        m.checkNotNullExpressionValue(cardView2, "binding.guildProfileSheetIconCard");
        constraintSet.clear(cardView2.getId(), 4);
        CardView cardView3 = getBinding().j;
        m.checkNotNullExpressionValue(cardView3, "binding.guildProfileSheetIconCard");
        int id2 = cardView3.getId();
        SimpleDraweeView simpleDraweeView = getBinding().f2409b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildProfileSheetBanner");
        constraintSet.connect(id2, 3, simpleDraweeView.getId(), 4);
        CardView cardView4 = getBinding().j;
        m.checkNotNullExpressionValue(cardView4, "binding.guildProfileSheetIconCard");
        int id3 = cardView4.getId();
        SimpleDraweeView simpleDraweeView2 = getBinding().f2409b;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildProfileSheetBanner");
        constraintSet.connect(id3, 4, simpleDraweeView2.getId(), 4);
        constraintSet.applyTo(getBinding().d);
    }

    private final void constrainIconToParent() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(getBinding().d);
        CardView cardView = getBinding().j;
        m.checkNotNullExpressionValue(cardView, "binding.guildProfileSheetIconCard");
        constraintSet.clear(cardView.getId(), 3);
        CardView cardView2 = getBinding().j;
        m.checkNotNullExpressionValue(cardView2, "binding.guildProfileSheetIconCard");
        constraintSet.clear(cardView2.getId(), 4);
        CardView cardView3 = getBinding().j;
        m.checkNotNullExpressionValue(cardView3, "binding.guildProfileSheetIconCard");
        int id2 = cardView3.getId();
        ConstraintLayout constraintLayout = getBinding().d;
        m.checkNotNullExpressionValue(constraintLayout, "binding.guildProfileSheetConstraintLayout");
        int id3 = constraintLayout.getId();
        CardView cardView4 = getBinding().j;
        m.checkNotNullExpressionValue(cardView4, "binding.guildProfileSheetIconCard");
        ViewGroup.LayoutParams layoutParams = cardView4.getLayoutParams();
        ViewGroup.MarginLayoutParams marginLayoutParams = layoutParams instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams : null;
        constraintSet.connect(id2, 3, id3, 3, marginLayoutParams == null ? 0 : marginLayoutParams.topMargin);
        constraintSet.applyTo(getBinding().d);
    }

    private final Unit dismissAlert() {
        Unit unit;
        AlertDialog alertDialog = this.dialog;
        if (alertDialog != null) {
            alertDialog.dismiss();
            unit = Unit.a;
        } else {
            unit = null;
        }
        hideKeyboard(getView());
        return unit;
    }

    private final WidgetGuildProfileSheetBinding getBinding() {
        return (WidgetGuildProfileSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final boolean getFromGuildEventUpsell() {
        return ((Boolean) this.fromGuildEventUpsell$delegate.getValue()).booleanValue();
    }

    public final WidgetGuildProfileSheetViewModel getViewModel() {
        return (WidgetGuildProfileSheetViewModel) this.viewModel$delegate.getValue();
    }

    private final void handleDismissAndShowToast(WidgetGuildProfileSheetViewModel.Event.DismissAndShowToast dismissAndShowToast) {
        b.a.d.m.i(this, dismissAndShowToast.getStringRes(), 0, 4);
        dismiss();
    }

    public final void handleEvent(WidgetGuildProfileSheetViewModel.Event event) {
        if (event instanceof WidgetGuildProfileSheetViewModel.Event.DismissAndShowToast) {
            handleDismissAndShowToast((WidgetGuildProfileSheetViewModel.Event.DismissAndShowToast) event);
        }
    }

    public final void handleViewState(WidgetGuildProfileSheetViewModel.ViewState viewState) {
        if (viewState instanceof WidgetGuildProfileSheetViewModel.ViewState.Loading) {
            showLoadingView();
        } else if (viewState instanceof WidgetGuildProfileSheetViewModel.ViewState.Invalid) {
            dismiss();
        } else if (viewState instanceof WidgetGuildProfileSheetViewModel.ViewState.Loaded) {
            updateView((WidgetGuildProfileSheetViewModel.ViewState.Loaded) viewState);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final void launchInvite(long j) {
        WidgetGuildInviteShare.Companion companion = WidgetGuildInviteShare.Companion;
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.launch(context, parentFragmentManager, j, (r22 & 8) != 0 ? null : null, (r22 & 16) != 0 ? false : false, (r22 & 32) != 0 ? null : null, (r22 & 64) != 0 ? null : null, "Guild Profile");
        }
    }

    private final int maxEmojisPerRow() {
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        int i = resources.getDisplayMetrics().widthPixels;
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CardView cardView = widgetGuildProfileActionsBinding.n;
        m.checkNotNullExpressionValue(cardView, "guildActionBinding.guildProfileSheetEmojisCard");
        int contentPaddingLeft = cardView.getContentPaddingLeft();
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding2 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding2 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        CardView cardView2 = widgetGuildProfileActionsBinding2.n;
        m.checkNotNullExpressionValue(cardView2, "guildActionBinding.guildProfileSheetEmojisCard");
        int contentPaddingRight = cardView2.getContentPaddingRight() + contentPaddingLeft;
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding3 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding3 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        LinearLayout linearLayout = widgetGuildProfileActionsBinding3.f;
        m.checkNotNullExpressionValue(linearLayout, "guildActionBinding.guild…ofileSheetBottomContainer");
        int paddingStart = linearLayout.getPaddingStart();
        WidgetGuildProfileActionsBinding widgetGuildProfileActionsBinding4 = this.guildActionBinding;
        if (widgetGuildProfileActionsBinding4 == null) {
            m.throwUninitializedPropertyAccessException("guildActionBinding");
        }
        LinearLayout linearLayout2 = widgetGuildProfileActionsBinding4.f;
        m.checkNotNullExpressionValue(linearLayout2, "guildActionBinding.guild…ofileSheetBottomContainer");
        return ((i - contentPaddingRight) - (linearLayout2.getPaddingEnd() + paddingStart)) / getResources().getDimensionPixelSize(R.dimen.emoji_size);
    }

    public static final void show(FragmentManager fragmentManager, boolean z2, long j, long j2, boolean z3) {
        Companion.show(fragmentManager, z2, j, j2, z3);
    }

    public final void showLeaveServerDialog(long j) {
        WidgetLeaveGuildDialog.Companion companion = WidgetLeaveGuildDialog.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager, j);
        dismissAlert();
    }

    private final void showLoadingView() {
        AppViewFlipper appViewFlipper = getBinding().g;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.guildProfileSheetFlipper");
        appViewFlipper.setDisplayedChild(0);
    }

    private final void updateView(WidgetGuildProfileSheetViewModel.ViewState.Loaded loaded) {
        AppViewFlipper appViewFlipper = getBinding().g;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.guildProfileSheetFlipper");
        appViewFlipper.setDisplayedChild(1);
        configureUI(loaded);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_guild_profile_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        this.channelId = Long.valueOf(getArgumentsOrDefault().getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID"));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildProfileSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildProfileSheet$onViewCreated$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetGuildProfileSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildProfileSheet$onViewCreated$2(this));
        this.emojisAdapter.setOnClickEmoji(new WidgetGuildProfileSheet$onViewCreated$3(this));
    }
}
