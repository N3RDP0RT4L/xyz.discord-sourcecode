package com.discord.widgets.guilds.invite;

import androidx.annotation.IdRes;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetGuildInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\u000b¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "getTextViewId"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteSettings$ChannelsSpinnerAdapter$setupViews$1 extends o implements Function0<Integer> {
    public final /* synthetic */ boolean $dropDownMode;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildInviteSettings$ChannelsSpinnerAdapter$setupViews$1(boolean z2) {
        super(0);
        this.$dropDownMode = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    @IdRes
    public final Integer invoke() {
        return this.$dropDownMode ? R.id.channel_spinner_dropdown_item_textview : R.id.channel_spinner_item_textview;
    }
}
