package com.discord.widgets.guilds.invite;

import android.content.Context;
import com.discord.models.domain.ModelInvite;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildInviteShare.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteSuggestionItem;", "item", "", "invoke", "(Lcom/discord/widgets/guilds/invite/InviteSuggestionItem;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShare$configureUI$3 extends o implements Function1<InviteSuggestionItem, Unit> {
    public final /* synthetic */ ModelInvite $invite;
    public final /* synthetic */ WidgetGuildInviteShareViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ WidgetGuildInviteShare this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildInviteShare$configureUI$3(WidgetGuildInviteShare widgetGuildInviteShare, WidgetGuildInviteShareViewModel.ViewState.Loaded loaded, ModelInvite modelInvite) {
        super(1);
        this.this$0 = widgetGuildInviteShare;
        this.$viewState = loaded;
        this.$invite = modelInvite;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(InviteSuggestionItem inviteSuggestionItem) {
        invoke2(inviteSuggestionItem);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(InviteSuggestionItem inviteSuggestionItem) {
        m.checkNotNullParameter(inviteSuggestionItem, "item");
        this.this$0.sendInvite(inviteSuggestionItem, this.$viewState, this.$invite);
        AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
        Context context = WidgetGuildInviteShare.access$getAdapter$p(this.this$0).getContext();
        String string = this.this$0.getString(R.string.invite_sent);
        m.checkNotNullExpressionValue(string, "getString(R.string.invite_sent)");
        accessibilityUtils.sendAnnouncement(context, string);
    }
}
