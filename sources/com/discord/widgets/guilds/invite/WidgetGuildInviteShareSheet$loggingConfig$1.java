package com.discord.widgets.guilds.invite;

import android.os.Bundle;
import com.discord.analytics.generated.events.impression.TrackImpressionGuildAddGuildInvite;
import com.discord.analytics.generated.events.impression.TrackImpressionGuildInvite;
import com.discord.analytics.generated.traits.TrackImpressionMetadata;
import com.discord.analytics.utils.ImpressionGroups;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildInviteShareSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/science/AnalyticsSchema;", "invoke", "()Lcom/discord/api/science/AnalyticsSchema;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShareSheet$loggingConfig$1 extends o implements Function0<AnalyticsSchema> {
    public final /* synthetic */ WidgetGuildInviteShareSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildInviteShareSheet$loggingConfig$1(WidgetGuildInviteShareSheet widgetGuildInviteShareSheet) {
        super(0);
        this.this$0 = widgetGuildInviteShareSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AnalyticsSchema invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        Bundle argumentsOrDefault3;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        String string = argumentsOrDefault.getString("ARG_ANALYTICS_SOURCE");
        if (string != null && string.hashCode() == -1756346871 && string.equals("Guild Create")) {
            TrackImpressionGuildAddGuildInvite trackImpressionGuildAddGuildInvite = new TrackImpressionGuildAddGuildInvite();
            trackImpressionGuildAddGuildInvite.c(new TrackImpressionMetadata(null, null, null, ImpressionGroups.GUILD_ADD_FLOW, 7));
            return trackImpressionGuildAddGuildInvite;
        }
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        long j = argumentsOrDefault2.getLong("ARG_GUILD_ID");
        argumentsOrDefault3 = this.this$0.getArgumentsOrDefault();
        return new TrackImpressionGuildInvite(Long.valueOf(j), Long.valueOf(argumentsOrDefault3.getLong("ARG_CHANNEL_ID")));
    }
}
