package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreMessagesMostRecent;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserAffinities;
import com.discord.stores.StoreUserRelationships;
import com.discord.widgets.guilds.invite.InviteSuggestionsManager;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: InviteSuggestionsManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u0018B9\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u000e\u001a\u00020\r\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0016\u0010\u0017J\u0019\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager;", "", "Lrx/Observable;", "", "Lcom/discord/widgets/guilds/invite/InviteSuggestion;", "observeInviteSuggestions", "()Lrx/Observable;", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StoreUserRelationships;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreUserAffinities;", "storeUserAffinities", "Lcom/discord/stores/StoreUserAffinities;", "Lcom/discord/stores/StoreMessagesMostRecent;", "storeMessagesMostRecent", "Lcom/discord/stores/StoreMessagesMostRecent;", HookHelper.constructorName, "(Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreMessagesMostRecent;Lcom/discord/stores/StoreUserAffinities;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserRelationships;)V", "UserAffinityData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InviteSuggestionsManager {
    private final StoreChannels storeChannels;
    private final StoreMessagesMostRecent storeMessagesMostRecent;
    private final StoreUserAffinities storeUserAffinities;
    private final StoreUserRelationships storeUserRelationships;
    private final StoreUser storeUsers;

    /* compiled from: InviteSuggestionsManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\b\u0082\b\u0018\u00002\u00020\u0001BM\u0012\u0010\u0010\u000e\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002\u0012\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0007\u0012\u001a\u0010\u0010\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u000bj\u0002`\f0\u0007¢\u0006\u0004\b!\u0010\"J\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ$\u0010\r\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u000bj\u0002`\f0\u0007HÆ\u0003¢\u0006\u0004\b\r\u0010\nJ\\\u0010\u0011\u001a\u00020\u00002\u0012\b\u0002\u0010\u000e\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\u0018\b\u0002\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u00072\u001c\b\u0002\u0010\u0010\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u000bj\u0002`\f0\u0007HÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR-\u0010\u0010\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u000bj\u0002`\f0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b\u001d\u0010\nR#\u0010\u000e\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u0006R)\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b \u0010\n¨\u0006#"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager$UserAffinityData;", "", "", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/util/List;", "", "Lcom/discord/models/user/User;", "component2", "()Ljava/util/Map;", "", "Lcom/discord/primitives/RelationshipType;", "component3", "userIds", "users", "relationships", "copy", "(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager$UserAffinityData;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getRelationships", "Ljava/util/List;", "getUserIds", "getUsers", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserAffinityData {
        private final Map<Long, Integer> relationships;
        private final List<Long> userIds;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public UserAffinityData(List<Long> list, Map<Long, ? extends User> map, Map<Long, Integer> map2) {
            m.checkNotNullParameter(list, "userIds");
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "relationships");
            this.userIds = list;
            this.users = map;
            this.relationships = map2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ UserAffinityData copy$default(UserAffinityData userAffinityData, List list, Map map, Map map2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = userAffinityData.userIds;
            }
            if ((i & 2) != 0) {
                map = userAffinityData.users;
            }
            if ((i & 4) != 0) {
                map2 = userAffinityData.relationships;
            }
            return userAffinityData.copy(list, map, map2);
        }

        public final List<Long> component1() {
            return this.userIds;
        }

        public final Map<Long, User> component2() {
            return this.users;
        }

        public final Map<Long, Integer> component3() {
            return this.relationships;
        }

        public final UserAffinityData copy(List<Long> list, Map<Long, ? extends User> map, Map<Long, Integer> map2) {
            m.checkNotNullParameter(list, "userIds");
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "relationships");
            return new UserAffinityData(list, map, map2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UserAffinityData)) {
                return false;
            }
            UserAffinityData userAffinityData = (UserAffinityData) obj;
            return m.areEqual(this.userIds, userAffinityData.userIds) && m.areEqual(this.users, userAffinityData.users) && m.areEqual(this.relationships, userAffinityData.relationships);
        }

        public final Map<Long, Integer> getRelationships() {
            return this.relationships;
        }

        public final List<Long> getUserIds() {
            return this.userIds;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            List<Long> list = this.userIds;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            Map<Long, User> map = this.users;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Integer> map2 = this.relationships;
            if (map2 != null) {
                i = map2.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("UserAffinityData(userIds=");
            R.append(this.userIds);
            R.append(", users=");
            R.append(this.users);
            R.append(", relationships=");
            return a.L(R, this.relationships, ")");
        }
    }

    public InviteSuggestionsManager() {
        this(null, null, null, null, null, 31, null);
    }

    public InviteSuggestionsManager(StoreChannels storeChannels, StoreMessagesMostRecent storeMessagesMostRecent, StoreUserAffinities storeUserAffinities, StoreUser storeUser, StoreUserRelationships storeUserRelationships) {
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeMessagesMostRecent, "storeMessagesMostRecent");
        m.checkNotNullParameter(storeUserAffinities, "storeUserAffinities");
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(storeUserRelationships, "storeUserRelationships");
        this.storeChannels = storeChannels;
        this.storeMessagesMostRecent = storeMessagesMostRecent;
        this.storeUserAffinities = storeUserAffinities;
        this.storeUsers = storeUser;
        this.storeUserRelationships = storeUserRelationships;
    }

    public final Observable<List<InviteSuggestion>> observeInviteSuggestions() {
        Observable<List<InviteSuggestion>> i = Observable.i(this.storeChannels.observePrivateChannels(), this.storeMessagesMostRecent.observeRecentMessageIds().Z(1), this.storeUserAffinities.observeAffinityUserIds().Y(new b<List<? extends Long>, Observable<? extends UserAffinityData>>() { // from class: com.discord.widgets.guilds.invite.InviteSuggestionsManager$observeInviteSuggestions$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends InviteSuggestionsManager.UserAffinityData> call(List<? extends Long> list) {
                return call2((List<Long>) list);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends InviteSuggestionsManager.UserAffinityData> call2(final List<Long> list) {
                StoreUser storeUser;
                StoreUserRelationships storeUserRelationships;
                storeUser = InviteSuggestionsManager.this.storeUsers;
                m.checkNotNullExpressionValue(list, "affinityUserIds");
                Observable<Map<Long, User>> q = storeUser.observeUsers(list).q();
                storeUserRelationships = InviteSuggestionsManager.this.storeUserRelationships;
                return Observable.j(q, storeUserRelationships.observe(list), new Func2<Map<Long, ? extends User>, Map<Long, ? extends Integer>, InviteSuggestionsManager.UserAffinityData>() { // from class: com.discord.widgets.guilds.invite.InviteSuggestionsManager$observeInviteSuggestions$1.1
                    @Override // rx.functions.Func2
                    public /* bridge */ /* synthetic */ InviteSuggestionsManager.UserAffinityData call(Map<Long, ? extends User> map, Map<Long, ? extends Integer> map2) {
                        return call2(map, (Map<Long, Integer>) map2);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final InviteSuggestionsManager.UserAffinityData call2(Map<Long, ? extends User> map, Map<Long, Integer> map2) {
                        List list2 = list;
                        m.checkNotNullExpressionValue(list2, "affinityUserIds");
                        m.checkNotNullExpressionValue(map, "users");
                        m.checkNotNullExpressionValue(map2, "relationships");
                        return new InviteSuggestionsManager.UserAffinityData(list2, map, map2);
                    }
                });
            }
        }), InviteSuggestionsManager$observeInviteSuggestions$2.INSTANCE);
        m.checkNotNullExpressionValue(i, "Observable.combineLatest… inviteSuggestionList\n  }");
        return i;
    }

    public /* synthetic */ InviteSuggestionsManager(StoreChannels storeChannels, StoreMessagesMostRecent storeMessagesMostRecent, StoreUserAffinities storeUserAffinities, StoreUser storeUser, StoreUserRelationships storeUserRelationships, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 2) != 0 ? StoreStream.Companion.getMessagesMostRecent() : storeMessagesMostRecent, (i & 4) != 0 ? StoreStream.Companion.getUserAffinities() : storeUserAffinities, (i & 8) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 16) != 0 ? StoreStream.Companion.getUserRelationships() : storeUserRelationships);
    }
}
