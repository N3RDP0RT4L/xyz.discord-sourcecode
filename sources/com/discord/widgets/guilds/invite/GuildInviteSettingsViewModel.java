package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreInviteSettings;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: GuildInviteSettingsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 72\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004789:BG\u0012\u000e\u0010(\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t\u0012\n\u0010$\u001a\u00060\bj\u0002`#\u0012\b\b\u0002\u00102\u001a\u000201\u0012\b\b\u0002\u0010/\u001a\u00020.\u0012\u000e\b\u0002\u00104\u001a\b\u0012\u0004\u0012\u00020\u00030\u0013¢\u0006\u0004\b5\u00106J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\u000b\u001a\u00020\u00052\n\u0010\n\u001a\u00060\bj\u0002`\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\rH\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0013\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0017H\u0007¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u001b\u0010\u0012J\u0019\u0010\u001e\u001a\u00020\u00052\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cH\u0007¢\u0006\u0004\b\u001e\u0010\u001fR\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00140 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\"R\u001a\u0010$\u001a\u00060\bj\u0002`#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0018\u0010&\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u001e\u0010(\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0018\u0010*\u001a\u0004\u0018\u00010\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010+R\u0018\u0010,\u001a\u0004\u0018\u00010\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103¨\u0006;"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$ViewState;", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;)V", "", "Lcom/discord/primitives/ChannelId;", "targetChannelId", "generateInviteForChannel", "(J)V", "Lcom/discord/models/domain/ModelInvite;", "invite", "handleInviteCreationSuccess", "(Lcom/discord/models/domain/ModelInvite;)V", "handleInviteCreationFailure", "()V", "Lrx/Observable;", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/models/domain/ModelInvite$Settings;", "settings", "updatePendingInviteSettings", "(Lcom/discord/models/domain/ModelInvite$Settings;)V", "saveInviteSettings", "Lcom/discord/api/channel/Channel;", "channel", "selectChannel", "(Lcom/discord/api/channel/Channel;)V", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/primitives/GuildId;", "guildId", "J", "currentStoreState", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;", "channelId", "Ljava/lang/Long;", "targetChannel", "Lcom/discord/api/channel/Channel;", "inviteSettings", "Lcom/discord/models/domain/ModelInvite$Settings;", "Lcom/discord/widgets/guilds/invite/TargetChannelSelector;", "targetChannelSelector", "Lcom/discord/widgets/guilds/invite/TargetChannelSelector;", "Lcom/discord/stores/StoreInviteSettings;", "storeInviteSettings", "Lcom/discord/stores/StoreInviteSettings;", "storeStateObservable", HookHelper.constructorName, "(Ljava/lang/Long;JLcom/discord/stores/StoreInviteSettings;Lcom/discord/widgets/guilds/invite/TargetChannelSelector;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildInviteSettingsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final Long channelId;
    private StoreState currentStoreState;
    private PublishSubject<Event> eventSubject;
    private final long guildId;
    private ModelInvite.Settings inviteSettings;
    private final StoreInviteSettings storeInviteSettings;
    private Channel targetChannel;
    private final TargetChannelSelector targetChannelSelector;

    /* compiled from: GuildInviteSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.invite.GuildInviteSettingsViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GuildInviteSettingsViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GuildInviteSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ)\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreInviteSettings;", "storeInviteSettings", "Lrx/Observable;", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreInviteSettings;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, StoreInviteSettings storeInviteSettings) {
            Observable<StoreState> j2 = Observable.j(storeInviteSettings.getInviteSettings(), storeInviteSettings.getInvitableChannels(j), GuildInviteSettingsViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(j2, "Observable.combineLatest…hannels\n        )\n      }");
            return j2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildInviteSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event;", "", HookHelper.constructorName, "()V", "InviteCreationFailure", "InviteCreationSuccess", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event$InviteCreationSuccess;", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event$InviteCreationFailure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: GuildInviteSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event$InviteCreationFailure;", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class InviteCreationFailure extends Event {
            public static final InviteCreationFailure INSTANCE = new InviteCreationFailure();

            private InviteCreationFailure() {
                super(null);
            }
        }

        /* compiled from: GuildInviteSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event$InviteCreationSuccess;", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event;", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "component1", "()Lcom/discord/widgets/guilds/invite/GuildInvite;", "invite", "copy", "(Lcom/discord/widgets/guilds/invite/GuildInvite;)Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event$InviteCreationSuccess;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "getInvite", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/invite/GuildInvite;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class InviteCreationSuccess extends Event {
            private final GuildInvite invite;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public InviteCreationSuccess(GuildInvite guildInvite) {
                super(null);
                m.checkNotNullParameter(guildInvite, "invite");
                this.invite = guildInvite;
            }

            public static /* synthetic */ InviteCreationSuccess copy$default(InviteCreationSuccess inviteCreationSuccess, GuildInvite guildInvite, int i, Object obj) {
                if ((i & 1) != 0) {
                    guildInvite = inviteCreationSuccess.invite;
                }
                return inviteCreationSuccess.copy(guildInvite);
            }

            public final GuildInvite component1() {
                return this.invite;
            }

            public final InviteCreationSuccess copy(GuildInvite guildInvite) {
                m.checkNotNullParameter(guildInvite, "invite");
                return new InviteCreationSuccess(guildInvite);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof InviteCreationSuccess) && m.areEqual(this.invite, ((InviteCreationSuccess) obj).invite);
                }
                return true;
            }

            public final GuildInvite getInvite() {
                return this.invite;
            }

            public int hashCode() {
                GuildInvite guildInvite = this.invite;
                if (guildInvite != null) {
                    return guildInvite.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("InviteCreationSuccess(invite=");
                R.append(this.invite);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildInviteSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ4\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u0018\b\u0002\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R)\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;", "", "Lcom/discord/models/domain/ModelInvite$Settings;", "component1", "()Lcom/discord/models/domain/ModelInvite$Settings;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component2", "()Ljava/util/Map;", "inviteSettings", "invitableChannels", "copy", "(Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/Map;)Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getInvitableChannels", "Lcom/discord/models/domain/ModelInvite$Settings;", "getInviteSettings", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, Channel> invitableChannels;
        private final ModelInvite.Settings inviteSettings;

        public StoreState(ModelInvite.Settings settings, Map<Long, Channel> map) {
            m.checkNotNullParameter(settings, "inviteSettings");
            m.checkNotNullParameter(map, "invitableChannels");
            this.inviteSettings = settings;
            this.invitableChannels = map;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, ModelInvite.Settings settings, Map map, int i, Object obj) {
            if ((i & 1) != 0) {
                settings = storeState.inviteSettings;
            }
            if ((i & 2) != 0) {
                map = storeState.invitableChannels;
            }
            return storeState.copy(settings, map);
        }

        public final ModelInvite.Settings component1() {
            return this.inviteSettings;
        }

        public final Map<Long, Channel> component2() {
            return this.invitableChannels;
        }

        public final StoreState copy(ModelInvite.Settings settings, Map<Long, Channel> map) {
            m.checkNotNullParameter(settings, "inviteSettings");
            m.checkNotNullParameter(map, "invitableChannels");
            return new StoreState(settings, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.inviteSettings, storeState.inviteSettings) && m.areEqual(this.invitableChannels, storeState.invitableChannels);
        }

        public final Map<Long, Channel> getInvitableChannels() {
            return this.invitableChannels;
        }

        public final ModelInvite.Settings getInviteSettings() {
            return this.inviteSettings;
        }

        public int hashCode() {
            ModelInvite.Settings settings = this.inviteSettings;
            int i = 0;
            int hashCode = (settings != null ? settings.hashCode() : 0) * 31;
            Map<Long, Channel> map = this.invitableChannels;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(inviteSettings=");
            R.append(this.inviteSettings);
            R.append(", invitableChannels=");
            return a.L(R, this.invitableChannels, ")");
        }
    }

    /* compiled from: GuildInviteSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\f\u001a\u00020\u0006\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b \u0010!J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ6\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\f\u001a\u00020\u00062\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\bR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\nR\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005¨\u0006\""}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$ViewState;", "", "", "Lcom/discord/api/channel/Channel;", "component1", "()Ljava/util/List;", "Lcom/discord/models/domain/ModelInvite$Settings;", "component2", "()Lcom/discord/models/domain/ModelInvite$Settings;", "component3", "()Lcom/discord/api/channel/Channel;", "invitableChannels", "inviteSettings", "targetChannel", "copy", "(Ljava/util/List;Lcom/discord/models/domain/ModelInvite$Settings;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelInvite$Settings;", "getInviteSettings", "Lcom/discord/api/channel/Channel;", "getTargetChannel", "Ljava/util/List;", "getInvitableChannels", HookHelper.constructorName, "(Ljava/util/List;Lcom/discord/models/domain/ModelInvite$Settings;Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final List<Channel> invitableChannels;
        private final ModelInvite.Settings inviteSettings;
        private final Channel targetChannel;

        public ViewState(List<Channel> list, ModelInvite.Settings settings, Channel channel) {
            m.checkNotNullParameter(list, "invitableChannels");
            m.checkNotNullParameter(settings, "inviteSettings");
            this.invitableChannels = list;
            this.inviteSettings = settings;
            this.targetChannel = channel;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, List list, ModelInvite.Settings settings, Channel channel, int i, Object obj) {
            if ((i & 1) != 0) {
                list = viewState.invitableChannels;
            }
            if ((i & 2) != 0) {
                settings = viewState.inviteSettings;
            }
            if ((i & 4) != 0) {
                channel = viewState.targetChannel;
            }
            return viewState.copy(list, settings, channel);
        }

        public final List<Channel> component1() {
            return this.invitableChannels;
        }

        public final ModelInvite.Settings component2() {
            return this.inviteSettings;
        }

        public final Channel component3() {
            return this.targetChannel;
        }

        public final ViewState copy(List<Channel> list, ModelInvite.Settings settings, Channel channel) {
            m.checkNotNullParameter(list, "invitableChannels");
            m.checkNotNullParameter(settings, "inviteSettings");
            return new ViewState(list, settings, channel);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.invitableChannels, viewState.invitableChannels) && m.areEqual(this.inviteSettings, viewState.inviteSettings) && m.areEqual(this.targetChannel, viewState.targetChannel);
        }

        public final List<Channel> getInvitableChannels() {
            return this.invitableChannels;
        }

        public final ModelInvite.Settings getInviteSettings() {
            return this.inviteSettings;
        }

        public final Channel getTargetChannel() {
            return this.targetChannel;
        }

        public int hashCode() {
            List<Channel> list = this.invitableChannels;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            ModelInvite.Settings settings = this.inviteSettings;
            int hashCode2 = (hashCode + (settings != null ? settings.hashCode() : 0)) * 31;
            Channel channel = this.targetChannel;
            if (channel != null) {
                i = channel.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(invitableChannels=");
            R.append(this.invitableChannels);
            R.append(", inviteSettings=");
            R.append(this.inviteSettings);
            R.append(", targetChannel=");
            R.append(this.targetChannel);
            R.append(")");
            return R.toString();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildInviteSettingsViewModel(java.lang.Long r8, long r9, com.discord.stores.StoreInviteSettings r11, com.discord.widgets.guilds.invite.TargetChannelSelector r12, rx.Observable r13, int r14, kotlin.jvm.internal.DefaultConstructorMarker r15) {
        /*
            r7 = this;
            r15 = r14 & 4
            if (r15 == 0) goto La
            com.discord.stores.StoreStream$Companion r11 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreInviteSettings r11 = r11.getInviteSettings()
        La:
            r4 = r11
            r11 = r14 & 8
            if (r11 == 0) goto L14
            com.discord.widgets.guilds.invite.TargetChannelSelector r12 = new com.discord.widgets.guilds.invite.TargetChannelSelector
            r12.<init>()
        L14:
            r5 = r12
            r11 = r14 & 16
            if (r11 == 0) goto L1f
            com.discord.widgets.guilds.invite.GuildInviteSettingsViewModel$Companion r11 = com.discord.widgets.guilds.invite.GuildInviteSettingsViewModel.Companion
            rx.Observable r13 = com.discord.widgets.guilds.invite.GuildInviteSettingsViewModel.Companion.access$observeStoreState(r11, r9, r4)
        L1f:
            r6 = r13
            r0 = r7
            r1 = r8
            r2 = r9
            r0.<init>(r1, r2, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.invite.GuildInviteSettingsViewModel.<init>(java.lang.Long, long, com.discord.stores.StoreInviteSettings, com.discord.widgets.guilds.invite.TargetChannelSelector, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void generateInviteForChannel(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(StoreInviteSettings.generateInvite$default(this.storeInviteSettings, j, null, 2, null), false, 1, null), this, null, 2, null), GuildInviteSettingsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new GuildInviteSettingsViewModel$generateInviteForChannel$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildInviteSettingsViewModel$generateInviteForChannel$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleInviteCreationFailure() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.InviteCreationFailure.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleInviteCreationSuccess(ModelInvite modelInvite) {
        GuildInvite createFromModelInvite = GuildInvite.Companion.createFromModelInvite(modelInvite);
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.InviteCreationSuccess(createFromModelInvite));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        ModelInvite.Settings settings = this.inviteSettings;
        if (settings == null) {
            this.inviteSettings = storeState.getInviteSettings();
            settings = storeState.getInviteSettings();
        }
        Map<Long, Channel> invitableChannels = storeState.getInvitableChannels();
        Channel channel = this.targetChannel;
        if (channel == null) {
            channel = this.targetChannelSelector.getTargetChannel(invitableChannels, this.channelId);
            this.targetChannel = channel;
        }
        updateViewState(new ViewState(u.toList(invitableChannels.values()), settings, channel));
        this.currentStoreState = storeState;
    }

    public final Observable<Event> observeEvents() {
        return this.eventSubject;
    }

    @MainThread
    public final void saveInviteSettings() {
        Channel channel;
        ModelInvite.Settings settings = this.inviteSettings;
        if (settings != null && (channel = this.targetChannel) != null) {
            this.storeInviteSettings.setInviteSettings(settings);
            generateInviteForChannel(channel.h());
        }
    }

    @MainThread
    public final void selectChannel(Channel channel) {
        ViewState viewState = getViewState();
        if (viewState != null && channel != null) {
            this.targetChannel = channel;
            updateViewState(ViewState.copy$default(viewState, null, null, channel, 3, null));
        }
    }

    @MainThread
    public final void updatePendingInviteSettings(ModelInvite.Settings settings) {
        m.checkNotNullParameter(settings, "settings");
        ViewState viewState = getViewState();
        if (viewState != null) {
            this.inviteSettings = settings;
            updateViewState(ViewState.copy$default(viewState, null, settings, null, 5, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildInviteSettingsViewModel(Long l, long j, StoreInviteSettings storeInviteSettings, TargetChannelSelector targetChannelSelector, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeInviteSettings, "storeInviteSettings");
        m.checkNotNullParameter(targetChannelSelector, "targetChannelSelector");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.channelId = l;
        this.guildId = j;
        this.storeInviteSettings = storeInviteSettings;
        this.targetChannelSelector = targetChannelSelector;
        PublishSubject<Event> k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.eventSubject = k0;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GuildInviteSettingsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
