package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.MainThread;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.i.c0;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetGuildInviteSettingsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.guilds.invite.GuildInviteSettingsViewModel;
import d0.d0.f;
import d0.t.o;
import d0.z.d.a0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGuildInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 52\u00020\u0001:\u000265B\u0007¢\u0006\u0004\b4\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J3\u0010\r\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0017\u0010\u0004J\u0015\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fR\u001d\u0010%\u001a\u00020 8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b'\u0010(R\u001c\u0010*\u001a\u00020)8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u001d\u00103\u001a\u00020.8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102¨\u00067"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteSettings;", "Lcom/discord/app/AppFragment;", "", "setOnItemSelected", "()V", "Landroid/widget/RadioGroup;", "radioGroup", "", "valueSet", "Lkotlin/Function1;", "", "", "textFactory", "createHorizontalCheckableButtons", "(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V", "numUses", "", "getMaxUsesString", "(I)Ljava/lang/String;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$ViewState;", "viewState", "configureUi", "(Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$ViewState;)V", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel$Event;)V", "Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/invite/GuildInviteSettingsViewModel;", "viewModel", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteSettings$ChannelsSpinnerAdapter;", "channelsSpinnerAdapter", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteSettings$ChannelsSpinnerAdapter;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/databinding/WidgetGuildInviteSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildInviteSettingsBinding;", "binding", HookHelper.constructorName, "Companion", "ChannelsSpinnerAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildInviteSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildInviteSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_CREATED_INVITE = "EXTRA_CREATED_INVITE";
    private ChannelsSpinnerAdapter channelsSpinnerAdapter;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildInviteSettings$binding$2.INSTANCE, null, 2, null);
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetGuildInviteSettings$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetGuildInviteSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B'\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\u0006\u0010 \u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u0018¢\u0006\u0004\b!\u0010\"J1\u0010\n\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J)\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J)\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0017\u0010\u0016J\u001b\u0010\u001a\u001a\u00020\f2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001d¨\u0006#"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteSettings$ChannelsSpinnerAdapter;", "Landroid/widget/ArrayAdapter;", "Lcom/discord/api/channel/Channel;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "layoutId", "Landroid/view/View;", "convertView", "", "dropDownMode", "getItemView", "(IILandroid/view/View;Z)Landroid/view/View;", "", "setupViews", "(Landroid/view/View;IZ)V", "getCount", "()I", "getItem", "(I)Lcom/discord/api/channel/Channel;", "Landroid/view/ViewGroup;", "parent", "getView", "(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;", "getDropDownView", "", "newData", "setData", "([Lcom/discord/api/channel/Channel;)V", "channels", "[Lcom/discord/api/channel/Channel;", "Landroid/content/Context;", "context", "textViewResourceId", HookHelper.constructorName, "(Landroid/content/Context;I[Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelsSpinnerAdapter extends ArrayAdapter<Channel> {
        private Channel[] channels;

        public /* synthetic */ ChannelsSpinnerAdapter(Context context, int i, Channel[] channelArr, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(context, i, (i2 & 4) != 0 ? new Channel[0] : channelArr);
        }

        private final View getItemView(int i, int i2, View view, boolean z2) {
            if (view == null) {
                view = View.inflate(getContext(), i2, null);
            }
            m.checkNotNullExpressionValue(view, "view");
            setupViews(view, i, z2);
            return view;
        }

        private final void setupViews(View view, int i, boolean z2) {
            TextView textView = (TextView) view.findViewById(new WidgetGuildInviteSettings$ChannelsSpinnerAdapter$setupViews$1(z2).invoke());
            m.checkNotNullExpressionValue(textView, "label");
            String format = String.format("#%s", Arrays.copyOf(new Object[]{ChannelUtils.c(this.channels[i])}, 1));
            m.checkNotNullExpressionValue(format, "java.lang.String.format(format, *args)");
            textView.setText(format);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public int getCount() {
            return this.channels.length;
        }

        @Override // android.widget.ArrayAdapter, android.widget.BaseAdapter, android.widget.SpinnerAdapter
        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            m.checkNotNullParameter(viewGroup, "parent");
            return getItemView(i, R.layout.view_invite_settngs_channel_spinner_item_open, view, true);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            m.checkNotNullParameter(viewGroup, "parent");
            return getItemView(i, R.layout.view_invite_settings_channel_spinner_item, view, false);
        }

        public final void setData(Channel[] channelArr) {
            m.checkNotNullParameter(channelArr, "newData");
            this.channels = channelArr;
            notifyDataSetChanged();
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ChannelsSpinnerAdapter(Context context, int i, Channel[] channelArr) {
            super(context, i, channelArr);
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(channelArr, "channels");
            this.channels = channelArr;
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public Channel getItem(int i) {
            return this.channels[i];
        }
    }

    /* compiled from: WidgetGuildInviteSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001a\u0010\u001bJG\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u000e\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n2\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000f\u0010\u0010J/\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0012\u001a\u00020\u00112\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u000e0\u0013¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteSettings$Companion;", "", "Landroid/content/Context;", "context", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "launcher", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "", "source", "", "launch", "(Landroid/content/Context;Landroidx/activity/result/ActivityResultLauncher;Ljava/lang/Long;JLjava/lang/String;)V", "Landroidx/fragment/app/DialogFragment;", "fragment", "Lkotlin/Function1;", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "callback", "registerForResult", "(Landroidx/fragment/app/DialogFragment;Lkotlin/jvm/functions/Function1;)Landroidx/activity/result/ActivityResultLauncher;", WidgetGuildInviteSettings.EXTRA_CREATED_INVITE, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, ActivityResultLauncher<Intent> activityResultLauncher, Long l, long j, String str) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(activityResultLauncher, "launcher");
            m.checkNotNullParameter(str, "source");
            AnalyticsTracker.openModal$default("Link Settings", str, null, 4, null);
            Intent intent = new Intent();
            if (l != null) {
                intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", l.longValue());
            }
            intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            j.g.f(context, activityResultLauncher, WidgetGuildInviteSettings.class, intent);
        }

        public final ActivityResultLauncher<Intent> registerForResult(DialogFragment dialogFragment, final Function1<? super GuildInvite, Unit> function1) {
            m.checkNotNullParameter(dialogFragment, "fragment");
            m.checkNotNullParameter(function1, "callback");
            ActivityResultLauncher<Intent> registerForActivityResult = dialogFragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteSettings$Companion$registerForResult$1
                public final void onActivityResult(ActivityResult activityResult) {
                    m.checkNotNullExpressionValue(activityResult, "activityResult");
                    if (activityResult.getResultCode() == -1) {
                        Intent data = activityResult.getData();
                        GuildInvite guildInvite = null;
                        Serializable serializableExtra = data != null ? data.getSerializableExtra("EXTRA_CREATED_INVITE") : null;
                        if (serializableExtra instanceof GuildInvite) {
                            guildInvite = serializableExtra;
                        }
                        GuildInvite guildInvite2 = guildInvite;
                        if (guildInvite2 != null) {
                            Function1.this.invoke(guildInvite2);
                        }
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…  }\n          }\n        }");
            return registerForActivityResult;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildInviteSettings() {
        super(R.layout.widget_guild_invite_settings);
        WidgetGuildInviteSettings$viewModel$2 widgetGuildInviteSettings$viewModel$2 = new WidgetGuildInviteSettings$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildInviteSettingsViewModel.class), new WidgetGuildInviteSettings$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildInviteSettings$viewModel$2));
    }

    public static final /* synthetic */ ChannelsSpinnerAdapter access$getChannelsSpinnerAdapter$p(WidgetGuildInviteSettings widgetGuildInviteSettings) {
        ChannelsSpinnerAdapter channelsSpinnerAdapter = widgetGuildInviteSettings.channelsSpinnerAdapter;
        if (channelsSpinnerAdapter == null) {
            m.throwUninitializedPropertyAccessException("channelsSpinnerAdapter");
        }
        return channelsSpinnerAdapter;
    }

    @MainThread
    private final void createHorizontalCheckableButtons(RadioGroup radioGroup, int[] iArr, Function1<? super Integer, ? extends CharSequence> function1) {
        if (radioGroup.getChildCount() <= 0) {
            boolean z2 = false;
            for (int i : iArr) {
                View inflate = LayoutInflater.from(getContext()).inflate(R.layout.invite_settings_radio_button, (ViewGroup) radioGroup, false);
                Objects.requireNonNull(inflate, "rootView");
                AppCompatRadioButton appCompatRadioButton = (AppCompatRadioButton) inflate;
                m.checkNotNullExpressionValue(new c0(appCompatRadioButton), "InviteSettingsRadioButto…text), radioGroup, false)");
                m.checkNotNullExpressionValue(appCompatRadioButton, "binding.root");
                appCompatRadioButton.setId(i);
                m.checkNotNullExpressionValue(appCompatRadioButton, "binding.root");
                appCompatRadioButton.setText(function1.invoke(Integer.valueOf(i)));
                if (!z2) {
                    m.checkNotNullExpressionValue(appCompatRadioButton, "binding.root");
                    ViewGroup.LayoutParams layoutParams = appCompatRadioButton.getLayoutParams();
                    Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.RadioGroup.LayoutParams");
                    RadioGroup.LayoutParams layoutParams2 = (RadioGroup.LayoutParams) layoutParams;
                    layoutParams2.leftMargin = DimenUtils.dpToPixels(16);
                    m.checkNotNullExpressionValue(appCompatRadioButton, "binding.root");
                    appCompatRadioButton.setLayoutParams(layoutParams2);
                    z2 = true;
                }
                radioGroup.addView(appCompatRadioButton);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGuildInviteSettingsBinding getBinding() {
        return (WidgetGuildInviteSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getMaxUsesString(int i) {
        return i != 0 ? String.valueOf(i) : "∞";
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildInviteSettingsViewModel getViewModel() {
        return (GuildInviteSettingsViewModel) this.viewModel$delegate.getValue();
    }

    private final void setOnItemSelected() {
        Spinner spinner = getBinding().f2395b;
        m.checkNotNullExpressionValue(spinner, "binding.guildInviteChannelSpinner");
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteSettings$setOnItemSelected$1
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                GuildInviteSettingsViewModel viewModel;
                m.checkNotNullParameter(adapterView, "parent");
                m.checkNotNullParameter(view, "view");
                Channel item = WidgetGuildInviteSettings.access$getChannelsSpinnerAdapter$p(WidgetGuildInviteSettings.this).getItem(i);
                viewModel = WidgetGuildInviteSettings.this.getViewModel();
                viewModel.selectChannel(item);
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> adapterView) {
                m.checkNotNullParameter(adapterView, "parent");
            }
        });
    }

    public final void configureUi(GuildInviteSettingsViewModel.ViewState viewState) {
        Object obj;
        Object obj2;
        boolean z2;
        boolean z3;
        m.checkNotNullParameter(viewState, "viewState");
        List<Channel> invitableChannels = viewState.getInvitableChannels();
        ChannelsSpinnerAdapter channelsSpinnerAdapter = this.channelsSpinnerAdapter;
        if (channelsSpinnerAdapter == null) {
            m.throwUninitializedPropertyAccessException("channelsSpinnerAdapter");
        }
        Object[] array = invitableChannels.toArray(new Channel[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        channelsSpinnerAdapter.setData((Channel[]) array);
        Iterator<Channel> it = invitableChannels.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            Channel next = it.next();
            Channel targetChannel = viewState.getTargetChannel();
            if (targetChannel != null && targetChannel.h() == next.h()) {
                break;
            }
            i++;
        }
        getBinding().f2395b.setSelection(Math.max(i, 0), false);
        final ModelInvite.Settings inviteSettings = viewState.getInviteSettings();
        RadioGroup radioGroup = getBinding().c;
        m.checkNotNullExpressionValue(radioGroup, "binding.guildInviteExpiresAfterRadiogroup");
        IntRange until = f.until(0, radioGroup.getChildCount());
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(until, 10));
        Iterator<Integer> it2 = until.iterator();
        while (it2.hasNext()) {
            View childAt = getBinding().c.getChildAt(((d0.t.c0) it2).nextInt());
            Objects.requireNonNull(childAt, "null cannot be cast to non-null type android.widget.RadioButton");
            arrayList.add((RadioButton) childAt);
        }
        Iterator it3 = arrayList.iterator();
        while (true) {
            obj = null;
            if (!it3.hasNext()) {
                obj2 = null;
                break;
            }
            obj2 = it3.next();
            if (((RadioButton) obj2).getId() == inviteSettings.getMaxAge()) {
                z3 = true;
                continue;
            } else {
                z3 = false;
                continue;
            }
            if (z3) {
                break;
            }
        }
        RadioButton radioButton = (RadioButton) obj2;
        if (radioButton != null) {
            radioButton.setChecked(true);
        }
        getBinding().c.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteSettings$configureUi$4
            @Override // android.widget.RadioGroup.OnCheckedChangeListener
            public final void onCheckedChanged(RadioGroup radioGroup2, int i2) {
                GuildInviteSettingsViewModel viewModel;
                viewModel = WidgetGuildInviteSettings.this.getViewModel();
                ModelInvite.Settings mergeMaxAge = inviteSettings.mergeMaxAge(i2);
                m.checkNotNullExpressionValue(mergeMaxAge, "inviteSettings.mergeMaxAge(checkedId)");
                viewModel.updatePendingInviteSettings(mergeMaxAge);
            }
        });
        RadioGroup radioGroup2 = getBinding().e;
        m.checkNotNullExpressionValue(radioGroup2, "binding.guildInviteMaxUsesRadiogroup");
        IntRange until2 = f.until(0, radioGroup2.getChildCount());
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(until2, 10));
        Iterator<Integer> it4 = until2.iterator();
        while (it4.hasNext()) {
            View childAt2 = getBinding().e.getChildAt(((d0.t.c0) it4).nextInt());
            Objects.requireNonNull(childAt2, "null cannot be cast to non-null type android.widget.RadioButton");
            arrayList2.add((RadioButton) childAt2);
        }
        Iterator it5 = arrayList2.iterator();
        while (true) {
            if (!it5.hasNext()) {
                break;
            }
            Object next2 = it5.next();
            if (((RadioButton) next2).getId() == inviteSettings.getMaxUses()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                obj = next2;
                break;
            }
        }
        RadioButton radioButton2 = (RadioButton) obj;
        if (radioButton2 != null) {
            radioButton2.setChecked(true);
        }
        getBinding().e.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteSettings$configureUi$8
            @Override // android.widget.RadioGroup.OnCheckedChangeListener
            public final void onCheckedChanged(RadioGroup radioGroup3, int i2) {
                GuildInviteSettingsViewModel viewModel;
                viewModel = WidgetGuildInviteSettings.this.getViewModel();
                ModelInvite.Settings mergeMaxUses = inviteSettings.mergeMaxUses(i2);
                m.checkNotNullExpressionValue(mergeMaxUses, "inviteSettings.mergeMaxUses(checkedId)");
                viewModel.updatePendingInviteSettings(mergeMaxUses);
            }
        });
        CheckedSetting checkedSetting = getBinding().f;
        m.checkNotNullExpressionValue(checkedSetting, "binding.guildInviteTemporaryMembership");
        checkedSetting.setChecked(inviteSettings.isTemporary());
        getBinding().f.e(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteSettings$configureUi$9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildInviteSettingsBinding binding;
                GuildInviteSettingsViewModel viewModel;
                WidgetGuildInviteSettingsBinding binding2;
                binding = WidgetGuildInviteSettings.this.getBinding();
                binding.f.toggle();
                viewModel = WidgetGuildInviteSettings.this.getViewModel();
                ModelInvite.Settings settings = inviteSettings;
                binding2 = WidgetGuildInviteSettings.this.getBinding();
                CheckedSetting checkedSetting2 = binding2.f;
                m.checkNotNullExpressionValue(checkedSetting2, "binding.guildInviteTemporaryMembership");
                ModelInvite.Settings mergeTemporary = settings.mergeTemporary(checkedSetting2.isChecked());
                m.checkNotNullExpressionValue(mergeTemporary, "inviteSettings.mergeTemp…raryMembership.isChecked)");
                viewModel.updatePendingInviteSettings(mergeTemporary);
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteSettings$configureUi$10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildInviteSettingsViewModel viewModel;
                viewModel = WidgetGuildInviteSettings.this.getViewModel();
                viewModel.saveInviteSettings();
            }
        });
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    public final void handleEvent(GuildInviteSettingsViewModel.Event event) {
        m.checkNotNullParameter(event, "event");
        if (event instanceof GuildInviteSettingsViewModel.Event.InviteCreationSuccess) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_CREATED_INVITE, ((GuildInviteSettingsViewModel.Event.InviteCreationSuccess) event).getInvite());
            requireActivity().setResult(-1, intent);
            requireActivity().finish();
        } else if (m.areEqual(event, GuildInviteSettingsViewModel.Event.InviteCreationFailure.INSTANCE)) {
            b.a.d.m.g(requireContext(), R.string.default_failure_to_perform_action_message, 0, null, 12);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        this.channelsSpinnerAdapter = new ChannelsSpinnerAdapter(requireContext(), R.layout.view_invite_settings_channel_spinner_item, null, 4, null);
        Spinner spinner = getBinding().f2395b;
        m.checkNotNullExpressionValue(spinner, "binding.guildInviteChannelSpinner");
        ChannelsSpinnerAdapter channelsSpinnerAdapter = this.channelsSpinnerAdapter;
        if (channelsSpinnerAdapter == null) {
            m.throwUninitializedPropertyAccessException("channelsSpinnerAdapter");
        }
        spinner.setAdapter((SpinnerAdapter) channelsSpinnerAdapter);
        RadioGroup radioGroup = getBinding().c;
        m.checkNotNullExpressionValue(radioGroup, "binding.guildInviteExpiresAfterRadiogroup");
        int[] iArr = ModelInvite.Settings.EXPIRES_AFTER_ARRAY;
        m.checkNotNullExpressionValue(iArr, "ModelInvite.Settings.EXPIRES_AFTER_ARRAY");
        createHorizontalCheckableButtons(radioGroup, iArr, new WidgetGuildInviteSettings$onViewBound$1(this));
        RadioGroup radioGroup2 = getBinding().e;
        m.checkNotNullExpressionValue(radioGroup2, "binding.guildInviteMaxUsesRadiogroup");
        int[] iArr2 = ModelInvite.Settings.MAX_USES_ARRAY;
        m.checkNotNullExpressionValue(iArr2, "ModelInvite.Settings.MAX_USES_ARRAY");
        createHorizontalCheckableButtons(radioGroup2, iArr2, new WidgetGuildInviteSettings$onViewBound$2(this));
        setOnItemSelected();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<GuildInviteSettingsViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel.observeViewSta…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetGuildInviteSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteSettings$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetGuildInviteSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteSettings$onViewBoundOrOnResume$2(this));
    }
}
