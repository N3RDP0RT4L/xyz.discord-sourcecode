package com.discord.widgets.guilds.invite;

import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreStream;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel;
import d0.t.g0;
import d0.t.h0;
import d0.t.n0;
import d0.t.o0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildInviteShareViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult;", "result", "", "invoke", "(Lcom/discord/utilities/messagesend/MessageResult;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShareViewModel$sendInviteToChannel$1 extends o implements Function1<MessageResult, Unit> {
    public final /* synthetic */ ModelInvite $invite;
    public final /* synthetic */ String $inviteLink;
    public final /* synthetic */ WidgetGuildInviteShareViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ WidgetGuildInviteShareViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildInviteShareViewModel$sendInviteToChannel$1(WidgetGuildInviteShareViewModel widgetGuildInviteShareViewModel, ModelInvite modelInvite, WidgetGuildInviteShareViewModel.ViewState.Loaded loaded, String str) {
        super(1);
        this.this$0 = widgetGuildInviteShareViewModel;
        this.$invite = modelInvite;
        this.$viewState = loaded;
        this.$inviteLink = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MessageResult messageResult) {
        invoke2(messageResult);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MessageResult messageResult) {
        m.checkNotNullParameter(messageResult, "result");
        if (messageResult instanceof MessageResult.Success) {
            MessageResult.Success success = (MessageResult.Success) messageResult;
            StoreStream.Companion.getAnalytics().inviteSent(this.$invite, success.getMessage(), "Guild Create Invite Suggestion");
            Map<String, Set<Long>> sentInvites = this.$viewState.getSentInvites();
            String str = this.$inviteLink;
            m.checkNotNullExpressionValue(str, "inviteLink");
            Set<Long> set = sentInvites.get(str);
            if (set == null) {
                set = n0.emptySet();
            }
            this.this$0.updateSentInvites(h0.plus(sentInvites, g0.mapOf(d0.o.to(this.$inviteLink, o0.plus(set, Long.valueOf(success.getMessage().g()))))));
        }
    }
}
