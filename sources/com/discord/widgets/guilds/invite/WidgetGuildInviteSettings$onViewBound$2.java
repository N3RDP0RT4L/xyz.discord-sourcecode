package com.discord.widgets.guilds.invite;

import d0.z.d.k;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGuildInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(I)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildInviteSettings$onViewBound$2 extends k implements Function1<Integer, String> {
    public WidgetGuildInviteSettings$onViewBound$2(WidgetGuildInviteSettings widgetGuildInviteSettings) {
        super(1, widgetGuildInviteSettings, WidgetGuildInviteSettings.class, "getMaxUsesString", "getMaxUsesString(I)Ljava/lang/String;", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ String invoke(Integer num) {
        return invoke(num.intValue());
    }

    public final String invoke(int i) {
        String maxUsesString;
        maxUsesString = ((WidgetGuildInviteSettings) this.receiver).getMaxUsesString(i);
        return maxUsesString;
    }
}
