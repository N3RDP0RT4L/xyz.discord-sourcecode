package com.discord.widgets.guilds.invite;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildInviteShareSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShareSheet$viewModel$2 extends o implements Function0<AppViewModel<GuildInviteShareSheetViewModel.ViewState>> {
    public final /* synthetic */ WidgetGuildInviteShareSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildInviteShareSheet$viewModel$2(WidgetGuildInviteShareSheet widgetGuildInviteShareSheet) {
        super(0);
        this.this$0 = widgetGuildInviteShareSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<GuildInviteShareSheetViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        long j = argumentsOrDefault.getLong("ARG_CHANNEL_ID");
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        return new GuildInviteShareSheetViewModel(j != 0 ? Long.valueOf(j) : null, argumentsOrDefault2.getLong("ARG_GUILD_ID"), null, null, null, null, null, null, 252, null);
    }
}
