package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.core.widget.NestedScrollView;
import b.a.i.z2;
import b.a.k.b;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.databinding.ViewGuildInviteBottomSheetBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.resources.DurationUtilsKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.guilds.invite.ViewInviteSettingsSheet;
import d0.d0.f;
import d0.t.c0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import xyz.discord.R;
/* compiled from: ViewInviteSettingsSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u00019B\u0011\b\u0016\u0012\u0006\u00101\u001a\u000200¢\u0006\u0004\b2\u00103B\u0019\b\u0016\u0012\u0006\u00101\u001a\u000200\u0012\u0006\u00105\u001a\u000204¢\u0006\u0004\b2\u00106B!\b\u0016\u0012\u0006\u00101\u001a\u000200\u0012\u0006\u00105\u001a\u000204\u0012\u0006\u00107\u001a\u00020\n¢\u0006\u0004\b2\u00108J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J3\u0010\r\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R(\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00020\u001a8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\"\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R\"\u0010%\u001a\u00020$8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R\u0018\u0010+\u001a\u0004\u0018\u00010!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R\u0016\u0010.\u001a\u00020-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/¨\u0006:"}, d2 = {"Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;", "Landroidx/core/widget/NestedScrollView;", "", "setOnItemSelected", "()V", "Landroid/widget/RadioGroup;", "radioGroup", "", "valueSet", "Lkotlin/Function1;", "", "", "textFactory", "createHorizontalCheckableButtons", "(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V", "numUses", "", "getMaxUsesString", "(I)Ljava/lang/String;", "Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "data", "configureUi", "(Lcom/discord/widgets/guilds/invite/WidgetInviteModel;)V", "Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;", "channelsSpinnerAdapter", "Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;", "Lkotlin/Function0;", "onGenerateLinkListener", "Lkotlin/jvm/functions/Function0;", "getOnGenerateLinkListener", "()Lkotlin/jvm/functions/Function0;", "setOnGenerateLinkListener", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/models/domain/ModelInvite$Settings;", "updateSettings", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "viewModel", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "getViewModel", "()Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "setViewModel", "(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;)V", "pendingInviteSettings", "Lcom/discord/models/domain/ModelInvite$Settings;", "Lcom/discord/databinding/ViewGuildInviteBottomSheetBinding;", "binding", "Lcom/discord/databinding/ViewGuildInviteBottomSheetBinding;", "Landroid/content/Context;", "ctx", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrSet", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "attr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "ChannelsSpinnerAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewInviteSettingsSheet extends NestedScrollView {
    private final ViewGuildInviteBottomSheetBinding binding;
    private final ChannelsSpinnerAdapter channelsSpinnerAdapter;
    private ModelInvite.Settings pendingInviteSettings;
    public WidgetGuildInviteShareViewModel viewModel;
    private Function1<? super ModelInvite.Settings, Unit> updateSettings = ViewInviteSettingsSheet$updateSettings$1.INSTANCE;
    private Function0<Unit> onGenerateLinkListener = ViewInviteSettingsSheet$onGenerateLinkListener$1.INSTANCE;

    /* compiled from: ViewInviteSettingsSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "expirationTime", "", "invoke", "(I)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.invite.ViewInviteSettingsSheet$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Integer, CharSequence> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ CharSequence invoke(Integer num) {
            return invoke(num.intValue());
        }

        public final CharSequence invoke(int i) {
            Context context = ViewInviteSettingsSheet.this.getContext();
            m.checkNotNullExpressionValue(context, "context");
            return DurationUtilsKt.formatInviteExpireAfterString(context, i);
        }
    }

    /* compiled from: ViewInviteSettingsSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(I)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.invite.ViewInviteSettingsSheet$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<Integer, String> {
        public AnonymousClass2(ViewInviteSettingsSheet viewInviteSettingsSheet) {
            super(1, viewInviteSettingsSheet, ViewInviteSettingsSheet.class, "getMaxUsesString", "getMaxUsesString(I)Ljava/lang/String;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ String invoke(Integer num) {
            return invoke(num.intValue());
        }

        public final String invoke(int i) {
            return ((ViewInviteSettingsSheet) this.receiver).getMaxUsesString(i);
        }
    }

    /* compiled from: ViewInviteSettingsSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B'\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\u0006\u0010 \u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u0018¢\u0006\u0004\b!\u0010\"J1\u0010\n\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J)\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J)\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0017\u0010\u0016J\u001b\u0010\u001a\u001a\u00020\f2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001d¨\u0006#"}, d2 = {"Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;", "Landroid/widget/ArrayAdapter;", "Lcom/discord/api/channel/Channel;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "layoutId", "Landroid/view/View;", "convertView", "", "dropDownMode", "getItemView", "(IILandroid/view/View;Z)Landroid/view/View;", "", "setupViews", "(Landroid/view/View;IZ)V", "getCount", "()I", "getItem", "(I)Lcom/discord/api/channel/Channel;", "Landroid/view/ViewGroup;", "parent", "getView", "(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;", "getDropDownView", "", "newData", "setData", "([Lcom/discord/api/channel/Channel;)V", "channels", "[Lcom/discord/api/channel/Channel;", "Landroid/content/Context;", "context", "textViewResourceId", HookHelper.constructorName, "(Landroid/content/Context;I[Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelsSpinnerAdapter extends ArrayAdapter<Channel> {
        private Channel[] channels;

        public /* synthetic */ ChannelsSpinnerAdapter(Context context, int i, Channel[] channelArr, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(context, i, (i2 & 4) != 0 ? new Channel[0] : channelArr);
        }

        private final View getItemView(int i, int i2, View view, boolean z2) {
            if (view == null) {
                view = View.inflate(getContext(), i2, null);
            }
            m.checkNotNullExpressionValue(view, "view");
            setupViews(view, i, z2);
            return view;
        }

        private final void setupViews(View view, int i, boolean z2) {
            TextView textView = (TextView) view.findViewById(new ViewInviteSettingsSheet$ChannelsSpinnerAdapter$setupViews$1(z2).invoke());
            m.checkNotNullExpressionValue(textView, "label");
            String format = String.format("#%s", Arrays.copyOf(new Object[]{ChannelUtils.c(this.channels[i])}, 1));
            m.checkNotNullExpressionValue(format, "java.lang.String.format(format, *args)");
            textView.setText(format);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public int getCount() {
            return this.channels.length;
        }

        @Override // android.widget.ArrayAdapter, android.widget.BaseAdapter, android.widget.SpinnerAdapter
        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            m.checkNotNullParameter(viewGroup, "parent");
            return getItemView(i, R.layout.view_invite_settngs_channel_spinner_item_open, view, true);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            m.checkNotNullParameter(viewGroup, "parent");
            return getItemView(i, R.layout.view_invite_settings_channel_spinner_item, view, false);
        }

        public final void setData(Channel[] channelArr) {
            m.checkNotNullParameter(channelArr, "newData");
            this.channels = channelArr;
            notifyDataSetChanged();
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ChannelsSpinnerAdapter(Context context, int i, Channel[] channelArr) {
            super(context, i, channelArr);
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(channelArr, "channels");
            this.channels = channelArr;
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public Channel getItem(int i) {
            return this.channels[i];
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewInviteSettingsSheet(Context context) {
        super(context);
        CharSequence d;
        m.checkNotNullParameter(context, "ctx");
        ViewGuildInviteBottomSheetBinding a = ViewGuildInviteBottomSheetBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildInviteBottomShe…ater.from(context), this)");
        this.binding = a;
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        ChannelsSpinnerAdapter channelsSpinnerAdapter = new ChannelsSpinnerAdapter(context2, R.layout.view_invite_settings_channel_spinner_item, null, 4, null);
        this.channelsSpinnerAdapter = channelsSpinnerAdapter;
        setFocusable(true);
        d = b.d(this, R.string.invite_settings_title, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        setContentDescription(d);
        Spinner spinner = a.f2172b;
        m.checkNotNullExpressionValue(spinner, "binding.guildInviteChannelSpinner");
        spinner.setAdapter((SpinnerAdapter) channelsSpinnerAdapter);
        RadioGroup radioGroup = a.c;
        m.checkNotNullExpressionValue(radioGroup, "binding.guildInviteExpiresAfterRadiogroup");
        int[] iArr = ModelInvite.Settings.EXPIRES_AFTER_ARRAY;
        m.checkNotNullExpressionValue(iArr, "ModelInvite.Settings.EXPIRES_AFTER_ARRAY");
        createHorizontalCheckableButtons(radioGroup, iArr, new AnonymousClass1());
        RadioGroup radioGroup2 = a.e;
        m.checkNotNullExpressionValue(radioGroup2, "binding.guildInviteMaxUsesRadiogroup");
        int[] iArr2 = ModelInvite.Settings.MAX_USES_ARRAY;
        m.checkNotNullExpressionValue(iArr2, "ModelInvite.Settings.MAX_USES_ARRAY");
        createHorizontalCheckableButtons(radioGroup2, iArr2, new AnonymousClass2(this));
        setOnItemSelected();
    }

    @MainThread
    private final void createHorizontalCheckableButtons(RadioGroup radioGroup, int[] iArr, Function1<? super Integer, ? extends CharSequence> function1) {
        if (radioGroup.getChildCount() <= 0) {
            boolean z2 = false;
            for (int i : iArr) {
                View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_radio_button, (ViewGroup) radioGroup, false);
                Objects.requireNonNull(inflate, "rootView");
                RadioButton radioButton = (RadioButton) inflate;
                m.checkNotNullExpressionValue(new z2(radioButton), "ViewRadioButtonBinding.i…text), radioGroup, false)");
                m.checkNotNullExpressionValue(radioButton, "binding.root");
                radioButton.setId(i);
                m.checkNotNullExpressionValue(radioButton, "binding.root");
                radioButton.setText(function1.invoke(Integer.valueOf(i)));
                if (!z2) {
                    m.checkNotNullExpressionValue(radioButton, "binding.root");
                    ViewGroup.LayoutParams layoutParams = radioButton.getLayoutParams();
                    Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.RadioGroup.LayoutParams");
                    RadioGroup.LayoutParams layoutParams2 = (RadioGroup.LayoutParams) layoutParams;
                    layoutParams2.leftMargin = DimenUtils.dpToPixels(16);
                    m.checkNotNullExpressionValue(radioButton, "binding.root");
                    radioButton.setLayoutParams(layoutParams2);
                    z2 = true;
                }
                radioGroup.addView(radioButton);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getMaxUsesString(int i) {
        return i != 0 ? String.valueOf(i) : "∞";
    }

    private final void setOnItemSelected() {
        Spinner spinner = this.binding.f2172b;
        m.checkNotNullExpressionValue(spinner, "binding.guildInviteChannelSpinner");
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: com.discord.widgets.guilds.invite.ViewInviteSettingsSheet$setOnItemSelected$1
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                ViewInviteSettingsSheet.ChannelsSpinnerAdapter channelsSpinnerAdapter;
                m.checkNotNullParameter(adapterView, "parent");
                m.checkNotNullParameter(view, "view");
                channelsSpinnerAdapter = ViewInviteSettingsSheet.this.channelsSpinnerAdapter;
                ViewInviteSettingsSheet.this.getViewModel().selectChannel(channelsSpinnerAdapter.getItem(i).h());
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> adapterView) {
                m.checkNotNullParameter(adapterView, "parent");
            }
        });
    }

    public final void configureUi(final WidgetInviteModel widgetInviteModel) {
        Object obj;
        Object obj2;
        boolean z2;
        boolean z3;
        m.checkNotNullParameter(widgetInviteModel, "data");
        ChannelsSpinnerAdapter channelsSpinnerAdapter = this.channelsSpinnerAdapter;
        boolean z4 = false;
        Object[] array = widgetInviteModel.getInvitableChannels().toArray(new Channel[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        channelsSpinnerAdapter.setData((Channel[]) array);
        Iterator<Channel> it = widgetInviteModel.getInvitableChannels().iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            Channel next = it.next();
            Channel targetChannel = widgetInviteModel.getTargetChannel();
            if (targetChannel != null && targetChannel.h() == next.h()) {
                break;
            }
            i++;
        }
        this.binding.f2172b.setSelection(Math.max(i, 0), false);
        ModelInvite.Settings settings = widgetInviteModel.getSettings();
        if (settings != null) {
            this.pendingInviteSettings = settings;
            RadioGroup radioGroup = this.binding.c;
            m.checkNotNullExpressionValue(radioGroup, "binding.guildInviteExpiresAfterRadiogroup");
            IntRange until = f.until(0, radioGroup.getChildCount());
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(until, 10));
            Iterator<Integer> it2 = until.iterator();
            while (it2.hasNext()) {
                View childAt = this.binding.c.getChildAt(((c0) it2).nextInt());
                Objects.requireNonNull(childAt, "null cannot be cast to non-null type android.widget.RadioButton");
                arrayList.add((RadioButton) childAt);
            }
            Iterator it3 = arrayList.iterator();
            while (true) {
                obj = null;
                if (!it3.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it3.next();
                int id2 = ((RadioButton) obj2).getId();
                ModelInvite.Settings settings2 = this.pendingInviteSettings;
                if (settings2 == null || id2 != settings2.getMaxAge()) {
                    z3 = false;
                    continue;
                } else {
                    z3 = true;
                    continue;
                }
                if (z3) {
                    break;
                }
            }
            RadioButton radioButton = (RadioButton) obj2;
            if (radioButton != null) {
                radioButton.setChecked(true);
            }
            this.binding.c.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { // from class: com.discord.widgets.guilds.invite.ViewInviteSettingsSheet$configureUi$4
                @Override // android.widget.RadioGroup.OnCheckedChangeListener
                public final void onCheckedChanged(RadioGroup radioGroup2, int i2) {
                    ModelInvite.Settings settings3;
                    ViewInviteSettingsSheet viewInviteSettingsSheet = ViewInviteSettingsSheet.this;
                    settings3 = viewInviteSettingsSheet.pendingInviteSettings;
                    viewInviteSettingsSheet.pendingInviteSettings = settings3 != null ? settings3.mergeMaxAge(i2) : null;
                }
            });
            RadioGroup radioGroup2 = this.binding.e;
            m.checkNotNullExpressionValue(radioGroup2, "binding.guildInviteMaxUsesRadiogroup");
            IntRange until2 = f.until(0, radioGroup2.getChildCount());
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(until2, 10));
            Iterator<Integer> it4 = until2.iterator();
            while (it4.hasNext()) {
                View childAt2 = this.binding.e.getChildAt(((c0) it4).nextInt());
                Objects.requireNonNull(childAt2, "null cannot be cast to non-null type android.widget.RadioButton");
                arrayList2.add((RadioButton) childAt2);
            }
            Iterator it5 = arrayList2.iterator();
            while (true) {
                if (!it5.hasNext()) {
                    break;
                }
                Object next2 = it5.next();
                int id3 = ((RadioButton) next2).getId();
                ModelInvite.Settings settings3 = this.pendingInviteSettings;
                if (settings3 == null || id3 != settings3.getMaxUses()) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    obj = next2;
                    break;
                }
            }
            RadioButton radioButton2 = (RadioButton) obj;
            if (radioButton2 != null) {
                radioButton2.setChecked(true);
            }
            this.binding.e.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { // from class: com.discord.widgets.guilds.invite.ViewInviteSettingsSheet$configureUi$8
                @Override // android.widget.RadioGroup.OnCheckedChangeListener
                public final void onCheckedChanged(RadioGroup radioGroup3, int i2) {
                    ModelInvite.Settings settings4;
                    ViewInviteSettingsSheet viewInviteSettingsSheet = ViewInviteSettingsSheet.this;
                    settings4 = viewInviteSettingsSheet.pendingInviteSettings;
                    viewInviteSettingsSheet.pendingInviteSettings = settings4 != null ? settings4.mergeMaxUses(i2) : null;
                }
            });
            CheckedSetting checkedSetting = this.binding.f;
            m.checkNotNullExpressionValue(checkedSetting, "binding.guildInviteTemporaryMembership");
            ModelInvite.Settings settings4 = this.pendingInviteSettings;
            if (settings4 != null) {
                z4 = settings4.isTemporary();
            }
            checkedSetting.setChecked(z4);
            this.binding.f.e(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.ViewInviteSettingsSheet$configureUi$9
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ViewGuildInviteBottomSheetBinding viewGuildInviteBottomSheetBinding;
                    ModelInvite.Settings settings5;
                    ModelInvite.Settings settings6;
                    ViewGuildInviteBottomSheetBinding viewGuildInviteBottomSheetBinding2;
                    viewGuildInviteBottomSheetBinding = ViewInviteSettingsSheet.this.binding;
                    viewGuildInviteBottomSheetBinding.f.toggle();
                    ViewInviteSettingsSheet viewInviteSettingsSheet = ViewInviteSettingsSheet.this;
                    settings5 = viewInviteSettingsSheet.pendingInviteSettings;
                    if (settings5 != null) {
                        viewGuildInviteBottomSheetBinding2 = ViewInviteSettingsSheet.this.binding;
                        CheckedSetting checkedSetting2 = viewGuildInviteBottomSheetBinding2.f;
                        m.checkNotNullExpressionValue(checkedSetting2, "binding.guildInviteTemporaryMembership");
                        settings6 = settings5.mergeTemporary(checkedSetting2.isChecked());
                    } else {
                        settings6 = null;
                    }
                    viewInviteSettingsSheet.pendingInviteSettings = settings6;
                }
            });
            this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.ViewInviteSettingsSheet$configureUi$10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ModelInvite.Settings settings5;
                    settings5 = ViewInviteSettingsSheet.this.pendingInviteSettings;
                    if (settings5 != null) {
                        ViewInviteSettingsSheet.this.getViewModel().updateInviteSettings(settings5);
                    }
                    Channel targetChannel2 = widgetInviteModel.getTargetChannel();
                    if (targetChannel2 != null) {
                        ViewInviteSettingsSheet.this.getViewModel().generateInviteLink(Long.valueOf(targetChannel2.h()).longValue());
                    }
                    ViewInviteSettingsSheet.this.getOnGenerateLinkListener().invoke();
                }
            });
        }
    }

    public final Function0<Unit> getOnGenerateLinkListener() {
        return this.onGenerateLinkListener;
    }

    public final WidgetGuildInviteShareViewModel getViewModel() {
        WidgetGuildInviteShareViewModel widgetGuildInviteShareViewModel = this.viewModel;
        if (widgetGuildInviteShareViewModel == null) {
            m.throwUninitializedPropertyAccessException("viewModel");
        }
        return widgetGuildInviteShareViewModel;
    }

    public final void setOnGenerateLinkListener(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onGenerateLinkListener = function0;
    }

    public final void setViewModel(WidgetGuildInviteShareViewModel widgetGuildInviteShareViewModel) {
        m.checkNotNullParameter(widgetGuildInviteShareViewModel, "<set-?>");
        this.viewModel = widgetGuildInviteShareViewModel;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewInviteSettingsSheet(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        CharSequence d;
        m.checkNotNullParameter(context, "ctx");
        m.checkNotNullParameter(attributeSet, "attrSet");
        ViewGuildInviteBottomSheetBinding a = ViewGuildInviteBottomSheetBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildInviteBottomShe…ater.from(context), this)");
        this.binding = a;
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        ChannelsSpinnerAdapter channelsSpinnerAdapter = new ChannelsSpinnerAdapter(context2, R.layout.view_invite_settings_channel_spinner_item, null, 4, null);
        this.channelsSpinnerAdapter = channelsSpinnerAdapter;
        setFocusable(true);
        d = b.d(this, R.string.invite_settings_title, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        setContentDescription(d);
        Spinner spinner = a.f2172b;
        m.checkNotNullExpressionValue(spinner, "binding.guildInviteChannelSpinner");
        spinner.setAdapter((SpinnerAdapter) channelsSpinnerAdapter);
        RadioGroup radioGroup = a.c;
        m.checkNotNullExpressionValue(radioGroup, "binding.guildInviteExpiresAfterRadiogroup");
        int[] iArr = ModelInvite.Settings.EXPIRES_AFTER_ARRAY;
        m.checkNotNullExpressionValue(iArr, "ModelInvite.Settings.EXPIRES_AFTER_ARRAY");
        createHorizontalCheckableButtons(radioGroup, iArr, new AnonymousClass1());
        RadioGroup radioGroup2 = a.e;
        m.checkNotNullExpressionValue(radioGroup2, "binding.guildInviteMaxUsesRadiogroup");
        int[] iArr2 = ModelInvite.Settings.MAX_USES_ARRAY;
        m.checkNotNullExpressionValue(iArr2, "ModelInvite.Settings.MAX_USES_ARRAY");
        createHorizontalCheckableButtons(radioGroup2, iArr2, new AnonymousClass2(this));
        setOnItemSelected();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewInviteSettingsSheet(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        CharSequence d;
        m.checkNotNullParameter(context, "ctx");
        m.checkNotNullParameter(attributeSet, "attrSet");
        ViewGuildInviteBottomSheetBinding a = ViewGuildInviteBottomSheetBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildInviteBottomShe…ater.from(context), this)");
        this.binding = a;
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        ChannelsSpinnerAdapter channelsSpinnerAdapter = new ChannelsSpinnerAdapter(context2, R.layout.view_invite_settings_channel_spinner_item, null, 4, null);
        this.channelsSpinnerAdapter = channelsSpinnerAdapter;
        setFocusable(true);
        d = b.d(this, R.string.invite_settings_title, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        setContentDescription(d);
        Spinner spinner = a.f2172b;
        m.checkNotNullExpressionValue(spinner, "binding.guildInviteChannelSpinner");
        spinner.setAdapter((SpinnerAdapter) channelsSpinnerAdapter);
        RadioGroup radioGroup = a.c;
        m.checkNotNullExpressionValue(radioGroup, "binding.guildInviteExpiresAfterRadiogroup");
        int[] iArr = ModelInvite.Settings.EXPIRES_AFTER_ARRAY;
        m.checkNotNullExpressionValue(iArr, "ModelInvite.Settings.EXPIRES_AFTER_ARRAY");
        createHorizontalCheckableButtons(radioGroup, iArr, new AnonymousClass1());
        RadioGroup radioGroup2 = a.e;
        m.checkNotNullExpressionValue(radioGroup2, "binding.guildInviteMaxUsesRadiogroup");
        int[] iArr2 = ModelInvite.Settings.MAX_USES_ARRAY;
        m.checkNotNullExpressionValue(iArr2, "ModelInvite.Settings.MAX_USES_ARRAY");
        createHorizontalCheckableButtons(radioGroup2, iArr2, new AnonymousClass2(this));
        setOnItemSelected();
    }
}
