package com.discord.widgets.guilds.invite;

import android.content.res.Resources;
import com.discord.app.AppViewModel;
import com.discord.utilities.intent.IntentUtilsKt;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildInviteShareCompact.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShareCompact$viewModel$2 extends o implements Function0<AppViewModel<WidgetGuildInviteShareViewModel.ViewState>> {
    public final /* synthetic */ WidgetGuildInviteShareCompact this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildInviteShareCompact$viewModel$2(WidgetGuildInviteShareCompact widgetGuildInviteShareCompact) {
        super(0);
        this.this$0 = widgetGuildInviteShareCompact;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetGuildInviteShareViewModel.ViewState> invoke() {
        long longExtra = this.this$0.getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_GUILD_ID", 0L);
        Long longExtraOrNull = IntentUtilsKt.getLongExtraOrNull(this.this$0.getMostRecentIntent(), "com.discord.intent.extra.EXTRA_GUILD_SCHEDULED_EVENT_ID");
        String stringExtra = this.this$0.getMostRecentIntent().getStringExtra(WidgetGuildInviteShare.INTENT_INVITE_STORE_KEY);
        Resources resources = this.this$0.getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        return new WidgetGuildInviteShareViewModel(null, null, null, null, null, null, null, null, null, resources, false, longExtra, longExtraOrNull, stringExtra, 1535, null);
    }
}
