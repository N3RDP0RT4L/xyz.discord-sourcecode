package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGuildInvitePageBinding;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.error.Error;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guilds.invite.GuildInviteViewModel;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventItemView;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventModel;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventModelKt;
import com.discord.widgets.home.HomeConfig;
import com.google.android.material.button.MaterialButton;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.b;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import org.objectweb.asm.Opcodes;
import xyz.discord.R;
/* compiled from: WidgetGuildInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 (2\u00020\u0001:\u0001(B\u0007¢\u0006\u0004\b'\u0010\u0014J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\r\u001a\u00020\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0015¢\u0006\u0004\b\u0016\u0010\u0017R\u001d\u0010\u001d\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0018\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u001d\u0010&\u001a\u00020!8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006)"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInvite;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/invite/GuildInviteViewModel$ViewState$Loaded;", "viewState", "", "configureLoadedUI", "(Lcom/discord/widgets/guilds/invite/GuildInviteViewModel$ViewState$Loaded;)V", "Lcom/discord/utilities/error/Error;", "e", "configureUIFailure", "(Lcom/discord/utilities/error/Error;)V", "Lcom/discord/models/domain/ModelInvite;", "invite", "trackAndConsumeDynamicLinkCache", "(Lcom/discord/models/domain/ModelInvite;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/guilds/invite/GuildInviteViewModel$ViewState;", "configureUI", "(Lcom/discord/widgets/guilds/invite/GuildInviteViewModel$ViewState;)V", "Lcom/discord/databinding/WidgetGuildInvitePageBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildInvitePageBinding;", "binding", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "inviteCode", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "Lcom/discord/widgets/guilds/invite/GuildInviteViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/invite/GuildInviteViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInvite extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildInvite.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildInvitePageBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_CODE = "EXTRA_CODE";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildInvite$binding$2.INSTANCE, null, 2, null);
    private final StoreInviteSettings.InviteCode inviteCode = (StoreInviteSettings.InviteCode) getMostRecentIntent().getParcelableExtra(EXTRA_CODE);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetGuildInvite.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "inviteCode", "", "launch", "(Landroid/content/Context;Lcom/discord/stores/StoreInviteSettings$InviteCode;)V", "", WidgetGuildInvite.EXTRA_CODE, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, StoreInviteSettings.InviteCode inviteCode) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(inviteCode, "inviteCode");
            Intent intent = new Intent();
            intent.putExtra(WidgetGuildInvite.EXTRA_CODE, inviteCode);
            j.d(context, WidgetGuildInvite.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildInvite() {
        super(R.layout.widget_guild_invite_page);
        WidgetGuildInvite$viewModel$2 widgetGuildInvite$viewModel$2 = new WidgetGuildInvite$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildInviteViewModel.class), new WidgetGuildInvite$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildInvite$viewModel$2));
    }

    private final void configureLoadedUI(GuildInviteViewModel.ViewState.Loaded loaded) {
        boolean z2;
        GuildScheduledEventModel model;
        final ModelInvite invite = loaded.getInvite();
        getBinding().e.configureUI(invite);
        GuildScheduledEventItemView guildScheduledEventItemView = getBinding().d;
        m.checkNotNullExpressionValue(guildScheduledEventItemView, "binding.guildInviteEventInfo");
        GuildScheduledEvent guildScheduledEvent = invite.getGuildScheduledEvent();
        if (guildScheduledEvent == null || (model = GuildScheduledEventModelKt.toModel(guildScheduledEvent)) == null) {
            z2 = false;
        } else {
            getBinding().d.configureAsPreview(model, invite.getChannel(), GuildScheduledEventUtilitiesKt.getCreatorUserGuildMember$default(model, (StoreGuilds) null, (StoreUser) null, 3, (Object) null));
            z2 = true;
        }
        guildScheduledEventItemView.setVisibility(z2 ? 0 : 8);
        MaterialButton materialButton = getBinding().c;
        m.checkNotNullExpressionValue(materialButton, "binding.guildInviteCancel");
        materialButton.setVisibility(8);
        MaterialButton materialButton2 = getBinding().f2394b;
        m.checkNotNullExpressionValue(materialButton2, "binding.guildInviteAccept");
        materialButton2.setVisibility(0);
        getBinding().f2394b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInvite$configureLoadedUI$2

            /* compiled from: WidgetGuildInvite.kt */
            @e(c = "com.discord.widgets.guilds.invite.WidgetGuildInvite$configureLoadedUI$2$1", f = "WidgetGuildInvite.kt", l = {}, m = "invokeSuspend")
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.guilds.invite.WidgetGuildInvite$configureLoadedUI$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends k implements Function2<Error, Continuation<? super Unit>, Object> {
                private /* synthetic */ Object L$0;
                public int label;

                public AnonymousClass1(Continuation continuation) {
                    super(2, continuation);
                }

                @Override // d0.w.i.a.a
                public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
                    m.checkNotNullParameter(continuation, "completion");
                    AnonymousClass1 r0 = new AnonymousClass1(continuation);
                    r0.L$0 = obj;
                    return r0;
                }

                @Override // kotlin.jvm.functions.Function2
                public final Object invoke(Error error, Continuation<? super Unit> continuation) {
                    return ((AnonymousClass1) create(error, continuation)).invokeSuspend(Unit.a);
                }

                @Override // d0.w.i.a.a
                public final Object invokeSuspend(Object obj) {
                    c.getCOROUTINE_SUSPENDED();
                    if (this.label == 0) {
                        l.throwOnFailure(obj);
                        WidgetGuildInvite.this.configureUIFailure((Error) this.L$0);
                        return Unit.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            /* compiled from: WidgetGuildInvite.kt */
            @e(c = "com.discord.widgets.guilds.invite.WidgetGuildInvite$configureLoadedUI$2$2", f = "WidgetGuildInvite.kt", l = {}, m = "invokeSuspend")
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelInvite;", "invite", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.guilds.invite.WidgetGuildInvite$configureLoadedUI$2$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends k implements Function2<ModelInvite, Continuation<? super Unit>, Object> {
                private /* synthetic */ Object L$0;
                public int label;

                public AnonymousClass2(Continuation continuation) {
                    super(2, continuation);
                }

                @Override // d0.w.i.a.a
                public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
                    m.checkNotNullParameter(continuation, "completion");
                    AnonymousClass2 r0 = new AnonymousClass2(continuation);
                    r0.L$0 = obj;
                    return r0;
                }

                @Override // kotlin.jvm.functions.Function2
                public final Object invoke(ModelInvite modelInvite, Continuation<? super Unit> continuation) {
                    return ((AnonymousClass2) create(modelInvite, continuation)).invokeSuspend(Unit.a);
                }

                @Override // d0.w.i.a.a
                public final Object invokeSuspend(Object obj) {
                    c.getCOROUTINE_SUSPENDED();
                    if (this.label == 0) {
                        l.throwOnFailure(obj);
                        ModelInvite modelInvite = (ModelInvite) this.L$0;
                        Intent addFlags = new Intent().addFlags(268468224);
                        Guild guild = modelInvite.guild;
                        Long l = null;
                        Long boxLong = b.boxBoolean(modelInvite.isNewMember()).booleanValue() ? guild != null ? b.boxLong(guild.r()) : null : null;
                        GuildScheduledEvent guildScheduledEvent = modelInvite.getGuildScheduledEvent();
                        if (guildScheduledEvent != null) {
                            l = b.boxLong(guildScheduledEvent.i());
                        }
                        addFlags.putExtra("com.discord.intent.extra.EXTRA_HOME_CONFIG", new HomeConfig(boxLong, l, false, 4, null));
                        m.checkNotNullExpressionValue(addFlags, "Intent()\n               …      )\n                }");
                        Context context = WidgetGuildInvite.this.getContext();
                        if (context != null) {
                            m.checkNotNullExpressionValue(context, "context");
                            j.c(context, false, addFlags, 2);
                            AppActivity appActivity = WidgetGuildInvite.this.getAppActivity();
                            if (appActivity != null) {
                                appActivity.finish();
                            }
                        }
                        return Unit.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                InviteJoinHelper.joinViaInvite$default(InviteJoinHelper.INSTANCE, invite, WidgetGuildInvite.this.getClass(), WidgetGuildInvite.this, "Accept Invite Page", new AnonymousClass1(null), null, new AnonymousClass2(null), null, Opcodes.IF_ICMPNE, null);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUIFailure(Error error) {
        MaterialButton materialButton = getBinding().c;
        m.checkNotNullExpressionValue(materialButton, "binding.guildInviteCancel");
        materialButton.setVisibility(0);
        MaterialButton materialButton2 = getBinding().f2394b;
        m.checkNotNullExpressionValue(materialButton2, "binding.guildInviteAccept");
        materialButton2.setVisibility(8);
        getBinding().e.configureUIFailure(error);
        trackAndConsumeDynamicLinkCache(null);
    }

    private final WidgetGuildInvitePageBinding getBinding() {
        return (WidgetGuildInvitePageBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final GuildInviteViewModel getViewModel() {
        return (GuildInviteViewModel) this.viewModel$delegate.getValue();
    }

    private final void trackAndConsumeDynamicLinkCache(ModelInvite modelInvite) {
        String source;
        String inviteCode;
        String source2;
        StoreStream.Companion.getInviteSettings().clearInviteCode();
        if (!getViewModel().getInviteResolved()) {
            getViewModel().setInviteResolved(true);
            String str = "";
            if (modelInvite != null) {
                AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                StoreInviteSettings.InviteCode inviteCode2 = this.inviteCode;
                if (!(inviteCode2 == null || (source2 = inviteCode2.getSource()) == null)) {
                    str = source2;
                }
                analyticsTracker.inviteResolved(modelInvite, str);
                return;
            }
            StoreInviteSettings.InviteCode inviteCode3 = this.inviteCode;
            String str2 = (inviteCode3 == null || (inviteCode = inviteCode3.getInviteCode()) == null) ? str : inviteCode;
            StoreInviteSettings.InviteCode inviteCode4 = this.inviteCode;
            AnalyticsTracker.inviteResolveFailed$default(str2, (inviteCode4 == null || (source = inviteCode4.getSource()) == null) ? str : source, null, null, 12, null);
        }
    }

    public final void configureUI(GuildInviteViewModel.ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        if (viewState instanceof GuildInviteViewModel.ViewState.Invalid) {
            configureUIFailure(null);
        } else if (viewState instanceof GuildInviteViewModel.ViewState.Loaded) {
            GuildInviteViewModel.ViewState.Loaded loaded = (GuildInviteViewModel.ViewState.Loaded) viewState;
            trackAndConsumeDynamicLinkCache(loaded.getInvite());
            configureLoadedUI(loaded);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        String str = null;
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
        StoreInviteSettings.InviteCode inviteCode = this.inviteCode;
        if (inviteCode != null) {
            str = inviteCode.getInviteCode();
        }
        analyticsTracker.impressionInviteAccept(str);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInvite$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AppActivity appActivity = WidgetGuildInvite.this.getAppActivity();
                if (appActivity != null) {
                    appActivity.onBackPressed();
                }
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        getViewModel().fetchInviteIfNotLoaded();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildInvite.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInvite$onViewBoundOrOnResume$1(this));
    }
}
