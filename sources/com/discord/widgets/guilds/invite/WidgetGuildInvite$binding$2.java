package com.discord.widgets.guilds.invite;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import com.discord.databinding.WidgetGuildInvitePageBinding;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventItemView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildInvitePageBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildInvitePageBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildInvite$binding$2 extends k implements Function1<View, WidgetGuildInvitePageBinding> {
    public static final WidgetGuildInvite$binding$2 INSTANCE = new WidgetGuildInvite$binding$2();

    public WidgetGuildInvite$binding$2() {
        super(1, WidgetGuildInvitePageBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildInvitePageBinding;", 0);
    }

    public final WidgetGuildInvitePageBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_invite_accept;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.guild_invite_accept);
        if (materialButton != null) {
            i = R.id.guild_invite_actions;
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.guild_invite_actions);
            if (linearLayout != null) {
                i = R.id.guild_invite_cancel;
                MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.guild_invite_cancel);
                if (materialButton2 != null) {
                    i = R.id.guild_invite_event_info;
                    GuildScheduledEventItemView guildScheduledEventItemView = (GuildScheduledEventItemView) view.findViewById(R.id.guild_invite_event_info);
                    if (guildScheduledEventItemView != null) {
                        i = R.id.guild_invite_info;
                        WidgetInviteInfo widgetInviteInfo = (WidgetInviteInfo) view.findViewById(R.id.guild_invite_info);
                        if (widgetInviteInfo != null) {
                            i = R.id.scroll_view;
                            NestedScrollView nestedScrollView = (NestedScrollView) view.findViewById(R.id.scroll_view);
                            if (nestedScrollView != null) {
                                i = R.id.toolbar;
                                AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.toolbar);
                                if (appBarLayout != null) {
                                    return new WidgetGuildInvitePageBinding((ConstraintLayout) view, materialButton, linearLayout, materialButton2, guildScheduledEventItemView, widgetInviteInfo, nestedScrollView, appBarLayout);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
