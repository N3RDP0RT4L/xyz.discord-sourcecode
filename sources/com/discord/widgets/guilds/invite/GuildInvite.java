package com.discord.widgets.guilds.invite;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.Guild;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.user.User;
import com.discord.models.domain.ModelInvite;
import com.discord.models.invite.InviteUtils;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0011\b\u0086\b\u0018\u0000 22\u00020\u0001:\u00012B_\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u000e\u0010\u0016\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\u000e\u0010\u0017\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n\u0012\u000e\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\f\u0012\u0006\u0010\u0019\u001a\u00020\u000e\u0012\u0006\u0010\u001a\u001a\u00020\u0006\u0012\u000e\u0010\u001b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0013¢\u0006\u0004\b0\u00101J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0018\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u0018\u0010\r\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\tJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0018\u0010\u0014\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\tJv\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\u0010\b\u0002\u0010\u0016\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u0010\b\u0002\u0010\u0017\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n2\u0010\b\u0002\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\f2\b\b\u0002\u0010\u0019\u001a\u00020\u000e2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\u0010\b\u0002\u0010\u001b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0013HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0004J\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010$\u001a\u00020\u000e2\b\u0010#\u001a\u0004\u0018\u00010\"HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\u0004R!\u0010\u0016\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\tR\u0019\u0010\u0019\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b\u0019\u0010\u0010R!\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b+\u0010\tR!\u0010\u0017\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010(\u001a\u0004\b,\u0010\tR!\u0010\u001b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010(\u001a\u0004\b-\u0010\tR\u0019\u0010\u001a\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010.\u001a\u0004\b/\u0010\u0012¨\u00063"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInvite;", "Ljava/io/Serializable;", "", "toLink", "()Ljava/lang/String;", "component1", "", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "Lcom/discord/primitives/ChannelId;", "component3", "Lcom/discord/primitives/UserId;", "component4", "", "component5", "()Z", "component6", "()J", "Lcom/discord/primitives/GuildScheduledEventId;", "component7", "inviteCode", "guildId", "channelId", "inviterId", "isStaticInvite", "expirationTimeMs", "guildScheduledEventId", "copy", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;ZJLjava/lang/Long;)Lcom/discord/widgets/guilds/invite/GuildInvite;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getInviteCode", "Ljava/lang/Long;", "getGuildId", "Z", "getInviterId", "getChannelId", "getGuildScheduledEventId", "J", "getExpirationTimeMs", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;ZJLjava/lang/Long;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildInvite implements Serializable {
    public static final Companion Companion = new Companion(null);
    private final Long channelId;
    private final long expirationTimeMs;
    private final Long guildId;
    private final Long guildScheduledEventId;
    private final String inviteCode;
    private final Long inviterId;
    private final boolean isStaticInvite;

    /* compiled from: GuildInvite.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInvite$Companion;", "", "Lcom/discord/models/domain/ModelInvite;", "invite", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "createFromModelInvite", "(Lcom/discord/models/domain/ModelInvite;)Lcom/discord/widgets/guilds/invite/GuildInvite;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final GuildInvite createFromModelInvite(ModelInvite modelInvite) {
            boolean z2;
            m.checkNotNullParameter(modelInvite, "invite");
            GuildScheduledEvent guildScheduledEvent = modelInvite.getGuildScheduledEvent();
            Long l = null;
            if ((guildScheduledEvent != null ? guildScheduledEvent.b() : null) != null) {
                GuildScheduledEvent guildScheduledEvent2 = modelInvite.getGuildScheduledEvent();
                Long b2 = guildScheduledEvent2 != null ? guildScheduledEvent2.b() : null;
                Channel channel = modelInvite.getChannel();
                z2 = m.areEqual(b2, channel != null ? Long.valueOf(channel.h()) : null);
            } else {
                z2 = true;
            }
            String str = modelInvite.code;
            m.checkNotNullExpressionValue(str, "invite.code");
            Guild guild = modelInvite.guild;
            Long valueOf = guild != null ? Long.valueOf(guild.r()) : null;
            Channel channel2 = modelInvite.getChannel();
            Long valueOf2 = channel2 != null ? Long.valueOf(channel2.h()) : null;
            User inviter = modelInvite.getInviter();
            Long valueOf3 = inviter != null ? Long.valueOf(inviter.i()) : null;
            boolean isStatic = modelInvite.isStatic();
            long expirationTime = modelInvite.getExpirationTime();
            if (z2) {
                GuildScheduledEvent guildScheduledEvent3 = modelInvite.getGuildScheduledEvent();
                if (guildScheduledEvent3 != null) {
                    l = Long.valueOf(guildScheduledEvent3.i());
                }
            } else if (z2) {
                throw new NoWhenBranchMatchedException();
            }
            return new GuildInvite(str, valueOf, valueOf2, valueOf3, isStatic, expirationTime, l);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public GuildInvite(String str, Long l, Long l2, Long l3, boolean z2, long j, Long l4) {
        m.checkNotNullParameter(str, "inviteCode");
        this.inviteCode = str;
        this.guildId = l;
        this.channelId = l2;
        this.inviterId = l3;
        this.isStaticInvite = z2;
        this.expirationTimeMs = j;
        this.guildScheduledEventId = l4;
    }

    public final String component1() {
        return this.inviteCode;
    }

    public final Long component2() {
        return this.guildId;
    }

    public final Long component3() {
        return this.channelId;
    }

    public final Long component4() {
        return this.inviterId;
    }

    public final boolean component5() {
        return this.isStaticInvite;
    }

    public final long component6() {
        return this.expirationTimeMs;
    }

    public final Long component7() {
        return this.guildScheduledEventId;
    }

    public final GuildInvite copy(String str, Long l, Long l2, Long l3, boolean z2, long j, Long l4) {
        m.checkNotNullParameter(str, "inviteCode");
        return new GuildInvite(str, l, l2, l3, z2, j, l4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildInvite)) {
            return false;
        }
        GuildInvite guildInvite = (GuildInvite) obj;
        return m.areEqual(this.inviteCode, guildInvite.inviteCode) && m.areEqual(this.guildId, guildInvite.guildId) && m.areEqual(this.channelId, guildInvite.channelId) && m.areEqual(this.inviterId, guildInvite.inviterId) && this.isStaticInvite == guildInvite.isStaticInvite && this.expirationTimeMs == guildInvite.expirationTimeMs && m.areEqual(this.guildScheduledEventId, guildInvite.guildScheduledEventId);
    }

    public final Long getChannelId() {
        return this.channelId;
    }

    public final long getExpirationTimeMs() {
        return this.expirationTimeMs;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final Long getGuildScheduledEventId() {
        return this.guildScheduledEventId;
    }

    public final String getInviteCode() {
        return this.inviteCode;
    }

    public final Long getInviterId() {
        return this.inviterId;
    }

    public int hashCode() {
        String str = this.inviteCode;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Long l = this.guildId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.inviterId;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        boolean z2 = this.isStaticInvite;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int a = (b.a(this.expirationTimeMs) + ((hashCode4 + i2) * 31)) * 31;
        Long l4 = this.guildScheduledEventId;
        if (l4 != null) {
            i = l4.hashCode();
        }
        return a + i;
    }

    public final boolean isStaticInvite() {
        return this.isStaticInvite;
    }

    public final String toLink() {
        return InviteUtils.INSTANCE.createLinkFromCode(this.inviteCode, this.guildScheduledEventId);
    }

    public String toString() {
        StringBuilder R = a.R("GuildInvite(inviteCode=");
        R.append(this.inviteCode);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", inviterId=");
        R.append(this.inviterId);
        R.append(", isStaticInvite=");
        R.append(this.isStaticInvite);
        R.append(", expirationTimeMs=");
        R.append(this.expirationTimeMs);
        R.append(", guildScheduledEventId=");
        return a.F(R, this.guildScheduledEventId, ")");
    }
}
