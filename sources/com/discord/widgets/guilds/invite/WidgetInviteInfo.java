package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.api.guild.GuildFeature;
import com.discord.api.user.User;
import com.discord.databinding.WidgetGuildInviteInfoViewBinding;
import com.discord.models.domain.ModelInvite;
import com.discord.models.user.CoreUser;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.error.Error;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.GuildView;
import com.discord.views.guilds.ServerMemberCount;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetInviteInfo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010%\u001a\u00020\u0002\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0004\u0012\b\b\u0002\u0010'\u001a\u00020\u0006¢\u0006\u0004\b(\u0010\nJ)\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u000f\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u000eJ\u0017\u0010\u0010\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0010\u0010\u000eJ\u0019\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0016\u0010\u0015J\u0017\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0017\u0010\u0015J\u001f\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0018\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u001c\u0010\u0015J\u0015\u0010\u001d\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u001d\u0010\u000eJ\u0017\u0010 \u001a\u00020\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001e¢\u0006\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006)"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetInviteInfo;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attributeSet", "", "defStyleAttr", "", "parseAttributeSet", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "Lcom/discord/models/domain/ModelInvite;", "model", "configureForDirectFriend", "(Lcom/discord/models/domain/ModelInvite;)V", "configureForGDM", "configureForGuild", "", "userName", "", "getIntroText", "(Ljava/lang/String;)Ljava/lang/CharSequence;", "getInviteTitleForDirectFriend", "getInviteSubtitleForDirectFriend", "channelName", "getIntroTextForGroup", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;", "guildName", "getFormattedGuildName", "configureUI", "Lcom/discord/utilities/error/Error;", "e", "configureUIFailure", "(Lcom/discord/utilities/error/Error;)V", "Lcom/discord/databinding/WidgetGuildInviteInfoViewBinding;", "binding", "Lcom/discord/databinding/WidgetGuildInviteInfoViewBinding;", "ctx", "attrSet", "attr", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetInviteInfo extends ConstraintLayout {
    private final WidgetGuildInviteInfoViewBinding binding;

    public WidgetInviteInfo(Context context) {
        this(context, null, 0, 6, null);
    }

    public WidgetInviteInfo(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ WidgetInviteInfo(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    private final void configureForDirectFriend(ModelInvite modelInvite) {
        GuildView guildView = this.binding.f2393b;
        User inviter = modelInvite.getInviter();
        String str = null;
        String forUser$default = IconUtils.getForUser$default(inviter != null ? new CoreUser(inviter) : null, true, null, 4, null);
        int i = GuildView.j;
        guildView.a(forUser$default, null);
        User inviter2 = modelInvite.getInviter();
        String r = inviter2 != null ? inviter2.r() : null;
        String str2 = "";
        if (r == null) {
            r = str2;
        }
        StringBuilder sb = new StringBuilder();
        User inviter3 = modelInvite.getInviter();
        String r2 = inviter3 != null ? inviter3.r() : null;
        if (r2 == null) {
            r2 = str2;
        }
        sb.append(r2);
        sb.append("#");
        User inviter4 = modelInvite.getInviter();
        if (inviter4 != null) {
            str = inviter4.f();
        }
        if (str != null) {
            str2 = str;
        }
        sb.append(str2);
        String sb2 = sb.toString();
        SimpleDraweeView simpleDraweeView = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.inviteAvatarSmall");
        simpleDraweeView.setVisibility(8);
        ServerMemberCount serverMemberCount = this.binding.d;
        m.checkNotNullExpressionValue(serverMemberCount, "binding.inviteMemberContainerLayout");
        serverMemberCount.setVisibility(8);
        TextView textView = this.binding.f;
        m.checkNotNullExpressionValue(textView, "binding.inviteTitle");
        textView.setText(getInviteTitleForDirectFriend(r));
        this.binding.f.setTextSize(2, 26.0f);
        TextView textView2 = this.binding.e;
        m.checkNotNullExpressionValue(textView2, "binding.inviteMessage");
        textView2.setText(getInviteSubtitleForDirectFriend(sb2));
        this.binding.e.setTextSize(2, 18.0f);
    }

    /* JADX WARN: Code restructure failed: missing block: B:29:0x006c, code lost:
        if ((r6.length() > 0) == true) goto L31;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureForGDM(com.discord.models.domain.ModelInvite r12) {
        /*
            r11 = this;
            com.discord.api.channel.Channel r0 = r12.getChannel()
            if (r0 == 0) goto Ld2
            java.lang.String r1 = "model.channel ?: return"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            com.discord.databinding.WidgetGuildInviteInfoViewBinding r1 = r11.binding
            com.facebook.drawee.view.SimpleDraweeView r2 = r1.c
            java.lang.String r1 = "binding.inviteAvatarSmall"
            d0.z.d.m.checkNotNullExpressionValue(r2, r1)
            com.discord.api.user.User r3 = r12.getInviter()
            r10 = 0
            if (r3 == 0) goto L22
            com.discord.models.user.CoreUser r4 = new com.discord.models.user.CoreUser
            r4.<init>(r3)
            r3 = r4
            goto L23
        L22:
            r3 = r10
        L23:
            r4 = 2131165291(0x7f07006b, float:1.7944795E38)
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 56
            r9 = 0
            com.discord.utilities.icon.IconUtils.setIcon$default(r2, r3, r4, r5, r6, r7, r8, r9)
            com.discord.api.user.User r2 = r12.getInviter()
            if (r2 == 0) goto L3a
            java.lang.String r2 = r2.r()
            goto L3b
        L3a:
            r2 = r10
        L3b:
            if (r2 == 0) goto L3e
            goto L40
        L3e:
            java.lang.String r2 = ""
        L40:
            java.lang.String r3 = com.discord.api.channel.ChannelUtils.c(r0)
            int r3 = r3.length()
            r4 = 1
            r5 = 0
            if (r3 <= 0) goto L4e
            r3 = 1
            goto L4f
        L4e:
            r3 = 0
        L4f:
            if (r3 == 0) goto L56
            java.lang.String r3 = com.discord.api.channel.ChannelUtils.c(r0)
            goto L57
        L56:
            r3 = r2
        L57:
            boolean r6 = com.discord.api.channel.ChannelUtils.w(r0)
            if (r6 == 0) goto L6f
            java.lang.String r6 = r0.g()
            if (r6 == 0) goto L6f
            int r6 = r6.length()
            if (r6 <= 0) goto L6b
            r6 = 1
            goto L6c
        L6b:
            r6 = 0
        L6c:
            if (r6 != r4) goto L6f
            goto L70
        L6f:
            r4 = 0
        L70:
            com.discord.databinding.WidgetGuildInviteInfoViewBinding r6 = r11.binding
            com.discord.views.GuildView r6 = r6.f2393b
            if (r4 == 0) goto L7c
            r7 = 2
            java.lang.String r7 = com.discord.utilities.icon.IconUtils.getForChannel$default(r0, r10, r7, r10)
            goto L86
        L7c:
            com.discord.utilities.icon.IconUtils r7 = com.discord.utilities.icon.IconUtils.INSTANCE
            long r8 = r0.h()
            java.lang.String r7 = r7.getDefaultForGroupDM(r8)
        L86:
            r6.a(r7, r10)
            com.discord.databinding.WidgetGuildInviteInfoViewBinding r6 = r11.binding
            com.facebook.drawee.view.SimpleDraweeView r6 = r6.c
            d0.z.d.m.checkNotNullExpressionValue(r6, r1)
            if (r4 == 0) goto L93
            goto L95
        L93:
            r5 = 8
        L95:
            r6.setVisibility(r5)
            com.discord.databinding.WidgetGuildInviteInfoViewBinding r1 = r11.binding
            android.widget.TextView r1 = r1.e
            java.lang.String r4 = "binding.inviteMessage"
            d0.z.d.m.checkNotNullExpressionValue(r1, r4)
            java.lang.CharSequence r3 = r11.getFormattedGuildName(r3)
            r1.setText(r3)
            com.discord.databinding.WidgetGuildInviteInfoViewBinding r1 = r11.binding
            android.widget.TextView r1 = r1.f
            java.lang.String r3 = "binding.inviteTitle"
            d0.z.d.m.checkNotNullExpressionValue(r1, r3)
            java.lang.String r0 = com.discord.api.channel.ChannelUtils.c(r0)
            java.lang.CharSequence r0 = r11.getIntroTextForGroup(r2, r0)
            r1.setText(r0)
            com.discord.databinding.WidgetGuildInviteInfoViewBinding r0 = r11.binding
            com.discord.views.guilds.ServerMemberCount r0 = r0.d
            r0.setOnline(r10)
            com.discord.databinding.WidgetGuildInviteInfoViewBinding r0 = r11.binding
            com.discord.views.guilds.ServerMemberCount r0 = r0.d
            int r12 = r12.getApproximateMemberCount()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r0.setMembers(r12)
        Ld2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.invite.WidgetInviteInfo.configureForGDM(com.discord.models.domain.ModelInvite):void");
    }

    private final void configureForGuild(ModelInvite modelInvite) {
        List<GuildFeature> m;
        List<GuildFeature> m2;
        int i = 0;
        boolean z2 = modelInvite.getInviter() != null && modelInvite.getApproximateMemberCount() < 200;
        Guild guild = modelInvite.guild;
        if (guild != null) {
            GuildView guildView = this.binding.f2393b;
            m.checkNotNullExpressionValue(guild, "it");
            guildView.a(IconUtils.getForGuild$default(new com.discord.models.guild.Guild(guild), null, true, null, 10, null), GuildUtilsKt.computeShortName(guild.x()));
        }
        String str = null;
        if (z2) {
            SimpleDraweeView simpleDraweeView = this.binding.c;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.inviteAvatarSmall");
            User inviter = modelInvite.getInviter();
            IconUtils.setIcon$default(simpleDraweeView, IconUtils.getForUser$default(inviter != null ? new CoreUser(inviter) : null, true, null, 4, null), (int) R.dimen.avatar_size_medium, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        }
        this.binding.d.setMembers(!z2 ? Integer.valueOf(modelInvite.getApproximateMemberCount()) : null);
        this.binding.d.setOnline(!z2 ? Integer.valueOf(modelInvite.getApproximatePresenceCount()) : null);
        SimpleDraweeView simpleDraweeView2 = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.inviteAvatarSmall");
        if (!z2) {
            i = 8;
        }
        simpleDraweeView2.setVisibility(i);
        TextView textView = this.binding.e;
        m.checkNotNullExpressionValue(textView, "binding.inviteMessage");
        Guild guild2 = modelInvite.guild;
        String x2 = guild2 != null ? guild2.x() : null;
        if (x2 == null) {
            x2 = "";
        }
        textView.setText(getFormattedGuildName(x2));
        Guild guild3 = modelInvite.guild;
        if (guild3 == null || (m2 = guild3.m()) == null || !m2.contains(GuildFeature.VERIFIED)) {
            Guild guild4 = modelInvite.guild;
            if (!(guild4 == null || (m = guild4.m()) == null || !m.contains(GuildFeature.PARTNERED))) {
                TextView textView2 = this.binding.e;
                m.checkNotNullExpressionValue(textView2, "binding.inviteMessage");
                ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView2, R.drawable.ic_partnered_badge, 0, 0, 0, 14, null);
            }
        } else {
            TextView textView3 = this.binding.e;
            m.checkNotNullExpressionValue(textView3, "binding.inviteMessage");
            ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView3, R.drawable.ic_verified_badge, 0, 0, 0, 14, null);
        }
        TextView textView4 = this.binding.f;
        m.checkNotNullExpressionValue(textView4, "binding.inviteTitle");
        User inviter2 = modelInvite.getInviter();
        if (inviter2 != null) {
            str = inviter2.r();
        }
        textView4.setText(getIntroText(str));
    }

    private final CharSequence getFormattedGuildName(String str) {
        CharSequence g;
        g = b.g(a.w("**", str, "**"), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
        return g;
    }

    private final CharSequence getIntroText(String str) {
        CharSequence d;
        CharSequence d2;
        if (str == null) {
            d2 = b.d(this, R.string.instant_invite_you_have_been_invited_to_join, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            return d2;
        }
        d = b.d(this, R.string.auth_message_invited_by, new Object[]{str}, (r4 & 4) != 0 ? b.c.j : null);
        return d;
    }

    private final CharSequence getIntroTextForGroup(String str, String str2) {
        CharSequence d;
        CharSequence d2;
        if (str2.length() > 0) {
            d2 = b.d(this, R.string.auth_message_invited_by, new Object[]{str}, (r4 & 4) != 0 ? b.c.j : null);
            return d2;
        }
        d = b.d(this, R.string.instant_invite_you_have_been_invited_to_join_group_dm, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        return d;
    }

    private final CharSequence getInviteSubtitleForDirectFriend(String str) {
        CharSequence d;
        d = b.d(this, R.string.instant_invite_direct_friend_description, new Object[]{str}, (r4 & 4) != 0 ? b.c.j : null);
        return d;
    }

    private final CharSequence getInviteTitleForDirectFriend(String str) {
        CharSequence d;
        d = b.d(this, R.string.instant_invite_you_have_been_invited_to_chat, new Object[]{str}, (r4 & 4) != 0 ? b.c.j : null);
        return d;
    }

    private final void parseAttributeSet(Context context, AttributeSet attributeSet, int i) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.WidgetInviteInfo, i, 0);
            m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…iteInfo, defStyleAttr, 0)");
            final int dimension = (int) obtainStyledAttributes.getDimension(0, 0.0f);
            final int dimension2 = (int) obtainStyledAttributes.getDimension(1, 0.0f);
            obtainStyledAttributes.recycle();
            post(new Runnable() { // from class: com.discord.widgets.guilds.invite.WidgetInviteInfo$parseAttributeSet$1
                @Override // java.lang.Runnable
                public final void run() {
                    WidgetGuildInviteInfoViewBinding widgetGuildInviteInfoViewBinding;
                    WidgetGuildInviteInfoViewBinding widgetGuildInviteInfoViewBinding2;
                    if (dimension != 0) {
                        widgetGuildInviteInfoViewBinding2 = WidgetInviteInfo.this.binding;
                        GuildView guildView = widgetGuildInviteInfoViewBinding2.f2393b;
                        m.checkNotNullExpressionValue(guildView, "binding.inviteAvatar");
                        ViewGroup.LayoutParams layoutParams = guildView.getLayoutParams();
                        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
                        int i2 = dimension;
                        layoutParams.height = i2;
                        layoutParams.width = i2;
                        guildView.setLayoutParams(layoutParams);
                    }
                    if (dimension2 != 0) {
                        widgetGuildInviteInfoViewBinding = WidgetInviteInfo.this.binding;
                        SimpleDraweeView simpleDraweeView = widgetGuildInviteInfoViewBinding.c;
                        m.checkNotNullExpressionValue(simpleDraweeView, "binding.inviteAvatarSmall");
                        ViewGroup.LayoutParams layoutParams2 = simpleDraweeView.getLayoutParams();
                        Objects.requireNonNull(layoutParams2, "null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
                        int i3 = dimension2;
                        layoutParams2.height = i3;
                        layoutParams2.width = i3;
                        simpleDraweeView.setLayoutParams(layoutParams2);
                    }
                }
            });
        }
    }

    public final void configureUI(ModelInvite modelInvite) {
        m.checkNotNullParameter(modelInvite, "model");
        this.binding.f2393b.b();
        if (modelInvite.guild == null && modelInvite.getChannel() == null && modelInvite.getInviter() != null) {
            configureForDirectFriend(modelInvite);
        } else if (modelInvite.guild == null) {
            configureForGDM(modelInvite);
        } else {
            configureForGuild(modelInvite);
        }
    }

    public final void configureUIFailure(Error error) {
        this.binding.f.setText(R.string.instant_invite_expired);
        this.binding.e.setText(R.string.invite_button_expired);
        if (error != null) {
            Error.Response response = error.getResponse();
            m.checkNotNullExpressionValue(response, "e.response");
            if (response.getCode() == 30001) {
                MeUser me2 = StoreStream.Companion.getUsers().getMe();
                TextView textView = this.binding.f;
                m.checkNotNullExpressionValue(textView, "binding.inviteTitle");
                b.m(textView, R.string.too_many_user_guilds_alert_description, new Object[0], new WidgetInviteInfo$configureUIFailure$1(me2));
                this.binding.e.setText(R.string.too_many_user_guilds_description);
                this.binding.e.setTextSize(0, getResources().getDimension(R.dimen.uikit_textsize_medium));
            }
        }
        TextView textView2 = this.binding.e;
        m.checkNotNullExpressionValue(textView2, "binding.inviteMessage");
        ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView2, 0, 0, 0, 0, 15, null);
        ServerMemberCount serverMemberCount = this.binding.d;
        m.checkNotNullExpressionValue(serverMemberCount, "binding.inviteMemberContainerLayout");
        serverMemberCount.setVisibility(8);
        GuildView guildView = this.binding.f2393b;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        int themedDrawableRes = DrawableCompat.getThemedDrawableRes(context, (int) R.attr.img_poop, (int) R.drawable.img_poop_dark);
        Objects.requireNonNull(guildView);
        MGImages mGImages = MGImages.INSTANCE;
        SimpleDraweeView simpleDraweeView = guildView.k.f115b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildAvatar");
        MGImages.setImage$default(mGImages, simpleDraweeView, themedDrawableRes, (MGImages.ChangeDetector) null, 4, (Object) null);
        TextView textView3 = guildView.k.c;
        m.checkNotNullExpressionValue(textView3, "binding.guildText");
        textView3.setVisibility(8);
        SimpleDraweeView simpleDraweeView2 = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.inviteAvatarSmall");
        simpleDraweeView2.setVisibility(8);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetInviteInfo(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "ctx");
        LayoutInflater.from(getContext()).inflate(R.layout.widget_guild_invite_info_view, this);
        int i2 = R.id.invite_avatar;
        GuildView guildView = (GuildView) findViewById(R.id.invite_avatar);
        if (guildView != null) {
            i2 = R.id.invite_avatar_small;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.invite_avatar_small);
            if (simpleDraweeView != null) {
                i2 = R.id.invite_member_container_layout;
                ServerMemberCount serverMemberCount = (ServerMemberCount) findViewById(R.id.invite_member_container_layout);
                if (serverMemberCount != null) {
                    i2 = R.id.invite_message;
                    TextView textView = (TextView) findViewById(R.id.invite_message);
                    if (textView != null) {
                        i2 = R.id.invite_title;
                        TextView textView2 = (TextView) findViewById(R.id.invite_title);
                        if (textView2 != null) {
                            i2 = R.id.title_layout;
                            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.title_layout);
                            if (linearLayout != null) {
                                WidgetGuildInviteInfoViewBinding widgetGuildInviteInfoViewBinding = new WidgetGuildInviteInfoViewBinding(this, guildView, simpleDraweeView, serverMemberCount, textView, textView2, linearLayout);
                                m.checkNotNullExpressionValue(widgetGuildInviteInfoViewBinding, "WidgetGuildInviteInfoVie…ater.from(context), this)");
                                this.binding = widgetGuildInviteInfoViewBinding;
                                Context context2 = getContext();
                                m.checkNotNullExpressionValue(context2, "context");
                                parseAttributeSet(context2, attributeSet, i);
                                setBackgroundResource(R.drawable.bg_guild_scheduled_event_list_item_primary);
                                int dpToPixels = DimenUtils.dpToPixels(24);
                                setPadding(dpToPixels, dpToPixels, dpToPixels, dpToPixels);
                                return;
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i2)));
    }
}
