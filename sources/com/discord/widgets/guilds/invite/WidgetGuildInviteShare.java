package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.api.channel.Channel;
import com.discord.api.guild.Guild;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetGuildInviteShareBinding;
import com.discord.models.domain.ModelInvite;
import com.discord.models.experiments.domain.Experiment;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.guilds.invite.InviteSuggestionItem;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputLayout;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetGuildInviteShare.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u0000 72\u00020\u0001:\u00017B\u0007¢\u0006\u0004\b6\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\f\u001a\u00020\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJ)\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\bR\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001d\u001a\u00020\u001c8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R\u001d\u0010)\u001a\u00020$8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u001c\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000b\u0010*R\u001d\u00100\u001a\u00020+8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R$\u00105\u001a\n 2*\u0004\u0018\u00010101*\u0004\u0018\u00010\u00108B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b3\u00104¨\u00068"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;", "viewState", "", "configureUI", "(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;)V", "initBottomSheet", "()V", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;", "bottomSheetBehavior", "initBottomSheetBehavior", "(Lcom/google/android/material/bottomsheet/BottomSheetBehavior;)V", "Lcom/discord/widgets/guilds/invite/InviteSuggestionItem;", "item", "Lcom/discord/models/domain/ModelInvite;", "invite", "sendInvite", "(Lcom/discord/widgets/guilds/invite/InviteSuggestionItem;Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelInvite;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/guilds/invite/PrivateChannelAdapter;", "adapter", "Lcom/discord/widgets/guilds/invite/PrivateChannelAdapter;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "", "hasTrackedSuggestionsViewed", "Z", "Lcom/discord/databinding/WidgetGuildInviteShareBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildInviteShareBinding;", "binding", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "viewModel", "", "kotlin.jvm.PlatformType", "getInviteLink", "(Lcom/discord/models/domain/ModelInvite;)Ljava/lang/String;", "inviteLink", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShare extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildInviteShare.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildInviteShareBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final String INTENT_INVITE_STORE_KEY = "INTENT_INVITE_STORE_KEY";
    public static final String INTENT_IS_NUX_FLOW = "INTENT_IS_NUX_FLOW";
    private PrivateChannelAdapter adapter;
    private BottomSheetBehavior<ViewInviteSettingsSheet> bottomSheetBehavior;
    private boolean hasTrackedSuggestionsViewed;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildInviteShare$binding$2.INSTANCE, null, 2, null);
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, new WidgetGuildInviteShare$loggingConfig$1(this), 3);

    /* compiled from: WidgetGuildInviteShare.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019Jm\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\t2\b\b\u0002\u0010\f\u001a\u00020\u000b2\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\r2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0011\u001a\u00020\u000fH\u0007¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u000f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u000f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;", "", "Landroid/content/Context;", "context", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "", "isNuxFlow", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "", "inviteStoreKey", "source", "", "launch", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;JLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V", WidgetGuildInviteShare.INTENT_INVITE_STORE_KEY, "Ljava/lang/String;", WidgetGuildInviteShare.INTENT_IS_NUX_FLOW, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, FragmentManager fragmentManager, long j, Long l, boolean z2, Long l2, String str, String str2, int i, Object obj) {
            companion.launch(context, fragmentManager, j, (i & 8) != 0 ? null : l, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? null : l2, (i & 64) != 0 ? null : str, str2);
        }

        public final void launch(Context context, FragmentManager fragmentManager, long j, Long l, boolean z2, Long l2, String str, String str2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(str2, "source");
            if (GuildInviteShareSheetFeatureFlag.Companion.getINSTANCE().isEnabled()) {
                WidgetGuildInviteShareSheet.Companion.show(fragmentManager, l, j, str2);
                return;
            }
            Intent intent = new Intent();
            intent.putExtra(WidgetGuildInviteShare.INTENT_IS_NUX_FLOW, z2);
            intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", l != null ? l.longValue() : 0L);
            intent.putExtra("com.discord.intent.ORIGIN_SOURCE", str2);
            intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_SCHEDULED_EVENT_ID", l2);
            intent.putExtra(WidgetGuildInviteShare.INTENT_INVITE_STORE_KEY, str);
            Observable j2 = Observable.j(StoreStream.Companion.getExperiments().observeUserExperiment("2020-01_mobile_invite_suggestion_compact", true), new InviteSuggestionsManager(null, null, null, null, null, 31, null).observeInviteSuggestions(), WidgetGuildInviteShare$Companion$launch$1.INSTANCE);
            m.checkNotNullExpressionValue(j2, "Observable.combineLatest…-> exp to inviteService }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout(ObservableExtensionsKt.computationLatest(j2), 50L, false), WidgetGuildInviteShare.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetGuildInviteShare$Companion$launch$2(str2, context, intent), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteShare$Companion$launch$3(str2, context, intent));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildInviteShare() {
        super(R.layout.widget_guild_invite_share);
        WidgetGuildInviteShare$viewModel$2 widgetGuildInviteShare$viewModel$2 = new WidgetGuildInviteShare$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGuildInviteShareViewModel.class), new WidgetGuildInviteShare$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildInviteShare$viewModel$2));
    }

    public static final /* synthetic */ PrivateChannelAdapter access$getAdapter$p(WidgetGuildInviteShare widgetGuildInviteShare) {
        PrivateChannelAdapter privateChannelAdapter = widgetGuildInviteShare.adapter;
        if (privateChannelAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        return privateChannelAdapter;
    }

    public static final /* synthetic */ BottomSheetBehavior access$getBottomSheetBehavior$p(WidgetGuildInviteShare widgetGuildInviteShare) {
        BottomSheetBehavior<ViewInviteSettingsSheet> bottomSheetBehavior = widgetGuildInviteShare.bottomSheetBehavior;
        if (bottomSheetBehavior == null) {
            m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        return bottomSheetBehavior;
    }

    public final void configureUI(WidgetGuildInviteShareViewModel.ViewState.Loaded loaded) {
        Guild guild;
        final WidgetInviteModel widgetInviteModel = loaded.getWidgetInviteModel();
        List<InviteSuggestionItem> inviteSuggestionItems = loaded.getInviteSuggestionItems();
        final ModelInvite invite = widgetInviteModel.getInvite();
        AppViewFlipper appViewFlipper = getBinding().j;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.guildInviteSuggestionsFlipper");
        boolean z2 = true;
        appViewFlipper.setDisplayedChild(!inviteSuggestionItems.isEmpty() ? 1 : 0);
        int i = 0;
        if (inviteSuggestionItems.size() == 1 && (u.firstOrNull((List<? extends Object>) inviteSuggestionItems) instanceof InviteSuggestionItem.SearchNoResultsItem)) {
            inviteSuggestionItems = null;
        }
        if (inviteSuggestionItems == null) {
            inviteSuggestionItems = n.emptyList();
        }
        if ((!inviteSuggestionItems.isEmpty()) && !this.hasTrackedSuggestionsViewed) {
            ModelInvite invite2 = widgetInviteModel.getInvite();
            long r = (invite2 == null || (guild = invite2.guild) == null) ? 0L : guild.r();
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            ArrayList<InviteSuggestionItem.ChannelItem> arrayList = new ArrayList();
            for (Object obj : inviteSuggestionItems) {
                if (obj instanceof InviteSuggestionItem.ChannelItem) {
                    arrayList.add(obj);
                }
            }
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
            for (InviteSuggestionItem.ChannelItem channelItem : arrayList) {
                arrayList2.add(channelItem.getChannel());
            }
            ArrayList<InviteSuggestionItem.UserItem> arrayList3 = new ArrayList();
            for (Object obj2 : inviteSuggestionItems) {
                if (obj2 instanceof InviteSuggestionItem.UserItem) {
                    arrayList3.add(obj2);
                }
            }
            ArrayList arrayList4 = new ArrayList(o.collectionSizeOrDefault(arrayList3, 10));
            for (InviteSuggestionItem.UserItem userItem : arrayList3) {
                arrayList4.add(userItem.getUser());
            }
            analyticsTracker.inviteSuggestionOpened(r, arrayList2, arrayList4);
            this.hasTrackedSuggestionsViewed = true;
        }
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.guildInviteEmptyResults");
        textView.setVisibility(inviteSuggestionItems.isEmpty() ? 0 : 8);
        RecyclerView recyclerView = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView, "binding.guildInviteSuggestionList");
        recyclerView.setVisibility(inviteSuggestionItems.isEmpty() ^ true ? 0 : 8);
        PrivateChannelAdapter privateChannelAdapter = this.adapter;
        if (privateChannelAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        privateChannelAdapter.setData(inviteSuggestionItems);
        PrivateChannelAdapter privateChannelAdapter2 = this.adapter;
        if (privateChannelAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        privateChannelAdapter2.setOnClick(new WidgetGuildInviteShare$configureUI$3(this, loaded, invite));
        getBinding().k.configureUi(loaded.getWidgetInviteModel());
        TextView textView2 = getBinding().d;
        m.checkNotNullExpressionValue(textView2, "binding.guildInviteLink");
        textView2.setText(getInviteLink(invite));
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShare$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildInviteUiHelperKt.copyLinkClick(a.x(view, "it", "it.context"), invite, WidgetGuildInviteShare.this.getMostRecentIntent());
            }
        });
        ModelInvite.Settings settings = widgetInviteModel.getSettings();
        if (settings == null || settings.getMaxAge() != 0) {
            z2 = false;
        }
        CheckedSetting checkedSetting = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting, "binding.guildInviteNeverExpire");
        checkedSetting.setChecked(z2);
        final Experiment defaultInviteExperiment = getViewModel().getDefaultInviteExperiment();
        CheckedSetting.i(getBinding().e, GuildInviteUiHelperKt.getInviteLinkText(requireContext(), defaultInviteExperiment), false, 2);
        getBinding().e.e(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShare$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildInviteShareBinding binding;
                WidgetGuildInviteShareViewModel viewModel;
                WidgetGuildInviteShareViewModel viewModel2;
                WidgetGuildInviteShareBinding binding2;
                int i2;
                binding = WidgetGuildInviteShare.this.getBinding();
                binding.e.toggle();
                ModelInvite.Settings settings2 = widgetInviteModel.getSettings();
                if (settings2 != null) {
                    viewModel2 = WidgetGuildInviteShare.this.getViewModel();
                    binding2 = WidgetGuildInviteShare.this.getBinding();
                    CheckedSetting checkedSetting2 = binding2.e;
                    m.checkNotNullExpressionValue(checkedSetting2, "binding.guildInviteNeverExpire");
                    if (checkedSetting2.isChecked()) {
                        i2 = 0;
                    } else {
                        Experiment experiment = defaultInviteExperiment;
                        i2 = (experiment == null || experiment.getBucket() != 1) ? 86400 : ModelInvite.Settings.SEVEN_DAYS;
                    }
                    ModelInvite.Settings mergeMaxAge = settings2.mergeMaxAge(i2);
                    m.checkNotNullExpressionValue(mergeMaxAge, "settings.mergeMaxAge(\n  …          }\n            )");
                    viewModel2.updateInviteSettings(mergeMaxAge);
                }
                Channel targetChannel = widgetInviteModel.getTargetChannel();
                if (targetChannel != null) {
                    long longValue = Long.valueOf(targetChannel.h()).longValue();
                    viewModel = WidgetGuildInviteShare.this.getViewModel();
                    viewModel.generateInviteLink(longValue);
                }
            }
        });
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShare$configureUI$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ModelInvite modelInvite = invite;
                if (modelInvite != null) {
                    GuildInviteUiHelperKt.shareLinkClick(WidgetGuildInviteShare.this.getContext(), modelInvite);
                }
            }
        });
        ImageView imageView = getBinding().f;
        m.checkNotNullExpressionValue(imageView, "binding.guildInviteSettingsEdit");
        imageView.setVisibility(loaded.getShowInviteSettings() ? 0 : 8);
        CheckedSetting checkedSetting2 = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.guildInviteNeverExpire");
        if (!loaded.getShowInviteSettings()) {
            i = 8;
        }
        checkedSetting2.setVisibility(i);
    }

    public final WidgetGuildInviteShareBinding getBinding() {
        return (WidgetGuildInviteShareBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final String getInviteLink(ModelInvite modelInvite) {
        if (modelInvite == null) {
            return BuildConfig.HOST_INVITE;
        }
        return modelInvite.toLink(getResources(), BuildConfig.HOST_INVITE);
    }

    public final WidgetGuildInviteShareViewModel getViewModel() {
        return (WidgetGuildInviteShareViewModel) this.viewModel$delegate.getValue();
    }

    private final void initBottomSheet() {
        getBinding().k.setOnGenerateLinkListener(new WidgetGuildInviteShare$initBottomSheet$1(this));
        getBinding().k.setViewModel(getViewModel());
    }

    private final void initBottomSheetBehavior(BottomSheetBehavior<ViewInviteSettingsSheet> bottomSheetBehavior) {
        bottomSheetBehavior.setState(5);
        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShare$initBottomSheetBehavior$1
            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onSlide(View view, float f) {
                m.checkNotNullParameter(view, "bottomSheet");
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onStateChanged(View view, int i) {
                WidgetGuildInviteShareBinding binding;
                WidgetGuildInviteShareBinding binding2;
                WidgetGuildInviteShareViewModel viewModel;
                WidgetGuildInviteShareBinding binding3;
                m.checkNotNullParameter(view, "bottomSheet");
                if (i == 5) {
                    viewModel = WidgetGuildInviteShare.this.getViewModel();
                    viewModel.refreshUi();
                    binding3 = WidgetGuildInviteShare.this.getBinding();
                    binding3.f.sendAccessibilityEvent(8);
                } else if (i == 3) {
                    binding2 = WidgetGuildInviteShare.this.getBinding();
                    binding2.k.sendAccessibilityEvent(8);
                    WidgetGuildInviteShare.this.getAppLogger().a(null);
                }
                binding = WidgetGuildInviteShare.this.getBinding();
                ViewExtensions.fadeBy$default(binding.f2396b, i != 5, 0L, 2, null);
            }
        });
        bottomSheetBehavior.setUpdateImportantForAccessibilityOnSiblings(true);
    }

    public static final void launch(Context context, FragmentManager fragmentManager, long j, Long l, boolean z2, Long l2, String str, String str2) {
        Companion.launch(context, fragmentManager, j, l, z2, l2, str, str2);
    }

    public final void sendInvite(InviteSuggestionItem inviteSuggestionItem, WidgetGuildInviteShareViewModel.ViewState.Loaded loaded, ModelInvite modelInvite) {
        if (inviteSuggestionItem instanceof InviteSuggestionItem.ChannelItem) {
            getViewModel().sendInviteToChannel(((InviteSuggestionItem.ChannelItem) inviteSuggestionItem).getChannel().h(), loaded, modelInvite);
        } else if (inviteSuggestionItem instanceof InviteSuggestionItem.UserItem) {
            getViewModel().sendInviteToUser(((InviteSuggestionItem.UserItem) inviteSuggestionItem).getUser().getId(), loaded, modelInvite);
        }
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        Window window;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView, "binding.guildInviteSuggestionList");
        this.adapter = (PrivateChannelAdapter) companion.configure(new PrivateChannelAdapter(recyclerView));
        final boolean booleanExtra = getMostRecentIntent().getBooleanExtra(INTENT_IS_NUX_FLOW, false);
        long longExtra = getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", 0L);
        if (longExtra != 0) {
            getViewModel().selectChannel(longExtra);
        }
        setActionBarDisplayHomeAsUpEnabled(true, booleanExtra ? Integer.valueOf(DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.ic_close_24dp, 0, 2, (Object) null)) : null, booleanExtra ? Integer.valueOf((int) R.string.close) : null);
        setActionBarTitle(R.string.invite_people);
        AppActivity appActivity = getAppActivity();
        if (!(appActivity == null || (window = appActivity.getWindow()) == null)) {
            window.setSoftInputMode(32);
        }
        AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShare$onViewBound$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                if (WidgetGuildInviteShare.access$getBottomSheetBehavior$p(WidgetGuildInviteShare.this).getState() != 5) {
                    WidgetGuildInviteShare.access$getBottomSheetBehavior$p(WidgetGuildInviteShare.this).setState(5);
                    return Boolean.TRUE;
                } else if (!booleanExtra) {
                    return Boolean.FALSE;
                } else {
                    j.c(WidgetGuildInviteShare.this.requireContext(), false, null, 6);
                    return Boolean.TRUE;
                }
            }
        }, 0, 2, null);
        TextInputLayout textInputLayout = getBinding().h;
        m.checkNotNullExpressionValue(textInputLayout, "binding.guildInviteShareSearch");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetGuildInviteShare$onViewBound$2(this));
        BottomSheetBehavior<ViewInviteSettingsSheet> from = BottomSheetBehavior.from(getBinding().k);
        m.checkNotNullExpressionValue(from, "BottomSheetBehavior.from…nviteSettingsBottomSheet)");
        this.bottomSheetBehavior = from;
        if (from == null) {
            m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        initBottomSheetBehavior(from);
        initBottomSheet();
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShare$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetGuildInviteShare.access$getBottomSheetBehavior$p(WidgetGuildInviteShare.this).setState(3);
            }
        });
        getBinding().f2396b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShare$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetGuildInviteShare.access$getBottomSheetBehavior$p(WidgetGuildInviteShare.this).setState(5);
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<WidgetGuildInviteShareViewModel.ViewState> J = getViewModel().observeViewState().J();
        m.checkNotNullExpressionValue(J, "viewModel.observeViewSta…  .onBackpressureBuffer()");
        Observable<R> F = J.x(WidgetGuildInviteShare$onViewBoundOrOnResume$$inlined$filterIs$1.INSTANCE).F(WidgetGuildInviteShare$onViewBoundOrOnResume$$inlined$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        PrivateChannelAdapter privateChannelAdapter = this.adapter;
        if (privateChannelAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle(F, this, privateChannelAdapter), WidgetGuildInviteShare.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteShare$onViewBoundOrOnResume$1(this));
    }
}
