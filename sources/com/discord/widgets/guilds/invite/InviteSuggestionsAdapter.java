package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.databinding.WidgetGuildInviteShareItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.user.User;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.widgets.guilds.invite.InviteSuggestionItemV2;
import com.discord.widgets.guilds.invite.InviteSuggestionsAdapter;
import com.discord.widgets.user.UserNameFormatterKt;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: InviteSuggestionsAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0016\u0017B\u000f\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR.\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/guilds/invite/InviteSuggestionItemV2;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function1;", "", "onClick", "Lkotlin/jvm/functions/Function1;", "getOnClick", "()Lkotlin/jvm/functions/Function1;", "setOnClick", "(Lkotlin/jvm/functions/Function1;)V", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "EmptySearchResultsViewHolder", "InviteSuggestionViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InviteSuggestionsAdapter extends MGRecyclerAdapterSimple<InviteSuggestionItemV2> {
    private Function1<? super InviteSuggestionItemV2, Unit> onClick = InviteSuggestionsAdapter$onClick$1.INSTANCE;

    /* compiled from: InviteSuggestionsAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter$EmptySearchResultsViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter;", "Lcom/discord/widgets/guilds/invite/InviteSuggestionItemV2;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EmptySearchResultsViewHolder extends MGRecyclerViewHolder<InviteSuggestionsAdapter, InviteSuggestionItemV2> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public EmptySearchResultsViewHolder(InviteSuggestionsAdapter inviteSuggestionsAdapter) {
            super((int) R.layout.guild_invite_empty_search_results_item, inviteSuggestionsAdapter);
            m.checkNotNullParameter(inviteSuggestionsAdapter, "adapter");
        }
    }

    /* compiled from: InviteSuggestionsAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0015\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter$InviteSuggestionViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter;", "Lcom/discord/widgets/guilds/invite/InviteSuggestionItemV2;", "Lcom/discord/models/user/User;", "user", "", "configureItemForUser", "(Lcom/discord/models/user/User;)V", "Lcom/discord/api/channel/Channel;", "channel", "configureItemForChannel", "(Lcom/discord/api/channel/Channel;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "onConfigure", "(ILcom/discord/widgets/guilds/invite/InviteSuggestionItemV2;)V", "Lcom/discord/databinding/WidgetGuildInviteShareItemBinding;", "binding", "Lcom/discord/databinding/WidgetGuildInviteShareItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class InviteSuggestionViewHolder extends MGRecyclerViewHolder<InviteSuggestionsAdapter, InviteSuggestionItemV2> {
        private final WidgetGuildInviteShareItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public InviteSuggestionViewHolder(InviteSuggestionsAdapter inviteSuggestionsAdapter) {
            super((int) R.layout.widget_guild_invite_share_item, inviteSuggestionsAdapter);
            m.checkNotNullParameter(inviteSuggestionsAdapter, "adapter");
            WidgetGuildInviteShareItemBinding a = WidgetGuildInviteShareItemBinding.a(this.itemView);
            m.checkNotNullExpressionValue(a, "WidgetGuildInviteShareItemBinding.bind(itemView)");
            this.binding = a;
        }

        public static final /* synthetic */ InviteSuggestionsAdapter access$getAdapter$p(InviteSuggestionViewHolder inviteSuggestionViewHolder) {
            return (InviteSuggestionsAdapter) inviteSuggestionViewHolder.adapter;
        }

        private final void configureItemForChannel(Channel channel) {
            SimpleDraweeView simpleDraweeView = this.binding.f2399b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemIconIv");
            IconUtils.setIcon$default(simpleDraweeView, channel, 0, (MGImages.ChangeDetector) null, 12, (Object) null);
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.itemNameTv");
            textView.setText(ChannelUtils.c(channel));
        }

        private final void configureItemForUser(User user) {
            SimpleDraweeView simpleDraweeView = this.binding.f2399b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemIconIv");
            IconUtils.setIcon$default(simpleDraweeView, user, 0, null, null, null, 60, null);
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.itemNameTv");
            textView.setText(UserNameFormatterKt.getSpannableForUserNameWithDiscrim(user, null, ((InviteSuggestionsAdapter) this.adapter).getContext(), R.attr.colorHeaderPrimary, R.attr.font_primary_semibold, R.integer.uikit_textsize_large_sp, R.attr.colorTextMuted, R.attr.font_primary_normal, R.integer.uikit_textsize_large_sp));
        }

        public void onConfigure(int i, final InviteSuggestionItemV2 inviteSuggestionItemV2) {
            m.checkNotNullParameter(inviteSuggestionItemV2, "data");
            super.onConfigure(i, (int) inviteSuggestionItemV2);
            boolean z2 = inviteSuggestionItemV2 instanceof InviteSuggestionItemV2.ChannelItem;
            if (z2) {
                InviteSuggestionItemV2.ChannelItem channelItem = (InviteSuggestionItemV2.ChannelItem) inviteSuggestionItemV2;
                User a = ChannelUtils.a(channelItem.getChannel());
                if (a != null) {
                    configureItemForUser(a);
                } else {
                    configureItemForChannel(channelItem.getChannel());
                }
            } else if (inviteSuggestionItemV2 instanceof InviteSuggestionItemV2.UserItem) {
                configureItemForUser(((InviteSuggestionItemV2.UserItem) inviteSuggestionItemV2).getUser());
            }
            if (z2 || (inviteSuggestionItemV2 instanceof InviteSuggestionItemV2.UserItem)) {
                MaterialButton materialButton = this.binding.e;
                m.checkNotNullExpressionValue(materialButton, "binding.itemSent");
                int i2 = 0;
                materialButton.setVisibility(inviteSuggestionItemV2.hasSentInvite() ? 0 : 8);
                MaterialButton materialButton2 = this.binding.c;
                m.checkNotNullExpressionValue(materialButton2, "binding.itemInviteBtn");
                if (!(!inviteSuggestionItemV2.hasSentInvite())) {
                    i2 = 8;
                }
                materialButton2.setVisibility(i2);
                this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.InviteSuggestionsAdapter$InviteSuggestionViewHolder$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        InviteSuggestionsAdapter.InviteSuggestionViewHolder.access$getAdapter$p(InviteSuggestionsAdapter.InviteSuggestionViewHolder.this).getOnClick().invoke(inviteSuggestionItemV2);
                    }
                });
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InviteSuggestionsAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
    }

    public final Function1<InviteSuggestionItemV2, Unit> getOnClick() {
        return this.onClick;
    }

    public final void setOnClick(Function1<? super InviteSuggestionItemV2, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClick = function1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<InviteSuggestionsAdapter, InviteSuggestionItemV2> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 1) {
            return new EmptySearchResultsViewHolder(this);
        }
        if (i == 2 || i == 3) {
            return new InviteSuggestionViewHolder(this);
        }
        throw invalidViewTypeException(i);
    }
}
