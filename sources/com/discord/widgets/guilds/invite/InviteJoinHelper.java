package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.view.PointerIconCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleOwnerKt;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.app.AppFragment;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreStream;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.coroutines.AppCoroutineScopeKt;
import com.discord.utilities.error.Error;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet;
import com.discord.widgets.stage.StageChannelJoinHelper;
import com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Job;
/* compiled from: InviteJoinHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\fJ´\u0001\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\r\u001a\u00020\u00022\n\u0010\u000f\u001a\u0006\u0012\u0002\b\u00030\u000e2\u0006\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u00112&\b\u0002\u0010\u0016\u001a \b\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0015\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00132$\b\u0002\u0010\u0017\u001a\u001e\b\u0001\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0015\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00132$\b\u0002\u0010\u0018\u001a\u001e\b\u0001\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0015\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00132\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0011ø\u0001\u0000¢\u0006\u0004\b\u001b\u0010\u001c\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteJoinHelper;", "", "Lcom/discord/models/domain/ModelInvite;", "modelInvite", "", "navigateToGuild", "(Lcom/discord/models/domain/ModelInvite;)V", "Landroid/content/Context;", "context", "Lcom/discord/app/AppFragment;", "appFragment", "navigateToChannel", "(Landroid/content/Context;Lcom/discord/app/AppFragment;Lcom/discord/models/domain/ModelInvite;)V", "invite", "Ljava/lang/Class;", "javaClass", "fragment", "", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Lkotlin/Function2;", "Lcom/discord/utilities/error/Error;", "Lkotlin/coroutines/Continuation;", "onInvitePostError", "onInvitePostSuccess", "onInviteFlowFinished", "captchaKey", "Lkotlinx/coroutines/Job;", "joinViaInvite", "(Lcom/discord/models/domain/ModelInvite;Ljava/lang/Class;Lcom/discord/app/AppFragment;Ljava/lang/String;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Ljava/lang/String;)Lkotlinx/coroutines/Job;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InviteJoinHelper {
    public static final InviteJoinHelper INSTANCE = new InviteJoinHelper();

    private InviteJoinHelper() {
    }

    public static /* synthetic */ Job joinViaInvite$default(InviteJoinHelper inviteJoinHelper, ModelInvite modelInvite, Class cls, AppFragment appFragment, String str, Function2 function2, Function2 function22, Function2 function23, String str2, int i, Object obj) {
        return inviteJoinHelper.joinViaInvite(modelInvite, cls, appFragment, str, (i & 16) != 0 ? new InviteJoinHelper$joinViaInvite$1(null) : function2, (i & 32) != 0 ? new InviteJoinHelper$joinViaInvite$2(null) : function22, (i & 64) != 0 ? new InviteJoinHelper$joinViaInvite$3(null) : function23, (i & 128) != 0 ? null : str2);
    }

    public final void navigateToChannel(Context context, AppFragment appFragment, ModelInvite modelInvite) {
        Channel channel = modelInvite.getChannel();
        if (channel != null) {
            ChannelSelector.Companion.getInstance().findAndSet(context, channel.h());
            m.checkNotNullExpressionValue(channel, "channel");
            if (ChannelUtils.E(channel)) {
                if (TextInVoiceFeatureFlag.Companion.getINSTANCE().isEnabled(Long.valueOf(channel.f()))) {
                    WidgetCallPreviewFullscreen.Companion.launch$default(WidgetCallPreviewFullscreen.Companion, context, channel.h(), null, 4, null);
                    return;
                }
                WidgetVoiceBottomSheet.Companion companion = WidgetVoiceBottomSheet.Companion;
                FragmentManager parentFragmentManager = appFragment.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "appFragment.parentFragmentManager");
                companion.show(parentFragmentManager, channel.h(), true, WidgetVoiceBottomSheet.FeatureContext.HOME);
            } else if (ChannelUtils.z(channel)) {
                StageChannelJoinHelper stageChannelJoinHelper = StageChannelJoinHelper.INSTANCE;
                Context requireContext = appFragment.requireContext();
                FragmentManager parentFragmentManager2 = appFragment.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager2, "appFragment.parentFragmentManager");
                StageChannelJoinHelper.connectToStage$default(stageChannelJoinHelper, requireContext, parentFragmentManager2, channel.h(), false, false, null, null, null, null, null, PointerIconCompat.TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW, null);
            }
        }
    }

    public final void navigateToGuild(ModelInvite modelInvite) {
        Guild guild = modelInvite.guild;
        if (guild != null) {
            StoreStream.Companion.getGuildSelected().set(guild.r());
        }
        GuildScheduledEvent guildScheduledEvent = modelInvite.getGuildScheduledEvent();
        if (guildScheduledEvent != null) {
            WidgetGuildScheduledEventDetailsBottomSheet.Companion.enqueue(guildScheduledEvent.i());
        }
    }

    public final Job joinViaInvite(ModelInvite modelInvite, Class<?> cls, AppFragment appFragment, String str, Function2<? super Error, ? super Continuation<? super Unit>, ? extends Object> function2, Function2<? super ModelInvite, ? super Continuation<? super Unit>, ? extends Object> function22, Function2<? super ModelInvite, ? super Continuation<? super Unit>, ? extends Object> function23, String str2) {
        m.checkNotNullParameter(modelInvite, "invite");
        m.checkNotNullParameter(cls, "javaClass");
        m.checkNotNullParameter(appFragment, "fragment");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        m.checkNotNullParameter(function2, "onInvitePostError");
        m.checkNotNullParameter(function22, "onInvitePostSuccess");
        m.checkNotNullParameter(function23, "onInviteFlowFinished");
        LifecycleOwner viewLifecycleOwner = appFragment.getViewLifecycleOwner();
        m.checkNotNullExpressionValue(viewLifecycleOwner, "fragment.viewLifecycleOwner");
        return AppCoroutineScopeKt.appLaunch$default(LifecycleOwnerKt.getLifecycleScope(viewLifecycleOwner), cls, (CoroutineContext) null, (CoroutineStart) null, new InviteJoinHelper$joinViaInvite$4(appFragment, modelInvite, str, function22, str2, cls, function2, function23, null), 6, (Object) null);
    }
}
