package com.discord.widgets.guilds.invite;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import b.a.d.m;
import b.a.k.b;
import com.discord.BuildConfig;
import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelInvite;
import com.discord.models.experiments.domain.Experiment;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.duration.DurationUtilsKt;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildInviteUiHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a'\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b\u001a'\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u0007\u0010\u000b\u001a/\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u0007\u0010\u0010\u001a\u001f\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0011\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0012\u0010\u0013\u001a\u001f\u0010\u0014\u001a\u00020\u00062\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015\u001a)\u0010\u0014\u001a\u00020\u00062\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u0006\u0010\r\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\u0004\b\u0014\u0010\u0016\u001a%\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001c\u0010\u001d\u001a\u001d\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u001c\u0010\u001e\u001a\u001f\u0010!\u001a\u00020\u001b2\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010 \u001a\u0004\u0018\u00010\u001f¢\u0006\u0004\b!\u0010\"\u001a\u001f\u0010#\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0011\u001a\u00020\tH\u0002¢\u0006\u0004\b#\u0010\u0013\u001a!\u0010'\u001a\u00020\t2\b\u0010$\u001a\u0004\u0018\u00010\u00022\u0006\u0010&\u001a\u00020%H\u0002¢\u0006\u0004\b'\u0010(¨\u0006)"}, d2 = {"Landroid/content/Context;", "context", "Lcom/discord/models/domain/ModelInvite;", "invite", "Landroid/content/Intent;", "mostRecentIntent", "", "copyLinkClick", "(Landroid/content/Context;Lcom/discord/models/domain/ModelInvite;Landroid/content/Intent;)V", "", "analyticsSource", "(Landroid/content/Context;Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;)V", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "guildInvite", "Lcom/discord/api/channel/Channel;", "channel", "(Landroid/content/Context;Lcom/discord/widgets/guilds/invite/GuildInvite;Lcom/discord/api/channel/Channel;Ljava/lang/String;)V", "inviteLink", "copyLink", "(Landroid/content/Context;Ljava/lang/String;)V", "shareLinkClick", "(Landroid/content/Context;Lcom/discord/models/domain/ModelInvite;)V", "(Landroid/content/Context;Lcom/discord/widgets/guilds/invite/GuildInvite;Lcom/discord/api/channel/Channel;)V", "", "expirationDurationMs", "", "maxUses", "", "getInviteSettingsText", "(Landroid/content/Context;JI)Ljava/lang/CharSequence;", "(Landroid/content/Context;J)Ljava/lang/CharSequence;", "Lcom/discord/models/experiments/domain/Experiment;", "experiment", "getInviteLinkText", "(Landroid/content/Context;Lcom/discord/models/experiments/domain/Experiment;)Ljava/lang/CharSequence;", "shareLink", "inviteModel", "Landroid/content/res/Resources;", "resources", "getInviteLink", "(Lcom/discord/models/domain/ModelInvite;Landroid/content/res/Resources;)Ljava/lang/String;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildInviteUiHelperKt {
    private static final void copyLink(Context context, String str) {
        m.a(context, str, R.string.invite_link_copied);
    }

    public static final void copyLinkClick(Context context, ModelInvite modelInvite, Intent intent) {
        d0.z.d.m.checkNotNullParameter(context, "context");
        d0.z.d.m.checkNotNullParameter(intent, "mostRecentIntent");
        String stringExtra = intent.getStringExtra("com.discord.intent.ORIGIN_SOURCE");
        if (stringExtra == null) {
            stringExtra = "";
        }
        copyLinkClick(context, modelInvite, stringExtra);
    }

    private static final String getInviteLink(ModelInvite modelInvite, Resources resources) {
        if (modelInvite == null) {
            return BuildConfig.HOST_INVITE;
        }
        String link = modelInvite.toLink(resources, BuildConfig.HOST_INVITE);
        d0.z.d.m.checkNotNullExpressionValue(link, "inviteModel.toLink(resou… BuildConfig.HOST_INVITE)");
        return link;
    }

    public static final CharSequence getInviteLinkText(Context context, Experiment experiment) {
        CharSequence charSequence;
        CharSequence b2;
        d0.z.d.m.checkNotNullParameter(context, "context");
        if (experiment == null || experiment.getBucket() != 1) {
            charSequence = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.duration_days_days, 1, 1);
        } else {
            charSequence = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.duration_days_days, 7, 7);
        }
        b2 = b.b(context, R.string.invite_links_expire_after_default, new Object[]{charSequence}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    public static final CharSequence getInviteSettingsText(Context context, long j, int i) {
        Object obj;
        CharSequence b2;
        CharSequence b3;
        d0.z.d.m.checkNotNullParameter(context, "context");
        if (i == 0) {
            obj = context.getString(R.string.max_uses_description_unlimited_uses);
            d0.z.d.m.checkNotNullExpressionValue(obj, "context.getString(R.stri…scription_unlimited_uses)");
        } else {
            Resources resources = context.getResources();
            d0.z.d.m.checkNotNullExpressionValue(resources, "context.resources");
            obj = StringResourceUtilsKt.getQuantityString(resources, context, (int) R.plurals.max_uses_description_mobile_maxUses, i, Integer.valueOf(i));
        }
        if (j <= 0) {
            b3 = b.b(context, R.string.invite_settings_description_no_expiration, new Object[]{context.getString(R.string.max_age_never_description_mobile), obj}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        }
        b2 = b.b(context, R.string.invite_settings_expired_description, new Object[]{DurationUtilsKt.humanizeDurationRounded(context, j), obj}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    private static final void shareLink(Context context, String str) {
        String string = context.getResources().getString(R.string.tip_instant_invite_title3);
        d0.z.d.m.checkNotNullExpressionValue(string, "context.resources.getStr…ip_instant_invite_title3)");
        IntentUtils.performChooserSendIntent(context, str, string);
    }

    public static final void shareLinkClick(Context context, ModelInvite modelInvite) {
        d0.z.d.m.checkNotNullParameter(modelInvite, "invite");
        AnalyticsTracker.inviteShareClicked(modelInvite);
        if (context != null) {
            Resources resources = context.getResources();
            d0.z.d.m.checkNotNullExpressionValue(resources, "context.resources");
            shareLink(context, getInviteLink(modelInvite, resources));
        }
    }

    public static final void copyLinkClick(Context context, ModelInvite modelInvite, String str) {
        d0.z.d.m.checkNotNullParameter(context, "context");
        d0.z.d.m.checkNotNullParameter(str, "analyticsSource");
        AnalyticsTracker.inviteCopied(modelInvite, str);
        Resources resources = context.getResources();
        d0.z.d.m.checkNotNullExpressionValue(resources, "context.resources");
        copyLink(context, getInviteLink(modelInvite, resources));
    }

    public static final void shareLinkClick(Context context, GuildInvite guildInvite, Channel channel) {
        d0.z.d.m.checkNotNullParameter(guildInvite, "guildInvite");
        AnalyticsTracker.INSTANCE.inviteShareClicked(guildInvite, channel);
        if (context != null) {
            shareLink(context, guildInvite.toLink());
        }
    }

    public static final void copyLinkClick(Context context, GuildInvite guildInvite, Channel channel, String str) {
        d0.z.d.m.checkNotNullParameter(context, "context");
        d0.z.d.m.checkNotNullParameter(guildInvite, "guildInvite");
        d0.z.d.m.checkNotNullParameter(str, "analyticsSource");
        AnalyticsTracker.INSTANCE.inviteCopied(guildInvite, channel, str);
        copyLink(context, guildInvite.toLink());
    }

    public static final CharSequence getInviteSettingsText(Context context, long j) {
        CharSequence b2;
        CharSequence b3;
        d0.z.d.m.checkNotNullParameter(context, "context");
        if (j <= 0) {
            b3 = b.b(context, R.string.max_age_never_description_mobile, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        }
        b2 = b.b(context, R.string.invite_expired_subtext_mobile, new Object[]{DurationUtilsKt.humanizeDurationRounded(context, j)}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }
}
