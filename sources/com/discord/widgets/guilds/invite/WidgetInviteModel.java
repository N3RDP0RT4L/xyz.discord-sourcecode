package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.stageinstance.StageInstance;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.utilities.guilds.GuildUtilsKt;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetInviteModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0015\b\u0086\b\u0018\u0000 92\u00020\u0001:\u00019Bg\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\b\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u000b\u0012\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f\u0012\u0006\u0010\u001d\u001a\u00020\u0012\u0012\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u000b¢\u0006\u0004\b7\u00108J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0016\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0011J\u0010\u0010\u0016\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u0016\u0010\rJ|\u0010 \u001a\u00020\u00002\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u001a\u001a\u00020\u000b2\b\b\u0002\u0010\u001b\u001a\u00020\u000b2\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f2\b\b\u0002\u0010\u001d\u001a\u00020\u00122\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f2\b\b\u0002\u0010\u001f\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u001a\u0010)\u001a\u00020\u000b2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b)\u0010*R\u0019\u0010\u001d\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010+\u001a\u0004\b,\u0010\u0014R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010-\u001a\u0004\b.\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010/\u001a\u0004\b0\u0010\u0007R\u001f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00101\u001a\u0004\b2\u0010\u0011R\u001f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b3\u0010\u0011R\u0019\u0010\u001b\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00104\u001a\u0004\b\u001b\u0010\rR\u0019\u0010\u001a\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00104\u001a\u0004\b\u001a\u0010\rR\u0019\u0010\u001f\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00104\u001a\u0004\b\u001f\u0010\rR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00105\u001a\u0004\b6\u0010\n¨\u0006:"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/domain/ModelInvite$Settings;", "component2", "()Lcom/discord/models/domain/ModelInvite$Settings;", "Lcom/discord/models/domain/ModelInvite;", "component3", "()Lcom/discord/models/domain/ModelInvite;", "", "component4", "()Z", "component5", "", "component6", "()Ljava/util/List;", "Lcom/discord/models/user/MeUser;", "component7", "()Lcom/discord/models/user/MeUser;", "component8", "component9", "targetChannel", "settings", "invite", "isGeneratingInvite", "isValidInvite", "invitableChannels", "me", "dms", "isInviteFromStore", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/domain/ModelInvite$Settings;Lcom/discord/models/domain/ModelInvite;ZZLjava/util/List;Lcom/discord/models/user/MeUser;Ljava/util/List;Z)Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/user/MeUser;", "getMe", "Lcom/discord/api/channel/Channel;", "getTargetChannel", "Lcom/discord/models/domain/ModelInvite$Settings;", "getSettings", "Ljava/util/List;", "getInvitableChannels", "getDms", "Z", "Lcom/discord/models/domain/ModelInvite;", "getInvite", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/domain/ModelInvite$Settings;Lcom/discord/models/domain/ModelInvite;ZZLjava/util/List;Lcom/discord/models/user/MeUser;Ljava/util/List;Z)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetInviteModel {
    public static final Companion Companion = new Companion(null);
    private final List<Channel> dms;
    private final List<Channel> invitableChannels;
    private final ModelInvite invite;
    private final boolean isGeneratingInvite;
    private final boolean isInviteFromStore;
    private final boolean isValidInvite;

    /* renamed from: me  reason: collision with root package name */
    private final MeUser f2829me;
    private final ModelInvite.Settings settings;
    private final Channel targetChannel;

    /* compiled from: WidgetInviteModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJC\u0010\u000b\u001a\u0004\u0018\u00010\n2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0016\u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\b0\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u008d\u0001\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u000e\u001a\u00020\r2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000f0\u00072\u0006\u0010\u0012\u001a\u00020\u00112\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010\u0014\u001a\u00020\u00132\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00152\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0016\u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\b0\u00072\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\b\u0010\u0019\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetInviteModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "selectedChannelId", "Lcom/discord/models/guild/Guild;", "guild", "", "Lcom/discord/api/stageinstance/StageInstance;", "guildStageInstances", "Lcom/discord/models/domain/ModelInvite;", "tryGetStaticInvite", "(Ljava/lang/Long;Lcom/discord/models/guild/Guild;Ljava/util/Map;)Lcom/discord/models/domain/ModelInvite;", "Lcom/discord/models/domain/ModelInvite$Settings;", "settings", "Lcom/discord/api/channel/Channel;", "invitableChannels", "Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;", "inviteGenerationState", "Lcom/discord/models/user/MeUser;", "me", "", "dms", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "existingInviteFromStore", "Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "create", "(Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/Map;Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;Ljava/lang/Long;Lcom/discord/models/user/MeUser;Ljava/util/List;Lcom/discord/models/guild/Guild;Ljava/util/Map;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/models/domain/ModelInvite;)Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final ModelInvite tryGetStaticInvite(Long l, Guild guild, Map<Long, StageInstance> map) {
            String d;
            if (guild == null) {
                return null;
            }
            StageInstance stageInstance = map.get(l);
            if (stageInstance != null && (d = stageInstance.d()) != null) {
                return ModelInvite.createForStaticUrl(d, GuildUtilsKt.createApiGuild(guild));
            }
            String vanityUrlCode = guild.getVanityUrlCode();
            if (vanityUrlCode != null) {
                return ModelInvite.createForStaticUrl(vanityUrlCode, GuildUtilsKt.createApiGuild(guild));
            }
            return null;
        }

        /* JADX WARN: Removed duplicated region for block: B:40:0x00d4  */
        /* JADX WARN: Removed duplicated region for block: B:41:0x00d6  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final com.discord.widgets.guilds.invite.WidgetInviteModel create(com.discord.models.domain.ModelInvite.Settings r19, java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r20, com.discord.widgets.guilds.invite.InviteGenerator.InviteGenerationState r21, java.lang.Long r22, com.discord.models.user.MeUser r23, java.util.List<com.discord.api.channel.Channel> r24, com.discord.models.guild.Guild r25, java.util.Map<java.lang.Long, com.discord.api.stageinstance.StageInstance> r26, com.discord.api.guildscheduledevent.GuildScheduledEvent r27, com.discord.models.domain.ModelInvite r28) {
            /*
                Method dump skipped, instructions count: 228
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.invite.WidgetInviteModel.Companion.create(com.discord.models.domain.ModelInvite$Settings, java.util.Map, com.discord.widgets.guilds.invite.InviteGenerator$InviteGenerationState, java.lang.Long, com.discord.models.user.MeUser, java.util.List, com.discord.models.guild.Guild, java.util.Map, com.discord.api.guildscheduledevent.GuildScheduledEvent, com.discord.models.domain.ModelInvite):com.discord.widgets.guilds.invite.WidgetInviteModel");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetInviteModel(Channel channel, ModelInvite.Settings settings, ModelInvite modelInvite, boolean z2, boolean z3, List<Channel> list, MeUser meUser, List<Channel> list2, boolean z4) {
        m.checkNotNullParameter(list, "invitableChannels");
        m.checkNotNullParameter(meUser, "me");
        m.checkNotNullParameter(list2, "dms");
        this.targetChannel = channel;
        this.settings = settings;
        this.invite = modelInvite;
        this.isGeneratingInvite = z2;
        this.isValidInvite = z3;
        this.invitableChannels = list;
        this.f2829me = meUser;
        this.dms = list2;
        this.isInviteFromStore = z4;
    }

    public final Channel component1() {
        return this.targetChannel;
    }

    public final ModelInvite.Settings component2() {
        return this.settings;
    }

    public final ModelInvite component3() {
        return this.invite;
    }

    public final boolean component4() {
        return this.isGeneratingInvite;
    }

    public final boolean component5() {
        return this.isValidInvite;
    }

    public final List<Channel> component6() {
        return this.invitableChannels;
    }

    public final MeUser component7() {
        return this.f2829me;
    }

    public final List<Channel> component8() {
        return this.dms;
    }

    public final boolean component9() {
        return this.isInviteFromStore;
    }

    public final WidgetInviteModel copy(Channel channel, ModelInvite.Settings settings, ModelInvite modelInvite, boolean z2, boolean z3, List<Channel> list, MeUser meUser, List<Channel> list2, boolean z4) {
        m.checkNotNullParameter(list, "invitableChannels");
        m.checkNotNullParameter(meUser, "me");
        m.checkNotNullParameter(list2, "dms");
        return new WidgetInviteModel(channel, settings, modelInvite, z2, z3, list, meUser, list2, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetInviteModel)) {
            return false;
        }
        WidgetInviteModel widgetInviteModel = (WidgetInviteModel) obj;
        return m.areEqual(this.targetChannel, widgetInviteModel.targetChannel) && m.areEqual(this.settings, widgetInviteModel.settings) && m.areEqual(this.invite, widgetInviteModel.invite) && this.isGeneratingInvite == widgetInviteModel.isGeneratingInvite && this.isValidInvite == widgetInviteModel.isValidInvite && m.areEqual(this.invitableChannels, widgetInviteModel.invitableChannels) && m.areEqual(this.f2829me, widgetInviteModel.f2829me) && m.areEqual(this.dms, widgetInviteModel.dms) && this.isInviteFromStore == widgetInviteModel.isInviteFromStore;
    }

    public final List<Channel> getDms() {
        return this.dms;
    }

    public final List<Channel> getInvitableChannels() {
        return this.invitableChannels;
    }

    public final ModelInvite getInvite() {
        return this.invite;
    }

    public final MeUser getMe() {
        return this.f2829me;
    }

    public final ModelInvite.Settings getSettings() {
        return this.settings;
    }

    public final Channel getTargetChannel() {
        return this.targetChannel;
    }

    public int hashCode() {
        Channel channel = this.targetChannel;
        int i = 0;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        ModelInvite.Settings settings = this.settings;
        int hashCode2 = (hashCode + (settings != null ? settings.hashCode() : 0)) * 31;
        ModelInvite modelInvite = this.invite;
        int hashCode3 = (hashCode2 + (modelInvite != null ? modelInvite.hashCode() : 0)) * 31;
        boolean z2 = this.isGeneratingInvite;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        boolean z3 = this.isValidInvite;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        List<Channel> list = this.invitableChannels;
        int hashCode4 = (i8 + (list != null ? list.hashCode() : 0)) * 31;
        MeUser meUser = this.f2829me;
        int hashCode5 = (hashCode4 + (meUser != null ? meUser.hashCode() : 0)) * 31;
        List<Channel> list2 = this.dms;
        if (list2 != null) {
            i = list2.hashCode();
        }
        int i9 = (hashCode5 + i) * 31;
        boolean z4 = this.isInviteFromStore;
        if (!z4) {
            i2 = z4 ? 1 : 0;
        }
        return i9 + i2;
    }

    public final boolean isGeneratingInvite() {
        return this.isGeneratingInvite;
    }

    public final boolean isInviteFromStore() {
        return this.isInviteFromStore;
    }

    public final boolean isValidInvite() {
        return this.isValidInvite;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetInviteModel(targetChannel=");
        R.append(this.targetChannel);
        R.append(", settings=");
        R.append(this.settings);
        R.append(", invite=");
        R.append(this.invite);
        R.append(", isGeneratingInvite=");
        R.append(this.isGeneratingInvite);
        R.append(", isValidInvite=");
        R.append(this.isValidInvite);
        R.append(", invitableChannels=");
        R.append(this.invitableChannels);
        R.append(", me=");
        R.append(this.f2829me);
        R.append(", dms=");
        R.append(this.dms);
        R.append(", isInviteFromStore=");
        return a.M(R, this.isInviteFromStore, ")");
    }

    public /* synthetic */ WidgetInviteModel(Channel channel, ModelInvite.Settings settings, ModelInvite modelInvite, boolean z2, boolean z3, List list, MeUser meUser, List list2, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(channel, settings, modelInvite, (i & 8) != 0 ? false : z2, (i & 16) != 0 ? false : z3, list, meUser, list2, (i & 256) != 0 ? false : z4);
    }
}
