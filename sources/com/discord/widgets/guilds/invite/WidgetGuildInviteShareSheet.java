package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.cardview.widget.CardView;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppViewFlipper;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetGuildInviteShareSheetBinding;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.SearchInputView;
import com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel;
import com.discord.widgets.guilds.invite.WidgetGuildInviteSettings;
import com.discord.widgets.home.WidgetHome;
import d0.g0.t;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import org.objectweb.asm.Opcodes;
import xyz.discord.R;
/* compiled from: WidgetGuildInviteShareSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 52\u00020\u0001:\u00015B\u0007¢\u0006\u0004\b4\u0010\tJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\u000b\u0010\fJ!\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0013\u0010\tJ\u0015\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0006R\u001d\u0010\u001a\u001a\u00020\u00158B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001d\u0010 \u001a\u00020\u001b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010*\u001a\u00020!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010#R\u001c\u0010-\u001a\b\u0012\u0004\u0012\u00020,0+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u001c\u00100\u001a\u00020/8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103¨\u00066"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;", "viewState", "", "configureEmptyStateUI", "(Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;)V", "configureNormalUI", "setUpSearchBar", "()V", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "configureUI", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel;", "viewModel", "Lcom/discord/databinding/WidgetGuildInviteShareSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildInviteShareSheetBinding;", "binding", "", "isFixedHeight", "Z", "", "analyticsSource", "Ljava/lang/String;", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter;", "adapter", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsAdapter;", "restoredSearchQueryFromViewModel", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "guildInviteSettingsLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShareSheet extends AppBottomSheet {
    private static final String ARG_ANALYTICS_SOURCE = "ARG_ANALYTICS_SOURCE";
    private static final String ARG_CHANNEL_ID = "ARG_CHANNEL_ID";
    private static final String ARG_GUILD_ID = "ARG_GUILD_ID";
    private InviteSuggestionsAdapter adapter;
    private boolean isFixedHeight;
    private boolean restoredSearchQueryFromViewModel;
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildInviteShareSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildInviteShareSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildInviteShareSheet$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> guildInviteSettingsLauncher = WidgetGuildInviteSettings.Companion.registerForResult(this, new WidgetGuildInviteShareSheet$guildInviteSettingsLauncher$1(this));
    private String analyticsSource = "";
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, new WidgetGuildInviteShareSheet$loggingConfig$1(this), 3);

    /* compiled from: WidgetGuildInviteShareSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J3\u0010\f\u001a\u00020\u000b2\u0010\b\u0002\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\n\u0010\t\u001a\u00060\u0005j\u0002`\b2\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\f\u0010\rJ;\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000e2\u0010\b\u0002\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\n\u0010\t\u001a\u00060\u0005j\u0002`\b2\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0013¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareSheet$Companion;", "", "", "getNoticeName", "()Ljava/lang/String;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "source", "", "enqueueNoticeForHomeTab", "(Ljava/lang/Long;JLjava/lang/String;)V", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "show", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;JLjava/lang/String;)V", WidgetGuildInviteShareSheet.ARG_ANALYTICS_SOURCE, "Ljava/lang/String;", WidgetGuildInviteShareSheet.ARG_CHANNEL_ID, WidgetGuildInviteShareSheet.ARG_GUILD_ID, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void enqueueNoticeForHomeTab$default(Companion companion, Long l, long j, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                l = null;
            }
            companion.enqueueNoticeForHomeTab(l, j, str);
        }

        private final String getNoticeName() {
            return WidgetGuildInviteShareSheet.class.getSimpleName() + " - " + ClockFactory.get().currentTimeMillis();
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, Long l, long j, String str, int i, Object obj) {
            if ((i & 2) != 0) {
                l = null;
            }
            companion.show(fragmentManager, l, j, str);
        }

        public final void enqueueNoticeForHomeTab(Long l, long j, String str) {
            m.checkNotNullParameter(str, "source");
            StoreNotices notices = StoreStream.Companion.getNotices();
            notices.requestToShow(new StoreNotices.Notice(getNoticeName(), null, ClockFactory.get().currentTimeMillis(), 0, false, d0.t.m.listOf(a0.getOrCreateKotlinClass(WidgetHome.class)), 0L, false, 0L, new WidgetGuildInviteShareSheet$Companion$enqueueNoticeForHomeTab$notice$1(l, j, str, notices, getNoticeName()), Opcodes.I2C, null));
        }

        public final void show(FragmentManager fragmentManager, Long l, long j, String str) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(str, "source");
            AnalyticsTracker.INSTANCE.openPopout("Instant Invite", str);
            WidgetGuildInviteShareSheet widgetGuildInviteShareSheet = new WidgetGuildInviteShareSheet();
            Bundle bundle = new Bundle();
            if (l != null) {
                bundle.putLong(WidgetGuildInviteShareSheet.ARG_CHANNEL_ID, l.longValue());
            }
            bundle.putLong(WidgetGuildInviteShareSheet.ARG_GUILD_ID, j);
            bundle.putString(WidgetGuildInviteShareSheet.ARG_ANALYTICS_SOURCE, str);
            widgetGuildInviteShareSheet.setArguments(bundle);
            widgetGuildInviteShareSheet.show(fragmentManager, WidgetGuildInviteShareSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildInviteShareSheet() {
        super(false, 1, null);
        WidgetGuildInviteShareSheet$viewModel$2 widgetGuildInviteShareSheet$viewModel$2 = new WidgetGuildInviteShareSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildInviteShareSheetViewModel.class), new WidgetGuildInviteShareSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildInviteShareSheet$viewModel$2));
    }

    private final void configureEmptyStateUI(final GuildInviteShareSheetViewModel.ViewState viewState) {
        int i = 0;
        if (this.isFixedHeight) {
            this.isFixedHeight = false;
            FrameLayout frameLayout = getBinding().j;
            m.checkNotNullExpressionValue(frameLayout, "binding.root");
            if (!ViewCompat.isLaidOut(frameLayout) || frameLayout.isLayoutRequested()) {
                frameLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureEmptyStateUI$$inlined$doOnLayout$1
                    @Override // android.view.View.OnLayoutChangeListener
                    public void onLayoutChange(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
                        m.checkNotNullParameter(view, "view");
                        view.removeOnLayoutChangeListener(this);
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
                        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                        layoutParams2.height = -1;
                        view.setLayoutParams(layoutParams2);
                    }
                });
            } else {
                ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.height = -1;
                frameLayout.setLayoutParams(layoutParams2);
            }
        }
        WidgetGuildInviteShareSheetBinding binding = getBinding();
        AppViewFlipper appViewFlipper = binding.n;
        m.checkNotNullExpressionValue(appViewFlipper, "suggestionsFlipper");
        appViewFlipper.setDisplayedChild(0);
        NestedScrollView nestedScrollView = binding.f;
        m.checkNotNullExpressionValue(nestedScrollView, "emptyStateScroller");
        nestedScrollView.setNestedScrollingEnabled(true);
        final GuildInvite invite = viewState.getInvite();
        ModelInvite.Settings inviteSettings = viewState.getInviteSettings();
        CharSequence charSequence = null;
        if (invite != null) {
            long expirationTimeMs = invite.getExpirationTimeMs() - ClockFactory.get().currentTimeMillis();
            TextView textView = binding.d;
            m.checkNotNullExpressionValue(textView, "emptyStateInviteLink");
            textView.setText(invite.toLink());
            TextView textView2 = binding.g;
            m.checkNotNullExpressionValue(textView2, "emptyStateSettingsSubtext");
            if (inviteSettings != null) {
                Context requireContext = requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                charSequence = GuildInviteUiHelperKt.getInviteSettingsText(requireContext, expirationTimeMs, inviteSettings.getMaxUses());
            }
            textView2.setText(charSequence);
        } else {
            TextView textView3 = binding.d;
            m.checkNotNullExpressionValue(textView3, "emptyStateInviteLink");
            textView3.setText(getString(R.string.loading));
            TextView textView4 = binding.g;
            m.checkNotNullExpressionValue(textView4, "emptyStateSettingsSubtext");
            textView4.setText((CharSequence) null);
        }
        binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureEmptyStateUI$$inlined$with$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                String str;
                if (GuildInvite.this != null) {
                    Context x2 = a.x(view, "it", "it.context");
                    GuildInvite guildInvite = GuildInvite.this;
                    Channel channel = viewState.getChannel();
                    str = this.analyticsSource;
                    GuildInviteUiHelperKt.copyLinkClick(x2, guildInvite, channel, str);
                }
            }
        });
        binding.h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureEmptyStateUI$$inlined$with$lambda$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (GuildInvite.this != null) {
                    GuildInviteUiHelperKt.shareLinkClick(this.getContext(), GuildInvite.this, viewState.getChannel());
                }
            }
        });
        ImageButton imageButton = binding.e;
        m.checkNotNullExpressionValue(imageButton, "emptyStateLinkOptions");
        if (!viewState.getShowInviteSettings()) {
            i = 8;
        }
        imageButton.setVisibility(i);
        binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureEmptyStateUI$$inlined$with$lambda$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ActivityResultLauncher<Intent> activityResultLauncher;
                GuildInviteShareSheetViewModel viewModel;
                WidgetGuildInviteSettings.Companion companion = WidgetGuildInviteSettings.Companion;
                Context requireContext2 = this.requireContext();
                m.checkNotNullExpressionValue(requireContext2, "requireContext()");
                activityResultLauncher = this.guildInviteSettingsLauncher;
                GuildInvite guildInvite = GuildInvite.this;
                Long channelId = guildInvite != null ? guildInvite.getChannelId() : null;
                viewModel = this.getViewModel();
                companion.launch(requireContext2, activityResultLauncher, channelId, viewModel.getGuildId(), "Instant Invite Action Sheet");
            }
        });
    }

    private final void configureNormalUI(final GuildInviteShareSheetViewModel.ViewState viewState) {
        CharSequence e;
        CharSequence e2;
        boolean z2 = true;
        if (!this.isFixedHeight) {
            this.isFixedHeight = true;
            FrameLayout frameLayout = getBinding().j;
            m.checkNotNullExpressionValue(frameLayout, "binding.root");
            if (!ViewCompat.isLaidOut(frameLayout) || frameLayout.isLayoutRequested()) {
                frameLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureNormalUI$$inlined$doOnLayout$1
                    @Override // android.view.View.OnLayoutChangeListener
                    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                        m.checkNotNullParameter(view, "view");
                        view.removeOnLayoutChangeListener(this);
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
                        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                        Resources resources = WidgetGuildInviteShareSheet.this.getResources();
                        m.checkNotNullExpressionValue(resources, "resources");
                        layoutParams2.height = (int) (resources.getDisplayMetrics().heightPixels * 0.9d);
                        view.setLayoutParams(layoutParams2);
                        WidgetGuildInviteShareSheet.this.getBinding().m.requestLayout();
                    }
                });
            } else {
                ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                Resources resources = getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                layoutParams2.height = (int) (resources.getDisplayMetrics().heightPixels * 0.9d);
                frameLayout.setLayoutParams(layoutParams2);
                getBinding().m.requestLayout();
            }
        }
        final String searchQuery = viewState.getSearchQuery();
        WidgetGuildInviteShareSheetBinding binding = getBinding();
        if (!this.restoredSearchQueryFromViewModel && (!t.isBlank(searchQuery))) {
            this.restoredSearchQueryFromViewModel = true;
            binding.k.setText(searchQuery);
        }
        AppViewFlipper appViewFlipper = binding.n;
        m.checkNotNullExpressionValue(appViewFlipper, "suggestionsFlipper");
        appViewFlipper.setDisplayedChild(1);
        NestedScrollView nestedScrollView = binding.f;
        m.checkNotNullExpressionValue(nestedScrollView, "emptyStateScroller");
        int i = 0;
        nestedScrollView.setNestedScrollingEnabled(false);
        if (viewState.getChannel() != null) {
            SearchInputView searchInputView = binding.k;
            Channel channel = viewState.getChannel();
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            e2 = b.e(this, R.string.invite_your_friends_channel_mobile, new Object[]{ChannelUtils.e(channel, requireContext, false, 2)}, (r4 & 4) != 0 ? b.a.j : null);
            searchInputView.setHint(e2);
        } else {
            SearchInputView searchInputView2 = binding.k;
            String string = getString(R.string.invite_your_friends);
            m.checkNotNullExpressionValue(string, "getString(R.string.invite_your_friends)");
            searchInputView2.setHint(string);
        }
        final GuildInvite invite = viewState.getInvite();
        if (invite != null) {
            CardView cardView = binding.l;
            m.checkNotNullExpressionValue(cardView, "shareButton");
            e = b.e(this, R.string.share_invite_mobile, new Object[]{invite}, (r4 & 4) != 0 ? b.a.j : null);
            cardView.setContentDescription(e);
            long expirationTimeMs = invite.getExpirationTimeMs() - ClockFactory.get().currentTimeMillis();
            String link = invite.toLink();
            Context requireContext2 = requireContext();
            m.checkNotNullExpressionValue(requireContext2, "requireContext()");
            CharSequence inviteSettingsText = GuildInviteUiHelperKt.getInviteSettingsText(requireContext2, expirationTimeMs);
            TextView textView = binding.c;
            m.checkNotNullExpressionValue(textView, "copyLinkSubtitle");
            textView.setText(link + ' ' + inviteSettingsText);
            binding.l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureNormalUI$$inlined$with$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GuildInviteUiHelperKt.shareLinkClick(this.getContext(), GuildInvite.this, viewState.getChannel());
                }
            });
            binding.f2400b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureNormalUI$$inlined$with$lambda$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    String str;
                    Context x2 = a.x(view, "it", "it.context");
                    GuildInvite guildInvite = GuildInvite.this;
                    Channel channel2 = viewState.getChannel();
                    str = this.analyticsSource;
                    GuildInviteUiHelperKt.copyLinkClick(x2, guildInvite, channel2, str);
                }
            });
        }
        CardView cardView2 = binding.l;
        m.checkNotNullExpressionValue(cardView2, "shareButton");
        if (invite == null) {
            z2 = false;
        }
        cardView2.setVisibility(z2 ? 0 : 8);
        FrameLayout frameLayout2 = binding.i;
        m.checkNotNullExpressionValue(frameLayout2, "inviteSettingsButton");
        if (!viewState.getShowInviteSettings()) {
            i = 8;
        }
        frameLayout2.setVisibility(i);
        binding.i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareSheet$configureNormalUI$$inlined$with$lambda$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ActivityResultLauncher<Intent> activityResultLauncher;
                GuildInviteShareSheetViewModel viewModel;
                WidgetGuildInviteSettings.Companion companion = WidgetGuildInviteSettings.Companion;
                Context requireContext3 = this.requireContext();
                m.checkNotNullExpressionValue(requireContext3, "requireContext()");
                activityResultLauncher = this.guildInviteSettingsLauncher;
                GuildInvite guildInvite = GuildInvite.this;
                Long channelId = guildInvite != null ? guildInvite.getChannelId() : null;
                viewModel = this.getViewModel();
                companion.launch(requireContext3, activityResultLauncher, channelId, viewModel.getGuildId(), "Instant Invite Action Sheet");
            }
        });
        InviteSuggestionsAdapter inviteSuggestionsAdapter = this.adapter;
        if (inviteSuggestionsAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        inviteSuggestionsAdapter.setOnClick(new WidgetGuildInviteShareSheet$configureNormalUI$3(this));
        InviteSuggestionsAdapter inviteSuggestionsAdapter2 = this.adapter;
        if (inviteSuggestionsAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        inviteSuggestionsAdapter2.setData(viewState.getInviteSuggestionItems());
    }

    public final WidgetGuildInviteShareSheetBinding getBinding() {
        return (WidgetGuildInviteShareSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final GuildInviteShareSheetViewModel getViewModel() {
        return (GuildInviteShareSheetViewModel) this.viewModel$delegate.getValue();
    }

    private final void setUpSearchBar() {
        getBinding().k.a(this, new WidgetGuildInviteShareSheet$setUpSearchBar$1(this));
    }

    public final void configureUI(GuildInviteShareSheetViewModel.ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        if (viewState.getHasResults()) {
            configureNormalUI(viewState);
        } else {
            configureEmptyStateUI(viewState);
        }
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_guild_invite_share_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildInviteShareSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteShareSheet$onResume$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        setBottomSheetCollapsedStateDisabled();
        String string = getArgumentsOrDefault().getString(ARG_ANALYTICS_SOURCE);
        if (string == null) {
            string = "";
        }
        this.analyticsSource = string;
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView, "binding.suggestionList");
        this.adapter = (InviteSuggestionsAdapter) companion.configure(new InviteSuggestionsAdapter(recyclerView));
        setUpSearchBar();
    }
}
