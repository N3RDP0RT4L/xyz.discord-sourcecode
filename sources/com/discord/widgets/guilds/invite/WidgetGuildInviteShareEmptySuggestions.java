package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGuildInviteShareEmptySuggestionsBinding;
import com.discord.models.domain.ModelInvite;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetGuildInviteShareEmptySuggestions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b2\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\f\u001a\u00020\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\bR!\u0010\u001f\u001a\u00060\u0019j\u0002`\u001a8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR$\u0010%\u001a\n \"*\u0004\u0018\u00010!0!*\u0004\u0018\u00010 8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b#\u0010$R\u001c\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000b\u0010&R\u001d\u0010,\u001a\u00020'8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+R\u001d\u00101\u001a\u00020-8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010\u001c\u001a\u0004\b/\u00100¨\u00063"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareEmptySuggestions;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;", "viewState", "", "updateUi", "(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;)V", "initBottomSheet", "()V", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;", "bottomSheetBehavior", "initBottomSheetBehavior", "(Lcom/google/android/material/bottomsheet/BottomSheetBehavior;)V", "", "hours", "", "maxUses", "getHoursExpirationString", "(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "", "Lcom/discord/primitives/GuildId;", "guildId$delegate", "Lkotlin/Lazy;", "getGuildId", "()J", "guildId", "Lcom/discord/models/domain/ModelInvite;", "", "kotlin.jvm.PlatformType", "getInviteLink", "(Lcom/discord/models/domain/ModelInvite;)Ljava/lang/String;", "inviteLink", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Lcom/discord/databinding/WidgetGuildInviteShareEmptySuggestionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildInviteShareEmptySuggestionsBinding;", "binding", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "viewModel", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShareEmptySuggestions extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildInviteShareEmptySuggestions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildInviteShareEmptySuggestionsBinding;", 0)};
    private BottomSheetBehavior<ViewInviteSettingsSheet> bottomSheetBehavior;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildInviteShareEmptySuggestions$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetGuildInviteShareEmptySuggestions$guildId$2(this));

    public WidgetGuildInviteShareEmptySuggestions() {
        super(R.layout.widget_guild_invite_share_empty_suggestions);
        WidgetGuildInviteShareEmptySuggestions$viewModel$2 widgetGuildInviteShareEmptySuggestions$viewModel$2 = new WidgetGuildInviteShareEmptySuggestions$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGuildInviteShareViewModel.class), new WidgetGuildInviteShareEmptySuggestions$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildInviteShareEmptySuggestions$viewModel$2));
    }

    public static final /* synthetic */ BottomSheetBehavior access$getBottomSheetBehavior$p(WidgetGuildInviteShareEmptySuggestions widgetGuildInviteShareEmptySuggestions) {
        BottomSheetBehavior<ViewInviteSettingsSheet> bottomSheetBehavior = widgetGuildInviteShareEmptySuggestions.bottomSheetBehavior;
        if (bottomSheetBehavior == null) {
            m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        return bottomSheetBehavior;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGuildInviteShareEmptySuggestionsBinding getBinding() {
        return (WidgetGuildInviteShareEmptySuggestionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final CharSequence getHoursExpirationString(int i, CharSequence charSequence) {
        CharSequence c;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources, requireContext(), (int) R.plurals.duration_hours_hours, i, Integer.valueOf(i));
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        c = b.c(resources2, R.string.invite_settings_expired_description, new Object[]{quantityString, charSequence}, (r4 & 4) != 0 ? b.d.j : null);
        return c;
    }

    private final String getInviteLink(ModelInvite modelInvite) {
        if (modelInvite == null) {
            return BuildConfig.HOST_INVITE;
        }
        return modelInvite.toLink(getResources(), BuildConfig.HOST_INVITE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGuildInviteShareViewModel getViewModel() {
        return (WidgetGuildInviteShareViewModel) this.viewModel$delegate.getValue();
    }

    private final void initBottomSheet() {
        getBinding().g.setOnGenerateLinkListener(new WidgetGuildInviteShareEmptySuggestions$initBottomSheet$1(this));
        getBinding().g.setViewModel(getViewModel());
    }

    private final void initBottomSheetBehavior(BottomSheetBehavior<ViewInviteSettingsSheet> bottomSheetBehavior) {
        bottomSheetBehavior.setState(5);
        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareEmptySuggestions$initBottomSheetBehavior$1
            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onSlide(View view, float f) {
                m.checkNotNullParameter(view, "bottomSheet");
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onStateChanged(View view, int i) {
                WidgetGuildInviteShareEmptySuggestionsBinding binding;
                WidgetGuildInviteShareViewModel viewModel;
                m.checkNotNullParameter(view, "bottomSheet");
                if (i == 5) {
                    viewModel = WidgetGuildInviteShareEmptySuggestions.this.getViewModel();
                    viewModel.refreshUi();
                }
                binding = WidgetGuildInviteShareEmptySuggestions.this.getBinding();
                ViewExtensions.fadeBy$default(binding.f2398b, i != 5, 0L, 2, null);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateUi(WidgetGuildInviteShareViewModel.ViewState.Loaded loaded) {
        CharSequence charSequence;
        CharSequence c;
        CharSequence c2;
        CharSequence c3;
        WidgetInviteModel widgetInviteModel = loaded.getWidgetInviteModel();
        getBinding().g.configureUi(widgetInviteModel);
        final ModelInvite invite = widgetInviteModel.getInvite();
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.guildInviteEmptySuggestionsInviteLink");
        textView.setText(getInviteLink(invite));
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareEmptySuggestions$updateUi$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildInviteUiHelperKt.copyLinkClick(a.x(view, "it", "it.context"), invite, WidgetGuildInviteShareEmptySuggestions.this.getMostRecentIntent());
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareEmptySuggestions$updateUi$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ModelInvite modelInvite = invite;
                if (modelInvite != null) {
                    GuildInviteUiHelperKt.shareLinkClick(WidgetGuildInviteShareEmptySuggestions.this.getContext(), modelInvite);
                }
            }
        });
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareEmptySuggestions$updateUi$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildInviteShareEmptySuggestions.access$getBottomSheetBehavior$p(WidgetGuildInviteShareEmptySuggestions.this).setState(3);
            }
        });
        ModelInvite.Settings settings = widgetInviteModel.getSettings();
        if (settings != null) {
            if (settings.getMaxUses() == 0) {
                charSequence = getResources().getString(R.string.max_uses_description_unlimited_uses);
                m.checkNotNullExpressionValue(charSequence, "resources.getString(R.st…scription_unlimited_uses)");
            } else {
                Resources resources = getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                charSequence = StringResourceUtilsKt.getQuantityString(resources, requireContext(), (int) R.plurals.max_uses_description_mobile_maxUses, settings.getMaxUses(), Integer.valueOf(settings.getMaxUses()));
            }
            int maxAge = widgetInviteModel.getSettings().getMaxAge();
            if (maxAge == 0) {
                String str = getResources().getString(R.string.max_age_never_description_mobile) + ", " + charSequence;
                m.checkNotNullExpressionValue(str, "StringBuilder()\n        …              .toString()");
                TextView textView2 = getBinding().f;
                m.checkNotNullExpressionValue(textView2, "binding.guildInviteEmpty…SettingsInviteLinkSubtext");
                textView2.setText(str);
            } else if (maxAge == 1800) {
                Resources resources2 = getResources();
                m.checkNotNullExpressionValue(resources2, "resources");
                CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources2, requireContext(), (int) R.plurals.duration_minutes_minutes, 30, 30);
                TextView textView3 = getBinding().f;
                m.checkNotNullExpressionValue(textView3, "binding.guildInviteEmpty…SettingsInviteLinkSubtext");
                Resources resources3 = getResources();
                m.checkNotNullExpressionValue(resources3, "resources");
                c = b.c(resources3, R.string.invite_settings_expired_description, new Object[]{quantityString, charSequence}, (r4 & 4) != 0 ? b.d.j : null);
                textView3.setText(c);
            } else if (maxAge == 3600) {
                TextView textView4 = getBinding().f;
                m.checkNotNullExpressionValue(textView4, "binding.guildInviteEmpty…SettingsInviteLinkSubtext");
                textView4.setText(getHoursExpirationString(1, charSequence));
            } else if (maxAge == 21600) {
                TextView textView5 = getBinding().f;
                m.checkNotNullExpressionValue(textView5, "binding.guildInviteEmpty…SettingsInviteLinkSubtext");
                textView5.setText(getHoursExpirationString(6, charSequence));
            } else if (maxAge == 43200) {
                TextView textView6 = getBinding().f;
                m.checkNotNullExpressionValue(textView6, "binding.guildInviteEmpty…SettingsInviteLinkSubtext");
                textView6.setText(getHoursExpirationString(12, charSequence));
            } else if (maxAge == 86400) {
                Resources resources4 = getResources();
                m.checkNotNullExpressionValue(resources4, "resources");
                CharSequence quantityString2 = StringResourceUtilsKt.getQuantityString(resources4, requireContext(), (int) R.plurals.duration_days_days, 1, 1);
                TextView textView7 = getBinding().f;
                m.checkNotNullExpressionValue(textView7, "binding.guildInviteEmpty…SettingsInviteLinkSubtext");
                Resources resources5 = getResources();
                m.checkNotNullExpressionValue(resources5, "resources");
                c2 = b.c(resources5, R.string.invite_settings_expired_description, new Object[]{quantityString2, charSequence}, (r4 & 4) != 0 ? b.d.j : null);
                textView7.setText(c2);
            } else if (maxAge == 604800) {
                Resources resources6 = getResources();
                m.checkNotNullExpressionValue(resources6, "resources");
                CharSequence quantityString3 = StringResourceUtilsKt.getQuantityString(resources6, requireContext(), (int) R.plurals.duration_days_days, 7, 7);
                TextView textView8 = getBinding().f;
                m.checkNotNullExpressionValue(textView8, "binding.guildInviteEmpty…SettingsInviteLinkSubtext");
                Resources resources7 = getResources();
                m.checkNotNullExpressionValue(resources7, "resources");
                c3 = b.c(resources7, R.string.invite_settings_expired_description, new Object[]{quantityString3, charSequence}, (r4 & 4) != 0 ? b.d.j : null);
                textView8.setText(c3);
            }
        }
    }

    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        if (getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_GUILD_ID", 0L) <= 0) {
            requireActivity().finish();
            return;
        }
        long longExtra = getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", 0L);
        if (longExtra != 0) {
            getViewModel().selectChannel(longExtra);
        }
        final boolean booleanExtra = getMostRecentIntent().getBooleanExtra(WidgetGuildInviteShare.INTENT_IS_NUX_FLOW, false);
        setActionBarDisplayHomeAsUpEnabled(true, booleanExtra ? Integer.valueOf(DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.ic_close_24dp, 0, 2, (Object) null)) : null, booleanExtra ? Integer.valueOf((int) R.string.close) : null);
        AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareEmptySuggestions$onViewBound$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                if (WidgetGuildInviteShareEmptySuggestions.access$getBottomSheetBehavior$p(WidgetGuildInviteShareEmptySuggestions.this).getState() != 5) {
                    WidgetGuildInviteShareEmptySuggestions.access$getBottomSheetBehavior$p(WidgetGuildInviteShareEmptySuggestions.this).setState(5);
                    return Boolean.TRUE;
                } else if (!booleanExtra) {
                    return Boolean.FALSE;
                } else {
                    j.c(WidgetGuildInviteShareEmptySuggestions.this.requireContext(), false, null, 6);
                    return Boolean.TRUE;
                }
            }
        }, 0, 2, null);
        BottomSheetBehavior<ViewInviteSettingsSheet> from = BottomSheetBehavior.from(getBinding().g);
        m.checkNotNullExpressionValue(from, "BottomSheetBehavior.from…nviteSettingsBottomSheet)");
        this.bottomSheetBehavior = from;
        if (from == null) {
            m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        initBottomSheetBehavior(from);
        initBottomSheet();
        getBinding().f2398b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareEmptySuggestions$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetGuildInviteShareEmptySuggestions.access$getBottomSheetBehavior$p(WidgetGuildInviteShareEmptySuggestions.this).setState(5);
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<WidgetGuildInviteShareViewModel.ViewState> J = getViewModel().observeViewState().J();
        m.checkNotNullExpressionValue(J, "viewModel.observeViewSta…  .onBackpressureBuffer()");
        Observable<R> F = J.x(WidgetGuildInviteShareEmptySuggestions$onViewBoundOrOnResume$$inlined$filterIs$1.INSTANCE).F(WidgetGuildInviteShareEmptySuggestions$onViewBoundOrOnResume$$inlined$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(F, this, null, 2, null), WidgetGuildInviteShareEmptySuggestions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteShareEmptySuggestions$onViewBoundOrOnResume$1(this));
    }
}
