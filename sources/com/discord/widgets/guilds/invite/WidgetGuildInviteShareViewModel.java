package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.app.AppComponent;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelInvite;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreInstantInvites;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreMessages;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel;
import d0.t.h0;
import d0.z.d.k;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func5;
import rx.subjects.BehaviorSubject;
/* compiled from: WidgetGuildInviteShareViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ê\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001~B\u0093\u0001\u0012\b\b\u0002\u00103\u001a\u000202\u0012\b\b\u0002\u0010s\u001a\u00020r\u0012\b\b\u0002\u0010f\u001a\u00020e\u0012\b\b\u0002\u0010V\u001a\u00020U\u0012\b\b\u0002\u0010Q\u001a\u00020P\u0012\b\b\u0002\u0010B\u001a\u00020A\u0012\b\b\u0002\u0010x\u001a\u00020w\u0012\b\b\u0002\u0010[\u001a\u00020Z\u0012\b\b\u0002\u0010`\u001a\u00020_\u0012\u0006\u0010n\u001a\u00020m\u0012\b\b\u0002\u0010I\u001a\u00020H\u0012\n\u0010\u0012\u001a\u00060\fj\u0002`\u0011\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\f\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b|\u0010}J\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ-\u0010\u000f\u001a\u00020\u00062\u001c\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00020\n\u0012\u000e\u0012\f\u0012\b\u0012\u00060\fj\u0002`\r0\u000b0\tH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J5\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\n\u0010\u0012\u001a\u00060\fj\u0002`\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\f2\b\u0010\u0014\u001a\u0004\u0018\u00010\nH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J5\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00040\u00152\n\u0010\u0012\u001a\u00060\fj\u0002`\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\f2\b\u0010\u0014\u001a\u0004\u0018\u00010\nH\u0000¢\u0006\u0004\b\u0019\u0010\u0018J\u0015\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\n¢\u0006\u0004\b\u001c\u0010\u001dJ\u0019\u0010\u001f\u001a\u00020\u00062\n\u0010\u001e\u001a\u00060\fj\u0002`\r¢\u0006\u0004\b\u001f\u0010 J\u0019\u0010!\u001a\u00020\u00062\n\u0010\u001e\u001a\u00060\fj\u0002`\r¢\u0006\u0004\b!\u0010 J\u000f\u0010#\u001a\u0004\u0018\u00010\"¢\u0006\u0004\b#\u0010$J+\u0010'\u001a\u00020\u00062\n\u0010\u001e\u001a\u00060\fj\u0002`\r2\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010&\u001a\u0004\u0018\u00010%¢\u0006\u0004\b'\u0010(J+\u0010+\u001a\u00020\u00062\n\u0010*\u001a\u00060\fj\u0002`)2\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010&\u001a\u0004\u0018\u00010%¢\u0006\u0004\b+\u0010(J\u0015\u0010.\u001a\u00020\u00062\u0006\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010/J\r\u00100\u001a\u00020\u0006¢\u0006\u0004\b0\u00101R\u0019\u00103\u001a\u0002028\u0006@\u0006¢\u0006\f\n\u0004\b3\u00104\u001a\u0004\b5\u00106R \u00108\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010:\u001a\u0004\b;\u0010<R$\u0010@\u001a\n =*\u0004\u0018\u00010\n0\n*\u0004\u0018\u00010%8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b>\u0010?R\u0019\u0010B\u001a\u00020A8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010C\u001a\u0004\bD\u0010ER\u001c\u0010F\u001a\b\u0012\u0004\u0012\u00020\u0006078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u00109R2\u0010G\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\n\u0012\u000e\u0012\f\u0012\b\u0012\u00060\fj\u0002`\r0\u000b0\t078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u00109R\u0019\u0010I\u001a\u00020H8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010J\u001a\u0004\bK\u0010LR\u001d\u0010\u0012\u001a\u00060\fj\u0002`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010M\u001a\u0004\bN\u0010OR\u0019\u0010Q\u001a\u00020P8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010R\u001a\u0004\bS\u0010TR\u0019\u0010V\u001a\u00020U8\u0006@\u0006¢\u0006\f\n\u0004\bV\u0010W\u001a\u0004\bX\u0010YR\u0019\u0010[\u001a\u00020Z8\u0006@\u0006¢\u0006\f\n\u0004\b[\u0010\\\u001a\u0004\b]\u0010^R\u0019\u0010`\u001a\u00020_8\u0006@\u0006¢\u0006\f\n\u0004\b`\u0010a\u001a\u0004\bb\u0010cR:\u0010d\u001a&\u0012\f\u0012\n =*\u0004\u0018\u00010\n0\n =*\u0012\u0012\f\u0012\n =*\u0004\u0018\u00010\n0\n\u0018\u000107078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u00109R\u0019\u0010f\u001a\u00020e8\u0006@\u0006¢\u0006\f\n\u0004\bf\u0010g\u001a\u0004\bh\u0010iR\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010j\u001a\u0004\bk\u0010lR\u0019\u0010n\u001a\u00020m8\u0006@\u0006¢\u0006\f\n\u0004\bn\u0010o\u001a\u0004\bp\u0010qR\u0019\u0010s\u001a\u00020r8\u0006@\u0006¢\u0006\f\n\u0004\bs\u0010t\u001a\u0004\bu\u0010vR\u0019\u0010x\u001a\u00020w8\u0006@\u0006¢\u0006\f\n\u0004\bx\u0010y\u001a\u0004\bz\u0010{¨\u0006\u007f"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState;", "Lcom/discord/app/AppComponent;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;", "viewState", "", "generateInviteLinkFromViewState", "(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;)V", "", "", "", "", "Lcom/discord/primitives/ChannelId;", "sentInvites", "updateSentInvites", "(Ljava/util/Map;)V", "Lcom/discord/primitives/GuildId;", "guildId", "eventId", "inviteStoreKey", "Lrx/Observable;", "Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "observeWidgetInviteViewModel", "(JLjava/lang/Long;Ljava/lang/String;)Lrx/Observable;", "observeViewStateFromStores$app_productionGoogleRelease", "observeViewStateFromStores", "searchQuery", "updateSearchQuery", "(Ljava/lang/String;)V", "channelId", "selectChannel", "(J)V", "generateInviteLink", "Lcom/discord/models/experiments/domain/Experiment;", "getDefaultInviteExperiment", "()Lcom/discord/models/experiments/domain/Experiment;", "Lcom/discord/models/domain/ModelInvite;", "invite", "sendInviteToChannel", "(JLcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelInvite;)V", "Lcom/discord/primitives/UserId;", "userId", "sendInviteToUser", "Lcom/discord/models/domain/ModelInvite$Settings;", "settings", "updateInviteSettings", "(Lcom/discord/models/domain/ModelInvite$Settings;)V", "refreshUi", "()V", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "getStoreChannels", "()Lcom/discord/stores/StoreChannels;", "Lrx/subjects/BehaviorSubject;", "selectedChannelSubject", "Lrx/subjects/BehaviorSubject;", "Ljava/lang/String;", "getInviteStoreKey", "()Ljava/lang/String;", "kotlin.jvm.PlatformType", "getInviteLink", "(Lcom/discord/models/domain/ModelInvite;)Ljava/lang/String;", "inviteLink", "Lcom/discord/stores/StoreGuildScheduledEvents;", "storeGuildScheduledEvents", "Lcom/discord/stores/StoreGuildScheduledEvents;", "getStoreGuildScheduledEvents", "()Lcom/discord/stores/StoreGuildScheduledEvents;", "refreshUiSubject", "sentInvitesSubject", "", "subscribeOnInit", "Z", "getSubscribeOnInit", "()Z", "J", "getGuildId", "()J", "Lcom/discord/stores/StoreStageInstances;", "storeStageInstances", "Lcom/discord/stores/StoreStageInstances;", "getStoreStageInstances", "()Lcom/discord/stores/StoreStageInstances;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "getStoreUser", "()Lcom/discord/stores/StoreUser;", "Lcom/discord/widgets/guilds/invite/InviteGenerator;", "inviteGenerator", "Lcom/discord/widgets/guilds/invite/InviteGenerator;", "getInviteGenerator", "()Lcom/discord/widgets/guilds/invite/InviteGenerator;", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager;", "inviteSuggestionsManager", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager;", "getInviteSuggestionsManager", "()Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager;", "filterPublisher", "Lcom/discord/stores/StoreInviteSettings;", "storeInviteSettings", "Lcom/discord/stores/StoreInviteSettings;", "getStoreInviteSettings", "()Lcom/discord/stores/StoreInviteSettings;", "Ljava/lang/Long;", "getEventId", "()Ljava/lang/Long;", "Landroid/content/res/Resources;", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "getStoreGuilds", "()Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreInstantInvites;", "storeInstantInvites", "Lcom/discord/stores/StoreInstantInvites;", "getStoreInstantInvites", "()Lcom/discord/stores/StoreInstantInvites;", HookHelper.constructorName, "(Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreStageInstances;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StoreInstantInvites;Lcom/discord/widgets/guilds/invite/InviteGenerator;Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager;Landroid/content/res/Resources;ZJLjava/lang/Long;Ljava/lang/String;)V", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildInviteShareViewModel extends AppViewModel<ViewState> implements AppComponent {
    private final Long eventId;
    private final BehaviorSubject<String> filterPublisher;
    private final long guildId;
    private final InviteGenerator inviteGenerator;
    private final String inviteStoreKey;
    private final InviteSuggestionsManager inviteSuggestionsManager;
    private final BehaviorSubject<Unit> refreshUiSubject;
    private final Resources resources;
    private final BehaviorSubject<Long> selectedChannelSubject;
    private final BehaviorSubject<Map<String, Set<Long>>> sentInvitesSubject;
    private final StoreChannels storeChannels;
    private final StoreGuildScheduledEvents storeGuildScheduledEvents;
    private final StoreGuilds storeGuilds;
    private final StoreInstantInvites storeInstantInvites;
    private final StoreInviteSettings storeInviteSettings;
    private final StoreStageInstances storeStageInstances;
    private final StoreUser storeUser;
    private final boolean subscribeOnInit;

    /* compiled from: WidgetGuildInviteShareViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState;", "p1", "", "invoke", "(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<ViewState, Unit> {
        public AnonymousClass2(WidgetGuildInviteShareViewModel widgetGuildInviteShareViewModel) {
            super(1, widgetGuildInviteShareViewModel, WidgetGuildInviteShareViewModel.class, "updateViewState", "updateViewState(Ljava/lang/Object;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ViewState viewState) {
            invoke2(viewState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ViewState viewState) {
            m.checkNotNullParameter(viewState, "p1");
            ((WidgetGuildInviteShareViewModel) this.receiver).updateViewState(viewState);
        }
    }

    /* compiled from: WidgetGuildInviteShareViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetGuildInviteShareViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\u0017\u001a\u00020\t\u0012\u001c\u0010\u0018\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r0\f\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0012¢\u0006\u0004\b.\u0010/J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ&\u0010\u0010\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r0\fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J^\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0017\u001a\u00020\t2\u001e\b\u0002\u0010\u0018\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r0\f2\b\b\u0002\u0010\u0019\u001a\u00020\u0012HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u000bJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010\"\u001a\u00020\u00122\b\u0010!\u001a\u0004\u0018\u00010 HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010$\u001a\u0004\b%\u0010\bR\u0019\u0010\u0017\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\u000bR/\u0010\u0018\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b)\u0010\u0011R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010*\u001a\u0004\b+\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010,\u001a\u0004\b-\u0010\u0014¨\u00060"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState;", "Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "component1", "()Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "", "Lcom/discord/widgets/guilds/invite/InviteSuggestionItem;", "component2", "()Ljava/util/List;", "", "component3", "()Ljava/lang/String;", "", "", "", "Lcom/discord/primitives/ChannelId;", "component4", "()Ljava/util/Map;", "", "component5", "()Z", "widgetInviteModel", "inviteSuggestionItems", "searchQuery", "sentInvites", "showInviteSettings", "copy", "(Lcom/discord/widgets/guilds/invite/WidgetInviteModel;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Z)Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Loaded;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getInviteSuggestionItems", "Ljava/lang/String;", "getSearchQuery", "Ljava/util/Map;", "getSentInvites", "Lcom/discord/widgets/guilds/invite/WidgetInviteModel;", "getWidgetInviteModel", "Z", "getShowInviteSettings", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/invite/WidgetInviteModel;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final List<InviteSuggestionItem> inviteSuggestionItems;
            private final String searchQuery;
            private final Map<String, Set<Long>> sentInvites;
            private final boolean showInviteSettings;
            private final WidgetInviteModel widgetInviteModel;

            public /* synthetic */ Loaded(WidgetInviteModel widgetInviteModel, List list, String str, Map map, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(widgetInviteModel, list, (i & 4) != 0 ? "" : str, map, (i & 16) != 0 ? true : z2);
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, WidgetInviteModel widgetInviteModel, List list, String str, Map map, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    widgetInviteModel = loaded.widgetInviteModel;
                }
                List<InviteSuggestionItem> list2 = list;
                if ((i & 2) != 0) {
                    list2 = loaded.inviteSuggestionItems;
                }
                List list3 = list2;
                if ((i & 4) != 0) {
                    str = loaded.searchQuery;
                }
                String str2 = str;
                Map<String, Set<Long>> map2 = map;
                if ((i & 8) != 0) {
                    map2 = loaded.sentInvites;
                }
                Map map3 = map2;
                if ((i & 16) != 0) {
                    z2 = loaded.showInviteSettings;
                }
                return loaded.copy(widgetInviteModel, list3, str2, map3, z2);
            }

            public final WidgetInviteModel component1() {
                return this.widgetInviteModel;
            }

            public final List<InviteSuggestionItem> component2() {
                return this.inviteSuggestionItems;
            }

            public final String component3() {
                return this.searchQuery;
            }

            public final Map<String, Set<Long>> component4() {
                return this.sentInvites;
            }

            public final boolean component5() {
                return this.showInviteSettings;
            }

            public final Loaded copy(WidgetInviteModel widgetInviteModel, List<? extends InviteSuggestionItem> list, String str, Map<String, ? extends Set<Long>> map, boolean z2) {
                m.checkNotNullParameter(widgetInviteModel, "widgetInviteModel");
                m.checkNotNullParameter(list, "inviteSuggestionItems");
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(map, "sentInvites");
                return new Loaded(widgetInviteModel, list, str, map, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.widgetInviteModel, loaded.widgetInviteModel) && m.areEqual(this.inviteSuggestionItems, loaded.inviteSuggestionItems) && m.areEqual(this.searchQuery, loaded.searchQuery) && m.areEqual(this.sentInvites, loaded.sentInvites) && this.showInviteSettings == loaded.showInviteSettings;
            }

            public final List<InviteSuggestionItem> getInviteSuggestionItems() {
                return this.inviteSuggestionItems;
            }

            public final String getSearchQuery() {
                return this.searchQuery;
            }

            public final Map<String, Set<Long>> getSentInvites() {
                return this.sentInvites;
            }

            public final boolean getShowInviteSettings() {
                return this.showInviteSettings;
            }

            public final WidgetInviteModel getWidgetInviteModel() {
                return this.widgetInviteModel;
            }

            public int hashCode() {
                WidgetInviteModel widgetInviteModel = this.widgetInviteModel;
                int i = 0;
                int hashCode = (widgetInviteModel != null ? widgetInviteModel.hashCode() : 0) * 31;
                List<InviteSuggestionItem> list = this.inviteSuggestionItems;
                int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
                String str = this.searchQuery;
                int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
                Map<String, Set<Long>> map = this.sentInvites;
                if (map != null) {
                    i = map.hashCode();
                }
                int i2 = (hashCode3 + i) * 31;
                boolean z2 = this.showInviteSettings;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(widgetInviteModel=");
                R.append(this.widgetInviteModel);
                R.append(", inviteSuggestionItems=");
                R.append(this.inviteSuggestionItems);
                R.append(", searchQuery=");
                R.append(this.searchQuery);
                R.append(", sentInvites=");
                R.append(this.sentInvites);
                R.append(", showInviteSettings=");
                return a.M(R, this.showInviteSettings, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(WidgetInviteModel widgetInviteModel, List<? extends InviteSuggestionItem> list, String str, Map<String, ? extends Set<Long>> map, boolean z2) {
                super(null);
                m.checkNotNullParameter(widgetInviteModel, "widgetInviteModel");
                m.checkNotNullParameter(list, "inviteSuggestionItems");
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(map, "sentInvites");
                this.widgetInviteModel = widgetInviteModel;
                this.inviteSuggestionItems = list;
                this.searchQuery = str;
                this.sentInvites = map;
                this.showInviteSettings = z2;
            }
        }

        /* compiled from: WidgetGuildInviteShareViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetGuildInviteShareViewModel(StoreChannels storeChannels, StoreGuilds storeGuilds, StoreInviteSettings storeInviteSettings, StoreUser storeUser, StoreStageInstances storeStageInstances, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreInstantInvites storeInstantInvites, InviteGenerator inviteGenerator, InviteSuggestionsManager inviteSuggestionsManager, Resources resources, boolean z2, long j, Long l, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 2) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 4) != 0 ? StoreStream.Companion.getInviteSettings() : storeInviteSettings, (i & 8) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 16) != 0 ? StoreStream.Companion.getStageInstances() : storeStageInstances, (i & 32) != 0 ? StoreStream.Companion.getGuildScheduledEvents() : storeGuildScheduledEvents, (i & 64) != 0 ? StoreStream.Companion.getInstantInvites() : storeInstantInvites, (i & 128) != 0 ? new InviteGenerator() : inviteGenerator, (i & 256) != 0 ? new InviteSuggestionsManager(null, null, null, null, null, 31, null) : inviteSuggestionsManager, resources, (i & 1024) != 0 ? true : z2, j, l, str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void generateInviteLinkFromViewState(ViewState.Loaded loaded) {
        WidgetInviteModel widgetInviteModel = loaded.getWidgetInviteModel();
        if (!widgetInviteModel.isValidInvite() && !widgetInviteModel.isGeneratingInvite() && widgetInviteModel.getTargetChannel() != null) {
            this.inviteGenerator.generateForAppComponent(this, widgetInviteModel.getTargetChannel().h());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getInviteLink(ModelInvite modelInvite) {
        if (modelInvite == null) {
            return BuildConfig.HOST_INVITE;
        }
        return modelInvite.toLink(this.resources, BuildConfig.HOST_INVITE);
    }

    private final Observable<WidgetInviteModel> observeWidgetInviteViewModel(long j, Long l, String str) {
        Observable<WidgetInviteModel> q = ObservableExtensionsKt.computationLatest(ObservableCombineLatestOverloadsKt.combineLatest(this.storeInviteSettings.getInviteSettings(), this.storeInviteSettings.getInvitableChannels(j), this.inviteGenerator.getGenerationState(), this.selectedChannelSubject, StoreUser.observeMe$default(this.storeUser, false, 1, null), this.storeChannels.observeDMs(), this.storeGuilds.observeGuild(j), this.storeStageInstances.observeStageInstancesForGuild(j), this.storeGuildScheduledEvents.observeGuildScheduledEvent(l, Long.valueOf(j)), this.storeInstantInvites.observeInvite(str), WidgetGuildInviteShareViewModel$observeWidgetInviteViewModel$1.INSTANCE)).q();
        m.checkNotNullExpressionValue(q, "combineLatest(\n        s…  .distinctUntilChanged()");
        return q;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateSentInvites(Map<String, ? extends Set<Long>> map) {
        this.sentInvitesSubject.onNext(map);
    }

    public final void generateInviteLink(long j) {
        this.inviteGenerator.generateForAppComponent(this, j);
    }

    public final Experiment getDefaultInviteExperiment() {
        return this.storeInviteSettings.getInviteGuildExperiment(this.guildId, true);
    }

    public final Long getEventId() {
        return this.eventId;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final InviteGenerator getInviteGenerator() {
        return this.inviteGenerator;
    }

    public final String getInviteStoreKey() {
        return this.inviteStoreKey;
    }

    public final InviteSuggestionsManager getInviteSuggestionsManager() {
        return this.inviteSuggestionsManager;
    }

    public final Resources getResources() {
        return this.resources;
    }

    public final StoreChannels getStoreChannels() {
        return this.storeChannels;
    }

    public final StoreGuildScheduledEvents getStoreGuildScheduledEvents() {
        return this.storeGuildScheduledEvents;
    }

    public final StoreGuilds getStoreGuilds() {
        return this.storeGuilds;
    }

    public final StoreInstantInvites getStoreInstantInvites() {
        return this.storeInstantInvites;
    }

    public final StoreInviteSettings getStoreInviteSettings() {
        return this.storeInviteSettings;
    }

    public final StoreStageInstances getStoreStageInstances() {
        return this.storeStageInstances;
    }

    public final StoreUser getStoreUser() {
        return this.storeUser;
    }

    public final boolean getSubscribeOnInit() {
        return this.subscribeOnInit;
    }

    public final Observable<ViewState.Loaded> observeViewStateFromStores$app_productionGoogleRelease(long j, Long l, String str) {
        Observable<ViewState.Loaded> g = Observable.g(observeWidgetInviteViewModel(j, l, str), this.inviteSuggestionsManager.observeInviteSuggestions(), this.filterPublisher, this.sentInvitesSubject, this.refreshUiSubject, new Func5<WidgetInviteModel, List<? extends InviteSuggestion>, String, Map<String, ? extends Set<? extends Long>>, Unit, ViewState.Loaded>() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel$observeViewStateFromStores$1
            @Override // rx.functions.Func5
            public /* bridge */ /* synthetic */ WidgetGuildInviteShareViewModel.ViewState.Loaded call(WidgetInviteModel widgetInviteModel, List<? extends InviteSuggestion> list, String str2, Map<String, ? extends Set<? extends Long>> map, Unit unit) {
                return call2(widgetInviteModel, list, str2, (Map<String, ? extends Set<Long>>) map, unit);
            }

            /* JADX WARN: Removed duplicated region for block: B:60:0x014c  */
            /* JADX WARN: Removed duplicated region for block: B:66:0x015f  */
            /* renamed from: call  reason: avoid collision after fix types in other method */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel.ViewState.Loaded call2(com.discord.widgets.guilds.invite.WidgetInviteModel r10, java.util.List<? extends com.discord.widgets.guilds.invite.InviteSuggestion> r11, java.lang.String r12, java.util.Map<java.lang.String, ? extends java.util.Set<java.lang.Long>> r13, kotlin.Unit r14) {
                /*
                    Method dump skipped, instructions count: 373
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel$observeViewStateFromStores$1.call2(com.discord.widgets.guilds.invite.WidgetInviteModel, java.util.List, java.lang.String, java.util.Map, kotlin.Unit):com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel$ViewState$Loaded");
            }
        });
        m.checkNotNullExpressionValue(g, "Observable.combineLatest…iteSettings\n      )\n    }");
        return g;
    }

    public final void refreshUi() {
        this.refreshUiSubject.onNext(Unit.a);
    }

    public final void selectChannel(long j) {
        this.selectedChannelSubject.onNext(Long.valueOf(j));
    }

    public final void sendInviteToChannel(long j, ViewState.Loaded loaded, ModelInvite modelInvite) {
        m.checkNotNullParameter(loaded, "viewState");
        String inviteLink = getInviteLink(modelInvite);
        StoreMessages messages = StoreStream.Companion.getMessages();
        MeUser me2 = loaded.getWidgetInviteModel().getMe();
        m.checkNotNullExpressionValue(inviteLink, "inviteLink");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreMessages.sendMessage$default(messages, j, me2, inviteLink, null, null, null, null, null, null, null, null, null, null, null, null, 32736, null), this, null, 2, null), WidgetGuildInviteShareViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteShareViewModel$sendInviteToChannel$1(this, modelInvite, loaded, inviteLink));
    }

    public final void sendInviteToUser(long j, ViewState.Loaded loaded, ModelInvite modelInvite) {
        m.checkNotNullParameter(loaded, "viewState");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().createOrFetchDM(j), false, 1, null), this, null, 2, null), WidgetGuildInviteShareViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildInviteShareViewModel$sendInviteToUser$1(this, loaded, modelInvite));
    }

    public final void updateInviteSettings(ModelInvite.Settings settings) {
        m.checkNotNullParameter(settings, "settings");
        this.storeInviteSettings.setInviteSettings(settings);
    }

    public final void updateSearchQuery(String str) {
        m.checkNotNullParameter(str, "searchQuery");
        this.filterPublisher.onNext(str);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildInviteShareViewModel(StoreChannels storeChannels, StoreGuilds storeGuilds, StoreInviteSettings storeInviteSettings, StoreUser storeUser, StoreStageInstances storeStageInstances, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreInstantInvites storeInstantInvites, InviteGenerator inviteGenerator, InviteSuggestionsManager inviteSuggestionsManager, Resources resources, boolean z2, long j, Long l, String str) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeInviteSettings, "storeInviteSettings");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeStageInstances, "storeStageInstances");
        m.checkNotNullParameter(storeGuildScheduledEvents, "storeGuildScheduledEvents");
        m.checkNotNullParameter(storeInstantInvites, "storeInstantInvites");
        m.checkNotNullParameter(inviteGenerator, "inviteGenerator");
        m.checkNotNullParameter(inviteSuggestionsManager, "inviteSuggestionsManager");
        m.checkNotNullParameter(resources, "resources");
        this.storeChannels = storeChannels;
        this.storeGuilds = storeGuilds;
        this.storeInviteSettings = storeInviteSettings;
        this.storeUser = storeUser;
        this.storeStageInstances = storeStageInstances;
        this.storeGuildScheduledEvents = storeGuildScheduledEvents;
        this.storeInstantInvites = storeInstantInvites;
        this.inviteGenerator = inviteGenerator;
        this.inviteSuggestionsManager = inviteSuggestionsManager;
        this.resources = resources;
        this.subscribeOnInit = z2;
        this.guildId = j;
        this.eventId = l;
        this.inviteStoreKey = str;
        BehaviorSubject<Map<String, Set<Long>>> l0 = BehaviorSubject.l0(h0.emptyMap());
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(emptyMap())");
        this.sentInvitesSubject = l0;
        this.filterPublisher = BehaviorSubject.l0("");
        BehaviorSubject<Long> l02 = BehaviorSubject.l0(null);
        m.checkNotNullExpressionValue(l02, "BehaviorSubject.create(null as ChannelId?)");
        this.selectedChannelSubject = l02;
        BehaviorSubject<Unit> l03 = BehaviorSubject.l0(Unit.a);
        m.checkNotNullExpressionValue(l03, "BehaviorSubject.create(Unit)");
        this.refreshUiSubject = l03;
        if (z2) {
            Observable<ViewState.Loaded> t = observeViewStateFromStores$app_productionGoogleRelease(j, l, str).t(new Action1<ViewState.Loaded>() { // from class: com.discord.widgets.guilds.invite.WidgetGuildInviteShareViewModel.1
                public final void call(ViewState.Loaded loaded) {
                    WidgetGuildInviteShareViewModel widgetGuildInviteShareViewModel = WidgetGuildInviteShareViewModel.this;
                    m.checkNotNullExpressionValue(loaded, "viewState");
                    widgetGuildInviteShareViewModel.generateInviteLinkFromViewState(loaded);
                }
            });
            m.checkNotNullExpressionValue(t, "observeViewStateFromStor…romViewState(viewState) }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationBuffered(t), this, null, 2, null), WidgetGuildInviteShareViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(this));
        }
    }
}
