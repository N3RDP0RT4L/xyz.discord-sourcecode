package com.discord.widgets.guilds.invite;

import com.discord.app.AppFragment;
import com.discord.models.domain.ModelInvite;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import org.objectweb.asm.Opcodes;
/* compiled from: InviteJoinHelper.kt */
@e(c = "com.discord.widgets.guilds.invite.InviteJoinHelper$joinViaInvite$4", f = "InviteJoinHelper.kt", l = {78, 103, 112, 113, Opcodes.LAND, Opcodes.IF_ACMPNE, Opcodes.MONITOREXIT}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InviteJoinHelper$joinViaInvite$4 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public final /* synthetic */ String $captchaKey;
    public final /* synthetic */ AppFragment $fragment;
    public final /* synthetic */ ModelInvite $invite;
    public final /* synthetic */ Class $javaClass;
    public final /* synthetic */ String $location;
    public final /* synthetic */ Function2 $onInviteFlowFinished;
    public final /* synthetic */ Function2 $onInvitePostError;
    public final /* synthetic */ Function2 $onInvitePostSuccess;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InviteJoinHelper$joinViaInvite$4(AppFragment appFragment, ModelInvite modelInvite, String str, Function2 function2, String str2, Class cls, Function2 function22, Function2 function23, Continuation continuation) {
        super(2, continuation);
        this.$fragment = appFragment;
        this.$invite = modelInvite;
        this.$location = str;
        this.$onInvitePostSuccess = function2;
        this.$captchaKey = str2;
        this.$javaClass = cls;
        this.$onInvitePostError = function22;
        this.$onInviteFlowFinished = function23;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new InviteJoinHelper$joinViaInvite$4(this.$fragment, this.$invite, this.$location, this.$onInvitePostSuccess, this.$captchaKey, this.$javaClass, this.$onInvitePostError, this.$onInviteFlowFinished, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((InviteJoinHelper$joinViaInvite$4) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:106:0x0273  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x02be  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x02cd  */
    /* JADX WARN: Removed duplicated region for block: B:153:0x035e  */
    /* JADX WARN: Removed duplicated region for block: B:154:0x036c  */
    /* JADX WARN: Removed duplicated region for block: B:166:0x03c0  */
    /* JADX WARN: Removed duplicated region for block: B:169:0x03d3 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x01a1  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x01ac  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x01d2  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x01d4  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x01d7  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x01f2 A[Catch: AppCancellationException -> 0x0053, TRY_ENTER, TryCatch #0 {AppCancellationException -> 0x0053, blocks: (B:10:0x003d, B:12:0x004c, B:90:0x01f2, B:93:0x0229), top: B:181:0x000b }] */
    /* JADX WARN: Removed duplicated region for block: B:96:0x023b A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:97:0x023c  */
    /* JADX WARN: Type inference failed for: r0v47, types: [android.content.Context, T] */
    /* JADX WARN: Type inference failed for: r7v18, types: [androidx.fragment.app.FragmentManager, T, java.lang.Object] */
    @Override // d0.w.i.a.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r19) {
        /*
            Method dump skipped, instructions count: 1034
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.invite.InviteJoinHelper$joinViaInvite$4.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
