package com.discord.widgets.guilds.invite;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.stageinstance.StageInstance;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreMessages;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreUser;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guilds.invite.GuildInvite;
import com.discord.widgets.guilds.invite.InviteSuggestion;
import com.discord.widgets.guilds.invite.InviteSuggestionItemV2;
import d0.g0.t;
import d0.g0.w;
import d0.t.h0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: GuildInviteShareSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000®\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\"\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 W2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003WXYBe\u0012\u000e\u0010\u0005\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u0004\u0012\n\u0010I\u001a\u00060\u0003j\u0002`H\u0012\b\b\u0002\u0010N\u001a\u00020M\u0012\b\b\u0002\u0010<\u001a\u00020;\u0012\b\b\u0002\u0010E\u001a\u00020D\u0012\b\b\u0002\u0010Q\u001a\u00020P\u0012\b\b\u0002\u00104\u001a\u000203\u0012\u000e\b\u0002\u0010T\u001a\b\u0012\u0004\u0012\u00020\u00170S¢\u0006\u0004\bU\u0010VJ\u001b\u0010\u0007\u001a\u00020\u00062\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001b\u0010\u000b\u001a\u00020\u00062\n\u0010\n\u001a\u00060\u0003j\u0002`\tH\u0002¢\u0006\u0004\b\u000b\u0010\bJ\u0017\u0010\u000e\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ-\u0010\u0013\u001a\u00020\u00062\u001c\u0010\u0012\u001a\u0018\u0012\u0004\u0012\u00020\f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00110\u0010H\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u001b\u0010\u0016\u001a\u00020\u00062\n\u0010\u0015\u001a\u00060\u0003j\u0002`\u0004H\u0002¢\u0006\u0004\b\u0016\u0010\bJ\u0017\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\u0017H\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001bH\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\u001f\u0010 JG\u0010$\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\f2\u001c\u0010\u0012\u001a\u0018\u0012\u0004\u0012\u00020\f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00110\u00102\u0006\u0010\u0018\u001a\u00020!2\b\u0010#\u001a\u0004\u0018\u00010\"H\u0002¢\u0006\u0004\b$\u0010%J\u0017\u0010'\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\u0002H\u0015¢\u0006\u0004\b'\u0010(J\u0017\u0010+\u001a\u00020\u00062\u0006\u0010*\u001a\u00020)H\u0007¢\u0006\u0004\b+\u0010,J\u0015\u0010-\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b-\u0010\u000fJ\u0017\u0010.\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\"H\u0007¢\u0006\u0004\b.\u0010/R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R,\u0010\u0012\u001a\u0018\u0012\u0004\u0012\u00020\f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00110\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u00106R\u0018\u00107\u001a\u0004\u0018\u00010\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u00109R\u001e\u0010\u0005\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010:R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u001c\u0010?\u001a\b\u0012\u0004\u0012\u00020\f0>8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u0010@R\u0018\u0010B\u001a\u0004\u0018\u00010A8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010CR\u0016\u0010E\u001a\u00020D8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u0010FR\u0018\u0010\u001c\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010GR\u001d\u0010I\u001a\u00060\u0003j\u0002`H8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010J\u001a\u0004\bK\u0010LR\u0016\u0010N\u001a\u00020M8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010R¨\u0006Z"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "sendInviteToChannel", "(J)V", "Lcom/discord/primitives/UserId;", "userId", "sendInviteToUser", "", "searchQuery", "updateSearchQuery", "(Ljava/lang/String;)V", "", "", "sentInvites", "updateSentInvites", "(Ljava/util/Map;)V", "targetChannelId", "generateInviteForChannel", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;)V", "Lcom/discord/models/domain/ModelInvite;", "invite", "handleInviteCreationSuccess", "(Lcom/discord/models/domain/ModelInvite;)V", "handleInviteCreationFailure", "()V", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState$Valid;", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "generatedInvite", "createViewState", "(Ljava/lang/String;Ljava/util/Map;Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState$Valid;Lcom/discord/widgets/guilds/invite/GuildInvite;)Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;", "viewState", "updateViewState", "(Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;)V", "Lcom/discord/widgets/guilds/invite/InviteSuggestionItemV2;", "item", "sendInvite", "(Lcom/discord/widgets/guilds/invite/InviteSuggestionItemV2;)V", "onSearchTextChanged", "updateInvite", "(Lcom/discord/widgets/guilds/invite/GuildInvite;)V", "", "hasTrackedSuggestionsViewed", "Z", "Lcom/discord/utilities/logging/Logger;", "logger", "Lcom/discord/utilities/logging/Logger;", "Ljava/util/Map;", "currentStoreState", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;", "Ljava/lang/String;", "Ljava/lang/Long;", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Lrx/subjects/BehaviorSubject;", "searchQuerySubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/api/channel/Channel;", "targetChannel", "Lcom/discord/api/channel/Channel;", "Lcom/discord/stores/StoreMessages;", "storeMessages", "Lcom/discord/stores/StoreMessages;", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "Lcom/discord/primitives/GuildId;", "guildId", "J", "getGuildId", "()J", "Lcom/discord/stores/StoreInviteSettings;", "storeInviteSettings", "Lcom/discord/stores/StoreInviteSettings;", "Lcom/discord/widgets/guilds/invite/TargetChannelSelector;", "targetChannelSelector", "Lcom/discord/widgets/guilds/invite/TargetChannelSelector;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Ljava/lang/Long;JLcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreMessages;Lcom/discord/widgets/guilds/invite/TargetChannelSelector;Lcom/discord/utilities/logging/Logger;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildInviteShareSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final Long channelId;
    private StoreState currentStoreState;
    private final long guildId;
    private boolean hasTrackedSuggestionsViewed;
    private GuildInvite invite;
    private final Logger logger;
    private String searchQuery;
    private BehaviorSubject<String> searchQuerySubject;
    private Map<String, ? extends Set<Long>> sentInvites;
    private final StoreAnalytics storeAnalytics;
    private final StoreInviteSettings storeInviteSettings;
    private final StoreMessages storeMessages;
    private Channel targetChannel;
    private final TargetChannelSelector targetChannelSelector;

    /* compiled from: GuildInviteShareSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GuildInviteShareSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GuildInviteShareSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "searchQuery", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<String, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            GuildInviteShareSheetViewModel guildInviteShareSheetViewModel = GuildInviteShareSheetViewModel.this;
            m.checkNotNullExpressionValue(str, "searchQuery");
            guildInviteShareSheetViewModel.updateSearchQuery(str);
        }
    }

    /* compiled from: GuildInviteShareSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016JQ\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager;", "inviteSuggestionsManager", "Lcom/discord/stores/StoreInviteSettings;", "storeInviteSettings", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreStageInstances;", "storeStageInstances", "Lrx/Observable;", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;", "observeStoreState", "(JLcom/discord/widgets/guilds/invite/InviteSuggestionsManager;Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreStageInstances;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, InviteSuggestionsManager inviteSuggestionsManager, StoreInviteSettings storeInviteSettings, StoreUser storeUser, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreStageInstances storeStageInstances) {
            Observable<StoreState> e = Observable.e(storeInviteSettings.getInviteSettings(), storeInviteSettings.getInvitableChannels(j), StoreUser.observeMe$default(storeUser, false, 1, null), storeChannels.observeDMs(), storeGuilds.observeGuild(j), inviteSuggestionsManager.observeInviteSuggestions(), storeStageInstances.observeStageInstancesForGuild(j), GuildInviteShareSheetViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(e, "Observable.combineLatest…      )\n        }\n      }");
            return e;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildInviteShareSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState$Valid;", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState$Invalid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: GuildInviteShareSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState$Invalid;", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends StoreState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: GuildInviteShareSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001Bk\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0006\u0010\u001a\u001a\u00020\u000b\u0012\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\b0\u000e\u0012\u0006\u0010\u001c\u001a\u00020\u0011\u0012\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00140\u000e\u0012\u0016\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u0005¢\u0006\u0004\b8\u00109J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0016\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u000eHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0010J \u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u0005HÆ\u0003¢\u0006\u0004\b\u0017\u0010\nJ\u0082\u0001\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00022\u0018\b\u0002\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\b\b\u0002\u0010\u001a\u001a\u00020\u000b2\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\b0\u000e2\b\b\u0002\u0010\u001c\u001a\u00020\u00112\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00140\u000e2\u0018\b\u0002\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u0005HÆ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\"\u001a\u00020!HÖ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u001a\u0010*\u001a\u00020)2\b\u0010(\u001a\u0004\u0018\u00010'HÖ\u0003¢\u0006\u0004\b*\u0010+R\u0019\u0010\u001c\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010,\u001a\u0004\b-\u0010\u0013R)\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010.\u001a\u0004\b/\u0010\nR\u0019\u0010\u001a\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00100\u001a\u0004\b1\u0010\rR)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010.\u001a\u0004\b2\u0010\nR\u001f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\b0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00103\u001a\u0004\b4\u0010\u0010R\u001f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00140\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00103\u001a\u0004\b5\u0010\u0010R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00106\u001a\u0004\b7\u0010\u0004¨\u0006:"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState$Valid;", "Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState;", "Lcom/discord/models/domain/ModelInvite$Settings;", "component1", "()Lcom/discord/models/domain/ModelInvite$Settings;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component2", "()Ljava/util/Map;", "Lcom/discord/models/user/MeUser;", "component3", "()Lcom/discord/models/user/MeUser;", "", "component4", "()Ljava/util/List;", "Lcom/discord/models/guild/Guild;", "component5", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/widgets/guilds/invite/InviteSuggestion;", "component6", "Lcom/discord/api/stageinstance/StageInstance;", "component7", "inviteSettings", "invitableChannels", "me", "dms", "guild", "inviteSuggestions", "guildStageInstances", "copy", "(Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/Map;Lcom/discord/models/user/MeUser;Ljava/util/List;Lcom/discord/models/guild/Guild;Ljava/util/List;Ljava/util/Map;)Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$StoreState$Valid;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/util/Map;", "getGuildStageInstances", "Lcom/discord/models/user/MeUser;", "getMe", "getInvitableChannels", "Ljava/util/List;", "getDms", "getInviteSuggestions", "Lcom/discord/models/domain/ModelInvite$Settings;", "getInviteSettings", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/Map;Lcom/discord/models/user/MeUser;Ljava/util/List;Lcom/discord/models/guild/Guild;Ljava/util/List;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends StoreState {
            private final List<Channel> dms;
            private final Guild guild;
            private final Map<Long, StageInstance> guildStageInstances;
            private final Map<Long, Channel> invitableChannels;
            private final ModelInvite.Settings inviteSettings;
            private final List<InviteSuggestion> inviteSuggestions;

            /* renamed from: me  reason: collision with root package name */
            private final MeUser f2828me;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Valid(ModelInvite.Settings settings, Map<Long, Channel> map, MeUser meUser, List<Channel> list, Guild guild, List<? extends InviteSuggestion> list2, Map<Long, StageInstance> map2) {
                super(null);
                m.checkNotNullParameter(settings, "inviteSettings");
                m.checkNotNullParameter(map, "invitableChannels");
                m.checkNotNullParameter(meUser, "me");
                m.checkNotNullParameter(list, "dms");
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(list2, "inviteSuggestions");
                m.checkNotNullParameter(map2, "guildStageInstances");
                this.inviteSettings = settings;
                this.invitableChannels = map;
                this.f2828me = meUser;
                this.dms = list;
                this.guild = guild;
                this.inviteSuggestions = list2;
                this.guildStageInstances = map2;
            }

            public static /* synthetic */ Valid copy$default(Valid valid, ModelInvite.Settings settings, Map map, MeUser meUser, List list, Guild guild, List list2, Map map2, int i, Object obj) {
                if ((i & 1) != 0) {
                    settings = valid.inviteSettings;
                }
                Map<Long, Channel> map3 = map;
                if ((i & 2) != 0) {
                    map3 = valid.invitableChannels;
                }
                Map map4 = map3;
                if ((i & 4) != 0) {
                    meUser = valid.f2828me;
                }
                MeUser meUser2 = meUser;
                List<Channel> list3 = list;
                if ((i & 8) != 0) {
                    list3 = valid.dms;
                }
                List list4 = list3;
                if ((i & 16) != 0) {
                    guild = valid.guild;
                }
                Guild guild2 = guild;
                List<InviteSuggestion> list5 = list2;
                if ((i & 32) != 0) {
                    list5 = valid.inviteSuggestions;
                }
                List list6 = list5;
                Map<Long, StageInstance> map5 = map2;
                if ((i & 64) != 0) {
                    map5 = valid.guildStageInstances;
                }
                return valid.copy(settings, map4, meUser2, list4, guild2, list6, map5);
            }

            public final ModelInvite.Settings component1() {
                return this.inviteSettings;
            }

            public final Map<Long, Channel> component2() {
                return this.invitableChannels;
            }

            public final MeUser component3() {
                return this.f2828me;
            }

            public final List<Channel> component4() {
                return this.dms;
            }

            public final Guild component5() {
                return this.guild;
            }

            public final List<InviteSuggestion> component6() {
                return this.inviteSuggestions;
            }

            public final Map<Long, StageInstance> component7() {
                return this.guildStageInstances;
            }

            public final Valid copy(ModelInvite.Settings settings, Map<Long, Channel> map, MeUser meUser, List<Channel> list, Guild guild, List<? extends InviteSuggestion> list2, Map<Long, StageInstance> map2) {
                m.checkNotNullParameter(settings, "inviteSettings");
                m.checkNotNullParameter(map, "invitableChannels");
                m.checkNotNullParameter(meUser, "me");
                m.checkNotNullParameter(list, "dms");
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(list2, "inviteSuggestions");
                m.checkNotNullParameter(map2, "guildStageInstances");
                return new Valid(settings, map, meUser, list, guild, list2, map2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.inviteSettings, valid.inviteSettings) && m.areEqual(this.invitableChannels, valid.invitableChannels) && m.areEqual(this.f2828me, valid.f2828me) && m.areEqual(this.dms, valid.dms) && m.areEqual(this.guild, valid.guild) && m.areEqual(this.inviteSuggestions, valid.inviteSuggestions) && m.areEqual(this.guildStageInstances, valid.guildStageInstances);
            }

            public final List<Channel> getDms() {
                return this.dms;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final Map<Long, StageInstance> getGuildStageInstances() {
                return this.guildStageInstances;
            }

            public final Map<Long, Channel> getInvitableChannels() {
                return this.invitableChannels;
            }

            public final ModelInvite.Settings getInviteSettings() {
                return this.inviteSettings;
            }

            public final List<InviteSuggestion> getInviteSuggestions() {
                return this.inviteSuggestions;
            }

            public final MeUser getMe() {
                return this.f2828me;
            }

            public int hashCode() {
                ModelInvite.Settings settings = this.inviteSettings;
                int i = 0;
                int hashCode = (settings != null ? settings.hashCode() : 0) * 31;
                Map<Long, Channel> map = this.invitableChannels;
                int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
                MeUser meUser = this.f2828me;
                int hashCode3 = (hashCode2 + (meUser != null ? meUser.hashCode() : 0)) * 31;
                List<Channel> list = this.dms;
                int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
                Guild guild = this.guild;
                int hashCode5 = (hashCode4 + (guild != null ? guild.hashCode() : 0)) * 31;
                List<InviteSuggestion> list2 = this.inviteSuggestions;
                int hashCode6 = (hashCode5 + (list2 != null ? list2.hashCode() : 0)) * 31;
                Map<Long, StageInstance> map2 = this.guildStageInstances;
                if (map2 != null) {
                    i = map2.hashCode();
                }
                return hashCode6 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(inviteSettings=");
                R.append(this.inviteSettings);
                R.append(", invitableChannels=");
                R.append(this.invitableChannels);
                R.append(", me=");
                R.append(this.f2828me);
                R.append(", dms=");
                R.append(this.dms);
                R.append(", guild=");
                R.append(this.guild);
                R.append(", inviteSuggestions=");
                R.append(this.inviteSuggestions);
                R.append(", guildStageInstances=");
                return a.L(R, this.guildStageInstances, ")");
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildInviteShareSheetViewModel(java.lang.Long r21, long r22, com.discord.stores.StoreInviteSettings r24, com.discord.stores.StoreAnalytics r25, com.discord.stores.StoreMessages r26, com.discord.widgets.guilds.invite.TargetChannelSelector r27, com.discord.utilities.logging.Logger r28, rx.Observable r29, int r30, kotlin.jvm.internal.DefaultConstructorMarker r31) {
        /*
            r20 = this;
            r0 = r30
            r1 = r0 & 4
            if (r1 == 0) goto Le
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreInviteSettings r1 = r1.getInviteSettings()
            r6 = r1
            goto L10
        Le:
            r6 = r24
        L10:
            r1 = r0 & 8
            if (r1 == 0) goto L1c
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAnalytics r1 = r1.getAnalytics()
            r7 = r1
            goto L1e
        L1c:
            r7 = r25
        L1e:
            r1 = r0 & 16
            if (r1 == 0) goto L2a
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreMessages r1 = r1.getMessages()
            r8 = r1
            goto L2c
        L2a:
            r8 = r26
        L2c:
            r1 = r0 & 32
            if (r1 == 0) goto L37
            com.discord.widgets.guilds.invite.TargetChannelSelector r1 = new com.discord.widgets.guilds.invite.TargetChannelSelector
            r1.<init>()
            r9 = r1
            goto L39
        L37:
            r9 = r27
        L39:
            r1 = r0 & 64
            if (r1 == 0) goto L41
            com.discord.app.AppLog r1 = com.discord.app.AppLog.g
            r10 = r1
            goto L43
        L41:
            r10 = r28
        L43:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L84
            com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel$Companion r11 = com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel.Companion
            com.discord.widgets.guilds.invite.InviteSuggestionsManager r14 = new com.discord.widgets.guilds.invite.InviteSuggestionsManager
            r0 = 0
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 31
            r12 = 0
            r24 = r14
            r25 = r0
            r26 = r1
            r27 = r2
            r28 = r3
            r29 = r4
            r30 = r5
            r31 = r12
            r24.<init>(r25, r26, r27, r28, r29, r30, r31)
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreInviteSettings r15 = r0.getInviteSettings()
            com.discord.stores.StoreUser r16 = r0.getUsers()
            com.discord.stores.StoreChannels r17 = r0.getChannels()
            com.discord.stores.StoreGuilds r18 = r0.getGuilds()
            com.discord.stores.StoreStageInstances r19 = r0.getStageInstances()
            r12 = r22
            rx.Observable r0 = com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel.Companion.access$observeStoreState(r11, r12, r14, r15, r16, r17, r18, r19)
            r11 = r0
            goto L86
        L84:
            r11 = r29
        L86:
            r2 = r20
            r3 = r21
            r4 = r22
            r2.<init>(r3, r4, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guilds.invite.GuildInviteShareSheetViewModel.<init>(java.lang.Long, long, com.discord.stores.StoreInviteSettings, com.discord.stores.StoreAnalytics, com.discord.stores.StoreMessages, com.discord.widgets.guilds.invite.TargetChannelSelector, com.discord.utilities.logging.Logger, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final ViewState createViewState(String str, Map<String, ? extends Set<Long>> map, StoreState.Valid valid, GuildInvite guildInvite) {
        String str2;
        Long l;
        List list;
        Object obj;
        boolean z2;
        Collections.sort(new ArrayList(valid.getInvitableChannels().values()), ChannelUtils.h(Channel.Companion));
        if (guildInvite == null || (str2 = guildInvite.toLink()) == null) {
            str2 = BuildConfig.HOST_INVITE;
        }
        Set<Long> set = map.get(str2);
        if (set == null) {
            set = n0.emptySet();
        }
        Set<Long> set2 = set;
        List<InviteSuggestion> inviteSuggestions = valid.getInviteSuggestions();
        if (!t.isBlank(str)) {
            ArrayList arrayList = new ArrayList();
            for (Object obj2 : inviteSuggestions) {
                InviteSuggestion inviteSuggestion = (InviteSuggestion) obj2;
                if (inviteSuggestion instanceof InviteSuggestion.ChannelItem) {
                    z2 = w.contains((CharSequence) ChannelUtils.c(((InviteSuggestion.ChannelItem) inviteSuggestion).getChannel()), (CharSequence) str, true);
                } else if (inviteSuggestion instanceof InviteSuggestion.UserSuggestion) {
                    z2 = w.contains((CharSequence) ((InviteSuggestion.UserSuggestion) inviteSuggestion).getUser().getUsername(), (CharSequence) str, true);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                if (z2) {
                    arrayList.add(obj2);
                }
            }
            inviteSuggestions = arrayList;
        }
        List<Channel> dms = valid.getDms();
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(dms, 10));
        Iterator<T> it = dms.iterator();
        while (true) {
            l = null;
            if (!it.hasNext()) {
                break;
            }
            Channel channel = (Channel) it.next();
            User a = ChannelUtils.a(channel);
            if (a != null) {
                l = Long.valueOf(a.getId());
            }
            arrayList2.add(d0.o.to(l, Long.valueOf(channel.h())));
        }
        Map map2 = h0.toMap(arrayList2);
        if ((!inviteSuggestions.isEmpty()) || t.isBlank(str)) {
            list = new ArrayList(d0.t.o.collectionSizeOrDefault(inviteSuggestions, 10));
            for (InviteSuggestion inviteSuggestion2 : inviteSuggestions) {
                if (inviteSuggestion2 instanceof InviteSuggestion.ChannelItem) {
                    InviteSuggestion.ChannelItem channelItem = (InviteSuggestion.ChannelItem) inviteSuggestion2;
                    obj = new InviteSuggestionItemV2.ChannelItem(channelItem.getChannel(), set2.contains(Long.valueOf(channelItem.getChannel().h())), str);
                } else if (inviteSuggestion2 instanceof InviteSuggestion.UserSuggestion) {
                    InviteSuggestion.UserSuggestion userSuggestion = (InviteSuggestion.UserSuggestion) inviteSuggestion2;
                    obj = new InviteSuggestionItemV2.UserItem(userSuggestion.getUser(), u.contains(set2, map2.get(Long.valueOf(userSuggestion.getUser().getId()))), str);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                list.add(obj);
            }
        } else {
            list = d0.t.m.listOf(InviteSuggestionItemV2.SearchNoResultsItem.INSTANCE);
        }
        boolean z3 = guildInvite != null ? !guildInvite.isStaticInvite() : true;
        ModelInvite.Settings inviteSettings = valid.getInviteSettings();
        Map<Long, Channel> invitableChannels = valid.getInvitableChannels();
        if (guildInvite != null) {
            l = guildInvite.getChannelId();
        }
        return new ViewState(guildInvite, inviteSettings, list, invitableChannels.get(l), str, map, z3, valid.getGuild().getId());
    }

    private final void generateInviteForChannel(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(StoreInviteSettings.generateInvite$default(this.storeInviteSettings, j, null, 2, null), false, 1, null), this, null, 2, null), GuildInviteShareSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new GuildInviteShareSheetViewModel$generateInviteForChannel$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildInviteShareSheetViewModel$generateInviteForChannel$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleInviteCreationFailure() {
        Guild guild;
        String vanityUrlCode;
        StoreState storeState = this.currentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null && (vanityUrlCode = (guild = valid.getGuild()).getVanityUrlCode()) != null) {
            ModelInvite createForStaticUrl = ModelInvite.createForStaticUrl(vanityUrlCode, GuildUtilsKt.createApiGuild(guild));
            GuildInvite.Companion companion = GuildInvite.Companion;
            m.checkNotNullExpressionValue(createForStaticUrl, "vanityUrlInvite");
            GuildInvite createFromModelInvite = companion.createFromModelInvite(createForStaticUrl);
            this.invite = createFromModelInvite;
            updateViewState(createViewState(this.searchQuery, this.sentInvites, valid, createFromModelInvite));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleInviteCreationSuccess(ModelInvite modelInvite) {
        GuildInvite createFromModelInvite = GuildInvite.Companion.createFromModelInvite(modelInvite);
        this.invite = createFromModelInvite;
        StoreState storeState = this.currentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null) {
            updateViewState(createViewState(this.searchQuery, this.sentInvites, valid, createFromModelInvite));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        StoreState storeState2 = this.currentStoreState;
        if (m.areEqual(storeState, StoreState.Invalid.INSTANCE)) {
            Logger logger = this.logger;
            StringBuilder R = a.R("invalid StoreState in ");
            R.append(GuildInviteShareSheetViewModel.class.getSimpleName());
            Logger.e$default(logger, R.toString(), null, null, 6, null);
            return;
        }
        if (storeState instanceof StoreState.Valid) {
            Channel channel = this.targetChannel;
            StoreState.Valid valid = (StoreState.Valid) storeState;
            Channel targetChannel = this.targetChannelSelector.getTargetChannel(valid.getInvitableChannels(), this.channelId);
            Guild guild = valid.getGuild();
            String vanityUrlCode = valid.getGuild().getVanityUrlCode();
            StageInstance stageInstance = valid.getGuildStageInstances().get(this.channelId);
            String d = stageInstance != null ? stageInstance.d() : null;
            if (this.invite == null) {
                boolean z2 = true;
                boolean z3 = this.channelId == null;
                if (storeState2 == null) {
                    if (!(vanityUrlCode == null || vanityUrlCode.length() == 0)) {
                        z2 = false;
                    }
                    if (!z2 && z3) {
                        ModelInvite createForStaticUrl = ModelInvite.createForStaticUrl(vanityUrlCode, GuildUtilsKt.createApiGuild(guild));
                        GuildInvite.Companion companion = GuildInvite.Companion;
                        m.checkNotNullExpressionValue(createForStaticUrl, "vanityUrlInvite");
                        this.invite = companion.createFromModelInvite(createForStaticUrl);
                    }
                }
                if (channel == null && targetChannel != null) {
                    generateInviteForChannel(targetChannel.h());
                } else if (d != null) {
                    ModelInvite createForStaticUrl2 = ModelInvite.createForStaticUrl(d, GuildUtilsKt.createApiGuild(guild));
                    GuildInvite.Companion companion2 = GuildInvite.Companion;
                    m.checkNotNullExpressionValue(createForStaticUrl2, "stageInstanceInvite");
                    this.invite = companion2.createFromModelInvite(createForStaticUrl2);
                }
            }
            this.targetChannel = targetChannel;
            updateViewState(createViewState(this.searchQuery, this.sentInvites, valid, this.invite));
        }
        this.currentStoreState = storeState;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void sendInviteToChannel(long j) {
        ViewState viewState;
        GuildInvite guildInvite;
        StoreState storeState = this.currentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null && (viewState = getViewState()) != null && (guildInvite = this.invite) != null) {
            String link = guildInvite.toLink();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreMessages.sendMessage$default(this.storeMessages, j, valid.getMe(), link, null, null, null, null, null, null, null, null, null, null, null, null, 32736, null), this, null, 2, null), GuildInviteShareSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildInviteShareSheetViewModel$sendInviteToChannel$1(this, guildInvite, viewState, link));
        }
    }

    private final void sendInviteToUser(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().createOrFetchDM(j), false, 1, null), this, null, 2, null), GuildInviteShareSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildInviteShareSheetViewModel$sendInviteToUser$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void updateSearchQuery(String str) {
        StoreState storeState = this.currentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null && !m.areEqual(str, this.searchQuery)) {
            this.searchQuery = str;
            updateViewState(createViewState(str, this.sentInvites, valid, this.invite));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void updateSentInvites(Map<String, ? extends Set<Long>> map) {
        this.sentInvites = map;
        StoreState storeState = this.currentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null) {
            updateViewState(createViewState(this.searchQuery, map, valid, this.invite));
        }
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final void onSearchTextChanged(String str) {
        m.checkNotNullParameter(str, "searchQuery");
        this.searchQuerySubject.onNext(str);
    }

    @MainThread
    public final void sendInvite(InviteSuggestionItemV2 inviteSuggestionItemV2) {
        m.checkNotNullParameter(inviteSuggestionItemV2, "item");
        if (inviteSuggestionItemV2 instanceof InviteSuggestionItemV2.ChannelItem) {
            sendInviteToChannel(((InviteSuggestionItemV2.ChannelItem) inviteSuggestionItemV2).getChannel().h());
        } else if (inviteSuggestionItemV2 instanceof InviteSuggestionItemV2.UserItem) {
            sendInviteToUser(((InviteSuggestionItemV2.UserItem) inviteSuggestionItemV2).getUser().getId());
        }
    }

    @MainThread
    public final void updateInvite(GuildInvite guildInvite) {
        m.checkNotNullParameter(guildInvite, "invite");
        this.invite = guildInvite;
        ViewState viewState = getViewState();
        if (viewState != null) {
            StoreState storeState = this.currentStoreState;
            if (!(storeState instanceof StoreState.Valid)) {
                storeState = null;
            }
            StoreState.Valid valid = (StoreState.Valid) storeState;
            if (valid != null) {
                updateViewState(ViewState.copy$default(viewState, guildInvite, null, null, valid.getInvitableChannels().get(guildInvite.getChannelId()), null, null, false, 0L, 246, null));
            }
        }
    }

    @MainThread
    public void updateViewState(ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        super.updateViewState((GuildInviteShareSheetViewModel) viewState);
        if (viewState.getHasSearchResults() && !this.hasTrackedSuggestionsViewed) {
            long guildId = viewState.getGuildId();
            List<InviteSuggestionItemV2> inviteSuggestionItems = viewState.getInviteSuggestionItems();
            StoreAnalytics storeAnalytics = this.storeAnalytics;
            ArrayList<InviteSuggestionItemV2.ChannelItem> arrayList = new ArrayList();
            for (Object obj : inviteSuggestionItems) {
                if (obj instanceof InviteSuggestionItemV2.ChannelItem) {
                    arrayList.add(obj);
                }
            }
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
            for (InviteSuggestionItemV2.ChannelItem channelItem : arrayList) {
                arrayList2.add(channelItem.getChannel());
            }
            ArrayList<InviteSuggestionItemV2.UserItem> arrayList3 = new ArrayList();
            for (Object obj2 : inviteSuggestionItems) {
                if (obj2 instanceof InviteSuggestionItemV2.UserItem) {
                    arrayList3.add(obj2);
                }
            }
            ArrayList arrayList4 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList3, 10));
            for (InviteSuggestionItemV2.UserItem userItem : arrayList3) {
                arrayList4.add(userItem.getUser());
            }
            storeAnalytics.inviteSuggestionOpened(guildId, arrayList2, arrayList4);
            this.hasTrackedSuggestionsViewed = true;
        }
    }

    /* compiled from: GuildInviteShareSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u001c\b\u0086\b\u0018\u00002\u00020\u0001Bo\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u0005\u0012\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\b\u0010!\u001a\u0004\u0018\u00010\f\u0012\b\b\u0002\u0010\"\u001a\u00020\u000f\u0012\u001c\u0010#\u001a\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u00130\u0012\u0012\u0006\u0010$\u001a\u00020\u0018\u0012\n\u0010%\u001a\u00060\u0014j\u0002`\u001b¢\u0006\u0004\bC\u0010DJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J&\u0010\u0016\u001a\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u00130\u0012HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u001c\u001a\u00060\u0014j\u0002`\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0086\u0001\u0010&\u001a\u00020\u00002\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\b2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\"\u001a\u00020\u000f2\u001e\b\u0002\u0010#\u001a\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u00130\u00122\b\b\u0002\u0010$\u001a\u00020\u00182\f\b\u0002\u0010%\u001a\u00060\u0014j\u0002`\u001bHÆ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010(\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b(\u0010\u0011J\u0010\u0010*\u001a\u00020)HÖ\u0001¢\u0006\u0004\b*\u0010+J\u001a\u0010-\u001a\u00020\u00182\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b-\u0010.R\u0019\u0010/\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u001aR/\u0010#\u001a\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b#\u00102\u001a\u0004\b3\u0010\u0017R\u001d\u0010%\u001a\u00060\u0014j\u0002`\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u00104\u001a\u0004\b5\u0010\u001dR\u001b\u0010!\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00106\u001a\u0004\b7\u0010\u000eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00108\u001a\u0004\b9\u0010\u0007R\u001f\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010:\u001a\u0004\b;\u0010\u000bR\u0019\u0010\"\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010<\u001a\u0004\b=\u0010\u0011R\u0019\u0010$\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b$\u00100\u001a\u0004\b>\u0010\u001aR\u0019\u0010?\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b?\u00100\u001a\u0004\b@\u0010\u001aR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010A\u001a\u0004\bB\u0010\u0004¨\u0006E"}, d2 = {"Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;", "", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "component1", "()Lcom/discord/widgets/guilds/invite/GuildInvite;", "Lcom/discord/models/domain/ModelInvite$Settings;", "component2", "()Lcom/discord/models/domain/ModelInvite$Settings;", "", "Lcom/discord/widgets/guilds/invite/InviteSuggestionItemV2;", "component3", "()Ljava/util/List;", "Lcom/discord/api/channel/Channel;", "component4", "()Lcom/discord/api/channel/Channel;", "", "component5", "()Ljava/lang/String;", "", "", "", "Lcom/discord/primitives/ChannelId;", "component6", "()Ljava/util/Map;", "", "component7", "()Z", "Lcom/discord/primitives/GuildId;", "component8", "()J", "invite", "inviteSettings", "inviteSuggestionItems", "channel", "searchQuery", "sentInvites", "showInviteSettings", "guildId", "copy", "(Lcom/discord/widgets/guilds/invite/GuildInvite;Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/List;Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/Map;ZJ)Lcom/discord/widgets/guilds/invite/GuildInviteShareSheetViewModel$ViewState;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "hasResults", "Z", "getHasResults", "Ljava/util/Map;", "getSentInvites", "J", "getGuildId", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/domain/ModelInvite$Settings;", "getInviteSettings", "Ljava/util/List;", "getInviteSuggestionItems", "Ljava/lang/String;", "getSearchQuery", "getShowInviteSettings", "hasSearchResults", "getHasSearchResults", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "getInvite", HookHelper.constructorName, "(Lcom/discord/widgets/guilds/invite/GuildInvite;Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/List;Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/Map;ZJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Channel channel;
        private final long guildId;
        private final boolean hasResults;
        private final boolean hasSearchResults;
        private final GuildInvite invite;
        private final ModelInvite.Settings inviteSettings;
        private final List<InviteSuggestionItemV2> inviteSuggestionItems;
        private final String searchQuery;
        private final Map<String, Set<Long>> sentInvites;
        private final boolean showInviteSettings;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(GuildInvite guildInvite, ModelInvite.Settings settings, List<? extends InviteSuggestionItemV2> list, Channel channel, String str, Map<String, ? extends Set<Long>> map, boolean z2, long j) {
            m.checkNotNullParameter(list, "inviteSuggestionItems");
            m.checkNotNullParameter(str, "searchQuery");
            m.checkNotNullParameter(map, "sentInvites");
            this.invite = guildInvite;
            this.inviteSettings = settings;
            this.inviteSuggestionItems = list;
            this.channel = channel;
            this.searchQuery = str;
            this.sentInvites = map;
            this.showInviteSettings = z2;
            this.guildId = j;
            boolean z3 = true;
            this.hasResults = !list.isEmpty();
            this.hasSearchResults = (list.size() <= 1 || (list.get(0) instanceof InviteSuggestionItemV2.SearchNoResultsItem)) ? false : z3;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, GuildInvite guildInvite, ModelInvite.Settings settings, List list, Channel channel, String str, Map map, boolean z2, long j, int i, Object obj) {
            return viewState.copy((i & 1) != 0 ? viewState.invite : guildInvite, (i & 2) != 0 ? viewState.inviteSettings : settings, (i & 4) != 0 ? viewState.inviteSuggestionItems : list, (i & 8) != 0 ? viewState.channel : channel, (i & 16) != 0 ? viewState.searchQuery : str, (i & 32) != 0 ? viewState.sentInvites : map, (i & 64) != 0 ? viewState.showInviteSettings : z2, (i & 128) != 0 ? viewState.guildId : j);
        }

        public final GuildInvite component1() {
            return this.invite;
        }

        public final ModelInvite.Settings component2() {
            return this.inviteSettings;
        }

        public final List<InviteSuggestionItemV2> component3() {
            return this.inviteSuggestionItems;
        }

        public final Channel component4() {
            return this.channel;
        }

        public final String component5() {
            return this.searchQuery;
        }

        public final Map<String, Set<Long>> component6() {
            return this.sentInvites;
        }

        public final boolean component7() {
            return this.showInviteSettings;
        }

        public final long component8() {
            return this.guildId;
        }

        public final ViewState copy(GuildInvite guildInvite, ModelInvite.Settings settings, List<? extends InviteSuggestionItemV2> list, Channel channel, String str, Map<String, ? extends Set<Long>> map, boolean z2, long j) {
            m.checkNotNullParameter(list, "inviteSuggestionItems");
            m.checkNotNullParameter(str, "searchQuery");
            m.checkNotNullParameter(map, "sentInvites");
            return new ViewState(guildInvite, settings, list, channel, str, map, z2, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.invite, viewState.invite) && m.areEqual(this.inviteSettings, viewState.inviteSettings) && m.areEqual(this.inviteSuggestionItems, viewState.inviteSuggestionItems) && m.areEqual(this.channel, viewState.channel) && m.areEqual(this.searchQuery, viewState.searchQuery) && m.areEqual(this.sentInvites, viewState.sentInvites) && this.showInviteSettings == viewState.showInviteSettings && this.guildId == viewState.guildId;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final boolean getHasResults() {
            return this.hasResults;
        }

        public final boolean getHasSearchResults() {
            return this.hasSearchResults;
        }

        public final GuildInvite getInvite() {
            return this.invite;
        }

        public final ModelInvite.Settings getInviteSettings() {
            return this.inviteSettings;
        }

        public final List<InviteSuggestionItemV2> getInviteSuggestionItems() {
            return this.inviteSuggestionItems;
        }

        public final String getSearchQuery() {
            return this.searchQuery;
        }

        public final Map<String, Set<Long>> getSentInvites() {
            return this.sentInvites;
        }

        public final boolean getShowInviteSettings() {
            return this.showInviteSettings;
        }

        public int hashCode() {
            GuildInvite guildInvite = this.invite;
            int i = 0;
            int hashCode = (guildInvite != null ? guildInvite.hashCode() : 0) * 31;
            ModelInvite.Settings settings = this.inviteSettings;
            int hashCode2 = (hashCode + (settings != null ? settings.hashCode() : 0)) * 31;
            List<InviteSuggestionItemV2> list = this.inviteSuggestionItems;
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode4 = (hashCode3 + (channel != null ? channel.hashCode() : 0)) * 31;
            String str = this.searchQuery;
            int hashCode5 = (hashCode4 + (str != null ? str.hashCode() : 0)) * 31;
            Map<String, Set<Long>> map = this.sentInvites;
            if (map != null) {
                i = map.hashCode();
            }
            int i2 = (hashCode5 + i) * 31;
            boolean z2 = this.showInviteSettings;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return b.a(this.guildId) + ((i2 + i3) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(invite=");
            R.append(this.invite);
            R.append(", inviteSettings=");
            R.append(this.inviteSettings);
            R.append(", inviteSuggestionItems=");
            R.append(this.inviteSuggestionItems);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", searchQuery=");
            R.append(this.searchQuery);
            R.append(", sentInvites=");
            R.append(this.sentInvites);
            R.append(", showInviteSettings=");
            R.append(this.showInviteSettings);
            R.append(", guildId=");
            return a.B(R, this.guildId, ")");
        }

        public /* synthetic */ ViewState(GuildInvite guildInvite, ModelInvite.Settings settings, List list, Channel channel, String str, Map map, boolean z2, long j, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(guildInvite, settings, list, channel, (i & 16) != 0 ? "" : str, map, z2, j);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildInviteShareSheetViewModel(Long l, long j, StoreInviteSettings storeInviteSettings, StoreAnalytics storeAnalytics, StoreMessages storeMessages, TargetChannelSelector targetChannelSelector, Logger logger, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeInviteSettings, "storeInviteSettings");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(storeMessages, "storeMessages");
        m.checkNotNullParameter(targetChannelSelector, "targetChannelSelector");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.channelId = l;
        this.guildId = j;
        this.storeInviteSettings = storeInviteSettings;
        this.storeAnalytics = storeAnalytics;
        this.storeMessages = storeMessages;
        this.targetChannelSelector = targetChannelSelector;
        this.logger = logger;
        BehaviorSubject<String> l0 = BehaviorSubject.l0("");
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(\"\")");
        this.searchQuerySubject = l0;
        this.searchQuery = "";
        this.sentInvites = h0.emptyMap();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GuildInviteShareSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        Observable<String> o = this.searchQuerySubject.o(250L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "searchQuerySubject\n     …0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(o, this, null, 2, null), GuildInviteShareSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
