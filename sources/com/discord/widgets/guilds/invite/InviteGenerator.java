package com.discord.widgets.guilds.invite;

import andhook.lib.HookHelper;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.app.AppFragment;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreStream;
import com.discord.utilities.error.Error;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import java.io.Closeable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
/* compiled from: InviteGenerator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0002 !B\u0007¢\u0006\u0004\b\u001f\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ!\u0010\u000e\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\t2\n\u0010\r\u001a\u00060\u000bj\u0002`\f¢\u0006\u0004\b\u000e\u0010\u000fJ!\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\r\u001a\u00060\u000bj\u0002`\f¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0014\u0010\bR\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR:\u0010\u001d\u001a&\u0012\f\u0012\n \u001c*\u0004\u0018\u00010\u00160\u0016 \u001c*\u0012\u0012\f\u0012\n \u001c*\u0004\u0018\u00010\u00160\u0016\u0018\u00010\u001b0\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006\""}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteGenerator;", "Ljava/io/Closeable;", "Lcom/discord/models/domain/ModelInvite;", "invite", "", "handleGeneratedInvite", "(Lcom/discord/models/domain/ModelInvite;)V", "handleRestCallFailed", "()V", "Lcom/discord/app/AppFragment;", "fragment", "", "Lcom/discord/primitives/ChannelId;", "channelId", "generate", "(Lcom/discord/app/AppFragment;J)V", "Lcom/discord/app/AppComponent;", "appComponent", "generateForAppComponent", "(Lcom/discord/app/AppComponent;J)V", "close", "Lrx/Observable;", "Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;", "generationState", "Lrx/Observable;", "getGenerationState", "()Lrx/Observable;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "generationStateSubject", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "GenerationState", "InviteGenerationState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InviteGenerator implements Closeable {
    private final Observable<InviteGenerationState> generationState;
    private final BehaviorSubject<InviteGenerationState> generationStateSubject;

    /* compiled from: InviteGenerator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "INIT", "GENERATING", "SUCCESS", "FAILURE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum GenerationState {
        INIT,
        GENERATING,
        SUCCESS,
        FAILURE
    }

    /* compiled from: InviteGenerator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;", "", "Lcom/discord/models/domain/ModelInvite;", "component1", "()Lcom/discord/models/domain/ModelInvite;", "Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;", "component2", "()Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;", "lastGeneratedInvite", "state", "copy", "(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;)Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;", "getState", "Lcom/discord/models/domain/ModelInvite;", "getLastGeneratedInvite", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class InviteGenerationState {
        private final ModelInvite lastGeneratedInvite;
        private final GenerationState state;

        public InviteGenerationState() {
            this(null, null, 3, null);
        }

        public InviteGenerationState(ModelInvite modelInvite, GenerationState generationState) {
            m.checkNotNullParameter(generationState, "state");
            this.lastGeneratedInvite = modelInvite;
            this.state = generationState;
        }

        public static /* synthetic */ InviteGenerationState copy$default(InviteGenerationState inviteGenerationState, ModelInvite modelInvite, GenerationState generationState, int i, Object obj) {
            if ((i & 1) != 0) {
                modelInvite = inviteGenerationState.lastGeneratedInvite;
            }
            if ((i & 2) != 0) {
                generationState = inviteGenerationState.state;
            }
            return inviteGenerationState.copy(modelInvite, generationState);
        }

        public final ModelInvite component1() {
            return this.lastGeneratedInvite;
        }

        public final GenerationState component2() {
            return this.state;
        }

        public final InviteGenerationState copy(ModelInvite modelInvite, GenerationState generationState) {
            m.checkNotNullParameter(generationState, "state");
            return new InviteGenerationState(modelInvite, generationState);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InviteGenerationState)) {
                return false;
            }
            InviteGenerationState inviteGenerationState = (InviteGenerationState) obj;
            return m.areEqual(this.lastGeneratedInvite, inviteGenerationState.lastGeneratedInvite) && m.areEqual(this.state, inviteGenerationState.state);
        }

        public final ModelInvite getLastGeneratedInvite() {
            return this.lastGeneratedInvite;
        }

        public final GenerationState getState() {
            return this.state;
        }

        public int hashCode() {
            ModelInvite modelInvite = this.lastGeneratedInvite;
            int i = 0;
            int hashCode = (modelInvite != null ? modelInvite.hashCode() : 0) * 31;
            GenerationState generationState = this.state;
            if (generationState != null) {
                i = generationState.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("InviteGenerationState(lastGeneratedInvite=");
            R.append(this.lastGeneratedInvite);
            R.append(", state=");
            R.append(this.state);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ InviteGenerationState(ModelInvite modelInvite, GenerationState generationState, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : modelInvite, (i & 2) != 0 ? GenerationState.INIT : generationState);
        }
    }

    public InviteGenerator() {
        BehaviorSubject<InviteGenerationState> l0 = BehaviorSubject.l0(new InviteGenerationState(null, null, 3, null));
        this.generationStateSubject = l0;
        Observable<InviteGenerationState> q = l0.q();
        m.checkNotNullExpressionValue(q, "generationStateSubject.distinctUntilChanged()");
        this.generationState = q;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleGeneratedInvite(ModelInvite modelInvite) {
        this.generationStateSubject.onNext(new InviteGenerationState(modelInvite, GenerationState.SUCCESS));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleRestCallFailed() {
        BehaviorSubject<InviteGenerationState> behaviorSubject = this.generationStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "generationStateSubject");
        this.generationStateSubject.onNext(InviteGenerationState.copy$default(behaviorSubject.n0(), null, GenerationState.FAILURE, 1, null));
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.generationStateSubject.onCompleted();
    }

    public final void generate(AppFragment appFragment, long j) {
        m.checkNotNullParameter(appFragment, "fragment");
        BehaviorSubject<InviteGenerationState> behaviorSubject = this.generationStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "generationStateSubject");
        this.generationStateSubject.onNext(InviteGenerationState.copy$default(behaviorSubject.n0(), null, GenerationState.GENERATING, 1, null));
        ObservableExtensionsKt.ui$default(StoreInviteSettings.generateInvite$default(StoreStream.Companion.getInviteSettings(), j, null, 2, null), appFragment, null, 2, null).k(o.a.g(appFragment.getContext(), new InviteGenerator$generate$1(this), new Action1<Error>() { // from class: com.discord.widgets.guilds.invite.InviteGenerator$generate$2
            public final void call(Error error) {
                InviteGenerator.this.handleRestCallFailed();
            }
        }));
    }

    public final void generateForAppComponent(AppComponent appComponent, long j) {
        m.checkNotNullParameter(appComponent, "appComponent");
        BehaviorSubject<InviteGenerationState> behaviorSubject = this.generationStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "generationStateSubject");
        this.generationStateSubject.onNext(InviteGenerationState.copy$default(behaviorSubject.n0(), null, GenerationState.GENERATING, 1, null));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationBuffered(StoreInviteSettings.generateInvite$default(StoreStream.Companion.getInviteSettings(), j, null, 2, null)), appComponent, null, 2, null), InviteGenerator.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new InviteGenerator$generateForAppComponent$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new InviteGenerator$generateForAppComponent$1(this));
    }

    public final Observable<InviteGenerationState> getGenerationState() {
        return this.generationState;
    }
}
