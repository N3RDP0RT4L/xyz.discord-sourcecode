package com.discord.widgets.guilds.invite;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.channel.ChannelUtils$getSortByMostRecent$1;
import com.discord.models.user.User;
import com.discord.widgets.guilds.invite.InviteSuggestion;
import com.discord.widgets.guilds.invite.InviteSuggestionsManager;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.functions.Func3;
/* compiled from: InviteSuggestionsManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000e\u001a\u0016\u0012\u0004\u0012\u00020\u000b \u0004*\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n0\n2.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u000026\u0010\u0007\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0001j\u0002`\u0006 \u0004*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0001j\u0002`\u0006\u0018\u00010\u00000\u00002\u000e\u0010\t\u001a\n \u0004*\u0004\u0018\u00010\b0\bH\n¢\u0006\u0004\b\f\u0010\r"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "privateChannels", "Lcom/discord/primitives/MessageId;", "mostRecentMessages", "Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager$UserAffinityData;", "affinityUserData", "", "Lcom/discord/widgets/guilds/invite/InviteSuggestion;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;Ljava/util/Map;Lcom/discord/widgets/guilds/invite/InviteSuggestionsManager$UserAffinityData;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InviteSuggestionsManager$observeInviteSuggestions$2<T1, T2, T3, R> implements Func3<Map<Long, ? extends Channel>, Map<Long, ? extends Long>, InviteSuggestionsManager.UserAffinityData, List<? extends InviteSuggestion>> {
    public static final InviteSuggestionsManager$observeInviteSuggestions$2 INSTANCE = new InviteSuggestionsManager$observeInviteSuggestions$2();

    @Override // rx.functions.Func3
    public /* bridge */ /* synthetic */ List<? extends InviteSuggestion> call(Map<Long, ? extends Channel> map, Map<Long, ? extends Long> map2, InviteSuggestionsManager.UserAffinityData userAffinityData) {
        return call2((Map<Long, Channel>) map, (Map<Long, Long>) map2, userAffinityData);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<InviteSuggestion> call2(Map<Long, Channel> map, Map<Long, Long> map2, InviteSuggestionsManager.UserAffinityData userAffinityData) {
        User a;
        ArrayList arrayList = new ArrayList();
        Collection<Channel> values = map.values();
        Channel.Companion companion = Channel.Companion;
        m.checkNotNullExpressionValue(map2, "mostRecentMessages");
        m.checkNotNullParameter(companion, "$this$getSortByMostRecent");
        m.checkNotNullParameter(map2, "mostRecentMessageIds");
        List sortedWith = u.sortedWith(values, new ChannelUtils$getSortByMostRecent$1(map2));
        Channel channel = sortedWith.isEmpty() ^ true ? (Channel) sortedWith.get(0) : null;
        if (channel != null) {
            arrayList.add(new InviteSuggestion.ChannelItem(channel));
        }
        List<Long> userIds = userAffinityData.getUserIds();
        ArrayList arrayList2 = new ArrayList();
        for (Number number : userIds) {
            User user = userAffinityData.getUsers().get(Long.valueOf(number.longValue()));
            if (user != null) {
                arrayList2.add(user);
            }
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            User user2 = (User) next;
            Integer num = (Integer) a.e(user2, userAffinityData.getRelationships());
            if (!(!(channel == null || (a = ChannelUtils.a(channel)) == null || a.getId() != user2.getId()) || (num != null && num.intValue() == 2))) {
                arrayList3.add(next);
            }
        }
        List<User> distinct = u.distinct(arrayList3);
        for (User user3 : distinct) {
            arrayList.add(new InviteSuggestion.UserSuggestion(user3));
        }
        ArrayList arrayList4 = new ArrayList();
        for (Object obj : sortedWith) {
            Channel channel2 = (Channel) obj;
            if ((m.areEqual(channel, channel2) ^ true) && (ChannelUtils.a(channel2) == null || !u.contains(distinct, ChannelUtils.a(channel2)))) {
                arrayList4.add(obj);
            }
        }
        ArrayList arrayList5 = new ArrayList(o.collectionSizeOrDefault(arrayList4, 10));
        Iterator it2 = arrayList4.iterator();
        while (it2.hasNext()) {
            arrayList5.add(Boolean.valueOf(arrayList.add(new InviteSuggestion.ChannelItem((Channel) it2.next()))));
        }
        return arrayList;
    }
}
