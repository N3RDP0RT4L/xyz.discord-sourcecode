package com.discord.widgets.client;

import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import rx.functions.Func0;
/* compiled from: WidgetClientOutdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "()Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetClientOutdated$onViewCreated$2<R> implements Func0<Boolean> {
    public static final WidgetClientOutdated$onViewCreated$2 INSTANCE = new WidgetClientOutdated$onViewCreated$2();

    @Override // rx.functions.Func0, java.util.concurrent.Callable
    public final Boolean call() {
        return Boolean.TRUE;
    }
}
