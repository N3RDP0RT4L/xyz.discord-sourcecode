package com.discord.widgets.client;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.discord.databinding.WidgetClientOutdatedBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetClientOutdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetClientOutdatedBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetClientOutdatedBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetClientOutdated$binding$2 extends k implements Function1<View, WidgetClientOutdatedBinding> {
    public static final WidgetClientOutdated$binding$2 INSTANCE = new WidgetClientOutdated$binding$2();

    public WidgetClientOutdated$binding$2() {
        super(1, WidgetClientOutdatedBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetClientOutdatedBinding;", 0);
    }

    public final WidgetClientOutdatedBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.client_outdated_anchor_wrap;
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.client_outdated_anchor_wrap);
        if (linearLayout != null) {
            i = R.id.client_outdated_update;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.client_outdated_update);
            if (materialButton != null) {
                return new WidgetClientOutdatedBinding((RelativeLayout) view, linearLayout, materialButton);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
