package com.discord.widgets.stage;

import com.discord.api.stageinstance.StageInstance;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildIdsWithVisibleStageInstanceModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010&\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\t\u001a\u00060\u0001j\u0002`\u00022&\u0010\u0006\u001a\"\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00030\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/stageinstance/StageInstance;", "entry", "invoke", "(Ljava/util/Map$Entry;)J", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildIdsWithVisibleStageInstanceModel$compute$2 extends o implements Function1<Map.Entry<? extends Long, ? extends Map<Long, ? extends StageInstance>>, Long> {
    public static final GuildIdsWithVisibleStageInstanceModel$compute$2 INSTANCE = new GuildIdsWithVisibleStageInstanceModel$compute$2();

    public GuildIdsWithVisibleStageInstanceModel$compute$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Long invoke(Map.Entry<? extends Long, ? extends Map<Long, ? extends StageInstance>> entry) {
        return Long.valueOf(invoke2((Map.Entry<Long, ? extends Map<Long, StageInstance>>) entry));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final long invoke2(Map.Entry<Long, ? extends Map<Long, StageInstance>> entry) {
        m.checkNotNullParameter(entry, "entry");
        return entry.getKey().longValue();
    }
}
