package com.discord.widgets.stage.model;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.stageinstance.StageInstance;
import com.discord.models.guild.UserGuildMember;
import com.discord.models.user.User;
import com.discord.widgets.stage.StageRoles;
import d0.z.d.m;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: StageChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u001b\b\u0086\b\u0018\u00002\u00020\u0001Bl\u0012\u0006\u0010\u001e\u001a\u00020\u0002\u0012\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\u0010 \u001a\u0004\u0018\u00010\t\u0012\u0010\u0010!\u001a\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r\u0012\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00120\u0005\u0012\u0006\u0010#\u001a\u00020\u0014\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0017\u0012\u0006\u0010%\u001a\u00020\u001a\u0012\u0006\u0010&\u001a\u00020\u001aø\u0001\u0000¢\u0006\u0004\bC\u0010DJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001b\u0010\f\u001a\u0004\u0018\u00010\tHÆ\u0003ø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u0010\u001a\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0005HÆ\u0003¢\u0006\u0004\b\u0013\u0010\bJ\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001cJ\u008a\u0001\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u001e\u001a\u00020\u00022\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\t2\u0012\b\u0002\u0010!\u001a\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00120\u00052\b\b\u0002\u0010#\u001a\u00020\u00142\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00172\b\b\u0002\u0010%\u001a\u00020\u001a2\b\b\u0002\u0010&\u001a\u00020\u001aHÆ\u0001ø\u0001\u0000ø\u0001\u0002¢\u0006\u0004\b'\u0010(J\u0010\u0010+\u001a\u00020*HÖ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010-\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b-\u0010\u0016J\u001a\u0010/\u001a\u00020\u001a2\b\u0010.\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b/\u00100R\u001b\u0010$\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b$\u00101\u001a\u0004\b2\u0010\u0019R$\u0010 \u001a\u0004\u0018\u00010\t8\u0006@\u0006ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\f\n\u0004\b \u00103\u001a\u0004\b4\u0010\u000bR\u0019\u0010&\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00105\u001a\u0004\b6\u0010\u001cR\u001f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00120\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00107\u001a\u0004\b8\u0010\bR#\u0010!\u001a\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00109\u001a\u0004\b:\u0010\u0011R\u001f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00107\u001a\u0004\b;\u0010\bR\u0019\u0010%\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b%\u00105\u001a\u0004\b<\u0010\u001cR\u0019\u0010\u001e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010=\u001a\u0004\b>\u0010\u0004R\u0013\u0010@\u001a\u00020\u001a8F@\u0006¢\u0006\u0006\u001a\u0004\b?\u0010\u001cR\u0019\u0010#\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010A\u001a\u0004\bB\u0010\u0016\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b¡\u001e0\u0001¨\u0006E"}, d2 = {"Lcom/discord/widgets/stage/model/StageChannel;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/models/user/User;", "component2", "()Ljava/util/List;", "Lcom/discord/widgets/stage/StageRoles;", "component3-twRsX-0", "()Lcom/discord/widgets/stage/StageRoles;", "component3", "", "", "Lcom/discord/primitives/UserId;", "component4", "()Ljava/util/Set;", "Lcom/discord/models/guild/UserGuildMember;", "component5", "", "component6", "()I", "Lcom/discord/api/stageinstance/StageInstance;", "component7", "()Lcom/discord/api/stageinstance/StageInstance;", "", "component8", "()Z", "component9", "channel", "participants", "myRoles", "speakerIds", "speakers", "audienceSize", "stageInstance", "canAccess", "containsMe", "copy-LcZnYPc", "(Lcom/discord/api/channel/Channel;Ljava/util/List;Lcom/discord/widgets/stage/StageRoles;Ljava/util/Set;Ljava/util/List;ILcom/discord/api/stageinstance/StageInstance;ZZ)Lcom/discord/widgets/stage/model/StageChannel;", "copy", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "Lcom/discord/widgets/stage/StageRoles;", "getMyRoles-twRsX-0", "Z", "getContainsMe", "Ljava/util/List;", "getSpeakers", "Ljava/util/Set;", "getSpeakerIds", "getParticipants", "getCanAccess", "Lcom/discord/api/channel/Channel;", "getChannel", "getHasActiveStageInstance", "hasActiveStageInstance", "I", "getAudienceSize", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/util/List;Lcom/discord/widgets/stage/StageRoles;Ljava/util/Set;Ljava/util/List;ILcom/discord/api/stageinstance/StageInstance;ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageChannel {
    private final int audienceSize;
    private final boolean canAccess;
    private final Channel channel;
    private final boolean containsMe;
    private final StageRoles myRoles;
    private final List<User> participants;
    private final Set<Long> speakerIds;
    private final List<UserGuildMember> speakers;
    private final StageInstance stageInstance;

    /* JADX WARN: Multi-variable type inference failed */
    private StageChannel(Channel channel, List<? extends User> list, StageRoles stageRoles, Set<Long> set, List<UserGuildMember> list2, int i, StageInstance stageInstance, boolean z2, boolean z3) {
        this.channel = channel;
        this.participants = list;
        this.myRoles = stageRoles;
        this.speakerIds = set;
        this.speakers = list2;
        this.audienceSize = i;
        this.stageInstance = stageInstance;
        this.canAccess = z2;
        this.containsMe = z3;
    }

    public final Channel component1() {
        return this.channel;
    }

    public final List<User> component2() {
        return this.participants;
    }

    /* renamed from: component3-twRsX-0  reason: not valid java name */
    public final StageRoles m38component3twRsX0() {
        return this.myRoles;
    }

    public final Set<Long> component4() {
        return this.speakerIds;
    }

    public final List<UserGuildMember> component5() {
        return this.speakers;
    }

    public final int component6() {
        return this.audienceSize;
    }

    public final StageInstance component7() {
        return this.stageInstance;
    }

    public final boolean component8() {
        return this.canAccess;
    }

    public final boolean component9() {
        return this.containsMe;
    }

    /* renamed from: copy-LcZnYPc  reason: not valid java name */
    public final StageChannel m39copyLcZnYPc(Channel channel, List<? extends User> list, StageRoles stageRoles, Set<Long> set, List<UserGuildMember> list2, int i, StageInstance stageInstance, boolean z2, boolean z3) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(list, "participants");
        m.checkNotNullParameter(set, "speakerIds");
        m.checkNotNullParameter(list2, "speakers");
        return new StageChannel(channel, list, stageRoles, set, list2, i, stageInstance, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StageChannel)) {
            return false;
        }
        StageChannel stageChannel = (StageChannel) obj;
        return m.areEqual(this.channel, stageChannel.channel) && m.areEqual(this.participants, stageChannel.participants) && m.areEqual(this.myRoles, stageChannel.myRoles) && m.areEqual(this.speakerIds, stageChannel.speakerIds) && m.areEqual(this.speakers, stageChannel.speakers) && this.audienceSize == stageChannel.audienceSize && m.areEqual(this.stageInstance, stageChannel.stageInstance) && this.canAccess == stageChannel.canAccess && this.containsMe == stageChannel.containsMe;
    }

    public final int getAudienceSize() {
        return this.audienceSize;
    }

    public final boolean getCanAccess() {
        return this.canAccess;
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final boolean getContainsMe() {
        return this.containsMe;
    }

    public final boolean getHasActiveStageInstance() {
        return this.stageInstance != null;
    }

    /* renamed from: getMyRoles-twRsX-0  reason: not valid java name */
    public final StageRoles m40getMyRolestwRsX0() {
        return this.myRoles;
    }

    public final List<User> getParticipants() {
        return this.participants;
    }

    public final Set<Long> getSpeakerIds() {
        return this.speakerIds;
    }

    public final List<UserGuildMember> getSpeakers() {
        return this.speakers;
    }

    public final StageInstance getStageInstance() {
        return this.stageInstance;
    }

    public int hashCode() {
        Channel channel = this.channel;
        int i = 0;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        List<User> list = this.participants;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        StageRoles stageRoles = this.myRoles;
        int hashCode3 = (hashCode2 + (stageRoles != null ? stageRoles.hashCode() : 0)) * 31;
        Set<Long> set = this.speakerIds;
        int hashCode4 = (hashCode3 + (set != null ? set.hashCode() : 0)) * 31;
        List<UserGuildMember> list2 = this.speakers;
        int hashCode5 = (((hashCode4 + (list2 != null ? list2.hashCode() : 0)) * 31) + this.audienceSize) * 31;
        StageInstance stageInstance = this.stageInstance;
        if (stageInstance != null) {
            i = stageInstance.hashCode();
        }
        int i2 = (hashCode5 + i) * 31;
        boolean z2 = this.canAccess;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z3 = this.containsMe;
        if (!z3) {
            i3 = z3 ? 1 : 0;
        }
        return i6 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("StageChannel(channel=");
        R.append(this.channel);
        R.append(", participants=");
        R.append(this.participants);
        R.append(", myRoles=");
        R.append(this.myRoles);
        R.append(", speakerIds=");
        R.append(this.speakerIds);
        R.append(", speakers=");
        R.append(this.speakers);
        R.append(", audienceSize=");
        R.append(this.audienceSize);
        R.append(", stageInstance=");
        R.append(this.stageInstance);
        R.append(", canAccess=");
        R.append(this.canAccess);
        R.append(", containsMe=");
        return a.M(R, this.containsMe, ")");
    }

    public /* synthetic */ StageChannel(Channel channel, List list, StageRoles stageRoles, Set set, List list2, int i, StageInstance stageInstance, boolean z2, boolean z3, DefaultConstructorMarker defaultConstructorMarker) {
        this(channel, list, stageRoles, set, list2, i, stageInstance, z2, z3);
    }
}
