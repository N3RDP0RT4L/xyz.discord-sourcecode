package com.discord.widgets.stage.model;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.role.GuildRole;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.StageRequestToSpeakState;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.model.StageCallModel;
import com.discord.widgets.voice.fullscreen.stage.StageCallItem;
import d0.d0.f;
import d0.f0.q;
import d0.t.g0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func7;
/* compiled from: StageCallModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0002\b\u0019\b\u0086\b\u0018\u0000 C2\u00020\u0001:\u0001CB~\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\n0\u0006\u0012\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\f0\u0006\u0012\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0006\u0012\u0006\u0010\"\u001a\u00020\u0010\u0012\b\u0010#\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010$\u001a\u00020\u0010\u0012\b\u0010%\u001a\u0004\u0018\u00010\u0016\u0012\u0006\u0010&\u001a\u00020\u0019\u0012\u0006\u0010'\u001a\u00020\u0010ø\u0001\u0000¢\u0006\u0004\bA\u0010BJ\u0019\u0010\u0005\u001a\u00020\u0002HÆ\u0003ø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u0006HÆ\u0003¢\u0006\u0004\b\r\u0010\tJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0006HÆ\u0003¢\u0006\u0004\b\u000f\u0010\tJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0004J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0004J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0004J \u0001\u0010*\u001a\u00020\u00002\b\b\u0002\u0010\u001d\u001a\u00020\u00022\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\n0\u00062\u000e\b\u0002\u0010 \u001a\b\u0012\u0004\u0012\u00020\f0\u00062\u000e\b\u0002\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00062\b\b\u0002\u0010\"\u001a\u00020\u00102\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010$\u001a\u00020\u00102\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00162\b\b\u0002\u0010&\u001a\u00020\u00192\b\b\u0002\u0010'\u001a\u00020\u0010HÆ\u0001ø\u0001\u0000ø\u0001\u0002¢\u0006\u0004\b(\u0010)J\u0010\u0010,\u001a\u00020+HÖ\u0001¢\u0006\u0004\b,\u0010-J\u0010\u0010.\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b.\u0010\u0004J\u001a\u00100\u001a\u00020\u00192\b\u0010/\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b0\u00101R\u001f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b!\u00102\u001a\u0004\b3\u0010\tR\u001b\u0010%\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b%\u00104\u001a\u0004\b5\u0010\u0018R\"\u0010\u001d\u001a\u00020\u00028\u0006@\u0006ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\f\n\u0004\b\u001d\u00106\u001a\u0004\b7\u0010\u0004R\u0019\u0010\"\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00106\u001a\u0004\b8\u0010\u0004R\u001f\u0010 \u001a\b\u0012\u0004\u0012\u00020\f0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b \u00102\u001a\u0004\b9\u0010\tR\u001f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00102\u001a\u0004\b:\u0010\tR\u0019\u0010&\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010;\u001a\u0004\b&\u0010\u001bR\u0019\u0010$\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b$\u00106\u001a\u0004\b<\u0010\u0004R\u001b\u0010#\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010=\u001a\u0004\b>\u0010\u0014R\u001f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\n0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00102\u001a\u0004\b?\u0010\tR\u0019\u0010'\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b'\u00106\u001a\u0004\b@\u0010\u0004\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b¡\u001e0\u0001¨\u0006D"}, d2 = {"Lcom/discord/widgets/stage/model/StageCallModel;", "", "Lcom/discord/widgets/stage/StageRoles;", "component1-1LxfuJo", "()I", "component1", "", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem$SpeakerItem;", "component2", "()Ljava/util/List;", "Lcom/discord/widgets/stage/model/StageMediaParticipant;", "component3", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem$AudienceItem;", "component4", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component5", "", "component6", "Lcom/discord/api/stageinstance/StageInstance;", "component7", "()Lcom/discord/api/stageinstance/StageInstance;", "component8", "Lcom/discord/models/guild/Guild;", "component9", "()Lcom/discord/models/guild/Guild;", "", "component10", "()Z", "component11", "myStageRoles", "speakerItems", "mediaParticipants", "audience", "speakingVoiceUsers", "requestingToSpeakCount", "stageInstance", "numBlockedUsers", "guild", "isLurking", "numSpeakers", "copy-YvLQhEs", "(ILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ILcom/discord/api/stageinstance/StageInstance;ILcom/discord/models/guild/Guild;ZI)Lcom/discord/widgets/stage/model/StageCallModel;", "copy", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getSpeakingVoiceUsers", "Lcom/discord/models/guild/Guild;", "getGuild", "I", "getMyStageRoles-1LxfuJo", "getRequestingToSpeakCount", "getAudience", "getSpeakerItems", "Z", "getNumBlockedUsers", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "getMediaParticipants", "getNumSpeakers", HookHelper.constructorName, "(ILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ILcom/discord/api/stageinstance/StageInstance;ILcom/discord/models/guild/Guild;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageCallModel {
    public static final Companion Companion = new Companion(null);
    private final List<StageCallItem.AudienceItem> audience;
    private final Guild guild;
    private final boolean isLurking;
    private final List<StageMediaParticipant> mediaParticipants;
    private final int myStageRoles;
    private final int numBlockedUsers;
    private final int numSpeakers;
    private final int requestingToSpeakCount;
    private final List<StageCallItem.SpeakerItem> speakerItems;
    private final List<StoreVoiceParticipants.VoiceUser> speakingVoiceUsers;
    private final StageInstance stageInstance;

    /* compiled from: StageCallModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b \u0010!J\u0097\u0001\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0003\u001a\u00020\u00022\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00042\u0016\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\t\u0012\u0004\u0012\u00020\n0\u00042\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\u00042\u0010\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0015\u001a\u00020\u00142\b\b\u0002\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ!\u0010\u001e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180\u001d2\n\u0010\u001c\u001a\u00060\u0005j\u0002`\u001b¢\u0006\u0004\b\u001e\u0010\u001f¨\u0006\""}, d2 = {"Lcom/discord/widgets/stage/model/StageCallModel$Companion;", "", "Lcom/discord/api/channel/Channel;", "channel", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "guildMembers", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "participants", "", "blockedUsers", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstance", "", "isLurking", "Lcom/discord/stores/StoreStageChannels;", "stagesStore", "Lcom/discord/widgets/stage/model/StageCallModel;", "create", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Set;Lcom/discord/api/stageinstance/StageInstance;ZLcom/discord/stores/StoreStageChannels;)Lcom/discord/widgets/stage/model/StageCallModel;", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "observeStageCallModel", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final StageCallModel create(Channel channel, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Guild guild, Map<Long, StoreVoiceParticipants.VoiceUser> map3, Set<Long> set, StageInstance stageInstance, boolean z2, StoreStageChannels storeStageChannels) {
            int i;
            int i2;
            boolean z3;
            int i3;
            String str;
            VoiceState voiceState;
            Collection<StoreVoiceParticipants.VoiceUser> values = map3.values();
            ArrayList<StoreVoiceParticipants.VoiceUser> arrayList = new ArrayList();
            for (Object obj : values) {
                if (((StoreVoiceParticipants.VoiceUser) obj).isConnected()) {
                    arrayList.add(obj);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(arrayList, 10)), 16));
            for (Object obj2 : arrayList) {
                linkedHashMap.put(Long.valueOf(((StoreVoiceParticipants.VoiceUser) obj2).getUser().getId()), obj2);
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(linkedHashMap.size()));
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                Object key = entry.getKey();
                StageRoles stageRoles = storeStageChannels.m12getUserRolesuOBN1zc(((Number) entry.getKey()).longValue(), channel.h());
                linkedHashMap2.put(key, StageRoles.m19boximpl(stageRoles != null ? stageRoles.m29unboximpl() : StageRoles.Companion.m31getAUDIENCE1LxfuJo()));
            }
            Collection<StageRoles> values2 = linkedHashMap2.values();
            if (!(values2 instanceof Collection) || !values2.isEmpty()) {
                int i4 = 0;
                for (StageRoles stageRoles2 : values2) {
                    if (StageRoles.m27isSpeakerimpl(stageRoles2.m29unboximpl()) && (i4 = i4 + 1) < 0) {
                        n.throwCountOverflow();
                    }
                }
                i = i4;
            } else {
                i = 0;
            }
            if (arrayList.isEmpty()) {
                i2 = 0;
            } else {
                int i5 = 0;
                for (StoreVoiceParticipants.VoiceUser voiceUser : arrayList) {
                    StageRoles stageRoles3 = storeStageChannels.m12getUserRolesuOBN1zc(voiceUser.getUser().getId(), channel.h());
                    if ((StageRoles.m27isSpeakerimpl(stageRoles3 != null ? stageRoles3.m29unboximpl() : StageRoles.Companion.m31getAUDIENCE1LxfuJo()) && ((voiceState = voiceUser.getVoiceState()) == null || !voiceState.j())) && (i5 = i5 + 1) < 0) {
                        n.throwCountOverflow();
                    }
                }
                i2 = i5;
            }
            int coerceAtMost = f.coerceAtMost(i2, 3);
            int i6 = 0;
            int i7 = 0;
            for (StoreVoiceParticipants.VoiceUser voiceUser2 : arrayList) {
                VoiceState voiceState2 = voiceUser2.getVoiceState();
                if (voiceState2 != null) {
                    m.checkNotNullParameter(voiceState2, "$this$hasUnackedRequestToSpeak");
                    if (d.y0(voiceState2) == StageRequestToSpeakState.REQUESTED_TO_SPEAK) {
                        i6++;
                    }
                }
                i6 = i6;
                boolean contains = set.contains(Long.valueOf(voiceUser2.getUser().getId()));
                if (contains) {
                    i7++;
                }
                i7 = i7;
                StageRoles stageRoles4 = (StageRoles) linkedHashMap2.get(Long.valueOf(voiceUser2.getUser().getId()));
                if (stageRoles4 == null || !StageRoles.m27isSpeakerimpl(stageRoles4.m29unboximpl())) {
                    arrayList4.add(new StageCallItem.AudienceItem(voiceUser2, channel, RoleUtils.getHighestHoistedRole(map2, map.get(Long.valueOf(voiceUser2.getUser().getId()))), contains));
                } else {
                    if (voiceUser2.getStreamContext() != null) {
                        StageCallItem.MediaType mediaType = StageCallItem.MediaType.STREAM;
                        StringBuilder R = a.R("STREAM-");
                        R.append(voiceUser2.getUser().getId());
                        arrayList2.add(new StageMediaParticipant(R.toString(), voiceUser2, channel, false, contains, mediaType));
                    }
                    VoiceState voiceState3 = voiceUser2.getVoiceState();
                    if (voiceState3 != null && voiceState3.j()) {
                        StageRoles stageRoles5 = (StageRoles) linkedHashMap2.get(Long.valueOf(voiceUser2.getUser().getId()));
                        boolean z4 = stageRoles5 != null && StageRoles.m26isModeratorimpl(stageRoles5.m29unboximpl());
                        StageCallItem.MediaType mediaType2 = StageCallItem.MediaType.USER;
                        StringBuilder R2 = a.R("USER-");
                        R2.append(voiceUser2.getUser().getId());
                        arrayList2.add(new StageMediaParticipant(R2.toString(), voiceUser2, channel, z4, contains, mediaType2));
                    } else {
                        StageRoles stageRoles6 = (StageRoles) linkedHashMap2.get(Long.valueOf(voiceUser2.getUser().getId()));
                        if (stageRoles6 == null || !StageRoles.m26isModeratorimpl(stageRoles6.m29unboximpl())) {
                            i3 = 3;
                            z3 = false;
                        } else {
                            i3 = 3;
                            z3 = true;
                        }
                        if (i > i3) {
                            str = String.valueOf(arrayList3.size() % i3);
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append(arrayList3.size() % i3);
                            sb.append('-');
                            sb.append(i);
                            str = sb.toString();
                        }
                        arrayList3.add(new StageCallItem.SpeakerItem(voiceUser2, channel, z3, str, coerceAtMost, contains));
                    }
                }
            }
            StageRoles stageRoles7 = storeStageChannels.m10getMyRolesvisDeB4(channel.h());
            return new StageCallModel(stageRoles7 != null ? stageRoles7.m29unboximpl() : StageRoles.Companion.m31getAUDIENCE1LxfuJo(), arrayList3, arrayList2, u.sorted(arrayList4), q.toList(q.map(q.filter(u.asSequence(arrayList3), StageCallModel$Companion$create$2.INSTANCE), StageCallModel$Companion$create$3.INSTANCE)), i6, stageInstance, i7, guild, z2, i, null);
        }

        public final Observable<StageCallModel> observeStageCallModel(final long j) {
            Observable Y = StoreStream.Companion.getChannels().observeChannel(j).Y(new b<Channel, Observable<? extends StageCallModel>>() { // from class: com.discord.widgets.stage.model.StageCallModel$Companion$observeStageCallModel$1
                public final Observable<? extends StageCallModel> call(final Channel channel) {
                    if (channel == null) {
                        return new k(null);
                    }
                    if (!ChannelUtils.z(channel)) {
                        return new k(null);
                    }
                    StoreStream.Companion companion = StoreStream.Companion;
                    return Observable.e(ObservableExtensionsKt.leadingEdgeThrottle(companion.getVoiceParticipants().get(channel.h()), 250L, TimeUnit.MILLISECONDS), companion.getGuilds().observeComputed(channel.f()), companion.getGuilds().observeRoles(channel.f()), companion.getGuilds().observeGuild(channel.f()), companion.getUserRelationships().observeForType(2), companion.getStageInstances().observeStageInstanceForChannel(j), companion.getLurking().isLurkingObs(channel.f()), new Func7<Map<Long, ? extends StoreVoiceParticipants.VoiceUser>, Map<Long, ? extends GuildMember>, Map<Long, ? extends GuildRole>, Guild, Map<Long, ? extends Integer>, StageInstance, Boolean, StageCallModel>() { // from class: com.discord.widgets.stage.model.StageCallModel$Companion$observeStageCallModel$1.1
                        @Override // rx.functions.Func7
                        public /* bridge */ /* synthetic */ StageCallModel call(Map<Long, ? extends StoreVoiceParticipants.VoiceUser> map, Map<Long, ? extends GuildMember> map2, Map<Long, ? extends GuildRole> map3, Guild guild, Map<Long, ? extends Integer> map4, StageInstance stageInstance, Boolean bool) {
                            return call2((Map<Long, StoreVoiceParticipants.VoiceUser>) map, (Map<Long, GuildMember>) map2, (Map<Long, GuildRole>) map3, guild, (Map<Long, Integer>) map4, stageInstance, bool);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final StageCallModel call2(Map<Long, StoreVoiceParticipants.VoiceUser> map, Map<Long, GuildMember> map2, Map<Long, GuildRole> map3, Guild guild, Map<Long, Integer> map4, StageInstance stageInstance, Boolean bool) {
                            StageCallModel create;
                            StageCallModel.Companion companion2 = StageCallModel.Companion;
                            Channel channel2 = Channel.this;
                            m.checkNotNullExpressionValue(map2, "guildMembers");
                            m.checkNotNullExpressionValue(map3, "guildRoles");
                            m.checkNotNullExpressionValue(map, "participants");
                            Set<Long> keySet = map4.keySet();
                            m.checkNotNullExpressionValue(bool, "isLurking");
                            create = companion2.create(channel2, map2, map3, guild, map, keySet, stageInstance, bool.booleanValue(), (r21 & 256) != 0 ? StoreStream.Companion.getStageChannels() : null);
                            return create;
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "StoreStream\n          .g…            }\n          }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private StageCallModel(int i, List<StageCallItem.SpeakerItem> list, List<StageMediaParticipant> list2, List<StageCallItem.AudienceItem> list3, List<StoreVoiceParticipants.VoiceUser> list4, int i2, StageInstance stageInstance, int i3, Guild guild, boolean z2, int i4) {
        this.myStageRoles = i;
        this.speakerItems = list;
        this.mediaParticipants = list2;
        this.audience = list3;
        this.speakingVoiceUsers = list4;
        this.requestingToSpeakCount = i2;
        this.stageInstance = stageInstance;
        this.numBlockedUsers = i3;
        this.guild = guild;
        this.isLurking = z2;
        this.numSpeakers = i4;
    }

    /* renamed from: component1-1LxfuJo  reason: not valid java name */
    public final int m34component11LxfuJo() {
        return this.myStageRoles;
    }

    public final boolean component10() {
        return this.isLurking;
    }

    public final int component11() {
        return this.numSpeakers;
    }

    public final List<StageCallItem.SpeakerItem> component2() {
        return this.speakerItems;
    }

    public final List<StageMediaParticipant> component3() {
        return this.mediaParticipants;
    }

    public final List<StageCallItem.AudienceItem> component4() {
        return this.audience;
    }

    public final List<StoreVoiceParticipants.VoiceUser> component5() {
        return this.speakingVoiceUsers;
    }

    public final int component6() {
        return this.requestingToSpeakCount;
    }

    public final StageInstance component7() {
        return this.stageInstance;
    }

    public final int component8() {
        return this.numBlockedUsers;
    }

    public final Guild component9() {
        return this.guild;
    }

    /* renamed from: copy-YvLQhEs  reason: not valid java name */
    public final StageCallModel m35copyYvLQhEs(int i, List<StageCallItem.SpeakerItem> list, List<StageMediaParticipant> list2, List<StageCallItem.AudienceItem> list3, List<StoreVoiceParticipants.VoiceUser> list4, int i2, StageInstance stageInstance, int i3, Guild guild, boolean z2, int i4) {
        m.checkNotNullParameter(list, "speakerItems");
        m.checkNotNullParameter(list2, "mediaParticipants");
        m.checkNotNullParameter(list3, "audience");
        m.checkNotNullParameter(list4, "speakingVoiceUsers");
        return new StageCallModel(i, list, list2, list3, list4, i2, stageInstance, i3, guild, z2, i4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StageCallModel)) {
            return false;
        }
        StageCallModel stageCallModel = (StageCallModel) obj;
        return this.myStageRoles == stageCallModel.myStageRoles && m.areEqual(this.speakerItems, stageCallModel.speakerItems) && m.areEqual(this.mediaParticipants, stageCallModel.mediaParticipants) && m.areEqual(this.audience, stageCallModel.audience) && m.areEqual(this.speakingVoiceUsers, stageCallModel.speakingVoiceUsers) && this.requestingToSpeakCount == stageCallModel.requestingToSpeakCount && m.areEqual(this.stageInstance, stageCallModel.stageInstance) && this.numBlockedUsers == stageCallModel.numBlockedUsers && m.areEqual(this.guild, stageCallModel.guild) && this.isLurking == stageCallModel.isLurking && this.numSpeakers == stageCallModel.numSpeakers;
    }

    public final List<StageCallItem.AudienceItem> getAudience() {
        return this.audience;
    }

    public final Guild getGuild() {
        return this.guild;
    }

    public final List<StageMediaParticipant> getMediaParticipants() {
        return this.mediaParticipants;
    }

    /* renamed from: getMyStageRoles-1LxfuJo  reason: not valid java name */
    public final int m36getMyStageRoles1LxfuJo() {
        return this.myStageRoles;
    }

    public final int getNumBlockedUsers() {
        return this.numBlockedUsers;
    }

    public final int getNumSpeakers() {
        return this.numSpeakers;
    }

    public final int getRequestingToSpeakCount() {
        return this.requestingToSpeakCount;
    }

    public final List<StageCallItem.SpeakerItem> getSpeakerItems() {
        return this.speakerItems;
    }

    public final List<StoreVoiceParticipants.VoiceUser> getSpeakingVoiceUsers() {
        return this.speakingVoiceUsers;
    }

    public final StageInstance getStageInstance() {
        return this.stageInstance;
    }

    public int hashCode() {
        int i = this.myStageRoles * 31;
        List<StageCallItem.SpeakerItem> list = this.speakerItems;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        List<StageMediaParticipant> list2 = this.mediaParticipants;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<StageCallItem.AudienceItem> list3 = this.audience;
        int hashCode3 = (hashCode2 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<StoreVoiceParticipants.VoiceUser> list4 = this.speakingVoiceUsers;
        int hashCode4 = (((hashCode3 + (list4 != null ? list4.hashCode() : 0)) * 31) + this.requestingToSpeakCount) * 31;
        StageInstance stageInstance = this.stageInstance;
        int hashCode5 = (((hashCode4 + (stageInstance != null ? stageInstance.hashCode() : 0)) * 31) + this.numBlockedUsers) * 31;
        Guild guild = this.guild;
        if (guild != null) {
            i2 = guild.hashCode();
        }
        int i3 = (hashCode5 + i2) * 31;
        boolean z2 = this.isLurking;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        return ((i3 + i4) * 31) + this.numSpeakers;
    }

    public final boolean isLurking() {
        return this.isLurking;
    }

    public String toString() {
        StringBuilder R = a.R("StageCallModel(myStageRoles=");
        R.append(StageRoles.m28toStringimpl(this.myStageRoles));
        R.append(", speakerItems=");
        R.append(this.speakerItems);
        R.append(", mediaParticipants=");
        R.append(this.mediaParticipants);
        R.append(", audience=");
        R.append(this.audience);
        R.append(", speakingVoiceUsers=");
        R.append(this.speakingVoiceUsers);
        R.append(", requestingToSpeakCount=");
        R.append(this.requestingToSpeakCount);
        R.append(", stageInstance=");
        R.append(this.stageInstance);
        R.append(", numBlockedUsers=");
        R.append(this.numBlockedUsers);
        R.append(", guild=");
        R.append(this.guild);
        R.append(", isLurking=");
        R.append(this.isLurking);
        R.append(", numSpeakers=");
        return a.A(R, this.numSpeakers, ")");
    }

    public /* synthetic */ StageCallModel(int i, List list, List list2, List list3, List list4, int i2, StageInstance stageInstance, int i3, Guild guild, boolean z2, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, list, list2, list3, list4, i2, stageInstance, i3, guild, z2, i4);
    }
}
