package com.discord.widgets.stage.model;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: DiscoveryModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004¨\u0006\r"}, d2 = {"Lcom/discord/widgets/stage/model/AnalyticsDiscoverySourceValues;", "", "", "RICH_PRESENCE", "I", "FEATURED", "UNSPECIFIED", "INTRO_CARD", "TRENDING", "PERSONALIZED", "USER_GUILDS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AnalyticsDiscoverySourceValues {
    public static final int FEATURED = 6;
    public static final AnalyticsDiscoverySourceValues INSTANCE = new AnalyticsDiscoverySourceValues();
    public static final int INTRO_CARD = 5;
    public static final int PERSONALIZED = 2;
    public static final int RICH_PRESENCE = 4;
    public static final int TRENDING = 1;
    public static final int UNSPECIFIED = 0;
    public static final int USER_GUILDS = 3;

    private AnalyticsDiscoverySourceValues() {
    }
}
