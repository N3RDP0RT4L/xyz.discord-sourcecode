package com.discord.widgets.stage.model;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.widgets.voice.fullscreen.stage.StageCallItem;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StageCallModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\b\u0012\u0006\u0010\u0015\u001a\u00020\u000b\u0012\u0006\u0010\u0016\u001a\u00020\u000b\u0012\u0006\u0010\u0017\u001a\u00020\u000f¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011JL\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\b2\b\b\u0002\u0010\u0015\u001a\u00020\u000b2\b\b\u0002\u0010\u0016\u001a\u00020\u000b2\b\b\u0002\u0010\u0017\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u0004J\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010\u001f\u001a\u00020\u000b2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u0019\u0010\u0017\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010!\u001a\u0004\b\"\u0010\u0011R\u0019\u0010\u0016\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b\u0016\u0010\rR\u0019\u0010\u0015\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010#\u001a\u0004\b\u0015\u0010\rR\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b%\u0010\u0007R\u0019\u0010\u0014\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b'\u0010\nR\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010(\u001a\u0004\b)\u0010\u0004¨\u0006,"}, d2 = {"Lcom/discord/widgets/stage/model/StageMediaParticipant;", "", "", "component1", "()Ljava/lang/String;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component2", "()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "", "component4", "()Z", "component5", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem$MediaType;", "component6", "()Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem$MediaType;", ModelAuditLogEntry.CHANGE_KEY_ID, "voiceUser", "channel", "isModerator", "isBlocked", "mediaType", "copy", "(Ljava/lang/String;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Lcom/discord/api/channel/Channel;ZZLcom/discord/widgets/voice/fullscreen/stage/StageCallItem$MediaType;)Lcom/discord/widgets/stage/model/StageMediaParticipant;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem$MediaType;", "getMediaType", "Z", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "getVoiceUser", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/lang/String;", "getId", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Lcom/discord/api/channel/Channel;ZZLcom/discord/widgets/voice/fullscreen/stage/StageCallItem$MediaType;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageMediaParticipant {
    private final Channel channel;

    /* renamed from: id  reason: collision with root package name */
    private final String f2843id;
    private final boolean isBlocked;
    private final boolean isModerator;
    private final StageCallItem.MediaType mediaType;
    private final StoreVoiceParticipants.VoiceUser voiceUser;

    public StageMediaParticipant(String str, StoreVoiceParticipants.VoiceUser voiceUser, Channel channel, boolean z2, boolean z3, StageCallItem.MediaType mediaType) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(voiceUser, "voiceUser");
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(mediaType, "mediaType");
        this.f2843id = str;
        this.voiceUser = voiceUser;
        this.channel = channel;
        this.isModerator = z2;
        this.isBlocked = z3;
        this.mediaType = mediaType;
    }

    public static /* synthetic */ StageMediaParticipant copy$default(StageMediaParticipant stageMediaParticipant, String str, StoreVoiceParticipants.VoiceUser voiceUser, Channel channel, boolean z2, boolean z3, StageCallItem.MediaType mediaType, int i, Object obj) {
        if ((i & 1) != 0) {
            str = stageMediaParticipant.f2843id;
        }
        if ((i & 2) != 0) {
            voiceUser = stageMediaParticipant.voiceUser;
        }
        StoreVoiceParticipants.VoiceUser voiceUser2 = voiceUser;
        if ((i & 4) != 0) {
            channel = stageMediaParticipant.channel;
        }
        Channel channel2 = channel;
        if ((i & 8) != 0) {
            z2 = stageMediaParticipant.isModerator;
        }
        boolean z4 = z2;
        if ((i & 16) != 0) {
            z3 = stageMediaParticipant.isBlocked;
        }
        boolean z5 = z3;
        if ((i & 32) != 0) {
            mediaType = stageMediaParticipant.mediaType;
        }
        return stageMediaParticipant.copy(str, voiceUser2, channel2, z4, z5, mediaType);
    }

    public final String component1() {
        return this.f2843id;
    }

    public final StoreVoiceParticipants.VoiceUser component2() {
        return this.voiceUser;
    }

    public final Channel component3() {
        return this.channel;
    }

    public final boolean component4() {
        return this.isModerator;
    }

    public final boolean component5() {
        return this.isBlocked;
    }

    public final StageCallItem.MediaType component6() {
        return this.mediaType;
    }

    public final StageMediaParticipant copy(String str, StoreVoiceParticipants.VoiceUser voiceUser, Channel channel, boolean z2, boolean z3, StageCallItem.MediaType mediaType) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(voiceUser, "voiceUser");
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(mediaType, "mediaType");
        return new StageMediaParticipant(str, voiceUser, channel, z2, z3, mediaType);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StageMediaParticipant)) {
            return false;
        }
        StageMediaParticipant stageMediaParticipant = (StageMediaParticipant) obj;
        return m.areEqual(this.f2843id, stageMediaParticipant.f2843id) && m.areEqual(this.voiceUser, stageMediaParticipant.voiceUser) && m.areEqual(this.channel, stageMediaParticipant.channel) && this.isModerator == stageMediaParticipant.isModerator && this.isBlocked == stageMediaParticipant.isBlocked && m.areEqual(this.mediaType, stageMediaParticipant.mediaType);
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final String getId() {
        return this.f2843id;
    }

    public final StageCallItem.MediaType getMediaType() {
        return this.mediaType;
    }

    public final StoreVoiceParticipants.VoiceUser getVoiceUser() {
        return this.voiceUser;
    }

    public int hashCode() {
        String str = this.f2843id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        StoreVoiceParticipants.VoiceUser voiceUser = this.voiceUser;
        int hashCode2 = (hashCode + (voiceUser != null ? voiceUser.hashCode() : 0)) * 31;
        Channel channel = this.channel;
        int hashCode3 = (hashCode2 + (channel != null ? channel.hashCode() : 0)) * 31;
        boolean z2 = this.isModerator;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        boolean z3 = this.isBlocked;
        if (!z3) {
            i2 = z3 ? 1 : 0;
        }
        int i6 = (i5 + i2) * 31;
        StageCallItem.MediaType mediaType = this.mediaType;
        if (mediaType != null) {
            i = mediaType.hashCode();
        }
        return i6 + i;
    }

    public final boolean isBlocked() {
        return this.isBlocked;
    }

    public final boolean isModerator() {
        return this.isModerator;
    }

    public String toString() {
        StringBuilder R = a.R("StageMediaParticipant(id=");
        R.append(this.f2843id);
        R.append(", voiceUser=");
        R.append(this.voiceUser);
        R.append(", channel=");
        R.append(this.channel);
        R.append(", isModerator=");
        R.append(this.isModerator);
        R.append(", isBlocked=");
        R.append(this.isBlocked);
        R.append(", mediaType=");
        R.append(this.mediaType);
        R.append(")");
        return R.toString();
    }
}
