package com.discord.widgets.stage.model;

import com.discord.stores.StoreVoiceParticipants;
import com.discord.widgets.voice.fullscreen.stage.StageCallItem;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StageCallModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem$SpeakerItem;", "it", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "invoke", "(Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem$SpeakerItem;)Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageCallModel$Companion$create$3 extends o implements Function1<StageCallItem.SpeakerItem, StoreVoiceParticipants.VoiceUser> {
    public static final StageCallModel$Companion$create$3 INSTANCE = new StageCallModel$Companion$create$3();

    public StageCallModel$Companion$create$3() {
        super(1);
    }

    public final StoreVoiceParticipants.VoiceUser invoke(StageCallItem.SpeakerItem speakerItem) {
        m.checkNotNullParameter(speakerItem, "it");
        return speakerItem.getVoiceUser();
    }
}
