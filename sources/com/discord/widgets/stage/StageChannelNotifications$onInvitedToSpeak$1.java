package com.discord.widgets.stage;

import android.app.Application;
import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: StageChannelNotifications.kt */
@e(c = "com.discord.widgets.stage.StageChannelNotifications$onInvitedToSpeak$1", f = "StageChannelNotifications.kt", l = {49}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageChannelNotifications$onInvitedToSpeak$1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public final /* synthetic */ NotificationCompat.Builder $builder;
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ Application $context;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StageChannelNotifications$onInvitedToSpeak$1(Channel channel, NotificationCompat.Builder builder, Application application, Continuation continuation) {
        super(2, continuation);
        this.$channel = channel;
        this.$builder = builder;
        this.$context = application;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new StageChannelNotifications$onInvitedToSpeak$1(this.$channel, this.$builder, this.$context, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((StageChannelNotifications$onInvitedToSpeak$1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0061  */
    @Override // d0.w.i.a.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r11) {
        /*
            r10 = this;
            java.lang.Object r0 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r1 = r10.label
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L18
            if (r1 != r2) goto L10
            d0.l.throwOnFailure(r11)
            goto L5c
        L10:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r0)
            throw r11
        L18:
            d0.l.throwOnFailure(r11)
            com.discord.api.channel.Channel r11 = r10.$channel
            r1 = 2
            java.lang.String r11 = com.discord.utilities.icon.IconUtils.getForChannel$default(r11, r3, r1, r3)
            if (r11 == 0) goto L25
            goto L4a
        L25:
            com.discord.stores.StoreStream$Companion r11 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r11 = r11.getGuilds()
            java.util.Map r11 = r11.getGuilds()
            com.discord.api.channel.Channel r1 = r10.$channel
            long r4 = r1.f()
            java.lang.Long r1 = d0.w.i.a.b.boxLong(r4)
            java.lang.Object r11 = r11.get(r1)
            r4 = r11
            com.discord.models.guild.Guild r4 = (com.discord.models.guild.Guild) r4
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 14
            r9 = 0
            java.lang.String r11 = com.discord.utilities.icon.IconUtils.getForGuild$default(r4, r5, r6, r7, r8, r9)
        L4a:
            if (r11 == 0) goto L5f
            r4 = 250(0xfa, double:1.235E-321)
            com.discord.widgets.stage.StageChannelNotifications$onInvitedToSpeak$1$invokeSuspend$$inlined$let$lambda$1 r1 = new com.discord.widgets.stage.StageChannelNotifications$onInvitedToSpeak$1$invokeSuspend$$inlined$let$lambda$1
            r1.<init>(r3, r10, r11)
            r10.label = r2
            java.lang.Object r11 = s.a.h.b(r4, r1, r10)
            if (r11 != r0) goto L5c
            return r0
        L5c:
            r3 = r11
            android.graphics.Bitmap r3 = (android.graphics.Bitmap) r3
        L5f:
            if (r3 == 0) goto L66
            androidx.core.app.NotificationCompat$Builder r11 = r10.$builder
            r11.setLargeIcon(r3)
        L66:
            com.discord.widgets.stage.StageChannelNotifications$Notifications$InvitedToSpeak r11 = com.discord.widgets.stage.StageChannelNotifications.Notifications.InvitedToSpeak.INSTANCE
            android.app.Application r0 = r10.$context
            androidx.core.app.NotificationCompat$Builder r1 = r10.$builder
            android.app.Notification r1 = r1.build()
            java.lang.String r2 = "builder.build()"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
            r11.notify(r0, r1)
            kotlin.Unit r11 = kotlin.Unit.a
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.StageChannelNotifications$onInvitedToSpeak$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
