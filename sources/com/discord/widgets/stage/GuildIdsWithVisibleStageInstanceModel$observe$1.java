package com.discord.widgets.stage;

import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStageInstances;
import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: GuildIdsWithVisibleStageInstanceModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0001j\u0002`\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "invoke", "()Ljava/util/Set;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildIdsWithVisibleStageInstanceModel$observe$1 extends o implements Function0<Set<? extends Long>> {
    public final /* synthetic */ StorePermissions $storePermissions;
    public final /* synthetic */ StoreStageInstances $storeStageInstances;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildIdsWithVisibleStageInstanceModel$observe$1(StoreStageInstances storeStageInstances, StorePermissions storePermissions) {
        super(0);
        this.$storeStageInstances = storeStageInstances;
        this.$storePermissions = storePermissions;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Set<? extends Long> invoke() {
        return GuildIdsWithVisibleStageInstanceModel.INSTANCE.compute(this.$storeStageInstances.getStageInstancesByGuild(), this.$storePermissions.getPermissionsByChannel());
    }
}
