package com.discord.widgets.stage;

import andhook.lib.HookHelper;
import com.discord.api.stageinstance.StageInstance;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.f0.q;
import d0.t.i0;
import d0.z.d.m;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: GuildIdsWithVisibleStageInstanceModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J;\u0010\f\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\nj\u0002`\u000b0\t0\b2\b\b\u0002\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\f\u0010\rJ[\u0010\u0014\u001a\f\u0012\b\u0012\u00060\nj\u0002`\u000b0\t2&\u0010\u0011\u001a\"\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00100\u000e0\u000e2\u001a\u0010\u0013\u001a\u0016\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\b\u0012\u00060\nj\u0002`\u00120\u000e¢\u0006\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/stage/GuildIdsWithVisibleStageInstanceModel;", "", "Lcom/discord/stores/StoreStageInstances;", "storeStageInstances", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lrx/Observable;", "", "", "Lcom/discord/primitives/GuildId;", "observe", "(Lcom/discord/stores/StoreStageInstances;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/updates/ObservationDeck;)Lrx/Observable;", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/stageinstance/StageInstance;", "instancesByGuild", "Lcom/discord/api/permission/PermissionBit;", "permissionsByChannel", "compute", "(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Set;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildIdsWithVisibleStageInstanceModel {
    public static final GuildIdsWithVisibleStageInstanceModel INSTANCE = new GuildIdsWithVisibleStageInstanceModel();

    private GuildIdsWithVisibleStageInstanceModel() {
    }

    public static /* synthetic */ Observable observe$default(GuildIdsWithVisibleStageInstanceModel guildIdsWithVisibleStageInstanceModel, StoreStageInstances storeStageInstances, StorePermissions storePermissions, ObservationDeck observationDeck, int i, Object obj) {
        if ((i & 1) != 0) {
            storeStageInstances = StoreStream.Companion.getStageInstances();
        }
        if ((i & 2) != 0) {
            storePermissions = StoreStream.Companion.getPermissions();
        }
        if ((i & 4) != 0) {
            observationDeck = ObservationDeckProvider.get();
        }
        return guildIdsWithVisibleStageInstanceModel.observe(storeStageInstances, storePermissions, observationDeck);
    }

    public final Set<Long> compute(Map<Long, ? extends Map<Long, StageInstance>> map, Map<Long, Long> map2) {
        m.checkNotNullParameter(map, "instancesByGuild");
        m.checkNotNullParameter(map2, "permissionsByChannel");
        return q.toSet(q.map(q.filter(i0.asSequence(map), new GuildIdsWithVisibleStageInstanceModel$compute$1(map2)), GuildIdsWithVisibleStageInstanceModel$compute$2.INSTANCE));
    }

    public final Observable<Set<Long>> observe(StoreStageInstances storeStageInstances, StorePermissions storePermissions, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeStageInstances, "storeStageInstances");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeStageInstances, storePermissions}, false, null, null, new GuildIdsWithVisibleStageInstanceModel$observe$1(storeStageInstances, storePermissions), 14, null);
    }
}
