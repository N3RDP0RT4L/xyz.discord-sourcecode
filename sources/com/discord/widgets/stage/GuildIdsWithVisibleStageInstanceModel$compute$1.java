package com.discord.widgets.stage;

import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstance;
import com.discord.utilities.permissions.PermissionUtils;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildIdsWithVisibleStageInstanceModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0010&\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072&\u0010\u0006\u001a\"\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00030\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/stageinstance/StageInstance;", "<name for destructuring parameter 0>", "", "invoke", "(Ljava/util/Map$Entry;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildIdsWithVisibleStageInstanceModel$compute$1 extends o implements Function1<Map.Entry<? extends Long, ? extends Map<Long, ? extends StageInstance>>, Boolean> {
    public final /* synthetic */ Map $permissionsByChannel;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildIdsWithVisibleStageInstanceModel$compute$1(Map map) {
        super(1);
        this.$permissionsByChannel = map;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(Map.Entry<? extends Long, ? extends Map<Long, ? extends StageInstance>> entry) {
        return Boolean.valueOf(invoke2((Map.Entry<Long, ? extends Map<Long, StageInstance>>) entry));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(Map.Entry<Long, ? extends Map<Long, StageInstance>> entry) {
        int i;
        m.checkNotNullParameter(entry, "<name for destructuring parameter 0>");
        entry.getKey().longValue();
        Set<Long> keySet = entry.getValue().keySet();
        if (!(keySet instanceof Collection) || !keySet.isEmpty()) {
            i = 0;
            for (Number number : keySet) {
                if (PermissionUtils.can(Permission.VIEW_CHANNEL, (Long) this.$permissionsByChannel.get(Long.valueOf(number.longValue()))) && (i = i + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
        } else {
            i = 0;
        }
        return i > 0;
    }
}
