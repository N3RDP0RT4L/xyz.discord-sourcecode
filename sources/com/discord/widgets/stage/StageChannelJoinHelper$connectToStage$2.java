package com.discord.widgets.stage;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.app.AppTransitionActivity;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreUserRelationships;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceStates;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StageChannelJoinHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageChannelJoinHelper$connectToStage$2 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreChannels $channelsStore;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ FragmentManager $fragmentManager;
    public final /* synthetic */ boolean $launchFullscreen;
    public final /* synthetic */ Function0 $onCompleted;
    public final /* synthetic */ StoreUserRelationships $userRelationshipsStore;
    public final /* synthetic */ StoreVoiceChannelSelected $voiceChannelSelectedStore;
    public final /* synthetic */ StoreVoiceStates $voiceStatesStore;
    public final /* synthetic */ boolean $warnedAboutBlockedUsers;

    /* compiled from: StageChannelJoinHelper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreVoiceChannelSelected$JoinVoiceChannelResult;", "it", "", "invoke", "(Lcom/discord/stores/StoreVoiceChannelSelected$JoinVoiceChannelResult;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreVoiceChannelSelected.JoinVoiceChannelResult, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreVoiceChannelSelected.JoinVoiceChannelResult joinVoiceChannelResult) {
            invoke2(joinVoiceChannelResult);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreVoiceChannelSelected.JoinVoiceChannelResult joinVoiceChannelResult) {
            m.checkNotNullParameter(joinVoiceChannelResult, "it");
            StageChannelJoinHelper$connectToStage$2 stageChannelJoinHelper$connectToStage$2 = StageChannelJoinHelper$connectToStage$2.this;
            if (stageChannelJoinHelper$connectToStage$2.$launchFullscreen) {
                WidgetCallFullscreen.Companion.launch(stageChannelJoinHelper$connectToStage$2.$context, stageChannelJoinHelper$connectToStage$2.$channelId, (r14 & 4) != 0 ? false : true, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : AppTransitionActivity.Transition.TYPE_SLIDE_VERTICAL_WITH_FADE);
            }
            StageChannelJoinHelper$connectToStage$2.this.$onCompleted.invoke();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StageChannelJoinHelper$connectToStage$2(StoreChannels storeChannels, long j, StoreVoiceStates storeVoiceStates, StoreUserRelationships storeUserRelationships, boolean z2, StoreVoiceChannelSelected storeVoiceChannelSelected, FragmentManager fragmentManager, boolean z3, Function0 function0, Context context) {
        super(0);
        this.$channelsStore = storeChannels;
        this.$channelId = j;
        this.$voiceStatesStore = storeVoiceStates;
        this.$userRelationshipsStore = storeUserRelationships;
        this.$warnedAboutBlockedUsers = z2;
        this.$voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.$fragmentManager = fragmentManager;
        this.$launchFullscreen = z3;
        this.$onCompleted = function0;
        this.$context = context;
    }

    /* JADX WARN: Code restructure failed: missing block: B:22:0x007b, code lost:
        if (r13.$voiceChannelSelectedStore.getSelectedVoiceChannelId() != r13.$channelId) goto L24;
     */
    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke2() {
        /*
            r13 = this;
            com.discord.stores.StoreChannels r0 = r13.$channelsStore
            long r1 = r13.$channelId
            com.discord.api.channel.Channel r0 = r0.getChannel(r1)
            if (r0 == 0) goto Lb0
            com.discord.stores.StoreVoiceStates r1 = r13.$voiceStatesStore
            java.util.Map r1 = r1.get()
            java.lang.Object r1 = b.d.b.a.a.u0(r0, r1)
            java.util.Map r1 = (java.util.Map) r1
            if (r1 == 0) goto L19
            goto L1d
        L19:
            java.util.Map r1 = d0.t.h0.emptyMap()
        L1d:
            java.util.Collection r1 = r1.values()
            kotlin.sequences.Sequence r1 = d0.t.u.asSequence(r1)
            com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2$channelVoiceStateUserIds$1 r2 = new com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2$channelVoiceStateUserIds$1
            r2.<init>(r0)
            kotlin.sequences.Sequence r0 = d0.f0.q.filter(r1, r2)
            com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2$channelVoiceStateUserIds$2 r1 = com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2$channelVoiceStateUserIds$2.INSTANCE
            kotlin.sequences.Sequence r0 = d0.f0.q.map(r0, r1)
            com.discord.stores.StoreUserRelationships r1 = r13.$userRelationshipsStore
            java.util.Map r1 = r1.getRelationships()
            boolean r2 = r13.$warnedAboutBlockedUsers
            r3 = 1
            if (r2 != 0) goto L7e
            java.util.Iterator r0 = r0.iterator()
        L43:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L6e
            java.lang.Object r2 = r0.next()
            r4 = r2
            java.lang.Number r4 = (java.lang.Number) r4
            long r4 = r4.longValue()
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            java.lang.Object r4 = r1.get(r4)
            java.lang.Integer r4 = (java.lang.Integer) r4
            r5 = 2
            if (r4 != 0) goto L62
            goto L6a
        L62:
            int r4 = r4.intValue()
            if (r4 != r5) goto L6a
            r4 = 1
            goto L6b
        L6a:
            r4 = 0
        L6b:
            if (r4 == 0) goto L43
            goto L6f
        L6e:
            r2 = 0
        L6f:
            if (r2 == 0) goto L7e
            com.discord.stores.StoreVoiceChannelSelected r0 = r13.$voiceChannelSelectedStore
            long r0 = r0.getSelectedVoiceChannelId()
            long r4 = r13.$channelId
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 == 0) goto L7e
            goto L7f
        L7e:
            r3 = 0
        L7f:
            if (r3 == 0) goto L92
            com.discord.widgets.stage.sheet.WidgetStageAudienceBlockedBottomSheet$Companion r0 = com.discord.widgets.stage.sheet.WidgetStageAudienceBlockedBottomSheet.Companion
            androidx.fragment.app.FragmentManager r1 = r13.$fragmentManager
            long r2 = r13.$channelId
            boolean r4 = r13.$launchFullscreen
            r0.show(r1, r2, r4)
            kotlin.jvm.functions.Function0 r0 = r13.$onCompleted
            r0.invoke()
            goto Lb0
        L92:
            com.discord.stores.StoreVoiceChannelSelected r0 = r13.$voiceChannelSelectedStore
            long r1 = r13.$channelId
            rx.Observable r3 = r0.selectVoiceChannel(r1)
            com.discord.widgets.stage.StageChannelJoinHelper r0 = com.discord.widgets.stage.StageChannelJoinHelper.INSTANCE
            java.lang.Class r4 = r0.getClass()
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2$1 r10 = new com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2$1
            r10.<init>()
            r11 = 62
            r12 = 0
            com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
        Lb0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.StageChannelJoinHelper$connectToStage$2.invoke2():void");
    }
}
