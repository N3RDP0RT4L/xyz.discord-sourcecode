package com.discord.widgets.stage;

import andhook.lib.HookHelper;
import android.app.Application;
import android.app.Notification;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import b.a.e.d;
import b.i.a.f.e.o.f;
import com.discord.api.channel.Channel;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.lifecycle.ApplicationProvider;
import com.discord.utilities.voice.VoiceEngineForegroundService;
import com.discord.utilities.voice.VoiceEngineNotificationBuilder;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import s.a.k0;
import s.a.x0;
import xyz.discord.R;
/* compiled from: StageChannelNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B\u0011\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\r\u001a\u00020\f2\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u000f\u001a\u00020\f¢\u0006\u0004\b\u000f\u0010\u0010J\u0019\u0010\u0011\u001a\u00020\f2\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\u0011\u0010\u000eR\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/stage/StageChannelNotifications;", "", "Landroid/content/Context;", "context", "Lcom/discord/api/channel/Channel;", "channel", "Landroidx/core/app/NotificationCompat$Builder;", "createChannelNotificationBuilder", "(Landroid/content/Context;Lcom/discord/api/channel/Channel;)Landroidx/core/app/NotificationCompat$Builder;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "onInvitedToSpeak", "(J)V", "onInviteToSpeakRescinded", "()V", "onInvitedToSpeakAckFailed", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", HookHelper.constructorName, "(Lcom/discord/stores/StoreChannels;)V", "Companion", "Notifications", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageChannelNotifications {
    public static final Companion Companion = new Companion(null);
    private static final Lazy INSTANCE$delegate = g.lazy(StageChannelNotifications$Companion$INSTANCE$2.INSTANCE);
    private static final long NOTIFICATION_ICON_FETCH_DELAY_MS = 250;
    public static final String NOTIFICATION_TAG = "stage-channels";
    private final StoreChannels channelsStore;

    /* compiled from: StageChannelNotifications.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0007\u001a\u00020\u00028F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/stage/StageChannelNotifications$Companion;", "", "Lcom/discord/widgets/stage/StageChannelNotifications;", "INSTANCE$delegate", "Lkotlin/Lazy;", "getINSTANCE", "()Lcom/discord/widgets/stage/StageChannelNotifications;", "INSTANCE", "", "NOTIFICATION_ICON_FETCH_DELAY_MS", "J", "", "NOTIFICATION_TAG", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final StageChannelNotifications getINSTANCE() {
            Lazy lazy = StageChannelNotifications.INSTANCE$delegate;
            Companion companion = StageChannelNotifications.Companion;
            return (StageChannelNotifications) lazy.getValue();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StageChannelNotifications.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/widgets/stage/StageChannelNotifications$Notifications;", "", HookHelper.constructorName, "()V", "InvitedToSpeak", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Notifications {
        public static final Notifications INSTANCE = new Notifications();

        /* compiled from: StageChannelNotifications.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/stage/StageChannelNotifications$Notifications$InvitedToSpeak;", "", "Landroid/content/Context;", "context", "Landroid/app/Notification;", "notification", "", "notify", "(Landroid/content/Context;Landroid/app/Notification;)V", "cancel", "(Landroid/content/Context;)V", "", "NOTIFICATION_ID", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class InvitedToSpeak {
            public static final InvitedToSpeak INSTANCE = new InvitedToSpeak();
            private static final int NOTIFICATION_ID = 102;

            private InvitedToSpeak() {
            }

            public final void cancel(Context context) {
                m.checkNotNullParameter(context, "context");
                NotificationManagerCompat.from(context).cancel(StageChannelNotifications.NOTIFICATION_TAG, 102);
            }

            public final void notify(Context context, Notification notification) {
                m.checkNotNullParameter(context, "context");
                m.checkNotNullParameter(notification, "notification");
                NotificationManagerCompat.from(context).notify(StageChannelNotifications.NOTIFICATION_TAG, 102, notification);
            }
        }

        private Notifications() {
        }
    }

    public StageChannelNotifications() {
        this(null, 1, null);
    }

    public StageChannelNotifications(StoreChannels storeChannels) {
        m.checkNotNullParameter(storeChannels, "channelsStore");
        this.channelsStore = storeChannels;
    }

    private final NotificationCompat.Builder createChannelNotificationBuilder(Context context, Channel channel) {
        NotificationCompat.Builder color = new NotificationCompat.Builder(context, NotificationClient.NOTIF_CHANNEL_CALLS).setAutoCancel(true).setOnlyAlertOnce(true).setColor(ColorCompat.getThemedColor(context, (int) R.attr.color_brand_500));
        String z2 = channel.z();
        if (z2 == null) {
            z2 = channel.m();
        }
        NotificationCompat.Builder contentIntent = color.setContentTitle(z2).setSmallIcon(R.drawable.ic_channel_stage_24dp_white).setContentIntent(VoiceEngineNotificationBuilder.getCallScreenNavigationIntent$default(VoiceEngineNotificationBuilder.INSTANCE, context, channel.h(), null, null, 6, null));
        m.checkNotNullExpressionValue(contentIntent, "NotificationCompat.Build…gationIntent(channel.id))");
        return contentIntent;
    }

    public final void onInviteToSpeakRescinded() {
        Notifications.InvitedToSpeak.INSTANCE.cancel(ApplicationProvider.INSTANCE.get());
    }

    public final void onInvitedToSpeak(long j) {
        Channel channel;
        d dVar = d.d;
        if (d.a && (channel = this.channelsStore.getChannel(j)) != null) {
            Application application = ApplicationProvider.INSTANCE.get();
            VoiceEngineForegroundService.Companion companion = VoiceEngineForegroundService.Companion;
            NotificationCompat.Builder addAction = createChannelNotificationBuilder(application, channel).setContentText(application.getString(R.string.stage_speak_invite_header)).addAction(0, application.getString(R.string.stage_speak_invite_accept), companion.stageInviteAckPendingIntent(application, j, true)).addAction(0, application.getString(R.string.stage_speak_invite_decline), companion.stageInviteAckPendingIntent(application, j, false));
            m.checkNotNullExpressionValue(addAction, "createChannelNotificatio…_decline), declineIntent)");
            f.H0(x0.j, k0.f3814b, null, new StageChannelNotifications$onInvitedToSpeak$1(channel, addAction, application, null), 2, null);
        }
    }

    public final void onInvitedToSpeakAckFailed(long j) {
        Channel channel = this.channelsStore.getChannel(j);
        if (channel != null) {
            Application application = ApplicationProvider.INSTANCE.get();
            Notifications.InvitedToSpeak invitedToSpeak = Notifications.InvitedToSpeak.INSTANCE;
            Notification build = createChannelNotificationBuilder(application, channel).setContentText(application.getString(R.string.error_generic_title)).build();
            m.checkNotNullExpressionValue(build, "createChannelNotificatio…le))\n            .build()");
            invitedToSpeak.notify(application, build);
        }
    }

    public /* synthetic */ StageChannelNotifications(StoreChannels storeChannels, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getChannels() : storeChannels);
    }
}
