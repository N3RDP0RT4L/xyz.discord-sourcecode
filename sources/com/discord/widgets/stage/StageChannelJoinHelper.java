package com.discord.widgets.stage;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import androidx.fragment.app.FragmentManager;
import b.i.a.f.e.o.f;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreLurking;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserRelationships;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceStates;
import com.discord.utilities.voice.VoiceChannelJoinability;
import com.discord.utilities.voice.VoiceChannelJoinabilityUtils;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Job;
import xyz.discord.R;
/* compiled from: StageChannelJoinHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b,\u0010-J9\u0010\f\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0007¢\u0006\u0004\b\f\u0010\rJw\u0010\u001a\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\b\b\u0002\u0010\u000f\u001a\u00020\u000e2\b\b\u0002\u0010\u0010\u001a\u00020\u000e2\b\b\u0002\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u00152\b\b\u0002\u0010\u0018\u001a\u00020\u00172\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0007¢\u0006\u0004\b\u001a\u0010\u001bJi\u0010%\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u001d\u001a\u0004\u0018\u00010\u001c2\n\u0010\u001f\u001a\u00060\u0006j\u0002`\u001e2\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\b\b\u0002\u0010\u0010\u001a\u00020\u000e2\b\b\u0002\u0010!\u001a\u00020 2\b\b\u0002\u0010#\u001a\u00020\"2\b\b\u0002\u0010$\u001a\u00020\u0011H\u0007¢\u0006\u0004\b%\u0010&Jc\u0010'\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\u001f\u001a\u00060\u0006j\u0002`\u001e2\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\b\b\u0002\u0010\u0010\u001a\u00020\u000e2\b\b\u0002\u0010!\u001a\u00020 2\b\b\u0002\u0010#\u001a\u00020\"2\b\b\u0002\u0010$\u001a\u00020\u0011H\u0086@ø\u0001\u0000¢\u0006\u0004\b'\u0010(R\u0018\u0010*\u001a\u0004\u0018\u00010)8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010+\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006."}, d2 = {"Lcom/discord/widgets/stage/StageChannelJoinHelper;", "", "Landroid/content/Context;", "context", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lkotlin/Function0;", "", "onJoinVerified", "verifyStageJoinability", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;JLkotlin/jvm/functions/Function0;)V", "", "warnedAboutBlockedUsers", "launchFullscreen", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreVoiceStates;", "voiceStatesStore", "Lcom/discord/stores/StoreUserRelationships;", "userRelationshipsStore", "onCompleted", "connectToStage", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;JZZLcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreUserRelationships;Lkotlin/jvm/functions/Function0;)V", "Lkotlinx/coroutines/CoroutineScope;", "coroutineScope", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreLurking;", "lurkingStore", "selectedVoiceChannelStore", "lurkAndJoinStage", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lkotlinx/coroutines/CoroutineScope;JJZLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreVoiceChannelSelected;)V", "lurkAndJoinStageAsync", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;JJZLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreVoiceChannelSelected;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Lkotlinx/coroutines/Job;", "lurkJob", "Lkotlinx/coroutines/Job;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageChannelJoinHelper {
    public static final StageChannelJoinHelper INSTANCE = new StageChannelJoinHelper();
    private static Job lurkJob;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            VoiceChannelJoinability.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[VoiceChannelJoinability.GUILD_VIDEO_AT_CAPACITY.ordinal()] = 1;
            iArr[VoiceChannelJoinability.PERMISSIONS_MISSING.ordinal()] = 2;
            iArr[VoiceChannelJoinability.CHANNEL_FULL.ordinal()] = 3;
            iArr[VoiceChannelJoinability.CHANNEL_DOES_NOT_EXIST.ordinal()] = 4;
            iArr[VoiceChannelJoinability.CAN_JOIN.ordinal()] = 5;
        }
    }

    private StageChannelJoinHelper() {
    }

    public static /* synthetic */ void connectToStage$default(StageChannelJoinHelper stageChannelJoinHelper, Context context, FragmentManager fragmentManager, long j, boolean z2, boolean z3, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreChannels storeChannels, StoreVoiceStates storeVoiceStates, StoreUserRelationships storeUserRelationships, Function0 function0, int i, Object obj) {
        stageChannelJoinHelper.connectToStage(context, fragmentManager, j, (i & 8) != 0 ? false : z2, (i & 16) != 0 ? true : z3, (i & 32) != 0 ? StoreStream.Companion.getVoiceChannelSelected() : storeVoiceChannelSelected, (i & 64) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 128) != 0 ? StoreStream.Companion.getVoiceStates() : storeVoiceStates, (i & 256) != 0 ? StoreStream.Companion.getUserRelationships() : storeUserRelationships, (i & 512) != 0 ? StageChannelJoinHelper$connectToStage$1.INSTANCE : function0);
    }

    public static /* synthetic */ Object lurkAndJoinStageAsync$default(StageChannelJoinHelper stageChannelJoinHelper, Context context, FragmentManager fragmentManager, long j, long j2, boolean z2, StoreGuilds storeGuilds, StoreLurking storeLurking, StoreVoiceChannelSelected storeVoiceChannelSelected, Continuation continuation, int i, Object obj) {
        return stageChannelJoinHelper.lurkAndJoinStageAsync(context, fragmentManager, j, j2, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 64) != 0 ? StoreStream.Companion.getLurking() : storeLurking, (i & 128) != 0 ? StoreStream.Companion.getVoiceChannelSelected() : storeVoiceChannelSelected, continuation);
    }

    @MainThread
    public final void connectToStage(Context context, FragmentManager fragmentManager, long j, boolean z2, boolean z3, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreChannels storeChannels, StoreVoiceStates storeVoiceStates, StoreUserRelationships storeUserRelationships, Function0<Unit> function0) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeVoiceStates, "voiceStatesStore");
        m.checkNotNullParameter(storeUserRelationships, "userRelationshipsStore");
        m.checkNotNullParameter(function0, "onCompleted");
        verifyStageJoinability(context, fragmentManager, j, new StageChannelJoinHelper$connectToStage$2(storeChannels, j, storeVoiceStates, storeUserRelationships, z2, storeVoiceChannelSelected, fragmentManager, z3, function0, context));
    }

    @MainThread
    public final void lurkAndJoinStage(Context context, FragmentManager fragmentManager, CoroutineScope coroutineScope, long j, long j2, boolean z2, StoreGuilds storeGuilds, StoreLurking storeLurking, StoreVoiceChannelSelected storeVoiceChannelSelected) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeLurking, "lurkingStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "selectedVoiceChannelStore");
        Job job = lurkJob;
        Job job2 = null;
        if (job != null) {
            f.t(job, null, 1, null);
        }
        if (coroutineScope != null) {
            job2 = f.H0(coroutineScope, null, null, new StageChannelJoinHelper$lurkAndJoinStage$1(context, fragmentManager, j, j2, z2, storeGuilds, storeLurking, storeVoiceChannelSelected, null), 3, null);
        }
        lurkJob = job2;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x00a8  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x011e  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0184 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0185  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x01af  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object lurkAndJoinStageAsync(android.content.Context r34, androidx.fragment.app.FragmentManager r35, long r36, long r38, boolean r40, com.discord.stores.StoreGuilds r41, com.discord.stores.StoreLurking r42, com.discord.stores.StoreVoiceChannelSelected r43, kotlin.coroutines.Continuation<? super java.lang.Boolean> r44) {
        /*
            Method dump skipped, instructions count: 437
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.StageChannelJoinHelper.lurkAndJoinStageAsync(android.content.Context, androidx.fragment.app.FragmentManager, long, long, boolean, com.discord.stores.StoreGuilds, com.discord.stores.StoreLurking, com.discord.stores.StoreVoiceChannelSelected, kotlin.coroutines.Continuation):java.lang.Object");
    }

    @MainThread
    public final void verifyStageJoinability(Context context, FragmentManager fragmentManager, long j, Function0<Unit> function0) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(function0, "onJoinVerified");
        int ordinal = VoiceChannelJoinabilityUtils.INSTANCE.getJoinability(j).ordinal();
        if (ordinal == 0) {
            function0.invoke();
        } else if (ordinal == 1) {
            b.a.d.m.g(context, R.string.channel_locked, 0, null, 12);
        } else if (ordinal == 2) {
            b.a.a.m.k.a(fragmentManager);
        } else if (ordinal == 3) {
            b.a.d.m.g(context, R.string.unable_to_join_channel_full, 0, null, 12);
        } else if (ordinal == 4) {
            b.a.d.m.g(context, R.string.guild_settings_public_welcome_invalid_channel, 0, null, 12);
        }
    }
}
