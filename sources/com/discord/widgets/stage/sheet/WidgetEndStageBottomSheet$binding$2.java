package com.discord.widgets.stage.sheet;

import android.view.View;
import com.discord.databinding.WidgetEndStageBottomSheetBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEndStageBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetEndStageBottomSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetEndStageBottomSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetEndStageBottomSheet$binding$2 extends k implements Function1<View, WidgetEndStageBottomSheetBinding> {
    public static final WidgetEndStageBottomSheet$binding$2 INSTANCE = new WidgetEndStageBottomSheet$binding$2();

    public WidgetEndStageBottomSheet$binding$2() {
        super(1, WidgetEndStageBottomSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetEndStageBottomSheetBinding;", 0);
    }

    public final WidgetEndStageBottomSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        return WidgetEndStageBottomSheetBinding.a(view);
    }
}
