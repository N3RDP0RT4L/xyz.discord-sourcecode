package com.discord.widgets.stage.sheet;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.databinding.StageRaisedHandsItemUserBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.extensions.SimpleDraweeViewExtensionsKt;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetStageRaisedHandsBottomSheetAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001d2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001d\u001e\u001fB\u000f\u0012\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR.\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R.\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010\"\u0004\b\u0015\u0010\u0012R.\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010\u000e\u001a\u0004\b\u0017\u0010\u0010\"\u0004\b\u0018\u0010\u0012¨\u0006 "}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function1;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "", "onDismissRequest", "Lkotlin/jvm/functions/Function1;", "getOnDismissRequest", "()Lkotlin/jvm/functions/Function1;", "setOnDismissRequest", "(Lkotlin/jvm/functions/Function1;)V", "onViewProfile", "getOnViewProfile", "setOnViewProfile", "onInviteToSpeak", "getOnInviteToSpeak", "setOnInviteToSpeak", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "Companion", "ListItem", "ViewHolderParticipant", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStageRaisedHandsBottomSheetAdapter extends MGRecyclerAdapterSimple<ListItem> {
    public static final Companion Companion = new Companion(null);
    private static final int MILLIS_PER_DAY = 86400000;
    private static final int NEW_USER_DAYS_THRESOLD = 7;
    private static final int VIEW_TYPE_PARTICIPANT = 0;
    private Function1<? super StoreVoiceParticipants.VoiceUser, Unit> onViewProfile = WidgetStageRaisedHandsBottomSheetAdapter$onViewProfile$1.INSTANCE;
    private Function1<? super StoreVoiceParticipants.VoiceUser, Unit> onInviteToSpeak = WidgetStageRaisedHandsBottomSheetAdapter$onInviteToSpeak$1.INSTANCE;
    private Function1<? super StoreVoiceParticipants.VoiceUser, Unit> onDismissRequest = WidgetStageRaisedHandsBottomSheetAdapter$onDismissRequest$1.INSTANCE;

    /* compiled from: WidgetStageRaisedHandsBottomSheetAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$Companion;", "", "", "MILLIS_PER_DAY", "I", "NEW_USER_DAYS_THRESOLD", "VIEW_TYPE_PARTICIPANT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetStageRaisedHandsBottomSheetAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", "Participant", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem$Participant;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ListItem implements MGRecyclerDataPayload {

        /* compiled from: WidgetStageRaisedHandsBottomSheetAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b-\u0010.J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ>\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001d\u001a\u00020\u001c2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u001c\u0010\u001f\u001a\u00020\u00178\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\u0019R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\rR\u001c\u0010$\u001a\u00020\u00148\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010\u0016R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010'\u001a\u0004\b(\u0010\u0007R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010)\u001a\u0004\b*\u0010\nR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010+\u001a\u0004\b,\u0010\u0004¨\u0006/"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem$Participant;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component1", "()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component2", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/api/role/GuildRole;", "component4", "()Lcom/discord/api/role/GuildRole;", "participant", "requestToSpeakTimestamp", "member", "role", "copy", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Lcom/discord/api/utcdatetime/UtcDateTime;Lcom/discord/models/member/GuildMember;Lcom/discord/api/role/GuildRole;)Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem$Participant;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "Lcom/discord/api/role/GuildRole;", "getRole", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getRequestToSpeakTimestamp", "Lcom/discord/models/member/GuildMember;", "getMember", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "getParticipant", HookHelper.constructorName, "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Lcom/discord/api/utcdatetime/UtcDateTime;Lcom/discord/models/member/GuildMember;Lcom/discord/api/role/GuildRole;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Participant extends ListItem {
            private final String key;
            private final GuildMember member;
            private final StoreVoiceParticipants.VoiceUser participant;
            private final UtcDateTime requestToSpeakTimestamp;
            private final GuildRole role;
            private final int type;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Participant(StoreVoiceParticipants.VoiceUser voiceUser, UtcDateTime utcDateTime, GuildMember guildMember, GuildRole guildRole) {
                super(null);
                m.checkNotNullParameter(voiceUser, "participant");
                this.participant = voiceUser;
                this.requestToSpeakTimestamp = utcDateTime;
                this.member = guildMember;
                this.role = guildRole;
                this.key = String.valueOf(voiceUser.getUser().getId());
            }

            public static /* synthetic */ Participant copy$default(Participant participant, StoreVoiceParticipants.VoiceUser voiceUser, UtcDateTime utcDateTime, GuildMember guildMember, GuildRole guildRole, int i, Object obj) {
                if ((i & 1) != 0) {
                    voiceUser = participant.participant;
                }
                if ((i & 2) != 0) {
                    utcDateTime = participant.requestToSpeakTimestamp;
                }
                if ((i & 4) != 0) {
                    guildMember = participant.member;
                }
                if ((i & 8) != 0) {
                    guildRole = participant.role;
                }
                return participant.copy(voiceUser, utcDateTime, guildMember, guildRole);
            }

            public final StoreVoiceParticipants.VoiceUser component1() {
                return this.participant;
            }

            public final UtcDateTime component2() {
                return this.requestToSpeakTimestamp;
            }

            public final GuildMember component3() {
                return this.member;
            }

            public final GuildRole component4() {
                return this.role;
            }

            public final Participant copy(StoreVoiceParticipants.VoiceUser voiceUser, UtcDateTime utcDateTime, GuildMember guildMember, GuildRole guildRole) {
                m.checkNotNullParameter(voiceUser, "participant");
                return new Participant(voiceUser, utcDateTime, guildMember, guildRole);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Participant)) {
                    return false;
                }
                Participant participant = (Participant) obj;
                return m.areEqual(this.participant, participant.participant) && m.areEqual(this.requestToSpeakTimestamp, participant.requestToSpeakTimestamp) && m.areEqual(this.member, participant.member) && m.areEqual(this.role, participant.role);
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final GuildMember getMember() {
                return this.member;
            }

            public final StoreVoiceParticipants.VoiceUser getParticipant() {
                return this.participant;
            }

            public final UtcDateTime getRequestToSpeakTimestamp() {
                return this.requestToSpeakTimestamp;
            }

            public final GuildRole getRole() {
                return this.role;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                StoreVoiceParticipants.VoiceUser voiceUser = this.participant;
                int i = 0;
                int hashCode = (voiceUser != null ? voiceUser.hashCode() : 0) * 31;
                UtcDateTime utcDateTime = this.requestToSpeakTimestamp;
                int hashCode2 = (hashCode + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
                GuildMember guildMember = this.member;
                int hashCode3 = (hashCode2 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
                GuildRole guildRole = this.role;
                if (guildRole != null) {
                    i = guildRole.hashCode();
                }
                return hashCode3 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Participant(participant=");
                R.append(this.participant);
                R.append(", requestToSpeakTimestamp=");
                R.append(this.requestToSpeakTimestamp);
                R.append(", member=");
                R.append(this.member);
                R.append(", role=");
                R.append(this.role);
                R.append(")");
                return R.toString();
            }
        }

        private ListItem() {
        }

        public /* synthetic */ ListItem(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetStageRaisedHandsBottomSheetAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0018\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0007\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ViewHolderParticipant;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem;", "Landroid/content/Context;", "context", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem$Participant;", "data", "", "getParticipantMemberInfo", "(Landroid/content/Context;Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem$Participant;)Ljava/lang/String;", "Lcom/discord/models/user/User;", "user", "", "isNewUser", "(Lcom/discord/models/user/User;)Z", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onConfigure", "(ILcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem;)V", "Lcom/discord/databinding/StageRaisedHandsItemUserBinding;", "binding", "Lcom/discord/databinding/StageRaisedHandsItemUserBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewHolderParticipant extends MGRecyclerViewHolder<WidgetStageRaisedHandsBottomSheetAdapter, ListItem> {
        private final StageRaisedHandsItemUserBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolderParticipant(WidgetStageRaisedHandsBottomSheetAdapter widgetStageRaisedHandsBottomSheetAdapter) {
            super((int) R.layout.stage_raised_hands_item_user, widgetStageRaisedHandsBottomSheetAdapter);
            m.checkNotNullParameter(widgetStageRaisedHandsBottomSheetAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.stage_raised_hands_item_dismiss_button;
            ImageView imageView = (ImageView) view.findViewById(R.id.stage_raised_hands_item_dismiss_button);
            if (imageView != null) {
                i = R.id.stage_raised_hands_item_invite_button;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.stage_raised_hands_item_invite_button);
                if (imageView2 != null) {
                    i = R.id.stage_raised_hands_item_role;
                    TextView textView = (TextView) view.findViewById(R.id.stage_raised_hands_item_role);
                    if (textView != null) {
                        i = R.id.stage_raised_hands_item_user_avatar;
                        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.stage_raised_hands_item_user_avatar);
                        if (simpleDraweeView != null) {
                            i = R.id.stage_raised_hands_item_user_name;
                            TextView textView2 = (TextView) view.findViewById(R.id.stage_raised_hands_item_user_name);
                            if (textView2 != null) {
                                StageRaisedHandsItemUserBinding stageRaisedHandsItemUserBinding = new StageRaisedHandsItemUserBinding((ConstraintLayout) view, imageView, imageView2, textView, simpleDraweeView, textView2);
                                m.checkNotNullExpressionValue(stageRaisedHandsItemUserBinding, "StageRaisedHandsItemUserBinding.bind(itemView)");
                                this.binding = stageRaisedHandsItemUserBinding;
                                return;
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetStageRaisedHandsBottomSheetAdapter access$getAdapter$p(ViewHolderParticipant viewHolderParticipant) {
            return (WidgetStageRaisedHandsBottomSheetAdapter) viewHolderParticipant.adapter;
        }

        private final String getParticipantMemberInfo(Context context, ListItem.Participant participant) {
            String g;
            if (isNewUser(participant.getParticipant().getUser())) {
                String string = context.getString(R.string.request_to_speak_member_info_new_to_discord);
                m.checkNotNullExpressionValue(string, "context.getString(R.stri…mber_info_new_to_discord)");
                return string;
            }
            GuildMember member = participant.getMember();
            UtcDateTime joinedAt = member != null ? member.getJoinedAt() : null;
            if (joinedAt == null) {
                String string2 = context.getString(R.string.request_to_speak_member_info_non_member);
                m.checkNotNullExpressionValue(string2, "context.getString(R.stri…k_member_info_non_member)");
                return string2;
            } else if (!participant.getMember().getRoles().isEmpty()) {
                GuildRole role = participant.getRole();
                if (role != null && (g = role.g()) != null) {
                    return g;
                }
                String string3 = context.getString(R.string.request_to_speak_member_info_member_roles);
                m.checkNotNullExpressionValue(string3, "context.getString(R.stri…member_info_member_roles)");
                return string3;
            } else if (ClockFactory.get().currentTimeMillis() - joinedAt.g() < ((long) WidgetStageRaisedHandsBottomSheetAdapter.MILLIS_PER_DAY)) {
                String string4 = context.getString(R.string.request_to_speak_member_info_new_member);
                m.checkNotNullExpressionValue(string4, "context.getString(R.stri…k_member_info_new_member)");
                return string4;
            } else {
                String string5 = context.getString(R.string.request_to_speak_member_info_member);
                m.checkNotNullExpressionValue(string5, "context.getString(R.stri…speak_member_info_member)");
                return string5;
            }
        }

        private final boolean isNewUser(User user) {
            return ClockFactory.get().currentTimeMillis() - ((user.getId() >>> 22) + SnowflakeUtils.DISCORD_EPOCH) <= ((long) 604800000);
        }

        public void onConfigure(int i, ListItem listItem) {
            m.checkNotNullParameter(listItem, "data");
            super.onConfigure(i, (int) listItem);
            ListItem.Participant participant = (ListItem.Participant) listItem;
            final StoreVoiceParticipants.VoiceUser component1 = participant.component1();
            GuildMember component3 = participant.component3();
            this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetAdapter$ViewHolderParticipant$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetStageRaisedHandsBottomSheetAdapter.ViewHolderParticipant.access$getAdapter$p(WidgetStageRaisedHandsBottomSheetAdapter.ViewHolderParticipant.this).getOnViewProfile().invoke(component1);
                }
            });
            ImageView imageView = this.binding.c;
            ViewExtensions.setEnabledAndAlpha$default(imageView, !component1.isInvitedToSpeak(), 0.0f, 2, null);
            imageView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetAdapter$ViewHolderParticipant$onConfigure$$inlined$apply$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetStageRaisedHandsBottomSheetAdapter.ViewHolderParticipant.access$getAdapter$p(WidgetStageRaisedHandsBottomSheetAdapter.ViewHolderParticipant.this).getOnInviteToSpeak().invoke(component1);
                }
            });
            this.binding.f2129b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetAdapter$ViewHolderParticipant$onConfigure$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetStageRaisedHandsBottomSheetAdapter.ViewHolderParticipant.access$getAdapter$p(WidgetStageRaisedHandsBottomSheetAdapter.ViewHolderParticipant.this).getOnDismissRequest().invoke(component1);
                }
            });
            SimpleDraweeView simpleDraweeView = this.binding.e;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.stageRaisedHandsItemUserAvatar");
            SimpleDraweeViewExtensionsKt.setAvatar$default(simpleDraweeView, component1.getUser(), false, R.dimen.avatar_size_standard, component1.getGuildMember(), 2, null);
            TextView textView = this.binding.f;
            m.checkNotNullExpressionValue(textView, "binding.stageRaisedHandsItemUserName");
            textView.setText(component1.getDisplayName());
            if (!(component3 == null || component3.getColor() == -16777216)) {
                this.binding.f.setTextColor(component3.getColor());
            }
            TextView textView2 = this.binding.d;
            Context context = textView2.getContext();
            m.checkNotNullExpressionValue(context, "context");
            textView2.setText(getParticipantMemberInfo(context, participant));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStageRaisedHandsBottomSheetAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
    }

    public final Function1<StoreVoiceParticipants.VoiceUser, Unit> getOnDismissRequest() {
        return this.onDismissRequest;
    }

    public final Function1<StoreVoiceParticipants.VoiceUser, Unit> getOnInviteToSpeak() {
        return this.onInviteToSpeak;
    }

    public final Function1<StoreVoiceParticipants.VoiceUser, Unit> getOnViewProfile() {
        return this.onViewProfile;
    }

    public final void setOnDismissRequest(Function1<? super StoreVoiceParticipants.VoiceUser, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onDismissRequest = function1;
    }

    public final void setOnInviteToSpeak(Function1<? super StoreVoiceParticipants.VoiceUser, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onInviteToSpeak = function1;
    }

    public final void setOnViewProfile(Function1<? super StoreVoiceParticipants.VoiceUser, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onViewProfile = function1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, ListItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new ViewHolderParticipant(this);
        }
        throw invalidViewTypeException(i);
    }
}
