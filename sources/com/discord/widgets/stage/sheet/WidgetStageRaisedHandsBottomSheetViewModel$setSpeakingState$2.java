package com.discord.widgets.stage.sheet;

import com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStageRaisedHandsBottomSheetViewModel$setSpeakingState$2 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetStageRaisedHandsBottomSheetViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStageRaisedHandsBottomSheetViewModel$setSpeakingState$2(WidgetStageRaisedHandsBottomSheetViewModel widgetStageRaisedHandsBottomSheetViewModel) {
        super(0);
        this.this$0 = widgetStageRaisedHandsBottomSheetViewModel;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetStageRaisedHandsBottomSheetViewModel.ViewState viewState;
        viewState = this.this$0.getViewState();
        if (!(viewState instanceof WidgetStageRaisedHandsBottomSheetViewModel.ViewState.Loaded)) {
            viewState = null;
        }
        WidgetStageRaisedHandsBottomSheetViewModel.ViewState.Loaded loaded = (WidgetStageRaisedHandsBottomSheetViewModel.ViewState.Loaded) viewState;
        if (loaded != null) {
            this.this$0.updateViewState(WidgetStageRaisedHandsBottomSheetViewModel.ViewState.Loaded.copy$default(loaded, null, null, false, false, false, false, 31, null));
        }
    }
}
