package com.discord.widgets.stage.sheet;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.databinding.StageAudienceBlockedBottomSheetHeaderItemBinding;
import com.discord.databinding.StageAudienceBlockedBottomSheetItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.user.User;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.views.CutoutView;
import com.discord.widgets.user.UserNameFormatterKt;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textview.MaterialTextView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: StageAudienceBlockedBottomSheetAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00112\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u0011\u0012\u0013\u0014B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\u000b\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "getItemViewType", "(I)I", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "Companion", "HeaderViewHolder", "ListItem", "ParticipantViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageAudienceBlockedBottomSheetAdapter extends MGRecyclerAdapterSimple<MGRecyclerDataPayload> {
    public static final Companion Companion = new Companion(null);
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_PARTICIPANT = 1;

    /* compiled from: StageAudienceBlockedBottomSheetAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$Companion;", "", "", "TYPE_HEADER", "I", "TYPE_PARTICIPANT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StageAudienceBlockedBottomSheetAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$HeaderViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Lcom/discord/databinding/StageAudienceBlockedBottomSheetHeaderItemBinding;", "binding", "Lcom/discord/databinding/StageAudienceBlockedBottomSheetHeaderItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class HeaderViewHolder extends MGRecyclerViewHolder<StageAudienceBlockedBottomSheetAdapter, MGRecyclerDataPayload> {
        private final StageAudienceBlockedBottomSheetHeaderItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public HeaderViewHolder(StageAudienceBlockedBottomSheetAdapter stageAudienceBlockedBottomSheetAdapter) {
            super((int) R.layout.stage_audience_blocked_bottom_sheet_header_item, stageAudienceBlockedBottomSheetAdapter);
            m.checkNotNullParameter(stageAudienceBlockedBottomSheetAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.description;
            MaterialTextView materialTextView = (MaterialTextView) view.findViewById(R.id.description);
            if (materialTextView != null) {
                i = R.id.title;
                MaterialTextView materialTextView2 = (MaterialTextView) view.findViewById(R.id.title);
                if (materialTextView2 != null) {
                    StageAudienceBlockedBottomSheetHeaderItemBinding stageAudienceBlockedBottomSheetHeaderItemBinding = new StageAudienceBlockedBottomSheetHeaderItemBinding((ConstraintLayout) view, materialTextView, materialTextView2);
                    m.checkNotNullExpressionValue(stageAudienceBlockedBottomSheetHeaderItemBinding, "StageAudienceBlockedBott…temBinding.bind(itemView)");
                    this.binding = stageAudienceBlockedBottomSheetHeaderItemBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public void onConfigure(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
            CharSequence b2;
            CharSequence b3;
            m.checkNotNullParameter(mGRecyclerDataPayload, "data");
            super.onConfigure(i, (int) mGRecyclerDataPayload);
            if (mGRecyclerDataPayload instanceof ListItem.Header) {
                MaterialTextView materialTextView = this.binding.c;
                m.checkNotNullExpressionValue(materialTextView, "binding.title");
                ListItem.Header header = (ListItem.Header) mGRecyclerDataPayload;
                b2 = b.b(a.x(this.itemView, "itemView", "itemView.context"), R.string.stage_blocked_users_title_plural, new Object[]{StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.stage_blocked_users_title_plural_number, header.getNumBlockedUsers(), new Object[0])}, (r4 & 4) != 0 ? b.C0034b.j : null);
                materialTextView.setText(b2);
                MaterialTextView materialTextView2 = this.binding.f2127b;
                m.checkNotNullExpressionValue(materialTextView2, "binding.description");
                b3 = b.b(a.x(this.itemView, "itemView", "itemView.context"), R.string.stage_blocked_users_body, new Object[]{StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.stage_blocked_users_body_number, header.getNumBlockedUsers(), Integer.valueOf(header.getNumBlockedUsers()))}, (r4 & 4) != 0 ? b.C0034b.j : null);
                materialTextView2.setText(b3);
            }
        }
    }

    /* compiled from: StageAudienceBlockedBottomSheetAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", Traits.Location.Section.HEADER, "Participant", "Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem$Participant;", "Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem$Header;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ListItem implements MGRecyclerDataPayload {

        /* compiled from: StageAudienceBlockedBottomSheetAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u00020\b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\nR\u001c\u0010\u0014\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0015\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem$Header;", "Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem;", "", "component1", "()I", "numBlockedUsers", "copy", "(I)Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem$Header;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "type", "I", "getType", "getNumBlockedUsers", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Header extends ListItem {
            private final String key = "AudienceBlockedHeader";
            private final int numBlockedUsers;
            private final int type;

            public Header(int i) {
                super(null);
                this.numBlockedUsers = i;
            }

            public static /* synthetic */ Header copy$default(Header header, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = header.numBlockedUsers;
                }
                return header.copy(i);
            }

            public final int component1() {
                return this.numBlockedUsers;
            }

            public final Header copy(int i) {
                return new Header(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Header) && this.numBlockedUsers == ((Header) obj).numBlockedUsers;
                }
                return true;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final int getNumBlockedUsers() {
                return this.numBlockedUsers;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                return this.numBlockedUsers;
            }

            public String toString() {
                return a.A(a.R("Header(numBlockedUsers="), this.numBlockedUsers, ")");
            }
        }

        /* compiled from: StageAudienceBlockedBottomSheetAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\t\u0010\u0007R\u001c\u0010\u0019\u001a\u00020\u000f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u0011R\u001c\u0010\u001c\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u000e¨\u0006!"}, d2 = {"Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem$Participant;", "Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component1", "()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "", "component2", "()Z", "participant", "isSpeaker", "copy", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Z)Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ListItem$Participant;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "getParticipant", "Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Participant extends ListItem {
            private final boolean isSpeaker;
            private final String key;
            private final StoreVoiceParticipants.VoiceUser participant;
            private final int type = 1;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Participant(StoreVoiceParticipants.VoiceUser voiceUser, boolean z2) {
                super(null);
                m.checkNotNullParameter(voiceUser, "participant");
                this.participant = voiceUser;
                this.isSpeaker = z2;
                this.key = String.valueOf(voiceUser.getUser().getId());
            }

            public static /* synthetic */ Participant copy$default(Participant participant, StoreVoiceParticipants.VoiceUser voiceUser, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    voiceUser = participant.participant;
                }
                if ((i & 2) != 0) {
                    z2 = participant.isSpeaker;
                }
                return participant.copy(voiceUser, z2);
            }

            public final StoreVoiceParticipants.VoiceUser component1() {
                return this.participant;
            }

            public final boolean component2() {
                return this.isSpeaker;
            }

            public final Participant copy(StoreVoiceParticipants.VoiceUser voiceUser, boolean z2) {
                m.checkNotNullParameter(voiceUser, "participant");
                return new Participant(voiceUser, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Participant)) {
                    return false;
                }
                Participant participant = (Participant) obj;
                return m.areEqual(this.participant, participant.participant) && this.isSpeaker == participant.isSpeaker;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final StoreVoiceParticipants.VoiceUser getParticipant() {
                return this.participant;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                StoreVoiceParticipants.VoiceUser voiceUser = this.participant;
                int hashCode = (voiceUser != null ? voiceUser.hashCode() : 0) * 31;
                boolean z2 = this.isSpeaker;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public final boolean isSpeaker() {
                return this.isSpeaker;
            }

            public String toString() {
                StringBuilder R = a.R("Participant(participant=");
                R.append(this.participant);
                R.append(", isSpeaker=");
                return a.M(R, this.isSpeaker, ")");
            }
        }

        private ListItem() {
        }

        public /* synthetic */ ListItem(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StageAudienceBlockedBottomSheetAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter$ParticipantViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Lcom/discord/databinding/StageAudienceBlockedBottomSheetItemBinding;", "binding", "Lcom/discord/databinding/StageAudienceBlockedBottomSheetItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/stage/sheet/StageAudienceBlockedBottomSheetAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ParticipantViewHolder extends MGRecyclerViewHolder<StageAudienceBlockedBottomSheetAdapter, MGRecyclerDataPayload> {
        private final StageAudienceBlockedBottomSheetItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ParticipantViewHolder(StageAudienceBlockedBottomSheetAdapter stageAudienceBlockedBottomSheetAdapter) {
            super((int) R.layout.stage_audience_blocked_bottom_sheet_item, stageAudienceBlockedBottomSheetAdapter);
            m.checkNotNullParameter(stageAudienceBlockedBottomSheetAdapter, "adapter");
            View view = this.itemView;
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            int i = R.id.stage_channel_blocked_member_icon;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.stage_channel_blocked_member_icon);
            if (simpleDraweeView != null) {
                i = R.id.stage_channel_blocked_member_icon_container;
                CutoutView cutoutView = (CutoutView) view.findViewById(R.id.stage_channel_blocked_member_icon_container);
                if (cutoutView != null) {
                    i = R.id.stage_channel_blocked_member_name;
                    TextView textView = (TextView) view.findViewById(R.id.stage_channel_blocked_member_name);
                    if (textView != null) {
                        i = R.id.stage_channel_blocked_member_speaker;
                        ImageView imageView = (ImageView) view.findViewById(R.id.stage_channel_blocked_member_speaker);
                        if (imageView != null) {
                            i = R.id.stage_channel_blocked_text;
                            TextView textView2 = (TextView) view.findViewById(R.id.stage_channel_blocked_text);
                            if (textView2 != null) {
                                i = R.id.stage_channel_blocked_text_divider;
                                View findViewById = view.findViewById(R.id.stage_channel_blocked_text_divider);
                                if (findViewById != null) {
                                    i = R.id.stage_channel_blocked_text_speaker;
                                    TextView textView3 = (TextView) view.findViewById(R.id.stage_channel_blocked_text_speaker);
                                    if (textView3 != null) {
                                        StageAudienceBlockedBottomSheetItemBinding stageAudienceBlockedBottomSheetItemBinding = new StageAudienceBlockedBottomSheetItemBinding((ConstraintLayout) view, constraintLayout, simpleDraweeView, cutoutView, textView, imageView, textView2, findViewById, textView3);
                                        m.checkNotNullExpressionValue(stageAudienceBlockedBottomSheetItemBinding, "StageAudienceBlockedBott…temBinding.bind(itemView)");
                                        this.binding = stageAudienceBlockedBottomSheetItemBinding;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public void onConfigure(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
            String str;
            m.checkNotNullParameter(mGRecyclerDataPayload, "data");
            super.onConfigure(i, (int) mGRecyclerDataPayload);
            if (mGRecyclerDataPayload instanceof ListItem.Participant) {
                ListItem.Participant participant = (ListItem.Participant) mGRecyclerDataPayload;
                this.binding.c.setCutoutEnabled(participant.isSpeaker());
                ImageView imageView = this.binding.e;
                imageView.setBackgroundResource(R.drawable.drawable_uikit_white_circle);
                imageView.setImageResource(R.drawable.ic_mic_grey_24dp);
                imageView.setVisibility(participant.isSpeaker() ? 0 : 4);
                imageView.setActivated(participant.isSpeaker());
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Resources resources = view.getResources();
                TextView textView = this.binding.f;
                m.checkNotNullExpressionValue(textView, "binding.stageChannelBlockedTextSpeaker");
                if (participant.isSpeaker()) {
                    str = resources.getString(R.string.stage_speaker);
                } else {
                    str = resources.getString(R.string.stage_audience);
                }
                textView.setText(str);
                TextView textView2 = this.binding.d;
                User user = participant.getParticipant().getUser();
                String nickname = participant.getParticipant().getNickname();
                Context context = textView2.getContext();
                m.checkNotNullExpressionValue(context, "context");
                textView2.setText(UserNameFormatterKt.getSpannableForUserNameWithDiscrim(user, nickname, context, R.attr.colorHeaderPrimary, R.attr.font_primary_semibold, R.integer.uikit_textsize_large_sp, R.attr.colorTextMuted, R.attr.font_primary_normal, R.integer.uikit_textsize_medium_sp));
                SimpleDraweeView simpleDraweeView = this.binding.f2128b;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.stageChannelBlockedMemberIcon");
                IconUtils.setIcon$default(simpleDraweeView, participant.getParticipant().getUser().getUsername(), Long.valueOf(participant.getParticipant().getUser().getId()), participant.getParticipant().getUser().getAvatar(), Integer.valueOf((int) R.dimen.avatar_size_large), 0, null, null, participant.getParticipant().getGuildMember(), 224, null);
                SimpleDraweeView simpleDraweeView2 = this.binding.f2128b;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.stageChannelBlockedMemberIcon");
                simpleDraweeView2.setAlpha(0.5f);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StageAudienceBlockedBottomSheetAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recyclerView");
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(this);
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple, androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return i != 0 ? 1 : 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, MGRecyclerDataPayload> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new HeaderViewHolder(this);
        }
        if (i == 1) {
            return new ParticipantViewHolder(this);
        }
        throw invalidViewTypeException(i);
    }
}
