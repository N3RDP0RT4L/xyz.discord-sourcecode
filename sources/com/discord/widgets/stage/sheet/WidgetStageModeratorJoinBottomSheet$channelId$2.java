package com.discord.widgets.stage.sheet;

import android.os.Bundle;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetStageModeratorJoinBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "invoke", "()J", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStageModeratorJoinBottomSheet$channelId$2 extends o implements Function0<Long> {
    public final /* synthetic */ WidgetStageModeratorJoinBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStageModeratorJoinBottomSheet$channelId$2(WidgetStageModeratorJoinBottomSheet widgetStageModeratorJoinBottomSheet) {
        super(0);
        this.this$0 = widgetStageModeratorJoinBottomSheet;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [long, java.lang.Long] */
    @Override // kotlin.jvm.functions.Function0
    public final Long invoke() {
        Bundle argumentsOrDefault;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        return argumentsOrDefault.getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID");
    }
}
