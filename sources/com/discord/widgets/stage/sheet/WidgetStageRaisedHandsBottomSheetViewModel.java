package com.discord.widgets.stage.sheet;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.PermissionOverwriteUtilsKt;
import com.discord.utilities.error.Error;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.time.Clock;
import com.discord.widgets.stage.StageChannelAPI;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetAdapter;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.subjects.PublishSubject;
/* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 ?2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004?@ABB_\u0012\n\u0010'\u001a\u00060\u0003j\u0002`&\u0012\b\b\u0002\u0010$\u001a\u00020#\u0012\b\b\u0002\u0010*\u001a\u00020)\u0012\b\b\u0002\u0010-\u001a\u00020,\u0012\b\b\u0002\u00107\u001a\u000206\u0012\b\b\u0002\u00109\u001a\u000208\u0012\b\b\u0002\u0010;\u001a\u00020:\u0012\u000e\b\u0002\u0010<\u001a\b\u0012\u0004\u0012\u00020\f0\b¢\u0006\u0004\b=\u0010>J+\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\t0\b2\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0013\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\b¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u001f\u001a\u00020\u000e2\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 J\u0015\u0010!\u001a\u00020\u000e2\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b!\u0010 J\u0015\u0010\"\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\"\u0010\u001cR\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u001a\u0010'\u001a\u00060\u0003j\u0002`&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u0018\u00100\u001a\u0004\u0018\u00010/8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101R:\u00104\u001a&\u0012\f\u0012\n 3*\u0004\u0018\u00010\u00170\u0017 3*\u0012\u0012\f\u0012\n 3*\u0004\u0018\u00010\u00170\u0017\u0018\u000102028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105¨\u0006C"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState;", "", "Lcom/discord/primitives/UserId;", "userId", "", "isSuppressed", "Lrx/Observable;", "Ljava/lang/Void;", "setUserSuppressed", "(JZ)Lrx/Observable;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;)V", "emitDismiss", "()V", "Lcom/discord/utilities/error/Error;", "error", "emitError", "(Lcom/discord/utilities/error/Error;)V", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event;", "observeEvents", "()Lrx/Observable;", "isEnabled", "setRequestToSpeakEnabled", "(Z)V", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "participant", "dismissRequestToSpeak", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V", "inviteToSpeak", "setSpeakingState", "Lcom/discord/stores/StoreVoiceParticipants;", "voiceParticipants", "Lcom/discord/stores/StoreVoiceParticipants;", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreUser;", "usersStore", "Lcom/discord/stores/StoreUser;", "Lrx/Subscription;", "lastRequestToSpeakSubscription", "Lrx/Subscription;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreStageChannels;", "stageChannelsStore", "Lcom/discord/utilities/time/Clock;", "clock", "storeStateObservable", HookHelper.constructorName, "(JLcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreStageChannels;Lcom/discord/utilities/time/Clock;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStageRaisedHandsBottomSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final long channelId;
    private final StoreChannels channelsStore;
    private final PublishSubject<Event> eventSubject;
    private Subscription lastRequestToSpeakSubscription;
    private final StoreUser usersStore;
    private final StoreVoiceParticipants voiceParticipants;

    /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetStageRaisedHandsBottomSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014JI\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreVoiceParticipants;", "voiceParticipants", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreStageChannels;", "stageChannels", "Lcom/discord/utilities/time/Clock;", "clock", "Lrx/Observable;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreStageChannels;Lcom/discord/utilities/time/Clock;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, StoreVoiceParticipants storeVoiceParticipants, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreStageChannels storeStageChannels, Clock clock) {
            Observable<R> F = storeChannels.observeChannel(j).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
            m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
            Observable<StoreState> Y = F.q().Y(new WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1(storeGuilds, storeVoiceParticipants, j, storeStageChannels, clock));
            m.checkNotNullExpressionValue(Y, "channelsStore.observeCha…          }\n            }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "Dismiss", "Error", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event$Dismiss;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event$Dismiss;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Dismiss extends Event {
            public static final Dismiss INSTANCE = new Dismiss();

            private Dismiss() {
                super(null);
            }
        }

        /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event$Error;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event;", "Lcom/discord/utilities/error/Error;", "component1", "()Lcom/discord/utilities/error/Error;", "error", "copy", "(Lcom/discord/utilities/error/Error;)Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$Event$Error;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/error/Error;", "getError", HookHelper.constructorName, "(Lcom/discord/utilities/error/Error;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends Event {
            private final com.discord.utilities.error.Error error;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Error(com.discord.utilities.error.Error error) {
                super(null);
                m.checkNotNullParameter(error, "error");
                this.error = error;
            }

            public static /* synthetic */ Error copy$default(Error error, com.discord.utilities.error.Error error2, int i, Object obj) {
                if ((i & 1) != 0) {
                    error2 = error.error;
                }
                return error.copy(error2);
            }

            public final com.discord.utilities.error.Error component1() {
                return this.error;
            }

            public final Error copy(com.discord.utilities.error.Error error) {
                m.checkNotNullParameter(error, "error");
                return new Error(error);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Error) && m.areEqual(this.error, ((Error) obj).error);
                }
                return true;
            }

            public final com.discord.utilities.error.Error getError() {
                return this.error;
            }

            public int hashCode() {
                com.discord.utilities.error.Error error = this.error;
                if (error != null) {
                    return error.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Error(error=");
                R.append(this.error);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B@\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0006\u0010\u0016\u001a\u00020\u000fø\u0001\u0000¢\u0006\u0004\b+\u0010,J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u0012\u001a\u00020\u000fHÆ\u0003ø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b\u0010\u0010\u0011JT\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0013\u001a\u00020\u00022\u0018\b\u0002\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\b\u0002\u0010\u0016\u001a\u00020\u000fHÆ\u0001ø\u0001\u0000ø\u0001\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0011J\u001a\u0010!\u001a\u00020 2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b$\u0010\u0004R\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b&\u0010\u000eR)\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010'\u001a\u0004\b(\u0010\nR\"\u0010\u0016\u001a\u00020\u000f8\u0006@\u0006ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\f\n\u0004\b\u0016\u0010)\u001a\u0004\b*\u0010\u0011\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b¡\u001e0\u0001¨\u0006-"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component2", "()Ljava/util/Map;", "", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem;", "component3", "()Ljava/util/List;", "Lcom/discord/widgets/stage/StageRoles;", "component4-1LxfuJo", "()I", "component4", "channel", "roles", "raisedHandsParticipants", "myStageRoles", "copy-FZjiw-U", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/List;I)Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;", "copy", "", "toString", "()Ljava/lang/String;", "", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/List;", "getRaisedHandsParticipants", "Ljava/util/Map;", "getRoles", "I", "getMyStageRoles-1LxfuJo", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Channel channel;
        private final int myStageRoles;
        private final List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> raisedHandsParticipants;
        private final Map<Long, GuildRole> roles;

        /* JADX WARN: Multi-variable type inference failed */
        private StoreState(Channel channel, Map<Long, GuildRole> map, List<? extends WidgetStageRaisedHandsBottomSheetAdapter.ListItem> list, int i) {
            this.channel = channel;
            this.roles = map;
            this.raisedHandsParticipants = list;
            this.myStageRoles = i;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: copy-FZjiw-U$default  reason: not valid java name */
        public static /* synthetic */ StoreState m41copyFZjiwU$default(StoreState storeState, Channel channel, Map map, List list, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                channel = storeState.channel;
            }
            if ((i2 & 2) != 0) {
                map = storeState.roles;
            }
            if ((i2 & 4) != 0) {
                list = storeState.raisedHandsParticipants;
            }
            if ((i2 & 8) != 0) {
                i = storeState.myStageRoles;
            }
            return storeState.m43copyFZjiwU(channel, map, list, i);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final Map<Long, GuildRole> component2() {
            return this.roles;
        }

        public final List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> component3() {
            return this.raisedHandsParticipants;
        }

        /* renamed from: component4-1LxfuJo  reason: not valid java name */
        public final int m42component41LxfuJo() {
            return this.myStageRoles;
        }

        /* renamed from: copy-FZjiw-U  reason: not valid java name */
        public final StoreState m43copyFZjiwU(Channel channel, Map<Long, GuildRole> map, List<? extends WidgetStageRaisedHandsBottomSheetAdapter.ListItem> list, int i) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "roles");
            m.checkNotNullParameter(list, "raisedHandsParticipants");
            return new StoreState(channel, map, list, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.channel, storeState.channel) && m.areEqual(this.roles, storeState.roles) && m.areEqual(this.raisedHandsParticipants, storeState.raisedHandsParticipants) && this.myStageRoles == storeState.myStageRoles;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        /* renamed from: getMyStageRoles-1LxfuJo  reason: not valid java name */
        public final int m44getMyStageRoles1LxfuJo() {
            return this.myStageRoles;
        }

        public final List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> getRaisedHandsParticipants() {
            return this.raisedHandsParticipants;
        }

        public final Map<Long, GuildRole> getRoles() {
            return this.roles;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            Map<Long, GuildRole> map = this.roles;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> list = this.raisedHandsParticipants;
            if (list != null) {
                i = list.hashCode();
            }
            return ((hashCode2 + i) * 31) + this.myStageRoles;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(channel=");
            R.append(this.channel);
            R.append(", roles=");
            R.append(this.roles);
            R.append(", raisedHandsParticipants=");
            R.append(this.raisedHandsParticipants);
            R.append(", myStageRoles=");
            R.append(StageRoles.m28toStringimpl(this.myStageRoles));
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ StoreState(Channel channel, Map map, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(channel, map, list, i);
        }
    }

    /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Loaded", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u0006\u0010\u0012\u001a\u00020\t\u0012\b\b\u0002\u0010\u0013\u001a\u00020\t\u0012\b\b\u0002\u0010\u0014\u001a\u00020\t¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\r\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJ\u0010\u0010\u000e\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000bJR\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0011\u001a\u00020\t2\b\b\u0002\u0010\u0012\u001a\u00020\t2\b\b\u0002\u0010\u0013\u001a\u00020\t2\b\b\u0002\u0010\u0014\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001f\u001a\u00020\t2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b\u0012\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\u0011\u0010\u000bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0004R\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010$\u001a\u0004\b%\u0010\bR\u0019\u0010&\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010!\u001a\u0004\b&\u0010\u000bR\u0019\u0010\u0014\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b\u0014\u0010\u000bR\u0019\u0010\u0013\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010!\u001a\u0004\b\u0013\u0010\u000b¨\u0006)"}, d2 = {"Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem;", "component2", "()Ljava/util/List;", "", "component3", "()Z", "component4", "component5", "component6", "channel", "raisedHandsParticipants", "isRequestToSpeakEnabled", "isSpeaker", "isUpdatingRequestToSpeakPermissions", "isTogglingSpeakingState", "copy", "(Lcom/discord/api/channel/Channel;Ljava/util/List;ZZZZ)Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/List;", "getRaisedHandsParticipants", "isEmpty", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/util/List;ZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final Channel channel;
            private final boolean isEmpty;
            private final boolean isRequestToSpeakEnabled;
            private final boolean isSpeaker;
            private final boolean isTogglingSpeakingState;
            private final boolean isUpdatingRequestToSpeakPermissions;
            private final List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> raisedHandsParticipants;

            public /* synthetic */ Loaded(Channel channel, List list, boolean z2, boolean z3, boolean z4, boolean z5, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(channel, list, z2, z3, (i & 16) != 0 ? false : z4, (i & 32) != 0 ? false : z5);
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, Channel channel, List list, boolean z2, boolean z3, boolean z4, boolean z5, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = loaded.channel;
                }
                List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> list2 = list;
                if ((i & 2) != 0) {
                    list2 = loaded.raisedHandsParticipants;
                }
                List list3 = list2;
                if ((i & 4) != 0) {
                    z2 = loaded.isRequestToSpeakEnabled;
                }
                boolean z6 = z2;
                if ((i & 8) != 0) {
                    z3 = loaded.isSpeaker;
                }
                boolean z7 = z3;
                if ((i & 16) != 0) {
                    z4 = loaded.isUpdatingRequestToSpeakPermissions;
                }
                boolean z8 = z4;
                if ((i & 32) != 0) {
                    z5 = loaded.isTogglingSpeakingState;
                }
                return loaded.copy(channel, list3, z6, z7, z8, z5);
            }

            public final Channel component1() {
                return this.channel;
            }

            public final List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> component2() {
                return this.raisedHandsParticipants;
            }

            public final boolean component3() {
                return this.isRequestToSpeakEnabled;
            }

            public final boolean component4() {
                return this.isSpeaker;
            }

            public final boolean component5() {
                return this.isUpdatingRequestToSpeakPermissions;
            }

            public final boolean component6() {
                return this.isTogglingSpeakingState;
            }

            public final Loaded copy(Channel channel, List<? extends WidgetStageRaisedHandsBottomSheetAdapter.ListItem> list, boolean z2, boolean z3, boolean z4, boolean z5) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(list, "raisedHandsParticipants");
                return new Loaded(channel, list, z2, z3, z4, z5);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.channel, loaded.channel) && m.areEqual(this.raisedHandsParticipants, loaded.raisedHandsParticipants) && this.isRequestToSpeakEnabled == loaded.isRequestToSpeakEnabled && this.isSpeaker == loaded.isSpeaker && this.isUpdatingRequestToSpeakPermissions == loaded.isUpdatingRequestToSpeakPermissions && this.isTogglingSpeakingState == loaded.isTogglingSpeakingState;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> getRaisedHandsParticipants() {
                return this.raisedHandsParticipants;
            }

            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                List<WidgetStageRaisedHandsBottomSheetAdapter.ListItem> list = this.raisedHandsParticipants;
                if (list != null) {
                    i = list.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.isRequestToSpeakEnabled;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.isSpeaker;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.isUpdatingRequestToSpeakPermissions;
                if (z4) {
                    z4 = true;
                }
                int i10 = z4 ? 1 : 0;
                int i11 = z4 ? 1 : 0;
                int i12 = (i9 + i10) * 31;
                boolean z5 = this.isTogglingSpeakingState;
                if (!z5) {
                    i3 = z5 ? 1 : 0;
                }
                return i12 + i3;
            }

            public final boolean isEmpty() {
                return this.isEmpty;
            }

            public final boolean isRequestToSpeakEnabled() {
                return this.isRequestToSpeakEnabled;
            }

            public final boolean isSpeaker() {
                return this.isSpeaker;
            }

            public final boolean isTogglingSpeakingState() {
                return this.isTogglingSpeakingState;
            }

            public final boolean isUpdatingRequestToSpeakPermissions() {
                return this.isUpdatingRequestToSpeakPermissions;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(channel=");
                R.append(this.channel);
                R.append(", raisedHandsParticipants=");
                R.append(this.raisedHandsParticipants);
                R.append(", isRequestToSpeakEnabled=");
                R.append(this.isRequestToSpeakEnabled);
                R.append(", isSpeaker=");
                R.append(this.isSpeaker);
                R.append(", isUpdatingRequestToSpeakPermissions=");
                R.append(this.isUpdatingRequestToSpeakPermissions);
                R.append(", isTogglingSpeakingState=");
                return a.M(R, this.isTogglingSpeakingState, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(Channel channel, List<? extends WidgetStageRaisedHandsBottomSheetAdapter.ListItem> list, boolean z2, boolean z3, boolean z4, boolean z5) {
                super(null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(list, "raisedHandsParticipants");
                this.channel = channel;
                this.raisedHandsParticipants = list;
                this.isRequestToSpeakEnabled = z2;
                this.isSpeaker = z3;
                this.isUpdatingRequestToSpeakPermissions = z4;
                this.isTogglingSpeakingState = z5;
                this.isEmpty = list.isEmpty();
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetStageRaisedHandsBottomSheetViewModel(long r13, com.discord.stores.StoreVoiceParticipants r15, com.discord.stores.StoreChannels r16, com.discord.stores.StoreUser r17, com.discord.stores.StoreGuilds r18, com.discord.stores.StoreStageChannels r19, com.discord.utilities.time.Clock r20, rx.Observable r21, int r22, kotlin.jvm.internal.DefaultConstructorMarker r23) {
        /*
            r12 = this;
            r0 = r22
            r1 = r0 & 2
            if (r1 == 0) goto Le
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreVoiceParticipants r1 = r1.getVoiceParticipants()
            r5 = r1
            goto Lf
        Le:
            r5 = r15
        Lf:
            r1 = r0 & 4
            if (r1 == 0) goto L1b
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r1 = r1.getChannels()
            r6 = r1
            goto L1d
        L1b:
            r6 = r16
        L1d:
            r1 = r0 & 8
            if (r1 == 0) goto L29
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r1 = r1.getUsers()
            r7 = r1
            goto L2b
        L29:
            r7 = r17
        L2b:
            r1 = r0 & 16
            if (r1 == 0) goto L37
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r1 = r1.getGuilds()
            r8 = r1
            goto L39
        L37:
            r8 = r18
        L39:
            r1 = r0 & 32
            if (r1 == 0) goto L45
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreStageChannels r1 = r1.getStageChannels()
            r9 = r1
            goto L47
        L45:
            r9 = r19
        L47:
            r1 = r0 & 64
            if (r1 == 0) goto L51
            com.discord.utilities.time.Clock r1 = com.discord.utilities.time.ClockFactory.get()
            r10 = r1
            goto L53
        L51:
            r10 = r20
        L53:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L6c
            com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel$Companion r0 = com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel.Companion
            r15 = r0
            r16 = r13
            r18 = r5
            r19 = r6
            r20 = r8
            r21 = r9
            r22 = r10
            rx.Observable r0 = com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel.Companion.access$observeStoreState(r15, r16, r18, r19, r20, r21, r22)
            r11 = r0
            goto L6e
        L6c:
            r11 = r21
        L6e:
            r2 = r12
            r3 = r13
            r2.<init>(r3, r5, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel.<init>(long, com.discord.stores.StoreVoiceParticipants, com.discord.stores.StoreChannels, com.discord.stores.StoreUser, com.discord.stores.StoreGuilds, com.discord.stores.StoreStageChannels, com.discord.utilities.time.Clock, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void emitDismiss() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.Dismiss.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitError(Error error) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.Error(error));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        updateViewState(new ViewState.Loaded(storeState.getChannel(), storeState.getRaisedHandsParticipants(), PermissionUtils.INSTANCE.canEveryoneRole(Permission.REQUEST_TO_SPEAK, storeState.getChannel(), storeState.getRoles()), StageRoles.m27isSpeakerimpl(storeState.m44getMyStageRoles1LxfuJo()), false, false, 48, null));
    }

    private final Observable<Void> setUserSuppressed(final long j, final boolean z2) {
        Observable<R> F = this.channelsStore.observeChannel(this.channelId).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable z3 = F.y().z(new b<Channel, Observable<? extends Void>>() { // from class: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel$setUserSuppressed$1
            public final Observable<? extends Void> call(Channel channel) {
                StageChannelAPI stageChannelAPI = StageChannelAPI.INSTANCE;
                m.checkNotNullExpressionValue(channel, "channel");
                return ObservableExtensionsKt.restSubscribeOn$default(StageChannelAPI.setUserSuppressedInChannel$default(stageChannelAPI, channel, j, z2, 0L, 8, null), false, 1, null);
            }
        });
        m.checkNotNullExpressionValue(z3, "channelSingle\n        .f…stSubscribeOn()\n        }");
        return ObservableExtensionsKt.ui$default(z3, this, null, 2, null);
    }

    public final void dismissRequestToSpeak(StoreVoiceParticipants.VoiceUser voiceUser) {
        m.checkNotNullParameter(voiceUser, "participant");
        ObservableExtensionsKt.appSubscribe(setUserSuppressed(voiceUser.getUser().getId(), true), WidgetStageRaisedHandsBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetStageRaisedHandsBottomSheetViewModel$dismissRequestToSpeak$1.INSTANCE);
    }

    public final void inviteToSpeak(StoreVoiceParticipants.VoiceUser voiceUser) {
        m.checkNotNullParameter(voiceUser, "participant");
        ObservableExtensionsKt.appSubscribe(setUserSuppressed(voiceUser.getUser().getId(), false), WidgetStageRaisedHandsBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetStageRaisedHandsBottomSheetViewModel$inviteToSpeak$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetStageRaisedHandsBottomSheetViewModel$inviteToSpeak$2.INSTANCE);
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void setRequestToSpeakEnabled(boolean z2) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, false, false, true, false, 47, null));
            Pair<Long, Long> computeAllowDenyUpdateBits = PermissionOverwriteUtilsKt.computeAllowDenyUpdateBits(ChannelUtils.f(loaded.getChannel(), loaded.getChannel().f()), Permission.REQUEST_TO_SPEAK, z2);
            long longValue = computeAllowDenyUpdateBits.component1().longValue();
            long longValue2 = computeAllowDenyUpdateBits.component2().longValue();
            Subscription subscription = this.lastRequestToSpeakSubscription;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updatePermissionOverwrites(loaded.getChannel().h(), loaded.getChannel().f(), RestAPIParams.ChannelPermissionOverwrites.Companion.createForRole(loaded.getChannel().f(), Long.valueOf(longValue), Long.valueOf(longValue2))), false, 1, null), this, null, 2, null), WidgetStageRaisedHandsBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetStageRaisedHandsBottomSheetViewModel$setRequestToSpeakEnabled$1(this), (r18 & 8) != 0 ? null : new WidgetStageRaisedHandsBottomSheetViewModel$setRequestToSpeakEnabled$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetStageRaisedHandsBottomSheetViewModel$setRequestToSpeakEnabled$3(this, z2));
        }
    }

    public final void setSpeakingState(boolean z2) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, false, false, false, true, 31, null));
            ObservableExtensionsKt.appSubscribe(setUserSuppressed(this.usersStore.getMe().getId(), z2), WidgetStageRaisedHandsBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetStageRaisedHandsBottomSheetViewModel$setSpeakingState$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : new WidgetStageRaisedHandsBottomSheetViewModel$setSpeakingState$2(this), WidgetStageRaisedHandsBottomSheetViewModel$setSpeakingState$3.INSTANCE);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStageRaisedHandsBottomSheetViewModel(long j, StoreVoiceParticipants storeVoiceParticipants, StoreChannels storeChannels, StoreUser storeUser, StoreGuilds storeGuilds, StoreStageChannels storeStageChannels, Clock clock, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeVoiceParticipants, "voiceParticipants");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeUser, "usersStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeStageChannels, "stageChannelsStore");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.channelId = j;
        this.voiceParticipants = storeVoiceParticipants;
        this.channelsStore = storeChannels;
        this.usersStore = storeUser;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetStageRaisedHandsBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        this.eventSubject = PublishSubject.k0();
    }
}
