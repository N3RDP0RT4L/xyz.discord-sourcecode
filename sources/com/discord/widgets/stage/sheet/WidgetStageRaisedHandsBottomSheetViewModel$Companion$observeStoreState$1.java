package com.discord.widgets.stage.sheet;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.time.Clock;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetAdapter;
import com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel;
import d0.f0.q;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.Comparator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Func4;
/* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channel", "Lrx/Observable;", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1<T, R> implements b<Channel, Observable<? extends WidgetStageRaisedHandsBottomSheetViewModel.StoreState>> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Clock $clock;
    public final /* synthetic */ StoreGuilds $guildsStore;
    public final /* synthetic */ StoreStageChannels $stageChannels;
    public final /* synthetic */ StoreVoiceParticipants $voiceParticipants;

    public WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1(StoreGuilds storeGuilds, StoreVoiceParticipants storeVoiceParticipants, long j, StoreStageChannels storeStageChannels, Clock clock) {
        this.$guildsStore = storeGuilds;
        this.$voiceParticipants = storeVoiceParticipants;
        this.$channelId = j;
        this.$stageChannels = storeStageChannels;
        this.$clock = clock;
    }

    public final Observable<? extends WidgetStageRaisedHandsBottomSheetViewModel.StoreState> call(final Channel channel) {
        return Observable.h(this.$guildsStore.observeRoles(channel.f()), this.$guildsStore.observeComputed(channel.f()), this.$voiceParticipants.get(this.$channelId), this.$stageChannels.observeMyRoles(this.$channelId), new Func4<Map<Long, ? extends GuildRole>, Map<Long, ? extends GuildMember>, Map<Long, ? extends StoreVoiceParticipants.VoiceUser>, StageRoles, WidgetStageRaisedHandsBottomSheetViewModel.StoreState>() { // from class: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1.1

            /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "voiceUser", "", "invoke", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1$1$1  reason: invalid class name and collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C02611 extends o implements Function1<StoreVoiceParticipants.VoiceUser, Boolean> {
                public static final C02611 INSTANCE = new C02611();

                public C02611() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Boolean invoke(StoreVoiceParticipants.VoiceUser voiceUser) {
                    return Boolean.valueOf(invoke2(voiceUser));
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final boolean invoke2(StoreVoiceParticipants.VoiceUser voiceUser) {
                    m.checkNotNullParameter(voiceUser, "voiceUser");
                    return voiceUser.isRequestingToSpeak();
                }
            }

            /* compiled from: WidgetStageRaisedHandsBottomSheetViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "voiceUser", "Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem$Participant;", "invoke", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)Lcom/discord/widgets/stage/sheet/WidgetStageRaisedHandsBottomSheetAdapter$ListItem$Participant;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends o implements Function1<StoreVoiceParticipants.VoiceUser, WidgetStageRaisedHandsBottomSheetAdapter.ListItem.Participant> {
                public final /* synthetic */ Map $members;
                public final /* synthetic */ Map $roles;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass2(Map map, Map map2) {
                    super(1);
                    this.$members = map;
                    this.$roles = map2;
                }

                public final WidgetStageRaisedHandsBottomSheetAdapter.ListItem.Participant invoke(StoreVoiceParticipants.VoiceUser voiceUser) {
                    UtcDateTime utcDateTime;
                    m.checkNotNullParameter(voiceUser, "voiceUser");
                    VoiceState voiceState = voiceUser.getVoiceState();
                    if (voiceState == null || (utcDateTime = voiceState.f()) == null) {
                        utcDateTime = new UtcDateTime(WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1.this.$clock.currentTimeMillis());
                    }
                    Map map = this.$roles;
                    m.checkNotNullExpressionValue(map, "roles");
                    return new WidgetStageRaisedHandsBottomSheetAdapter.ListItem.Participant(voiceUser, utcDateTime, (GuildMember) this.$members.get(Long.valueOf(voiceUser.getUser().getId())), RoleUtils.getHighestHoistedRole(map, (GuildMember) this.$members.get(Long.valueOf(voiceUser.getUser().getId()))));
                }
            }

            @Override // rx.functions.Func4
            public /* bridge */ /* synthetic */ WidgetStageRaisedHandsBottomSheetViewModel.StoreState call(Map<Long, ? extends GuildRole> map, Map<Long, ? extends GuildMember> map2, Map<Long, ? extends StoreVoiceParticipants.VoiceUser> map3, StageRoles stageRoles) {
                return call2((Map<Long, GuildRole>) map, (Map<Long, GuildMember>) map2, (Map<Long, StoreVoiceParticipants.VoiceUser>) map3, stageRoles);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final WidgetStageRaisedHandsBottomSheetViewModel.StoreState call2(Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Map<Long, StoreVoiceParticipants.VoiceUser> map3, StageRoles stageRoles) {
                Channel channel2 = channel;
                m.checkNotNullExpressionValue(channel2, "channel");
                m.checkNotNullExpressionValue(map, "roles");
                return new WidgetStageRaisedHandsBottomSheetViewModel.StoreState(channel2, map, q.toList(q.sortedWith(q.map(q.filter(u.asSequence(map3.values()), C02611.INSTANCE), new AnonymousClass2(map2, map)), new Comparator() { // from class: com.discord.widgets.stage.sheet.WidgetStageRaisedHandsBottomSheetViewModel$Companion$observeStoreState$1$1$$special$$inlined$sortedBy$1
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return a.compareValues(((WidgetStageRaisedHandsBottomSheetAdapter.ListItem.Participant) t).getRequestToSpeakTimestamp(), ((WidgetStageRaisedHandsBottomSheetAdapter.ListItem.Participant) t2).getRequestToSpeakTimestamp());
                    }
                })), stageRoles != null ? stageRoles.m29unboximpl() : StageRoles.Companion.m31getAUDIENCE1LxfuJo(), null);
            }
        });
    }
}
