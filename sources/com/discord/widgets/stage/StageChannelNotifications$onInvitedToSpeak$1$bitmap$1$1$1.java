package com.discord.widgets.stage;

import android.graphics.Bitmap;
import androidx.core.app.NotificationCompat;
import j0.k.b;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StageChannelNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "Lrx/Observable;", "Landroid/graphics/Bitmap;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageChannelNotifications$onInvitedToSpeak$1$bitmap$1$1$1<T, R> implements b<Throwable, Observable<? extends Bitmap>> {
    public static final StageChannelNotifications$onInvitedToSpeak$1$bitmap$1$1$1 INSTANCE = new StageChannelNotifications$onInvitedToSpeak$1$bitmap$1$1$1();

    public final Observable<? extends Bitmap> call(Throwable th) {
        return null;
    }
}
