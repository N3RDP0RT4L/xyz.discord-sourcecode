package com.discord.widgets.stage.start;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.api.permission.Permission;
import com.discord.app.AppViewModel;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.widgets.stage.start.ModeratorStartStageItem;
import com.discord.widgets.stage.start.ModeratorStartStageViewModel;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
/* compiled from: ModeratorStartStageViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 !2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003!\"#BU\u0012\n\u0010\u0015\u001a\u00060\u0013j\u0002`\u0014\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0017\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u001a\u0012\b\b\u0002\u0010\u000e\u001a\u00020\r\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0010\u0012\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00030\u001d¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\r\u0010\b\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0015\u001a\u00060\u0013j\u0002`\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001c¨\u0006$"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$ViewState;", "Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;)V", "disconnectFromStage", "()V", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreVoiceParticipants;", "voiceParticipants", "Lcom/discord/stores/StoreVoiceParticipants;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "selectedVoiceChannelStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceParticipants;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ModeratorStartStageViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_DISPLAY_WAITING_USERS = 3;
    private final long channelId;
    private final StoreChannels channelsStore;
    private final StoreGuildScheduledEvents guildScheduledEventsStore;
    private final StorePermissions permissionsStore;
    private final StoreVoiceChannelSelected selectedVoiceChannelStore;
    private final StoreVoiceParticipants voiceParticipants;

    /* compiled from: ModeratorStartStageViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.stage.start.ModeratorStartStageViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(ModeratorStartStageViewModel moderatorStartStageViewModel) {
            super(1, moderatorStartStageViewModel, ModeratorStartStageViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((ModeratorStartStageViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: ModeratorStartStageViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015JA\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreVoiceParticipants;", "voiceParticipantsStore", "Lrx/Observable;", "Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;", "observeStores", "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceParticipants;)Lrx/Observable;", "", "MAX_DISPLAY_WAITING_USERS", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(final long j, StoreChannels storeChannels, final StoreGuildScheduledEvents storeGuildScheduledEvents, final StorePermissions storePermissions, final StoreVoiceParticipants storeVoiceParticipants) {
            Observable<R> F = storeChannels.observeChannel(j).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
            m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
            Observable<StoreState> Y = F.Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.stage.start.ModeratorStartStageViewModel$Companion$observeStores$1
                public final Observable<? extends ModeratorStartStageViewModel.StoreState> call(Channel channel) {
                    return Observable.i(ObservableExtensionsKt.leadingEdgeThrottle(StoreVoiceParticipants.this.get(channel.h()), 250L, TimeUnit.MILLISECONDS), StoreGuildScheduledEvents.observeGuildScheduledEvents$default(storeGuildScheduledEvents, channel.f(), false, 2, null), storePermissions.observePermissionsForChannel(channel.h()), new Func3<Map<Long, ? extends StoreVoiceParticipants.VoiceUser>, List<? extends GuildScheduledEvent>, Long, ModeratorStartStageViewModel.StoreState>() { // from class: com.discord.widgets.stage.start.ModeratorStartStageViewModel$Companion$observeStores$1.1
                        @Override // rx.functions.Func3
                        public /* bridge */ /* synthetic */ ModeratorStartStageViewModel.StoreState call(Map<Long, ? extends StoreVoiceParticipants.VoiceUser> map, List<? extends GuildScheduledEvent> list, Long l) {
                            return call2((Map<Long, StoreVoiceParticipants.VoiceUser>) map, (List<GuildScheduledEvent>) list, l);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final ModeratorStartStageViewModel.StoreState call2(Map<Long, StoreVoiceParticipants.VoiceUser> map, List<GuildScheduledEvent> list, Long l) {
                            ArrayList Y2 = a.Y(list, "guildScheduledEvents");
                            Iterator<T> it = list.iterator();
                            while (true) {
                                boolean z2 = true;
                                if (!it.hasNext()) {
                                    break;
                                }
                                T next = it.next();
                                Long b2 = ((GuildScheduledEvent) next).b();
                                if (b2 == null || b2.longValue() != j) {
                                    z2 = false;
                                }
                                if (z2) {
                                    Y2.add(next);
                                }
                            }
                            List sortedWith = u.sortedWith(Y2, new Comparator() { // from class: com.discord.widgets.stage.start.ModeratorStartStageViewModel$Companion$observeStores$1$1$$special$$inlined$sortedBy$1
                                @Override // java.util.Comparator
                                public final int compare(T t, T t2) {
                                    return d0.u.a.compareValues(((GuildScheduledEvent) t).l(), ((GuildScheduledEvent) t2).l());
                                }
                            });
                            Collection<StoreVoiceParticipants.VoiceUser> values = map.values();
                            ArrayList<StoreVoiceParticipants.VoiceUser> arrayList = new ArrayList();
                            for (T t : values) {
                                if (!((StoreVoiceParticipants.VoiceUser) t).isMe()) {
                                    arrayList.add(t);
                                }
                            }
                            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
                            for (StoreVoiceParticipants.VoiceUser voiceUser : arrayList) {
                                arrayList2.add(new UserGuildMember(voiceUser.getUser(), voiceUser.getGuildMember()));
                            }
                            return new ModeratorStartStageViewModel.StoreState(sortedWith, arrayList2, PermissionUtils.can(Permission.START_STAGE_EVENT, l));
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "channelStore.observeChan…  )\n          }\n        }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ModeratorStartStageViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ:\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00022\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0017\u001a\u00020\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\nR\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001d\u0010\u0005¨\u0006 "}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;", "", "", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Ljava/util/List;", "Lcom/discord/models/guild/UserGuildMember;", "component2", "", "component3", "()Z", "channelEvents", "voiceParticipants", "canStartEvent", "copy", "(Ljava/util/List;Ljava/util/List;Z)Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanStartEvent", "Ljava/util/List;", "getChannelEvents", "getVoiceParticipants", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final boolean canStartEvent;
        private final List<GuildScheduledEvent> channelEvents;
        private final List<UserGuildMember> voiceParticipants;

        public StoreState(List<GuildScheduledEvent> list, List<UserGuildMember> list2, boolean z2) {
            m.checkNotNullParameter(list, "channelEvents");
            m.checkNotNullParameter(list2, "voiceParticipants");
            this.channelEvents = list;
            this.voiceParticipants = list2;
            this.canStartEvent = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, List list, List list2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = storeState.channelEvents;
            }
            if ((i & 2) != 0) {
                list2 = storeState.voiceParticipants;
            }
            if ((i & 4) != 0) {
                z2 = storeState.canStartEvent;
            }
            return storeState.copy(list, list2, z2);
        }

        public final List<GuildScheduledEvent> component1() {
            return this.channelEvents;
        }

        public final List<UserGuildMember> component2() {
            return this.voiceParticipants;
        }

        public final boolean component3() {
            return this.canStartEvent;
        }

        public final StoreState copy(List<GuildScheduledEvent> list, List<UserGuildMember> list2, boolean z2) {
            m.checkNotNullParameter(list, "channelEvents");
            m.checkNotNullParameter(list2, "voiceParticipants");
            return new StoreState(list, list2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.channelEvents, storeState.channelEvents) && m.areEqual(this.voiceParticipants, storeState.voiceParticipants) && this.canStartEvent == storeState.canStartEvent;
        }

        public final boolean getCanStartEvent() {
            return this.canStartEvent;
        }

        public final List<GuildScheduledEvent> getChannelEvents() {
            return this.channelEvents;
        }

        public final List<UserGuildMember> getVoiceParticipants() {
            return this.voiceParticipants;
        }

        public int hashCode() {
            List<GuildScheduledEvent> list = this.channelEvents;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            List<UserGuildMember> list2 = this.voiceParticipants;
            if (list2 != null) {
                i = list2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.canStartEvent;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(channelEvents=");
            R.append(this.channelEvents);
            R.append(", voiceParticipants=");
            R.append(this.voiceParticipants);
            R.append(", canStartEvent=");
            return a.M(R, this.canStartEvent, ")");
        }
    }

    /* compiled from: ModeratorStartStageViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ:\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00022\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0017\u001a\u00020\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\nR\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001d\u0010\u0005¨\u0006 "}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$ViewState;", "", "", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Ljava/util/List;", "Lcom/discord/widgets/stage/start/ModeratorStartStageItem;", "component2", "", "component3", "()Z", "channelEvents", "items", "channelEventActive", "copy", "(Ljava/util/List;Ljava/util/List;Z)Lcom/discord/widgets/stage/start/ModeratorStartStageViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getChannelEventActive", "Ljava/util/List;", "getItems", "getChannelEvents", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean channelEventActive;
        private final List<GuildScheduledEvent> channelEvents;
        private final List<ModeratorStartStageItem> items;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(List<GuildScheduledEvent> list, List<? extends ModeratorStartStageItem> list2, boolean z2) {
            m.checkNotNullParameter(list, "channelEvents");
            m.checkNotNullParameter(list2, "items");
            this.channelEvents = list;
            this.items = list2;
            this.channelEventActive = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, List list, List list2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = viewState.channelEvents;
            }
            if ((i & 2) != 0) {
                list2 = viewState.items;
            }
            if ((i & 4) != 0) {
                z2 = viewState.channelEventActive;
            }
            return viewState.copy(list, list2, z2);
        }

        public final List<GuildScheduledEvent> component1() {
            return this.channelEvents;
        }

        public final List<ModeratorStartStageItem> component2() {
            return this.items;
        }

        public final boolean component3() {
            return this.channelEventActive;
        }

        public final ViewState copy(List<GuildScheduledEvent> list, List<? extends ModeratorStartStageItem> list2, boolean z2) {
            m.checkNotNullParameter(list, "channelEvents");
            m.checkNotNullParameter(list2, "items");
            return new ViewState(list, list2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.channelEvents, viewState.channelEvents) && m.areEqual(this.items, viewState.items) && this.channelEventActive == viewState.channelEventActive;
        }

        public final boolean getChannelEventActive() {
            return this.channelEventActive;
        }

        public final List<GuildScheduledEvent> getChannelEvents() {
            return this.channelEvents;
        }

        public final List<ModeratorStartStageItem> getItems() {
            return this.items;
        }

        public int hashCode() {
            List<GuildScheduledEvent> list = this.channelEvents;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            List<ModeratorStartStageItem> list2 = this.items;
            if (list2 != null) {
                i = list2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.channelEventActive;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(channelEvents=");
            R.append(this.channelEvents);
            R.append(", items=");
            R.append(this.items);
            R.append(", channelEventActive=");
            return a.M(R, this.channelEventActive, ")");
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ ModeratorStartStageViewModel(long r11, com.discord.stores.StoreChannels r13, com.discord.stores.StoreGuildScheduledEvents r14, com.discord.stores.StoreVoiceChannelSelected r15, com.discord.stores.StorePermissions r16, com.discord.stores.StoreVoiceParticipants r17, rx.Observable r18, int r19, kotlin.jvm.internal.DefaultConstructorMarker r20) {
        /*
            r10 = this;
            r0 = r19 & 2
            if (r0 == 0) goto Lc
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r0 = r0.getChannels()
            r4 = r0
            goto Ld
        Lc:
            r4 = r13
        Ld:
            r0 = r19 & 4
            if (r0 == 0) goto L19
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildScheduledEvents r0 = r0.getGuildScheduledEvents()
            r5 = r0
            goto L1a
        L19:
            r5 = r14
        L1a:
            r0 = r19 & 8
            if (r0 == 0) goto L26
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreVoiceChannelSelected r0 = r0.getVoiceChannelSelected()
            r6 = r0
            goto L27
        L26:
            r6 = r15
        L27:
            r0 = r19 & 16
            if (r0 == 0) goto L33
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePermissions r0 = r0.getPermissions()
            r7 = r0
            goto L35
        L33:
            r7 = r16
        L35:
            r0 = r19 & 32
            if (r0 == 0) goto L41
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreVoiceParticipants r0 = r0.getVoiceParticipants()
            r8 = r0
            goto L43
        L41:
            r8 = r17
        L43:
            r0 = r19 & 64
            if (r0 == 0) goto L59
            com.discord.widgets.stage.start.ModeratorStartStageViewModel$Companion r0 = com.discord.widgets.stage.start.ModeratorStartStageViewModel.Companion
            r13 = r0
            r14 = r11
            r16 = r4
            r17 = r5
            r18 = r7
            r19 = r8
            rx.Observable r0 = com.discord.widgets.stage.start.ModeratorStartStageViewModel.Companion.access$observeStores(r13, r14, r16, r17, r18, r19)
            r9 = r0
            goto L5b
        L59:
            r9 = r18
        L5b:
            r1 = r10
            r2 = r11
            r1.<init>(r2, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageViewModel.<init>(long, com.discord.stores.StoreChannels, com.discord.stores.StoreGuildScheduledEvents, com.discord.stores.StoreVoiceChannelSelected, com.discord.stores.StorePermissions, com.discord.stores.StoreVoiceParticipants, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        boolean z2;
        Object obj;
        boolean z3 = true;
        Object obj2 = null;
        List mutableListOf = n.mutableListOf(new ModeratorStartStageItem.Header(0, null, 3, null));
        if (storeState.getCanStartEvent()) {
            Iterator<T> it = storeState.getChannelEvents().iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (GuildScheduledEventUtilitiesKt.getEventTiming((GuildScheduledEvent) obj).isStartable()) {
                    break;
                }
            }
            GuildScheduledEvent guildScheduledEvent = (GuildScheduledEvent) obj;
            if (guildScheduledEvent != null) {
                mutableListOf.add(new ModeratorStartStageItem.ListItem.Event(guildScheduledEvent));
            }
        }
        mutableListOf.add(new ModeratorStartStageItem.ListItem.StaticOption.StartStage(null, 1, null));
        if (storeState.getCanStartEvent()) {
            mutableListOf.add(new ModeratorStartStageItem.ListItem.StaticOption.ScheduleEvent(null, 1, null));
        }
        mutableListOf.add(new ModeratorStartStageItem.Continue(0, null, 3, null));
        if (!storeState.getVoiceParticipants().isEmpty()) {
            mutableListOf.add(new ModeratorStartStageItem.Waiting(0, null, u.take(storeState.getVoiceParticipants(), 3), 3, null));
        }
        List<GuildScheduledEvent> channelEvents = storeState.getChannelEvents();
        Iterator<T> it2 = storeState.getChannelEvents().iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            Object next = it2.next();
            if (((GuildScheduledEvent) next).m() == GuildScheduledEventStatus.ACTIVE) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                obj2 = next;
                break;
            }
        }
        if (obj2 == null) {
            z3 = false;
        }
        updateViewState(new ViewState(channelEvents, mutableListOf, z3));
    }

    public final void disconnectFromStage() {
        this.selectedVoiceChannelStore.clear();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ModeratorStartStageViewModel(long j, StoreChannels storeChannels, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreVoiceChannelSelected storeVoiceChannelSelected, StorePermissions storePermissions, StoreVoiceParticipants storeVoiceParticipants, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventsStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "selectedVoiceChannelStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeVoiceParticipants, "voiceParticipants");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.channelId = j;
        this.channelsStore = storeChannels;
        this.guildScheduledEventsStore = storeGuildScheduledEvents;
        this.selectedVoiceChannelStore = storeVoiceChannelSelected;
        this.permissionsStore = storePermissions;
        this.voiceParticipants = storeVoiceParticipants;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), ModeratorStartStageViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
