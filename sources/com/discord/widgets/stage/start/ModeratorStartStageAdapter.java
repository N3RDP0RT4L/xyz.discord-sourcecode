package com.discord.widgets.stage.start;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.databinding.ModeratorStartStageContinueItemBinding;
import com.discord.databinding.ModeratorStartStageHeaderItemBinding;
import com.discord.databinding.ModeratorStartStageListItemBinding;
import com.discord.databinding.ModeratorStartStageWaitingItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.stage.start.ModeratorStartStageAdapter;
import com.discord.widgets.stage.start.ModeratorStartStageItem;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: ModeratorStartStageAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005\u0014\u0015\u0016\u0017\u0018B#\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0004\b\u0012\u0010\u0013J)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR%\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b0\n8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/stage/start/ModeratorStartStageItem;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function1;", "", "onItemClicked", "Lkotlin/jvm/functions/Function1;", "getOnItemClicked", "()Lkotlin/jvm/functions/Function1;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;)V", "ContinueViewHolder", "CreateStageEventViewHolder", "HeaderViewHolder", "ListItemViewHolder", "WaitingViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ModeratorStartStageAdapter extends MGRecyclerAdapterSimple<ModeratorStartStageItem> {
    private final Function1<ModeratorStartStageItem, Unit> onItemClicked;

    /* compiled from: ModeratorStartStageAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b¦\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u000b\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$CreateStageEventViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;", "Lcom/discord/widgets/stage/start/ModeratorStartStageItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/stage/start/ModeratorStartStageItem;)V", "Landroid/view/View;", "rootView", "Landroid/view/View;", "getRootView", "()Landroid/view/View;", HookHelper.constructorName, "(Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;Landroid/view/View;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public abstract class CreateStageEventViewHolder extends MGRecyclerViewHolder<ModeratorStartStageAdapter, ModeratorStartStageItem> {
        private final View rootView;
        public final /* synthetic */ ModeratorStartStageAdapter this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public CreateStageEventViewHolder(ModeratorStartStageAdapter moderatorStartStageAdapter, View view) {
            super(view, moderatorStartStageAdapter);
            m.checkNotNullParameter(view, "rootView");
            this.this$0 = moderatorStartStageAdapter;
            this.rootView = view;
        }

        public final View getRootView() {
            return this.rootView;
        }

        public void onConfigure(int i, final ModeratorStartStageItem moderatorStartStageItem) {
            m.checkNotNullParameter(moderatorStartStageItem, "data");
            super.onConfigure(i, (int) moderatorStartStageItem);
            this.rootView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stage.start.ModeratorStartStageAdapter$CreateStageEventViewHolder$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ModeratorStartStageAdapter.CreateStageEventViewHolder.this.this$0.getOnItemClicked().invoke(moderatorStartStageItem);
                }
            });
        }
    }

    /* compiled from: ModeratorStartStageAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0004\u0018\u00002\u00060\u0001R\u00020\u0002B\u0011\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u000b\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$ListItemViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$CreateStageEventViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/stage/start/ModeratorStartStageItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/stage/start/ModeratorStartStageItem;)V", "Lcom/discord/databinding/ModeratorStartStageListItemBinding;", "binding", "Lcom/discord/databinding/ModeratorStartStageListItemBinding;", "getBinding", "()Lcom/discord/databinding/ModeratorStartStageListItemBinding;", HookHelper.constructorName, "(Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;Lcom/discord/databinding/ModeratorStartStageListItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class ListItemViewHolder extends CreateStageEventViewHolder {
        private final ModeratorStartStageListItemBinding binding;
        public final /* synthetic */ ModeratorStartStageAdapter this$0;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public ListItemViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r3, com.discord.databinding.ModeratorStartStageListItemBinding r4) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                r2.this$0 = r3
                com.discord.widgets.guildscheduledevent.GuildEventPromptView r0 = r4.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r3, r0)
                r2.binding = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.ListItemViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageListItemBinding):void");
        }

        public final ModeratorStartStageListItemBinding getBinding() {
            return this.binding;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.widgets.stage.start.ModeratorStartStageAdapter.CreateStageEventViewHolder
        public void onConfigure(int i, ModeratorStartStageItem moderatorStartStageItem) {
            Pair pair;
            CharSequence b2;
            CharSequence b3;
            m.checkNotNullParameter(moderatorStartStageItem, "data");
            super.onConfigure(i, moderatorStartStageItem);
            ModeratorStartStageItem.ListItem listItem = (ModeratorStartStageItem.ListItem) moderatorStartStageItem;
            if (listItem instanceof ModeratorStartStageItem.ListItem.StaticOption) {
                ModeratorStartStageItem.ListItem.StaticOption staticOption = (ModeratorStartStageItem.ListItem.StaticOption) listItem;
                b2 = b.b(this.this$0.getContext(), staticOption.getTitleRes(), new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                b3 = b.b(this.this$0.getContext(), staticOption.getSubtitleRes(), new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                pair = new Pair(b2, b3);
            } else if (listItem instanceof ModeratorStartStageItem.ListItem.Event) {
                ModeratorStartStageItem.ListItem.Event event = (ModeratorStartStageItem.ListItem.Event) listItem;
                pair = new Pair(event.getEvent().j(), GuildScheduledEventUtilitiesKt.getEventStartingTimeString(event.getEvent(), this.this$0.getContext()));
            } else {
                throw new NoWhenBranchMatchedException();
            }
            this.binding.a.configure(listItem.getIconRes(), listItem.getIconBgColorRes(), (CharSequence) pair.component1(), (CharSequence) pair.component2());
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public ListItemViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r2, com.discord.databinding.ModeratorStartStageListItemBinding r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
            /*
                r1 = this;
                r4 = r4 & 1
                if (r4 == 0) goto L2a
                android.content.Context r3 = r2.getContext()
                android.view.LayoutInflater r3 = android.view.LayoutInflater.from(r3)
                androidx.recyclerview.widget.RecyclerView r4 = r2.getRecycler()
                r5 = 2131558587(0x7f0d00bb, float:1.8742494E38)
                r0 = 0
                android.view.View r3 = r3.inflate(r5, r4, r0)
                java.lang.String r4 = "rootView"
                java.util.Objects.requireNonNull(r3, r4)
                com.discord.databinding.ModeratorStartStageListItemBinding r4 = new com.discord.databinding.ModeratorStartStageListItemBinding
                com.discord.widgets.guildscheduledevent.GuildEventPromptView r3 = (com.discord.widgets.guildscheduledevent.GuildEventPromptView) r3
                r4.<init>(r3)
                java.lang.String r3 = "ModeratorStartStageListI…ontext), recycler, false)"
                d0.z.d.m.checkNotNullExpressionValue(r4, r3)
                r3 = r4
            L2a:
                r1.<init>(r2, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.ListItemViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageListItemBinding, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    /* compiled from: ModeratorStartStageAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0004\u0018\u00002\u00060\u0001R\u00020\u0002B\u0011\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u000b\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$WaitingViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$CreateStageEventViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/stage/start/ModeratorStartStageItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/stage/start/ModeratorStartStageItem;)V", "Lcom/discord/databinding/ModeratorStartStageWaitingItemBinding;", "binding", "Lcom/discord/databinding/ModeratorStartStageWaitingItemBinding;", "getBinding", "()Lcom/discord/databinding/ModeratorStartStageWaitingItemBinding;", HookHelper.constructorName, "(Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;Lcom/discord/databinding/ModeratorStartStageWaitingItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class WaitingViewHolder extends CreateStageEventViewHolder {
        private final ModeratorStartStageWaitingItemBinding binding;
        public final /* synthetic */ ModeratorStartStageAdapter this$0;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public WaitingViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r3, com.discord.databinding.ModeratorStartStageWaitingItemBinding r4) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                r2.this$0 = r3
                androidx.constraintlayout.widget.ConstraintLayout r0 = r4.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r3, r0)
                r2.binding = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.WaitingViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageWaitingItemBinding):void");
        }

        public final ModeratorStartStageWaitingItemBinding getBinding() {
            return this.binding;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.widgets.stage.start.ModeratorStartStageAdapter.CreateStageEventViewHolder
        public void onConfigure(int i, ModeratorStartStageItem moderatorStartStageItem) {
            m.checkNotNullParameter(moderatorStartStageItem, "data");
            super.onConfigure(i, moderatorStartStageItem);
            ModeratorStartStageItem.Waiting waiting = (ModeratorStartStageItem.Waiting) moderatorStartStageItem;
            this.binding.f2113b.setMembers(waiting.getUsers());
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.moderatorStartStageWaitingUserText");
            int size = waiting.getUsers().size();
            CharSequence charSequence = null;
            if (size == 1) {
                charSequence = b.b(this.this$0.getContext(), R.string.stage_audience_waiting_one, new Object[]{waiting.getUsers().get(0).getNickOrUserName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            } else if (size == 2) {
                charSequence = b.b(this.this$0.getContext(), R.string.stage_audience_waiting_two, new Object[]{waiting.getUsers().get(0).getNickOrUserName(), waiting.getUsers().get(1).getNickOrUserName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            } else if (size == 3) {
                charSequence = StringResourceUtilsKt.getI18nPluralString(this.this$0.getContext(), R.plurals.stage_audience_waiting_many_numOthers, waiting.getUsers().size() - 2, waiting.getUsers().get(0).getNickOrUserName(), waiting.getUsers().get(1).getNickOrUserName());
            }
            ViewExtensions.setTextAndVisibilityBy(textView, charSequence);
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public WaitingViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r2, com.discord.databinding.ModeratorStartStageWaitingItemBinding r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
            /*
                r1 = this;
                r4 = r4 & 1
                if (r4 == 0) goto L50
                android.content.Context r3 = r2.getContext()
                android.view.LayoutInflater r3 = android.view.LayoutInflater.from(r3)
                androidx.recyclerview.widget.RecyclerView r4 = r2.getRecycler()
                r5 = 2131558588(0x7f0d00bc, float:1.8742496E38)
                r0 = 0
                android.view.View r3 = r3.inflate(r5, r4, r0)
                r4 = 2131364391(0x7f0a0a27, float:1.8348618E38)
                android.view.View r5 = r3.findViewById(r4)
                com.discord.views.user.UserSummaryView r5 = (com.discord.views.user.UserSummaryView) r5
                if (r5 == 0) goto L3c
                r4 = 2131364392(0x7f0a0a28, float:1.834862E38)
                android.view.View r0 = r3.findViewById(r4)
                android.widget.TextView r0 = (android.widget.TextView) r0
                if (r0 == 0) goto L3c
                com.discord.databinding.ModeratorStartStageWaitingItemBinding r4 = new com.discord.databinding.ModeratorStartStageWaitingItemBinding
                androidx.constraintlayout.widget.ConstraintLayout r3 = (androidx.constraintlayout.widget.ConstraintLayout) r3
                r4.<init>(r3, r5, r0)
                java.lang.String r3 = "ModeratorStartStageWaiti…ontext), recycler, false)"
                d0.z.d.m.checkNotNullExpressionValue(r4, r3)
                r3 = r4
                goto L50
            L3c:
                android.content.res.Resources r2 = r3.getResources()
                java.lang.String r2 = r2.getResourceName(r4)
                java.lang.NullPointerException r3 = new java.lang.NullPointerException
                java.lang.String r4 = "Missing required view with ID: "
                java.lang.String r2 = r4.concat(r2)
                r3.<init>(r2)
                throw r3
            L50:
                r1.<init>(r2, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.WaitingViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageWaitingItemBinding, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public ModeratorStartStageAdapter(RecyclerView recyclerView, Function1<? super ModeratorStartStageItem, Unit> function1) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recyclerView");
        m.checkNotNullParameter(function1, "onItemClicked");
        this.onItemClicked = function1;
    }

    public final Function1<ModeratorStartStageItem, Unit> getOnItemClicked() {
        return this.onItemClicked;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, ModeratorStartStageItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new HeaderViewHolder(this, null, 1, null);
        }
        if (i == 1) {
            return new ListItemViewHolder(this, null, 1, null);
        }
        if (i == 2) {
            return new ContinueViewHolder(this, null, 1, null);
        }
        if (i == 3) {
            return new WaitingViewHolder(this, null, 1, null);
        }
        throw invalidViewTypeException(i);
    }

    /* compiled from: ModeratorStartStageAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0004\u0018\u00002\u00060\u0001R\u00020\u0002B\u0011\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u0004\u001a\u00020\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$ContinueViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$CreateStageEventViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;", "Lcom/discord/databinding/ModeratorStartStageContinueItemBinding;", "binding", "Lcom/discord/databinding/ModeratorStartStageContinueItemBinding;", "getBinding", "()Lcom/discord/databinding/ModeratorStartStageContinueItemBinding;", HookHelper.constructorName, "(Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;Lcom/discord/databinding/ModeratorStartStageContinueItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class ContinueViewHolder extends CreateStageEventViewHolder {
        private final ModeratorStartStageContinueItemBinding binding;
        public final /* synthetic */ ModeratorStartStageAdapter this$0;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public ContinueViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r3, com.discord.databinding.ModeratorStartStageContinueItemBinding r4) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                r2.this$0 = r3
                androidx.constraintlayout.widget.ConstraintLayout r0 = r4.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r3, r0)
                r2.binding = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.ContinueViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageContinueItemBinding):void");
        }

        public final ModeratorStartStageContinueItemBinding getBinding() {
            return this.binding;
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public ContinueViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r2, com.discord.databinding.ModeratorStartStageContinueItemBinding r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
            /*
                r1 = this;
                r4 = r4 & 1
                if (r4 == 0) goto L2a
                android.content.Context r3 = r2.getContext()
                android.view.LayoutInflater r3 = android.view.LayoutInflater.from(r3)
                androidx.recyclerview.widget.RecyclerView r4 = r2.getRecycler()
                r5 = 2131558585(0x7f0d00b9, float:1.874249E38)
                r0 = 0
                android.view.View r3 = r3.inflate(r5, r4, r0)
                java.lang.String r4 = "rootView"
                java.util.Objects.requireNonNull(r3, r4)
                com.discord.databinding.ModeratorStartStageContinueItemBinding r4 = new com.discord.databinding.ModeratorStartStageContinueItemBinding
                androidx.constraintlayout.widget.ConstraintLayout r3 = (androidx.constraintlayout.widget.ConstraintLayout) r3
                r4.<init>(r3)
                java.lang.String r3 = "ModeratorStartStageConti…ontext), recycler, false)"
                d0.z.d.m.checkNotNullExpressionValue(r4, r3)
                r3 = r4
            L2a:
                r1.<init>(r2, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.ContinueViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageContinueItemBinding, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    /* compiled from: ModeratorStartStageAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0004\u0018\u00002\u00060\u0001R\u00020\u0002B\u0011\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u0004\u001a\u00020\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$HeaderViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter$CreateStageEventViewHolder;", "Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;", "Lcom/discord/databinding/ModeratorStartStageHeaderItemBinding;", "binding", "Lcom/discord/databinding/ModeratorStartStageHeaderItemBinding;", "getBinding", "()Lcom/discord/databinding/ModeratorStartStageHeaderItemBinding;", HookHelper.constructorName, "(Lcom/discord/widgets/stage/start/ModeratorStartStageAdapter;Lcom/discord/databinding/ModeratorStartStageHeaderItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class HeaderViewHolder extends CreateStageEventViewHolder {
        private final ModeratorStartStageHeaderItemBinding binding;
        public final /* synthetic */ ModeratorStartStageAdapter this$0;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public HeaderViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r3, com.discord.databinding.ModeratorStartStageHeaderItemBinding r4) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                r2.this$0 = r3
                androidx.constraintlayout.widget.ConstraintLayout r0 = r4.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r3, r0)
                r2.binding = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.HeaderViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageHeaderItemBinding):void");
        }

        public final ModeratorStartStageHeaderItemBinding getBinding() {
            return this.binding;
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public HeaderViewHolder(com.discord.widgets.stage.start.ModeratorStartStageAdapter r3, com.discord.databinding.ModeratorStartStageHeaderItemBinding r4, int r5, kotlin.jvm.internal.DefaultConstructorMarker r6) {
            /*
                r2 = this;
                r5 = r5 & 1
                if (r5 == 0) goto L5b
                android.content.Context r4 = r3.getContext()
                android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
                androidx.recyclerview.widget.RecyclerView r5 = r3.getRecycler()
                r6 = 2131558586(0x7f0d00ba, float:1.8742492E38)
                r0 = 0
                android.view.View r4 = r4.inflate(r6, r5, r0)
                r5 = 2131364388(0x7f0a0a24, float:1.8348612E38)
                android.view.View r6 = r4.findViewById(r5)
                android.widget.ImageView r6 = (android.widget.ImageView) r6
                if (r6 == 0) goto L47
                r5 = 2131364389(0x7f0a0a25, float:1.8348614E38)
                android.view.View r0 = r4.findViewById(r5)
                android.widget.TextView r0 = (android.widget.TextView) r0
                if (r0 == 0) goto L47
                r5 = 2131364390(0x7f0a0a26, float:1.8348616E38)
                android.view.View r1 = r4.findViewById(r5)
                android.widget.TextView r1 = (android.widget.TextView) r1
                if (r1 == 0) goto L47
                com.discord.databinding.ModeratorStartStageHeaderItemBinding r5 = new com.discord.databinding.ModeratorStartStageHeaderItemBinding
                androidx.constraintlayout.widget.ConstraintLayout r4 = (androidx.constraintlayout.widget.ConstraintLayout) r4
                r5.<init>(r4, r6, r0, r1)
                java.lang.String r4 = "ModeratorStartStageHeade…ontext), recycler, false)"
                d0.z.d.m.checkNotNullExpressionValue(r5, r4)
                r4 = r5
                goto L5b
            L47:
                android.content.res.Resources r3 = r4.getResources()
                java.lang.String r3 = r3.getResourceName(r5)
                java.lang.NullPointerException r4 = new java.lang.NullPointerException
                java.lang.String r5 = "Missing required view with ID: "
                java.lang.String r3 = r5.concat(r3)
                r4.<init>(r3)
                throw r4
            L5b:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stage.start.ModeratorStartStageAdapter.HeaderViewHolder.<init>(com.discord.widgets.stage.start.ModeratorStartStageAdapter, com.discord.databinding.ModeratorStartStageHeaderItemBinding, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }
}
