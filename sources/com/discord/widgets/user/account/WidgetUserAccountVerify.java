package com.discord.widgets.user.account;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.d.f;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetUserAccountVerifyBinding;
import com.discord.models.requiredaction.RequiredAction;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import com.discord.widgets.user.email.WidgetUserEmailVerify;
import com.discord.widgets.user.phone.WidgetUserPhoneManage;
import d0.z.d.m;
import d0.z.d.w;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetUserAccountVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u0007¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/user/account/WidgetUserAccountVerify;", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetUserAccountVerifyBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserAccountVerifyBinding;", "binding", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserAccountVerify extends WidgetUserAccountVerifyBase {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserAccountVerify.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserAccountVerifyBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserAccountVerify$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetUserAccountVerify.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/user/account/WidgetUserAccountVerify$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/models/requiredaction/RequiredAction;", "action", "", "launch", "(Landroid/content/Context;Lcom/discord/models/requiredaction/RequiredAction;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, RequiredAction requiredAction) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(requiredAction, "action");
            boolean z2 = false;
            boolean z3 = requiredAction == RequiredAction.REQUIRE_VERIFIED_PHONE || requiredAction == RequiredAction.REQUIRE_VERIFIED_EMAIL || requiredAction == RequiredAction.REQUIRE_CAPTCHA;
            if (requiredAction == RequiredAction.REQUIRE_VERIFIED_EMAIL) {
                z2 = true;
            }
            Intent launchIntent = WidgetUserAccountVerifyBase.Companion.getLaunchIntent(WidgetUserAccountVerifyBase.Mode.FORCED, z3, z2);
            AnalyticsTracker.openModal$default("Suspicious Activity", "", null, 4, null);
            j.d(context, WidgetUserAccountVerify.class, launchIntent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetUserAccountVerify() {
        super(R.layout.widget_user_account_verify);
    }

    private final WidgetUserAccountVerifyBinding getBinding() {
        return (WidgetUserAccountVerifyBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.widgets.user.account.WidgetUserAccountVerifyBase, com.discord.app.AppFragment
    public void onViewBound(View view) {
        CharSequence e;
        CharSequence e2;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        final w widgetUserAccountVerify$onViewBound$1 = new w(this) { // from class: com.discord.widgets.user.account.WidgetUserAccountVerify$onViewBound$1
            {
                super(this, WidgetUserAccountVerify.class, "isForced", "isForced()Z", 0);
            }

            @Override // d0.z.d.w, kotlin.reflect.KProperty0
            public Object get() {
                return Boolean.valueOf(((WidgetUserAccountVerify) this.receiver).isForced());
            }
        };
        Func0 widgetUserAccountVerify$sam$rx_functions_Func0$0 = new Func0() { // from class: com.discord.widgets.user.account.WidgetUserAccountVerify$sam$rx_functions_Func0$0
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final /* synthetic */ Object call() {
                return Function0.this.invoke();
            }
        };
        int i = 0;
        AppFragment.setOnBackPressed$default(this, widgetUserAccountVerify$sam$rx_functions_Func0$0, 0, 2, null);
        WidgetUserAccountVerifyBinding binding = getBinding();
        TextView textView = binding.e;
        m.checkNotNullExpressionValue(textView, "verifyAccountTextBody");
        e = b.e(this, R.string.verification_body, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        textView.setText(e);
        LinkifiedTextView linkifiedTextView = binding.d;
        m.checkNotNullExpressionValue(linkifiedTextView, "verifyAccountSupport");
        e2 = b.e(this, R.string.verification_footer_support, new Object[]{f.c()}, (r4 & 4) != 0 ? b.a.j : null);
        linkifiedTextView.setText(e2);
        RelativeLayout relativeLayout = binding.f2652b;
        relativeLayout.setVisibility(isEmailAllowed() ? 0 : 8);
        relativeLayout.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.account.WidgetUserAccountVerify$onViewBound$$inlined$with$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                m.checkNotNullParameter(view2, "v");
                WidgetUserEmailVerify.Companion companion = WidgetUserEmailVerify.Companion;
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "v.context");
                companion.launch(context, WidgetUserAccountVerify.this.getMode());
            }
        });
        RelativeLayout relativeLayout2 = binding.c;
        if (!isPhoneAllowed()) {
            i = 8;
        }
        relativeLayout2.setVisibility(i);
        relativeLayout2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.account.WidgetUserAccountVerify$onViewBound$$inlined$with$lambda$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                m.checkNotNullParameter(view2, "v");
                WidgetUserPhoneManage.Companion companion = WidgetUserPhoneManage.Companion;
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "v.context");
                companion.launch(context, WidgetUserAccountVerify.this.getMode(), WidgetUserPhoneManage.Companion.Source.USER_ACTION_REQUIRED);
            }
        });
    }
}
