package com.discord.widgets.user.account;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.annotation.LayoutRes;
import androidx.fragment.app.FragmentActivity;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.models.requiredaction.RequiredAction;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
import xyz.discord.R;
/* compiled from: WidgetUserAccountVerifyBase.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0006\b&\u0018\u0000 )2\u00020\u0001:\u0002)*B\u0011\u0012\b\b\u0001\u0010&\u001a\u00020%¢\u0006\u0004\b'\u0010(J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\tH\u0002¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\u0010\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0016\u0010\u0004R\u0013\u0010\u0017\u001a\u00020\u00058F@\u0006¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\"\u0010\u001a\u001a\u00020\u00198\u0004@\u0004X\u0084.¢\u0006\u0012\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\"\u0010 \u001a\u00020\u00058\u0004@\u0004X\u0084\u000e¢\u0006\u0012\n\u0004\b \u0010!\u001a\u0004\b \u0010\u0018\"\u0004\b\"\u0010\bR\"\u0010#\u001a\u00020\u00058\u0004@\u0004X\u0084\u000e¢\u0006\u0012\n\u0004\b#\u0010!\u001a\u0004\b#\u0010\u0018\"\u0004\b$\u0010\b¨\u0006+"}, d2 = {"Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;", "Lcom/discord/app/AppFragment;", "", "setOptionsMenu", "()V", "", "isAuthorized", "handleIsAuthorized", "(Z)V", "Lrx/Observable;", "getShouldDismissObservable", "()Lrx/Observable;", "isMissingPhone", "isMissingEmailOrUnverified", "Lcom/discord/models/requiredaction/RequiredAction;", "requiredAction", "computeShouldDismiss", "(ZZLcom/discord/models/requiredaction/RequiredAction;)Z", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "isForced", "()Z", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "mode", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "getMode", "()Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "setMode", "(Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;)V", "isEmailAllowed", "Z", "setEmailAllowed", "isPhoneAllowed", "setPhoneAllowed", "", "contentResId", HookHelper.constructorName, "(I)V", "Companion", "Mode", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class WidgetUserAccountVerifyBase extends AppFragment {
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EMAIL_ALLOWED = "INTENT_EMAIL_ALLOWED";
    private static final String INTENT_MODE = "INTENT_MODE";
    private static final String INTENT_PHONE_ALLOWED = "INTENT_PHONE_ALLOWED";
    public Mode mode;
    private boolean isPhoneAllowed = true;
    private boolean isEmailAllowed = true;

    /* compiled from: WidgetUserAccountVerifyBase.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\fR\u0016\u0010\u000e\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Companion;", "", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "mode", "", "phoneAllowed", "emailAllowed", "Landroid/content/Intent;", "getLaunchIntent", "(Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;ZZ)Landroid/content/Intent;", "", WidgetUserAccountVerifyBase.INTENT_EMAIL_ALLOWED, "Ljava/lang/String;", WidgetUserAccountVerifyBase.INTENT_MODE, WidgetUserAccountVerifyBase.INTENT_PHONE_ALLOWED, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Intent getLaunchIntent(Mode mode, boolean z2, boolean z3) {
            m.checkNotNullParameter(mode, "mode");
            Intent intent = new Intent();
            intent.putExtra(WidgetUserAccountVerifyBase.INTENT_PHONE_ALLOWED, z2);
            intent.putExtra(WidgetUserAccountVerifyBase.INTENT_EMAIL_ALLOWED, z3);
            intent.putExtra(WidgetUserAccountVerifyBase.INTENT_MODE, mode.ordinal());
            return intent;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserAccountVerifyBase.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "UNFORCED", "FORCED", "NO_HISTORY_FROM_USER_SETTINGS", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum Mode {
        UNFORCED,
        FORCED,
        NO_HISTORY_FROM_USER_SETTINGS
    }

    public WidgetUserAccountVerifyBase(@LayoutRes int i) {
        super(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean computeShouldDismiss(boolean z2, boolean z3, RequiredAction requiredAction) {
        Mode mode = this.mode;
        if (mode == null) {
            m.throwUninitializedPropertyAccessException("mode");
        }
        if (mode == Mode.UNFORCED) {
            return true;
        }
        Mode mode2 = this.mode;
        if (mode2 == null) {
            m.throwUninitializedPropertyAccessException("mode");
        }
        if (mode2 == Mode.NO_HISTORY_FROM_USER_SETTINGS) {
            return true;
        }
        if (!z2 || requiredAction != RequiredAction.REQUIRE_VERIFIED_PHONE) {
            return (z2 || z3) && requiredAction == RequiredAction.REQUIRE_VERIFIED_EMAIL;
        }
        return true;
    }

    public static final Intent getLaunchIntent(Mode mode, boolean z2, boolean z3) {
        return Companion.getLaunchIntent(mode, z2, z3);
    }

    private final Observable<Boolean> getShouldDismissObservable() {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable j = Observable.j(companion.getUserRequiredActions().observeUserRequiredAction(), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), new Func2<RequiredAction, MeUser, Boolean>() { // from class: com.discord.widgets.user.account.WidgetUserAccountVerifyBase$getShouldDismissObservable$1
            public final Boolean call(RequiredAction requiredAction, MeUser meUser) {
                boolean z2;
                boolean computeShouldDismiss;
                boolean z3 = true;
                if (WidgetUserAccountVerifyBase.this.isPhoneAllowed()) {
                    UserUtils userUtils = UserUtils.INSTANCE;
                    m.checkNotNullExpressionValue(meUser, "me");
                    if (!userUtils.getHasPhone(meUser)) {
                        z2 = true;
                        if (WidgetUserAccountVerifyBase.this.isEmailAllowed() || meUser.isVerified()) {
                            z3 = false;
                        }
                        WidgetUserAccountVerifyBase widgetUserAccountVerifyBase = WidgetUserAccountVerifyBase.this;
                        m.checkNotNullExpressionValue(requiredAction, "requiredAction");
                        computeShouldDismiss = widgetUserAccountVerifyBase.computeShouldDismiss(z2, z3, requiredAction);
                        return Boolean.valueOf(computeShouldDismiss);
                    }
                }
                z2 = false;
                if (WidgetUserAccountVerifyBase.this.isEmailAllowed()) {
                }
                z3 = false;
                WidgetUserAccountVerifyBase widgetUserAccountVerifyBase2 = WidgetUserAccountVerifyBase.this;
                m.checkNotNullExpressionValue(requiredAction, "requiredAction");
                computeShouldDismiss = widgetUserAccountVerifyBase2.computeShouldDismiss(z2, z3, requiredAction);
                return Boolean.valueOf(computeShouldDismiss);
            }
        });
        m.checkNotNullExpressionValue(j, "Observable\n        .comb…requiredAction)\n        }");
        Observable<Boolean> q = ObservableExtensionsKt.computationBuffered(j).q();
        m.checkNotNullExpressionValue(q, "Observable\n        .comb…  .distinctUntilChanged()");
        return q;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleIsAuthorized(boolean z2) {
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            if (!z2) {
                j.c(context, false, null, 6);
            }
        }
    }

    private final void setOptionsMenu() {
        Mode mode = this.mode;
        if (mode == null) {
            m.throwUninitializedPropertyAccessException("mode");
        }
        AppFragment.setActionBarOptionsMenu$default(this, mode == Mode.FORCED ? R.menu.menu_settings_logout : R.menu.menu_empty, WidgetUserAccountVerifyBase$setOptionsMenu$1.INSTANCE, null, 4, null);
    }

    public final Mode getMode() {
        Mode mode = this.mode;
        if (mode == null) {
            m.throwUninitializedPropertyAccessException("mode");
        }
        return mode;
    }

    public final boolean isEmailAllowed() {
        return this.isEmailAllowed;
    }

    public final boolean isForced() {
        Mode mode = this.mode;
        if (mode == null) {
            m.throwUninitializedPropertyAccessException("mode");
        }
        return mode == Mode.FORCED;
    }

    public final boolean isPhoneAllowed() {
        return this.isPhoneAllowed;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        int intExtra = getMostRecentIntent().getIntExtra(INTENT_MODE, -1);
        Mode.values();
        if (intExtra < 0 || 3 <= intExtra) {
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("Invalid mode passed into ");
            R.append(WidgetUserAccountVerifyBase.class.getName());
            Logger.e$default(appLog, R.toString(), null, null, 6, null);
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        this.mode = Mode.values()[intExtra];
        setOptionsMenu();
        this.isPhoneAllowed = getMostRecentIntent().getBooleanExtra(INTENT_PHONE_ALLOWED, this.isPhoneAllowed);
        this.isEmailAllowed = getMostRecentIntent().getBooleanExtra(INTENT_EMAIL_ALLOWED, this.isEmailAllowed);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getShouldDismissObservable(), this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserAccountVerifyBase$onViewBoundOrOnResume$1(this));
    }

    public final void setEmailAllowed(boolean z2) {
        this.isEmailAllowed = z2;
    }

    public final void setMode(Mode mode) {
        m.checkNotNullParameter(mode, "<set-?>");
        this.mode = mode;
    }

    public final void setPhoneAllowed(boolean z2) {
        this.isPhoneAllowed = z2;
    }
}
