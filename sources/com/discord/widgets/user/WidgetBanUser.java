package com.discord.widgets.user;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetBanUserBinding;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.t;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetBanUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u0007¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u001c\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/user/WidgetBanUser;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetBanUserBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetBanUserBinding;", "binding", "", "Lcom/discord/views/CheckedSetting;", "getHistoryRadios", "()Ljava/util/List;", "historyRadios", "Lcom/discord/views/RadioManager;", "deleteHistoryRadioManager", "Lcom/discord/views/RadioManager;", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetBanUser extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetBanUser.class, "binding", "getBinding()Lcom/discord/databinding/WidgetBanUserBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetBanUser$binding$2.INSTANCE, null, 2, null);
    private RadioManager deleteHistoryRadioManager;

    /* compiled from: WidgetBanUser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ1\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/user/WidgetBanUser$Companion;", "", "", "userName", "", "guildId", "userId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "launch", "(Ljava/lang/String;JJLandroidx/fragment/app/FragmentManager;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(String str, long j, long j2, FragmentManager fragmentManager) {
            m.checkNotNullParameter(str, "userName");
            if (fragmentManager != null) {
                WidgetBanUser widgetBanUser = new WidgetBanUser();
                Bundle bundle = new Bundle();
                bundle.putString("com.discord.intent.extra.EXTRA_USER_NAME", str);
                bundle.putLong("com.discord.intent.extra.EXTRA_GUILD_ID", j);
                bundle.putLong("com.discord.intent.extra.EXTRA_USER_ID", j2);
                widgetBanUser.setArguments(bundle);
                widgetBanUser.show(fragmentManager, WidgetBanUser.class.getSimpleName());
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetBanUser() {
        super(R.layout.widget_ban_user);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetBanUserBinding getBinding() {
        return (WidgetBanUserBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<CheckedSetting> getHistoryRadios() {
        CheckedSetting checkedSetting = getBinding().g;
        m.checkNotNullExpressionValue(checkedSetting, "binding.banUserDeleteMessagesNone");
        CheckedSetting checkedSetting2 = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.banUserDeleteMessages1Day");
        CheckedSetting checkedSetting3 = getBinding().f;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.banUserDeleteMessages7Days");
        return n.listOf((Object[]) new CheckedSetting[]{checkedSetting, checkedSetting2, checkedSetting3});
    }

    public static final void launch(String str, long j, long j2, FragmentManager fragmentManager) {
        Companion.launch(str, j, j2, fragmentManager);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        this.deleteHistoryRadioManager = new RadioManager(getHistoryRadios());
        for (final CheckedSetting checkedSetting : getHistoryRadios()) {
            checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetBanUser$onViewBound$$inlined$forEach$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    RadioManager radioManager;
                    radioManager = this.deleteHistoryRadioManager;
                    if (radioManager != null) {
                        radioManager.a(CheckedSetting.this);
                    }
                }
            });
        }
        final String string = getArgumentsOrDefault().getString("com.discord.intent.extra.EXTRA_USER_NAME", "");
        final long j = getArgumentsOrDefault().getLong("com.discord.intent.extra.EXTRA_GUILD_ID", -1L);
        final long j2 = getArgumentsOrDefault().getLong("com.discord.intent.extra.EXTRA_USER_ID", -1L);
        TextView textView = getBinding().i;
        m.checkNotNullExpressionValue(textView, "binding.banUserTitle");
        b.m(textView, R.string.ban_user_title, new Object[]{string}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView2 = getBinding().f2223b;
        m.checkNotNullExpressionValue(textView2, "binding.banUserBody");
        b.m(textView2, R.string.ban_user_body, new Object[]{string}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetBanUser$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetBanUser.this.dismiss();
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetBanUser$onViewBound$3

            /* compiled from: WidgetBanUser.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.user.WidgetBanUser$onViewBound$3$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Void, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
                    invoke2(r1);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Void r7) {
                    CharSequence charSequence;
                    Context context = WidgetBanUser.this.getContext();
                    Context context2 = WidgetBanUser.this.getContext();
                    if (context2 != null) {
                        charSequence = b.b(context2, R.string.ban_user_confirmed, new Object[]{string}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    } else {
                        charSequence = null;
                    }
                    b.a.d.m.h(context, charSequence, 0, null, 12);
                    WidgetBanUser.this.dismiss();
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                List historyRadios;
                RadioManager radioManager;
                int i;
                WidgetBanUserBinding binding;
                historyRadios = WidgetBanUser.this.getHistoryRadios();
                radioManager = WidgetBanUser.this.deleteHistoryRadioManager;
                switch (((CheckedSetting) historyRadios.get(radioManager != null ? radioManager.b() : 0)).getId()) {
                    case R.id.ban_user_delete_messages_1_day /* 2131362122 */:
                        i = 1;
                        break;
                    case R.id.ban_user_delete_messages_7_days /* 2131362123 */:
                        i = 7;
                        break;
                    case R.id.ban_user_delete_messages_none /* 2131362124 */:
                    default:
                        i = 0;
                        break;
                }
                binding = WidgetBanUser.this.getBinding();
                TextInputLayout textInputLayout = binding.h;
                m.checkNotNullExpressionValue(textInputLayout, "binding.banUserReason");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().banGuildMember(j, j2, new RestAPIParams.BanGuildMember(Integer.valueOf(i)), t.isBlank(textOrEmpty) ^ true ? textOrEmpty : null), false, 1, null), WidgetBanUser.this, null, 2, null), WidgetBanUser.this.getClass(), (r18 & 2) != 0 ? null : WidgetBanUser.this.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
            }
        });
    }
}
