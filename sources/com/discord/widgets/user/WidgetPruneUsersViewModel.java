package com.discord.widgets.user;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.user.WidgetPruneUsersViewModel;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
/* compiled from: WidgetPruneUsersViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 32\b\u0012\u0004\u0012\u00020\u00020\u0001:\u000534567BG\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u0010\"\u001a\u00020!\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u001e\u0012\u000e\b\u0002\u00100\u001a\b\u0012\u0004\u0012\u00020/0\n¢\u0006\u0004\b1\u00102J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0013\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0004\b\f\u0010\rJ\u0015\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0010\u0010\u0011J\r\u0010\u0012\u001a\u00020\u0003¢\u0006\u0004\b\u0012\u0010\u0005R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001c\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0018\u0010%\u001a\u0004\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R:\u0010)\u001a&\u0012\f\u0012\n (*\u0004\u0018\u00010\u000b0\u000b (*\u0012\u0012\f\u0012\n (*\u0004\u0018\u00010\u000b0\u000b\u0018\u00010'0'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010.\u001a\u00020+8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b,\u0010-¨\u00068"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;", "", "getUpdatedPruneCount", "()V", "", "inProgress", "pruneInProgress", "(Z)V", "Lrx/Observable;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "days", "pruneDaysSelected", "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;)V", "pruneClicked", "", "guildId", "J", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "", "guildName", "Ljava/lang/String;", "whichPruneDays", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreUser;", "Lrx/Subscription;", "pruneCountRequest", "Lrx/Subscription;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loading;", "getLoadingState", "()Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loading;", "loadingState", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$StoreData;", "storeDataObservable", HookHelper.constructorName, "(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Ljava/lang/String;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V", "Companion", "Event", "PruneDays", "StoreData", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPruneUsersViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private static final PruneDays DEFAULT_DAYS = PruneDays.Thirty;
    private final PublishSubject<Event> eventSubject;
    private final long guildId;
    private final String guildName;
    private Subscription pruneCountRequest;
    private final RestAPI restAPI;
    private final StoreGuilds storeGuilds;
    private final StoreUser storeUsers;
    private PruneDays whichPruneDays;

    /* compiled from: WidgetPruneUsersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$StoreData;", "kotlin.jvm.PlatformType", "storeData", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$StoreData;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.WidgetPruneUsersViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T, R> implements b<StoreData, Boolean> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Boolean call(StoreData storeData) {
            return Boolean.valueOf((storeData.getPermission() == null || storeData.getUser() == null || storeData.getGuild() == null || !PermissionUtils.canAndIsElevated(2L, storeData.getPermission(), storeData.getUser().getMfaEnabled(), storeData.getGuild().getMfaLevel())) ? false : true);
        }
    }

    /* compiled from: WidgetPruneUsersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "canKick", "", "invoke", "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.WidgetPruneUsersViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<Boolean, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke2(bool);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Boolean bool) {
            m.checkNotNullExpressionValue(bool, "canKick");
            if (bool.booleanValue()) {
                WidgetPruneUsersViewModel.this.getUpdatedPruneCount();
            } else {
                WidgetPruneUsersViewModel.this.updateViewState(new ViewState.LoadFailed(true));
            }
        }
    }

    /* compiled from: WidgetPruneUsersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J?\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lrx/Observable;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$StoreData;", "observeStoreState", "(JLcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;)Lrx/Observable;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "DEFAULT_DAYS", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<StoreData> observeStoreState(long j, ObservationDeck observationDeck, StorePermissions storePermissions, StoreGuilds storeGuilds, StoreUser storeUser) {
            m.checkNotNullParameter(observationDeck, "observationDeck");
            m.checkNotNullParameter(storePermissions, "storePermissions");
            m.checkNotNullParameter(storeGuilds, "storeGuilds");
            m.checkNotNullParameter(storeUser, "storeUsers");
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storePermissions, storeGuilds, storeUser}, false, null, null, new WidgetPruneUsersViewModel$Companion$observeStoreState$1(storePermissions, j, storeGuilds, storeUser), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetPruneUsersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event;", "", HookHelper.constructorName, "()V", "Dismiss", "RestClientFailed", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event$Dismiss;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event$RestClientFailed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetPruneUsersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event$Dismiss;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Dismiss extends Event {
            public static final Dismiss INSTANCE = new Dismiss();

            private Dismiss() {
                super(null);
            }
        }

        /* compiled from: WidgetPruneUsersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event$RestClientFailed;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event;", "", "component1", "()Ljava/lang/Throwable;", "throwable", "copy", "(Ljava/lang/Throwable;)Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event$RestClientFailed;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Throwable;", "getThrowable", HookHelper.constructorName, "(Ljava/lang/Throwable;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RestClientFailed extends Event {
            private final Throwable throwable;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public RestClientFailed(Throwable th) {
                super(null);
                m.checkNotNullParameter(th, "throwable");
                this.throwable = th;
            }

            public static /* synthetic */ RestClientFailed copy$default(RestClientFailed restClientFailed, Throwable th, int i, Object obj) {
                if ((i & 1) != 0) {
                    th = restClientFailed.throwable;
                }
                return restClientFailed.copy(th);
            }

            public final Throwable component1() {
                return this.throwable;
            }

            public final RestClientFailed copy(Throwable th) {
                m.checkNotNullParameter(th, "throwable");
                return new RestClientFailed(th);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof RestClientFailed) && m.areEqual(this.throwable, ((RestClientFailed) obj).throwable);
                }
                return true;
            }

            public final Throwable getThrowable() {
                return this.throwable;
            }

            public int hashCode() {
                Throwable th = this.throwable;
                if (th != null) {
                    return th.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("RestClientFailed(throwable=");
                R.append(this.throwable);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetPruneUsersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\n¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "", "", "count", "I", "getCount", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "Seven", "Thirty", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum PruneDays {
        Seven(7),
        Thirty(30);
        
        private final int count;

        PruneDays(int i) {
            this.count = i;
        }

        public final int getCount() {
            return this.count;
        }
    }

    /* compiled from: WidgetPruneUsersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u000e\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b!\u0010\"J\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u000f\u001a\u00020\u00002\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR!\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b \u0010\u000b¨\u0006#"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$StoreData;", "", "", "Lcom/discord/api/permission/PermissionBit;", "component1", "()Ljava/lang/Long;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/user/MeUser;", "component3", "()Lcom/discord/models/user/MeUser;", "permission", "guild", "user", "copy", "(Ljava/lang/Long;Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;)Lcom/discord/widgets/user/WidgetPruneUsersViewModel$StoreData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getPermission", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/user/MeUser;", "getUser", HookHelper.constructorName, "(Ljava/lang/Long;Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreData {
        private final Guild guild;
        private final Long permission;
        private final MeUser user;

        public StoreData(Long l, Guild guild, MeUser meUser) {
            this.permission = l;
            this.guild = guild;
            this.user = meUser;
        }

        public static /* synthetic */ StoreData copy$default(StoreData storeData, Long l, Guild guild, MeUser meUser, int i, Object obj) {
            if ((i & 1) != 0) {
                l = storeData.permission;
            }
            if ((i & 2) != 0) {
                guild = storeData.guild;
            }
            if ((i & 4) != 0) {
                meUser = storeData.user;
            }
            return storeData.copy(l, guild, meUser);
        }

        public final Long component1() {
            return this.permission;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final MeUser component3() {
            return this.user;
        }

        public final StoreData copy(Long l, Guild guild, MeUser meUser) {
            return new StoreData(l, guild, meUser);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreData)) {
                return false;
            }
            StoreData storeData = (StoreData) obj;
            return m.areEqual(this.permission, storeData.permission) && m.areEqual(this.guild, storeData.guild) && m.areEqual(this.user, storeData.user);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Long getPermission() {
            return this.permission;
        }

        public final MeUser getUser() {
            return this.user;
        }

        public int hashCode() {
            Long l = this.permission;
            int i = 0;
            int hashCode = (l != null ? l.hashCode() : 0) * 31;
            Guild guild = this.guild;
            int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
            MeUser meUser = this.user;
            if (meUser != null) {
                i = meUser.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreData(permission=");
            R.append(this.permission);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", user=");
            R.append(this.user);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetPruneUsersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;", "", HookHelper.constructorName, "()V", "LoadFailed", "Loaded", "Loading", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loading;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loaded;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$LoadFailed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetPruneUsersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$LoadFailed;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;", "", "component1", "()Z", "dismiss", "copy", "(Z)Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$LoadFailed;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getDismiss", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LoadFailed extends ViewState {
            private final boolean dismiss;

            public LoadFailed(boolean z2) {
                super(null);
                this.dismiss = z2;
            }

            public static /* synthetic */ LoadFailed copy$default(LoadFailed loadFailed, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = loadFailed.dismiss;
                }
                return loadFailed.copy(z2);
            }

            public final boolean component1() {
                return this.dismiss;
            }

            public final LoadFailed copy(boolean z2) {
                return new LoadFailed(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof LoadFailed) && this.dismiss == ((LoadFailed) obj).dismiss;
                }
                return true;
            }

            public final boolean getDismiss() {
                return this.dismiss;
            }

            public int hashCode() {
                boolean z2 = this.dismiss;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            public String toString() {
                return a.M(a.R("LoadFailed(dismiss="), this.dismiss, ")");
            }
        }

        /* compiled from: WidgetPruneUsersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\b\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\b2\b\b\u0002\u0010\r\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0013\u0010\nJ\u001a\u0010\u0016\u001a\u00020\u00022\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007R\u0016\u0010\r\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u001aR\u0019\u0010\f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\nR\u0013\u0010\u001e\u001a\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u0004¨\u0006!"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loaded;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;", "", "component3", "()Z", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "component1", "()Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "", "component2", "()I", "pruneDays", "pruneCount", "pruneInProgress", "copy", "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;IZ)Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "getPruneDays", "Z", "I", "getPruneCount", "getPruneButtonEnabled", "pruneButtonEnabled", HookHelper.constructorName, "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;IZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final int pruneCount;
            private final PruneDays pruneDays;
            private final boolean pruneInProgress;

            public /* synthetic */ Loaded(PruneDays pruneDays, int i, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
                this(pruneDays, i, (i2 & 4) != 0 ? false : z2);
            }

            private final boolean component3() {
                return this.pruneInProgress;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, PruneDays pruneDays, int i, boolean z2, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    pruneDays = loaded.pruneDays;
                }
                if ((i2 & 2) != 0) {
                    i = loaded.pruneCount;
                }
                if ((i2 & 4) != 0) {
                    z2 = loaded.pruneInProgress;
                }
                return loaded.copy(pruneDays, i, z2);
            }

            public final PruneDays component1() {
                return this.pruneDays;
            }

            public final int component2() {
                return this.pruneCount;
            }

            public final Loaded copy(PruneDays pruneDays, int i, boolean z2) {
                m.checkNotNullParameter(pruneDays, "pruneDays");
                return new Loaded(pruneDays, i, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.pruneDays, loaded.pruneDays) && this.pruneCount == loaded.pruneCount && this.pruneInProgress == loaded.pruneInProgress;
            }

            public final boolean getPruneButtonEnabled() {
                return this.pruneCount > 0 && !this.pruneInProgress;
            }

            public final int getPruneCount() {
                return this.pruneCount;
            }

            public final PruneDays getPruneDays() {
                return this.pruneDays;
            }

            public int hashCode() {
                PruneDays pruneDays = this.pruneDays;
                int hashCode = (((pruneDays != null ? pruneDays.hashCode() : 0) * 31) + this.pruneCount) * 31;
                boolean z2 = this.pruneInProgress;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(pruneDays=");
                R.append(this.pruneDays);
                R.append(", pruneCount=");
                R.append(this.pruneCount);
                R.append(", pruneInProgress=");
                return a.M(R, this.pruneInProgress, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(PruneDays pruneDays, int i, boolean z2) {
                super(null);
                m.checkNotNullParameter(pruneDays, "pruneDays");
                this.pruneDays = pruneDays;
                this.pruneCount = i;
                this.pruneInProgress = z2;
            }
        }

        /* compiled from: WidgetPruneUsersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loading;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "component1", "()Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "", "component2", "()Ljava/lang/String;", "whichPruneDays", "guildName", "copy", "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;Ljava/lang/String;)Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState$Loading;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;", "getWhichPruneDays", "Ljava/lang/String;", "getGuildName", HookHelper.constructorName, "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$PruneDays;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            private final String guildName;
            private final PruneDays whichPruneDays;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loading(PruneDays pruneDays, String str) {
                super(null);
                m.checkNotNullParameter(pruneDays, "whichPruneDays");
                m.checkNotNullParameter(str, "guildName");
                this.whichPruneDays = pruneDays;
                this.guildName = str;
            }

            public static /* synthetic */ Loading copy$default(Loading loading, PruneDays pruneDays, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    pruneDays = loading.whichPruneDays;
                }
                if ((i & 2) != 0) {
                    str = loading.guildName;
                }
                return loading.copy(pruneDays, str);
            }

            public final PruneDays component1() {
                return this.whichPruneDays;
            }

            public final String component2() {
                return this.guildName;
            }

            public final Loading copy(PruneDays pruneDays, String str) {
                m.checkNotNullParameter(pruneDays, "whichPruneDays");
                m.checkNotNullParameter(str, "guildName");
                return new Loading(pruneDays, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loading)) {
                    return false;
                }
                Loading loading = (Loading) obj;
                return m.areEqual(this.whichPruneDays, loading.whichPruneDays) && m.areEqual(this.guildName, loading.guildName);
            }

            public final String getGuildName() {
                return this.guildName;
            }

            public final PruneDays getWhichPruneDays() {
                return this.whichPruneDays;
            }

            public int hashCode() {
                PruneDays pruneDays = this.whichPruneDays;
                int i = 0;
                int hashCode = (pruneDays != null ? pruneDays.hashCode() : 0) * 31;
                String str = this.guildName;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loading(whichPruneDays=");
                R.append(this.whichPruneDays);
                R.append(", guildName=");
                return a.H(R, this.guildName, ")");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetPruneUsersViewModel(long r10, com.discord.stores.StoreGuilds r12, com.discord.stores.StoreUser r13, java.lang.String r14, com.discord.utilities.rest.RestAPI r15, rx.Observable r16, int r17, kotlin.jvm.internal.DefaultConstructorMarker r18) {
        /*
            r9 = this;
            r0 = r17 & 2
            if (r0 == 0) goto Lc
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r0 = r0.getGuilds()
            r4 = r0
            goto Ld
        Lc:
            r4 = r12
        Ld:
            r0 = r17 & 4
            if (r0 == 0) goto L19
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r0 = r0.getUsers()
            r5 = r0
            goto L1a
        L19:
            r5 = r13
        L1a:
            r0 = r17 & 8
            if (r0 == 0) goto L3b
            java.util.Map r0 = r4.getGuilds()
            java.lang.Long r1 = java.lang.Long.valueOf(r10)
            java.lang.Object r0 = r0.get(r1)
            com.discord.models.guild.Guild r0 = (com.discord.models.guild.Guild) r0
            if (r0 == 0) goto L33
            java.lang.String r0 = r0.getName()
            goto L34
        L33:
            r0 = 0
        L34:
            if (r0 == 0) goto L37
            goto L39
        L37:
            java.lang.String r0 = ""
        L39:
            r6 = r0
            goto L3c
        L3b:
            r6 = r14
        L3c:
            r0 = r17 & 16
            if (r0 == 0) goto L48
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            r7 = r0
            goto L49
        L48:
            r7 = r15
        L49:
            r0 = r17 & 32
            if (r0 == 0) goto L6c
            com.discord.widgets.user.WidgetPruneUsersViewModel$Companion r0 = com.discord.widgets.user.WidgetPruneUsersViewModel.Companion
            com.discord.stores.updates.ObservationDeck r1 = com.discord.stores.updates.ObservationDeckProvider.get()
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePermissions r3 = r2.getPermissions()
            com.discord.stores.StoreGuilds r2 = r2.getGuilds()
            r12 = r0
            r13 = r10
            r15 = r1
            r16 = r3
            r17 = r2
            r18 = r5
            rx.Observable r0 = r12.observeStoreState(r13, r15, r16, r17, r18)
            r8 = r0
            goto L6e
        L6c:
            r8 = r16
        L6e:
            r1 = r9
            r2 = r10
            r1.<init>(r2, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.WidgetPruneUsersViewModel.<init>(long, com.discord.stores.StoreGuilds, com.discord.stores.StoreUser, java.lang.String, com.discord.utilities.rest.RestAPI, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final ViewState.Loading getLoadingState() {
        return new ViewState.Loading(this.whichPruneDays, this.guildName);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void getUpdatedPruneCount() {
        updateViewState(getLoadingState());
        Observable L = ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getPruneCount(this.guildId, this.whichPruneDays.getCount()), false, 1, null).F(WidgetPruneUsersViewModel$getUpdatedPruneCount$1.INSTANCE).F(new b<Integer, ViewState>() { // from class: com.discord.widgets.user.WidgetPruneUsersViewModel$getUpdatedPruneCount$2
            public final WidgetPruneUsersViewModel.ViewState call(Integer num) {
                WidgetPruneUsersViewModel.PruneDays pruneDays;
                pruneDays = WidgetPruneUsersViewModel.this.whichPruneDays;
                m.checkNotNullExpressionValue(num, "count");
                return new WidgetPruneUsersViewModel.ViewState.Loaded(pruneDays, num.intValue(), false, 4, null);
            }
        }).s(new Action1<Throwable>() { // from class: com.discord.widgets.user.WidgetPruneUsersViewModel$getUpdatedPruneCount$3
            public final void call(Throwable th) {
                PublishSubject publishSubject;
                publishSubject = WidgetPruneUsersViewModel.this.eventSubject;
                m.checkNotNullExpressionValue(th, "it");
                publishSubject.k.onNext(new WidgetPruneUsersViewModel.Event.RestClientFailed(th));
            }
        }).L(WidgetPruneUsersViewModel$getUpdatedPruneCount$4.INSTANCE);
        m.checkNotNullExpressionValue(L, "restAPI.getPruneCount(gu…Failed(dismiss = false) }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(L, this, null, 2, null), WidgetPruneUsersViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetPruneUsersViewModel$getUpdatedPruneCount$5(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPruneUsersViewModel$getUpdatedPruneCount$6(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void pruneInProgress(boolean z2) {
        ViewState.Loaded copy$default;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (copy$default = ViewState.Loaded.copy$default(loaded, null, 0, z2, 3, null)) != null) {
            updateViewState(copy$default);
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void pruneClicked() {
        pruneInProgress(true);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.pruneMembers(this.guildId, new RestAPIParams.PruneGuild(Integer.valueOf(this.whichPruneDays.getCount()), Boolean.FALSE)), false, 1, null), this, null, 2, null), WidgetPruneUsersViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetPruneUsersViewModel$pruneClicked$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : new WidgetPruneUsersViewModel$pruneClicked$2(this), new WidgetPruneUsersViewModel$pruneClicked$1(this));
    }

    public final void pruneDaysSelected(PruneDays pruneDays) {
        m.checkNotNullParameter(pruneDays, "days");
        this.whichPruneDays = pruneDays;
        getUpdatedPruneCount();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetPruneUsersViewModel(long j, StoreGuilds storeGuilds, StoreUser storeUser, String str, RestAPI restAPI, Observable<StoreData> observable) {
        super(new ViewState.Loading(DEFAULT_DAYS, str));
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(str, "guildName");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observable, "storeDataObservable");
        this.guildId = j;
        this.storeGuilds = storeGuilds;
        this.storeUsers = storeUser;
        this.guildName = str;
        this.restAPI = restAPI;
        this.eventSubject = PublishSubject.k0();
        ViewState viewState = getViewState();
        Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.user.WidgetPruneUsersViewModel.ViewState.Loading");
        this.whichPruneDays = ((ViewState.Loading) viewState).getWhichPruneDays();
        Observable q = observable.F(AnonymousClass1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "storeDataObservable\n    …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), WidgetPruneUsersViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
