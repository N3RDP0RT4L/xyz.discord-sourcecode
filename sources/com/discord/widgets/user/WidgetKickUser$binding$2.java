package com.discord.widgets.user;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetKickUserBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetKickUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetKickUserBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetKickUserBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetKickUser$binding$2 extends k implements Function1<View, WidgetKickUserBinding> {
    public static final WidgetKickUser$binding$2 INSTANCE = new WidgetKickUser$binding$2();

    public WidgetKickUser$binding$2() {
        super(1, WidgetKickUserBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetKickUserBinding;", 0);
    }

    public final WidgetKickUserBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.kick_user_body;
        TextView textView = (TextView) view.findViewById(R.id.kick_user_body);
        if (textView != null) {
            i = R.id.kick_user_cancel;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.kick_user_cancel);
            if (materialButton != null) {
                i = R.id.kick_user_confirm;
                MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.kick_user_confirm);
                if (materialButton2 != null) {
                    i = R.id.kick_user_reason;
                    TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.kick_user_reason);
                    if (textInputLayout != null) {
                        i = R.id.kick_user_title;
                        TextView textView2 = (TextView) view.findViewById(R.id.kick_user_title);
                        if (textView2 != null) {
                            return new WidgetKickUserBinding((LinearLayout) view, textView, materialButton, materialButton2, textInputLayout, textView2);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
