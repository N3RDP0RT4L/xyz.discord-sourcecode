package com.discord.widgets.user;

import andhook.lib.HookHelper;
import android.content.Context;
import android.net.Uri;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.NotificationCompat;
import androidx.core.os.BundleKt;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.k.b;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.message.MessageReference;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetUserMentionsBinding;
import com.discord.databinding.WidgetUserMentionsFilterBinding;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreChat;
import com.discord.stores.StoreMessageReplies;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreTabsNavigation;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.embed.InviteEmbedModel;
import com.discord.utilities.fragment.FragmentExtensionsKt;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.LeadingEdgeThrottle;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.chat.list.ThreadSpineItemDecoration;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.LoadingEntry;
import com.discord.widgets.chat.list.entries.MentionFooterEntry;
import com.discord.widgets.chat.list.entries.MessageHeaderEntry;
import com.discord.widgets.chat.list.model.WidgetChatListModelMessages;
import com.discord.widgets.tabs.NavigationTab;
import com.discord.widgets.tabs.OnTabSelectedListener;
import com.discord.widgets.tabs.WidgetTabsHost;
import com.discord.widgets.user.WidgetUserMentions;
import com.discord.widgets.user.search.WidgetGlobalSearchDismissModel;
import d0.g0.t;
import d0.o;
import d0.t.h0;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.r;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function11;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Job;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetUserMentions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 ?2\u00020\u00012\u00020\u0002:\u0004?@ABB\u0007¢\u0006\u0004\b>\u0010\u001fJ\u0015\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0013\u0010\b\u001a\u00020\u0007*\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\u000f\u001a\u00020\u00072\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J#\u0010\u0015\u001a\u00020\u00142\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00070\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\u0007H\u0016¢\u0006\u0004\b \u0010\u001fJ\u000f\u0010!\u001a\u00020\u0007H\u0016¢\u0006\u0004\b!\u0010\u001fJ\u000f\u0010\"\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\"\u0010\u001fR\u001d\u0010%\u001a\u00020\n8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010\fR\u001d\u0010*\u001a\u00020&8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010$\u001a\u0004\b(\u0010)R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u001d\u00102\u001a\u00020.8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b/\u0010$\u001a\u0004\b0\u00101R\u001d\u00108\u001a\u0002038B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u0016\u0010:\u001a\u0002098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R\u0018\u0010<\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u0010=¨\u0006C"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/tabs/OnTabSelectedListener;", "Lrx/Observable;", "Lcom/discord/widgets/user/WidgetUserMentions$Model;", "observeModel", "()Lrx/Observable;", "", "configureUI", "(Lcom/discord/widgets/user/WidgetUserMentions$Model;)V", "", "isOnMentionsTab", "()Z", "", "guildName", "configureToolbar", "(Ljava/lang/String;)V", "Lkotlin/Function1;", "Lcom/discord/stores/StoreChat$InteractionState;", "onInteractionStateUpdated", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "createAdapter", "(Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", "addThreadSpineItemDecoration", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "onPause", "onDestroy", "onTabSelected", "isEmbedded$delegate", "Lkotlin/Lazy;", "isEmbedded", "Lcom/discord/widgets/user/search/WidgetGlobalSearchDismissModel;", "dismissViewModel$delegate", "getDismissViewModel", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchDismissModel;", "dismissViewModel", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;", "mentionsLoader", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;", "Lcom/discord/widgets/user/WidgetUserMentionsViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/user/WidgetUserMentionsViewModel;", "viewModel", "Lcom/discord/databinding/WidgetUserMentionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserMentionsBinding;", "binding", "Lcom/discord/stores/StoreTabsNavigation;", "storeTabsNavigation", "Lcom/discord/stores/StoreTabsNavigation;", "mentionsAdapter", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "UserMentionsAdapterEventHandler", "WidgetUserMentionFilter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserMentions extends AppFragment implements OnTabSelectedListener {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserMentions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserMentionsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_IS_EMBEDDED = "EXTRA_HIDE_TITLE";
    private WidgetChatListAdapter mentionsAdapter;
    private final Lazy viewModel$delegate;
    private final Lazy isEmbedded$delegate = FragmentExtensionsKt.booleanExtra$default(this, EXTRA_IS_EMBEDDED, false, 2, null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserMentions$binding$2.INSTANCE, null, 2, null);
    private final Model.MessageLoader mentionsLoader = new Model.MessageLoader(1000);
    private final StoreTabsNavigation storeTabsNavigation = StoreStream.Companion.getTabsNavigation();
    private final Lazy dismissViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGlobalSearchDismissModel.class), new WidgetUserMentions$$special$$inlined$activityViewModels$1(this), new WidgetUserMentions$$special$$inlined$activityViewModels$2(this));

    /* compiled from: WidgetUserMentions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$Companion;", "", "", "isEmbedded", "Lcom/discord/widgets/user/WidgetUserMentions;", "create", "(Z)Lcom/discord/widgets/user/WidgetUserMentions;", "", "EXTRA_IS_EMBEDDED", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ WidgetUserMentions create$default(Companion companion, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = false;
            }
            return companion.create(z2);
        }

        public final WidgetUserMentions create(boolean z2) {
            WidgetUserMentions widgetUserMentions = new WidgetUserMentions();
            widgetUserMentions.setArguments(BundleKt.bundleOf(o.to(WidgetUserMentions.EXTRA_IS_EMBEDDED, Boolean.valueOf(z2))));
            return widgetUserMentions;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserMentions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u001c\b\u0086\b\u0018\u0000 Q2\u00020\u0001:\u0002QRB¥\u0001\u0012\n\u0010$\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010%\u001a\u00060\u0002j\u0002`\u0006\u0012\b\u0010&\u001a\u0004\u0018\u00010\b\u0012\n\u0010'\u001a\u00060\u0002j\u0002`\u000b\u0012\u0016\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r\u0012\n\u0010)\u001a\u00060\u0002j\u0002`\u0011\u0012\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013\u0012\f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00020\u0017\u0012\n\u0010,\u001a\u00060\u0002j\u0002`\u0011\u0012\b\b\u0002\u0010-\u001a\u00020\u001b\u0012\u0006\u0010.\u001a\u00020\u001b\u0012\b\u0010/\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u00100\u001a\u00020!¢\u0006\u0004\bO\u0010PJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\u0002j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\u0005J \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\rHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0014\u0010\u0012\u001a\u00060\u0002j\u0002`\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0005J\u0016\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0016\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0014\u0010\u001a\u001a\u00060\u0002j\u0002`\u0011HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0005J\u0010\u0010\u001c\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001dJ\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\"\u001a\u00020!HÆ\u0003¢\u0006\u0004\b\"\u0010#JÆ\u0001\u00101\u001a\u00020\u00002\f\b\u0002\u0010$\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010%\u001a\u00060\u0002j\u0002`\u00062\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\b2\f\b\u0002\u0010'\u001a\u00060\u0002j\u0002`\u000b2\u0018\b\u0002\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r2\f\b\u0002\u0010)\u001a\u00060\u0002j\u0002`\u00112\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\u000e\b\u0002\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00020\u00172\f\b\u0002\u0010,\u001a\u00060\u0002j\u0002`\u00112\b\b\u0002\u0010-\u001a\u00020\u001b2\b\b\u0002\u0010.\u001a\u00020\u001b2\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u00100\u001a\u00020!HÆ\u0001¢\u0006\u0004\b1\u00102J\u0010\u00103\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b3\u0010 J\u0010\u00105\u001a\u000204HÖ\u0001¢\u0006\u0004\b5\u00106J\u001a\u00109\u001a\u00020\u001b2\b\u00108\u001a\u0004\u0018\u000107HÖ\u0003¢\u0006\u0004\b9\u0010:R\u001c\u0010-\u001a\u00020\u001b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b-\u0010;\u001a\u0004\b-\u0010\u001dR \u0010)\u001a\u00060\u0002j\u0002`\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b)\u0010<\u001a\u0004\b=\u0010\u0005R\u0019\u0010.\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010;\u001a\u0004\b>\u0010\u001dR\u001e\u0010&\u001a\u0004\u0018\u00010\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010?\u001a\u0004\b@\u0010\nR \u0010%\u001a\u00060\u0002j\u0002`\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u0010<\u001a\u0004\bA\u0010\u0005R\u0019\u00100\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010B\u001a\u0004\bC\u0010#R\"\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00140\u00138\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010D\u001a\u0004\bE\u0010\u0016R \u0010'\u001a\u00060\u0002j\u0002`\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u0010<\u001a\u0004\bF\u0010\u0005R \u0010$\u001a\u00060\u0002j\u0002`\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010<\u001a\u0004\bG\u0010\u0005R,\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b(\u0010H\u001a\u0004\bI\u0010\u0010R\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010J\u001a\u0004\bK\u0010 R\"\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00020\u00178\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b+\u0010L\u001a\u0004\bM\u0010\u0019R \u0010,\u001a\u00060\u0002j\u0002`\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b,\u0010<\u001a\u0004\bN\u0010\u0005¨\u0006S"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$Model;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/primitives/GuildId;", "component4", "", "", "component5", "()Ljava/util/Map;", "Lcom/discord/primitives/MessageId;", "component6", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "component7", "()Ljava/util/List;", "", "component8", "()Ljava/util/Set;", "component9", "", "component10", "()Z", "component11", "component12", "()Ljava/lang/String;", "Lcom/discord/widgets/tabs/NavigationTab;", "component13", "()Lcom/discord/widgets/tabs/NavigationTab;", "userId", "channelId", "guild", "guildId", "channelNames", "oldestMessageId", "list", "myRoleIds", "newMessagesMarkerMessageId", "isSpoilerClickAllowed", "animateEmojis", "guildName", "selectedTab", "copy", "(JJLcom/discord/models/guild/Guild;JLjava/util/Map;JLjava/util/List;Ljava/util/Set;JZZLjava/lang/String;Lcom/discord/widgets/tabs/NavigationTab;)Lcom/discord/widgets/user/WidgetUserMentions$Model;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "J", "getOldestMessageId", "getAnimateEmojis", "Lcom/discord/models/guild/Guild;", "getGuild", "getChannelId", "Lcom/discord/widgets/tabs/NavigationTab;", "getSelectedTab", "Ljava/util/List;", "getList", "getGuildId", "getUserId", "Ljava/util/Map;", "getChannelNames", "Ljava/lang/String;", "getGuildName", "Ljava/util/Set;", "getMyRoleIds", "getNewMessagesMarkerMessageId", HookHelper.constructorName, "(JJLcom/discord/models/guild/Guild;JLjava/util/Map;JLjava/util/List;Ljava/util/Set;JZZLjava/lang/String;Lcom/discord/widgets/tabs/NavigationTab;)V", "Companion", "MessageLoader", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model implements WidgetChatListAdapter.Data {
        public static final Companion Companion = new Companion(null);
        private final boolean animateEmojis;
        private final long channelId;
        private final Map<Long, String> channelNames;
        private final Guild guild;
        private final long guildId;
        private final String guildName;
        private final boolean isSpoilerClickAllowed;
        private final List<ChatListEntry> list;
        private final Set<Long> myRoleIds;
        private final long newMessagesMarkerMessageId;
        private final long oldestMessageId;
        private final NavigationTab selectedTab;
        private final long userId;

        /* compiled from: WidgetUserMentions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ#\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$Model$Companion;", "", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;", "messageLoader", "Lcom/discord/widgets/tabs/NavigationTab;", "selectedTab", "Lrx/Observable;", "Lcom/discord/widgets/user/WidgetUserMentions$Model;", "get", "(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;Lcom/discord/widgets/tabs/NavigationTab;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX WARN: Multi-variable type inference failed */
            public final Observable<Model> get(MessageLoader messageLoader, final NavigationTab navigationTab) {
                m.checkNotNullParameter(messageLoader, "messageLoader");
                m.checkNotNullParameter(navigationTab, "selectedTab");
                Observable<R> Y = messageLoader.getMentionsLoadingStateSubject().Y(new b<MessageLoader.LoadingState, Observable<? extends Model>>() { // from class: com.discord.widgets.user.WidgetUserMentions$Model$Companion$get$1

                    /* compiled from: WidgetUserMentions.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00042\u0016\u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\t0\u00042\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u0004\u0012\u00020\u00020\u00042&\u0010\u000f\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u00040\u00042*\u0010\u0013\u001a&\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0005j\u0002`\u0010\u0012\b\u0012\u00060\u0011j\u0002`\u00120\u00040\u00042\n\u0010\u0014\u001a\u00060\u0005j\u0002`\u00102\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u0018H\n¢\u0006\u0004\b\u001b\u0010\u001c"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "messagesWithMetadata", "Lcom/discord/models/guild/Guild;", "selectedGuild", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channels", "", "names", "Lcom/discord/primitives/GuildId;", "guilds", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "roles", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "members", "meUserId", "", "allowAnimatedEmojis", "autoPlayGifs", "Lcom/discord/utilities/embed/InviteEmbedModel;", "inviteEmbedModel", "Lcom/discord/widgets/user/WidgetUserMentions$Model;", "invoke", "(Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;JZZLcom/discord/utilities/embed/InviteEmbedModel;)Lcom/discord/widgets/user/WidgetUserMentions$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.user.WidgetUserMentions$Model$Companion$get$1$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1 extends d0.z.d.o implements Function11<WidgetChatListModelMessages.MessagesWithMetadata, Guild, Map<Long, ? extends Channel>, Map<Long, ? extends String>, Map<Long, ? extends Guild>, Map<Long, ? extends Map<Long, ? extends GuildRole>>, Map<Long, ? extends Map<Long, ? extends GuildMember>>, Long, Boolean, Boolean, InviteEmbedModel, WidgetUserMentions.Model> {
                        public final /* synthetic */ WidgetUserMentions.Model.MessageLoader.LoadingState $loadedState;

                        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                        public AnonymousClass1(WidgetUserMentions.Model.MessageLoader.LoadingState loadingState) {
                            super(11);
                            this.$loadedState = loadingState;
                        }

                        @Override // kotlin.jvm.functions.Function11
                        public /* bridge */ /* synthetic */ WidgetUserMentions.Model invoke(WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Guild guild, Map<Long, ? extends Channel> map, Map<Long, ? extends String> map2, Map<Long, ? extends Guild> map3, Map<Long, ? extends Map<Long, ? extends GuildRole>> map4, Map<Long, ? extends Map<Long, ? extends GuildMember>> map5, Long l, Boolean bool, Boolean bool2, InviteEmbedModel inviteEmbedModel) {
                            return invoke(messagesWithMetadata, guild, (Map<Long, Channel>) map, (Map<Long, String>) map2, (Map<Long, Guild>) map3, (Map<Long, ? extends Map<Long, GuildRole>>) map4, (Map<Long, ? extends Map<Long, GuildMember>>) map5, l.longValue(), bool.booleanValue(), bool2.booleanValue(), inviteEmbedModel);
                        }

                        public final WidgetUserMentions.Model invoke(WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Guild guild, Map<Long, Channel> map, Map<Long, String> map2, Map<Long, Guild> map3, Map<Long, ? extends Map<Long, GuildRole>> map4, Map<Long, ? extends Map<Long, GuildMember>> map5, long j, boolean z2, boolean z3, InviteEmbedModel inviteEmbedModel) {
                            Map<Long, Channel> map6 = map;
                            Map<Long, ? extends Map<Long, GuildRole>> map7 = map4;
                            Map<Long, ? extends Map<Long, GuildMember>> map8 = map5;
                            m.checkNotNullParameter(messagesWithMetadata, "messagesWithMetadata");
                            m.checkNotNullParameter(map6, "channels");
                            m.checkNotNullParameter(map2, "names");
                            m.checkNotNullParameter(map3, "guilds");
                            m.checkNotNullParameter(map7, "roles");
                            m.checkNotNullParameter(map8, "members");
                            m.checkNotNullParameter(inviteEmbedModel, "inviteEmbedModel");
                            long id2 = guild != null ? guild.getId() : 0L;
                            String name = guild != null ? guild.getName() : null;
                            ArrayList arrayList = new ArrayList(this.$loadedState.getMentions().size() * 2);
                            Set emptySet = n0.emptySet();
                            Map<Long, StoreMessageReplies.MessageState> allMessageReferences = StoreStream.Companion.getRepliedMessages().getAllMessageReferences();
                            for (Message message : this.$loadedState.getMentions()) {
                                Channel channel = map6.get(Long.valueOf(message.getChannelId()));
                                if (channel != null) {
                                    Map<Long, GuildMember> map9 = map8.get(Long.valueOf(message.getChannelId()));
                                    if (map9 == null) {
                                        map9 = h0.emptyMap();
                                    }
                                    Map<Long, GuildMember> map10 = map9;
                                    Object u0 = a.u0(channel, map7);
                                    if (u0 == null) {
                                        u0 = h0.emptyMap();
                                    }
                                    Map map11 = (Map) u0;
                                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                                    MessageReference messageReference = message.getMessageReference();
                                    Long c = messageReference != null ? messageReference.c() : null;
                                    if (c != null) {
                                        Object obj = (StoreMessageReplies.MessageState) allMessageReferences.get(c);
                                        if (obj == null) {
                                            obj = StoreMessageReplies.MessageState.Unloaded.INSTANCE;
                                        }
                                        linkedHashMap.put(c, obj);
                                    }
                                    arrayList.add(new MessageHeaderEntry(message, channel, (Guild) a.u0(channel, map3)));
                                    arrayList.addAll(WidgetChatListModelMessages.Companion.getMessageItems$default(WidgetChatListModelMessages.Companion, channel, map10, map11, h0.emptyMap(), messagesWithMetadata.getMessageThreads().get(Long.valueOf(message.getId())), messagesWithMetadata.getThreadCountsAndLatestMessages().get(Long.valueOf(message.getId())), message, messagesWithMetadata.getMessageState().get(Long.valueOf(message.getId())), linkedHashMap, false, false, null, z2, z3, StoreStream.Companion.getUserSettings().getIsRenderEmbedsEnabled(), j, false, new HashMap(), inviteEmbedModel, false, 524288, null));
                                }
                                map6 = map;
                                map7 = map4;
                                map8 = map5;
                            }
                            if (this.$loadedState.isLoading()) {
                                arrayList.add(new LoadingEntry());
                            } else if (this.$loadedState.isAllLoaded()) {
                                arrayList.add(new MentionFooterEntry());
                            }
                            return new WidgetUserMentions.Model(j, -1L, map3.get(Long.valueOf(id2)), id2, map2, -1L, arrayList, emptySet, -1L, false, z2, name, NavigationTab.this, 512, null);
                        }
                    }

                    public final Observable<? extends WidgetUserMentions.Model> call(WidgetUserMentions.Model.MessageLoader.LoadingState loadingState) {
                        Observable<WidgetChatListModelMessages.MessagesWithMetadata> observable = WidgetChatListModelMessages.MessagesWithMetadata.Companion.get(loadingState.getMentions());
                        StoreStream.Companion companion = StoreStream.Companion;
                        return ObservableCombineLatestOverloadsKt.combineLatest(observable, companion.getGuildSelected().observeSelectedGuild(), companion.getChannels().observeAllChannels(), companion.getChannels().observeNames(), companion.getGuilds().observeGuilds(), companion.getGuilds().observeRoles(), companion.getGuilds().observeComputed(), companion.getUsers().observeMeId(), StoreUserSettings.observeIsAnimatedEmojisEnabled$default(companion.getUserSettings(), false, 1, null), StoreUserSettings.observeIsAutoPlayGifsEnabled$default(companion.getUserSettings(), false, 1, null), InviteEmbedModel.Companion.observe$default(InviteEmbedModel.Companion, null, null, null, null, 15, null), new AnonymousClass1(loadingState));
                    }
                });
                Observable<Model> q = Observable.h0(new r(Y.j, new LeadingEdgeThrottle(300L, TimeUnit.MILLISECONDS, j0.p.a.a()))).q();
                m.checkNotNullExpressionValue(q, "messageLoader\n          …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetUserMentions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0002ABB\u000f\u0012\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b?\u0010@J\u001d\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\f\u001a\u00020\u00052\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00050\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u000eH\u0007¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0012H\u0007¢\u0006\u0004\b\u0014\u0010\u0015J%\u0010\u0019\u001a\u00020\u00052\u0014\b\u0002\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00170\u0016H\u0007¢\u0006\u0004\b\u0019\u0010\u001aR$\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\u00178\u0002@BX\u0082\u000e¢\u0006\f\n\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u0018\u0010!\u001a\u0004\u0018\u00010 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\"R\u0018\u0010#\u001a\u0004\u0018\u00010 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010\"R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010'R&\u0010*\u001a\u0012\u0012\u0004\u0012\u00020\u00030(j\b\u0012\u0004\u0012\u00020\u0003`)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u001f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00170,8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b2\u00103R\u0018\u00104\u001a\u0004\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b4\u00105R\u001e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000b\u00106R\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000f\u00107R*\u00109\u001a\u0002082\u0006\u0010\u001b\u001a\u0002088\u0006@GX\u0086\u000e¢\u0006\u0012\n\u0004\b9\u0010:\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>¨\u0006C"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;", "", "", "Lcom/discord/models/message/Message;", "messages", "", "handleLoaded", "(Ljava/util/List;)V", "handleLoadError", "()V", "Lkotlin/Function0;", "retryAction", "retry", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/stores/StoreChat$InteractionState;", "interactionState", "setInteractionState", "(Lcom/discord/stores/StoreChat$InteractionState;)V", "", "isFocused", "setIsFocused", "(Z)V", "Lkotlin/Function1;", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$LoadingState;", "loadingStateUpdater", "tryLoad", "(Lkotlin/jvm/functions/Function1;)V", "value", "mentionsLoadingState", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$LoadingState;", "setMentionsLoadingState", "(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$LoadingState;)V", "Lrx/Subscription;", "retrySubscription", "Lrx/Subscription;", "loadSubscription", "", "retryDelayMs", "J", "Z", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "mentions", "Ljava/util/ArrayList;", "Lrx/subjects/BehaviorSubject;", "mentionsLoadingStateSubject", "Lrx/subjects/BehaviorSubject;", "getMentionsLoadingStateSubject", "()Lrx/subjects/BehaviorSubject;", "", "mentionLimit", "I", "loadBeforeMessageId", "Ljava/lang/Long;", "Lkotlin/jvm/functions/Function0;", "Lcom/discord/stores/StoreChat$InteractionState;", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "filters", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "getFilters", "()Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "setFilters", "(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;)V", HookHelper.constructorName, "(J)V", "Filters", "LoadingState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MessageLoader {
            private StoreChat.InteractionState interactionState;
            private boolean isFocused;
            private Long loadBeforeMessageId;
            private Subscription loadSubscription;
            private final BehaviorSubject<LoadingState> mentionsLoadingStateSubject;
            private Function0<Unit> retryAction;
            private final long retryDelayMs;
            private Subscription retrySubscription;
            private final ArrayList<Message> mentions = new ArrayList<>();
            private final int mentionLimit = 25;
            private LoadingState mentionsLoadingState = new LoadingState(false, false, null, 7, null);
            private Filters filters = new Filters(0, false, false, false, 15, null);

            /* compiled from: WidgetUserMentions.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\f\b\u0002\u0010\u000b\u001a\u00060\u0002j\u0002`\u0003\u0012\b\b\u0002\u0010\f\u001a\u00020\u0006\u0012\b\b\u0002\u0010\r\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0006¢\u0006\u0004\b \u0010!J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\n\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ<\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\u000b\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\f\u001a\u00020\u00062\b\b\u0002\u0010\r\u001a\u00020\u00062\b\b\u0002\u0010\u000e\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0018\u001a\u00020\u00062\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\r\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\bR\u0019\u0010\u000e\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u001c\u0010\bR\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001d\u0010\bR\u001d\u0010\u000b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005¨\u0006\""}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Z", "component3", "component4", "guildId", "allGuilds", "includeEveryone", "includeRoles", "copy", "(JZZZ)Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getIncludeEveryone", "getIncludeRoles", "getAllGuilds", "J", "getGuildId", HookHelper.constructorName, "(JZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Filters {
                private final boolean allGuilds;
                private final long guildId;
                private final boolean includeEveryone;
                private final boolean includeRoles;

                public Filters() {
                    this(0L, false, false, false, 15, null);
                }

                public Filters(long j, boolean z2, boolean z3, boolean z4) {
                    this.guildId = j;
                    this.allGuilds = z2;
                    this.includeEveryone = z3;
                    this.includeRoles = z4;
                }

                public static /* synthetic */ Filters copy$default(Filters filters, long j, boolean z2, boolean z3, boolean z4, int i, Object obj) {
                    if ((i & 1) != 0) {
                        j = filters.guildId;
                    }
                    long j2 = j;
                    if ((i & 2) != 0) {
                        z2 = filters.allGuilds;
                    }
                    boolean z5 = z2;
                    if ((i & 4) != 0) {
                        z3 = filters.includeEveryone;
                    }
                    boolean z6 = z3;
                    if ((i & 8) != 0) {
                        z4 = filters.includeRoles;
                    }
                    return filters.copy(j2, z5, z6, z4);
                }

                public final long component1() {
                    return this.guildId;
                }

                public final boolean component2() {
                    return this.allGuilds;
                }

                public final boolean component3() {
                    return this.includeEveryone;
                }

                public final boolean component4() {
                    return this.includeRoles;
                }

                public final Filters copy(long j, boolean z2, boolean z3, boolean z4) {
                    return new Filters(j, z2, z3, z4);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof Filters)) {
                        return false;
                    }
                    Filters filters = (Filters) obj;
                    return this.guildId == filters.guildId && this.allGuilds == filters.allGuilds && this.includeEveryone == filters.includeEveryone && this.includeRoles == filters.includeRoles;
                }

                public final boolean getAllGuilds() {
                    return this.allGuilds;
                }

                public final long getGuildId() {
                    return this.guildId;
                }

                public final boolean getIncludeEveryone() {
                    return this.includeEveryone;
                }

                public final boolean getIncludeRoles() {
                    return this.includeRoles;
                }

                public int hashCode() {
                    int a = a0.a.a.b.a(this.guildId) * 31;
                    boolean z2 = this.allGuilds;
                    int i = 1;
                    if (z2) {
                        z2 = true;
                    }
                    int i2 = z2 ? 1 : 0;
                    int i3 = z2 ? 1 : 0;
                    int i4 = (a + i2) * 31;
                    boolean z3 = this.includeEveryone;
                    if (z3) {
                        z3 = true;
                    }
                    int i5 = z3 ? 1 : 0;
                    int i6 = z3 ? 1 : 0;
                    int i7 = (i4 + i5) * 31;
                    boolean z4 = this.includeRoles;
                    if (!z4) {
                        i = z4 ? 1 : 0;
                    }
                    return i7 + i;
                }

                public String toString() {
                    StringBuilder R = a.R("Filters(guildId=");
                    R.append(this.guildId);
                    R.append(", allGuilds=");
                    R.append(this.allGuilds);
                    R.append(", includeEveryone=");
                    R.append(this.includeEveryone);
                    R.append(", includeRoles=");
                    return a.M(R, this.includeRoles, ")");
                }

                public /* synthetic */ Filters(long j, boolean z2, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
                    this((i & 1) != 0 ? 0L : j, (i & 2) != 0 ? true : z2, (i & 4) != 0 ? true : z3, (i & 8) != 0 ? true : z4);
                }
            }

            /* compiled from: WidgetUserMentions.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\b\b\u0002\u0010\n\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0002\u0012\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ4\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0016\u001a\u00020\u00022\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\tR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u000b\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\n\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$LoadingState;", "", "", "component1", "()Z", "component2", "", "Lcom/discord/models/message/Message;", "component3", "()Ljava/util/List;", "isLoading", "isAllLoaded", "mentions", "copy", "(ZZLjava/util/List;)Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$LoadingState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getMentions", "Z", HookHelper.constructorName, "(ZZLjava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class LoadingState {
                private final boolean isAllLoaded;
                private final boolean isLoading;
                private final List<Message> mentions;

                public LoadingState() {
                    this(false, false, null, 7, null);
                }

                public LoadingState(boolean z2, boolean z3, List<Message> list) {
                    m.checkNotNullParameter(list, "mentions");
                    this.isLoading = z2;
                    this.isAllLoaded = z3;
                    this.mentions = list;
                }

                /* JADX WARN: Multi-variable type inference failed */
                public static /* synthetic */ LoadingState copy$default(LoadingState loadingState, boolean z2, boolean z3, List list, int i, Object obj) {
                    if ((i & 1) != 0) {
                        z2 = loadingState.isLoading;
                    }
                    if ((i & 2) != 0) {
                        z3 = loadingState.isAllLoaded;
                    }
                    if ((i & 4) != 0) {
                        list = loadingState.mentions;
                    }
                    return loadingState.copy(z2, z3, list);
                }

                public final boolean component1() {
                    return this.isLoading;
                }

                public final boolean component2() {
                    return this.isAllLoaded;
                }

                public final List<Message> component3() {
                    return this.mentions;
                }

                public final LoadingState copy(boolean z2, boolean z3, List<Message> list) {
                    m.checkNotNullParameter(list, "mentions");
                    return new LoadingState(z2, z3, list);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof LoadingState)) {
                        return false;
                    }
                    LoadingState loadingState = (LoadingState) obj;
                    return this.isLoading == loadingState.isLoading && this.isAllLoaded == loadingState.isAllLoaded && m.areEqual(this.mentions, loadingState.mentions);
                }

                public final List<Message> getMentions() {
                    return this.mentions;
                }

                public int hashCode() {
                    boolean z2 = this.isLoading;
                    int i = 1;
                    if (z2) {
                        z2 = true;
                    }
                    int i2 = z2 ? 1 : 0;
                    int i3 = z2 ? 1 : 0;
                    int i4 = i2 * 31;
                    boolean z3 = this.isAllLoaded;
                    if (!z3) {
                        i = z3 ? 1 : 0;
                    }
                    int i5 = (i4 + i) * 31;
                    List<Message> list = this.mentions;
                    return i5 + (list != null ? list.hashCode() : 0);
                }

                public final boolean isAllLoaded() {
                    return this.isAllLoaded;
                }

                public final boolean isLoading() {
                    return this.isLoading;
                }

                public String toString() {
                    StringBuilder R = a.R("LoadingState(isLoading=");
                    R.append(this.isLoading);
                    R.append(", isAllLoaded=");
                    R.append(this.isAllLoaded);
                    R.append(", mentions=");
                    return a.K(R, this.mentions, ")");
                }

                public /* synthetic */ LoadingState(boolean z2, boolean z3, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
                    this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? false : z3, (i & 4) != 0 ? n.emptyList() : list);
                }
            }

            public MessageLoader(long j) {
                this.retryDelayMs = j;
                BehaviorSubject<LoadingState> l0 = BehaviorSubject.l0(this.mentionsLoadingState);
                m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(mentionsLoadingState)");
                this.mentionsLoadingStateSubject = l0;
            }

            /* JADX INFO: Access modifiers changed from: private */
            @MainThread
            public final void handleLoadError() {
                retry(new WidgetUserMentions$Model$MessageLoader$handleLoadError$1(this));
            }

            /* JADX INFO: Access modifiers changed from: private */
            @MainThread
            public final void handleLoaded(List<Message> list) {
                boolean z2 = list.isEmpty() || list.size() < this.mentionLimit;
                Message message = (Message) u.lastOrNull((List<? extends Object>) list);
                this.loadBeforeMessageId = message != null ? Long.valueOf(message.getId()) : null;
                this.mentions.addAll(list);
                setMentionsLoadingState(LoadingState.copy$default(this.mentionsLoadingState, false, false, new ArrayList(this.mentions), 3, null));
                retry(new WidgetUserMentions$Model$MessageLoader$handleLoaded$1(this, z2));
            }

            private final void retry(Function0<Unit> function0) {
                Subscription subscription = this.retrySubscription;
                if (subscription != null) {
                    subscription.unsubscribe();
                }
                this.retryAction = function0;
                Observable<Long> d02 = Observable.d0(this.retryDelayMs, TimeUnit.MILLISECONDS);
                m.checkNotNullExpressionValue(d02, "Observable.timer(retryDe…s, TimeUnit.MILLISECONDS)");
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(d02), MessageLoader.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetUserMentions$Model$MessageLoader$retry$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserMentions$Model$MessageLoader$retry$2(function0));
            }

            private final void setMentionsLoadingState(LoadingState loadingState) {
                if (!m.areEqual(this.mentionsLoadingState, loadingState)) {
                    this.mentionsLoadingState = loadingState;
                    this.mentionsLoadingStateSubject.onNext(loadingState);
                }
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ void tryLoad$default(MessageLoader messageLoader, Function1 function1, int i, Object obj) {
                if ((i & 1) != 0) {
                    function1 = WidgetUserMentions$Model$MessageLoader$tryLoad$1.INSTANCE;
                }
                messageLoader.tryLoad(function1);
            }

            public final Filters getFilters() {
                return this.filters;
            }

            public final BehaviorSubject<LoadingState> getMentionsLoadingStateSubject() {
                return this.mentionsLoadingStateSubject;
            }

            @MainThread
            public final void setFilters(Filters filters) {
                m.checkNotNullParameter(filters, "value");
                if (!m.areEqual(this.filters, filters)) {
                    this.filters = filters;
                    Subscription subscription = this.loadSubscription;
                    if (subscription != null) {
                        subscription.unsubscribe();
                    }
                    this.loadBeforeMessageId = null;
                    this.mentions.clear();
                    tryLoad(WidgetUserMentions$Model$MessageLoader$filters$1.INSTANCE);
                }
            }

            @MainThread
            public final void setInteractionState(StoreChat.InteractionState interactionState) {
                m.checkNotNullParameter(interactionState, "interactionState");
                this.interactionState = interactionState;
                tryLoad$default(this, null, 1, null);
            }

            @MainThread
            public final void setIsFocused(boolean z2) {
                this.isFocused = z2;
                tryLoad$default(this, null, 1, null);
            }

            @MainThread
            public final void tryLoad(Function1<? super LoadingState, LoadingState> function1) {
                StoreChat.InteractionState interactionState;
                m.checkNotNullParameter(function1, "loadingStateUpdater");
                setMentionsLoadingState(function1.invoke(this.mentionsLoadingState));
                if (!this.mentionsLoadingState.isLoading() && !this.mentionsLoadingState.isAllLoaded() && (interactionState = this.interactionState) != null && interactionState.isAtTopIgnoringTouch() && this.isFocused) {
                    setMentionsLoadingState(LoadingState.copy$default(this.mentionsLoadingState, true, false, null, 6, null));
                    Observable F = ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getMentions(this.mentionLimit, this.filters.getIncludeRoles(), this.filters.getIncludeEveryone(), Long.valueOf(this.filters.getAllGuilds() ? 0L : this.filters.getGuildId()), this.loadBeforeMessageId), false, 1, null)).F(WidgetUserMentions$Model$MessageLoader$tryLoad$2.INSTANCE);
                    m.checkNotNullExpressionValue(F, "RestAPI\n            .api…messages.map(::Message) }");
                    ObservableExtensionsKt.appSubscribe(F, MessageLoader.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetUserMentions$Model$MessageLoader$tryLoad$5(this), (r18 & 8) != 0 ? null : new WidgetUserMentions$Model$MessageLoader$tryLoad$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserMentions$Model$MessageLoader$tryLoad$4(this));
                }
            }
        }

        public Model(long j, long j2, Guild guild, long j3, Map<Long, String> map, long j4, List<ChatListEntry> list, Set<Long> set, long j5, boolean z2, boolean z3, String str, NavigationTab navigationTab) {
            m.checkNotNullParameter(map, "channelNames");
            m.checkNotNullParameter(list, "list");
            m.checkNotNullParameter(set, "myRoleIds");
            m.checkNotNullParameter(navigationTab, "selectedTab");
            this.userId = j;
            this.channelId = j2;
            this.guild = guild;
            this.guildId = j3;
            this.channelNames = map;
            this.oldestMessageId = j4;
            this.list = list;
            this.myRoleIds = set;
            this.newMessagesMarkerMessageId = j5;
            this.isSpoilerClickAllowed = z2;
            this.animateEmojis = z3;
            this.guildName = str;
            this.selectedTab = navigationTab;
        }

        public final long component1() {
            return getUserId();
        }

        public final boolean component10() {
            return isSpoilerClickAllowed();
        }

        public final boolean component11() {
            return this.animateEmojis;
        }

        public final String component12() {
            return this.guildName;
        }

        public final NavigationTab component13() {
            return this.selectedTab;
        }

        public final long component2() {
            return getChannelId();
        }

        public final Guild component3() {
            return getGuild();
        }

        public final long component4() {
            return getGuildId();
        }

        public final Map<Long, String> component5() {
            return getChannelNames();
        }

        public final long component6() {
            return getOldestMessageId();
        }

        public final List<ChatListEntry> component7() {
            return getList();
        }

        public final Set<Long> component8() {
            return getMyRoleIds();
        }

        public final long component9() {
            return getNewMessagesMarkerMessageId();
        }

        public final Model copy(long j, long j2, Guild guild, long j3, Map<Long, String> map, long j4, List<ChatListEntry> list, Set<Long> set, long j5, boolean z2, boolean z3, String str, NavigationTab navigationTab) {
            m.checkNotNullParameter(map, "channelNames");
            m.checkNotNullParameter(list, "list");
            m.checkNotNullParameter(set, "myRoleIds");
            m.checkNotNullParameter(navigationTab, "selectedTab");
            return new Model(j, j2, guild, j3, map, j4, list, set, j5, z2, z3, str, navigationTab);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return getUserId() == model.getUserId() && getChannelId() == model.getChannelId() && m.areEqual(getGuild(), model.getGuild()) && getGuildId() == model.getGuildId() && m.areEqual(getChannelNames(), model.getChannelNames()) && getOldestMessageId() == model.getOldestMessageId() && m.areEqual(getList(), model.getList()) && m.areEqual(getMyRoleIds(), model.getMyRoleIds()) && getNewMessagesMarkerMessageId() == model.getNewMessagesMarkerMessageId() && isSpoilerClickAllowed() == model.isSpoilerClickAllowed() && this.animateEmojis == model.animateEmojis && m.areEqual(this.guildName, model.guildName) && m.areEqual(this.selectedTab, model.selectedTab);
        }

        public final boolean getAnimateEmojis() {
            return this.animateEmojis;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getChannelId() {
            return this.channelId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Map<Long, String> getChannelNames() {
            return this.channelNames;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Guild getGuild() {
            return this.guild;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getGuildId() {
            return this.guildId;
        }

        public final String getGuildName() {
            return this.guildName;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public List<ChatListEntry> getList() {
            return this.list;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Set<Long> getMyRoleIds() {
            return this.myRoleIds;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getNewMessagesMarkerMessageId() {
            return this.newMessagesMarkerMessageId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getOldestMessageId() {
            return this.oldestMessageId;
        }

        public final NavigationTab getSelectedTab() {
            return this.selectedTab;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getUserId() {
            return this.userId;
        }

        public int hashCode() {
            int a = (a0.a.a.b.a(getChannelId()) + (a0.a.a.b.a(getUserId()) * 31)) * 31;
            Guild guild = getGuild();
            int i = 0;
            int a2 = (a0.a.a.b.a(getGuildId()) + ((a + (guild != null ? guild.hashCode() : 0)) * 31)) * 31;
            Map<Long, String> channelNames = getChannelNames();
            int a3 = (a0.a.a.b.a(getOldestMessageId()) + ((a2 + (channelNames != null ? channelNames.hashCode() : 0)) * 31)) * 31;
            List<ChatListEntry> list = getList();
            int hashCode = (a3 + (list != null ? list.hashCode() : 0)) * 31;
            Set<Long> myRoleIds = getMyRoleIds();
            int a4 = (a0.a.a.b.a(getNewMessagesMarkerMessageId()) + ((hashCode + (myRoleIds != null ? myRoleIds.hashCode() : 0)) * 31)) * 31;
            boolean isSpoilerClickAllowed = isSpoilerClickAllowed();
            int i2 = 1;
            if (isSpoilerClickAllowed) {
                isSpoilerClickAllowed = true;
            }
            int i3 = isSpoilerClickAllowed ? 1 : 0;
            int i4 = isSpoilerClickAllowed ? 1 : 0;
            int i5 = (a4 + i3) * 31;
            boolean z2 = this.animateEmojis;
            if (!z2) {
                i2 = z2 ? 1 : 0;
            }
            int i6 = (i5 + i2) * 31;
            String str = this.guildName;
            int hashCode2 = (i6 + (str != null ? str.hashCode() : 0)) * 31;
            NavigationTab navigationTab = this.selectedTab;
            if (navigationTab != null) {
                i = navigationTab.hashCode();
            }
            return hashCode2 + i;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public boolean isSpoilerClickAllowed() {
            return this.isSpoilerClickAllowed;
        }

        public String toString() {
            StringBuilder R = a.R("Model(userId=");
            R.append(getUserId());
            R.append(", channelId=");
            R.append(getChannelId());
            R.append(", guild=");
            R.append(getGuild());
            R.append(", guildId=");
            R.append(getGuildId());
            R.append(", channelNames=");
            R.append(getChannelNames());
            R.append(", oldestMessageId=");
            R.append(getOldestMessageId());
            R.append(", list=");
            R.append(getList());
            R.append(", myRoleIds=");
            R.append(getMyRoleIds());
            R.append(", newMessagesMarkerMessageId=");
            R.append(getNewMessagesMarkerMessageId());
            R.append(", isSpoilerClickAllowed=");
            R.append(isSpoilerClickAllowed());
            R.append(", animateEmojis=");
            R.append(this.animateEmojis);
            R.append(", guildName=");
            R.append(this.guildName);
            R.append(", selectedTab=");
            R.append(this.selectedTab);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ Model(long j, long j2, Guild guild, long j3, Map map, long j4, List list, Set set, long j5, boolean z2, boolean z3, String str, NavigationTab navigationTab, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(j, j2, guild, j3, map, j4, list, set, j5, (i & 512) != 0 ? false : z2, z3, str, navigationTab);
        }
    }

    /* compiled from: WidgetUserMentions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010P\u001a\u00020\u000b\u0012\u0006\u0010N\u001a\u00020M\u0012\u0012\u0010S\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040R¢\u0006\u0004\bU\u0010VJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\r\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J'\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0019\u001a\u00060\u0017j\u0002`\u0018H\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ#\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0019\u001a\u00060\u0017j\u0002`\u0018H\u0016¢\u0006\u0004\b\u001c\u0010\u001bJ'\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u000e\u0010\u0019\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`\u0018H\u0016¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u001f\u0010\u0006J#\u0010#\u001a\u00020\u00042\n\u0010!\u001a\u00060\u0017j\u0002` 2\u0006\u0010\"\u001a\u00020\u0017H\u0016¢\u0006\u0004\b#\u0010$JO\u0010,\u001a\u00020\u00042\n\u0010\u0019\u001a\u00060\u0017j\u0002`\u00182\n\u0010&\u001a\u00060\u0017j\u0002`%2\n\u0010!\u001a\u00060\u0017j\u0002` 2\n\u0010(\u001a\u00060\u0017j\u0002`'2\u0006\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020\u000bH\u0016¢\u0006\u0004\b,\u0010-J;\u0010.\u001a\u00020\u00042\n\u0010\u0019\u001a\u00060\u0017j\u0002`\u00182\n\u0010!\u001a\u00060\u0017j\u0002` 2\n\u0010(\u001a\u00060\u0017j\u0002`'2\u0006\u0010*\u001a\u00020)H\u0016¢\u0006\u0004\b.\u0010/J?\u00100\u001a\u00020\u00042\n\u0010\u0019\u001a\u00060\u0017j\u0002`\u00182\n\u0010&\u001a\u00060\u0017j\u0002`%2\n\u0010!\u001a\u00060\u0017j\u0002` 2\n\u0010(\u001a\u00060\u0017j\u0002`'H\u0016¢\u0006\u0004\b0\u00101J\u001f\u00106\u001a\u00020\u000b2\u0006\u00103\u001a\u0002022\u0006\u00105\u001a\u000204H\u0016¢\u0006\u0004\b6\u00107J\u0017\u00108\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b8\u0010\u0006JK\u0010@\u001a\u00020\u00042\n\u00109\u001a\u00060\u0017j\u0002`%2\n\u0010!\u001a\u00060\u0017j\u0002` 2\n\u0010(\u001a\u00060\u0017j\u0002`'2\u0006\u0010;\u001a\u00020:2\u0006\u0010=\u001a\u00020<2\u0006\u0010?\u001a\u00020>H\u0016¢\u0006\u0004\b@\u0010AJ\u000f\u0010B\u001a\u00020\u0004H\u0016¢\u0006\u0004\bB\u0010CJ#\u0010G\u001a\u00020\u00042\n\u0010D\u001a\u00060\u0017j\u0002` 2\u0006\u0010F\u001a\u00020EH\u0016¢\u0006\u0004\bG\u0010HJ\u001f\u0010K\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010J\u001a\u00020IH\u0016¢\u0006\u0004\bK\u0010LR\u0016\u0010N\u001a\u00020M8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u0016\u0010P\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bP\u0010QR\"\u0010S\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040R8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010T¨\u0006W"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$UserMentionsAdapterEventHandler;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "Lcom/discord/models/message/Message;", "message", "", "jumpToMessage", "(Lcom/discord/models/message/Message;)V", "Lcom/discord/stores/StoreChat$InteractionState;", "interactionState", "onInteractionStateUpdated", "(Lcom/discord/stores/StoreChat$InteractionState;)V", "", "isThreadStarterMessage", "onMessageClicked", "(Lcom/discord/models/message/Message;Z)V", "Lcom/discord/api/channel/Channel;", "channel", "onThreadClicked", "(Lcom/discord/api/channel/Channel;)V", "", "formattedMessage", "onMessageLongClicked", "(Lcom/discord/models/message/Message;Ljava/lang/CharSequence;Z)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "onMessageAuthorNameClicked", "(Lcom/discord/models/message/Message;J)V", "onMessageAuthorAvatarClicked", "onMessageAuthorLongClicked", "(Lcom/discord/models/message/Message;Ljava/lang/Long;)V", "onMessageBlockedGroupClicked", "Lcom/discord/primitives/ChannelId;", "channelId", "oldestMessageId", "onOldestMessageId", "(JJ)V", "Lcom/discord/primitives/UserId;", "myUserId", "Lcom/discord/primitives/MessageId;", "messageId", "Lcom/discord/api/message/reaction/MessageReaction;", "reaction", "canAddReactions", "onReactionClicked", "(JJJJLcom/discord/api/message/reaction/MessageReaction;Z)V", "onReactionLongClicked", "(JJJLcom/discord/api/message/reaction/MessageReaction;)V", "onQuickAddReactionClicked", "(JJJJ)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "fileName", "onQuickDownloadClicked", "(Landroid/net/Uri;Ljava/lang/String;)Z", "onOpenPinsClicked", "authorId", "Lcom/discord/api/message/activity/MessageActivityType;", "actionType", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/api/application/Application;", "application", "onUserActivityAction", "(JJJLcom/discord/api/message/activity/MessageActivityType;Lcom/discord/api/activity/Activity;Lcom/discord/api/application/Application;)V", "onListClicked", "()V", "voiceChannelId", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;", "callStatus", "onCallMessageClicked", "(JLcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;)V", "Lcom/discord/api/sticker/BaseSticker;", "sticker", "onStickerClicked", "(Lcom/discord/models/message/Message;Lcom/discord/api/sticker/BaseSticker;)V", "Lcom/discord/utilities/channel/ChannelSelector;", "channelSelector", "Lcom/discord/utilities/channel/ChannelSelector;", "isEmbedded", "Z", "Lkotlin/Function1;", "interactionStateUpdated", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(ZLcom/discord/utilities/channel/ChannelSelector;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserMentionsAdapterEventHandler implements WidgetChatListAdapter.EventHandler {
        private final ChannelSelector channelSelector;
        private final Function1<StoreChat.InteractionState, Unit> interactionStateUpdated;
        private final boolean isEmbedded;

        /* JADX WARN: Multi-variable type inference failed */
        public UserMentionsAdapterEventHandler(boolean z2, ChannelSelector channelSelector, Function1<? super StoreChat.InteractionState, Unit> function1) {
            m.checkNotNullParameter(channelSelector, "channelSelector");
            m.checkNotNullParameter(function1, "interactionStateUpdated");
            this.isEmbedded = z2;
            this.channelSelector = channelSelector;
            this.interactionStateUpdated = function1;
        }

        private final void jumpToMessage(Message message) {
            StoreStream.Companion companion = StoreStream.Companion;
            companion.getMessagesLoader().jumpToMessage(message.getChannelId(), message.getId());
            companion.getTabsNavigation().selectHomeTab(StoreNavigation.PanelAction.CLOSE, this.isEmbedded);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onBotUiComponentClicked(long j, Long l, long j2, long j3, Long l2, int i, RestAPIParams.ComponentInteractionData componentInteractionData) {
            m.checkNotNullParameter(componentInteractionData, "componentSendData");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onBotUiComponentClicked(this, j, l, j2, j3, l2, i, componentInteractionData);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onCallMessageClicked(long j, WidgetChatListAdapterItemCallMessage.CallStatus callStatus) {
            m.checkNotNullParameter(callStatus, "callStatus");
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onCommandClicked(long j, Long l, long j2, long j3, long j4, long j5, String str) {
            WidgetChatListAdapter.EventHandler.DefaultImpls.onCommandClicked(this, j, l, j2, j3, j4, j5, str);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onDismissClicked(Message message) {
            m.checkNotNullParameter(message, "message");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onDismissClicked(this, message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onInteractionStateUpdated(StoreChat.InteractionState interactionState) {
            m.checkNotNullParameter(interactionState, "interactionState");
            this.interactionStateUpdated.invoke(interactionState);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onListClicked() {
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageAuthorAvatarClicked(Message message, long j) {
            m.checkNotNullParameter(message, "message");
            jumpToMessage(message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageAuthorLongClicked(Message message, Long l) {
            m.checkNotNullParameter(message, "message");
            jumpToMessage(message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageAuthorNameClicked(Message message, long j) {
            m.checkNotNullParameter(message, "message");
            jumpToMessage(message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageBlockedGroupClicked(Message message) {
            m.checkNotNullParameter(message, "message");
            jumpToMessage(message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageClicked(Message message, boolean z2) {
            m.checkNotNullParameter(message, "message");
            jumpToMessage(message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageLongClicked(Message message, CharSequence charSequence, boolean z2) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(charSequence, "formattedMessage");
            jumpToMessage(message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onOldestMessageId(long j, long j2) {
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onOpenPinsClicked(Message message) {
            m.checkNotNullParameter(message, "message");
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onQuickAddReactionClicked(long j, long j2, long j3, long j4) {
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public boolean onQuickDownloadClicked(Uri uri, String str) {
            m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            m.checkNotNullParameter(str, "fileName");
            return false;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onReactionClicked(long j, long j2, long j3, long j4, MessageReaction messageReaction, boolean z2) {
            m.checkNotNullParameter(messageReaction, "reaction");
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onReactionLongClicked(long j, long j2, long j3, MessageReaction messageReaction) {
            m.checkNotNullParameter(messageReaction, "reaction");
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onSendGreetMessageClicked(long j, int i, Sticker sticker) {
            m.checkNotNullParameter(sticker, "sticker");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onSendGreetMessageClicked(this, j, i, sticker);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onShareButtonClick(GuildScheduledEvent guildScheduledEvent, WeakReference<Context> weakReference, WeakReference<AppFragment> weakReference2) {
            m.checkNotNullParameter(guildScheduledEvent, "guildEvent");
            m.checkNotNullParameter(weakReference, "weakContext");
            m.checkNotNullParameter(weakReference2, "weakFragment");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onShareButtonClick(this, guildScheduledEvent, weakReference, weakReference2);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onStickerClicked(Message message, BaseSticker baseSticker) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(baseSticker, "sticker");
            StoreStream.Companion.getMessagesLoader().jumpToMessage(message.getChannelId(), message.getId());
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onThreadClicked(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            ChannelSelector.selectChannel$default(this.channelSelector, channel, null, SelectedChannelAnalyticsLocation.EMBED, 2, null);
            StoreStream.Companion.getTabsNavigation().selectHomeTab(StoreNavigation.PanelAction.CLOSE, this.isEmbedded);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onThreadLongClicked(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onThreadLongClicked(this, channel);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onUrlLongClicked(String str) {
            m.checkNotNullParameter(str, "url");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onUrlLongClicked(this, str);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onUserActivityAction(long j, long j2, long j3, MessageActivityType messageActivityType, Activity activity, Application application) {
            m.checkNotNullParameter(messageActivityType, "actionType");
            m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            m.checkNotNullParameter(application, "application");
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onUserMentionClicked(long j, long j2, long j3) {
            WidgetChatListAdapter.EventHandler.DefaultImpls.onUserMentionClicked(this, j, j2, j3);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onWelcomeCtaClicked(Message message, Channel channel, BaseSticker baseSticker) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(baseSticker, "sticker");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onWelcomeCtaClicked(this, message, channel, baseSticker);
        }
    }

    /* compiled from: WidgetUserMentions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u0007¢\u0006\u0004\b\u001d\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0011\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u000eR\u0018\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0003\u0010\u0019R\"\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001a8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "filters", "", "updateFilters", "(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;)V", "Lkotlinx/coroutines/Job;", "delayedDismiss", "()Lkotlinx/coroutines/Job;", "", "getContentViewResId", "()I", "onResume", "()V", "onPause", "", "guildName", "Ljava/lang/String;", "Lcom/discord/databinding/WidgetUserMentionsFilterBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserMentionsFilterBinding;", "binding", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "Lkotlin/Function1;", "onFiltersUpdated", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class WidgetUserMentionFilter extends AppBottomSheet {
        public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserMentionFilter.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserMentionsFilterBinding;", 0)};
        public static final Companion Companion = new Companion(null);
        private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserMentions$WidgetUserMentionFilter$binding$2.INSTANCE, null, 2, null);
        private Model.MessageLoader.Filters filters;
        private String guildName;
        private Function1<? super Model.MessageLoader.Filters, Unit> onFiltersUpdated;

        /* compiled from: WidgetUserMentions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ;\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "guildName", "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "filters", "Lkotlin/Function1;", "", "onFiltersUpdated", "Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter;", "show", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final WidgetUserMentionFilter show(FragmentManager fragmentManager, String str, Model.MessageLoader.Filters filters, Function1<? super Model.MessageLoader.Filters, Unit> function1) {
                m.checkNotNullParameter(fragmentManager, "fragmentManager");
                m.checkNotNullParameter(filters, "filters");
                m.checkNotNullParameter(function1, "onFiltersUpdated");
                WidgetUserMentionFilter widgetUserMentionFilter = new WidgetUserMentionFilter();
                widgetUserMentionFilter.onFiltersUpdated = function1;
                widgetUserMentionFilter.filters = filters;
                widgetUserMentionFilter.guildName = str;
                widgetUserMentionFilter.show(fragmentManager, WidgetUserMentionFilter.class.getName());
                return widgetUserMentionFilter;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public WidgetUserMentionFilter() {
            super(false, 1, null);
        }

        public static final /* synthetic */ Model.MessageLoader.Filters access$getFilters$p(WidgetUserMentionFilter widgetUserMentionFilter) {
            Model.MessageLoader.Filters filters = widgetUserMentionFilter.filters;
            if (filters == null) {
                m.throwUninitializedPropertyAccessException("filters");
            }
            return filters;
        }

        public static final /* synthetic */ Function1 access$getOnFiltersUpdated$p(WidgetUserMentionFilter widgetUserMentionFilter) {
            Function1<? super Model.MessageLoader.Filters, Unit> function1 = widgetUserMentionFilter.onFiltersUpdated;
            if (function1 == null) {
                m.throwUninitializedPropertyAccessException("onFiltersUpdated");
            }
            return function1;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Job delayedDismiss() {
            CoroutineScope coroutineScope;
            View view = getView();
            if (view == null || (coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(view)) == null) {
                return null;
            }
            return f.H0(coroutineScope, null, null, new WidgetUserMentions$WidgetUserMentionFilter$delayedDismiss$1(this, null), 3, null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final WidgetUserMentionsFilterBinding getBinding() {
            return (WidgetUserMentionsFilterBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void updateFilters(Model.MessageLoader.Filters filters) {
            this.filters = filters;
            Function1<? super Model.MessageLoader.Filters, Unit> function1 = this.onFiltersUpdated;
            if (function1 == null) {
                m.throwUninitializedPropertyAccessException("onFiltersUpdated");
            }
            function1.invoke(filters);
        }

        @Override // com.discord.app.AppBottomSheet
        public int getContentViewResId() {
            return R.layout.widget_user_mentions_filter;
        }

        @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
        public void onPause() {
            dismissAllowingStateLoss();
            super.onPause();
        }

        @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
        public void onResume() {
            super.onResume();
            CheckedSetting checkedSetting = getBinding().d;
            m.checkNotNullExpressionValue(checkedSetting, "binding.userMentionsFilterThisServer");
            Model.MessageLoader.Filters filters = this.filters;
            if (filters == null) {
                m.throwUninitializedPropertyAccessException("filters");
            }
            checkedSetting.setChecked(!filters.getAllGuilds());
            String str = this.guildName;
            if (!(str == null || t.isBlank(str))) {
                CheckedSetting checkedSetting2 = getBinding().d;
                m.checkNotNullExpressionValue(checkedSetting2, "binding.userMentionsFilterThisServer");
                checkedSetting2.setVisibility(0);
                CheckedSetting checkedSetting3 = getBinding().d;
                Context context = getContext();
                CharSequence charSequence = null;
                if (context != null) {
                    charSequence = b.a.k.b.b(context, R.string.this_server_named, new Object[]{this.guildName}, (r4 & 4) != 0 ? b.C0034b.j : null);
                }
                checkedSetting3.setText(charSequence);
                getBinding().d.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.user.WidgetUserMentions$WidgetUserMentionFilter$onResume$1
                    public final void call(Boolean bool) {
                        WidgetUserMentionsFilterBinding binding;
                        WidgetUserMentions.WidgetUserMentionFilter widgetUserMentionFilter = WidgetUserMentions.WidgetUserMentionFilter.this;
                        WidgetUserMentions.Model.MessageLoader.Filters access$getFilters$p = WidgetUserMentions.WidgetUserMentionFilter.access$getFilters$p(widgetUserMentionFilter);
                        binding = WidgetUserMentions.WidgetUserMentionFilter.this.getBinding();
                        CheckedSetting checkedSetting4 = binding.d;
                        m.checkNotNullExpressionValue(checkedSetting4, "binding.userMentionsFilterThisServer");
                        widgetUserMentionFilter.updateFilters(WidgetUserMentions.Model.MessageLoader.Filters.copy$default(access$getFilters$p, 0L, !checkedSetting4.isChecked(), false, false, 13, null));
                        WidgetUserMentions.WidgetUserMentionFilter.this.delayedDismiss();
                    }
                });
            } else {
                CheckedSetting checkedSetting4 = getBinding().d;
                m.checkNotNullExpressionValue(checkedSetting4, "binding.userMentionsFilterThisServer");
                checkedSetting4.setVisibility(8);
            }
            CheckedSetting checkedSetting5 = getBinding().f2656b;
            m.checkNotNullExpressionValue(checkedSetting5, "binding.userMentionsFilterIncludeEveryone");
            Model.MessageLoader.Filters filters2 = this.filters;
            if (filters2 == null) {
                m.throwUninitializedPropertyAccessException("filters");
            }
            checkedSetting5.setChecked(filters2.getIncludeEveryone());
            getBinding().f2656b.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.user.WidgetUserMentions$WidgetUserMentionFilter$onResume$2
                public final void call(Boolean bool) {
                    WidgetUserMentionsFilterBinding binding;
                    WidgetUserMentions.WidgetUserMentionFilter widgetUserMentionFilter = WidgetUserMentions.WidgetUserMentionFilter.this;
                    WidgetUserMentions.Model.MessageLoader.Filters access$getFilters$p = WidgetUserMentions.WidgetUserMentionFilter.access$getFilters$p(widgetUserMentionFilter);
                    binding = WidgetUserMentions.WidgetUserMentionFilter.this.getBinding();
                    CheckedSetting checkedSetting6 = binding.f2656b;
                    m.checkNotNullExpressionValue(checkedSetting6, "binding.userMentionsFilterIncludeEveryone");
                    widgetUserMentionFilter.updateFilters(WidgetUserMentions.Model.MessageLoader.Filters.copy$default(access$getFilters$p, 0L, false, checkedSetting6.isChecked(), false, 11, null));
                    WidgetUserMentions.WidgetUserMentionFilter.this.delayedDismiss();
                }
            });
            CheckedSetting checkedSetting6 = getBinding().c;
            m.checkNotNullExpressionValue(checkedSetting6, "binding.userMentionsFilterIncludeRoles");
            Model.MessageLoader.Filters filters3 = this.filters;
            if (filters3 == null) {
                m.throwUninitializedPropertyAccessException("filters");
            }
            checkedSetting6.setChecked(filters3.getIncludeRoles());
            getBinding().c.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.user.WidgetUserMentions$WidgetUserMentionFilter$onResume$3
                public final void call(Boolean bool) {
                    WidgetUserMentionsFilterBinding binding;
                    WidgetUserMentions.WidgetUserMentionFilter widgetUserMentionFilter = WidgetUserMentions.WidgetUserMentionFilter.this;
                    WidgetUserMentions.Model.MessageLoader.Filters access$getFilters$p = WidgetUserMentions.WidgetUserMentionFilter.access$getFilters$p(widgetUserMentionFilter);
                    binding = WidgetUserMentions.WidgetUserMentionFilter.this.getBinding();
                    CheckedSetting checkedSetting7 = binding.c;
                    m.checkNotNullExpressionValue(checkedSetting7, "binding.userMentionsFilterIncludeRoles");
                    widgetUserMentionFilter.updateFilters(WidgetUserMentions.Model.MessageLoader.Filters.copy$default(access$getFilters$p, 0L, false, false, checkedSetting7.isChecked(), 7, null));
                    WidgetUserMentions.WidgetUserMentionFilter.this.delayedDismiss();
                }
            });
        }
    }

    public WidgetUserMentions() {
        super(R.layout.widget_user_mentions);
        WidgetUserMentions$viewModel$2 widgetUserMentions$viewModel$2 = WidgetUserMentions$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetUserMentionsViewModel.class), new WidgetUserMentions$appViewModels$$inlined$viewModels$1(f0Var), new b.a.d.h0(widgetUserMentions$viewModel$2));
    }

    private final void addThreadSpineItemDecoration(WidgetChatListAdapter widgetChatListAdapter) {
        getBinding().c.addItemDecoration(new ThreadSpineItemDecoration(requireContext(), widgetChatListAdapter));
    }

    private final void configureToolbar(final String str) {
        AppFragment.bindToolbar$default(this, null, 1, null);
        String string = this.mentionsLoader.getFilters().getAllGuilds() ? getString(R.string.all_servers) : str;
        if (isEmbedded()) {
            setActionBarTitle(string);
        } else {
            setActionBarSubtitle(string);
        }
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_user_mentions, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.user.WidgetUserMentions$configureToolbar$1

            /* compiled from: WidgetUserMentions.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;", "filters", "", "invoke", "(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.user.WidgetUserMentions$configureToolbar$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends d0.z.d.o implements Function1<WidgetUserMentions.Model.MessageLoader.Filters, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(WidgetUserMentions.Model.MessageLoader.Filters filters) {
                    invoke2(filters);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(WidgetUserMentions.Model.MessageLoader.Filters filters) {
                    WidgetUserMentions.Model.MessageLoader messageLoader;
                    m.checkNotNullParameter(filters, "filters");
                    messageLoader = WidgetUserMentions.this.mentionsLoader;
                    messageLoader.setFilters(filters);
                }
            }

            public final void call(MenuItem menuItem, Context context) {
                WidgetUserMentions.Model.MessageLoader messageLoader;
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_user_mentions_filter) {
                    WidgetUserMentions.WidgetUserMentionFilter.Companion companion = WidgetUserMentions.WidgetUserMentionFilter.Companion;
                    FragmentManager parentFragmentManager = WidgetUserMentions.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    String str2 = str;
                    messageLoader = WidgetUserMentions.this.mentionsLoader;
                    companion.show(parentFragmentManager, str2, messageLoader.getFilters(), new AnonymousClass1());
                }
            }
        }, null, 4, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        boolean z2 = true;
        boolean allGuilds = model.getGuildId() > 0 ? this.mentionsLoader.getFilters().getAllGuilds() : true;
        WidgetChatListAdapter widgetChatListAdapter = this.mentionsAdapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.setData(model);
        }
        Model.MessageLoader messageLoader = this.mentionsLoader;
        messageLoader.setFilters(Model.MessageLoader.Filters.copy$default(messageLoader.getFilters(), model.getGuildId(), allGuilds, false, false, 12, null));
        Model.MessageLoader messageLoader2 = this.mentionsLoader;
        if (model.getSelectedTab() != NavigationTab.MENTIONS) {
            z2 = false;
        }
        messageLoader2.setIsFocused(z2);
        if (isEmbedded() || isOnMentionsTab()) {
            configureToolbar(model.getGuildName());
        }
        getViewModel().setModel$app_productionGoogleRelease(model);
    }

    private final WidgetChatListAdapter createAdapter(Function1<? super StoreChat.InteractionState, Unit> function1) {
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.userMentionsList");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        WidgetChatListAdapter widgetChatListAdapter = new WidgetChatListAdapter(recyclerView, this, parentFragmentManager, new UserMentionsAdapterEventHandler(isEmbedded(), ChannelSelector.Companion.getInstance(), function1), null, null, 48, null);
        addThreadSpineItemDecoration(widgetChatListAdapter);
        return widgetChatListAdapter;
    }

    private final WidgetUserMentionsBinding getBinding() {
        return (WidgetUserMentionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGlobalSearchDismissModel getDismissViewModel() {
        return (WidgetGlobalSearchDismissModel) this.dismissViewModel$delegate.getValue();
    }

    private final WidgetUserMentionsViewModel getViewModel() {
        return (WidgetUserMentionsViewModel) this.viewModel$delegate.getValue();
    }

    private final boolean isEmbedded() {
        return ((Boolean) this.isEmbedded$delegate.getValue()).booleanValue();
    }

    private final boolean isOnMentionsTab() {
        return this.storeTabsNavigation.getSelectedTab() == NavigationTab.MENTIONS;
    }

    private final Observable<Model> observeModel() {
        if (isEmbedded()) {
            return Model.Companion.get(this.mentionsLoader, NavigationTab.MENTIONS);
        }
        Observable Y = StoreStream.Companion.getTabsNavigation().observeSelectedTab().Y(new j0.k.b<NavigationTab, Observable<? extends Model>>() { // from class: com.discord.widgets.user.WidgetUserMentions$observeModel$1
            public final Observable<? extends WidgetUserMentions.Model> call(NavigationTab navigationTab) {
                WidgetUserMentions.Model.MessageLoader messageLoader;
                WidgetUserMentions.Model.MessageLoader messageLoader2;
                if (navigationTab == NavigationTab.MENTIONS) {
                    WidgetUserMentions.Model.Companion companion = WidgetUserMentions.Model.Companion;
                    messageLoader2 = WidgetUserMentions.this.mentionsLoader;
                    return companion.get(messageLoader2, navigationTab);
                }
                WidgetUserMentions.Model.Companion companion2 = WidgetUserMentions.Model.Companion;
                messageLoader = WidgetUserMentions.this.mentionsLoader;
                m.checkNotNullExpressionValue(navigationTab, "selectedTab");
                return companion2.get(messageLoader, navigationTab).Z(1);
            }
        });
        m.checkNotNullExpressionValue(Y, "StoreStream.getTabsNavig…          }\n            }");
        return Y;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        WidgetChatListAdapter widgetChatListAdapter = this.mentionsAdapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.dispose();
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        WidgetChatListAdapter widgetChatListAdapter = this.mentionsAdapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.disposeHandlers();
        }
    }

    @Override // com.discord.widgets.tabs.OnTabSelectedListener
    public void onTabSelected() {
        Model model$app_productionGoogleRelease = getViewModel().getModel$app_productionGoogleRelease();
        if (model$app_productionGoogleRelease != null) {
            configureToolbar(model$app_productionGoogleRelease.getGuildName());
        }
        setActionBarTitleAccessibilityViewFocused();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        LinearLayoutManager layoutManager;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        WidgetTabsHost widgetTabsHost = null;
        if (isEmbedded()) {
            onTabSelected();
            AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
            getBinding().f2655b.setNavigationOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetUserMentions$onViewBound$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WidgetGlobalSearchDismissModel dismissViewModel;
                    dismissViewModel = WidgetUserMentions.this.getDismissViewModel();
                    dismissViewModel.dismiss();
                }
            });
        } else {
            setActionBarTitle(R.string.recent_mentions);
        }
        WidgetChatListAdapter widgetChatListAdapter = (WidgetChatListAdapter) MGRecyclerAdapter.Companion.configure(createAdapter(new WidgetUserMentions$onViewBound$2(this.mentionsLoader)));
        this.mentionsAdapter = widgetChatListAdapter;
        if (!(widgetChatListAdapter == null || (layoutManager = widgetChatListAdapter.getLayoutManager()) == null)) {
            layoutManager.setSmoothScrollbarEnabled(true);
        }
        WidgetChatListAdapter widgetChatListAdapter2 = this.mentionsAdapter;
        if (widgetChatListAdapter2 != null) {
            widgetChatListAdapter2.setMentionMeMessageLevelHighlighting(false);
        }
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof WidgetTabsHost) {
            widgetTabsHost = parentFragment;
        }
        WidgetTabsHost widgetTabsHost2 = widgetTabsHost;
        if (widgetTabsHost2 != null) {
            widgetTabsHost2.registerTabSelectionListener(NavigationTab.MENTIONS, this);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observeModel()), this, null, 2, null), WidgetUserMentions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserMentions$onViewBoundOrOnResume$1(this));
        WidgetChatListAdapter widgetChatListAdapter = this.mentionsAdapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.setHandlers();
        }
        Model.MessageLoader.tryLoad$default(this.mentionsLoader, null, 1, null);
    }
}
