package com.discord.widgets.user.profile;

import com.discord.databinding.UserProfileHeaderViewBinding;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: UserProfileHeaderView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "bannerBackgroundColor", "", "invoke", "(Ljava/lang/Integer;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileHeaderView$updateBannerBackgroundColorAsync$2 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ UserProfileHeaderView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserProfileHeaderView$updateBannerBackgroundColorAsync$2(UserProfileHeaderView userProfileHeaderView) {
        super(1);
        this.this$0 = userProfileHeaderView;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke2(num);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Integer num) {
        UserProfileHeaderViewBinding userProfileHeaderViewBinding;
        if (num != null) {
            userProfileHeaderViewBinding = this.this$0.binding;
            userProfileHeaderViewBinding.c.setBackgroundColor(num.intValue());
        }
    }
}
