package com.discord.widgets.user.profile;

import androidx.core.app.NotificationCompat;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: UserProfileHeaderView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "it", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Integer;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileHeaderView$observeRepresentativeColor$1<T, R> implements b<Integer, Boolean> {
    public static final UserProfileHeaderView$observeRepresentativeColor$1 INSTANCE = new UserProfileHeaderView$observeRepresentativeColor$1();

    public final Boolean call(Integer num) {
        return Boolean.valueOf(num != null);
    }
}
