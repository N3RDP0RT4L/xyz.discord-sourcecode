package com.discord.widgets.user.profile;

import com.discord.utilities.fresco.GrayscalePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: UserStatusPresenceCustomView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/facebook/imagepipeline/request/ImageRequestBuilder;", "it", "", "invoke", "(Lcom/facebook/imagepipeline/request/ImageRequestBuilder;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserStatusPresenceCustomView$configurePlaceholderEmoji$1 extends o implements Function1<ImageRequestBuilder, Unit> {
    public static final UserStatusPresenceCustomView$configurePlaceholderEmoji$1 INSTANCE = new UserStatusPresenceCustomView$configurePlaceholderEmoji$1();

    public UserStatusPresenceCustomView$configurePlaceholderEmoji$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ImageRequestBuilder imageRequestBuilder) {
        invoke2(imageRequestBuilder);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ImageRequestBuilder imageRequestBuilder) {
        GrayscalePostprocessor grayscalePostprocessor;
        m.checkNotNullParameter(imageRequestBuilder, "it");
        grayscalePostprocessor = UserStatusPresenceCustomView.CUSTOM_EMOJI_PLACEHOLDER_POSTPROCESSOR;
        imageRequestBuilder.l = grayscalePostprocessor;
    }
}
