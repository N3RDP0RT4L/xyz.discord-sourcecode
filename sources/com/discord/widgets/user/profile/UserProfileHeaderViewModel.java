package com.discord.widgets.user.profile;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.annotation.VisibleForTesting;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.user.UserProfile;
import com.discord.app.AppViewModel;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.nullserializable.NullSerializable;
import com.discord.stores.StoreAccessibility;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserPresence;
import com.discord.stores.StoreUserProfile;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.streams.StreamContextService;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.user.presence.ModelRichPresence;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import d0.g;
import d0.g0.t;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: UserProfileHeaderViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001c2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001c\u001d\u001eBe\u0012\n\u0010\f\u001a\u00060\nj\u0002`\u000b\u0012\u0010\b\u0002\u0010\u000f\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u000e\u0012\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u0010\u0012\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u0012\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0014\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u001a\u0010\f\u001a\u00060\nj\u0002`\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;)V", "toggleAllowAnimationInReducedMotion", "()V", "", "Lcom/discord/primitives/UserId;", "userId", "J", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "storeObservable", "", "shouldFetchProfile", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreUserProfile;", "storeUserProfile", HookHelper.constructorName, "(JLjava/lang/Long;Ljava/lang/Long;Lrx/Observable;ZLcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserProfile;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileHeaderViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    public static final long ME = -1;
    private final long userId;

    /* compiled from: UserProfileHeaderViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.profile.UserProfileHeaderViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            UserProfileHeaderViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: UserProfileHeaderViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b'\u0010(Jc\u0010\u0011\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u000f\u0012\u0004\u0012\u00020\u00100\u000e0\r2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u009b\u0001\u0010#\u001a\b\u0012\u0004\u0012\u00020\"0\r2\n\u0010\u0013\u001a\u00060\u0002j\u0002`\u000f2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\b\b\u0002\u0010\u0015\u001a\u00020\u00142\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\u0017\u001a\u00020\u00162\b\b\u0002\u0010\u0019\u001a\u00020\u00182\b\b\u0002\u0010\u001b\u001a\u00020\u001a2\b\b\u0002\u0010\u001d\u001a\u00020\u001c2\b\b\u0002\u0010\u001f\u001a\u00020\u001e2\b\b\u0002\u0010!\u001a\u00020 H\u0007¢\u0006\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b%\u0010&¨\u0006)"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "", "users", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lrx/Observable;", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "observeComputedMembers", "(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Collection;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;", "userId", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUserPresence;", "storeUserPresence", "Lcom/discord/stores/StoreUserProfile;", "storeUserProfile", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/utilities/streams/StreamContextService;", "streamContextService", "Lcom/discord/stores/StoreAccessibility;", "storeAccessibility", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;", "observeStoreState", "(JLjava/lang/Long;Ljava/lang/Long;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUserSettings;Lcom/discord/utilities/streams/StreamContextService;Lcom/discord/stores/StoreAccessibility;)Lrx/Observable;", "ME", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Map<Long, GuildMember>> observeComputedMembers(Long l, Long l2, final Collection<Long> collection, StoreChannels storeChannels, final StoreGuilds storeGuilds) {
            if (l != null && l.longValue() > 0) {
                Observable Y = storeChannels.observeChannel(l.longValue()).Y(new b<Channel, Observable<? extends Map<Long, ? extends GuildMember>>>() { // from class: com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion$observeComputedMembers$1
                    public final Observable<? extends Map<Long, GuildMember>> call(Channel channel) {
                        Observable<Map<Long, GuildMember>> observeComputed;
                        return (channel == null || (observeComputed = StoreGuilds.this.observeComputed(channel.f(), collection)) == null) ? new k(h0.emptyMap()) : observeComputed;
                    }
                });
                m.checkNotNullExpressionValue(Y, "storeChannels\n          …emptyMap())\n            }");
                return Y;
            } else if (l2 != null && l2.longValue() > 0) {
                return storeGuilds.observeComputed(l2.longValue(), collection);
            } else {
                k kVar = new k(h0.emptyMap());
                m.checkNotNullExpressionValue(kVar, "Observable.just(emptyMap())");
                return kVar;
            }
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, long j, Long l, Long l2, StoreUser storeUser, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreUserPresence storeUserPresence, StoreUserProfile storeUserProfile, StoreExperiments storeExperiments, StoreUserSettings storeUserSettings, StreamContextService streamContextService, StoreAccessibility storeAccessibility, int i, Object obj) {
            return companion.observeStoreState(j, l, l2, (i & 8) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 16) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 32) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 64) != 0 ? StoreStream.Companion.getPresences() : storeUserPresence, (i & 128) != 0 ? StoreStream.Companion.getUserProfile() : storeUserProfile, (i & 256) != 0 ? StoreStream.Companion.getExperiments() : storeExperiments, (i & 512) != 0 ? StoreStream.Companion.getUserSettings() : storeUserSettings, (i & 1024) != 0 ? new StreamContextService(null, null, null, null, null, null, null, null, 255, null) : streamContextService, (i & 2048) != 0 ? StoreStream.Companion.getAccessibility() : storeAccessibility);
        }

        @VisibleForTesting
        public final Observable<StoreState> observeStoreState(final long j, final Long l, final Long l2, final StoreUser storeUser, final StoreChannels storeChannels, final StoreGuilds storeGuilds, final StoreUserPresence storeUserPresence, final StoreUserProfile storeUserProfile, StoreExperiments storeExperiments, final StoreUserSettings storeUserSettings, final StreamContextService streamContextService, final StoreAccessibility storeAccessibility) {
            m.checkNotNullParameter(storeUser, "storeUser");
            m.checkNotNullParameter(storeChannels, "storeChannels");
            m.checkNotNullParameter(storeGuilds, "storeGuilds");
            m.checkNotNullParameter(storeUserPresence, "storeUserPresence");
            m.checkNotNullParameter(storeUserProfile, "storeUserProfile");
            m.checkNotNullParameter(storeExperiments, "storeExperiments");
            m.checkNotNullParameter(storeUserSettings, "storeUserSettings");
            m.checkNotNullParameter(streamContextService, "streamContextService");
            m.checkNotNullParameter(storeAccessibility, "storeAccessibility");
            Observable<StoreState> Y = StoreUser.observeMe$default(storeUser, false, 1, null).Y(new b<MeUser, Observable<? extends Pair<? extends MeUser, ? extends User>>>() { // from class: com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion$observeStoreState$1
                public final Observable<? extends Pair<MeUser, User>> call(final MeUser meUser) {
                    long j2 = j;
                    if (j2 == -1) {
                        return new k(new Pair(meUser, meUser));
                    }
                    return (Observable<R>) storeUser.observeUser(j2).F(new b<User, Pair<? extends MeUser, ? extends User>>() { // from class: com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion$observeStoreState$1.1
                        public final Pair<MeUser, User> call(User user) {
                            return new Pair<>(MeUser.this, user);
                        }
                    });
                }
            }).Y(new b<Pair<? extends MeUser, ? extends User>, Observable<? extends StoreState>>() { // from class: com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion$observeStoreState$2
                /* JADX WARN: Code restructure failed: missing block: B:9:0x0025, code lost:
                    if (r1 != null) goto L11;
                 */
                /* renamed from: call  reason: avoid collision after fix types in other method */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public final rx.Observable<? extends com.discord.widgets.user.profile.UserProfileHeaderViewModel.StoreState> call2(kotlin.Pair<com.discord.models.user.MeUser, ? extends com.discord.models.user.User> r13) {
                    /*
                        r12 = this;
                        java.lang.Object r0 = r13.component1()
                        com.discord.models.user.MeUser r0 = (com.discord.models.user.MeUser) r0
                        java.lang.Object r13 = r13.component2()
                        com.discord.models.user.User r13 = (com.discord.models.user.User) r13
                        if (r13 != 0) goto L11
                        rx.Observable<java.lang.Object> r13 = j0.l.a.c.k
                        return r13
                    L11:
                        java.lang.Long r1 = r1
                        r2 = 0
                        if (r1 == 0) goto L28
                        r1.longValue()
                        com.discord.stores.StoreChannels r1 = r2
                        java.lang.Long r3 = r1
                        long r3 = r3.longValue()
                        rx.Observable r1 = r1.observeChannel(r3)
                        if (r1 == 0) goto L28
                        goto L2d
                    L28:
                        j0.l.e.k r1 = new j0.l.e.k
                        r1.<init>(r2)
                    L2d:
                        r10 = r1
                        com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion r3 = com.discord.widgets.user.profile.UserProfileHeaderViewModel.Companion
                        r1 = 2
                        java.lang.Long[] r1 = new java.lang.Long[r1]
                        long r4 = r0.getId()
                        java.lang.Long r4 = java.lang.Long.valueOf(r4)
                        r9 = 0
                        r1[r9] = r4
                        long r4 = r13.getId()
                        java.lang.Long r4 = java.lang.Long.valueOf(r4)
                        r11 = 1
                        r1[r11] = r4
                        java.util.Set r6 = d0.t.n0.setOf(r1)
                        com.discord.stores.StoreChannels r7 = r2
                        com.discord.stores.StoreGuilds r8 = r3
                        java.lang.Long r4 = r1
                        java.lang.Long r5 = r4
                        rx.Observable r3 = com.discord.widgets.user.profile.UserProfileHeaderViewModel.Companion.access$observeComputedMembers(r3, r4, r5, r6, r7, r8)
                        com.discord.stores.StoreGuilds r1 = r3
                        rx.Observable r4 = r1.observeComputed()
                        com.discord.widgets.user.presence.ModelRichPresence$Companion r1 = com.discord.widgets.user.presence.ModelRichPresence.Companion
                        long r5 = r13.getId()
                        com.discord.stores.StoreUserPresence r7 = r5
                        rx.Observable r5 = r1.get(r5, r7)
                        com.discord.utilities.streams.StreamContextService r1 = r6
                        long r6 = r13.getId()
                        rx.Observable r6 = r1.getForUser(r6, r11)
                        com.discord.stores.StoreUserProfile r1 = r7
                        long r7 = r13.getId()
                        rx.Observable r7 = r1.observeUserProfile(r7)
                        com.discord.stores.StoreUserSettings r1 = r8
                        rx.Observable r8 = com.discord.stores.StoreUserSettings.observeIsAnimatedEmojisEnabled$default(r1, r9, r11, r2)
                        com.discord.stores.StoreAccessibility r1 = r9
                        rx.Observable r9 = r1.observeReducedMotionEnabled()
                        com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion$observeStoreState$2$1 r11 = new com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion$observeStoreState$2$1
                        r11.<init>()
                        rx.Observable r13 = rx.Observable.d(r3, r4, r5, r6, r7, r8, r9, r10, r11)
                        return r13
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion$observeStoreState$2.call2(kotlin.Pair):rx.Observable");
                }

                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends UserProfileHeaderViewModel.StoreState> call(Pair<? extends MeUser, ? extends User> pair) {
                    return call2((Pair<MeUser, ? extends User>) pair);
                }
            });
            m.checkNotNullExpressionValue(Y, "storeUser.observeMe()\n  …            }\n          }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: UserProfileHeaderViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u001b\b\u0086\b\u0018\u00002\u00020\u0001B\u0083\u0001\u0012\u0006\u0010!\u001a\u00020\u0002\u0012\u0006\u0010\"\u001a\u00020\u0005\u0012\u0016\u0010#\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b\u0012\u001c\u0010$\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b0\u000e\u0012\b\u0010%\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010&\u001a\u0004\u0018\u00010\u0014\u0012\u0006\u0010'\u001a\u00020\u0017\u0012\u0006\u0010(\u001a\u00020\u001a\u0012\u0006\u0010)\u001a\u00020\u001a\u0012\b\u0010*\u001a\u0004\u0018\u00010\u001e¢\u0006\u0004\bI\u0010JJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J \u0010\f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ&\u0010\u000f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b0\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001cJ\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J \u0001\u0010+\u001a\u00020\u00002\b\b\u0002\u0010!\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020\u00052\u0018\b\u0002\u0010#\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b2\u001e\b\u0002\u0010$\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b0\u000e2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010'\u001a\u00020\u00172\b\b\u0002\u0010(\u001a\u00020\u001a2\b\b\u0002\u0010)\u001a\u00020\u001a2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u001eHÆ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010.\u001a\u00020-HÖ\u0001¢\u0006\u0004\b.\u0010/J\u0010\u00101\u001a\u000200HÖ\u0001¢\u0006\u0004\b1\u00102J\u001a\u00104\u001a\u00020\u001a2\b\u00103\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b4\u00105R/\u0010$\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00106\u001a\u0004\b7\u0010\u0010R\u001b\u0010%\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b%\u00108\u001a\u0004\b9\u0010\u0013R\u001b\u0010*\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010:\u001a\u0004\b;\u0010 R)\u0010#\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010<\u001a\u0004\b=\u0010\rR\u0019\u0010)\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010>\u001a\u0004\b?\u0010\u001cR\u0019\u0010\"\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010@\u001a\u0004\bA\u0010\u0007R\u0019\u0010(\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010>\u001a\u0004\bB\u0010\u001cR\u0019\u0010'\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010C\u001a\u0004\bD\u0010\u0019R\u0019\u0010!\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010E\u001a\u0004\bF\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010G\u001a\u0004\bH\u0010\u0016¨\u0006K"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/user/User;", "component2", "()Lcom/discord/models/user/User;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "component3", "()Ljava/util/Map;", "", "component4", "()Ljava/util/Collection;", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "component5", "()Lcom/discord/widgets/user/presence/ModelRichPresence;", "Lcom/discord/utilities/streams/StreamContext;", "component6", "()Lcom/discord/utilities/streams/StreamContext;", "Lcom/discord/api/user/UserProfile;", "component7", "()Lcom/discord/api/user/UserProfile;", "", "component8", "()Z", "component9", "Lcom/discord/api/channel/Channel;", "component10", "()Lcom/discord/api/channel/Channel;", "me", "user", "userIdToGuildMemberMap", "guildMembers", "richPresence", "streamContext", "userProfile", "allowAnimatedEmojis", "reducedMotionEnabled", "channel", "copy", "(Lcom/discord/models/user/MeUser;Lcom/discord/models/user/User;Ljava/util/Map;Ljava/util/Collection;Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/user/UserProfile;ZZLcom/discord/api/channel/Channel;)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Collection;", "getGuildMembers", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "getRichPresence", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/Map;", "getUserIdToGuildMemberMap", "Z", "getReducedMotionEnabled", "Lcom/discord/models/user/User;", "getUser", "getAllowAnimatedEmojis", "Lcom/discord/api/user/UserProfile;", "getUserProfile", "Lcom/discord/models/user/MeUser;", "getMe", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/models/user/User;Ljava/util/Map;Ljava/util/Collection;Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/user/UserProfile;ZZLcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final boolean allowAnimatedEmojis;
        private final Channel channel;
        private final Collection<Map<Long, GuildMember>> guildMembers;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2845me;
        private final boolean reducedMotionEnabled;
        private final ModelRichPresence richPresence;
        private final StreamContext streamContext;
        private final User user;
        private final Map<Long, GuildMember> userIdToGuildMemberMap;
        private final UserProfile userProfile;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(MeUser meUser, User user, Map<Long, GuildMember> map, Collection<? extends Map<Long, GuildMember>> collection, ModelRichPresence modelRichPresence, StreamContext streamContext, UserProfile userProfile, boolean z2, boolean z3, Channel channel) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(map, "userIdToGuildMemberMap");
            m.checkNotNullParameter(collection, "guildMembers");
            m.checkNotNullParameter(userProfile, "userProfile");
            this.f2845me = meUser;
            this.user = user;
            this.userIdToGuildMemberMap = map;
            this.guildMembers = collection;
            this.richPresence = modelRichPresence;
            this.streamContext = streamContext;
            this.userProfile = userProfile;
            this.allowAnimatedEmojis = z2;
            this.reducedMotionEnabled = z3;
            this.channel = channel;
        }

        public final MeUser component1() {
            return this.f2845me;
        }

        public final Channel component10() {
            return this.channel;
        }

        public final User component2() {
            return this.user;
        }

        public final Map<Long, GuildMember> component3() {
            return this.userIdToGuildMemberMap;
        }

        public final Collection<Map<Long, GuildMember>> component4() {
            return this.guildMembers;
        }

        public final ModelRichPresence component5() {
            return this.richPresence;
        }

        public final StreamContext component6() {
            return this.streamContext;
        }

        public final UserProfile component7() {
            return this.userProfile;
        }

        public final boolean component8() {
            return this.allowAnimatedEmojis;
        }

        public final boolean component9() {
            return this.reducedMotionEnabled;
        }

        public final StoreState copy(MeUser meUser, User user, Map<Long, GuildMember> map, Collection<? extends Map<Long, GuildMember>> collection, ModelRichPresence modelRichPresence, StreamContext streamContext, UserProfile userProfile, boolean z2, boolean z3, Channel channel) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(map, "userIdToGuildMemberMap");
            m.checkNotNullParameter(collection, "guildMembers");
            m.checkNotNullParameter(userProfile, "userProfile");
            return new StoreState(meUser, user, map, collection, modelRichPresence, streamContext, userProfile, z2, z3, channel);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.f2845me, storeState.f2845me) && m.areEqual(this.user, storeState.user) && m.areEqual(this.userIdToGuildMemberMap, storeState.userIdToGuildMemberMap) && m.areEqual(this.guildMembers, storeState.guildMembers) && m.areEqual(this.richPresence, storeState.richPresence) && m.areEqual(this.streamContext, storeState.streamContext) && m.areEqual(this.userProfile, storeState.userProfile) && this.allowAnimatedEmojis == storeState.allowAnimatedEmojis && this.reducedMotionEnabled == storeState.reducedMotionEnabled && m.areEqual(this.channel, storeState.channel);
        }

        public final boolean getAllowAnimatedEmojis() {
            return this.allowAnimatedEmojis;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Collection<Map<Long, GuildMember>> getGuildMembers() {
            return this.guildMembers;
        }

        public final MeUser getMe() {
            return this.f2845me;
        }

        public final boolean getReducedMotionEnabled() {
            return this.reducedMotionEnabled;
        }

        public final ModelRichPresence getRichPresence() {
            return this.richPresence;
        }

        public final StreamContext getStreamContext() {
            return this.streamContext;
        }

        public final User getUser() {
            return this.user;
        }

        public final Map<Long, GuildMember> getUserIdToGuildMemberMap() {
            return this.userIdToGuildMemberMap;
        }

        public final UserProfile getUserProfile() {
            return this.userProfile;
        }

        public int hashCode() {
            MeUser meUser = this.f2845me;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            User user = this.user;
            int hashCode2 = (hashCode + (user != null ? user.hashCode() : 0)) * 31;
            Map<Long, GuildMember> map = this.userIdToGuildMemberMap;
            int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
            Collection<Map<Long, GuildMember>> collection = this.guildMembers;
            int hashCode4 = (hashCode3 + (collection != null ? collection.hashCode() : 0)) * 31;
            ModelRichPresence modelRichPresence = this.richPresence;
            int hashCode5 = (hashCode4 + (modelRichPresence != null ? modelRichPresence.hashCode() : 0)) * 31;
            StreamContext streamContext = this.streamContext;
            int hashCode6 = (hashCode5 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
            UserProfile userProfile = this.userProfile;
            int hashCode7 = (hashCode6 + (userProfile != null ? userProfile.hashCode() : 0)) * 31;
            boolean z2 = this.allowAnimatedEmojis;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode7 + i3) * 31;
            boolean z3 = this.reducedMotionEnabled;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            int i6 = (i5 + i2) * 31;
            Channel channel = this.channel;
            if (channel != null) {
                i = channel.hashCode();
            }
            return i6 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(me=");
            R.append(this.f2845me);
            R.append(", user=");
            R.append(this.user);
            R.append(", userIdToGuildMemberMap=");
            R.append(this.userIdToGuildMemberMap);
            R.append(", guildMembers=");
            R.append(this.guildMembers);
            R.append(", richPresence=");
            R.append(this.richPresence);
            R.append(", streamContext=");
            R.append(this.streamContext);
            R.append(", userProfile=");
            R.append(this.userProfile);
            R.append(", allowAnimatedEmojis=");
            R.append(this.allowAnimatedEmojis);
            R.append(", reducedMotionEnabled=");
            R.append(this.reducedMotionEnabled);
            R.append(", channel=");
            R.append(this.channel);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: UserProfileHeaderViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: UserProfileHeaderViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: UserProfileHeaderViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001c\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b1\b\u0086\b\u0018\u00002\u00020\u0001B«\u0001\u0012\u0006\u0010\"\u001a\u00020\u0002\u0012\b\u0010#\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\t\u0012\u000e\b\u0002\u0010&\u001a\b\u0012\u0004\u0012\u00020\t0\f\u0012\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u0012\u0012\b\b\u0002\u0010)\u001a\u00020\u0015\u0012\u0006\u0010*\u001a\u00020\u0018\u0012\u0006\u0010+\u001a\u00020\u0018\u0012\b\b\u0002\u0010,\u001a\u00020\u0018\u0012\u0006\u0010-\u001a\u00020\u0018\u0012\b\b\u0002\u0010.\u001a\u00020\u0018\u0012\b\b\u0002\u0010/\u001a\u00020\u0018\u0012\b\b\u0002\u00100\u001a\u00020\u0018\u0012\b\b\u0002\u00101\u001a\u00020\u0018¢\u0006\u0004\bg\u0010hJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001aJ\u0010\u0010\u001d\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001aJ\u0010\u0010\u001e\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001aJ\u0010\u0010\u001f\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u001f\u0010\u001aJ\u0010\u0010 \u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b \u0010\u001aJ\u0010\u0010!\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b!\u0010\u001aJÀ\u0001\u00102\u001a\u00020\u00002\b\b\u0002\u0010\"\u001a\u00020\u00022\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\t2\u000e\b\u0002\u0010&\u001a\b\u0012\u0004\u0012\u00020\t0\f2\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010)\u001a\u00020\u00152\b\b\u0002\u0010*\u001a\u00020\u00182\b\b\u0002\u0010+\u001a\u00020\u00182\b\b\u0002\u0010,\u001a\u00020\u00182\b\b\u0002\u0010-\u001a\u00020\u00182\b\b\u0002\u0010.\u001a\u00020\u00182\b\b\u0002\u0010/\u001a\u00020\u00182\b\b\u0002\u00100\u001a\u00020\u00182\b\b\u0002\u00101\u001a\u00020\u0018HÆ\u0001¢\u0006\u0004\b2\u00103J\u0010\u00104\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b4\u0010\u0007J\u0010\u00106\u001a\u000205HÖ\u0001¢\u0006\u0004\b6\u00107J\u001a\u0010:\u001a\u00020\u00182\b\u00109\u001a\u0004\u0018\u000108HÖ\u0003¢\u0006\u0004\b:\u0010;R\u0019\u0010<\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010\u001aR\u001b\u0010'\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010?\u001a\u0004\b@\u0010\u0011R\u0019\u0010A\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010=\u001a\u0004\bB\u0010\u001aR\u0019\u00101\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010=\u001a\u0004\b1\u0010\u001aR\u0019\u0010C\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010=\u001a\u0004\bC\u0010\u001aR\u0019\u0010.\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010=\u001a\u0004\bD\u0010\u001aR\u001b\u0010#\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010E\u001a\u0004\bF\u0010\u0007R\u0019\u0010-\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010=\u001a\u0004\bG\u0010\u001aR\u0019\u0010\"\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010H\u001a\u0004\bI\u0010\u0004R\u0019\u0010J\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010=\u001a\u0004\bK\u0010\u001aR\u0019\u0010+\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010=\u001a\u0004\b+\u0010\u001aR\u001d\u0010O\u001a\u00020\u00058F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\bL\u0010M\u001a\u0004\bN\u0010\u0007R\u001d\u0010R\u001a\u00020\u00058F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\bP\u0010M\u001a\u0004\bQ\u0010\u0007R\u001b\u0010$\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010E\u001a\u0004\bS\u0010\u0007R\u001b\u0010(\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010T\u001a\u0004\bU\u0010\u0014R\u001f\u0010&\u001a\b\u0012\u0004\u0012\u00020\t0\f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010V\u001a\u0004\bW\u0010\u000eR\u0019\u0010X\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010=\u001a\u0004\bY\u0010\u001aR\u0019\u0010*\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010=\u001a\u0004\b*\u0010\u001aR\u0019\u00100\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010=\u001a\u0004\bZ\u0010\u001aR\u0019\u0010/\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010=\u001a\u0004\b[\u0010\u001aR\u001b\u0010%\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\\\u001a\u0004\b]\u0010\u000bR\u0019\u0010,\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010=\u001a\u0004\b^\u0010\u001aR\u0019\u0010)\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010_\u001a\u0004\b`\u0010\u0017R\u0019\u0010a\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\ba\u0010=\u001a\u0004\bb\u0010\u001aR\u0019\u0010c\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\bc\u0010=\u001a\u0004\bd\u0010\u001aR\u0019\u0010e\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\be\u0010=\u001a\u0004\bf\u0010\u001a¨\u0006i"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "", "component2", "()Ljava/lang/String;", "component3", "Lcom/discord/models/member/GuildMember;", "component4", "()Lcom/discord/models/member/GuildMember;", "", "component5", "()Ljava/util/List;", "Lcom/discord/models/presence/Presence;", "component6", "()Lcom/discord/models/presence/Presence;", "Lcom/discord/utilities/streams/StreamContext;", "component7", "()Lcom/discord/utilities/streams/StreamContext;", "Lcom/discord/api/user/UserProfile;", "component8", "()Lcom/discord/api/user/UserProfile;", "", "component9", "()Z", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "user", "banner", "bannerColorHex", "guildMember", "guildMembersForAka", "presence", "streamContext", "userProfile", "isMeUserPremium", "isMeUserVerified", "allowAnimatedEmojis", "showPresence", "editable", "reducedMotionEnabled", "allowAnimationInReducedMotion", "isMe", "copy", "(Lcom/discord/models/user/User;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/member/GuildMember;Ljava/util/List;Lcom/discord/models/presence/Presence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/user/UserProfile;ZZZZZZZZ)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "showAkas", "Z", "getShowAkas", "Lcom/discord/models/presence/Presence;", "getPresence", "showMediumAvatar", "getShowMediumAvatar", "isProfileLoaded", "getEditable", "Ljava/lang/String;", "getBanner", "getShowPresence", "Lcom/discord/models/user/User;", "getUser", "showSmallAvatar", "getShowSmallAvatar", "avatarColorId$delegate", "Lkotlin/Lazy;", "getAvatarColorId", "avatarColorId", "guildMemberColorId$delegate", "getGuildMemberColorId", "guildMemberColorId", "getBannerColorHex", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", "Ljava/util/List;", "getGuildMembersForAka", "shouldShowGIFTag", "getShouldShowGIFTag", "getAllowAnimationInReducedMotion", "getReducedMotionEnabled", "Lcom/discord/models/member/GuildMember;", "getGuildMember", "getAllowAnimatedEmojis", "Lcom/discord/api/user/UserProfile;", "getUserProfile", "shouldAnimateBanner", "getShouldAnimateBanner", "hasGuildMemberAvatar", "getHasGuildMemberAvatar", "hasNickname", "getHasNickname", HookHelper.constructorName, "(Lcom/discord/models/user/User;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/member/GuildMember;Ljava/util/List;Lcom/discord/models/presence/Presence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/user/UserProfile;ZZZZZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean allowAnimatedEmojis;
            private final boolean allowAnimationInReducedMotion;
            private final Lazy avatarColorId$delegate;
            private final String banner;
            private final String bannerColorHex;
            private final boolean editable;
            private final GuildMember guildMember;
            private final Lazy guildMemberColorId$delegate;
            private final List<GuildMember> guildMembersForAka;
            private final boolean hasGuildMemberAvatar;
            private final boolean hasNickname;
            private final boolean isMe;
            private final boolean isMeUserPremium;
            private final boolean isMeUserVerified;
            private final boolean isProfileLoaded;
            private final Presence presence;
            private final boolean reducedMotionEnabled;
            private final boolean shouldAnimateBanner;
            private final boolean shouldShowGIFTag;
            private final boolean showAkas;
            private final boolean showMediumAvatar;
            private final boolean showPresence;
            private final boolean showSmallAvatar;
            private final StreamContext streamContext;
            private final User user;
            private final UserProfile userProfile;

            public /* synthetic */ Loaded(User user, String str, String str2, GuildMember guildMember, List list, Presence presence, StreamContext streamContext, UserProfile userProfile, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(user, str, str2, (i & 8) != 0 ? null : guildMember, (i & 16) != 0 ? n.emptyList() : list, (i & 32) != 0 ? null : presence, (i & 64) != 0 ? null : streamContext, (i & 128) != 0 ? StoreUserProfile.Companion.getEMPTY_PROFILE() : userProfile, z2, z3, (i & 1024) != 0 ? false : z4, z5, (i & 4096) != 0 ? false : z6, (i & 8192) != 0 ? false : z7, (i & 16384) != 0 ? false : z8, (i & 32768) != 0 ? false : z9);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, User user, String str, String str2, GuildMember guildMember, List list, Presence presence, StreamContext streamContext, UserProfile userProfile, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, int i, Object obj) {
                return loaded.copy((i & 1) != 0 ? loaded.user : user, (i & 2) != 0 ? loaded.banner : str, (i & 4) != 0 ? loaded.bannerColorHex : str2, (i & 8) != 0 ? loaded.guildMember : guildMember, (i & 16) != 0 ? loaded.guildMembersForAka : list, (i & 32) != 0 ? loaded.presence : presence, (i & 64) != 0 ? loaded.streamContext : streamContext, (i & 128) != 0 ? loaded.userProfile : userProfile, (i & 256) != 0 ? loaded.isMeUserPremium : z2, (i & 512) != 0 ? loaded.isMeUserVerified : z3, (i & 1024) != 0 ? loaded.allowAnimatedEmojis : z4, (i & 2048) != 0 ? loaded.showPresence : z5, (i & 4096) != 0 ? loaded.editable : z6, (i & 8192) != 0 ? loaded.reducedMotionEnabled : z7, (i & 16384) != 0 ? loaded.allowAnimationInReducedMotion : z8, (i & 32768) != 0 ? loaded.isMe : z9);
            }

            public final User component1() {
                return this.user;
            }

            public final boolean component10() {
                return this.isMeUserVerified;
            }

            public final boolean component11() {
                return this.allowAnimatedEmojis;
            }

            public final boolean component12() {
                return this.showPresence;
            }

            public final boolean component13() {
                return this.editable;
            }

            public final boolean component14() {
                return this.reducedMotionEnabled;
            }

            public final boolean component15() {
                return this.allowAnimationInReducedMotion;
            }

            public final boolean component16() {
                return this.isMe;
            }

            public final String component2() {
                return this.banner;
            }

            public final String component3() {
                return this.bannerColorHex;
            }

            public final GuildMember component4() {
                return this.guildMember;
            }

            public final List<GuildMember> component5() {
                return this.guildMembersForAka;
            }

            public final Presence component6() {
                return this.presence;
            }

            public final StreamContext component7() {
                return this.streamContext;
            }

            public final UserProfile component8() {
                return this.userProfile;
            }

            public final boolean component9() {
                return this.isMeUserPremium;
            }

            public final Loaded copy(User user, String str, String str2, GuildMember guildMember, List<GuildMember> list, Presence presence, StreamContext streamContext, UserProfile userProfile, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9) {
                m.checkNotNullParameter(user, "user");
                m.checkNotNullParameter(list, "guildMembersForAka");
                m.checkNotNullParameter(userProfile, "userProfile");
                return new Loaded(user, str, str2, guildMember, list, presence, streamContext, userProfile, z2, z3, z4, z5, z6, z7, z8, z9);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.user, loaded.user) && m.areEqual(this.banner, loaded.banner) && m.areEqual(this.bannerColorHex, loaded.bannerColorHex) && m.areEqual(this.guildMember, loaded.guildMember) && m.areEqual(this.guildMembersForAka, loaded.guildMembersForAka) && m.areEqual(this.presence, loaded.presence) && m.areEqual(this.streamContext, loaded.streamContext) && m.areEqual(this.userProfile, loaded.userProfile) && this.isMeUserPremium == loaded.isMeUserPremium && this.isMeUserVerified == loaded.isMeUserVerified && this.allowAnimatedEmojis == loaded.allowAnimatedEmojis && this.showPresence == loaded.showPresence && this.editable == loaded.editable && this.reducedMotionEnabled == loaded.reducedMotionEnabled && this.allowAnimationInReducedMotion == loaded.allowAnimationInReducedMotion && this.isMe == loaded.isMe;
            }

            public final boolean getAllowAnimatedEmojis() {
                return this.allowAnimatedEmojis;
            }

            public final boolean getAllowAnimationInReducedMotion() {
                return this.allowAnimationInReducedMotion;
            }

            public final String getAvatarColorId() {
                return (String) this.avatarColorId$delegate.getValue();
            }

            public final String getBanner() {
                return this.banner;
            }

            public final String getBannerColorHex() {
                return this.bannerColorHex;
            }

            public final boolean getEditable() {
                return this.editable;
            }

            public final GuildMember getGuildMember() {
                return this.guildMember;
            }

            public final String getGuildMemberColorId() {
                return (String) this.guildMemberColorId$delegate.getValue();
            }

            public final List<GuildMember> getGuildMembersForAka() {
                return this.guildMembersForAka;
            }

            public final boolean getHasGuildMemberAvatar() {
                return this.hasGuildMemberAvatar;
            }

            public final boolean getHasNickname() {
                return this.hasNickname;
            }

            public final Presence getPresence() {
                return this.presence;
            }

            public final boolean getReducedMotionEnabled() {
                return this.reducedMotionEnabled;
            }

            public final boolean getShouldAnimateBanner() {
                return this.shouldAnimateBanner;
            }

            public final boolean getShouldShowGIFTag() {
                return this.shouldShowGIFTag;
            }

            public final boolean getShowAkas() {
                return this.showAkas;
            }

            public final boolean getShowMediumAvatar() {
                return this.showMediumAvatar;
            }

            public final boolean getShowPresence() {
                return this.showPresence;
            }

            public final boolean getShowSmallAvatar() {
                return this.showSmallAvatar;
            }

            public final StreamContext getStreamContext() {
                return this.streamContext;
            }

            public final User getUser() {
                return this.user;
            }

            public final UserProfile getUserProfile() {
                return this.userProfile;
            }

            public int hashCode() {
                User user = this.user;
                int i = 0;
                int hashCode = (user != null ? user.hashCode() : 0) * 31;
                String str = this.banner;
                int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.bannerColorHex;
                int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
                GuildMember guildMember = this.guildMember;
                int hashCode4 = (hashCode3 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
                List<GuildMember> list = this.guildMembersForAka;
                int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
                Presence presence = this.presence;
                int hashCode6 = (hashCode5 + (presence != null ? presence.hashCode() : 0)) * 31;
                StreamContext streamContext = this.streamContext;
                int hashCode7 = (hashCode6 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
                UserProfile userProfile = this.userProfile;
                if (userProfile != null) {
                    i = userProfile.hashCode();
                }
                int i2 = (hashCode7 + i) * 31;
                boolean z2 = this.isMeUserPremium;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.isMeUserVerified;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.allowAnimatedEmojis;
                if (z4) {
                    z4 = true;
                }
                int i10 = z4 ? 1 : 0;
                int i11 = z4 ? 1 : 0;
                int i12 = (i9 + i10) * 31;
                boolean z5 = this.showPresence;
                if (z5) {
                    z5 = true;
                }
                int i13 = z5 ? 1 : 0;
                int i14 = z5 ? 1 : 0;
                int i15 = (i12 + i13) * 31;
                boolean z6 = this.editable;
                if (z6) {
                    z6 = true;
                }
                int i16 = z6 ? 1 : 0;
                int i17 = z6 ? 1 : 0;
                int i18 = (i15 + i16) * 31;
                boolean z7 = this.reducedMotionEnabled;
                if (z7) {
                    z7 = true;
                }
                int i19 = z7 ? 1 : 0;
                int i20 = z7 ? 1 : 0;
                int i21 = (i18 + i19) * 31;
                boolean z8 = this.allowAnimationInReducedMotion;
                if (z8) {
                    z8 = true;
                }
                int i22 = z8 ? 1 : 0;
                int i23 = z8 ? 1 : 0;
                int i24 = (i21 + i22) * 31;
                boolean z9 = this.isMe;
                if (!z9) {
                    i3 = z9 ? 1 : 0;
                }
                return i24 + i3;
            }

            public final boolean isMe() {
                return this.isMe;
            }

            public final boolean isMeUserPremium() {
                return this.isMeUserPremium;
            }

            public final boolean isMeUserVerified() {
                return this.isMeUserVerified;
            }

            public final boolean isProfileLoaded() {
                return this.isProfileLoaded;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(user=");
                R.append(this.user);
                R.append(", banner=");
                R.append(this.banner);
                R.append(", bannerColorHex=");
                R.append(this.bannerColorHex);
                R.append(", guildMember=");
                R.append(this.guildMember);
                R.append(", guildMembersForAka=");
                R.append(this.guildMembersForAka);
                R.append(", presence=");
                R.append(this.presence);
                R.append(", streamContext=");
                R.append(this.streamContext);
                R.append(", userProfile=");
                R.append(this.userProfile);
                R.append(", isMeUserPremium=");
                R.append(this.isMeUserPremium);
                R.append(", isMeUserVerified=");
                R.append(this.isMeUserVerified);
                R.append(", allowAnimatedEmojis=");
                R.append(this.allowAnimatedEmojis);
                R.append(", showPresence=");
                R.append(this.showPresence);
                R.append(", editable=");
                R.append(this.editable);
                R.append(", reducedMotionEnabled=");
                R.append(this.reducedMotionEnabled);
                R.append(", allowAnimationInReducedMotion=");
                R.append(this.allowAnimationInReducedMotion);
                R.append(", isMe=");
                return a.M(R, this.isMe, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(User user, String str, String str2, GuildMember guildMember, List<GuildMember> list, Presence presence, StreamContext streamContext, UserProfile userProfile, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9) {
                super(null);
                String str3;
                m.checkNotNullParameter(user, "user");
                m.checkNotNullParameter(list, "guildMembersForAka");
                m.checkNotNullParameter(userProfile, "userProfile");
                this.user = user;
                this.banner = str;
                this.bannerColorHex = str2;
                this.guildMember = guildMember;
                this.guildMembersForAka = list;
                this.presence = presence;
                this.streamContext = streamContext;
                this.userProfile = userProfile;
                this.isMeUserPremium = z2;
                this.isMeUserVerified = z3;
                this.allowAnimatedEmojis = z4;
                this.showPresence = z5;
                this.editable = z6;
                this.reducedMotionEnabled = z7;
                this.allowAnimationInReducedMotion = z8;
                this.isMe = z9;
                boolean z10 = false;
                boolean z11 = guildMember != null && guildMember.hasAvatar();
                this.hasGuildMemberAvatar = z11;
                boolean z12 = !t.isBlank((guildMember == null || (str3 = guildMember.getNick()) == null) ? "" : str3);
                this.hasNickname = z12;
                this.showMediumAvatar = z11 && !z12;
                this.showSmallAvatar = z11 && z12;
                this.shouldAnimateBanner = !z7 || z8;
                this.shouldShowGIFTag = z7 && !z8 && str != null && t.startsWith$default(str, "a_", false, 2, null);
                this.isProfileLoaded = !m.areEqual(userProfile, StoreUserProfile.Companion.getEMPTY_PROFILE());
                this.avatarColorId$delegate = g.lazy(new UserProfileHeaderViewModel$ViewState$Loaded$avatarColorId$2(this));
                this.guildMemberColorId$delegate = g.lazy(new UserProfileHeaderViewModel$ViewState$Loaded$guildMemberColorId$2(this));
                if ((!list.isEmpty()) && !z9) {
                    z10 = true;
                }
                this.showAkas = z10;
            }
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ UserProfileHeaderViewModel(long r19, java.lang.Long r21, java.lang.Long r22, rx.Observable r23, boolean r24, com.discord.stores.StoreUser r25, com.discord.stores.StoreUserProfile r26, int r27, kotlin.jvm.internal.DefaultConstructorMarker r28) {
        /*
            r18 = this;
            r0 = r27 & 2
            r1 = 0
            if (r0 == 0) goto L7
            r0 = r1
            goto L9
        L7:
            r0 = r21
        L9:
            r2 = r27 & 4
            if (r2 == 0) goto Le
            goto L10
        Le:
            r1 = r22
        L10:
            r2 = r27 & 8
            if (r2 == 0) goto L2d
            com.discord.widgets.user.profile.UserProfileHeaderViewModel$Companion r2 = com.discord.widgets.user.profile.UserProfileHeaderViewModel.Companion
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 4088(0xff8, float:5.729E-42)
            r17 = 0
            r3 = r19
            r5 = r0
            r6 = r1
            rx.Observable r2 = com.discord.widgets.user.profile.UserProfileHeaderViewModel.Companion.observeStoreState$default(r2, r3, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            r7 = r2
            goto L2f
        L2d:
            r7 = r23
        L2f:
            r2 = r27 & 16
            if (r2 == 0) goto L36
            r2 = 0
            r8 = 0
            goto L38
        L36:
            r8 = r24
        L38:
            r2 = r27 & 32
            if (r2 == 0) goto L44
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r2 = r2.getUsers()
            r9 = r2
            goto L46
        L44:
            r9 = r25
        L46:
            r2 = r27 & 64
            if (r2 == 0) goto L52
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUserProfile r2 = r2.getUserProfile()
            r10 = r2
            goto L54
        L52:
            r10 = r26
        L54:
            r2 = r18
            r3 = r19
            r5 = r0
            r6 = r1
            r2.<init>(r3, r5, r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.profile.UserProfileHeaderViewModel.<init>(long, java.lang.Long, java.lang.Long, rx.Observable, boolean, com.discord.stores.StoreUser, com.discord.stores.StoreUserProfile, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        List list;
        long id2 = storeState.getUser().getId();
        GuildMember guildMember = storeState.getUserIdToGuildMemberMap().get(Long.valueOf(id2));
        NullSerializable<String> b2 = storeState.getUserProfile().g().b();
        Presence presence = null;
        if (!(b2 instanceof NullSerializable.b)) {
            b2 = null;
        }
        NullSerializable.b bVar = (NullSerializable.b) b2;
        String str = bVar != null ? (String) bVar.a() : null;
        Channel channel = storeState.getChannel();
        boolean z2 = channel != null && ChannelUtils.x(channel);
        ViewState viewState = getViewState();
        User user = storeState.getUser();
        NullSerializable<String> c = storeState.getUserProfile().g().c();
        String a = c != null ? c.a() : null;
        if (z2) {
            Collection<Map<Long, GuildMember>> guildMembers = storeState.getGuildMembers();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = guildMembers.iterator();
            while (it.hasNext()) {
                GuildMember guildMember2 = (GuildMember) ((Map) it.next()).get(Long.valueOf(id2));
                if (guildMember2 != null) {
                    arrayList.add(guildMember2);
                }
            }
            list = u.toList(arrayList);
        } else {
            list = n.emptyList();
        }
        ModelRichPresence richPresence = storeState.getRichPresence();
        if (richPresence != null) {
            presence = richPresence.getPresence();
        }
        updateViewState(new ViewState.Loaded(user, str, a, guildMember, list, presence, storeState.getStreamContext(), storeState.getUserProfile(), UserUtils.INSTANCE.isPremium(storeState.getMe()), storeState.getMe().isVerified(), storeState.getAllowAnimatedEmojis(), true, false, storeState.getReducedMotionEnabled(), viewState instanceof ViewState.Loaded ? ((ViewState.Loaded) viewState).getAllowAnimationInReducedMotion() : false, id2 == storeState.getMe().getId(), 4096, null));
    }

    @MainThread
    public final void toggleAllowAnimationInReducedMotion() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && loaded.getReducedMotionEnabled()) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, null, null, null, false, false, false, false, false, false, !loaded.getAllowAnimationInReducedMotion(), false, 49151, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserProfileHeaderViewModel(long j, Long l, Long l2, Observable<StoreState> observable, boolean z2, StoreUser storeUser, StoreUserProfile storeUserProfile) {
        super(ViewState.Uninitialized.INSTANCE);
        long j2 = j;
        m.checkNotNullParameter(observable, "storeObservable");
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(storeUserProfile, "storeUserProfile");
        this.userId = j2;
        if (z2) {
            storeUserProfile.fetchProfile(j2 == -1 ? storeUser.getMe().getId() : j2, (r13 & 2) != 0 ? null : l2, (r13 & 4) != 0 ? false : false, (r13 & 8) != 0 ? null : null);
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), UserProfileHeaderViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
