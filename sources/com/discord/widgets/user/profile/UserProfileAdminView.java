package com.discord.widgets.user.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import b.d.b.a.a;
import com.discord.databinding.UserProfileAdminViewBinding;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.view.extensions.ViewExtensions;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: UserProfileAdminView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\"B\u0017\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b \u0010!J\u001b\u0010\u0005\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\b\u001a\u00020\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\b\u0010\u0006J\u001b\u0010\n\u001a\u00020\u00032\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\n\u0010\u0006J\u001b\u0010\f\u001a\u00020\u00032\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\f\u0010\u0006J\u001b\u0010\u000e\u001a\u00020\u00032\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u000e\u0010\u0006J\u001b\u0010\u0010\u001a\u00020\u00032\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0010\u0010\u0006J\u001b\u0010\u0012\u001a\u00020\u00032\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0012\u0010\u0006J\u001b\u0010\u0014\u001a\u00020\u00032\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0014\u0010\u0006J\u0015\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006#"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileAdminView;", "Landroid/widget/LinearLayout;", "Lkotlin/Function0;", "", "onEditMember", "setOnEditMember", "(Lkotlin/jvm/functions/Function0;)V", "onKick", "setOnKick", "onBan", "setOnBan", "onDisableCommunication", "setOnDisableCommunication", "onServerMute", "setOnServerMute", "onServerDeafen", "setOnServerDeafen", "onServerMove", "setOnServerMove", "onDisconnect", "setOnDisconnect", "Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "viewState", "updateView", "(Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;)V", "Lcom/discord/databinding/UserProfileAdminViewBinding;", "binding", "Lcom/discord/databinding/UserProfileAdminViewBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileAdminView extends LinearLayout {
    private final UserProfileAdminViewBinding binding;

    /* compiled from: UserProfileAdminView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u001d\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001Bo\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0002\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0002¢\u0006\u0004\b1\u00102J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0004J\u0092\u0001\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00022\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00022\b\b\u0002\u0010\u001b\u001a\u00020\u00022\b\b\u0002\u0010\u001c\u001a\u00020\u00022\b\b\u0002\u0010\u001d\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010'\u001a\u00020\u00022\b\u0010&\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010)\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010)\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u001d\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010)\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010)\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010)\u001a\u0004\b+\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010)\u001a\u0004\b,\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010)\u001a\u0004\b-\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010)\u001a\u0004\b\u0019\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010)\u001a\u0004\b.\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b/\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010)\u001a\u0004\b0\u0010\u0004¨\u00063"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "", "", "component1", "()Z", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "component10", "component11", "component12", "component13", "showEditMemberButton", "showKickButton", "showDisableCommunicationButton", "isMultiUserDM", "showBanButton", "showServerMuteButton", "isServerMuted", "showServerDeafenButton", "isServerDeafened", "showServerMoveAndDisconnectButtons", "isAdminSectionEnabled", "isMe", "isCommunicationDisabled", "copy", "(ZZZZZZZZZZZZZ)Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowBanButton", "getShowServerMuteButton", "getShowEditMemberButton", "getShowServerDeafenButton", "getShowKickButton", "getShowServerMoveAndDisconnectButtons", "getShowDisableCommunicationButton", HookHelper.constructorName, "(ZZZZZZZZZZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean isAdminSectionEnabled;
        private final boolean isCommunicationDisabled;
        private final boolean isMe;
        private final boolean isMultiUserDM;
        private final boolean isServerDeafened;
        private final boolean isServerMuted;
        private final boolean showBanButton;
        private final boolean showDisableCommunicationButton;
        private final boolean showEditMemberButton;
        private final boolean showKickButton;
        private final boolean showServerDeafenButton;
        private final boolean showServerMoveAndDisconnectButtons;
        private final boolean showServerMuteButton;

        public ViewState(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13, boolean z14) {
            this.showEditMemberButton = z2;
            this.showKickButton = z3;
            this.showDisableCommunicationButton = z4;
            this.isMultiUserDM = z5;
            this.showBanButton = z6;
            this.showServerMuteButton = z7;
            this.isServerMuted = z8;
            this.showServerDeafenButton = z9;
            this.isServerDeafened = z10;
            this.showServerMoveAndDisconnectButtons = z11;
            this.isAdminSectionEnabled = z12;
            this.isMe = z13;
            this.isCommunicationDisabled = z14;
        }

        public final boolean component1() {
            return this.showEditMemberButton;
        }

        public final boolean component10() {
            return this.showServerMoveAndDisconnectButtons;
        }

        public final boolean component11() {
            return this.isAdminSectionEnabled;
        }

        public final boolean component12() {
            return this.isMe;
        }

        public final boolean component13() {
            return this.isCommunicationDisabled;
        }

        public final boolean component2() {
            return this.showKickButton;
        }

        public final boolean component3() {
            return this.showDisableCommunicationButton;
        }

        public final boolean component4() {
            return this.isMultiUserDM;
        }

        public final boolean component5() {
            return this.showBanButton;
        }

        public final boolean component6() {
            return this.showServerMuteButton;
        }

        public final boolean component7() {
            return this.isServerMuted;
        }

        public final boolean component8() {
            return this.showServerDeafenButton;
        }

        public final boolean component9() {
            return this.isServerDeafened;
        }

        public final ViewState copy(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13, boolean z14) {
            return new ViewState(z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return this.showEditMemberButton == viewState.showEditMemberButton && this.showKickButton == viewState.showKickButton && this.showDisableCommunicationButton == viewState.showDisableCommunicationButton && this.isMultiUserDM == viewState.isMultiUserDM && this.showBanButton == viewState.showBanButton && this.showServerMuteButton == viewState.showServerMuteButton && this.isServerMuted == viewState.isServerMuted && this.showServerDeafenButton == viewState.showServerDeafenButton && this.isServerDeafened == viewState.isServerDeafened && this.showServerMoveAndDisconnectButtons == viewState.showServerMoveAndDisconnectButtons && this.isAdminSectionEnabled == viewState.isAdminSectionEnabled && this.isMe == viewState.isMe && this.isCommunicationDisabled == viewState.isCommunicationDisabled;
        }

        public final boolean getShowBanButton() {
            return this.showBanButton;
        }

        public final boolean getShowDisableCommunicationButton() {
            return this.showDisableCommunicationButton;
        }

        public final boolean getShowEditMemberButton() {
            return this.showEditMemberButton;
        }

        public final boolean getShowKickButton() {
            return this.showKickButton;
        }

        public final boolean getShowServerDeafenButton() {
            return this.showServerDeafenButton;
        }

        public final boolean getShowServerMoveAndDisconnectButtons() {
            return this.showServerMoveAndDisconnectButtons;
        }

        public final boolean getShowServerMuteButton() {
            return this.showServerMuteButton;
        }

        public int hashCode() {
            boolean z2 = this.showEditMemberButton;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.showKickButton;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.showDisableCommunicationButton;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.isMultiUserDM;
            if (z5) {
                z5 = true;
            }
            int i11 = z5 ? 1 : 0;
            int i12 = z5 ? 1 : 0;
            int i13 = (i10 + i11) * 31;
            boolean z6 = this.showBanButton;
            if (z6) {
                z6 = true;
            }
            int i14 = z6 ? 1 : 0;
            int i15 = z6 ? 1 : 0;
            int i16 = (i13 + i14) * 31;
            boolean z7 = this.showServerMuteButton;
            if (z7) {
                z7 = true;
            }
            int i17 = z7 ? 1 : 0;
            int i18 = z7 ? 1 : 0;
            int i19 = (i16 + i17) * 31;
            boolean z8 = this.isServerMuted;
            if (z8) {
                z8 = true;
            }
            int i20 = z8 ? 1 : 0;
            int i21 = z8 ? 1 : 0;
            int i22 = (i19 + i20) * 31;
            boolean z9 = this.showServerDeafenButton;
            if (z9) {
                z9 = true;
            }
            int i23 = z9 ? 1 : 0;
            int i24 = z9 ? 1 : 0;
            int i25 = (i22 + i23) * 31;
            boolean z10 = this.isServerDeafened;
            if (z10) {
                z10 = true;
            }
            int i26 = z10 ? 1 : 0;
            int i27 = z10 ? 1 : 0;
            int i28 = (i25 + i26) * 31;
            boolean z11 = this.showServerMoveAndDisconnectButtons;
            if (z11) {
                z11 = true;
            }
            int i29 = z11 ? 1 : 0;
            int i30 = z11 ? 1 : 0;
            int i31 = (i28 + i29) * 31;
            boolean z12 = this.isAdminSectionEnabled;
            if (z12) {
                z12 = true;
            }
            int i32 = z12 ? 1 : 0;
            int i33 = z12 ? 1 : 0;
            int i34 = (i31 + i32) * 31;
            boolean z13 = this.isMe;
            if (z13) {
                z13 = true;
            }
            int i35 = z13 ? 1 : 0;
            int i36 = z13 ? 1 : 0;
            int i37 = (i34 + i35) * 31;
            boolean z14 = this.isCommunicationDisabled;
            if (!z14) {
                i = z14 ? 1 : 0;
            }
            return i37 + i;
        }

        public final boolean isAdminSectionEnabled() {
            return this.isAdminSectionEnabled;
        }

        public final boolean isCommunicationDisabled() {
            return this.isCommunicationDisabled;
        }

        public final boolean isMe() {
            return this.isMe;
        }

        public final boolean isMultiUserDM() {
            return this.isMultiUserDM;
        }

        public final boolean isServerDeafened() {
            return this.isServerDeafened;
        }

        public final boolean isServerMuted() {
            return this.isServerMuted;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(showEditMemberButton=");
            R.append(this.showEditMemberButton);
            R.append(", showKickButton=");
            R.append(this.showKickButton);
            R.append(", showDisableCommunicationButton=");
            R.append(this.showDisableCommunicationButton);
            R.append(", isMultiUserDM=");
            R.append(this.isMultiUserDM);
            R.append(", showBanButton=");
            R.append(this.showBanButton);
            R.append(", showServerMuteButton=");
            R.append(this.showServerMuteButton);
            R.append(", isServerMuted=");
            R.append(this.isServerMuted);
            R.append(", showServerDeafenButton=");
            R.append(this.showServerDeafenButton);
            R.append(", isServerDeafened=");
            R.append(this.isServerDeafened);
            R.append(", showServerMoveAndDisconnectButtons=");
            R.append(this.showServerMoveAndDisconnectButtons);
            R.append(", isAdminSectionEnabled=");
            R.append(this.isAdminSectionEnabled);
            R.append(", isMe=");
            R.append(this.isMe);
            R.append(", isCommunicationDisabled=");
            return a.M(R, this.isCommunicationDisabled, ")");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserProfileAdminView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        View inflate = LayoutInflater.from(context).inflate(R.layout.user_profile_admin_view, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.user_profile_admin_ban;
        TextView textView = (TextView) inflate.findViewById(R.id.user_profile_admin_ban);
        if (textView != null) {
            i = R.id.user_profile_admin_disable_communication;
            TextView textView2 = (TextView) inflate.findViewById(R.id.user_profile_admin_disable_communication);
            if (textView2 != null) {
                i = R.id.user_profile_admin_edit_member;
                TextView textView3 = (TextView) inflate.findViewById(R.id.user_profile_admin_edit_member);
                if (textView3 != null) {
                    i = R.id.user_profile_admin_kick;
                    TextView textView4 = (TextView) inflate.findViewById(R.id.user_profile_admin_kick);
                    if (textView4 != null) {
                        i = R.id.user_profile_admin_server_deafen;
                        TextView textView5 = (TextView) inflate.findViewById(R.id.user_profile_admin_server_deafen);
                        if (textView5 != null) {
                            i = R.id.user_profile_admin_server_disconnect;
                            TextView textView6 = (TextView) inflate.findViewById(R.id.user_profile_admin_server_disconnect);
                            if (textView6 != null) {
                                i = R.id.user_profile_admin_server_move;
                                TextView textView7 = (TextView) inflate.findViewById(R.id.user_profile_admin_server_move);
                                if (textView7 != null) {
                                    i = R.id.user_profile_admin_server_mute;
                                    TextView textView8 = (TextView) inflate.findViewById(R.id.user_profile_admin_server_mute);
                                    if (textView8 != null) {
                                        UserProfileAdminViewBinding userProfileAdminViewBinding = new UserProfileAdminViewBinding((LinearLayout) inflate, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8);
                                        m.checkNotNullExpressionValue(userProfileAdminViewBinding, "UserProfileAdminViewBind…rom(context), this, true)");
                                        this.binding = userProfileAdminViewBinding;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void setOnBan(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onBan");
        this.binding.f2146b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnBan$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void setOnDisableCommunication(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onDisableCommunication");
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnDisableCommunication$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void setOnDisconnect(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onDisconnect");
        this.binding.g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnDisconnect$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void setOnEditMember(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onEditMember");
        this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnEditMember$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void setOnKick(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onKick");
        this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnKick$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void setOnServerDeafen(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onServerDeafen");
        this.binding.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnServerDeafen$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void setOnServerMove(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onServerMove");
        this.binding.h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnServerMove$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void setOnServerMute(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onServerMute");
        this.binding.i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileAdminView$setOnServerMute$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void updateView(ViewState viewState) {
        Drawable drawable;
        int i;
        int i2;
        m.checkNotNullParameter(viewState, "viewState");
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.userProfileAdminEditMember");
        int i3 = 8;
        textView.setVisibility(viewState.getShowEditMemberButton() ? 0 : 8);
        TextView textView2 = this.binding.e;
        m.checkNotNullExpressionValue(textView2, "binding.userProfileAdminKick");
        textView2.setVisibility(viewState.getShowKickButton() ? 0 : 8);
        TextView textView3 = this.binding.c;
        m.checkNotNullExpressionValue(textView3, "binding.userProfileAdminDisableCommunication");
        textView3.setVisibility(viewState.getShowDisableCommunicationButton() ? 0 : 8);
        this.binding.e.setText(viewState.isMultiUserDM() ? R.string.remove_from_group : R.string.kick);
        TextView textView4 = this.binding.f2146b;
        m.checkNotNullExpressionValue(textView4, "binding.userProfileAdminBan");
        textView4.setVisibility(viewState.getShowBanButton() ? 0 : 8);
        boolean isCommunicationDisabled = viewState.isCommunicationDisabled();
        int i4 = isCommunicationDisabled ? R.string.remove_time_out : R.string.time_out;
        if (isCommunicationDisabled) {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_enable_guild_communication_24dp);
        } else {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_disable_guild_communication_red_24dp);
        }
        this.binding.c.setText(i4);
        TextView textView5 = this.binding.c;
        m.checkNotNullExpressionValue(textView5, "binding.userProfileAdminDisableCommunication");
        DrawableCompat.setCompoundDrawablesCompat$default(textView5, drawable, (Drawable) null, (Drawable) null, (Drawable) null, 14, (Object) null);
        TextView textView6 = this.binding.i;
        m.checkNotNullExpressionValue(textView6, "binding.userProfileAdminServerMute");
        textView6.setVisibility(viewState.getShowServerMuteButton() ? 0 : 8);
        boolean isServerMuted = viewState.isServerMuted();
        boolean isMe = viewState.isMe();
        if (isServerMuted) {
            i = DrawableCompat.getThemedDrawableRes$default(this, (int) R.attr.ic_mic_muted_grey, 0, 2, (Object) null);
        } else {
            i = DrawableCompat.getThemedDrawableRes$default(this, (int) R.attr.ic_mic_grey, 0, 2, (Object) null);
        }
        int i5 = i;
        int i6 = isServerMuted ? R.string.server_unmute : R.string.server_mute;
        TextView textView7 = this.binding.i;
        m.checkNotNullExpressionValue(textView7, "binding.userProfileAdminServerMute");
        ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView7, i5, 0, 0, 0, 14, null);
        this.binding.i.setText(i6);
        TextView textView8 = this.binding.f;
        m.checkNotNullExpressionValue(textView8, "binding.userProfileAdminServerDeafen");
        textView8.setVisibility(viewState.getShowServerDeafenButton() ? 0 : 8);
        boolean isServerDeafened = viewState.isServerDeafened();
        if (isServerDeafened) {
            i2 = DrawableCompat.getThemedDrawableRes$default(this, (int) R.attr.ic_headset_deafened_grey, 0, 2, (Object) null);
        } else {
            i2 = DrawableCompat.getThemedDrawableRes$default(this, (int) R.attr.ic_headset_deafened_grey, 0, 2, (Object) null);
        }
        int i7 = i2;
        int i8 = isServerDeafened ? R.string.server_undeafen : R.string.server_deafen;
        int i9 = isMe ? R.string.disconnect_self : R.string.disconnect_other;
        TextView textView9 = this.binding.f;
        m.checkNotNullExpressionValue(textView9, "binding.userProfileAdminServerDeafen");
        ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView9, i7, 0, 0, 0, 14, null);
        this.binding.f.setText(i8);
        TextView textView10 = this.binding.h;
        m.checkNotNullExpressionValue(textView10, "binding.userProfileAdminServerMove");
        textView10.setVisibility(viewState.getShowServerMoveAndDisconnectButtons() ? 0 : 8);
        this.binding.g.setText(i9);
        TextView textView11 = this.binding.g;
        m.checkNotNullExpressionValue(textView11, "binding.userProfileAdminServerDisconnect");
        if (viewState.getShowServerMoveAndDisconnectButtons()) {
            i3 = 0;
        }
        textView11.setVisibility(i3);
    }
}
