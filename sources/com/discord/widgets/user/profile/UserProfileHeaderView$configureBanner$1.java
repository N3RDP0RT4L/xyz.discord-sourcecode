package com.discord.widgets.user.profile;

import android.graphics.Bitmap;
import b.f.j.q.a;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Ref$BooleanRef;
/* compiled from: UserProfileHeaderView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/facebook/imagepipeline/request/ImageRequestBuilder;", "imageRequestBuilder", "", "invoke", "(Lcom/facebook/imagepipeline/request/ImageRequestBuilder;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileHeaderView$configureBanner$1 extends o implements Function1<ImageRequestBuilder, Unit> {
    public final /* synthetic */ Ref$BooleanRef $bannerColorUpdatedFromViewState;
    public final /* synthetic */ UserProfileHeaderViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ UserProfileHeaderView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserProfileHeaderView$configureBanner$1(UserProfileHeaderView userProfileHeaderView, Ref$BooleanRef ref$BooleanRef, UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = userProfileHeaderView;
        this.$bannerColorUpdatedFromViewState = ref$BooleanRef;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ImageRequestBuilder imageRequestBuilder) {
        invoke2(imageRequestBuilder);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ImageRequestBuilder imageRequestBuilder) {
        m.checkNotNullParameter(imageRequestBuilder, "imageRequestBuilder");
        imageRequestBuilder.l = new a() { // from class: com.discord.widgets.user.profile.UserProfileHeaderView$configureBanner$1.1
            @Override // b.f.j.q.a
            public void process(Bitmap bitmap) {
                if (bitmap != null) {
                    UserProfileHeaderView$configureBanner$1 userProfileHeaderView$configureBanner$1 = UserProfileHeaderView$configureBanner$1.this;
                    if (!userProfileHeaderView$configureBanner$1.$bannerColorUpdatedFromViewState.element) {
                        userProfileHeaderView$configureBanner$1.this$0.updateBannerColor(userProfileHeaderView$configureBanner$1.$viewState);
                    }
                }
            }
        };
    }
}
