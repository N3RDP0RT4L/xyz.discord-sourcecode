package com.discord.widgets.user.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import b.d.b.a.a;
import com.discord.databinding.ViewUserStatusPresenceCustomBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.stores.StoreStream;
import com.discord.utilities.fresco.GrayscalePostprocessor;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: UserStatusPresenceCustomView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001c2\u00020\u0001:\u0003\u001c\u001d\u001eB\u0017\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\r\u001a\u00020\u00042\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u000b¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;", "Landroid/widget/LinearLayout;", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;", "emoji", "", "configureStatusEmoji", "(Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;)V", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "placeholderEmoji", "configurePlaceholderEmoji", "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;)V", "Lkotlin/Function0;", "onClear", "setOnClear", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState;", "viewState", "updateViewState", "(Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState;)V", "Lcom/discord/databinding/ViewUserStatusPresenceCustomBinding;", "binding", "Lcom/discord/databinding/ViewUserStatusPresenceCustomBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "Companion", "Emoji", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserStatusPresenceCustomView extends LinearLayout {
    private final ViewUserStatusPresenceCustomBinding binding;
    public static final Companion Companion = new Companion(null);
    private static final GrayscalePostprocessor CUSTOM_EMOJI_PLACEHOLDER_POSTPROCESSOR = new GrayscalePostprocessor();

    /* compiled from: UserStatusPresenceCustomView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Companion;", "", "Lcom/discord/utilities/fresco/GrayscalePostprocessor;", "CUSTOM_EMOJI_PLACEHOLDER_POSTPROCESSOR", "Lcom/discord/utilities/fresco/GrayscalePostprocessor;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: UserStatusPresenceCustomView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ2\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00062\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u000b\u0010\bR\u001b\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()Z", ModelAuditLogEntry.CHANGE_KEY_ID, ModelAuditLogEntry.CHANGE_KEY_NAME, "isAnimated", "copy", "(Ljava/lang/String;Ljava/lang/String;Z)Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getName", "Z", "getId", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Emoji {

        /* renamed from: id  reason: collision with root package name */
        private final String f2846id;
        private final boolean isAnimated;
        private final String name;

        public Emoji(String str, String str2, boolean z2) {
            this.f2846id = str;
            this.name = str2;
            this.isAnimated = z2;
        }

        public static /* synthetic */ Emoji copy$default(Emoji emoji, String str, String str2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = emoji.f2846id;
            }
            if ((i & 2) != 0) {
                str2 = emoji.name;
            }
            if ((i & 4) != 0) {
                z2 = emoji.isAnimated;
            }
            return emoji.copy(str, str2, z2);
        }

        public final String component1() {
            return this.f2846id;
        }

        public final String component2() {
            return this.name;
        }

        public final boolean component3() {
            return this.isAnimated;
        }

        public final Emoji copy(String str, String str2, boolean z2) {
            return new Emoji(str, str2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Emoji)) {
                return false;
            }
            Emoji emoji = (Emoji) obj;
            return m.areEqual(this.f2846id, emoji.f2846id) && m.areEqual(this.name, emoji.name) && this.isAnimated == emoji.isAnimated;
        }

        public final String getId() {
            return this.f2846id;
        }

        public final String getName() {
            return this.name;
        }

        public int hashCode() {
            String str = this.f2846id;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.name;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.isAnimated;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isAnimated() {
            return this.isAnimated;
        }

        public String toString() {
            StringBuilder R = a.R("Emoji(id=");
            R.append(this.f2846id);
            R.append(", name=");
            R.append(this.name);
            R.append(", isAnimated=");
            return a.M(R, this.isAnimated, ")");
        }
    }

    /* compiled from: UserStatusPresenceCustomView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState;", "", HookHelper.constructorName, "()V", "WithPlaceholder", "WithStatus", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState$WithStatus;", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState$WithPlaceholder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: UserStatusPresenceCustomView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState$WithPlaceholder;", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState;", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "component1", "()Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "placeholderEmoji", "copy", "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;)Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState$WithPlaceholder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "getPlaceholderEmoji", HookHelper.constructorName, "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class WithPlaceholder extends ViewState {
            private final ModelEmojiUnicode placeholderEmoji;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public WithPlaceholder(ModelEmojiUnicode modelEmojiUnicode) {
                super(null);
                m.checkNotNullParameter(modelEmojiUnicode, "placeholderEmoji");
                this.placeholderEmoji = modelEmojiUnicode;
            }

            public static /* synthetic */ WithPlaceholder copy$default(WithPlaceholder withPlaceholder, ModelEmojiUnicode modelEmojiUnicode, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelEmojiUnicode = withPlaceholder.placeholderEmoji;
                }
                return withPlaceholder.copy(modelEmojiUnicode);
            }

            public final ModelEmojiUnicode component1() {
                return this.placeholderEmoji;
            }

            public final WithPlaceholder copy(ModelEmojiUnicode modelEmojiUnicode) {
                m.checkNotNullParameter(modelEmojiUnicode, "placeholderEmoji");
                return new WithPlaceholder(modelEmojiUnicode);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof WithPlaceholder) && m.areEqual(this.placeholderEmoji, ((WithPlaceholder) obj).placeholderEmoji);
                }
                return true;
            }

            public final ModelEmojiUnicode getPlaceholderEmoji() {
                return this.placeholderEmoji;
            }

            public int hashCode() {
                ModelEmojiUnicode modelEmojiUnicode = this.placeholderEmoji;
                if (modelEmojiUnicode != null) {
                    return modelEmojiUnicode.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("WithPlaceholder(placeholderEmoji=");
                R.append(this.placeholderEmoji);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: UserStatusPresenceCustomView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J(\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState$WithStatus;", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState;", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;", "component1", "()Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;", "", "component2", "()Ljava/lang/String;", "emoji", "statusText", "copy", "(Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;Ljava/lang/String;)Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState$WithStatus;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;", "getEmoji", "Ljava/lang/String;", "getStatusText", HookHelper.constructorName, "(Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$Emoji;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class WithStatus extends ViewState {
            private final Emoji emoji;
            private final String statusText;

            public WithStatus(Emoji emoji, String str) {
                super(null);
                this.emoji = emoji;
                this.statusText = str;
            }

            public static /* synthetic */ WithStatus copy$default(WithStatus withStatus, Emoji emoji, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    emoji = withStatus.emoji;
                }
                if ((i & 2) != 0) {
                    str = withStatus.statusText;
                }
                return withStatus.copy(emoji, str);
            }

            public final Emoji component1() {
                return this.emoji;
            }

            public final String component2() {
                return this.statusText;
            }

            public final WithStatus copy(Emoji emoji, String str) {
                return new WithStatus(emoji, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof WithStatus)) {
                    return false;
                }
                WithStatus withStatus = (WithStatus) obj;
                return m.areEqual(this.emoji, withStatus.emoji) && m.areEqual(this.statusText, withStatus.statusText);
            }

            public final Emoji getEmoji() {
                return this.emoji;
            }

            public final String getStatusText() {
                return this.statusText;
            }

            public int hashCode() {
                Emoji emoji = this.emoji;
                int i = 0;
                int hashCode = (emoji != null ? emoji.hashCode() : 0) * 31;
                String str = this.statusText;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("WithStatus(emoji=");
                R.append(this.emoji);
                R.append(", statusText=");
                return a.H(R, this.statusText, ")");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserStatusPresenceCustomView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_user_status_presence_custom, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.user_status_presence_custom_clear;
        AppCompatImageView appCompatImageView = (AppCompatImageView) inflate.findViewById(R.id.user_status_presence_custom_clear);
        if (appCompatImageView != null) {
            i = R.id.user_status_presence_custom_emoji;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.user_status_presence_custom_emoji);
            if (simpleDraweeView != null) {
                i = R.id.user_status_presence_custom_text;
                TextView textView = (TextView) inflate.findViewById(R.id.user_status_presence_custom_text);
                if (textView != null) {
                    ViewUserStatusPresenceCustomBinding viewUserStatusPresenceCustomBinding = new ViewUserStatusPresenceCustomBinding((LinearLayout) inflate, appCompatImageView, simpleDraweeView, textView);
                    m.checkNotNullExpressionValue(viewUserStatusPresenceCustomBinding, "ViewUserStatusPresenceCu…rom(context), this, true)");
                    this.binding = viewUserStatusPresenceCustomBinding;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    private final void configurePlaceholderEmoji(ModelEmojiUnicode modelEmojiUnicode) {
        SimpleDraweeView simpleDraweeView = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.userStatusPresenceCustomEmoji");
        MGImages.setImage$default(simpleDraweeView, ModelEmojiUnicode.getImageUri(modelEmojiUnicode.getCodePoints(), getContext()), 0, 0, false, UserStatusPresenceCustomView$configurePlaceholderEmoji$1.INSTANCE, null, 92, null);
        SimpleDraweeView simpleDraweeView2 = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.userStatusPresenceCustomEmoji");
        simpleDraweeView2.setVisibility(0);
    }

    private final void configureStatusEmoji(Emoji emoji) {
        String str = null;
        if (emoji != null) {
            if (emoji.getId() != null) {
                str = ModelEmojiCustom.getImageUri(Long.parseLong(emoji.getId()), emoji.isAnimated(), IconUtils.getMediaProxySize(getResources().getDimensionPixelSize(R.dimen.custom_status_emoji_preview_size)));
            } else {
                ModelEmojiUnicode modelEmojiUnicode = StoreStream.Companion.getEmojis().getUnicodeEmojiSurrogateMap().get(emoji.getName());
                if (modelEmojiUnicode != null) {
                    str = ModelEmojiUnicode.getImageUri(modelEmojiUnicode.getCodePoints(), getContext());
                }
            }
        }
        SimpleDraweeView simpleDraweeView = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.userStatusPresenceCustomEmoji");
        MGImages.setImage$default(simpleDraweeView, str, 0, 0, false, null, null, 124, null);
        SimpleDraweeView simpleDraweeView2 = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.userStatusPresenceCustomEmoji");
        int i = 0;
        if (!(str != null)) {
            i = 8;
        }
        simpleDraweeView2.setVisibility(i);
    }

    public final void setOnClear(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onClear");
        this.binding.f2199b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserStatusPresenceCustomView$setOnClear$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void updateViewState(ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        if (viewState instanceof ViewState.WithStatus) {
            ViewState.WithStatus withStatus = (ViewState.WithStatus) viewState;
            configureStatusEmoji(withStatus.getEmoji());
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.userStatusPresenceCustomText");
            textView.setText(withStatus.getStatusText());
            AppCompatImageView appCompatImageView = this.binding.f2199b;
            m.checkNotNullExpressionValue(appCompatImageView, "binding.userStatusPresenceCustomClear");
            appCompatImageView.setVisibility(0);
        } else if (viewState instanceof ViewState.WithPlaceholder) {
            configurePlaceholderEmoji(((ViewState.WithPlaceholder) viewState).getPlaceholderEmoji());
            this.binding.d.setText(R.string.custom_status_set_custom_status);
            AppCompatImageView appCompatImageView2 = this.binding.f2199b;
            m.checkNotNullExpressionValue(appCompatImageView2, "binding.userStatusPresenceCustomClear");
            appCompatImageView2.setVisibility(8);
        }
    }
}
