package com.discord.widgets.user.profile;

import android.content.Context;
import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import b.f.g.a.a.b;
import b.f.g.a.a.d;
import b.f.g.e.v;
import b.f.g.f.a;
import b.f.g.f.c;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: DraweeSpanStringBuilderExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\f\n\u0002\b\u0004\u001aS\u0010\f\u001a\u00020\u0000*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b\f\u0010\r¨\u0006\u000e"}, d2 = {"Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "Landroid/content/Context;", "context", "", "avatarUrl", "", "animateAvatar", "", "avatarSizePx", "roundingOverlayColor", "", "spaceCharacter", "setAvatar", "(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Landroid/content/Context;Ljava/lang/String;ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Character;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DraweeSpanStringBuilderExtensionsKt {
    /* JADX WARN: Type inference failed for: r0v2, types: [REQUEST, com.facebook.imagepipeline.request.ImageRequest] */
    public static final DraweeSpanStringBuilder setAvatar(DraweeSpanStringBuilder draweeSpanStringBuilder, Context context, String str, boolean z2, Integer num, @ColorInt Integer num2, Character ch) {
        m.checkNotNullParameter(draweeSpanStringBuilder, "$this$setAvatar");
        m.checkNotNullParameter(context, "context");
        if (str != null) {
            int length = draweeSpanStringBuilder.length();
            draweeSpanStringBuilder.append("#");
            draweeSpanStringBuilder.append((CharSequence) String.valueOf(ch));
            int intValue = num != null ? num.intValue() : DimenUtils.dpToPixels(16);
            ?? a = MGImages.getImageRequest(str, IconUtils.getMediaProxySize(intValue), IconUtils.getMediaProxySize(intValue), true).a();
            d a2 = b.a();
            a2.h = a;
            a2.m = z2;
            c cVar = new c();
            cVar.f519b = true;
            cVar.a = 1;
            if (num2 != null) {
                int intValue2 = num2.intValue();
                m.checkNotNullExpressionValue(cVar, "roundingParams");
                cVar.b(intValue2);
            }
            a aVar = new a(context.getResources());
            aVar.f = ContextCompat.getDrawable(context, R.drawable.asset_default_avatar_32dp);
            aVar.r = cVar;
            ScalingUtils$ScaleType scalingUtils$ScaleType = ScalingUtils$ScaleType.a;
            aVar.n = v.l;
            draweeSpanStringBuilder.c(context, aVar.a(), a2.a(), length, length, intValue, intValue, false, 2);
        }
        return draweeSpanStringBuilder;
    }

    public static /* synthetic */ DraweeSpanStringBuilder setAvatar$default(DraweeSpanStringBuilder draweeSpanStringBuilder, Context context, String str, boolean z2, Integer num, Integer num2, Character ch, int i, Object obj) {
        Integer num3 = null;
        String str2 = (i & 2) != 0 ? null : str;
        boolean z3 = (i & 4) != 0 ? false : z2;
        Integer num4 = (i & 8) != 0 ? null : num;
        if ((i & 16) == 0) {
            num3 = num2;
        }
        return setAvatar(draweeSpanStringBuilder, context, str2, z3, num4, num3, (i & 32) != 0 ? ' ' : ch);
    }
}
