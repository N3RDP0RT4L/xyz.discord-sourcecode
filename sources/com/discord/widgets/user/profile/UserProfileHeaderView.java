package com.discord.widgets.user.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityEmoji;
import com.discord.api.user.UserProfile;
import com.discord.app.AppComponent;
import com.discord.databinding.UserProfileHeaderBadgeBinding;
import com.discord.databinding.UserProfileHeaderViewBinding;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.colors.RepresentativeColorsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.utilities.viewcontroller.ViewDetachedFromWindowObservable;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.discord.views.UsernameView;
import com.discord.views.user.UserAvatarPresenceView;
import com.discord.widgets.channels.UserAkaView;
import com.discord.widgets.user.Badge;
import com.discord.widgets.user.UserNameFormatterKt;
import com.discord.widgets.user.profile.UserProfileHeaderView;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$BooleanRef;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: UserProfileHeaderView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 R2\u00020\u0001:\u0002SRB\u0017\u0012\u0006\u0010M\u001a\u00020L\u0012\u0006\u0010O\u001a\u00020N¢\u0006\u0004\bP\u0010QJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\u0006J\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\f\u0010\u0006J\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\u0006J\u000f\u0010\u000e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0010\u0010\u0006J\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0011\u0010\u0006J\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0012\u0010\u0006J!\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0014\u001a\u00020\u00132\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J#\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u0014\u001a\u00020\u00132\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u001f\u0010\"\u001a\u00020!2\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u001fH\u0002¢\u0006\u0004\b\"\u0010#J\u000f\u0010$\u001a\u00020\u0004H\u0014¢\u0006\u0004\b$\u0010\u000fJ\u0015\u0010%\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b%\u0010\u0006J\u0015\u0010&\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b&\u0010\u0006R\u0016\u0010'\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0018\u0010-\u001a\u0004\u0018\u00010,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R&\u00102\u001a\u0012\u0012\u0004\u0012\u000200\u0012\b\u0012\u000601R\u00020\u00000/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103R\u0018\u00104\u001a\u0004\u0018\u00010,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b4\u0010.R.\u00106\u001a\u000e\u0012\u0004\u0012\u000200\u0012\u0004\u0012\u00020\u0004058\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b6\u00107\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b=\u0010>R(\u0010@\u001a\b\u0012\u0004\u0012\u00020\u00040?8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b@\u0010A\u001a\u0004\bB\u0010C\"\u0004\bD\u0010ER(\u0010F\u001a\b\u0012\u0004\u0012\u00020\u00040?8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bF\u0010A\u001a\u0004\bG\u0010C\"\u0004\bH\u0010ER.\u0010I\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004058\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bI\u00107\u001a\u0004\bJ\u00109\"\u0004\bK\u0010;¨\u0006T"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;", "viewState", "", "configureBanner", "(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;)V", "Lrx/Observable;", "", "observeRepresentativeColor", "(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;)Lrx/Observable;", "updateBannerBackgroundColorAsync", "notifyAvatarColorListenerAsync", "editAvatar", "onTapBanner", "()V", "configurePrimaryName", "configureAka", "configureSecondaryName", "Lcom/discord/models/user/User;", "user", "Lcom/discord/models/member/GuildMember;", "guildMember", "Landroid/text/SpannableStringBuilder;", "getPrimaryNameTextForUser", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)Landroid/text/SpannableStringBuilder;", "", "getSecondaryNameTextForUser", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)Ljava/lang/CharSequence;", "Lcom/discord/api/activity/Activity;", "customStatusActivity", "", "shouldAnimate", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "getCustomStatusDraweeSpanStringBuilder", "(Lcom/discord/api/activity/Activity;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "onFinishInflate", "updateViewState", "updateBannerColor", "userProfileHeaderBackgroundColor", "I", "Lcom/discord/databinding/UserProfileHeaderViewBinding;", "binding", "Lcom/discord/databinding/UserProfileHeaderViewBinding;", "Lrx/Subscription;", "setBannerBackgroundColorSubscription", "Lrx/Subscription;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/widgets/user/Badge;", "Lcom/discord/widgets/user/profile/UserProfileHeaderView$BadgeViewHolder;", "badgesAdapter", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "syncAvatarRepresentativeColorSubscription", "Lkotlin/Function1;", "onBadgeClick", "Lkotlin/jvm/functions/Function1;", "getOnBadgeClick", "()Lkotlin/jvm/functions/Function1;", "setOnBadgeClick", "(Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "bannerChangeDetector", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "Lkotlin/Function0;", "onAvatarEdit", "Lkotlin/jvm/functions/Function0;", "getOnAvatarEdit", "()Lkotlin/jvm/functions/Function0;", "setOnAvatarEdit", "(Lkotlin/jvm/functions/Function0;)V", "onBannerPress", "getOnBannerPress", "setOnBannerPress", "onAvatarRepresentativeColorUpdated", "getOnAvatarRepresentativeColorUpdated", "setOnAvatarRepresentativeColorUpdated", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "Companion", "BadgeViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileHeaderView extends ConstraintLayout {
    public static final Companion Companion = new Companion(null);
    private final SimpleRecyclerAdapter<Badge, BadgeViewHolder> badgesAdapter;
    private MGImages.DistinctChangeDetector bannerChangeDetector;
    private final UserProfileHeaderViewBinding binding;
    private Function0<Unit> onAvatarEdit;
    private Function1<? super Integer, Unit> onAvatarRepresentativeColorUpdated;
    private Function1<? super Badge, Unit> onBadgeClick;
    private Function0<Unit> onBannerPress;
    private Subscription setBannerBackgroundColorSubscription;
    private Subscription syncAvatarRepresentativeColorSubscription;
    private int userProfileHeaderBackgroundColor;

    /* compiled from: UserProfileHeaderView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u00060\u0004R\u00020\u00052\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "parent", "Lcom/discord/widgets/user/profile/UserProfileHeaderView$BadgeViewHolder;", "Lcom/discord/widgets/user/profile/UserProfileHeaderView;", "invoke", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/discord/widgets/user/profile/UserProfileHeaderView$BadgeViewHolder;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.profile.UserProfileHeaderView$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function2<LayoutInflater, ViewGroup, BadgeViewHolder> {
        public AnonymousClass1() {
            super(2);
        }

        public final BadgeViewHolder invoke(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            m.checkNotNullParameter(layoutInflater, "inflater");
            m.checkNotNullParameter(viewGroup, "parent");
            View inflate = layoutInflater.inflate(R.layout.user_profile_header_badge, viewGroup, false);
            Objects.requireNonNull(inflate, "rootView");
            ImageView imageView = (ImageView) inflate;
            UserProfileHeaderBadgeBinding userProfileHeaderBadgeBinding = new UserProfileHeaderBadgeBinding(imageView, imageView);
            m.checkNotNullExpressionValue(userProfileHeaderBadgeBinding, "UserProfileHeaderBadgeBi…(inflater, parent, false)");
            return new BadgeViewHolder(UserProfileHeaderView.this, userProfileHeaderBadgeBinding);
        }
    }

    /* compiled from: UserProfileHeaderView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderView$BadgeViewHolder;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;", "Lcom/discord/widgets/user/Badge;", "data", "", "bind", "(Lcom/discord/widgets/user/Badge;)V", "Lcom/discord/databinding/UserProfileHeaderBadgeBinding;", "binding", "Lcom/discord/databinding/UserProfileHeaderBadgeBinding;", HookHelper.constructorName, "(Lcom/discord/widgets/user/profile/UserProfileHeaderView;Lcom/discord/databinding/UserProfileHeaderBadgeBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class BadgeViewHolder extends SimpleRecyclerAdapter.ViewHolder<Badge> {
        private final UserProfileHeaderBadgeBinding binding;
        public final /* synthetic */ UserProfileHeaderView this$0;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public BadgeViewHolder(com.discord.widgets.user.profile.UserProfileHeaderView r2, com.discord.databinding.UserProfileHeaderBadgeBinding r3) {
            /*
                r1 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                r1.this$0 = r2
                android.widget.ImageView r2 = r3.a
                java.lang.String r0 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r2, r0)
                r1.<init>(r2)
                r1.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.profile.UserProfileHeaderView.BadgeViewHolder.<init>(com.discord.widgets.user.profile.UserProfileHeaderView, com.discord.databinding.UserProfileHeaderBadgeBinding):void");
        }

        public void bind(final Badge badge) {
            m.checkNotNullParameter(badge, "data");
            this.binding.f2148b.setImageResource(badge.getIcon());
            ImageView imageView = this.binding.f2148b;
            m.checkNotNullExpressionValue(imageView, "binding.userSheetBadgeImage");
            CharSequence text = badge.getText();
            if (text == null) {
                text = badge.getTooltip();
            }
            imageView.setContentDescription(text);
            this.binding.f2148b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileHeaderView$BadgeViewHolder$bind$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    UserProfileHeaderView.BadgeViewHolder.this.this$0.getOnBadgeClick().invoke(badge);
                }
            });
        }
    }

    /* compiled from: UserProfileHeaderView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ'\u0010\t\u001a\u00020\b*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;", "", "Lcom/discord/widgets/user/profile/UserProfileHeaderView;", "Lcom/discord/app/AppComponent;", "appComponent", "Lrx/Observable;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "observable", "", "bind", "(Lcom/discord/widgets/user/profile/UserProfileHeaderView;Lcom/discord/app/AppComponent;Lrx/Observable;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void bind(UserProfileHeaderView userProfileHeaderView, AppComponent appComponent, Observable<UserProfileHeaderViewModel.ViewState> observable) {
            m.checkNotNullParameter(userProfileHeaderView, "$this$bind");
            m.checkNotNullParameter(appComponent, "appComponent");
            m.checkNotNullParameter(observable, "observable");
            Observable<R> F = observable.x(UserProfileHeaderView$Companion$bind$$inlined$filterIs$1.INSTANCE).F(UserProfileHeaderView$Companion$bind$$inlined$filterIs$2.INSTANCE);
            m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(F, appComponent, null, 2, null), appComponent.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new UserProfileHeaderView$Companion$bind$1(userProfileHeaderView));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserProfileHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        View inflate = LayoutInflater.from(context).inflate(R.layout.user_profile_header_view, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.avatar_edit;
        CardView cardView = (CardView) inflate.findViewById(R.id.avatar_edit);
        if (cardView != null) {
            i = R.id.banner;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.banner);
            if (simpleDraweeView != null) {
                i = R.id.banner_edit;
                CardView cardView2 = (CardView) inflate.findViewById(R.id.banner_edit);
                if (cardView2 != null) {
                    i = R.id.gif_tag;
                    ImageView imageView = (ImageView) inflate.findViewById(R.id.gif_tag);
                    if (imageView != null) {
                        i = R.id.large_avatar;
                        UserAvatarPresenceView userAvatarPresenceView = (UserAvatarPresenceView) inflate.findViewById(R.id.large_avatar);
                        if (userAvatarPresenceView != null) {
                            i = R.id.large_avatar_barrier;
                            Barrier barrier = (Barrier) inflate.findViewById(R.id.large_avatar_barrier);
                            if (barrier != null) {
                                i = R.id.user_aka;
                                UserAkaView userAkaView = (UserAkaView) inflate.findViewById(R.id.user_aka);
                                if (userAkaView != null) {
                                    i = R.id.user_profile_header_badges_recycler;
                                    RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.user_profile_header_badges_recycler);
                                    if (recyclerView != null) {
                                        i = R.id.user_profile_header_custom_status;
                                        SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) inflate.findViewById(R.id.user_profile_header_custom_status);
                                        if (simpleDraweeSpanTextView != null) {
                                            i = R.id.user_profile_header_name_wrap;
                                            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.user_profile_header_name_wrap);
                                            if (linearLayout != null) {
                                                i = R.id.user_profile_header_primary_name;
                                                UsernameView usernameView = (UsernameView) inflate.findViewById(R.id.user_profile_header_primary_name);
                                                if (usernameView != null) {
                                                    i = R.id.user_profile_header_secondary_name;
                                                    SimpleDraweeSpanTextView simpleDraweeSpanTextView2 = (SimpleDraweeSpanTextView) inflate.findViewById(R.id.user_profile_header_secondary_name);
                                                    if (simpleDraweeSpanTextView2 != null) {
                                                        ConstraintLayout constraintLayout = (ConstraintLayout) inflate;
                                                        UserProfileHeaderViewBinding userProfileHeaderViewBinding = new UserProfileHeaderViewBinding(constraintLayout, cardView, simpleDraweeView, cardView2, imageView, userAvatarPresenceView, barrier, userAkaView, recyclerView, simpleDraweeSpanTextView, linearLayout, usernameView, simpleDraweeSpanTextView2, constraintLayout);
                                                        m.checkNotNullExpressionValue(userProfileHeaderViewBinding, "UserProfileHeaderViewBin…rom(context), this, true)");
                                                        this.binding = userProfileHeaderViewBinding;
                                                        this.bannerChangeDetector = new MGImages.DistinctChangeDetector();
                                                        this.onBadgeClick = UserProfileHeaderView$onBadgeClick$1.INSTANCE;
                                                        this.onAvatarEdit = UserProfileHeaderView$onAvatarEdit$1.INSTANCE;
                                                        this.onBannerPress = UserProfileHeaderView$onBannerPress$1.INSTANCE;
                                                        this.onAvatarRepresentativeColorUpdated = UserProfileHeaderView$onAvatarRepresentativeColorUpdated$1.INSTANCE;
                                                        RightToLeftGridLayoutManager rightToLeftGridLayoutManager = new RightToLeftGridLayoutManager(context, 3, 1, true);
                                                        m.checkNotNullExpressionValue(recyclerView, "binding.userProfileHeaderBadgesRecycler");
                                                        recyclerView.setLayoutManager(rightToLeftGridLayoutManager);
                                                        SimpleRecyclerAdapter<Badge, BadgeViewHolder> simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(null, new AnonymousClass1(), 1, null);
                                                        this.badgesAdapter = simpleRecyclerAdapter;
                                                        m.checkNotNullExpressionValue(recyclerView, "binding.userProfileHeaderBadgesRecycler");
                                                        recyclerView.setAdapter(simpleRecyclerAdapter);
                                                        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.UserProfileHeaderView);
                                                        m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…le.UserProfileHeaderView)");
                                                        this.userProfileHeaderBackgroundColor = obtainStyledAttributes.getColor(0, ColorCompat.getThemedColor(this, (int) R.attr.primary_700));
                                                        obtainStyledAttributes.recycle();
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    private final void configureAka(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        UserAkaView userAkaView = this.binding.g;
        m.checkNotNullExpressionValue(userAkaView, "binding.userAka");
        userAkaView.setVisibility(loaded.getShowAkas() ? 0 : 8);
        this.binding.g.configure(loaded.getGuildMembersForAka());
    }

    private final void configureBanner(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        String str;
        String str2;
        String str3;
        GuildMember guildMember = loaded.getGuildMember();
        if (guildMember == null || (str = guildMember.getBannerHash()) == null) {
            str = loaded.getBanner();
        }
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        int i = resources.getDisplayMetrics().widthPixels;
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        int max = Math.max(i, resources2.getDisplayMetrics().heightPixels);
        int i2 = 0;
        if (str == null || !t.startsWith$default(str, "data:", false, 2, null)) {
            GuildMember guildMember2 = loaded.getGuildMember();
            if (guildMember2 == null || !guildMember2.hasBanner()) {
                str3 = IconUtils.INSTANCE.getForUserBanner(loaded.getUser().getId(), str, Integer.valueOf(max), loaded.getShouldAnimateBanner());
            } else {
                IconUtils iconUtils = IconUtils.INSTANCE;
                GuildMember guildMember3 = loaded.getGuildMember();
                str3 = iconUtils.getForGuildMemberBanner(str, (guildMember3 != null ? Long.valueOf(guildMember3.getGuildId()) : null).longValue(), loaded.getUser().getId(), Integer.valueOf(max), loaded.getShouldAnimateBanner());
            }
            str2 = str3;
        } else {
            str2 = str;
        }
        boolean z2 = !(str2 == null || t.isBlank(str2));
        boolean z3 = (str != null && IconUtils.INSTANCE.isDataUrlForGif(str)) || (str != null && !t.startsWith$default(str, "data:", false, 2, null) && IconUtils.INSTANCE.isImageHashAnimated(str));
        SimpleDraweeView simpleDraweeView = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.banner");
        simpleDraweeView.setAspectRatio(!z2 ? 5.0f : 2.5f);
        Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ref$BooleanRef.element = false;
        notifyAvatarColorListenerAsync(loaded);
        if ((!z2 || z3) && loaded.isProfileLoaded()) {
            updateBannerColor(loaded);
            ref$BooleanRef.element = true;
        } else if (!loaded.isProfileLoaded()) {
            this.binding.c.setBackgroundColor(this.userProfileHeaderBackgroundColor);
        }
        SimpleDraweeView simpleDraweeView2 = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.banner");
        MGImages.setImage$default(simpleDraweeView2, str2, 0, 0, false, new UserProfileHeaderView$configureBanner$1(this, ref$BooleanRef, loaded), this.bannerChangeDetector, 28, null);
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileHeaderView$configureBanner$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UserProfileHeaderView.this.onTapBanner();
            }
        });
        this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileHeaderView$configureBanner$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UserProfileHeaderView.this.onTapBanner();
            }
        });
        ImageView imageView = this.binding.e;
        m.checkNotNullExpressionValue(imageView, "binding.gifTag");
        if (!loaded.getShouldShowGIFTag()) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
    }

    private final void configurePrimaryName(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        User user = loaded.getUser();
        GuildMember guildMember = loaded.getGuildMember();
        int dimension = (int) getResources().getDimension(R.dimen.avatar_size_medium);
        this.binding.j.b(getPrimaryNameTextForUser(user, guildMember), loaded.getShowMediumAvatar() ? IconUtils.getForUser(loaded.getUser(), false, Integer.valueOf(dimension)) : null, false, Integer.valueOf(dimension), Integer.valueOf(this.userProfileHeaderBackgroundColor));
        this.binding.j.a(user.isBot(), user.isSystemUser() ? R.string.system_dm_tag_system : R.string.bot_tag, UserUtils.INSTANCE.isVerifiedBot(user));
    }

    private final void configureSecondaryName(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        User user = loaded.getUser();
        GuildMember guildMember = loaded.getGuildMember();
        DraweeSpanStringBuilder draweeSpanStringBuilder = new DraweeSpanStringBuilder();
        int i = 0;
        if (loaded.getShowSmallAvatar()) {
            int dimension = (int) getResources().getDimension(R.dimen.avatar_size_profile_small);
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            DraweeSpanStringBuilderExtensionsKt.setAvatar$default(draweeSpanStringBuilder, context, IconUtils.getForUser(loaded.getUser(), false, Integer.valueOf(dimension)), false, Integer.valueOf(dimension), Integer.valueOf(this.userProfileHeaderBackgroundColor), null, 32, null);
        }
        CharSequence secondaryNameTextForUser = getSecondaryNameTextForUser(user, guildMember);
        boolean z2 = true;
        if ((secondaryNameTextForUser == null || t.isBlank(secondaryNameTextForUser)) || loaded.getShowAkas()) {
            z2 = false;
        }
        SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.k;
        m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.userProfileHeaderSecondaryName");
        if (!z2) {
            i = 8;
        }
        simpleDraweeSpanTextView.setVisibility(i);
        if (z2) {
            draweeSpanStringBuilder.append(secondaryNameTextForUser);
            this.binding.k.setDraweeSpanStringBuilder(draweeSpanStringBuilder);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void editAvatar(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        if (loaded.getEditable()) {
            this.onAvatarEdit.invoke();
        }
    }

    private final DraweeSpanStringBuilder getCustomStatusDraweeSpanStringBuilder(Activity activity, final boolean z2) {
        DraweeSpanStringBuilder draweeSpanStringBuilder = new DraweeSpanStringBuilder();
        ActivityEmoji f = activity.f();
        EmojiNode emojiNode = null;
        if (f != null) {
            EmojiNode.Companion companion = EmojiNode.Companion;
            emojiNode = EmojiNode.Companion.from$default(companion, 0, companion.generateEmojiIdAndType(f), 1, (Object) null);
        }
        if (emojiNode != null) {
            emojiNode.render((SpannableStringBuilder) draweeSpanStringBuilder, (DraweeSpanStringBuilder) new EmojiNode.RenderContext(z2) { // from class: com.discord.widgets.user.profile.UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1
                public final /* synthetic */ boolean $shouldAnimate;
                private final Context context;
                private final boolean isAnimationEnabled;

                {
                    this.$shouldAnimate = z2;
                    this.context = UserProfileHeaderView.this.getContext();
                    this.isAnimationEnabled = z2;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public Context getContext() {
                    return this.context;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public boolean isAnimationEnabled() {
                    return this.isAnimationEnabled;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public void onEmojiClicked(EmojiNode.EmojiIdAndType emojiIdAndType) {
                    m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
                    EmojiNode.RenderContext.DefaultImpls.onEmojiClicked(this, emojiIdAndType);
                }
            });
        }
        String l = activity.l();
        if (l != null) {
            if (emojiNode != null) {
                draweeSpanStringBuilder.append((char) 8194);
            }
            draweeSpanStringBuilder.append((CharSequence) l);
        }
        return draweeSpanStringBuilder;
    }

    private final SpannableStringBuilder getPrimaryNameTextForUser(User user, GuildMember guildMember) {
        String nick = guildMember != null ? guildMember.getNick() : null;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        return UserNameFormatterKt.getSpannableForUserNameWithDiscrim(user, nick, context, R.attr.colorHeaderPrimary, R.attr.font_display_bold, R.integer.uikit_textsize_xxlarge_sp, R.attr.colorHeaderSecondary, R.attr.font_primary_semibold, R.integer.uikit_textsize_xxlarge_sp);
    }

    private final CharSequence getSecondaryNameTextForUser(User user, GuildMember guildMember) {
        if (guildMember == null || guildMember.getNick() == null) {
            return null;
        }
        return UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user, null, null, 3, null);
    }

    private final void notifyAvatarColorListenerAsync(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        Subscription subscription = this.syncAvatarRepresentativeColorSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        ObservableExtensionsKt.appSubscribe(observeRepresentativeColor(loaded), UserProfileHeaderView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new UserProfileHeaderView$notifyAvatarColorListenerAsync$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new UserProfileHeaderView$notifyAvatarColorListenerAsync$2(this));
    }

    private final Observable<Integer> observeRepresentativeColor(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        Observable<Integer> observable;
        GuildMember guildMember = loaded.getGuildMember();
        if (guildMember == null || !guildMember.hasAvatar()) {
            observable = RepresentativeColorsKt.getUserRepresentativeColors().observeRepresentativeColor(loaded.getAvatarColorId());
        } else {
            observable = RepresentativeColorsKt.getGuildMemberRepresentativeColors().observeRepresentativeColor(loaded.getGuildMemberColorId());
        }
        Observable<Integer> b02 = ObservableExtensionsKt.ui(observable).a0(new ViewDetachedFromWindowObservable(this).observe()).b0(UserProfileHeaderView$observeRepresentativeColor$1.INSTANCE);
        m.checkNotNullExpressionValue(b02, "representativeColorObser….takeUntil { it != null }");
        return b02;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onTapBanner() {
        this.onBannerPress.invoke();
    }

    private final void updateBannerBackgroundColorAsync(UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        Subscription subscription = this.setBannerBackgroundColorSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        ObservableExtensionsKt.appSubscribe(observeRepresentativeColor(loaded), UserProfileHeaderView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new UserProfileHeaderView$updateBannerBackgroundColorAsync$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new UserProfileHeaderView$updateBannerBackgroundColorAsync$2(this));
    }

    public final Function0<Unit> getOnAvatarEdit() {
        return this.onAvatarEdit;
    }

    public final Function1<Integer, Unit> getOnAvatarRepresentativeColorUpdated() {
        return this.onAvatarRepresentativeColorUpdated;
    }

    public final Function1<Badge, Unit> getOnBadgeClick() {
        return this.onBadgeClick;
    }

    public final Function0<Unit> getOnBannerPress() {
        return this.onBannerPress;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        int i = this.userProfileHeaderBackgroundColor;
        if (i != 0) {
            setBackgroundColor(i);
            this.binding.f.setAvatarBackgroundColor(this.userProfileHeaderBackgroundColor);
            this.binding.c.setBackgroundColor(this.userProfileHeaderBackgroundColor);
        }
    }

    public final void setOnAvatarEdit(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onAvatarEdit = function0;
    }

    public final void setOnAvatarRepresentativeColorUpdated(Function1<? super Integer, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onAvatarRepresentativeColorUpdated = function1;
    }

    public final void setOnBadgeClick(Function1<? super Badge, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onBadgeClick = function1;
    }

    public final void setOnBannerPress(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onBannerPress = function0;
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARN: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:9:0x0027  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void updateBannerColor(com.discord.widgets.user.profile.UserProfileHeaderViewModel.ViewState.Loaded r9) {
        /*
            r8 = this;
            java.lang.String r0 = "viewState"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            java.lang.String r0 = r9.getBannerColorHex()
            if (r0 == 0) goto L24
            int r1 = android.graphics.Color.parseColor(r0)     // Catch: java.lang.IllegalArgumentException -> L14
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)     // Catch: java.lang.IllegalArgumentException -> L14
            goto L25
        L14:
            r1 = move-exception
            r4 = r1
            com.discord.app.AppLog r2 = com.discord.app.AppLog.g
            java.lang.String r1 = "failed to parse banner color string: "
            java.lang.String r3 = b.d.b.a.a.v(r1, r0)
            r5 = 0
            r6 = 4
            r7 = 0
            com.discord.utilities.logging.Logger.e$default(r2, r3, r4, r5, r6, r7)
        L24:
            r0 = 0
        L25:
            if (r0 == 0) goto L32
            com.discord.databinding.UserProfileHeaderViewBinding r1 = r8.binding
            com.facebook.drawee.view.SimpleDraweeView r1 = r1.c
            int r2 = r0.intValue()
            r1.setBackgroundColor(r2)
        L32:
            if (r0 != 0) goto L37
            r8.updateBannerBackgroundColorAsync(r9)
        L37:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.profile.UserProfileHeaderView.updateBannerColor(com.discord.widgets.user.profile.UserProfileHeaderViewModel$ViewState$Loaded):void");
    }

    public final void updateViewState(final UserProfileHeaderViewModel.ViewState.Loaded loaded) {
        m.checkNotNullParameter(loaded, "viewState");
        this.binding.f.setOnAvatarBitmapLoadedListener(new UserProfileHeaderView$updateViewState$1(loaded));
        this.binding.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileHeaderView$updateViewState$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UserProfileHeaderView.this.editAvatar(loaded);
            }
        });
        this.binding.f2149b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileHeaderView$updateViewState$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UserProfileHeaderView.this.editAvatar(loaded);
            }
        });
        configureBanner(loaded);
        this.binding.f.a(new UserAvatarPresenceView.a(loaded.getUser(), loaded.getPresence(), loaded.getStreamContext(), loaded.getShowPresence(), loaded.getGuildMember()));
        configurePrimaryName(loaded);
        configureAka(loaded);
        configureSecondaryName(loaded);
        Badge.Companion companion = Badge.Companion;
        User user = loaded.getUser();
        UserProfile userProfile = loaded.getUserProfile();
        boolean isMeUserPremium = loaded.isMeUserPremium();
        boolean isMeUserVerified = loaded.isMeUserVerified();
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        this.badgesAdapter.setData(companion.getBadgesForUser(user, userProfile, isMeUserPremium, isMeUserVerified, context));
        Presence presence = loaded.getPresence();
        Activity customStatusActivity = presence != null ? PresenceUtils.INSTANCE.getCustomStatusActivity(presence) : null;
        if (customStatusActivity != null) {
            this.binding.i.setDraweeSpanStringBuilder(getCustomStatusDraweeSpanStringBuilder(customStatusActivity, loaded.getAllowAnimatedEmojis()));
        }
        SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.i;
        m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.userProfileHeaderCustomStatus");
        int i = 0;
        simpleDraweeSpanTextView.setVisibility(customStatusActivity != null ? 0 : 8);
        CardView cardView = this.binding.f2149b;
        m.checkNotNullExpressionValue(cardView, "binding.avatarEdit");
        cardView.setVisibility(loaded.getEditable() ? 0 : 8);
        CardView cardView2 = this.binding.d;
        m.checkNotNullExpressionValue(cardView2, "binding.bannerEdit");
        if (!loaded.getEditable()) {
            i = 8;
        }
        cardView2.setVisibility(i);
    }
}
