package com.discord.widgets.user.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.databinding.UserProfileConnectionsViewBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.user.profile.UserProfileConnectionsView;
import d0.z.d.m;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function3;
import xyz.discord.R;
/* compiled from: UserProfileConnectionsView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0004\u001c\u001d\u001e\u001fB\u0017\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0003\u0010\u0004JS\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052 \u0010\n\u001a\u001c\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0012\u0006\u0012\u0004\u0018\u00010\b\u0012\u0004\u0012\u00020\u00020\u00072\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006 "}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileConnectionsView;", "Landroid/widget/LinearLayout;", "", "onAttachedToWindow", "()V", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "viewState", "Lkotlin/Function3;", "", "", "onConnectedAccountClick", "Lkotlin/Function0;", "onMutualGuildsItemClick", "onMutualFriendsItemClick", "updateViewState", "(Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountsAdapter;", "connectedAccountsAdapter", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountsAdapter;", "Lcom/discord/databinding/UserProfileConnectionsViewBinding;", "binding", "Lcom/discord/databinding/UserProfileConnectionsViewBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "ConnectedAccountItem", "ConnectedAccountsAdapter", "ViewHolder", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileConnectionsView extends LinearLayout {
    private final UserProfileConnectionsViewBinding binding;
    private final ConnectedAccountsAdapter connectedAccountsAdapter;

    /* compiled from: UserProfileConnectionsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001c\u0010\u0018\u001a\u00020\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\r¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component1", "()Lcom/discord/api/connectedaccounts/ConnectedAccount;", "connectedAccount", "copy", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;)Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "getConnectedAccount", "type", "I", "getType", HookHelper.constructorName, "(Lcom/discord/api/connectedaccounts/ConnectedAccount;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ConnectedAccountItem implements MGRecyclerDataPayload {
        private final ConnectedAccount connectedAccount;
        private final String key;
        private final int type;

        public ConnectedAccountItem(ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(connectedAccount, "connectedAccount");
            this.connectedAccount = connectedAccount;
            this.key = connectedAccount.b();
        }

        public static /* synthetic */ ConnectedAccountItem copy$default(ConnectedAccountItem connectedAccountItem, ConnectedAccount connectedAccount, int i, Object obj) {
            if ((i & 1) != 0) {
                connectedAccount = connectedAccountItem.connectedAccount;
            }
            return connectedAccountItem.copy(connectedAccount);
        }

        public final ConnectedAccount component1() {
            return this.connectedAccount;
        }

        public final ConnectedAccountItem copy(ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(connectedAccount, "connectedAccount");
            return new ConnectedAccountItem(connectedAccount);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ConnectedAccountItem) && m.areEqual(this.connectedAccount, ((ConnectedAccountItem) obj).connectedAccount);
            }
            return true;
        }

        public final ConnectedAccount getConnectedAccount() {
            return this.connectedAccount;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            ConnectedAccount connectedAccount = this.connectedAccount;
            if (connectedAccount != null) {
                return connectedAccount.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("ConnectedAccountItem(connectedAccount=");
            R.append(this.connectedAccount);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: UserProfileConnectionsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016J+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR<\u0010\r\u001a\u001c\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\u0004\u0012\u00020\f0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountsAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function3;", "", "", "onConnectedAccountClick", "Lkotlin/jvm/functions/Function3;", "getOnConnectedAccountClick", "()Lkotlin/jvm/functions/Function3;", "setOnConnectedAccountClick", "(Lkotlin/jvm/functions/Function3;)V", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ConnectedAccountsAdapter extends MGRecyclerAdapterSimple<ConnectedAccountItem> {
        private Function3<? super String, ? super Integer, ? super String, Unit> onConnectedAccountClick = UserProfileConnectionsView$ConnectedAccountsAdapter$onConnectedAccountClick$1.INSTANCE;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ConnectedAccountsAdapter(RecyclerView recyclerView) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recyclerView");
        }

        public final Function3<String, Integer, String, Unit> getOnConnectedAccountClick() {
            return this.onConnectedAccountClick;
        }

        public final void setOnConnectedAccountClick(Function3<? super String, ? super Integer, ? super String, Unit> function3) {
            m.checkNotNullParameter(function3, "<set-?>");
            this.onConnectedAccountClick = function3;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public MGRecyclerViewHolder<ConnectedAccountsAdapter, ConnectedAccountItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            return new ViewHolder(R.layout.icon_list_item_text_view, this);
        }
    }

    /* compiled from: UserProfileConnectionsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountsAdapter;", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;)V", "Landroid/widget/TextView;", "listItemTextView", "Landroid/widget/TextView;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountsAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewHolder extends MGRecyclerViewHolder<ConnectedAccountsAdapter, ConnectedAccountItem> {
        private final TextView listItemTextView;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(@LayoutRes int i, ConnectedAccountsAdapter connectedAccountsAdapter) {
            super(i, connectedAccountsAdapter);
            m.checkNotNullParameter(connectedAccountsAdapter, "adapter");
            View view = this.itemView;
            Objects.requireNonNull(view, "null cannot be cast to non-null type android.widget.TextView");
            this.listItemTextView = (TextView) view;
        }

        public static final /* synthetic */ ConnectedAccountsAdapter access$getAdapter$p(ViewHolder viewHolder) {
            return (ConnectedAccountsAdapter) viewHolder.adapter;
        }

        public void onConfigure(int i, ConnectedAccountItem connectedAccountItem) {
            m.checkNotNullParameter(connectedAccountItem, "data");
            super.onConfigure(i, (int) connectedAccountItem);
            ConnectedAccount connectedAccount = connectedAccountItem.getConnectedAccount();
            final String d = connectedAccount.d();
            final Platform from = Platform.Companion.from(connectedAccount);
            Integer themedPlatformImage = from.getThemedPlatformImage();
            int intValue = themedPlatformImage != null ? themedPlatformImage.intValue() : 0;
            final String profileUrl = from.getProfileUrl(connectedAccount);
            TextView textView = this.listItemTextView;
            Integer themedPlatformImage2 = from.getThemedPlatformImage();
            ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView, DrawableCompat.getThemedDrawableRes$default(textView, themedPlatformImage2 != null ? themedPlatformImage2.intValue() : 0, 0, 2, (Object) null), 0, 0, 0, 14, null);
            this.listItemTextView.setText(connectedAccount.d());
            TextView textView2 = this.listItemTextView;
            String format = String.format("%s, %s", Arrays.copyOf(new Object[]{connectedAccount.g(), connectedAccount.d()}, 2));
            m.checkNotNullExpressionValue(format, "java.lang.String.format(format, *args)");
            textView2.setContentDescription(format);
            final int i2 = intValue;
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileConnectionsView$ViewHolder$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AnalyticsTracker.connectedAccountViewed(from.getPlatformId());
                    UserProfileConnectionsView.ViewHolder.access$getAdapter$p(UserProfileConnectionsView.ViewHolder.this).getOnConnectedAccountClick().invoke(d, Integer.valueOf(i2), profileUrl);
                }
            });
        }
    }

    /* compiled from: UserProfileConnectionsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ4\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0016\u001a\u00020\u00022\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\tR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001c\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "", "", "component1", "()Z", "component2", "", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;", "component3", "()Ljava/util/List;", "showConnectionsSection", "showMutualGuildsAndFriends", "connectedAccountItems", "copy", "(ZZLjava/util/List;)Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getConnectedAccountItems", "Z", "getShowMutualGuildsAndFriends", "getShowConnectionsSection", HookHelper.constructorName, "(ZZLjava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final List<ConnectedAccountItem> connectedAccountItems;
        private final boolean showConnectionsSection;
        private final boolean showMutualGuildsAndFriends;

        public ViewState(boolean z2, boolean z3, List<ConnectedAccountItem> list) {
            m.checkNotNullParameter(list, "connectedAccountItems");
            this.showConnectionsSection = z2;
            this.showMutualGuildsAndFriends = z3;
            this.connectedAccountItems = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, boolean z2, boolean z3, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = viewState.showConnectionsSection;
            }
            if ((i & 2) != 0) {
                z3 = viewState.showMutualGuildsAndFriends;
            }
            if ((i & 4) != 0) {
                list = viewState.connectedAccountItems;
            }
            return viewState.copy(z2, z3, list);
        }

        public final boolean component1() {
            return this.showConnectionsSection;
        }

        public final boolean component2() {
            return this.showMutualGuildsAndFriends;
        }

        public final List<ConnectedAccountItem> component3() {
            return this.connectedAccountItems;
        }

        public final ViewState copy(boolean z2, boolean z3, List<ConnectedAccountItem> list) {
            m.checkNotNullParameter(list, "connectedAccountItems");
            return new ViewState(z2, z3, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return this.showConnectionsSection == viewState.showConnectionsSection && this.showMutualGuildsAndFriends == viewState.showMutualGuildsAndFriends && m.areEqual(this.connectedAccountItems, viewState.connectedAccountItems);
        }

        public final List<ConnectedAccountItem> getConnectedAccountItems() {
            return this.connectedAccountItems;
        }

        public final boolean getShowConnectionsSection() {
            return this.showConnectionsSection;
        }

        public final boolean getShowMutualGuildsAndFriends() {
            return this.showMutualGuildsAndFriends;
        }

        public int hashCode() {
            boolean z2 = this.showConnectionsSection;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.showMutualGuildsAndFriends;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            int i5 = (i4 + i) * 31;
            List<ConnectedAccountItem> list = this.connectedAccountItems;
            return i5 + (list != null ? list.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(showConnectionsSection=");
            R.append(this.showConnectionsSection);
            R.append(", showMutualGuildsAndFriends=");
            R.append(this.showMutualGuildsAndFriends);
            R.append(", connectedAccountItems=");
            return a.K(R, this.connectedAccountItems, ")");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserProfileConnectionsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        View inflate = LayoutInflater.from(context).inflate(R.layout.user_profile_connections_view, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.user_profile_connections_mutual_friends_item;
        TextView textView = (TextView) inflate.findViewById(R.id.user_profile_connections_mutual_friends_item);
        if (textView != null) {
            i = R.id.user_profile_connections_mutual_guilds_item;
            TextView textView2 = (TextView) inflate.findViewById(R.id.user_profile_connections_mutual_guilds_item);
            if (textView2 != null) {
                i = R.id.user_profile_connections_view_recycler;
                RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.user_profile_connections_view_recycler);
                if (recyclerView != null) {
                    UserProfileConnectionsViewBinding userProfileConnectionsViewBinding = new UserProfileConnectionsViewBinding((LinearLayout) inflate, textView, textView2, recyclerView);
                    m.checkNotNullExpressionValue(userProfileConnectionsViewBinding, "UserProfileConnectionsVi…rom(context), this, true)");
                    this.binding = userProfileConnectionsViewBinding;
                    MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
                    m.checkNotNullExpressionValue(recyclerView, "binding.userProfileConnectionsViewRecycler");
                    this.connectedAccountsAdapter = (ConnectedAccountsAdapter) companion.configure(new ConnectedAccountsAdapter(recyclerView));
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.binding.d.setHasFixedSize(false);
        ViewCompat.setNestedScrollingEnabled(this.binding.d, false);
    }

    public final void updateViewState(ViewState viewState, Function3<? super String, ? super Integer, ? super String, Unit> function3, final Function0<Unit> function0, final Function0<Unit> function02) {
        m.checkNotNullParameter(viewState, "viewState");
        m.checkNotNullParameter(function3, "onConnectedAccountClick");
        m.checkNotNullParameter(function0, "onMutualGuildsItemClick");
        m.checkNotNullParameter(function02, "onMutualFriendsItemClick");
        this.connectedAccountsAdapter.setOnConnectedAccountClick(new UserProfileConnectionsView$updateViewState$1(function3));
        this.connectedAccountsAdapter.setData(viewState.getConnectedAccountItems());
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileConnectionsView$updateViewState$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.userProfileConnectionsMutualGuildsItem");
        int i = 0;
        textView.setVisibility(viewState.getShowMutualGuildsAndFriends() ? 0 : 8);
        this.binding.f2147b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.profile.UserProfileConnectionsView$updateViewState$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        TextView textView2 = this.binding.f2147b;
        m.checkNotNullExpressionValue(textView2, "binding.userProfileConnectionsMutualFriendsItem");
        if (!viewState.getShowMutualGuildsAndFriends()) {
            i = 8;
        }
        textView2.setVisibility(i);
    }
}
