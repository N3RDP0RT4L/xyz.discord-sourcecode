package com.discord.widgets.user.usersheet;

import b.a.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheet$onViewCreated$2 extends o implements Function0<Unit> {
    public final /* synthetic */ boolean $isMe;
    public final /* synthetic */ WidgetUserSheet this$0;

    /* compiled from: WidgetUserSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheet$onViewCreated$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public AnonymousClass1() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            m.g(WidgetUserSheet$onViewCreated$2.this.this$0.getContext(), R.string.stage_channel_permission_microphone_denied, 0, null, 12);
        }
    }

    /* compiled from: WidgetUserSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheet$onViewCreated$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function0<Unit> {
        public AnonymousClass2() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            WidgetUserSheetViewModel viewModel;
            viewModel = WidgetUserSheet$onViewCreated$2.this.this$0.getViewModel();
            viewModel.setUserSuppressedInChannel(false);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheet$onViewCreated$2(WidgetUserSheet widgetUserSheet, boolean z2) {
        super(0);
        this.this$0 = widgetUserSheet;
        this.$isMe = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetUserSheetViewModel viewModel;
        if (this.$isMe) {
            this.this$0.requestMicrophone(new AnonymousClass1(), new AnonymousClass2());
            return;
        }
        viewModel = this.this$0.getViewModel();
        viewModel.inviteUserToSpeak();
    }
}
