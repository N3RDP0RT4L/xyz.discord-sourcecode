package com.discord.widgets.user.usersheet;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.role.GuildRole;
import com.discord.api.user.UserProfile;
import com.discord.api.voice.state.StageRequestToSpeakState;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserNotes;
import com.discord.stores.StoreUserPresence;
import com.discord.stores.StoreUserProfile;
import com.discord.stores.StoreUserRelationships;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceStates;
import com.discord.utilities.rx.LeadingEdgeThrottle;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.streams.StreamContextService;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.user.presence.ModelRichPresence;
import com.discord.widgets.user.usersheet.WidgetUserSheetViewModel;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.a.r;
import j0.l.e.k;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function16;
import kotlin.jvm.functions.Function4;
import rx.Observable;
import rx.Scheduler;
import rx.functions.Func4;
/* compiled from: WidgetUserSheetViewModelStoreState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001+B\t\b\u0002¢\u0006\u0004\b)\u0010*JÍ\u0001\u0010'\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010&0%2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\u000e\u0010\b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00072\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u0010\u001a\u00020\u000f2\b\b\u0002\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u00152\b\b\u0002\u0010\u0018\u001a\u00020\u00172\b\b\u0002\u0010\u001a\u001a\u00020\u00192\b\b\u0002\u0010\u001c\u001a\u00020\u001b2\b\b\u0002\u0010\u001e\u001a\u00020\u001d2\b\b\u0002\u0010 \u001a\u00020\u001f2\b\b\u0002\u0010\"\u001a\u00020!2\b\b\u0002\u0010$\u001a\u00020#¢\u0006\u0004\b'\u0010(¨\u0006,"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelStoreState;", "", "", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreVoiceChannelSelected;", "storeVoiceChannelSelected", "Lcom/discord/stores/StoreUserProfile;", "storeUserProfile", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StoreVoiceStates;", "storeVoiceStates", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreMediaSettings;", "storeMediaSettings", "Lcom/discord/stores/StoreUserPresence;", "storeUserPresence", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StoreUserNotes;", "storeUserNotes", "Lcom/discord/stores/StoreStageChannels;", "storeStageChannels", "Lcom/discord/utilities/streams/StreamContextService;", "streamContextService", "Lrx/Scheduler;", "storeStateRxScheduler", "Lrx/Observable;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "observeStoreState", "(JLjava/lang/Long;Ljava/lang/Long;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserNotes;Lcom/discord/stores/StoreStageChannels;Lcom/discord/utilities/streams/StreamContextService;Lrx/Scheduler;)Lrx/Observable;", HookHelper.constructorName, "()V", "BootstrapData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheetViewModelStoreState {
    public static final WidgetUserSheetViewModelStoreState INSTANCE = new WidgetUserSheetViewModelStoreState();

    /* compiled from: WidgetUserSheetViewModelStoreState.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0082\b\u0018\u00002\u00020\u0001B-\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b#\u0010$J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ>\u0010\u0010\u001a\u00020\u00002\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u001d\u0010\nR\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b \u0010\nR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010!\u001a\u0004\b\"\u0010\u0004¨\u0006%"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelStoreState$BootstrapData;", "", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "component4", "user", "me", "channel", "selectedVoiceChannel", "copy", "(Lcom/discord/models/user/User;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelStoreState$BootstrapData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/user/MeUser;", "getMe", "getSelectedVoiceChannel", "Lcom/discord/models/user/User;", "getUser", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BootstrapData {
        private final Channel channel;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2849me;
        private final Channel selectedVoiceChannel;
        private final User user;

        public BootstrapData(User user, MeUser meUser, Channel channel, Channel channel2) {
            m.checkNotNullParameter(meUser, "me");
            this.user = user;
            this.f2849me = meUser;
            this.channel = channel;
            this.selectedVoiceChannel = channel2;
        }

        public static /* synthetic */ BootstrapData copy$default(BootstrapData bootstrapData, User user, MeUser meUser, Channel channel, Channel channel2, int i, Object obj) {
            if ((i & 1) != 0) {
                user = bootstrapData.user;
            }
            if ((i & 2) != 0) {
                meUser = bootstrapData.f2849me;
            }
            if ((i & 4) != 0) {
                channel = bootstrapData.channel;
            }
            if ((i & 8) != 0) {
                channel2 = bootstrapData.selectedVoiceChannel;
            }
            return bootstrapData.copy(user, meUser, channel, channel2);
        }

        public final User component1() {
            return this.user;
        }

        public final MeUser component2() {
            return this.f2849me;
        }

        public final Channel component3() {
            return this.channel;
        }

        public final Channel component4() {
            return this.selectedVoiceChannel;
        }

        public final BootstrapData copy(User user, MeUser meUser, Channel channel, Channel channel2) {
            m.checkNotNullParameter(meUser, "me");
            return new BootstrapData(user, meUser, channel, channel2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BootstrapData)) {
                return false;
            }
            BootstrapData bootstrapData = (BootstrapData) obj;
            return m.areEqual(this.user, bootstrapData.user) && m.areEqual(this.f2849me, bootstrapData.f2849me) && m.areEqual(this.channel, bootstrapData.channel) && m.areEqual(this.selectedVoiceChannel, bootstrapData.selectedVoiceChannel);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final MeUser getMe() {
            return this.f2849me;
        }

        public final Channel getSelectedVoiceChannel() {
            return this.selectedVoiceChannel;
        }

        public final User getUser() {
            return this.user;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            MeUser meUser = this.f2849me;
            int hashCode2 = (hashCode + (meUser != null ? meUser.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode3 = (hashCode2 + (channel != null ? channel.hashCode() : 0)) * 31;
            Channel channel2 = this.selectedVoiceChannel;
            if (channel2 != null) {
                i = channel2.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("BootstrapData(user=");
            R.append(this.user);
            R.append(", me=");
            R.append(this.f2849me);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", selectedVoiceChannel=");
            R.append(this.selectedVoiceChannel);
            R.append(")");
            return R.toString();
        }
    }

    private WidgetUserSheetViewModelStoreState() {
    }

    public static /* synthetic */ Observable observeStoreState$default(WidgetUserSheetViewModelStoreState widgetUserSheetViewModelStoreState, long j, Long l, Long l2, StoreUser storeUser, StoreChannels storeChannels, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreUserProfile storeUserProfile, StoreUserRelationships storeUserRelationships, StoreVoiceStates storeVoiceStates, StoreGuilds storeGuilds, StoreMediaSettings storeMediaSettings, StoreUserPresence storeUserPresence, StorePermissions storePermissions, StoreUserNotes storeUserNotes, StoreStageChannels storeStageChannels, StreamContextService streamContextService, Scheduler scheduler, int i, Object obj) {
        Scheduler scheduler2;
        StoreUser users = (i & 8) != 0 ? StoreStream.Companion.getUsers() : storeUser;
        StoreChannels channels = (i & 16) != 0 ? StoreStream.Companion.getChannels() : storeChannels;
        StoreVoiceChannelSelected voiceChannelSelected = (i & 32) != 0 ? StoreStream.Companion.getVoiceChannelSelected() : storeVoiceChannelSelected;
        StoreUserProfile userProfile = (i & 64) != 0 ? StoreStream.Companion.getUserProfile() : storeUserProfile;
        StoreUserRelationships userRelationships = (i & 128) != 0 ? StoreStream.Companion.getUserRelationships() : storeUserRelationships;
        StoreVoiceStates voiceStates = (i & 256) != 0 ? StoreStream.Companion.getVoiceStates() : storeVoiceStates;
        StoreGuilds guilds = (i & 512) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds;
        StoreMediaSettings mediaSettings = (i & 1024) != 0 ? StoreStream.Companion.getMediaSettings() : storeMediaSettings;
        StoreUserPresence presences = (i & 2048) != 0 ? StoreStream.Companion.getPresences() : storeUserPresence;
        StorePermissions permissions = (i & 4096) != 0 ? StoreStream.Companion.getPermissions() : storePermissions;
        StoreUserNotes usersNotes = (i & 8192) != 0 ? StoreStream.Companion.getUsersNotes() : storeUserNotes;
        StoreStageChannels stageChannels = (i & 16384) != 0 ? StoreStream.Companion.getStageChannels() : storeStageChannels;
        StreamContextService streamContextService2 = (32768 & i) != 0 ? new StreamContextService(null, null, null, null, null, null, null, null, 255, null) : streamContextService;
        if ((i & 65536) != 0) {
            Scheduler a = j0.p.a.a();
            m.checkNotNullExpressionValue(a, "Schedulers.computation()");
            scheduler2 = a;
        } else {
            scheduler2 = scheduler;
        }
        return widgetUserSheetViewModelStoreState.observeStoreState(j, l, l2, users, channels, voiceChannelSelected, userProfile, userRelationships, voiceStates, guilds, mediaSettings, presences, permissions, usersNotes, stageChannels, streamContextService2, scheduler2);
    }

    public final Observable<WidgetUserSheetViewModel.StoreState> observeStoreState(long j, Long l, final Long l2, StoreUser storeUser, StoreChannels storeChannels, StoreVoiceChannelSelected storeVoiceChannelSelected, final StoreUserProfile storeUserProfile, final StoreUserRelationships storeUserRelationships, final StoreVoiceStates storeVoiceStates, final StoreGuilds storeGuilds, final StoreMediaSettings storeMediaSettings, final StoreUserPresence storeUserPresence, final StorePermissions storePermissions, final StoreUserNotes storeUserNotes, final StoreStageChannels storeStageChannels, final StreamContextService streamContextService, Scheduler scheduler) {
        Observable<Channel> observable;
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeVoiceChannelSelected, "storeVoiceChannelSelected");
        m.checkNotNullParameter(storeUserProfile, "storeUserProfile");
        m.checkNotNullParameter(storeUserRelationships, "storeUserRelationships");
        m.checkNotNullParameter(storeVoiceStates, "storeVoiceStates");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeMediaSettings, "storeMediaSettings");
        m.checkNotNullParameter(storeUserPresence, "storeUserPresence");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(storeUserNotes, "storeUserNotes");
        m.checkNotNullParameter(storeStageChannels, "storeStageChannels");
        m.checkNotNullParameter(streamContextService, "streamContextService");
        m.checkNotNullParameter(scheduler, "storeStateRxScheduler");
        Observable<User> observeUser = storeUser.observeUser(j);
        Observable observeMe$default = StoreUser.observeMe$default(storeUser, false, 1, null);
        if (l == null || (observable = storeChannels.observeChannel(l.longValue())) == null) {
            observable = new k<>(null);
        }
        Observable<Channel> observeSelectedChannel = storeVoiceChannelSelected.observeSelectedChannel();
        final WidgetUserSheetViewModelStoreState$observeStoreState$2 widgetUserSheetViewModelStoreState$observeStoreState$2 = WidgetUserSheetViewModelStoreState$observeStoreState$2.INSTANCE;
        Object obj = widgetUserSheetViewModelStoreState$observeStoreState$2;
        if (widgetUserSheetViewModelStoreState$observeStoreState$2 != null) {
            obj = new Func4() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState$sam$rx_functions_Func4$0
                @Override // rx.functions.Func4
                public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                    return Function4.this.invoke(obj2, obj3, obj4, obj5);
                }
            };
        }
        Observable<WidgetUserSheetViewModel.StoreState> q = Observable.h0(new r(Observable.h(observeUser, observeMe$default, observable, observeSelectedChannel, (Func4) obj).Y(new b<BootstrapData, Observable<? extends WidgetUserSheetViewModel.StoreState>>() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState$observeStoreState$3

            /* compiled from: WidgetUserSheetViewModelStoreState.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0001\n\u0002\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Long;)Ljava/lang/Void;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState$observeStoreState$3$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1<T, R> implements b {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public final Void call(Long l) {
                    return null;
                }
            }

            /* compiled from: WidgetUserSheetViewModelStoreState.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010'\u001a\u00020$2\u001a\u0010\u0005\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00002\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00002\u0016\u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\t0\u00002\u0016\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\t0\u00002\u0006\u0010\r\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u000e\u0010\u0013\u001a\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00122\b\u0010\u0015\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0017\u001a\u00020\u00162\u000e\u0010\u001a\u001a\n\u0018\u00010\u0018j\u0004\u0018\u0001`\u00192\u0006\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001d2\u0006\u0010 \u001a\u00020\u001f2\b\u0010!\u001a\u0004\u0018\u00010\u001d2\u0006\u0010#\u001a\u00020\"H\n¢\u0006\u0004\b%\u0010&"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "computedMembers", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "Lcom/discord/api/voice/state/VoiceState;", "mySelectedVoiceChannelVoiceStates", "currentChannelVoiceStates", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "voiceConfig", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "richPresence", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "Lcom/discord/api/user/UserProfile;", "userProfile", "", "Lcom/discord/primitives/RelationshipType;", "userRelationshipType", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", "userNote", "Lcom/discord/widgets/stage/StageRoles;", "userStageRoles", "Lcom/discord/api/voice/state/StageRequestToSpeakState;", "userRequestToSpeakState", "myStageRoles", "", "canDisableCommunication", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "invoke", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/models/guild/Guild;Ljava/lang/Long;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/user/UserProfile;Ljava/lang/Integer;Lcom/discord/stores/StoreUserNotes$UserNoteState;Lcom/discord/widgets/stage/StageRoles;Lcom/discord/api/voice/state/StageRequestToSpeakState;Lcom/discord/widgets/stage/StageRoles;Z)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState$observeStoreState$3$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends o implements Function16<Map<Long, ? extends GuildMember>, Map<Long, ? extends GuildRole>, Map<Long, ? extends VoiceState>, Map<Long, ? extends VoiceState>, StoreMediaSettings.VoiceConfiguration, ModelRichPresence, Guild, Long, StreamContext, UserProfile, Integer, StoreUserNotes.UserNoteState, StageRoles, StageRequestToSpeakState, StageRoles, Boolean, WidgetUserSheetViewModel.StoreState> {
                public final /* synthetic */ Channel $channel;
                public final /* synthetic */ MeUser $me;
                public final /* synthetic */ Channel $selectedVoiceChannel;
                public final /* synthetic */ User $user;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass2(User user, MeUser meUser, Channel channel, Channel channel2) {
                    super(16);
                    this.$user = user;
                    this.$me = meUser;
                    this.$channel = channel;
                    this.$selectedVoiceChannel = channel2;
                }

                @Override // kotlin.jvm.functions.Function16
                public /* bridge */ /* synthetic */ WidgetUserSheetViewModel.StoreState invoke(Map<Long, ? extends GuildMember> map, Map<Long, ? extends GuildRole> map2, Map<Long, ? extends VoiceState> map3, Map<Long, ? extends VoiceState> map4, StoreMediaSettings.VoiceConfiguration voiceConfiguration, ModelRichPresence modelRichPresence, Guild guild, Long l, StreamContext streamContext, UserProfile userProfile, Integer num, StoreUserNotes.UserNoteState userNoteState, StageRoles stageRoles, StageRequestToSpeakState stageRequestToSpeakState, StageRoles stageRoles2, Boolean bool) {
                    return invoke((Map<Long, GuildMember>) map, (Map<Long, GuildRole>) map2, (Map<Long, VoiceState>) map3, (Map<Long, VoiceState>) map4, voiceConfiguration, modelRichPresence, guild, l, streamContext, userProfile, num, userNoteState, stageRoles, stageRequestToSpeakState, stageRoles2, bool.booleanValue());
                }

                public final WidgetUserSheetViewModel.StoreState invoke(Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, VoiceState> map3, Map<Long, VoiceState> map4, StoreMediaSettings.VoiceConfiguration voiceConfiguration, ModelRichPresence modelRichPresence, Guild guild, Long l, StreamContext streamContext, UserProfile userProfile, Integer num, StoreUserNotes.UserNoteState userNoteState, StageRoles stageRoles, StageRequestToSpeakState stageRequestToSpeakState, StageRoles stageRoles2, boolean z2) {
                    m.checkNotNullParameter(map, "computedMembers");
                    m.checkNotNullParameter(map2, "guildRoles");
                    m.checkNotNullParameter(map3, "mySelectedVoiceChannelVoiceStates");
                    m.checkNotNullParameter(map4, "currentChannelVoiceStates");
                    m.checkNotNullParameter(voiceConfiguration, "voiceConfig");
                    m.checkNotNullParameter(userProfile, "userProfile");
                    m.checkNotNullParameter(userNoteState, "userNote");
                    m.checkNotNullParameter(stageRequestToSpeakState, "userRequestToSpeakState");
                    Boolean bool = (Boolean) a.e(this.$user, voiceConfiguration.getMutedUsers());
                    boolean booleanValue = bool != null ? bool.booleanValue() : false;
                    boolean isSelfMuted = voiceConfiguration.isSelfMuted();
                    boolean isSelfDeafened = voiceConfiguration.isSelfDeafened();
                    Float f = (Float) a.e(this.$user, voiceConfiguration.getUserOutputVolumes());
                    float floatValue = f != null ? f.floatValue() : 100.0f;
                    User user = this.$user;
                    MeUser meUser = this.$me;
                    Channel channel = this.$channel;
                    Channel channel2 = this.$selectedVoiceChannel;
                    Channel channel3 = null;
                    if (channel2 != null && ChannelUtils.z(channel2)) {
                        channel3 = channel2;
                    }
                    return new WidgetUserSheetViewModel.StoreState(user, meUser, channel, map, map2, map3, map4, booleanValue, isSelfMuted, isSelfDeafened, floatValue, modelRichPresence, guild, l, streamContext, userProfile, num, userNoteState, channel3, stageRoles, stageRequestToSpeakState, stageRoles2, z2, null);
                }
            }

            /* JADX WARN: Code restructure failed: missing block: B:34:0x00e3, code lost:
                if (r6 != null) goto L36;
             */
            /* JADX WARN: Code restructure failed: missing block: B:38:0x00f9, code lost:
                if (r7 != null) goto L41;
             */
            /* JADX WARN: Code restructure failed: missing block: B:44:0x0123, code lost:
                if (r2 != null) goto L47;
             */
            /* JADX WARN: Code restructure failed: missing block: B:49:0x0141, code lost:
                if (r3 != null) goto L52;
             */
            /* JADX WARN: Code restructure failed: missing block: B:58:0x0176, code lost:
                if (r5 != null) goto L60;
             */
            /* JADX WARN: Removed duplicated region for block: B:17:0x0071  */
            /* JADX WARN: Removed duplicated region for block: B:22:0x008e  */
            /* JADX WARN: Removed duplicated region for block: B:27:0x00ac  */
            /* JADX WARN: Removed duplicated region for block: B:33:0x00d6  */
            /* JADX WARN: Removed duplicated region for block: B:37:0x00ee  */
            /* JADX WARN: Removed duplicated region for block: B:39:0x00fc  */
            /* JADX WARN: Removed duplicated region for block: B:43:0x0112  */
            /* JADX WARN: Removed duplicated region for block: B:45:0x0126  */
            /* JADX WARN: Removed duplicated region for block: B:48:0x0131  */
            /* JADX WARN: Removed duplicated region for block: B:50:0x0144  */
            /* JADX WARN: Removed duplicated region for block: B:53:0x0150  */
            /* JADX WARN: Removed duplicated region for block: B:57:0x0165  */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final rx.Observable<? extends com.discord.widgets.user.usersheet.WidgetUserSheetViewModel.StoreState> call(com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState.BootstrapData r28) {
                /*
                    Method dump skipped, instructions count: 506
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState$observeStoreState$3.call(com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState$BootstrapData):rx.Observable");
            }
        }).j, new LeadingEdgeThrottle(250L, TimeUnit.MILLISECONDS, scheduler))).q();
        m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
        return q;
    }
}
