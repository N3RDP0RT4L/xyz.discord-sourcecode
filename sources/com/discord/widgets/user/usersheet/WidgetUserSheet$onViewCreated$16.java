package com.discord.widgets.user.usersheet;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.discord.databinding.WidgetUserSheetBinding;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/widget/TextView;", "it", "", "invoke", "(Landroid/widget/TextView;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheet$onViewCreated$16 extends o implements Function1<TextView, Unit> {
    public final /* synthetic */ View $view;
    public final /* synthetic */ WidgetUserSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheet$onViewCreated$16(WidgetUserSheet widgetUserSheet, View view) {
        super(1);
        this.this$0 = widgetUserSheet;
        this.$view = view;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(TextView textView) {
        invoke2(textView);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(TextView textView) {
        WidgetUserSheetViewModel viewModel;
        WidgetUserSheetBinding binding;
        WidgetUserSheetBinding binding2;
        m.checkNotNullParameter(textView, "it");
        viewModel = this.this$0.getViewModel();
        Context context = this.this$0.getContext();
        binding = this.this$0.getBinding();
        TextInputLayout textInputLayout = binding.B;
        m.checkNotNullExpressionValue(textInputLayout, "binding.userSheetNoteTextFieldWrap");
        viewModel.saveUserNote(context, ViewExtensions.getTextOrEmpty(textInputLayout));
        binding2 = this.this$0.getBinding();
        binding2.B.clearFocus();
        this.this$0.hideKeyboard(this.$view);
    }
}
