package com.discord.widgets.user.usersheet;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.d.a;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.role.GuildRole;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetUserSheetBinding;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.simpleast.core.node.Node;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserNotes;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.textprocessing.AstRenderer;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.WidgetChannelSelector;
import com.discord.widgets.guildcommunicationdisabled.start.WidgetDisableGuildCommunication;
import com.discord.widgets.guildcommunicationdisabled.start.WidgetEnableGuildCommunication;
import com.discord.widgets.roles.RolesListView;
import com.discord.widgets.servers.WidgetServerSettingsEditMember;
import com.discord.widgets.stage.usersheet.UserProfileStageActionsView;
import com.discord.widgets.user.Badge;
import com.discord.widgets.user.WidgetBanUser;
import com.discord.widgets.user.WidgetKickUser;
import com.discord.widgets.user.calls.PrivateCallLauncher;
import com.discord.widgets.user.presence.ViewHolderStreamRichPresence;
import com.discord.widgets.user.presence.ViewHolderUserRichPresence;
import com.discord.widgets.user.profile.UserProfileAdminView;
import com.discord.widgets.user.profile.UserProfileConnectionsView;
import com.discord.widgets.user.profile.UserProfileHeaderView;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import com.discord.widgets.user.usersheet.UserProfileVoiceSettingsView;
import com.discord.widgets.user.usersheet.WidgetUserSheetViewModel;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textfield.TextInputLayout;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import org.objectweb.asm.Opcodes;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000è\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 t2\u00020\u0001:\u0002tuB\u0007¢\u0006\u0004\bs\u0010\u001cJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u000eJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0010\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0011\u0010\u000eJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0012\u0010\u000eJ\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0013\u0010\u000eJ\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0014\u0010\u000eJ\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0015\u0010\u000eJ\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0016\u0010\u000eJ\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u001d\u0010\u001aJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\u0002¢\u0006\u0004\b \u0010!J\u0017\u0010#\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\"H\u0002¢\u0006\u0004\b#\u0010$J\u0017\u0010&\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020%H\u0002¢\u0006\u0004\b&\u0010'J\u0017\u0010)\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020(H\u0002¢\u0006\u0004\b)\u0010*J\u0017\u0010,\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020+H\u0002¢\u0006\u0004\b,\u0010-J\u0017\u0010/\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020.H\u0002¢\u0006\u0004\b/\u00100J\u0017\u00102\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u000201H\u0002¢\u0006\u0004\b2\u00103J\u0017\u00105\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u000204H\u0002¢\u0006\u0004\b5\u00106J\u0017\u00108\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u000207H\u0002¢\u0006\u0004\b8\u00109J\u0017\u0010;\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020:H\u0002¢\u0006\u0004\b;\u0010<J\u0017\u0010>\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020=H\u0002¢\u0006\u0004\b>\u0010?J\u0017\u0010A\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020@H\u0002¢\u0006\u0004\bA\u0010BJ\u0017\u0010D\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020CH\u0002¢\u0006\u0004\bD\u0010EJ\u000f\u0010F\u001a\u00020\u0004H\u0002¢\u0006\u0004\bF\u0010\u001cJ\u000f\u0010H\u001a\u00020GH\u0016¢\u0006\u0004\bH\u0010IJ-\u0010Q\u001a\u0004\u0018\u00010P2\u0006\u0010K\u001a\u00020J2\b\u0010M\u001a\u0004\u0018\u00010L2\b\u0010O\u001a\u0004\u0018\u00010NH\u0016¢\u0006\u0004\bQ\u0010RJ!\u0010T\u001a\u00020\u00042\u0006\u0010S\u001a\u00020P2\b\u0010O\u001a\u0004\u0018\u00010NH\u0016¢\u0006\u0004\bT\u0010UJ\u000f\u0010V\u001a\u00020\u0004H\u0016¢\u0006\u0004\bV\u0010\u001cJ\u000f\u0010W\u001a\u00020\u0004H\u0016¢\u0006\u0004\bW\u0010\u001cJ\u0017\u0010Z\u001a\u00020\u00042\u0006\u0010Y\u001a\u00020XH\u0016¢\u0006\u0004\bZ\u0010[R\u0018\u0010]\u001a\u0004\u0018\u00010\\8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b`\u0010aR\u001d\u0010g\u001a\u00020b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bc\u0010d\u001a\u0004\be\u0010fR\u001d\u0010m\u001a\u00020h8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bi\u0010j\u001a\u0004\bk\u0010lR\u001d\u0010r\u001a\u00020n8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bo\u0010j\u001a\u0004\bp\u0010q¨\u0006v"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;", "model", "", "configureUI", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "onStreamPreviewClicked", "(Lcom/discord/utilities/streams/StreamContext;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;", "viewState", "configureGuildSection", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V", "configureConnectionsSection", "configureNote", "configureProfileActionButtons", "configureStageActionsSection", "configureIncomingFriendRequest", "configureAboutMe", "configureVoiceSection", "configureDeveloperSection", "", "username", "acceptFriendRequest", "(Ljava/lang/String;)V", "ignoreFriendRequest", "()V", "addFriend", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;", "handleRequestPermissionsForSpectateStream", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;", "handleShowToast", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;", "handleShowFriendRequestErrorToast", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;", "handleLaunchVoiceCall", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;", "handleLaunchVideoCall", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;", "handleLaunchSpectate", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;", "handleLaunchEditMember", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;", "handleKickUser", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;", "handleBanUser", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchDisableCommunication;", "handleDisableCommunication", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchDisableCommunication;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEnableCommunication;", "handleEnableCommunication", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEnableCommunication;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;", "handleMoveUser", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;)V", "handleDismissSheet", "", "getContentViewResId", "()I", "Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "container", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/view/View;", "onCreateView", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;", "view", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "onPause", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "activityViewHolder", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "Lcom/discord/widgets/user/calls/PrivateCallLauncher;", "privateCallLauncher", "Lcom/discord/widgets/user/calls/PrivateCallLauncher;", "Lcom/discord/databinding/WidgetUserSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserSheetBinding;", "binding", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;", "viewModelUserProfileHeader$delegate", "Lkotlin/Lazy;", "getViewModelUserProfileHeader", "()Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;", "viewModelUserProfileHeader", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;", "viewModel", HookHelper.constructorName, "Companion", "StreamPreviewClickBehavior", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheet extends AppBottomSheet {
    private static final String ARG_CHANNEL_ID = "ARG_CHANNEL_ID";
    private static final String ARG_FRIEND_TOKEN = "ARG_FRIEND_TOKEN";
    private static final String ARG_GUILD_ID = "ARG_GUILD_ID";
    private static final String ARG_IS_VOICE_CONTEXT = "ARG_IS_VOICE_CONTEXT";
    private static final String ARG_STREAM_PREVIEW_CLICK_BEHAVIOR = "ARG_STREAM_PREVIEW_CLICK_BEHAVIOR";
    private static final String ARG_USER_ID = "ARG_USER_ID";
    private static final String REQUEST_KEY_MOVE_USER = "REQUEST_KEY_MOVE_USER";
    private ViewHolderUserRichPresence activityViewHolder;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserSheet$binding$2.INSTANCE, null, 2, null);
    private PrivateCallLauncher privateCallLauncher;
    private final Lazy viewModel$delegate;
    private final Lazy viewModelUserProfileHeader$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetUserSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u000f\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b \u0010!J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007Ji\u0010\u0014\u001a\u00020\u00132\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\t\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\b2\u0006\u0010\u000b\u001a\u00020\n2\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\f2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010\u0011\u001a\u00020\u00102\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0005H\u0007¢\u0006\u0004\b\u0014\u0010\u0015J%\u0010\u0016\u001a\u00020\u00132\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001a\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0019R\u0016\u0010\u001c\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u0019R\u0016\u0010\u001d\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u0019R\u0016\u0010\u001e\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001e\u0010\u0019R\u0016\u0010\u001f\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u0019¨\u0006\""}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;", "", "", "Lcom/discord/primitives/UserId;", "userId", "", "getNoticeName", "(J)Ljava/lang/String;", "Lcom/discord/primitives/ChannelId;", "channelId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/primitives/GuildId;", "guildId", "", "isVoiceContext", "Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;", "streamPreviewClickBehavior", "friendToken", "", "show", "(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;)V", "enqueueNotice", "(JLjava/lang/String;)V", WidgetUserSheet.ARG_CHANNEL_ID, "Ljava/lang/String;", WidgetUserSheet.ARG_FRIEND_TOKEN, WidgetUserSheet.ARG_GUILD_ID, WidgetUserSheet.ARG_IS_VOICE_CONTEXT, WidgetUserSheet.ARG_STREAM_PREVIEW_CLICK_BEHAVIOR, WidgetUserSheet.ARG_USER_ID, WidgetUserSheet.REQUEST_KEY_MOVE_USER, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void enqueueNotice$default(Companion companion, long j, String str, int i, Object obj) {
            if ((i & 2) != 0) {
                str = null;
            }
            companion.enqueueNotice(j, str);
        }

        private final String getNoticeName(long j) {
            String str = "User Sheet Notice for user ID: " + j;
            m.checkNotNullExpressionValue(str, "StringBuilder(\"User Shee…)\n            .toString()");
            return str;
        }

        public static /* synthetic */ void show$default(Companion companion, long j, Long l, FragmentManager fragmentManager, Long l2, Boolean bool, StreamPreviewClickBehavior streamPreviewClickBehavior, String str, int i, Object obj) {
            companion.show(j, (i & 2) != 0 ? null : l, fragmentManager, (i & 8) != 0 ? null : l2, (i & 16) != 0 ? null : bool, (i & 32) != 0 ? StreamPreviewClickBehavior.TARGET_AND_LAUNCH_SPECTATE : streamPreviewClickBehavior, (i & 64) != 0 ? null : str);
        }

        public final void enqueueNotice(long j, String str) {
            StoreNotices notices = StoreStream.Companion.getNotices();
            String noticeName = getNoticeName(j);
            notices.requestToShow(new StoreNotices.Notice(noticeName, null, 0L, 0, false, null, 0L, false, 0L, new WidgetUserSheet$Companion$enqueueNotice$showUserSheetNotice$1(j, str, notices, noticeName), Opcodes.INVOKEVIRTUAL, null));
        }

        public final void show(long j, FragmentManager fragmentManager) {
            show$default(this, j, null, fragmentManager, null, null, null, null, 122, null);
        }

        public final void show(long j, Long l, FragmentManager fragmentManager) {
            show$default(this, j, l, fragmentManager, null, null, null, null, 120, null);
        }

        public final void show(long j, Long l, FragmentManager fragmentManager, Long l2) {
            show$default(this, j, l, fragmentManager, l2, null, null, null, 112, null);
        }

        public final void show(long j, Long l, FragmentManager fragmentManager, Long l2, Boolean bool) {
            show$default(this, j, l, fragmentManager, l2, bool, null, null, 96, null);
        }

        public final void show(long j, Long l, FragmentManager fragmentManager, Long l2, Boolean bool, StreamPreviewClickBehavior streamPreviewClickBehavior) {
            show$default(this, j, l, fragmentManager, l2, bool, streamPreviewClickBehavior, null, 64, null);
        }

        public final void show(long j, Long l, FragmentManager fragmentManager, Long l2, Boolean bool, StreamPreviewClickBehavior streamPreviewClickBehavior, String str) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(streamPreviewClickBehavior, "streamPreviewClickBehavior");
            WidgetUserSheet widgetUserSheet = new WidgetUserSheet();
            Bundle I = a.I(WidgetUserSheet.ARG_USER_ID, j);
            if (l != null) {
                I.putLong(WidgetUserSheet.ARG_CHANNEL_ID, l.longValue());
            }
            if (str != null) {
                I.putString(WidgetUserSheet.ARG_FRIEND_TOKEN, str);
            }
            if (l2 != null) {
                I.putLong(WidgetUserSheet.ARG_GUILD_ID, l2.longValue());
            }
            I.putBoolean(WidgetUserSheet.ARG_IS_VOICE_CONTEXT, bool != null ? bool.booleanValue() : false);
            I.putSerializable(WidgetUserSheet.ARG_STREAM_PREVIEW_CLICK_BEHAVIOR, streamPreviewClickBehavior);
            widgetUserSheet.setArguments(I);
            widgetUserSheet.show(fragmentManager, WidgetUserSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "TARGET_AND_LAUNCH_SPECTATE", "TARGET_AND_DISMISS", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum StreamPreviewClickBehavior {
        TARGET_AND_LAUNCH_SPECTATE,
        TARGET_AND_DISMISS
    }

    public WidgetUserSheet() {
        super(false, 1, null);
        WidgetUserSheet$viewModelUserProfileHeader$2 widgetUserSheet$viewModelUserProfileHeader$2 = new WidgetUserSheet$viewModelUserProfileHeader$2(this);
        f0 f0Var = new f0(this);
        this.viewModelUserProfileHeader$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(UserProfileHeaderViewModel.class), new WidgetUserSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetUserSheet$viewModelUserProfileHeader$2));
        WidgetUserSheet$viewModel$2 widgetUserSheet$viewModel$2 = new WidgetUserSheet$viewModel$2(this);
        f0 f0Var2 = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetUserSheetViewModel.class), new WidgetUserSheet$appViewModels$$inlined$viewModels$2(f0Var2), new h0(widgetUserSheet$viewModel$2));
    }

    public final void acceptFriendRequest(String str) {
        WidgetUserSheetViewModel.addRelationship$default(getViewModel(), null, str, R.string.accept_request_button_after, null, 8, null);
    }

    public final void addFriend(String str) {
        WidgetUserSheetViewModel.addRelationship$default(getViewModel(), null, str, R.string.friend_request_sent, null, 8, null);
    }

    private final void configureAboutMe(final WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        String str;
        List<Node<MessageRenderContext>> bioAst = loaded.getBioAst();
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheet$configureAboutMe$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CharSequence e;
                if (loaded.getHasGuildMemberBio()) {
                    m.checkNotNullExpressionValue(view, "it");
                    Context context = view.getContext();
                    e = b.e(WidgetUserSheet.this, R.string.guild_identity_bio_toast, new Object[]{loaded.getGuildName()}, (r4 & 4) != 0 ? b.a.j : null);
                    b.a.d.m.h(context, e, 0, null, 12);
                }
            }
        });
        TextView textView = getBinding().e;
        m.checkNotNullExpressionValue(textView, "binding.aboutMeHeader");
        boolean z2 = true;
        int i = 0;
        textView.setVisibility(bioAst != null ? 0 : 8);
        SimpleDraweeView simpleDraweeView = getBinding().c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.aboutMeGuildIcon");
        simpleDraweeView.setVisibility(loaded.getHasGuildMemberBio() ? 0 : 8);
        String guildIconURL = loaded.getGuildIconURL();
        if (guildIconURL == null || guildIconURL.length() == 0) {
            SimpleDraweeView simpleDraweeView2 = getBinding().c;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.aboutMeGuildIcon");
            IconUtils.setIcon$default(simpleDraweeView2, IconUtils.DEFAULT_ICON_BLURPLE, 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
            TextView textView2 = getBinding().d;
            m.checkNotNullExpressionValue(textView2, "binding.aboutMeGuildIconName");
            textView2.setVisibility(0);
            TextView textView3 = getBinding().d;
            m.checkNotNullExpressionValue(textView3, "binding.aboutMeGuildIconName");
            String guildName = loaded.getGuildName();
            if (guildName == null || (str = GuildUtilsKt.computeShortName(guildName)) == null) {
                str = "";
            }
            textView3.setText(str);
        } else {
            SimpleDraweeView simpleDraweeView3 = getBinding().c;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.aboutMeGuildIcon");
            IconUtils.setIcon$default(simpleDraweeView3, loaded.getGuildIconURL(), 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
            TextView textView4 = getBinding().d;
            m.checkNotNullExpressionValue(textView4, "binding.aboutMeGuildIconName");
            textView4.setVisibility(8);
        }
        CardView cardView = getBinding().f2666b;
        m.checkNotNullExpressionValue(cardView, "binding.aboutMeCard");
        if (bioAst == null) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        cardView.setVisibility(i);
        if (bioAst != null) {
            LinkifiedTextView linkifiedTextView = getBinding().g;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.aboutMeText");
            Context context = linkifiedTextView.getContext();
            m.checkNotNullExpressionValue(context, "binding.aboutMeText.context");
            getBinding().g.setDraweeSpanStringBuilder(AstRenderer.render(loaded.getBioAst(), new MessageRenderContext(context, 0L, false, null, null, null, 0, null, null, 0, 0, new WidgetUserSheet$configureAboutMe$renderContext$1(getViewModel()), null, null, 14328, null)));
        }
    }

    private final void configureConnectionsSection(WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        UserProfileConnectionsView.ViewState connectionsViewState = loaded.getConnectionsViewState();
        TextView textView = getBinding().m;
        m.checkNotNullExpressionValue(textView, "binding.userSheetConnectionsHeader");
        int i = 0;
        textView.setVisibility(connectionsViewState.getShowConnectionsSection() ? 0 : 8);
        UserProfileConnectionsView userProfileConnectionsView = getBinding().n;
        m.checkNotNullExpressionValue(userProfileConnectionsView, "binding.userSheetConnectionsView");
        if (!connectionsViewState.getShowConnectionsSection()) {
            i = 8;
        }
        userProfileConnectionsView.setVisibility(i);
        WidgetUserSheet$configureConnectionsSection$onConnectedAccountClick$1 widgetUserSheet$configureConnectionsSection$onConnectedAccountClick$1 = new WidgetUserSheet$configureConnectionsSection$onConnectedAccountClick$1(this);
        User user = loaded.getUser();
        getBinding().n.updateViewState(connectionsViewState, widgetUserSheet$configureConnectionsSection$onConnectedAccountClick$1, new WidgetUserSheet$configureConnectionsSection$onMutualGuildsItemClick$1(this, user), new WidgetUserSheet$configureConnectionsSection$onMutualFriendsItemClick$1(this, user));
    }

    private final void configureDeveloperSection(final WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        final TextView textView = getBinding().o;
        int i = 8;
        textView.setVisibility(StoreStream.Companion.getUserSettings().getIsDeveloperMode() ? 0 : 8);
        textView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheet$configureDeveloperSection$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Context context = textView.getContext();
                m.checkNotNullExpressionValue(context, "context");
                b.a.d.m.c(context, String.valueOf(loaded.getUser().getId()), 0, 4);
                this.dismiss();
            }
        });
        TextView textView2 = getBinding().o;
        m.checkNotNullExpressionValue(textView2, "binding.userSheetCopyId");
        boolean z2 = textView2.getVisibility() == 0;
        TextView textView3 = getBinding().p;
        m.checkNotNullExpressionValue(textView3, "binding.userSheetDeveloperHeader");
        if (z2) {
            i = 0;
        }
        textView3.setVisibility(i);
    }

    private final void configureGuildSection(WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        List<GuildRole> roleItems = loaded.getRoleItems();
        UserProfileAdminView.ViewState adminViewState = loaded.getAdminViewState();
        boolean shouldShowRoles = loaded.shouldShowRoles();
        boolean z2 = true;
        int i = 0;
        boolean z3 = adminViewState != null && adminViewState.isAdminSectionEnabled();
        LinearLayout linearLayout = getBinding().t;
        m.checkNotNullExpressionValue(linearLayout, "binding.userSheetGuildContainer");
        if (!shouldShowRoles && !z3) {
            z2 = false;
        }
        linearLayout.setVisibility(z2 ? 0 : 8);
        RolesListView rolesListView = getBinding().P;
        m.checkNotNullExpressionValue(rolesListView, "binding.userSheetRolesList");
        rolesListView.setVisibility(shouldShowRoles ? 0 : 8);
        Long guildId = loaded.getGuildId();
        if (shouldShowRoles && guildId != null) {
            RolesListView rolesListView2 = getBinding().P;
            RolesListView rolesListView3 = getBinding().P;
            m.checkNotNullExpressionValue(rolesListView3, "binding.userSheetRolesList");
            rolesListView2.updateView(roleItems, ColorCompat.getThemedColor(rolesListView3.getContext(), (int) R.attr.primary_300), guildId.longValue());
        }
        String guildSectionHeaderText = loaded.getGuildSectionHeaderText();
        if (guildSectionHeaderText != null) {
            TextView textView = getBinding().u;
            m.checkNotNullExpressionValue(textView, "binding.userSheetGuildHeader");
            textView.setText(guildSectionHeaderText);
        }
        CardView cardView = getBinding().j;
        m.checkNotNullExpressionValue(cardView, "binding.userSheetAdminCard");
        cardView.setVisibility(z3 ? 0 : 8);
        UserProfileAdminView userProfileAdminView = getBinding().k;
        m.checkNotNullExpressionValue(userProfileAdminView, "binding.userSheetAdminView");
        if (!z3) {
            i = 8;
        }
        userProfileAdminView.setVisibility(i);
        if (adminViewState != null) {
            getBinding().k.updateView(adminViewState);
        }
    }

    private final void configureIncomingFriendRequest(final WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        int i = 0;
        boolean z2 = ModelUserRelationship.getType(Integer.valueOf(loaded.getUserRelationshipType())) == 3;
        TextView textView = getBinding().v;
        m.checkNotNullExpressionValue(textView, "binding.userSheetIncomingFriendRequestHeader");
        textView.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout = getBinding().f2667s;
        m.checkNotNullExpressionValue(linearLayout, "binding.userSheetFriendRequestIncomingContainer");
        if (!z2) {
            i = 8;
        }
        linearLayout.setVisibility(i);
        getBinding().q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheet$configureIncomingFriendRequest$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetUserSheet.this.acceptFriendRequest(loaded.getUser().getUsername());
            }
        });
        getBinding().r.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheet$configureIncomingFriendRequest$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetUserSheet.this.ignoreFriendRequest();
            }
        });
    }

    private final void configureNote(WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        getBinding().A.setRawInputType(1);
        StoreUserNotes.UserNoteState userNoteFetchState = loaded.getUserNoteFetchState();
        if (userNoteFetchState instanceof StoreUserNotes.UserNoteState.Empty) {
            TextInputLayout textInputLayout = getBinding().B;
            m.checkNotNullExpressionValue(textInputLayout, "binding.userSheetNoteTextFieldWrap");
            textInputLayout.setHint(getString(R.string.note_placeholder_mobile));
            TextInputLayout textInputLayout2 = getBinding().B;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.userSheetNoteTextFieldWrap");
            textInputLayout2.setEnabled(true);
        } else if (userNoteFetchState instanceof StoreUserNotes.UserNoteState.Loading) {
            TextInputLayout textInputLayout3 = getBinding().B;
            m.checkNotNullExpressionValue(textInputLayout3, "binding.userSheetNoteTextFieldWrap");
            textInputLayout3.setHint(getString(R.string.loading_note));
            TextInputLayout textInputLayout4 = getBinding().B;
            m.checkNotNullExpressionValue(textInputLayout4, "binding.userSheetNoteTextFieldWrap");
            textInputLayout4.setEnabled(false);
        } else if (userNoteFetchState instanceof StoreUserNotes.UserNoteState.Loaded) {
            TextInputLayout textInputLayout5 = getBinding().B;
            m.checkNotNullExpressionValue(textInputLayout5, "binding.userSheetNoteTextFieldWrap");
            textInputLayout5.setHint(getString(R.string.note_placeholder_mobile));
            TextInputLayout textInputLayout6 = getBinding().B;
            m.checkNotNullExpressionValue(textInputLayout6, "binding.userSheetNoteTextFieldWrap");
            textInputLayout6.setEnabled(true);
        }
        if (loaded.getUserNote() != null) {
            TextInputLayout textInputLayout7 = getBinding().B;
            m.checkNotNullExpressionValue(textInputLayout7, "binding.userSheetNoteTextFieldWrap");
            ViewExtensions.setTextIfDifferent(textInputLayout7, loaded.getUserNote());
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0082  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0084  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00bf  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00c1  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00d3  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00d5  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00dc A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00dd  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureProfileActionButtons(final com.discord.widgets.user.usersheet.WidgetUserSheetViewModel.ViewState.Loaded r12) {
        /*
            Method dump skipped, instructions count: 437
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.usersheet.WidgetUserSheet.configureProfileActionButtons(com.discord.widgets.user.usersheet.WidgetUserSheetViewModel$ViewState$Loaded):void");
    }

    private final void configureStageActionsSection(WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        getBinding().M.updateView(loaded.getStageViewState());
        CardView cardView = getBinding().L;
        m.checkNotNullExpressionValue(cardView, "binding.userSheetProfileStageActionsCard");
        UserProfileStageActionsView userProfileStageActionsView = getBinding().M;
        m.checkNotNullExpressionValue(userProfileStageActionsView, "binding.userSheetProfileStageActionsView");
        int i = 0;
        if (!(userProfileStageActionsView.getVisibility() == 0)) {
            i = 8;
        }
        cardView.setVisibility(i);
    }

    public final void configureUI(WidgetUserSheetViewModel.ViewState viewState) {
        int i = 0;
        if (viewState instanceof WidgetUserSheetViewModel.ViewState.Uninitialized) {
            FrameLayout frameLayout = getBinding().w;
            m.checkNotNullExpressionValue(frameLayout, "binding.userSheetLoadingContainer");
            frameLayout.setVisibility(0);
        } else if (viewState instanceof WidgetUserSheetViewModel.ViewState.Loaded) {
            FrameLayout frameLayout2 = getBinding().w;
            m.checkNotNullExpressionValue(frameLayout2, "binding.userSheetLoadingContainer");
            frameLayout2.setVisibility(8);
            WidgetUserSheetViewModel.ViewState.Loaded loaded = (WidgetUserSheetViewModel.ViewState.Loaded) viewState;
            Presence presence = loaded.getPresence();
            ViewHolderStreamRichPresence viewHolderStreamRichPresence = null;
            Activity primaryActivity = presence != null ? PresenceUtils.INSTANCE.getPrimaryActivity(presence) : null;
            boolean isMe = loaded.isMe();
            boolean isSystemUser = loaded.getUser().isSystemUser();
            boolean z2 = true;
            if (!getViewModel().getOpenPopoutLogged() && loaded.getProfileLoaded()) {
                AnalyticsTracker.INSTANCE.openUserSheet(loaded.getHasPremiumCustomization(), primaryActivity != null ? primaryActivity.h() : null, primaryActivity != null ? ActivityUtilsKt.getGamePlatform(primaryActivity) : null, loaded.getGuildMember());
                getViewModel().setOpenPopoutLogged(true);
            }
            getBinding().J.setOnBannerPress(new WidgetUserSheet$configureUI$1(this));
            ImageView imageView = getBinding().f2669y;
            m.checkNotNullExpressionValue(imageView, "binding.userSheetMoreButton");
            if (isMe || isSystemUser) {
                z2 = false;
            }
            if (!z2) {
                i = 8;
            }
            imageView.setVisibility(i);
            if (loaded.getConnectionsViewState().getShowConnectionsSection()) {
                setPeekHeightBottomView(getBinding().m);
            }
            ViewHolderUserRichPresence.Companion companion = ViewHolderUserRichPresence.Companion;
            FrameLayout frameLayout3 = getBinding().h;
            m.checkNotNullExpressionValue(frameLayout3, "binding.richPresenceContainer");
            ViewHolderUserRichPresence richPresence = companion.setRichPresence(frameLayout3, primaryActivity, loaded.getStreamContext(), this.activityViewHolder);
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            StreamContext streamContext = loaded.getStreamContext();
            boolean isMe2 = loaded.isMe();
            User user = loaded.getUser();
            Context applicationContext = requireAppActivity().getApplicationContext();
            m.checkNotNullExpressionValue(applicationContext, "requireAppActivity().applicationContext");
            richPresence.configureUi(parentFragmentManager, streamContext, isMe2, user, applicationContext, loaded.getRichPresence(), loaded.getUserInSameVoiceChannel());
            richPresence.configureUiTimestamp(primaryActivity, this);
            this.activityViewHolder = richPresence;
            if (richPresence instanceof ViewHolderStreamRichPresence) {
                viewHolderStreamRichPresence = richPresence;
            }
            ViewHolderStreamRichPresence viewHolderStreamRichPresence2 = viewHolderStreamRichPresence;
            if (!(loaded.getStreamContext() == null || viewHolderStreamRichPresence2 == null)) {
                viewHolderStreamRichPresence2.setOnStreamPreviewClicked(new WidgetUserSheet$configureUI$3(this, viewState));
            }
            ViewHolderUserRichPresence viewHolderUserRichPresence = this.activityViewHolder;
            if (viewHolderUserRichPresence != null) {
                viewHolderUserRichPresence.setOnActivityCustomButtonClicked(new WidgetUserSheet$configureUI$4(getViewModel()));
            }
            configureProfileActionButtons(loaded);
            configureStageActionsSection(loaded);
            configureAboutMe(loaded);
            configureIncomingFriendRequest(loaded);
            configureVoiceSection(loaded);
            configureGuildSection(loaded);
            configureConnectionsSection(loaded);
            configureNote(loaded);
            configureDeveloperSection(loaded);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void configureVoiceSection(WidgetUserSheetViewModel.ViewState.Loaded loaded) {
        UserProfileVoiceSettingsView.ViewState voiceSettingsViewState = loaded.getVoiceSettingsViewState();
        boolean showVoiceSettings = loaded.getShowVoiceSettings();
        TextView textView = getBinding().N;
        m.checkNotNullExpressionValue(textView, "binding.userSheetProfileVoiceSettingsHeader");
        int i = 0;
        textView.setVisibility(showVoiceSettings ? 0 : 8);
        CardView cardView = getBinding().R;
        m.checkNotNullExpressionValue(cardView, "binding.voiceSettingsViewCard");
        if (!showVoiceSettings) {
            i = 8;
        }
        cardView.setVisibility(i);
        getBinding().O.updateView(voiceSettingsViewState);
    }

    public final WidgetUserSheetBinding getBinding() {
        return (WidgetUserSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final WidgetUserSheetViewModel getViewModel() {
        return (WidgetUserSheetViewModel) this.viewModel$delegate.getValue();
    }

    public final UserProfileHeaderViewModel getViewModelUserProfileHeader() {
        return (UserProfileHeaderViewModel) this.viewModelUserProfileHeader$delegate.getValue();
    }

    private final void handleBanUser(WidgetUserSheetViewModel.Event.LaunchBanUser launchBanUser) {
        WidgetBanUser.Companion.launch(launchBanUser.getUsername(), launchBanUser.getGuildId(), launchBanUser.getUserId(), getParentFragmentManager());
    }

    private final void handleDisableCommunication(WidgetUserSheetViewModel.Event.LaunchDisableCommunication launchDisableCommunication) {
        WidgetDisableGuildCommunication.Companion companion = WidgetDisableGuildCommunication.Companion;
        long userId = launchDisableCommunication.getUserId();
        long guildId = launchDisableCommunication.getGuildId();
        WidgetUserSheetBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        NestedScrollView nestedScrollView = binding.a;
        m.checkNotNullExpressionValue(nestedScrollView, "binding.root");
        Context context = nestedScrollView.getContext();
        m.checkNotNullExpressionValue(context, "binding.root.context");
        companion.launch(userId, guildId, context);
    }

    private final void handleDismissSheet() {
        dismiss();
    }

    private final void handleEnableCommunication(WidgetUserSheetViewModel.Event.LaunchEnableCommunication launchEnableCommunication) {
        WidgetEnableGuildCommunication.Companion companion = WidgetEnableGuildCommunication.Companion;
        long userId = launchEnableCommunication.getUserId();
        long guildId = launchEnableCommunication.getGuildId();
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.launch(userId, guildId, parentFragmentManager);
    }

    public final void handleEvent(WidgetUserSheetViewModel.Event event) {
        if (event instanceof WidgetUserSheetViewModel.Event.ShowToast) {
            handleShowToast((WidgetUserSheetViewModel.Event.ShowToast) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.ShowFriendRequestErrorToast) {
            handleShowFriendRequestErrorToast((WidgetUserSheetViewModel.Event.ShowFriendRequestErrorToast) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchVoiceCall) {
            handleLaunchVoiceCall((WidgetUserSheetViewModel.Event.LaunchVoiceCall) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchVideoCall) {
            handleLaunchVideoCall((WidgetUserSheetViewModel.Event.LaunchVideoCall) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchSpectate) {
            handleLaunchSpectate((WidgetUserSheetViewModel.Event.LaunchSpectate) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchEditMember) {
            handleLaunchEditMember((WidgetUserSheetViewModel.Event.LaunchEditMember) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchKickUser) {
            handleKickUser((WidgetUserSheetViewModel.Event.LaunchKickUser) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchBanUser) {
            handleBanUser((WidgetUserSheetViewModel.Event.LaunchBanUser) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchDisableCommunication) {
            handleDisableCommunication((WidgetUserSheetViewModel.Event.LaunchDisableCommunication) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchEnableCommunication) {
            handleEnableCommunication((WidgetUserSheetViewModel.Event.LaunchEnableCommunication) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.LaunchMoveUser) {
            handleMoveUser((WidgetUserSheetViewModel.Event.LaunchMoveUser) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.RequestPermissionsForSpectateStream) {
            handleRequestPermissionsForSpectateStream((WidgetUserSheetViewModel.Event.RequestPermissionsForSpectateStream) event);
        } else if (event instanceof WidgetUserSheetViewModel.Event.UserNotFound) {
            b.a.d.m.i(this, R.string.user_profile_failure_to_open_message, 0, 4);
            handleDismissSheet();
        } else if (event instanceof WidgetUserSheetViewModel.Event.DismissSheet) {
            handleDismissSheet();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void handleKickUser(WidgetUserSheetViewModel.Event.LaunchKickUser launchKickUser) {
        WidgetKickUser.Companion companion = WidgetKickUser.Companion;
        String username = launchKickUser.getUsername();
        long guildId = launchKickUser.getGuildId();
        long userId = launchKickUser.getUserId();
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.launch(username, guildId, userId, parentFragmentManager);
    }

    private final void handleLaunchEditMember(WidgetUserSheetViewModel.Event.LaunchEditMember launchEditMember) {
        WidgetServerSettingsEditMember.Companion companion = WidgetServerSettingsEditMember.Companion;
        long guildId = launchEditMember.getGuildId();
        long userId = launchEditMember.getUserId();
        WidgetUserSheetBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        NestedScrollView nestedScrollView = binding.a;
        m.checkNotNullExpressionValue(nestedScrollView, "binding.root");
        Context context = nestedScrollView.getContext();
        m.checkNotNullExpressionValue(context, "binding.root.context");
        companion.launch(guildId, userId, context);
    }

    private final void handleLaunchSpectate(WidgetUserSheetViewModel.Event.LaunchSpectate launchSpectate) {
        WidgetCallFullscreen.Companion companion = WidgetCallFullscreen.Companion;
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        companion.launch(requireContext, launchSpectate.getStream().getChannelId(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : launchSpectate.getStream().getEncodedStreamKey(), (r14 & 16) != 0 ? null : null);
        dismiss();
    }

    private final void handleLaunchVideoCall(WidgetUserSheetViewModel.Event.LaunchVideoCall launchVideoCall) {
        PrivateCallLauncher privateCallLauncher = this.privateCallLauncher;
        if (privateCallLauncher == null) {
            m.throwUninitializedPropertyAccessException("privateCallLauncher");
        }
        privateCallLauncher.launchVideoCall(launchVideoCall.getChannelId());
    }

    private final void handleLaunchVoiceCall(WidgetUserSheetViewModel.Event.LaunchVoiceCall launchVoiceCall) {
        PrivateCallLauncher privateCallLauncher = this.privateCallLauncher;
        if (privateCallLauncher == null) {
            m.throwUninitializedPropertyAccessException("privateCallLauncher");
        }
        privateCallLauncher.launchVoiceCall(launchVoiceCall.getChannelId());
    }

    private final void handleMoveUser(WidgetUserSheetViewModel.Event.LaunchMoveUser launchMoveUser) {
        WidgetChannelSelector.Companion.launchForVocal(this, launchMoveUser.getGuildId(), REQUEST_KEY_MOVE_USER, (r14 & 8) != 0 ? false : false, (r14 & 16) != 0 ? R.string.none : 0);
    }

    private final void handleRequestPermissionsForSpectateStream(WidgetUserSheetViewModel.Event.RequestPermissionsForSpectateStream requestPermissionsForSpectateStream) {
        d.S1(this, null, new WidgetUserSheet$handleRequestPermissionsForSpectateStream$1(this, requestPermissionsForSpectateStream), 1, null);
    }

    private final void handleShowFriendRequestErrorToast(WidgetUserSheetViewModel.Event.ShowFriendRequestErrorToast showFriendRequestErrorToast) {
        b.a.d.m.j(this, RestAPIAbortMessages.ResponseResolver.INSTANCE.getRelationshipResponse(getContext(), showFriendRequestErrorToast.getAbortCode(), showFriendRequestErrorToast.getUsername()), 0, 4);
    }

    private final void handleShowToast(WidgetUserSheetViewModel.Event.ShowToast showToast) {
        b.a.d.m.i(this, showToast.getStringRes(), 0, 4);
    }

    public final void ignoreFriendRequest() {
        getViewModel().removeRelationship(R.string.friend_request_ignored);
    }

    public final void onStreamPreviewClicked(StreamContext streamContext) {
        getViewModel().onStreamPreviewClicked(streamContext);
    }

    public static final void show(long j, FragmentManager fragmentManager) {
        Companion.show$default(Companion, j, null, fragmentManager, null, null, null, null, 122, null);
    }

    public static final void show(long j, Long l, FragmentManager fragmentManager) {
        Companion.show$default(Companion, j, l, fragmentManager, null, null, null, null, 120, null);
    }

    public static final void show(long j, Long l, FragmentManager fragmentManager, Long l2) {
        Companion.show$default(Companion, j, l, fragmentManager, l2, null, null, null, 112, null);
    }

    public static final void show(long j, Long l, FragmentManager fragmentManager, Long l2, Boolean bool) {
        Companion.show$default(Companion, j, l, fragmentManager, l2, bool, null, null, 96, null);
    }

    public static final void show(long j, Long l, FragmentManager fragmentManager, Long l2, Boolean bool, StreamPreviewClickBehavior streamPreviewClickBehavior) {
        Companion.show$default(Companion, j, l, fragmentManager, l2, bool, streamPreviewClickBehavior, null, 64, null);
    }

    public static final void show(long j, Long l, FragmentManager fragmentManager, Long l2, Boolean bool, StreamPreviewClickBehavior streamPreviewClickBehavior, String str) {
        Companion.show(j, l, fragmentManager, l2, bool, streamPreviewClickBehavior, str);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetUserSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheet$bindSubscriptions$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetUserSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheet$bindSubscriptions$2(this));
        UserProfileHeaderView.Companion companion = UserProfileHeaderView.Companion;
        UserProfileHeaderView userProfileHeaderView = getBinding().J;
        m.checkNotNullExpressionValue(userProfileHeaderView, "binding.userSheetProfileHeaderView");
        companion.bind(userProfileHeaderView, this, getViewModelUserProfileHeader().observeViewState());
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_user_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        m.checkNotNullParameter(layoutInflater, "inflater");
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        this.privateCallLauncher = new PrivateCallLauncher(this, this, requireContext, parentFragmentManager);
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        ViewHolderUserRichPresence viewHolderUserRichPresence = this.activityViewHolder;
        if (viewHolderUserRichPresence != null) {
            viewHolderUserRichPresence.disposeSubscriptions();
        }
        long j = getArgumentsOrDefault().getLong(ARG_USER_ID);
        long j2 = getArgumentsOrDefault().getLong(ARG_GUILD_ID);
        if (j > 0 && j2 > 0) {
            StoreStream.Companion.getGuildSubscriptions().unsubscribeUser(j2, j);
        }
        hideKeyboard(getBinding().B);
        WidgetUserSheetViewModel viewModel = getViewModel();
        Context context = getContext();
        TextInputLayout textInputLayout = getBinding().B;
        m.checkNotNullExpressionValue(textInputLayout, "binding.userSheetNoteTextFieldWrap");
        viewModel.saveUserNote(context, ViewExtensions.getTextOrEmpty(textInputLayout));
        super.onPause();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AppBottomSheet.hideKeyboard$default(this, null, 1, null);
        long j = getArgumentsOrDefault().getLong(ARG_USER_ID);
        long j2 = getArgumentsOrDefault().getLong(ARG_GUILD_ID);
        if (j > 0 && j2 > 0) {
            StoreStream.Companion.getGuildSubscriptions().subscribeUser(j2, j);
        }
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        final long j = getArgumentsOrDefault().getLong(ARG_USER_ID);
        boolean z2 = j == StoreStream.Companion.getUsers().getMe().getId();
        getBinding().f2669y.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheet$onViewCreated$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Bundle argumentsOrDefault;
                a.b bVar = b.a.a.d.a.k;
                FragmentManager parentFragmentManager = WidgetUserSheet.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                long j2 = j;
                argumentsOrDefault = WidgetUserSheet.this.getArgumentsOrDefault();
                long j3 = argumentsOrDefault.getLong("ARG_CHANNEL_ID");
                Objects.requireNonNull(bVar);
                m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
                b.a.a.d.a aVar = new b.a.a.d.a();
                Bundle I = b.d.b.a.a.I("com.discord.intent.extra.EXTRA_USER_ID", j2);
                I.putLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", j3);
                aVar.setArguments(I);
                aVar.show(parentFragmentManager, b.a.a.d.a.class.getName());
            }
        });
        UserProfileHeaderView userProfileHeaderView = getBinding().J;
        Badge.Companion companion = Badge.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        userProfileHeaderView.setOnBadgeClick(companion.onBadgeClick(parentFragmentManager, requireContext));
        getBinding().M.setOnInviteToSpeak(new WidgetUserSheet$onViewCreated$2(this, z2));
        getBinding().M.setOnMoveToAudience(new WidgetUserSheet$onViewCreated$3(this));
        getBinding().O.setOnMuteChecked(new WidgetUserSheet$onViewCreated$4(this));
        getBinding().O.setOnDeafenChecked(new WidgetUserSheet$onViewCreated$5(this));
        getBinding().O.setOnVolumeChange(new WidgetUserSheet$onViewCreated$6(this));
        getBinding().k.setOnEditMember(new WidgetUserSheet$onViewCreated$7(this));
        getBinding().k.setOnKick(new WidgetUserSheet$onViewCreated$8(this));
        getBinding().k.setOnBan(new WidgetUserSheet$onViewCreated$9(this));
        getBinding().k.setOnDisableCommunication(new WidgetUserSheet$onViewCreated$10(this));
        getBinding().k.setOnServerMute(new WidgetUserSheet$onViewCreated$11(this));
        getBinding().k.setOnServerDeafen(new WidgetUserSheet$onViewCreated$12(this));
        getBinding().k.setOnServerMove(new WidgetUserSheet$onViewCreated$13(this));
        getBinding().k.setOnDisconnect(new WidgetUserSheet$onViewCreated$14(this));
        TextInputLayout textInputLayout = getBinding().B;
        m.checkNotNullExpressionValue(textInputLayout, "binding.userSheetNoteTextFieldWrap");
        ViewExtensions.setOnEditTextFocusChangeListener(textInputLayout, new View.OnFocusChangeListener() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheet$onViewCreated$15
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view2, boolean z3) {
                WidgetUserSheetViewModel viewModel;
                WidgetUserSheetBinding binding;
                if (!z3) {
                    viewModel = WidgetUserSheet.this.getViewModel();
                    Context context = WidgetUserSheet.this.getContext();
                    binding = WidgetUserSheet.this.getBinding();
                    TextInputLayout textInputLayout2 = binding.B;
                    m.checkNotNullExpressionValue(textInputLayout2, "binding.userSheetNoteTextFieldWrap");
                    viewModel.saveUserNote(context, ViewExtensions.getTextOrEmpty(textInputLayout2));
                }
            }
        });
        TextInputLayout textInputLayout2 = getBinding().B;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.userSheetNoteTextFieldWrap");
        ViewExtensions.setOnImeActionDone$default(textInputLayout2, false, new WidgetUserSheet$onViewCreated$16(this, view), 1, null);
        ViewCompat.setAccessibilityDelegate(getBinding().f2669y, new AccessibilityDelegateCompat() { // from class: com.discord.widgets.user.usersheet.WidgetUserSheet$onViewCreated$17
            @Override // androidx.core.view.AccessibilityDelegateCompat
            public void onInitializeAccessibilityNodeInfo(View view2, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
                WidgetUserSheetBinding binding;
                m.checkNotNullParameter(view2, "host");
                m.checkNotNullParameter(accessibilityNodeInfoCompat, "info");
                super.onInitializeAccessibilityNodeInfo(view2, accessibilityNodeInfoCompat);
                binding = WidgetUserSheet.this.getBinding();
                accessibilityNodeInfoCompat.setTraversalAfter(binding.o);
            }
        });
        for (TextView textView : n.listOf((Object[]) new TextView[]{getBinding().m, getBinding().u, getBinding().f2670z, getBinding().p, getBinding().v, getBinding().N})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView, "header");
            accessibilityUtils.setViewIsHeading(textView);
        }
        WidgetChannelSelector.Companion.registerForResult$default(WidgetChannelSelector.Companion, this, REQUEST_KEY_MOVE_USER, false, new WidgetUserSheet$onViewCreated$18(this), 4, null);
    }
}
