package com.discord.widgets.user.usersheet;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import com.discord.widgets.user.usersheet.WidgetUserSheetViewModel;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheet$viewModel$2 extends o implements Function0<AppViewModel<WidgetUserSheetViewModel.ViewState>> {
    public final /* synthetic */ WidgetUserSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheet$viewModel$2(WidgetUserSheet widgetUserSheet) {
        super(0);
        this.this$0 = widgetUserSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetUserSheetViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        Bundle argumentsOrDefault3;
        Bundle argumentsOrDefault4;
        Bundle argumentsOrDefault5;
        Bundle argumentsOrDefault6;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        long j = argumentsOrDefault.getLong("ARG_USER_ID");
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        long j2 = argumentsOrDefault2.getLong("ARG_CHANNEL_ID");
        argumentsOrDefault3 = this.this$0.getArgumentsOrDefault();
        long j3 = argumentsOrDefault3.getLong("ARG_GUILD_ID");
        argumentsOrDefault4 = this.this$0.getArgumentsOrDefault();
        boolean z2 = argumentsOrDefault4.getBoolean("ARG_IS_VOICE_CONTEXT");
        argumentsOrDefault5 = this.this$0.getArgumentsOrDefault();
        Serializable serializable = argumentsOrDefault5.getSerializable("ARG_STREAM_PREVIEW_CLICK_BEHAVIOR");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.widgets.user.usersheet.WidgetUserSheet.StreamPreviewClickBehavior");
        WidgetUserSheet.StreamPreviewClickBehavior streamPreviewClickBehavior = (WidgetUserSheet.StreamPreviewClickBehavior) serializable;
        argumentsOrDefault6 = this.this$0.getArgumentsOrDefault();
        return new WidgetUserSheetViewModel(j, j2, Long.valueOf(j3), argumentsOrDefault6.getString("ARG_FRIEND_TOKEN"), z2, null, streamPreviewClickBehavior, null, null, null, null, null, null, null, null, 32672, null);
    }
}
