package com.discord.widgets.user.usersheet;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.widgets.user.usersheet.WidgetUserSheetViewModelStoreState;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function4;
/* compiled from: WidgetUserSheetViewModelStoreState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lcom/discord/models/user/User;", "p1", "Lcom/discord/models/user/MeUser;", "p2", "Lcom/discord/api/channel/Channel;", "p3", "p4", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelStoreState$BootstrapData;", "invoke", "(Lcom/discord/models/user/User;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelStoreState$BootstrapData;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUserSheetViewModelStoreState$observeStoreState$2 extends k implements Function4<User, MeUser, Channel, Channel, WidgetUserSheetViewModelStoreState.BootstrapData> {
    public static final WidgetUserSheetViewModelStoreState$observeStoreState$2 INSTANCE = new WidgetUserSheetViewModelStoreState$observeStoreState$2();

    public WidgetUserSheetViewModelStoreState$observeStoreState$2() {
        super(4, WidgetUserSheetViewModelStoreState.BootstrapData.class, HookHelper.constructorName, "<init>(Lcom/discord/models/user/User;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)V", 0);
    }

    public final WidgetUserSheetViewModelStoreState.BootstrapData invoke(User user, MeUser meUser, Channel channel, Channel channel2) {
        m.checkNotNullParameter(meUser, "p2");
        return new WidgetUserSheetViewModelStoreState.BootstrapData(user, meUser, channel, channel2);
    }
}
