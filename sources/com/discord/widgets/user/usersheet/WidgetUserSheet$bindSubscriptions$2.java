package com.discord.widgets.user.usersheet;

import com.discord.widgets.user.usersheet.WidgetUserSheetViewModel;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "p1", "", "invoke", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUserSheet$bindSubscriptions$2 extends k implements Function1<WidgetUserSheetViewModel.Event, Unit> {
    public WidgetUserSheet$bindSubscriptions$2(WidgetUserSheet widgetUserSheet) {
        super(1, widgetUserSheet, WidgetUserSheet.class, "handleEvent", "handleEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetUserSheetViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetUserSheetViewModel.Event event) {
        m.checkNotNullParameter(event, "p1");
        ((WidgetUserSheet) this.receiver).handleEvent(event);
    }
}
