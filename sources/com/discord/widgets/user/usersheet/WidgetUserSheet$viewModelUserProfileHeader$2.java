package com.discord.widgets.user.usersheet;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheet$viewModelUserProfileHeader$2 extends o implements Function0<AppViewModel<UserProfileHeaderViewModel.ViewState>> {
    public final /* synthetic */ WidgetUserSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheet$viewModelUserProfileHeader$2(WidgetUserSheet widgetUserSheet) {
        super(0);
        this.this$0 = widgetUserSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<UserProfileHeaderViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        Bundle argumentsOrDefault3;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        long j = argumentsOrDefault.getLong("ARG_USER_ID");
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        long j2 = argumentsOrDefault2.getLong("ARG_CHANNEL_ID");
        argumentsOrDefault3 = this.this$0.getArgumentsOrDefault();
        return new UserProfileHeaderViewModel(j, Long.valueOf(j2), Long.valueOf(argumentsOrDefault3.getLong("ARG_GUILD_ID")), null, false, null, null, 120, null);
    }
}
