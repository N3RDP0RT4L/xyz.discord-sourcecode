package com.discord.widgets.user.usersheet;

import android.content.Context;
import com.discord.api.activity.ActivityMetadata;
import com.discord.utilities.uri.UriHandler;
import d0.t.u;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/activity/ActivityMetadata;", "metaData", "", "invoke", "(Lcom/discord/api/activity/ActivityMetadata;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheetViewModel$onActivityCustomButtonClicked$1 extends o implements Function1<ActivityMetadata, Unit> {
    public final /* synthetic */ Context $applicationContext;
    public final /* synthetic */ int $buttonIndex;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheetViewModel$onActivityCustomButtonClicked$1(int i, Context context) {
        super(1);
        this.$buttonIndex = i;
        this.$applicationContext = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ActivityMetadata activityMetadata) {
        invoke2(activityMetadata);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ActivityMetadata activityMetadata) {
        List<String> b2;
        String str;
        if (activityMetadata != null && (b2 = activityMetadata.b()) != null && (str = (String) u.getOrNull(b2, this.$buttonIndex)) != null) {
            UriHandler.handleOrUntrusted(this.$applicationContext, str, "");
        }
    }
}
