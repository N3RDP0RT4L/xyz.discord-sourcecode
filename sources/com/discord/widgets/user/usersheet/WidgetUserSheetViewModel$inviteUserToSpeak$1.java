package com.discord.widgets.user.usersheet;

import com.discord.widgets.stage.usersheet.UserProfileStageActionsView;
import com.discord.widgets.user.usersheet.WidgetUserSheetViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheetViewModel$inviteUserToSpeak$1 extends o implements Function1<Void, Unit> {
    public final /* synthetic */ WidgetUserSheetViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheetViewModel$inviteUserToSpeak$1(WidgetUserSheetViewModel widgetUserSheetViewModel) {
        super(1);
        this.this$0 = widgetUserSheetViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
        invoke2(r1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Void r30) {
        WidgetUserSheetViewModel.ViewState viewState;
        UserProfileStageActionsView.ViewState viewState2;
        viewState = this.this$0.getViewState();
        if (!(viewState instanceof WidgetUserSheetViewModel.ViewState.Loaded)) {
            viewState = null;
        }
        WidgetUserSheetViewModel.ViewState.Loaded loaded = (WidgetUserSheetViewModel.ViewState.Loaded) viewState;
        if (loaded != null) {
            WidgetUserSheetViewModel widgetUserSheetViewModel = this.this$0;
            viewState2 = r12.m47copyam1GJgw((r18 & 1) != 0 ? r12.isMe : false, (r18 & 2) != 0 ? r12.channel : null, (r18 & 4) != 0 ? r12.userStageRole : null, (r18 & 8) != 0 ? r12.userRequestToSpeakState : null, (r18 & 16) != 0 ? r12.userInSameVoiceChannel : false, (r18 & 32) != 0 ? r12.canMuteMembers : false, (r18 & 64) != 0 ? r12.isUpdatingSuppressed : false, (r18 & 128) != 0 ? loaded.getStageViewState().isInvitingToSpeak : false);
            widgetUserSheetViewModel.updateViewState(WidgetUserSheetViewModel.ViewState.Loaded.copy$default(loaded, null, false, false, null, null, null, null, null, null, viewState2, null, null, null, null, 0, null, null, null, false, null, false, null, null, null, 16776703, null));
            this.this$0.emitDismissSheetEvent();
        }
    }
}
