package com.discord.widgets.user.usersheet;

import android.content.Context;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function5;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0010\u001a\u00020\r2\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\n\u0010\n\u001a\u00060\u0003j\u0002`\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000f"}, d2 = {"Landroid/content/Context;", "Lcom/discord/app/ApplicationContext;", "p1", "", "Lcom/discord/primitives/UserId;", "p2", "", "Lcom/discord/primitives/SessionId;", "p3", "Lcom/discord/primitives/ApplicationId;", "p4", "", "p5", "", "invoke", "(Landroid/content/Context;JLjava/lang/String;JI)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUserSheet$configureUI$4 extends k implements Function5<Context, Long, String, Long, Integer, Unit> {
    public WidgetUserSheet$configureUI$4(WidgetUserSheetViewModel widgetUserSheetViewModel) {
        super(5, widgetUserSheetViewModel, WidgetUserSheetViewModel.class, "onActivityCustomButtonClicked", "onActivityCustomButtonClicked(Landroid/content/Context;JLjava/lang/String;JI)V", 0);
    }

    @Override // kotlin.jvm.functions.Function5
    public /* bridge */ /* synthetic */ Unit invoke(Context context, Long l, String str, Long l2, Integer num) {
        invoke(context, l.longValue(), str, l2.longValue(), num.intValue());
        return Unit.a;
    }

    public final void invoke(Context context, long j, String str, long j2, int i) {
        m.checkNotNullParameter(context, "p1");
        m.checkNotNullParameter(str, "p3");
        ((WidgetUserSheetViewModel) this.receiver).onActivityCustomButtonClicked(context, j, str, j2, i);
    }
}
