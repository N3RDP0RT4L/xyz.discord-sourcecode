package com.discord.widgets.user.usersheet;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.api.role.GuildRole;
import com.discord.api.user.UserProfile;
import com.discord.api.voice.state.StageRequestToSpeakState;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.nullserializable.NullSerializable;
import com.discord.restapi.RestAPIParams;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.Parser;
import com.discord.stores.StoreApplicationStreamPreviews;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserNotes;
import com.discord.stores.StoreUserProfile;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.permissions.ManageUserContext;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.textprocessing.DiscordParser;
import com.discord.utilities.textprocessing.MessageParseState;
import com.discord.utilities.textprocessing.MessagePreprocessor;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.utilities.voice.PerceptualVolumeUtils;
import com.discord.widgets.stage.StageChannelAPI;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.usersheet.UserProfileStageActionsView;
import com.discord.widgets.user.presence.ModelRichPresence;
import com.discord.widgets.user.profile.UserProfileAdminView;
import com.discord.widgets.user.profile.UserProfileConnectionsView;
import com.discord.widgets.user.usersheet.UserProfileVoiceSettingsView;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import d0.g0.t;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetUserSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Â\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u0007\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 µ\u00012\b\u0012\u0004\u0012\u00020\u00020\u0001:\bµ\u0001¶\u0001·\u0001¸\u0001Bâ\u0001\u0012\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f\u0012\n\u0010 \u001a\u00060\u000ej\u0002`\u001f\u0012\u0010\u0010¤\u0001\u001a\u000b\u0018\u00010\u000ej\u0005\u0018\u0001`£\u0001\u0012\t\u0010\u0088\u0001\u001a\u0004\u0018\u00010\u001b\u0012\u0007\u0010\u0095\u0001\u001a\u000200\u0012\u0011\b\u0002\u0010²\u0001\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010+0\u0011\u0012\b\u0010\u0081\u0001\u001a\u00030\u0080\u0001\u0012\n\b\u0002\u0010\u00ad\u0001\u001a\u00030¬\u0001\u0012\n\b\u0002\u0010§\u0001\u001a\u00030¦\u0001\u0012\n\b\u0002\u0010\u0086\u0001\u001a\u00030\u0085\u0001\u0012\n\b\u0002\u0010°\u0001\u001a\u00030¯\u0001\u0012\n\b\u0002\u0010©\u0001\u001a\u00030\u0096\u0001\u0012\n\b\u0002\u0010\u0097\u0001\u001a\u00030\u0096\u0001\u0012\n\b\u0002\u0010\u009a\u0001\u001a\u00030\u0099\u0001\u0012(\b\u0002\u0010\u008d\u0001\u001a!\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0005\u0012\u00030\u008b\u00010\u008a\u0001j\u0003`\u008c\u0001¢\u0006\u0006\b³\u0001\u0010´\u0001J'\u0010\t\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005j\u0002`\b2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\f\u0010\rJ!\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000fH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0019\u0010\u0018\u001a\u00020\u00172\b\b\u0001\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001f\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u001b\u0010!\u001a\u00020\u00172\n\u0010 \u001a\u00060\u000ej\u0002`\u001fH\u0002¢\u0006\u0004\b!\u0010\"J\u001b\u0010#\u001a\u00020\u00172\n\u0010 \u001a\u00060\u000ej\u0002`\u001fH\u0002¢\u0006\u0004\b#\u0010\"J\u0017\u0010&\u001a\u00020\u00172\u0006\u0010%\u001a\u00020$H\u0002¢\u0006\u0004\b&\u0010'J\u0017\u0010(\u001a\u00020\u00172\u0006\u0010%\u001a\u00020$H\u0002¢\u0006\u0004\b(\u0010'J\u000f\u0010)\u001a\u00020\u0017H\u0002¢\u0006\u0004\b)\u0010*J\u0019\u0010-\u001a\u00020\u00172\b\u0010,\u001a\u0004\u0018\u00010+H\u0002¢\u0006\u0004\b-\u0010.JO\u0010:\u001a\u0004\u0018\u0001092\b\u0010/\u001a\u0004\u0018\u00010\u00122\u0006\u00101\u001a\u0002002\u0006\u00102\u001a\u0002002\b\u00104\u001a\u0004\u0018\u0001032\b\u00106\u001a\u0004\u0018\u0001052\u0006\u00107\u001a\u0002002\u0006\u00108\u001a\u000200H\u0002¢\u0006\u0004\b:\u0010;J'\u0010@\u001a\u00020?2\u0006\u0010=\u001a\u00020<2\u0006\u00102\u001a\u0002002\u0006\u0010>\u001a\u000200H\u0002¢\u0006\u0004\b@\u0010AJ#\u0010D\u001a\u0002002\b\u0010B\u001a\u0004\u0018\u0001052\b\u0010C\u001a\u0004\u0018\u000105H\u0002¢\u0006\u0004\bD\u0010EJ-\u0010G\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0018\u00010\u0005j\u0004\u0018\u0001`\b2\b\u0010F\u001a\u0004\u0018\u00010\u001bH\u0002¢\u0006\u0004\bG\u0010HJ\u0013\u0010J\u001a\b\u0012\u0004\u0012\u00020I0\u0011¢\u0006\u0004\bJ\u0010KJ5\u0010O\u001a\u00020\u00172\b\u0010L\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u001c\u001a\u00020\u001b2\b\b\u0001\u0010M\u001a\u00020\u00152\n\b\u0002\u0010N\u001a\u0004\u0018\u00010\u001b¢\u0006\u0004\bO\u0010PJ\u0017\u0010Q\u001a\u00020\u00172\b\b\u0001\u0010M\u001a\u00020\u0015¢\u0006\u0004\bQ\u0010\u0019J\r\u0010R\u001a\u00020\u0017¢\u0006\u0004\bR\u0010*J\r\u0010S\u001a\u00020\u0017¢\u0006\u0004\bS\u0010*J\u0015\u0010U\u001a\u00020\u00172\u0006\u0010T\u001a\u000200¢\u0006\u0004\bU\u0010VJ\u0015\u0010W\u001a\u00020\u00172\u0006\u0010T\u001a\u000200¢\u0006\u0004\bW\u0010VJ\u0015\u0010Z\u001a\u00020\u00172\u0006\u0010Y\u001a\u00020X¢\u0006\u0004\bZ\u0010[J\r\u0010\\\u001a\u00020\u0017¢\u0006\u0004\b\\\u0010*J\r\u0010]\u001a\u00020\u0017¢\u0006\u0004\b]\u0010*J\r\u0010^\u001a\u00020\u0017¢\u0006\u0004\b^\u0010*J\r\u0010_\u001a\u00020\u0017¢\u0006\u0004\b_\u0010*J\r\u0010`\u001a\u00020\u0017¢\u0006\u0004\b`\u0010*J\r\u0010a\u001a\u00020\u0017¢\u0006\u0004\ba\u0010*J\r\u0010b\u001a\u00020\u0017¢\u0006\u0004\bb\u0010*J\u0019\u0010c\u001a\u00020\u00172\n\u0010 \u001a\u00060\u000ej\u0002`\u001f¢\u0006\u0004\bc\u0010\"J\u0019\u0010d\u001a\u00020\u00172\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\bd\u0010eJ\u001f\u0010i\u001a\u00020\u00172\b\u0010g\u001a\u0004\u0018\u00010f2\u0006\u0010h\u001a\u00020\u001b¢\u0006\u0004\bi\u0010jJ\u0015\u0010m\u001a\u00020\u00172\u0006\u0010l\u001a\u00020k¢\u0006\u0004\bm\u0010nJ\u0017\u0010o\u001a\u00020\u00172\u0006\u0010%\u001a\u00020$H\u0007¢\u0006\u0004\bo\u0010'JE\u0010w\u001a\u00020\u00172\n\u0010q\u001a\u00060fj\u0002`p2\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f2\n\u0010s\u001a\u00060\u001bj\u0002`r2\n\u0010u\u001a\u00060\u000ej\u0002`t2\u0006\u0010v\u001a\u00020\u0015¢\u0006\u0004\bw\u0010xJ\u0015\u0010z\u001a\u00020\u00172\u0006\u0010y\u001a\u000200¢\u0006\u0004\bz\u0010VJ\r\u0010{\u001a\u00020\u0017¢\u0006\u0004\b{\u0010*J\u001b\u0010~\u001a\u00020\u00172\n\u0010}\u001a\u0006\u0012\u0002\b\u00030|H\u0007¢\u0006\u0004\b~\u0010\u007fR\u001a\u0010\u0081\u0001\u001a\u00030\u0080\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001R\u001b\u0010\u0083\u0001\u001a\u0004\u0018\u00010+8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0083\u0001\u0010\u0084\u0001R\u001a\u0010\u0086\u0001\u001a\u00030\u0085\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0086\u0001\u0010\u0087\u0001R\u001b\u0010\u0088\u0001\u001a\u0004\u0018\u00010\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0088\u0001\u0010\u0089\u0001R8\u0010\u008d\u0001\u001a!\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0005\u0012\u00030\u008b\u00010\u008a\u0001j\u0003`\u008c\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008d\u0001\u0010\u008e\u0001R\u001b\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0010\u0010\u008f\u0001R(\u0010\u0090\u0001\u001a\u0002008\u0006@\u0006X\u0086\u000e¢\u0006\u0017\n\u0006\b\u0090\u0001\u0010\u0091\u0001\u001a\u0006\b\u0092\u0001\u0010\u0093\u0001\"\u0005\b\u0094\u0001\u0010VR\u0019\u0010\u0095\u0001\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0095\u0001\u0010\u0091\u0001R\u001a\u0010\u0097\u0001\u001a\u00030\u0096\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0097\u0001\u0010\u0098\u0001R\u001a\u0010\u009a\u0001\u001a\u00030\u0099\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009a\u0001\u0010\u009b\u0001RB\u0010\u009e\u0001\u001a+\u0012\r\u0012\u000b \u009d\u0001*\u0004\u0018\u00010I0I \u009d\u0001*\u0014\u0012\r\u0012\u000b \u009d\u0001*\u0004\u0018\u00010I0I\u0018\u00010\u009c\u00010\u009c\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009e\u0001\u0010\u009f\u0001R\u001b\u0010 \u001a\u00060\u000ej\u0002`\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b \u0010\u008f\u0001R \u0010¡\u0001\u001a\t\u0012\u0004\u0012\u00020\u00150 \u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¡\u0001\u0010¢\u0001R\"\u0010¤\u0001\u001a\u000b\u0018\u00010\u000ej\u0005\u0018\u0001`£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¤\u0001\u0010¥\u0001R\u001a\u0010§\u0001\u001a\u00030¦\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b§\u0001\u0010¨\u0001R\u001a\u0010©\u0001\u001a\u00030\u0096\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b©\u0001\u0010\u0098\u0001R%\u0010«\u0001\u001a\u000e\u0012\t\u0012\u00070\u001bj\u0003`ª\u00010 \u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b«\u0001\u0010¢\u0001R\u001a\u0010\u00ad\u0001\u001a\u00030¬\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u00ad\u0001\u0010®\u0001R\u001a\u0010°\u0001\u001a\u00030¯\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b°\u0001\u0010±\u0001¨\u0006¹\u0001"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/user/usersheet/AST;", "generateAst", "(Ljava/lang/CharSequence;)Ljava/util/List;", "Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "createMessagePreprocessor", "()Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "", "Lcom/discord/primitives/UserId;", "userId", "Lrx/Observable;", "Lcom/discord/api/channel/Channel;", "createPrivateChannelWithUser", "(J)Lrx/Observable;", "", "stringRes", "", "emitShowToastEvent", "(I)V", "abortCode", "", "username", "emitShowFriendRequestAbortToast", "(ILjava/lang/String;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "emitLaunchVoiceCallEvent", "(J)V", "emitLaunchVideoCallEvent", "Lcom/discord/models/domain/ModelApplicationStream;", "stream", "emitLaunchSpectateEvent", "(Lcom/discord/models/domain/ModelApplicationStream;)V", "emitRequestStreamPermissionsEvent", "emitDismissSheetEvent", "()V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;)V", "channel", "", "isChannelOwner", "isMe", "Lcom/discord/utilities/permissions/ManageUserContext;", "manageUserContext", "Lcom/discord/api/voice/state/VoiceState;", "channelVoiceState", "canDisableCommunication", "isCommunicationDisabled", "Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "createAdminViewState", "(Lcom/discord/api/channel/Channel;ZZLcom/discord/utilities/permissions/ManageUserContext;Lcom/discord/api/voice/state/VoiceState;ZZ)Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "Lcom/discord/api/user/UserProfile;", "userProfile", "isSystemUser", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "createConnectionsViewState", "(Lcom/discord/api/user/UserProfile;ZZ)Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "myVoiceState", "userVoiceState", "isInSameVoiceChannel", "(Lcom/discord/api/voice/state/VoiceState;Lcom/discord/api/voice/state/VoiceState;)Z", "bio", "createAndProcessBioAstFromText", "(Ljava/lang/String;)Ljava/util/List;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "observeEvents", "()Lrx/Observable;", "type", "successMessageStringRes", "captchaKey", "addRelationship", "(Ljava/lang/Integer;Ljava/lang/String;ILjava/lang/String;)V", "removeRelationship", "launchVoiceCall", "launchVideoCall", "isChecked", "toggleMute", "(Z)V", "toggleDeafen", "", "volume", "setUserOutputVolume", "(F)V", "editMember", "kickUser", "banUser", "disableCommunication", "guildMuteUser", "guildDeafenUser", "guildMoveForUser", "moveUserToChannel", "disconnectUser", "(Lcom/discord/api/channel/Channel;)V", "Landroid/content/Context;", "context", "noteText", "saveUserNote", "(Landroid/content/Context;Ljava/lang/String;)V", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "onStreamPreviewClicked", "(Lcom/discord/utilities/streams/StreamContext;)V", "onSpectatePermissionsGranted", "Lcom/discord/app/ApplicationContext;", "applicationContext", "Lcom/discord/primitives/SessionId;", "sessionId", "Lcom/discord/primitives/ApplicationId;", "applicationId", "buttonIndex", "onActivityCustomButtonClicked", "(Landroid/content/Context;JLjava/lang/String;JI)V", "isSuppressed", "setUserSuppressedInChannel", "inviteUserToSpeak", "Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "spoilerNode", "handleBioIndexClicked", "(Lcom/discord/utilities/textprocessing/node/SpoilerNode;)V", "Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;", "streamPreviewClickBehavior", "Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;", "mostRecentStoreState", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "Lcom/discord/stores/StoreUserNotes;", "storeUserNotes", "Lcom/discord/stores/StoreUserNotes;", "friendToken", "Ljava/lang/String;", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/utilities/textprocessing/MessageParseState;", "Lcom/discord/widgets/user/usersheet/BioParser;", "bioParser", "Lcom/discord/simpleast/core/parser/Parser;", "J", "openPopoutLogged", "Z", "getOpenPopoutLogged", "()Z", "setOpenPopoutLogged", "isVoiceContext", "Lcom/discord/utilities/rest/RestAPI;", "restAPISerializeNulls", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "storeApplicationStreamPreviews", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "", "revealedBioIndices", "Ljava/util/Set;", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/Long;", "Lcom/discord/stores/StoreApplicationStreaming;", "storeApplicationStreaming", "Lcom/discord/stores/StoreApplicationStreaming;", "restAPI", "Lcom/discord/primitives/StreamKey;", "fetchedPreviews", "Lcom/discord/stores/StoreMediaSettings;", "storeMediaSettings", "Lcom/discord/stores/StoreMediaSettings;", "Lcom/discord/stores/StoreUserProfile;", "storeUserProfile", "Lcom/discord/stores/StoreUserProfile;", "storeObservable", HookHelper.constructorName, "(JJLjava/lang/Long;Ljava/lang/String;ZLrx/Observable;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUserNotes;Lcom/discord/stores/StoreUserProfile;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/simpleast/core/parser/Parser;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    public static final String LOCATION = "User Profile";
    private final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> bioParser;
    private final long channelId;
    private final PublishSubject<Event> eventSubject;
    private Set<String> fetchedPreviews;
    private final String friendToken;
    private final Long guildId;
    private final boolean isVoiceContext;
    private StoreState mostRecentStoreState;
    private boolean openPopoutLogged;
    private final RestAPI restAPI;
    private final RestAPI restAPISerializeNulls;
    private final Set<Integer> revealedBioIndices;
    private final StoreApplicationStreamPreviews storeApplicationStreamPreviews;
    private final StoreApplicationStreaming storeApplicationStreaming;
    private final StoreMediaSettings storeMediaSettings;
    private final StoreUserNotes storeUserNotes;
    private final StoreUserProfile storeUserProfile;
    private final WidgetUserSheet.StreamPreviewClickBehavior streamPreviewClickBehavior;
    private final long userId;

    /* compiled from: WidgetUserSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheetViewModel$1 */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1() {
            super(1);
            WidgetUserSheetViewModel.this = r1;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(StoreState storeState) {
            WidgetUserSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetUserSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Companion;", "", "", "LOCATION", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u000e\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u000e\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f¨\u0006 "}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "DismissSheet", "LaunchBanUser", "LaunchDisableCommunication", "LaunchEditMember", "LaunchEnableCommunication", "LaunchKickUser", "LaunchMoveUser", "LaunchSpectate", "LaunchVideoCall", "LaunchVoiceCall", "RequestPermissionsForSpectateStream", "ShowFriendRequestErrorToast", "ShowToast", "UserNotFound", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchDisableCommunication;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEnableCommunication;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$UserNotFound;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$DismissSheet;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$DismissSheet;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DismissSheet extends Event {
            public static final DismissSheet INSTANCE = new DismissSheet();

            private DismissSheet() {
                super(null);
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\n\u0010\f\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010\r\u001a\u00060\u0005j\u0002`\t¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0005j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\bJ6\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\f\b\u0002\u0010\f\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010\r\u001a\u00060\u0005j\u0002`\tHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001d\u0010\r\u001a\u00060\u0005j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\bR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001d\u0010\f\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001d\u0010\b¨\u0006 "}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/GuildId;", "component2", "()J", "Lcom/discord/primitives/UserId;", "component3", "username", "guildId", "userId", "copy", "(Ljava/lang/String;JJ)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getUserId", "Ljava/lang/String;", "getUsername", "getGuildId", HookHelper.constructorName, "(Ljava/lang/String;JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchBanUser extends Event {
            private final long guildId;
            private final long userId;
            private final String username;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public LaunchBanUser(String str, long j, long j2) {
                super(null);
                m.checkNotNullParameter(str, "username");
                this.username = str;
                this.guildId = j;
                this.userId = j2;
            }

            public static /* synthetic */ LaunchBanUser copy$default(LaunchBanUser launchBanUser, String str, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = launchBanUser.username;
                }
                if ((i & 2) != 0) {
                    j = launchBanUser.guildId;
                }
                long j3 = j;
                if ((i & 4) != 0) {
                    j2 = launchBanUser.userId;
                }
                return launchBanUser.copy(str, j3, j2);
            }

            public final String component1() {
                return this.username;
            }

            public final long component2() {
                return this.guildId;
            }

            public final long component3() {
                return this.userId;
            }

            public final LaunchBanUser copy(String str, long j, long j2) {
                m.checkNotNullParameter(str, "username");
                return new LaunchBanUser(str, j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof LaunchBanUser)) {
                    return false;
                }
                LaunchBanUser launchBanUser = (LaunchBanUser) obj;
                return m.areEqual(this.username, launchBanUser.username) && this.guildId == launchBanUser.guildId && this.userId == launchBanUser.userId;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final long getUserId() {
                return this.userId;
            }

            public final String getUsername() {
                return this.username;
            }

            public int hashCode() {
                String str = this.username;
                int hashCode = str != null ? str.hashCode() : 0;
                return b.a(this.userId) + ((b.a(this.guildId) + (hashCode * 31)) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("LaunchBanUser(username=");
                R.append(this.username);
                R.append(", guildId=");
                R.append(this.guildId);
                R.append(", userId=");
                return a.B(R, this.userId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\u0010\b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J,\u0010\n\u001a\u00020\u00002\f\b\u0002\u0010\b\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u0006HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u001d\u0010\b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0019\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchDisableCommunication;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "userId", "guildId", "copy", "(JJ)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchDisableCommunication;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "getUserId", HookHelper.constructorName, "(JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchDisableCommunication extends Event {
            private final long guildId;
            private final long userId;

            public LaunchDisableCommunication(long j, long j2) {
                super(null);
                this.userId = j;
                this.guildId = j2;
            }

            public static /* synthetic */ LaunchDisableCommunication copy$default(LaunchDisableCommunication launchDisableCommunication, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchDisableCommunication.userId;
                }
                if ((i & 2) != 0) {
                    j2 = launchDisableCommunication.guildId;
                }
                return launchDisableCommunication.copy(j, j2);
            }

            public final long component1() {
                return this.userId;
            }

            public final long component2() {
                return this.guildId;
            }

            public final LaunchDisableCommunication copy(long j, long j2) {
                return new LaunchDisableCommunication(j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof LaunchDisableCommunication)) {
                    return false;
                }
                LaunchDisableCommunication launchDisableCommunication = (LaunchDisableCommunication) obj;
                return this.userId == launchDisableCommunication.userId && this.guildId == launchDisableCommunication.guildId;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final long getUserId() {
                return this.userId;
            }

            public int hashCode() {
                return b.a(this.guildId) + (b.a(this.userId) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("LaunchDisableCommunication(userId=");
                R.append(this.userId);
                R.append(", guildId=");
                return a.B(R, this.guildId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\u0010\b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J,\u0010\n\u001a\u00020\u00002\f\b\u0002\u0010\b\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u0006HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0019\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "Lcom/discord/primitives/UserId;", "component2", "guildId", "userId", "copy", "(JJ)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "getUserId", HookHelper.constructorName, "(JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchEditMember extends Event {
            private final long guildId;
            private final long userId;

            public LaunchEditMember(long j, long j2) {
                super(null);
                this.guildId = j;
                this.userId = j2;
            }

            public static /* synthetic */ LaunchEditMember copy$default(LaunchEditMember launchEditMember, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchEditMember.guildId;
                }
                if ((i & 2) != 0) {
                    j2 = launchEditMember.userId;
                }
                return launchEditMember.copy(j, j2);
            }

            public final long component1() {
                return this.guildId;
            }

            public final long component2() {
                return this.userId;
            }

            public final LaunchEditMember copy(long j, long j2) {
                return new LaunchEditMember(j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof LaunchEditMember)) {
                    return false;
                }
                LaunchEditMember launchEditMember = (LaunchEditMember) obj;
                return this.guildId == launchEditMember.guildId && this.userId == launchEditMember.userId;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final long getUserId() {
                return this.userId;
            }

            public int hashCode() {
                return b.a(this.userId) + (b.a(this.guildId) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("LaunchEditMember(guildId=");
                R.append(this.guildId);
                R.append(", userId=");
                return a.B(R, this.userId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\u0010\b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J,\u0010\n\u001a\u00020\u00002\f\b\u0002\u0010\b\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u0006HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u001d\u0010\b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0019\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEnableCommunication;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "userId", "guildId", "copy", "(JJ)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEnableCommunication;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "getUserId", HookHelper.constructorName, "(JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchEnableCommunication extends Event {
            private final long guildId;
            private final long userId;

            public LaunchEnableCommunication(long j, long j2) {
                super(null);
                this.userId = j;
                this.guildId = j2;
            }

            public static /* synthetic */ LaunchEnableCommunication copy$default(LaunchEnableCommunication launchEnableCommunication, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchEnableCommunication.userId;
                }
                if ((i & 2) != 0) {
                    j2 = launchEnableCommunication.guildId;
                }
                return launchEnableCommunication.copy(j, j2);
            }

            public final long component1() {
                return this.userId;
            }

            public final long component2() {
                return this.guildId;
            }

            public final LaunchEnableCommunication copy(long j, long j2) {
                return new LaunchEnableCommunication(j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof LaunchEnableCommunication)) {
                    return false;
                }
                LaunchEnableCommunication launchEnableCommunication = (LaunchEnableCommunication) obj;
                return this.userId == launchEnableCommunication.userId && this.guildId == launchEnableCommunication.guildId;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final long getUserId() {
                return this.userId;
            }

            public int hashCode() {
                return b.a(this.guildId) + (b.a(this.userId) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("LaunchEnableCommunication(userId=");
                R.append(this.userId);
                R.append(", guildId=");
                return a.B(R, this.guildId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\n\u0010\f\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010\r\u001a\u00060\u0005j\u0002`\t¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0005j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\bJ6\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\f\b\u0002\u0010\f\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010\r\u001a\u00060\u0005j\u0002`\tHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001d\u0010\r\u001a\u00060\u0005j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\bR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001d\u0010\f\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001d\u0010\b¨\u0006 "}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/GuildId;", "component2", "()J", "Lcom/discord/primitives/UserId;", "component3", "username", "guildId", "userId", "copy", "(Ljava/lang/String;JJ)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getUserId", "Ljava/lang/String;", "getUsername", "getGuildId", HookHelper.constructorName, "(Ljava/lang/String;JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchKickUser extends Event {
            private final long guildId;
            private final long userId;
            private final String username;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public LaunchKickUser(String str, long j, long j2) {
                super(null);
                m.checkNotNullParameter(str, "username");
                this.username = str;
                this.guildId = j;
                this.userId = j2;
            }

            public static /* synthetic */ LaunchKickUser copy$default(LaunchKickUser launchKickUser, String str, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = launchKickUser.username;
                }
                if ((i & 2) != 0) {
                    j = launchKickUser.guildId;
                }
                long j3 = j;
                if ((i & 4) != 0) {
                    j2 = launchKickUser.userId;
                }
                return launchKickUser.copy(str, j3, j2);
            }

            public final String component1() {
                return this.username;
            }

            public final long component2() {
                return this.guildId;
            }

            public final long component3() {
                return this.userId;
            }

            public final LaunchKickUser copy(String str, long j, long j2) {
                m.checkNotNullParameter(str, "username");
                return new LaunchKickUser(str, j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof LaunchKickUser)) {
                    return false;
                }
                LaunchKickUser launchKickUser = (LaunchKickUser) obj;
                return m.areEqual(this.username, launchKickUser.username) && this.guildId == launchKickUser.guildId && this.userId == launchKickUser.userId;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final long getUserId() {
                return this.userId;
            }

            public final String getUsername() {
                return this.username;
            }

            public int hashCode() {
                String str = this.username;
                int hashCode = str != null ? str.hashCode() : 0;
                return b.a(this.userId) + ((b.a(this.guildId) + (hashCode * 31)) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("LaunchKickUser(username=");
                R.append(this.username);
                R.append(", guildId=");
                R.append(this.guildId);
                R.append(", userId=");
                return a.B(R, this.userId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "guildId", "copy", "(J)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchMoveUser extends Event {
            private final long guildId;

            public LaunchMoveUser(long j) {
                super(null);
                this.guildId = j;
            }

            public static /* synthetic */ LaunchMoveUser copy$default(LaunchMoveUser launchMoveUser, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchMoveUser.guildId;
                }
                return launchMoveUser.copy(j);
            }

            public final long component1() {
                return this.guildId;
            }

            public final LaunchMoveUser copy(long j) {
                return new LaunchMoveUser(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof LaunchMoveUser) && this.guildId == ((LaunchMoveUser) obj).guildId;
                }
                return true;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public int hashCode() {
                return b.a(this.guildId);
            }

            public String toString() {
                return a.B(a.R("LaunchMoveUser(guildId="), this.guildId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "Lcom/discord/models/domain/ModelApplicationStream;", "component1", "()Lcom/discord/models/domain/ModelApplicationStream;", "stream", "copy", "(Lcom/discord/models/domain/ModelApplicationStream;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelApplicationStream;", "getStream", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelApplicationStream;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchSpectate extends Event {
            private final ModelApplicationStream stream;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public LaunchSpectate(ModelApplicationStream modelApplicationStream) {
                super(null);
                m.checkNotNullParameter(modelApplicationStream, "stream");
                this.stream = modelApplicationStream;
            }

            public static /* synthetic */ LaunchSpectate copy$default(LaunchSpectate launchSpectate, ModelApplicationStream modelApplicationStream, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelApplicationStream = launchSpectate.stream;
                }
                return launchSpectate.copy(modelApplicationStream);
            }

            public final ModelApplicationStream component1() {
                return this.stream;
            }

            public final LaunchSpectate copy(ModelApplicationStream modelApplicationStream) {
                m.checkNotNullParameter(modelApplicationStream, "stream");
                return new LaunchSpectate(modelApplicationStream);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof LaunchSpectate) && m.areEqual(this.stream, ((LaunchSpectate) obj).stream);
                }
                return true;
            }

            public final ModelApplicationStream getStream() {
                return this.stream;
            }

            public int hashCode() {
                ModelApplicationStream modelApplicationStream = this.stream;
                if (modelApplicationStream != null) {
                    return modelApplicationStream.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("LaunchSpectate(stream=");
                R.append(this.stream);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "channelId", "copy", "(J)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchVideoCall extends Event {
            private final long channelId;

            public LaunchVideoCall(long j) {
                super(null);
                this.channelId = j;
            }

            public static /* synthetic */ LaunchVideoCall copy$default(LaunchVideoCall launchVideoCall, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchVideoCall.channelId;
                }
                return launchVideoCall.copy(j);
            }

            public final long component1() {
                return this.channelId;
            }

            public final LaunchVideoCall copy(long j) {
                return new LaunchVideoCall(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof LaunchVideoCall) && this.channelId == ((LaunchVideoCall) obj).channelId;
                }
                return true;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                return b.a(this.channelId);
            }

            public String toString() {
                return a.B(a.R("LaunchVideoCall(channelId="), this.channelId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "channelId", "copy", "(J)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchVoiceCall extends Event {
            private final long channelId;

            public LaunchVoiceCall(long j) {
                super(null);
                this.channelId = j;
            }

            public static /* synthetic */ LaunchVoiceCall copy$default(LaunchVoiceCall launchVoiceCall, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchVoiceCall.channelId;
                }
                return launchVoiceCall.copy(j);
            }

            public final long component1() {
                return this.channelId;
            }

            public final LaunchVoiceCall copy(long j) {
                return new LaunchVoiceCall(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof LaunchVoiceCall) && this.channelId == ((LaunchVoiceCall) obj).channelId;
                }
                return true;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                return b.a(this.channelId);
            }

            public String toString() {
                return a.B(a.R("LaunchVoiceCall(channelId="), this.channelId, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "Lcom/discord/models/domain/ModelApplicationStream;", "component1", "()Lcom/discord/models/domain/ModelApplicationStream;", "stream", "copy", "(Lcom/discord/models/domain/ModelApplicationStream;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelApplicationStream;", "getStream", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelApplicationStream;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RequestPermissionsForSpectateStream extends Event {
            private final ModelApplicationStream stream;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public RequestPermissionsForSpectateStream(ModelApplicationStream modelApplicationStream) {
                super(null);
                m.checkNotNullParameter(modelApplicationStream, "stream");
                this.stream = modelApplicationStream;
            }

            public static /* synthetic */ RequestPermissionsForSpectateStream copy$default(RequestPermissionsForSpectateStream requestPermissionsForSpectateStream, ModelApplicationStream modelApplicationStream, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelApplicationStream = requestPermissionsForSpectateStream.stream;
                }
                return requestPermissionsForSpectateStream.copy(modelApplicationStream);
            }

            public final ModelApplicationStream component1() {
                return this.stream;
            }

            public final RequestPermissionsForSpectateStream copy(ModelApplicationStream modelApplicationStream) {
                m.checkNotNullParameter(modelApplicationStream, "stream");
                return new RequestPermissionsForSpectateStream(modelApplicationStream);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof RequestPermissionsForSpectateStream) && m.areEqual(this.stream, ((RequestPermissionsForSpectateStream) obj).stream);
                }
                return true;
            }

            public final ModelApplicationStream getStream() {
                return this.stream;
            }

            public int hashCode() {
                ModelApplicationStream modelApplicationStream = this.stream;
                if (modelApplicationStream != null) {
                    return modelApplicationStream.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("RequestPermissionsForSpectateStream(stream=");
                R.append(this.stream);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0013\u001a\u0004\b\u0014\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "abortCode", "username", "copy", "(ILjava/lang/String;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getUsername", "I", "getAbortCode", HookHelper.constructorName, "(ILjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowFriendRequestErrorToast extends Event {
            private final int abortCode;
            private final String username;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ShowFriendRequestErrorToast(int i, String str) {
                super(null);
                m.checkNotNullParameter(str, "username");
                this.abortCode = i;
                this.username = str;
            }

            public static /* synthetic */ ShowFriendRequestErrorToast copy$default(ShowFriendRequestErrorToast showFriendRequestErrorToast, int i, String str, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showFriendRequestErrorToast.abortCode;
                }
                if ((i2 & 2) != 0) {
                    str = showFriendRequestErrorToast.username;
                }
                return showFriendRequestErrorToast.copy(i, str);
            }

            public final int component1() {
                return this.abortCode;
            }

            public final String component2() {
                return this.username;
            }

            public final ShowFriendRequestErrorToast copy(int i, String str) {
                m.checkNotNullParameter(str, "username");
                return new ShowFriendRequestErrorToast(i, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ShowFriendRequestErrorToast)) {
                    return false;
                }
                ShowFriendRequestErrorToast showFriendRequestErrorToast = (ShowFriendRequestErrorToast) obj;
                return this.abortCode == showFriendRequestErrorToast.abortCode && m.areEqual(this.username, showFriendRequestErrorToast.username);
            }

            public final int getAbortCode() {
                return this.abortCode;
            }

            public final String getUsername() {
                return this.username;
            }

            public int hashCode() {
                int i = this.abortCode * 31;
                String str = this.username;
                return i + (str != null ? str.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("ShowFriendRequestErrorToast(abortCode=");
                R.append(this.abortCode);
                R.append(", username=");
                return a.H(R, this.username, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", "", "component1", "()I", "stringRes", "copy", "(I)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowToast extends Event {
            private final int stringRes;

            public ShowToast(int i) {
                super(null);
                this.stringRes = i;
            }

            public static /* synthetic */ ShowToast copy$default(ShowToast showToast, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showToast.stringRes;
                }
                return showToast.copy(i);
            }

            public final int component1() {
                return this.stringRes;
            }

            public final ShowToast copy(int i) {
                return new ShowToast(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowToast) && this.stringRes == ((ShowToast) obj).stringRes;
                }
                return true;
            }

            public final int getStringRes() {
                return this.stringRes;
            }

            public int hashCode() {
                return this.stringRes;
            }

            public String toString() {
                return a.A(a.R("ShowToast(stringRes="), this.stringRes, ")");
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$UserNotFound;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UserNotFound extends Event {
            public static final UserNotFound INSTANCE = new UserNotFound();

            private UserNotFound() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000¦\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0010\u000e\n\u0002\b0\b\u0086\b\u0018\u00002\u00020\u0001B\u009a\u0002\u0012\u0006\u0010?\u001a\u00020\u0002\u0012\u0006\u0010@\u001a\u00020\u0005\u0012\b\u0010A\u001a\u0004\u0018\u00010\b\u0012\u0012\u0010B\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u0012\u0012\u0010C\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00100\u000b\u0012\u0016\u0010D\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000b\u0012\u0016\u0010E\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000b\u0012\u0006\u0010F\u001a\u00020\u0016\u0012\u0006\u0010G\u001a\u00020\u0016\u0012\u0006\u0010H\u001a\u00020\u0016\u0012\u0006\u0010I\u001a\u00020\u001b\u0012\b\u0010J\u001a\u0004\u0018\u00010\u001e\u0012\b\u0010K\u001a\u0004\u0018\u00010!\u0012\u000e\u0010L\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`$\u0012\b\u0010M\u001a\u0004\u0018\u00010'\u0012\u0006\u0010N\u001a\u00020*\u0012\u000e\u0010O\u001a\n\u0018\u00010-j\u0004\u0018\u0001`.\u0012\u0006\u0010P\u001a\u000201\u0012\b\u0010Q\u001a\u0004\u0018\u00010\b\u0012\b\u0010R\u001a\u0004\u0018\u000105\u0012\u0006\u0010S\u001a\u000209\u0012\b\u0010T\u001a\u0004\u0018\u000105\u0012\u0006\u0010U\u001a\u00020\u0016ø\u0001\u0000¢\u0006\u0006\b\u0087\u0001\u0010\u0088\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u001c\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u001c\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00100\u000bHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000fJ \u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u000fJ \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000bHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u000fJ\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0018J\u0010\u0010\u001c\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0012\u0010\"\u001a\u0004\u0018\u00010!HÆ\u0003¢\u0006\u0004\b\"\u0010#J\u0018\u0010%\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`$HÆ\u0003¢\u0006\u0004\b%\u0010&J\u0012\u0010(\u001a\u0004\u0018\u00010'HÆ\u0003¢\u0006\u0004\b(\u0010)J\u0010\u0010+\u001a\u00020*HÆ\u0003¢\u0006\u0004\b+\u0010,J\u0018\u0010/\u001a\n\u0018\u00010-j\u0004\u0018\u0001`.HÆ\u0003¢\u0006\u0004\b/\u00100J\u0010\u00102\u001a\u000201HÆ\u0003¢\u0006\u0004\b2\u00103J\u0012\u00104\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b4\u0010\nJ\u001b\u00108\u001a\u0004\u0018\u000105HÆ\u0003ø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b6\u00107J\u0010\u0010:\u001a\u000209HÆ\u0003¢\u0006\u0004\b:\u0010;J\u001b\u0010=\u001a\u0004\u0018\u000105HÆ\u0003ø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b<\u00107J\u0010\u0010>\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b>\u0010\u0018JÒ\u0002\u0010X\u001a\u00020\u00002\b\b\u0002\u0010?\u001a\u00020\u00022\b\b\u0002\u0010@\u001a\u00020\u00052\n\b\u0002\u0010A\u001a\u0004\u0018\u00010\b2\u0014\b\u0002\u0010B\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b2\u0014\b\u0002\u0010C\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00100\u000b2\u0018\b\u0002\u0010D\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000b2\u0018\b\u0002\u0010E\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000b2\b\b\u0002\u0010F\u001a\u00020\u00162\b\b\u0002\u0010G\u001a\u00020\u00162\b\b\u0002\u0010H\u001a\u00020\u00162\b\b\u0002\u0010I\u001a\u00020\u001b2\n\b\u0002\u0010J\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010K\u001a\u0004\u0018\u00010!2\u0010\b\u0002\u0010L\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`$2\n\b\u0002\u0010M\u001a\u0004\u0018\u00010'2\b\b\u0002\u0010N\u001a\u00020*2\u0010\b\u0002\u0010O\u001a\n\u0018\u00010-j\u0004\u0018\u0001`.2\b\b\u0002\u0010P\u001a\u0002012\n\b\u0002\u0010Q\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010R\u001a\u0004\u0018\u0001052\b\b\u0002\u0010S\u001a\u0002092\n\b\u0002\u0010T\u001a\u0004\u0018\u0001052\b\b\u0002\u0010U\u001a\u00020\u0016HÆ\u0001ø\u0001\u0000ø\u0001\u0002¢\u0006\u0004\bV\u0010WJ\u0010\u0010Z\u001a\u00020YHÖ\u0001¢\u0006\u0004\bZ\u0010[J\u0010\u0010\\\u001a\u00020-HÖ\u0001¢\u0006\u0004\b\\\u0010]J\u001a\u0010_\u001a\u00020\u00162\b\u0010^\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b_\u0010`R%\u0010C\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00100\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010a\u001a\u0004\bb\u0010\u000fR\u0019\u0010P\u001a\u0002018\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010c\u001a\u0004\bd\u00103R\u0019\u0010G\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010e\u001a\u0004\bf\u0010\u0018R\u0019\u0010H\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010e\u001a\u0004\bg\u0010\u0018R\u0019\u0010@\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010h\u001a\u0004\bi\u0010\u0007R\u0019\u0010S\u001a\u0002098\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010j\u001a\u0004\bk\u0010;R\u001b\u0010M\u001a\u0004\u0018\u00010'8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010l\u001a\u0004\bm\u0010)R\u0019\u0010?\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010n\u001a\u0004\bo\u0010\u0004R%\u0010B\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010a\u001a\u0004\bp\u0010\u000fR)\u0010E\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010a\u001a\u0004\bq\u0010\u000fR$\u0010R\u001a\u0004\u0018\u0001058\u0006@\u0006ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\f\n\u0004\bR\u0010r\u001a\u0004\bs\u00107R\u0019\u0010U\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010e\u001a\u0004\bt\u0010\u0018R\u001b\u0010Q\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010u\u001a\u0004\bv\u0010\nR\u001b\u0010K\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010w\u001a\u0004\bx\u0010#R\u0019\u0010I\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010y\u001a\u0004\bz\u0010\u001dR\u001b\u0010J\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010{\u001a\u0004\b|\u0010 R!\u0010L\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`$8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010}\u001a\u0004\b~\u0010&R\u001b\u0010A\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010u\u001a\u0004\b\u007f\u0010\nR\u001b\u0010N\u001a\u00020*8\u0006@\u0006¢\u0006\u000e\n\u0005\bN\u0010\u0080\u0001\u001a\u0005\b\u0081\u0001\u0010,R#\u0010O\u001a\n\u0018\u00010-j\u0004\u0018\u0001`.8\u0006@\u0006¢\u0006\u000e\n\u0005\bO\u0010\u0082\u0001\u001a\u0005\b\u0083\u0001\u00100R%\u0010T\u001a\u0004\u0018\u0001058\u0006@\u0006ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\r\n\u0004\bT\u0010r\u001a\u0005\b\u0084\u0001\u00107R*\u0010D\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u000b8\u0006@\u0006¢\u0006\r\n\u0004\bD\u0010a\u001a\u0005\b\u0085\u0001\u0010\u000fR\u001a\u0010F\u001a\u00020\u00168\u0006@\u0006¢\u0006\r\n\u0004\bF\u0010e\u001a\u0005\b\u0086\u0001\u0010\u0018\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b¡\u001e0\u0001¨\u0006\u0089\u0001"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/models/member/GuildMember;", "component4", "()Ljava/util/Map;", "Lcom/discord/api/role/GuildRole;", "component5", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/voice/state/VoiceState;", "component6", "component7", "", "component8", "()Z", "component9", "component10", "", "component11", "()F", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "component12", "()Lcom/discord/widgets/user/presence/ModelRichPresence;", "Lcom/discord/models/guild/Guild;", "component13", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/permission/PermissionBit;", "component14", "()Ljava/lang/Long;", "Lcom/discord/utilities/streams/StreamContext;", "component15", "()Lcom/discord/utilities/streams/StreamContext;", "Lcom/discord/api/user/UserProfile;", "component16", "()Lcom/discord/api/user/UserProfile;", "", "Lcom/discord/primitives/RelationshipType;", "component17", "()Ljava/lang/Integer;", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", "component18", "()Lcom/discord/stores/StoreUserNotes$UserNoteState;", "component19", "Lcom/discord/widgets/stage/StageRoles;", "component20-twRsX-0", "()Lcom/discord/widgets/stage/StageRoles;", "component20", "Lcom/discord/api/voice/state/StageRequestToSpeakState;", "component21", "()Lcom/discord/api/voice/state/StageRequestToSpeakState;", "component22-twRsX-0", "component22", "component23", "user", "me", "channel", "computedMembers", "guildRoles", "mySelectedVoiceChannelVoiceStates", "currentChannelVoiceStates", "muted", "selfMuted", "selfDeafened", "outputVolume", "richPresence", "guild", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "streamContext", "userProfile", "userRelationshipType", "userNoteFetchState", "stageChannel", "userStageRoles", "userRequestToSpeakState", "myStageRoles", "canDisableCommunication", "copy-U9gTzXU", "(Lcom/discord/models/user/User;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZZZFLcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/models/guild/Guild;Ljava/lang/Long;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/user/UserProfile;Ljava/lang/Integer;Lcom/discord/stores/StoreUserNotes$UserNoteState;Lcom/discord/api/channel/Channel;Lcom/discord/widgets/stage/StageRoles;Lcom/discord/api/voice/state/StageRequestToSpeakState;Lcom/discord/widgets/stage/StageRoles;Z)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;", "copy", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getGuildRoles", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", "getUserNoteFetchState", "Z", "getSelfMuted", "getSelfDeafened", "Lcom/discord/models/user/MeUser;", "getMe", "Lcom/discord/api/voice/state/StageRequestToSpeakState;", "getUserRequestToSpeakState", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", "Lcom/discord/models/user/User;", "getUser", "getComputedMembers", "getCurrentChannelVoiceStates", "Lcom/discord/widgets/stage/StageRoles;", "getUserStageRoles-twRsX-0", "getCanDisableCommunication", "Lcom/discord/api/channel/Channel;", "getStageChannel", "Lcom/discord/models/guild/Guild;", "getGuild", "F", "getOutputVolume", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "getRichPresence", "Ljava/lang/Long;", "getPermissions", "getChannel", "Lcom/discord/api/user/UserProfile;", "getUserProfile", "Ljava/lang/Integer;", "getUserRelationshipType", "getMyStageRoles-twRsX-0", "getMySelectedVoiceChannelVoiceStates", "getMuted", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZZZFLcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/models/guild/Guild;Ljava/lang/Long;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/user/UserProfile;Ljava/lang/Integer;Lcom/discord/stores/StoreUserNotes$UserNoteState;Lcom/discord/api/channel/Channel;Lcom/discord/widgets/stage/StageRoles;Lcom/discord/api/voice/state/StageRequestToSpeakState;Lcom/discord/widgets/stage/StageRoles;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final boolean canDisableCommunication;
        private final Channel channel;
        private final Map<Long, GuildMember> computedMembers;
        private final Map<Long, VoiceState> currentChannelVoiceStates;
        private final Guild guild;
        private final Map<Long, GuildRole> guildRoles;

        /* renamed from: me */
        private final MeUser f2848me;
        private final boolean muted;
        private final Map<Long, VoiceState> mySelectedVoiceChannelVoiceStates;
        private final StageRoles myStageRoles;
        private final float outputVolume;
        private final Long permissions;
        private final ModelRichPresence richPresence;
        private final boolean selfDeafened;
        private final boolean selfMuted;
        private final Channel stageChannel;
        private final StreamContext streamContext;
        private final User user;
        private final StoreUserNotes.UserNoteState userNoteFetchState;
        private final UserProfile userProfile;
        private final Integer userRelationshipType;
        private final StageRequestToSpeakState userRequestToSpeakState;
        private final StageRoles userStageRoles;

        private StoreState(User user, MeUser meUser, Channel channel, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, VoiceState> map3, Map<Long, VoiceState> map4, boolean z2, boolean z3, boolean z4, float f, ModelRichPresence modelRichPresence, Guild guild, Long l, StreamContext streamContext, UserProfile userProfile, Integer num, StoreUserNotes.UserNoteState userNoteState, Channel channel2, StageRoles stageRoles, StageRequestToSpeakState stageRequestToSpeakState, StageRoles stageRoles2, boolean z5) {
            this.user = user;
            this.f2848me = meUser;
            this.channel = channel;
            this.computedMembers = map;
            this.guildRoles = map2;
            this.mySelectedVoiceChannelVoiceStates = map3;
            this.currentChannelVoiceStates = map4;
            this.muted = z2;
            this.selfMuted = z3;
            this.selfDeafened = z4;
            this.outputVolume = f;
            this.richPresence = modelRichPresence;
            this.guild = guild;
            this.permissions = l;
            this.streamContext = streamContext;
            this.userProfile = userProfile;
            this.userRelationshipType = num;
            this.userNoteFetchState = userNoteState;
            this.stageChannel = channel2;
            this.userStageRoles = stageRoles;
            this.userRequestToSpeakState = stageRequestToSpeakState;
            this.myStageRoles = stageRoles2;
            this.canDisableCommunication = z5;
        }

        public final User component1() {
            return this.user;
        }

        public final boolean component10() {
            return this.selfDeafened;
        }

        public final float component11() {
            return this.outputVolume;
        }

        public final ModelRichPresence component12() {
            return this.richPresence;
        }

        public final Guild component13() {
            return this.guild;
        }

        public final Long component14() {
            return this.permissions;
        }

        public final StreamContext component15() {
            return this.streamContext;
        }

        public final UserProfile component16() {
            return this.userProfile;
        }

        public final Integer component17() {
            return this.userRelationshipType;
        }

        public final StoreUserNotes.UserNoteState component18() {
            return this.userNoteFetchState;
        }

        public final Channel component19() {
            return this.stageChannel;
        }

        public final MeUser component2() {
            return this.f2848me;
        }

        /* renamed from: component20-twRsX-0 */
        public final StageRoles m50component20twRsX0() {
            return this.userStageRoles;
        }

        public final StageRequestToSpeakState component21() {
            return this.userRequestToSpeakState;
        }

        /* renamed from: component22-twRsX-0 */
        public final StageRoles m51component22twRsX0() {
            return this.myStageRoles;
        }

        public final boolean component23() {
            return this.canDisableCommunication;
        }

        public final Channel component3() {
            return this.channel;
        }

        public final Map<Long, GuildMember> component4() {
            return this.computedMembers;
        }

        public final Map<Long, GuildRole> component5() {
            return this.guildRoles;
        }

        public final Map<Long, VoiceState> component6() {
            return this.mySelectedVoiceChannelVoiceStates;
        }

        public final Map<Long, VoiceState> component7() {
            return this.currentChannelVoiceStates;
        }

        public final boolean component8() {
            return this.muted;
        }

        public final boolean component9() {
            return this.selfMuted;
        }

        /* renamed from: copy-U9gTzXU */
        public final StoreState m52copyU9gTzXU(User user, MeUser meUser, Channel channel, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, VoiceState> map3, Map<Long, VoiceState> map4, boolean z2, boolean z3, boolean z4, float f, ModelRichPresence modelRichPresence, Guild guild, Long l, StreamContext streamContext, UserProfile userProfile, Integer num, StoreUserNotes.UserNoteState userNoteState, Channel channel2, StageRoles stageRoles, StageRequestToSpeakState stageRequestToSpeakState, StageRoles stageRoles2, boolean z5) {
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(map, "computedMembers");
            m.checkNotNullParameter(map2, "guildRoles");
            m.checkNotNullParameter(map3, "mySelectedVoiceChannelVoiceStates");
            m.checkNotNullParameter(map4, "currentChannelVoiceStates");
            m.checkNotNullParameter(userProfile, "userProfile");
            m.checkNotNullParameter(userNoteState, "userNoteFetchState");
            m.checkNotNullParameter(stageRequestToSpeakState, "userRequestToSpeakState");
            return new StoreState(user, meUser, channel, map, map2, map3, map4, z2, z3, z4, f, modelRichPresence, guild, l, streamContext, userProfile, num, userNoteState, channel2, stageRoles, stageRequestToSpeakState, stageRoles2, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.user, storeState.user) && m.areEqual(this.f2848me, storeState.f2848me) && m.areEqual(this.channel, storeState.channel) && m.areEqual(this.computedMembers, storeState.computedMembers) && m.areEqual(this.guildRoles, storeState.guildRoles) && m.areEqual(this.mySelectedVoiceChannelVoiceStates, storeState.mySelectedVoiceChannelVoiceStates) && m.areEqual(this.currentChannelVoiceStates, storeState.currentChannelVoiceStates) && this.muted == storeState.muted && this.selfMuted == storeState.selfMuted && this.selfDeafened == storeState.selfDeafened && Float.compare(this.outputVolume, storeState.outputVolume) == 0 && m.areEqual(this.richPresence, storeState.richPresence) && m.areEqual(this.guild, storeState.guild) && m.areEqual(this.permissions, storeState.permissions) && m.areEqual(this.streamContext, storeState.streamContext) && m.areEqual(this.userProfile, storeState.userProfile) && m.areEqual(this.userRelationshipType, storeState.userRelationshipType) && m.areEqual(this.userNoteFetchState, storeState.userNoteFetchState) && m.areEqual(this.stageChannel, storeState.stageChannel) && m.areEqual(this.userStageRoles, storeState.userStageRoles) && m.areEqual(this.userRequestToSpeakState, storeState.userRequestToSpeakState) && m.areEqual(this.myStageRoles, storeState.myStageRoles) && this.canDisableCommunication == storeState.canDisableCommunication;
        }

        public final boolean getCanDisableCommunication() {
            return this.canDisableCommunication;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Map<Long, GuildMember> getComputedMembers() {
            return this.computedMembers;
        }

        public final Map<Long, VoiceState> getCurrentChannelVoiceStates() {
            return this.currentChannelVoiceStates;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Map<Long, GuildRole> getGuildRoles() {
            return this.guildRoles;
        }

        public final MeUser getMe() {
            return this.f2848me;
        }

        public final boolean getMuted() {
            return this.muted;
        }

        public final Map<Long, VoiceState> getMySelectedVoiceChannelVoiceStates() {
            return this.mySelectedVoiceChannelVoiceStates;
        }

        /* renamed from: getMyStageRoles-twRsX-0 */
        public final StageRoles m53getMyStageRolestwRsX0() {
            return this.myStageRoles;
        }

        public final float getOutputVolume() {
            return this.outputVolume;
        }

        public final Long getPermissions() {
            return this.permissions;
        }

        public final ModelRichPresence getRichPresence() {
            return this.richPresence;
        }

        public final boolean getSelfDeafened() {
            return this.selfDeafened;
        }

        public final boolean getSelfMuted() {
            return this.selfMuted;
        }

        public final Channel getStageChannel() {
            return this.stageChannel;
        }

        public final StreamContext getStreamContext() {
            return this.streamContext;
        }

        public final User getUser() {
            return this.user;
        }

        public final StoreUserNotes.UserNoteState getUserNoteFetchState() {
            return this.userNoteFetchState;
        }

        public final UserProfile getUserProfile() {
            return this.userProfile;
        }

        public final Integer getUserRelationshipType() {
            return this.userRelationshipType;
        }

        public final StageRequestToSpeakState getUserRequestToSpeakState() {
            return this.userRequestToSpeakState;
        }

        /* renamed from: getUserStageRoles-twRsX-0 */
        public final StageRoles m54getUserStageRolestwRsX0() {
            return this.userStageRoles;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            MeUser meUser = this.f2848me;
            int hashCode2 = (hashCode + (meUser != null ? meUser.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode3 = (hashCode2 + (channel != null ? channel.hashCode() : 0)) * 31;
            Map<Long, GuildMember> map = this.computedMembers;
            int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map2 = this.guildRoles;
            int hashCode5 = (hashCode4 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, VoiceState> map3 = this.mySelectedVoiceChannelVoiceStates;
            int hashCode6 = (hashCode5 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, VoiceState> map4 = this.currentChannelVoiceStates;
            int hashCode7 = (hashCode6 + (map4 != null ? map4.hashCode() : 0)) * 31;
            boolean z2 = this.muted;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode7 + i3) * 31;
            boolean z3 = this.selfMuted;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            boolean z4 = this.selfDeafened;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int floatToIntBits = (Float.floatToIntBits(this.outputVolume) + ((i8 + i9) * 31)) * 31;
            ModelRichPresence modelRichPresence = this.richPresence;
            int hashCode8 = (floatToIntBits + (modelRichPresence != null ? modelRichPresence.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            int hashCode9 = (hashCode8 + (guild != null ? guild.hashCode() : 0)) * 31;
            Long l = this.permissions;
            int hashCode10 = (hashCode9 + (l != null ? l.hashCode() : 0)) * 31;
            StreamContext streamContext = this.streamContext;
            int hashCode11 = (hashCode10 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
            UserProfile userProfile = this.userProfile;
            int hashCode12 = (hashCode11 + (userProfile != null ? userProfile.hashCode() : 0)) * 31;
            Integer num = this.userRelationshipType;
            int hashCode13 = (hashCode12 + (num != null ? num.hashCode() : 0)) * 31;
            StoreUserNotes.UserNoteState userNoteState = this.userNoteFetchState;
            int hashCode14 = (hashCode13 + (userNoteState != null ? userNoteState.hashCode() : 0)) * 31;
            Channel channel2 = this.stageChannel;
            int hashCode15 = (hashCode14 + (channel2 != null ? channel2.hashCode() : 0)) * 31;
            StageRoles stageRoles = this.userStageRoles;
            int hashCode16 = (hashCode15 + (stageRoles != null ? stageRoles.hashCode() : 0)) * 31;
            StageRequestToSpeakState stageRequestToSpeakState = this.userRequestToSpeakState;
            int hashCode17 = (hashCode16 + (stageRequestToSpeakState != null ? stageRequestToSpeakState.hashCode() : 0)) * 31;
            StageRoles stageRoles2 = this.myStageRoles;
            if (stageRoles2 != null) {
                i = stageRoles2.hashCode();
            }
            int i11 = (hashCode17 + i) * 31;
            boolean z5 = this.canDisableCommunication;
            if (!z5) {
                i2 = z5 ? 1 : 0;
            }
            return i11 + i2;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(user=");
            R.append(this.user);
            R.append(", me=");
            R.append(this.f2848me);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", computedMembers=");
            R.append(this.computedMembers);
            R.append(", guildRoles=");
            R.append(this.guildRoles);
            R.append(", mySelectedVoiceChannelVoiceStates=");
            R.append(this.mySelectedVoiceChannelVoiceStates);
            R.append(", currentChannelVoiceStates=");
            R.append(this.currentChannelVoiceStates);
            R.append(", muted=");
            R.append(this.muted);
            R.append(", selfMuted=");
            R.append(this.selfMuted);
            R.append(", selfDeafened=");
            R.append(this.selfDeafened);
            R.append(", outputVolume=");
            R.append(this.outputVolume);
            R.append(", richPresence=");
            R.append(this.richPresence);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", permissions=");
            R.append(this.permissions);
            R.append(", streamContext=");
            R.append(this.streamContext);
            R.append(", userProfile=");
            R.append(this.userProfile);
            R.append(", userRelationshipType=");
            R.append(this.userRelationshipType);
            R.append(", userNoteFetchState=");
            R.append(this.userNoteFetchState);
            R.append(", stageChannel=");
            R.append(this.stageChannel);
            R.append(", userStageRoles=");
            R.append(this.userStageRoles);
            R.append(", userRequestToSpeakState=");
            R.append(this.userRequestToSpeakState);
            R.append(", myStageRoles=");
            R.append(this.myStageRoles);
            R.append(", canDisableCommunication=");
            return a.M(R, this.canDisableCommunication, ")");
        }

        public /* synthetic */ StoreState(User user, MeUser meUser, Channel channel, Map map, Map map2, Map map3, Map map4, boolean z2, boolean z3, boolean z4, float f, ModelRichPresence modelRichPresence, Guild guild, Long l, StreamContext streamContext, UserProfile userProfile, Integer num, StoreUserNotes.UserNoteState userNoteState, Channel channel2, StageRoles stageRoles, StageRequestToSpeakState stageRequestToSpeakState, StageRoles stageRoles2, boolean z5, DefaultConstructorMarker defaultConstructorMarker) {
            this(user, meUser, channel, map, map2, map3, map4, z2, z3, z4, f, modelRichPresence, guild, l, streamContext, userProfile, num, userNoteState, channel2, stageRoles, stageRequestToSpeakState, stageRoles2, z5);
        }
    }

    /* compiled from: WidgetUserSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Uninitialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000À\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001e\n\u0002\u0010\u0000\n\u0002\b#\n\u0002\u0018\u0002\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001B\u0085\u0002\u0012\u0006\u0010D\u001a\u00020\u0005\u0012\u0006\u0010E\u001a\u00020\u0002\u0012\u0006\u0010F\u001a\u00020\u0002\u0012\u0006\u0010G\u001a\u00020\n\u0012\b\u0010H\u001a\u0004\u0018\u00010\r\u0012\b\u0010I\u001a\u0004\u0018\u00010\u0010\u0012\b\u0010J\u001a\u0004\u0018\u00010\u0013\u0012\f\u0010K\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016\u0012\b\u0010L\u001a\u0004\u0018\u00010\u001a\u0012\u0006\u0010M\u001a\u00020\u001d\u0012\b\u0010N\u001a\u0004\u0018\u00010 \u0012\b\u0010O\u001a\u0004\u0018\u00010#\u0012\b\u0010P\u001a\u0004\u0018\u00010\u0013\u0012\b\u0010Q\u001a\u0004\u0018\u00010\u0013\u0012\n\u0010R\u001a\u00060(j\u0002`)\u0012\u0006\u0010S\u001a\u00020,\u0012\u0006\u0010T\u001a\u00020/\u0012\b\u0010U\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010V\u001a\u00020\u0002\u0012\u001a\u0010W\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020605\u0018\u000104j\u0004\u0018\u0001`7\u0012\u0006\u0010X\u001a\u00020\u0002\u0012\u000e\u0010Y\u001a\n\u0018\u00010:j\u0004\u0018\u0001`;\u0012\b\u0010Z\u001a\u0004\u0018\u00010>\u0012\b\u0010[\u001a\u0004\u0018\u00010A¢\u0006\u0006\b\u0098\u0001\u0010\u0099\u0001J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0016\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ\u0012\u0010!\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b!\u0010\"J\u0012\u0010$\u001a\u0004\u0018\u00010#HÆ\u0003¢\u0006\u0004\b$\u0010%J\u0012\u0010&\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b&\u0010\u0015J\u0012\u0010'\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b'\u0010\u0015J\u0014\u0010*\u001a\u00060(j\u0002`)HÆ\u0003¢\u0006\u0004\b*\u0010+J\u0010\u0010-\u001a\u00020,HÆ\u0003¢\u0006\u0004\b-\u0010.J\u0010\u00100\u001a\u00020/HÆ\u0003¢\u0006\u0004\b0\u00101J\u0012\u00102\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b2\u0010\u0015J\u0010\u00103\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b3\u0010\u0004J$\u00108\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020605\u0018\u000104j\u0004\u0018\u0001`7HÆ\u0003¢\u0006\u0004\b8\u0010\u0019J\u0010\u00109\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b9\u0010\u0004J\u0018\u0010<\u001a\n\u0018\u00010:j\u0004\u0018\u0001`;HÆ\u0003¢\u0006\u0004\b<\u0010=J\u0012\u0010?\u001a\u0004\u0018\u00010>HÆ\u0003¢\u0006\u0004\b?\u0010@J\u0012\u0010B\u001a\u0004\u0018\u00010AHÆ\u0003¢\u0006\u0004\bB\u0010CJ¼\u0002\u0010\\\u001a\u00020\u00002\b\b\u0002\u0010D\u001a\u00020\u00052\b\b\u0002\u0010E\u001a\u00020\u00022\b\b\u0002\u0010F\u001a\u00020\u00022\b\b\u0002\u0010G\u001a\u00020\n2\n\b\u0002\u0010H\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010I\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010J\u001a\u0004\u0018\u00010\u00132\u000e\b\u0002\u0010K\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\n\b\u0002\u0010L\u001a\u0004\u0018\u00010\u001a2\b\b\u0002\u0010M\u001a\u00020\u001d2\n\b\u0002\u0010N\u001a\u0004\u0018\u00010 2\n\b\u0002\u0010O\u001a\u0004\u0018\u00010#2\n\b\u0002\u0010P\u001a\u0004\u0018\u00010\u00132\n\b\u0002\u0010Q\u001a\u0004\u0018\u00010\u00132\f\b\u0002\u0010R\u001a\u00060(j\u0002`)2\b\b\u0002\u0010S\u001a\u00020,2\b\b\u0002\u0010T\u001a\u00020/2\n\b\u0002\u0010U\u001a\u0004\u0018\u00010\u00132\b\b\u0002\u0010V\u001a\u00020\u00022\u001c\b\u0002\u0010W\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020605\u0018\u000104j\u0004\u0018\u0001`72\b\b\u0002\u0010X\u001a\u00020\u00022\u0010\b\u0002\u0010Y\u001a\n\u0018\u00010:j\u0004\u0018\u0001`;2\n\b\u0002\u0010Z\u001a\u0004\u0018\u00010>2\n\b\u0002\u0010[\u001a\u0004\u0018\u00010AHÆ\u0001¢\u0006\u0004\b\\\u0010]J\u0010\u0010^\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b^\u0010\u0015J\u0010\u0010_\u001a\u00020(HÖ\u0001¢\u0006\u0004\b_\u0010+J\u001a\u0010b\u001a\u00020\u00022\b\u0010a\u001a\u0004\u0018\u00010`HÖ\u0003¢\u0006\u0004\bb\u0010cR\u0019\u0010T\u001a\u00020/8\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010d\u001a\u0004\be\u00101R\u0019\u0010S\u001a\u00020,8\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010f\u001a\u0004\bg\u0010.R\u001d\u0010R\u001a\u00060(j\u0002`)8\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010h\u001a\u0004\bi\u0010+R\u0019\u0010V\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bV\u0010j\u001a\u0004\bk\u0010\u0004R\u0019\u0010F\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010j\u001a\u0004\bl\u0010\u0004R\u0019\u0010m\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bm\u0010j\u001a\u0004\bn\u0010\u0004R\u001b\u0010P\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010o\u001a\u0004\bp\u0010\u0015R\u001b\u0010U\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010o\u001a\u0004\bq\u0010\u0015R\u0019\u0010X\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010j\u001a\u0004\br\u0010\u0004R\u001b\u0010I\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010s\u001a\u0004\bt\u0010\u0012R\u001b\u0010u\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bu\u0010o\u001a\u0004\bv\u0010\u0015R\u0019\u0010G\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010w\u001a\u0004\bx\u0010\fR\u001b\u0010L\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010y\u001a\u0004\bz\u0010\u001cR\u001f\u0010K\u001a\b\u0012\u0004\u0012\u00020\u00170\u00168\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010{\u001a\u0004\b|\u0010\u0019R\u001b\u0010Z\u001a\u0004\u0018\u00010>8\u0006@\u0006¢\u0006\f\n\u0004\bZ\u0010}\u001a\u0004\b~\u0010@R\u001c\u0010O\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\r\n\u0004\bO\u0010\u007f\u001a\u0005\b\u0080\u0001\u0010%R\u001c\u0010J\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\r\n\u0004\bJ\u0010o\u001a\u0005\b\u0081\u0001\u0010\u0015R\u001b\u0010D\u001a\u00020\u00058\u0006@\u0006¢\u0006\u000e\n\u0005\bD\u0010\u0082\u0001\u001a\u0005\b\u0083\u0001\u0010\u0007R\u0019\u0010\u0087\u0001\u001a\u0005\u0018\u00010\u0084\u00018F@\u0006¢\u0006\b\u001a\u0006\b\u0085\u0001\u0010\u0086\u0001R\u001d\u0010H\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\u000e\n\u0005\bH\u0010\u0088\u0001\u001a\u0005\b\u0089\u0001\u0010\u000fR\u001c\u0010\u008a\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008a\u0001\u0010j\u001a\u0005\b\u008b\u0001\u0010\u0004R\u0019\u0010E\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010j\u001a\u0004\bE\u0010\u0004R#\u0010Y\u001a\n\u0018\u00010:j\u0004\u0018\u0001`;8\u0006@\u0006¢\u0006\u000e\n\u0005\bY\u0010\u008c\u0001\u001a\u0005\b\u008d\u0001\u0010=R\u001d\u0010[\u001a\u0004\u0018\u00010A8\u0006@\u0006¢\u0006\u000e\n\u0005\b[\u0010\u008e\u0001\u001a\u0005\b\u008f\u0001\u0010CR\u001c\u0010Q\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\r\n\u0004\bQ\u0010o\u001a\u0005\b\u0090\u0001\u0010\u0015R%\u0010\u0091\u0001\u001a\n\u0018\u00010:j\u0004\u0018\u0001`;8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0091\u0001\u0010\u008c\u0001\u001a\u0005\b\u0092\u0001\u0010=R\u001b\u0010M\u001a\u00020\u001d8\u0006@\u0006¢\u0006\u000e\n\u0005\bM\u0010\u0093\u0001\u001a\u0005\b\u0094\u0001\u0010\u001fR\u001d\u0010N\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\u000e\n\u0005\bN\u0010\u0095\u0001\u001a\u0005\b\u0096\u0001\u0010\"R.\u0010W\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020605\u0018\u000104j\u0004\u0018\u0001`78\u0006@\u0006¢\u0006\r\n\u0004\bW\u0010{\u001a\u0005\b\u0097\u0001\u0010\u0019¨\u0006\u009a\u0001"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;", "", "shouldShowRoles", "()Z", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "component2", "component3", "Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;", "component4", "()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;", "Lcom/discord/api/voice/state/VoiceState;", "component5", "()Lcom/discord/api/voice/state/VoiceState;", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "component6", "()Lcom/discord/widgets/user/presence/ModelRichPresence;", "", "component7", "()Ljava/lang/String;", "", "Lcom/discord/api/role/GuildRole;", "component8", "()Ljava/util/List;", "Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "component9", "()Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "Lcom/discord/widgets/stage/usersheet/UserProfileStageActionsView$ViewState;", "component10", "()Lcom/discord/widgets/stage/usersheet/UserProfileStageActionsView$ViewState;", "Lcom/discord/api/channel/Channel;", "component11", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/utilities/streams/StreamContext;", "component12", "()Lcom/discord/utilities/streams/StreamContext;", "component13", "component14", "", "Lcom/discord/primitives/RelationshipType;", "component15", "()I", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "component16", "()Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", "component17", "()Lcom/discord/stores/StoreUserNotes$UserNoteState;", "component18", "component19", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/user/usersheet/AST;", "component20", "component21", "", "Lcom/discord/primitives/GuildId;", "component22", "()Ljava/lang/Long;", "Lcom/discord/models/member/GuildMember;", "component23", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/api/user/UserProfile;", "component24", "()Lcom/discord/api/user/UserProfile;", "user", "isMe", "showVoiceSettings", "voiceSettingsViewState", "channelVoiceState", "richPresence", "guildSectionHeaderText", "roleItems", "adminViewState", "stageViewState", "channel", "streamContext", "guildName", "guildIcon", "userRelationshipType", "connectionsViewState", "userNoteFetchState", "userNote", "userInSameVoiceChannel", "bioAst", "profileLoaded", "guildId", "guildMember", "userProfile", "copy", "(Lcom/discord/models/user/User;ZZLcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;Lcom/discord/api/voice/state/VoiceState;Lcom/discord/widgets/user/presence/ModelRichPresence;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;Lcom/discord/widgets/stage/usersheet/UserProfileStageActionsView$ViewState;Lcom/discord/api/channel/Channel;Lcom/discord/utilities/streams/StreamContext;Ljava/lang/String;Ljava/lang/String;ILcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;Lcom/discord/stores/StoreUserNotes$UserNoteState;Ljava/lang/String;ZLjava/util/List;ZLjava/lang/Long;Lcom/discord/models/member/GuildMember;Lcom/discord/api/user/UserProfile;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", "getUserNoteFetchState", "Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;", "getConnectionsViewState", "I", "getUserRelationshipType", "Z", "getUserInSameVoiceChannel", "getShowVoiceSettings", "hasPremiumCustomization", "getHasPremiumCustomization", "Ljava/lang/String;", "getGuildName", "getUserNote", "getProfileLoaded", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "getRichPresence", "guildIconURL", "getGuildIconURL", "Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;", "getVoiceSettingsViewState", "Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;", "getAdminViewState", "Ljava/util/List;", "getRoleItems", "Lcom/discord/models/member/GuildMember;", "getGuildMember", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", "getGuildSectionHeaderText", "Lcom/discord/models/user/User;", "getUser", "Lcom/discord/models/presence/Presence;", "getPresence", "()Lcom/discord/models/presence/Presence;", "presence", "Lcom/discord/api/voice/state/VoiceState;", "getChannelVoiceState", "hasGuildMemberBio", "getHasGuildMemberBio", "Ljava/lang/Long;", "getGuildId", "Lcom/discord/api/user/UserProfile;", "getUserProfile", "getGuildIcon", "currentGuildId", "getCurrentGuildId", "Lcom/discord/widgets/stage/usersheet/UserProfileStageActionsView$ViewState;", "getStageViewState", "Lcom/discord/api/channel/Channel;", "getChannel", "getBioAst", HookHelper.constructorName, "(Lcom/discord/models/user/User;ZZLcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;Lcom/discord/api/voice/state/VoiceState;Lcom/discord/widgets/user/presence/ModelRichPresence;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;Lcom/discord/widgets/stage/usersheet/UserProfileStageActionsView$ViewState;Lcom/discord/api/channel/Channel;Lcom/discord/utilities/streams/StreamContext;Ljava/lang/String;Ljava/lang/String;ILcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;Lcom/discord/stores/StoreUserNotes$UserNoteState;Ljava/lang/String;ZLjava/util/List;ZLjava/lang/Long;Lcom/discord/models/member/GuildMember;Lcom/discord/api/user/UserProfile;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final UserProfileAdminView.ViewState adminViewState;
            private final List<Node<MessageRenderContext>> bioAst;
            private final Channel channel;
            private final VoiceState channelVoiceState;
            private final UserProfileConnectionsView.ViewState connectionsViewState;
            private final Long currentGuildId;
            private final String guildIcon;
            private final String guildIconURL;
            private final Long guildId;
            private final GuildMember guildMember;
            private final String guildName;
            private final String guildSectionHeaderText;
            private final boolean hasGuildMemberBio;
            private final boolean hasPremiumCustomization;
            private final boolean isMe;
            private final boolean profileLoaded;
            private final ModelRichPresence richPresence;
            private final List<GuildRole> roleItems;
            private final boolean showVoiceSettings;
            private final UserProfileStageActionsView.ViewState stageViewState;
            private final StreamContext streamContext;
            private final User user;
            private final boolean userInSameVoiceChannel;
            private final String userNote;
            private final StoreUserNotes.UserNoteState userNoteFetchState;
            private final UserProfile userProfile;
            private final int userRelationshipType;
            private final UserProfileVoiceSettingsView.ViewState voiceSettingsViewState;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(User user, boolean z2, boolean z3, UserProfileVoiceSettingsView.ViewState viewState, VoiceState voiceState, ModelRichPresence modelRichPresence, String str, List<GuildRole> list, UserProfileAdminView.ViewState viewState2, UserProfileStageActionsView.ViewState viewState3, Channel channel, StreamContext streamContext, String str2, String str3, int i, UserProfileConnectionsView.ViewState viewState4, StoreUserNotes.UserNoteState userNoteState, String str4, boolean z4, List<Node<MessageRenderContext>> list2, boolean z5, Long l, GuildMember guildMember, UserProfile userProfile) {
                super(null);
                String avatar;
                m.checkNotNullParameter(user, "user");
                m.checkNotNullParameter(viewState, "voiceSettingsViewState");
                m.checkNotNullParameter(list, "roleItems");
                m.checkNotNullParameter(viewState3, "stageViewState");
                m.checkNotNullParameter(viewState4, "connectionsViewState");
                m.checkNotNullParameter(userNoteState, "userNoteFetchState");
                String str5 = null;
                this.user = user;
                this.isMe = z2;
                this.showVoiceSettings = z3;
                this.voiceSettingsViewState = viewState;
                this.channelVoiceState = voiceState;
                this.richPresence = modelRichPresence;
                this.guildSectionHeaderText = str;
                this.roleItems = list;
                this.adminViewState = viewState2;
                this.stageViewState = viewState3;
                this.channel = channel;
                this.streamContext = streamContext;
                this.guildName = str2;
                this.guildIcon = str3;
                this.userRelationshipType = i;
                this.connectionsViewState = viewState4;
                this.userNoteFetchState = userNoteState;
                this.userNote = str4;
                this.userInSameVoiceChannel = z4;
                this.bioAst = list2;
                this.profileLoaded = z5;
                this.guildId = l;
                this.guildMember = guildMember;
                this.userProfile = userProfile;
                boolean z6 = false;
                this.hasPremiumCustomization = user.getBanner() != null || ((avatar = user.getAvatar()) != null && IconUtils.INSTANCE.isImageHashAnimated(avatar));
                this.currentGuildId = channel != null ? Long.valueOf(channel.f()) : l;
                String bio = guildMember != null ? guildMember.getBio() : null;
                this.hasGuildMemberBio = !((bio == null || bio.length() == 0) ? true : z6);
                this.guildIconURL = str3 != null ? IconUtils.getForGuild$default(l, str3, null, false, Integer.valueOf(DimenUtils.dpToPixels(16)), 4, null) : str5;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, User user, boolean z2, boolean z3, UserProfileVoiceSettingsView.ViewState viewState, VoiceState voiceState, ModelRichPresence modelRichPresence, String str, List list, UserProfileAdminView.ViewState viewState2, UserProfileStageActionsView.ViewState viewState3, Channel channel, StreamContext streamContext, String str2, String str3, int i, UserProfileConnectionsView.ViewState viewState4, StoreUserNotes.UserNoteState userNoteState, String str4, boolean z4, List list2, boolean z5, Long l, GuildMember guildMember, UserProfile userProfile, int i2, Object obj) {
                return loaded.copy((i2 & 1) != 0 ? loaded.user : user, (i2 & 2) != 0 ? loaded.isMe : z2, (i2 & 4) != 0 ? loaded.showVoiceSettings : z3, (i2 & 8) != 0 ? loaded.voiceSettingsViewState : viewState, (i2 & 16) != 0 ? loaded.channelVoiceState : voiceState, (i2 & 32) != 0 ? loaded.richPresence : modelRichPresence, (i2 & 64) != 0 ? loaded.guildSectionHeaderText : str, (i2 & 128) != 0 ? loaded.roleItems : list, (i2 & 256) != 0 ? loaded.adminViewState : viewState2, (i2 & 512) != 0 ? loaded.stageViewState : viewState3, (i2 & 1024) != 0 ? loaded.channel : channel, (i2 & 2048) != 0 ? loaded.streamContext : streamContext, (i2 & 4096) != 0 ? loaded.guildName : str2, (i2 & 8192) != 0 ? loaded.guildIcon : str3, (i2 & 16384) != 0 ? loaded.userRelationshipType : i, (i2 & 32768) != 0 ? loaded.connectionsViewState : viewState4, (i2 & 65536) != 0 ? loaded.userNoteFetchState : userNoteState, (i2 & 131072) != 0 ? loaded.userNote : str4, (i2 & 262144) != 0 ? loaded.userInSameVoiceChannel : z4, (i2 & 524288) != 0 ? loaded.bioAst : list2, (i2 & 1048576) != 0 ? loaded.profileLoaded : z5, (i2 & 2097152) != 0 ? loaded.guildId : l, (i2 & 4194304) != 0 ? loaded.guildMember : guildMember, (i2 & 8388608) != 0 ? loaded.userProfile : userProfile);
            }

            public final User component1() {
                return this.user;
            }

            public final UserProfileStageActionsView.ViewState component10() {
                return this.stageViewState;
            }

            public final Channel component11() {
                return this.channel;
            }

            public final StreamContext component12() {
                return this.streamContext;
            }

            public final String component13() {
                return this.guildName;
            }

            public final String component14() {
                return this.guildIcon;
            }

            public final int component15() {
                return this.userRelationshipType;
            }

            public final UserProfileConnectionsView.ViewState component16() {
                return this.connectionsViewState;
            }

            public final StoreUserNotes.UserNoteState component17() {
                return this.userNoteFetchState;
            }

            public final String component18() {
                return this.userNote;
            }

            public final boolean component19() {
                return this.userInSameVoiceChannel;
            }

            public final boolean component2() {
                return this.isMe;
            }

            public final List<Node<MessageRenderContext>> component20() {
                return this.bioAst;
            }

            public final boolean component21() {
                return this.profileLoaded;
            }

            public final Long component22() {
                return this.guildId;
            }

            public final GuildMember component23() {
                return this.guildMember;
            }

            public final UserProfile component24() {
                return this.userProfile;
            }

            public final boolean component3() {
                return this.showVoiceSettings;
            }

            public final UserProfileVoiceSettingsView.ViewState component4() {
                return this.voiceSettingsViewState;
            }

            public final VoiceState component5() {
                return this.channelVoiceState;
            }

            public final ModelRichPresence component6() {
                return this.richPresence;
            }

            public final String component7() {
                return this.guildSectionHeaderText;
            }

            public final List<GuildRole> component8() {
                return this.roleItems;
            }

            public final UserProfileAdminView.ViewState component9() {
                return this.adminViewState;
            }

            public final Loaded copy(User user, boolean z2, boolean z3, UserProfileVoiceSettingsView.ViewState viewState, VoiceState voiceState, ModelRichPresence modelRichPresence, String str, List<GuildRole> list, UserProfileAdminView.ViewState viewState2, UserProfileStageActionsView.ViewState viewState3, Channel channel, StreamContext streamContext, String str2, String str3, int i, UserProfileConnectionsView.ViewState viewState4, StoreUserNotes.UserNoteState userNoteState, String str4, boolean z4, List<Node<MessageRenderContext>> list2, boolean z5, Long l, GuildMember guildMember, UserProfile userProfile) {
                m.checkNotNullParameter(user, "user");
                m.checkNotNullParameter(viewState, "voiceSettingsViewState");
                m.checkNotNullParameter(list, "roleItems");
                m.checkNotNullParameter(viewState3, "stageViewState");
                m.checkNotNullParameter(viewState4, "connectionsViewState");
                m.checkNotNullParameter(userNoteState, "userNoteFetchState");
                return new Loaded(user, z2, z3, viewState, voiceState, modelRichPresence, str, list, viewState2, viewState3, channel, streamContext, str2, str3, i, viewState4, userNoteState, str4, z4, list2, z5, l, guildMember, userProfile);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.user, loaded.user) && this.isMe == loaded.isMe && this.showVoiceSettings == loaded.showVoiceSettings && m.areEqual(this.voiceSettingsViewState, loaded.voiceSettingsViewState) && m.areEqual(this.channelVoiceState, loaded.channelVoiceState) && m.areEqual(this.richPresence, loaded.richPresence) && m.areEqual(this.guildSectionHeaderText, loaded.guildSectionHeaderText) && m.areEqual(this.roleItems, loaded.roleItems) && m.areEqual(this.adminViewState, loaded.adminViewState) && m.areEqual(this.stageViewState, loaded.stageViewState) && m.areEqual(this.channel, loaded.channel) && m.areEqual(this.streamContext, loaded.streamContext) && m.areEqual(this.guildName, loaded.guildName) && m.areEqual(this.guildIcon, loaded.guildIcon) && this.userRelationshipType == loaded.userRelationshipType && m.areEqual(this.connectionsViewState, loaded.connectionsViewState) && m.areEqual(this.userNoteFetchState, loaded.userNoteFetchState) && m.areEqual(this.userNote, loaded.userNote) && this.userInSameVoiceChannel == loaded.userInSameVoiceChannel && m.areEqual(this.bioAst, loaded.bioAst) && this.profileLoaded == loaded.profileLoaded && m.areEqual(this.guildId, loaded.guildId) && m.areEqual(this.guildMember, loaded.guildMember) && m.areEqual(this.userProfile, loaded.userProfile);
            }

            public final UserProfileAdminView.ViewState getAdminViewState() {
                return this.adminViewState;
            }

            public final List<Node<MessageRenderContext>> getBioAst() {
                return this.bioAst;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final VoiceState getChannelVoiceState() {
                return this.channelVoiceState;
            }

            public final UserProfileConnectionsView.ViewState getConnectionsViewState() {
                return this.connectionsViewState;
            }

            public final Long getCurrentGuildId() {
                return this.currentGuildId;
            }

            public final String getGuildIcon() {
                return this.guildIcon;
            }

            public final String getGuildIconURL() {
                return this.guildIconURL;
            }

            public final Long getGuildId() {
                return this.guildId;
            }

            public final GuildMember getGuildMember() {
                return this.guildMember;
            }

            public final String getGuildName() {
                return this.guildName;
            }

            public final String getGuildSectionHeaderText() {
                return this.guildSectionHeaderText;
            }

            public final boolean getHasGuildMemberBio() {
                return this.hasGuildMemberBio;
            }

            public final boolean getHasPremiumCustomization() {
                return this.hasPremiumCustomization;
            }

            public final Presence getPresence() {
                ModelRichPresence modelRichPresence = this.richPresence;
                if (modelRichPresence != null) {
                    return modelRichPresence.getPresence();
                }
                return null;
            }

            public final boolean getProfileLoaded() {
                return this.profileLoaded;
            }

            public final ModelRichPresence getRichPresence() {
                return this.richPresence;
            }

            public final List<GuildRole> getRoleItems() {
                return this.roleItems;
            }

            public final boolean getShowVoiceSettings() {
                return this.showVoiceSettings;
            }

            public final UserProfileStageActionsView.ViewState getStageViewState() {
                return this.stageViewState;
            }

            public final StreamContext getStreamContext() {
                return this.streamContext;
            }

            public final User getUser() {
                return this.user;
            }

            public final boolean getUserInSameVoiceChannel() {
                return this.userInSameVoiceChannel;
            }

            public final String getUserNote() {
                return this.userNote;
            }

            public final StoreUserNotes.UserNoteState getUserNoteFetchState() {
                return this.userNoteFetchState;
            }

            public final UserProfile getUserProfile() {
                return this.userProfile;
            }

            public final int getUserRelationshipType() {
                return this.userRelationshipType;
            }

            public final UserProfileVoiceSettingsView.ViewState getVoiceSettingsViewState() {
                return this.voiceSettingsViewState;
            }

            public int hashCode() {
                User user = this.user;
                int i = 0;
                int hashCode = (user != null ? user.hashCode() : 0) * 31;
                boolean z2 = this.isMe;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode + i3) * 31;
                boolean z3 = this.showVoiceSettings;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (i5 + i6) * 31;
                UserProfileVoiceSettingsView.ViewState viewState = this.voiceSettingsViewState;
                int hashCode2 = (i8 + (viewState != null ? viewState.hashCode() : 0)) * 31;
                VoiceState voiceState = this.channelVoiceState;
                int hashCode3 = (hashCode2 + (voiceState != null ? voiceState.hashCode() : 0)) * 31;
                ModelRichPresence modelRichPresence = this.richPresence;
                int hashCode4 = (hashCode3 + (modelRichPresence != null ? modelRichPresence.hashCode() : 0)) * 31;
                String str = this.guildSectionHeaderText;
                int hashCode5 = (hashCode4 + (str != null ? str.hashCode() : 0)) * 31;
                List<GuildRole> list = this.roleItems;
                int hashCode6 = (hashCode5 + (list != null ? list.hashCode() : 0)) * 31;
                UserProfileAdminView.ViewState viewState2 = this.adminViewState;
                int hashCode7 = (hashCode6 + (viewState2 != null ? viewState2.hashCode() : 0)) * 31;
                UserProfileStageActionsView.ViewState viewState3 = this.stageViewState;
                int hashCode8 = (hashCode7 + (viewState3 != null ? viewState3.hashCode() : 0)) * 31;
                Channel channel = this.channel;
                int hashCode9 = (hashCode8 + (channel != null ? channel.hashCode() : 0)) * 31;
                StreamContext streamContext = this.streamContext;
                int hashCode10 = (hashCode9 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
                String str2 = this.guildName;
                int hashCode11 = (hashCode10 + (str2 != null ? str2.hashCode() : 0)) * 31;
                String str3 = this.guildIcon;
                int hashCode12 = (((hashCode11 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.userRelationshipType) * 31;
                UserProfileConnectionsView.ViewState viewState4 = this.connectionsViewState;
                int hashCode13 = (hashCode12 + (viewState4 != null ? viewState4.hashCode() : 0)) * 31;
                StoreUserNotes.UserNoteState userNoteState = this.userNoteFetchState;
                int hashCode14 = (hashCode13 + (userNoteState != null ? userNoteState.hashCode() : 0)) * 31;
                String str4 = this.userNote;
                int hashCode15 = (hashCode14 + (str4 != null ? str4.hashCode() : 0)) * 31;
                boolean z4 = this.userInSameVoiceChannel;
                if (z4) {
                    z4 = true;
                }
                int i9 = z4 ? 1 : 0;
                int i10 = z4 ? 1 : 0;
                int i11 = (hashCode15 + i9) * 31;
                List<Node<MessageRenderContext>> list2 = this.bioAst;
                int hashCode16 = (i11 + (list2 != null ? list2.hashCode() : 0)) * 31;
                boolean z5 = this.profileLoaded;
                if (!z5) {
                    i2 = z5 ? 1 : 0;
                }
                int i12 = (hashCode16 + i2) * 31;
                Long l = this.guildId;
                int hashCode17 = (i12 + (l != null ? l.hashCode() : 0)) * 31;
                GuildMember guildMember = this.guildMember;
                int hashCode18 = (hashCode17 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
                UserProfile userProfile = this.userProfile;
                if (userProfile != null) {
                    i = userProfile.hashCode();
                }
                return hashCode18 + i;
            }

            public final boolean isMe() {
                return this.isMe;
            }

            public final boolean shouldShowRoles() {
                Long l = this.currentGuildId;
                return (this.roleItems.isEmpty() ^ true) && (l != null && (l.longValue() > 0L ? 1 : (l.longValue() == 0L ? 0 : -1)) > 0);
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(user=");
                R.append(this.user);
                R.append(", isMe=");
                R.append(this.isMe);
                R.append(", showVoiceSettings=");
                R.append(this.showVoiceSettings);
                R.append(", voiceSettingsViewState=");
                R.append(this.voiceSettingsViewState);
                R.append(", channelVoiceState=");
                R.append(this.channelVoiceState);
                R.append(", richPresence=");
                R.append(this.richPresence);
                R.append(", guildSectionHeaderText=");
                R.append(this.guildSectionHeaderText);
                R.append(", roleItems=");
                R.append(this.roleItems);
                R.append(", adminViewState=");
                R.append(this.adminViewState);
                R.append(", stageViewState=");
                R.append(this.stageViewState);
                R.append(", channel=");
                R.append(this.channel);
                R.append(", streamContext=");
                R.append(this.streamContext);
                R.append(", guildName=");
                R.append(this.guildName);
                R.append(", guildIcon=");
                R.append(this.guildIcon);
                R.append(", userRelationshipType=");
                R.append(this.userRelationshipType);
                R.append(", connectionsViewState=");
                R.append(this.connectionsViewState);
                R.append(", userNoteFetchState=");
                R.append(this.userNoteFetchState);
                R.append(", userNote=");
                R.append(this.userNote);
                R.append(", userInSameVoiceChannel=");
                R.append(this.userInSameVoiceChannel);
                R.append(", bioAst=");
                R.append(this.bioAst);
                R.append(", profileLoaded=");
                R.append(this.profileLoaded);
                R.append(", guildId=");
                R.append(this.guildId);
                R.append(", guildMember=");
                R.append(this.guildMember);
                R.append(", userProfile=");
                R.append(this.userProfile);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            StoreMediaSettings.SelfMuteFailure.values();
            int[] iArr = new int[1];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreMediaSettings.SelfMuteFailure.CANNOT_USE_VAD.ordinal()] = 1;
            WidgetUserSheet.StreamPreviewClickBehavior.values();
            int[] iArr2 = new int[2];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[WidgetUserSheet.StreamPreviewClickBehavior.TARGET_AND_LAUNCH_SPECTATE.ordinal()] = 1;
            iArr2[WidgetUserSheet.StreamPreviewClickBehavior.TARGET_AND_DISMISS.ordinal()] = 2;
        }
    }

    public /* synthetic */ WidgetUserSheetViewModel(long j, long j2, Long l, String str, boolean z2, Observable observable, WidgetUserSheet.StreamPreviewClickBehavior streamPreviewClickBehavior, StoreMediaSettings storeMediaSettings, StoreApplicationStreaming storeApplicationStreaming, StoreUserNotes storeUserNotes, StoreUserProfile storeUserProfile, RestAPI restAPI, RestAPI restAPI2, StoreApplicationStreamPreviews storeApplicationStreamPreviews, Parser parser, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, l, str, z2, (i & 32) != 0 ? WidgetUserSheetViewModelStoreState.observeStoreState$default(WidgetUserSheetViewModelStoreState.INSTANCE, j, Long.valueOf(j2), l, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 131064, null) : observable, streamPreviewClickBehavior, (i & 128) != 0 ? StoreStream.Companion.getMediaSettings() : storeMediaSettings, (i & 256) != 0 ? StoreStream.Companion.getApplicationStreaming() : storeApplicationStreaming, (i & 512) != 0 ? StoreStream.Companion.getUsersNotes() : storeUserNotes, (i & 1024) != 0 ? StoreStream.Companion.getUserProfile() : storeUserProfile, (i & 2048) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 4096) != 0 ? RestAPI.Companion.getApiSerializeNulls() : restAPI2, (i & 8192) != 0 ? StoreStream.Companion.getApplicationStreamPreviews() : storeApplicationStreamPreviews, (i & 16384) != 0 ? DiscordParser.createParser$default(false, false, false, 4, null) : parser);
    }

    public static /* synthetic */ void addRelationship$default(WidgetUserSheetViewModel widgetUserSheetViewModel, Integer num, String str, int i, String str2, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            str2 = null;
        }
        widgetUserSheetViewModel.addRelationship(num, str, i, str2);
    }

    private final UserProfileAdminView.ViewState createAdminViewState(Channel channel, boolean z2, boolean z3, ManageUserContext manageUserContext, VoiceState voiceState, boolean z4, boolean z5) {
        boolean z6;
        if (channel == null) {
            return null;
        }
        boolean z7 = manageUserContext != null && (manageUserContext.getCanChangeNickname() || manageUserContext.getCanManageRoles());
        boolean z8 = !ChannelUtils.w(channel) ? !(manageUserContext == null || !manageUserContext.getCanKick()) : !(ChannelUtils.v(channel) || !z2 || z3);
        boolean z9 = z4 && manageUserContext != null && manageUserContext.getCanDisableCommunication() && !z3;
        boolean w = ChannelUtils.w(channel);
        boolean z10 = manageUserContext != null && manageUserContext.getCanBan();
        boolean z11 = voiceState != null && manageUserContext != null && manageUserContext.getCanMute() && !ChannelUtils.z(channel);
        boolean z12 = voiceState != null && voiceState.e();
        boolean z13 = (voiceState == null || manageUserContext == null || !manageUserContext.getCanDeafen()) ? false : true;
        boolean z14 = voiceState != null && voiceState.b();
        boolean z15 = (voiceState == null || manageUserContext == null || !manageUserContext.getCanMove()) ? false : true;
        Boolean[] boolArr = {Boolean.valueOf(z7), Boolean.valueOf(z8), Boolean.valueOf(z10), Boolean.valueOf(z9), Boolean.valueOf(z11), Boolean.valueOf(z13), Boolean.valueOf(z15)};
        int i = 0;
        while (true) {
            if (i >= 7) {
                z6 = false;
                break;
            } else if (boolArr[i].booleanValue()) {
                z6 = true;
                break;
            } else {
                i++;
            }
        }
        return new UserProfileAdminView.ViewState(z7, z8, z9, w, z10, z11, z12, z13, z14, z15, z6, z3, z5);
    }

    private final List<Node<MessageRenderContext>> createAndProcessBioAstFromText(String str) {
        if (str == null || t.isBlank(str)) {
            return null;
        }
        List<Node<MessageRenderContext>> generateAst = generateAst(str);
        createMessagePreprocessor().process(generateAst);
        return generateAst;
    }

    private final UserProfileConnectionsView.ViewState createConnectionsViewState(UserProfile userProfile, boolean z2, boolean z3) {
        List<ConnectedAccount> b2 = userProfile.b();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(b2, 10));
        for (ConnectedAccount connectedAccount : b2) {
            arrayList.add(new UserProfileConnectionsView.ConnectedAccountItem(connectedAccount));
        }
        boolean z4 = false;
        boolean z5 = !z2 && !z3;
        if (z5 || (!arrayList.isEmpty())) {
            z4 = true;
        }
        return new UserProfileConnectionsView.ViewState(z4, z5, arrayList);
    }

    private final MessagePreprocessor createMessagePreprocessor() {
        return new MessagePreprocessor(-1L, this.revealedBioIndices, null, false, null, 28, null);
    }

    private final Observable<Channel> createPrivateChannelWithUser(long j) {
        return this.restAPI.createOrFetchDM(j);
    }

    public static /* synthetic */ void disconnectUser$default(WidgetUserSheetViewModel widgetUserSheetViewModel, Channel channel, int i, Object obj) {
        if ((i & 1) != 0) {
            channel = null;
        }
        widgetUserSheetViewModel.disconnectUser(channel);
    }

    public final void emitDismissSheetEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.DismissSheet.INSTANCE);
    }

    private final void emitLaunchSpectateEvent(ModelApplicationStream modelApplicationStream) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.LaunchSpectate(modelApplicationStream));
    }

    public final void emitLaunchVideoCallEvent(long j) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.LaunchVideoCall(j));
    }

    public final void emitLaunchVoiceCallEvent(long j) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.LaunchVoiceCall(j));
    }

    private final void emitRequestStreamPermissionsEvent(ModelApplicationStream modelApplicationStream) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.RequestPermissionsForSpectateStream(modelApplicationStream));
    }

    public final void emitShowFriendRequestAbortToast(int i, String str) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowFriendRequestErrorToast(i, str));
    }

    public final void emitShowToastEvent(@StringRes int i) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowToast(i));
    }

    private final List<Node<MessageRenderContext>> generateAst(CharSequence charSequence) {
        return Parser.parse$default(this.bioParser, charSequence, MessageParseState.Companion.getInitialState(), null, 4, null);
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x008e, code lost:
        if (r3 != null) goto L23;
     */
    /* JADX WARN: Removed duplicated region for block: B:100:0x023d  */
    /* JADX WARN: Removed duplicated region for block: B:107:0x0254  */
    /* JADX WARN: Removed duplicated region for block: B:114:0x026b  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x0270  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x0284  */
    /* JADX WARN: Removed duplicated region for block: B:126:0x0289  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x028e  */
    /* JADX WARN: Removed duplicated region for block: B:130:0x0293  */
    /* JADX WARN: Removed duplicated region for block: B:136:0x02a6  */
    /* JADX WARN: Removed duplicated region for block: B:144:0x02b8 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:148:0x02c2  */
    /* JADX WARN: Removed duplicated region for block: B:151:0x02cd  */
    /* JADX WARN: Removed duplicated region for block: B:152:0x02d2  */
    /* JADX WARN: Removed duplicated region for block: B:155:0x02d7  */
    /* JADX WARN: Removed duplicated region for block: B:156:0x02dc  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x0213  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.user.usersheet.WidgetUserSheetViewModel.StoreState r39) {
        /*
            Method dump skipped, instructions count: 782
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.usersheet.WidgetUserSheetViewModel.handleStoreState(com.discord.widgets.user.usersheet.WidgetUserSheetViewModel$StoreState):void");
    }

    private final boolean isInSameVoiceChannel(VoiceState voiceState, VoiceState voiceState2) {
        Long l = null;
        Long a = voiceState2 != null ? voiceState2.a() : null;
        if (voiceState != null) {
            l = voiceState.a();
        }
        if (a == null || l == null) {
            return false;
        }
        return m.areEqual(a, l);
    }

    public final void addRelationship(Integer num, String str, @StringRes int i, String str2) {
        m.checkNotNullParameter(str, "username");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.addRelationship("User Profile", this.userId, num, this.friendToken, str2), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$addRelationship$2(this, num, str, i), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$addRelationship$1(this, i));
    }

    public final void banUser() {
        Channel channel;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (channel = loaded.getChannel()) != null) {
            User user = loaded.getUser();
            this.eventSubject.k.onNext(new Event.LaunchBanUser(user.getUsername(), channel.f(), user.getId()));
        }
    }

    public final void disableCommunication() {
        Channel channel;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (channel = loaded.getChannel()) != null) {
            User user = loaded.getUser();
            GuildMember guildMember = loaded.getGuildMember();
            if (guildMember != null ? guildMember.isCommunicationDisabled() : false) {
                this.eventSubject.k.onNext(new Event.LaunchEnableCommunication(user.getId(), channel.f()));
            } else {
                this.eventSubject.k.onNext(new Event.LaunchDisableCommunication(user.getId(), channel.f()));
            }
        }
    }

    public final void disconnectUser(Channel channel) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            if (channel == null) {
                channel = loaded.getChannel();
            }
            if (channel != null) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPISerializeNulls.disconnectGuildMember(channel.f(), loaded.getUser().getId(), new RestAPIParams.GuildMemberDisconnect(null, 1, null)), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$disconnectUser$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$disconnectUser$1(this));
            }
        }
    }

    public final void editMember() {
        Channel channel;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (channel = loaded.getChannel()) != null) {
            this.eventSubject.k.onNext(new Event.LaunchEditMember(channel.f(), this.userId));
        }
    }

    public final boolean getOpenPopoutLogged() {
        return this.openPopoutLogged;
    }

    public final void guildDeafenUser() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            Channel channel = loaded.getChannel();
            User user = loaded.getUser();
            UserProfileAdminView.ViewState adminViewState = loaded.getAdminViewState();
            if (channel != null && adminViewState != null) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.changeGuildMember(channel.f(), user.getId(), RestAPIParams.GuildMember.Companion.createWithDeaf(!adminViewState.isServerDeafened())), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$guildDeafenUser$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetUserSheetViewModel$guildDeafenUser$1.INSTANCE);
            }
        }
    }

    public final void guildMoveForUser() {
        Channel channel;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (channel = loaded.getChannel()) != null) {
            this.eventSubject.k.onNext(new Event.LaunchMoveUser(channel.f()));
        }
    }

    public final void guildMuteUser() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            Channel channel = loaded.getChannel();
            UserProfileAdminView.ViewState adminViewState = loaded.getAdminViewState();
            User user = loaded.getUser();
            if (channel != null && adminViewState != null) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.changeGuildMember(channel.f(), user.getId(), RestAPIParams.GuildMember.Companion.createWithMute(!loaded.getAdminViewState().isServerMuted())), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$guildMuteUser$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetUserSheetViewModel$guildMuteUser$1.INSTANCE);
            }
        }
    }

    @MainThread
    public final void handleBioIndexClicked(SpoilerNode<?> spoilerNode) {
        StoreState storeState;
        UserProfile userProfile;
        com.discord.api.user.User g;
        NullSerializable<String> d;
        String a;
        m.checkNotNullParameter(spoilerNode, "spoilerNode");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (storeState = this.mostRecentStoreState) != null && (userProfile = storeState.getUserProfile()) != null && (g = userProfile.g()) != null && (d = g.d()) != null && (a = d.a()) != null) {
            this.revealedBioIndices.add(Integer.valueOf(spoilerNode.getId()));
            updateViewState(ViewState.Loaded.copy$default(loaded, null, false, false, null, null, null, null, null, null, null, null, null, null, null, 0, null, null, null, false, createAndProcessBioAstFromText(a), false, null, null, null, 16252927, null));
        }
    }

    public final void inviteUserToSpeak() {
        Channel channel;
        UserProfileStageActionsView.ViewState viewState;
        ViewState viewState2 = getViewState();
        if (!(viewState2 instanceof ViewState.Loaded)) {
            viewState2 = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState2;
        if (loaded != null && (channel = loaded.getStageViewState().getChannel()) != null) {
            long id2 = loaded.getUser().getId();
            viewState = r14.m47copyam1GJgw((r18 & 1) != 0 ? r14.isMe : false, (r18 & 2) != 0 ? r14.channel : null, (r18 & 4) != 0 ? r14.userStageRole : null, (r18 & 8) != 0 ? r14.userRequestToSpeakState : null, (r18 & 16) != 0 ? r14.userInSameVoiceChannel : false, (r18 & 32) != 0 ? r14.canMuteMembers : false, (r18 & 64) != 0 ? r14.isUpdatingSuppressed : false, (r18 & 128) != 0 ? loaded.getStageViewState().isInvitingToSpeak : true);
            updateViewState(ViewState.Loaded.copy$default(loaded, null, false, false, null, null, null, null, null, null, viewState, null, null, null, null, 0, null, null, null, false, null, false, null, null, null, 16776703, null));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(StageChannelAPI.setUserSuppressedInChannel$default(StageChannelAPI.INSTANCE, channel, id2, false, 0L, 8, null), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$inviteUserToSpeak$1(this));
        }
    }

    public final void kickUser() {
        Channel channel;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (channel = loaded.getChannel()) != null) {
            User user = loaded.getUser();
            if (ChannelUtils.w(channel)) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().removeChannelRecipient(channel.h(), user.getId()), false, 1, null), 0L, false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : null, "REST: remove group member", (r18 & 4) != 0 ? null : null, new WidgetUserSheetViewModel$kickUser$1(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
                return;
            }
            this.eventSubject.k.onNext(new Event.LaunchKickUser(user.getUsername(), channel.f(), user.getId()));
        }
    }

    public final void launchVideoCall() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(createPrivateChannelWithUser(this.userId), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$launchVideoCall$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$launchVideoCall$1(this));
    }

    public final void launchVoiceCall() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(createPrivateChannelWithUser(this.userId), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$launchVoiceCall$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$launchVoiceCall$1(this));
    }

    public final void moveUserToChannel(long j) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            Channel channel = loaded.getChannel();
            User user = loaded.getUser();
            if (channel != null) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.changeGuildMember(channel.f(), user.getId(), RestAPIParams.GuildMember.Companion.createWithChannelId(j)), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$moveUserToChannel$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$moveUserToChannel$1(this));
            }
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void onActivityCustomButtonClicked(Context context, long j, String str, long j2, int i) {
        m.checkNotNullParameter(context, "applicationContext");
        m.checkNotNullParameter(str, "sessionId");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getActivityMetadata(j, str, j2), false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : null, "REST: Custom Button GetActivityMetadata", (r18 & 4) != 0 ? null : null, new WidgetUserSheetViewModel$onActivityCustomButtonClicked$1(i, context), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    @MainThread
    public final void onSpectatePermissionsGranted(ModelApplicationStream modelApplicationStream) {
        m.checkNotNullParameter(modelApplicationStream, "stream");
        StoreApplicationStreaming.targetStream$default(this.storeApplicationStreaming, modelApplicationStream.getEncodedStreamKey(), false, 2, null);
        int ordinal = this.streamPreviewClickBehavior.ordinal();
        if (ordinal == 0) {
            emitLaunchSpectateEvent(modelApplicationStream);
        } else if (ordinal == 1) {
            emitDismissSheetEvent();
        }
    }

    public final void onStreamPreviewClicked(StreamContext streamContext) {
        m.checkNotNullParameter(streamContext, "streamContext");
        if (streamContext.getJoinability() == StreamContext.Joinability.MISSING_PERMISSIONS) {
            emitShowToastEvent(R.string.channel_locked);
        } else {
            emitRequestStreamPermissionsEvent(streamContext.getStream());
        }
    }

    public final void removeRelationship(@StringRes int i) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.removeRelationship("User Profile", this.userId), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSheetViewModel$removeRelationship$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$removeRelationship$1(this, i));
    }

    public final void saveUserNote(Context context, String str) {
        m.checkNotNullParameter(str, "noteText");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            boolean z2 = true;
            if ((!(loaded.getUserNoteFetchState() instanceof StoreUserNotes.UserNoteState.Empty) || !(!m.areEqual(str, ""))) && (!(loaded.getUserNoteFetchState() instanceof StoreUserNotes.UserNoteState.Loaded) || !(!m.areEqual(((StoreUserNotes.UserNoteState.Loaded) loaded.getUserNoteFetchState()).getNote().getNote(), str)))) {
                z2 = false;
            }
            if (z2) {
                this.storeUserNotes.saveNote(context, this.userId, str);
            }
        }
    }

    public final void setOpenPopoutLogged(boolean z2) {
        this.openPopoutLogged = z2;
    }

    public final void setUserOutputVolume(float f) {
        this.storeMediaSettings.setUserOutputVolume(this.userId, PerceptualVolumeUtils.perceptualToAmplitude$default(PerceptualVolumeUtils.INSTANCE, f, 0.0f, 2, null));
    }

    public final void setUserSuppressedInChannel(boolean z2) {
        Channel channel;
        UserProfileStageActionsView.ViewState viewState;
        ViewState viewState2 = getViewState();
        if (!(viewState2 instanceof ViewState.Loaded)) {
            viewState2 = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState2;
        if (loaded != null && (channel = loaded.getStageViewState().getChannel()) != null) {
            long id2 = loaded.getUser().getId();
            viewState = r14.m47copyam1GJgw((r18 & 1) != 0 ? r14.isMe : false, (r18 & 2) != 0 ? r14.channel : null, (r18 & 4) != 0 ? r14.userStageRole : null, (r18 & 8) != 0 ? r14.userRequestToSpeakState : null, (r18 & 16) != 0 ? r14.userInSameVoiceChannel : false, (r18 & 32) != 0 ? r14.canMuteMembers : false, (r18 & 64) != 0 ? r14.isUpdatingSuppressed : true, (r18 & 128) != 0 ? loaded.getStageViewState().isInvitingToSpeak : false);
            updateViewState(ViewState.Loaded.copy$default(loaded, null, false, false, null, null, null, null, null, null, viewState, null, null, null, null, 0, null, null, null, false, null, false, null, null, null, 16776703, null));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(StageChannelAPI.setUserSuppressedInChannel$default(StageChannelAPI.INSTANCE, channel, id2, z2, 0L, 8, null), false, 1, null), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSheetViewModel$setUserSuppressedInChannel$1(this));
        }
    }

    public final void toggleDeafen(boolean z2) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && loaded.isMe() && (!m.areEqual(loaded.getVoiceSettingsViewState().isDeafened(), Boolean.valueOf(z2)))) {
            this.storeMediaSettings.toggleSelfDeafened();
        }
    }

    public final void toggleMute(boolean z2) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && loaded.getVoiceSettingsViewState().isMuted() != z2) {
            if (loaded.isMe()) {
                StoreMediaSettings.SelfMuteFailure selfMuteFailure = this.storeMediaSettings.toggleSelfMuted();
                if (selfMuteFailure != null && selfMuteFailure.ordinal() == 0) {
                    emitShowToastEvent(R.string.vad_permission_small);
                    return;
                }
                return;
            }
            this.storeMediaSettings.toggleUserMuted(this.userId);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheetViewModel(long j, long j2, Long l, String str, boolean z2, Observable<StoreState> observable, WidgetUserSheet.StreamPreviewClickBehavior streamPreviewClickBehavior, StoreMediaSettings storeMediaSettings, StoreApplicationStreaming storeApplicationStreaming, StoreUserNotes storeUserNotes, StoreUserProfile storeUserProfile, RestAPI restAPI, RestAPI restAPI2, StoreApplicationStreamPreviews storeApplicationStreamPreviews, Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(observable, "storeObservable");
        m.checkNotNullParameter(streamPreviewClickBehavior, "streamPreviewClickBehavior");
        m.checkNotNullParameter(storeMediaSettings, "storeMediaSettings");
        m.checkNotNullParameter(storeApplicationStreaming, "storeApplicationStreaming");
        m.checkNotNullParameter(storeUserNotes, "storeUserNotes");
        m.checkNotNullParameter(storeUserProfile, "storeUserProfile");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(restAPI2, "restAPISerializeNulls");
        m.checkNotNullParameter(storeApplicationStreamPreviews, "storeApplicationStreamPreviews");
        m.checkNotNullParameter(parser, "bioParser");
        this.userId = j;
        this.channelId = j2;
        this.guildId = l;
        this.friendToken = str;
        this.isVoiceContext = z2;
        this.streamPreviewClickBehavior = streamPreviewClickBehavior;
        this.storeMediaSettings = storeMediaSettings;
        this.storeApplicationStreaming = storeApplicationStreaming;
        this.storeUserNotes = storeUserNotes;
        this.storeUserProfile = storeUserProfile;
        this.restAPI = restAPI;
        this.restAPISerializeNulls = restAPI2;
        this.storeApplicationStreamPreviews = storeApplicationStreamPreviews;
        this.bioParser = parser;
        this.eventSubject = PublishSubject.k0();
        this.fetchedPreviews = new LinkedHashSet();
        this.revealedBioIndices = new LinkedHashSet();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetUserSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        storeUserNotes.loadNote(j);
        storeUserProfile.fetchProfile(j, (r13 & 2) != 0 ? null : l, (r13 & 4) != 0 ? false : false, (r13 & 8) != 0 ? null : null);
    }
}
