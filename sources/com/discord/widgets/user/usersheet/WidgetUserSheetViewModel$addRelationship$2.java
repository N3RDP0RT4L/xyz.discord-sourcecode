package com.discord.widgets.user.usersheet;

import com.discord.app.AppFragment;
import com.discord.utilities.captcha.CaptchaErrorBody;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.widgets.captcha.WidgetCaptchaBottomSheet;
import com.discord.widgets.captcha.WidgetCaptchaKt;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetUserSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheetViewModel$addRelationship$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ int $successMessageStringRes;
    public final /* synthetic */ Integer $type;
    public final /* synthetic */ String $username;
    public final /* synthetic */ WidgetUserSheetViewModel this$0;

    /* compiled from: WidgetUserSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheetViewModel$addRelationship$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Error $error;

        /* compiled from: WidgetUserSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/app/AppFragment;", "<anonymous parameter 0>", "", "captchaToken", "", "invoke", "(Lcom/discord/app/AppFragment;Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheetViewModel$addRelationship$2$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02621 extends o implements Function2<AppFragment, String, Unit> {
            public C02621() {
                super(2);
            }

            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(AppFragment appFragment, String str) {
                invoke2(appFragment, str);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(AppFragment appFragment, String str) {
                m.checkNotNullParameter(appFragment, "<anonymous parameter 0>");
                m.checkNotNullParameter(str, "captchaToken");
                WidgetUserSheetViewModel$addRelationship$2 widgetUserSheetViewModel$addRelationship$2 = WidgetUserSheetViewModel$addRelationship$2.this;
                widgetUserSheetViewModel$addRelationship$2.this$0.addRelationship(widgetUserSheetViewModel$addRelationship$2.$type, widgetUserSheetViewModel$addRelationship$2.$username, widgetUserSheetViewModel$addRelationship$2.$successMessageStringRes, str);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Error error) {
            super(0);
            this.$error = error;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            if (WidgetCaptchaKt.isCaptchaError(this.$error)) {
                WidgetCaptchaBottomSheet.Companion.enqueue$default(WidgetCaptchaBottomSheet.Companion, "Add Friend Captcha", new C02621(), null, CaptchaErrorBody.Companion.createFromError(this.$error), 4, null);
                return;
            }
            WidgetUserSheetViewModel widgetUserSheetViewModel = WidgetUserSheetViewModel$addRelationship$2.this.this$0;
            Error.Response response = this.$error.getResponse();
            m.checkNotNullExpressionValue(response, "error.response");
            widgetUserSheetViewModel.emitShowFriendRequestAbortToast(response.getCode(), WidgetUserSheetViewModel$addRelationship$2.this.$username);
        }
    }

    /* compiled from: WidgetUserSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.usersheet.WidgetUserSheetViewModel$addRelationship$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function0<Boolean> {
        public AnonymousClass2() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final Boolean invoke2() {
            WidgetUserSheetViewModel$addRelationship$2.this.this$0.emitShowToastEvent(R.string.default_failure_to_perform_action_message);
            return null;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheetViewModel$addRelationship$2(WidgetUserSheetViewModel widgetUserSheetViewModel, Integer num, String str, int i) {
        super(1);
        this.this$0 = widgetUserSheetViewModel;
        this.$type = num;
        this.$username = str;
        this.$successMessageStringRes = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        m.checkNotNullParameter(error, "error");
        RestAPIAbortMessages.INSTANCE.handleAbortCodeOrDefault(error, new AnonymousClass1(error), new AnonymousClass2());
    }
}
