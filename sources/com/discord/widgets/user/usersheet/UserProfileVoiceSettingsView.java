package com.discord.widgets.user.usersheet;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import b.d.b.a.a;
import com.discord.databinding.UserProfileVoiceSettingsViewBinding;
import com.discord.views.calls.VolumeSliderView;
import com.google.android.material.switchmaterial.SwitchMaterial;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: UserProfileVoiceSettingsView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u001bB\u0017\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J!\u0010\n\u001a\u00020\u00042\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\u0007¢\u0006\u0004\b\n\u0010\u000bJ!\u0010\f\u001a\u00020\u00042\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\u0007¢\u0006\u0004\b\f\u0010\u000bJ'\u0010\u0010\u001a\u00020\u00042\u0018\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\r¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;", "Landroid/widget/LinearLayout;", "Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;", "viewState", "", "updateView", "(Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;)V", "Lkotlin/Function1;", "", "onChecked", "setOnMuteChecked", "(Lkotlin/jvm/functions/Function1;)V", "setOnDeafenChecked", "Lkotlin/Function2;", "", "onProgressChanged", "setOnVolumeChange", "(Lkotlin/jvm/functions/Function2;)V", "Lcom/discord/databinding/UserProfileVoiceSettingsViewBinding;", "binding", "Lcom/discord/databinding/UserProfileVoiceSettingsViewBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileVoiceSettingsView extends LinearLayout {
    private final UserProfileVoiceSettingsViewBinding binding;

    /* compiled from: UserProfileVoiceSettingsView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ2\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0016\u001a\u00020\u00022\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\n\u0010\u0004R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u000b\u0010\u0006R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\t¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;", "", "", "component1", "()Z", "component2", "()Ljava/lang/Boolean;", "", "component3", "()Ljava/lang/Float;", "isMuted", "isDeafened", "outputVolume", "copy", "(ZLjava/lang/Boolean;Ljava/lang/Float;)Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/Boolean;", "Ljava/lang/Float;", "getOutputVolume", HookHelper.constructorName, "(ZLjava/lang/Boolean;Ljava/lang/Float;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Boolean isDeafened;
        private final boolean isMuted;
        private final Float outputVolume;

        public ViewState(boolean z2, Boolean bool, Float f) {
            this.isMuted = z2;
            this.isDeafened = bool;
            this.outputVolume = f;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, boolean z2, Boolean bool, Float f, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = viewState.isMuted;
            }
            if ((i & 2) != 0) {
                bool = viewState.isDeafened;
            }
            if ((i & 4) != 0) {
                f = viewState.outputVolume;
            }
            return viewState.copy(z2, bool, f);
        }

        public final boolean component1() {
            return this.isMuted;
        }

        public final Boolean component2() {
            return this.isDeafened;
        }

        public final Float component3() {
            return this.outputVolume;
        }

        public final ViewState copy(boolean z2, Boolean bool, Float f) {
            return new ViewState(z2, bool, f);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return this.isMuted == viewState.isMuted && m.areEqual(this.isDeafened, viewState.isDeafened) && m.areEqual(this.outputVolume, viewState.outputVolume);
        }

        public final Float getOutputVolume() {
            return this.outputVolume;
        }

        public int hashCode() {
            boolean z2 = this.isMuted;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            int i3 = i * 31;
            Boolean bool = this.isDeafened;
            int i4 = 0;
            int hashCode = (i3 + (bool != null ? bool.hashCode() : 0)) * 31;
            Float f = this.outputVolume;
            if (f != null) {
                i4 = f.hashCode();
            }
            return hashCode + i4;
        }

        public final Boolean isDeafened() {
            return this.isDeafened;
        }

        public final boolean isMuted() {
            return this.isMuted;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(isMuted=");
            R.append(this.isMuted);
            R.append(", isDeafened=");
            R.append(this.isDeafened);
            R.append(", outputVolume=");
            R.append(this.outputVolume);
            R.append(")");
            return R.toString();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserProfileVoiceSettingsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        View inflate = LayoutInflater.from(context).inflate(R.layout.user_profile_voice_settings_view, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.user_sheet_deafen;
        SwitchMaterial switchMaterial = (SwitchMaterial) inflate.findViewById(R.id.user_sheet_deafen);
        if (switchMaterial != null) {
            i = R.id.user_sheet_muted;
            SwitchMaterial switchMaterial2 = (SwitchMaterial) inflate.findViewById(R.id.user_sheet_muted);
            if (switchMaterial2 != null) {
                i = R.id.user_sheet_volume_label;
                TextView textView = (TextView) inflate.findViewById(R.id.user_sheet_volume_label);
                if (textView != null) {
                    i = R.id.user_sheet_volume_slider;
                    VolumeSliderView volumeSliderView = (VolumeSliderView) inflate.findViewById(R.id.user_sheet_volume_slider);
                    if (volumeSliderView != null) {
                        UserProfileVoiceSettingsViewBinding userProfileVoiceSettingsViewBinding = new UserProfileVoiceSettingsViewBinding((LinearLayout) inflate, switchMaterial, switchMaterial2, textView, volumeSliderView);
                        m.checkNotNullExpressionValue(userProfileVoiceSettingsViewBinding, "UserProfileVoiceSettings…rom(context), this, true)");
                        this.binding = userProfileVoiceSettingsViewBinding;
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void setOnDeafenChecked(final Function1<? super Boolean, Unit> function1) {
        m.checkNotNullParameter(function1, "onChecked");
        this.binding.f2151b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: com.discord.widgets.user.usersheet.UserProfileVoiceSettingsView$setOnDeafenChecked$1
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
                Function1.this.invoke(Boolean.valueOf(z2));
            }
        });
    }

    public final void setOnMuteChecked(final Function1<? super Boolean, Unit> function1) {
        m.checkNotNullParameter(function1, "onChecked");
        this.binding.c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: com.discord.widgets.user.usersheet.UserProfileVoiceSettingsView$setOnMuteChecked$1
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
                Function1.this.invoke(Boolean.valueOf(z2));
            }
        });
    }

    public final void setOnVolumeChange(Function2<? super Float, ? super Boolean, Unit> function2) {
        m.checkNotNullParameter(function2, "onProgressChanged");
        this.binding.e.setOnVolumeChange(function2);
    }

    public final void updateView(ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        VolumeSliderView volumeSliderView = this.binding.e;
        Float outputVolume = viewState.getOutputVolume();
        boolean z2 = false;
        int roundToInt = outputVolume != null ? d0.a0.a.roundToInt(outputVolume.floatValue()) : 0;
        SeekBar seekBar = volumeSliderView.j.d;
        m.checkNotNullExpressionValue(seekBar, "binding.volumeSliderSeekBar");
        seekBar.setProgress(roundToInt);
        VolumeSliderView volumeSliderView2 = this.binding.e;
        m.checkNotNullExpressionValue(volumeSliderView2, "binding.userSheetVolumeSlider");
        boolean z3 = true;
        int i = 8;
        volumeSliderView2.setVisibility(viewState.getOutputVolume() != null ? 0 : 8);
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.userSheetVolumeLabel");
        textView.setVisibility(viewState.getOutputVolume() != null ? 0 : 8);
        SwitchMaterial switchMaterial = this.binding.c;
        m.checkNotNullExpressionValue(switchMaterial, "binding.userSheetMuted");
        switchMaterial.setChecked(viewState.isMuted());
        SwitchMaterial switchMaterial2 = this.binding.f2151b;
        m.checkNotNullExpressionValue(switchMaterial2, "binding.userSheetDeafen");
        if (viewState.isDeafened() == null) {
            z3 = false;
        }
        if (z3) {
            i = 0;
        }
        switchMaterial2.setVisibility(i);
        SwitchMaterial switchMaterial3 = this.binding.f2151b;
        m.checkNotNullExpressionValue(switchMaterial3, "binding.userSheetDeafen");
        Boolean isDeafened = viewState.isDeafened();
        if (isDeafened != null) {
            z2 = isDeafened.booleanValue();
        }
        switchMaterial3.setChecked(z2);
    }
}
