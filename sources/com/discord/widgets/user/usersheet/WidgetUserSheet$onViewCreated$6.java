package com.discord.widgets.user.usersheet;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetUserSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "progress", "", "fromUser", "", "invoke", "(FZ)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSheet$onViewCreated$6 extends o implements Function2<Float, Boolean, Unit> {
    public final /* synthetic */ WidgetUserSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSheet$onViewCreated$6(WidgetUserSheet widgetUserSheet) {
        super(2);
        this.this$0 = widgetUserSheet;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Float f, Boolean bool) {
        invoke(f.floatValue(), bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(float f, boolean z2) {
        WidgetUserSheetViewModel viewModel;
        if (z2) {
            viewModel = this.this$0.getViewModel();
            viewModel.setUserOutputVolume(f);
        }
    }
}
