package com.discord.widgets.user;

import com.discord.widgets.user.WidgetUserStatusSheetViewModel;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserStatusSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;", "p1", "", "invoke", "(Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUserStatusSheet$onResume$1 extends k implements Function1<WidgetUserStatusSheetViewModel.ViewState, Unit> {
    public WidgetUserStatusSheet$onResume$1(WidgetUserStatusSheet widgetUserStatusSheet) {
        super(1, widgetUserStatusSheet, WidgetUserStatusSheet.class, "updateView", "updateView(Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetUserStatusSheetViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetUserStatusSheetViewModel.ViewState viewState) {
        m.checkNotNullParameter(viewState, "p1");
        ((WidgetUserStatusSheet) this.receiver).updateView(viewState);
    }
}
