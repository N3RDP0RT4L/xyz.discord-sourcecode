package com.discord.widgets.user;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.i.a6;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetUserMutualFriendsBinding;
import com.discord.databinding.WidgetUserProfileAdapterItemFriendBinding;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.guild.Guild;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.StatusView;
import com.discord.widgets.user.WidgetUserMutualFriends;
import com.discord.widgets.user.profile.WidgetUserProfileEmptyListItem;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func6;
import xyz.discord.R;
/* compiled from: WidgetUserMutualFriends.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001d2\u00020\u0001:\u0003\u001d\u001e\u001fB\u0007¢\u0006\u0004\b\u001c\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006 "}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model;", "data", "", "configureUI", "(Lcom/discord/widgets/user/WidgetUserMutualFriends$Model;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "", "userId$delegate", "Lkotlin/Lazy;", "getUserId", "()J", "userId", "Lcom/discord/databinding/WidgetUserMutualFriendsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserMutualFriendsBinding;", "binding", "Lcom/discord/widgets/user/WidgetUserMutualFriends$MutualFriendsAdapter;", "adapter", "Lcom/discord/widgets/user/WidgetUserMutualFriends$MutualFriendsAdapter;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "MutualFriendsAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserMutualFriends extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserMutualFriends.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserMutualFriendsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private MutualFriendsAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserMutualFriends$binding$2.INSTANCE, null, 2, null);
    private final Lazy userId$delegate = g.lazy(new WidgetUserMutualFriends$userId$2(this));

    /* compiled from: WidgetUserMutualFriends.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/models/user/User;", "user", "", "show", "(Landroid/content/Context;Lcom/discord/models/user/User;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Context context, User user) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(user, "user");
            Intent putExtra = new Intent().putExtra("com.discord.intent.extra.EXTRA_USER_ID", user.getId());
            m.checkNotNullExpressionValue(putExtra, "Intent().putExtra(Intents.EXTRA_USER_ID, user.id)");
            j.d(context, WidgetUserMutualFriends.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserMutualFriends.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\r\b\u0082\b\u0018\u0000 !2\u00020\u0001:\u0002!\"B'\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\u001f\u0010 J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ6\u0010\u000f\u001a\u00020\u00002\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0014\u0010\u000bJ\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u000bR\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\b¨\u0006#"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$Model;", "", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item;", "component2", "()Ljava/util/List;", "", "component3", "()I", "user", "items", "numMutualFriends", "copy", "(Lcom/discord/models/user/User;Ljava/util/List;I)Lcom/discord/widgets/user/WidgetUserMutualFriends$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/user/User;", "getUser", "I", "getNumMutualFriends", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Lcom/discord/models/user/User;Ljava/util/List;I)V", "Companion", "Item", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final List<Item> items;
        private final int numMutualFriends;
        private final User user;

        /* compiled from: WidgetUserMutualFriends.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Companion;", "", "", "Lcom/discord/primitives/UserId;", "userId", "Lrx/Observable;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final long j) {
                Observable Y = ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getRelationships(j), false, 1, null).Y(new b<List<? extends ModelUserRelationship>, Observable<? extends Model>>() { // from class: com.discord.widgets.user.WidgetUserMutualFriends$Model$Companion$get$1

                    /* compiled from: WidgetUserMutualFriends.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0015\u001a\n \u0005*\u0004\u0018\u00010\u00120\u00122:\u0010\u0006\u001a6\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0003 \u0005*\u001a\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0003\u0018\u00010\u00000\u00002.\u0010\b\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0007 \u0005*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00000\u000026\u0010\u000b\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\tj\u0002`\n \u0005*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\tj\u0002`\n\u0018\u00010\u00000\u000026\u0010\u000e\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\fj\u0002`\r \u0005*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\fj\u0002`\r\u0018\u00010\u00000\u00002.\u0010\u0010\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u000f \u0005*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u00000\u00002\b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\n¢\u0006\u0004\b\u0013\u0010\u0014"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/models/guild/Guild;", "kotlin.jvm.PlatformType", "mutualGuilds", "Lcom/discord/models/user/User;", "users", "Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "presences", "", "Lcom/discord/primitives/RelationshipType;", "relationships", "Lcom/discord/models/domain/ModelApplicationStream;", "applicationStreams", "modelUser", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/user/User;)Lcom/discord/widgets/user/WidgetUserMutualFriends$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.user.WidgetUserMutualFriends$Model$Companion$get$1$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1<T1, T2, T3, T4, T5, T6, R> implements Func6<Map<Long, ? extends List<? extends Guild>>, Map<Long, ? extends User>, Map<Long, ? extends Presence>, Map<Long, ? extends Integer>, Map<Long, ? extends ModelApplicationStream>, User, WidgetUserMutualFriends.Model> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        @Override // rx.functions.Func6
                        public /* bridge */ /* synthetic */ WidgetUserMutualFriends.Model call(Map<Long, ? extends List<? extends Guild>> map, Map<Long, ? extends User> map2, Map<Long, ? extends Presence> map3, Map<Long, ? extends Integer> map4, Map<Long, ? extends ModelApplicationStream> map5, User user) {
                            return call2((Map<Long, ? extends List<Guild>>) map, map2, (Map<Long, Presence>) map3, (Map<Long, Integer>) map4, map5, user);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final WidgetUserMutualFriends.Model call2(Map<Long, ? extends List<Guild>> map, Map<Long, ? extends User> map2, Map<Long, Presence> map3, Map<Long, Integer> map4, Map<Long, ? extends ModelApplicationStream> map5, User user) {
                            m.checkNotNullExpressionValue(map2, "users");
                            LinkedHashMap linkedHashMap = new LinkedHashMap();
                            for (Map.Entry<Long, ? extends User> entry : map2.entrySet()) {
                                if (map4.containsKey(Long.valueOf(entry.getKey().longValue()))) {
                                    linkedHashMap.put(entry.getKey(), entry.getValue());
                                }
                            }
                            ArrayList arrayList = new ArrayList(linkedHashMap.size());
                            for (Map.Entry entry2 : linkedHashMap.entrySet()) {
                                long longValue = ((Number) entry2.getKey()).longValue();
                                User user2 = (User) entry2.getValue();
                                Presence presence = map3.get(Long.valueOf(longValue));
                                List<Guild> list = map.get(Long.valueOf(longValue));
                                if (list == null) {
                                    list = n.emptyList();
                                }
                                arrayList.add(new WidgetUserMutualFriends.Model.Item.MutualFriend(user2, presence, list, Boolean.valueOf(map5.containsKey(Long.valueOf(longValue)))));
                            }
                            return new WidgetUserMutualFriends.Model(user, arrayList.isEmpty() ? u.plus((Collection<? extends WidgetUserMutualFriends.Model.Item.Empty>) arrayList, WidgetUserMutualFriends.Model.Item.Empty.INSTANCE) : arrayList, arrayList.size());
                        }
                    }

                    public final Observable<? extends WidgetUserMutualFriends.Model> call(List<? extends ModelUserRelationship> list) {
                        m.checkNotNullExpressionValue(list, "relationshipsApiResponse");
                        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                        for (ModelUserRelationship modelUserRelationship : list) {
                            arrayList.add(Long.valueOf(modelUserRelationship.getId()));
                        }
                        Observable<Map<Long, List<Guild>>> observeMutualGuilds = new UserMutualGuildsManager(null, null, null, 7, null).observeMutualGuilds(arrayList);
                        StoreStream.Companion companion = StoreStream.Companion;
                        return ObservableWithLeadingEdgeThrottle.combineLatest(observeMutualGuilds, companion.getUsers().observeUsers(arrayList), companion.getPresences().observePresencesForUsers(arrayList), companion.getUserRelationships().observe(), companion.getApplicationStreaming().observeStreamsByUser(), companion.getUsers().observeUser(j), AnonymousClass1.INSTANCE, 1000L, TimeUnit.MILLISECONDS);
                    }
                });
                m.checkNotNullExpressionValue(Y, "RestAPI\n            .api…          )\n            }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(Y).q();
                m.checkNotNullExpressionValue(q, "RestAPI\n            .api…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetUserMutualFriends.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", "Companion", "Empty", "MutualFriend", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item$MutualFriend;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item$Empty;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static abstract class Item implements MGRecyclerDataPayload {
            public static final Companion Companion = new Companion(null);
            public static final int TYPE_EMPTY = 0;
            public static final int TYPE_FRIEND = 1;

            /* compiled from: WidgetUserMutualFriends.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item$Companion;", "", "", "TYPE_EMPTY", "I", "TYPE_FRIEND", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            /* compiled from: WidgetUserMutualFriends.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item$Empty;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item;", "", "type", "I", "getType", "()I", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Empty extends Item {
                public static final Empty INSTANCE;
                private static final String key;
                private static final int type = 0;

                static {
                    Empty empty = new Empty();
                    INSTANCE = empty;
                    StringBuilder R = a.R("empty");
                    R.append(empty.getType());
                    key = R.toString();
                }

                private Empty() {
                    super(null);
                }

                @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
                public String getKey() {
                    return key;
                }

                @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
                public int getType() {
                    return type;
                }
            }

            /* compiled from: WidgetUserMutualFriends.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJB\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001d\u001a\u00020\f2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001f\u001a\u0004\b \u0010\u0007R\u001c\u0010!\u001a\u00020\u00158\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\u0017R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010$\u001a\u0004\b%\u0010\u0004R\u001c\u0010&\u001a\u00020\u00188\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010\u001aR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010)\u001a\u0004\b\u0012\u0010\u000eR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010*\u001a\u0004\b+\u0010\u000b¨\u0006."}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item$MutualFriend;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item;", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/presence/Presence;", "component2", "()Lcom/discord/models/presence/Presence;", "", "Lcom/discord/models/guild/Guild;", "component3", "()Ljava/util/List;", "", "component4", "()Ljava/lang/Boolean;", "user", "presence", "mutualGuilds", "isApplicationStreaming", "copy", "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;Ljava/util/List;Ljava/lang/Boolean;)Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item$MutualFriend;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/presence/Presence;", "getPresence", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/user/User;", "getUser", "type", "I", "getType", "Ljava/lang/Boolean;", "Ljava/util/List;", "getMutualGuilds", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;Ljava/util/List;Ljava/lang/Boolean;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class MutualFriend extends Item {
                private final Boolean isApplicationStreaming;
                private final String key;
                private final List<Guild> mutualGuilds;
                private final Presence presence;
                private final int type;
                private final User user;

                public /* synthetic */ MutualFriend(User user, Presence presence, List list, Boolean bool, int i, DefaultConstructorMarker defaultConstructorMarker) {
                    this(user, presence, list, (i & 8) != 0 ? null : bool);
                }

                /* JADX WARN: Multi-variable type inference failed */
                public static /* synthetic */ MutualFriend copy$default(MutualFriend mutualFriend, User user, Presence presence, List list, Boolean bool, int i, Object obj) {
                    if ((i & 1) != 0) {
                        user = mutualFriend.user;
                    }
                    if ((i & 2) != 0) {
                        presence = mutualFriend.presence;
                    }
                    if ((i & 4) != 0) {
                        list = mutualFriend.mutualGuilds;
                    }
                    if ((i & 8) != 0) {
                        bool = mutualFriend.isApplicationStreaming;
                    }
                    return mutualFriend.copy(user, presence, list, bool);
                }

                public final User component1() {
                    return this.user;
                }

                public final Presence component2() {
                    return this.presence;
                }

                public final List<Guild> component3() {
                    return this.mutualGuilds;
                }

                public final Boolean component4() {
                    return this.isApplicationStreaming;
                }

                public final MutualFriend copy(User user, Presence presence, List<Guild> list, Boolean bool) {
                    m.checkNotNullParameter(user, "user");
                    m.checkNotNullParameter(list, "mutualGuilds");
                    return new MutualFriend(user, presence, list, bool);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof MutualFriend)) {
                        return false;
                    }
                    MutualFriend mutualFriend = (MutualFriend) obj;
                    return m.areEqual(this.user, mutualFriend.user) && m.areEqual(this.presence, mutualFriend.presence) && m.areEqual(this.mutualGuilds, mutualFriend.mutualGuilds) && m.areEqual(this.isApplicationStreaming, mutualFriend.isApplicationStreaming);
                }

                @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
                public String getKey() {
                    return this.key;
                }

                public final List<Guild> getMutualGuilds() {
                    return this.mutualGuilds;
                }

                public final Presence getPresence() {
                    return this.presence;
                }

                @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
                public int getType() {
                    return this.type;
                }

                public final User getUser() {
                    return this.user;
                }

                public int hashCode() {
                    User user = this.user;
                    int i = 0;
                    int hashCode = (user != null ? user.hashCode() : 0) * 31;
                    Presence presence = this.presence;
                    int hashCode2 = (hashCode + (presence != null ? presence.hashCode() : 0)) * 31;
                    List<Guild> list = this.mutualGuilds;
                    int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
                    Boolean bool = this.isApplicationStreaming;
                    if (bool != null) {
                        i = bool.hashCode();
                    }
                    return hashCode3 + i;
                }

                public final Boolean isApplicationStreaming() {
                    return this.isApplicationStreaming;
                }

                public String toString() {
                    StringBuilder R = a.R("MutualFriend(user=");
                    R.append(this.user);
                    R.append(", presence=");
                    R.append(this.presence);
                    R.append(", mutualGuilds=");
                    R.append(this.mutualGuilds);
                    R.append(", isApplicationStreaming=");
                    return a.C(R, this.isApplicationStreaming, ")");
                }

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public MutualFriend(User user, Presence presence, List<Guild> list, Boolean bool) {
                    super(null);
                    m.checkNotNullParameter(user, "user");
                    m.checkNotNullParameter(list, "mutualGuilds");
                    this.user = user;
                    this.presence = presence;
                    this.mutualGuilds = list;
                    this.isApplicationStreaming = bool;
                    this.type = 1;
                    StringBuilder sb = new StringBuilder();
                    sb.append(getType());
                    sb.append(user.getId());
                    this.key = sb.toString();
                }
            }

            private Item() {
            }

            public /* synthetic */ Item(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(User user, List<? extends Item> list, int i) {
            m.checkNotNullParameter(list, "items");
            this.user = user;
            this.items = list;
            this.numMutualFriends = i;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Model copy$default(Model model, User user, List list, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                user = model.user;
            }
            if ((i2 & 2) != 0) {
                list = model.items;
            }
            if ((i2 & 4) != 0) {
                i = model.numMutualFriends;
            }
            return model.copy(user, list, i);
        }

        public final User component1() {
            return this.user;
        }

        public final List<Item> component2() {
            return this.items;
        }

        public final int component3() {
            return this.numMutualFriends;
        }

        public final Model copy(User user, List<? extends Item> list, int i) {
            m.checkNotNullParameter(list, "items");
            return new Model(user, list, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.user, model.user) && m.areEqual(this.items, model.items) && this.numMutualFriends == model.numMutualFriends;
        }

        public final List<Item> getItems() {
            return this.items;
        }

        public final int getNumMutualFriends() {
            return this.numMutualFriends;
        }

        public final User getUser() {
            return this.user;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            List<Item> list = this.items;
            if (list != null) {
                i = list.hashCode();
            }
            return ((hashCode + i) * 31) + this.numMutualFriends;
        }

        public String toString() {
            StringBuilder R = a.R("Model(user=");
            R.append(this.user);
            R.append(", items=");
            R.append(this.items);
            R.append(", numMutualFriends=");
            return a.A(R, this.numMutualFriends, ")");
        }
    }

    /* compiled from: WidgetUserMutualFriends.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B\u0017\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$MutualFriendsAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/app/AppFragment;", "fragment", "Lcom/discord/app/AppFragment;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppFragment;)V", "ViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MutualFriendsAdapter extends MGRecyclerAdapterSimple<Model.Item> {
        private final AppFragment fragment;

        /* compiled from: WidgetUserMutualFriends.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/user/WidgetUserMutualFriends$MutualFriendsAdapter$ViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$MutualFriendsAdapter;", "Lcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/user/WidgetUserMutualFriends$Model$Item;)V", "Lcom/discord/databinding/WidgetUserProfileAdapterItemFriendBinding;", "binding", "Lcom/discord/databinding/WidgetUserProfileAdapterItemFriendBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/user/WidgetUserMutualFriends$MutualFriendsAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ViewHolder extends MGRecyclerViewHolder<MutualFriendsAdapter, Model.Item> {
            private final WidgetUserProfileAdapterItemFriendBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ViewHolder(MutualFriendsAdapter mutualFriendsAdapter) {
                super((int) R.layout.widget_user_profile_adapter_item_friend, mutualFriendsAdapter);
                m.checkNotNullParameter(mutualFriendsAdapter, "adapter");
                View view = this.itemView;
                int i = R.id.mutual_server_1;
                View findViewById = view.findViewById(R.id.mutual_server_1);
                if (findViewById != null) {
                    a6 a = a6.a(findViewById);
                    i = R.id.mutual_server_2;
                    View findViewById2 = view.findViewById(R.id.mutual_server_2);
                    if (findViewById2 != null) {
                        a6 a2 = a6.a(findViewById2);
                        i = R.id.mutual_server_3;
                        View findViewById3 = view.findViewById(R.id.mutual_server_3);
                        if (findViewById3 != null) {
                            a6 a3 = a6.a(findViewById3);
                            i = R.id.mutual_server_4;
                            View findViewById4 = view.findViewById(R.id.mutual_server_4);
                            if (findViewById4 != null) {
                                a6 a4 = a6.a(findViewById4);
                                i = R.id.user_profile_adapter_item_friend_avatar;
                                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.user_profile_adapter_item_friend_avatar);
                                if (simpleDraweeView != null) {
                                    i = R.id.user_profile_adapter_item_friend_mutual;
                                    LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.user_profile_adapter_item_friend_mutual);
                                    if (linearLayout != null) {
                                        i = R.id.user_profile_adapter_item_friend_status;
                                        StatusView statusView = (StatusView) view.findViewById(R.id.user_profile_adapter_item_friend_status);
                                        if (statusView != null) {
                                            i = R.id.user_profile_adapter_item_friend_user_game;
                                            SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) view.findViewById(R.id.user_profile_adapter_item_friend_user_game);
                                            if (simpleDraweeSpanTextView != null) {
                                                i = R.id.user_profile_adapter_item_friend_user_name;
                                                TextView textView = (TextView) view.findViewById(R.id.user_profile_adapter_item_friend_user_name);
                                                if (textView != null) {
                                                    WidgetUserProfileAdapterItemFriendBinding widgetUserProfileAdapterItemFriendBinding = new WidgetUserProfileAdapterItemFriendBinding((RelativeLayout) view, a, a2, a3, a4, simpleDraweeView, linearLayout, statusView, simpleDraweeSpanTextView, textView);
                                                    m.checkNotNullExpressionValue(widgetUserProfileAdapterItemFriendBinding, "WidgetUserProfileAdapter…endBinding.bind(itemView)");
                                                    this.binding = widgetUserProfileAdapterItemFriendBinding;
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
            }

            public static final /* synthetic */ MutualFriendsAdapter access$getAdapter$p(ViewHolder viewHolder) {
                return (MutualFriendsAdapter) viewHolder.adapter;
            }

            public void onConfigure(int i, Model.Item item) {
                a6 a6Var;
                m.checkNotNullParameter(item, "data");
                super.onConfigure(i, (int) item);
                final Model.Item.MutualFriend mutualFriend = (Model.Item.MutualFriend) item;
                this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetUserMutualFriends$MutualFriendsAdapter$ViewHolder$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        AppFragment appFragment;
                        WidgetUserSheet.Companion companion = WidgetUserSheet.Companion;
                        long id2 = mutualFriend.getUser().getId();
                        appFragment = WidgetUserMutualFriends.MutualFriendsAdapter.ViewHolder.access$getAdapter$p(WidgetUserMutualFriends.MutualFriendsAdapter.ViewHolder.this).fragment;
                        FragmentManager parentFragmentManager = appFragment.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager, "adapter.fragment.parentFragmentManager");
                        WidgetUserSheet.Companion.show$default(companion, id2, null, parentFragmentManager, null, null, null, null, 122, null);
                    }
                });
                TextView textView = this.binding.i;
                m.checkNotNullExpressionValue(textView, "binding.userProfileAdapterItemFriendUserName");
                textView.setText(mutualFriend.getUser().getUsername());
                this.binding.g.setPresence(mutualFriend.getPresence());
                SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.h;
                Presence presence = mutualFriend.getPresence();
                Boolean isApplicationStreaming = mutualFriend.isApplicationStreaming();
                m.checkNotNull(isApplicationStreaming);
                boolean booleanValue = isApplicationStreaming.booleanValue();
                m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "it");
                PresenceUtils.setPresenceText$default(presence, booleanValue, simpleDraweeSpanTextView, true, false, 16, null);
                SimpleDraweeView simpleDraweeView = this.binding.f;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.userProfileAdapterItemFriendAvatar");
                IconUtils.setIcon$default(simpleDraweeView, mutualFriend.getUser(), R.dimen.avatar_size_standard, null, null, null, 56, null);
                for (int i2 = 0; i2 <= 3; i2++) {
                    if (i2 == 1) {
                        a6Var = this.binding.f2663b;
                    } else if (i2 == 2) {
                        a6Var = this.binding.c;
                    } else if (i2 == 3) {
                        a6Var = this.binding.d;
                    } else if (i2 != 4) {
                        a6Var = this.binding.e;
                    } else {
                        a6Var = this.binding.e;
                    }
                    m.checkNotNullExpressionValue(a6Var, "when (i) {\n            1…mutualServer4\n          }");
                    RelativeLayout relativeLayout = a6Var.a;
                    m.checkNotNullExpressionValue(relativeLayout, "item.root");
                    relativeLayout.setVisibility(8);
                    TextView textView2 = a6Var.c;
                    m.checkNotNullExpressionValue(textView2, "item.userProfileAdapterItemFriendMutualText");
                    textView2.setVisibility(8);
                    SimpleDraweeView simpleDraweeView2 = a6Var.f79b;
                    m.checkNotNullExpressionValue(simpleDraweeView2, "item.userProfileAdapterItemFriendMutualImage");
                    simpleDraweeView2.setVisibility(8);
                    Guild guild = (Guild) u.getOrNull(mutualFriend.getMutualGuilds(), i2);
                    if (guild != null) {
                        RelativeLayout relativeLayout2 = a6Var.a;
                        m.checkNotNullExpressionValue(relativeLayout2, "item.root");
                        relativeLayout2.setVisibility(0);
                        if (guild.getIcon() != null) {
                            SimpleDraweeView simpleDraweeView3 = a6Var.f79b;
                            m.checkNotNullExpressionValue(simpleDraweeView3, "item.userProfileAdapterItemFriendMutualImage");
                            simpleDraweeView3.setVisibility(0);
                            SimpleDraweeView simpleDraweeView4 = a6Var.f79b;
                            m.checkNotNullExpressionValue(simpleDraweeView4, "imageView");
                            IconUtils.setIcon$default((ImageView) simpleDraweeView4, guild, 0, (MGImages.ChangeDetector) null, false, 28, (Object) null);
                        } else {
                            TextView textView3 = a6Var.c;
                            m.checkNotNullExpressionValue(textView3, "item.userProfileAdapterItemFriendMutualText");
                            textView3.setVisibility(0);
                            TextView textView4 = a6Var.c;
                            m.checkNotNullExpressionValue(textView4, "item.userProfileAdapterItemFriendMutualText");
                            textView4.setText(guild.getShortName());
                        }
                    }
                }
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public MutualFriendsAdapter(RecyclerView recyclerView, AppFragment appFragment) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recyclerView");
            m.checkNotNullParameter(appFragment, "fragment");
            this.fragment = appFragment;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public MGRecyclerViewHolder<?, Model.Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            if (i == 0) {
                return new WidgetUserProfileEmptyListItem(R.layout.widget_user_profile_adapter_item_empty, this, R.attr.img_no_mutual_friends, R.string.no_mutual_friends);
            }
            if (i == 1) {
                return new ViewHolder(this);
            }
            throw invalidViewTypeException(i);
        }
    }

    public WidgetUserMutualFriends() {
        super(R.layout.widget_user_mutual_friends);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        setActionBarTitle(StringResourceUtilsKt.getQuantityString(resources, (int) R.plurals.user_profile_mutual_friends_count, (int) R.string.mutual_friends, model.getNumMutualFriends(), Integer.valueOf(model.getNumMutualFriends())));
        User user = model.getUser();
        setActionBarSubtitle(user != null ? user.getUsername() : null);
        MutualFriendsAdapter mutualFriendsAdapter = this.adapter;
        if (mutualFriendsAdapter != null) {
            mutualFriendsAdapter.setData(model.getItems());
        }
    }

    private final WidgetUserMutualFriendsBinding getBinding() {
        return (WidgetUserMutualFriendsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final long getUserId() {
        return ((Number) this.userId$delegate.getValue()).longValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2657b;
        m.checkNotNullExpressionValue(recyclerView, "binding.userMutualFriendsRecyclerView");
        this.adapter = (MutualFriendsAdapter) companion.configure(new MutualFriendsAdapter(recyclerView, this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getUserId()), this, null, 2, null), WidgetUserMutualFriends.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserMutualFriends$onViewBoundOrOnResume$1(this));
    }
}
