package com.discord.widgets.user;

import android.view.View;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import b.a.i.a4;
import com.discord.databinding.WidgetUserStatusUpdateBinding;
import com.discord.widgets.user.profile.UserStatusPresenceCustomView;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetUserStatusSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetUserStatusUpdateBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetUserStatusUpdateBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUserStatusSheet$binding$2 extends k implements Function1<View, WidgetUserStatusUpdateBinding> {
    public static final WidgetUserStatusSheet$binding$2 INSTANCE = new WidgetUserStatusSheet$binding$2();

    public WidgetUserStatusSheet$binding$2() {
        super(1, WidgetUserStatusUpdateBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetUserStatusUpdateBinding;", 0);
    }

    public final WidgetUserStatusUpdateBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_actions_overview_header_tv;
        TextView textView = (TextView) view.findViewById(R.id.guild_actions_overview_header_tv);
        if (textView != null) {
            i = R.id.user_status_update_custom;
            UserStatusPresenceCustomView userStatusPresenceCustomView = (UserStatusPresenceCustomView) view.findViewById(R.id.user_status_update_custom);
            if (userStatusPresenceCustomView != null) {
                i = R.id.user_status_update_dnd;
                View findViewById = view.findViewById(R.id.user_status_update_dnd);
                if (findViewById != null) {
                    a4 a = a4.a(findViewById);
                    i = R.id.user_status_update_idle;
                    View findViewById2 = view.findViewById(R.id.user_status_update_idle);
                    if (findViewById2 != null) {
                        a4 a2 = a4.a(findViewById2);
                        i = R.id.user_status_update_invisible;
                        View findViewById3 = view.findViewById(R.id.user_status_update_invisible);
                        if (findViewById3 != null) {
                            a4 a3 = a4.a(findViewById3);
                            i = R.id.user_status_update_online;
                            View findViewById4 = view.findViewById(R.id.user_status_update_online);
                            if (findViewById4 != null) {
                                return new WidgetUserStatusUpdateBinding((NestedScrollView) view, textView, userStatusPresenceCustomView, a, a2, a3, a4.a(findViewById4));
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
