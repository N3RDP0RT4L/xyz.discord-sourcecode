package com.discord.widgets.user;

import com.discord.stores.StoreChat;
import com.discord.widgets.user.WidgetUserMentions;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserMentions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreChat$InteractionState;", "p1", "", "invoke", "(Lcom/discord/stores/StoreChat$InteractionState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUserMentions$onViewBound$2 extends k implements Function1<StoreChat.InteractionState, Unit> {
    public WidgetUserMentions$onViewBound$2(WidgetUserMentions.Model.MessageLoader messageLoader) {
        super(1, messageLoader, WidgetUserMentions.Model.MessageLoader.class, "setInteractionState", "setInteractionState(Lcom/discord/stores/StoreChat$InteractionState;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreChat.InteractionState interactionState) {
        invoke2(interactionState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreChat.InteractionState interactionState) {
        m.checkNotNullParameter(interactionState, "p1");
        ((WidgetUserMentions.Model.MessageLoader) this.receiver).setInteractionState(interactionState);
    }
}
