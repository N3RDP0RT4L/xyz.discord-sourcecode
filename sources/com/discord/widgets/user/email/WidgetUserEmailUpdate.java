package com.discord.widgets.user.email;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.util.Patterns;
import android.view.View;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetUserEmailUpdateBinding;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetUserEmailUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0007¢\u0006\u0004\b\u0010\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\b\u0010\tR\u001d\u0010\u000f\u001a\u00020\n8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/user/email/WidgetUserEmailUpdate;", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;", "", "handleTextChanged", "()V", "onEmailUpdated", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetUserEmailUpdateBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserEmailUpdateBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserEmailUpdate extends WidgetUserAccountVerifyBase {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserEmailUpdate.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserEmailUpdateBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserEmailUpdate$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetUserEmailUpdate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0010\b\u0002\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006¢\u0006\u0004\b\n\u0010\u000bJ)\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\r\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\u000e¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/user/email/WidgetUserEmailUpdate$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "mode", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "launcher", "", "launch", "(Landroid/content/Context;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;Landroidx/activity/result/ActivityResultLauncher;)V", "Lcom/discord/app/AppFragment;", "fragment", "Lkotlin/Function0;", "callback", "registerForResult", "(Lcom/discord/app/AppFragment;Lkotlin/jvm/functions/Function0;)Landroidx/activity/result/ActivityResultLauncher;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ void launch$default(Companion companion, Context context, WidgetUserAccountVerifyBase.Mode mode, ActivityResultLauncher activityResultLauncher, int i, Object obj) {
            if ((i & 4) != 0) {
                activityResultLauncher = null;
            }
            companion.launch(context, mode, activityResultLauncher);
        }

        public final void launch(Context context, WidgetUserAccountVerifyBase.Mode mode, ActivityResultLauncher<Intent> activityResultLauncher) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(mode, "mode");
            Intent launchIntent = WidgetUserAccountVerifyBase.Companion.getLaunchIntent(mode, false, true);
            if (activityResultLauncher != null) {
                j.g.f(context, activityResultLauncher, WidgetUserEmailUpdate.class, launchIntent);
            } else {
                j.d(context, WidgetUserEmailUpdate.class, launchIntent);
            }
        }

        public final ActivityResultLauncher<Intent> registerForResult(AppFragment appFragment, final Function0<Unit> function0) {
            m.checkNotNullParameter(appFragment, "fragment");
            m.checkNotNullParameter(function0, "callback");
            ActivityResultLauncher<Intent> registerForActivityResult = appFragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.user.email.WidgetUserEmailUpdate$Companion$registerForResult$1
                public final void onActivityResult(ActivityResult activityResult) {
                    m.checkNotNullExpressionValue(activityResult, "activityResult");
                    if (activityResult.getResultCode() == -1) {
                        Function0.this.invoke();
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…k()\n          }\n        }");
            return registerForActivityResult;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetUserEmailUpdate() {
        super(R.layout.widget_user_email_update);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetUserEmailUpdateBinding getBinding() {
        return (WidgetUserEmailUpdateBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleTextChanged() {
        boolean z2;
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.alertVerifyEmailChangePassword");
        if (ViewExtensions.getTextOrEmpty(textInputLayout).length() >= 3) {
            Pattern pattern = Patterns.EMAIL_ADDRESS;
            TextInputLayout textInputLayout2 = getBinding().c;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.alertVerifyEmailChangeEmail");
            if (pattern.matcher(ViewExtensions.getTextOrEmpty(textInputLayout2)).matches()) {
                z2 = true;
                MaterialButton materialButton = getBinding().f2653b;
                m.checkNotNullExpressionValue(materialButton, "binding.alertVerifyEmailChange");
                materialButton.setEnabled(z2);
            }
        }
        z2 = false;
        MaterialButton materialButton2 = getBinding().f2653b;
        m.checkNotNullExpressionValue(materialButton2, "binding.alertVerifyEmailChange");
        materialButton2.setEnabled(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onEmailUpdated() {
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.setResult(-1);
        }
        AppActivity appActivity2 = getAppActivity();
        if (appActivity2 != null) {
            appActivity2.finish();
        }
    }

    @Override // com.discord.widgets.user.account.WidgetUserAccountVerifyBase, com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.alertVerifyEmailChangeEmail");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetUserEmailUpdate$onViewBound$1(this));
        TextInputLayout textInputLayout2 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.alertVerifyEmailChangePassword");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, new WidgetUserEmailUpdate$onViewBound$2(this));
        getBinding().f2653b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.email.WidgetUserEmailUpdate$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserEmailUpdateBinding binding;
                WidgetUserEmailUpdateBinding binding2;
                WidgetUserEmailUpdateBinding binding3;
                RestAPI api = RestAPI.Companion.getApi();
                RestAPIParams.UserInfo.Companion companion = RestAPIParams.UserInfo.Companion;
                binding = WidgetUserEmailUpdate.this.getBinding();
                TextInputLayout textInputLayout3 = binding.c;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.alertVerifyEmailChangeEmail");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout3);
                binding2 = WidgetUserEmailUpdate.this.getBinding();
                TextInputLayout textInputLayout4 = binding2.d;
                m.checkNotNullExpressionValue(textInputLayout4, "binding.alertVerifyEmailChangePassword");
                Observable restSubscribeOn$default = ObservableExtensionsKt.restSubscribeOn$default(api.patchUser(companion.createForEmail(textOrEmpty, ViewExtensions.getTextOrEmpty(textInputLayout4))), false, 1, null);
                binding3 = WidgetUserEmailUpdate.this.getBinding();
                ObservableExtensionsKt.ui$default(ObservableExtensionsKt.withDimmer$default(restSubscribeOn$default, binding3.e, 0L, 2, null), WidgetUserEmailUpdate.this, null, 2, null).k(o.i(new Action1<User>() { // from class: com.discord.widgets.user.email.WidgetUserEmailUpdate$onViewBound$3.1
                    public final void call(User user) {
                        WidgetUserEmailUpdate.this.onEmailUpdated();
                    }
                }, WidgetUserEmailUpdate.this));
            }
        });
    }
}
