package com.discord.widgets.user.email;

import androidx.core.app.NotificationCompat;
import com.discord.models.user.MeUser;
import com.discord.widgets.user.email.WidgetUserEmailVerify;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: WidgetUserEmailVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "me", "Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/user/MeUser;)Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserEmailVerify$Model$Companion$get$1<T, R> implements b<MeUser, WidgetUserEmailVerify.Model> {
    public static final WidgetUserEmailVerify$Model$Companion$get$1 INSTANCE = new WidgetUserEmailVerify$Model$Companion$get$1();

    public final WidgetUserEmailVerify.Model call(MeUser meUser) {
        m.checkNotNullExpressionValue(meUser, "me");
        return new WidgetUserEmailVerify.Model(meUser);
    }
}
