package com.discord.widgets.user.search;

import android.annotation.SuppressLint;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
/* compiled from: WidgetGlobalSearchModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u000b¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/models/user/User;", "", "userFilter", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;", "invoke", "(Lcom/discord/models/user/User;Ljava/lang/String;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;", "toItemUser"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearchModel$Companion$create$2 extends o implements Function3<User, String, Channel, WidgetGlobalSearchModel.ItemUser> {
    public final /* synthetic */ WidgetGlobalSearchModel.ChannelContext $channelContext;
    public final /* synthetic */ WidgetGlobalSearchModel.SearchContext $searchContext;
    public final /* synthetic */ WidgetGlobalSearchModel.UsersContext $usersContext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGlobalSearchModel$Companion$create$2(WidgetGlobalSearchModel.UsersContext usersContext, WidgetGlobalSearchModel.SearchContext searchContext, WidgetGlobalSearchModel.ChannelContext channelContext) {
        super(3);
        this.$usersContext = usersContext;
        this.$searchContext = searchContext;
        this.$channelContext = channelContext;
    }

    public static /* synthetic */ WidgetGlobalSearchModel.ItemUser invoke$default(WidgetGlobalSearchModel$Companion$create$2 widgetGlobalSearchModel$Companion$create$2, User user, String str, Channel channel, int i, Object obj) {
        if ((i & 2) != 0) {
            channel = null;
        }
        return widgetGlobalSearchModel$Companion$create$2.invoke(user, str, channel);
    }

    @SuppressLint({"DefaultLocale"})
    public final WidgetGlobalSearchModel.ItemUser invoke(User user, String str, Channel channel) {
        Integer num;
        m.checkNotNullParameter(user, "$this$toItemUser");
        m.checkNotNullParameter(str, "userFilter");
        Collection<Map<Long, GuildMember>> values = this.$usersContext.getMembers().values();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (true) {
            String str2 = null;
            if (!it.hasNext()) {
                break;
            }
            GuildMember guildMember = (GuildMember) a.e(user, (Map) it.next());
            if (guildMember != null) {
                str2 = guildMember.getNick();
            }
            if (str2 != null) {
                arrayList.add(str2);
            }
        }
        List<String> distinct = u.distinct(arrayList);
        ArrayList<WidgetGlobalSearchModel.MatchedResult> arrayList2 = new ArrayList();
        for (String str3 : distinct) {
            WidgetGlobalSearchModel.MatchedResult matchedResult = WidgetGlobalSearchModel.Companion.toMatchedResult(str3, str);
            if (matchedResult != null) {
                arrayList2.add(matchedResult);
            }
        }
        ArrayList<WidgetGlobalSearchModel.MatchedResult> arrayList3 = new ArrayList();
        for (String str4 : distinct) {
            WidgetGlobalSearchModel.MatchedResult fuzzyMatchedResult = WidgetGlobalSearchModel.Companion.toFuzzyMatchedResult(str4, str);
            if (fuzzyMatchedResult != null) {
                arrayList3.add(fuzzyMatchedResult);
            }
        }
        String username = this.$searchContext.getHasDiscriminator() ? user.getUsername() + UserUtils.INSTANCE.getDiscriminatorWithPadding(user) : user.getUsername();
        WidgetGlobalSearchModel.Companion companion = WidgetGlobalSearchModel.Companion;
        WidgetGlobalSearchModel.MatchedResult matchedResult2 = companion.toMatchedResult(username, str);
        WidgetGlobalSearchModel.MatchedResult fuzzyMatchedResult2 = companion.toFuzzyMatchedResult(username, str);
        if (matchedResult2 == null) {
            if (!arrayList2.isEmpty()) {
                fuzzyMatchedResult2 = (WidgetGlobalSearchModel.MatchedResult) u.first((List<? extends Object>) arrayList2);
            } else if (fuzzyMatchedResult2 == null) {
                if (!arrayList3.isEmpty()) {
                    fuzzyMatchedResult2 = (WidgetGlobalSearchModel.MatchedResult) u.first((List<? extends Object>) arrayList3);
                } else {
                    matchedResult2 = null;
                }
            }
            matchedResult2 = fuzzyMatchedResult2;
        }
        if (matchedResult2 == null) {
            return null;
        }
        Integer num2 = (Integer) a.e(user, this.$usersContext.getRelationships());
        boolean z2 = num2 != null && num2.intValue() == 1;
        List listOf = d0.t.m.listOf(user.getUsername() + UserUtils.INSTANCE.getDiscriminatorWithPadding(user));
        ArrayList arrayList4 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList2, 10));
        for (WidgetGlobalSearchModel.MatchedResult matchedResult3 : arrayList2) {
            arrayList4.add(matchedResult3.getValue());
        }
        List plus = u.plus((Collection) listOf, (Iterable) arrayList4);
        ArrayList arrayList5 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList3, 10));
        for (WidgetGlobalSearchModel.MatchedResult matchedResult4 : arrayList3) {
            arrayList5.add(matchedResult4.getValue());
        }
        return new WidgetGlobalSearchModel.ItemUser(matchedResult2, user, u.distinct(u.plus((Collection) plus, (Iterable) arrayList5)), z2, (Presence) a.e(user, this.$usersContext.getPresences()), channel, (channel == null || (num = (Integer) a.c(channel, this.$searchContext.getMentionCounts())) == null) ? 0 : num.intValue(), channel != null ? this.$channelContext.getUnreadChannelIds().contains(Long.valueOf(channel.h())) : false);
    }
}
