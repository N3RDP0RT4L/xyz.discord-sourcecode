package com.discord.widgets.user.search;

import andhook.lib.HookHelper;
import com.discord.api.presence.ClientStatus;
import com.discord.models.guild.Guild;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import kotlin.Metadata;
/* compiled from: WidgetGlobalSearchScoreStrategy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u001eB\t\b\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ+\u0010\t\u001a\u00020\u0003*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0010\u0010\b\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u0005¢\u0006\u0004\b\t\u0010\nJ'\u0010\f\u001a\u00020\u0003*\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u00052\n\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000f\u001a\u00020\u0003*\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0011\u0010\u0012\u001a\u00020\u0003*\u00020\u0011¢\u0006\u0004\b\u0012\u0010\u0013J\u0011\u0010\u0015\u001a\u00020\u0003*\u00020\u0014¢\u0006\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0018R\u0016\u0010\u001b\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0018¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchScoreStrategy;", "", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "", "searchType", "", "", "Lcom/discord/primitives/ChannelId;", "frecencyChannels", "score", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;ILjava/util/Collection;)I", "channelId", "scoreFrecency", "(Ljava/util/Collection;J)I", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;", "scoreUser", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;I)I", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;", "scoreChannel", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;)I", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "scoreMatchedResult", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;)I", "FUZZY_MATCH_SCORE_BASE", "I", "MENTIONS_MATCH_SCORE", "FRECENCY_PRI_LIMIT", "FUZZY_MATCH_SCORE_MAX_DEDUCTION", HookHelper.constructorName, "()V", "SearchType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearchScoreStrategy {
    public static final int FRECENCY_PRI_LIMIT = 50;
    private static final int FUZZY_MATCH_SCORE_BASE = -100;
    private static final int FUZZY_MATCH_SCORE_MAX_DEDUCTION = 20;
    public static final WidgetGlobalSearchScoreStrategy INSTANCE = new WidgetGlobalSearchScoreStrategy();
    private static final int MENTIONS_MATCH_SCORE = 100;

    /* compiled from: WidgetGlobalSearchScoreStrategy.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006j\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchScoreStrategy$SearchType;", "", "", "friendWeight", "I", "getFriendWeight", "()I", "dmChannelWeight", "getDmChannelWeight", HookHelper.constructorName, "(Ljava/lang/String;III)V", "USER", "NONE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum SearchType {
        USER(10, 5),
        NONE(1, 1);
        
        private final int dmChannelWeight;
        private final int friendWeight;

        SearchType(int i, int i2) {
            this.friendWeight = i;
            this.dmChannelWeight = i2;
        }

        public final int getDmChannelWeight() {
            return this.dmChannelWeight;
        }

        public final int getFriendWeight() {
            return this.friendWeight;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ClientStatus.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[ClientStatus.ONLINE.ordinal()] = 1;
            iArr[ClientStatus.IDLE.ordinal()] = 2;
            iArr[ClientStatus.DND.ordinal()] = 3;
        }
    }

    private WidgetGlobalSearchScoreStrategy() {
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x003a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int score(com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload r3, int r4, java.util.Collection<java.lang.Long> r5) {
        /*
            r2 = this;
            java.lang.String r0 = "$this$score"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "frecencyChannels"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            com.discord.api.channel.Channel r0 = r3.getChannel()
            if (r0 == 0) goto L15
            long r0 = r0.h()
            goto L17
        L15:
            r0 = 0
        L17:
            int r5 = r2.scoreFrecency(r5, r0)
            boolean r0 = r3 instanceof com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemUser
            if (r0 == 0) goto L28
            r0 = r3
            com.discord.widgets.user.search.WidgetGlobalSearchModel$ItemUser r0 = (com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemUser) r0
            int r4 = r2.scoreUser(r0, r4)
        L26:
            int r5 = r5 + r4
            goto L34
        L28:
            boolean r4 = r3 instanceof com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemChannel
            if (r4 == 0) goto L34
            r4 = r3
            com.discord.widgets.user.search.WidgetGlobalSearchModel$ItemChannel r4 = (com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemChannel) r4
            int r4 = r2.scoreChannel(r4)
            goto L26
        L34:
            int r4 = r3.getMentions()
            if (r4 <= 0) goto L41
            int r4 = r3.getMentions()
            int r4 = r4 + 100
            int r5 = r5 + r4
        L41:
            com.discord.widgets.user.search.WidgetGlobalSearchModel$MatchedResult r3 = r3.getMatchedResult()
            int r3 = r2.scoreMatchedResult(r3)
            int r3 = r3 + r5
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.search.WidgetGlobalSearchScoreStrategy.score(com.discord.widgets.user.search.WidgetGlobalSearchModel$ItemDataPayload, int, java.util.Collection):int");
    }

    public final int scoreChannel(WidgetGlobalSearchModel.ItemChannel itemChannel) {
        m.checkNotNullParameter(itemChannel, "$this$scoreChannel");
        Guild guild = itemChannel.getGuild();
        return (guild != null ? guild.getMemberCount() : 0) > 200 ? -5 : 0;
    }

    public final int scoreFrecency(Collection<Long> collection, long j) {
        m.checkNotNullParameter(collection, "$this$scoreFrecency");
        int indexOf = u.indexOf(collection, Long.valueOf(j));
        if (indexOf == -1) {
            return 0;
        }
        if (indexOf >= 0 && 5 > indexOf) {
            return 60 - indexOf;
        }
        if (5 <= indexOf && 50 > indexOf) {
            return 55 - indexOf;
        }
        return 2;
    }

    public final int scoreMatchedResult(WidgetGlobalSearchModel.MatchedResult matchedResult) {
        int i;
        m.checkNotNullParameter(matchedResult, "$this$scoreMatchedResult");
        int firstMatchIndex = matchedResult.getFirstMatchIndex();
        int i2 = 0;
        if (firstMatchIndex == -1) {
            i = (-100) - Math.min(matchedResult.getValue().length(), 20);
        } else {
            i = (firstMatchIndex >= 0 && 10 >= firstMatchIndex) ? 15 - matchedResult.getFirstMatchIndex() : 0;
        }
        if (matchedResult.getFirstMatchIndex() == 0) {
            i2 = 2;
        } else if (matchedResult.getFirstMatchIndex() > 0 && !Character.isLetterOrDigit(matchedResult.getValue().charAt(matchedResult.getFirstMatchIndex() - 1))) {
            i2 = 1;
        }
        return i + i2;
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0035  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x003a  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0047  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0060  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int scoreUser(com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemUser r5, int r6) {
        /*
            r4 = this;
            java.lang.String r0 = "$this$scoreUser"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            r0 = 1
            if (r6 == r0) goto Lb
            com.discord.widgets.user.search.WidgetGlobalSearchScoreStrategy$SearchType r6 = com.discord.widgets.user.search.WidgetGlobalSearchScoreStrategy.SearchType.NONE
            goto Ld
        Lb:
            com.discord.widgets.user.search.WidgetGlobalSearchScoreStrategy$SearchType r6 = com.discord.widgets.user.search.WidgetGlobalSearchScoreStrategy.SearchType.USER
        Ld:
            com.discord.models.presence.Presence r1 = r5.getPresence()
            if (r1 == 0) goto L18
            com.discord.api.presence.ClientStatus r1 = r1.getStatus()
            goto L19
        L18:
            r1 = 0
        L19:
            r2 = 2
            r3 = 0
            if (r1 != 0) goto L1e
            goto L28
        L1e:
            int r1 = r1.ordinal()
            if (r1 == 0) goto L2e
            if (r1 == r0) goto L2c
            if (r1 == r2) goto L2a
        L28:
            r0 = 0
            goto L2f
        L2a:
            r0 = 2
            goto L2f
        L2c:
            r0 = 3
            goto L2f
        L2e:
            r0 = 4
        L2f:
            boolean r1 = r5.isFriend()
            if (r1 == 0) goto L3a
            int r1 = r6.getFriendWeight()
            goto L3b
        L3a:
            r1 = 0
        L3b:
            int r0 = r0 + r1
            com.discord.api.channel.Channel r1 = r5.getChannel()
            if (r1 == 0) goto L47
            int r6 = r6.getDmChannelWeight()
            goto L48
        L47:
            r6 = 0
        L48:
            int r0 = r0 + r6
            com.discord.widgets.user.search.WidgetGlobalSearchModel$MatchedResult r6 = r5.getMatchedResult()
            java.lang.CharSequence r6 = r6.getValue()
            com.discord.models.user.User r5 = r5.getUser()
            java.lang.String r5 = r5.getUsername()
            boolean r5 = d0.z.d.m.areEqual(r6, r5)
            if (r5 == 0) goto L60
            goto L61
        L60:
            r2 = 0
        L61:
            int r0 = r0 + r2
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.search.WidgetGlobalSearchScoreStrategy.scoreUser(com.discord.widgets.user.search.WidgetGlobalSearchModel$ItemUser, int):int");
    }
}
