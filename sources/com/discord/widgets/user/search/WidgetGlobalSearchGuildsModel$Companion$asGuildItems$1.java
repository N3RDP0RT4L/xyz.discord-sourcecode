package com.discord.widgets.user.search;

import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.widgets.user.search.WidgetGlobalSearchGuildsModel;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGlobalSearchGuildsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "invoke", "(Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearchGuildsModel$Companion$asGuildItems$1 extends o implements Function1<Guild, WidgetGlobalSearchGuildsModel.Item> {
    public final /* synthetic */ Map $channelIds;
    public final /* synthetic */ Map $guildSettings;
    public final /* synthetic */ Map $mentionCounts;
    public final /* synthetic */ Long $selectedGuildId;
    public final /* synthetic */ long $selectedVoiceChannelId;
    public final /* synthetic */ Set $unreadGuildIds;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGlobalSearchGuildsModel$Companion$asGuildItems$1(Map map, Map map2, Map map3, Set set, Long l, long j) {
        super(1);
        this.$channelIds = map;
        this.$mentionCounts = map2;
        this.$guildSettings = map3;
        this.$unreadGuildIds = set;
        this.$selectedGuildId = l;
        this.$selectedVoiceChannelId = j;
    }

    public final WidgetGlobalSearchGuildsModel.Item invoke(Guild guild) {
        int i;
        boolean z2;
        boolean z3;
        m.checkNotNullParameter(guild, "guild");
        long id2 = guild.getId();
        List<Number> list = (List) this.$channelIds.get(Long.valueOf(id2));
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (Number number : list) {
                Integer num = (Integer) this.$mentionCounts.get(Long.valueOf(number.longValue()));
                if (num != null) {
                    arrayList.add(num);
                }
            }
            i = u.sumOfInt(arrayList);
        } else {
            i = 0;
        }
        ModelNotificationSettings modelNotificationSettings = (ModelNotificationSettings) this.$guildSettings.get(Long.valueOf(id2));
        boolean contains = (modelNotificationSettings == null || !modelNotificationSettings.isMuted()) ? this.$unreadGuildIds.contains(Long.valueOf(id2)) : false;
        Long l = this.$selectedGuildId;
        boolean z4 = (l == null || l == null || id2 != l.longValue()) ? false : true;
        if (!z4 && this.$selectedVoiceChannelId > 0 && list != null && !list.isEmpty()) {
            for (Number number2 : list) {
                if (this.$selectedVoiceChannelId == number2.longValue()) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        return WidgetGlobalSearchGuildsModel.Item.Companion.createGuild$app_productionGoogleRelease(guild, i, contains, z4, z2);
    }
}
