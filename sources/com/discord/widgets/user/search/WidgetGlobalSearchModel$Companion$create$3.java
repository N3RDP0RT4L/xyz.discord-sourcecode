package com.discord.widgets.user.search;

import b.d.b.a.a;
import com.discord.models.guild.Guild;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetGlobalSearchModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/guild/Guild;", "", "guildFilter", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;", "invoke", "(Lcom/discord/models/guild/Guild;Ljava/lang/String;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;", "toItemGuild"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearchModel$Companion$create$3 extends o implements Function2<Guild, String, WidgetGlobalSearchModel.ItemGuild> {
    public final /* synthetic */ WidgetGlobalSearchModel.ChannelContext $channelContext;
    public final /* synthetic */ WidgetGlobalSearchModel.SearchContext $searchContext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGlobalSearchModel$Companion$create$3(WidgetGlobalSearchModel.ChannelContext channelContext, WidgetGlobalSearchModel.SearchContext searchContext) {
        super(2);
        this.$channelContext = channelContext;
        this.$searchContext = searchContext;
    }

    public final WidgetGlobalSearchModel.ItemGuild invoke(Guild guild, String str) {
        int i;
        boolean z2;
        m.checkNotNullParameter(guild, "$this$toItemGuild");
        m.checkNotNullParameter(str, "guildFilter");
        WidgetGlobalSearchModel.MatchedResult matchedResult = WidgetGlobalSearchModel.Companion.toMatchedResult(guild.getName(), str);
        if (matchedResult == null) {
            return null;
        }
        List<Number> list = (List) a.d(guild, this.$channelContext.getGuildToChannels());
        if (list != null) {
            int i2 = 0;
            for (Number number : list) {
                Integer num = this.$searchContext.getMentionCounts().get(Long.valueOf(number.longValue()));
                i2 += num != null ? num.intValue() : 0;
            }
            i = i2;
        } else {
            i = 0;
        }
        if (list != null && !list.isEmpty()) {
            for (Number number2 : list) {
                if (this.$channelContext.getUnreadChannelIds().contains(Long.valueOf(number2.longValue()))) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        return new WidgetGlobalSearchModel.ItemGuild(matchedResult, guild, null, i, z2, 4, null);
    }
}
