package com.discord.widgets.user.search;

import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.widgets.tabs.NavigationTab;
import com.discord.widgets.user.search.WidgetGlobalSearchGuildsModel;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
/* compiled from: WidgetGlobalSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "viewType", "index", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "selected", "", "invoke", "(IILcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearch$configureUI$2 extends o implements Function3<Integer, Integer, WidgetGlobalSearchGuildsModel.Item, Unit> {
    public final /* synthetic */ WidgetGlobalSearchModel $model;
    public final /* synthetic */ WidgetGlobalSearch this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGlobalSearch$configureUI$2(WidgetGlobalSearch widgetGlobalSearch, WidgetGlobalSearchModel widgetGlobalSearchModel) {
        super(3);
        this.this$0 = widgetGlobalSearch;
        this.$model = widgetGlobalSearchModel;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Unit invoke(Integer num, Integer num2, WidgetGlobalSearchGuildsModel.Item item) {
        invoke(num.intValue(), num2.intValue(), item);
        return Unit.a;
    }

    public final void invoke(int i, int i2, WidgetGlobalSearchGuildsModel.Item item) {
        WidgetGlobalSearchModel.ItemDataPayload widgetGlobalSearchModelItem;
        m.checkNotNullParameter(item, "selected");
        if (i == 2) {
            ChannelSelector.Companion.getInstance().selectChannel(0L, item.getId(), (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null);
        } else if (i == 3) {
            StoreStream.Companion.getGuildSelected().set(item.getId());
        }
        widgetGlobalSearchModelItem = WidgetGlobalSearch.Companion.toWidgetGlobalSearchModelItem(item);
        if (widgetGlobalSearchModelItem != null) {
            AnalyticsTracker.INSTANCE.quickSwitcherSelect(this.$model, widgetGlobalSearchModelItem, i2);
        }
        this.this$0.onSelected(NavigationTab.HOME, StoreNavigation.PanelAction.OPEN);
    }
}
