package com.discord.widgets.user.search;

import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGlobalSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "p1", "", "invoke", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGlobalSearch$onViewBoundOrOnResume$1 extends k implements Function1<WidgetGlobalSearchModel, Unit> {
    public WidgetGlobalSearch$onViewBoundOrOnResume$1(WidgetGlobalSearch widgetGlobalSearch) {
        super(1, widgetGlobalSearch, WidgetGlobalSearch.class, "configureUI", "configureUI(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetGlobalSearchModel widgetGlobalSearchModel) {
        invoke2(widgetGlobalSearchModel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetGlobalSearchModel widgetGlobalSearchModel) {
        m.checkNotNullParameter(widgetGlobalSearchModel, "p1");
        ((WidgetGlobalSearch) this.receiver).configureUI(widgetGlobalSearchModel);
    }
}
