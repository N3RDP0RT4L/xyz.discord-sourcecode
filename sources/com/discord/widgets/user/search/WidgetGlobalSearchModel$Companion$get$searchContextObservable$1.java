package com.discord.widgets.user.search;

import androidx.core.app.NotificationCompat;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: WidgetGlobalSearchModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "kotlin.jvm.PlatformType", "filterStr", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;", "partialSearchContext", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/String;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearchModel$Companion$get$searchContextObservable$1<T1, T2, R> implements Func2<String, WidgetGlobalSearchModel.SearchContext, WidgetGlobalSearchModel.SearchContext> {
    public static final WidgetGlobalSearchModel$Companion$get$searchContextObservable$1 INSTANCE = new WidgetGlobalSearchModel$Companion$get$searchContextObservable$1();

    public final WidgetGlobalSearchModel.SearchContext call(String str, WidgetGlobalSearchModel.SearchContext searchContext) {
        WidgetGlobalSearchModel.SearchContext copy;
        m.checkNotNullExpressionValue(str, "filterStr");
        copy = searchContext.copy((r22 & 1) != 0 ? searchContext.filter : str, (r22 & 2) != 0 ? searchContext.recentGuildIds : null, (r22 & 4) != 0 ? searchContext.selectedChannelId : 0L, (r22 & 8) != 0 ? searchContext.prevSelectedChannelId : 0L, (r22 & 16) != 0 ? searchContext.selectedVoiceChannelId : 0L, (r22 & 32) != 0 ? searchContext.mostRecent : null, (r22 & 64) != 0 ? searchContext.mentionCounts : null);
        return copy;
    }
}
