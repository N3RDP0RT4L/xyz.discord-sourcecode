package com.discord.widgets.user.search;

import androidx.fragment.app.FragmentManager;
import com.discord.models.guild.Guild;
import com.discord.widgets.channels.list.WidgetChannelsListItemChannelActions;
import com.discord.widgets.guilds.profile.WidgetGuildProfileSheet;
import com.discord.widgets.user.search.WidgetGlobalSearchGuildsModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
/* compiled from: WidgetGlobalSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "viewType", "<anonymous parameter 1>", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "data", "", "invoke", "(IILcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearch$configureUI$1 extends o implements Function3<Integer, Integer, WidgetGlobalSearchGuildsModel.Item, Unit> {
    public final /* synthetic */ WidgetGlobalSearch this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGlobalSearch$configureUI$1(WidgetGlobalSearch widgetGlobalSearch) {
        super(3);
        this.this$0 = widgetGlobalSearch;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Unit invoke(Integer num, Integer num2, WidgetGlobalSearchGuildsModel.Item item) {
        invoke(num.intValue(), num2.intValue(), item);
        return Unit.a;
    }

    public final void invoke(int i, int i2, WidgetGlobalSearchGuildsModel.Item item) {
        m.checkNotNullParameter(item, "data");
        if (i == 2) {
            WidgetChannelsListItemChannelActions.Companion companion = WidgetChannelsListItemChannelActions.Companion;
            FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(parentFragmentManager, item.getId());
        } else if (i == 3) {
            WidgetGuildProfileSheet.Companion companion2 = WidgetGuildProfileSheet.Companion;
            FragmentManager parentFragmentManager2 = this.this$0.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            Guild guild = item.getGuild();
            WidgetGuildProfileSheet.Companion.show$default(companion2, parentFragmentManager2, false, guild != null ? guild.getId() : 0L, 0L, false, 24, null);
        }
    }
}
