package com.discord.widgets.user.search;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.databinding.WidgetGlobalSearchItemGuildBinding;
import com.discord.databinding.WidgetGuildsListItemDmBinding;
import com.discord.databinding.WidgetGuildsListItemGuildBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.extensions.SimpleDraweeViewExtensionsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.user.search.WidgetGlobalSearchGuildsAdapter;
import com.discord.widgets.user.search.WidgetGlobalSearchGuildsModel;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import xyz.discord.R;
/* compiled from: WidgetGlobalSearchGuildsAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u001b\u001c\u001d\u001eB\u0019\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\b\b\u0001\u0010\u0012\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR:\u0010\f\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R:\u0010\u0014\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0015\u0010\u000f\"\u0004\b\u0016\u0010\u0011¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function3;", "", "onClickListener", "Lkotlin/jvm/functions/Function3;", "getOnClickListener", "()Lkotlin/jvm/functions/Function3;", "setOnClickListener", "(Lkotlin/jvm/functions/Function3;)V", "overlayColor", "I", "onLongClickListener", "getOnLongClickListener", "setOnLongClickListener", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;I)V", "Item", "ItemDirectMessage", "ItemDivider", "ItemGuild", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearchGuildsAdapter extends MGRecyclerAdapterSimple<WidgetGlobalSearchGuildsModel.Item> {
    private Function3<? super Integer, ? super Integer, ? super WidgetGlobalSearchGuildsModel.Item, Unit> onClickListener = WidgetGlobalSearchGuildsAdapter$onClickListener$1.INSTANCE;
    private Function3<? super Integer, ? super Integer, ? super WidgetGlobalSearchGuildsModel.Item, Unit> onLongClickListener = WidgetGlobalSearchGuildsAdapter$onLongClickListener$1.INSTANCE;
    private final int overlayColor;

    /* compiled from: WidgetGlobalSearchGuildsAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b&\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\u000f\u001a\u00020\u0004\u0012\u0006\u0010\u0010\u001a\u00020\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\r\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\u0004H\u0004¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;)V", "Landroid/widget/TextView;", "textView", "count", "configureMentionsCount", "(Landroid/widget/TextView;I)V", "layoutRes", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item extends MGRecyclerViewHolder<WidgetGlobalSearchGuildsAdapter, WidgetGlobalSearchGuildsModel.Item> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Item(@LayoutRes int i, WidgetGlobalSearchGuildsAdapter widgetGlobalSearchGuildsAdapter) {
            super(i, widgetGlobalSearchGuildsAdapter);
            m.checkNotNullParameter(widgetGlobalSearchGuildsAdapter, "adapter");
        }

        public static final /* synthetic */ WidgetGlobalSearchGuildsAdapter access$getAdapter$p(Item item) {
            return (WidgetGlobalSearchGuildsAdapter) item.adapter;
        }

        public final void configureMentionsCount(TextView textView, int i) {
            CharSequence b2;
            m.checkNotNullParameter(textView, "textView");
            if (i < 1) {
                ViewExtensions.setTextAndVisibilityBy(textView, null);
                textView.setContentDescription(null);
                return;
            }
            textView.setVisibility(0);
            textView.setText(String.valueOf(i));
            Context context = textView.getContext();
            m.checkNotNullExpressionValue(context, "context");
            textView.setBackgroundResource(DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.overlay_guild_mentions_primary_630, 0, 2, (Object) null));
            b2 = b.b(context, R.string.mentions_count, new Object[]{String.valueOf(i)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            textView.setContentDescription(b2);
        }

        public void onConfigure(final int i, final WidgetGlobalSearchGuildsModel.Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchGuildsAdapter$Item$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetGlobalSearchGuildsAdapter.Item.access$getAdapter$p(WidgetGlobalSearchGuildsAdapter.Item.this).getOnClickListener().invoke(Integer.valueOf(WidgetGlobalSearchGuildsAdapter.Item.this.getItemViewType()), Integer.valueOf(i), item);
                }
            });
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            ViewExtensions.setOnLongClickListenerConsumeClick(view, new WidgetGlobalSearchGuildsAdapter$Item$onConfigure$2(this, i, item));
        }
    }

    /* compiled from: WidgetGlobalSearchGuildsAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$ItemDirectMessage;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;)V", "Lcom/discord/databinding/WidgetGuildsListItemDmBinding;", "binding", "Lcom/discord/databinding/WidgetGuildsListItemDmBinding;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;", "adapter", "layoutResId", HookHelper.constructorName, "(Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemDirectMessage extends Item {
        private final WidgetGuildsListItemDmBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemDirectMessage(WidgetGlobalSearchGuildsAdapter widgetGlobalSearchGuildsAdapter, int i) {
            super(i, widgetGlobalSearchGuildsAdapter);
            m.checkNotNullParameter(widgetGlobalSearchGuildsAdapter, "adapter");
            WidgetGuildsListItemDmBinding a = WidgetGuildsListItemDmBinding.a(this.itemView);
            m.checkNotNullExpressionValue(a, "WidgetGuildsListItemDmBinding.bind(itemView)");
            this.binding = a;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.widgets.user.search.WidgetGlobalSearchGuildsAdapter.Item
        public void onConfigure(int i, WidgetGlobalSearchGuildsModel.Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, item);
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.guildsItemDmCount");
            configureMentionsCount(textView, item.getMentionCount());
            SimpleDraweeView simpleDraweeView = this.binding.f2436b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildsItemDmAvatar");
            IconUtils.setIcon$default(simpleDraweeView, item.getChannel(), (int) R.dimen.avatar_size_large, (MGImages.ChangeDetector) null, 8, (Object) null);
        }
    }

    /* compiled from: WidgetGlobalSearchGuildsAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$ItemDivider;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;", "adapter", "", "layoutResId", HookHelper.constructorName, "(Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemDivider extends Item {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemDivider(WidgetGlobalSearchGuildsAdapter widgetGlobalSearchGuildsAdapter, int i) {
            super(i, widgetGlobalSearchGuildsAdapter);
            m.checkNotNullParameter(widgetGlobalSearchGuildsAdapter, "adapter");
        }
    }

    /* compiled from: WidgetGlobalSearchGuildsAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u000f\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$ItemGuild;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;)V", "Lcom/discord/databinding/WidgetGuildsListItemGuildBinding;", "bindingGuild", "Lcom/discord/databinding/WidgetGuildsListItemGuildBinding;", "Lcom/discord/databinding/WidgetGlobalSearchItemGuildBinding;", "binding", "Lcom/discord/databinding/WidgetGlobalSearchItemGuildBinding;", "overlayColor", "I", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;", "adapter", "layoutResId", HookHelper.constructorName, "(Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemGuild extends Item {
        private final WidgetGlobalSearchItemGuildBinding binding;
        private final WidgetGuildsListItemGuildBinding bindingGuild;
        private final int overlayColor;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemGuild(WidgetGlobalSearchGuildsAdapter widgetGlobalSearchGuildsAdapter, int i, @ColorInt int i2) {
            super(i, widgetGlobalSearchGuildsAdapter);
            m.checkNotNullParameter(widgetGlobalSearchGuildsAdapter, "adapter");
            this.overlayColor = i2;
            View view = this.itemView;
            int i3 = R.id.guilds_item_selected;
            ImageView imageView = (ImageView) view.findViewById(R.id.guilds_item_selected);
            if (imageView != null) {
                i3 = R.id.guilds_item_unread;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.guilds_item_unread);
                if (imageView2 != null) {
                    WidgetGlobalSearchItemGuildBinding widgetGlobalSearchItemGuildBinding = new WidgetGlobalSearchItemGuildBinding((RelativeLayout) view, imageView, imageView2);
                    m.checkNotNullExpressionValue(widgetGlobalSearchItemGuildBinding, "WidgetGlobalSearchItemGuildBinding.bind(itemView)");
                    this.binding = widgetGlobalSearchItemGuildBinding;
                    WidgetGuildsListItemGuildBinding a = WidgetGuildsListItemGuildBinding.a(this.itemView);
                    m.checkNotNullExpressionValue(a, "WidgetGuildsListItemGuildBinding.bind(itemView)");
                    this.bindingGuild = a;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i3)));
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.widgets.user.search.WidgetGlobalSearchGuildsAdapter.Item
        public void onConfigure(int i, WidgetGlobalSearchGuildsModel.Item item) {
            Guild guild;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, item);
            Guild guild2 = item.getGuild();
            boolean z2 = true;
            int i2 = 0;
            if (guild2 == null || !guild2.hasIcon()) {
                z2 = false;
            }
            SimpleDraweeView simpleDraweeView = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "bindingGuild.guildsItemAvatar");
            boolean isSelected = item.isSelected();
            Guild guild3 = item.getGuild();
            Integer valueOf = Integer.valueOf(this.overlayColor);
            SimpleDraweeView simpleDraweeView2 = this.bindingGuild.d;
            m.checkNotNullExpressionValue(simpleDraweeView2, "bindingGuild.guildsItemAvatar");
            SimpleDraweeViewExtensionsKt.setGuildIcon(simpleDraweeView, isSelected, (r23 & 2) != 0 ? null : guild3, simpleDraweeView2.getResources().getDimensionPixelSize(R.dimen.guild_icon_radius), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? null : valueOf, (r23 & 32) != 0 ? null : null, (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? false : false, (r23 & 256) != 0 ? null : null);
            TextView textView = this.bindingGuild.e;
            m.checkNotNullExpressionValue(textView, "bindingGuild.guildsItemAvatarText");
            String str = null;
            if (!z2 && (guild = item.getGuild()) != null) {
                str = guild.getShortName();
            }
            textView.setText(str);
            TextView textView2 = this.bindingGuild.h;
            m.checkNotNullExpressionValue(textView2, "bindingGuild.guildsItemMentions");
            configureMentionsCount(textView2, item.getMentionCount());
            ImageView imageView = this.bindingGuild.i;
            m.checkNotNullExpressionValue(imageView, "bindingGuild.guildsItemVoice");
            imageView.setVisibility(item.getConnectedToVoice() ? 0 : 8);
            ImageView imageView2 = this.binding.c;
            m.checkNotNullExpressionValue(imageView2, "binding.guildsItemUnread");
            imageView2.setVisibility(item.isUnread() ? 0 : 8);
            ImageView imageView3 = this.binding.f2382b;
            m.checkNotNullExpressionValue(imageView3, "binding.guildsItemSelected");
            if (!item.isSelected()) {
                i2 = 8;
            }
            imageView3.setVisibility(i2);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGlobalSearchGuildsAdapter(RecyclerView recyclerView, @ColorInt int i) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        this.overlayColor = i;
    }

    public final Function3<Integer, Integer, WidgetGlobalSearchGuildsModel.Item, Unit> getOnClickListener() {
        return this.onClickListener;
    }

    public final Function3<Integer, Integer, WidgetGlobalSearchGuildsModel.Item, Unit> getOnLongClickListener() {
        return this.onLongClickListener;
    }

    public final void setOnClickListener(Function3<? super Integer, ? super Integer, ? super WidgetGlobalSearchGuildsModel.Item, Unit> function3) {
        m.checkNotNullParameter(function3, "<set-?>");
        this.onClickListener = function3;
    }

    public final void setOnLongClickListener(Function3<? super Integer, ? super Integer, ? super WidgetGlobalSearchGuildsModel.Item, Unit> function3) {
        m.checkNotNullParameter(function3, "<set-?>");
        this.onLongClickListener = function3;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, WidgetGlobalSearchGuildsModel.Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 1) {
            return new ItemDivider(this, R.layout.widget_global_search_item_divider);
        }
        if (i == 2) {
            return new ItemDirectMessage(this, R.layout.widget_global_search_item_dm);
        }
        if (i == 3) {
            return new ItemGuild(this, R.layout.widget_global_search_item_guild, this.overlayColor);
        }
        throw invalidViewTypeException(i);
    }
}
