package com.discord.widgets.user.search;

import andhook.lib.HookHelper;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.frecency.FrecencyTracker;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import com.discord.utilities.search.SearchUtils;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.user.search.WidgetGlobalSearchGuildsModel;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import d0.f0.n;
import d0.f0.q;
import d0.g0.t;
import d0.g0.w;
import d0.g0.y;
import d0.t.h0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.functions.Function7;
import kotlin.jvm.functions.Function8;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.sequences.Sequence;
import rx.Observable;
import rx.functions.Func4;
import rx.functions.Func6;
import rx.functions.Func7;
import rx.functions.Func8;
import xyz.discord.R;
/* compiled from: WidgetGlobalSearchModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0016\b\u0086\b\u0018\u0000 #2\u00020\u0001:\n$#%&'()*+,B7\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0010\b\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\b¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0018\u0010\r\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJF\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0010\b\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0004J\u0010\u0010\u0015\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0007J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R!\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001a\u001a\u0004\b\u001b\u0010\u000bR\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001a\u001a\u0004\b\u001c\u0010\u000bR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0007¨\u0006-"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", "", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "component3", "()Ljava/util/List;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;", "component4", "filter", "searchType", "data", "guildsList", "copy", "(Ljava/lang/String;ILjava/util/List;Ljava/util/List;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getGuildsList", "getData", "Ljava/lang/String;", "getFilter", "I", "getSearchType", HookHelper.constructorName, "(Ljava/lang/String;ILjava/util/List;Ljava/util/List;)V", "Companion", "ChannelContext", "ItemChannel", "ItemDataPayload", "ItemGuild", "ItemHeader", "ItemUser", "MatchedResult", "SearchContext", "UsersContext", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalSearchModel {
    public static final Companion Companion = new Companion(null);
    private static final Function2<Channel, Map<Long, Long>, Boolean> DEFAULT_PERMISSIONS_PREDICATE = WidgetGlobalSearchModel$Companion$DEFAULT_PERMISSIONS_PREDICATE$1.INSTANCE;
    private static final Pattern DISCRIMINATOR_PATTERN;
    private static final int MAX_RESULTS = 50;
    public static final int SEARCH_TYPE_GENERAL = 0;
    public static final int SEARCH_TYPE_GUILD = 3;
    public static final int SEARCH_TYPE_TEXT_CHANNEL = 2;
    public static final int SEARCH_TYPE_USER = 1;
    public static final int SEARCH_TYPE_VOICE_CHANNEL = 4;
    public static final int TYPE_CHANNEL = 0;
    public static final int TYPE_GUILD = 2;
    public static final int TYPE_HEADER = -1;
    public static final int TYPE_USER = 1;
    private final List<ItemDataPayload> data;
    private final String filter;
    private final List<WidgetGlobalSearchGuildsModel.Item> guildsList;
    private final int searchType;

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\"\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001BÉ\u0001\u0012\u0016\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0016\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0002\u0012\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0002\u0012\u001a\u0010\u0018\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\b\u0012\u00060\u0003j\u0002`\f0\u0002\u0012\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0002\u0012 \u0010\u001a\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u000f0\u0002\u0012\u0010\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u0011\u0012\u0010\u0010\u001c\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0011¢\u0006\u0004\b5\u00106J \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J \u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0007J \u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J$\u0010\r\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\b\u0012\u00060\u0003j\u0002`\f0\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0007J \u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0007J*\u0010\u0010\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u000f0\u0002HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\u001a\u0010\u0012\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0014\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0013Jâ\u0001\u0010\u001d\u001a\u00020\u00002\u0018\b\u0002\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0018\b\u0002\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00022\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00022\u001c\b\u0002\u0010\u0018\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\b\u0012\u00060\u0003j\u0002`\f0\u00022\u0018\b\u0002\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00022\"\b\u0002\u0010\u001a\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u000f0\u00022\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u00112\u0012\b\u0002\u0010\u001c\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0011HÆ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010'\u001a\u00020&2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R3\u0010\u001a\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u000f0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b*\u0010\u0007R#\u0010+\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00118\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010\u0013R)\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010)\u001a\u0004\b.\u0010\u0007R-\u0010\u0018\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\b\u0012\u00060\u0003j\u0002`\f0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010)\u001a\u0004\b/\u0010\u0007R#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\b0\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010,\u001a\u0004\b0\u0010\u0013R)\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010)\u001a\u0004\b1\u0010\u0007R#\u0010\u001c\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010,\u001a\u0004\b2\u0010\u0013R)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010)\u001a\u0004\b3\u0010\u0007R)\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b4\u0010\u0007¨\u00067"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;", "", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "component1", "()Ljava/util/Map;", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component2", "component3", "Lcom/discord/api/permission/PermissionBit;", "component4", "component5", "", "component6", "", "component7", "()Ljava/util/Set;", "component8", "guilds", "channels", "activeJoinedThreads", "channelPerms", "channelsPrivate", "guildToChannels", "unreadChannelIds", "unreadGuildIds", "copy", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getGuildToChannels", "smallGuildIds", "Ljava/util/Set;", "getSmallGuildIds", "getChannels", "getChannelPerms", "getUnreadChannelIds", "getGuilds", "getUnreadGuildIds", "getChannelsPrivate", "getActiveJoinedThreads", HookHelper.constructorName, "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelContext {
        private final Map<Long, Channel> activeJoinedThreads;
        private final Map<Long, Long> channelPerms;
        private final Map<Long, Channel> channels;
        private final Map<Long, Channel> channelsPrivate;
        private final Map<Long, List<Long>> guildToChannels;
        private final Map<Long, Guild> guilds;
        private final Set<Long> smallGuildIds;
        private final Set<Long> unreadChannelIds;
        private final Set<Long> unreadGuildIds;

        /* JADX WARN: Multi-variable type inference failed */
        public ChannelContext(Map<Long, Guild> map, Map<Long, Channel> map2, Map<Long, Channel> map3, Map<Long, Long> map4, Map<Long, Channel> map5, Map<Long, ? extends List<Long>> map6, Set<Long> set, Set<Long> set2) {
            m.checkNotNullParameter(map, "guilds");
            m.checkNotNullParameter(map2, "channels");
            m.checkNotNullParameter(map3, "activeJoinedThreads");
            m.checkNotNullParameter(map4, "channelPerms");
            m.checkNotNullParameter(map5, "channelsPrivate");
            m.checkNotNullParameter(map6, "guildToChannels");
            m.checkNotNullParameter(set, "unreadChannelIds");
            m.checkNotNullParameter(set2, "unreadGuildIds");
            this.guilds = map;
            this.channels = map2;
            this.activeJoinedThreads = map3;
            this.channelPerms = map4;
            this.channelsPrivate = map5;
            this.guildToChannels = map6;
            this.unreadChannelIds = set;
            this.unreadGuildIds = set2;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Map.Entry<Long, Guild> entry : map.entrySet()) {
                if (entry.getValue().getMemberCount() <= 200) {
                    linkedHashMap.put(entry.getKey(), entry.getValue());
                }
            }
            this.smallGuildIds = linkedHashMap.keySet();
        }

        public final Map<Long, Guild> component1() {
            return this.guilds;
        }

        public final Map<Long, Channel> component2() {
            return this.channels;
        }

        public final Map<Long, Channel> component3() {
            return this.activeJoinedThreads;
        }

        public final Map<Long, Long> component4() {
            return this.channelPerms;
        }

        public final Map<Long, Channel> component5() {
            return this.channelsPrivate;
        }

        public final Map<Long, List<Long>> component6() {
            return this.guildToChannels;
        }

        public final Set<Long> component7() {
            return this.unreadChannelIds;
        }

        public final Set<Long> component8() {
            return this.unreadGuildIds;
        }

        public final ChannelContext copy(Map<Long, Guild> map, Map<Long, Channel> map2, Map<Long, Channel> map3, Map<Long, Long> map4, Map<Long, Channel> map5, Map<Long, ? extends List<Long>> map6, Set<Long> set, Set<Long> set2) {
            m.checkNotNullParameter(map, "guilds");
            m.checkNotNullParameter(map2, "channels");
            m.checkNotNullParameter(map3, "activeJoinedThreads");
            m.checkNotNullParameter(map4, "channelPerms");
            m.checkNotNullParameter(map5, "channelsPrivate");
            m.checkNotNullParameter(map6, "guildToChannels");
            m.checkNotNullParameter(set, "unreadChannelIds");
            m.checkNotNullParameter(set2, "unreadGuildIds");
            return new ChannelContext(map, map2, map3, map4, map5, map6, set, set2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChannelContext)) {
                return false;
            }
            ChannelContext channelContext = (ChannelContext) obj;
            return m.areEqual(this.guilds, channelContext.guilds) && m.areEqual(this.channels, channelContext.channels) && m.areEqual(this.activeJoinedThreads, channelContext.activeJoinedThreads) && m.areEqual(this.channelPerms, channelContext.channelPerms) && m.areEqual(this.channelsPrivate, channelContext.channelsPrivate) && m.areEqual(this.guildToChannels, channelContext.guildToChannels) && m.areEqual(this.unreadChannelIds, channelContext.unreadChannelIds) && m.areEqual(this.unreadGuildIds, channelContext.unreadGuildIds);
        }

        public final Map<Long, Channel> getActiveJoinedThreads() {
            return this.activeJoinedThreads;
        }

        public final Map<Long, Long> getChannelPerms() {
            return this.channelPerms;
        }

        public final Map<Long, Channel> getChannels() {
            return this.channels;
        }

        public final Map<Long, Channel> getChannelsPrivate() {
            return this.channelsPrivate;
        }

        public final Map<Long, List<Long>> getGuildToChannels() {
            return this.guildToChannels;
        }

        public final Map<Long, Guild> getGuilds() {
            return this.guilds;
        }

        public final Set<Long> getSmallGuildIds() {
            return this.smallGuildIds;
        }

        public final Set<Long> getUnreadChannelIds() {
            return this.unreadChannelIds;
        }

        public final Set<Long> getUnreadGuildIds() {
            return this.unreadGuildIds;
        }

        public int hashCode() {
            Map<Long, Guild> map = this.guilds;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            Map<Long, Channel> map2 = this.channels;
            int hashCode2 = (hashCode + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, Channel> map3 = this.activeJoinedThreads;
            int hashCode3 = (hashCode2 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, Long> map4 = this.channelPerms;
            int hashCode4 = (hashCode3 + (map4 != null ? map4.hashCode() : 0)) * 31;
            Map<Long, Channel> map5 = this.channelsPrivate;
            int hashCode5 = (hashCode4 + (map5 != null ? map5.hashCode() : 0)) * 31;
            Map<Long, List<Long>> map6 = this.guildToChannels;
            int hashCode6 = (hashCode5 + (map6 != null ? map6.hashCode() : 0)) * 31;
            Set<Long> set = this.unreadChannelIds;
            int hashCode7 = (hashCode6 + (set != null ? set.hashCode() : 0)) * 31;
            Set<Long> set2 = this.unreadGuildIds;
            if (set2 != null) {
                i = set2.hashCode();
            }
            return hashCode7 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelContext(guilds=");
            R.append(this.guilds);
            R.append(", channels=");
            R.append(this.channels);
            R.append(", activeJoinedThreads=");
            R.append(this.activeJoinedThreads);
            R.append(", channelPerms=");
            R.append(this.channelPerms);
            R.append(", channelsPrivate=");
            R.append(this.channelsPrivate);
            R.append(", guildToChannels=");
            R.append(this.guildToChannels);
            R.append(", unreadChannelIds=");
            R.append(this.unreadChannelIds);
            R.append(", unreadGuildIds=");
            R.append(this.unreadGuildIds);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\bD\u0010EJ\u0081\u0001\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022,\u0010\f\u001a(\u0012\u0004\u0012\u00020\u0006\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\bj\u0002`\n0\u0007\u0012\u0004\u0012\u00020\u000b0\u00052\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b\u0018\u00010\r2\u0016\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\u0010\u0012\b\u0012\u00060\bj\u0002`\t0\rH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J?\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d2\u0006\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00182\u0010\u0010\u001c\u001a\f\u0012\b\u0012\u00060\bj\u0002`\u001b0\u001aH\u0002¢\u0006\u0004\b\u001f\u0010 J\u001d\u0010!\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u001d*\u00020\u0010H\u0002¢\u0006\u0004\b!\u0010\"J\u001b\u0010%\u001a\u0004\u0018\u00010$*\u00020\u00032\u0006\u0010#\u001a\u00020\u0003¢\u0006\u0004\b%\u0010&J\u001b\u0010'\u001a\u0004\u0018\u00010$*\u00020\u00032\u0006\u0010#\u001a\u00020\u0003¢\u0006\u0004\b'\u0010&J9\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00120\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b\u0018\u00010\r¢\u0006\u0004\b(\u0010)J!\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00120\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b*\u0010+J_\u0010.\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010-\u001a\u00020,2\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b\u0018\u00010\r2\u0018\b\u0002\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\u0010\u0012\b\u0012\u00060\bj\u0002`\t0\r¢\u0006\u0004\b.\u0010/R\u0013\u00102\u001a\u00020$8F@\u0006¢\u0006\u0006\u001a\u0004\b0\u00101R<\u00103\u001a(\u0012\u0004\u0012\u00020\u0006\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\bj\u0002`\n0\u0007\u0012\u0004\u0012\u00020\u000b0\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b9\u0010:R\u0016\u0010;\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b;\u0010:R\u0016\u0010<\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b<\u0010:R\u0016\u0010=\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b=\u0010:R\u0016\u0010>\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b>\u0010:R\u0016\u0010?\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b?\u0010:R\u0016\u0010@\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b@\u0010:R\u0016\u0010A\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bA\u0010:R\u0016\u0010B\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bB\u0010:R\u0016\u0010C\u001a\u0002088\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bC\u0010:¨\u0006F"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;", "", "Lrx/Observable;", "", "filterPublisher", "Lkotlin/Function2;", "Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/permission/PermissionBit;", "", "permissionsPredicate", "Lkotlin/Function1;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "resultsFilter", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;", "lastChannelIdProvider", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "get", "(Lrx/Observable;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lrx/Observable;", "searchContext", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;", "channelContext", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;", "usersContext", "", "Lcom/discord/primitives/UserId;", "selectedGuildUserIds", "Lkotlin/sequences/Sequence;", "Lcom/discord/models/user/User;", "getDefaultUserSearch", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Ljava/util/Set;)Lkotlin/sequences/Sequence;", "getRecentChannelIds", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)Lkotlin/sequences/Sequence;", "filter", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "toMatchedResult", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "toFuzzyMatchedResult", "getForSend", "(Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Observable;", "getForNav", "(Lrx/Observable;)Lrx/Observable;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel;", "widgetGuildsListModel", "create", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "getEMPTY_MATCH_RESULT", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "EMPTY_MATCH_RESULT", "DEFAULT_PERMISSIONS_PREDICATE", "Lkotlin/jvm/functions/Function2;", "Ljava/util/regex/Pattern;", "DISCRIMINATOR_PATTERN", "Ljava/util/regex/Pattern;", "", "MAX_RESULTS", "I", "SEARCH_TYPE_GENERAL", "SEARCH_TYPE_GUILD", "SEARCH_TYPE_TEXT_CHANNEL", "SEARCH_TYPE_USER", "SEARCH_TYPE_VOICE_CHANNEL", "TYPE_CHANNEL", "TYPE_GUILD", "TYPE_HEADER", "TYPE_USER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ WidgetGlobalSearchModel create$default(Companion companion, SearchContext searchContext, UsersContext usersContext, ChannelContext channelContext, WidgetGlobalSearchGuildsModel widgetGlobalSearchGuildsModel, Function1 function1, Function1 function12, int i, Object obj) {
            Function1<? super ItemDataPayload, Boolean> function13 = function1;
            if ((i & 16) != 0) {
                function13 = null;
            }
            Function1<? super ItemDataPayload, Boolean> function14 = function13;
            WidgetGlobalSearchModel$Companion$create$1 widgetGlobalSearchModel$Companion$create$1 = function12;
            if ((i & 32) != 0) {
                widgetGlobalSearchModel$Companion$create$1 = WidgetGlobalSearchModel$Companion$create$1.INSTANCE;
            }
            return companion.create(searchContext, usersContext, channelContext, widgetGlobalSearchGuildsModel, function14, widgetGlobalSearchModel$Companion$create$1);
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r2v8, types: [com.discord.widgets.user.search.WidgetGlobalSearchModel$sam$rx_functions_Func4$0] */
        /* JADX WARN: Type inference failed for: r3v16, types: [com.discord.widgets.user.search.WidgetGlobalSearchModel$sam$rx_functions_Func8$0] */
        private final Observable<WidgetGlobalSearchModel> get(Observable<String> observable, final Function2<? super Channel, ? super Map<Long, Long>, Boolean> function2, Function1<? super ItemDataPayload, Boolean> function1, Function1<? super SearchContext, Long> function12) {
            k kVar = new k("");
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<List<Long>> observeRecentSelectedGuildIds = companion.getGuildSelected().observeRecentSelectedGuildIds();
            Observable<Long> observeId = companion.getChannelsSelected().observeId();
            Observable<Long> observePreviousId = companion.getChannelsSelected().observePreviousId();
            Observable<Long> observeSelectedVoiceChannelId = companion.getVoiceChannelSelected().observeSelectedVoiceChannelId();
            Observable<Map<Long, Long>> observeRecentMessageIds = companion.getMessagesMostRecent().observeRecentMessageIds();
            TimeUnit timeUnit = TimeUnit.SECONDS;
            Observable leadingEdgeThrottle = ObservableExtensionsKt.leadingEdgeThrottle(observeRecentMessageIds, 10L, timeUnit);
            Observable<Map<Long, Integer>> observeMentionCounts = companion.getMentions().observeMentionCounts();
            final WidgetGlobalSearchModel$Companion$get$partialSearchContextObservable$1 widgetGlobalSearchModel$Companion$get$partialSearchContextObservable$1 = WidgetGlobalSearchModel$Companion$get$partialSearchContextObservable$1.INSTANCE;
            Object obj = widgetGlobalSearchModel$Companion$get$partialSearchContextObservable$1;
            if (widgetGlobalSearchModel$Companion$get$partialSearchContextObservable$1 != null) {
                obj = new Func7() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$sam$rx_functions_Func7$0
                    @Override // rx.functions.Func7
                    public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8) {
                        return Function7.this.invoke(obj2, obj3, obj4, obj5, obj6, obj7, obj8);
                    }
                };
            }
            Observable q = ObservableWithLeadingEdgeThrottle.combineLatest(kVar, observeRecentSelectedGuildIds, observeId, observePreviousId, observeSelectedVoiceChannelId, leadingEdgeThrottle, observeMentionCounts, (Func7) obj, 2L, timeUnit).q();
            WidgetGlobalSearchModel$Companion$get$searchContextObservable$1 widgetGlobalSearchModel$Companion$get$searchContextObservable$1 = WidgetGlobalSearchModel$Companion$get$searchContextObservable$1.INSTANCE;
            TimeUnit timeUnit2 = TimeUnit.MILLISECONDS;
            Observable q2 = ObservableWithLeadingEdgeThrottle.combineLatest(observable, q, widgetGlobalSearchModel$Companion$get$searchContextObservable$1, 250L, timeUnit2).q();
            Observable<Map<Long, User>> observeAllUsers = companion.getUsers().observeAllUsers();
            Observable<Map<Long, Presence>> observeAllPresences = companion.getPresences().observeAllPresences();
            Observable<Map<Long, Integer>> observe = companion.getUserRelationships().observe();
            Observable<Map<Long, Map<Long, GuildMember>>> observeComputed = companion.getGuilds().observeComputed();
            final WidgetGlobalSearchModel$Companion$get$usersContextObservable$1 widgetGlobalSearchModel$Companion$get$usersContextObservable$1 = WidgetGlobalSearchModel$Companion$get$usersContextObservable$1.INSTANCE;
            WidgetGlobalSearchModel$Companion$get$usersContextObservable$1 widgetGlobalSearchModel$Companion$get$usersContextObservable$12 = widgetGlobalSearchModel$Companion$get$usersContextObservable$1;
            if (widgetGlobalSearchModel$Companion$get$usersContextObservable$1 != null) {
                widgetGlobalSearchModel$Companion$get$usersContextObservable$12 = new Func4() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$sam$rx_functions_Func4$0
                    @Override // rx.functions.Func4
                    public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                        return Function4.this.invoke(obj2, obj3, obj4, obj5);
                    }
                };
            }
            Observable q3 = ObservableWithLeadingEdgeThrottle.combineLatest(observeAllUsers, observeAllPresences, observe, observeComputed, (Func4) widgetGlobalSearchModel$Companion$get$usersContextObservable$12, 10L, timeUnit).q();
            Observable<Map<Long, Guild>> observeGuilds = companion.getGuilds().observeGuilds();
            Observable<Map<Long, Channel>> observeGuildAndPrivateChannels = companion.getChannels().observeGuildAndPrivateChannels();
            Observable<Map<Long, Channel>> observeAllActiveJoinedThreadsChannelsById = companion.getThreadsActiveJoined().observeAllActiveJoinedThreadsChannelsById();
            Observable<Map<Long, Long>> observePermissionsForAllChannels = companion.getPermissions().observePermissionsForAllChannels();
            Observable<Map<Long, Channel>> observePrivateChannels = companion.getChannels().observePrivateChannels();
            Observable<Map<Long, List<Long>>> observeIds = companion.getChannels().observeIds(true);
            Observable<Set<Long>> unreadChannelIds = companion.getReadStates().getUnreadChannelIds();
            Observable<Set<Long>> unreadGuildIds = companion.getReadStates().getUnreadGuildIds();
            final WidgetGlobalSearchModel$Companion$get$channelContextObservable$1 widgetGlobalSearchModel$Companion$get$channelContextObservable$1 = WidgetGlobalSearchModel$Companion$get$channelContextObservable$1.INSTANCE;
            WidgetGlobalSearchModel$Companion$get$channelContextObservable$1 widgetGlobalSearchModel$Companion$get$channelContextObservable$12 = widgetGlobalSearchModel$Companion$get$channelContextObservable$1;
            if (widgetGlobalSearchModel$Companion$get$channelContextObservable$1 != null) {
                widgetGlobalSearchModel$Companion$get$channelContextObservable$12 = new Func8() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$sam$rx_functions_Func8$0
                    @Override // rx.functions.Func8
                    public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9) {
                        return Function8.this.invoke(obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9);
                    }
                };
            }
            Observable q4 = ObservableWithLeadingEdgeThrottle.combineLatest(observeGuilds, observeGuildAndPrivateChannels, observeAllActiveJoinedThreadsChannelsById, observePermissionsForAllChannels, observePrivateChannels, observeIds, unreadChannelIds, unreadGuildIds, (Func8) widgetGlobalSearchModel$Companion$get$channelContextObservable$12, 3L, timeUnit).F(new b<ChannelContext, ChannelContext>() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$Companion$get$channelContextObservable$2
                public final WidgetGlobalSearchModel.ChannelContext call(WidgetGlobalSearchModel.ChannelContext channelContext) {
                    WidgetGlobalSearchModel.ChannelContext copy;
                    Map plus = h0.plus(channelContext.getChannels(), channelContext.getActiveJoinedThreads());
                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                    for (Map.Entry entry : plus.entrySet()) {
                        if (((Boolean) Function2.this.invoke((Channel) entry.getValue(), channelContext.getChannelPerms())).booleanValue()) {
                            linkedHashMap.put(entry.getKey(), entry.getValue());
                        }
                    }
                    copy = channelContext.copy((r18 & 1) != 0 ? channelContext.guilds : null, (r18 & 2) != 0 ? channelContext.channels : linkedHashMap, (r18 & 4) != 0 ? channelContext.activeJoinedThreads : null, (r18 & 8) != 0 ? channelContext.channelPerms : null, (r18 & 16) != 0 ? channelContext.channelsPrivate : null, (r18 & 32) != 0 ? channelContext.guildToChannels : null, (r18 & 64) != 0 ? channelContext.unreadChannelIds : null, (r18 & 128) != 0 ? channelContext.unreadGuildIds : null);
                    return copy;
                }
            }).q();
            Observable q5 = ObservableWithLeadingEdgeThrottle.combineLatest(q2, q4, companion.getUserGuildSettings().observeGuildSettings(), companion.getGuildsSorted().observeOrderedGuilds(), WidgetGlobalSearchModel$Companion$get$guildsListObservable$1.INSTANCE, 5L, timeUnit).q();
            k kVar2 = new k(function1);
            k kVar3 = new k(function12);
            final WidgetGlobalSearchModel$Companion$get$1 widgetGlobalSearchModel$Companion$get$1 = new WidgetGlobalSearchModel$Companion$get$1(this);
            Observable<WidgetGlobalSearchModel> q6 = ObservableWithLeadingEdgeThrottle.combineLatest(q2, q3, q4, q5, kVar2, kVar3, new Func6() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$sam$rx_functions_Func6$0
                @Override // rx.functions.Func6
                public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7) {
                    return Function6.this.invoke(obj2, obj3, obj4, obj5, obj6, obj7);
                }
            }, 200L, timeUnit2).q();
            m.checkNotNullExpressionValue(q6, "ObservableWithLeadingEdg…  .distinctUntilChanged()");
            return q6;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Observable get$default(Companion companion, Observable observable, Function2 function2, Function1 function1, Function1 function12, int i, Object obj) {
            if ((i & 4) != 0) {
                function1 = null;
            }
            return companion.get(observable, function2, function1, function12);
        }

        private final Sequence<User> getDefaultUserSearch(SearchContext searchContext, ChannelContext channelContext, UsersContext usersContext, Set<Long> set) {
            Sequence mapNotNull = q.mapNotNull(getRecentChannelIds(searchContext), new WidgetGlobalSearchModel$Companion$getDefaultUserSearch$recentDmUserIds$1(channelContext));
            return q.filterNot(q.take(q.mapNotNull(q.distinct(q.plus(q.plus(mapNotNull, (Iterable) set), n.flattenSequenceOfIterable(q.mapNotNull(u.asSequence(channelContext.getSmallGuildIds()), new WidgetGlobalSearchModel$Companion$getDefaultUserSearch$smallGuildUserIds$1(usersContext))))), new WidgetGlobalSearchModel$Companion$getDefaultUserSearch$1(usersContext)), 100), WidgetGlobalSearchModel$Companion$getDefaultUserSearch$2.INSTANCE);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Observable getForSend$default(Companion companion, Observable observable, Function1 function1, int i, Object obj) {
            if ((i & 2) != 0) {
                function1 = null;
            }
            return companion.getForSend(observable, function1);
        }

        private final Sequence<Long> getRecentChannelIds(SearchContext searchContext) {
            if (!searchContext.getFrecencyChannels().isEmpty()) {
                return u.asSequence(searchContext.getFrecencyChannels());
            }
            return q.map(q.take(u.asSequence(u.sortedWith(searchContext.getMostRecent().entrySet(), new Comparator() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$Companion$getRecentChannelIds$$inlined$sortedBy$1
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return d0.u.a.compareValues((Long) ((Map.Entry) t).getValue(), (Long) ((Map.Entry) t2).getValue());
                }
            })), 50), WidgetGlobalSearchModel$Companion$getRecentChannelIds$2.INSTANCE);
        }

        public final WidgetGlobalSearchModel create(final SearchContext searchContext, UsersContext usersContext, ChannelContext channelContext, WidgetGlobalSearchGuildsModel widgetGlobalSearchGuildsModel, Function1<? super ItemDataPayload, Boolean> function1, Function1<? super SearchContext, Long> function12) {
            Set<Long> set;
            Sequence sequence;
            Sequence sequence2;
            Sequence<User> sequence3;
            Sequence sequence4;
            m.checkNotNullParameter(searchContext, "searchContext");
            m.checkNotNullParameter(usersContext, "usersContext");
            m.checkNotNullParameter(channelContext, "channelContext");
            m.checkNotNullParameter(widgetGlobalSearchGuildsModel, "widgetGuildsListModel");
            m.checkNotNullParameter(function12, "lastChannelIdProvider");
            WidgetGlobalSearchModel$Companion$create$2 widgetGlobalSearchModel$Companion$create$2 = new WidgetGlobalSearchModel$Companion$create$2(usersContext, searchContext, channelContext);
            WidgetGlobalSearchModel$Companion$create$3 widgetGlobalSearchModel$Companion$create$3 = new WidgetGlobalSearchModel$Companion$create$3(channelContext, searchContext);
            WidgetGlobalSearchModel$Companion$create$4 widgetGlobalSearchModel$Companion$create$4 = new WidgetGlobalSearchModel$Companion$create$4(channelContext, widgetGlobalSearchModel$Companion$create$2, searchContext);
            int searchType = searchContext.getSearchType();
            String sanitizedFilter = searchContext.getSanitizedFilter();
            Map<Long, GuildMember> map = usersContext.getMembers().get(u.first((List<? extends Object>) searchContext.getRecentGuildIds()));
            if (map == null || (set = map.keySet()) == null) {
                set = n0.emptySet();
            }
            Set<Long> set2 = set;
            if (searchType != 1) {
                if (searchType == 2) {
                    sequence4 = q.mapNotNull(q.filter(u.asSequence(channelContext.getChannels().values()), WidgetGlobalSearchModel$Companion$create$filteredResults$3.INSTANCE), new WidgetGlobalSearchModel$Companion$create$filteredResults$4(widgetGlobalSearchModel$Companion$create$4, sanitizedFilter));
                } else if (searchType == 3) {
                    sequence4 = q.mapNotNull(u.asSequence(channelContext.getGuilds().values()), new WidgetGlobalSearchModel$Companion$create$filteredResults$2(widgetGlobalSearchModel$Companion$create$3, sanitizedFilter));
                } else if (searchType == 4) {
                    sequence4 = q.sortedWith(q.mapNotNull(q.filter(q.filter(u.asSequence(channelContext.getChannels().values()), WidgetGlobalSearchModel$Companion$create$filteredResults$5.INSTANCE), new WidgetGlobalSearchModel$Companion$create$filteredResults$6(channelContext)), new WidgetGlobalSearchModel$Companion$create$filteredResults$7(widgetGlobalSearchModel$Companion$create$4, sanitizedFilter)), new Comparator() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$Companion$create$$inlined$sortedBy$1
                        /* JADX WARN: Removed duplicated region for block: B:15:0x0048  */
                        @Override // java.util.Comparator
                        /*
                            Code decompiled incorrectly, please refer to instructions dump.
                            To view partially-correct add '--show-bad-code' argument
                        */
                        public final int compare(T r8, T r9) {
                            /*
                                r7 = this;
                                com.discord.widgets.user.search.WidgetGlobalSearchModel$ItemDataPayload r8 = (com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload) r8
                                com.discord.api.channel.Channel r8 = r8.getChannel()
                                r0 = 0
                                r1 = 1
                                r2 = 0
                                r3 = -1
                                r4 = 2147483647(0x7fffffff, float:NaN)
                                if (r8 == 0) goto L39
                                long r5 = r8.f()
                                com.discord.widgets.user.search.WidgetGlobalSearchModel$SearchContext r8 = com.discord.widgets.user.search.WidgetGlobalSearchModel.SearchContext.this
                                java.util.List r8 = r8.getRecentGuildIds()
                                java.lang.Long r5 = java.lang.Long.valueOf(r5)
                                int r8 = r8.indexOf(r5)
                                java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
                                int r5 = r8.intValue()
                                if (r5 == r3) goto L2d
                                r5 = 1
                                goto L2e
                            L2d:
                                r5 = 0
                            L2e:
                                if (r5 == 0) goto L31
                                goto L32
                            L31:
                                r8 = r0
                            L32:
                                if (r8 == 0) goto L39
                                int r8 = r8.intValue()
                                goto L3c
                            L39:
                                r8 = 2147483647(0x7fffffff, float:NaN)
                            L3c:
                                java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
                                com.discord.widgets.user.search.WidgetGlobalSearchModel$ItemDataPayload r9 = (com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload) r9
                                com.discord.api.channel.Channel r9 = r9.getChannel()
                                if (r9 == 0) goto L6f
                                long r5 = r9.f()
                                com.discord.widgets.user.search.WidgetGlobalSearchModel$SearchContext r9 = com.discord.widgets.user.search.WidgetGlobalSearchModel.SearchContext.this
                                java.util.List r9 = r9.getRecentGuildIds()
                                java.lang.Long r5 = java.lang.Long.valueOf(r5)
                                int r9 = r9.indexOf(r5)
                                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
                                int r5 = r9.intValue()
                                if (r5 == r3) goto L65
                                goto L66
                            L65:
                                r1 = 0
                            L66:
                                if (r1 == 0) goto L69
                                r0 = r9
                            L69:
                                if (r0 == 0) goto L6f
                                int r4 = r0.intValue()
                            L6f:
                                java.lang.Integer r9 = java.lang.Integer.valueOf(r4)
                                int r8 = d0.u.a.compareValues(r8, r9)
                                return r8
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.search.WidgetGlobalSearchModel$Companion$create$$inlined$sortedBy$1.compare(java.lang.Object, java.lang.Object):int");
                        }
                    });
                } else if (t.isBlank(sanitizedFilter)) {
                    sequence4 = q.mapNotNull(q.filter(q.mapNotNull(u.asSequence(u.union(searchContext.getMentionCounts().keySet(), q.asIterable(getRecentChannelIds(searchContext)))), new WidgetGlobalSearchModel$Companion$create$filteredResults$9(channelContext)), WidgetGlobalSearchModel$Companion$create$filteredResults$10.INSTANCE), new WidgetGlobalSearchModel$Companion$create$filteredResults$11(widgetGlobalSearchModel$Companion$create$4, sanitizedFilter));
                } else {
                    HashSet hashSet = new HashSet();
                    sequence4 = q.plus(q.mapNotNull(q.filter(u.asSequence(channelContext.getChannels().values()), WidgetGlobalSearchModel$Companion$create$filteredResults$channelResults$1.INSTANCE), new WidgetGlobalSearchModel$Companion$create$filteredResults$channelResults$2(hashSet, widgetGlobalSearchModel$Companion$create$2, usersContext, sanitizedFilter, widgetGlobalSearchModel$Companion$create$4)), q.mapNotNull(q.filterNot(u.asSequence(set2), new WidgetGlobalSearchModel$Companion$create$filteredResults$selectedGuildUserResults$1(hashSet)), new WidgetGlobalSearchModel$Companion$create$filteredResults$selectedGuildUserResults$2(widgetGlobalSearchModel$Companion$create$2, usersContext, sanitizedFilter)));
                }
                sequence = sequence4;
            } else {
                if (t.isBlank(sanitizedFilter)) {
                    sequence3 = getDefaultUserSearch(searchContext, channelContext, usersContext, set2);
                } else {
                    sequence3 = u.asSequence(usersContext.getUsers().values());
                }
                sequence = q.mapNotNull(sequence3, new WidgetGlobalSearchModel$Companion$create$filteredResults$1(widgetGlobalSearchModel$Companion$create$2, sanitizedFilter));
            }
            if (function1 != null) {
                sequence = q.filter(sequence, function1);
            }
            Sequence take = q.take(q.sortedWith(sequence, new Comparator() { // from class: com.discord.widgets.user.search.WidgetGlobalSearchModel$Companion$create$$inlined$sortedByDescending$1
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    WidgetGlobalSearchScoreStrategy widgetGlobalSearchScoreStrategy = WidgetGlobalSearchScoreStrategy.INSTANCE;
                    return d0.u.a.compareValues(Integer.valueOf(widgetGlobalSearchScoreStrategy.score((WidgetGlobalSearchModel.ItemDataPayload) t2, WidgetGlobalSearchModel.SearchContext.this.getSearchType(), WidgetGlobalSearchModel.SearchContext.this.getFrecencyChannels())), Integer.valueOf(widgetGlobalSearchScoreStrategy.score((WidgetGlobalSearchModel.ItemDataPayload) t, WidgetGlobalSearchModel.SearchContext.this.getSearchType(), WidgetGlobalSearchModel.SearchContext.this.getFrecencyChannels())));
                }
            }), 50);
            if (searchType == 0 && t.isBlank(searchContext.getFilter())) {
                Channel channel = channelContext.getChannels().get(function12.invoke(searchContext));
                ItemDataPayload invoke = channel != null ? widgetGlobalSearchModel$Companion$create$4.invoke(channel, "") : null;
                if (invoke == null) {
                    sequence2 = q.plus(n.sequenceOf(new ItemHeader(R.string.suggestions, 0, false, 6, null)), take);
                } else {
                    sequence2 = q.plus(q.plus(n.sequenceOf(new ItemHeader(R.string.quickswitcher_last_channel, 0, false, 6, null), invoke), n.sequenceOf(new ItemHeader(R.string.suggestions, 0, false, 6, null))), q.filterNot(take, new WidgetGlobalSearchModel$Companion$create$results$1(channel)));
                }
                return new WidgetGlobalSearchModel(sanitizedFilter, searchType, q.toList(sequence2), widgetGlobalSearchGuildsModel.getItems());
            }
            if (q.firstOrNull(take) != null) {
                take = q.plus(n.sequenceOf(new ItemHeader(R.string.suggestions, 0, false, 6, null)), take);
            }
            return new WidgetGlobalSearchModel(sanitizedFilter, searchType, q.toList(take), null, 8, null);
        }

        public final MatchedResult getEMPTY_MATCH_RESULT() {
            return new MatchedResult("", -1, 0);
        }

        public final Observable<WidgetGlobalSearchModel> getForNav(Observable<String> observable) {
            m.checkNotNullParameter(observable, "filterPublisher");
            return get$default(this, observable, WidgetGlobalSearchModel.DEFAULT_PERMISSIONS_PREDICATE, null, WidgetGlobalSearchModel$Companion$getForNav$1.INSTANCE, 4, null);
        }

        public final Observable<WidgetGlobalSearchModel> getForSend(Observable<String> observable, Function1<? super ItemDataPayload, Boolean> function1) {
            m.checkNotNullParameter(observable, "filterPublisher");
            return get(observable, WidgetGlobalSearchModel$Companion$getForSend$1.INSTANCE, function1, WidgetGlobalSearchModel$Companion$getForSend$2.INSTANCE);
        }

        public final MatchedResult toFuzzyMatchedResult(String str, String str2) {
            m.checkNotNullParameter(str, "$this$toFuzzyMatchedResult");
            m.checkNotNullParameter(str2, "filter");
            SearchUtils searchUtils = SearchUtils.INSTANCE;
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            String lowerCase = str2.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            String lowerCase2 = str.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
            if (searchUtils.fuzzyMatch(lowerCase, StringUtilsKt.stripAccents(lowerCase2))) {
                return new MatchedResult(str, -1, str2.length());
            }
            return null;
        }

        public final MatchedResult toMatchedResult(String str, String str2) {
            m.checkNotNullParameter(str, "$this$toMatchedResult");
            m.checkNotNullParameter(str2, "filter");
            int indexOf$default = w.indexOf$default((CharSequence) str, str2, 0, true, 2, (Object) null);
            if (indexOf$default != -1) {
                return new MatchedResult(str, indexOf$default, str2.length());
            }
            return null;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00068&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\r\u001a\u00020\n8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\u0011\u001a\u00020\u000e8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "getMatchedResult", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "matchedResult", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", "channel", "", "getUnread", "()Z", "unread", "", "getMentions", "()I", "mentions", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface ItemDataPayload extends MGRecyclerDataPayload {
        Channel getChannel();

        MatchedResult getMatchedResult();

        int getMentions();

        boolean getUnread();
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0007J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0016\u001a\u0004\b\u0018\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "", "", "component1", "()Ljava/lang/CharSequence;", "", "component2", "()I", "component3", "value", "firstMatchIndex", "filterLength", "copy", "(Ljava/lang/CharSequence;II)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getFirstMatchIndex", "getFilterLength", "Ljava/lang/CharSequence;", "getValue", HookHelper.constructorName, "(Ljava/lang/CharSequence;II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MatchedResult {
        private final int filterLength;
        private final int firstMatchIndex;
        private final CharSequence value;

        public MatchedResult(CharSequence charSequence, int i, int i2) {
            m.checkNotNullParameter(charSequence, "value");
            this.value = charSequence;
            this.firstMatchIndex = i;
            this.filterLength = i2;
        }

        public static /* synthetic */ MatchedResult copy$default(MatchedResult matchedResult, CharSequence charSequence, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                charSequence = matchedResult.value;
            }
            if ((i3 & 2) != 0) {
                i = matchedResult.firstMatchIndex;
            }
            if ((i3 & 4) != 0) {
                i2 = matchedResult.filterLength;
            }
            return matchedResult.copy(charSequence, i, i2);
        }

        public final CharSequence component1() {
            return this.value;
        }

        public final int component2() {
            return this.firstMatchIndex;
        }

        public final int component3() {
            return this.filterLength;
        }

        public final MatchedResult copy(CharSequence charSequence, int i, int i2) {
            m.checkNotNullParameter(charSequence, "value");
            return new MatchedResult(charSequence, i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MatchedResult)) {
                return false;
            }
            MatchedResult matchedResult = (MatchedResult) obj;
            return m.areEqual(this.value, matchedResult.value) && this.firstMatchIndex == matchedResult.firstMatchIndex && this.filterLength == matchedResult.filterLength;
        }

        public final int getFilterLength() {
            return this.filterLength;
        }

        public final int getFirstMatchIndex() {
            return this.firstMatchIndex;
        }

        public final CharSequence getValue() {
            return this.value;
        }

        public int hashCode() {
            CharSequence charSequence = this.value;
            return ((((charSequence != null ? charSequence.hashCode() : 0) * 31) + this.firstMatchIndex) * 31) + this.filterLength;
        }

        public String toString() {
            StringBuilder R = a.R("MatchedResult(value=");
            R.append(this.value);
            R.append(", firstMatchIndex=");
            R.append(this.firstMatchIndex);
            R.append(", filterLength=");
            return a.A(R, this.filterLength, ")");
        }
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u001e\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001By\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0010\u0010\u0016\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u0005\u0012\n\u0010\u0017\u001a\u00060\u0006j\u0002`\n\u0012\n\u0010\u0018\u001a\u00060\u0006j\u0002`\n\u0012\n\u0010\u0019\u001a\u00060\u0006j\u0002`\n\u0012\u001a\u0010\u001a\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\b\u0012\u00060\u0006j\u0002`\u00100\u000f\u0012\u0016\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00130\u000f¢\u0006\u0004\b>\u0010?J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\b\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0014\u0010\u000b\u001a\u00060\u0006j\u0002`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0014\u0010\r\u001a\u00060\u0006j\u0002`\nHÆ\u0003¢\u0006\u0004\b\r\u0010\fJ\u0014\u0010\u000e\u001a\u00060\u0006j\u0002`\nHÆ\u0003¢\u0006\u0004\b\u000e\u0010\fJ$\u0010\u0011\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\b\u0012\u00060\u0006j\u0002`\u00100\u000fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J \u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00130\u000fHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0012J\u0090\u0001\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\u0012\b\u0002\u0010\u0016\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u00052\f\b\u0002\u0010\u0017\u001a\u00060\u0006j\u0002`\n2\f\b\u0002\u0010\u0018\u001a\u00060\u0006j\u0002`\n2\f\b\u0002\u0010\u0019\u001a\u00060\u0006j\u0002`\n2\u001c\b\u0002\u0010\u001a\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\b\u0012\u00060\u0006j\u0002`\u00100\u000f2\u0018\b\u0002\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00130\u000fHÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0004J\u0010\u0010\u001f\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u001a\u0010#\u001a\u00020\"2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u001d\u0010\u0018\u001a\u00060\u0006j\u0002`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010%\u001a\u0004\b&\u0010\fR\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010'\u001a\u0004\b(\u0010\u0004R\u0019\u0010)\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010'\u001a\u0004\b*\u0010\u0004R#\u0010,\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\n0+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R)\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00130\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00100\u001a\u0004\b1\u0010\u0012R#\u0010\u0016\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u00102\u001a\u0004\b3\u0010\tR\u0019\u00104\u001a\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u001d\u0010\u0019\u001a\u00060\u0006j\u0002`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010%\u001a\u0004\b8\u0010\fR-\u0010\u001a\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\b\u0012\u00060\u0006j\u0002`\u00100\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00100\u001a\u0004\b9\u0010\u0012R\u001d\u0010\u0017\u001a\u00060\u0006j\u0002`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010%\u001a\u0004\b:\u0010\fR\u0019\u0010;\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010 ¨\u0006@"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;", "", "", "component1", "()Ljava/lang/String;", "", "", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/util/List;", "Lcom/discord/primitives/ChannelId;", "component3", "()J", "component4", "component5", "", "Lcom/discord/primitives/MessageId;", "component6", "()Ljava/util/Map;", "", "component7", "filter", "recentGuildIds", "selectedChannelId", "prevSelectedChannelId", "selectedVoiceChannelId", "mostRecent", "mentionCounts", "copy", "(Ljava/lang/String;Ljava/util/List;JJJLjava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;", "toString", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getPrevSelectedChannelId", "Ljava/lang/String;", "getFilter", "sanitizedFilter", "getSanitizedFilter", "", "frecencyChannels", "Ljava/util/Collection;", "getFrecencyChannels", "()Ljava/util/Collection;", "Ljava/util/Map;", "getMentionCounts", "Ljava/util/List;", "getRecentGuildIds", "hasDiscriminator", "Z", "getHasDiscriminator", "()Z", "getSelectedVoiceChannelId", "getMostRecent", "getSelectedChannelId", "searchType", "I", "getSearchType", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;JJJLjava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SearchContext {
        private final String filter;
        private final Collection<Long> frecencyChannels = FrecencyTracker.getSortedKeys$default(StoreStream.Companion.getChannelsSelected().getFrecency(), 0, 1, null);
        private final boolean hasDiscriminator;
        private final Map<Long, Integer> mentionCounts;
        private final Map<Long, Long> mostRecent;
        private final long prevSelectedChannelId;
        private final List<Long> recentGuildIds;
        private final String sanitizedFilter;
        private final int searchType;
        private final long selectedChannelId;
        private final long selectedVoiceChannelId;

        public SearchContext(String str, List<Long> list, long j, long j2, long j3, Map<Long, Long> map, Map<Long, Integer> map2) {
            int i;
            String str2;
            m.checkNotNullParameter(str, "filter");
            m.checkNotNullParameter(list, "recentGuildIds");
            m.checkNotNullParameter(map, "mostRecent");
            m.checkNotNullParameter(map2, "mentionCounts");
            this.filter = str;
            this.recentGuildIds = list;
            this.selectedChannelId = j;
            this.prevSelectedChannelId = j2;
            this.selectedVoiceChannelId = j3;
            this.mostRecent = map;
            this.mentionCounts = map2;
            Character firstOrNull = y.firstOrNull(str);
            boolean z2 = false;
            if (firstOrNull != null && firstOrNull.charValue() == '@') {
                i = 1;
            } else if (firstOrNull != null && firstOrNull.charValue() == '#') {
                i = 2;
            } else if (firstOrNull != null && firstOrNull.charValue() == '!') {
                i = 4;
            } else {
                i = (firstOrNull != null && firstOrNull.charValue() == '*') ? 3 : 0;
            }
            this.searchType = i;
            if (i != 0) {
                Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
                str2 = str.substring(1);
                m.checkNotNullExpressionValue(str2, "(this as java.lang.String).substring(startIndex)");
            } else {
                str2 = str;
            }
            this.sanitizedFilter = str2;
            this.hasDiscriminator = (i == 0 || i == 1) ? WidgetGlobalSearchModel.DISCRIMINATOR_PATTERN.matcher(str).find() : z2;
        }

        public final String component1() {
            return this.filter;
        }

        public final List<Long> component2() {
            return this.recentGuildIds;
        }

        public final long component3() {
            return this.selectedChannelId;
        }

        public final long component4() {
            return this.prevSelectedChannelId;
        }

        public final long component5() {
            return this.selectedVoiceChannelId;
        }

        public final Map<Long, Long> component6() {
            return this.mostRecent;
        }

        public final Map<Long, Integer> component7() {
            return this.mentionCounts;
        }

        public final SearchContext copy(String str, List<Long> list, long j, long j2, long j3, Map<Long, Long> map, Map<Long, Integer> map2) {
            m.checkNotNullParameter(str, "filter");
            m.checkNotNullParameter(list, "recentGuildIds");
            m.checkNotNullParameter(map, "mostRecent");
            m.checkNotNullParameter(map2, "mentionCounts");
            return new SearchContext(str, list, j, j2, j3, map, map2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SearchContext)) {
                return false;
            }
            SearchContext searchContext = (SearchContext) obj;
            return m.areEqual(this.filter, searchContext.filter) && m.areEqual(this.recentGuildIds, searchContext.recentGuildIds) && this.selectedChannelId == searchContext.selectedChannelId && this.prevSelectedChannelId == searchContext.prevSelectedChannelId && this.selectedVoiceChannelId == searchContext.selectedVoiceChannelId && m.areEqual(this.mostRecent, searchContext.mostRecent) && m.areEqual(this.mentionCounts, searchContext.mentionCounts);
        }

        public final String getFilter() {
            return this.filter;
        }

        public final Collection<Long> getFrecencyChannels() {
            return this.frecencyChannels;
        }

        public final boolean getHasDiscriminator() {
            return this.hasDiscriminator;
        }

        public final Map<Long, Integer> getMentionCounts() {
            return this.mentionCounts;
        }

        public final Map<Long, Long> getMostRecent() {
            return this.mostRecent;
        }

        public final long getPrevSelectedChannelId() {
            return this.prevSelectedChannelId;
        }

        public final List<Long> getRecentGuildIds() {
            return this.recentGuildIds;
        }

        public final String getSanitizedFilter() {
            return this.sanitizedFilter;
        }

        public final int getSearchType() {
            return this.searchType;
        }

        public final long getSelectedChannelId() {
            return this.selectedChannelId;
        }

        public final long getSelectedVoiceChannelId() {
            return this.selectedVoiceChannelId;
        }

        public int hashCode() {
            String str = this.filter;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            List<Long> list = this.recentGuildIds;
            int hashCode2 = list != null ? list.hashCode() : 0;
            int a = a0.a.a.b.a(this.selectedChannelId);
            int a2 = (a0.a.a.b.a(this.selectedVoiceChannelId) + ((a0.a.a.b.a(this.prevSelectedChannelId) + ((a + ((hashCode + hashCode2) * 31)) * 31)) * 31)) * 31;
            Map<Long, Long> map = this.mostRecent;
            int hashCode3 = (a2 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Integer> map2 = this.mentionCounts;
            if (map2 != null) {
                i = map2.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("SearchContext(filter=");
            R.append(this.filter);
            R.append(", recentGuildIds=");
            R.append(this.recentGuildIds);
            R.append(", selectedChannelId=");
            R.append(this.selectedChannelId);
            R.append(", prevSelectedChannelId=");
            R.append(this.prevSelectedChannelId);
            R.append(", selectedVoiceChannelId=");
            R.append(this.selectedVoiceChannelId);
            R.append(", mostRecent=");
            R.append(this.mostRecent);
            R.append(", mentionCounts=");
            return a.L(R, this.mentionCounts, ")");
        }
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001Bw\u0012\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0002\u0012\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u0002\u0012&\u0010\u0012\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\f\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u00020\u0002¢\u0006\u0004\b#\u0010$J \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J \u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J0\u0010\u000e\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\f\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0007J\u0088\u0001\u0010\u0013\u001a\u00020\u00002\u0018\b\u0002\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0018\b\u0002\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u00022\u0018\b\u0002\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u00022(\b\u0002\u0010\u0012\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\f\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR9\u0010\u0012\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\f\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R)\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b \u0010\u0007R)\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b!\u0010\u0007R)\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\"\u0010\u0007¨\u0006%"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;", "", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component1", "()Ljava/util/Map;", "Lcom/discord/models/presence/Presence;", "component2", "", "component3", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/member/GuildMember;", "component4", "users", "presences", "relationships", "members", "copy", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getMembers", "getRelationships", "getPresences", "getUsers", HookHelper.constructorName, "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UsersContext {
        private final Map<Long, Map<Long, GuildMember>> members;
        private final Map<Long, Presence> presences;
        private final Map<Long, Integer> relationships;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public UsersContext(Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, Integer> map3, Map<Long, ? extends Map<Long, GuildMember>> map4) {
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "presences");
            m.checkNotNullParameter(map3, "relationships");
            m.checkNotNullParameter(map4, "members");
            this.users = map;
            this.presences = map2;
            this.relationships = map3;
            this.members = map4;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ UsersContext copy$default(UsersContext usersContext, Map map, Map map2, Map map3, Map map4, int i, Object obj) {
            if ((i & 1) != 0) {
                map = usersContext.users;
            }
            if ((i & 2) != 0) {
                map2 = usersContext.presences;
            }
            if ((i & 4) != 0) {
                map3 = usersContext.relationships;
            }
            if ((i & 8) != 0) {
                map4 = usersContext.members;
            }
            return usersContext.copy(map, map2, map3, map4);
        }

        public final Map<Long, User> component1() {
            return this.users;
        }

        public final Map<Long, Presence> component2() {
            return this.presences;
        }

        public final Map<Long, Integer> component3() {
            return this.relationships;
        }

        public final Map<Long, Map<Long, GuildMember>> component4() {
            return this.members;
        }

        public final UsersContext copy(Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, Integer> map3, Map<Long, ? extends Map<Long, GuildMember>> map4) {
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "presences");
            m.checkNotNullParameter(map3, "relationships");
            m.checkNotNullParameter(map4, "members");
            return new UsersContext(map, map2, map3, map4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UsersContext)) {
                return false;
            }
            UsersContext usersContext = (UsersContext) obj;
            return m.areEqual(this.users, usersContext.users) && m.areEqual(this.presences, usersContext.presences) && m.areEqual(this.relationships, usersContext.relationships) && m.areEqual(this.members, usersContext.members);
        }

        public final Map<Long, Map<Long, GuildMember>> getMembers() {
            return this.members;
        }

        public final Map<Long, Presence> getPresences() {
            return this.presences;
        }

        public final Map<Long, Integer> getRelationships() {
            return this.relationships;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            Map<Long, User> map = this.users;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            Map<Long, Presence> map2 = this.presences;
            int hashCode2 = (hashCode + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, Integer> map3 = this.relationships;
            int hashCode3 = (hashCode2 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, Map<Long, GuildMember>> map4 = this.members;
            if (map4 != null) {
                i = map4.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("UsersContext(users=");
            R.append(this.users);
            R.append(", presences=");
            R.append(this.presences);
            R.append(", relationships=");
            R.append(this.relationships);
            R.append(", members=");
            return a.L(R, this.members, ")");
        }
    }

    static {
        Pattern compile = Pattern.compile("[^\\s]#\\d{0,4}$", 0);
        m.checkNotNullExpressionValue(compile, "java.util.regex.Pattern.compile(this, flags)");
        DISCRIMINATOR_PATTERN = compile;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public WidgetGlobalSearchModel(String str, int i, List<? extends ItemDataPayload> list, List<WidgetGlobalSearchGuildsModel.Item> list2) {
        m.checkNotNullParameter(str, "filter");
        m.checkNotNullParameter(list, "data");
        this.filter = str;
        this.searchType = i;
        this.data = list;
        this.guildsList = list2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WidgetGlobalSearchModel copy$default(WidgetGlobalSearchModel widgetGlobalSearchModel, String str, int i, List list, List list2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = widgetGlobalSearchModel.filter;
        }
        if ((i2 & 2) != 0) {
            i = widgetGlobalSearchModel.searchType;
        }
        if ((i2 & 4) != 0) {
            list = widgetGlobalSearchModel.data;
        }
        if ((i2 & 8) != 0) {
            list2 = widgetGlobalSearchModel.guildsList;
        }
        return widgetGlobalSearchModel.copy(str, i, list, list2);
    }

    public final String component1() {
        return this.filter;
    }

    public final int component2() {
        return this.searchType;
    }

    public final List<ItemDataPayload> component3() {
        return this.data;
    }

    public final List<WidgetGlobalSearchGuildsModel.Item> component4() {
        return this.guildsList;
    }

    public final WidgetGlobalSearchModel copy(String str, int i, List<? extends ItemDataPayload> list, List<WidgetGlobalSearchGuildsModel.Item> list2) {
        m.checkNotNullParameter(str, "filter");
        m.checkNotNullParameter(list, "data");
        return new WidgetGlobalSearchModel(str, i, list, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetGlobalSearchModel)) {
            return false;
        }
        WidgetGlobalSearchModel widgetGlobalSearchModel = (WidgetGlobalSearchModel) obj;
        return m.areEqual(this.filter, widgetGlobalSearchModel.filter) && this.searchType == widgetGlobalSearchModel.searchType && m.areEqual(this.data, widgetGlobalSearchModel.data) && m.areEqual(this.guildsList, widgetGlobalSearchModel.guildsList);
    }

    public final List<ItemDataPayload> getData() {
        return this.data;
    }

    public final String getFilter() {
        return this.filter;
    }

    public final List<WidgetGlobalSearchGuildsModel.Item> getGuildsList() {
        return this.guildsList;
    }

    public final int getSearchType() {
        return this.searchType;
    }

    public int hashCode() {
        String str = this.filter;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.searchType) * 31;
        List<ItemDataPayload> list = this.data;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        List<WidgetGlobalSearchGuildsModel.Item> list2 = this.guildsList;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetGlobalSearchModel(filter=");
        R.append(this.filter);
        R.append(", searchType=");
        R.append(this.searchType);
        R.append(", data=");
        R.append(this.data);
        R.append(", guildsList=");
        return a.K(R, this.guildsList, ")");
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\f\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0012¢\u0006\u0004\b2\u00103J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014JP\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\b2\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u0019\u001a\u00020\u000f2\b\b\u0002\u0010\u001a\u001a\u00020\u0012HÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u0011J\u001a\u0010 \u001a\u00020\u00122\b\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÖ\u0003¢\u0006\u0004\b \u0010!R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\"\u001a\u0004\b#\u0010\nR\u001c\u0010$\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010'\u001a\u0004\b(\u0010\u000eR\u001c\u0010)\u001a\u00020\u000f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010\u0011R\u001c\u0010\u0019\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b,\u0010\u0011R\u001c\u0010\u0016\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010\"\u001a\u0004\b-\u0010\nR\u001c\u0010\u0015\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0015\u0010.\u001a\u0004\b/\u0010\u0007R\u001c\u0010\u001a\u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u00100\u001a\u0004\b1\u0010\u0014¨\u00064"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "", "toString", "()Ljava/lang/String;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "component1", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "component3", "Lcom/discord/models/guild/Guild;", "component4", "()Lcom/discord/models/guild/Guild;", "", "component5", "()I", "", "component6", "()Z", "matchedResult", "channel", "parentChannel", "guild", "mentions", "unread", "copy", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;IZ)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getParentChannel", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/guild/Guild;", "getGuild", "type", "I", "getType", "getMentions", "getChannel", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "getMatchedResult", "Z", "getUnread", HookHelper.constructorName, "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;IZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannel implements ItemDataPayload {
        private final Channel channel;
        private final Guild guild;
        private final String key;
        private final MatchedResult matchedResult;
        private final int mentions;
        private final Channel parentChannel;
        private final int type;
        private final boolean unread;

        public ItemChannel(MatchedResult matchedResult, Channel channel, Channel channel2, Guild guild, int i, boolean z2) {
            m.checkNotNullParameter(matchedResult, "matchedResult");
            m.checkNotNullParameter(channel, "channel");
            this.matchedResult = matchedResult;
            this.channel = channel;
            this.parentChannel = channel2;
            this.guild = guild;
            this.mentions = i;
            this.unread = z2;
            this.key = String.valueOf(getChannel().h());
        }

        public static /* synthetic */ ItemChannel copy$default(ItemChannel itemChannel, MatchedResult matchedResult, Channel channel, Channel channel2, Guild guild, int i, boolean z2, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                matchedResult = itemChannel.getMatchedResult();
            }
            if ((i2 & 2) != 0) {
                channel = itemChannel.getChannel();
            }
            Channel channel3 = channel;
            if ((i2 & 4) != 0) {
                channel2 = itemChannel.parentChannel;
            }
            Channel channel4 = channel2;
            if ((i2 & 8) != 0) {
                guild = itemChannel.guild;
            }
            Guild guild2 = guild;
            if ((i2 & 16) != 0) {
                i = itemChannel.getMentions();
            }
            int i3 = i;
            if ((i2 & 32) != 0) {
                z2 = itemChannel.getUnread();
            }
            return itemChannel.copy(matchedResult, channel3, channel4, guild2, i3, z2);
        }

        public final MatchedResult component1() {
            return getMatchedResult();
        }

        public final Channel component2() {
            return getChannel();
        }

        public final Channel component3() {
            return this.parentChannel;
        }

        public final Guild component4() {
            return this.guild;
        }

        public final int component5() {
            return getMentions();
        }

        public final boolean component6() {
            return getUnread();
        }

        public final ItemChannel copy(MatchedResult matchedResult, Channel channel, Channel channel2, Guild guild, int i, boolean z2) {
            m.checkNotNullParameter(matchedResult, "matchedResult");
            m.checkNotNullParameter(channel, "channel");
            return new ItemChannel(matchedResult, channel, channel2, guild, i, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ItemChannel)) {
                return false;
            }
            ItemChannel itemChannel = (ItemChannel) obj;
            return m.areEqual(getMatchedResult(), itemChannel.getMatchedResult()) && m.areEqual(getChannel(), itemChannel.getChannel()) && m.areEqual(this.parentChannel, itemChannel.parentChannel) && m.areEqual(this.guild, itemChannel.guild) && getMentions() == itemChannel.getMentions() && getUnread() == itemChannel.getUnread();
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public MatchedResult getMatchedResult() {
            return this.matchedResult;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public int getMentions() {
            return this.mentions;
        }

        public final Channel getParentChannel() {
            return this.parentChannel;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public boolean getUnread() {
            return this.unread;
        }

        public int hashCode() {
            MatchedResult matchedResult = getMatchedResult();
            int i = 0;
            int hashCode = (matchedResult != null ? matchedResult.hashCode() : 0) * 31;
            Channel channel = getChannel();
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            Channel channel2 = this.parentChannel;
            int hashCode3 = (hashCode2 + (channel2 != null ? channel2.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            if (guild != null) {
                i = guild.hashCode();
            }
            int mentions = (getMentions() + ((hashCode3 + i) * 31)) * 31;
            boolean unread = getUnread();
            if (unread) {
                unread = true;
            }
            int i2 = unread ? 1 : 0;
            int i3 = unread ? 1 : 0;
            return mentions + i2;
        }

        public String toString() {
            String c = ChannelUtils.c(getChannel());
            return String.valueOf((char) MentionUtilsKt.CHANNELS_CHAR) + c;
        }

        public /* synthetic */ ItemChannel(MatchedResult matchedResult, Channel channel, Channel channel2, Guild guild, int i, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(matchedResult, channel, channel2, guild, (i2 & 16) != 0 ? 0 : i, (i2 & 32) != 0 ? false : z2);
        }
    }

    public /* synthetic */ WidgetGlobalSearchModel(String str, int i, List list, List list2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, i, list, (i2 & 8) != 0 ? null : list2);
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0014\u001a\u00020\u0005\u0012\u0006\u0010\u0015\u001a\u00020\b\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u000b\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0011¢\u0006\u0004\b/\u00100J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013JD\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00052\b\b\u0002\u0010\u0015\u001a\u00020\b2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\u0017\u001a\u00020\u000e2\b\b\u0002\u0010\u0018\u001a\u00020\u0011HÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u0010J\u001a\u0010\u001e\u001a\u00020\u00112\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u001c\u0010\u0018\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010 \u001a\u0004\b!\u0010\u0013R\u001e\u0010\u0016\u001a\u0004\u0018\u00010\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010\"\u001a\u0004\b#\u0010\rR\u001c\u0010\u0017\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0017\u0010$\u001a\u0004\b%\u0010\u0010R\u001c\u0010&\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010\u0004R\u001c\u0010\u0014\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b*\u0010\u0007R\u0019\u0010\u0015\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010+\u001a\u0004\b,\u0010\nR\u001c\u0010-\u001a\u00020\u000e8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b-\u0010$\u001a\u0004\b.\u0010\u0010¨\u00061"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "", "toString", "()Ljava/lang/String;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "component1", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "", "component4", "()I", "", "component5", "()Z", "matchedResult", "guild", "channel", "mentions", "unread", "copy", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;IZ)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getUnread", "Lcom/discord/api/channel/Channel;", "getChannel", "I", "getMentions", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "getMatchedResult", "Lcom/discord/models/guild/Guild;", "getGuild", "type", "getType", HookHelper.constructorName, "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;IZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemGuild implements ItemDataPayload {
        private final Channel channel;
        private final Guild guild;
        private final String key;
        private final MatchedResult matchedResult;
        private final int mentions;
        private final int type;
        private final boolean unread;

        public ItemGuild(MatchedResult matchedResult, Guild guild, Channel channel, int i, boolean z2) {
            m.checkNotNullParameter(matchedResult, "matchedResult");
            m.checkNotNullParameter(guild, "guild");
            this.matchedResult = matchedResult;
            this.guild = guild;
            this.channel = channel;
            this.mentions = i;
            this.unread = z2;
            this.type = 2;
            this.key = String.valueOf(guild.getId());
        }

        public static /* synthetic */ ItemGuild copy$default(ItemGuild itemGuild, MatchedResult matchedResult, Guild guild, Channel channel, int i, boolean z2, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                matchedResult = itemGuild.getMatchedResult();
            }
            if ((i2 & 2) != 0) {
                guild = itemGuild.guild;
            }
            Guild guild2 = guild;
            if ((i2 & 4) != 0) {
                channel = itemGuild.getChannel();
            }
            Channel channel2 = channel;
            if ((i2 & 8) != 0) {
                i = itemGuild.getMentions();
            }
            int i3 = i;
            if ((i2 & 16) != 0) {
                z2 = itemGuild.getUnread();
            }
            return itemGuild.copy(matchedResult, guild2, channel2, i3, z2);
        }

        public final MatchedResult component1() {
            return getMatchedResult();
        }

        public final Guild component2() {
            return this.guild;
        }

        public final Channel component3() {
            return getChannel();
        }

        public final int component4() {
            return getMentions();
        }

        public final boolean component5() {
            return getUnread();
        }

        public final ItemGuild copy(MatchedResult matchedResult, Guild guild, Channel channel, int i, boolean z2) {
            m.checkNotNullParameter(matchedResult, "matchedResult");
            m.checkNotNullParameter(guild, "guild");
            return new ItemGuild(matchedResult, guild, channel, i, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ItemGuild)) {
                return false;
            }
            ItemGuild itemGuild = (ItemGuild) obj;
            return m.areEqual(getMatchedResult(), itemGuild.getMatchedResult()) && m.areEqual(this.guild, itemGuild.guild) && m.areEqual(getChannel(), itemGuild.getChannel()) && getMentions() == itemGuild.getMentions() && getUnread() == itemGuild.getUnread();
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public MatchedResult getMatchedResult() {
            return this.matchedResult;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public int getMentions() {
            return this.mentions;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public boolean getUnread() {
            return this.unread;
        }

        public int hashCode() {
            MatchedResult matchedResult = getMatchedResult();
            int i = 0;
            int hashCode = (matchedResult != null ? matchedResult.hashCode() : 0) * 31;
            Guild guild = this.guild;
            int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
            Channel channel = getChannel();
            if (channel != null) {
                i = channel.hashCode();
            }
            int mentions = (getMentions() + ((hashCode2 + i) * 31)) * 31;
            boolean unread = getUnread();
            if (unread) {
                unread = true;
            }
            int i2 = unread ? 1 : 0;
            int i3 = unread ? 1 : 0;
            return mentions + i2;
        }

        public String toString() {
            String name = this.guild.getName();
            return String.valueOf('*') + name;
        }

        public /* synthetic */ ItemGuild(MatchedResult matchedResult, Guild guild, Channel channel, int i, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(matchedResult, guild, (i2 & 4) != 0 ? null : channel, (i2 & 8) != 0 ? 0 : i, (i2 & 16) != 0 ? false : z2);
        }
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u001c\u001a\u00020\u0005\u0012\u0006\u0010\u001d\u001a\u00020\b\u0012\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u000f\u0012\b\u0010 \u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0015\u0012\b\b\u0002\u0010\"\u001a\u00020\u0018\u0012\b\b\u0002\u0010#\u001a\u00020\u000f¢\u0006\u0004\b>\u0010?J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0011Jj\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001c\u001a\u00020\u00052\b\b\u0002\u0010\u001d\u001a\u00020\b2\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\b\u0002\u0010\u001f\u001a\u00020\u000f2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00152\b\b\u0002\u0010\"\u001a\u00020\u00182\b\b\u0002\u0010#\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010&\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b&\u0010\u001aJ\u001a\u0010)\u001a\u00020\u000f2\b\u0010(\u001a\u0004\u0018\u00010'HÖ\u0003¢\u0006\u0004\b)\u0010*R\u001f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010+\u001a\u0004\b,\u0010\u000eR\u001e\u0010!\u001a\u0004\u0018\u00010\u00158\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u0010-\u001a\u0004\b.\u0010\u0017R\u001b\u0010 \u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b \u0010/\u001a\u0004\b0\u0010\u0014R\u0019\u0010\u001d\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b2\u0010\nR\u001c\u0010#\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b#\u00103\u001a\u0004\b4\u0010\u0011R\u001c\u00105\u001a\u00020\u00188\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u0010\u001aR\u001c\u00108\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010\u0004R\u001c\u0010\u001c\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010;\u001a\u0004\b<\u0010\u0007R\u0019\u0010\u001f\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00103\u001a\u0004\b\u001f\u0010\u0011R\u001c\u0010\"\u001a\u00020\u00188\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u00106\u001a\u0004\b=\u0010\u001a¨\u0006@"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "", "toString", "()Ljava/lang/String;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "component1", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "Lcom/discord/models/user/User;", "component2", "()Lcom/discord/models/user/User;", "", "", "component3", "()Ljava/util/List;", "", "component4", "()Z", "Lcom/discord/models/presence/Presence;", "component5", "()Lcom/discord/models/presence/Presence;", "Lcom/discord/api/channel/Channel;", "component6", "()Lcom/discord/api/channel/Channel;", "", "component7", "()I", "component8", "matchedResult", "user", "aliases", "isFriend", "presence", "channel", "mentions", "unread", "copy", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;Lcom/discord/models/user/User;Ljava/util/List;ZLcom/discord/models/presence/Presence;Lcom/discord/api/channel/Channel;IZ)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getAliases", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/presence/Presence;", "getPresence", "Lcom/discord/models/user/User;", "getUser", "Z", "getUnread", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "getMatchedResult", "getMentions", HookHelper.constructorName, "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;Lcom/discord/models/user/User;Ljava/util/List;ZLcom/discord/models/presence/Presence;Lcom/discord/api/channel/Channel;IZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemUser implements ItemDataPayload {
        private final List<CharSequence> aliases;
        private final Channel channel;
        private final boolean isFriend;
        private final String key;
        private final MatchedResult matchedResult;
        private final int mentions;
        private final Presence presence;
        private final int type;
        private final boolean unread;
        private final User user;

        /* JADX WARN: Multi-variable type inference failed */
        public ItemUser(MatchedResult matchedResult, User user, List<? extends CharSequence> list, boolean z2, Presence presence, Channel channel, int i, boolean z3) {
            m.checkNotNullParameter(matchedResult, "matchedResult");
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(list, "aliases");
            this.matchedResult = matchedResult;
            this.user = user;
            this.aliases = list;
            this.isFriend = z2;
            this.presence = presence;
            this.channel = channel;
            this.mentions = i;
            this.unread = z3;
            this.type = 1;
            this.key = String.valueOf(user.getId());
        }

        public final MatchedResult component1() {
            return getMatchedResult();
        }

        public final User component2() {
            return this.user;
        }

        public final List<CharSequence> component3() {
            return this.aliases;
        }

        public final boolean component4() {
            return this.isFriend;
        }

        public final Presence component5() {
            return this.presence;
        }

        public final Channel component6() {
            return getChannel();
        }

        public final int component7() {
            return getMentions();
        }

        public final boolean component8() {
            return getUnread();
        }

        public final ItemUser copy(MatchedResult matchedResult, User user, List<? extends CharSequence> list, boolean z2, Presence presence, Channel channel, int i, boolean z3) {
            m.checkNotNullParameter(matchedResult, "matchedResult");
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(list, "aliases");
            return new ItemUser(matchedResult, user, list, z2, presence, channel, i, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ItemUser)) {
                return false;
            }
            ItemUser itemUser = (ItemUser) obj;
            return m.areEqual(getMatchedResult(), itemUser.getMatchedResult()) && m.areEqual(this.user, itemUser.user) && m.areEqual(this.aliases, itemUser.aliases) && this.isFriend == itemUser.isFriend && m.areEqual(this.presence, itemUser.presence) && m.areEqual(getChannel(), itemUser.getChannel()) && getMentions() == itemUser.getMentions() && getUnread() == itemUser.getUnread();
        }

        public final List<CharSequence> getAliases() {
            return this.aliases;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public Channel getChannel() {
            return this.channel;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public MatchedResult getMatchedResult() {
            return this.matchedResult;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public int getMentions() {
            return this.mentions;
        }

        public final Presence getPresence() {
            return this.presence;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public boolean getUnread() {
            return this.unread;
        }

        public final User getUser() {
            return this.user;
        }

        public int hashCode() {
            MatchedResult matchedResult = getMatchedResult();
            int i = 0;
            int hashCode = (matchedResult != null ? matchedResult.hashCode() : 0) * 31;
            User user = this.user;
            int hashCode2 = (hashCode + (user != null ? user.hashCode() : 0)) * 31;
            List<CharSequence> list = this.aliases;
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            boolean z2 = this.isFriend;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode3 + i3) * 31;
            Presence presence = this.presence;
            int hashCode4 = (i5 + (presence != null ? presence.hashCode() : 0)) * 31;
            Channel channel = getChannel();
            if (channel != null) {
                i = channel.hashCode();
            }
            int mentions = (getMentions() + ((hashCode4 + i) * 31)) * 31;
            boolean unread = getUnread();
            if (!unread) {
                i2 = unread;
            }
            return mentions + i2;
        }

        public final boolean isFriend() {
            return this.isFriend;
        }

        public String toString() {
            StringBuilder O = a.O(MentionUtilsKt.MENTIONS_CHAR);
            O.append(getMatchedResult().getValue());
            return O.toString();
        }

        public /* synthetic */ ItemUser(MatchedResult matchedResult, User user, List list, boolean z2, Presence presence, Channel channel, int i, boolean z3, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(matchedResult, user, list, (i2 & 8) != 0 ? false : z2, presence, (i2 & 32) != 0 ? null : channel, (i2 & 64) != 0 ? 0 : i, (i2 & 128) != 0 ? false : z3);
        }
    }

    /* compiled from: WidgetGlobalSearchModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\b\b\u0001\u0010\t\u001a\u00020\u0002\u0012\b\b\u0002\u0010\n\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0006¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ.\u0010\f\u001a\u00020\u00002\b\b\u0003\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u001a\u0010\u0014\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001c\u0010\u000b\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000b\u0010\u0016\u001a\u0004\b\u0017\u0010\bR\u001c\u0010\u0019\u001a\u00020\u00188\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u001c\u0010\u001f\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\u0010R\u001c\u0010\n\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\n\u0010\u001d\u001a\u0004\b\"\u0010\u0004R\u001c\u0010#\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b#\u0010\u001d\u001a\u0004\b$\u0010\u0004R\u001e\u0010&\u001a\u0004\u0018\u00010%8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)¨\u0006,"}, d2 = {"Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "", "component1", "()I", "component2", "", "component3", "()Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "mentions", "unread", "copy", "(IIZ)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getUnread", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "matchedResult", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "getMatchedResult", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;", "I", "getName", "key", "Ljava/lang/String;", "getKey", "getMentions", "type", "getType", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", HookHelper.constructorName, "(IIZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemHeader implements ItemDataPayload {
        private final Channel channel;
        private final String key;
        private final MatchedResult matchedResult;
        private final int mentions;
        private final int name;
        private final int type;
        private final boolean unread;

        public ItemHeader(@StringRes int i, int i2, boolean z2) {
            this.name = i;
            this.mentions = i2;
            this.unread = z2;
            this.matchedResult = WidgetGlobalSearchModel.Companion.getEMPTY_MATCH_RESULT();
            this.type = -1;
            this.key = a.p("header", i);
        }

        public static /* synthetic */ ItemHeader copy$default(ItemHeader itemHeader, int i, int i2, boolean z2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = itemHeader.name;
            }
            if ((i3 & 2) != 0) {
                i2 = itemHeader.getMentions();
            }
            if ((i3 & 4) != 0) {
                z2 = itemHeader.getUnread();
            }
            return itemHeader.copy(i, i2, z2);
        }

        public final int component1() {
            return this.name;
        }

        public final int component2() {
            return getMentions();
        }

        public final boolean component3() {
            return getUnread();
        }

        public final ItemHeader copy(@StringRes int i, int i2, boolean z2) {
            return new ItemHeader(i, i2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ItemHeader)) {
                return false;
            }
            ItemHeader itemHeader = (ItemHeader) obj;
            return this.name == itemHeader.name && getMentions() == itemHeader.getMentions() && getUnread() == itemHeader.getUnread();
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public Channel getChannel() {
            return this.channel;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public MatchedResult getMatchedResult() {
            return this.matchedResult;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public int getMentions() {
            return this.mentions;
        }

        public final int getName() {
            return this.name;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        @Override // com.discord.widgets.user.search.WidgetGlobalSearchModel.ItemDataPayload
        public boolean getUnread() {
            return this.unread;
        }

        public int hashCode() {
            int mentions = (getMentions() + (this.name * 31)) * 31;
            boolean unread = getUnread();
            if (unread) {
                unread = true;
            }
            int i = unread ? 1 : 0;
            int i2 = unread ? 1 : 0;
            return mentions + i;
        }

        public String toString() {
            StringBuilder R = a.R("ItemHeader(name=");
            R.append(this.name);
            R.append(", mentions=");
            R.append(getMentions());
            R.append(", unread=");
            R.append(getUnread());
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ ItemHeader(int i, int i2, boolean z2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, (i3 & 2) != 0 ? 0 : i2, (i3 & 4) != 0 ? false : z2);
        }
    }
}
