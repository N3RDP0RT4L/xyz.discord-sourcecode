package com.discord.widgets.user;

import android.content.Context;
import b.a.k.b;
import com.discord.utilities.resources.StringResourceUtilsKt;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetPruneUsers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/content/Context;", "", "days", "", "invoke", "(Landroid/content/Context;I)Ljava/lang/CharSequence;", "formatLastSeenDays"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPruneUsers$onViewBound$1 extends o implements Function2<Context, Integer, CharSequence> {
    public static final WidgetPruneUsers$onViewBound$1 INSTANCE = new WidgetPruneUsers$onViewBound$1();

    public WidgetPruneUsers$onViewBound$1() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ CharSequence invoke(Context context, Integer num) {
        return invoke(context, num.intValue());
    }

    public final CharSequence invoke(Context context, int i) {
        CharSequence b2;
        m.checkNotNullParameter(context, "$this$formatLastSeenDays");
        b2 = b.b(context, R.string.last_seen, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.last_seen_days, i, Integer.valueOf(i))}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }
}
