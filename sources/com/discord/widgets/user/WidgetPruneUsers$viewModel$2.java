package com.discord.widgets.user;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.user.WidgetPruneUsersViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetPruneUsers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPruneUsers$viewModel$2 extends o implements Function0<AppViewModel<WidgetPruneUsersViewModel.ViewState>> {
    public final /* synthetic */ WidgetPruneUsers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetPruneUsers$viewModel$2(WidgetPruneUsers widgetPruneUsers) {
        super(0);
        this.this$0 = widgetPruneUsers;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetPruneUsersViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        return new WidgetPruneUsersViewModel(argumentsOrDefault.getLong("ARG_GUILD_ID", -1L), null, null, null, null, null, 62, null);
    }
}
