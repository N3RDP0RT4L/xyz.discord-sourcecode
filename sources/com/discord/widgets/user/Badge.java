package com.discord.widgets.user;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.DrawableRes;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.user.UserProfile;
import com.discord.models.user.User;
import com.discord.utilities.user.UserProfileUtilsKt;
import com.discord.utilities.user.UserUtils;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: Badge.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001b\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0001&B?\u0012\b\b\u0003\u0010\u000f\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0012\u001a\u00020\t\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJH\u0010\u0014\u001a\u00020\u00002\b\b\u0003\u0010\u000f\u001a\u00020\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0012\u001a\u00020\t2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\u0016\u0010\u000eJ\u0010\u0010\u0017\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0004J\u001a\u0010\u0019\u001a\u00020\t2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001b\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b!\u0010\u000bR\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b#\u0010\u000e¨\u0006'"}, d2 = {"Lcom/discord/widgets/user/Badge;", "", "", "component1", "()I", "", "component2", "()Ljava/lang/CharSequence;", "component3", "", "component4", "()Z", "", "component5", "()Ljava/lang/String;", "icon", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "tooltip", "showPremiumUpSell", "objectType", "copy", "(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/String;)Lcom/discord/widgets/user/Badge;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/CharSequence;", "getTooltip", "I", "getIcon", "getText", "Z", "getShowPremiumUpSell", "Ljava/lang/String;", "getObjectType", HookHelper.constructorName, "(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/String;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Badge {
    public static final Companion Companion = new Companion(null);
    private static final int GUILD_BOOST_LEVEL_1_MONTHS = 1;
    private static final int GUILD_BOOST_LEVEL_2_MONTHS = 2;
    private static final int GUILD_BOOST_LEVEL_3_MONTHS = 3;
    private static final int GUILD_BOOST_LEVEL_4_MONTHS = 6;
    private static final int GUILD_BOOST_LEVEL_5_MONTHS = 9;
    private static final int GUILD_BOOST_LEVEL_6_MONTHS = 12;
    private static final int GUILD_BOOST_LEVEL_7_MONTHS = 15;
    private static final int GUILD_BOOST_LEVEL_8_MONTHS = 18;
    private static final int GUILD_BOOST_LEVEL_9_MONTHS = 24;
    private final int icon;
    private final String objectType;
    private final boolean showPremiumUpSell;
    private final CharSequence text;
    private final CharSequence tooltip;

    /* compiled from: Badge.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b \u0010!J+\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\t\u0010\nJ;\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0017R\u0016\u0010\u001a\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0017R\u0016\u0010\u001b\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0017R\u0016\u0010\u001c\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u0017R\u0016\u0010\u001d\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u0017R\u0016\u0010\u001e\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001e\u0010\u0017R\u0016\u0010\u001f\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u0017¨\u0006\""}, d2 = {"Lcom/discord/widgets/user/Badge$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroid/content/Context;", "context", "Lkotlin/Function1;", "Lcom/discord/widgets/user/Badge;", "", "onBadgeClick", "(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;)Lkotlin/jvm/functions/Function1;", "Lcom/discord/models/user/User;", "user", "Lcom/discord/api/user/UserProfile;", "profile", "", "isMeUserPremium", "isMeUserVerified", "", "getBadgesForUser", "(Lcom/discord/models/user/User;Lcom/discord/api/user/UserProfile;ZZLandroid/content/Context;)Ljava/util/List;", "", "GUILD_BOOST_LEVEL_1_MONTHS", "I", "GUILD_BOOST_LEVEL_2_MONTHS", "GUILD_BOOST_LEVEL_3_MONTHS", "GUILD_BOOST_LEVEL_4_MONTHS", "GUILD_BOOST_LEVEL_5_MONTHS", "GUILD_BOOST_LEVEL_6_MONTHS", "GUILD_BOOST_LEVEL_7_MONTHS", "GUILD_BOOST_LEVEL_8_MONTHS", "GUILD_BOOST_LEVEL_9_MONTHS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final List<Badge> getBadgesForUser(User user, UserProfile userProfile, boolean z2, boolean z3, Context context) {
            CharSequence b2;
            CharSequence b3;
            CharSequence b4;
            CharSequence b5;
            CharSequence b6;
            CharSequence b7;
            CharSequence b8;
            CharSequence b9;
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(userProfile, "profile");
            m.checkNotNullParameter(context, "context");
            ArrayList arrayList = new ArrayList(8);
            UserUtils userUtils = UserUtils.INSTANCE;
            if (userUtils.isStaff(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_staff_32dp, context.getString(R.string.staff_badge_tooltip), null, false, null, 28, null));
            }
            if (userUtils.isPartner(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_partner_32dp, context.getString(R.string.partner_badge_tooltip), null, false, null, 28, null));
            }
            if (userUtils.isCertifiedModerator(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_certified_moderator_32dp, context.getString(R.string.certified_moderator_badge_tooltip), null, false, null, 28, null));
            }
            if (userUtils.isHypeSquad(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_hypesquad_32dp, context.getString(R.string.hypesquad_badge_tooltip), null, false, null, 28, null));
            }
            if (userUtils.isHypesquadHouse1(user)) {
                String string = context.getString(R.string.hypesquad_house_1);
                b8 = b.b(context, R.string.hypesquad_house_1, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                b9 = b.b(context, R.string.hypesquad_online_badge_tooltip, new Object[]{b8}, (r4 & 4) != 0 ? b.C0034b.j : null);
                arrayList.add(new Badge(R.drawable.ic_hypesquad_house1_32dp, string, b9, false, null, 24, null));
            }
            if (userUtils.isHypesquadHouse2(user)) {
                b6 = b.b(context, R.string.hypesquad_house_2, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                b7 = b.b(context, R.string.hypesquad_online_badge_tooltip, new Object[]{context.getString(R.string.hypesquad_house_2)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                arrayList.add(new Badge(R.drawable.ic_hypesquad_house2_32dp, b6, b7, false, null, 24, null));
            }
            if (userUtils.isHypesquadHouse3(user)) {
                String string2 = context.getString(R.string.hypesquad_house_3);
                b4 = b.b(context, R.string.hypesquad_house_3, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                b5 = b.b(context, R.string.hypesquad_online_badge_tooltip, new Object[]{b4}, (r4 & 4) != 0 ? b.C0034b.j : null);
                arrayList.add(new Badge(R.drawable.ic_hypesquad_house3_32dp, string2, b5, false, null, 24, null));
            }
            if (userUtils.isBugHunterLevel1(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_bughunter_level_1_32dp, context.getString(R.string.bug_hunter_badge_tooltip), null, false, null, 28, null));
            }
            if (userUtils.isBugHunterLevel2(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_bughunter_level_2_32dp, context.getString(R.string.bug_hunter_badge_tooltip), null, false, null, 28, null));
            }
            if (userUtils.isVerifiedDeveloper(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_verified_developer_32dp, context.getString(R.string.verified_developer_badge_tooltip), null, false, null, 28, null));
            }
            if (userUtils.isPremiumEarlySupporter(user)) {
                arrayList.add(new Badge(R.drawable.ic_profile_badge_premium_early_supporter_32dp, context.getString(R.string.early_supporter_tooltip), null, !z2 && z3, "PREMIUM_EARLY_SUPPORTER", 4, null));
            }
            if (UserProfileUtilsKt.isPremium(userProfile)) {
                String string3 = context.getString(R.string.premium_title);
                b3 = b.b(context, R.string.premium_badge_tooltip, new Object[]{UserProfileUtilsKt.getPremiumSince(userProfile, context)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                arrayList.add(new Badge(R.drawable.ic_profile_badge_nitro_32dp, string3, b3, !z2 && z3, "PREMIUM"));
            }
            if (UserProfileUtilsKt.isGuildBooster(userProfile)) {
                Integer guildBoostMonthsSubscribed = UserProfileUtilsKt.getGuildBoostMonthsSubscribed(userProfile);
                int intValue = guildBoostMonthsSubscribed != null ? guildBoostMonthsSubscribed.intValue() : 0;
                int i = intValue >= 24 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl9_32dp : intValue >= 18 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl8_32dp : intValue >= 15 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl7_32dp : intValue >= 12 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl6_32dp : intValue >= 9 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl5_32dp : intValue >= 6 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl4_32dp : intValue >= 3 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl3_32dp : intValue >= 2 ? R.drawable.ic_profile_badge_premium_guild_subscription_lvl2_32dp : R.drawable.ic_profile_badge_premium_guild_subscription_lvl1_32dp;
                String string4 = context.getString(R.string.premium_title);
                b2 = b.b(context, R.string.premium_guild_subscription_tooltip, new Object[]{UserProfileUtilsKt.getBoostingSince(userProfile, context)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                arrayList.add(new Badge(i, string4, b2, !z2 && z3, "PREMIUM_GUILD"));
            }
            return arrayList;
        }

        public final Function1<Badge, Unit> onBadgeClick(FragmentManager fragmentManager, Context context) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(context, "context");
            return new Badge$Companion$onBadgeClick$1(fragmentManager, context);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public Badge() {
        this(0, null, null, false, null, 31, null);
    }

    public Badge(@DrawableRes int i, CharSequence charSequence, CharSequence charSequence2, boolean z2, String str) {
        this.icon = i;
        this.text = charSequence;
        this.tooltip = charSequence2;
        this.showPremiumUpSell = z2;
        this.objectType = str;
    }

    public static /* synthetic */ Badge copy$default(Badge badge, int i, CharSequence charSequence, CharSequence charSequence2, boolean z2, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = badge.icon;
        }
        if ((i2 & 2) != 0) {
            charSequence = badge.text;
        }
        CharSequence charSequence3 = charSequence;
        if ((i2 & 4) != 0) {
            charSequence2 = badge.tooltip;
        }
        CharSequence charSequence4 = charSequence2;
        if ((i2 & 8) != 0) {
            z2 = badge.showPremiumUpSell;
        }
        boolean z3 = z2;
        if ((i2 & 16) != 0) {
            str = badge.objectType;
        }
        return badge.copy(i, charSequence3, charSequence4, z3, str);
    }

    public static final Function1<Badge, Unit> onBadgeClick(FragmentManager fragmentManager, Context context) {
        return Companion.onBadgeClick(fragmentManager, context);
    }

    public final int component1() {
        return this.icon;
    }

    public final CharSequence component2() {
        return this.text;
    }

    public final CharSequence component3() {
        return this.tooltip;
    }

    public final boolean component4() {
        return this.showPremiumUpSell;
    }

    public final String component5() {
        return this.objectType;
    }

    public final Badge copy(@DrawableRes int i, CharSequence charSequence, CharSequence charSequence2, boolean z2, String str) {
        return new Badge(i, charSequence, charSequence2, z2, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Badge)) {
            return false;
        }
        Badge badge = (Badge) obj;
        return this.icon == badge.icon && m.areEqual(this.text, badge.text) && m.areEqual(this.tooltip, badge.tooltip) && this.showPremiumUpSell == badge.showPremiumUpSell && m.areEqual(this.objectType, badge.objectType);
    }

    public final int getIcon() {
        return this.icon;
    }

    public final String getObjectType() {
        return this.objectType;
    }

    public final boolean getShowPremiumUpSell() {
        return this.showPremiumUpSell;
    }

    public final CharSequence getText() {
        return this.text;
    }

    public final CharSequence getTooltip() {
        return this.tooltip;
    }

    public int hashCode() {
        int i = this.icon * 31;
        CharSequence charSequence = this.text;
        int i2 = 0;
        int hashCode = (i + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.tooltip;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        boolean z2 = this.showPremiumUpSell;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode2 + i3) * 31;
        String str = this.objectType;
        if (str != null) {
            i2 = str.hashCode();
        }
        return i5 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("Badge(icon=");
        R.append(this.icon);
        R.append(", text=");
        R.append(this.text);
        R.append(", tooltip=");
        R.append(this.tooltip);
        R.append(", showPremiumUpSell=");
        R.append(this.showPremiumUpSell);
        R.append(", objectType=");
        return a.H(R, this.objectType, ")");
    }

    public /* synthetic */ Badge(int i, CharSequence charSequence, CharSequence charSequence2, boolean z2, String str, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? 0 : i, (i2 & 2) != 0 ? null : charSequence, (i2 & 4) != 0 ? null : charSequence2, (i2 & 8) == 0 ? z2 : false, (i2 & 16) != 0 ? null : str);
    }
}
