package com.discord.widgets.user;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetPruneUsersBinding;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.error.Error;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.discord.widgets.user.WidgetPruneUsersViewModel;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetPruneUsers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 #2\u00020\u0001:\u0001#B\u0007¢\u0006\u0004\b\"\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0016\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010!\u001a\u00020\u001d8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u0013\u001a\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsers;", "Lcom/discord/app/AppDialog;", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;", "state", "", "updateUI", "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$ViewState;)V", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/user/WidgetPruneUsersViewModel$Event;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/user/WidgetPruneUsersViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/user/WidgetPruneUsersViewModel;", "viewModel", "Lcom/discord/databinding/WidgetPruneUsersBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetPruneUsersBinding;", "binding", "Lcom/discord/views/RadioManager;", "radioManager$delegate", "getRadioManager", "()Lcom/discord/views/RadioManager;", "radioManager", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPruneUsers extends AppDialog {
    private static final String ARG_GUILD_ID = "ARG_GUILD_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetPruneUsers$binding$2.INSTANCE, null, 2, null);
    private final Lazy radioManager$delegate = g.lazy(new WidgetPruneUsers$radioManager$2(this));
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetPruneUsers.class, "binding", "getBinding()Lcom/discord/databinding/WidgetPruneUsersBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetPruneUsers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/user/WidgetPruneUsers$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "create", "(JLandroidx/fragment/app/FragmentManager;)V", "", WidgetPruneUsers.ARG_GUILD_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(long j, FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetPruneUsers widgetPruneUsers = new WidgetPruneUsers();
            Bundle bundle = new Bundle();
            bundle.putLong(WidgetPruneUsers.ARG_GUILD_ID, j);
            widgetPruneUsers.setArguments(bundle);
            widgetPruneUsers.show(fragmentManager, WidgetPruneUsers.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            WidgetPruneUsersViewModel.PruneDays.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[WidgetPruneUsersViewModel.PruneDays.Seven.ordinal()] = 1;
            iArr[WidgetPruneUsersViewModel.PruneDays.Thirty.ordinal()] = 2;
        }
    }

    public WidgetPruneUsers() {
        super(R.layout.widget_prune_users);
        WidgetPruneUsers$viewModel$2 widgetPruneUsers$viewModel$2 = new WidgetPruneUsers$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetPruneUsersViewModel.class), new WidgetPruneUsers$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetPruneUsers$viewModel$2));
    }

    public static final void create(long j, FragmentManager fragmentManager) {
        Companion.create(j, fragmentManager);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetPruneUsersBinding getBinding() {
        return (WidgetPruneUsersBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final RadioManager getRadioManager() {
        return (RadioManager) this.radioManager$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetPruneUsersViewModel getViewModel() {
        return (WidgetPruneUsersViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(WidgetPruneUsersViewModel.Event event) {
        Unit unit;
        if (event instanceof WidgetPruneUsersViewModel.Event.Dismiss) {
            dismiss();
            unit = Unit.a;
        } else if (event instanceof WidgetPruneUsersViewModel.Event.RestClientFailed) {
            Error.handle(((WidgetPruneUsersViewModel.Event.RestClientFailed) event).getThrowable(), "restClient", null, getContext());
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateUI(WidgetPruneUsersViewModel.ViewState viewState) {
        Unit unit;
        CharSequence e;
        CheckedSetting checkedSetting;
        if (viewState instanceof WidgetPruneUsersViewModel.ViewState.Loading) {
            StringBuilder sb = new StringBuilder();
            e = b.e(this, R.string.prune_members, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            sb.append(e);
            sb.append(" - ");
            WidgetPruneUsersViewModel.ViewState.Loading loading = (WidgetPruneUsersViewModel.ViewState.Loading) viewState;
            sb.append(loading.getGuildName());
            String sb2 = sb.toString();
            TextView textView = getBinding().c;
            m.checkNotNullExpressionValue(textView, "binding.pruneUserHeader");
            textView.setText(sb2);
            int ordinal = loading.getWhichPruneDays().ordinal();
            if (ordinal == 0) {
                checkedSetting = getBinding().f;
            } else if (ordinal == 1) {
                checkedSetting = getBinding().g;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            RadioManager radioManager = getRadioManager();
            m.checkNotNullExpressionValue(checkedSetting, "button");
            radioManager.a(checkedSetting);
            ProgressBar progressBar = getBinding().h;
            m.checkNotNullExpressionValue(progressBar, "binding.pruneUsersLoadingProgressbar");
            progressBar.setVisibility(0);
            TextView textView2 = getBinding().e;
            m.checkNotNullExpressionValue(textView2, "binding.pruneUsersEstimateText");
            textView2.setVisibility(8);
            MaterialButton materialButton = getBinding().d;
            m.checkNotNullExpressionValue(materialButton, "binding.pruneUserPrune");
            ViewExtensions.disable(materialButton);
            unit = Unit.a;
        } else if (viewState instanceof WidgetPruneUsersViewModel.ViewState.Loaded) {
            ProgressBar progressBar2 = getBinding().h;
            m.checkNotNullExpressionValue(progressBar2, "binding.pruneUsersLoadingProgressbar");
            progressBar2.setVisibility(8);
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            WidgetPruneUsersViewModel.ViewState.Loaded loaded = (WidgetPruneUsersViewModel.ViewState.Loaded) viewState;
            CharSequence i18nPluralString = StringResourceUtilsKt.getI18nPluralString(requireContext, R.plurals.form_help_last_seen_members, loaded.getPruneCount(), Integer.valueOf(loaded.getPruneCount()));
            Context requireContext2 = requireContext();
            m.checkNotNullExpressionValue(requireContext2, "requireContext()");
            CharSequence i18nPluralString2 = StringResourceUtilsKt.getI18nPluralString(requireContext2, R.plurals.form_help_last_seen_days, loaded.getPruneDays().getCount(), Integer.valueOf(loaded.getPruneDays().getCount()));
            TextView textView3 = getBinding().e;
            b.m(textView3, R.string.form_help_last_seen, new Object[]{i18nPluralString, i18nPluralString2}, (r4 & 4) != 0 ? b.g.j : null);
            textView3.setVisibility(0);
            MaterialButton materialButton2 = getBinding().d;
            m.checkNotNullExpressionValue(materialButton2, "binding.pruneUserPrune");
            materialButton2.setEnabled(loaded.getPruneButtonEnabled());
            unit = Unit.a;
        } else if (!(viewState instanceof WidgetPruneUsersViewModel.ViewState.LoadFailed)) {
            throw new NoWhenBranchMatchedException();
        } else if (((WidgetPruneUsersViewModel.ViewState.LoadFailed) viewState).getDismiss()) {
            dismiss();
            unit = Unit.a;
        } else {
            ProgressBar progressBar3 = getBinding().h;
            m.checkNotNullExpressionValue(progressBar3, "binding.pruneUsersLoadingProgressbar");
            progressBar3.setVisibility(8);
            TextView textView4 = getBinding().e;
            m.checkNotNullExpressionValue(textView4, "binding.pruneUsersEstimateText");
            textView4.setVisibility(8);
            MaterialButton materialButton3 = getBinding().d;
            m.checkNotNullExpressionValue(materialButton3, "binding.pruneUserPrune");
            ViewExtensions.disable(materialButton3);
            unit = Unit.a;
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        WidgetPruneUsers$onViewBound$1 widgetPruneUsers$onViewBound$1 = WidgetPruneUsers$onViewBound$1.INSTANCE;
        CheckedSetting checkedSetting = getBinding().f;
        Context context = checkedSetting.getContext();
        m.checkNotNullExpressionValue(context, "context");
        checkedSetting.setText(widgetPruneUsers$onViewBound$1.invoke(context, 7));
        checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetPruneUsers$onViewBound$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetPruneUsersViewModel viewModel;
                viewModel = WidgetPruneUsers.this.getViewModel();
                viewModel.pruneDaysSelected(WidgetPruneUsersViewModel.PruneDays.Seven);
            }
        });
        CheckedSetting checkedSetting2 = getBinding().g;
        Context context2 = checkedSetting2.getContext();
        m.checkNotNullExpressionValue(context2, "context");
        checkedSetting2.setText(widgetPruneUsers$onViewBound$1.invoke(context2, 30));
        checkedSetting2.e(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetPruneUsers$onViewBound$$inlined$apply$lambda$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetPruneUsersViewModel viewModel;
                viewModel = WidgetPruneUsers.this.getViewModel();
                viewModel.pruneDaysSelected(WidgetPruneUsersViewModel.PruneDays.Thirty);
            }
        });
        getBinding().f2491b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetPruneUsers$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetPruneUsers.this.dismiss();
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetPruneUsers$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetPruneUsersViewModel viewModel;
                viewModel = WidgetPruneUsers.this.getViewModel();
                viewModel.pruneClicked();
            }
        });
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<WidgetPruneUsersViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel.observeViewSta…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetPruneUsers.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPruneUsers$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetPruneUsers.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPruneUsers$onViewBoundOrOnResume$2(this));
    }
}
