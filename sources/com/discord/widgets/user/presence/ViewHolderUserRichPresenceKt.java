package com.discord.widgets.user.presence;

import kotlin.Metadata;
/* compiled from: ViewHolderUserRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0010\u000e\n\u0002\b\u0002*d\b\u0002\u0010\n\"&\u0012\u0004\u0012\u0002`\u0001\u0012\u0004\u0012\u0002`\u0002\u0012\u0004\u0012\u0002`\u0003\u0012\u0004\u0012\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u000026\u0012\b\u0012\u00060\u0007j\u0002`\u0001\u0012\b\u0012\u00060\bj\u0002`\u0002\u0012\b\u0012\u00060\tj\u0002`\u0003\u0012\b\u0012\u00060\bj\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0000¨\u0006\u000b"}, d2 = {"Lkotlin/Function5;", "Lcom/discord/app/ApplicationContext;", "Lcom/discord/primitives/UserId;", "Lcom/discord/primitives/SessionId;", "Lcom/discord/primitives/ApplicationId;", "", "", "Landroid/content/Context;", "", "", "ActivityCustomButtonClickHandler", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewHolderUserRichPresenceKt {
}
