package com.discord.widgets.user.presence;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.core.view.ViewKt;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityAssets;
import com.discord.api.activity.ActivityPlatform;
import com.discord.api.activity.ActivityTimestamps;
import com.discord.models.user.User;
import com.discord.utilities.integrations.SpotifyHelper;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ViewHolderMusicRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001Bg\u0012\u0006\u0010&\u001a\u00020%\u0012\u0006\u0010(\u001a\u00020'\u0012\u0006\u0010)\u001a\u00020'\u0012\u0006\u0010*\u001a\u00020\u001e\u0012\u0006\u0010+\u001a\u00020\u001e\u0012\u0006\u0010,\u001a\u00020\u001e\u0012\u0006\u0010-\u001a\u00020\u001e\u0012\u0006\u0010.\u001a\u00020%\u0012\u0006\u00100\u001a\u00020/\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\u0006\u0010$\u001a\u00020\u001e¢\u0006\u0004\b1\u00102JQ\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00062\b\u0010\t\u001a\u0004\u0018\u00010\b2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000f\u001a\u00020\u0006H\u0017¢\u0006\u0004\b\u0011\u0010\u0012J#\u0010\u0015\u001a\u00020\u00102\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0014¢\u0006\u0004\b\u0015\u0010\u0016J\u0019\u0010\u0019\u001a\u00020\u00102\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017H\u0014¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010$\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010 ¨\u00063"}, d2 = {"Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "", "isMe", "Lcom/discord/models/user/User;", "user", "Landroid/content/Context;", "Lcom/discord/app/ApplicationContext;", "applicationContext", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "richPresence", "userInSameVoiceChannel", "", "configureUi", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/user/User;Landroid/content/Context;Lcom/discord/widgets/user/presence/ModelRichPresence;Z)V", "Lcom/discord/api/activity/Activity;", "model", "configureAssetUi", "(Lcom/discord/api/activity/Activity;Lcom/discord/utilities/streams/StreamContext;)V", "Lcom/discord/api/activity/ActivityTimestamps;", "timestamps", "setTimeTextViews", "(Lcom/discord/api/activity/ActivityTimestamps;)V", "Landroid/widget/SeekBar;", "richPresenceSeekbar", "Landroid/widget/SeekBar;", "Landroid/widget/TextView;", "richPresenceMusicDuration", "Landroid/widget/TextView;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "richPresenceMusicElapsed", "Landroid/view/View;", "root", "Lcom/facebook/drawee/view/SimpleDraweeView;", "richPresenceImageLarge", "richPresenceImageSmall", "richPresenceHeader", "richPresenceTitle", "richPresenceDetails", "richPresenceTime", "richPresenceTextContainer", "Landroid/widget/Button;", "richPresencePrimaryButton", HookHelper.constructorName, "(Landroid/view/View;Lcom/facebook/drawee/view/SimpleDraweeView;Lcom/facebook/drawee/view/SimpleDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/Button;Landroid/widget/SeekBar;Landroid/widget/TextView;Landroid/widget/TextView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewHolderMusicRichPresence extends ViewHolderUserRichPresence {
    private final Clock clock = ClockFactory.get();
    private final TextView richPresenceMusicDuration;
    private final TextView richPresenceMusicElapsed;
    private final SeekBar richPresenceSeekbar;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewHolderMusicRichPresence(View view, SimpleDraweeView simpleDraweeView, SimpleDraweeView simpleDraweeView2, TextView textView, TextView textView2, TextView textView3, TextView textView4, View view2, Button button, SeekBar seekBar, TextView textView5, TextView textView6) {
        super(view, simpleDraweeView, simpleDraweeView2, textView, textView2, textView3, textView4, null, view2, button, null, 2);
        m.checkNotNullParameter(view, "root");
        m.checkNotNullParameter(simpleDraweeView, "richPresenceImageLarge");
        m.checkNotNullParameter(simpleDraweeView2, "richPresenceImageSmall");
        m.checkNotNullParameter(textView, "richPresenceHeader");
        m.checkNotNullParameter(textView2, "richPresenceTitle");
        m.checkNotNullParameter(textView3, "richPresenceDetails");
        m.checkNotNullParameter(textView4, "richPresenceTime");
        m.checkNotNullParameter(view2, "richPresenceTextContainer");
        m.checkNotNullParameter(button, "richPresencePrimaryButton");
        m.checkNotNullParameter(seekBar, "richPresenceSeekbar");
        m.checkNotNullParameter(textView5, "richPresenceMusicDuration");
        m.checkNotNullParameter(textView6, "richPresenceMusicElapsed");
        this.richPresenceSeekbar = seekBar;
        this.richPresenceMusicDuration = textView5;
        this.richPresenceMusicElapsed = textView6;
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    public void configureAssetUi(Activity activity, StreamContext streamContext) {
        super.configureAssetUi(activity, streamContext);
        SimpleDraweeView richPresenceImageSmall = getRichPresenceImageSmall();
        if (richPresenceImageSmall != null) {
            ViewKt.setVisible(richPresenceImageSmall, true);
        }
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    @MainThread
    public void configureUi(FragmentManager fragmentManager, StreamContext streamContext, final boolean z2, final User user, Context context, ModelRichPresence modelRichPresence, boolean z3) {
        CharSequence b2;
        CharSequence b3;
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "applicationContext");
        disposeTimer();
        Context context2 = getRoot().getContext();
        final Activity primaryActivity = modelRichPresence != null ? modelRichPresence.getPrimaryActivity() : null;
        int i = 8;
        if (primaryActivity == null || !ActivityUtilsKt.isRichPresence(primaryActivity)) {
            getRoot().setVisibility(8);
            return;
        }
        getRoot().setVisibility(0);
        boolean equals = t.equals(primaryActivity.h(), ActivityPlatform.SPOTIFY.name(), true);
        String l = primaryActivity.l();
        String replace$default = l != null ? t.replace$default(l, ';', ',', false, 4, (Object) null) : null;
        TextView richPresenceHeader = getRichPresenceHeader();
        Context context3 = getRichPresenceHeader().getContext();
        m.checkNotNullExpressionValue(context3, "richPresenceHeader.context");
        richPresenceHeader.setText(PresenceUtils.getActivityHeader(context3, primaryActivity));
        getRichPresenceTitle().setText(primaryActivity.e());
        TextView richPresenceDetails = getRichPresenceDetails();
        if (richPresenceDetails != null) {
            m.checkNotNullExpressionValue(context2, "context");
            b3 = b.b(context2, R.string.user_activity_listening_artists, new Object[]{replace$default}, (r4 & 4) != 0 ? b.C0034b.j : null);
            ViewExtensions.setTextAndVisibilityBy(richPresenceDetails, b3);
        }
        TextView richPresenceTime = getRichPresenceTime();
        m.checkNotNullExpressionValue(context2, "context");
        Object[] objArr = new Object[1];
        ActivityAssets b4 = primaryActivity.b();
        objArr[0] = b4 != null ? b4.b() : null;
        b2 = b.b(context2, R.string.user_activity_listening_album, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
        ViewExtensions.setTextAndVisibilityBy(richPresenceTime, b2);
        getRichPresenceTextContainer().setSelected(true);
        configureAssetUi(primaryActivity, streamContext);
        Button richPresencePrimaryButton = getRichPresencePrimaryButton();
        if (richPresencePrimaryButton != null) {
            ViewKt.setVisible(richPresencePrimaryButton, equals);
        }
        this.richPresenceSeekbar.setVisibility(equals ? 0 : 8);
        this.richPresenceMusicDuration.setVisibility(equals ? 0 : 8);
        TextView textView = this.richPresenceMusicElapsed;
        if (equals) {
            i = 0;
        }
        textView.setVisibility(i);
        if (z2) {
            Button richPresencePrimaryButton2 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton2 != null) {
                b.m(richPresencePrimaryButton2, R.string.user_activity_cannot_play_self, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            }
            Button richPresencePrimaryButton3 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton3 != null) {
                richPresencePrimaryButton3.setEnabled(false);
            }
        } else {
            Button richPresencePrimaryButton4 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton4 != null) {
                b.m(richPresencePrimaryButton4, R.string.user_activity_play_on_platform, new Object[]{primaryActivity.h()}, (r4 & 4) != 0 ? b.g.j : null);
            }
            Button richPresencePrimaryButton5 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton5 != null) {
                richPresencePrimaryButton5.setEnabled(true);
            }
        }
        if (SpotifyHelper.INSTANCE.isSpotifyInstalled(context2)) {
            getRichPresenceTitle().setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.presence.ViewHolderMusicRichPresence$configureUi$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SpotifyHelper.INSTANCE.launchTrack(a.x(view, "it", "it.context"), Activity.this);
                }
            });
            Button richPresencePrimaryButton6 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton6 != null) {
                richPresencePrimaryButton6.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.presence.ViewHolderMusicRichPresence$configureUi$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        SpotifyHelper.INSTANCE.launchTrack(a.x(view, "it", "it.context"), Activity.this);
                    }
                });
            }
            getRichPresenceImageLarge().setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.presence.ViewHolderMusicRichPresence$configureUi$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SpotifyHelper spotifyHelper = SpotifyHelper.INSTANCE;
                    Context x2 = a.x(view, "it", "it.context");
                    Activity activity = Activity.this;
                    User user2 = user;
                    spotifyHelper.launchAlbum(x2, activity, user2 != null ? user2.getId() : 0L, z2);
                }
            });
            return;
        }
        Button richPresencePrimaryButton7 = getRichPresencePrimaryButton();
        if (richPresencePrimaryButton7 != null) {
            richPresencePrimaryButton7.setOnClickListener(ViewHolderMusicRichPresence$configureUi$4.INSTANCE);
        }
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    public void setTimeTextViews(ActivityTimestamps activityTimestamps) {
        if (activityTimestamps != null) {
            long currentTimeMillis = this.clock.currentTimeMillis();
            long b2 = activityTimestamps.b() - activityTimestamps.c();
            long c = currentTimeMillis >= activityTimestamps.b() ? b2 : currentTimeMillis - activityTimestamps.c();
            this.richPresenceSeekbar.setProgress((int) ((c / b2) * 100.0f));
            TextView textView = this.richPresenceMusicElapsed;
            TimeUtils timeUtils = TimeUtils.INSTANCE;
            textView.setText(TimeUtils.toFriendlyStringSimple$default(timeUtils, c, null, null, 4, null));
            this.richPresenceMusicDuration.setText(TimeUtils.toFriendlyStringSimple$default(timeUtils, b2, null, null, 4, null));
        }
    }
}
