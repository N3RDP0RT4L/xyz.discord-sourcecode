package com.discord.widgets.user.presence;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.core.view.ViewKt;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityAssets;
import com.discord.api.activity.ActivityTimestamps;
import com.discord.models.guild.Guild;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.extensions.SimpleDraweeViewExtensionsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.presence.StageCallRichPresencePartyData;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.widgets.stage.StageChannelJoinHelper;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ViewHolderStageChannelRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010#\u001a\u00020\u001a\u0012\u0006\u0010$\u001a\u00020\u001a\u0012\u0006\u0010%\u001a\u00020\u001a\u0012\u0006\u0010&\u001a\u00020\u001a\u0012\u0006\u0010'\u001a\u00020\u001f\u0012\u0006\u0010)\u001a\u00020(\u0012\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b*\u0010+J1\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000bJQ\u0010\u0015\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0007\u001a\u00020\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u00112\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u0006\u0010\b\u001a\u00020\u0006H\u0017¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\u0017\u001a\u00020\t2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0015¢\u0006\u0004\b\u0017\u0010\u0018J#\u0010\u0019\u001a\u00020\t2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0015¢\u0006\u0004\b\u0019\u0010\u0018R\u0019\u0010\u001b\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e¨\u0006,"}, d2 = {"Lcom/discord/widgets/user/presence/ViewHolderStageChannelRichPresence;", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/api/activity/Activity;", "model", "", "isMe", "userInSameVoiceChannel", "", "configureButtonUi", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/api/activity/Activity;ZZ)V", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "Lcom/discord/models/user/User;", "user", "Landroid/content/Context;", "Lcom/discord/app/ApplicationContext;", "applicationContext", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "richPresence", "configureUi", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/user/User;Landroid/content/Context;Lcom/discord/widgets/user/presence/ModelRichPresence;Z)V", "configureTextUi", "(Lcom/discord/api/activity/Activity;Lcom/discord/utilities/streams/StreamContext;)V", "configureAssetUi", "Landroid/widget/TextView;", "richPresenceImageLargeText", "Landroid/widget/TextView;", "getRichPresenceImageLargeText", "()Landroid/widget/TextView;", "Landroid/view/View;", "root", "Lcom/facebook/drawee/view/SimpleDraweeView;", "richPresenceImageLarge", "richPresenceHeader", "richPresenceTitle", "richPresenceDetails", "richPresenceTime", "richPresenceTextContainer", "Landroid/widget/Button;", "richPresencePrimaryButton", HookHelper.constructorName, "(Landroid/view/View;Lcom/facebook/drawee/view/SimpleDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/Button;Landroid/widget/TextView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewHolderStageChannelRichPresence extends ViewHolderUserRichPresence {
    private final TextView richPresenceImageLargeText;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewHolderStageChannelRichPresence(View view, SimpleDraweeView simpleDraweeView, TextView textView, TextView textView2, TextView textView3, TextView textView4, View view2, Button button, TextView textView5) {
        super(view, simpleDraweeView, null, textView, textView2, textView3, textView4, null, view2, button, null, 5);
        m.checkNotNullParameter(view, "root");
        m.checkNotNullParameter(simpleDraweeView, "richPresenceImageLarge");
        m.checkNotNullParameter(textView, "richPresenceHeader");
        m.checkNotNullParameter(textView2, "richPresenceTitle");
        m.checkNotNullParameter(textView3, "richPresenceDetails");
        m.checkNotNullParameter(textView4, "richPresenceTime");
        m.checkNotNullParameter(view2, "richPresenceTextContainer");
        m.checkNotNullParameter(button, "richPresencePrimaryButton");
        m.checkNotNullParameter(textView5, "richPresenceImageLargeText");
        this.richPresenceImageLargeText = textView5;
    }

    private final void configureButtonUi(final FragmentManager fragmentManager, Activity activity, final boolean z2, final boolean z3) {
        final StageCallRichPresencePartyData stageChannelRichPresencePartyData = activity != null ? ActivityUtilsKt.getStageChannelRichPresencePartyData(activity) : null;
        int i = 0;
        if (stageChannelRichPresencePartyData == null) {
            Button richPresencePrimaryButton = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton != null) {
                ViewKt.setVisible(richPresencePrimaryButton, false);
                return;
            }
            return;
        }
        final Button richPresencePrimaryButton2 = getRichPresencePrimaryButton();
        if (richPresencePrimaryButton2 != null) {
            if (!(!z2 && !z3)) {
                i = 8;
            }
            richPresencePrimaryButton2.setVisibility(i);
            richPresencePrimaryButton2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.presence.ViewHolderStageChannelRichPresence$configureButtonUi$$inlined$apply$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StageChannelJoinHelper.INSTANCE.lurkAndJoinStage(a.x(view, "it", "it.context"), fragmentManager, ViewCoroutineScopeKt.getCoroutineScope(richPresencePrimaryButton2), stageChannelRichPresencePartyData.getGuildId(), stageChannelRichPresencePartyData.getChannelId(), (r26 & 32) != 0 ? false : true, (r26 & 64) != 0 ? StoreStream.Companion.getGuilds() : null, (r26 & 128) != 0 ? StoreStream.Companion.getLurking() : null, (r26 & 256) != 0 ? StoreStream.Companion.getVoiceChannelSelected() : null);
                }
            });
        }
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    @MainThread
    public void configureAssetUi(Activity activity, StreamContext streamContext) {
        if (activity != null) {
            int i = 8;
            getRichPresenceImageLarge().setVisibility(8);
            StageCallRichPresencePartyData stageChannelRichPresencePartyData = ActivityUtilsKt.getStageChannelRichPresencePartyData(activity);
            ActivityAssets b2 = activity.b();
            String str = null;
            String d = b2 != null ? b2.d() : null;
            ActivityAssets b3 = activity.b();
            if (b3 != null) {
                str = b3.c();
            }
            String str2 = str;
            if (stageChannelRichPresencePartyData == null || d == null) {
                getRoot().setVisibility(8);
                return;
            }
            Guild guild = new Guild(null, null, null, d, null, 0, stageChannelRichPresencePartyData.getGuildId(), null, 0L, str2, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, false, null, -585, 3, null);
            Resources resources = getRichPresenceImageLarge().getResources();
            boolean hasIcon = guild.hasIcon();
            getRichPresenceImageLarge().setVisibility(hasIcon ? 0 : 8);
            TextView textView = this.richPresenceImageLargeText;
            if (!hasIcon) {
                i = 0;
            }
            textView.setVisibility(i);
            if (hasIcon) {
                SimpleDraweeViewExtensionsKt.setGuildIcon(getRichPresenceImageLarge(), true, (r23 & 2) != 0 ? null : guild, resources.getDimensionPixelSize(R.dimen.guild_icon_radius), (r23 & 8) != 0 ? null : Integer.valueOf(IconUtils.getMediaProxySize(resources.getDimensionPixelSize(R.dimen.avatar_size_large))), (r23 & 16) != 0 ? null : null, (r23 & 32) != 0 ? null : null, (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? false : false, (r23 & 256) != 0 ? null : null);
            } else {
                this.richPresenceImageLargeText.setText(guild.getShortName());
            }
        } else {
            throw new IllegalArgumentException("model must not be null");
        }
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    @MainThread
    public void configureTextUi(Activity activity, StreamContext streamContext) {
        String d;
        if (activity != null) {
            getRichPresenceTitle().setText(activity.h());
            TextView richPresenceTime = getRichPresenceTime();
            ActivityTimestamps o = activity.o();
            ViewExtensions.setTextAndVisibilityBy(richPresenceTime, o != null ? friendlyTime(o) : null);
            TextView richPresenceDetails = getRichPresenceDetails();
            if (richPresenceDetails != null) {
                ActivityAssets b2 = activity.b();
                richPresenceDetails.setVisibility((b2 != null ? b2.d() : null) != null ? 0 : 8);
                ActivityAssets b3 = activity.b();
                if (b3 != null && (d = b3.d()) != null) {
                    b.m(richPresenceDetails, R.string.stage_discovery_origin_text, new Object[]{d}, (r4 & 4) != 0 ? b.g.j : null);
                    return;
                }
                return;
            }
            return;
        }
        throw new IllegalArgumentException("model must not be null");
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    @MainThread
    public void configureUi(FragmentManager fragmentManager, StreamContext streamContext, boolean z2, User user, Context context, ModelRichPresence modelRichPresence, boolean z3) {
        Presence presence;
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "applicationContext");
        disposeTimer();
        Activity stageChannelActivity = (modelRichPresence == null || (presence = modelRichPresence.getPresence()) == null) ? null : PresenceUtils.INSTANCE.getStageChannelActivity(presence);
        if (stageChannelActivity == null) {
            getRoot().setVisibility(8);
            return;
        }
        getRoot().setVisibility(0);
        configureTextUi(stageChannelActivity, streamContext);
        configureAssetUi(stageChannelActivity, streamContext);
        configureButtonUi(fragmentManager, stageChannelActivity, z2, z3);
    }

    public final TextView getRichPresenceImageLargeText() {
        return this.richPresenceImageLargeText;
    }
}
