package com.discord.widgets.user.presence;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewKt;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityTimestamps;
import com.discord.api.application.Application;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserConnections;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.playstation.PlaystationExperimentUtilsKt;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import java.util.Collection;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ViewHolderPlatformRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001BG\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\u0006\u0010\u001a\u001a\u00020\u0019\u0012\u0006\u0010\u001b\u001a\u00020\u0019\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0006\u0010\u001e\u001a\u00020\u001c\u0012\u0006\u0010\u001f\u001a\u00020\u001c\u0012\u0006\u0010 \u001a\u00020\u0017\u0012\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b#\u0010$J!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJQ\u0010\u0015\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0014\u001a\u00020\rH\u0017¢\u0006\u0004\b\u0015\u0010\u0016¨\u0006%"}, d2 = {"Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "Lcom/discord/utilities/platform/Platform;", "platform", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "richPresence", "", "configureImages", "(Lcom/discord/utilities/platform/Platform;Lcom/discord/widgets/user/presence/ModelRichPresence;)V", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "", "isMe", "Lcom/discord/models/user/User;", "user", "Landroid/content/Context;", "Lcom/discord/app/ApplicationContext;", "applicationContext", "userInSameVoiceChannel", "configureUi", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/user/User;Landroid/content/Context;Lcom/discord/widgets/user/presence/ModelRichPresence;Z)V", "Landroid/view/View;", "root", "Lcom/facebook/drawee/view/SimpleDraweeView;", "richPresenceImageLarge", "richPresenceImageSmall", "Landroid/widget/TextView;", "richPresenceHeader", "richPresenceTitle", "richPresenceTime", "richPresenceTextContainer", "Landroid/widget/Button;", "richPresencePrimaryButton", HookHelper.constructorName, "(Landroid/view/View;Lcom/facebook/drawee/view/SimpleDraweeView;Lcom/facebook/drawee/view/SimpleDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/Button;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewHolderPlatformRichPresence extends ViewHolderUserRichPresence {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewHolderPlatformRichPresence(View view, SimpleDraweeView simpleDraweeView, SimpleDraweeView simpleDraweeView2, TextView textView, TextView textView2, TextView textView3, View view2, Button button) {
        super(view, simpleDraweeView, simpleDraweeView2, textView, textView2, null, textView3, null, view2, button, null, 3);
        m.checkNotNullParameter(view, "root");
        m.checkNotNullParameter(simpleDraweeView, "richPresenceImageLarge");
        m.checkNotNullParameter(simpleDraweeView2, "richPresenceImageSmall");
        m.checkNotNullParameter(textView, "richPresenceHeader");
        m.checkNotNullParameter(textView2, "richPresenceTitle");
        m.checkNotNullParameter(textView3, "richPresenceTime");
        m.checkNotNullParameter(view2, "richPresenceTextContainer");
        m.checkNotNullParameter(button, "richPresencePrimaryButton");
    }

    private final void configureImages(Platform platform, ModelRichPresence modelRichPresence) {
        Application primaryApplication;
        Drawable drawable;
        String str = null;
        super.configureAssetUi(modelRichPresence != null ? modelRichPresence.getPrimaryActivity() : null, null);
        if (platform != Platform.PLAYSTATION) {
            if (platform != Platform.XBOX || platform.getThemedPlatformImage() == null) {
                Integer whitePlatformImage = platform.getWhitePlatformImage();
                SimpleDraweeView richPresenceImageSmall = getRichPresenceImageSmall();
                if (richPresenceImageSmall != null) {
                    ViewKt.setVisible(richPresenceImageSmall, (whitePlatformImage == null || platform == Platform.NONE) ? false : true);
                }
                if (platform != Platform.NONE) {
                    SimpleDraweeView richPresenceImageSmall2 = getRichPresenceImageSmall();
                    Resources resources = richPresenceImageSmall2 != null ? richPresenceImageSmall2.getResources() : null;
                    if (whitePlatformImage == null || resources == null) {
                        drawable = null;
                    } else {
                        drawable = ResourcesCompat.getDrawable(resources, whitePlatformImage.intValue(), null);
                    }
                    SimpleDraweeView richPresenceImageSmall3 = getRichPresenceImageSmall();
                    if (richPresenceImageSmall3 != null) {
                        richPresenceImageSmall3.setImageDrawable(drawable);
                    }
                }
                if (getRichPresenceImageLarge().getVisibility() != 0) {
                    if (!(modelRichPresence == null || (primaryApplication = modelRichPresence.getPrimaryApplication()) == null)) {
                        long g = primaryApplication.g();
                        String f = primaryApplication.f();
                        if (f == null) {
                            f = "";
                        }
                        str = IconUtils.getApplicationIcon$default(g, f, 0, 4, (Object) null);
                    }
                    getRichPresenceImageLarge().setVisibility(0);
                    MGImages.setImage$default(getRichPresenceImageLarge(), str, 0, 0, false, null, null, 124, null);
                    return;
                }
                return;
            }
            MGImages.setImage$default(MGImages.INSTANCE, getRichPresenceImageLarge(), DrawableCompat.getThemedDrawableRes$default(getRichPresenceImageLarge(), platform.getThemedPlatformImage().intValue(), 0, 2, (Object) null), (MGImages.ChangeDetector) null, 4, (Object) null);
            getRichPresenceImageLarge().setVisibility(0);
        }
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    @MainThread
    public void configureUi(FragmentManager fragmentManager, StreamContext streamContext, boolean z2, User user, Context context, ModelRichPresence modelRichPresence, boolean z3) {
        boolean z4;
        CharSequence b2;
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "applicationContext");
        disposeSubscriptions();
        Activity primaryActivity = modelRichPresence != null ? modelRichPresence.getPrimaryActivity() : null;
        if (primaryActivity == null || !ActivityUtilsKt.isGamePlatform(primaryActivity)) {
            getRoot().setVisibility(8);
            return;
        }
        getRoot().setVisibility(0);
        final Platform from = ActivityUtilsKt.isXboxActivity(primaryActivity) ? Platform.XBOX : Platform.Companion.from(primaryActivity.j());
        TextView richPresenceHeader = getRichPresenceHeader();
        Context context2 = getRichPresenceHeader().getContext();
        m.checkNotNullExpressionValue(context2, "richPresenceHeader.context");
        richPresenceHeader.setText(PresenceUtils.getActivityHeader(context2, primaryActivity));
        getRichPresenceTitle().setText(primaryActivity.h());
        TextView richPresenceTime = getRichPresenceTime();
        ActivityTimestamps o = primaryActivity.o();
        ViewExtensions.setTextAndVisibilityBy(richPresenceTime, o != null ? friendlyTime(o) : null);
        getRichPresenceTextContainer().setSelected(true);
        StoreStream.Companion companion = StoreStream.Companion;
        boolean canSeePlaystationAccountIntegration = PlaystationExperimentUtilsKt.canSeePlaystationAccountIntegration(companion.getExperiments());
        if (!from.getEnabled() || (from == Platform.PLAYSTATION && !canSeePlaystationAccountIntegration)) {
            Button richPresencePrimaryButton = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton != null) {
                ViewKt.setVisible(richPresencePrimaryButton, false);
            }
            Button richPresencePrimaryButton2 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton2 != null) {
                richPresencePrimaryButton2.setOnClickListener(null);
            }
        } else {
            final StoreUserConnections userConnections = companion.getUserConnections();
            StoreUserConnections.State<ConnectedAccount> connectedAccounts = userConnections.getConnectedAccounts();
            if (!(connectedAccounts instanceof Collection) || !connectedAccounts.isEmpty()) {
                for (ConnectedAccount connectedAccount : connectedAccounts) {
                    String g = connectedAccount.g();
                    String name = from.name();
                    Locale locale = Locale.ENGLISH;
                    m.checkNotNullExpressionValue(locale, "Locale.ENGLISH");
                    Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase = name.toLowerCase(locale);
                    m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    if (m.areEqual(g, lowerCase)) {
                        z4 = true;
                        break;
                    }
                }
            }
            z4 = false;
            Button richPresencePrimaryButton3 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton3 != null) {
                ViewKt.setVisible(richPresencePrimaryButton3, !z4);
            }
            Button richPresencePrimaryButton4 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton4 != null) {
                Context context3 = getRoot().getContext();
                m.checkNotNullExpressionValue(context3, "root.context");
                b2 = b.b(context3, R.string.user_activity_connect_platform, new Object[]{from.getProperName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                richPresencePrimaryButton4.setText(b2);
            }
            Button richPresencePrimaryButton5 = getRichPresencePrimaryButton();
            if (richPresencePrimaryButton5 != null) {
                richPresencePrimaryButton5.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.presence.ViewHolderPlatformRichPresence$configureUi$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        StoreUserConnections storeUserConnections = StoreUserConnections.this;
                        String platformId = from.getPlatformId();
                        m.checkNotNullExpressionValue(view, "it");
                        Context context4 = view.getContext();
                        m.checkNotNullExpressionValue(context4, "it.context");
                        storeUserConnections.authorizeConnection(platformId, context4, Traits.Location.Obj.ACTIVITY_ACTION);
                    }
                });
            }
        }
        configureImages(from, modelRichPresence);
    }
}
