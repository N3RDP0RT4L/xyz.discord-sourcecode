package com.discord.widgets.user.presence;

import androidx.core.app.NotificationCompat;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.models.presence.Presence;
import com.discord.stores.StoreStream;
import com.discord.utilities.presence.PresenceUtils;
import j0.k.b;
import j0.l.e.k;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: ModelRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\b\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004 \u0005*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "presence", "Lrx/Observable;", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/presence/Presence;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ModelRichPresence$Companion$get$1<T, R> implements b<Presence, Observable<? extends ModelRichPresence>> {
    public static final ModelRichPresence$Companion$get$1 INSTANCE = new ModelRichPresence$Companion$get$1();

    public final Observable<? extends ModelRichPresence> call(final Presence presence) {
        Long a;
        if (presence == null) {
            return new k(null);
        }
        Activity primaryActivity = PresenceUtils.INSTANCE.getPrimaryActivity(presence);
        if (primaryActivity == null || (a = primaryActivity.a()) == null) {
            return new k(new ModelRichPresence(presence, null, 2, null));
        }
        return (Observable<R>) StoreStream.Companion.getApplication().observeApplication(Long.valueOf(a.longValue())).F(new b<Application, ModelRichPresence>() { // from class: com.discord.widgets.user.presence.ModelRichPresence$Companion$get$1$$special$$inlined$let$lambda$1
            public final ModelRichPresence call(Application application) {
                return new ModelRichPresence(Presence.this, application);
            }
        });
    }
}
