package com.discord.widgets.user.presence;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.fragment.app.FragmentManager;
import com.discord.api.application.Application;
import com.discord.models.user.User;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.streams.StreamContext;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ViewHolderGameRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\u0006\u0010\u001a\u001a\u00020\u0019\u0012\u0006\u0010\u001b\u001a\u00020\u0019\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0006\u0010\u001e\u001a\u00020\u001c\u0012\u0006\u0010\u001f\u001a\u00020\u001c\u0012\u0006\u0010 \u001a\u00020\u001c\u0012\u0006\u0010!\u001a\u00020\u001c\u0012\u0006\u0010\"\u001a\u00020\u0017\u0012\u0006\u0010$\u001a\u00020#\u0012\u0006\u0010%\u001a\u00020#¢\u0006\u0004\b&\u0010'JQ\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00062\b\u0010\t\u001a\u0004\u0018\u00010\b2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000f\u001a\u00020\u0006H\u0017¢\u0006\u0004\b\u0011\u0010\u0012J#\u0010\u0015\u001a\u00020\u00102\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0014¢\u0006\u0004\b\u0015\u0010\u0016¨\u0006("}, d2 = {"Lcom/discord/widgets/user/presence/ViewHolderGameRichPresence;", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "", "isMe", "Lcom/discord/models/user/User;", "user", "Landroid/content/Context;", "Lcom/discord/app/ApplicationContext;", "applicationContext", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "richPresence", "userInSameVoiceChannel", "", "configureUi", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/user/User;Landroid/content/Context;Lcom/discord/widgets/user/presence/ModelRichPresence;Z)V", "Lcom/discord/api/activity/Activity;", "model", "configureTextUi", "(Lcom/discord/api/activity/Activity;Lcom/discord/utilities/streams/StreamContext;)V", "Landroid/view/View;", "root", "Lcom/facebook/drawee/view/SimpleDraweeView;", "richPresenceImageLarge", "richPresenceImageSmall", "Landroid/widget/TextView;", "richPresenceHeader", "richPresenceTitle", "richPresenceDetails", "richPresenceTime", "richPresenceState", "richPresenceTextContainer", "Landroid/widget/Button;", "richPresencePrimaryButton", "richPresenceSecondaryButton", HookHelper.constructorName, "(Landroid/view/View;Lcom/facebook/drawee/view/SimpleDraweeView;Lcom/facebook/drawee/view/SimpleDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/Button;Landroid/widget/Button;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewHolderGameRichPresence extends ViewHolderUserRichPresence {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewHolderGameRichPresence(View view, SimpleDraweeView simpleDraweeView, SimpleDraweeView simpleDraweeView2, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view2, Button button, Button button2) {
        super(view, simpleDraweeView, simpleDraweeView2, textView, textView2, textView3, textView4, textView5, view2, button, button2, 1);
        m.checkNotNullParameter(view, "root");
        m.checkNotNullParameter(simpleDraweeView, "richPresenceImageLarge");
        m.checkNotNullParameter(simpleDraweeView2, "richPresenceImageSmall");
        m.checkNotNullParameter(textView, "richPresenceHeader");
        m.checkNotNullParameter(textView2, "richPresenceTitle");
        m.checkNotNullParameter(textView3, "richPresenceDetails");
        m.checkNotNullParameter(textView4, "richPresenceTime");
        m.checkNotNullParameter(textView5, "richPresenceState");
        m.checkNotNullParameter(view2, "richPresenceTextContainer");
        m.checkNotNullParameter(button, "richPresencePrimaryButton");
        m.checkNotNullParameter(button2, "richPresenceSecondaryButton");
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x0048, code lost:
        if (r1 != null) goto L15;
     */
    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void configureTextUi(com.discord.api.activity.Activity r10, com.discord.utilities.streams.StreamContext r11) {
        /*
            r9 = this;
            if (r10 == 0) goto Lbb
            java.lang.String r11 = r10.l()
            r0 = 0
            if (r11 == 0) goto L6f
            com.discord.api.activity.ActivityParty r1 = r10.i()
            if (r1 == 0) goto L4b
            com.discord.utilities.presence.PresenceUtils r2 = com.discord.utilities.presence.PresenceUtils.INSTANCE
            long r3 = r2.getMaxSize(r1)
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 != 0) goto L1d
        L1b:
            r1 = r0
            goto L48
        L1d:
            android.view.View r3 = r9.getRoot()
            android.content.Context r3 = r3.getContext()
            if (r3 == 0) goto L1b
            r4 = 2131895337(0x7f122429, float:1.9425504E38)
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            long r7 = r2.getCurrentSize(r1)
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r5[r6] = r7
            r6 = 1
            long r1 = r2.getMaxSize(r1)
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r5[r6] = r1
            r1 = 4
            java.lang.CharSequence r1 = b.a.k.b.h(r3, r4, r5, r0, r1)
        L48:
            if (r1 == 0) goto L4b
            goto L4d
        L4b:
            java.lang.String r1 = ""
        L4d:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r11)
            r11 = 32
            r2.append(r11)
            r2.append(r1)
            java.lang.String r11 = r2.toString()
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            java.util.Objects.requireNonNull(r11, r1)
            java.lang.CharSequence r11 = d0.g0.w.trim(r11)
            java.lang.String r11 = r11.toString()
            goto L70
        L6f:
            r11 = r0
        L70:
            android.widget.TextView r1 = r9.getRichPresenceHeader()
            android.widget.TextView r2 = r9.getRichPresenceHeader()
            android.content.Context r2 = r2.getContext()
            java.lang.String r3 = "richPresenceHeader.context"
            d0.z.d.m.checkNotNullExpressionValue(r2, r3)
            java.lang.CharSequence r2 = com.discord.utilities.presence.PresenceUtils.getActivityHeader(r2, r10)
            r1.setText(r2)
            android.widget.TextView r1 = r9.getRichPresenceTitle()
            java.lang.String r2 = r10.h()
            r1.setText(r2)
            android.widget.TextView r1 = r9.getRichPresenceDetails()
            if (r1 == 0) goto La0
            java.lang.String r2 = r10.e()
            com.discord.utilities.view.extensions.ViewExtensions.setTextAndVisibilityBy(r1, r2)
        La0:
            android.widget.TextView r1 = r9.getRichPresenceState()
            if (r1 == 0) goto La9
            com.discord.utilities.view.extensions.ViewExtensions.setTextAndVisibilityBy(r1, r11)
        La9:
            android.widget.TextView r11 = r9.getRichPresenceTime()
            com.discord.api.activity.ActivityTimestamps r10 = r10.o()
            if (r10 == 0) goto Lb7
            java.lang.CharSequence r0 = r9.friendlyTime(r10)
        Lb7:
            com.discord.utilities.view.extensions.ViewExtensions.setTextAndVisibilityBy(r11, r0)
            return
        Lbb:
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.String r11 = "model must not be null"
            r10.<init>(r11)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.presence.ViewHolderGameRichPresence.configureTextUi(com.discord.api.activity.Activity, com.discord.utilities.streams.StreamContext):void");
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    @MainThread
    public void configureUi(FragmentManager fragmentManager, StreamContext streamContext, boolean z2, User user, Context context, ModelRichPresence modelRichPresence, boolean z3) {
        String str;
        Application primaryApplication;
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "applicationContext");
        super.configureUi(fragmentManager, streamContext, z2, user, context, modelRichPresence, z3);
        if (getRichPresenceImageLarge().getVisibility() != 0) {
            if (modelRichPresence == null || (primaryApplication = modelRichPresence.getPrimaryApplication()) == null) {
                str = null;
            } else {
                long g = primaryApplication.g();
                String f = primaryApplication.f();
                if (f == null) {
                    f = "";
                }
                str = IconUtils.getApplicationIcon$default(g, f, 0, 4, (Object) null);
            }
            getRichPresenceImageLarge().setVisibility(0);
            MGImages.setImage$default(getRichPresenceImageLarge(), str, 0, 0, false, null, null, 124, null);
        }
    }
}
