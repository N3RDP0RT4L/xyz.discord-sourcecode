package com.discord.widgets.user.presence;

import androidx.core.app.NotificationCompat;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityAssets;
import com.discord.models.presence.Presence;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.presence.StageCallRichPresencePartyData;
import d0.f0.q;
import d0.t.m0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.o;
import j0.k.b;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: ModelStageRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000b\u001a\u0016\u0012\u0004\u0012\u00020\b \u0005*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00070\u000726\u0010\u0006\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004 \u0005*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\t\u0010\n"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "kotlin.jvm.PlatformType", "userPresences", "", "Lcom/discord/widgets/user/presence/ModelStageRichPresence;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ModelStageRichPresence$Companion$observeStageRichPresence$1<T, R> implements b<Map<Long, ? extends Presence>, List<? extends ModelStageRichPresence>> {
    public static final ModelStageRichPresence$Companion$observeStageRichPresence$1 INSTANCE = new ModelStageRichPresence$Companion$observeStageRichPresence$1();

    /* compiled from: ModelStageRichPresence.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/widgets/user/presence/ModelStageRichPresence;", "invoke", "(J)Lcom/discord/widgets/user/presence/ModelStageRichPresence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.presence.ModelStageRichPresence$Companion$observeStageRichPresence$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Long, ModelStageRichPresence> {
        public final /* synthetic */ Map $userPresences;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Map map) {
            super(1);
            this.$userPresences = map;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ ModelStageRichPresence invoke(Long l) {
            return invoke(l.longValue());
        }

        public final ModelStageRichPresence invoke(long j) {
            List<Activity> activities;
            T t;
            StageCallRichPresencePartyData stageChannelRichPresencePartyData;
            String str;
            Presence presence = (Presence) this.$userPresences.get(Long.valueOf(j));
            String str2 = null;
            if (!(presence == null || (activities = presence.getActivities()) == null)) {
                Iterator<T> it = activities.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ActivityUtilsKt.isStageChannelActivity((Activity) t)) {
                        break;
                    }
                }
                Activity activity = (Activity) t;
                if (!(activity == null || (stageChannelRichPresencePartyData = ActivityUtilsKt.getStageChannelRichPresencePartyData(activity)) == null)) {
                    long channelId = stageChannelRichPresencePartyData.getChannelId();
                    long stageInstanceId = stageChannelRichPresencePartyData.getStageInstanceId();
                    boolean userIsSpeaker = stageChannelRichPresencePartyData.getUserIsSpeaker();
                    long guildId = stageChannelRichPresencePartyData.getGuildId();
                    boolean guildIsPartnered = stageChannelRichPresencePartyData.getGuildIsPartnered();
                    boolean guildIsVerified = stageChannelRichPresencePartyData.getGuildIsVerified();
                    ActivityAssets b2 = activity.b();
                    if (b2 != null) {
                        str2 = b2.c();
                    }
                    String str3 = str2;
                    ActivityAssets b3 = activity.b();
                    if (b3 == null || (str = b3.d()) == null) {
                        str = "";
                    }
                    return new ModelStageRichPresence(channelId, stageInstanceId, userIsSpeaker, guildId, guildIsPartnered, guildIsVerified, str, str3, activity.h(), m0.setOf(Long.valueOf(j)), stageChannelRichPresencePartyData.getUserIsSpeaker() ? m0.setOf(Long.valueOf(j)) : n0.emptySet(), stageChannelRichPresencePartyData.getSpeakerCount(), stageChannelRichPresencePartyData.getAudienceSize());
                }
            }
            return null;
        }
    }

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends ModelStageRichPresence> call(Map<Long, ? extends Presence> map) {
        return call2((Map<Long, Presence>) map);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<ModelStageRichPresence> call2(Map<Long, Presence> map) {
        return q.toList(q.mapNotNull(u.asSequence(map.keySet()), new AnonymousClass1(map)));
    }
}
