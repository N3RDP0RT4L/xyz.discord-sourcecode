package com.discord.widgets.user.presence;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserPresence;
import d0.z.d.m;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: ModelStageRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\u001a\b\u0086\b\u0018\u0000 D2\u00020\u0001:\u0001DB\u0091\u0001\u0012\n\u0010\u001b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u001c\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\u001d\u001a\u00020\b\u0012\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u000b\u0012\u0006\u0010\u001f\u001a\u00020\b\u0012\u0006\u0010 \u001a\u00020\b\u0012\u0006\u0010!\u001a\u00020\u000f\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010#\u001a\u00020\u000f\u0012\u0010\u0010$\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u0014\u0012\u0010\u0010%\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u0014\u0012\u0006\u0010&\u001a\u00020\u0002\u0012\u0006\u0010'\u001a\u00020\u0002¢\u0006\u0004\bB\u0010CJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\u0002j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\u0005J\u0010\u0010\r\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\r\u0010\nJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0011J\u001a\u0010\u0016\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u0014HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0018\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u0014HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0005J\u0010\u0010\u001a\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0005J´\u0001\u0010(\u001a\u00020\u00002\f\b\u0002\u0010\u001b\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u001c\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\u001d\u001a\u00020\b2\f\b\u0002\u0010\u001e\u001a\u00060\u0002j\u0002`\u000b2\b\b\u0002\u0010\u001f\u001a\u00020\b2\b\b\u0002\u0010 \u001a\u00020\b2\b\b\u0002\u0010!\u001a\u00020\u000f2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010#\u001a\u00020\u000f2\u0012\b\u0002\u0010$\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u00142\u0012\b\u0002\u0010%\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u00142\b\b\u0002\u0010&\u001a\u00020\u00022\b\b\u0002\u0010'\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b(\u0010)J\u0010\u0010*\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b*\u0010\u0011J\u0010\u0010,\u001a\u00020+HÖ\u0001¢\u0006\u0004\b,\u0010-J\u001a\u0010/\u001a\u00020\b2\b\u0010.\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b/\u00100R\u0019\u0010#\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00101\u001a\u0004\b2\u0010\u0011R\u0019\u0010\u001f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00103\u001a\u0004\b4\u0010\nR\u0019\u0010 \u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b \u00103\u001a\u0004\b5\u0010\nR#\u0010%\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u00148\u0006@\u0006¢\u0006\f\n\u0004\b%\u00106\u001a\u0004\b7\u0010\u0017R\u0019\u0010'\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b'\u00108\u001a\u0004\b9\u0010\u0005R\u001d\u0010\u001b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00108\u001a\u0004\b:\u0010\u0005R\u001d\u0010\u001c\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00108\u001a\u0004\b;\u0010\u0005R\u0019\u0010!\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00101\u001a\u0004\b<\u0010\u0011R\u0019\u0010&\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b&\u00108\u001a\u0004\b=\u0010\u0005R\u0019\u0010\u001d\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00103\u001a\u0004\b>\u0010\nR\u001d\u0010\u001e\u001a\u00060\u0002j\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00108\u001a\u0004\b?\u0010\u0005R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00101\u001a\u0004\b@\u0010\u0011R#\u0010$\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00150\u00148\u0006@\u0006¢\u0006\f\n\u0004\b$\u00106\u001a\u0004\bA\u0010\u0017¨\u0006E"}, d2 = {"Lcom/discord/widgets/user/presence/ModelStageRichPresence;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/StageInstanceId;", "component2", "", "component3", "()Z", "Lcom/discord/primitives/GuildId;", "component4", "component5", "component6", "", "component7", "()Ljava/lang/String;", "component8", "component9", "", "Lcom/discord/primitives/UserId;", "component10", "()Ljava/util/Set;", "component11", "component12", "component13", "channelId", "stageInstanceId", "userIsSpeaker", "guildId", "guildIsPartnered", "guildIsVerified", "guildName", "guildIcon", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "knownUserIds", "speakerIds", "speakerCount", "audienceSize", "copy", "(JJZJZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;JJ)Lcom/discord/widgets/user/presence/ModelStageRichPresence;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getTopic", "Z", "getGuildIsPartnered", "getGuildIsVerified", "Ljava/util/Set;", "getSpeakerIds", "J", "getAudienceSize", "getChannelId", "getStageInstanceId", "getGuildName", "getSpeakerCount", "getUserIsSpeaker", "getGuildId", "getGuildIcon", "getKnownUserIds", HookHelper.constructorName, "(JJZJZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;JJ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ModelStageRichPresence {
    public static final Companion Companion = new Companion(null);
    private final long audienceSize;
    private final long channelId;
    private final String guildIcon;
    private final long guildId;
    private final boolean guildIsPartnered;
    private final boolean guildIsVerified;
    private final String guildName;
    private final Set<Long> knownUserIds;
    private final long speakerCount;
    private final Set<Long> speakerIds;
    private final long stageInstanceId;
    private final String topic;
    private final boolean userIsSpeaker;

    /* compiled from: ModelStageRichPresence.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/user/presence/ModelStageRichPresence$Companion;", "", "Lcom/discord/stores/StoreUserPresence;", "storeUserPresence", "Lrx/Observable;", "", "Lcom/discord/widgets/user/presence/ModelStageRichPresence;", "observeStageRichPresence", "(Lcom/discord/stores/StoreUserPresence;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ Observable observeStageRichPresence$default(Companion companion, StoreUserPresence storeUserPresence, int i, Object obj) {
            if ((i & 1) != 0) {
                storeUserPresence = StoreStream.Companion.getPresences();
            }
            return companion.observeStageRichPresence(storeUserPresence);
        }

        public final Observable<List<ModelStageRichPresence>> observeStageRichPresence(StoreUserPresence storeUserPresence) {
            m.checkNotNullParameter(storeUserPresence, "storeUserPresence");
            Observable F = storeUserPresence.observeAllPresences().F(ModelStageRichPresence$Companion$observeStageRichPresence$1.INSTANCE);
            m.checkNotNullExpressionValue(F, "storeUserPresence\n      …   }.toList()\n          }");
            return F;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ModelStageRichPresence(long j, long j2, boolean z2, long j3, boolean z3, boolean z4, String str, String str2, String str3, Set<Long> set, Set<Long> set2, long j4, long j5) {
        m.checkNotNullParameter(str, "guildName");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_TOPIC);
        m.checkNotNullParameter(set, "knownUserIds");
        m.checkNotNullParameter(set2, "speakerIds");
        this.channelId = j;
        this.stageInstanceId = j2;
        this.userIsSpeaker = z2;
        this.guildId = j3;
        this.guildIsPartnered = z3;
        this.guildIsVerified = z4;
        this.guildName = str;
        this.guildIcon = str2;
        this.topic = str3;
        this.knownUserIds = set;
        this.speakerIds = set2;
        this.speakerCount = j4;
        this.audienceSize = j5;
    }

    public final long component1() {
        return this.channelId;
    }

    public final Set<Long> component10() {
        return this.knownUserIds;
    }

    public final Set<Long> component11() {
        return this.speakerIds;
    }

    public final long component12() {
        return this.speakerCount;
    }

    public final long component13() {
        return this.audienceSize;
    }

    public final long component2() {
        return this.stageInstanceId;
    }

    public final boolean component3() {
        return this.userIsSpeaker;
    }

    public final long component4() {
        return this.guildId;
    }

    public final boolean component5() {
        return this.guildIsPartnered;
    }

    public final boolean component6() {
        return this.guildIsVerified;
    }

    public final String component7() {
        return this.guildName;
    }

    public final String component8() {
        return this.guildIcon;
    }

    public final String component9() {
        return this.topic;
    }

    public final ModelStageRichPresence copy(long j, long j2, boolean z2, long j3, boolean z3, boolean z4, String str, String str2, String str3, Set<Long> set, Set<Long> set2, long j4, long j5) {
        m.checkNotNullParameter(str, "guildName");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_TOPIC);
        m.checkNotNullParameter(set, "knownUserIds");
        m.checkNotNullParameter(set2, "speakerIds");
        return new ModelStageRichPresence(j, j2, z2, j3, z3, z4, str, str2, str3, set, set2, j4, j5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelStageRichPresence)) {
            return false;
        }
        ModelStageRichPresence modelStageRichPresence = (ModelStageRichPresence) obj;
        return this.channelId == modelStageRichPresence.channelId && this.stageInstanceId == modelStageRichPresence.stageInstanceId && this.userIsSpeaker == modelStageRichPresence.userIsSpeaker && this.guildId == modelStageRichPresence.guildId && this.guildIsPartnered == modelStageRichPresence.guildIsPartnered && this.guildIsVerified == modelStageRichPresence.guildIsVerified && m.areEqual(this.guildName, modelStageRichPresence.guildName) && m.areEqual(this.guildIcon, modelStageRichPresence.guildIcon) && m.areEqual(this.topic, modelStageRichPresence.topic) && m.areEqual(this.knownUserIds, modelStageRichPresence.knownUserIds) && m.areEqual(this.speakerIds, modelStageRichPresence.speakerIds) && this.speakerCount == modelStageRichPresence.speakerCount && this.audienceSize == modelStageRichPresence.audienceSize;
    }

    public final long getAudienceSize() {
        return this.audienceSize;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final String getGuildIcon() {
        return this.guildIcon;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final boolean getGuildIsPartnered() {
        return this.guildIsPartnered;
    }

    public final boolean getGuildIsVerified() {
        return this.guildIsVerified;
    }

    public final String getGuildName() {
        return this.guildName;
    }

    public final Set<Long> getKnownUserIds() {
        return this.knownUserIds;
    }

    public final long getSpeakerCount() {
        return this.speakerCount;
    }

    public final Set<Long> getSpeakerIds() {
        return this.speakerIds;
    }

    public final long getStageInstanceId() {
        return this.stageInstanceId;
    }

    public final String getTopic() {
        return this.topic;
    }

    public final boolean getUserIsSpeaker() {
        return this.userIsSpeaker;
    }

    public int hashCode() {
        int a = (b.a(this.stageInstanceId) + (b.a(this.channelId) * 31)) * 31;
        boolean z2 = this.userIsSpeaker;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int a2 = (b.a(this.guildId) + ((a + i2) * 31)) * 31;
        boolean z3 = this.guildIsPartnered;
        if (z3) {
            z3 = true;
        }
        int i4 = z3 ? 1 : 0;
        int i5 = z3 ? 1 : 0;
        int i6 = (a2 + i4) * 31;
        boolean z4 = this.guildIsVerified;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        int i7 = (i6 + i) * 31;
        String str = this.guildName;
        int i8 = 0;
        int hashCode = (i7 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.guildIcon;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.topic;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Set<Long> set = this.knownUserIds;
        int hashCode4 = (hashCode3 + (set != null ? set.hashCode() : 0)) * 31;
        Set<Long> set2 = this.speakerIds;
        if (set2 != null) {
            i8 = set2.hashCode();
        }
        return b.a(this.audienceSize) + ((b.a(this.speakerCount) + ((hashCode4 + i8) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("ModelStageRichPresence(channelId=");
        R.append(this.channelId);
        R.append(", stageInstanceId=");
        R.append(this.stageInstanceId);
        R.append(", userIsSpeaker=");
        R.append(this.userIsSpeaker);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", guildIsPartnered=");
        R.append(this.guildIsPartnered);
        R.append(", guildIsVerified=");
        R.append(this.guildIsVerified);
        R.append(", guildName=");
        R.append(this.guildName);
        R.append(", guildIcon=");
        R.append(this.guildIcon);
        R.append(", topic=");
        R.append(this.topic);
        R.append(", knownUserIds=");
        R.append(this.knownUserIds);
        R.append(", speakerIds=");
        R.append(this.speakerIds);
        R.append(", speakerCount=");
        R.append(this.speakerCount);
        R.append(", audienceSize=");
        return a.B(R, this.audienceSize, ")");
    }
}
