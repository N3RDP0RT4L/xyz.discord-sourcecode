package com.discord.widgets.user.presence;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Space;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewKt;
import androidx.fragment.app.FragmentManager;
import b.a.i.b6;
import b.a.i.n5;
import b.a.i.o5;
import b.a.i.x5;
import b.a.i.y5;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityAssets;
import com.discord.api.activity.ActivityTimestamps;
import com.discord.app.AppComponent;
import com.discord.models.user.User;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.StreamPreviewView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.g0.t;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: ViewHolderUserRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0016\u0018\u0000 l2\u00020\u0001:\u0001lBs\u0012\u0006\u0010E\u001a\u00020D\u0012\u0006\u0010J\u001a\u00020I\u0012\b\u0010R\u001a\u0004\u0018\u00010I\u0012\u0006\u0010@\u001a\u00020?\u0012\u0006\u0010N\u001a\u00020?\u0012\b\u0010]\u001a\u0004\u0018\u00010?\u0012\u0006\u0010d\u001a\u00020?\u0012\b\u0010P\u001a\u0004\u0018\u00010?\u0012\u0006\u0010Y\u001a\u00020D\u0012\b\u0010U\u001a\u0004\u0018\u00010T\u0012\b\u0010b\u001a\u0004\u0018\u00010T\u0012\b\b\u0002\u0010f\u001a\u00020\t¢\u0006\u0004\bj\u0010kJ7\u0010\f\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\f\u0010\rJQ\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0013\u001a\u00020\u00122\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010\u0015\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0016\u001a\u00020\u0012H\u0017¢\u0006\u0004\b\u0017\u0010\u0018J/\u0010\u0019\u001a\u00020\u000b2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0015¢\u0006\u0004\b\u0019\u0010\u001aJM\u0010#\u001a\u00020\u000b2>\u0010\"\u001a:\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u001cj\u0002`\u001d\u0012\b\u0012\u00060\u001ej\u0002`\u001f\u0012\b\u0012\u00060\u001cj\u0002` \u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000b0\u001bj\u0002`!¢\u0006\u0004\b#\u0010$J#\u0010&\u001a\u00020\u000b2\b\u0010%\u001a\u0004\u0018\u00010\u00072\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0015¢\u0006\u0004\b&\u0010'J\u000f\u0010(\u001a\u00020\u000bH\u0016¢\u0006\u0004\b(\u0010)J\u0015\u0010,\u001a\u0004\u0018\u00010+*\u00020*H\u0004¢\u0006\u0004\b,\u0010-J\u001b\u0010/\u001a\u0004\u0018\u00010\u001e2\b\b\u0001\u0010.\u001a\u00020\tH\u0004¢\u0006\u0004\b/\u00100J'\u00104\u001a\u00020\u000b*\u0002012\b\u00102\u001a\u0004\u0018\u00010\u001e2\b\b\u0002\u00103\u001a\u00020\u0012H\u0004¢\u0006\u0004\b4\u00105J!\u00108\u001a\u00020\u000b2\b\u0010%\u001a\u0004\u0018\u00010\u00072\u0006\u00107\u001a\u000206H\u0017¢\u0006\u0004\b8\u00109J\u000f\u0010:\u001a\u00020\u000bH\u0005¢\u0006\u0004\b:\u0010)J#\u0010;\u001a\u00020\u000b2\b\u0010%\u001a\u0004\u0018\u00010\u00072\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0014¢\u0006\u0004\b;\u0010'J\u0019\u0010=\u001a\u00020\u000b2\b\u0010<\u001a\u0004\u0018\u00010*H\u0014¢\u0006\u0004\b=\u0010>R\u001c\u0010@\u001a\u00020?8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b@\u0010A\u001a\u0004\bB\u0010CR\u001c\u0010E\u001a\u00020D8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bE\u0010F\u001a\u0004\bG\u0010HR\u001c\u0010J\u001a\u00020I8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bJ\u0010K\u001a\u0004\bL\u0010MR\u001c\u0010N\u001a\u00020?8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bN\u0010A\u001a\u0004\bO\u0010CR\u001e\u0010P\u001a\u0004\u0018\u00010?8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bP\u0010A\u001a\u0004\bQ\u0010CR\u001e\u0010R\u001a\u0004\u0018\u00010I8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bR\u0010K\u001a\u0004\bS\u0010MR\u001e\u0010U\u001a\u0004\u0018\u00010T8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bU\u0010V\u001a\u0004\bW\u0010XR\u001c\u0010Y\u001a\u00020D8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bY\u0010F\u001a\u0004\bZ\u0010HRR\u0010[\u001a>\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u001cj\u0002`\u001d\u0012\b\u0012\u00060\u001ej\u0002`\u001f\u0012\b\u0012\u00060\u001cj\u0002` \u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u001bj\u0004\u0018\u0001`!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b[\u0010\\R\u001e\u0010]\u001a\u0004\u0018\u00010?8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b]\u0010A\u001a\u0004\b^\u0010CR\u0018\u0010`\u001a\u0004\u0018\u00010_8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b`\u0010aR\u001e\u0010b\u001a\u0004\u0018\u00010T8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bb\u0010V\u001a\u0004\bc\u0010XR\u001c\u0010d\u001a\u00020?8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bd\u0010A\u001a\u0004\be\u0010CR\u001c\u0010f\u001a\u00020\t8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bf\u0010g\u001a\u0004\bh\u0010i¨\u0006m"}, d2 = {"Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "", "Landroid/content/Context;", "Lcom/discord/app/ApplicationContext;", "applicationContext", "Lcom/discord/models/user/User;", "user", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "", "buttonIndex", "", "handleActivityCustomButtonClick", "(Landroid/content/Context;Lcom/discord/models/user/User;Lcom/discord/api/activity/Activity;I)V", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "", "isMe", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "richPresence", "userInSameVoiceChannel", "configureUi", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/user/User;Landroid/content/Context;Lcom/discord/widgets/user/presence/ModelRichPresence;Z)V", "configureCustomButtonsUi", "(Lcom/discord/models/user/User;Lcom/discord/api/activity/Activity;Landroid/content/Context;)V", "Lkotlin/Function5;", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/SessionId;", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/widgets/user/presence/ActivityCustomButtonClickHandler;", "newOnActivityCustomButtonClicked", "setOnActivityCustomButtonClicked", "(Lkotlin/jvm/functions/Function5;)V", "model", "configureAssetUi", "(Lcom/discord/api/activity/Activity;Lcom/discord/utilities/streams/StreamContext;)V", "disposeSubscriptions", "()V", "Lcom/discord/api/activity/ActivityTimestamps;", "", "friendlyTime", "(Lcom/discord/api/activity/ActivityTimestamps;)Ljava/lang/CharSequence;", "stringId", "getString", "(I)Ljava/lang/String;", "Landroid/widget/ImageView;", "imageUri", "useSmallCache", "setImageAndVisibilityBy", "(Landroid/widget/ImageView;Ljava/lang/String;Z)V", "Lcom/discord/app/AppComponent;", "appComponent", "configureUiTimestamp", "(Lcom/discord/api/activity/Activity;Lcom/discord/app/AppComponent;)V", "disposeTimer", "configureTextUi", "timestamps", "setTimeTextViews", "(Lcom/discord/api/activity/ActivityTimestamps;)V", "Landroid/widget/TextView;", "richPresenceHeader", "Landroid/widget/TextView;", "getRichPresenceHeader", "()Landroid/widget/TextView;", "Landroid/view/View;", "root", "Landroid/view/View;", "getRoot", "()Landroid/view/View;", "Lcom/facebook/drawee/view/SimpleDraweeView;", "richPresenceImageLarge", "Lcom/facebook/drawee/view/SimpleDraweeView;", "getRichPresenceImageLarge", "()Lcom/facebook/drawee/view/SimpleDraweeView;", "richPresenceTitle", "getRichPresenceTitle", "richPresenceState", "getRichPresenceState", "richPresenceImageSmall", "getRichPresenceImageSmall", "Landroid/widget/Button;", "richPresencePrimaryButton", "Landroid/widget/Button;", "getRichPresencePrimaryButton", "()Landroid/widget/Button;", "richPresenceTextContainer", "getRichPresenceTextContainer", "onActivityCustomButtonClicked", "Lkotlin/jvm/functions/Function5;", "richPresenceDetails", "getRichPresenceDetails", "Lrx/Subscription;", "perSecondTimerSubscription", "Lrx/Subscription;", "richPresenceSecondaryButton", "getRichPresenceSecondaryButton", "richPresenceTime", "getRichPresenceTime", "richPresenceType", "I", "getRichPresenceType", "()I", HookHelper.constructorName, "(Landroid/view/View;Lcom/facebook/drawee/view/SimpleDraweeView;Lcom/facebook/drawee/view/SimpleDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/Button;Landroid/widget/Button;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class ViewHolderUserRichPresence {
    public static final int BASE_RICH_PRESENCE_TYPE = 0;
    public static final Companion Companion = new Companion(null);
    public static final int GAME_RICH_PRESENCE_TYPE = 1;
    public static final int MUSIC_RICH_PRESENCE_TYPE = 2;
    public static final int PLATFORM_RICH_PRESENCE_TYPE = 3;
    public static final int STAGE_CHANNEL_RICH_PRESENCE_TYPE = 5;
    public static final int STREAM_RICH_PRESENCE_TYPE = 4;
    private Function5<? super Context, ? super Long, ? super String, ? super Long, ? super Integer, Unit> onActivityCustomButtonClicked;
    private Subscription perSecondTimerSubscription;
    private final TextView richPresenceDetails;
    private final TextView richPresenceHeader;
    private final SimpleDraweeView richPresenceImageLarge;
    private final SimpleDraweeView richPresenceImageSmall;
    private final Button richPresencePrimaryButton;
    private final Button richPresenceSecondaryButton;
    private final TextView richPresenceState;
    private final View richPresenceTextContainer;
    private final TextView richPresenceTime;
    private final TextView richPresenceTitle;
    private final int richPresenceType;
    private final View root;

    /* compiled from: ViewHolderUserRichPresence.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ#\u0010\r\u001a\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ5\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0013R\u0016\u0010\u0016\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0013R\u0016\u0010\u0017\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0013R\u0016\u0010\u0018\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0013¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;", "", "Landroid/view/ViewGroup;", "parent", "", "type", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "createRPView", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "getRPViewHolderType", "(Lcom/discord/api/activity/Activity;Lcom/discord/utilities/streams/StreamContext;)I", "oldViewHolder", "setRichPresence", "(Landroid/view/ViewGroup;Lcom/discord/api/activity/Activity;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;)Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "BASE_RICH_PRESENCE_TYPE", "I", "GAME_RICH_PRESENCE_TYPE", "MUSIC_RICH_PRESENCE_TYPE", "PLATFORM_RICH_PRESENCE_TYPE", "STAGE_CHANNEL_RICH_PRESENCE_TYPE", "STREAM_RICH_PRESENCE_TYPE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final ViewHolderUserRichPresence createRPView(ViewGroup viewGroup, int i) {
            String str;
            int i2;
            ViewHolderUserRichPresence viewHolderUserRichPresence;
            int i3;
            String str2;
            int i4;
            String str3;
            int i5;
            LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
            if (i == 1) {
                b6 a = b6.a(from, viewGroup, true);
                m.checkNotNullExpressionValue(a, "WidgetUserRichPresenceBi…e(inflater, parent, true)");
                SimpleDraweeView simpleDraweeView = a.e;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.richPresenceImageLarge");
                SimpleDraweeView simpleDraweeView2 = a.f;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.richPresenceImageSmall");
                TextView textView = a.d;
                m.checkNotNullExpressionValue(textView, "binding.richPresenceHeader");
                TextView textView2 = a.k;
                m.checkNotNullExpressionValue(textView2, "binding.richPresenceTitle");
                TextView textView3 = a.c;
                m.checkNotNullExpressionValue(textView3, "binding.richPresenceDetails");
                TextView textView4 = a.j;
                m.checkNotNullExpressionValue(textView4, "binding.richPresenceTime");
                TextView textView5 = a.i;
                m.checkNotNullExpressionValue(textView5, "binding.richPresenceState");
                LinearLayout linearLayout = a.f87b;
                m.checkNotNullExpressionValue(linearLayout, "binding.richPresenceContainerData");
                MaterialButton materialButton = a.g;
                m.checkNotNullExpressionValue(materialButton, "binding.richPresencePrimaryButton");
                MaterialButton materialButton2 = a.h;
                m.checkNotNullExpressionValue(materialButton2, "binding.richPresenceSecondaryButton");
                return new ViewHolderGameRichPresence(viewGroup, simpleDraweeView, simpleDraweeView2, textView, textView2, textView3, textView4, textView5, linearLayout, materialButton, materialButton2);
            } else if (i != 2) {
                if (i == 3) {
                    View inflate = from.inflate(R.layout.widget_platform_rich_presence, viewGroup, false);
                    viewGroup.addView(inflate);
                    Barrier barrier = (Barrier) inflate.findViewById(R.id.barrier);
                    if (barrier != null) {
                        Space space = (Space) inflate.findViewById(R.id.image_bottom_guideline);
                        if (space != null) {
                            LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(R.id.rich_presence_container_data);
                            if (linearLayout2 != null) {
                                TextView textView6 = (TextView) inflate.findViewById(R.id.rich_presence_header);
                                if (textView6 != null) {
                                    SimpleDraweeView simpleDraweeView3 = (SimpleDraweeView) inflate.findViewById(R.id.rich_presence_image_large);
                                    if (simpleDraweeView3 != null) {
                                        SimpleDraweeView simpleDraweeView4 = (SimpleDraweeView) inflate.findViewById(R.id.rich_presence_image_small);
                                        if (simpleDraweeView4 != null) {
                                            MaterialButton materialButton3 = (MaterialButton) inflate.findViewById(R.id.rich_presence_primary_button);
                                            if (materialButton3 != null) {
                                                TextView textView7 = (TextView) inflate.findViewById(R.id.rich_presence_time);
                                                if (textView7 != null) {
                                                    TextView textView8 = (TextView) inflate.findViewById(R.id.rich_presence_title);
                                                    if (textView8 != null) {
                                                        m.checkNotNullExpressionValue(new o5((LinearLayout) inflate, barrier, space, linearLayout2, textView6, simpleDraweeView3, simpleDraweeView4, materialButton3, textView7, textView8), "WidgetPlatformRichPresen…e(inflater, parent, true)");
                                                        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.richPresenceImageLarge");
                                                        m.checkNotNullExpressionValue(simpleDraweeView4, "binding.richPresenceImageSmall");
                                                        m.checkNotNullExpressionValue(textView6, "binding.richPresenceHeader");
                                                        m.checkNotNullExpressionValue(textView8, "binding.richPresenceTitle");
                                                        m.checkNotNullExpressionValue(textView7, "binding.richPresenceTime");
                                                        m.checkNotNullExpressionValue(linearLayout2, "binding.richPresenceContainerData");
                                                        m.checkNotNullExpressionValue(materialButton3, "binding.richPresencePrimaryButton");
                                                        viewHolderUserRichPresence = new ViewHolderPlatformRichPresence(viewGroup, simpleDraweeView3, simpleDraweeView4, textView6, textView8, textView7, linearLayout2, materialButton3);
                                                    } else {
                                                        i3 = R.id.rich_presence_title;
                                                    }
                                                } else {
                                                    i3 = R.id.rich_presence_time;
                                                }
                                            } else {
                                                i3 = R.id.rich_presence_primary_button;
                                            }
                                        } else {
                                            i3 = R.id.rich_presence_image_small;
                                        }
                                    } else {
                                        i3 = R.id.rich_presence_image_large;
                                    }
                                } else {
                                    i3 = R.id.rich_presence_header;
                                }
                            } else {
                                i3 = R.id.rich_presence_container_data;
                            }
                        } else {
                            i3 = R.id.image_bottom_guideline;
                        }
                    } else {
                        i3 = R.id.barrier;
                    }
                    throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i3)));
                } else if (i == 4) {
                    View inflate2 = from.inflate(R.layout.widget_stream_rich_presence, viewGroup, false);
                    viewGroup.addView(inflate2);
                    LinearLayout linearLayout3 = (LinearLayout) inflate2.findViewById(R.id.rich_presence_container_data);
                    if (linearLayout3 != null) {
                        TextView textView9 = (TextView) inflate2.findViewById(R.id.rich_presence_details);
                        if (textView9 != null) {
                            TextView textView10 = (TextView) inflate2.findViewById(R.id.rich_presence_header);
                            if (textView10 != null) {
                                SimpleDraweeView simpleDraweeView5 = (SimpleDraweeView) inflate2.findViewById(R.id.rich_presence_image_large);
                                if (simpleDraweeView5 != null) {
                                    TextView textView11 = (TextView) inflate2.findViewById(R.id.rich_presence_state);
                                    if (textView11 != null) {
                                        TextView textView12 = (TextView) inflate2.findViewById(R.id.rich_presence_time);
                                        if (textView12 != null) {
                                            TextView textView13 = (TextView) inflate2.findViewById(R.id.rich_presence_title);
                                            str2 = "Missing required view with ID: ";
                                            if (textView13 != null) {
                                                StreamPreviewView streamPreviewView = (StreamPreviewView) inflate2.findViewById(R.id.stream_preview);
                                                if (streamPreviewView != null) {
                                                    m.checkNotNullExpressionValue(new y5((ConstraintLayout) inflate2, linearLayout3, textView9, textView10, simpleDraweeView5, textView11, textView12, textView13, streamPreviewView), "WidgetStreamRichPresence…e(inflater, parent, true)");
                                                    m.checkNotNullExpressionValue(simpleDraweeView5, "binding.richPresenceImageLarge");
                                                    m.checkNotNullExpressionValue(textView10, "binding.richPresenceHeader");
                                                    m.checkNotNullExpressionValue(textView13, "binding.richPresenceTitle");
                                                    m.checkNotNullExpressionValue(textView9, "binding.richPresenceDetails");
                                                    m.checkNotNullExpressionValue(textView12, "binding.richPresenceTime");
                                                    m.checkNotNullExpressionValue(textView11, "binding.richPresenceState");
                                                    m.checkNotNullExpressionValue(linearLayout3, "binding.richPresenceContainerData");
                                                    m.checkNotNullExpressionValue(streamPreviewView, "binding.streamPreview");
                                                    viewHolderUserRichPresence = new ViewHolderStreamRichPresence(viewGroup, simpleDraweeView5, textView10, textView13, textView9, textView12, textView11, linearLayout3, streamPreviewView);
                                                } else {
                                                    i4 = R.id.stream_preview;
                                                }
                                            } else {
                                                i4 = R.id.rich_presence_title;
                                            }
                                        } else {
                                            str2 = "Missing required view with ID: ";
                                            i4 = R.id.rich_presence_time;
                                        }
                                    } else {
                                        str2 = "Missing required view with ID: ";
                                        i4 = R.id.rich_presence_state;
                                    }
                                } else {
                                    str2 = "Missing required view with ID: ";
                                    i4 = R.id.rich_presence_image_large;
                                }
                            } else {
                                str2 = "Missing required view with ID: ";
                                i4 = R.id.rich_presence_header;
                            }
                        } else {
                            str2 = "Missing required view with ID: ";
                            i4 = R.id.rich_presence_details;
                        }
                    } else {
                        str2 = "Missing required view with ID: ";
                        i4 = R.id.rich_presence_container_data;
                    }
                    throw new NullPointerException(str2.concat(inflate2.getResources().getResourceName(i4)));
                } else if (i != 5) {
                    b6 a2 = b6.a(from, viewGroup, true);
                    m.checkNotNullExpressionValue(a2, "WidgetUserRichPresenceBi…e(inflater, parent, true)");
                    SimpleDraweeView simpleDraweeView6 = a2.e;
                    m.checkNotNullExpressionValue(simpleDraweeView6, "binding.richPresenceImageLarge");
                    SimpleDraweeView simpleDraweeView7 = a2.f;
                    TextView textView14 = a2.d;
                    m.checkNotNullExpressionValue(textView14, "binding.richPresenceHeader");
                    TextView textView15 = a2.k;
                    m.checkNotNullExpressionValue(textView15, "binding.richPresenceTitle");
                    TextView textView16 = a2.c;
                    TextView textView17 = a2.j;
                    m.checkNotNullExpressionValue(textView17, "binding.richPresenceTime");
                    TextView textView18 = a2.i;
                    LinearLayout linearLayout4 = a2.f87b;
                    m.checkNotNullExpressionValue(linearLayout4, "binding.richPresenceContainerData");
                    return new ViewHolderUserRichPresence(viewGroup, simpleDraweeView6, simpleDraweeView7, textView14, textView15, textView16, textView17, textView18, linearLayout4, a2.g, a2.h, 0, 2048, null);
                } else {
                    View inflate3 = from.inflate(R.layout.widget_stage_channel_rich_presence, viewGroup, false);
                    viewGroup.addView(inflate3);
                    Barrier barrier2 = (Barrier) inflate3.findViewById(R.id.barrier);
                    if (barrier2 != null) {
                        Space space2 = (Space) inflate3.findViewById(R.id.image_bottom_guideline);
                        if (space2 != null) {
                            LinearLayout linearLayout5 = (LinearLayout) inflate3.findViewById(R.id.rich_presence_container_data);
                            if (linearLayout5 != null) {
                                TextView textView19 = (TextView) inflate3.findViewById(R.id.rich_presence_details);
                                if (textView19 != null) {
                                    TextView textView20 = (TextView) inflate3.findViewById(R.id.rich_presence_header);
                                    if (textView20 != null) {
                                        SimpleDraweeView simpleDraweeView8 = (SimpleDraweeView) inflate3.findViewById(R.id.rich_presence_image_large);
                                        if (simpleDraweeView8 != null) {
                                            TextView textView21 = (TextView) inflate3.findViewById(R.id.rich_presence_image_large_text);
                                            if (textView21 != null) {
                                                FrameLayout frameLayout = (FrameLayout) inflate3.findViewById(R.id.rich_presence_image_large_wrap);
                                                if (frameLayout != null) {
                                                    MaterialButton materialButton4 = (MaterialButton) inflate3.findViewById(R.id.rich_presence_primary_button);
                                                    if (materialButton4 != null) {
                                                        TextView textView22 = (TextView) inflate3.findViewById(R.id.rich_presence_time);
                                                        str3 = "Missing required view with ID: ";
                                                        if (textView22 != null) {
                                                            TextView textView23 = (TextView) inflate3.findViewById(R.id.rich_presence_title);
                                                            if (textView23 != null) {
                                                                m.checkNotNullExpressionValue(new x5((LinearLayout) inflate3, barrier2, space2, linearLayout5, textView19, textView20, simpleDraweeView8, textView21, frameLayout, materialButton4, textView22, textView23), "WidgetStageChannelRichPr…e(inflater, parent, true)");
                                                                m.checkNotNullExpressionValue(simpleDraweeView8, "binding.richPresenceImageLarge");
                                                                m.checkNotNullExpressionValue(textView20, "binding.richPresenceHeader");
                                                                m.checkNotNullExpressionValue(textView23, "binding.richPresenceTitle");
                                                                m.checkNotNullExpressionValue(textView19, "binding.richPresenceDetails");
                                                                m.checkNotNullExpressionValue(textView22, "binding.richPresenceTime");
                                                                m.checkNotNullExpressionValue(linearLayout5, "binding.richPresenceContainerData");
                                                                m.checkNotNullExpressionValue(materialButton4, "binding.richPresencePrimaryButton");
                                                                m.checkNotNullExpressionValue(textView21, "binding.richPresenceImageLargeText");
                                                                viewHolderUserRichPresence = new ViewHolderStageChannelRichPresence(viewGroup, simpleDraweeView8, textView20, textView23, textView19, textView22, linearLayout5, materialButton4, textView21);
                                                            } else {
                                                                i5 = R.id.rich_presence_title;
                                                            }
                                                        } else {
                                                            i5 = R.id.rich_presence_time;
                                                        }
                                                    } else {
                                                        str3 = "Missing required view with ID: ";
                                                        i5 = R.id.rich_presence_primary_button;
                                                    }
                                                } else {
                                                    str3 = "Missing required view with ID: ";
                                                    i5 = R.id.rich_presence_image_large_wrap;
                                                }
                                            } else {
                                                str3 = "Missing required view with ID: ";
                                                i5 = R.id.rich_presence_image_large_text;
                                            }
                                        } else {
                                            str3 = "Missing required view with ID: ";
                                            i5 = R.id.rich_presence_image_large;
                                        }
                                    } else {
                                        str3 = "Missing required view with ID: ";
                                        i5 = R.id.rich_presence_header;
                                    }
                                } else {
                                    str3 = "Missing required view with ID: ";
                                    i5 = R.id.rich_presence_details;
                                }
                            } else {
                                str3 = "Missing required view with ID: ";
                                i5 = R.id.rich_presence_container_data;
                            }
                        } else {
                            str3 = "Missing required view with ID: ";
                            i5 = R.id.image_bottom_guideline;
                        }
                    } else {
                        str3 = "Missing required view with ID: ";
                        i5 = R.id.barrier;
                    }
                    throw new NullPointerException(str3.concat(inflate3.getResources().getResourceName(i5)));
                }
                return viewHolderUserRichPresence;
            } else {
                View inflate4 = from.inflate(R.layout.widget_music_rich_presence, viewGroup, false);
                viewGroup.addView(inflate4);
                Barrier barrier3 = (Barrier) inflate4.findViewById(R.id.barrier);
                if (barrier3 != null) {
                    Space space3 = (Space) inflate4.findViewById(R.id.image_bottom_guideline);
                    if (space3 != null) {
                        LinearLayout linearLayout6 = (LinearLayout) inflate4.findViewById(R.id.rich_presence_container_data);
                        if (linearLayout6 != null) {
                            TextView textView24 = (TextView) inflate4.findViewById(R.id.rich_presence_details);
                            if (textView24 != null) {
                                TextView textView25 = (TextView) inflate4.findViewById(R.id.rich_presence_header);
                                if (textView25 != null) {
                                    i2 = R.id.rich_presence_image_large;
                                    SimpleDraweeView simpleDraweeView9 = (SimpleDraweeView) inflate4.findViewById(R.id.rich_presence_image_large);
                                    if (simpleDraweeView9 != null) {
                                        SimpleDraweeView simpleDraweeView10 = (SimpleDraweeView) inflate4.findViewById(R.id.rich_presence_image_small);
                                        if (simpleDraweeView10 != null) {
                                            TextView textView26 = (TextView) inflate4.findViewById(R.id.rich_presence_music_duration);
                                            str = "Missing required view with ID: ";
                                            if (textView26 != null) {
                                                TextView textView27 = (TextView) inflate4.findViewById(R.id.rich_presence_music_elapsed);
                                                if (textView27 != null) {
                                                    FrameLayout frameLayout2 = (FrameLayout) inflate4.findViewById(R.id.rich_presence_music_progress_container);
                                                    if (frameLayout2 != null) {
                                                        MaterialButton materialButton5 = (MaterialButton) inflate4.findViewById(R.id.rich_presence_primary_button);
                                                        if (materialButton5 != null) {
                                                            SeekBar seekBar = (SeekBar) inflate4.findViewById(R.id.rich_presence_seekbar);
                                                            if (seekBar != null) {
                                                                TextView textView28 = (TextView) inflate4.findViewById(R.id.rich_presence_time);
                                                                if (textView28 != null) {
                                                                    TextView textView29 = (TextView) inflate4.findViewById(R.id.rich_presence_title);
                                                                    if (textView29 != null) {
                                                                        m.checkNotNullExpressionValue(new n5((LinearLayout) inflate4, barrier3, space3, linearLayout6, textView24, textView25, simpleDraweeView9, simpleDraweeView10, textView26, textView27, frameLayout2, materialButton5, seekBar, textView28, textView29), "WidgetMusicRichPresenceB…e(inflater, parent, true)");
                                                                        m.checkNotNullExpressionValue(simpleDraweeView9, "binding.richPresenceImageLarge");
                                                                        m.checkNotNullExpressionValue(simpleDraweeView10, "binding.richPresenceImageSmall");
                                                                        m.checkNotNullExpressionValue(textView25, "binding.richPresenceHeader");
                                                                        m.checkNotNullExpressionValue(textView29, "binding.richPresenceTitle");
                                                                        m.checkNotNullExpressionValue(textView24, "binding.richPresenceDetails");
                                                                        m.checkNotNullExpressionValue(textView28, "binding.richPresenceTime");
                                                                        m.checkNotNullExpressionValue(linearLayout6, "binding.richPresenceContainerData");
                                                                        m.checkNotNullExpressionValue(materialButton5, "binding.richPresencePrimaryButton");
                                                                        m.checkNotNullExpressionValue(seekBar, "binding.richPresenceSeekbar");
                                                                        m.checkNotNullExpressionValue(textView26, "binding.richPresenceMusicDuration");
                                                                        m.checkNotNullExpressionValue(textView27, "binding.richPresenceMusicElapsed");
                                                                        return new ViewHolderMusicRichPresence(viewGroup, simpleDraweeView9, simpleDraweeView10, textView25, textView29, textView24, textView28, linearLayout6, materialButton5, seekBar, textView26, textView27);
                                                                    }
                                                                    i2 = R.id.rich_presence_title;
                                                                } else {
                                                                    i2 = R.id.rich_presence_time;
                                                                }
                                                            } else {
                                                                i2 = R.id.rich_presence_seekbar;
                                                            }
                                                        } else {
                                                            i2 = R.id.rich_presence_primary_button;
                                                        }
                                                    } else {
                                                        i2 = R.id.rich_presence_music_progress_container;
                                                    }
                                                } else {
                                                    i2 = R.id.rich_presence_music_elapsed;
                                                }
                                            } else {
                                                i2 = R.id.rich_presence_music_duration;
                                            }
                                        } else {
                                            str = "Missing required view with ID: ";
                                            i2 = R.id.rich_presence_image_small;
                                        }
                                    } else {
                                        str = "Missing required view with ID: ";
                                    }
                                } else {
                                    str = "Missing required view with ID: ";
                                    i2 = R.id.rich_presence_header;
                                }
                            } else {
                                str = "Missing required view with ID: ";
                                i2 = R.id.rich_presence_details;
                            }
                        } else {
                            str = "Missing required view with ID: ";
                            i2 = R.id.rich_presence_container_data;
                        }
                    } else {
                        str = "Missing required view with ID: ";
                        i2 = R.id.image_bottom_guideline;
                    }
                } else {
                    str = "Missing required view with ID: ";
                    i2 = R.id.barrier;
                }
                throw new NullPointerException(str.concat(inflate4.getResources().getResourceName(i2)));
            }
        }

        private final int getRPViewHolderType(Activity activity, StreamContext streamContext) {
            if (streamContext != null) {
                return 4;
            }
            if (activity != null && ActivityUtilsKt.isGamePlatform(activity)) {
                return 3;
            }
            if (activity != null && ActivityUtilsKt.isGameActivity(activity)) {
                return 1;
            }
            if (activity == null || !ActivityUtilsKt.isSpotifyActivity(activity)) {
                return (activity == null || !ActivityUtilsKt.isStageChannelActivity(activity)) ? 0 : 5;
            }
            return 2;
        }

        public final ViewHolderUserRichPresence setRichPresence(ViewGroup viewGroup, Activity activity, StreamContext streamContext, ViewHolderUserRichPresence viewHolderUserRichPresence) {
            m.checkNotNullParameter(viewGroup, "parent");
            int rPViewHolderType = getRPViewHolderType(activity, streamContext);
            if (viewHolderUserRichPresence != null && rPViewHolderType == viewHolderUserRichPresence.getRichPresenceType()) {
                return viewHolderUserRichPresence;
            }
            viewGroup.removeAllViews();
            return ViewHolderUserRichPresence.Companion.createRPView(viewGroup, rPViewHolderType);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ViewHolderUserRichPresence(View view, SimpleDraweeView simpleDraweeView, SimpleDraweeView simpleDraweeView2, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view2, Button button, Button button2, int i) {
        m.checkNotNullParameter(view, "root");
        m.checkNotNullParameter(simpleDraweeView, "richPresenceImageLarge");
        m.checkNotNullParameter(textView, "richPresenceHeader");
        m.checkNotNullParameter(textView2, "richPresenceTitle");
        m.checkNotNullParameter(textView4, "richPresenceTime");
        m.checkNotNullParameter(view2, "richPresenceTextContainer");
        this.root = view;
        this.richPresenceImageLarge = simpleDraweeView;
        this.richPresenceImageSmall = simpleDraweeView2;
        this.richPresenceHeader = textView;
        this.richPresenceTitle = textView2;
        this.richPresenceDetails = textView3;
        this.richPresenceTime = textView4;
        this.richPresenceState = textView5;
        this.richPresenceTextContainer = view2;
        this.richPresencePrimaryButton = button;
        this.richPresenceSecondaryButton = button2;
        this.richPresenceType = i;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleActivityCustomButtonClick(Context context, User user, Activity activity, int i) {
        String k;
        Long a;
        if (user != null) {
            long id2 = user.getId();
            if (activity != null && (k = activity.k()) != null && (a = activity.a()) != null) {
                long longValue = a.longValue();
                Function5<? super Context, ? super Long, ? super String, ? super Long, ? super Integer, Unit> function5 = this.onActivityCustomButtonClicked;
                if (function5 != null) {
                    function5.invoke(context, Long.valueOf(id2), k, Long.valueOf(longValue), Integer.valueOf(i));
                }
            }
        }
    }

    public static /* synthetic */ void setImageAndVisibilityBy$default(ViewHolderUserRichPresence viewHolderUserRichPresence, ImageView imageView, String str, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            viewHolderUserRichPresence.setImageAndVisibilityBy(imageView, str, z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setImageAndVisibilityBy");
    }

    public static final ViewHolderUserRichPresence setRichPresence(ViewGroup viewGroup, Activity activity, StreamContext streamContext, ViewHolderUserRichPresence viewHolderUserRichPresence) {
        return Companion.setRichPresence(viewGroup, activity, streamContext, viewHolderUserRichPresence);
    }

    @MainThread
    public void configureAssetUi(Activity activity, StreamContext streamContext) {
        String str;
        String str2 = null;
        ActivityAssets b2 = activity != null ? activity.b() : null;
        if (b2 == null) {
            this.richPresenceImageLarge.setVisibility(8);
            SimpleDraweeView simpleDraweeView = this.richPresenceImageSmall;
            if (simpleDraweeView != null) {
                simpleDraweeView.setVisibility(8);
                return;
            }
            return;
        }
        Long a = activity.a();
        String a2 = b2.a();
        String assetImage$default = a2 != null ? IconUtils.getAssetImage$default(IconUtils.INSTANCE, a, a2, 0, 4, null) : null;
        String b3 = b2.b();
        String c = b2.c();
        String assetImage$default2 = c != null ? IconUtils.getAssetImage$default(IconUtils.INSTANCE, a, c, 0, 4, null) : null;
        String d = b2.d();
        if (assetImage$default2 == null || assetImage$default != null) {
            str2 = assetImage$default2;
            str = assetImage$default;
        } else {
            str = assetImage$default2;
            b3 = d;
            d = null;
        }
        setImageAndVisibilityBy$default(this, this.richPresenceImageLarge, str, false, 2, null);
        this.richPresenceImageLarge.setContentDescription(b3);
        SimpleDraweeView simpleDraweeView2 = this.richPresenceImageSmall;
        if (simpleDraweeView2 != null) {
            setImageAndVisibilityBy(simpleDraweeView2, str2, true);
        }
        SimpleDraweeView simpleDraweeView3 = this.richPresenceImageSmall;
        if (simpleDraweeView3 != null) {
            simpleDraweeView3.setContentDescription(d);
        }
    }

    @MainThread
    public void configureCustomButtonsUi(final User user, final Activity activity, final Context context) {
        m.checkNotNullParameter(context, "applicationContext");
        List<String> c = activity != null ? activity.c() : null;
        final int i = 0;
        for (Object obj : n.listOf((Object[]) new Button[]{this.richPresencePrimaryButton, this.richPresenceSecondaryButton})) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            Button button = (Button) obj;
            String str = c != null ? (String) u.getOrNull(c, i) : null;
            if (str != null) {
                if (button != null) {
                    button.setText(str);
                }
                if (button != null) {
                    ViewKt.setVisible(button, true);
                }
                if (button != null) {
                    final List<String> list = c;
                    button.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.presence.ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            this.handleActivityCustomButtonClick(context, user, activity, i);
                        }
                    });
                }
            } else if (button != null) {
                ViewKt.setVisible(button, false);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x0047, code lost:
        if (r1 != null) goto L16;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void configureTextUi(com.discord.api.activity.Activity r10, com.discord.utilities.streams.StreamContext r11) {
        /*
            r9 = this;
            if (r10 != 0) goto L3
            return
        L3:
            java.lang.String r11 = r10.l()
            r0 = 0
            if (r11 == 0) goto L6e
            com.discord.api.activity.ActivityParty r1 = r10.i()
            if (r1 == 0) goto L4a
            com.discord.utilities.presence.PresenceUtils r2 = com.discord.utilities.presence.PresenceUtils.INSTANCE
            long r3 = r2.getMaxSize(r1)
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 != 0) goto L1e
        L1c:
            r1 = r0
            goto L47
        L1e:
            android.view.View r3 = r9.root
            android.content.Context r3 = r3.getContext()
            if (r3 == 0) goto L1c
            r4 = 2131895337(0x7f122429, float:1.9425504E38)
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            long r7 = r2.getCurrentSize(r1)
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r5[r6] = r7
            r6 = 1
            long r1 = r2.getMaxSize(r1)
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r5[r6] = r1
            r1 = 4
            java.lang.CharSequence r1 = b.a.k.b.h(r3, r4, r5, r0, r1)
        L47:
            if (r1 == 0) goto L4a
            goto L4c
        L4a:
            java.lang.String r1 = ""
        L4c:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r11)
            r11 = 32
            r2.append(r11)
            r2.append(r1)
            java.lang.String r11 = r2.toString()
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            java.util.Objects.requireNonNull(r11, r1)
            java.lang.CharSequence r11 = d0.g0.w.trim(r11)
            java.lang.String r11 = r11.toString()
            goto L6f
        L6e:
            r11 = r0
        L6f:
            android.widget.TextView r1 = r9.richPresenceHeader
            android.content.Context r2 = r1.getContext()
            java.lang.String r3 = "richPresenceHeader.context"
            d0.z.d.m.checkNotNullExpressionValue(r2, r3)
            java.lang.CharSequence r2 = com.discord.utilities.presence.PresenceUtils.getActivityHeader(r2, r10)
            r1.setText(r2)
            android.widget.TextView r1 = r9.richPresenceTitle
            java.lang.String r2 = r10.e()
            r1.setText(r2)
            android.widget.TextView r1 = r9.richPresenceDetails
            if (r1 == 0) goto L91
            com.discord.utilities.view.extensions.ViewExtensions.setTextAndVisibilityBy(r1, r11)
        L91:
            android.widget.TextView r11 = r9.richPresenceState
            if (r11 == 0) goto La4
            com.discord.api.activity.ActivityAssets r1 = r10.b()
            if (r1 == 0) goto La0
            java.lang.String r1 = r1.b()
            goto La1
        La0:
            r1 = r0
        La1:
            com.discord.utilities.view.extensions.ViewExtensions.setTextAndVisibilityBy(r11, r1)
        La4:
            android.widget.TextView r11 = r9.richPresenceTime
            com.discord.api.activity.ActivityTimestamps r10 = r10.o()
            if (r10 == 0) goto Lb0
            java.lang.CharSequence r0 = r9.friendlyTime(r10)
        Lb0:
            com.discord.utilities.view.extensions.ViewExtensions.setTextAndVisibilityBy(r11, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.presence.ViewHolderUserRichPresence.configureTextUi(com.discord.api.activity.Activity, com.discord.utilities.streams.StreamContext):void");
    }

    @MainThread
    public void configureUi(FragmentManager fragmentManager, StreamContext streamContext, boolean z2, User user, Context context, ModelRichPresence modelRichPresence, boolean z3) {
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "applicationContext");
        disposeTimer();
        Activity primaryActivity = modelRichPresence != null ? modelRichPresence.getPrimaryActivity() : null;
        if (streamContext == null && primaryActivity == null) {
            this.root.setVisibility(8);
            return;
        }
        this.root.setVisibility(0);
        configureTextUi(primaryActivity, streamContext);
        this.richPresenceTextContainer.setSelected(true);
        configureAssetUi(primaryActivity, streamContext);
        configureCustomButtonsUi(user, primaryActivity, context);
    }

    @MainThread
    public void configureUiTimestamp(Activity activity, AppComponent appComponent) {
        m.checkNotNullParameter(appComponent, "appComponent");
        if (activity != null && this.root.getVisibility() == 0) {
            ActivityTimestamps o = activity.o();
            if ((o != null ? friendlyTime(o) : null) != null) {
                if (this.perSecondTimerSubscription == null) {
                    Observable<Long> D = Observable.D(0L, 1L, TimeUnit.SECONDS);
                    m.checkNotNullExpressionValue(D, "Observable\n          .in…0L, 1L, TimeUnit.SECONDS)");
                    ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(D, appComponent, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new ViewHolderUserRichPresence$configureUiTimestamp$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ViewHolderUserRichPresence$configureUiTimestamp$2(this, activity));
                    return;
                }
                return;
            }
        }
        disposeTimer();
    }

    public void disposeSubscriptions() {
        disposeTimer();
    }

    @MainThread
    public final void disposeTimer() {
        Subscription subscription = this.perSecondTimerSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.perSecondTimerSubscription = null;
    }

    public final CharSequence friendlyTime(ActivityTimestamps activityTimestamps) {
        m.checkNotNullParameter(activityTimestamps, "$this$friendlyTime");
        if (activityTimestamps.b() > 0) {
            return TimeUtils.INSTANCE.toFriendlyString(ClockFactory.get().currentTimeMillis(), activityTimestamps.b(), getString(R.string.user_activity_timestamp_end_simple), "timeDelta");
        }
        if (activityTimestamps.c() > 0) {
            return TimeUtils.INSTANCE.toFriendlyString(activityTimestamps.c(), ClockFactory.get().currentTimeMillis(), getString(R.string.user_activity_timestamp_start_simple), "timeDelta");
        }
        return null;
    }

    public final TextView getRichPresenceDetails() {
        return this.richPresenceDetails;
    }

    public final TextView getRichPresenceHeader() {
        return this.richPresenceHeader;
    }

    public final SimpleDraweeView getRichPresenceImageLarge() {
        return this.richPresenceImageLarge;
    }

    public final SimpleDraweeView getRichPresenceImageSmall() {
        return this.richPresenceImageSmall;
    }

    public final Button getRichPresencePrimaryButton() {
        return this.richPresencePrimaryButton;
    }

    public final Button getRichPresenceSecondaryButton() {
        return this.richPresenceSecondaryButton;
    }

    public final TextView getRichPresenceState() {
        return this.richPresenceState;
    }

    public final View getRichPresenceTextContainer() {
        return this.richPresenceTextContainer;
    }

    public final TextView getRichPresenceTime() {
        return this.richPresenceTime;
    }

    public final TextView getRichPresenceTitle() {
        return this.richPresenceTitle;
    }

    public final int getRichPresenceType() {
        return this.richPresenceType;
    }

    public final View getRoot() {
        return this.root;
    }

    public final String getString(@StringRes int i) {
        Context context = this.root.getContext();
        if (context != null) {
            return context.getString(i);
        }
        return null;
    }

    public final void setImageAndVisibilityBy(ImageView imageView, String str, boolean z2) {
        m.checkNotNullParameter(imageView, "$this$setImageAndVisibilityBy");
        MGImages.setImage$default(imageView, str, 0, 0, z2, null, null, 108, null);
        int i = 0;
        if (!(!(str == null || t.isBlank(str)))) {
            i = 8;
        }
        imageView.setVisibility(i);
    }

    public final void setOnActivityCustomButtonClicked(Function5<? super Context, ? super Long, ? super String, ? super Long, ? super Integer, Unit> function5) {
        m.checkNotNullParameter(function5, "newOnActivityCustomButtonClicked");
        this.onActivityCustomButtonClicked = function5;
    }

    public void setTimeTextViews(ActivityTimestamps activityTimestamps) {
        ViewExtensions.setTextAndVisibilityBy(this.richPresenceTime, activityTimestamps != null ? friendlyTime(activityTimestamps) : null);
    }

    public /* synthetic */ ViewHolderUserRichPresence(View view, SimpleDraweeView simpleDraweeView, SimpleDraweeView simpleDraweeView2, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view2, Button button, Button button2, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(view, simpleDraweeView, simpleDraweeView2, textView, textView2, textView3, textView4, textView5, view2, button, button2, (i2 & 2048) != 0 ? 0 : i);
    }
}
