package com.discord.widgets.user.presence;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityTimestamps;
import com.discord.models.guild.Guild;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreApplicationStreamPreviews;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.StreamPreviewView;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: ViewHolderStreamRichPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010(\u001a\u00020'\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u0010,\u001a\u00020+\u0012\u0006\u0010-\u001a\u00020+\u0012\u0006\u0010.\u001a\u00020+\u0012\u0006\u0010/\u001a\u00020+\u0012\u0006\u00100\u001a\u00020+\u0012\u0006\u00101\u001a\u00020'\u0012\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b2\u00103J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J%\u0010\u000b\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u00072\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJQ\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\n\u0010\u0015\u001a\u00060\u0013j\u0002`\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0018\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ#\u0010\u001d\u001a\u00020\u00042\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0014¢\u0006\u0004\b\u001d\u0010\u001eJ#\u0010\u001f\u001a\u00020\u00042\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0014¢\u0006\u0004\b\u001f\u0010\u001eJ\u001b\u0010\"\u001a\u00020\u00042\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00040 ¢\u0006\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&¨\u00064"}, d2 = {"Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;", "Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "", "configureStreamPreview", "(Lcom/discord/utilities/streams/StreamContext;)V", "", "state", "Lcom/discord/api/activity/ActivityParty;", "party", "getRichPresenceStateText", "(Ljava/lang/String;Lcom/discord/api/activity/ActivityParty;)Ljava/lang/String;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "isMe", "Lcom/discord/models/user/User;", "user", "Landroid/content/Context;", "Lcom/discord/app/ApplicationContext;", "applicationContext", "Lcom/discord/widgets/user/presence/ModelRichPresence;", "richPresence", "userInSameVoiceChannel", "configureUi", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/user/User;Landroid/content/Context;Lcom/discord/widgets/user/presence/ModelRichPresence;Z)V", "Lcom/discord/api/activity/Activity;", "model", "configureTextUi", "(Lcom/discord/api/activity/Activity;Lcom/discord/utilities/streams/StreamContext;)V", "configureAssetUi", "Lkotlin/Function0;", "onStreamPreviewClicked", "setOnStreamPreviewClicked", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/views/StreamPreviewView;", "streamPreview", "Lcom/discord/views/StreamPreviewView;", "Landroid/view/View;", "root", "Lcom/facebook/drawee/view/SimpleDraweeView;", "richPresenceImageLarge", "Landroid/widget/TextView;", "richPresenceHeader", "richPresenceTitle", "richPresenceDetails", "richPresenceTime", "richPresenceState", "richPresenceTextContainer", HookHelper.constructorName, "(Landroid/view/View;Lcom/facebook/drawee/view/SimpleDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Lcom/discord/views/StreamPreviewView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewHolderStreamRichPresence extends ViewHolderUserRichPresence {
    private final StreamPreviewView streamPreview;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewHolderStreamRichPresence(View view, SimpleDraweeView simpleDraweeView, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view2, StreamPreviewView streamPreviewView) {
        super(view, simpleDraweeView, null, textView, textView2, textView3, textView4, textView5, view2, null, null, 4);
        m.checkNotNullParameter(view, "root");
        m.checkNotNullParameter(simpleDraweeView, "richPresenceImageLarge");
        m.checkNotNullParameter(textView, "richPresenceHeader");
        m.checkNotNullParameter(textView2, "richPresenceTitle");
        m.checkNotNullParameter(textView3, "richPresenceDetails");
        m.checkNotNullParameter(textView4, "richPresenceTime");
        m.checkNotNullParameter(textView5, "richPresenceState");
        m.checkNotNullParameter(view2, "richPresenceTextContainer");
        m.checkNotNullParameter(streamPreviewView, "streamPreview");
        this.streamPreview = streamPreviewView;
    }

    private final void configureStreamPreview(StreamContext streamContext) {
        StoreApplicationStreamPreviews.StreamPreview preview = streamContext != null ? streamContext.getPreview() : null;
        if (preview == null) {
            this.streamPreview.setVisibility(8);
            return;
        }
        this.streamPreview.a(preview, streamContext.getJoinability(), streamContext.isCurrentUserParticipating());
        this.streamPreview.setVisibility(0);
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0038, code lost:
        if (r0 != null) goto L11;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final java.lang.String getRichPresenceStateText(java.lang.String r9, com.discord.api.activity.ActivityParty r10) {
        /*
            r8 = this;
            r0 = 0
            if (r9 != 0) goto L4
            return r0
        L4:
            if (r10 == 0) goto L3b
            com.discord.utilities.presence.PresenceUtils r1 = com.discord.utilities.presence.PresenceUtils.INSTANCE
            long r2 = r1.getMaxSize(r10)
            r4 = 0
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 != 0) goto L13
            goto L38
        L13:
            android.view.View r2 = r8.getRoot()
            r3 = 2131895337(0x7f122429, float:1.9425504E38)
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            long r6 = r1.getCurrentSize(r10)
            java.lang.String r6 = java.lang.String.valueOf(r6)
            r4[r5] = r6
            r5 = 1
            long r6 = r1.getMaxSize(r10)
            java.lang.String r10 = java.lang.String.valueOf(r6)
            r4[r5] = r10
            r10 = 4
            java.lang.CharSequence r0 = b.a.k.b.j(r2, r3, r4, r0, r10)
        L38:
            if (r0 == 0) goto L3b
            goto L3d
        L3b:
            java.lang.String r0 = ""
        L3d:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r10.append(r9)
            r9 = 32
            r10.append(r9)
            r10.append(r0)
            java.lang.String r9 = r10.toString()
            java.lang.String r10 = "null cannot be cast to non-null type kotlin.CharSequence"
            java.util.Objects.requireNonNull(r9, r10)
            java.lang.CharSequence r9 = d0.g0.w.trim(r9)
            java.lang.String r9 = r9.toString()
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.presence.ViewHolderStreamRichPresence.getRichPresenceStateText(java.lang.String, com.discord.api.activity.ActivityParty):java.lang.String");
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    public void configureAssetUi(Activity activity, StreamContext streamContext) {
        super.configureAssetUi(activity, streamContext);
        getRichPresenceImageLarge().setVisibility(8);
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    public void configureTextUi(Activity activity, StreamContext streamContext) {
        if (streamContext != null) {
            Guild guild = streamContext.getGuild();
            CharSequence charSequence = null;
            String name = guild != null ? guild.getName() : null;
            getRichPresenceHeader().setText(name != null ? b.d(getRichPresenceHeader(), R.string.user_activity_header_streaming_to_guild, new Object[]{name}, (r4 & 4) != 0 ? b.c.j : null) : b.d(getRichPresenceHeader(), R.string.user_activity_header_streaming_to_dm, new Object[0], (r4 & 4) != 0 ? b.c.j : null));
            if (activity == null || !ActivityUtilsKt.isGameActivity(activity)) {
                getRichPresenceTextContainer().setVisibility(8);
                return;
            }
            String richPresenceStateText = getRichPresenceStateText(activity.l(), activity.i());
            getRichPresenceTextContainer().setVisibility(0);
            getRichPresenceTitle().setText(activity.h());
            TextView richPresenceDetails = getRichPresenceDetails();
            if (richPresenceDetails != null) {
                ViewExtensions.setTextAndVisibilityBy(richPresenceDetails, activity.e());
            }
            TextView richPresenceState = getRichPresenceState();
            if (richPresenceState != null) {
                ViewExtensions.setTextAndVisibilityBy(richPresenceState, richPresenceStateText);
            }
            TextView richPresenceTime = getRichPresenceTime();
            ActivityTimestamps o = activity.o();
            if (o != null) {
                charSequence = friendlyTime(o);
            }
            ViewExtensions.setTextAndVisibilityBy(richPresenceTime, charSequence);
            return;
        }
        throw new IllegalArgumentException("streamContext must not be null");
    }

    @Override // com.discord.widgets.user.presence.ViewHolderUserRichPresence
    public void configureUi(FragmentManager fragmentManager, StreamContext streamContext, boolean z2, User user, Context context, ModelRichPresence modelRichPresence, boolean z3) {
        Presence presence;
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "applicationContext");
        disposeTimer();
        StreamContext.Joinability joinability = null;
        Activity playingActivity = (modelRichPresence == null || (presence = modelRichPresence.getPresence()) == null) ? null : PresenceUtils.INSTANCE.getPlayingActivity(presence);
        if (streamContext != null) {
            joinability = streamContext.getJoinability();
        }
        boolean z4 = joinability == StreamContext.Joinability.MISSING_PERMISSIONS;
        if (!(streamContext == null && playingActivity == null) && !z4) {
            getRoot().setVisibility(0);
            configureTextUi(playingActivity, streamContext);
            configureAssetUi(playingActivity, streamContext);
            configureStreamPreview(streamContext);
            return;
        }
        getRoot().setVisibility(8);
    }

    public final void setOnStreamPreviewClicked(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onStreamPreviewClicked");
        this.streamPreview.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.presence.ViewHolderStreamRichPresence$setOnStreamPreviewClicked$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }
}
