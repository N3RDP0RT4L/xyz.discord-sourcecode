package com.discord.widgets.user;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.a4;
import b.d.b.a.a;
import com.discord.api.presence.ClientStatus;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetUserStatusUpdateBinding;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.user.WidgetUserStatusSheetViewModel;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetUserStatusSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 )2\u00020\u0001:\u0001)B\u0007¢\u0006\u0004\b(\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J!\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001a\u0010\u0010J\u000f\u0010\u001b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001b\u0010\u0010R\u001d\u0010!\u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001d\u0010'\u001a\u00020\"8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&¨\u0006*"}, d2 = {"Lcom/discord/widgets/user/WidgetUserStatusSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;", "viewState", "", "updateView", "(Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;)V", "Lcom/discord/api/presence/ClientStatus;", "status", "updateStateAndDismiss", "(Lcom/discord/api/presence/ClientStatus;)V", "Landroid/content/Context;", "context", "openCustomStatusAndDismiss", "(Landroid/content/Context;)V", "clearCustomStatus", "()V", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "onPause", "Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;", "viewModel", "Lcom/discord/databinding/WidgetUserStatusUpdateBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserStatusUpdateBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserStatusSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserStatusSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserStatusUpdateBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserStatusSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetUserStatusSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;", "", "Landroidx/fragment/app/Fragment;", "fragment", "", "show", "(Landroidx/fragment/app/Fragment;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Fragment fragment) {
            m.checkNotNullParameter(fragment, "fragment");
            WidgetUserStatusSheet widgetUserStatusSheet = new WidgetUserStatusSheet();
            FragmentManager parentFragmentManager = fragment.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "fragment.parentFragmentManager");
            widgetUserStatusSheet.show(parentFragmentManager, "javaClass");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetUserStatusSheet() {
        super(false, 1, null);
        WidgetUserStatusSheet$viewModel$2 widgetUserStatusSheet$viewModel$2 = WidgetUserStatusSheet$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetUserStatusSheetViewModel.class), new WidgetUserStatusSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetUserStatusSheet$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void clearCustomStatus() {
        getViewModel().clearCustomStatus();
    }

    private final WidgetUserStatusUpdateBinding getBinding() {
        return (WidgetUserStatusUpdateBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final WidgetUserStatusSheetViewModel getViewModel() {
        return (WidgetUserStatusSheetViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void openCustomStatusAndDismiss(Context context) {
        AnalyticsTracker.INSTANCE.openModal("Custom Status Modal", new Traits.Source(null, "Account Panel", "Avatar", null, null, 25, null));
        WidgetUserSetCustomStatus.Companion.launch(context);
        dismiss();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void updateStateAndDismiss(ClientStatus clientStatus) {
        getViewModel().setStatus(clientStatus);
        dismiss();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateView(WidgetUserStatusSheetViewModel.ViewState viewState) {
        if (viewState instanceof WidgetUserStatusSheetViewModel.ViewState.Loaded) {
            getBinding().f2671b.updateViewState(((WidgetUserStatusSheetViewModel.ViewState.Loaded) viewState).getCustomStatusViewState());
        }
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_user_status_update;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        dismiss();
        super.onPause();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetUserStatusSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserStatusSheet$onResume$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        getBinding().f.f77b.setImageResource(R.drawable.ic_status_online_16dp);
        getBinding().f.d.setText(R.string.status_online);
        a4 a4Var = getBinding().f;
        m.checkNotNullExpressionValue(a4Var, "binding.userStatusUpdateOnline");
        a4Var.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetUserStatusSheet$onViewCreated$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserStatusSheet.this.updateStateAndDismiss(ClientStatus.ONLINE);
            }
        });
        getBinding().d.f77b.setImageResource(R.drawable.ic_status_idle_16dp);
        getBinding().d.d.setText(R.string.status_idle);
        a4 a4Var2 = getBinding().d;
        m.checkNotNullExpressionValue(a4Var2, "binding.userStatusUpdateIdle");
        a4Var2.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetUserStatusSheet$onViewCreated$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserStatusSheet.this.updateStateAndDismiss(ClientStatus.IDLE);
            }
        });
        getBinding().c.f77b.setImageResource(R.drawable.ic_status_dnd_16dp);
        getBinding().c.d.setText(R.string.status_dnd);
        getBinding().c.c.setText(R.string.status_dnd_help);
        a4 a4Var3 = getBinding().c;
        m.checkNotNullExpressionValue(a4Var3, "binding.userStatusUpdateDnd");
        a4Var3.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetUserStatusSheet$onViewCreated$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserStatusSheet.this.updateStateAndDismiss(ClientStatus.DND);
            }
        });
        getBinding().e.f77b.setImageResource(R.drawable.ic_status_invisible_16dp);
        getBinding().e.d.setText(R.string.status_invisible);
        getBinding().e.c.setText(R.string.status_invisible_helper);
        a4 a4Var4 = getBinding().e;
        m.checkNotNullExpressionValue(a4Var4, "binding.userStatusUpdateInvisible");
        a4Var4.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetUserStatusSheet$onViewCreated$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserStatusSheet.this.updateStateAndDismiss(ClientStatus.INVISIBLE);
            }
        });
        getBinding().f2671b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.WidgetUserStatusSheet$onViewCreated$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserStatusSheet widgetUserStatusSheet = WidgetUserStatusSheet.this;
                m.checkNotNullExpressionValue(view2, "it");
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                widgetUserStatusSheet.openCustomStatusAndDismiss(context);
            }
        });
        getBinding().f2671b.setOnClear(new WidgetUserStatusSheet$onViewCreated$6(this));
    }
}
