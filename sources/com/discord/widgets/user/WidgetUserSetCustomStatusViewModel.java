package com.discord.widgets.user;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelCustomStatusSetting;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.stores.StoreEmoji;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.TimeUtils;
import d0.g0.w;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Calendar;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetUserSetCustomStatusViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 >2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005>?@ABB+\u0012\b\b\u0002\u0010.\u001a\u00020-\u0012\b\b\u0002\u00107\u001a\u000206\u0012\u000e\b\u0002\u0010;\u001a\b\u0012\u0004\u0012\u00020\u00030\u001e¢\u0006\u0004\b<\u0010=J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u0010\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J!\u0010\u0017\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0019\u0010\u0019\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001d\u0010\u001cJ\u0013\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e¢\u0006\u0004\b \u0010!J\u0015\u0010#\u001a\u00020\u00052\u0006\u0010\"\u001a\u00020\u0016¢\u0006\u0004\b#\u0010$J\u0015\u0010&\u001a\u00020\u00052\u0006\u0010%\u001a\u00020\u000e¢\u0006\u0004\b&\u0010'J\r\u0010(\u001a\u00020\u0005¢\u0006\u0004\b(\u0010\u001cJ\u0015\u0010*\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\f¢\u0006\u0004\b*\u0010+J\r\u0010,\u001a\u00020\u0005¢\u0006\u0004\b,\u0010\u001cR\u0019\u0010.\u001a\u00020-8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R:\u00104\u001a&\u0012\f\u0012\n 3*\u0004\u0018\u00010\u001f0\u001f 3*\u0012\u0012\f\u0012\n 3*\u0004\u0018\u00010\u001f0\u001f\u0018\u000102028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R\u0019\u00107\u001a\u0002068\u0006@\u0006¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b9\u0010:¨\u0006C"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$StoreState;)V", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;", "formState", "updateFormState", "(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;)V", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", "expiresAt", "", "Lcom/discord/primitives/UtcTimestamp;", "getExpirationUTCDateString", "(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)Ljava/lang/String;", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "customStatusSetting", "Lcom/discord/models/domain/emoji/EmojiSet;", "emojiSet", "Lcom/discord/models/domain/emoji/Emoji;", "getEmojiFromSetting", "(Lcom/discord/models/domain/ModelCustomStatusSetting;Lcom/discord/models/domain/emoji/EmojiSet;)Lcom/discord/models/domain/emoji/Emoji;", "getStatusTextFromSetting", "(Lcom/discord/models/domain/ModelCustomStatusSetting;)Ljava/lang/String;", "emitSetStatusSuccessEvent", "()V", "emitSetStatusFailureEvent", "Lrx/Observable;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event;", "observeEvents", "()Lrx/Observable;", "emoji", "setStatusEmoji", "(Lcom/discord/models/domain/emoji/Emoji;)V", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setStatusText", "(Ljava/lang/String;)V", "clearStatusTextAndEmoji", "expiration", "setExpiration", "(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V", "saveStatus", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUserSettings;", "getStoreUserSettings", "()Lcom/discord/stores/StoreUserSettings;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "getClock", "()Lcom/discord/utilities/time/Clock;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreUserSettings;Lcom/discord/utilities/time/Clock;Lrx/Observable;)V", "Companion", "Event", "FormState", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserSetCustomStatusViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private static final FormState.Expiration DEFAULT_EXPIRATION = FormState.Expiration.TOMORROW;
    private final Clock clock;
    private final PublishSubject<Event> eventSubject;
    private final StoreUserSettings storeUserSettings;

    /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.user.WidgetUserSetCustomStatusViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetUserSetCustomStatusViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J%\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u000b\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Companion;", "", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreEmoji;", "storeEmoji", "Lrx/Observable;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreEmoji;)Lrx/Observable;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", "DEFAULT_EXPIRATION", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", "getDEFAULT_EXPIRATION", "()Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(StoreUserSettings storeUserSettings, StoreEmoji storeEmoji) {
            Observable<StoreState> Z = Observable.j(storeUserSettings.observeCustomStatus(), storeEmoji.getEmojiSet(StoreEmoji.EmojiContext.Global.INSTANCE, false, false), WidgetUserSetCustomStatusViewModel$Companion$observeStoreState$1.INSTANCE).Z(1);
            m.checkNotNullExpressionValue(Z, "Observable.combineLatest…        )\n      }.take(1)");
            return Z;
        }

        public final FormState.Expiration getDEFAULT_EXPIRATION() {
            return WidgetUserSetCustomStatusViewModel.DEFAULT_EXPIRATION;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event;", "", HookHelper.constructorName, "()V", "SetStatusFailure", "SetStatusSuccess", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusSuccess;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusFailure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusFailure;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event;", "", "component1", "()I", "failureMessageStringRes", "copy", "(I)Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusFailure;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getFailureMessageStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SetStatusFailure extends Event {
            private final int failureMessageStringRes;

            public SetStatusFailure(int i) {
                super(null);
                this.failureMessageStringRes = i;
            }

            public static /* synthetic */ SetStatusFailure copy$default(SetStatusFailure setStatusFailure, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = setStatusFailure.failureMessageStringRes;
                }
                return setStatusFailure.copy(i);
            }

            public final int component1() {
                return this.failureMessageStringRes;
            }

            public final SetStatusFailure copy(int i) {
                return new SetStatusFailure(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SetStatusFailure) && this.failureMessageStringRes == ((SetStatusFailure) obj).failureMessageStringRes;
                }
                return true;
            }

            public final int getFailureMessageStringRes() {
                return this.failureMessageStringRes;
            }

            public int hashCode() {
                return this.failureMessageStringRes;
            }

            public String toString() {
                return a.A(a.R("SetStatusFailure(failureMessageStringRes="), this.failureMessageStringRes, ")");
            }
        }

        /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusSuccess;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event;", "", "component1", "()I", "successMessageStringRes", "copy", "(I)Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusSuccess;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getSuccessMessageStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SetStatusSuccess extends Event {
            private final int successMessageStringRes;

            public SetStatusSuccess(int i) {
                super(null);
                this.successMessageStringRes = i;
            }

            public static /* synthetic */ SetStatusSuccess copy$default(SetStatusSuccess setStatusSuccess, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = setStatusSuccess.successMessageStringRes;
                }
                return setStatusSuccess.copy(i);
            }

            public final int component1() {
                return this.successMessageStringRes;
            }

            public final SetStatusSuccess copy(int i) {
                return new SetStatusSuccess(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SetStatusSuccess) && this.successMessageStringRes == ((SetStatusSuccess) obj).successMessageStringRes;
                }
                return true;
            }

            public final int getSuccessMessageStringRes() {
                return this.successMessageStringRes;
            }

            public int hashCode() {
                return this.successMessageStringRes;
            }

            public String toString() {
                return a.A(a.R("SetStatusSuccess(successMessageStringRes="), this.successMessageStringRes, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0001 B!\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\nR\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004¨\u0006!"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;", "", "Lcom/discord/models/domain/emoji/Emoji;", "component1", "()Lcom/discord/models/domain/emoji/Emoji;", "", "component2", "()Ljava/lang/String;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", "component3", "()Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", "emoji", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "expiration", "copy", "(Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getText", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", "getExpiration", "Lcom/discord/models/domain/emoji/Emoji;", "getEmoji", HookHelper.constructorName, "(Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V", "Expiration", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class FormState {
        private final Emoji emoji;
        private final Expiration expiration;
        private final String text;

        /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "NEVER", "IN_30_MINUTES", "IN_1_HOUR", "IN_4_HOURS", "TOMORROW", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum Expiration {
            NEVER,
            IN_30_MINUTES,
            IN_1_HOUR,
            IN_4_HOURS,
            TOMORROW
        }

        public FormState(Emoji emoji, String str, Expiration expiration) {
            m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(expiration, "expiration");
            this.emoji = emoji;
            this.text = str;
            this.expiration = expiration;
        }

        public static /* synthetic */ FormState copy$default(FormState formState, Emoji emoji, String str, Expiration expiration, int i, Object obj) {
            if ((i & 1) != 0) {
                emoji = formState.emoji;
            }
            if ((i & 2) != 0) {
                str = formState.text;
            }
            if ((i & 4) != 0) {
                expiration = formState.expiration;
            }
            return formState.copy(emoji, str, expiration);
        }

        public final Emoji component1() {
            return this.emoji;
        }

        public final String component2() {
            return this.text;
        }

        public final Expiration component3() {
            return this.expiration;
        }

        public final FormState copy(Emoji emoji, String str, Expiration expiration) {
            m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(expiration, "expiration");
            return new FormState(emoji, str, expiration);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FormState)) {
                return false;
            }
            FormState formState = (FormState) obj;
            return m.areEqual(this.emoji, formState.emoji) && m.areEqual(this.text, formState.text) && m.areEqual(this.expiration, formState.expiration);
        }

        public final Emoji getEmoji() {
            return this.emoji;
        }

        public final Expiration getExpiration() {
            return this.expiration;
        }

        public final String getText() {
            return this.text;
        }

        public int hashCode() {
            Emoji emoji = this.emoji;
            int i = 0;
            int hashCode = (emoji != null ? emoji.hashCode() : 0) * 31;
            String str = this.text;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            Expiration expiration = this.expiration;
            if (expiration != null) {
                i = expiration.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("FormState(emoji=");
            R.append(this.emoji);
            R.append(", text=");
            R.append(this.text);
            R.append(", expiration=");
            R.append(this.expiration);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$StoreState;", "", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "component1", "()Lcom/discord/models/domain/ModelCustomStatusSetting;", "Lcom/discord/models/domain/emoji/EmojiSet;", "component2", "()Lcom/discord/models/domain/emoji/EmojiSet;", "customStatusSetting", "emojiSet", "copy", "(Lcom/discord/models/domain/ModelCustomStatusSetting;Lcom/discord/models/domain/emoji/EmojiSet;)Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "getCustomStatusSetting", "Lcom/discord/models/domain/emoji/EmojiSet;", "getEmojiSet", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelCustomStatusSetting;Lcom/discord/models/domain/emoji/EmojiSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final ModelCustomStatusSetting customStatusSetting;
        private final EmojiSet emojiSet;

        public StoreState(ModelCustomStatusSetting modelCustomStatusSetting, EmojiSet emojiSet) {
            m.checkNotNullParameter(modelCustomStatusSetting, "customStatusSetting");
            m.checkNotNullParameter(emojiSet, "emojiSet");
            this.customStatusSetting = modelCustomStatusSetting;
            this.emojiSet = emojiSet;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, ModelCustomStatusSetting modelCustomStatusSetting, EmojiSet emojiSet, int i, Object obj) {
            if ((i & 1) != 0) {
                modelCustomStatusSetting = storeState.customStatusSetting;
            }
            if ((i & 2) != 0) {
                emojiSet = storeState.emojiSet;
            }
            return storeState.copy(modelCustomStatusSetting, emojiSet);
        }

        public final ModelCustomStatusSetting component1() {
            return this.customStatusSetting;
        }

        public final EmojiSet component2() {
            return this.emojiSet;
        }

        public final StoreState copy(ModelCustomStatusSetting modelCustomStatusSetting, EmojiSet emojiSet) {
            m.checkNotNullParameter(modelCustomStatusSetting, "customStatusSetting");
            m.checkNotNullParameter(emojiSet, "emojiSet");
            return new StoreState(modelCustomStatusSetting, emojiSet);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.customStatusSetting, storeState.customStatusSetting) && m.areEqual(this.emojiSet, storeState.emojiSet);
        }

        public final ModelCustomStatusSetting getCustomStatusSetting() {
            return this.customStatusSetting;
        }

        public final EmojiSet getEmojiSet() {
            return this.emojiSet;
        }

        public int hashCode() {
            ModelCustomStatusSetting modelCustomStatusSetting = this.customStatusSetting;
            int i = 0;
            int hashCode = (modelCustomStatusSetting != null ? modelCustomStatusSetting.hashCode() : 0) * 31;
            EmojiSet emojiSet = this.emojiSet;
            if (emojiSet != null) {
                i = emojiSet.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(customStatusSetting=");
            R.append(this.customStatusSetting);
            R.append(", emojiSet=");
            R.append(this.emojiSet);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Loaded;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Uninitialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0013\u0010\u0015\u001a\u00020\u00108F@\u0006¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Loaded;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;", "component1", "()Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;", "formState", "copy", "(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;)Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "getShowStatusClear", "()Z", "showStatusClear", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;", "getFormState", HookHelper.constructorName, "(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final FormState formState;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(FormState formState) {
                super(null);
                m.checkNotNullParameter(formState, "formState");
                this.formState = formState;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, FormState formState, int i, Object obj) {
                if ((i & 1) != 0) {
                    formState = loaded.formState;
                }
                return loaded.copy(formState);
            }

            public final FormState component1() {
                return this.formState;
            }

            public final Loaded copy(FormState formState) {
                m.checkNotNullParameter(formState, "formState");
                return new Loaded(formState);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.formState, ((Loaded) obj).formState);
                }
                return true;
            }

            public final FormState getFormState() {
                return this.formState;
            }

            public final boolean getShowStatusClear() {
                if (this.formState.getEmoji() == null) {
                    if (!(this.formState.getText().length() > 0)) {
                        return false;
                    }
                }
                return true;
            }

            public int hashCode() {
                FormState formState = this.formState;
                if (formState != null) {
                    return formState.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(formState=");
                R.append(this.formState);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetUserSetCustomStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            FormState.Expiration.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[FormState.Expiration.NEVER.ordinal()] = 1;
            iArr[FormState.Expiration.IN_30_MINUTES.ordinal()] = 2;
            iArr[FormState.Expiration.IN_1_HOUR.ordinal()] = 3;
            iArr[FormState.Expiration.IN_4_HOURS.ordinal()] = 4;
            iArr[FormState.Expiration.TOMORROW.ordinal()] = 5;
        }
    }

    public WidgetUserSetCustomStatusViewModel() {
        this(null, null, null, 7, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetUserSetCustomStatusViewModel(com.discord.stores.StoreUserSettings r1, com.discord.utilities.time.Clock r2, rx.Observable r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
        /*
            r0 = this;
            r5 = r4 & 1
            if (r5 == 0) goto La
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUserSettings r1 = r1.getUserSettings()
        La:
            r5 = r4 & 2
            if (r5 == 0) goto L12
            com.discord.utilities.time.Clock r2 = com.discord.utilities.time.ClockFactory.get()
        L12:
            r4 = r4 & 4
            if (r4 == 0) goto L26
            com.discord.widgets.user.WidgetUserSetCustomStatusViewModel$Companion r3 = com.discord.widgets.user.WidgetUserSetCustomStatusViewModel.Companion
            com.discord.stores.StoreStream$Companion r4 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUserSettings r5 = r4.getUserSettings()
            com.discord.stores.StoreEmoji r4 = r4.getEmojis()
            rx.Observable r3 = com.discord.widgets.user.WidgetUserSetCustomStatusViewModel.Companion.access$observeStoreState(r3, r5, r4)
        L26:
            r0.<init>(r1, r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.user.WidgetUserSetCustomStatusViewModel.<init>(com.discord.stores.StoreUserSettings, com.discord.utilities.time.Clock, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitSetStatusFailureEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.SetStatusFailure(R.string.default_failure_to_perform_action_message));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitSetStatusSuccessEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.SetStatusSuccess(R.string.custom_status_update_success));
    }

    private final Emoji getEmojiFromSetting(ModelCustomStatusSetting modelCustomStatusSetting, EmojiSet emojiSet) {
        String str;
        if (modelCustomStatusSetting == ModelCustomStatusSetting.Companion.getCLEAR()) {
            return null;
        }
        Long emojiId = modelCustomStatusSetting.getEmojiId();
        if (emojiId == null || (str = String.valueOf(emojiId.longValue())) == null) {
            str = modelCustomStatusSetting.getEmojiName();
        }
        return emojiSet.emojiIndex.get(str);
    }

    private final String getExpirationUTCDateString(FormState.Expiration expiration) {
        Long l;
        long currentTimeMillis = this.clock.currentTimeMillis();
        int ordinal = expiration.ordinal();
        if (ordinal == 0) {
            l = null;
        } else if (ordinal == 1) {
            l = Long.valueOf(currentTimeMillis + StoreGuildScheduledEvents.FETCH_GUILD_EVENTS_THRESHOLD);
        } else if (ordinal == 2) {
            l = Long.valueOf(currentTimeMillis + 3600000);
        } else if (ordinal == 3) {
            l = Long.valueOf(currentTimeMillis + 14400000);
        } else if (ordinal == 4) {
            Calendar calendar = TimeUtils.toCalendar(currentTimeMillis);
            calendar.add(5, 1);
            calendar.set(11, 0);
            calendar.set(12, 0);
            calendar.set(13, 0);
            calendar.set(14, 0);
            l = Long.valueOf(calendar.getTimeInMillis());
        } else {
            throw new NoWhenBranchMatchedException();
        }
        return TimeUtils.toUTCDateTime$default(l, null, 2, null);
    }

    private final String getStatusTextFromSetting(ModelCustomStatusSetting modelCustomStatusSetting) {
        if (modelCustomStatusSetting != ModelCustomStatusSetting.Companion.getCLEAR()) {
            return modelCustomStatusSetting.getText();
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        if (!(getViewState() instanceof ViewState.Loaded)) {
            Emoji emojiFromSetting = getEmojiFromSetting(storeState.getCustomStatusSetting(), storeState.getEmojiSet());
            String statusTextFromSetting = getStatusTextFromSetting(storeState.getCustomStatusSetting());
            if (statusTextFromSetting == null) {
                statusTextFromSetting = "";
            }
            updateViewState(new ViewState.Loaded(new FormState(emojiFromSetting, statusTextFromSetting, DEFAULT_EXPIRATION)));
        }
    }

    private final void updateFormState(FormState formState) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(loaded.copy(formState));
        }
    }

    public final void clearStatusTextAndEmoji() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateFormState(FormState.copy$default(loaded.getFormState(), null, "", null, 4, null));
        }
    }

    public final Clock getClock() {
        return this.clock;
    }

    public final StoreUserSettings getStoreUserSettings() {
        return this.storeUserSettings;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void saveStatus() {
        ModelCustomStatusSetting modelCustomStatusSetting;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            FormState formState = loaded.getFormState();
            String text = formState.getText();
            Objects.requireNonNull(text, "null cannot be cast to non-null type kotlin.CharSequence");
            String obj = w.trim(text).toString();
            if (!(obj.length() > 0)) {
                obj = null;
            }
            FormState formState2 = obj != null || formState.getEmoji() != null ? formState : null;
            if (formState2 != null) {
                Emoji emoji = formState2.getEmoji();
                if (!(emoji instanceof ModelEmojiCustom)) {
                    emoji = null;
                }
                ModelEmojiCustom modelEmojiCustom = (ModelEmojiCustom) emoji;
                Long valueOf = modelEmojiCustom != null ? Long.valueOf(modelEmojiCustom.getId()) : null;
                Emoji emoji2 = formState2.getEmoji();
                if (!(emoji2 instanceof ModelEmojiUnicode)) {
                    emoji2 = null;
                }
                ModelEmojiUnicode modelEmojiUnicode = (ModelEmojiUnicode) emoji2;
                modelCustomStatusSetting = new ModelCustomStatusSetting(obj, valueOf, modelEmojiUnicode != null ? modelEmojiUnicode.getSurrogates() : null, getExpirationUTCDateString(formState2.getExpiration()));
            } else {
                modelCustomStatusSetting = null;
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.storeUserSettings.updateCustomStatus(modelCustomStatusSetting), false, 1, null), this, null, 2, null), WidgetUserSetCustomStatusViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserSetCustomStatusViewModel$saveStatus$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserSetCustomStatusViewModel$saveStatus$1(this));
            if (modelCustomStatusSetting != null) {
                AnalyticsTracker.INSTANCE.customStatusUpdated(formState, new Traits.Location(null, "Account Panel", "Avatar", null, null, 25, null));
            }
        }
    }

    public final void setExpiration(FormState.Expiration expiration) {
        m.checkNotNullParameter(expiration, "expiration");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateFormState(FormState.copy$default(loaded.getFormState(), null, null, expiration, 3, null));
        }
    }

    public final void setStatusEmoji(Emoji emoji) {
        m.checkNotNullParameter(emoji, "emoji");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateFormState(FormState.copy$default(loaded.getFormState(), emoji, null, null, 6, null));
        }
    }

    public final void setStatusText(String str) {
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateFormState(FormState.copy$default(loaded.getFormState(), null, str, null, 5, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserSetCustomStatusViewModel(StoreUserSettings storeUserSettings, Clock clock, Observable<StoreState> observable) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(storeUserSettings, "storeUserSettings");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(observable, "storeObservable");
        this.storeUserSettings = storeUserSettings;
        this.clock = clock;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetUserSetCustomStatusViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        this.eventSubject = PublishSubject.k0();
    }
}
