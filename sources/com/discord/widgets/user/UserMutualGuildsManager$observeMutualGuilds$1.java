package com.discord.widgets.user;

import b.d.b.a.a;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreGuildsSorted;
import d0.d0.f;
import d0.t.g0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: UserMutualGuildsManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/models/guild/Guild;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserMutualGuildsManager$observeMutualGuilds$1 extends o implements Function0<Map<Long, ? extends List<? extends Guild>>> {
    public final /* synthetic */ Collection $userIds;
    public final /* synthetic */ UserMutualGuildsManager this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserMutualGuildsManager$observeMutualGuilds$1(UserMutualGuildsManager userMutualGuildsManager, Collection collection) {
        super(0);
        this.this$0 = userMutualGuildsManager;
        this.$userIds = collection;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends List<? extends Guild>> invoke() {
        StoreGuildsSorted storeGuildsSorted;
        StoreGuilds storeGuilds;
        storeGuildsSorted = this.this$0.storeGuildsSorted;
        LinkedHashMap<Long, Guild> orderedGuilds = storeGuildsSorted.getOrderedGuilds();
        storeGuilds = this.this$0.storeGuilds;
        Map<Long, Map<Long, GuildMember>> members = storeGuilds.getMembers();
        Collection<Guild> values = orderedGuilds.values();
        Collection<Number> collection = this.$userIds;
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(collection, 10)), 16));
        for (Number number : collection) {
            Long valueOf = Long.valueOf(number.longValue());
            long longValue = number.longValue();
            m.checkNotNullExpressionValue(values, "guildsList");
            ArrayList arrayList = new ArrayList();
            for (Object obj : values) {
                Map map = (Map) a.d((Guild) obj, members);
                boolean z2 = true;
                if (map == null || !map.containsKey(Long.valueOf(longValue))) {
                    z2 = false;
                }
                if (z2) {
                    arrayList.add(obj);
                }
            }
            linkedHashMap.put(valueOf, arrayList);
        }
        return linkedHashMap;
    }
}
