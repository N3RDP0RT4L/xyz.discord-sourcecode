package com.discord.widgets.user.phone;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import b.a.d.j;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetUserPhoneVerifyBinding;
import com.discord.models.domain.ModelPhoneVerificationToken;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.error.Error;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.sms.SmsListener;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.user.WidgetUserPasswordVerify;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetUserPhoneVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\u0006J\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0013\u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006 "}, d2 = {"Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;", "", "verificationCode", "", "autofillCode", "(Ljava/lang/String;)V", "phoneToken", "handleCodeReceived", "handleCodeEntered", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "passwordVerifyLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/databinding/WidgetUserPhoneVerifyBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserPhoneVerifyBinding;", "binding", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserPhoneVerify extends WidgetUserAccountVerifyBase {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserPhoneVerify.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserPhoneVerifyBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_PHONE_NUMBER = "INTENT_EXTRA_PHONE_NUMBER";
    private static final String INTENT_EXTRA_SOURCE_TYPE = "INTENT_EXTRA_SOURCE_TYPE";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserPhoneVerify$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> passwordVerifyLauncher = WidgetUserPasswordVerify.Companion.registerForResult(this, new WidgetUserPhoneVerify$passwordVerifyLauncher$1(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetUserPhoneVerify$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetUserPhoneVerify.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J=\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\u000b\u001a\u00020\t¢\u0006\u0004\b\r\u0010\u000eJ)\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0010\u001a\u00020\u000f2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u0011¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify$Companion;", "", "Landroid/content/Context;", "context", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "launcher", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "mode", "", "phoneNumber", "source", "", "launch", "(Landroid/content/Context;Landroidx/activity/result/ActivityResultLauncher;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/app/AppFragment;", "fragment", "Lkotlin/Function0;", "callback", "registerForResult", "(Lcom/discord/app/AppFragment;Lkotlin/jvm/functions/Function0;)Landroidx/activity/result/ActivityResultLauncher;", WidgetUserPhoneVerify.INTENT_EXTRA_PHONE_NUMBER, "Ljava/lang/String;", WidgetUserPhoneVerify.INTENT_EXTRA_SOURCE_TYPE, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, ActivityResultLauncher<Intent> activityResultLauncher, WidgetUserAccountVerifyBase.Mode mode, String str, String str2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(activityResultLauncher, "launcher");
            m.checkNotNullParameter(mode, "mode");
            m.checkNotNullParameter(str2, "source");
            Intent launchIntent = WidgetUserAccountVerifyBase.Companion.getLaunchIntent(mode, true, false);
            if (mode == WidgetUserAccountVerifyBase.Mode.NO_HISTORY_FROM_USER_SETTINGS) {
                launchIntent.addFlags(BasicMeasure.EXACTLY);
            }
            launchIntent.putExtra(WidgetUserPhoneVerify.INTENT_EXTRA_PHONE_NUMBER, str);
            launchIntent.putExtra(WidgetUserPhoneVerify.INTENT_EXTRA_SOURCE_TYPE, str2);
            j.g.f(context, activityResultLauncher, WidgetUserPhoneVerify.class, launchIntent);
        }

        public final ActivityResultLauncher<Intent> registerForResult(AppFragment appFragment, final Function0<Unit> function0) {
            m.checkNotNullParameter(appFragment, "fragment");
            m.checkNotNullParameter(function0, "callback");
            ActivityResultLauncher<Intent> registerForActivityResult = appFragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.user.phone.WidgetUserPhoneVerify$Companion$registerForResult$1
                public final void onActivityResult(ActivityResult activityResult) {
                    m.checkNotNullExpressionValue(activityResult, "activityResult");
                    if (activityResult.getResultCode() == -1) {
                        Function0.this.invoke();
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…k()\n          }\n        }");
            return registerForActivityResult;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetUserPhoneVerify() {
        super(R.layout.widget_user_phone_verify);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void autofillCode(String str) {
        if (getView() != null) {
            WidgetUserPhoneVerifyBinding binding = getBinding();
            binding.d.setOnCodeEntered(WidgetUserPhoneVerify$autofillCode$1$1.INSTANCE);
            binding.d.setCode(str);
            binding.d.setOnCodeEntered(new WidgetUserPhoneVerify$autofillCode$1$2(this));
            Observable<Long> d02 = Observable.d0(500L, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable\n        .time…L, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), WidgetUserPhoneVerify.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserPhoneVerify$autofillCode$2(this, str));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetUserPhoneVerifyBinding getBinding() {
        return (WidgetUserPhoneVerifyBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleCodeEntered(String str) {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_PHONE_NUMBER);
        if (stringExtra != null) {
            m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…A_PHONE_NUMBER) ?: return");
            ObservableExtensionsKt.ui$default(ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(RestAPI.Companion.getApi().phoneVerificationsVerify(new RestAPIParams.VerificationCode(stringExtra, str)), WidgetUserPhoneVerify$handleCodeEntered$1.INSTANCE), false, 1, null), getBinding().c, 0L, 2, null), this, null, 2, null).k(o.h(new Action1<ModelPhoneVerificationToken>() { // from class: com.discord.widgets.user.phone.WidgetUserPhoneVerify$handleCodeEntered$2
                public final void call(ModelPhoneVerificationToken modelPhoneVerificationToken) {
                    WidgetUserPhoneVerify.this.handleCodeReceived(modelPhoneVerificationToken.component1());
                }
            }, getContext(), new Action1<Error>() { // from class: com.discord.widgets.user.phone.WidgetUserPhoneVerify$handleCodeEntered$3
                public final void call(Error error) {
                    WidgetUserPhoneVerifyBinding binding;
                    binding = WidgetUserPhoneVerify.this.getBinding();
                    binding.d.b();
                }
            }));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleCodeReceived(String str) {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_SOURCE_TYPE);
        if (stringExtra != null) {
            m.checkNotNullExpressionValue(stringExtra, "mostRecentIntent.getStri…RA_SOURCE_TYPE) ?: return");
            WidgetUserPasswordVerify.Companion.launchUpdatePhoneNumber(requireContext(), this.passwordVerifyLauncher, str, stringExtra);
        }
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.widgets.user.account.WidgetUserAccountVerifyBase, com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        WidgetUserPhoneVerifyBinding binding = getBinding();
        binding.d.setOnCodeEntered(new WidgetUserPhoneVerify$onViewBound$1$1(this));
        ImageView imageView = binding.f2661b;
        m.checkNotNullExpressionValue(imageView, "closeButton");
        imageView.setVisibility(isForced() ? 8 : 0);
        binding.f2661b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.phone.WidgetUserPhoneVerify$onViewBound$$inlined$with$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                FragmentActivity activity = WidgetUserPhoneVerify.this.e();
                if (activity != null) {
                    activity.onBackPressed();
                }
            }
        });
        if (GrowthTeamFeatures.INSTANCE.isAndroidSmsAutofillEnabled()) {
            SmsListener.Companion.startSmsListener(new WidgetUserPhoneVerify$onViewBound$2(this));
        }
    }
}
