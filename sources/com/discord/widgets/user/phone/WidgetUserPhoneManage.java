package com.discord.widgets.user.phone;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.databinding.WidgetUserPhoneManageBinding;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StorePhone;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.intent.IntentUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.captcha.WidgetCaptcha;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.user.WidgetUserPasswordVerify;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.button.MaterialButton;
import d0.g0.t;
import d0.o;
import d0.t.g0;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetUserPhoneManage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0007¢\u0006\u0004\b!\u0010\bJ\u001b\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\r\u0010\bJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\bR\u0016\u0010\u0013\u001a\u00020\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u001c\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u0018R\u001c\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u0018R\u001d\u0010 \u001a\u00020\u001b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;", "", "captchaKey", "", "updatePhoneNumber", "(Ljava/lang/String;)V", "removePhoneNumber", "()V", "Lcom/discord/models/user/MeUser;", "meUser", "configureUI", "(Lcom/discord/models/user/MeUser;)V", "handlePhoneNumberTextChanged", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "source", "Ljava/lang/String;", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "passwordVerifyLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "captchaLauncher", "phoneVerificationLauncher", "Lcom/discord/databinding/WidgetUserPhoneManageBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetUserPhoneManageBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserPhoneManage extends WidgetUserAccountVerifyBase {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetUserPhoneManage.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUserPhoneManageBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private String source;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetUserPhoneManage$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> passwordVerifyLauncher = WidgetUserPasswordVerify.Companion.registerForResult(this, new WidgetUserPhoneManage$passwordVerifyLauncher$1(this));
    private final ActivityResultLauncher<Intent> phoneVerificationLauncher = WidgetUserPhoneVerify.Companion.registerForResult(this, new WidgetUserPhoneManage$phoneVerificationLauncher$1(this));
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetCaptcha.Companion.registerForResult(this, new WidgetUserPhoneManage$captchaLauncher$1(this));

    /* compiled from: WidgetUserPhoneManage.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\rB\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ%\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\n¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;", "mode", "Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$Companion$Source;", "source", "", "launch", "(Landroid/content/Context;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$Companion$Source;)V", HookHelper.constructorName, "()V", "Source", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetUserPhoneManage.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\u000e\n\u0002\b\r\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$Companion$Source;", "", "", "source", "Ljava/lang/String;", "getSource", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;)V", "USER_ACTION_REQUIRED", "USER_SETTINGS_UPDATE", "GUILD_PHONE_REQUIRED", "MFA_PHONE_UPDATE", "CONTACT_SYNC", "DEFAULT", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum Source {
            USER_ACTION_REQUIRED("user_action_required"),
            USER_SETTINGS_UPDATE("user_settings_update"),
            GUILD_PHONE_REQUIRED("guild_phone_required"),
            MFA_PHONE_UPDATE("mfa_phone_update"),
            CONTACT_SYNC("contact_sync"),
            DEFAULT("update_phone");
            
            private final String source;

            Source(String str) {
                this.source = str;
            }

            public final String getSource() {
                return this.source;
            }
        }

        private Companion() {
        }

        public final void launch(Context context, WidgetUserAccountVerifyBase.Mode mode, Source source) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(mode, "mode");
            m.checkNotNullParameter(source, "source");
            Intent launchIntent = WidgetUserAccountVerifyBase.Companion.getLaunchIntent(mode, true, false);
            if (mode == WidgetUserAccountVerifyBase.Mode.NO_HISTORY_FROM_USER_SETTINGS) {
                launchIntent.addFlags(BasicMeasure.EXACTLY);
            }
            launchIntent.putExtra("intent_args_key", source.getSource());
            j.d(context, WidgetUserPhoneManage.class, launchIntent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetUserPhoneManage() {
        super(R.layout.widget_user_phone_manage);
    }

    public static final /* synthetic */ String access$getSource$p(WidgetUserPhoneManage widgetUserPhoneManage) {
        String str = widgetUserPhoneManage.source;
        if (str == null) {
            m.throwUninitializedPropertyAccessException("source");
        }
        return str;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(MeUser meUser) {
        if (meUser.getPhoneNumber() != null) {
            getBinding().g.setText(R.string.phone_verification_update_title);
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.userPhoneAddDescriptionNote");
            b.m(textView, R.string.phone_verification_current_phone, new Object[]{meUser.getPhoneNumber()}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView2 = getBinding().c;
            m.checkNotNullExpressionValue(textView2, "binding.removePhone");
            textView2.setVisibility(0);
        } else {
            getBinding().g.setText(R.string.enter_phone_title);
            getBinding().d.setText(R.string.enter_phone_description);
            TextView textView3 = getBinding().c;
            m.checkNotNullExpressionValue(textView3, "binding.removePhone");
            textView3.setVisibility(8);
        }
        if (meUser.getEmail() == null) {
            TextView textView4 = getBinding().c;
            m.checkNotNullExpressionValue(textView4, "binding.removePhone");
            textView4.setVisibility(8);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetUserPhoneManageBinding getBinding() {
        return (WidgetUserPhoneManageBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handlePhoneNumberTextChanged() {
        String textOrEmpty = getBinding().f.getTextOrEmpty();
        MaterialButton materialButton = getBinding().e;
        m.checkNotNullExpressionValue(materialButton, "binding.userPhoneAddNext");
        boolean z2 = true;
        if (!(textOrEmpty.length() > 0) || !t.startsWith$default(textOrEmpty, BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX, false, 2, null)) {
            z2 = false;
        }
        materialButton.setEnabled(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void removePhoneNumber() {
        CharSequence c;
        CharSequence c2;
        CharSequence c3;
        CharSequence c4;
        if (!UserUtils.INSTANCE.isMfaSMSEnabled(StoreStream.Companion.getUsers().getMe())) {
            WidgetUserPasswordVerify.Companion.launchRemovePhoneNumber(requireContext(), this.passwordVerifyLauncher);
            return;
        }
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        c = b.c(resources, R.string.user_settings_account_remove_phone_number_warning_title, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        c2 = b.c(resources2, R.string.user_settings_account_remove_phone_number_warning_body, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        Resources resources3 = getResources();
        m.checkNotNullExpressionValue(resources3, "resources");
        c3 = b.c(resources3, R.string.remove, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        Resources resources4 = getResources();
        m.checkNotNullExpressionValue(resources4, "resources");
        c4 = b.c(resources4, R.string.cancel, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, c, c2, c3, c4, g0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new WidgetUserPhoneManage$removePhoneNumber$1(this))), null, null, null, Integer.valueOf((int) R.attr.notice_theme_positive_red), null, null, 0, null, 15808, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updatePhoneNumber(String str) {
        String textOrEmpty = getBinding().f.getTextOrEmpty();
        RestAPI api = RestAPI.Companion.getApi();
        String str2 = this.source;
        if (str2 == null) {
            m.throwUninitializedPropertyAccessException("source");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.restSubscribeOn$default(api.userAddPhone(new RestAPIParams.Phone(textOrEmpty, str2, str)), false, 1, null), getBinding().f2660b, 0L, 2, null), this, null, 2, null), WidgetUserPhoneManage.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetUserPhoneManage$updatePhoneNumber$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserPhoneManage$updatePhoneNumber$2(this, textOrEmpty));
    }

    public static /* synthetic */ void updatePhoneNumber$default(WidgetUserPhoneManage widgetUserPhoneManage, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        widgetUserPhoneManage.updatePhoneNumber(str);
    }

    @Override // com.discord.widgets.user.account.WidgetUserAccountVerifyBase, com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarDisplayHomeAsUpEnabled(!isForced(), Integer.valueOf(DrawableCompat.getThemedDrawableRes$default(view, (int) R.attr.ic_action_bar_close, 0, 2, (Object) null)), !isForced() ? Integer.valueOf((int) R.string.close) : null);
        this.source = IntentUtilsKt.getStringExtraOrDefault(getMostRecentIntent(), "intent_args_key", Companion.Source.DEFAULT.getSource());
        getBinding().f.b(this);
        StorePhone phone = StoreStream.Companion.getPhone();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{phone}, false, null, null, new WidgetUserPhoneManage$onViewBound$1(phone), 14, null), this, null, 2, null), WidgetUserPhoneManage.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserPhoneManage$onViewBound$2(this));
        getBinding().f.a(this, new WidgetUserPhoneManage$onViewBound$3(this));
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.phone.WidgetUserPhoneManage$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserPhoneManage.updatePhoneNumber$default(WidgetUserPhoneManage.this, null, 1, null);
            }
        });
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.user.phone.WidgetUserPhoneManage$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserPhoneManage.this.removePhoneNumber();
            }
        });
    }

    @Override // com.discord.widgets.user.account.WidgetUserAccountVerifyBase, com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreUser.observeMe$default(StoreStream.Companion.getUsers(), false, 1, null), this, null, 2, null), WidgetUserPhoneManage.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetUserPhoneManage$onViewBoundOrOnResume$1(this));
    }
}
