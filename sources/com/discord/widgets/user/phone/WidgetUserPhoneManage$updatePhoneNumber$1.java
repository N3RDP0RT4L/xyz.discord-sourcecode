package com.discord.widgets.user.phone;

import android.content.Context;
import android.content.Intent;
import androidx.activity.result.ActivityResultLauncher;
import com.discord.utilities.error.Error;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.widgets.captcha.WidgetCaptcha;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserPhoneManage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserPhoneManage$updatePhoneNumber$1 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ WidgetUserPhoneManage this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserPhoneManage$updatePhoneNumber$1(WidgetUserPhoneManage widgetUserPhoneManage) {
        super(1);
        this.this$0 = widgetUserPhoneManage;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        ActivityResultLauncher<Intent> activityResultLauncher;
        m.checkNotNullParameter(error, "error");
        if (GrowthTeamFeatures.INSTANCE.isPhoneVerifyCaptchaEnabled()) {
            WidgetCaptcha.Companion companion = WidgetCaptcha.Companion;
            Context requireContext = this.this$0.requireContext();
            activityResultLauncher = this.this$0.captchaLauncher;
            Error.Response response = error.getResponse();
            m.checkNotNullExpressionValue(response, "error.response");
            companion.processErrorsForCaptcha(requireContext, activityResultLauncher, u.toMutableList((Collection) response.getMessages().keySet()), error);
        }
    }
}
