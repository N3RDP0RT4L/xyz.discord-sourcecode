package com.discord.widgets.user.phone;

import android.content.Context;
import android.content.Intent;
import androidx.activity.result.ActivityResultLauncher;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import com.discord.widgets.user.phone.WidgetUserPhoneVerify;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUserPhoneManage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserPhoneManage$updatePhoneNumber$2 extends o implements Function1<Void, Unit> {
    public final /* synthetic */ String $phoneNumber;
    public final /* synthetic */ WidgetUserPhoneManage this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserPhoneManage$updatePhoneNumber$2(WidgetUserPhoneManage widgetUserPhoneManage, String str) {
        super(1);
        this.this$0 = widgetUserPhoneManage;
        this.$phoneNumber = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
        invoke2(r1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Void r7) {
        ActivityResultLauncher<Intent> activityResultLauncher;
        WidgetUserAccountVerifyBase.Mode mode;
        WidgetUserPhoneVerify.Companion companion = WidgetUserPhoneVerify.Companion;
        Context requireContext = this.this$0.requireContext();
        activityResultLauncher = this.this$0.phoneVerificationLauncher;
        mode = this.this$0.getMode();
        companion.launch(requireContext, activityResultLauncher, mode, this.$phoneNumber, WidgetUserPhoneManage.access$getSource$p(this.this$0));
    }
}
