package com.discord.widgets.user.phone;

import androidx.fragment.app.FragmentActivity;
import b.a.d.j;
import com.discord.widgets.settings.account.WidgetSettingsAccount;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetUserPhoneVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetUserPhoneVerify$passwordVerifyLauncher$1 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetUserPhoneVerify this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetUserPhoneVerify$passwordVerifyLauncher$1(WidgetUserPhoneVerify widgetUserPhoneVerify) {
        super(0);
        this.this$0 = widgetUserPhoneVerify;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetUserAccountVerifyBase.Mode mode;
        FragmentActivity requireActivity = this.this$0.requireActivity();
        m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        if (requireActivity.getCallingActivity() != null) {
            this.this$0.requireActivity().setResult(-1);
            this.this$0.requireActivity().finish();
            return;
        }
        mode = this.this$0.getMode();
        if (mode == WidgetUserAccountVerifyBase.Mode.NO_HISTORY_FROM_USER_SETTINGS) {
            WidgetSettingsAccount.Companion.launch(this.this$0.requireContext(), false, WidgetSettingsAccount.Redirect.SMS_BACKUP);
        } else if (!this.this$0.isForced()) {
            j.c(this.this$0.requireContext(), false, null, 6);
        }
    }
}
