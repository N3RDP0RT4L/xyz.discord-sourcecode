package com.discord.widgets.status;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.StageRequestToSpeakState;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreConnectivity;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreRtcConnection;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreUserRelationships;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.media.AppSound;
import com.discord.utilities.media.AppSoundManager;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.streams.StreamContextService;
import com.discord.widgets.stage.StageChannelAPI;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u00102\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0010\u0011\u0012B\u0017\u0012\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;)V", "", "accept", "ackStageInvitationToSpeak", "(Z)V", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalStatusIndicatorViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(WidgetGlobalStatusIndicatorViewModel widgetGlobalStatusIndicatorViewModel) {
            super(1, widgetGlobalStatusIndicatorViewModel, WidgetGlobalStatusIndicatorViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((WidgetGlobalStatusIndicatorViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u008d\u0001\u0010\u001b\u001a&\u0012\f\u0012\n \u001a*\u0004\u0018\u00010\u00190\u0019 \u001a*\u0012\u0012\f\u0012\n \u001a*\u0004\u0018\u00010\u00190\u0019\u0018\u00010\u00180\u00182\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00142\b\b\u0002\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Companion;", "", "Lcom/discord/utilities/streams/StreamContextService;", "streamContextService", "Lcom/discord/stores/StoreVoiceChannelSelected;", "storeVoiceChannelSelected", "Lcom/discord/stores/StoreConnectivity;", "storeConnectivity", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreRtcConnection;", "storeRtcConnection", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreVoiceParticipants;", "storeVoiceParticipants", "Lcom/discord/stores/StoreStageChannels;", "storeStageChannels", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StoreStageInstances;", "storeStageInstances", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lrx/Observable;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;", "kotlin.jvm.PlatformType", "observeStoreState", "(Lcom/discord/utilities/streams/StreamContextService;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreConnectivity;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreStageChannels;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreStageInstances;Lcom/discord/stores/updates/ObservationDeck;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStoreState(final StreamContextService streamContextService, StoreVoiceChannelSelected storeVoiceChannelSelected, final StoreConnectivity storeConnectivity, final StoreChannelsSelected storeChannelsSelected, final StoreRtcConnection storeRtcConnection, final StoreGuilds storeGuilds, final StoreVoiceParticipants storeVoiceParticipants, final StoreStageChannels storeStageChannels, final StoreUserRelationships storeUserRelationships, final StoreStageInstances storeStageInstances, final ObservationDeck observationDeck) {
            return storeVoiceChannelSelected.observeSelectedChannel().Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$Companion$observeStoreState$1

                /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreConnectivity$DelayedState;", "p1", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$ConnectivityState;", "invoke", "(Lcom/discord/stores/StoreConnectivity$DelayedState;)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$ConnectivityState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$Companion$observeStoreState$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreConnectivity.DelayedState, WidgetGlobalStatusIndicatorViewModel.StoreState.ConnectivityState> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    public AnonymousClass1() {
                        super(1, WidgetGlobalStatusIndicatorViewModel.StoreState.ConnectivityState.class, HookHelper.constructorName, "<init>(Lcom/discord/stores/StoreConnectivity$DelayedState;)V", 0);
                    }

                    public final WidgetGlobalStatusIndicatorViewModel.StoreState.ConnectivityState invoke(StoreConnectivity.DelayedState delayedState) {
                        m.checkNotNullParameter(delayedState, "p1");
                        return new WidgetGlobalStatusIndicatorViewModel.StoreState.ConnectivityState(delayedState);
                    }
                }

                /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$Companion$observeStoreState$1$2  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass2 extends o implements Function0<Integer> {
                    public final /* synthetic */ Channel $channel;

                    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                    public AnonymousClass2(Channel channel) {
                        super(0);
                        this.$channel = channel;
                    }

                    /* JADX WARN: Type inference failed for: r0v3, types: [int, java.lang.Integer] */
                    @Override // kotlin.jvm.functions.Function0
                    public final Integer invoke() {
                        Map<Long, StageRoles> channelRoles = storeStageChannels.getChannelRoles(this.$channel.h());
                        Map<Long, Integer> relationships = storeUserRelationships.getRelationships();
                        LinkedHashMap linkedHashMap = new LinkedHashMap();
                        for (Map.Entry<Long, Integer> entry : relationships.entrySet()) {
                            long longValue = entry.getKey().longValue();
                            boolean z2 = true;
                            if (!(entry.getValue().intValue() == 2 && channelRoles != null && channelRoles.containsKey(Long.valueOf(longValue)))) {
                                z2 = false;
                            }
                            if (z2) {
                                linkedHashMap.put(entry.getKey(), entry.getValue());
                            }
                        }
                        return linkedHashMap.size();
                    }
                }

                /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0002\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00072\u0016\u0010\r\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f0\t2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014¢\u0006\u0004\b\u0017\u0010\u0018"}, d2 = {"Lcom/discord/api/channel/Channel;", "p1", "p2", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "p3", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "p4", "Lcom/discord/models/guild/Guild;", "p5", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "p6", "Lcom/discord/utilities/streams/StreamContext;", "p7", "Lcom/discord/api/voice/state/StageRequestToSpeakState;", "p8", "", "p9", "Lcom/discord/api/stageinstance/StageInstance;", "p10", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$CallOngoing;", "invoke", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$StateChange;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;Ljava/util/Map;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/voice/state/StageRequestToSpeakState;ILcom/discord/api/stageinstance/StageInstance;)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$CallOngoing;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$Companion$observeStoreState$1$3  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final /* synthetic */ class AnonymousClass3 extends k implements Function10<Channel, Channel, RtcConnection.StateChange, RtcConnection.Quality, Guild, Map<Long, ? extends StoreVoiceParticipants.VoiceUser>, StreamContext, StageRequestToSpeakState, Integer, StageInstance, WidgetGlobalStatusIndicatorViewModel.StoreState.CallOngoing> {
                    public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

                    public AnonymousClass3() {
                        super(10, WidgetGlobalStatusIndicatorViewModel.StoreState.CallOngoing.class, HookHelper.constructorName, "<init>(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$StateChange;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;Ljava/util/Map;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/voice/state/StageRequestToSpeakState;ILcom/discord/api/stageinstance/StageInstance;)V", 0);
                    }

                    @Override // kotlin.jvm.functions.Function10
                    public /* bridge */ /* synthetic */ WidgetGlobalStatusIndicatorViewModel.StoreState.CallOngoing invoke(Channel channel, Channel channel2, RtcConnection.StateChange stateChange, RtcConnection.Quality quality, Guild guild, Map<Long, ? extends StoreVoiceParticipants.VoiceUser> map, StreamContext streamContext, StageRequestToSpeakState stageRequestToSpeakState, Integer num, StageInstance stageInstance) {
                        return invoke(channel, channel2, stateChange, quality, guild, (Map<Long, StoreVoiceParticipants.VoiceUser>) map, streamContext, stageRequestToSpeakState, num.intValue(), stageInstance);
                    }

                    public final WidgetGlobalStatusIndicatorViewModel.StoreState.CallOngoing invoke(Channel channel, Channel channel2, RtcConnection.StateChange stateChange, RtcConnection.Quality quality, Guild guild, Map<Long, StoreVoiceParticipants.VoiceUser> map, StreamContext streamContext, StageRequestToSpeakState stageRequestToSpeakState, int i, StageInstance stageInstance) {
                        m.checkNotNullParameter(channel, "p1");
                        m.checkNotNullParameter(stateChange, "p3");
                        m.checkNotNullParameter(quality, "p4");
                        m.checkNotNullParameter(map, "p6");
                        m.checkNotNullParameter(stageRequestToSpeakState, "p8");
                        return new WidgetGlobalStatusIndicatorViewModel.StoreState.CallOngoing(channel, channel2, stateChange, quality, guild, map, streamContext, stageRequestToSpeakState, i, stageInstance);
                    }
                }

                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r2v1, types: [com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$Companion$observeStoreState$1$1, kotlin.jvm.functions.Function1] */
                public final Observable<? extends WidgetGlobalStatusIndicatorViewModel.StoreState> call(Channel channel) {
                    if (channel == null) {
                        Observable<StoreConnectivity.DelayedState> observeState = StoreConnectivity.this.observeState();
                        final ?? r2 = AnonymousClass1.INSTANCE;
                        b<? super StoreConnectivity.DelayedState, ? extends R> bVar = r2;
                        if (r2 != 0) {
                            bVar = new b() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$sam$rx_functions_Func1$0
                                @Override // j0.k.b
                                public final /* synthetic */ Object call(Object obj) {
                                    return Function1.this.invoke(obj);
                                }
                            };
                        }
                        return (Observable<R>) observeState.F(bVar);
                    }
                    j0.l.e.k kVar = new j0.l.e.k(channel);
                    m.checkNotNullExpressionValue(kVar, "Observable\n                        .just(channel)");
                    return ObservableCombineLatestOverloadsKt.combineLatest(kVar, storeChannelsSelected.observeSelectedChannel(), storeRtcConnection.getConnectionState(), storeRtcConnection.getQuality(), storeGuilds.observeGuild(channel.f()), storeVoiceParticipants.get(channel.h()), streamContextService.getForActiveStream(), storeStageChannels.observeMyRequestToSpeakState(channel.h()), ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeUserRelationships, storeStageChannels}, false, null, null, new AnonymousClass2(channel), 14, null), storeStageInstances.observeStageInstanceForChannel(channel.h()), AnonymousClass3.INSTANCE);
                }
            });
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, StreamContextService streamContextService, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreConnectivity storeConnectivity, StoreChannelsSelected storeChannelsSelected, StoreRtcConnection storeRtcConnection, StoreGuilds storeGuilds, StoreVoiceParticipants storeVoiceParticipants, StoreStageChannels storeStageChannels, StoreUserRelationships storeUserRelationships, StoreStageInstances storeStageInstances, ObservationDeck observationDeck, int i, Object obj) {
            return companion.observeStoreState(streamContextService, storeVoiceChannelSelected, storeConnectivity, storeChannelsSelected, storeRtcConnection, storeGuilds, storeVoiceParticipants, storeStageChannels, storeUserRelationships, storeStageInstances, (i & 1024) != 0 ? ObservationDeckProvider.get() : observationDeck);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;", "", HookHelper.constructorName, "()V", "CallOngoing", "ConnectivityState", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$ConnectivityState;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$CallOngoing;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u0001Bo\u0012\u0006\u0010!\u001a\u00020\u0002\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010#\u001a\u00020\u0006\u0012\u0006\u0010$\u001a\u00020\t\u0012\b\u0010%\u001a\u0004\u0018\u00010\f\u0012\u0016\u0010&\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000f\u0012\b\u0010'\u001a\u0004\u0018\u00010\u0015\u0012\u0006\u0010(\u001a\u00020\u0018\u0012\u0006\u0010)\u001a\u00020\u001b\u0012\b\u0010*\u001a\u0004\u0018\u00010\u001e¢\u0006\u0004\bI\u0010JJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ \u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u008c\u0001\u0010+\u001a\u00020\u00002\b\b\u0002\u0010!\u001a\u00020\u00022\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010#\u001a\u00020\u00062\b\b\u0002\u0010$\u001a\u00020\t2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\f2\u0018\b\u0002\u0010&\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000f2\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u00152\b\b\u0002\u0010(\u001a\u00020\u00182\b\b\u0002\u0010)\u001a\u00020\u001b2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u001eHÆ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010.\u001a\u00020-HÖ\u0001¢\u0006\u0004\b.\u0010/J\u0010\u00100\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b0\u0010\u001dJ\u001a\u00104\u001a\u0002032\b\u00102\u001a\u0004\u0018\u000101HÖ\u0003¢\u0006\u0004\b4\u00105R\u001b\u0010%\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u00106\u001a\u0004\b7\u0010\u000eR\u0019\u0010)\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u00108\u001a\u0004\b9\u0010\u001dR\u0019\u0010#\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010:\u001a\u0004\b;\u0010\bR\u0019\u0010!\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010<\u001a\u0004\b=\u0010\u0004R\u001b\u0010*\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010>\u001a\u0004\b?\u0010 R\u001b\u0010'\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010@\u001a\u0004\bA\u0010\u0017R\u0019\u0010$\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010B\u001a\u0004\bC\u0010\u000bR\u0019\u0010(\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010D\u001a\u0004\bE\u0010\u001aR)\u0010&\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010F\u001a\u0004\bG\u0010\u0014R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010<\u001a\u0004\bH\u0010\u0004¨\u0006K"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$CallOngoing;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "component2", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "component3", "()Lcom/discord/rtcconnection/RtcConnection$StateChange;", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "component4", "()Lcom/discord/rtcconnection/RtcConnection$Quality;", "Lcom/discord/models/guild/Guild;", "component5", "()Lcom/discord/models/guild/Guild;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component6", "()Ljava/util/Map;", "Lcom/discord/utilities/streams/StreamContext;", "component7", "()Lcom/discord/utilities/streams/StreamContext;", "Lcom/discord/api/voice/state/StageRequestToSpeakState;", "component8", "()Lcom/discord/api/voice/state/StageRequestToSpeakState;", "", "component9", "()I", "Lcom/discord/api/stageinstance/StageInstance;", "component10", "()Lcom/discord/api/stageinstance/StageInstance;", "selectedVoiceChannel", "selectedTextChannel", "connectionStateChange", "connectionQuality", "guild", "participants", "streamContext", "requestToSpeakState", "blockedUsersOnStage", "stageInstance", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$StateChange;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;Ljava/util/Map;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/voice/state/StageRequestToSpeakState;ILcom/discord/api/stageinstance/StageInstance;)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$CallOngoing;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "I", "getBlockedUsersOnStage", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "getConnectionStateChange", "Lcom/discord/api/channel/Channel;", "getSelectedVoiceChannel", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "getConnectionQuality", "Lcom/discord/api/voice/state/StageRequestToSpeakState;", "getRequestToSpeakState", "Ljava/util/Map;", "getParticipants", "getSelectedTextChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$StateChange;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;Ljava/util/Map;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/api/voice/state/StageRequestToSpeakState;ILcom/discord/api/stageinstance/StageInstance;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CallOngoing extends StoreState {
            private final int blockedUsersOnStage;
            private final RtcConnection.Quality connectionQuality;
            private final RtcConnection.StateChange connectionStateChange;
            private final Guild guild;
            private final Map<Long, StoreVoiceParticipants.VoiceUser> participants;
            private final StageRequestToSpeakState requestToSpeakState;
            private final Channel selectedTextChannel;
            private final Channel selectedVoiceChannel;
            private final StageInstance stageInstance;
            private final StreamContext streamContext;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public CallOngoing(Channel channel, Channel channel2, RtcConnection.StateChange stateChange, RtcConnection.Quality quality, Guild guild, Map<Long, StoreVoiceParticipants.VoiceUser> map, StreamContext streamContext, StageRequestToSpeakState stageRequestToSpeakState, int i, StageInstance stageInstance) {
                super(null);
                m.checkNotNullParameter(channel, "selectedVoiceChannel");
                m.checkNotNullParameter(stateChange, "connectionStateChange");
                m.checkNotNullParameter(quality, "connectionQuality");
                m.checkNotNullParameter(map, "participants");
                m.checkNotNullParameter(stageRequestToSpeakState, "requestToSpeakState");
                this.selectedVoiceChannel = channel;
                this.selectedTextChannel = channel2;
                this.connectionStateChange = stateChange;
                this.connectionQuality = quality;
                this.guild = guild;
                this.participants = map;
                this.streamContext = streamContext;
                this.requestToSpeakState = stageRequestToSpeakState;
                this.blockedUsersOnStage = i;
                this.stageInstance = stageInstance;
            }

            public final Channel component1() {
                return this.selectedVoiceChannel;
            }

            public final StageInstance component10() {
                return this.stageInstance;
            }

            public final Channel component2() {
                return this.selectedTextChannel;
            }

            public final RtcConnection.StateChange component3() {
                return this.connectionStateChange;
            }

            public final RtcConnection.Quality component4() {
                return this.connectionQuality;
            }

            public final Guild component5() {
                return this.guild;
            }

            public final Map<Long, StoreVoiceParticipants.VoiceUser> component6() {
                return this.participants;
            }

            public final StreamContext component7() {
                return this.streamContext;
            }

            public final StageRequestToSpeakState component8() {
                return this.requestToSpeakState;
            }

            public final int component9() {
                return this.blockedUsersOnStage;
            }

            public final CallOngoing copy(Channel channel, Channel channel2, RtcConnection.StateChange stateChange, RtcConnection.Quality quality, Guild guild, Map<Long, StoreVoiceParticipants.VoiceUser> map, StreamContext streamContext, StageRequestToSpeakState stageRequestToSpeakState, int i, StageInstance stageInstance) {
                m.checkNotNullParameter(channel, "selectedVoiceChannel");
                m.checkNotNullParameter(stateChange, "connectionStateChange");
                m.checkNotNullParameter(quality, "connectionQuality");
                m.checkNotNullParameter(map, "participants");
                m.checkNotNullParameter(stageRequestToSpeakState, "requestToSpeakState");
                return new CallOngoing(channel, channel2, stateChange, quality, guild, map, streamContext, stageRequestToSpeakState, i, stageInstance);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof CallOngoing)) {
                    return false;
                }
                CallOngoing callOngoing = (CallOngoing) obj;
                return m.areEqual(this.selectedVoiceChannel, callOngoing.selectedVoiceChannel) && m.areEqual(this.selectedTextChannel, callOngoing.selectedTextChannel) && m.areEqual(this.connectionStateChange, callOngoing.connectionStateChange) && m.areEqual(this.connectionQuality, callOngoing.connectionQuality) && m.areEqual(this.guild, callOngoing.guild) && m.areEqual(this.participants, callOngoing.participants) && m.areEqual(this.streamContext, callOngoing.streamContext) && m.areEqual(this.requestToSpeakState, callOngoing.requestToSpeakState) && this.blockedUsersOnStage == callOngoing.blockedUsersOnStage && m.areEqual(this.stageInstance, callOngoing.stageInstance);
            }

            public final int getBlockedUsersOnStage() {
                return this.blockedUsersOnStage;
            }

            public final RtcConnection.Quality getConnectionQuality() {
                return this.connectionQuality;
            }

            public final RtcConnection.StateChange getConnectionStateChange() {
                return this.connectionStateChange;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final Map<Long, StoreVoiceParticipants.VoiceUser> getParticipants() {
                return this.participants;
            }

            public final StageRequestToSpeakState getRequestToSpeakState() {
                return this.requestToSpeakState;
            }

            public final Channel getSelectedTextChannel() {
                return this.selectedTextChannel;
            }

            public final Channel getSelectedVoiceChannel() {
                return this.selectedVoiceChannel;
            }

            public final StageInstance getStageInstance() {
                return this.stageInstance;
            }

            public final StreamContext getStreamContext() {
                return this.streamContext;
            }

            public int hashCode() {
                Channel channel = this.selectedVoiceChannel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Channel channel2 = this.selectedTextChannel;
                int hashCode2 = (hashCode + (channel2 != null ? channel2.hashCode() : 0)) * 31;
                RtcConnection.StateChange stateChange = this.connectionStateChange;
                int hashCode3 = (hashCode2 + (stateChange != null ? stateChange.hashCode() : 0)) * 31;
                RtcConnection.Quality quality = this.connectionQuality;
                int hashCode4 = (hashCode3 + (quality != null ? quality.hashCode() : 0)) * 31;
                Guild guild = this.guild;
                int hashCode5 = (hashCode4 + (guild != null ? guild.hashCode() : 0)) * 31;
                Map<Long, StoreVoiceParticipants.VoiceUser> map = this.participants;
                int hashCode6 = (hashCode5 + (map != null ? map.hashCode() : 0)) * 31;
                StreamContext streamContext = this.streamContext;
                int hashCode7 = (hashCode6 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
                StageRequestToSpeakState stageRequestToSpeakState = this.requestToSpeakState;
                int hashCode8 = (((hashCode7 + (stageRequestToSpeakState != null ? stageRequestToSpeakState.hashCode() : 0)) * 31) + this.blockedUsersOnStage) * 31;
                StageInstance stageInstance = this.stageInstance;
                if (stageInstance != null) {
                    i = stageInstance.hashCode();
                }
                return hashCode8 + i;
            }

            public String toString() {
                StringBuilder R = a.R("CallOngoing(selectedVoiceChannel=");
                R.append(this.selectedVoiceChannel);
                R.append(", selectedTextChannel=");
                R.append(this.selectedTextChannel);
                R.append(", connectionStateChange=");
                R.append(this.connectionStateChange);
                R.append(", connectionQuality=");
                R.append(this.connectionQuality);
                R.append(", guild=");
                R.append(this.guild);
                R.append(", participants=");
                R.append(this.participants);
                R.append(", streamContext=");
                R.append(this.streamContext);
                R.append(", requestToSpeakState=");
                R.append(this.requestToSpeakState);
                R.append(", blockedUsersOnStage=");
                R.append(this.blockedUsersOnStage);
                R.append(", stageInstance=");
                R.append(this.stageInstance);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$ConnectivityState;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;", "Lcom/discord/stores/StoreConnectivity$DelayedState;", "component1", "()Lcom/discord/stores/StoreConnectivity$DelayedState;", "connectivityState", "copy", "(Lcom/discord/stores/StoreConnectivity$DelayedState;)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState$ConnectivityState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreConnectivity$DelayedState;", "getConnectivityState", HookHelper.constructorName, "(Lcom/discord/stores/StoreConnectivity$DelayedState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ConnectivityState extends StoreState {
            private final StoreConnectivity.DelayedState connectivityState;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ConnectivityState(StoreConnectivity.DelayedState delayedState) {
                super(null);
                m.checkNotNullParameter(delayedState, "connectivityState");
                this.connectivityState = delayedState;
            }

            public static /* synthetic */ ConnectivityState copy$default(ConnectivityState connectivityState, StoreConnectivity.DelayedState delayedState, int i, Object obj) {
                if ((i & 1) != 0) {
                    delayedState = connectivityState.connectivityState;
                }
                return connectivityState.copy(delayedState);
            }

            public final StoreConnectivity.DelayedState component1() {
                return this.connectivityState;
            }

            public final ConnectivityState copy(StoreConnectivity.DelayedState delayedState) {
                m.checkNotNullParameter(delayedState, "connectivityState");
                return new ConnectivityState(delayedState);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ConnectivityState) && m.areEqual(this.connectivityState, ((ConnectivityState) obj).connectivityState);
                }
                return true;
            }

            public final StoreConnectivity.DelayedState getConnectivityState() {
                return this.connectivityState;
            }

            public int hashCode() {
                StoreConnectivity.DelayedState delayedState = this.connectivityState;
                if (delayedState != null) {
                    return delayedState.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("ConnectivityState(connectivityState=");
                R.append(this.connectivityState);
                R.append(")");
                return R.toString();
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\b\t\n\u000b\fB\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0003\u0010\u0005\u0082\u0001\u0005\r\u000e\u000f\u0010\u0011¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", "", "", "isSpeakingInOngoingCall", "Z", "()Z", HookHelper.constructorName, "()V", "CallOngoing", "Connecting", "Inactive", "Offline", "StageChannelOngoing", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Inactive;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Offline;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Connecting;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$StageChannelOngoing;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {
        private final boolean isSpeakingInOngoingCall;

        /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0006\u0012\u0006\u0010\u001b\u001a\u00020\t\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\u001d\u001a\u00020\u000f\u0012\u0006\u0010\u001e\u001a\u00020\u0012\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b:\u0010;J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017Jf\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00022\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\u001b\u001a\u00020\t2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u001d\u001a\u00020\u000f2\b\b\u0002\u0010\u001e\u001a\u00020\u00122\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0015HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b%\u0010\u0011J\u001a\u0010(\u001a\u00020\u00122\b\u0010'\u001a\u0004\u0018\u00010&HÖ\u0003¢\u0006\u0004\b(\u0010)R\u0019\u0010\u001a\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010*\u001a\u0004\b+\u0010\bR\u0019\u0010\u001b\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010,\u001a\u0004\b-\u0010\u000bR\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010.\u001a\u0004\b/\u0010\u0004R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00100\u001a\u0004\b1\u0010\u0017R\u0019\u0010\u001d\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00102\u001a\u0004\b3\u0010\u0011R\u0019\u0010\u001e\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00104\u001a\u0004\b5\u0010\u0014R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00106\u001a\u0004\b7\u0010\u000eR\u001c\u00108\u001a\u00020\u00128\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b8\u00104\u001a\u0004\b8\u0010\u0014R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010.\u001a\u0004\b9\u0010\u0004¨\u0006<"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "component2", "Lcom/discord/rtcconnection/RtcConnection$State;", "component3", "()Lcom/discord/rtcconnection/RtcConnection$State;", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "component4", "()Lcom/discord/rtcconnection/RtcConnection$Quality;", "Lcom/discord/models/guild/Guild;", "component5", "()Lcom/discord/models/guild/Guild;", "", "component6", "()I", "", "component7", "()Z", "Lcom/discord/utilities/streams/StreamContext;", "component8", "()Lcom/discord/utilities/streams/StreamContext;", "selectedVoiceChannel", "selectedTextChannel", "connectionState", "connectionQuality", "guild", "participants", "hasVideo", "streamContext", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;IZLcom/discord/utilities/streams/StreamContext;)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/rtcconnection/RtcConnection$State;", "getConnectionState", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "getConnectionQuality", "Lcom/discord/api/channel/Channel;", "getSelectedVoiceChannel", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", "I", "getParticipants", "Z", "getHasVideo", "Lcom/discord/models/guild/Guild;", "getGuild", "isSpeakingInOngoingCall", "getSelectedTextChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;IZLcom/discord/utilities/streams/StreamContext;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CallOngoing extends ViewState {
            private final RtcConnection.Quality connectionQuality;
            private final RtcConnection.State connectionState;
            private final Guild guild;
            private final boolean hasVideo;
            private final boolean isSpeakingInOngoingCall = true;
            private final int participants;
            private final Channel selectedTextChannel;
            private final Channel selectedVoiceChannel;
            private final StreamContext streamContext;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public CallOngoing(Channel channel, Channel channel2, RtcConnection.State state, RtcConnection.Quality quality, Guild guild, int i, boolean z2, StreamContext streamContext) {
                super(null);
                m.checkNotNullParameter(channel, "selectedVoiceChannel");
                m.checkNotNullParameter(state, "connectionState");
                m.checkNotNullParameter(quality, "connectionQuality");
                this.selectedVoiceChannel = channel;
                this.selectedTextChannel = channel2;
                this.connectionState = state;
                this.connectionQuality = quality;
                this.guild = guild;
                this.participants = i;
                this.hasVideo = z2;
                this.streamContext = streamContext;
            }

            public final Channel component1() {
                return this.selectedVoiceChannel;
            }

            public final Channel component2() {
                return this.selectedTextChannel;
            }

            public final RtcConnection.State component3() {
                return this.connectionState;
            }

            public final RtcConnection.Quality component4() {
                return this.connectionQuality;
            }

            public final Guild component5() {
                return this.guild;
            }

            public final int component6() {
                return this.participants;
            }

            public final boolean component7() {
                return this.hasVideo;
            }

            public final StreamContext component8() {
                return this.streamContext;
            }

            public final CallOngoing copy(Channel channel, Channel channel2, RtcConnection.State state, RtcConnection.Quality quality, Guild guild, int i, boolean z2, StreamContext streamContext) {
                m.checkNotNullParameter(channel, "selectedVoiceChannel");
                m.checkNotNullParameter(state, "connectionState");
                m.checkNotNullParameter(quality, "connectionQuality");
                return new CallOngoing(channel, channel2, state, quality, guild, i, z2, streamContext);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof CallOngoing)) {
                    return false;
                }
                CallOngoing callOngoing = (CallOngoing) obj;
                return m.areEqual(this.selectedVoiceChannel, callOngoing.selectedVoiceChannel) && m.areEqual(this.selectedTextChannel, callOngoing.selectedTextChannel) && m.areEqual(this.connectionState, callOngoing.connectionState) && m.areEqual(this.connectionQuality, callOngoing.connectionQuality) && m.areEqual(this.guild, callOngoing.guild) && this.participants == callOngoing.participants && this.hasVideo == callOngoing.hasVideo && m.areEqual(this.streamContext, callOngoing.streamContext);
            }

            public final RtcConnection.Quality getConnectionQuality() {
                return this.connectionQuality;
            }

            public final RtcConnection.State getConnectionState() {
                return this.connectionState;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final boolean getHasVideo() {
                return this.hasVideo;
            }

            public final int getParticipants() {
                return this.participants;
            }

            public final Channel getSelectedTextChannel() {
                return this.selectedTextChannel;
            }

            public final Channel getSelectedVoiceChannel() {
                return this.selectedVoiceChannel;
            }

            public final StreamContext getStreamContext() {
                return this.streamContext;
            }

            public int hashCode() {
                Channel channel = this.selectedVoiceChannel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Channel channel2 = this.selectedTextChannel;
                int hashCode2 = (hashCode + (channel2 != null ? channel2.hashCode() : 0)) * 31;
                RtcConnection.State state = this.connectionState;
                int hashCode3 = (hashCode2 + (state != null ? state.hashCode() : 0)) * 31;
                RtcConnection.Quality quality = this.connectionQuality;
                int hashCode4 = (hashCode3 + (quality != null ? quality.hashCode() : 0)) * 31;
                Guild guild = this.guild;
                int hashCode5 = (((hashCode4 + (guild != null ? guild.hashCode() : 0)) * 31) + this.participants) * 31;
                boolean z2 = this.hasVideo;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode5 + i2) * 31;
                StreamContext streamContext = this.streamContext;
                if (streamContext != null) {
                    i = streamContext.hashCode();
                }
                return i4 + i;
            }

            @Override // com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel.ViewState
            public boolean isSpeakingInOngoingCall() {
                return this.isSpeakingInOngoingCall;
            }

            public String toString() {
                StringBuilder R = a.R("CallOngoing(selectedVoiceChannel=");
                R.append(this.selectedVoiceChannel);
                R.append(", selectedTextChannel=");
                R.append(this.selectedTextChannel);
                R.append(", connectionState=");
                R.append(this.connectionState);
                R.append(", connectionQuality=");
                R.append(this.connectionQuality);
                R.append(", guild=");
                R.append(this.guild);
                R.append(", participants=");
                R.append(this.participants);
                R.append(", hasVideo=");
                R.append(this.hasVideo);
                R.append(", streamContext=");
                R.append(this.streamContext);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Connecting;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", "", "component1", "()J", "delay", "copy", "(J)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Connecting;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getDelay", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Connecting extends ViewState {
            private final long delay;

            public Connecting(long j) {
                super(null);
                this.delay = j;
            }

            public static /* synthetic */ Connecting copy$default(Connecting connecting, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = connecting.delay;
                }
                return connecting.copy(j);
            }

            public final long component1() {
                return this.delay;
            }

            public final Connecting copy(long j) {
                return new Connecting(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Connecting) && this.delay == ((Connecting) obj).delay;
                }
                return true;
            }

            public final long getDelay() {
                return this.delay;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.delay);
            }

            public String toString() {
                return a.B(a.R("Connecting(delay="), this.delay, ")");
            }
        }

        /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Inactive;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Inactive extends ViewState {
            public static final Inactive INSTANCE = new Inactive();

            private Inactive() {
                super(null);
            }
        }

        /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Offline;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", "", "component1", "()J", "", "component2", "()Z", "delay", "airplaneMode", "copy", "(JZ)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Offline;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getDelay", "Z", "getAirplaneMode", HookHelper.constructorName, "(JZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Offline extends ViewState {
            private final boolean airplaneMode;
            private final long delay;

            public Offline(long j, boolean z2) {
                super(null);
                this.delay = j;
                this.airplaneMode = z2;
            }

            public static /* synthetic */ Offline copy$default(Offline offline, long j, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = offline.delay;
                }
                if ((i & 2) != 0) {
                    z2 = offline.airplaneMode;
                }
                return offline.copy(j, z2);
            }

            public final long component1() {
                return this.delay;
            }

            public final boolean component2() {
                return this.airplaneMode;
            }

            public final Offline copy(long j, boolean z2) {
                return new Offline(j, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Offline)) {
                    return false;
                }
                Offline offline = (Offline) obj;
                return this.delay == offline.delay && this.airplaneMode == offline.airplaneMode;
            }

            public final boolean getAirplaneMode() {
                return this.airplaneMode;
            }

            public final long getDelay() {
                return this.delay;
            }

            public int hashCode() {
                int a = a0.a.a.b.a(this.delay) * 31;
                boolean z2 = this.airplaneMode;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return a + i;
            }

            public String toString() {
                StringBuilder R = a.R("Offline(delay=");
                R.append(this.delay);
                R.append(", airplaneMode=");
                return a.M(R, this.airplaneMode, ")");
            }
        }

        /* compiled from: WidgetGlobalStatusIndicatorViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001Bc\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0005\u0012\u0006\u0010\u001e\u001a\u00020\b\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010 \u001a\u0004\u0018\u00010\u000e\u0012\u0006\u0010!\u001a\u00020\u0011\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0014\u0012\b\b\u0002\u0010#\u001a\u00020\u0011\u0012\b\b\u0002\u0010$\u001a\u00020\u0011\u0012\b\b\u0002\u0010%\u001a\u00020\u0019¢\u0006\u0004\b@\u0010AJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0013J\u0010\u0010\u0018\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0013J\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJz\u0010&\u001a\u00020\u00002\b\b\u0002\u0010\u001c\u001a\u00020\u00022\b\b\u0002\u0010\u001d\u001a\u00020\u00052\b\b\u0002\u0010\u001e\u001a\u00020\b2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010!\u001a\u00020\u00112\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010#\u001a\u00020\u00112\b\b\u0002\u0010$\u001a\u00020\u00112\b\b\u0002\u0010%\u001a\u00020\u0019HÆ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010)\u001a\u00020(HÖ\u0001¢\u0006\u0004\b)\u0010*J\u0010\u0010+\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b+\u0010\u001bJ\u001a\u0010.\u001a\u00020\u00112\b\u0010-\u001a\u0004\u0018\u00010,HÖ\u0003¢\u0006\u0004\b.\u0010/R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00100\u001a\u0004\b1\u0010\rR\u001b\u0010\"\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00102\u001a\u0004\b3\u0010\u0016R\u0019\u0010#\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b#\u0010\u0013R\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u00105\u001a\u0004\b6\u0010\u0010R\u0019\u0010!\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\b!\u0010\u0013R\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00107\u001a\u0004\b8\u0010\u0004R\u0019\u0010%\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b%\u00109\u001a\u0004\b:\u0010\u001bR\u0019\u0010\u001d\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010;\u001a\u0004\b<\u0010\u0007R\u001c\u0010=\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b=\u00104\u001a\u0004\b=\u0010\u0013R\u0019\u0010$\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b$\u00104\u001a\u0004\b$\u0010\u0013R\u0019\u0010\u001e\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010>\u001a\u0004\b?\u0010\n¨\u0006B"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$StageChannelOngoing;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/rtcconnection/RtcConnection$State;", "component2", "()Lcom/discord/rtcconnection/RtcConnection$State;", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "component3", "()Lcom/discord/rtcconnection/RtcConnection$Quality;", "Lcom/discord/models/guild/Guild;", "component4", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/utilities/streams/StreamContext;", "component5", "()Lcom/discord/utilities/streams/StreamContext;", "", "component6", "()Z", "Lcom/discord/api/stageinstance/StageInstance;", "component7", "()Lcom/discord/api/stageinstance/StageInstance;", "component8", "component9", "", "component10", "()I", "selectedVoiceChannel", "connectionState", "connectionQuality", "guild", "streamContext", "isSpeaking", "stageInstance", "isInvitedToSpeak", "isAckingInvitation", "blockedUsersOnStage", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/api/stageinstance/StageInstance;ZZI)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$StageChannelOngoing;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "Z", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", "Lcom/discord/api/channel/Channel;", "getSelectedVoiceChannel", "I", "getBlockedUsersOnStage", "Lcom/discord/rtcconnection/RtcConnection$State;", "getConnectionState", "isSpeakingInOngoingCall", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "getConnectionQuality", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Lcom/discord/models/guild/Guild;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/api/stageinstance/StageInstance;ZZI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class StageChannelOngoing extends ViewState {
            private final int blockedUsersOnStage;
            private final RtcConnection.Quality connectionQuality;
            private final RtcConnection.State connectionState;
            private final Guild guild;
            private final boolean isAckingInvitation;
            private final boolean isInvitedToSpeak;
            private final boolean isSpeaking;
            private final boolean isSpeakingInOngoingCall;
            private final Channel selectedVoiceChannel;
            private final StageInstance stageInstance;
            private final StreamContext streamContext;

            public /* synthetic */ StageChannelOngoing(Channel channel, RtcConnection.State state, RtcConnection.Quality quality, Guild guild, StreamContext streamContext, boolean z2, StageInstance stageInstance, boolean z3, boolean z4, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
                this(channel, state, quality, guild, streamContext, z2, stageInstance, (i2 & 128) != 0 ? false : z3, (i2 & 256) != 0 ? false : z4, (i2 & 512) != 0 ? 0 : i);
            }

            public static /* synthetic */ StageChannelOngoing copy$default(StageChannelOngoing stageChannelOngoing, Channel channel, RtcConnection.State state, RtcConnection.Quality quality, Guild guild, StreamContext streamContext, boolean z2, StageInstance stageInstance, boolean z3, boolean z4, int i, int i2, Object obj) {
                return stageChannelOngoing.copy((i2 & 1) != 0 ? stageChannelOngoing.selectedVoiceChannel : channel, (i2 & 2) != 0 ? stageChannelOngoing.connectionState : state, (i2 & 4) != 0 ? stageChannelOngoing.connectionQuality : quality, (i2 & 8) != 0 ? stageChannelOngoing.guild : guild, (i2 & 16) != 0 ? stageChannelOngoing.streamContext : streamContext, (i2 & 32) != 0 ? stageChannelOngoing.isSpeaking : z2, (i2 & 64) != 0 ? stageChannelOngoing.stageInstance : stageInstance, (i2 & 128) != 0 ? stageChannelOngoing.isInvitedToSpeak : z3, (i2 & 256) != 0 ? stageChannelOngoing.isAckingInvitation : z4, (i2 & 512) != 0 ? stageChannelOngoing.blockedUsersOnStage : i);
            }

            public final Channel component1() {
                return this.selectedVoiceChannel;
            }

            public final int component10() {
                return this.blockedUsersOnStage;
            }

            public final RtcConnection.State component2() {
                return this.connectionState;
            }

            public final RtcConnection.Quality component3() {
                return this.connectionQuality;
            }

            public final Guild component4() {
                return this.guild;
            }

            public final StreamContext component5() {
                return this.streamContext;
            }

            public final boolean component6() {
                return this.isSpeaking;
            }

            public final StageInstance component7() {
                return this.stageInstance;
            }

            public final boolean component8() {
                return this.isInvitedToSpeak;
            }

            public final boolean component9() {
                return this.isAckingInvitation;
            }

            public final StageChannelOngoing copy(Channel channel, RtcConnection.State state, RtcConnection.Quality quality, Guild guild, StreamContext streamContext, boolean z2, StageInstance stageInstance, boolean z3, boolean z4, int i) {
                m.checkNotNullParameter(channel, "selectedVoiceChannel");
                m.checkNotNullParameter(state, "connectionState");
                m.checkNotNullParameter(quality, "connectionQuality");
                return new StageChannelOngoing(channel, state, quality, guild, streamContext, z2, stageInstance, z3, z4, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof StageChannelOngoing)) {
                    return false;
                }
                StageChannelOngoing stageChannelOngoing = (StageChannelOngoing) obj;
                return m.areEqual(this.selectedVoiceChannel, stageChannelOngoing.selectedVoiceChannel) && m.areEqual(this.connectionState, stageChannelOngoing.connectionState) && m.areEqual(this.connectionQuality, stageChannelOngoing.connectionQuality) && m.areEqual(this.guild, stageChannelOngoing.guild) && m.areEqual(this.streamContext, stageChannelOngoing.streamContext) && this.isSpeaking == stageChannelOngoing.isSpeaking && m.areEqual(this.stageInstance, stageChannelOngoing.stageInstance) && this.isInvitedToSpeak == stageChannelOngoing.isInvitedToSpeak && this.isAckingInvitation == stageChannelOngoing.isAckingInvitation && this.blockedUsersOnStage == stageChannelOngoing.blockedUsersOnStage;
            }

            public final int getBlockedUsersOnStage() {
                return this.blockedUsersOnStage;
            }

            public final RtcConnection.Quality getConnectionQuality() {
                return this.connectionQuality;
            }

            public final RtcConnection.State getConnectionState() {
                return this.connectionState;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final Channel getSelectedVoiceChannel() {
                return this.selectedVoiceChannel;
            }

            public final StageInstance getStageInstance() {
                return this.stageInstance;
            }

            public final StreamContext getStreamContext() {
                return this.streamContext;
            }

            public int hashCode() {
                Channel channel = this.selectedVoiceChannel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                RtcConnection.State state = this.connectionState;
                int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
                RtcConnection.Quality quality = this.connectionQuality;
                int hashCode3 = (hashCode2 + (quality != null ? quality.hashCode() : 0)) * 31;
                Guild guild = this.guild;
                int hashCode4 = (hashCode3 + (guild != null ? guild.hashCode() : 0)) * 31;
                StreamContext streamContext = this.streamContext;
                int hashCode5 = (hashCode4 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
                boolean z2 = this.isSpeaking;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode5 + i3) * 31;
                StageInstance stageInstance = this.stageInstance;
                if (stageInstance != null) {
                    i = stageInstance.hashCode();
                }
                int i6 = (i5 + i) * 31;
                boolean z3 = this.isInvitedToSpeak;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.isAckingInvitation;
                if (!z4) {
                    i2 = z4 ? 1 : 0;
                }
                return ((i9 + i2) * 31) + this.blockedUsersOnStage;
            }

            public final boolean isAckingInvitation() {
                return this.isAckingInvitation;
            }

            public final boolean isInvitedToSpeak() {
                return this.isInvitedToSpeak;
            }

            public final boolean isSpeaking() {
                return this.isSpeaking;
            }

            @Override // com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel.ViewState
            public boolean isSpeakingInOngoingCall() {
                return this.isSpeakingInOngoingCall;
            }

            public String toString() {
                StringBuilder R = a.R("StageChannelOngoing(selectedVoiceChannel=");
                R.append(this.selectedVoiceChannel);
                R.append(", connectionState=");
                R.append(this.connectionState);
                R.append(", connectionQuality=");
                R.append(this.connectionQuality);
                R.append(", guild=");
                R.append(this.guild);
                R.append(", streamContext=");
                R.append(this.streamContext);
                R.append(", isSpeaking=");
                R.append(this.isSpeaking);
                R.append(", stageInstance=");
                R.append(this.stageInstance);
                R.append(", isInvitedToSpeak=");
                R.append(this.isInvitedToSpeak);
                R.append(", isAckingInvitation=");
                R.append(this.isAckingInvitation);
                R.append(", blockedUsersOnStage=");
                return a.A(R, this.blockedUsersOnStage, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public StageChannelOngoing(Channel channel, RtcConnection.State state, RtcConnection.Quality quality, Guild guild, StreamContext streamContext, boolean z2, StageInstance stageInstance, boolean z3, boolean z4, int i) {
                super(null);
                m.checkNotNullParameter(channel, "selectedVoiceChannel");
                m.checkNotNullParameter(state, "connectionState");
                m.checkNotNullParameter(quality, "connectionQuality");
                this.selectedVoiceChannel = channel;
                this.connectionState = state;
                this.connectionQuality = quality;
                this.guild = guild;
                this.streamContext = streamContext;
                this.isSpeaking = z2;
                this.stageInstance = stageInstance;
                this.isInvitedToSpeak = z3;
                this.isAckingInvitation = z4;
                this.blockedUsersOnStage = i;
                this.isSpeakingInOngoingCall = z2 || z3;
            }
        }

        private ViewState() {
        }

        public boolean isSpeakingInOngoingCall() {
            return this.isSpeakingInOngoingCall;
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StoreConnectivity.State.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreConnectivity.State.ONLINE.ordinal()] = 1;
            iArr[StoreConnectivity.State.CONNECTING.ordinal()] = 2;
            iArr[StoreConnectivity.State.OFFLINE.ordinal()] = 3;
            iArr[StoreConnectivity.State.OFFLINE_AIRPLANE_MODE.ordinal()] = 4;
        }
    }

    public WidgetGlobalStatusIndicatorViewModel() {
        this(null, 1, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetGlobalStatusIndicatorViewModel(rx.Observable r16, int r17, kotlin.jvm.internal.DefaultConstructorMarker r18) {
        /*
            r15 = this;
            r0 = r17 & 1
            if (r0 == 0) goto L4c
            com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel$Companion r1 = com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel.Companion
            com.discord.utilities.streams.StreamContextService r0 = new com.discord.utilities.streams.StreamContextService
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 255(0xff, float:3.57E-43)
            r12 = 0
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreVoiceChannelSelected r3 = r2.getVoiceChannelSelected()
            com.discord.stores.StoreConnectivity r4 = r2.getConnectivity()
            com.discord.stores.StoreChannelsSelected r5 = r2.getChannelsSelected()
            com.discord.stores.StoreRtcConnection r6 = r2.getRtcConnection()
            com.discord.stores.StoreGuilds r7 = r2.getGuilds()
            com.discord.stores.StoreVoiceParticipants r8 = r2.getVoiceParticipants()
            com.discord.stores.StoreStageChannels r9 = r2.getStageChannels()
            com.discord.stores.StoreUserRelationships r10 = r2.getUserRelationships()
            com.discord.stores.StoreStageInstances r11 = r2.getStageInstances()
            r13 = 1024(0x400, float:1.435E-42)
            r14 = 0
            r2 = r0
            rx.Observable r0 = com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel.Companion.observeStoreState$default(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            java.lang.String r1 = "observeStoreState(\n     ….getStageInstances(),\n  )"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r1 = r15
            goto L4f
        L4c:
            r1 = r15
            r0 = r16
        L4f:
            r15.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel.<init>(rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        boolean z2;
        boolean z3;
        if (storeState instanceof StoreState.ConnectivityState) {
            StoreState.ConnectivityState connectivityState = (StoreState.ConnectivityState) storeState;
            int ordinal = connectivityState.getConnectivityState().getState().ordinal();
            if (ordinal == 0) {
                updateViewState(ViewState.Inactive.INSTANCE);
            } else if (ordinal == 1) {
                updateViewState(new ViewState.Offline(connectivityState.getConnectivityState().getDelay(), false));
            } else if (ordinal == 2) {
                updateViewState(new ViewState.Offline(connectivityState.getConnectivityState().getDelay(), true));
            } else if (ordinal == 3) {
                updateViewState(new ViewState.Connecting(connectivityState.getConnectivityState().getDelay()));
            }
        } else if (storeState instanceof StoreState.CallOngoing) {
            StoreState.CallOngoing callOngoing = (StoreState.CallOngoing) storeState;
            if (ChannelUtils.z(callOngoing.getSelectedVoiceChannel())) {
                boolean z4 = callOngoing.getRequestToSpeakState() == StageRequestToSpeakState.REQUESTED_TO_SPEAK_AND_AWAITING_USER_ACK;
                boolean z5 = callOngoing.getRequestToSpeakState() == StageRequestToSpeakState.ON_STAGE;
                ViewState viewState = getViewState();
                if (!(viewState instanceof ViewState.StageChannelOngoing)) {
                    viewState = null;
                }
                ViewState.StageChannelOngoing stageChannelOngoing = (ViewState.StageChannelOngoing) viewState;
                if (stageChannelOngoing != null && !stageChannelOngoing.isInvitedToSpeak() && z4) {
                    AppSoundManager.Provider.INSTANCE.get().play(AppSound.Companion.getSOUND_RECONNECT());
                }
                updateViewState(new ViewState.StageChannelOngoing(callOngoing.getSelectedVoiceChannel(), callOngoing.getConnectionStateChange().a, callOngoing.getConnectionQuality(), callOngoing.getGuild(), callOngoing.getStreamContext(), z5, callOngoing.getStageInstance(), z4, false, callOngoing.getBlockedUsersOnStage(), 256, null));
                return;
            }
            int size = callOngoing.getParticipants().size();
            Collection<StoreVoiceParticipants.VoiceUser> values = callOngoing.getParticipants().values();
            if (!(values instanceof Collection) || !values.isEmpty()) {
                for (StoreVoiceParticipants.VoiceUser voiceUser : values) {
                    VoiceState voiceState = voiceUser.getVoiceState();
                    if (voiceState == null || !voiceState.j()) {
                        z3 = false;
                        continue;
                    } else {
                        z3 = true;
                        continue;
                    }
                    if (z3) {
                        z2 = true;
                        break;
                    }
                }
            }
            z2 = false;
            updateViewState(new ViewState.CallOngoing(callOngoing.getSelectedVoiceChannel(), callOngoing.getSelectedTextChannel(), callOngoing.getConnectionStateChange().a, callOngoing.getConnectionQuality(), callOngoing.getGuild(), size, z2, callOngoing.getStreamContext()));
        }
    }

    public final void ackStageInvitationToSpeak(boolean z2) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.StageChannelOngoing)) {
            viewState = null;
        }
        ViewState.StageChannelOngoing stageChannelOngoing = (ViewState.StageChannelOngoing) viewState;
        if (stageChannelOngoing != null) {
            updateViewState(ViewState.StageChannelOngoing.copy$default(stageChannelOngoing, null, null, null, null, null, false, null, false, true, 0, 767, null));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(StageChannelAPI.INSTANCE.ackInvitationToSpeak(stageChannelOngoing.getSelectedVoiceChannel(), z2), false, 1, null), this, null, 2, null), WidgetGlobalStatusIndicatorViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGlobalStatusIndicatorViewModel$ackStageInvitationToSpeak$1(this, z2, stageChannelOngoing));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGlobalStatusIndicatorViewModel(Observable<StoreState> observable) {
        super(ViewState.Inactive.INSTANCE);
        m.checkNotNullParameter(observable, "storeStateObservable");
        Observable<StoreState> q = observable.q();
        m.checkNotNullExpressionValue(q, "storeStateObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), WidgetGlobalStatusIndicatorViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
