package com.discord.widgets.status;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.res.Resources;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChatStatusBinding;
import com.discord.models.application.Unread;
import com.discord.stores.StoreStream;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.status.WidgetChatStatus;
import d0.d0.f;
import d0.z.d.m;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func3;
import xyz.discord.R;
/* compiled from: WidgetChatStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u0019B\u0007¢\u0006\u0004\b\u0018\u0010\u0011J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\u000e\u001a\u00020\r2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0017\u001a\u00020\u00128B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/status/WidgetChatStatus;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/status/WidgetChatStatus$Model;", "data", "", "configureUI", "(Lcom/discord/widgets/status/WidgetChatStatus$Model;)V", "", "isEstimate", "", "count", "", "messageId", "", "getUnreadMessageText", "(ZIJ)Ljava/lang/CharSequence;", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetChatStatusBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChatStatusBinding;", "binding", HookHelper.constructorName, ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatStatus extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChatStatus.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChatStatusBinding;", 0)};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChatStatus$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetChatStatus.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0011\b\u0082\b\u0018\u0000 &2\u00020\u0001:\u0001&B7\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\n\u0010\u0010\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010\u0011\u001a\u00060\u0005j\u0002`\t\u0012\u0006\u0010\u0012\u001a\u00020\u000b\u0012\u0006\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0005j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004JJ\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\f\b\u0002\u0010\u0010\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010\u0011\u001a\u00060\u0005j\u0002`\t2\b\b\u0002\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\u0013\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u0019\u0010\rJ\u001a\u0010\u001b\u001a\u00020\u00022\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001d\u001a\u0004\b\u001e\u0010\rR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0004R\u001d\u0010\u0011\u001a\u00060\u0005j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\bR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001f\u001a\u0004\b\u0013\u0010\u0004R\u001d\u0010\u0010\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b#\u0010\b¨\u0006'"}, d2 = {"Lcom/discord/widgets/status/WidgetChatStatus$Model;", "", "", "component1", "()Z", "", "Lcom/discord/primitives/MessageId;", "component2", "()J", "Lcom/discord/primitives/ChannelId;", "component3", "", "component4", "()I", "component5", "unreadVisible", "unreadMessageId", "unreadChannelId", "unreadCount", "isUnreadEstimate", "copy", "(ZJJIZ)Lcom/discord/widgets/status/WidgetChatStatus$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getUnreadCount", "Z", "getUnreadVisible", "J", "getUnreadChannelId", "getUnreadMessageId", HookHelper.constructorName, "(ZJJIZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean isUnreadEstimate;
        private final long unreadChannelId;
        private final int unreadCount;
        private final long unreadMessageId;
        private final boolean unreadVisible;

        /* compiled from: WidgetChatStatus.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J1\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0010\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00042\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0013\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\r¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/status/WidgetChatStatus$Model$Companion;", "", "", "isUnreadValid", "", "", "Lcom/discord/primitives/ChannelId;", "detachedChannels", "Lcom/discord/models/application/Unread;", "unread", "Lcom/discord/widgets/status/WidgetChatStatus$Model;", "createModel", "(ZLjava/util/Set;Lcom/discord/models/application/Unread;)Lcom/discord/widgets/status/WidgetChatStatus$Model;", "Lrx/Observable;", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Model createModel(boolean z2, Set<Long> set, Unread unread) {
                Unread.Marker marker = unread.getMarker();
                boolean contains = set.contains(Long.valueOf(marker.getChannelId()));
                int count = unread.getCount();
                int i = contains ? 25 : 50;
                long j = 0;
                boolean z3 = z2 && unread.getCount() > 0 && marker.getChannelId() > 0;
                long channelId = marker.getChannelId();
                Long messageId = marker.getMessageId();
                if (messageId != null) {
                    j = messageId.longValue();
                }
                return new Model(z3, j, channelId, f.coerceIn(count, 0, i), count >= i);
            }

            public final Observable<Model> get() {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable q = companion.getChannelsSelected().observeId().q().Y(WidgetChatStatus$Model$Companion$get$isUnreadValidObs$1.INSTANCE).q();
                Observable<Set<Long>> allDetached = companion.getMessages().getAllDetached();
                Observable<Unread> unreadMarkerForSelectedChannel = companion.getReadStates().getUnreadMarkerForSelectedChannel();
                final WidgetChatStatus$Model$Companion$get$1 widgetChatStatus$Model$Companion$get$1 = new WidgetChatStatus$Model$Companion$get$1(this);
                Observable combineLatest = ObservableWithLeadingEdgeThrottle.combineLatest(q, allDetached, unreadMarkerForSelectedChannel, new Func3() { // from class: com.discord.widgets.status.WidgetChatStatus$sam$rx_functions_Func3$0
                    @Override // rx.functions.Func3
                    public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3) {
                        return Function3.this.invoke(obj, obj2, obj3);
                    }
                }, 500L, TimeUnit.MILLISECONDS);
                m.checkNotNullExpressionValue(combineLatest, "ObservableWithLeadingEdg…ILLISECONDS\n            )");
                Observable<Model> q2 = ObservableExtensionsKt.computationLatest(combineLatest).q();
                m.checkNotNullExpressionValue(q2, "ObservableWithLeadingEdg…  .distinctUntilChanged()");
                return q2;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(boolean z2, long j, long j2, int i, boolean z3) {
            this.unreadVisible = z2;
            this.unreadMessageId = j;
            this.unreadChannelId = j2;
            this.unreadCount = i;
            this.isUnreadEstimate = z3;
        }

        public static /* synthetic */ Model copy$default(Model model, boolean z2, long j, long j2, int i, boolean z3, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                z2 = model.unreadVisible;
            }
            if ((i2 & 2) != 0) {
                j = model.unreadMessageId;
            }
            long j3 = j;
            if ((i2 & 4) != 0) {
                j2 = model.unreadChannelId;
            }
            long j4 = j2;
            if ((i2 & 8) != 0) {
                i = model.unreadCount;
            }
            int i3 = i;
            if ((i2 & 16) != 0) {
                z3 = model.isUnreadEstimate;
            }
            return model.copy(z2, j3, j4, i3, z3);
        }

        public final boolean component1() {
            return this.unreadVisible;
        }

        public final long component2() {
            return this.unreadMessageId;
        }

        public final long component3() {
            return this.unreadChannelId;
        }

        public final int component4() {
            return this.unreadCount;
        }

        public final boolean component5() {
            return this.isUnreadEstimate;
        }

        public final Model copy(boolean z2, long j, long j2, int i, boolean z3) {
            return new Model(z2, j, j2, i, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return this.unreadVisible == model.unreadVisible && this.unreadMessageId == model.unreadMessageId && this.unreadChannelId == model.unreadChannelId && this.unreadCount == model.unreadCount && this.isUnreadEstimate == model.isUnreadEstimate;
        }

        public final long getUnreadChannelId() {
            return this.unreadChannelId;
        }

        public final int getUnreadCount() {
            return this.unreadCount;
        }

        public final long getUnreadMessageId() {
            return this.unreadMessageId;
        }

        public final boolean getUnreadVisible() {
            return this.unreadVisible;
        }

        public int hashCode() {
            boolean z2 = this.unreadVisible;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int a = b.a(this.unreadMessageId);
            int a2 = (((b.a(this.unreadChannelId) + ((a + (i2 * 31)) * 31)) * 31) + this.unreadCount) * 31;
            boolean z3 = this.isUnreadEstimate;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return a2 + i;
        }

        public final boolean isUnreadEstimate() {
            return this.isUnreadEstimate;
        }

        public String toString() {
            StringBuilder R = a.R("Model(unreadVisible=");
            R.append(this.unreadVisible);
            R.append(", unreadMessageId=");
            R.append(this.unreadMessageId);
            R.append(", unreadChannelId=");
            R.append(this.unreadChannelId);
            R.append(", unreadCount=");
            R.append(this.unreadCount);
            R.append(", isUnreadEstimate=");
            return a.M(R, this.isUnreadEstimate, ")");
        }
    }

    public WidgetChatStatus() {
        super(R.layout.widget_chat_status);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void configureUI(final Model model) {
        LinearLayout linearLayout = getBinding().f2327b;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatStatusUnreadMessages");
        linearLayout.setVisibility(model.getUnreadVisible() ? 0 : 8);
        getBinding().f2327b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetChatStatus$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoreStream.Companion.getMessagesLoader().jumpToMessage(WidgetChatStatus.Model.this.getUnreadChannelId(), WidgetChatStatus.Model.this.getUnreadMessageId());
            }
        });
        TextView textView = getBinding().d;
        m.checkNotNullExpressionValue(textView, "binding.chatStatusUnreadMessagesText");
        textView.setText(getUnreadMessageText(model.isUnreadEstimate(), model.getUnreadCount(), model.getUnreadMessageId()));
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetChatStatus$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoreStream.Companion.getReadStates().markAsRead(Long.valueOf(WidgetChatStatus.Model.this.getUnreadChannelId()));
            }
        });
    }

    private final WidgetChatStatusBinding getBinding() {
        return (WidgetChatStatusBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final CharSequence getUnreadMessageText(boolean z2, int i, long j) {
        String renderUtcDate$default = TimeUtils.renderUtcDate$default(TimeUtils.INSTANCE, TimeUtils.parseSnowflake(Long.valueOf(j)), requireContext(), 0, 4, null);
        if (z2) {
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            return b.a.k.b.c(resources, R.string.new_messages_estimated, new Object[0], new WidgetChatStatus$getUnreadMessageText$1(i, renderUtcDate$default));
        }
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources2, requireContext(), (int) R.plurals.new_messages_count, i, Integer.valueOf(i));
        Resources resources3 = getResources();
        m.checkNotNullExpressionValue(resources3, "resources");
        return b.a.k.b.c(resources3, R.string.new_messages, new Object[0], new WidgetChatStatus$getUnreadMessageText$2(quantityString, renderUtcDate$default));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(), this, null, 2, null), WidgetChatStatus.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatStatus$onViewBoundOrOnResume$1(this));
    }
}
