package com.discord.widgets.status;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetGlobalStatusIndicatorBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGlobalStatusIndicator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGlobalStatusIndicatorBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGlobalStatusIndicatorBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGlobalStatusIndicator$binding$2 extends k implements Function1<View, WidgetGlobalStatusIndicatorBinding> {
    public static final WidgetGlobalStatusIndicator$binding$2 INSTANCE = new WidgetGlobalStatusIndicator$binding$2();

    public WidgetGlobalStatusIndicator$binding$2() {
        super(1, WidgetGlobalStatusIndicatorBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGlobalStatusIndicatorBinding;", 0);
    }

    public final WidgetGlobalStatusIndicatorBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.alert_text;
        TextView textView = (TextView) view.findViewById(R.id.alert_text);
        if (textView != null) {
            LinearLayout linearLayout = (LinearLayout) view;
            i = R.id.indicator_accept;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.indicator_accept);
            if (materialButton != null) {
                i = R.id.indicator_actions;
                LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.indicator_actions);
                if (linearLayout2 != null) {
                    i = R.id.indicator_content;
                    LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.indicator_content);
                    if (linearLayout3 != null) {
                        i = R.id.indicator_decline;
                        MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.indicator_decline);
                        if (materialButton2 != null) {
                            i = R.id.indicator_icon;
                            ImageView imageView = (ImageView) view.findViewById(R.id.indicator_icon);
                            if (imageView != null) {
                                i = R.id.indicator_text;
                                TextView textView2 = (TextView) view.findViewById(R.id.indicator_text);
                                if (textView2 != null) {
                                    return new WidgetGlobalStatusIndicatorBinding(linearLayout, textView, linearLayout, materialButton, linearLayout2, linearLayout3, materialButton2, imageView, textView2);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
