package com.discord.widgets.status;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: WidgetChatStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\b\u001a*\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005 \u0002*\u0014\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00040\u00042\u0018\u0010\u0003\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "kotlin.jvm.PlatformType", "selectedChannelId", "Lrx/Observable;", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Long;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatStatus$Model$Companion$get$isUnreadValidObs$1<T, R> implements b<Long, Observable<? extends Boolean>> {
    public static final WidgetChatStatus$Model$Companion$get$isUnreadValidObs$1 INSTANCE = new WidgetChatStatus$Model$Companion$get$isUnreadValidObs$1();

    public final Observable<? extends Boolean> call(Long l) {
        StoreStream.Companion companion = StoreStream.Companion;
        StoreMessageAck messageAck = companion.getMessageAck();
        m.checkNotNullExpressionValue(l, "selectedChannelId");
        Observable<StoreMessageAck.Ack> observeForChannel = messageAck.observeForChannel(l.longValue());
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        return Observable.m(new k(Boolean.FALSE), Observable.j(observeForChannel.o(200L, timeUnit), companion.getMessagesMostRecent().observeRecentMessageIds(l.longValue()).o(200L, timeUnit).Z(1), WidgetChatStatus$Model$Companion$get$isUnreadValidObs$1$isLastAckOlderThanMostRecent$1.INSTANCE).b0(WidgetChatStatus$Model$Companion$get$isUnreadValidObs$1$isLastAckOlderThanMostRecent$2.INSTANCE));
    }
}
