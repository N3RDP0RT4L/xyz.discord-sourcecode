package com.discord.widgets.status;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.stageinstance.StageInstance;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGlobalStatusIndicatorBinding;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.guild.Guild;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.textprocessing.SpannableUtilsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.voice.VoiceViewUtils;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorState;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorViewModel;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import com.google.android.material.button.MaterialButton;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGlobalStatusIndicator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000i\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b*\u0001'\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b<\u0010\u0018J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J=\u0010\n\u001a&\u0012\f\u0012\n \t*\u0004\u0018\u00010\b0\b \t*\u0012\u0012\f\u0012\n \t*\u0004\u0018\u00010\b0\b\u0018\u00010\u00070\u0007*\b\u0012\u0004\u0012\u00020\b0\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0019H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0019H\u0002¢\u0006\u0004\b\u001c\u0010\u001bJ\u0017\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u001dH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u001dH\u0002¢\u0006\u0004\b \u0010\u001fJ\u0017\u0010!\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b!\u0010\u0006J\u0017\u0010$\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\"H\u0016¢\u0006\u0004\b$\u0010%J\u000f\u0010&\u001a\u00020\u0004H\u0016¢\u0006\u0004\b&\u0010\u0018R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u001d\u00102\u001a\u00020-8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R\u0018\u00103\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0018\u00106\u001a\u0004\u0018\u0001058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u00107R\u001d\u0010\u0003\u001a\u00020\u00028B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010;¨\u0006="}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;", "Lcom/discord/app/AppFragment;", "Lcom/discord/databinding/WidgetGlobalStatusIndicatorBinding;", "binding", "", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetGlobalStatusIndicatorBinding;)V", "Lrx/Observable;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;", "kotlin.jvm.PlatformType", "bindDelay", "(Lrx/Observable;)Lrx/Observable;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;", "state", "configureUIVisibility", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V", "viewState", "configureUI", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;)V", "", "isAirplaneMode", "setupOfflineState", "(Z)V", "setupConnectingState", "()V", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;", "setupContainerClicks", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V", "setupIndicatorStatus", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$StageChannelOngoing;", "setupStageContainerClicks", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$StageChannelOngoing;)V", "setupStageIndicatorStatus", "resetContentVisibility", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "com/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1", "connectingVectorReplayCallback", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "indicatorState", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;", "viewModel", "lastIndicatorState", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;", "Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;", "connectingVector", "Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGlobalStatusIndicatorBinding;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGlobalStatusIndicator extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGlobalStatusIndicator.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGlobalStatusIndicatorBinding;", 0)};
    private AnimatedVectorDrawableCompat connectingVector;
    private WidgetGlobalStatusIndicatorState.State lastIndicatorState;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetGlobalStatusIndicator$binding$2.INSTANCE, new WidgetGlobalStatusIndicator$binding$3(this));
    private final WidgetGlobalStatusIndicatorState indicatorState = WidgetGlobalStatusIndicatorState.Provider.get();
    private final WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1 connectingVectorReplayCallback = new Animatable2Compat.AnimationCallback() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1
        @Override // androidx.vectordrawable.graphics.drawable.Animatable2Compat.AnimationCallback
        public void onAnimationEnd(Drawable drawable) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat;
            m.checkNotNullParameter(drawable, "drawable");
            animatedVectorDrawableCompat = WidgetGlobalStatusIndicator.this.connectingVector;
            if (animatedVectorDrawableCompat != null) {
                animatedVectorDrawableCompat.start();
            }
        }
    };

    /* JADX WARN: Type inference failed for: r0v9, types: [com.discord.widgets.status.WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1] */
    public WidgetGlobalStatusIndicator() {
        super(R.layout.widget_global_status_indicator);
        WidgetGlobalStatusIndicator$viewModel$2 widgetGlobalStatusIndicator$viewModel$2 = WidgetGlobalStatusIndicator$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetGlobalStatusIndicatorViewModel.class), new WidgetGlobalStatusIndicator$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGlobalStatusIndicator$viewModel$2));
    }

    private final Observable<WidgetGlobalStatusIndicatorViewModel.ViewState> bindDelay(final Observable<WidgetGlobalStatusIndicatorViewModel.ViewState> observable) {
        return observable.Y(new b<WidgetGlobalStatusIndicatorViewModel.ViewState, Observable<? extends WidgetGlobalStatusIndicatorViewModel.ViewState>>() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicator$bindDelay$1
            public final Observable<? extends WidgetGlobalStatusIndicatorViewModel.ViewState> call(WidgetGlobalStatusIndicatorViewModel.ViewState viewState) {
                Long l;
                if (viewState instanceof WidgetGlobalStatusIndicatorViewModel.ViewState.Offline) {
                    l = Long.valueOf(((WidgetGlobalStatusIndicatorViewModel.ViewState.Offline) viewState).getDelay());
                } else {
                    l = viewState instanceof WidgetGlobalStatusIndicatorViewModel.ViewState.Connecting ? Long.valueOf(((WidgetGlobalStatusIndicatorViewModel.ViewState.Connecting) viewState).getDelay()) : null;
                }
                if (l == null) {
                    return Observable.this;
                }
                l.longValue();
                return (Observable<T>) new k(viewState).p(l.longValue(), TimeUnit.MILLISECONDS);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetGlobalStatusIndicatorViewModel.ViewState viewState) {
        boolean z2 = true;
        WidgetGlobalStatusIndicatorState.updateState$default(this.indicatorState, !m.areEqual(viewState, WidgetGlobalStatusIndicatorViewModel.ViewState.Inactive.INSTANCE), viewState.isSpeakingInOngoingCall(), false, 4, null);
        AnimatedVectorDrawableCompat animatedVectorDrawableCompat = this.connectingVector;
        if (animatedVectorDrawableCompat != null) {
            animatedVectorDrawableCompat.unregisterAnimationCallback(this.connectingVectorReplayCallback);
        }
        AnimatedVectorDrawableCompat animatedVectorDrawableCompat2 = this.connectingVector;
        if (animatedVectorDrawableCompat2 != null) {
            animatedVectorDrawableCompat2.stop();
        }
        if (viewState instanceof WidgetGlobalStatusIndicatorViewModel.ViewState.Offline) {
            setupOfflineState(((WidgetGlobalStatusIndicatorViewModel.ViewState.Offline) viewState).getAirplaneMode());
        } else if (viewState instanceof WidgetGlobalStatusIndicatorViewModel.ViewState.Connecting) {
            setupConnectingState();
        } else if (viewState instanceof WidgetGlobalStatusIndicatorViewModel.ViewState.CallOngoing) {
            WidgetGlobalStatusIndicatorViewModel.ViewState.CallOngoing callOngoing = (WidgetGlobalStatusIndicatorViewModel.ViewState.CallOngoing) viewState;
            setupContainerClicks(callOngoing);
            setupIndicatorStatus(callOngoing);
        } else if (viewState instanceof WidgetGlobalStatusIndicatorViewModel.ViewState.StageChannelOngoing) {
            WidgetGlobalStatusIndicatorViewModel.ViewState.StageChannelOngoing stageChannelOngoing = (WidgetGlobalStatusIndicatorViewModel.ViewState.StageChannelOngoing) viewState;
            setupStageContainerClicks(stageChannelOngoing);
            setupStageIndicatorStatus(stageChannelOngoing);
        } else {
            resetContentVisibility(getBinding());
        }
        WidgetGlobalStatusIndicatorState.State state = this.lastIndicatorState;
        if (state != null && state.isViewingCall()) {
            LinearLayout linearLayout = getBinding().c;
            m.checkNotNullExpressionValue(linearLayout, "binding.indicator");
            LinearLayout linearLayout2 = getBinding().e;
            m.checkNotNullExpressionValue(linearLayout2, "binding.indicatorActions");
            int i = 0;
            if (linearLayout2.getVisibility() != 0) {
                z2 = false;
            }
            if (!z2) {
                i = 8;
            }
            linearLayout.setVisibility(i);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUIVisibility(WidgetGlobalStatusIndicatorState.State state) {
        this.lastIndicatorState = state;
        LinearLayout linearLayout = getBinding().c;
        m.checkNotNullExpressionValue(linearLayout, "binding.indicator");
        linearLayout.setVisibility(state.isVisible() ? 0 : 8);
    }

    private final WidgetGlobalStatusIndicatorBinding getBinding() {
        return (WidgetGlobalStatusIndicatorBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGlobalStatusIndicatorViewModel getViewModel() {
        return (WidgetGlobalStatusIndicatorViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onViewBindingDestroy(WidgetGlobalStatusIndicatorBinding widgetGlobalStatusIndicatorBinding) {
        resetContentVisibility(widgetGlobalStatusIndicatorBinding);
    }

    private final void resetContentVisibility(WidgetGlobalStatusIndicatorBinding widgetGlobalStatusIndicatorBinding) {
        LinearLayout linearLayout = widgetGlobalStatusIndicatorBinding.f;
        m.checkNotNullExpressionValue(linearLayout, "binding.indicatorContent");
        linearLayout.setVisibility(0);
        TextView textView = widgetGlobalStatusIndicatorBinding.f2383b;
        m.checkNotNullExpressionValue(textView, "binding.alertText");
        textView.setVisibility(8);
    }

    private final void setupConnectingState() {
        resetContentVisibility(getBinding());
        getBinding().c.setBackgroundColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorBackgroundTertiary));
        getBinding().i.setTextColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorHeaderPrimary));
        TextView textView = getBinding().i;
        m.checkNotNullExpressionValue(textView, "binding.indicatorText");
        textView.setText(getString(R.string.connecting));
        AnimatedVectorDrawableCompat animatedVectorDrawableCompat = null;
        if (this.connectingVector == null && Build.VERSION.SDK_INT != 28) {
            this.connectingVector = AnimatedVectorDrawableCompat.create(requireContext(), DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.ic_network_connecting_animated_vector, 0, 2, (Object) null));
        }
        ImageView imageView = getBinding().h;
        imageView.setVisibility(0);
        if (Build.VERSION.SDK_INT != 28) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat2 = this.connectingVector;
            if (animatedVectorDrawableCompat2 != null) {
                animatedVectorDrawableCompat2.registerAnimationCallback(this.connectingVectorReplayCallback);
                animatedVectorDrawableCompat2.start();
                animatedVectorDrawableCompat = animatedVectorDrawableCompat2;
            }
            imageView.setImageDrawable(animatedVectorDrawableCompat);
            return;
        }
        imageView.setImageResource(DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.ic_network_connecting, 0, 2, (Object) null));
    }

    private final void setupContainerClicks(final WidgetGlobalStatusIndicatorViewModel.ViewState.CallOngoing callOngoing) {
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicator$setupContainerClicks$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ModelApplicationStream stream;
                ModelApplicationStream stream2;
                StreamContext streamContext;
                if (ChannelUtils.x(callOngoing.getSelectedVoiceChannel())) {
                    WidgetCallFullscreen.Companion.launch(WidgetGlobalStatusIndicator.this.requireContext(), callOngoing.getSelectedVoiceChannel().h(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
                    return;
                }
                TextInVoiceFeatureFlag instance = TextInVoiceFeatureFlag.Companion.getINSTANCE();
                Channel selectedTextChannel = callOngoing.getSelectedTextChannel();
                String str = null;
                if (instance.isEnabled(selectedTextChannel != null ? Long.valueOf(selectedTextChannel.f()) : null) || ((streamContext = callOngoing.getStreamContext()) != null && streamContext.isCurrentUserParticipating())) {
                    WidgetCallFullscreen.Companion companion = WidgetCallFullscreen.Companion;
                    Context requireContext = WidgetGlobalStatusIndicator.this.requireContext();
                    StreamContext streamContext2 = callOngoing.getStreamContext();
                    long h = (streamContext2 == null || (stream2 = streamContext2.getStream()) == null) ? callOngoing.getSelectedVoiceChannel().h() : stream2.getChannelId();
                    StreamContext streamContext3 = callOngoing.getStreamContext();
                    if (!(streamContext3 == null || (stream = streamContext3.getStream()) == null)) {
                        str = stream.getEncodedStreamKey();
                    }
                    companion.launch(requireContext, h, (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : str, (r14 & 16) != 0 ? null : null);
                    Channel selectedTextChannel2 = callOngoing.getSelectedTextChannel();
                    if (selectedTextChannel2 != null && ChannelUtils.E(selectedTextChannel2)) {
                        WidgetGlobalStatusIndicator.this.requireAppActivity().overridePendingTransition(R.anim.activity_slide_horizontal_close_in, R.anim.activity_slide_horizontal_close_out);
                        return;
                    }
                    return;
                }
                WidgetVoiceBottomSheet.Companion companion2 = WidgetVoiceBottomSheet.Companion;
                FragmentManager parentFragmentManager = WidgetGlobalStatusIndicator.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                companion2.show(parentFragmentManager, callOngoing.getSelectedVoiceChannel().h(), true, WidgetVoiceBottomSheet.FeatureContext.HOME);
            }
        });
    }

    private final void setupIndicatorStatus(WidgetGlobalStatusIndicatorViewModel.ViewState.CallOngoing callOngoing) {
        String name;
        String w;
        LinearLayout linearLayout = getBinding().e;
        m.checkNotNullExpressionValue(linearLayout, "binding.indicatorActions");
        linearLayout.setVisibility(8);
        LinearLayout linearLayout2 = getBinding().c;
        VoiceViewUtils voiceViewUtils = VoiceViewUtils.INSTANCE;
        linearLayout2.setBackgroundColor(voiceViewUtils.getConnectionStatusColor(callOngoing.getConnectionState(), callOngoing.getConnectionQuality(), requireContext()));
        String e = ChannelUtils.e(callOngoing.getSelectedVoiceChannel(), requireContext(), false, 2);
        CharSequence connectedText = voiceViewUtils.getConnectedText(requireContext(), callOngoing.getConnectionState(), callOngoing.getStreamContext(), callOngoing.getHasVideo());
        Guild guild = callOngoing.getGuild();
        if (!(guild == null || (name = guild.getName()) == null || (w = a.w(name, " / ", e)) == null)) {
            e = w;
        }
        String str = connectedText + ": " + e;
        TextView textView = getBinding().i;
        m.checkNotNullExpressionValue(textView, "binding.indicatorText");
        textView.setText(str);
        getBinding().i.setTextColor(ColorCompat.getColor(getContext(), (int) R.color.white));
        ImageView imageView = getBinding().h;
        imageView.setVisibility(0);
        imageView.setImageResource(voiceViewUtils.getCallIndicatorIcon(callOngoing.getHasVideo(), callOngoing.getStreamContext()));
    }

    private final void setupOfflineState(boolean z2) {
        int i;
        resetContentVisibility(getBinding());
        getBinding().c.setBackgroundColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorBackgroundTertiary));
        getBinding().i.setTextColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorHeaderPrimary));
        getBinding().i.setText(z2 ? R.string.network_offline_airplane_mode : R.string.network_offline);
        ImageView imageView = getBinding().h;
        imageView.setVisibility(0);
        if (z2) {
            i = DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.ic_network_airplane_mode, 0, 2, (Object) null);
        } else {
            i = DrawableCompat.getThemedDrawableRes$default(requireContext(), (int) R.attr.ic_network_offline, 0, 2, (Object) null);
        }
        imageView.setImageResource(i);
    }

    private final void setupStageContainerClicks(final WidgetGlobalStatusIndicatorViewModel.ViewState.StageChannelOngoing stageChannelOngoing) {
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicator$setupStageContainerClicks$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCallFullscreen.Companion.launch(WidgetGlobalStatusIndicator.this.requireContext(), stageChannelOngoing.getSelectedVoiceChannel().h(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicator$setupStageContainerClicks$2

            /* compiled from: WidgetGlobalStatusIndicator.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.status.WidgetGlobalStatusIndicator$setupStageContainerClicks$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function0<Unit> {
                public AnonymousClass1() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    b.a.d.m.g(WidgetGlobalStatusIndicator.this.getContext(), R.string.stage_channel_permission_microphone_denied, 0, null, 12);
                }
            }

            /* compiled from: WidgetGlobalStatusIndicator.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.status.WidgetGlobalStatusIndicator$setupStageContainerClicks$2$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends o implements Function0<Unit> {
                public AnonymousClass2() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetGlobalStatusIndicatorViewModel viewModel;
                    viewModel = WidgetGlobalStatusIndicator.this.getViewModel();
                    viewModel.ackStageInvitationToSpeak(true);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGlobalStatusIndicator.this.requestMicrophone(new AnonymousClass1(), new AnonymousClass2());
            }
        });
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetGlobalStatusIndicator$setupStageContainerClicks$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGlobalStatusIndicatorViewModel viewModel;
                viewModel = WidgetGlobalStatusIndicator.this.getViewModel();
                viewModel.ackStageInvitationToSpeak(false);
            }
        });
    }

    private final void setupStageIndicatorStatus(WidgetGlobalStatusIndicatorViewModel.ViewState.StageChannelOngoing stageChannelOngoing) {
        int i;
        int i2;
        resetContentVisibility(getBinding());
        if (stageChannelOngoing.isSpeakingInOngoingCall()) {
            i = ContextCompat.getColor(requireContext(), R.color.white);
        } else {
            i = ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorHeaderPrimary);
        }
        if (stageChannelOngoing.isSpeakingInOngoingCall()) {
            i2 = ContextCompat.getColor(requireContext(), R.color.status_green_600);
        } else {
            i2 = ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorBackgroundTertiary);
        }
        getBinding().c.setBackgroundColor(i2);
        getBinding().i.setTextColor(i);
        getBinding().g.setTextColor(i);
        LinearLayout linearLayout = getBinding().e;
        m.checkNotNullExpressionValue(linearLayout, "binding.indicatorActions");
        int i3 = 8;
        linearLayout.setVisibility(stageChannelOngoing.isInvitedToSpeak() ? 0 : 8);
        ImageView imageView = getBinding().h;
        WidgetGlobalStatusIndicatorState.State state = this.lastIndicatorState;
        imageView.setVisibility(state != null && !state.isViewingCall() ? 0 : 8);
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_channel_stage_16dp);
        CharSequence charSequence = null;
        if (drawable != null) {
            m.checkNotNullExpressionValue(drawable, "drawable");
            ColorCompatKt.setTint(drawable, i, false);
        } else {
            drawable = null;
        }
        imageView.setImageDrawable(drawable);
        if (stageChannelOngoing.isInvitedToSpeak()) {
            getBinding().i.setText(R.string.stage_speak_invite_header);
            getBinding().d.setText(R.string.stage_speak_invite_accept);
            MaterialButton materialButton = getBinding().d;
            m.checkNotNullExpressionValue(materialButton, "binding.indicatorAccept");
            materialButton.setEnabled(!stageChannelOngoing.isAckingInvitation());
            getBinding().g.setText(R.string.stage_speak_invite_decline);
            MaterialButton materialButton2 = getBinding().g;
            m.checkNotNullExpressionValue(materialButton2, "binding.indicatorDecline");
            materialButton2.setEnabled(!stageChannelOngoing.isAckingInvitation());
            TextView textView = getBinding().f2383b;
            m.checkNotNullExpressionValue(textView, "binding.alertText");
            if (stageChannelOngoing.getBlockedUsersOnStage() != 0) {
                i3 = 0;
            }
            textView.setVisibility(i3);
            TextView textView2 = getBinding().f2383b;
            m.checkNotNullExpressionValue(textView2, "binding.alertText");
            Context context = getContext();
            if (context != null) {
                Object[] objArr = new Object[1];
                Context context2 = getContext();
                objArr[0] = context2 != null ? StringResourceUtilsKt.getI18nPluralString(context2, R.plurals.stage_speak_invite_blocked_users_number, stageChannelOngoing.getBlockedUsersOnStage(), Integer.valueOf(stageChannelOngoing.getBlockedUsersOnStage())) : null;
                charSequence = b.a.k.b.b(context, R.string.stage_speak_invite_blocked_users, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
            }
            textView2.setText(charSequence);
            return;
        }
        StageInstance stageInstance = stageChannelOngoing.getStageInstance();
        String f = stageInstance != null ? stageInstance.f() : null;
        Guild guild = stageChannelOngoing.getGuild();
        if (guild != null) {
            charSequence = guild.getName();
        }
        String m = stageChannelOngoing.getSelectedVoiceChannel().m();
        Context requireContext = requireContext();
        TextView textView3 = getBinding().i;
        m.checkNotNullExpressionValue(textView3, "binding.indicatorText");
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (f != null) {
            SpannableUtilsKt.appendWithFont(spannableStringBuilder, requireContext, R.attr.font_primary_semibold, f);
            SpannableUtilsKt.appendWithFont(spannableStringBuilder, requireContext, R.attr.font_primary_semibold, ": ");
        }
        if (charSequence != null) {
            SpannableUtilsKt.appendWithFont(spannableStringBuilder, requireContext, R.attr.font_primary_normal, charSequence);
            SpannableUtilsKt.appendWithFont(spannableStringBuilder, requireContext, R.attr.font_primary_normal, ": ");
        }
        if (m != null) {
            SpannableUtilsKt.appendWithFont(spannableStringBuilder, requireContext, R.attr.font_primary_normal, m);
        }
        textView3.setText(spannableStringBuilder);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        ViewCompat.setOnApplyWindowInsetsListener(view, WidgetGlobalStatusIndicator$onViewBound$1.INSTANCE);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(this.indicatorState.observeState(), this, null, 2, null), WidgetGlobalStatusIndicator.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGlobalStatusIndicator$onViewBoundOrOnResume$1(this));
        Observable<WidgetGlobalStatusIndicatorViewModel.ViewState> bindDelay = bindDelay(getViewModel().observeViewState());
        m.checkNotNullExpressionValue(bindDelay, "viewModel\n        .obser…te()\n        .bindDelay()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(bindDelay, this, null, 2, null), WidgetGlobalStatusIndicator.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGlobalStatusIndicator$onViewBoundOrOnResume$2(this));
    }
}
