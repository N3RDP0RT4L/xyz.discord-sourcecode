package com.discord.widgets.status;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.thread.ThreadMetadata;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadsActiveJoined;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.status.WidgetThreadStatus;
import com.discord.widgets.status.WidgetThreadStatusViewModel;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
import rx.subjects.PublishSubject;
/* compiled from: WidgetThreadStatusViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 &2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004&'()B\u0017\u0012\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00100\u0017¢\u0006\u0004\b$\u0010%J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\r\u0010\u0014\u001a\u00020\u0005¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0016\u001a\u00020\u0005¢\u0006\u0004\b\u0016\u0010\u0015J\u0013\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017¢\u0006\u0004\b\u0019\u0010\u001aR\u0018\u0010\u001b\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00100\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010\u001aR:\u0010\"\u001a&\u0012\f\u0012\n !*\u0004\u0018\u00010\u00180\u0018 !*\u0012\u0012\f\u0012\n !*\u0004\u0018\u00010\u00180\u0018\u0018\u00010 0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#¨\u0006*"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$ViewState;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", "status", "", "updateViewWithStatus", "(Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;)V", "", "isLoading", "updateViewLoading", "(Z)V", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "emitError", "(I)V", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;)V", "onJoinTapped", "()V", "onUnarchiveTapped", "Lrx/Observable;", "Lcom/discord/widgets/status/WidgetThreadStatus$Event;", "observeEvents", "()Lrx/Observable;", "currentStoreState", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;", "storeStateObservable", "Lrx/Observable;", "getStoreStateObservable", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "(Lrx/Observable;)V", "Companion", "Status", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadStatusViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private StoreState currentStoreState;
    private final PublishSubject<WidgetThreadStatus.Event> eventSubject;
    private final Observable<StoreState> storeStateObservable;

    /* compiled from: WidgetThreadStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.status.WidgetThreadStatusViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetThreadStatusViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetThreadStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ3\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Companion;", "", "Lcom/discord/stores/StoreThreadsActiveJoined;", "storeThreadsActiveJoined", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lrx/Observable;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreThreadsActiveJoined;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStoreState(final StoreThreadsActiveJoined storeThreadsActiveJoined, StoreChannelsSelected storeChannelsSelected, final StorePermissions storePermissions) {
            Observable<R> F = storeChannelsSelected.observeSelectedChannel().x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
            m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
            Observable<StoreState> Y = F.Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.status.WidgetThreadStatusViewModel$Companion$observeStoreState$1
                public final Observable<? extends WidgetThreadStatusViewModel.StoreState> call(final Channel channel) {
                    return Observable.j(StoreThreadsActiveJoined.this.observeActiveJoinedThreadsForGuild(channel.f()), storePermissions.observePermissionsForChannel(channel.h()), new Func2<Map<Long, ? extends Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread>>, Long, WidgetThreadStatusViewModel.StoreState>() { // from class: com.discord.widgets.status.WidgetThreadStatusViewModel$Companion$observeStoreState$1.1
                        @Override // rx.functions.Func2
                        public /* bridge */ /* synthetic */ WidgetThreadStatusViewModel.StoreState call(Map<Long, ? extends Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread>> map, Long l) {
                            return call2((Map<Long, ? extends Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>>) map, l);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final WidgetThreadStatusViewModel.StoreState call2(Map<Long, ? extends Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> map, Long l) {
                            ThreadUtils threadUtils = ThreadUtils.INSTANCE;
                            Channel channel2 = Channel.this;
                            m.checkNotNullExpressionValue(channel2, "selectedChannel");
                            boolean canUnarchiveThread = threadUtils.canUnarchiveThread(channel2, l);
                            boolean isThreadModerator = threadUtils.isThreadModerator(Long.valueOf(l != null ? l.longValue() : 0L));
                            m.checkNotNullExpressionValue(map, "activeJoinedThreads");
                            return new WidgetThreadStatusViewModel.StoreState(map, Channel.this, canUnarchiveThread, isThreadModerator);
                        }
                    }).q();
                }
            });
            m.checkNotNullExpressionValue(Y, "storeChannelsSelected.ob…ntilChanged()\n          }");
            return Y;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, StoreThreadsActiveJoined storeThreadsActiveJoined, StoreChannelsSelected storeChannelsSelected, StorePermissions storePermissions, int i, Object obj) {
            if ((i & 1) != 0) {
                storeThreadsActiveJoined = StoreStream.Companion.getThreadsActiveJoined();
            }
            if ((i & 2) != 0) {
                storeChannelsSelected = StoreStream.Companion.getChannelsSelected();
            }
            if ((i & 4) != 0) {
                storePermissions = StoreStream.Companion.getPermissions();
            }
            return companion.observeStoreState(storeThreadsActiveJoined, storeChannelsSelected, storePermissions);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", "", HookHelper.constructorName, "()V", "Archived", "Hide", "Unjoined", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status$Hide;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status$Unjoined;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status$Archived;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Status {

        /* compiled from: WidgetThreadStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0006\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status$Archived;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", "", "component1", "()Z", "component2", "isLocked", "canArchive", "copy", "(ZZ)Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status$Archived;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanArchive", HookHelper.constructorName, "(ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Archived extends Status {
            private final boolean canArchive;
            private final boolean isLocked;

            public Archived(boolean z2, boolean z3) {
                super(null);
                this.isLocked = z2;
                this.canArchive = z3;
            }

            public static /* synthetic */ Archived copy$default(Archived archived, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = archived.isLocked;
                }
                if ((i & 2) != 0) {
                    z3 = archived.canArchive;
                }
                return archived.copy(z2, z3);
            }

            public final boolean component1() {
                return this.isLocked;
            }

            public final boolean component2() {
                return this.canArchive;
            }

            public final Archived copy(boolean z2, boolean z3) {
                return new Archived(z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Archived)) {
                    return false;
                }
                Archived archived = (Archived) obj;
                return this.isLocked == archived.isLocked && this.canArchive == archived.canArchive;
            }

            public final boolean getCanArchive() {
                return this.canArchive;
            }

            public int hashCode() {
                boolean z2 = this.isLocked;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                boolean z3 = this.canArchive;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                return i4 + i;
            }

            public final boolean isLocked() {
                return this.isLocked;
            }

            public String toString() {
                StringBuilder R = a.R("Archived(isLocked=");
                R.append(this.isLocked);
                R.append(", canArchive=");
                return a.M(R, this.canArchive, ")");
            }
        }

        /* compiled from: WidgetThreadStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status$Hide;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Hide extends Status {
            public static final Hide INSTANCE = new Hide();

            private Hide() {
                super(null);
            }
        }

        /* compiled from: WidgetThreadStatusViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status$Unjoined;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Unjoined extends Status {
            public static final Unjoined INSTANCE = new Unjoined();

            private Unjoined() {
                super(null);
            }
        }

        private Status() {
        }

        public /* synthetic */ Status(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001BI\u0012&\u0010\u000f\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00020\u0002\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b\u0012\u0006\u0010\u0012\u001a\u00020\u000b¢\u0006\u0004\b$\u0010%J0\u0010\u0006\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJZ\u0010\u0013\u001a\u00020\u00002(\b\u0002\u0010\u000f\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00020\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000b2\b\b\u0002\u0010\u0012\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\u000b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\u001f\u0010\rR9\u0010\u000f\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u0007R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\nR\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u0012\u0010\r¨\u0006&"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;", "", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "component1", "()Ljava/util/Map;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "component3", "()Z", "component4", "activeJoinedThreads", "selectedChannel", "canArchive", "isModerator", "copy", "(Ljava/util/Map;Lcom/discord/api/channel/Channel;ZZ)Lcom/discord/widgets/status/WidgetThreadStatusViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanArchive", "Ljava/util/Map;", "getActiveJoinedThreads", "Lcom/discord/api/channel/Channel;", "getSelectedChannel", HookHelper.constructorName, "(Ljava/util/Map;Lcom/discord/api/channel/Channel;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> activeJoinedThreads;
        private final boolean canArchive;
        private final boolean isModerator;
        private final Channel selectedChannel;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(Map<Long, ? extends Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> map, Channel channel, boolean z2, boolean z3) {
            m.checkNotNullParameter(map, "activeJoinedThreads");
            this.activeJoinedThreads = map;
            this.selectedChannel = channel;
            this.canArchive = z2;
            this.isModerator = z3;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, Map map, Channel channel, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                map = storeState.activeJoinedThreads;
            }
            if ((i & 2) != 0) {
                channel = storeState.selectedChannel;
            }
            if ((i & 4) != 0) {
                z2 = storeState.canArchive;
            }
            if ((i & 8) != 0) {
                z3 = storeState.isModerator;
            }
            return storeState.copy(map, channel, z2, z3);
        }

        public final Map<Long, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> component1() {
            return this.activeJoinedThreads;
        }

        public final Channel component2() {
            return this.selectedChannel;
        }

        public final boolean component3() {
            return this.canArchive;
        }

        public final boolean component4() {
            return this.isModerator;
        }

        public final StoreState copy(Map<Long, ? extends Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> map, Channel channel, boolean z2, boolean z3) {
            m.checkNotNullParameter(map, "activeJoinedThreads");
            return new StoreState(map, channel, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.activeJoinedThreads, storeState.activeJoinedThreads) && m.areEqual(this.selectedChannel, storeState.selectedChannel) && this.canArchive == storeState.canArchive && this.isModerator == storeState.isModerator;
        }

        public final Map<Long, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> getActiveJoinedThreads() {
            return this.activeJoinedThreads;
        }

        public final boolean getCanArchive() {
            return this.canArchive;
        }

        public final Channel getSelectedChannel() {
            return this.selectedChannel;
        }

        public int hashCode() {
            Map<Long, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> map = this.activeJoinedThreads;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            Channel channel = this.selectedChannel;
            if (channel != null) {
                i = channel.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.canArchive;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.isModerator;
            if (!z3) {
                i3 = z3 ? 1 : 0;
            }
            return i6 + i3;
        }

        public final boolean isModerator() {
            return this.isModerator;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(activeJoinedThreads=");
            R.append(this.activeJoinedThreads);
            R.append(", selectedChannel=");
            R.append(this.selectedChannel);
            R.append(", canArchive=");
            R.append(this.canArchive);
            R.append(", isModerator=");
            return a.M(R, this.isModerator, ")");
        }
    }

    /* compiled from: WidgetThreadStatusViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\b\u0002\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\t\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatusViewModel$ViewState;", "", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", "component1", "()Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", "", "component2", "()Z", "threadStatus", "isLoading", "copy", "(Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;Z)Lcom/discord/widgets/status/WidgetThreadStatusViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;", "getThreadStatus", HookHelper.constructorName, "(Lcom/discord/widgets/status/WidgetThreadStatusViewModel$Status;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean isLoading;
        private final Status threadStatus;

        public ViewState(Status status, boolean z2) {
            m.checkNotNullParameter(status, "threadStatus");
            this.threadStatus = status;
            this.isLoading = z2;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, Status status, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                status = viewState.threadStatus;
            }
            if ((i & 2) != 0) {
                z2 = viewState.isLoading;
            }
            return viewState.copy(status, z2);
        }

        public final Status component1() {
            return this.threadStatus;
        }

        public final boolean component2() {
            return this.isLoading;
        }

        public final ViewState copy(Status status, boolean z2) {
            m.checkNotNullParameter(status, "threadStatus");
            return new ViewState(status, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.threadStatus, viewState.threadStatus) && this.isLoading == viewState.isLoading;
        }

        public final Status getThreadStatus() {
            return this.threadStatus;
        }

        public int hashCode() {
            Status status = this.threadStatus;
            int hashCode = (status != null ? status.hashCode() : 0) * 31;
            boolean z2 = this.isLoading;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public final boolean isLoading() {
            return this.isLoading;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(threadStatus=");
            R.append(this.threadStatus);
            R.append(", isLoading=");
            return a.M(R, this.isLoading, ")");
        }

        public /* synthetic */ ViewState(Status status, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(status, (i & 2) != 0 ? false : z2);
        }
    }

    public WidgetThreadStatusViewModel() {
        this(null, 1, null);
    }

    public /* synthetic */ WidgetThreadStatusViewModel(Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? Companion.observeStoreState$default(Companion, null, null, null, 7, null) : observable);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitError(int i) {
        PublishSubject<WidgetThreadStatus.Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new WidgetThreadStatus.Event.Error(i));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Status status;
        this.currentStoreState = storeState;
        Channel selectedChannel = storeState.getSelectedChannel();
        Map<Long, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> activeJoinedThreads = storeState.getActiveJoinedThreads();
        Channel selectedChannel2 = storeState.getSelectedChannel();
        StoreThreadsActiveJoined.ActiveJoinedThread activeJoinedThread = null;
        Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> map = activeJoinedThreads.get(selectedChannel2 != null ? Long.valueOf(selectedChannel2.r()) : null);
        if (map != null) {
            Channel selectedChannel3 = storeState.getSelectedChannel();
            if (selectedChannel3 != null) {
                activeJoinedThread = Long.valueOf(selectedChannel3.h());
            }
            activeJoinedThread = map.get(activeJoinedThread);
        }
        boolean z2 = false;
        boolean z3 = activeJoinedThread != null;
        if (selectedChannel == null || !ChannelUtils.C(selectedChannel)) {
            status = Status.Hide.INSTANCE;
        } else {
            ThreadMetadata y2 = selectedChannel.y();
            if (y2 != null && y2.b()) {
                ThreadMetadata y3 = selectedChannel.y();
                if (y3 != null && y3.d()) {
                    z2 = true;
                }
                status = new Status.Archived(z2, storeState.getCanArchive());
            } else if (z3) {
                status = Status.Hide.INSTANCE;
            } else {
                status = Status.Unjoined.INSTANCE;
            }
        }
        updateViewWithStatus(status);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateViewLoading(boolean z2) {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, null, z2, 1, null));
        }
    }

    private final void updateViewWithStatus(Status status) {
        updateViewState(new ViewState(status, false, 2, null));
    }

    public final Observable<StoreState> getStoreStateObservable() {
        return this.storeStateObservable;
    }

    public final Observable<WidgetThreadStatus.Event> observeEvents() {
        PublishSubject<WidgetThreadStatus.Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void onJoinTapped() {
        StoreState storeState = this.currentStoreState;
        if (storeState != null) {
            if (storeState.getSelectedChannel() != null) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().joinThread(storeState.getSelectedChannel().h(), "Banner", new RestAPIParams.EmptyBody()), false, 1, null), this, null, 2, null), WidgetThreadStatusViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetThreadStatusViewModel$onJoinTapped$$inlined$let$lambda$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : new WidgetThreadStatusViewModel$onJoinTapped$$inlined$let$lambda$2(this), WidgetThreadStatusViewModel$onJoinTapped$1$2.INSTANCE);
            } else {
                return;
            }
        }
        updateViewLoading(true);
    }

    public final void onUnarchiveTapped() {
        StoreState storeState = this.currentStoreState;
        if (storeState != null) {
            if (storeState.getSelectedChannel() != null) {
                RestAPI api = RestAPI.Companion.getApi();
                long h = storeState.getSelectedChannel().h();
                Boolean bool = Boolean.FALSE;
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.editThread(h, new RestAPIParams.ThreadSettings(bool, storeState.isModerator() ? bool : null, null, 4, null)), false, 1, null), this, null, 2, null), WidgetThreadStatusViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetThreadStatusViewModel$onUnarchiveTapped$$inlined$let$lambda$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : new WidgetThreadStatusViewModel$onUnarchiveTapped$$inlined$let$lambda$2(this), WidgetThreadStatusViewModel$onUnarchiveTapped$1$2.INSTANCE);
            } else {
                return;
            }
        }
        updateViewLoading(true);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetThreadStatusViewModel(Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.storeStateObservable = observable;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetThreadStatusViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
