package com.discord.widgets.status;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetChatStatusBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetChatStatusBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetChatStatusBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetChatStatus$binding$2 extends k implements Function1<View, WidgetChatStatusBinding> {
    public static final WidgetChatStatus$binding$2 INSTANCE = new WidgetChatStatus$binding$2();

    public WidgetChatStatus$binding$2() {
        super(1, WidgetChatStatusBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetChatStatusBinding;", 0);
    }

    public final WidgetChatStatusBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        LinearLayout linearLayout = (LinearLayout) view;
        int i = R.id.chat_status_unread_messages_mark;
        ImageView imageView = (ImageView) view.findViewById(R.id.chat_status_unread_messages_mark);
        if (imageView != null) {
            i = R.id.chat_status_unread_messages_text;
            TextView textView = (TextView) view.findViewById(R.id.chat_status_unread_messages_text);
            if (textView != null) {
                return new WidgetChatStatusBinding((LinearLayout) view, linearLayout, imageView, textView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
