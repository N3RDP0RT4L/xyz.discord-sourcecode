package com.discord.widgets.status;

import andhook.lib.HookHelper;
import android.content.res.ColorStateList;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.m;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetThreadStatusBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.discord.widgets.status.WidgetThreadStatusViewModel;
import d0.z.d.a0;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetThreadStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001aB\u0007¢\u0006\u0004\b\u0019\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatus;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel$ViewState;", "viewState", "", "updateView", "(Lcom/discord/widgets/status/WidgetThreadStatusViewModel$ViewState;)V", "Lcom/discord/widgets/status/WidgetThreadStatus$Event;", "event", "handleEvent", "(Lcom/discord/widgets/status/WidgetThreadStatus$Event;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetThreadStatusBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetThreadStatusBinding;", "binding", "Lcom/discord/widgets/status/WidgetThreadStatusViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/status/WidgetThreadStatusViewModel;", "viewModel", HookHelper.constructorName, "Event", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadStatus extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetThreadStatus.class, "binding", "getBinding()Lcom/discord/databinding/WidgetThreadStatusBinding;", 0)};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetThreadStatus$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetThreadStatus.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatus$Event;", "", HookHelper.constructorName, "()V", "Error", "Lcom/discord/widgets/status/WidgetThreadStatus$Event$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetThreadStatus.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/status/WidgetThreadStatus$Event$Error;", "Lcom/discord/widgets/status/WidgetThreadStatus$Event;", "", "component1", "()I", ModelAuditLogEntry.CHANGE_KEY_CODE, "copy", "(I)Lcom/discord/widgets/status/WidgetThreadStatus$Event$Error;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getCode", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends Event {
            private final int code;

            public Error(int i) {
                super(null);
                this.code = i;
            }

            public static /* synthetic */ Error copy$default(Error error, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = error.code;
                }
                return error.copy(i);
            }

            public final int component1() {
                return this.code;
            }

            public final Error copy(int i) {
                return new Error(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Error) && this.code == ((Error) obj).code;
                }
                return true;
            }

            public final int getCode() {
                return this.code;
            }

            public int hashCode() {
                return this.code;
            }

            public String toString() {
                return a.A(a.R("Error(code="), this.code, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetThreadStatus() {
        super(R.layout.widget_thread_status);
        WidgetThreadStatus$viewModel$2 widgetThreadStatus$viewModel$2 = WidgetThreadStatus$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetThreadStatusViewModel.class), new WidgetThreadStatus$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetThreadStatus$viewModel$2));
    }

    private final WidgetThreadStatusBinding getBinding() {
        return (WidgetThreadStatusBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetThreadStatusViewModel getViewModel() {
        return (WidgetThreadStatusViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(Event event) {
        if (event instanceof Event.Error) {
            Integer abortCodeMessageResId = RestAPIAbortMessages.getAbortCodeMessageResId(((Event.Error) event).getCode());
            m.i(this, abortCodeMessageResId != null ? abortCodeMessageResId.intValue() : R.string.network_error_bad_request, 0, 4);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void updateView(WidgetThreadStatusViewModel.ViewState viewState) {
        String str;
        getBinding().c.setIsLoading(viewState.isLoading());
        WidgetThreadStatusViewModel.Status threadStatus = viewState.getThreadStatus();
        int i = 8;
        if (threadStatus instanceof WidgetThreadStatusViewModel.Status.Hide) {
            ConstraintLayout constraintLayout = getBinding().f2646b;
            d0.z.d.m.checkNotNullExpressionValue(constraintLayout, "binding.threadStatus");
            constraintLayout.setVisibility(8);
        } else if (threadStatus instanceof WidgetThreadStatusViewModel.Status.Archived) {
            ConstraintLayout constraintLayout2 = getBinding().f2646b;
            d0.z.d.m.checkNotNullExpressionValue(constraintLayout2, "binding.threadStatus");
            constraintLayout2.setVisibility(0);
            LoadingButton loadingButton = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(loadingButton, "binding.threadStatusButton");
            WidgetThreadStatusViewModel.Status.Archived archived = (WidgetThreadStatusViewModel.Status.Archived) threadStatus;
            if (archived.getCanArchive()) {
                i = 0;
            }
            loadingButton.setVisibility(i);
            LinkifiedTextView linkifiedTextView = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(linkifiedTextView, "binding.threadStatusText");
            if (archived.isLocked()) {
                str = getResources().getString(R.string.thread_header_notice_locked);
            } else {
                str = getResources().getString(R.string.thread_header_notice_archived);
            }
            linkifiedTextView.setText(str);
            getBinding().c.setText(getResources().getString(R.string.unarchive));
            LoadingButton loadingButton2 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(loadingButton2, "binding.threadStatusButton");
            loadingButton2.setBackgroundTintList(ColorStateList.valueOf(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundAccent)));
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetThreadStatus$updateView$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetThreadStatusViewModel viewModel;
                    viewModel = WidgetThreadStatus.this.getViewModel();
                    viewModel.onUnarchiveTapped();
                }
            });
        } else if (threadStatus instanceof WidgetThreadStatusViewModel.Status.Unjoined) {
            ConstraintLayout constraintLayout3 = getBinding().f2646b;
            d0.z.d.m.checkNotNullExpressionValue(constraintLayout3, "binding.threadStatus");
            constraintLayout3.setVisibility(0);
            LoadingButton loadingButton3 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(loadingButton3, "binding.threadStatusButton");
            loadingButton3.setVisibility(0);
            LinkifiedTextView linkifiedTextView2 = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(linkifiedTextView2, "binding.threadStatusText");
            linkifiedTextView2.setText(getResources().getString(R.string.thread_header_notice_join));
            getBinding().c.setText(getResources().getString(R.string.join));
            LoadingButton loadingButton4 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(loadingButton4, "binding.threadStatusButton");
            loadingButton4.setBackgroundTintList(ColorStateList.valueOf(ColorCompat.getThemedColor(getContext(), (int) R.attr.color_brand_500)));
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.status.WidgetThreadStatus$updateView$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetThreadStatusViewModel viewModel;
                    viewModel = WidgetThreadStatus.this.getViewModel();
                    viewModel.onJoinTapped();
                }
            });
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetThreadStatus.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadStatus$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetThreadStatus.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadStatus$onViewBoundOrOnResume$2(this));
    }
}
