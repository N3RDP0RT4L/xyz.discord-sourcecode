package com.discord.widgets.search.suggestions;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetSearchSuggestionsBinding;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSearchSuggestions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSearchSuggestionsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSearchSuggestionsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSearchSuggestions$binding$2 extends k implements Function1<View, WidgetSearchSuggestionsBinding> {
    public static final WidgetSearchSuggestions$binding$2 INSTANCE = new WidgetSearchSuggestions$binding$2();

    public WidgetSearchSuggestions$binding$2() {
        super(1, WidgetSearchSuggestionsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSearchSuggestionsBinding;", 0);
    }

    public final WidgetSearchSuggestionsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        Objects.requireNonNull(view, "rootView");
        RecyclerView recyclerView = (RecyclerView) view;
        return new WidgetSearchSuggestionsBinding(recyclerView, recyclerView);
    }
}
