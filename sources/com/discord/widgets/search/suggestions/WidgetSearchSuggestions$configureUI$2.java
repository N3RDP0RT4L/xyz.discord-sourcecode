package com.discord.widgets.search.suggestions;

import com.discord.stores.StoreStream;
import com.discord.utilities.search.suggestion.entries.UserSuggestion;
import com.discord.widgets.search.suggestions.WidgetSearchSuggestions;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSearchSuggestions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;", "userSuggestion", "", "invoke", "(Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSearchSuggestions$configureUI$2 extends o implements Function1<UserSuggestion, Unit> {
    public final /* synthetic */ WidgetSearchSuggestions.Model $model;
    public final /* synthetic */ WidgetSearchSuggestions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSearchSuggestions$configureUI$2(WidgetSearchSuggestions widgetSearchSuggestions, WidgetSearchSuggestions.Model model) {
        super(1);
        this.this$0 = widgetSearchSuggestions;
        this.$model = model;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(UserSuggestion userSuggestion) {
        invoke2(userSuggestion);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(UserSuggestion userSuggestion) {
        m.checkNotNullParameter(userSuggestion, "userSuggestion");
        int ordinal = userSuggestion.getTargetType().ordinal();
        if (ordinal == 0) {
            StoreStream.Companion companion = StoreStream.Companion;
            companion.getSearch().getStoreSearchInput().onFromUserClicked(userSuggestion, WidgetSearchSuggestions.access$getSearchStringProvider$p(this.this$0).getFromFilterString(), this.$model.getQuery());
            companion.getSearch().getStoreSearchInput().onMentionsUserClicked(userSuggestion, WidgetSearchSuggestions.access$getSearchStringProvider$p(this.this$0).getMentionsFilterString(), this.$model.getQuery());
        } else if (ordinal == 1) {
            StoreStream.Companion.getSearch().getStoreSearchInput().onMentionsUserClicked(userSuggestion, WidgetSearchSuggestions.access$getSearchStringProvider$p(this.this$0).getMentionsFilterString(), this.$model.getQuery());
        }
    }
}
