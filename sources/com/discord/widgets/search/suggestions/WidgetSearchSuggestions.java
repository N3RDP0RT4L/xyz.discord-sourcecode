package com.discord.widgets.search.suggestions;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSearchSuggestionsBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.search.strings.ContextSearchStringProvider;
import com.discord.utilities.search.strings.SearchStringProvider;
import com.discord.utilities.search.suggestion.SearchSuggestionEngine;
import com.discord.utilities.search.suggestion.entries.ChannelSuggestion;
import com.discord.utilities.search.suggestion.entries.FilterSuggestion;
import com.discord.utilities.search.suggestion.entries.HasSuggestion;
import com.discord.utilities.search.suggestion.entries.RecentQuerySuggestion;
import com.discord.utilities.search.suggestion.entries.SearchSuggestion;
import com.discord.utilities.search.suggestion.entries.UserSuggestion;
import com.discord.utilities.search.validation.SearchData;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.search.suggestions.WidgetSearchSuggestions;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func3;
import xyz.discord.R;
/* compiled from: WidgetSearchSuggestions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u001aB\u0007¢\u0006\u0004\b\u0019\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0015\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;", "adapter", "Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;", "Lcom/discord/databinding/WidgetSearchSuggestionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSearchSuggestionsBinding;", "binding", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "Lcom/discord/utilities/search/strings/SearchStringProvider;", HookHelper.constructorName, ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSearchSuggestions extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSearchSuggestions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSearchSuggestionsBinding;", 0)};
    private WidgetSearchSuggestionsAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSearchSuggestions$binding$2.INSTANCE, null, 2, null);
    private SearchStringProvider searchStringProvider;

    /* compiled from: WidgetSearchSuggestions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0082\b\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB#\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0004\b\u001d\u0010\u001eJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\u0005J0\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001b\u0010\u0005R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001c\u0010\u0005¨\u0006 "}, d2 = {"Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;", "", "", "Lcom/discord/utilities/search/query/node/QueryNode;", "component1", "()Ljava/util/List;", "", "Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;", "component2", "query", "suggestionEntries", "copy", "(Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "suggestionItems", "Ljava/util/List;", "getSuggestionItems", "getQuery", "getSuggestionEntries", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final List<QueryNode> query;
        private final List<SearchSuggestion> suggestionEntries;
        private final List<MGRecyclerDataPayload> suggestionItems = new ArrayList();

        /* compiled from: WidgetSearchSuggestions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001b\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;", "", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "Lrx/Observable;", "Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;", "get", "(Lcom/discord/utilities/search/strings/SearchStringProvider;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final SearchStringProvider searchStringProvider) {
                m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable i = Observable.i(companion.getSearch().getStoreSearchData().get(), companion.getSearch().getStoreSearchInput().getCurrentParsedInput(), companion.getSearch().getHistory(), new Func3<SearchData, List<? extends QueryNode>, Collection<? extends List<? extends QueryNode>>, Model>() { // from class: com.discord.widgets.search.suggestions.WidgetSearchSuggestions$Model$Companion$get$1
                    public final WidgetSearchSuggestions.Model call(SearchData searchData, List<? extends QueryNode> list, Collection<? extends List<? extends QueryNode>> collection) {
                        m.checkNotNullParameter(searchData, "searchData");
                        m.checkNotNullParameter(list, "queryNodes");
                        m.checkNotNullParameter(collection, "history");
                        List<QueryNode> mutableList = u.toMutableList((Collection) list);
                        QueryNode.Preprocessor.preprocess(mutableList, searchData);
                        return new WidgetSearchSuggestions.Model(mutableList, SearchSuggestionEngine.getSuggestions(mutableList, searchData, SearchStringProvider.this, collection));
                    }
                });
                m.checkNotNullExpressionValue(i, "Observable.combineLatest…gestionEntries)\n        }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(i).q();
                m.checkNotNullExpressionValue(q, "Observable.combineLatest…().distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                SearchSuggestion.Category.values();
                int[] iArr = new int[7];
                $EnumSwitchMapping$0 = iArr;
                iArr[SearchSuggestion.Category.FILTER.ordinal()] = 1;
                iArr[SearchSuggestion.Category.FROM_USER.ordinal()] = 2;
                iArr[SearchSuggestion.Category.MENTIONS_USER.ordinal()] = 3;
                iArr[SearchSuggestion.Category.IN_CHANNEL.ordinal()] = 4;
                iArr[SearchSuggestion.Category.HAS.ordinal()] = 5;
                iArr[SearchSuggestion.Category.RECENT_QUERY.ordinal()] = 6;
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(List<QueryNode> list, List<? extends SearchSuggestion> list2) {
            m.checkNotNullParameter(list, "query");
            m.checkNotNullParameter(list2, "suggestionEntries");
            this.query = list;
            this.suggestionEntries = list2;
            SearchSuggestion.Category category = null;
            for (SearchSuggestion searchSuggestion : list2) {
                SearchSuggestion.Category category2 = searchSuggestion.getCategory();
                if (category == null || category2 != category) {
                    this.suggestionItems.add(WidgetSearchSuggestionsAdapter.Companion.getHeaderItem(category2));
                    category = category2;
                }
                int ordinal = category2.ordinal();
                if (ordinal == 0) {
                    this.suggestionItems.add(WidgetSearchSuggestionsAdapter.Companion.getFilterItem((FilterSuggestion) searchSuggestion));
                } else if (ordinal == 1 || ordinal == 2) {
                    this.suggestionItems.add(WidgetSearchSuggestionsAdapter.Companion.getUserItem((UserSuggestion) searchSuggestion));
                } else if (ordinal == 3) {
                    this.suggestionItems.add(WidgetSearchSuggestionsAdapter.Companion.getHasItem((HasSuggestion) searchSuggestion));
                } else if (ordinal == 5) {
                    this.suggestionItems.add(WidgetSearchSuggestionsAdapter.Companion.getInChannelItem((ChannelSuggestion) searchSuggestion));
                } else if (ordinal == 6) {
                    this.suggestionItems.add(WidgetSearchSuggestionsAdapter.Companion.getRecentQueryItem((RecentQuerySuggestion) searchSuggestion));
                }
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Model copy$default(Model model, List list, List list2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = model.query;
            }
            if ((i & 2) != 0) {
                list2 = model.suggestionEntries;
            }
            return model.copy(list, list2);
        }

        public final List<QueryNode> component1() {
            return this.query;
        }

        public final List<SearchSuggestion> component2() {
            return this.suggestionEntries;
        }

        public final Model copy(List<QueryNode> list, List<? extends SearchSuggestion> list2) {
            m.checkNotNullParameter(list, "query");
            m.checkNotNullParameter(list2, "suggestionEntries");
            return new Model(list, list2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.query, model.query) && m.areEqual(this.suggestionEntries, model.suggestionEntries);
        }

        public final List<QueryNode> getQuery() {
            return this.query;
        }

        public final List<SearchSuggestion> getSuggestionEntries() {
            return this.suggestionEntries;
        }

        public final List<MGRecyclerDataPayload> getSuggestionItems() {
            return this.suggestionItems;
        }

        public int hashCode() {
            List<QueryNode> list = this.query;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            List<SearchSuggestion> list2 = this.suggestionEntries;
            if (list2 != null) {
                i = list2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(query=");
            R.append(this.query);
            R.append(", suggestionEntries=");
            return a.K(R, this.suggestionEntries, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            UserSuggestion.TargetType.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[UserSuggestion.TargetType.FROM.ordinal()] = 1;
            iArr[UserSuggestion.TargetType.MENTIONS.ordinal()] = 2;
        }
    }

    public WidgetSearchSuggestions() {
        super(R.layout.widget_search_suggestions);
    }

    public static final /* synthetic */ SearchStringProvider access$getSearchStringProvider$p(WidgetSearchSuggestions widgetSearchSuggestions) {
        SearchStringProvider searchStringProvider = widgetSearchSuggestions.searchStringProvider;
        if (searchStringProvider == null) {
            m.throwUninitializedPropertyAccessException("searchStringProvider");
        }
        return searchStringProvider;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        WidgetSearchSuggestionsAdapter widgetSearchSuggestionsAdapter = this.adapter;
        if (widgetSearchSuggestionsAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetSearchSuggestionsAdapter.setData(model.getSuggestionItems());
        WidgetSearchSuggestionsAdapter widgetSearchSuggestionsAdapter2 = this.adapter;
        if (widgetSearchSuggestionsAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetSearchSuggestionsAdapter2.setOnFilterClicked(new WidgetSearchSuggestions$configureUI$1(this, model));
        WidgetSearchSuggestionsAdapter widgetSearchSuggestionsAdapter3 = this.adapter;
        if (widgetSearchSuggestionsAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetSearchSuggestionsAdapter3.setOnUserClicked(new WidgetSearchSuggestions$configureUI$2(this, model));
        WidgetSearchSuggestionsAdapter widgetSearchSuggestionsAdapter4 = this.adapter;
        if (widgetSearchSuggestionsAdapter4 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetSearchSuggestionsAdapter4.setOnChannelClicked(new WidgetSearchSuggestions$configureUI$3(this, model));
        WidgetSearchSuggestionsAdapter widgetSearchSuggestionsAdapter5 = this.adapter;
        if (widgetSearchSuggestionsAdapter5 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetSearchSuggestionsAdapter5.setOnHasClicked(new WidgetSearchSuggestions$configureUI$4(this, model));
        WidgetSearchSuggestionsAdapter widgetSearchSuggestionsAdapter6 = this.adapter;
        if (widgetSearchSuggestionsAdapter6 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetSearchSuggestionsAdapter6.setOnRecentQueryClicked(WidgetSearchSuggestions$configureUI$5.INSTANCE);
        WidgetSearchSuggestionsAdapter widgetSearchSuggestionsAdapter7 = this.adapter;
        if (widgetSearchSuggestionsAdapter7 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetSearchSuggestionsAdapter7.setOnClearHistoryClicked(WidgetSearchSuggestions$configureUI$6.INSTANCE);
    }

    private final WidgetSearchSuggestionsBinding getBinding() {
        return (WidgetSearchSuggestionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2502b;
        m.checkNotNullExpressionValue(recyclerView, "binding.searchSuggestionsRecycler");
        this.adapter = (WidgetSearchSuggestionsAdapter) companion.configure(new WidgetSearchSuggestionsAdapter(recyclerView));
        if (AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
            RecyclerView recyclerView2 = getBinding().f2502b;
            m.checkNotNullExpressionValue(recyclerView2, "binding.searchSuggestionsRecycler");
            recyclerView2.setItemAnimator(null);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ContextSearchStringProvider contextSearchStringProvider = new ContextSearchStringProvider(requireContext());
        this.searchStringProvider = contextSearchStringProvider;
        Model.Companion companion = Model.Companion;
        if (contextSearchStringProvider == null) {
            m.throwUninitializedPropertyAccessException("searchStringProvider");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.get(contextSearchStringProvider), this, null, 2, null), WidgetSearchSuggestions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSearchSuggestions$onViewBoundOrOnResume$1(this));
    }
}
