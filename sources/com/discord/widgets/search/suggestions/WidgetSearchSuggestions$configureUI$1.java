package com.discord.widgets.search.suggestions;

import com.discord.stores.StoreStream;
import com.discord.utilities.search.query.FilterType;
import com.discord.widgets.search.suggestions.WidgetSearchSuggestions;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSearchSuggestions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/search/query/FilterType;", "filterType", "", "invoke", "(Lcom/discord/utilities/search/query/FilterType;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSearchSuggestions$configureUI$1 extends o implements Function1<FilterType, Unit> {
    public final /* synthetic */ WidgetSearchSuggestions.Model $model;
    public final /* synthetic */ WidgetSearchSuggestions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSearchSuggestions$configureUI$1(WidgetSearchSuggestions widgetSearchSuggestions, WidgetSearchSuggestions.Model model) {
        super(1);
        this.this$0 = widgetSearchSuggestions;
        this.$model = model;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(FilterType filterType) {
        invoke2(filterType);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(FilterType filterType) {
        m.checkNotNullParameter(filterType, "filterType");
        StoreStream.Companion.getSearch().getStoreSearchInput().onFilterClicked(filterType, WidgetSearchSuggestions.access$getSearchStringProvider$p(this.this$0), this.$model.getQuery());
    }
}
