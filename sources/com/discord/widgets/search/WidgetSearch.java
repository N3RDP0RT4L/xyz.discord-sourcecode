package com.discord.widgets.search;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSearchBinding;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreSearch;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.search.strings.ContextSearchStringProvider;
import com.discord.utilities.textprocessing.AstRenderer;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\n\u0018\u0000 (2\u00020\u0001:\u0002()B\u0007¢\u0006\u0004\b'\u0010\bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\bJ\u000f\u0010\u0012\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\bR\"\u0010\u0014\u001a\u00020\u00138\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001d\u0010\u001f\u001a\u00020\u001a8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\"\u0010!\u001a\u00020 8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&¨\u0006*"}, d2 = {"Lcom/discord/widgets/search/WidgetSearch;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/search/WidgetSearch$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/search/WidgetSearch$Model;)V", "configureSearchInput", "()V", "Landroid/content/Context;", "context", "sendQuery", "(Landroid/content/Context;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onDestroy", "", "targetType", "I", "getTargetType", "()I", "setTargetType", "(I)V", "Lcom/discord/databinding/WidgetSearchBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSearchBinding;", "binding", "", "targetId", "J", "getTargetId", "()J", "setTargetId", "(J)V", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSearch extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSearch.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSearchBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final String INTENT_EXTRA_TARGET_ID = "INTENT_EXTRA_TARGET_ID";
    public static final String INTENT_EXTRA_TARGET_TYPE = "INTENT_EXTRA_SEARCH_TYPE";
    private static final int TARGET_TYPE_CHANNEL = 1;
    private static final int TARGET_TYPE_GUILD = 0;
    private long targetId;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSearch$binding$2.INSTANCE, null, 2, null);
    private int targetType = -1;

    /* compiled from: WidgetSearch.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018J'\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\f\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u000f\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u000f\u0010\rR\u0016\u0010\u0011\u001a\u00020\u00108\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\u00108\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/search/WidgetSearch$Companion;", "", "", "targetId", "", "targetType", "Landroid/content/Context;", "context", "", "launch", "(JILandroid/content/Context;)V", "guildId", "launchForGuild", "(JLandroid/content/Context;)V", "channelId", "launchForChannel", "", WidgetSearch.INTENT_EXTRA_TARGET_ID, "Ljava/lang/String;", "INTENT_EXTRA_TARGET_TYPE", "TARGET_TYPE_CHANNEL", "I", "TARGET_TYPE_GUILD", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final void launch(long j, int i, Context context) {
            Intent putExtra = new Intent().putExtra(WidgetSearch.INTENT_EXTRA_TARGET_ID, j).putExtra(WidgetSearch.INTENT_EXTRA_TARGET_TYPE, i);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…_TARGET_TYPE, targetType)");
            j.d(context, WidgetSearch.class, putExtra);
        }

        public final void launchForChannel(long j, Context context) {
            m.checkNotNullParameter(context, "context");
            launch(j, 1, context);
        }

        public final void launchForGuild(long j, Context context) {
            m.checkNotNullParameter(context, "context");
            launch(j, 0, context);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSearch.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0082\b\u0018\u0000 &2\u00020\u0001:\u0001&B-\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b$\u0010%J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ>\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001b\u001a\u00020\u000b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b\u0011\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\nR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0007¨\u0006'"}, d2 = {"Lcom/discord/widgets/search/WidgetSearch$Model;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/stores/StoreSearch$DisplayState;", "component3", "()Lcom/discord/stores/StoreSearch$DisplayState;", "", "component4", "()Z", "guild", "channel", "displayState", "isQueryValid", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Lcom/discord/stores/StoreSearch$DisplayState;Z)Lcom/discord/widgets/search/WidgetSearch$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/stores/StoreSearch$DisplayState;", "getDisplayState", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Lcom/discord/stores/StoreSearch$DisplayState;Z)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Channel channel;
        private final StoreSearch.DisplayState displayState;
        private final Guild guild;
        private final boolean isQueryValid;

        /* compiled from: WidgetSearch.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ%\u0010\b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/search/WidgetSearch$Model$Companion;", "", "", "targetType", "", "targetId", "Lrx/Observable;", "Lcom/discord/widgets/search/WidgetSearch$Model;", "get", "(IJ)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(int i, long j) {
                Observable<Guild> observable;
                Observable<Channel> observable2;
                if (i == 0) {
                    observable = StoreStream.Companion.getGuilds().observeGuild(j);
                } else {
                    observable = new k<>(null);
                }
                if (i == 1) {
                    observable2 = StoreStream.Companion.getChannels().observePrivateChannel(j);
                } else {
                    observable2 = new k<>(null);
                }
                StoreStream.Companion companion = StoreStream.Companion;
                Observable h = Observable.h(observable, observable2, companion.getSearch().getDisplayState(), companion.getSearch().getStoreSearchInput().isInputValid(), WidgetSearch$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(h, "Observable\n            .…          }\n            }");
                return ObservableExtensionsKt.computationLatest(h);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Guild guild, Channel channel, StoreSearch.DisplayState displayState, boolean z2) {
            this.guild = guild;
            this.channel = channel;
            this.displayState = displayState;
            this.isQueryValid = z2;
        }

        public static /* synthetic */ Model copy$default(Model model, Guild guild, Channel channel, StoreSearch.DisplayState displayState, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = model.guild;
            }
            if ((i & 2) != 0) {
                channel = model.channel;
            }
            if ((i & 4) != 0) {
                displayState = model.displayState;
            }
            if ((i & 8) != 0) {
                z2 = model.isQueryValid;
            }
            return model.copy(guild, channel, displayState, z2);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final StoreSearch.DisplayState component3() {
            return this.displayState;
        }

        public final boolean component4() {
            return this.isQueryValid;
        }

        public final Model copy(Guild guild, Channel channel, StoreSearch.DisplayState displayState, boolean z2) {
            return new Model(guild, channel, displayState, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.guild, model.guild) && m.areEqual(this.channel, model.channel) && m.areEqual(this.displayState, model.displayState) && this.isQueryValid == model.isQueryValid;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final StoreSearch.DisplayState getDisplayState() {
            return this.displayState;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            StoreSearch.DisplayState displayState = this.displayState;
            if (displayState != null) {
                i = displayState.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isQueryValid;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isQueryValid() {
            return this.isQueryValid;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guild=");
            R.append(this.guild);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", displayState=");
            R.append(this.displayState);
            R.append(", isQueryValid=");
            return a.M(R, this.isQueryValid, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StoreSearch.DisplayState.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreSearch.DisplayState.SUGGESTIONS.ordinal()] = 1;
            iArr[StoreSearch.DisplayState.RESULTS.ordinal()] = 2;
        }
    }

    public WidgetSearch() {
        super(R.layout.widget_search);
    }

    private final void configureSearchInput() {
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.widgetSearchInput");
        ViewExtensions.setOnEditorActionListener(textInputLayout, new WidgetSearch$configureSearchInput$1(this));
        TextInputLayout textInputLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.widgetSearchInput");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, WidgetSearch$configureSearchInput$2.INSTANCE);
        Observable F = ObservableExtensionsKt.ui$default(StoreStream.Companion.getSearch().getStoreSearchInput().getForcedInput(), this, null, 2, null).F(new b<List<? extends QueryNode>, DraweeSpanStringBuilder>() { // from class: com.discord.widgets.search.WidgetSearch$configureSearchInput$3
            public final DraweeSpanStringBuilder call(List<? extends QueryNode> list) {
                m.checkNotNullExpressionValue(list, "queryNodes");
                return AstRenderer.render(list, WidgetSearch.this.requireContext());
            }
        }).F(WidgetSearch$configureSearchInput$4.INSTANCE);
        m.checkNotNullExpressionValue(F, "StoreStream\n        .get… obj.toString()\n        }");
        ObservableExtensionsKt.appSubscribe(F, WidgetSearch.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSearch$configureSearchInput$5(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        CharSequence e;
        if (model == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        int i = this.targetType;
        int i2 = 0;
        if (i == 0) {
            TextInputLayout textInputLayout = getBinding().c;
            m.checkNotNullExpressionValue(textInputLayout, "binding.widgetSearchInput");
            Object[] objArr = new Object[1];
            Guild guild = model.getGuild();
            objArr[0] = guild != null ? guild.getName() : null;
            e = b.a.k.b.e(this, R.string.search_in, objArr, (r4 & 4) != 0 ? b.a.j : null);
            ViewExtensions.setSingleLineHint(textInputLayout, e);
        } else if (i == 1) {
            Channel channel = model.getChannel();
            String d = channel != null ? ChannelUtils.d(channel, requireContext(), false) : null;
            TextInputLayout textInputLayout2 = getBinding().c;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.widgetSearchInput");
            Channel channel2 = model.getChannel();
            Integer valueOf = channel2 != null ? Integer.valueOf(channel2.A()) : null;
            ViewExtensions.setSingleLineHint(textInputLayout2, (valueOf != null && valueOf.intValue() == 1) ? b.a.k.b.e(this, R.string.search_dm_with, new Object[]{d}, (r4 & 4) != 0 ? b.a.j : null) : (valueOf != null && valueOf.intValue() == 3) ? b.a.k.b.e(this, R.string.search_in, new Object[]{d}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(this, R.string.search, new Object[0], (r4 & 4) != 0 ? b.a.j : null));
        }
        StoreSearch.DisplayState displayState = model.getDisplayState();
        if (displayState != null) {
            int ordinal = displayState.ordinal();
            if (ordinal == 0) {
                FragmentContainerView fragmentContainerView = getBinding().e;
                m.checkNotNullExpressionValue(fragmentContainerView, "binding.widgetSearchSuggestions");
                fragmentContainerView.setVisibility(0);
                FragmentContainerView fragmentContainerView2 = getBinding().d;
                m.checkNotNullExpressionValue(fragmentContainerView2, "binding.widgetSearchResults");
                fragmentContainerView2.setVisibility(4);
                FloatingActionButton floatingActionButton = getBinding().f2498b;
                m.checkNotNullExpressionValue(floatingActionButton, "binding.searchSendQueryFab");
                if (!model.isQueryValid()) {
                    i2 = 8;
                }
                floatingActionButton.setVisibility(i2);
            } else if (ordinal == 1) {
                FragmentContainerView fragmentContainerView3 = getBinding().e;
                m.checkNotNullExpressionValue(fragmentContainerView3, "binding.widgetSearchSuggestions");
                fragmentContainerView3.setVisibility(4);
                FragmentContainerView fragmentContainerView4 = getBinding().d;
                m.checkNotNullExpressionValue(fragmentContainerView4, "binding.widgetSearchResults");
                fragmentContainerView4.setVisibility(0);
                FloatingActionButton floatingActionButton2 = getBinding().f2498b;
                m.checkNotNullExpressionValue(floatingActionButton2, "binding.searchSendQueryFab");
                floatingActionButton2.setVisibility(8);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSearchBinding getBinding() {
        return (WidgetSearchBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void sendQuery(Context context) {
        AppFragment.hideKeyboard$default(this, null, 1, null);
        StoreSearch search = StoreStream.Companion.getSearch();
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.widgetSearchInput");
        search.loadInitial(ViewExtensions.getTextOrEmpty(textInputLayout), new ContextSearchStringProvider(context));
    }

    public final long getTargetId() {
        return this.targetId;
    }

    public final int getTargetType() {
        return this.targetType;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        StoreStream.Companion.getSearch().clear();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setRetainInstance(true);
        this.targetId = getMostRecentIntent().getLongExtra(INTENT_EXTRA_TARGET_ID, 0L);
        this.targetType = getMostRecentIntent().getIntExtra(INTENT_EXTRA_TARGET_TYPE, -1);
        if (!isRecreated()) {
            TextInputLayout textInputLayout = getBinding().c;
            m.checkNotNullExpressionValue(textInputLayout, "binding.widgetSearchInput");
            showKeyboard(textInputLayout);
        }
        int i = this.targetType;
        if (i == 0) {
            StoreStream.Companion.getSearch().initForGuild(this.targetId, new ContextSearchStringProvider(requireContext()));
        } else if (i == 1) {
            StoreStream.Companion.getSearch().initForChannel(this.targetId, new ContextSearchStringProvider(requireContext()));
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(this.targetType, this.targetId), this, null, 2, null), WidgetSearch.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSearch$onViewBoundOrOnResume$1(this));
        configureSearchInput();
        getBinding().f2498b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.search.WidgetSearch$onViewBoundOrOnResume$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSearch widgetSearch = WidgetSearch.this;
                m.checkNotNullExpressionValue(view, "v");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "v.context");
                widgetSearch.sendQuery(context);
            }
        });
        getBinding().c.setStartIconOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.search.WidgetSearch$onViewBoundOrOnResume$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSearch.this.hideKeyboard(view);
                WidgetSearch.this.requireActivity().onBackPressed();
            }
        });
    }

    public final void setTargetId(long j) {
        this.targetId = j;
    }

    public final void setTargetType(int i) {
        this.targetType = i;
    }
}
