package com.discord.widgets.search;

import androidx.core.app.NotificationCompat;
import androidx.core.graphics.drawable.IconCompat;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: WidgetSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", IconCompat.EXTRA_OBJ, "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSearch$configureSearchInput$4<T, R> implements b<DraweeSpanStringBuilder, String> {
    public static final WidgetSearch$configureSearchInput$4 INSTANCE = new WidgetSearch$configureSearchInput$4();

    public final String call(DraweeSpanStringBuilder draweeSpanStringBuilder) {
        m.checkNotNullParameter(draweeSpanStringBuilder, IconCompat.EXTRA_OBJ);
        return draweeSpanStringBuilder.toString();
    }
}
