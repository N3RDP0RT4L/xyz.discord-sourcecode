package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.IdRes;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.f;
import b.a.d.j;
import b.a.k.b;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsVoiceBinding;
import com.discord.databinding.WidgetSettingsVoiceInputModeBinding;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.utilities.voice.DiscordOverlayService;
import com.discord.utilities.voice.PerceptualVolumeUtils;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.discord.widgets.settings.WidgetSettingsVoice;
import com.google.android.material.button.MaterialButton;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlinx.coroutines.CoroutineScope;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func3;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetSettingsVoice.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 -2\u00020\u0001:\u0003-./B\u0007¢\u0006\u0004\b,\u0010\tJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J)\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u00122\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0016¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0019\u0010\tR\u001c\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R:\u0010'\u001a&\u0012\f\u0012\n &*\u0004\u0018\u00010%0% &*\u0012\u0012\f\u0012\n &*\u0004\u0018\u00010%0%\u0018\u00010$0$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b*\u0010+¨\u00060"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsVoice;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/WidgetSettingsVoice$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/settings/WidgetSettingsVoice$Model;)V", "configureVoiceSensitivity", "onOpenSLESConfigChanged", "()V", "Landroid/content/Context;", "context", "onOverlayToggled", "(Landroid/content/Context;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "", "requestCode", "resultCode", "Landroid/content/Intent;", "data", "onActivityResult", "(IILandroid/content/Intent;)V", "onViewBoundOrOnResume", "", "Lcom/discord/views/CheckedSetting;", "openSLESConfigRadioButtons", "Ljava/util/List;", "Lcom/discord/databinding/WidgetSettingsVoiceBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsVoiceBinding;", "binding", "Lrx/subjects/BehaviorSubject;", "", "kotlin.jvm.PlatformType", "requestListenForSensitivitySubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/views/RadioManager;", "openSLESConfigRadioManager", "Lcom/discord/views/RadioManager;", HookHelper.constructorName, "Companion", "InputModeSelector", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsVoice extends AppFragment {
    private static final String ARG_TARGET_AUTO_TOGGLE = "ARG_TARGET_AUTO_TOGGLE";
    private static final String ARG_TARGET_RES_ID = "ARG_TARGET_RES_ID";
    private static final int OVERLAY_PERMISSION_REQUEST_CODE = 2552;
    private List<CheckedSetting> openSLESConfigRadioButtons;
    private RadioManager openSLESConfigRadioManager;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsVoice.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsVoiceBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final MediaEngine.LocalVoiceStatus LOCAL_VOICE_STATUS_ENGINE_UNINITIALIZED = new MediaEngine.LocalVoiceStatus(-1.0f, false);
    private final BehaviorSubject<Boolean> requestListenForSensitivitySubject = BehaviorSubject.l0(Boolean.FALSE);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsVoice$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetSettingsVoice.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J-\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\t\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsVoice$Companion;", "", "Landroid/content/Context;", "context", "", "hasOverlayPermission", "(Landroid/content/Context;)Z", "", "targetResId", "isTargetAutoToggle", "", "launch", "(Landroid/content/Context;Ljava/lang/Integer;Z)V", "", WidgetSettingsVoice.ARG_TARGET_AUTO_TOGGLE, "Ljava/lang/String;", WidgetSettingsVoice.ARG_TARGET_RES_ID, "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "LOCAL_VOICE_STATUS_ENGINE_UNINITIALIZED", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "OVERLAY_PERMISSION_REQUEST_CODE", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final boolean hasOverlayPermission(Context context) {
            return Build.VERSION.SDK_INT <= 22 || Settings.canDrawOverlays(context);
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, Integer num, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                num = null;
            }
            if ((i & 4) != 0) {
                z2 = false;
            }
            companion.launch(context, num, z2);
        }

        public final void launch(Context context, @IdRes Integer num, boolean z2) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            if (num != null) {
                intent.putExtra(WidgetSettingsVoice.ARG_TARGET_RES_ID, num.intValue());
            }
            if (z2) {
                intent.putExtra(WidgetSettingsVoice.ARG_TARGET_AUTO_TOGGLE, z2);
            }
            j.d(context, WidgetSettingsVoice.class, intent);
            StoreAnalytics.onUserSettingsPaneViewed$default(StoreStream.Companion.getAnalytics(), "Voice & Video", null, 2, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsVoice.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J!\u0010\n\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bR\u001d\u0010\u0011\u001a\u00020\f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsVoice$InputModeSelector;", "Lcom/discord/app/AppBottomSheet;", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Lcom/discord/databinding/WidgetSettingsVoiceInputModeBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsVoiceInputModeBinding;", "binding", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class InputModeSelector extends AppBottomSheet {
        public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(InputModeSelector.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsVoiceInputModeBinding;", 0)};
        private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsVoice$InputModeSelector$binding$2.INSTANCE, null, 2, null);

        public InputModeSelector() {
            super(false, 1, null);
        }

        private final WidgetSettingsVoiceInputModeBinding getBinding() {
            return (WidgetSettingsVoiceInputModeBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
        }

        @Override // com.discord.app.AppBottomSheet
        public int getContentViewResId() {
            return R.layout.widget_settings_voice_input_mode;
        }

        @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
        public void onViewCreated(View view, Bundle bundle) {
            m.checkNotNullParameter(view, "view");
            super.onViewCreated(view, bundle);
            setBottomSheetCollapsedStateDisabled();
            getBinding().f2620b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$InputModeSelector$onViewCreated$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    StoreStream.Companion.getMediaSettings().setVoiceInputMode(MediaEngineConnection.InputMode.PUSH_TO_TALK);
                    WidgetSettingsVoice.InputModeSelector.this.dismiss();
                }
            });
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$InputModeSelector$onViewCreated$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    StoreStream.Companion.getMediaSettings().setVoiceInputMode(MediaEngineConnection.InputMode.VOICE_ACTIVITY);
                    WidgetSettingsVoice.InputModeSelector.this.dismiss();
                }
            });
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            MediaEngine.OpenSLESConfig.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[MediaEngine.OpenSLESConfig.DEFAULT.ordinal()] = 1;
            iArr[MediaEngine.OpenSLESConfig.FORCE_ENABLED.ordinal()] = 2;
            iArr[MediaEngine.OpenSLESConfig.FORCE_DISABLED.ordinal()] = 3;
        }
    }

    public WidgetSettingsVoice() {
        super(R.layout.widget_settings_voice);
    }

    public static final /* synthetic */ RadioManager access$getOpenSLESConfigRadioManager$p(WidgetSettingsVoice widgetSettingsVoice) {
        RadioManager radioManager = widgetSettingsVoice.openSLESConfigRadioManager;
        if (radioManager == null) {
            m.throwUninitializedPropertyAccessException("openSLESConfigRadioManager");
        }
        return radioManager;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        int i;
        CheckedSetting checkedSetting;
        CharSequence e;
        SeekBar seekBar = getBinding().f2616s;
        m.checkNotNullExpressionValue(seekBar, "binding.settingsVoiceOutputVolume");
        seekBar.setProgress(d0.a0.a.roundToInt(PerceptualVolumeUtils.amplitudeToPerceptual$default(PerceptualVolumeUtils.INSTANCE, model.getVoiceConfig().getOutputVolume(), 0.0f, 2, null)));
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsVoice.InputModeSelector inputModeSelector = new WidgetSettingsVoice.InputModeSelector();
                FragmentManager childFragmentManager = WidgetSettingsVoice.this.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                inputModeSelector.show(childFragmentManager, String.valueOf(a0.getOrCreateKotlinClass(WidgetSettingsVoice.InputModeSelector.class)));
            }
        });
        TextView textView = getBinding().j;
        int i2 = 0;
        if (model.getModePTT()) {
            i = R.string.input_mode_ptt;
        } else {
            i = model.getModeVAD() ? R.string.input_mode_vad : 0;
        }
        textView.setText(i);
        String H = a.H(new StringBuilder(), f.a.a(360045138471L, null), "?utm_source=discord&utm_medium=blog&utm_campaign=2020-06_help-voice-video&utm_content=--t%3Apm");
        LinkifiedTextView linkifiedTextView = getBinding().D;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.settingsVoiceVideoTroubleshootingGuide");
        boolean z2 = true;
        b.m(linkifiedTextView, R.string.form_help_voice_video_troubleshooting_guide, new Object[]{H}, (r4 & 4) != 0 ? b.g.j : null);
        RadioManager radioManager = this.openSLESConfigRadioManager;
        if (radioManager == null) {
            m.throwUninitializedPropertyAccessException("openSLESConfigRadioManager");
        }
        int ordinal = model.getOpenSLESConfig().ordinal();
        if (ordinal == 0) {
            checkedSetting = getBinding().n;
        } else if (ordinal == 1) {
            checkedSetting = getBinding().p;
        } else if (ordinal == 2) {
            checkedSetting = getBinding().o;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        m.checkNotNullExpressionValue(checkedSetting, "when (model.openSLESConf…esForceDisabled\n        }");
        radioManager.a(checkedSetting);
        List<CheckedSetting> list = this.openSLESConfigRadioButtons;
        if (list == null) {
            m.throwUninitializedPropertyAccessException("openSLESConfigRadioButtons");
        }
        for (final CheckedSetting checkedSetting2 : list) {
            checkedSetting2.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$configureUI$$inlined$forEach$lambda$1
                public final void call(Boolean bool) {
                    WidgetSettingsVoice.access$getOpenSLESConfigRadioManager$p(this).a(CheckedSetting.this);
                    this.onOpenSLESConfigChanged();
                }
            });
        }
        CheckedSetting checkedSetting3 = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.settingsVoiceGainControlToggle");
        checkedSetting3.setChecked(model.getVoiceConfig().getAutomaticGainControl());
        getBinding().e.setOnCheckedListener(WidgetSettingsVoice$configureUI$3.INSTANCE);
        CheckedSetting checkedSetting4 = getBinding().k;
        m.checkNotNullExpressionValue(checkedSetting4, "binding.settingsVoiceNoiseCancellationToggle");
        StoreMediaSettings.NoiseProcessing noiseProcessing = model.getVoiceConfig().getNoiseProcessing();
        StoreMediaSettings.NoiseProcessing noiseProcessing2 = StoreMediaSettings.NoiseProcessing.Cancellation;
        checkedSetting4.setChecked(noiseProcessing == noiseProcessing2);
        getBinding().k.setOnCheckedListener(WidgetSettingsVoice$configureUI$4.INSTANCE);
        TextView textView2 = getBinding().g;
        m.checkNotNullExpressionValue(textView2, "binding.settingsVoiceKrispInfo");
        b.m(textView2, R.string.learn_more_link, new Object[]{f.a.a(360040843952L, null)}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView3 = getBinding().g;
        m.checkNotNullExpressionValue(textView3, "binding.settingsVoiceKrispInfo");
        textView3.setMovementMethod(LinkMovementMethod.getInstance());
        CheckedSetting checkedSetting5 = getBinding().l;
        m.checkNotNullExpressionValue(checkedSetting5, "binding.settingsVoiceNoiseSuppressionToggle");
        checkedSetting5.setChecked(model.getVoiceConfig().getNoiseProcessing() == StoreMediaSettings.NoiseProcessing.Suppression);
        if (model.getVoiceConfig().getNoiseProcessing() == noiseProcessing2) {
            getBinding().l.b(R.string.user_settings_disable_noise_suppression);
            CheckedSetting checkedSetting6 = getBinding().l;
            e = b.e(this, R.string.user_settings_disable_noise_suppression, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            CheckedSetting.i(checkedSetting6, e, false, 2);
        } else {
            getBinding().l.e(WidgetSettingsVoice$configureUI$5.INSTANCE);
            CheckedSetting.i(getBinding().l, null, false, 2);
        }
        CheckedSetting checkedSetting7 = getBinding().h;
        m.checkNotNullExpressionValue(checkedSetting7, "binding.settingsVoiceKrispVadToggle");
        if (model.getVoiceConfig().getVadUseKrisp() != StoreMediaSettings.VadUseKrisp.Enabled) {
            z2 = false;
        }
        checkedSetting7.setChecked(z2);
        getBinding().h.setOnCheckedListener(WidgetSettingsVoice$configureUI$6.INSTANCE);
        CheckedSetting checkedSetting8 = getBinding().c;
        m.checkNotNullExpressionValue(checkedSetting8, "binding.settingsVoiceEchoCancellationToggle");
        checkedSetting8.setChecked(model.getVoiceConfig().getEchoCancellation());
        getBinding().c.setOnCheckedListener(WidgetSettingsVoice$configureUI$7.INSTANCE);
        CheckedSetting checkedSetting9 = getBinding().f2615b;
        m.checkNotNullExpressionValue(checkedSetting9, "binding.settingsVoiceAutoVadToggle");
        checkedSetting9.setChecked(model.getVoiceConfig().getAutomaticVad());
        CheckedSetting checkedSetting10 = getBinding().f2615b;
        m.checkNotNullExpressionValue(checkedSetting10, "binding.settingsVoiceAutoVadToggle");
        if (!model.getModeVAD()) {
            i2 = 8;
        }
        checkedSetting10.setVisibility(i2);
        getBinding().f2615b.setOnCheckedListener(WidgetSettingsVoice$configureUI$8.INSTANCE);
        CheckedSetting checkedSetting11 = getBinding().d;
        m.checkNotNullExpressionValue(checkedSetting11, "binding.settingsVoiceEnableHardwareScalingToggle");
        checkedSetting11.setChecked(model.getVoiceConfig().getEnableVideoHardwareScaling());
        getBinding().d.setOnCheckedListener(WidgetSettingsVoice$configureUI$9.INSTANCE);
        configureVoiceSensitivity(model);
    }

    private final void configureVoiceSensitivity(Model model) {
        RelativeLayout relativeLayout = getBinding().B;
        m.checkNotNullExpressionValue(relativeLayout, "binding.settingsVoiceSensitivityWrap");
        int i = 8;
        relativeLayout.setVisibility(model.getModeVAD() ? 0 : 8);
        TextView textView = getBinding().f2617x;
        m.checkNotNullExpressionValue(textView, "binding.settingsVoiceSensitivityLabel");
        textView.setVisibility(model.getVoiceConfig().getAutomaticVad() ? 0 : 8);
        boolean z2 = !m.areEqual(model.getLocalVoiceStatus(), LOCAL_VOICE_STATUS_ENGINE_UNINITIALIZED);
        LinearLayout linearLayout = getBinding().A;
        m.checkNotNullExpressionValue(linearLayout, "binding.settingsVoiceSensitivityTestingContainer");
        linearLayout.setVisibility(z2 ? 0 : 8);
        MaterialButton materialButton = getBinding().f2619z;
        m.checkNotNullExpressionValue(materialButton, "binding.settingsVoiceSensitivityTestButton");
        if (!z2) {
            i = 0;
        }
        materialButton.setVisibility(i);
        getBinding().f2619z.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$configureVoiceSensitivity$1

            /* compiled from: WidgetSettingsVoice.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettingsVoice$configureVoiceSensitivity$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function0<Unit> {
                public AnonymousClass1() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    BehaviorSubject behaviorSubject;
                    behaviorSubject = WidgetSettingsVoice.this.requestListenForSensitivitySubject;
                    behaviorSubject.onNext(Boolean.TRUE);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                d.S1(WidgetSettingsVoice.this, null, new AnonymousClass1(), 1, null);
            }
        });
        if (model.getVoiceConfig().getAutomaticVad()) {
            View view = getBinding().w;
            m.checkNotNullExpressionValue(view, "binding.settingsVoiceSensitivityAutomatic");
            view.setVisibility(0);
            SeekBar seekBar = getBinding().f2618y;
            m.checkNotNullExpressionValue(seekBar, "binding.settingsVoiceSensitivityManual");
            seekBar.setVisibility(4);
            int i2 = model.getLocalVoiceStatus().f2767b ? R.drawable.drawable_voice_indicator_speaking : R.drawable.drawable_voice_indicator_not_speaking;
            View view2 = getBinding().w;
            m.checkNotNullExpressionValue(view2, "binding.settingsVoiceSensitivityAutomatic");
            view2.setBackground(ContextCompat.getDrawable(requireContext(), i2));
            return;
        }
        View view3 = getBinding().w;
        m.checkNotNullExpressionValue(view3, "binding.settingsVoiceSensitivityAutomatic");
        view3.setVisibility(4);
        SeekBar seekBar2 = getBinding().f2618y;
        m.checkNotNullExpressionValue(seekBar2, "binding.settingsVoiceSensitivityManual");
        seekBar2.setVisibility(0);
        SeekBar seekBar3 = getBinding().f2618y;
        m.checkNotNullExpressionValue(seekBar3, "binding.settingsVoiceSensitivityManual");
        seekBar3.setSecondaryProgress(((int) model.getLocalVoiceStatus().a) + 100);
        SeekBar seekBar4 = getBinding().f2618y;
        m.checkNotNullExpressionValue(seekBar4, "binding.settingsVoiceSensitivityManual");
        seekBar4.setProgress(((int) model.getVoiceConfig().getSensitivity()) + 100);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSettingsVoiceBinding getBinding() {
        return (WidgetSettingsVoiceBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void launch(Context context, @IdRes Integer num, boolean z2) {
        Companion.launch(context, num, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onOpenSLESConfigChanged() {
        MediaEngine.OpenSLESConfig openSLESConfig;
        RadioManager radioManager = this.openSLESConfigRadioManager;
        if (radioManager == null) {
            m.throwUninitializedPropertyAccessException("openSLESConfigRadioManager");
        }
        Checkable checkable = radioManager.a.get(radioManager.b());
        if (m.areEqual(checkable, getBinding().n)) {
            openSLESConfig = MediaEngine.OpenSLESConfig.DEFAULT;
        } else if (m.areEqual(checkable, getBinding().p)) {
            openSLESConfig = MediaEngine.OpenSLESConfig.FORCE_ENABLED;
        } else {
            openSLESConfig = m.areEqual(checkable, getBinding().o) ? MediaEngine.OpenSLESConfig.FORCE_DISABLED : null;
        }
        if (openSLESConfig != null) {
            StoreStream.Companion.getMediaEngine().setOpenSLESConfig(openSLESConfig);
        }
        b.a.d.m.i(this, R.string.user_settings_restart_app_mobile, 0, 4);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onOverlayToggled(Context context) {
        StoreStream.Companion companion = StoreStream.Companion;
        if (companion.getUserSettings().getIsMobileOverlayEnabled()) {
            Observable<RtcConnection.StateChange> x2 = companion.getRtcConnection().getConnectionState().x(WidgetSettingsVoice$onOverlayToggled$1.INSTANCE);
            Observable<R> F = companion.getVoiceChannelSelected().observeSelectedChannel().x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
            m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
            Observable j = Observable.j(x2, F, WidgetSettingsVoice$onOverlayToggled$2.INSTANCE);
            m.checkNotNullExpressionValue(j, "Observable\n          .co… -> rtcState to channel }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout(j, 200L, false), this, null, 2, null), WidgetSettingsVoice.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsVoice$onOverlayToggled$3(context));
            return;
        }
        DiscordOverlayService.Companion.launchForClose(context);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == OVERLAY_PERMISSION_REQUEST_CODE) {
            boolean hasOverlayPermission = Companion.hasOverlayPermission(requireContext());
            StoreStream.Companion.getUserSettings().setIsMobileOverlayEnabled(hasOverlayPermission);
            CheckedSetting checkedSetting = getBinding().u;
            m.checkNotNullExpressionValue(checkedSetting, "binding.settingsVoiceOverlayToggle");
            checkedSetting.setChecked(hasOverlayPermission);
            CheckedSetting checkedSetting2 = getBinding().u;
            m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsVoiceOverlayToggle");
            Context context = checkedSetting2.getContext();
            m.checkNotNullExpressionValue(context, "binding.settingsVoiceOverlayToggle.context");
            onOverlayToggled(context);
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(final View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        TextView textView = getBinding().q;
        m.checkNotNullExpressionValue(textView, "binding.settingsVoiceOpenslesHelp");
        b.m(textView, R.string.form_label_android_opensl_desc, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        CheckedSetting checkedSetting = getBinding().n;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsVoiceOpenslesDefault");
        CheckedSetting checkedSetting2 = getBinding().p;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsVoiceOpenslesForceEnabled");
        boolean z2 = true;
        CheckedSetting checkedSetting3 = getBinding().o;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.settingsVoiceOpenslesForceDisabled");
        List<CheckedSetting> listOf = n.listOf((Object[]) new CheckedSetting[]{checkedSetting, checkedSetting2, checkedSetting3});
        this.openSLESConfigRadioButtons = listOf;
        if (listOf == null) {
            m.throwUninitializedPropertyAccessException("openSLESConfigRadioButtons");
        }
        this.openSLESConfigRadioManager = new RadioManager(listOf);
        CheckedSetting checkedSetting4 = getBinding().u;
        m.checkNotNullExpressionValue(checkedSetting4, "binding.settingsVoiceOverlayToggle");
        checkedSetting4.setChecked(StoreStream.Companion.getUserSettings().getIsMobileOverlayEnabled() && Companion.hasOverlayPermission(requireContext()));
        getBinding().u.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$onViewBound$1
            public final void call(Boolean bool) {
                WidgetSettingsVoiceBinding binding;
                WidgetSettingsVoiceBinding binding2;
                WidgetSettingsVoiceBinding binding3;
                if (!bool.booleanValue() || WidgetSettingsVoice.Companion.hasOverlayPermission(WidgetSettingsVoice.this.requireContext())) {
                    StoreUserSettings userSettings = StoreStream.Companion.getUserSettings();
                    m.checkNotNullExpressionValue(bool, "isChecked");
                    userSettings.setIsMobileOverlayEnabled(bool.booleanValue());
                    WidgetSettingsVoice widgetSettingsVoice = WidgetSettingsVoice.this;
                    binding = widgetSettingsVoice.getBinding();
                    CheckedSetting checkedSetting5 = binding.u;
                    m.checkNotNullExpressionValue(checkedSetting5, "binding.settingsVoiceOverlayToggle");
                    Context context = checkedSetting5.getContext();
                    m.checkNotNullExpressionValue(context, "binding.settingsVoiceOverlayToggle.context");
                    widgetSettingsVoice.onOverlayToggled(context);
                    return;
                }
                binding2 = WidgetSettingsVoice.this.getBinding();
                CheckedSetting checkedSetting6 = binding2.u;
                m.checkNotNullExpressionValue(checkedSetting6, "binding.settingsVoiceOverlayToggle");
                checkedSetting6.setChecked(false);
                AnalyticsTracker.INSTANCE.permissionsRequested("overlay");
                WidgetSettingsVoice widgetSettingsVoice2 = WidgetSettingsVoice.this;
                StringBuilder R = a.R("package:");
                binding3 = WidgetSettingsVoice.this.getBinding();
                CheckedSetting checkedSetting7 = binding3.u;
                m.checkNotNullExpressionValue(checkedSetting7, "binding.settingsVoiceOverlayToggle");
                Context context2 = checkedSetting7.getContext();
                m.checkNotNullExpressionValue(context2, "binding.settingsVoiceOverlayToggle.context");
                R.append(context2.getPackageName());
                widgetSettingsVoice2.startActivityForResult(new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse(R.toString())), 2552);
            }
        });
        for (TextView textView2 : n.listOf((Object[]) new TextView[]{getBinding().f, getBinding().r, getBinding().t, getBinding().v, getBinding().C, getBinding().m})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView2, "header");
            accessibilityUtils.setViewIsHeading(textView2);
        }
        Integer valueOf = Integer.valueOf(getMostRecentIntent().getIntExtra(ARG_TARGET_RES_ID, 0));
        if (valueOf.intValue() != 0) {
            z2 = false;
        }
        if (z2) {
            valueOf = null;
        }
        if (valueOf != null) {
            View findViewById = view.findViewById(valueOf.intValue());
            m.checkNotNullExpressionValue(findViewById, "target");
            CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(findViewById);
            if (coroutineScope != null) {
                b.i.a.f.e.o.f.H0(coroutineScope, null, null, new WidgetSettingsVoice$onViewBound$3$1(findViewById, null), 3, null);
            }
            if (m.areEqual(findViewById, getBinding().u)) {
                CheckedSetting checkedSetting5 = getBinding().u;
                m.checkNotNullExpressionValue(checkedSetting5, "binding.settingsVoiceOverlayToggle");
                if (!checkedSetting5.isChecked() && getMostRecentIntent().getBooleanExtra(ARG_TARGET_AUTO_TOGGLE, false)) {
                    findViewById.post(new Runnable() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$onViewBound$$inlined$let$lambda$1
                        @Override // java.lang.Runnable
                        public final void run() {
                            WidgetSettingsVoiceBinding binding;
                            binding = WidgetSettingsVoice.this.getBinding();
                            binding.u.g(true, true);
                        }
                    });
                }
            }
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        getBinding().f2616s.setOnSeekBarChangeListener(new b.a.y.j() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$onViewBoundOrOnResume$1
            @Override // b.a.y.j, android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int i, boolean z2) {
                m.checkNotNullParameter(seekBar, "seekBar");
                if (z2) {
                    StoreStream.Companion.getMediaSettings().setOutputVolume(PerceptualVolumeUtils.perceptualToAmplitude$default(PerceptualVolumeUtils.INSTANCE, i, 0.0f, 2, null));
                }
            }
        });
        getBinding().f2618y.setOnSeekBarChangeListener(new b.a.y.j() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$onViewBoundOrOnResume$2
            @Override // b.a.y.j, android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int i, boolean z2) {
                m.checkNotNullParameter(seekBar, "seekBar");
                if (z2) {
                    StoreStream.Companion.getMediaSettings().setSensitivity(i - 100.0f);
                }
            }
        });
        Model.Companion companion = Model.Companion;
        BehaviorSubject<Boolean> behaviorSubject = this.requestListenForSensitivitySubject;
        m.checkNotNullExpressionValue(behaviorSubject, "requestListenForSensitivitySubject");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.get(behaviorSubject), this, null, 2, null), WidgetSettingsVoice.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsVoice$onViewBoundOrOnResume$3(this));
    }

    /* compiled from: WidgetSettingsVoice.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\b\u0002\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB!\b\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\t\u001a\u0004\b\r\u0010\u000bR\u0019\u0010\u000f\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0014\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsVoice$Model;", "", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;", "openSLESConfig", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;", "getOpenSLESConfig", "()Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;", "", "modeVAD", "Z", "getModeVAD", "()Z", "modePTT", "getModePTT", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "localVoiceStatus", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "getLocalVoiceStatus", "()Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "voiceConfig", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "getVoiceConfig", "()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", HookHelper.constructorName, "(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final MediaEngine.LocalVoiceStatus localVoiceStatus;
        private final boolean modePTT;
        private final boolean modeVAD;
        private final MediaEngine.OpenSLESConfig openSLESConfig;
        private final StoreMediaSettings.VoiceConfiguration voiceConfig;

        /* compiled from: WidgetSettingsVoice.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ!\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsVoice$Model$Companion;", "", "Lrx/Observable;", "", "requestListenForSensitivity", "Lcom/discord/widgets/settings/WidgetSettingsVoice$Model;", "get", "(Lrx/Observable;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(Observable<Boolean> observable) {
                m.checkNotNullParameter(observable, "requestListenForSensitivity");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable j = Observable.j(observable, companion.getMediaEngine().getIsNativeEngineInitialized(), WidgetSettingsVoice$Model$Companion$get$shouldListenForSensitivity$1.INSTANCE);
                Observable<StoreMediaSettings.VoiceConfiguration> voiceConfig = companion.getMediaSettings().getVoiceConfig();
                Observable<MediaEngine.OpenSLESConfig> openSLESConfig = companion.getMediaEngine().getOpenSLESConfig();
                Observable Y = j.Y(WidgetSettingsVoice$Model$Companion$get$1.INSTANCE);
                final WidgetSettingsVoice$Model$Companion$get$2 widgetSettingsVoice$Model$Companion$get$2 = WidgetSettingsVoice$Model$Companion$get$2.INSTANCE;
                Object obj = widgetSettingsVoice$Model$Companion$get$2;
                if (widgetSettingsVoice$Model$Companion$get$2 != null) {
                    obj = new Func3() { // from class: com.discord.widgets.settings.WidgetSettingsVoice$sam$rx_functions_Func3$0
                        @Override // rx.functions.Func3
                        public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4) {
                            return Function3.this.invoke(obj2, obj3, obj4);
                        }
                    };
                }
                Observable i = Observable.i(voiceConfig, openSLESConfig, Y, (Func3) obj);
                m.checkNotNullExpressionValue(i, "Observable\n            .…    ::Model\n            )");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(i).q();
                m.checkNotNullExpressionValue(q, "Observable\n            .…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        private Model(StoreMediaSettings.VoiceConfiguration voiceConfiguration, MediaEngine.OpenSLESConfig openSLESConfig, MediaEngine.LocalVoiceStatus localVoiceStatus) {
            this.voiceConfig = voiceConfiguration;
            this.openSLESConfig = openSLESConfig;
            this.localVoiceStatus = localVoiceStatus;
            boolean z2 = true;
            this.modePTT = voiceConfiguration.getInputMode() == MediaEngineConnection.InputMode.PUSH_TO_TALK;
            this.modeVAD = voiceConfiguration.getInputMode() != MediaEngineConnection.InputMode.VOICE_ACTIVITY ? false : z2;
        }

        public final MediaEngine.LocalVoiceStatus getLocalVoiceStatus() {
            return this.localVoiceStatus;
        }

        public final boolean getModePTT() {
            return this.modePTT;
        }

        public final boolean getModeVAD() {
            return this.modeVAD;
        }

        public final MediaEngine.OpenSLESConfig getOpenSLESConfig() {
            return this.openSLESConfig;
        }

        public final StoreMediaSettings.VoiceConfiguration getVoiceConfig() {
            return this.voiceConfig;
        }

        public /* synthetic */ Model(StoreMediaSettings.VoiceConfiguration voiceConfiguration, MediaEngine.OpenSLESConfig openSLESConfig, MediaEngine.LocalVoiceStatus localVoiceStatus, DefaultConstructorMarker defaultConstructorMarker) {
            this(voiceConfiguration, openSLESConfig, localVoiceStatus);
        }
    }
}
