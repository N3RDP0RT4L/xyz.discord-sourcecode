package com.discord.widgets.settings;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.rtcconnection.RtcConnection;
import d0.o;
import kotlin.Metadata;
import kotlin.Pair;
import rx.functions.Func2;
/* compiled from: WidgetSettingsVoice.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001aB\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003 \u0001* \u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00050\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$StateChange;", "kotlin.jvm.PlatformType", "rtcState", "Lcom/discord/api/channel/Channel;", "channel", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/rtcconnection/RtcConnection$StateChange;Lcom/discord/api/channel/Channel;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsVoice$onOverlayToggled$2<T1, T2, R> implements Func2<RtcConnection.StateChange, Channel, Pair<? extends RtcConnection.StateChange, ? extends Channel>> {
    public static final WidgetSettingsVoice$onOverlayToggled$2 INSTANCE = new WidgetSettingsVoice$onOverlayToggled$2();

    public final Pair<RtcConnection.StateChange, Channel> call(RtcConnection.StateChange stateChange, Channel channel) {
        return o.to(stateChange, channel);
    }
}
