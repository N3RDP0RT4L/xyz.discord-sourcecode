package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsBehaviorBinding;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action0;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsBehavior.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u0007¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsBehavior;", "Lcom/discord/app/AppFragment;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetSettingsBehaviorBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsBehaviorBinding;", "binding", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsBehavior extends AppFragment {
    private static final String API_LINK = "https://discord.com/developers/docs/intro";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsBehavior$binding$2.INSTANCE, null, 2, null);
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsBehavior.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsBehaviorBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetSettingsBehavior.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsBehavior$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", "", "API_LINK", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsBehavior.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsBehavior() {
        super(R.layout.widget_settings_behavior);
    }

    private final WidgetSettingsBehaviorBinding getBinding() {
        return (WidgetSettingsBehaviorBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void launch(Context context) {
        Companion.launch(context);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        CharSequence b2;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.form_label_mobile_notifications_behavior);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        final StoreUserSettings userSettings = StoreStream.Companion.getUserSettings();
        CheckedSetting checkedSetting = getBinding().f;
        checkedSetting.setChecked(userSettings.getIsShiftEnterToSendEnabled());
        checkedSetting.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsBehavior$onViewBound$$inlined$apply$lambda$1
            public final void call(Boolean bool) {
                StoreUserSettings storeUserSettings = StoreUserSettings.this;
                m.checkNotNullExpressionValue(bool, "isChecked");
                storeUserSettings.setIsShiftEnterToSendEnabled(bool.booleanValue());
            }
        });
        final CheckedSetting checkedSetting2 = getBinding().d;
        checkedSetting2.setChecked(userSettings.getIsDeveloperMode());
        Context context = checkedSetting2.getContext();
        m.checkNotNullExpressionValue(context, "context");
        b.b(context, R.string.developer_mode_help_text, new Object[]{API_LINK}, (r4 & 4) != 0 ? b.C0034b.j : null);
        Context context2 = checkedSetting2.getContext();
        m.checkNotNullExpressionValue(context2, "context");
        b2 = b.b(context2, R.string.developer_mode_help_text, new Object[]{API_LINK}, (r4 & 4) != 0 ? b.C0034b.j : null);
        checkedSetting2.h(b2, false);
        checkedSetting2.setSubtextOnClickListener(new Action0() { // from class: com.discord.widgets.settings.WidgetSettingsBehavior$onViewBound$2$1
            @Override // rx.functions.Action0
            public final void call() {
                UriHandler uriHandler = UriHandler.INSTANCE;
                Context context3 = CheckedSetting.this.getContext();
                m.checkNotNullExpressionValue(context3, "context");
                UriHandler.handle$default(uriHandler, context3, "https://discord.com/developers/docs/intro", null, 4, null);
            }
        });
        checkedSetting2.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsBehavior$onViewBound$$inlined$apply$lambda$2
            public final void call(Boolean bool) {
                StoreUserSettings storeUserSettings = userSettings;
                AppActivity appActivity = WidgetSettingsBehavior.this.getAppActivity();
                m.checkNotNullExpressionValue(bool, "isChecked");
                storeUserSettings.setIsDeveloperMode(appActivity, bool.booleanValue());
            }
        });
        CheckedSetting checkedSetting3 = getBinding().f2579b;
        checkedSetting3.setChecked(userSettings.getIsChromeCustomTabsEnabled());
        checkedSetting3.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsBehavior$onViewBound$$inlined$apply$lambda$3
            public final void call(Boolean bool) {
                StoreUserSettings storeUserSettings = StoreUserSettings.this;
                m.checkNotNullExpressionValue(bool, "isChecked");
                storeUserSettings.setIsChromeCustomTabsEnabled(bool.booleanValue());
            }
        });
        for (TextView textView : n.listOf((Object[]) new TextView[]{getBinding().e, getBinding().c})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView, "header");
            accessibilityUtils.setViewIsHeading(textView);
        }
    }
}
