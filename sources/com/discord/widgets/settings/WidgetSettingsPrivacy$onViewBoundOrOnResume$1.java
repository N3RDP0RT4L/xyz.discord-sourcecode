package com.discord.widgets.settings;

import com.discord.widgets.settings.WidgetSettingsPrivacy;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;", "p1", "", "invoke", "(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettingsPrivacy$onViewBoundOrOnResume$1 extends k implements Function1<WidgetSettingsPrivacy.Model, Unit> {
    public WidgetSettingsPrivacy$onViewBoundOrOnResume$1(WidgetSettingsPrivacy widgetSettingsPrivacy) {
        super(1, widgetSettingsPrivacy, WidgetSettingsPrivacy.class, "configureUI", "configureUI(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetSettingsPrivacy.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetSettingsPrivacy.Model model) {
        m.checkNotNullParameter(model, "p1");
        ((WidgetSettingsPrivacy) this.receiver).configureUI(model);
    }
}
