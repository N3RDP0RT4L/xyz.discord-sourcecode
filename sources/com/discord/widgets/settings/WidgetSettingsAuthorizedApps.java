package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.auth.OAuthScope;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsAuthorizedAppsBinding;
import com.discord.databinding.WidgetSettingsAuthorizedAppsListItemBinding;
import com.discord.models.domain.ModelOAuth2Token;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.discord.views.OAuthPermissionViews;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.settings.WidgetSettingsAuthorizedApps;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.card.MaterialCardView;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetSettingsAuthorizedApps.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001a2\u00020\u0001:\u0002\u001b\u001aB\u0007¢\u0006\u0004\b\u0019\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\t\u0010\u0004J\u0017\u0010\f\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\nH\u0007¢\u0006\u0004\b\f\u0010\rR\u001d\u0010\u0013\u001a\u00020\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\"\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAuthorizedApps;", "Lcom/discord/app/AppFragment;", "", "loadAuthorizedApps", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "", "oauthId", "deauthorizeApp", "(J)V", "Lcom/discord/databinding/WidgetSettingsAuthorizedAppsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsAuthorizedAppsBinding;", "binding", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/models/domain/ModelOAuth2Token;", "Lcom/discord/widgets/settings/WidgetSettingsAuthorizedApps$AuthorizedAppViewHolder;", "adapter", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", HookHelper.constructorName, "Companion", "AuthorizedAppViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAuthorizedApps extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsAuthorizedApps.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsAuthorizedAppsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsAuthorizedApps$binding$2.INSTANCE, null, 2, null);
    private final SimpleRecyclerAdapter<ModelOAuth2Token, AuthorizedAppViewHolder> adapter = new SimpleRecyclerAdapter<>(null, new WidgetSettingsAuthorizedApps$adapter$1(this), 1, null);

    /* compiled from: WidgetSettingsAuthorizedApps.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000f¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\"\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\"\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAuthorizedApps$AuthorizedAppViewHolder;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;", "Lcom/discord/models/domain/ModelOAuth2Token;", "data", "", "bind", "(Lcom/discord/models/domain/ModelOAuth2Token;)V", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/api/auth/OAuthScope;", "Lcom/discord/views/OAuthPermissionViews$a;", "permissionsAdapter", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/databinding/WidgetSettingsAuthorizedAppsListItemBinding;", "binding", "Lcom/discord/databinding/WidgetSettingsAuthorizedAppsListItemBinding;", "Lkotlin/Function1;", "onDeauthorizeClick", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetSettingsAuthorizedAppsListItemBinding;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AuthorizedAppViewHolder extends SimpleRecyclerAdapter.ViewHolder<ModelOAuth2Token> {
        private final WidgetSettingsAuthorizedAppsListItemBinding binding;
        private final Function1<ModelOAuth2Token, Unit> onDeauthorizeClick;
        private final SimpleRecyclerAdapter<OAuthScope, OAuthPermissionViews.a> permissionsAdapter;

        /* JADX WARN: Illegal instructions before constructor call */
        /* JADX WARN: Multi-variable type inference failed */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public AuthorizedAppViewHolder(com.discord.databinding.WidgetSettingsAuthorizedAppsListItemBinding r4, kotlin.jvm.functions.Function1<? super com.discord.models.domain.ModelOAuth2Token, kotlin.Unit> r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                java.lang.String r0 = "onDeauthorizeClick"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                com.google.android.material.card.MaterialCardView r0 = r4.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r3.<init>(r0)
                r3.binding = r4
                r3.onDeauthorizeClick = r5
                com.discord.utilities.views.SimpleRecyclerAdapter r5 = new com.discord.utilities.views.SimpleRecyclerAdapter
                b.a.y.h r0 = b.a.y.h.j
                r1 = 0
                r2 = 1
                r5.<init>(r1, r0, r2, r1)
                r3.permissionsAdapter = r5
                androidx.recyclerview.widget.RecyclerView r0 = r4.i
                java.lang.String r1 = "binding.oauthApplicationPermissionsRv"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r0.setAdapter(r5)
                r5 = 3
                android.widget.TextView[] r5 = new android.widget.TextView[r5]
                android.widget.TextView r0 = r4.g
                r1 = 0
                r5[r1] = r0
                android.widget.TextView r0 = r4.c
                r5[r2] = r0
                android.widget.TextView r4 = r4.h
                r0 = 2
                r5[r0] = r4
                java.util.List r4 = d0.t.n.listOf(r5)
                java.util.Iterator r4 = r4.iterator()
            L46:
                boolean r5 = r4.hasNext()
                if (r5 == 0) goto L5d
                java.lang.Object r5 = r4.next()
                android.widget.TextView r5 = (android.widget.TextView) r5
                com.discord.utilities.accessibility.AccessibilityUtils r0 = com.discord.utilities.accessibility.AccessibilityUtils.INSTANCE
                java.lang.String r1 = "header"
                d0.z.d.m.checkNotNullExpressionValue(r5, r1)
                r0.setViewIsHeading(r5)
                goto L46
            L5d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.WidgetSettingsAuthorizedApps.AuthorizedAppViewHolder.<init>(com.discord.databinding.WidgetSettingsAuthorizedAppsListItemBinding, kotlin.jvm.functions.Function1):void");
        }

        public void bind(final ModelOAuth2Token modelOAuth2Token) {
            m.checkNotNullParameter(modelOAuth2Token, "data");
            Application application = modelOAuth2Token.getApplication();
            String f = application.f();
            String applicationIcon$default = f != null ? IconUtils.getApplicationIcon$default(application.g(), f, 0, 4, (Object) null) : null;
            SimpleDraweeView simpleDraweeView = this.binding.e;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.oauthApplicationIconIv");
            MGImages.setImage$default(simpleDraweeView, applicationIcon$default, 0, 0, false, null, null, 124, null);
            TextView textView = this.binding.g;
            m.checkNotNullExpressionValue(textView, "binding.oauthApplicationNameTv");
            textView.setText(application.h());
            MaterialCardView materialCardView = this.binding.f;
            m.checkNotNullExpressionValue(materialCardView, "binding.oauthApplicationListItem");
            materialCardView.setContentDescription(application.h());
            TextView textView2 = this.binding.c;
            m.checkNotNullExpressionValue(textView2, "binding.oauthApplicationDescriptionLabelTv");
            String c = application.c();
            int i = 0;
            if (!(!(c == null || t.isBlank(c)))) {
                i = 8;
            }
            textView2.setVisibility(i);
            TextView textView3 = this.binding.d;
            m.checkNotNullExpressionValue(textView3, "binding.oauthApplicationDescriptionTv");
            ViewExtensions.setTextAndVisibilityBy(textView3, application.c());
            this.permissionsAdapter.setData(modelOAuth2Token.getScopes());
            this.binding.f2578b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsAuthorizedApps$AuthorizedAppViewHolder$bind$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1 function1;
                    function1 = WidgetSettingsAuthorizedApps.AuthorizedAppViewHolder.this.onDeauthorizeClick;
                    function1.invoke(modelOAuth2Token);
                }
            });
        }
    }

    /* compiled from: WidgetSettingsAuthorizedApps.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAuthorizedApps$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsAuthorizedApps.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsAuthorizedApps() {
        super(R.layout.widget_settings_authorized_apps);
    }

    private final WidgetSettingsAuthorizedAppsBinding getBinding() {
        return (WidgetSettingsAuthorizedAppsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void loadAuthorizedApps() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getOAuthTokens(), false, 1, null), this, null, 2, null), WidgetSettingsAuthorizedApps.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsAuthorizedApps$loadAuthorizedApps$1(this));
    }

    @MainThread
    public final void deauthorizeApp(long j) {
        WidgetNoticeDialog.Builder positiveButton = WidgetNoticeDialog.Builder.setNegativeButton$default(new WidgetNoticeDialog.Builder(requireContext()).setTitle(R.string.deauthorize_app).setMessage(R.string.delete_app_confirm_msg), (int) R.string.cancel, (Function1) null, 2, (Object) null).setPositiveButton(R.string.deauthorize, new WidgetSettingsAuthorizedApps$deauthorizeApp$1(this, j));
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        positiveButton.show(parentFragmentManager);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RecyclerView recyclerView = getBinding().f2577b;
        m.checkNotNullExpressionValue(recyclerView, "binding.authorizedAppsList");
        recyclerView.setAdapter(this.adapter);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        loadAuthorizedApps();
    }
}
