package com.discord.widgets.settings;

import com.discord.stores.StoreStream;
import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0012\u0010\u0003\u001a\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "guildIds", "", "invoke", "(Ljava/util/Set;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPrivacy$updateDefaultGuildsRestricted$2 extends o implements Function1<Set<? extends Long>, Unit> {
    public final /* synthetic */ boolean $defaultGuildsRestricted;
    public final /* synthetic */ WidgetSettingsPrivacy this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPrivacy$updateDefaultGuildsRestricted$2(WidgetSettingsPrivacy widgetSettingsPrivacy, boolean z2) {
        super(1);
        this.this$0 = widgetSettingsPrivacy;
        this.$defaultGuildsRestricted = z2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Set<? extends Long> set) {
        invoke2((Set<Long>) set);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Set<Long> set) {
        StoreStream.Companion.getUserSettings().setDefaultGuildsRestricted(this.this$0.getAppActivity(), this.$defaultGuildsRestricted, set);
    }
}
