package com.discord.widgets.settings.guildboost;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.premium.PremiumTier;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAppliedGuildBoost;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.models.domain.ModelSubscription;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreGuildBoost;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreSubscriptions;
import com.discord.stores.StoreUser;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.settings.guildboost.SettingsGuildBoostSampleGuildAdapter;
import com.discord.widgets.settings.guildboost.WidgetSettingsGuildBoostSubscriptionAdapter;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func4;
import xyz.discord.R;
/* compiled from: SettingsGuildBoostViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 22\b\u0012\u0004\u0012\u00020\u00020\u0001:\u00042345B+\u0012\b\b\u0002\u0010,\u001a\u00020+\u0012\b\b\u0002\u0010)\u001a\u00020(\u0012\u000e\b\u0002\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00060.¢\u0006\u0004\b0\u00101J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\b\u0010\tJ?\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u000b\u001a\u00020\n2\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0017\u0010\u0005J#\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00182\n\u0010\u001a\u001a\u00060\rj\u0002`\u000eH\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ#\u0010!\u001a\u00020\u00032\n\u0010\u001e\u001a\u00060\rj\u0002`\u001d2\u0006\u0010 \u001a\u00020\u001fH\u0007¢\u0006\u0004\b!\u0010\"J\u001b\u0010#\u001a\u00020\u00032\n\u0010\u001e\u001a\u00060\rj\u0002`\u001dH\u0007¢\u0006\u0004\b#\u0010$J\u001b\u0010&\u001a\u00020\u00032\n\u0010%\u001a\u00060\rj\u0002`\u000eH\u0007¢\u0006\u0004\b&\u0010$J\u000f\u0010'\u001a\u00020\u0003H\u0007¢\u0006\u0004\b'\u0010\u0005R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-¨\u00066"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState;", "", "fetchData", "()V", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$StoreState;)V", "Lcom/discord/stores/StoreGuildBoost$State$Loaded;", "guildSubState", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "guilds", "Lcom/discord/models/domain/ModelSubscription;", Traits.Payment.Type.SUBSCRIPTION, "", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "createGuildBoostItems", "(Lcom/discord/stores/StoreGuildBoost$State$Loaded;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscription;)Ljava/util/List;", "retryClicked", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "slot", "previousGuildId", "transferClicked", "(Lcom/discord/models/domain/ModelGuildBoostSlot;J)V", "Lcom/discord/primitives/GuildBoostSlotId;", "slotId", "", "cancel", "cancelClicked", "(JZ)V", "subscribeClicked", "(J)V", "guildId", "handleGuildSearchCallback", "consumePendingAction", "Lcom/discord/stores/StoreSubscriptions;", "storeSubscriptions", "Lcom/discord/stores/StoreSubscriptions;", "Lcom/discord/stores/StoreGuildBoost;", "storeGuildBoost", "Lcom/discord/stores/StoreGuildBoost;", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreGuildBoost;Lcom/discord/stores/StoreSubscriptions;Lrx/Observable;)V", "Companion", "PendingAction", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsGuildBoostViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private static final int NUM_SAMPLE_GUILDS = 4;
    private static final Long UNUSED_GUILD_BOOST_GUILD_ID = null;
    private final StoreGuildBoost storeGuildBoost;
    private final StoreSubscriptions storeSubscriptions;

    /* compiled from: SettingsGuildBoostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.guildboost.SettingsGuildBoostViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            SettingsGuildBoostViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: SettingsGuildBoostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u001e\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$StoreState;", "observeStores", "()Lrx/Observable;", "", "NUM_SAMPLE_GUILDS", "I", "", "Lcom/discord/primitives/GuildId;", "UNUSED_GUILD_BOOST_GUILD_ID", "Ljava/lang/Long;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores() {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable observeGuildBoostState$default = StoreGuildBoost.observeGuildBoostState$default(companion.getGuildBoosts(), null, 1, null);
            Observable<StoreSubscriptions.SubscriptionsState> observeSubscriptions = companion.getSubscriptions().observeSubscriptions();
            Observable<LinkedHashMap<Long, Guild>> observeOrderedGuilds = companion.getGuildsSorted().observeOrderedGuilds();
            Observable q = StoreUser.observeMe$default(companion.getUsers(), false, 1, null).F(SettingsGuildBoostViewModel$Companion$observeStores$1.INSTANCE).q();
            final SettingsGuildBoostViewModel$Companion$observeStores$2 settingsGuildBoostViewModel$Companion$observeStores$2 = SettingsGuildBoostViewModel$Companion$observeStores$2.INSTANCE;
            Object obj = settingsGuildBoostViewModel$Companion$observeStores$2;
            if (settingsGuildBoostViewModel$Companion$observeStores$2 != null) {
                obj = new Func4() { // from class: com.discord.widgets.settings.guildboost.SettingsGuildBoostViewModel$sam$rx_functions_Func4$0
                    @Override // rx.functions.Func4
                    public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                        return Function4.this.invoke(obj2, obj3, obj4, obj5);
                    }
                };
            }
            Observable<StoreState> h = Observable.h(observeGuildBoostState$default, observeSubscriptions, observeOrderedGuilds, q, (Func4) obj);
            m.checkNotNullExpressionValue(h, "Observable.combineLatest…     ::StoreState\n      )");
            return h;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsGuildBoostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "", HookHelper.constructorName, "()V", "Cancel", "Subscribe", "Transfer", "Uncancel", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Transfer;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Subscribe;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Cancel;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Uncancel;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class PendingAction {

        /* compiled from: SettingsGuildBoostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Cancel;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "", "Lcom/discord/primitives/GuildBoostSlotId;", "component1", "()J", "slotId", "copy", "(J)Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Cancel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getSlotId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Cancel extends PendingAction {
            private final long slotId;

            public Cancel(long j) {
                super(null);
                this.slotId = j;
            }

            public static /* synthetic */ Cancel copy$default(Cancel cancel, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = cancel.slotId;
                }
                return cancel.copy(j);
            }

            public final long component1() {
                return this.slotId;
            }

            public final Cancel copy(long j) {
                return new Cancel(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Cancel) && this.slotId == ((Cancel) obj).slotId;
                }
                return true;
            }

            public final long getSlotId() {
                return this.slotId;
            }

            public int hashCode() {
                return b.a(this.slotId);
            }

            public String toString() {
                return a.B(a.R("Cancel(slotId="), this.slotId, ")");
            }
        }

        /* compiled from: SettingsGuildBoostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b\u001c\u0010\u001dJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ0\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R!\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001b\u0010\u0005¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Subscribe;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "", "Lcom/discord/primitives/GuildBoostSlotId;", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "slotId", "targetGuildId", "copy", "(JLjava/lang/Long;)Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Subscribe;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getTargetGuildId", "J", "getSlotId", HookHelper.constructorName, "(JLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Subscribe extends PendingAction {
            private final long slotId;
            private final Long targetGuildId;

            public /* synthetic */ Subscribe(long j, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(j, (i & 2) != 0 ? null : l);
            }

            public static /* synthetic */ Subscribe copy$default(Subscribe subscribe, long j, Long l, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = subscribe.slotId;
                }
                if ((i & 2) != 0) {
                    l = subscribe.targetGuildId;
                }
                return subscribe.copy(j, l);
            }

            public final long component1() {
                return this.slotId;
            }

            public final Long component2() {
                return this.targetGuildId;
            }

            public final Subscribe copy(long j, Long l) {
                return new Subscribe(j, l);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Subscribe)) {
                    return false;
                }
                Subscribe subscribe = (Subscribe) obj;
                return this.slotId == subscribe.slotId && m.areEqual(this.targetGuildId, subscribe.targetGuildId);
            }

            public final long getSlotId() {
                return this.slotId;
            }

            public final Long getTargetGuildId() {
                return this.targetGuildId;
            }

            public int hashCode() {
                int a = b.a(this.slotId) * 31;
                Long l = this.targetGuildId;
                return a + (l != null ? l.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("Subscribe(slotId=");
                R.append(this.slotId);
                R.append(", targetGuildId=");
                return a.F(R, this.targetGuildId, ")");
            }

            public Subscribe(long j, Long l) {
                super(null);
                this.slotId = j;
                this.targetGuildId = l;
            }
        }

        /* compiled from: SettingsGuildBoostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\n\u0010\f\u001a\u00060\u0005j\u0002`\u0006\u0012\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0018\u0010\t\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ:\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\f\b\u0002\u0010\f\u001a\u00060\u0005j\u0002`\u00062\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001d\u0010\f\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\bR!\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001f\u001a\u0004\b \u0010\n¨\u0006#"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Transfer;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "component1", "()Lcom/discord/models/domain/ModelGuildBoostSlot;", "", "Lcom/discord/primitives/GuildId;", "component2", "()J", "component3", "()Ljava/lang/Long;", "slot", "previousGuildId", "targetGuildId", "copy", "(Lcom/discord/models/domain/ModelGuildBoostSlot;JLjava/lang/Long;)Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Transfer;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "getSlot", "J", "getPreviousGuildId", "Ljava/lang/Long;", "getTargetGuildId", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGuildBoostSlot;JLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Transfer extends PendingAction {
            private final long previousGuildId;
            private final ModelGuildBoostSlot slot;
            private final Long targetGuildId;

            public /* synthetic */ Transfer(ModelGuildBoostSlot modelGuildBoostSlot, long j, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(modelGuildBoostSlot, j, (i & 4) != 0 ? null : l);
            }

            public static /* synthetic */ Transfer copy$default(Transfer transfer, ModelGuildBoostSlot modelGuildBoostSlot, long j, Long l, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGuildBoostSlot = transfer.slot;
                }
                if ((i & 2) != 0) {
                    j = transfer.previousGuildId;
                }
                if ((i & 4) != 0) {
                    l = transfer.targetGuildId;
                }
                return transfer.copy(modelGuildBoostSlot, j, l);
            }

            public final ModelGuildBoostSlot component1() {
                return this.slot;
            }

            public final long component2() {
                return this.previousGuildId;
            }

            public final Long component3() {
                return this.targetGuildId;
            }

            public final Transfer copy(ModelGuildBoostSlot modelGuildBoostSlot, long j, Long l) {
                m.checkNotNullParameter(modelGuildBoostSlot, "slot");
                return new Transfer(modelGuildBoostSlot, j, l);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Transfer)) {
                    return false;
                }
                Transfer transfer = (Transfer) obj;
                return m.areEqual(this.slot, transfer.slot) && this.previousGuildId == transfer.previousGuildId && m.areEqual(this.targetGuildId, transfer.targetGuildId);
            }

            public final long getPreviousGuildId() {
                return this.previousGuildId;
            }

            public final ModelGuildBoostSlot getSlot() {
                return this.slot;
            }

            public final Long getTargetGuildId() {
                return this.targetGuildId;
            }

            public int hashCode() {
                ModelGuildBoostSlot modelGuildBoostSlot = this.slot;
                int i = 0;
                int a = (b.a(this.previousGuildId) + ((modelGuildBoostSlot != null ? modelGuildBoostSlot.hashCode() : 0) * 31)) * 31;
                Long l = this.targetGuildId;
                if (l != null) {
                    i = l.hashCode();
                }
                return a + i;
            }

            public String toString() {
                StringBuilder R = a.R("Transfer(slot=");
                R.append(this.slot);
                R.append(", previousGuildId=");
                R.append(this.previousGuildId);
                R.append(", targetGuildId=");
                return a.F(R, this.targetGuildId, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Transfer(ModelGuildBoostSlot modelGuildBoostSlot, long j, Long l) {
                super(null);
                m.checkNotNullParameter(modelGuildBoostSlot, "slot");
                this.slot = modelGuildBoostSlot;
                this.previousGuildId = j;
                this.targetGuildId = l;
            }
        }

        /* compiled from: SettingsGuildBoostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Uncancel;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "", "Lcom/discord/primitives/GuildBoostSlotId;", "component1", "()J", "slotId", "copy", "(J)Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction$Uncancel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getSlotId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uncancel extends PendingAction {
            private final long slotId;

            public Uncancel(long j) {
                super(null);
                this.slotId = j;
            }

            public static /* synthetic */ Uncancel copy$default(Uncancel uncancel, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = uncancel.slotId;
                }
                return uncancel.copy(j);
            }

            public final long component1() {
                return this.slotId;
            }

            public final Uncancel copy(long j) {
                return new Uncancel(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Uncancel) && this.slotId == ((Uncancel) obj).slotId;
                }
                return true;
            }

            public final long getSlotId() {
                return this.slotId;
            }

            public int hashCode() {
                return b.a(this.slotId);
            }

            public String toString() {
                return a.B(a.R("Uncancel(slotId="), this.slotId, ")");
            }
        }

        private PendingAction() {
        }

        public /* synthetic */ PendingAction(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsGuildBoostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0005\u0012\u0016\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b\u0012\u0006\u0010\u0014\u001a\u00020\u000e¢\u0006\u0004\b)\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J \u0010\f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JH\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00052\u0018\b\u0002\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b2\b\b\u0002\u0010\u0014\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001f\u001a\u00020\u001e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u0019\u0010\u0012\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b\"\u0010\u0007R\u0019\u0010\u0014\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b$\u0010\u0010R)\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b&\u0010\rR\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010'\u001a\u0004\b(\u0010\u0004¨\u0006+"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$StoreState;", "", "Lcom/discord/stores/StoreGuildBoost$State;", "component1", "()Lcom/discord/stores/StoreGuildBoost$State;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "component2", "()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "component3", "()Ljava/util/Map;", "Lcom/discord/api/premium/PremiumTier;", "component4", "()Lcom/discord/api/premium/PremiumTier;", "guildBoostState", "subscriptionState", "guilds", "userPremiumTier", "copy", "(Lcom/discord/stores/StoreGuildBoost$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/api/premium/PremiumTier;)Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "getSubscriptionState", "Lcom/discord/api/premium/PremiumTier;", "getUserPremiumTier", "Ljava/util/Map;", "getGuilds", "Lcom/discord/stores/StoreGuildBoost$State;", "getGuildBoostState", HookHelper.constructorName, "(Lcom/discord/stores/StoreGuildBoost$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/api/premium/PremiumTier;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StoreGuildBoost.State guildBoostState;
        private final Map<Long, Guild> guilds;
        private final StoreSubscriptions.SubscriptionsState subscriptionState;
        private final PremiumTier userPremiumTier;

        public StoreState(StoreGuildBoost.State state, StoreSubscriptions.SubscriptionsState subscriptionsState, Map<Long, Guild> map, PremiumTier premiumTier) {
            m.checkNotNullParameter(state, "guildBoostState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionState");
            m.checkNotNullParameter(map, "guilds");
            m.checkNotNullParameter(premiumTier, "userPremiumTier");
            this.guildBoostState = state;
            this.subscriptionState = subscriptionsState;
            this.guilds = map;
            this.userPremiumTier = premiumTier;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, StoreGuildBoost.State state, StoreSubscriptions.SubscriptionsState subscriptionsState, Map map, PremiumTier premiumTier, int i, Object obj) {
            if ((i & 1) != 0) {
                state = storeState.guildBoostState;
            }
            if ((i & 2) != 0) {
                subscriptionsState = storeState.subscriptionState;
            }
            if ((i & 4) != 0) {
                map = storeState.guilds;
            }
            if ((i & 8) != 0) {
                premiumTier = storeState.userPremiumTier;
            }
            return storeState.copy(state, subscriptionsState, map, premiumTier);
        }

        public final StoreGuildBoost.State component1() {
            return this.guildBoostState;
        }

        public final StoreSubscriptions.SubscriptionsState component2() {
            return this.subscriptionState;
        }

        public final Map<Long, Guild> component3() {
            return this.guilds;
        }

        public final PremiumTier component4() {
            return this.userPremiumTier;
        }

        public final StoreState copy(StoreGuildBoost.State state, StoreSubscriptions.SubscriptionsState subscriptionsState, Map<Long, Guild> map, PremiumTier premiumTier) {
            m.checkNotNullParameter(state, "guildBoostState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionState");
            m.checkNotNullParameter(map, "guilds");
            m.checkNotNullParameter(premiumTier, "userPremiumTier");
            return new StoreState(state, subscriptionsState, map, premiumTier);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guildBoostState, storeState.guildBoostState) && m.areEqual(this.subscriptionState, storeState.subscriptionState) && m.areEqual(this.guilds, storeState.guilds) && m.areEqual(this.userPremiumTier, storeState.userPremiumTier);
        }

        public final StoreGuildBoost.State getGuildBoostState() {
            return this.guildBoostState;
        }

        public final Map<Long, Guild> getGuilds() {
            return this.guilds;
        }

        public final StoreSubscriptions.SubscriptionsState getSubscriptionState() {
            return this.subscriptionState;
        }

        public final PremiumTier getUserPremiumTier() {
            return this.userPremiumTier;
        }

        public int hashCode() {
            StoreGuildBoost.State state = this.guildBoostState;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            StoreSubscriptions.SubscriptionsState subscriptionsState = this.subscriptionState;
            int hashCode2 = (hashCode + (subscriptionsState != null ? subscriptionsState.hashCode() : 0)) * 31;
            Map<Long, Guild> map = this.guilds;
            int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
            PremiumTier premiumTier = this.userPremiumTier;
            if (premiumTier != null) {
                i = premiumTier.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guildBoostState=");
            R.append(this.guildBoostState);
            R.append(", subscriptionState=");
            R.append(this.subscriptionState);
            R.append(", guilds=");
            R.append(this.guilds);
            R.append(", userPremiumTier=");
            R.append(this.userPremiumTier);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: SettingsGuildBoostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Failure", "Loaded", "Loading", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState$Failure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: SettingsGuildBoostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState$Failure;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failure extends ViewState {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: SettingsGuildBoostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u0006\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\u0017\u001a\u00020\u000f¢\u0006\u0004\b.\u0010/J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011JZ\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00022\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u00062\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u0017\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010\"\u001a\u00020\u00022\b\u0010!\u001a\u0004\u0018\u00010 HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010$\u001a\u0004\b%\u0010\u0004R\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\tR\u001b\u0010\u0016\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\u000eR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010+\u001a\u0004\b,\u0010\u0011R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b-\u0010\t¨\u00060"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState;", "", "component1", "()Z", "component2", "", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "component3", "()Ljava/util/List;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostSampleGuildAdapter$Item;", "component4", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "component5", "()Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "Lcom/discord/api/premium/PremiumTier;", "component6", "()Lcom/discord/api/premium/PremiumTier;", "canCancelBoosts", "canUncancelBoosts", "guildBoostItems", "sampleGuildItems", "pendingAction", "userPremiumTier", "copy", "(ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;Lcom/discord/api/premium/PremiumTier;)Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanCancelBoosts", "Ljava/util/List;", "getSampleGuildItems", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;", "getPendingAction", "getCanUncancelBoosts", "Lcom/discord/api/premium/PremiumTier;", "getUserPremiumTier", "getGuildBoostItems", HookHelper.constructorName, "(ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$PendingAction;Lcom/discord/api/premium/PremiumTier;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean canCancelBoosts;
            private final boolean canUncancelBoosts;
            private final List<WidgetSettingsGuildBoostSubscriptionAdapter.Item> guildBoostItems;
            private final PendingAction pendingAction;
            private final List<SettingsGuildBoostSampleGuildAdapter.Item> sampleGuildItems;
            private final PremiumTier userPremiumTier;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(boolean z2, boolean z3, List<? extends WidgetSettingsGuildBoostSubscriptionAdapter.Item> list, List<SettingsGuildBoostSampleGuildAdapter.Item> list2, PendingAction pendingAction, PremiumTier premiumTier) {
                super(null);
                m.checkNotNullParameter(list, "guildBoostItems");
                m.checkNotNullParameter(list2, "sampleGuildItems");
                m.checkNotNullParameter(premiumTier, "userPremiumTier");
                this.canCancelBoosts = z2;
                this.canUncancelBoosts = z3;
                this.guildBoostItems = list;
                this.sampleGuildItems = list2;
                this.pendingAction = pendingAction;
                this.userPremiumTier = premiumTier;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, boolean z2, boolean z3, List list, List list2, PendingAction pendingAction, PremiumTier premiumTier, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = loaded.canCancelBoosts;
                }
                if ((i & 2) != 0) {
                    z3 = loaded.canUncancelBoosts;
                }
                boolean z4 = z3;
                List<WidgetSettingsGuildBoostSubscriptionAdapter.Item> list3 = list;
                if ((i & 4) != 0) {
                    list3 = loaded.guildBoostItems;
                }
                List list4 = list3;
                List<SettingsGuildBoostSampleGuildAdapter.Item> list5 = list2;
                if ((i & 8) != 0) {
                    list5 = loaded.sampleGuildItems;
                }
                List list6 = list5;
                if ((i & 16) != 0) {
                    pendingAction = loaded.pendingAction;
                }
                PendingAction pendingAction2 = pendingAction;
                if ((i & 32) != 0) {
                    premiumTier = loaded.userPremiumTier;
                }
                return loaded.copy(z2, z4, list4, list6, pendingAction2, premiumTier);
            }

            public final boolean component1() {
                return this.canCancelBoosts;
            }

            public final boolean component2() {
                return this.canUncancelBoosts;
            }

            public final List<WidgetSettingsGuildBoostSubscriptionAdapter.Item> component3() {
                return this.guildBoostItems;
            }

            public final List<SettingsGuildBoostSampleGuildAdapter.Item> component4() {
                return this.sampleGuildItems;
            }

            public final PendingAction component5() {
                return this.pendingAction;
            }

            public final PremiumTier component6() {
                return this.userPremiumTier;
            }

            public final Loaded copy(boolean z2, boolean z3, List<? extends WidgetSettingsGuildBoostSubscriptionAdapter.Item> list, List<SettingsGuildBoostSampleGuildAdapter.Item> list2, PendingAction pendingAction, PremiumTier premiumTier) {
                m.checkNotNullParameter(list, "guildBoostItems");
                m.checkNotNullParameter(list2, "sampleGuildItems");
                m.checkNotNullParameter(premiumTier, "userPremiumTier");
                return new Loaded(z2, z3, list, list2, pendingAction, premiumTier);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.canCancelBoosts == loaded.canCancelBoosts && this.canUncancelBoosts == loaded.canUncancelBoosts && m.areEqual(this.guildBoostItems, loaded.guildBoostItems) && m.areEqual(this.sampleGuildItems, loaded.sampleGuildItems) && m.areEqual(this.pendingAction, loaded.pendingAction) && m.areEqual(this.userPremiumTier, loaded.userPremiumTier);
            }

            public final boolean getCanCancelBoosts() {
                return this.canCancelBoosts;
            }

            public final boolean getCanUncancelBoosts() {
                return this.canUncancelBoosts;
            }

            public final List<WidgetSettingsGuildBoostSubscriptionAdapter.Item> getGuildBoostItems() {
                return this.guildBoostItems;
            }

            public final PendingAction getPendingAction() {
                return this.pendingAction;
            }

            public final List<SettingsGuildBoostSampleGuildAdapter.Item> getSampleGuildItems() {
                return this.sampleGuildItems;
            }

            public final PremiumTier getUserPremiumTier() {
                return this.userPremiumTier;
            }

            public int hashCode() {
                boolean z2 = this.canCancelBoosts;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                boolean z3 = this.canUncancelBoosts;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                int i5 = (i4 + i) * 31;
                List<WidgetSettingsGuildBoostSubscriptionAdapter.Item> list = this.guildBoostItems;
                int i6 = 0;
                int hashCode = (i5 + (list != null ? list.hashCode() : 0)) * 31;
                List<SettingsGuildBoostSampleGuildAdapter.Item> list2 = this.sampleGuildItems;
                int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
                PendingAction pendingAction = this.pendingAction;
                int hashCode3 = (hashCode2 + (pendingAction != null ? pendingAction.hashCode() : 0)) * 31;
                PremiumTier premiumTier = this.userPremiumTier;
                if (premiumTier != null) {
                    i6 = premiumTier.hashCode();
                }
                return hashCode3 + i6;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(canCancelBoosts=");
                R.append(this.canCancelBoosts);
                R.append(", canUncancelBoosts=");
                R.append(this.canUncancelBoosts);
                R.append(", guildBoostItems=");
                R.append(this.guildBoostItems);
                R.append(", sampleGuildItems=");
                R.append(this.sampleGuildItems);
                R.append(", pendingAction=");
                R.append(this.pendingAction);
                R.append(", userPremiumTier=");
                R.append(this.userPremiumTier);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: SettingsGuildBoostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public SettingsGuildBoostViewModel() {
        this(null, null, null, 7, null);
    }

    public /* synthetic */ SettingsGuildBoostViewModel(StoreGuildBoost storeGuildBoost, StoreSubscriptions storeSubscriptions, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getGuildBoosts() : storeGuildBoost, (i & 2) != 0 ? StoreStream.Companion.getSubscriptions() : storeSubscriptions, (i & 4) != 0 ? Companion.observeStores() : observable);
    }

    private final List<WidgetSettingsGuildBoostSubscriptionAdapter.Item> createGuildBoostItems(StoreGuildBoost.State.Loaded loaded, Map<Long, Guild> map, ModelSubscription modelSubscription) {
        Collection<ModelGuildBoostSlot> values = loaded.getBoostSlotMap().values();
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        Iterator<T> it = values.iterator();
        while (true) {
            Long l = null;
            if (!it.hasNext()) {
                break;
            }
            ModelGuildBoostSlot modelGuildBoostSlot = (ModelGuildBoostSlot) it.next();
            ModelAppliedGuildBoost premiumGuildSubscription = modelGuildBoostSlot.getPremiumGuildSubscription();
            if (premiumGuildSubscription != null) {
                l = Long.valueOf(premiumGuildSubscription.getGuildId());
            }
            Object obj = hashMap.get(l);
            if (obj == null) {
                obj = new ArrayList();
            }
            ((ArrayList) obj).add(modelGuildBoostSlot);
            hashMap.put(l, obj);
        }
        ArrayList<ModelGuildBoostSlot> arrayList2 = (ArrayList) hashMap.remove(UNUSED_GUILD_BOOST_GUILD_ID);
        if (arrayList2 != null) {
            for (ModelGuildBoostSlot modelGuildBoostSlot2 : arrayList2) {
                arrayList.add(new WidgetSettingsGuildBoostSubscriptionAdapter.Item.GuildBoostItem(modelGuildBoostSlot2, modelSubscription != null ? modelSubscription.getCurrentPeriodEnd() : null));
            }
        }
        if (!hashMap.isEmpty()) {
            arrayList.add(new WidgetSettingsGuildBoostSubscriptionAdapter.Item.HeaderItem(R.string.premium_guild_subscription_active_title));
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            Long l2 = (Long) entry.getKey();
            ArrayList<ModelGuildBoostSlot> arrayList3 = (ArrayList) entry.getValue();
            Guild guild = map.get(l2);
            if (!(l2 == null || guild == null)) {
                arrayList.add(new WidgetSettingsGuildBoostSubscriptionAdapter.Item.GuildItem(guild, arrayList3.size()));
                for (ModelGuildBoostSlot modelGuildBoostSlot3 : arrayList3) {
                    arrayList.add(new WidgetSettingsGuildBoostSubscriptionAdapter.Item.GuildBoostItem(modelGuildBoostSlot3, modelSubscription != null ? modelSubscription.getCurrentPeriodEnd() : null));
                }
            }
        }
        return arrayList;
    }

    private final void fetchData() {
        this.storeGuildBoost.fetchUserGuildBoostState();
        this.storeSubscriptions.fetchSubscriptions();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0093, code lost:
        if (r8.size() > 2) goto L37;
     */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00ce A[LOOP:1: B:39:0x00c8->B:41:0x00ce, LOOP_END] */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.settings.guildboost.SettingsGuildBoostViewModel.StoreState r17) {
        /*
            Method dump skipped, instructions count: 259
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.guildboost.SettingsGuildBoostViewModel.handleStoreState(com.discord.widgets.settings.guildboost.SettingsGuildBoostViewModel$StoreState):void");
    }

    @MainThread
    public final void cancelClicked(long j, boolean z2) {
        PendingAction pendingAction;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            if (z2) {
                pendingAction = new PendingAction.Cancel(j);
            } else {
                pendingAction = new PendingAction.Uncancel(j);
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, null, null, pendingAction, null, 47, null));
        }
    }

    @MainThread
    public final void consumePendingAction() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, null, null, null, null, 47, null));
        }
    }

    @MainThread
    public final void handleGuildSearchCallback(long j) {
        PendingAction copy$default;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            PendingAction pendingAction = loaded.getPendingAction();
            if (pendingAction instanceof PendingAction.Transfer) {
                copy$default = PendingAction.Transfer.copy$default((PendingAction.Transfer) loaded.getPendingAction(), null, 0L, Long.valueOf(j), 3, null);
            } else {
                copy$default = pendingAction instanceof PendingAction.Subscribe ? PendingAction.Subscribe.copy$default((PendingAction.Subscribe) loaded.getPendingAction(), 0L, Long.valueOf(j), 1, null) : loaded.getPendingAction();
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, null, null, copy$default, null, 47, null));
        }
    }

    @MainThread
    public final void retryClicked() {
        fetchData();
    }

    @MainThread
    public final void subscribeClicked(long j) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, null, null, new PendingAction.Subscribe(j, null, 2, null), null, 47, null));
        }
    }

    @MainThread
    public final void transferClicked(ModelGuildBoostSlot modelGuildBoostSlot, long j) {
        m.checkNotNullParameter(modelGuildBoostSlot, "slot");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, null, null, new PendingAction.Transfer(modelGuildBoostSlot, j, null, 4, null), null, 47, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsGuildBoostViewModel(StoreGuildBoost storeGuildBoost, StoreSubscriptions storeSubscriptions, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(storeGuildBoost, "storeGuildBoost");
        m.checkNotNullParameter(storeSubscriptions, "storeSubscriptions");
        m.checkNotNullParameter(observable, "storeObservable");
        this.storeGuildBoost = storeGuildBoost;
        this.storeSubscriptions = storeSubscriptions;
        fetchData();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), SettingsGuildBoostViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
