package com.discord.widgets.settings.guildboost;

import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.stores.StoreStream;
import com.discord.widgets.guilds.WidgetGuildSelector;
import d0.t.o0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetSettingsGuildBoost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u0006\u0010\u0001\u001a\u00020\u00002\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/models/domain/ModelGuildBoostSlot;", "slot", "", "Lcom/discord/primitives/GuildId;", "previousGuildId", "", "invoke", "(Lcom/discord/models/domain/ModelGuildBoostSlot;J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGuildBoost$showContent$1 extends o implements Function2<ModelGuildBoostSlot, Long, Unit> {
    public final /* synthetic */ WidgetSettingsGuildBoost this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsGuildBoost$showContent$1(WidgetSettingsGuildBoost widgetSettingsGuildBoost) {
        super(2);
        this.this$0 = widgetSettingsGuildBoost;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(ModelGuildBoostSlot modelGuildBoostSlot, Long l) {
        invoke(modelGuildBoostSlot, l.longValue());
        return Unit.a;
    }

    public final void invoke(ModelGuildBoostSlot modelGuildBoostSlot, long j) {
        SettingsGuildBoostViewModel viewModel;
        m.checkNotNullParameter(modelGuildBoostSlot, "slot");
        viewModel = this.this$0.getViewModel();
        viewModel.transferClicked(modelGuildBoostSlot, j);
        WidgetGuildSelector.Companion.launch$default(WidgetGuildSelector.Companion, this.this$0, null, false, 0, new WidgetGuildSelector.GuildFilterFunction(o0.minus(StoreStream.Companion.getGuilds().getGuilds().keySet(), Long.valueOf(j))), 14, null);
    }
}
