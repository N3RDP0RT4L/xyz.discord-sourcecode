package com.discord.widgets.settings.guildboost;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.format.DateFormat;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.databinding.ViewSettingsBoostedBoostListitemBinding;
import com.discord.databinding.ViewSettingsBoostedGuildListitemBinding;
import com.discord.databinding.ViewSettingsBoostedHeaderListitemBinding;
import com.discord.models.domain.ModelAppliedGuildBoost;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.models.guild.Guild;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.guildboost.GuildBoostProgressView;
import com.discord.widgets.settings.guildboost.WidgetSettingsGuildBoostSubscriptionAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\"#$%B\u000f\u0012\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b \u0010!J+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u007f\u0010\u0019\u001a\u00020\u000f2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\n2\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\f2\u001c\u0010\u0014\u001a\u0018\u0012\u0004\u0012\u00020\u0012\u0012\b\u0012\u00060\rj\u0002`\u0013\u0012\u0004\u0012\u00020\u000f0\u00112\u001c\u0010\u0016\u001a\u0018\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000f0\u00112\u0006\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0015¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u001bR,\u0010\u0014\u001a\u0018\u0012\u0004\u0012\u00020\u0012\u0012\b\u0012\u00060\rj\u0002`\u0013\u0012\u0004\u0012\u00020\u000f0\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u001cR&\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u001dR,\u0010\u0016\u001a\u0018\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000f0\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u001cR\u0016\u0010\u0017\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u001b¨\u0006&"}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "", "guildBoostItems", "Lkotlin/Function1;", "", "Lcom/discord/primitives/GuildBoostSlotId;", "", "subscribeListener", "Lkotlin/Function2;", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "Lcom/discord/primitives/GuildId;", "transferListener", "", "cancelListener", "canCancelBoosts", "canUncancelBoosts", "configure", "(Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;ZZ)V", "Z", "Lkotlin/jvm/functions/Function2;", "Lkotlin/jvm/functions/Function1;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "GuildBoostListItem", "GuildListItem", "HeaderListItem", "Item", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGuildBoostSubscriptionAdapter extends MGRecyclerAdapterSimple<Item> {
    private boolean canCancelBoosts;
    private boolean canUncancelBoosts;
    private Function1<? super Long, Unit> subscribeListener = WidgetSettingsGuildBoostSubscriptionAdapter$subscribeListener$1.INSTANCE;
    private Function2<? super ModelGuildBoostSlot, ? super Long, Unit> transferListener = WidgetSettingsGuildBoostSubscriptionAdapter$transferListener$1.INSTANCE;
    private Function2<? super Long, ? super Boolean, Unit> cancelListener = WidgetSettingsGuildBoostSubscriptionAdapter$cancelListener$1.INSTANCE;

    /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0016\u001a\u00020\u0002¢\u0006\u0004\b\u0017\u0010\u0018J7\u0010\r\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$GuildBoostListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "Landroid/view/View;", "sourceView", "", "hasCooldown", "canBeCancelled", "canBeUncancelled", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildBoostItem;", "data", "", "showGuildBoostPopup", "(Landroid/view/View;ZZZLcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildBoostItem;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onConfigure", "(ILcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;)V", "Lcom/discord/databinding/ViewSettingsBoostedBoostListitemBinding;", "binding", "Lcom/discord/databinding/ViewSettingsBoostedBoostListitemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GuildBoostListItem extends MGRecyclerViewHolder<WidgetSettingsGuildBoostSubscriptionAdapter, Item> {
        private final ViewSettingsBoostedBoostListitemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public GuildBoostListItem(WidgetSettingsGuildBoostSubscriptionAdapter widgetSettingsGuildBoostSubscriptionAdapter) {
            super((int) R.layout.view_settings_boosted_boost_listitem, widgetSettingsGuildBoostSubscriptionAdapter);
            m.checkNotNullParameter(widgetSettingsGuildBoostSubscriptionAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.boosted_boost_action;
            TextView textView = (TextView) view.findViewById(R.id.boosted_boost_action);
            if (textView != null) {
                i = R.id.boosted_boost_cancelled;
                ImageView imageView = (ImageView) view.findViewById(R.id.boosted_boost_cancelled);
                if (imageView != null) {
                    i = R.id.boosted_boost_cooldown;
                    TextView textView2 = (TextView) view.findViewById(R.id.boosted_boost_cooldown);
                    if (textView2 != null) {
                        i = R.id.boosted_boost_date;
                        TextView textView3 = (TextView) view.findViewById(R.id.boosted_boost_date);
                        if (textView3 != null) {
                            ViewSettingsBoostedBoostListitemBinding viewSettingsBoostedBoostListitemBinding = new ViewSettingsBoostedBoostListitemBinding((RelativeLayout) view, textView, imageView, textView2, textView3);
                            m.checkNotNullExpressionValue(viewSettingsBoostedBoostListitemBinding, "ViewSettingsBoostedBoost…temBinding.bind(itemView)");
                            this.binding = viewSettingsBoostedBoostListitemBinding;
                            return;
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetSettingsGuildBoostSubscriptionAdapter access$getAdapter$p(GuildBoostListItem guildBoostListItem) {
            return (WidgetSettingsGuildBoostSubscriptionAdapter) guildBoostListItem.adapter;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void showGuildBoostPopup(View view, boolean z2, boolean z3, boolean z4, final Item.GuildBoostItem guildBoostItem) {
            PopupMenu popupMenu = new PopupMenu(new ContextThemeWrapper(view.getContext(), (int) R.style.AppTheme_PopupMenu), view);
            popupMenu.getMenuInflater().inflate(R.menu.menu_settings_premium_guild_sub, popupMenu.getMenu());
            MenuItem findItem = popupMenu.getMenu().findItem(R.id.menu_settings_premium_guild_sub_transfer);
            MenuItem findItem2 = popupMenu.getMenu().findItem(R.id.menu_settings_premium_guild_sub_cancel);
            MenuItem findItem3 = popupMenu.getMenu().findItem(R.id.menu_settings_premium_guild_sub_uncancel);
            boolean canceled = guildBoostItem.getBoostSlot().getCanceled();
            m.checkNotNullExpressionValue(findItem, "transfer");
            boolean z5 = true;
            findItem.setVisible(!z2);
            m.checkNotNullExpressionValue(findItem2, "cancel");
            findItem2.setVisible(!canceled && z3);
            m.checkNotNullExpressionValue(findItem3, "uncancel");
            if (!canceled || !z4) {
                z5 = false;
            }
            findItem3.setVisible(z5);
            findItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.settings.guildboost.WidgetSettingsGuildBoostSubscriptionAdapter$GuildBoostListItem$showGuildBoostPopup$1
                @Override // android.view.MenuItem.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    Function2 function2;
                    ModelAppliedGuildBoost premiumGuildSubscription = guildBoostItem.getBoostSlot().getPremiumGuildSubscription();
                    if (premiumGuildSubscription == null) {
                        return true;
                    }
                    long guildId = premiumGuildSubscription.getGuildId();
                    function2 = WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.access$getAdapter$p(WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.this).transferListener;
                    function2.invoke(guildBoostItem.getBoostSlot(), Long.valueOf(guildId));
                    return true;
                }
            });
            findItem2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.settings.guildboost.WidgetSettingsGuildBoostSubscriptionAdapter$GuildBoostListItem$showGuildBoostPopup$2
                @Override // android.view.MenuItem.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    Function2 function2;
                    function2 = WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.access$getAdapter$p(WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.this).cancelListener;
                    function2.invoke(Long.valueOf(guildBoostItem.getBoostSlot().getId()), Boolean.TRUE);
                    return true;
                }
            });
            findItem3.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.settings.guildboost.WidgetSettingsGuildBoostSubscriptionAdapter$GuildBoostListItem$showGuildBoostPopup$3
                @Override // android.view.MenuItem.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    Function2 function2;
                    function2 = WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.access$getAdapter$p(WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.this).cancelListener;
                    function2.invoke(Long.valueOf(guildBoostItem.getBoostSlot().getId()), Boolean.FALSE);
                    return true;
                }
            });
            popupMenu.show();
        }

        public void onConfigure(int i, final Item item) {
            CharSequence charSequence;
            String str;
            char c;
            int i2;
            String str2;
            String str3;
            CharSequence charSequence2;
            int i3;
            String boostEndsAt;
            CharSequence b2;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            if (item instanceof Item.GuildBoostItem) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Context context = view.getContext();
                long currentTimeMillis = ClockFactory.get().currentTimeMillis();
                Item.GuildBoostItem guildBoostItem = (Item.GuildBoostItem) item;
                final ModelGuildBoostSlot boostSlot = guildBoostItem.getBoostSlot();
                ModelAppliedGuildBoost premiumGuildSubscription = boostSlot.getPremiumGuildSubscription();
                boolean z2 = (premiumGuildSubscription != null ? Long.valueOf(premiumGuildSubscription.getGuildId()) : null) != null;
                boolean z3 = boostSlot.getCooldownExpiresAtTimestamp() > currentTimeMillis;
                ModelAppliedGuildBoost premiumGuildSubscription2 = boostSlot.getPremiumGuildSubscription();
                Long valueOf = premiumGuildSubscription2 != null ? Long.valueOf((premiumGuildSubscription2.getId() >>> 22) + SnowflakeUtils.DISCORD_EPOCH) : null;
                boolean canceled = boostSlot.getCanceled();
                ImageView imageView = this.binding.c;
                m.checkNotNullExpressionValue(imageView, "binding.boostedBoostCancelled");
                imageView.setVisibility(canceled ? 0 : 8);
                TextView textView = this.binding.e;
                m.checkNotNullExpressionValue(textView, "binding.boostedBoostDate");
                if (valueOf == null || (charSequence = DateFormat.format("MMMM dd, yyy", valueOf.longValue())) == null) {
                    charSequence = context.getString(R.string.premium_guild_subscription_unused_slot_description);
                }
                textView.setText(charSequence);
                if (!canceled || (boostEndsAt = guildBoostItem.getBoostEndsAt()) == null) {
                    str = "context";
                    i2 = 0;
                    c = 1;
                    str2 = null;
                } else {
                    m.checkNotNullExpressionValue(context, "context");
                    str = "context";
                    i2 = 0;
                    c = 1;
                    b2 = b.b(context, R.string.premium_guild_subscription_pending_cancelation, new Object[]{TimeUtils.renderUtcDate$default(TimeUtils.INSTANCE, boostEndsAt, context, (String) null, (java.text.DateFormat) null, 0, 28, (Object) null)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    str2 = t.replace$default(b2.toString(), "*", "", false, 4, (Object) null);
                }
                if (z3) {
                    long max = Math.max(boostSlot.getCooldownExpiresAtTimestamp() - currentTimeMillis, 0L);
                    long j = max / 86400000;
                    long j2 = max - (86400000 * j);
                    long j3 = j2 / 3600000;
                    str3 = str;
                    m.checkNotNullExpressionValue(context, str3);
                    Object[] objArr = new Object[3];
                    objArr[i2] = String.valueOf(j);
                    objArr[c] = String.valueOf(j3);
                    objArr[2] = String.valueOf((j2 - (3600000 * j3)) / 60000);
                    charSequence2 = b.b(context, R.string.premium_guild_cooldown_available_countdown, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
                } else {
                    str3 = str;
                    charSequence2 = null;
                }
                if (str2 == null && charSequence2 == null) {
                    TextView textView2 = this.binding.d;
                    m.checkNotNullExpressionValue(textView2, "binding.boostedBoostCooldown");
                    textView2.setVisibility(8);
                } else if (str2 != null && charSequence2 != null) {
                    TextView textView3 = this.binding.d;
                    m.checkNotNullExpressionValue(textView3, "binding.boostedBoostCooldown");
                    ViewExtensions.setTextAndVisibilityBy(textView3, charSequence2 + " - " + str2);
                } else if (str2 != null && charSequence2 == null) {
                    TextView textView4 = this.binding.d;
                    m.checkNotNullExpressionValue(textView4, "binding.boostedBoostCooldown");
                    ViewExtensions.setTextAndVisibilityBy(textView4, str2);
                } else if (str2 == null && charSequence2 != null) {
                    TextView textView5 = this.binding.d;
                    m.checkNotNullExpressionValue(textView5, "binding.boostedBoostCooldown");
                    ViewExtensions.setTextAndVisibilityBy(textView5, charSequence2);
                }
                TextView textView6 = this.binding.f2195b;
                m.checkNotNullExpressionValue(textView6, "binding.boostedBoostAction");
                textView6.setText((z2 || z3) ? "" : context.getString(R.string.premium_guild_subscription_select_server_button));
                if (!z3 || (!canceled && ((WidgetSettingsGuildBoostSubscriptionAdapter) this.adapter).canCancelBoosts) || (canceled && ((WidgetSettingsGuildBoostSubscriptionAdapter) this.adapter).canUncancelBoosts)) {
                    m.checkNotNullExpressionValue(context, str3);
                    i3 = DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.ic_overflow, i2, 2, (Object) null);
                } else {
                    i3 = 0;
                }
                this.binding.f2195b.setCompoundDrawablesWithIntrinsicBounds(i2, i2, i3, i2);
                final boolean z4 = z2;
                final boolean z5 = z3;
                this.binding.f2195b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.guildboost.WidgetSettingsGuildBoostSubscriptionAdapter$GuildBoostListItem$onConfigure$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        Function1 function1;
                        if (z4 || z5) {
                            WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem guildBoostListItem = WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.this;
                            m.checkNotNullExpressionValue(view2, "view");
                            guildBoostListItem.showGuildBoostPopup(view2, z5, WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.access$getAdapter$p(WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.this).canCancelBoosts, WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.access$getAdapter$p(WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.this).canUncancelBoosts, (WidgetSettingsGuildBoostSubscriptionAdapter.Item.GuildBoostItem) item);
                            return;
                        }
                        function1 = WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.access$getAdapter$p(WidgetSettingsGuildBoostSubscriptionAdapter.GuildBoostListItem.this).subscribeListener;
                        function1.invoke(Long.valueOf(boostSlot.getId()));
                    }
                });
                return;
            }
            throw new Exception("Incorrect List Item Type or null data");
        }
    }

    /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$GuildListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;)V", "Lcom/discord/databinding/ViewSettingsBoostedGuildListitemBinding;", "binding", "Lcom/discord/databinding/ViewSettingsBoostedGuildListitemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GuildListItem extends MGRecyclerViewHolder<WidgetSettingsGuildBoostSubscriptionAdapter, Item> {
        private final ViewSettingsBoostedGuildListitemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public GuildListItem(WidgetSettingsGuildBoostSubscriptionAdapter widgetSettingsGuildBoostSubscriptionAdapter) {
            super((int) R.layout.view_settings_boosted_guild_listitem, widgetSettingsGuildBoostSubscriptionAdapter);
            m.checkNotNullParameter(widgetSettingsGuildBoostSubscriptionAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.boosted_guild_banner;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.boosted_guild_banner);
            if (simpleDraweeView != null) {
                i = R.id.boosted_guild_gradient;
                View findViewById = view.findViewById(R.id.boosted_guild_gradient);
                if (findViewById != null) {
                    i = R.id.boosted_guild_progress_view;
                    GuildBoostProgressView guildBoostProgressView = (GuildBoostProgressView) view.findViewById(R.id.boosted_guild_progress_view);
                    if (guildBoostProgressView != null) {
                        i = R.id.boosted_sample_guild_count;
                        TextView textView = (TextView) view.findViewById(R.id.boosted_sample_guild_count);
                        if (textView != null) {
                            i = R.id.boosted_sample_guild_icon;
                            SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) view.findViewById(R.id.boosted_sample_guild_icon);
                            if (simpleDraweeView2 != null) {
                                i = R.id.boosted_sample_guild_name;
                                TextView textView2 = (TextView) view.findViewById(R.id.boosted_sample_guild_name);
                                if (textView2 != null) {
                                    ViewSettingsBoostedGuildListitemBinding viewSettingsBoostedGuildListitemBinding = new ViewSettingsBoostedGuildListitemBinding((LinearLayout) view, simpleDraweeView, findViewById, guildBoostProgressView, textView, simpleDraweeView2, textView2);
                                    m.checkNotNullExpressionValue(viewSettingsBoostedGuildListitemBinding, "ViewSettingsBoostedGuild…temBinding.bind(itemView)");
                                    this.binding = viewSettingsBoostedGuildListitemBinding;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public void onConfigure(int i, Item item) {
            CharSequence b2;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            if (item instanceof Item.GuildItem) {
                Item.GuildItem guildItem = (Item.GuildItem) item;
                if (guildItem.getGuild() != null) {
                    View view = this.itemView;
                    m.checkNotNullExpressionValue(view, "itemView");
                    Context context = view.getContext();
                    if (guildItem.getGuild().getBanner() != null) {
                        SimpleDraweeView simpleDraweeView = this.binding.f2196b;
                        IconUtils iconUtils = IconUtils.INSTANCE;
                        Guild guild = guildItem.getGuild();
                        m.checkNotNullExpressionValue(context, "context");
                        simpleDraweeView.setImageURI(IconUtils.getBannerForGuild$default(iconUtils, guild, Integer.valueOf(context.getResources().getDimensionPixelSize(R.dimen.nav_panel_width)), false, 4, null));
                        View view2 = this.binding.c;
                        m.checkNotNullExpressionValue(view2, "binding.boostedGuildGradient");
                        view2.setVisibility(0);
                    } else {
                        SimpleDraweeView simpleDraweeView2 = this.binding.f2196b;
                        m.checkNotNullExpressionValue(context, "context");
                        simpleDraweeView2.setBackgroundResource(DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.bg_subscription_placeholder_pattern, 0, 2, (Object) null));
                        View view3 = this.binding.c;
                        m.checkNotNullExpressionValue(view3, "binding.boostedGuildGradient");
                        view3.setVisibility(8);
                    }
                    SimpleDraweeView simpleDraweeView3 = this.binding.f;
                    m.checkNotNullExpressionValue(simpleDraweeView3, "binding.boostedSampleGuildIcon");
                    IconUtils.setIcon$default(simpleDraweeView3, IconUtils.getForGuild$default(guildItem.getGuild(), null, false, null, 14, null), 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
                    TextView textView = this.binding.g;
                    m.checkNotNullExpressionValue(textView, "binding.boostedSampleGuildName");
                    textView.setText(guildItem.getGuild().getName());
                    CharSequence i18nPluralString = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.premium_guild_subscription_guild_subsription_subtitle_numSubscriptions, guildItem.getBoostCount(), Integer.valueOf(guildItem.getBoostCount()));
                    TextView textView2 = this.binding.e;
                    m.checkNotNullExpressionValue(textView2, "binding.boostedSampleGuildCount");
                    b2 = b.b(context, R.string.premium_guild_subscription_guild_subsription_subtitle, new Object[]{i18nPluralString}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    textView2.setText(b2);
                    this.binding.d.a(guildItem.getGuild().getPremiumTier(), guildItem.getGuild().getPremiumSubscriptionCount());
                    return;
                }
            }
            throw new Exception("Incorrect List Item Type or null data");
        }
    }

    /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$HeaderListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;)V", "Lcom/discord/databinding/ViewSettingsBoostedHeaderListitemBinding;", "binding", "Lcom/discord/databinding/ViewSettingsBoostedHeaderListitemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class HeaderListItem extends MGRecyclerViewHolder<WidgetSettingsGuildBoostSubscriptionAdapter, Item> {
        private final ViewSettingsBoostedHeaderListitemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public HeaderListItem(WidgetSettingsGuildBoostSubscriptionAdapter widgetSettingsGuildBoostSubscriptionAdapter) {
            super((int) R.layout.view_settings_boosted_header_listitem, widgetSettingsGuildBoostSubscriptionAdapter);
            m.checkNotNullParameter(widgetSettingsGuildBoostSubscriptionAdapter, "adapter");
            View view = this.itemView;
            Objects.requireNonNull(view, "rootView");
            TextView textView = (TextView) view;
            ViewSettingsBoostedHeaderListitemBinding viewSettingsBoostedHeaderListitemBinding = new ViewSettingsBoostedHeaderListitemBinding(textView, textView);
            m.checkNotNullExpressionValue(viewSettingsBoostedHeaderListitemBinding, "ViewSettingsBoostedHeade…temBinding.bind(itemView)");
            this.binding = viewSettingsBoostedHeaderListitemBinding;
        }

        public void onConfigure(int i, Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            if (item instanceof Item.HeaderItem) {
                TextView textView = this.binding.f2197b;
                m.checkNotNullExpressionValue(textView, "binding.boostedHeader");
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                textView.setText(view.getContext().getString(((Item.HeaderItem) item).getHeaderStringId()));
                return;
            }
            throw new Exception("Incorrect List Item Type or null data");
        }
    }

    /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\b\t\n¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", "Companion", "GuildBoostItem", "GuildItem", "HeaderItem", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildItem;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildBoostItem;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$HeaderItem;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item implements MGRecyclerDataPayload {
        public static final Companion Companion = new Companion(null);
        public static final int TYPE_GUILD = 0;
        public static final int TYPE_GUILD_BOOST = 1;
        public static final int TYPE_HEADER = 2;

        /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$Companion;", "", "", "TYPE_GUILD", "I", "TYPE_GUILD_BOOST", "TYPE_HEADER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0015\u001a\u00020\r8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u000fR\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001b\u0010\u0007R\u001c\u0010\u001c\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010\u001a\u001a\u0004\b\u001d\u0010\u0007¨\u0006 "}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildBoostItem;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "component1", "()Lcom/discord/models/domain/ModelGuildBoostSlot;", "", "component2", "()Ljava/lang/String;", "boostSlot", "boostEndsAt", "copy", "(Lcom/discord/models/domain/ModelGuildBoostSlot;Ljava/lang/String;)Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildBoostItem;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "getBoostSlot", "Ljava/lang/String;", "getBoostEndsAt", "key", "getKey", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGuildBoostSlot;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class GuildBoostItem extends Item {
            private final String boostEndsAt;
            private final ModelGuildBoostSlot boostSlot;
            private final String key;
            private final int type = 1;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public GuildBoostItem(ModelGuildBoostSlot modelGuildBoostSlot, String str) {
                super(null);
                m.checkNotNullParameter(modelGuildBoostSlot, "boostSlot");
                this.boostSlot = modelGuildBoostSlot;
                this.boostEndsAt = str;
                this.key = String.valueOf(modelGuildBoostSlot.getId());
            }

            public static /* synthetic */ GuildBoostItem copy$default(GuildBoostItem guildBoostItem, ModelGuildBoostSlot modelGuildBoostSlot, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGuildBoostSlot = guildBoostItem.boostSlot;
                }
                if ((i & 2) != 0) {
                    str = guildBoostItem.boostEndsAt;
                }
                return guildBoostItem.copy(modelGuildBoostSlot, str);
            }

            public final ModelGuildBoostSlot component1() {
                return this.boostSlot;
            }

            public final String component2() {
                return this.boostEndsAt;
            }

            public final GuildBoostItem copy(ModelGuildBoostSlot modelGuildBoostSlot, String str) {
                m.checkNotNullParameter(modelGuildBoostSlot, "boostSlot");
                return new GuildBoostItem(modelGuildBoostSlot, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof GuildBoostItem)) {
                    return false;
                }
                GuildBoostItem guildBoostItem = (GuildBoostItem) obj;
                return m.areEqual(this.boostSlot, guildBoostItem.boostSlot) && m.areEqual(this.boostEndsAt, guildBoostItem.boostEndsAt);
            }

            public final String getBoostEndsAt() {
                return this.boostEndsAt;
            }

            public final ModelGuildBoostSlot getBoostSlot() {
                return this.boostSlot;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                ModelGuildBoostSlot modelGuildBoostSlot = this.boostSlot;
                int i = 0;
                int hashCode = (modelGuildBoostSlot != null ? modelGuildBoostSlot.hashCode() : 0) * 31;
                String str = this.boostEndsAt;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("GuildBoostItem(boostSlot=");
                R.append(this.boostSlot);
                R.append(", boostEndsAt=");
                return a.H(R, this.boostEndsAt, ")");
            }
        }

        /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001e\u0010\u001fJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0007J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0015\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u000eR\u001c\u0010\u0018\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001b\u0010\u0007R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004¨\u0006 "}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildItem;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "component2", "()I", "guild", "boostCount", "copy", "(Lcom/discord/models/guild/Guild;I)Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$GuildItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "type", "I", "getType", "getBoostCount", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class GuildItem extends Item {
            private final int boostCount;
            private final Guild guild;
            private final String key;
            private final int type;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public GuildItem(Guild guild, int i) {
                super(null);
                Long l = null;
                this.guild = guild;
                this.boostCount = i;
                this.key = String.valueOf(guild != null ? Long.valueOf(guild.getId()) : l);
            }

            public static /* synthetic */ GuildItem copy$default(GuildItem guildItem, Guild guild, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    guild = guildItem.guild;
                }
                if ((i2 & 2) != 0) {
                    i = guildItem.boostCount;
                }
                return guildItem.copy(guild, i);
            }

            public final Guild component1() {
                return this.guild;
            }

            public final int component2() {
                return this.boostCount;
            }

            public final GuildItem copy(Guild guild, int i) {
                return new GuildItem(guild, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof GuildItem)) {
                    return false;
                }
                GuildItem guildItem = (GuildItem) obj;
                return m.areEqual(this.guild, guildItem.guild) && this.boostCount == guildItem.boostCount;
            }

            public final int getBoostCount() {
                return this.boostCount;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                Guild guild = this.guild;
                return ((guild != null ? guild.hashCode() : 0) * 31) + this.boostCount;
            }

            public String toString() {
                StringBuilder R = a.R("GuildItem(guild=");
                R.append(this.guild);
                R.append(", boostCount=");
                return a.A(R, this.boostCount, ")");
            }
        }

        /* compiled from: WidgetSettingsGuildBoostSubscriptionAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\nR\u001c\u0010\u0014\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0015\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$HeaderItem;", "Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item;", "", "component1", "()I", "headerStringId", "copy", "(I)Lcom/discord/widgets/settings/guildboost/WidgetSettingsGuildBoostSubscriptionAdapter$Item$HeaderItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "type", "I", "getType", "getHeaderStringId", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class HeaderItem extends Item {
            private final int headerStringId;
            private final String key;
            private final int type = 2;

            public HeaderItem(@StringRes int i) {
                super(null);
                this.headerStringId = i;
                this.key = String.valueOf(i);
            }

            public static /* synthetic */ HeaderItem copy$default(HeaderItem headerItem, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = headerItem.headerStringId;
                }
                return headerItem.copy(i);
            }

            public final int component1() {
                return this.headerStringId;
            }

            public final HeaderItem copy(@StringRes int i) {
                return new HeaderItem(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof HeaderItem) && this.headerStringId == ((HeaderItem) obj).headerStringId;
                }
                return true;
            }

            public final int getHeaderStringId() {
                return this.headerStringId;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                return this.headerStringId;
            }

            public String toString() {
                return a.A(a.R("HeaderItem(headerStringId="), this.headerStringId, ")");
            }
        }

        private Item() {
        }

        public /* synthetic */ Item(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsGuildBoostSubscriptionAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
    }

    public final void configure(List<? extends Item> list, Function1<? super Long, Unit> function1, Function2<? super ModelGuildBoostSlot, ? super Long, Unit> function2, Function2<? super Long, ? super Boolean, Unit> function22, boolean z2, boolean z3) {
        m.checkNotNullParameter(list, "guildBoostItems");
        m.checkNotNullParameter(function1, "subscribeListener");
        m.checkNotNullParameter(function2, "transferListener");
        m.checkNotNullParameter(function22, "cancelListener");
        setData(list);
        this.subscribeListener = function1;
        this.transferListener = function2;
        this.cancelListener = function22;
        this.canCancelBoosts = z2;
        this.canUncancelBoosts = z3;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<WidgetSettingsGuildBoostSubscriptionAdapter, Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new GuildListItem(this);
        }
        if (i == 1) {
            return new GuildBoostListItem(this);
        }
        if (i == 2) {
            return new HeaderListItem(this);
        }
        throw invalidViewTypeException(i);
    }
}
