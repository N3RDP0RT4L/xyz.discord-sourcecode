package com.discord.widgets.settings.guildboost;

import com.discord.widgets.settings.guildboost.SettingsGuildBoostViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsGuildBoost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState;", "kotlin.jvm.PlatformType", "model", "", "invoke", "(Lcom/discord/widgets/settings/guildboost/SettingsGuildBoostViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGuildBoost$onViewBoundOrOnResume$1 extends o implements Function1<SettingsGuildBoostViewModel.ViewState, Unit> {
    public final /* synthetic */ WidgetSettingsGuildBoost this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsGuildBoost$onViewBoundOrOnResume$1(WidgetSettingsGuildBoost widgetSettingsGuildBoost) {
        super(1);
        this.this$0 = widgetSettingsGuildBoost;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(SettingsGuildBoostViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(SettingsGuildBoostViewModel.ViewState viewState) {
        if (viewState instanceof SettingsGuildBoostViewModel.ViewState.Loaded) {
            this.this$0.showContent((SettingsGuildBoostViewModel.ViewState.Loaded) viewState);
        } else if (viewState instanceof SettingsGuildBoostViewModel.ViewState.Loading) {
            this.this$0.showLoadingUI();
        } else if (viewState instanceof SettingsGuildBoostViewModel.ViewState.Failure) {
            this.this$0.showFailureUI();
        }
    }
}
