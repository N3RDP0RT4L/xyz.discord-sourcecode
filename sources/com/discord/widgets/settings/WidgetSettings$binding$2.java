package com.discord.widgets.settings;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetSettingsBinding;
import com.discord.views.StatusView;
import com.discord.widgets.user.profile.UserProfileHeaderView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSettingsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettings$binding$2 extends k implements Function1<View, WidgetSettingsBinding> {
    public static final WidgetSettings$binding$2 INSTANCE = new WidgetSettings$binding$2();

    public WidgetSettings$binding$2() {
        super(1, WidgetSettingsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsBinding;", 0);
    }

    public final WidgetSettingsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.accessibility;
        TextView textView = (TextView) view.findViewById(R.id.accessibility);
        if (textView != null) {
            i = R.id.account;
            TextView textView2 = (TextView) view.findViewById(R.id.account);
            if (textView2 != null) {
                i = R.id.acknowledgements;
                TextView textView3 = (TextView) view.findViewById(R.id.acknowledgements);
                if (textView3 != null) {
                    i = R.id.activity_status;
                    TextView textView4 = (TextView) view.findViewById(R.id.activity_status);
                    if (textView4 != null) {
                        i = R.id.app_info_header;
                        TextView textView5 = (TextView) view.findViewById(R.id.app_info_header);
                        if (textView5 != null) {
                            i = R.id.app_settings_header;
                            TextView textView6 = (TextView) view.findViewById(R.id.app_settings_header);
                            if (textView6 != null) {
                                i = R.id.appearance;
                                TextView textView7 = (TextView) view.findViewById(R.id.appearance);
                                if (textView7 != null) {
                                    i = R.id.authorized_apps;
                                    TextView textView8 = (TextView) view.findViewById(R.id.authorized_apps);
                                    if (textView8 != null) {
                                        i = R.id.behavior;
                                        TextView textView9 = (TextView) view.findViewById(R.id.behavior);
                                        if (textView9 != null) {
                                            i = R.id.changelog;
                                            TextView textView10 = (TextView) view.findViewById(R.id.changelog);
                                            if (textView10 != null) {
                                                i = R.id.connections;
                                                TextView textView11 = (TextView) view.findViewById(R.id.connections);
                                                if (textView11 != null) {
                                                    i = R.id.developer_options;
                                                    TextView textView12 = (TextView) view.findViewById(R.id.developer_options);
                                                    if (textView12 != null) {
                                                        i = R.id.developer_options_divider;
                                                        View findViewById = view.findViewById(R.id.developer_options_divider);
                                                        if (findViewById != null) {
                                                            i = R.id.developer_options_header;
                                                            TextView textView13 = (TextView) view.findViewById(R.id.developer_options_header);
                                                            if (textView13 != null) {
                                                                i = R.id.language;
                                                                TextView textView14 = (TextView) view.findViewById(R.id.language);
                                                                if (textView14 != null) {
                                                                    i = R.id.nitro_boosting;
                                                                    TextView textView15 = (TextView) view.findViewById(R.id.nitro_boosting);
                                                                    if (textView15 != null) {
                                                                        i = R.id.nitro_gifting;
                                                                        TextView textView16 = (TextView) view.findViewById(R.id.nitro_gifting);
                                                                        if (textView16 != null) {
                                                                            i = R.id.nitroGiftingBadge;
                                                                            TextView textView17 = (TextView) view.findViewById(R.id.nitroGiftingBadge);
                                                                            if (textView17 != null) {
                                                                                i = R.id.nitroGiftingContainer;
                                                                                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.nitroGiftingContainer);
                                                                                if (linearLayout != null) {
                                                                                    i = R.id.nitro_header;
                                                                                    TextView textView18 = (TextView) view.findViewById(R.id.nitro_header);
                                                                                    if (textView18 != null) {
                                                                                        i = R.id.nitro_settings_container;
                                                                                        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.nitro_settings_container);
                                                                                        if (linearLayout2 != null) {
                                                                                            i = R.id.notification_upsell;
                                                                                            LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.notification_upsell);
                                                                                            if (linearLayout3 != null) {
                                                                                                i = R.id.notification_upsell_dismiss;
                                                                                                AppCompatImageView appCompatImageView = (AppCompatImageView) view.findViewById(R.id.notification_upsell_dismiss);
                                                                                                if (appCompatImageView != null) {
                                                                                                    i = R.id.notification_upsell_enable;
                                                                                                    MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.notification_upsell_enable);
                                                                                                    if (materialButton != null) {
                                                                                                        i = R.id.notifications;
                                                                                                        TextView textView19 = (TextView) view.findViewById(R.id.notifications);
                                                                                                        if (textView19 != null) {
                                                                                                            i = R.id.privacy;
                                                                                                            TextView textView20 = (TextView) view.findViewById(R.id.privacy);
                                                                                                            if (textView20 != null) {
                                                                                                                i = R.id.profile;
                                                                                                                TextView textView21 = (TextView) view.findViewById(R.id.profile);
                                                                                                                if (textView21 != null) {
                                                                                                                    i = R.id.qr_scanner;
                                                                                                                    TextView textView22 = (TextView) view.findViewById(R.id.qr_scanner);
                                                                                                                    if (textView22 != null) {
                                                                                                                        i = R.id.set_status_container;
                                                                                                                        LinearLayout linearLayout4 = (LinearLayout) view.findViewById(R.id.set_status_container);
                                                                                                                        if (linearLayout4 != null) {
                                                                                                                            i = R.id.settings_billing;
                                                                                                                            TextView textView23 = (TextView) view.findViewById(R.id.settings_billing);
                                                                                                                            if (textView23 != null) {
                                                                                                                                i = R.id.settings_nitro;
                                                                                                                                TextView textView24 = (TextView) view.findViewById(R.id.settings_nitro);
                                                                                                                                if (textView24 != null) {
                                                                                                                                    i = R.id.settings_presence_text;
                                                                                                                                    TextView textView25 = (TextView) view.findViewById(R.id.settings_presence_text);
                                                                                                                                    if (textView25 != null) {
                                                                                                                                        i = R.id.settings_status_view;
                                                                                                                                        StatusView statusView = (StatusView) view.findViewById(R.id.settings_status_view);
                                                                                                                                        if (statusView != null) {
                                                                                                                                            i = R.id.support;
                                                                                                                                            TextView textView26 = (TextView) view.findViewById(R.id.support);
                                                                                                                                            if (textView26 != null) {
                                                                                                                                                i = R.id.text_images_settings;
                                                                                                                                                TextView textView27 = (TextView) view.findViewById(R.id.text_images_settings);
                                                                                                                                                if (textView27 != null) {
                                                                                                                                                    i = R.id.upload_debug_logs;
                                                                                                                                                    TextView textView28 = (TextView) view.findViewById(R.id.upload_debug_logs);
                                                                                                                                                    if (textView28 != null) {
                                                                                                                                                        i = R.id.user_settings_header;
                                                                                                                                                        TextView textView29 = (TextView) view.findViewById(R.id.user_settings_header);
                                                                                                                                                        if (textView29 != null) {
                                                                                                                                                            i = R.id.user_settings_profile_header_view;
                                                                                                                                                            UserProfileHeaderView userProfileHeaderView = (UserProfileHeaderView) view.findViewById(R.id.user_settings_profile_header_view);
                                                                                                                                                            if (userProfileHeaderView != null) {
                                                                                                                                                                i = R.id.voice;
                                                                                                                                                                TextView textView30 = (TextView) view.findViewById(R.id.voice);
                                                                                                                                                                if (textView30 != null) {
                                                                                                                                                                    return new WidgetSettingsBinding((CoordinatorLayout) view, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9, textView10, textView11, textView12, findViewById, textView13, textView14, textView15, textView16, textView17, linearLayout, textView18, linearLayout2, linearLayout3, appCompatImageView, materialButton, textView19, textView20, textView21, textView22, linearLayout4, textView23, textView24, textView25, statusView, textView26, textView27, textView28, textView29, userProfileHeaderView, textView30);
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
