package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetMuteSettingsSheetBinding;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.settings.WidgetChannelNotificationSettings;
import com.discord.widgets.settings.MuteSettingsSheetViewModel;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetMuteSettingsSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 /2\u00020\u0001:\u0001/B\u0007¢\u0006\u0004\b-\u0010.J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001b\u0010\r\u001a\u00020\n2\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000f\u0010\tJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J!\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00172\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 R\u001d\u0010&\u001a\u00020!8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u001d\u0010,\u001a\u00020'8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+¨\u00060"}, d2 = {"Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;", "viewState", "", "updateViews", "(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;)V", "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;", "configureUnmuteButton", "(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;)V", "", "Lcom/discord/primitives/UtcTimestamp;", "muteEndTime", "parseMuteEndtime", "(Ljava/lang/String;)Ljava/lang/String;", "configureNotificationSettings", "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;)V", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;", "viewModel", "Lcom/discord/databinding/WidgetMuteSettingsSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetMuteSettingsSheetBinding;", "binding", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMuteSettingsSheet extends AppBottomSheet {
    private static final float ACTIVE_OPACITY = 1.0f;
    private static final String ARG_CHANNEL_ID = "ARG_CHANNEL_ID";
    private static final String ARG_GUILD_ID = "ARG_GUILD_ID";
    private static final float INACTIVE_OPACITY = 0.2f;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetMuteSettingsSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetMuteSettingsSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetMuteSettingsSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetMuteSettingsSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\b\u0010\tJ!\u0010\f\u001a\u00020\u00072\n\u0010\u000b\u001a\u00060\u0002j\u0002`\n2\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\f\u0010\tR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\u00108\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u000f¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "showForChannel", "(JLandroidx/fragment/app/FragmentManager;)V", "Lcom/discord/primitives/GuildId;", "guildId", "showForGuild", "", "ACTIVE_OPACITY", "F", "", WidgetMuteSettingsSheet.ARG_CHANNEL_ID, "Ljava/lang/String;", WidgetMuteSettingsSheet.ARG_GUILD_ID, "INACTIVE_OPACITY", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void showForChannel(long j, FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetMuteSettingsSheet widgetMuteSettingsSheet = new WidgetMuteSettingsSheet();
            Bundle bundle = new Bundle();
            bundle.putLong(WidgetMuteSettingsSheet.ARG_CHANNEL_ID, j);
            widgetMuteSettingsSheet.setArguments(bundle);
            widgetMuteSettingsSheet.show(fragmentManager, WidgetMuteSettingsSheet.class.getName());
        }

        public final void showForGuild(long j, FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetMuteSettingsSheet widgetMuteSettingsSheet = new WidgetMuteSettingsSheet();
            Bundle bundle = new Bundle();
            bundle.putLong(WidgetMuteSettingsSheet.ARG_GUILD_ID, j);
            widgetMuteSettingsSheet.setArguments(bundle);
            widgetMuteSettingsSheet.show(fragmentManager, WidgetMuteSettingsSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            MuteSettingsSheetViewModel.SettingsType.values();
            int[] iArr = new int[7];
            $EnumSwitchMapping$0 = iArr;
            MuteSettingsSheetViewModel.SettingsType settingsType = MuteSettingsSheetViewModel.SettingsType.GUILD;
            iArr[settingsType.ordinal()] = 1;
            MuteSettingsSheetViewModel.SettingsType settingsType2 = MuteSettingsSheetViewModel.SettingsType.DM;
            iArr[settingsType2.ordinal()] = 2;
            MuteSettingsSheetViewModel.SettingsType settingsType3 = MuteSettingsSheetViewModel.SettingsType.GROUP_DM;
            iArr[settingsType3.ordinal()] = 3;
            iArr[MuteSettingsSheetViewModel.SettingsType.GUILD_CHANNEL.ordinal()] = 4;
            iArr[MuteSettingsSheetViewModel.SettingsType.THREAD.ordinal()] = 5;
            MuteSettingsSheetViewModel.SettingsType settingsType4 = MuteSettingsSheetViewModel.SettingsType.CATEGORY;
            iArr[settingsType4.ordinal()] = 6;
            MuteSettingsSheetViewModel.SettingsType.values();
            int[] iArr2 = new int[7];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[settingsType3.ordinal()] = 1;
            iArr2[settingsType2.ordinal()] = 2;
            MuteSettingsSheetViewModel.SettingsType.values();
            int[] iArr3 = new int[7];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[settingsType2.ordinal()] = 1;
            iArr3[settingsType3.ordinal()] = 2;
            iArr3[settingsType.ordinal()] = 3;
            iArr3[settingsType4.ordinal()] = 4;
        }
    }

    public WidgetMuteSettingsSheet() {
        super(false, 1, null);
        WidgetMuteSettingsSheet$viewModel$2 widgetMuteSettingsSheet$viewModel$2 = new WidgetMuteSettingsSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(MuteSettingsSheetViewModel.class), new WidgetMuteSettingsSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetMuteSettingsSheet$viewModel$2));
    }

    private final void configureNotificationSettings(MuteSettingsSheetViewModel.ViewState.Loaded loaded) {
        CharSequence charSequence;
        CharSequence charSequence2;
        TextView textView = getBinding().g;
        m.checkNotNullExpressionValue(textView, "binding.notificationSettingsOverridesLabel");
        int notificationSetting = loaded.getNotificationSetting();
        if (notificationSetting == ModelNotificationSettings.FREQUENCY_ALL) {
            charSequence = b.e(this, R.string.form_label_all_messages_short, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        } else if (notificationSetting == ModelNotificationSettings.FREQUENCY_MENTIONS) {
            charSequence = b.e(this, R.string.form_label_only_mentions_short, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        } else if (notificationSetting == ModelNotificationSettings.FREQUENCY_NOTHING) {
            charSequence = b.e(this, R.string.form_label_nothing, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        } else {
            charSequence = "";
        }
        textView.setText(charSequence);
        int ordinal = loaded.getSettingsType().ordinal();
        if (ordinal != 0) {
            boolean z2 = true;
            if (!(ordinal == 1 || ordinal == 2 || ordinal == 5)) {
                if (!loaded.isChannelMuted() && !loaded.isGuildMuted()) {
                    z2 = false;
                }
                if (z2) {
                    TextView textView2 = getBinding().f;
                    m.checkNotNullExpressionValue(textView2, "binding.notificationSettingsLabel");
                    textView2.setAlpha(0.2f);
                    getBinding().d.setOnClickListener(null);
                    TextView textView3 = getBinding().f2475b;
                    m.checkNotNullExpressionValue(textView3, "binding.channelMutedDetails");
                    textView3.setVisibility(0);
                    TextView textView4 = getBinding().f2475b;
                    m.checkNotNullExpressionValue(textView4, "binding.channelMutedDetails");
                    if (loaded.isChannelMuted()) {
                        charSequence2 = b.e(this, R.string.form_description_mobile_notification_muted, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                    } else {
                        charSequence2 = b.e(this, R.string.form_label_mobile_channel_override_guild_muted, new Object[0], new WidgetMuteSettingsSheet$configureNotificationSettings$1(this));
                    }
                    textView4.setText(charSequence2);
                } else {
                    TextView textView5 = getBinding().f;
                    m.checkNotNullExpressionValue(textView5, "binding.notificationSettingsLabel");
                    textView5.setAlpha(1.0f);
                    getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetMuteSettingsSheet$configureNotificationSettings$2
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            MuteSettingsSheetViewModel viewModel;
                            viewModel = WidgetMuteSettingsSheet.this.getViewModel();
                            viewModel.onChannelSettingsSelected();
                        }
                    });
                    TextView textView6 = getBinding().f2475b;
                    m.checkNotNullExpressionValue(textView6, "binding.channelMutedDetails");
                    textView6.setVisibility(8);
                }
                FrameLayout frameLayout = getBinding().e;
                m.checkNotNullExpressionValue(frameLayout, "binding.notificationSettingsButtonContainer");
                frameLayout.setVisibility(0);
                return;
            }
        }
        FrameLayout frameLayout2 = getBinding().e;
        m.checkNotNullExpressionValue(frameLayout2, "binding.notificationSettingsButtonContainer");
        frameLayout2.setVisibility(8);
        TextView textView7 = getBinding().f2475b;
        m.checkNotNullExpressionValue(textView7, "binding.channelMutedDetails");
        textView7.setVisibility(8);
    }

    private final void configureUnmuteButton(MuteSettingsSheetViewModel.ViewState.Loaded loaded) {
        CharSequence charSequence;
        WidgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1 widgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1 = new WidgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1(this);
        if (loaded.isChannelMuted()) {
            TextView textView = getBinding().q;
            m.checkNotNullExpressionValue(textView, "binding.unmuteButtonLabel");
            b.m(textView, R.string.unmute_channel, new Object[]{loaded.getSubtitle()}, widgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1);
            TextView textView2 = getBinding().p;
            m.checkNotNullExpressionValue(textView2, "binding.unmuteButtonDetailsLabel");
            int ordinal = loaded.getSettingsType().ordinal();
            if (ordinal == 1 || ordinal == 2) {
                String muteEndTime = loaded.getMuteEndTime();
                if (muteEndTime == null) {
                    charSequence = b.e(this, R.string.form_label_mobile_dm_muted, new Object[0], widgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1);
                } else {
                    charSequence = b.e(this, R.string.form_label_mobile_dm_muted_until, new Object[]{parseMuteEndtime(muteEndTime)}, widgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1);
                }
            } else {
                String muteEndTime2 = loaded.getMuteEndTime();
                if (muteEndTime2 == null) {
                    charSequence = b.e(this, R.string.form_label_mobile_channel_muted, new Object[0], widgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1);
                } else {
                    charSequence = b.e(this, R.string.form_label_mobile_channel_muted_until, new Object[]{parseMuteEndtime(muteEndTime2)}, widgetMuteSettingsSheet$configureUnmuteButton$boldRenderContext$1);
                }
            }
            textView2.setText(charSequence);
            LinearLayout linearLayout = getBinding().o;
            m.checkNotNullExpressionValue(linearLayout, "binding.unmuteButton");
            linearLayout.setVisibility(0);
            LinearLayout linearLayout2 = getBinding().c;
            m.checkNotNullExpressionValue(linearLayout2, "binding.muteSettingsSheetMuteOptions");
            linearLayout2.setVisibility(8);
            return;
        }
        LinearLayout linearLayout3 = getBinding().o;
        m.checkNotNullExpressionValue(linearLayout3, "binding.unmuteButton");
        linearLayout3.setVisibility(8);
        LinearLayout linearLayout4 = getBinding().c;
        m.checkNotNullExpressionValue(linearLayout4, "binding.muteSettingsSheetMuteOptions");
        linearLayout4.setVisibility(0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetMuteSettingsSheetBinding getBinding() {
        return (WidgetMuteSettingsSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final MuteSettingsSheetViewModel getViewModel() {
        return (MuteSettingsSheetViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(MuteSettingsSheetViewModel.Event event) {
        if (event instanceof MuteSettingsSheetViewModel.Event.Dismiss) {
            dismiss();
        } else if (event instanceof MuteSettingsSheetViewModel.Event.NavigateToChannelSettings) {
            WidgetChannelNotificationSettings.Companion companion = WidgetChannelNotificationSettings.Companion;
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            WidgetChannelNotificationSettings.Companion.launch$default(companion, requireContext, ((MuteSettingsSheetViewModel.Event.NavigateToChannelSettings) event).getChannelId(), false, 4, null);
        }
    }

    private final String parseMuteEndtime(String str) {
        TimeUtils timeUtils = TimeUtils.INSTANCE;
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        return TimeUtils.renderUtcDateTime$default(timeUtils, str, requireContext, null, 3, 3, 4, null);
    }

    public static final void showForChannel(long j, FragmentManager fragmentManager) {
        Companion.showForChannel(j, fragmentManager);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateViews(MuteSettingsSheetViewModel.ViewState viewState) {
        Integer num;
        if (viewState instanceof MuteSettingsSheetViewModel.ViewState.Loaded) {
            MuteSettingsSheetViewModel.ViewState.Loaded loaded = (MuteSettingsSheetViewModel.ViewState.Loaded) viewState;
            int ordinal = loaded.getSettingsType().ordinal();
            CharSequence charSequence = null;
            if (ordinal == 0) {
                num = Integer.valueOf((int) R.string.mute_settings_mute_server);
            } else if (ordinal == 1 || ordinal == 2) {
                num = Integer.valueOf((int) R.string.mute_settings_mute_this_conversation);
            } else if (ordinal == 3) {
                num = Integer.valueOf((int) R.string.mute_settings_mute_thread);
            } else if (ordinal != 4) {
                num = ordinal != 5 ? null : Integer.valueOf((int) R.string.mute_settings_mute_category);
            } else {
                num = Integer.valueOf((int) R.string.mute_settings_mute_channel);
            }
            TextView textView = getBinding().n;
            m.checkNotNullExpressionValue(textView, "binding.title");
            if (num != null) {
                charSequence = b.e(this, num.intValue(), new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            }
            textView.setText(charSequence);
            TextView textView2 = getBinding().m;
            m.checkNotNullExpressionValue(textView2, "binding.subtitle");
            textView2.setText(loaded.getSubtitle());
            configureUnmuteButton(loaded);
            configureNotificationSettings(loaded);
        } else if (viewState instanceof MuteSettingsSheetViewModel.ViewState.Failure) {
            b.a.d.m.i(this, R.string.default_failure_to_perform_action_message, 0, 4);
            dismiss();
        }
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        super.bindSubscriptions(compositeSubscription);
        getBinding().o.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetMuteSettingsSheet$bindSubscriptions$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MuteSettingsSheetViewModel viewModel;
                viewModel = WidgetMuteSettingsSheet.this.getViewModel();
                m.checkNotNullExpressionValue(view, "it");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                viewModel.unmute(context);
            }
        });
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetMuteSettingsSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMuteSettingsSheet$bindSubscriptions$2(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetMuteSettingsSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMuteSettingsSheet$bindSubscriptions$3(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_mute_settings_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetMuteSettingsSheet$onViewCreated$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                MuteSettingsSheetViewModel viewModel;
                viewModel = WidgetMuteSettingsSheet.this.getViewModel();
                m.checkNotNullExpressionValue(view2, "it");
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                viewModel.selectMuteDurationMs(900000L, context);
            }
        });
        getBinding().k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetMuteSettingsSheet$onViewCreated$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                MuteSettingsSheetViewModel viewModel;
                viewModel = WidgetMuteSettingsSheet.this.getViewModel();
                m.checkNotNullExpressionValue(view2, "it");
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                viewModel.selectMuteDurationMs(3600000L, context);
            }
        });
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetMuteSettingsSheet$onViewCreated$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                MuteSettingsSheetViewModel viewModel;
                viewModel = WidgetMuteSettingsSheet.this.getViewModel();
                m.checkNotNullExpressionValue(view2, "it");
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                viewModel.selectMuteDurationMs(28800000L, context);
            }
        });
        getBinding().l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetMuteSettingsSheet$onViewCreated$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                MuteSettingsSheetViewModel viewModel;
                viewModel = WidgetMuteSettingsSheet.this.getViewModel();
                m.checkNotNullExpressionValue(view2, "it");
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                viewModel.selectMuteDurationMs(86400000L, context);
            }
        });
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetMuteSettingsSheet$onViewCreated$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                MuteSettingsSheetViewModel viewModel;
                viewModel = WidgetMuteSettingsSheet.this.getViewModel();
                m.checkNotNullExpressionValue(view2, "it");
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                viewModel.selectMuteDurationMs(0L, context);
            }
        });
    }
}
