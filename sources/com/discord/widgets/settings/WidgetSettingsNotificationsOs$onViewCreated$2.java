package com.discord.widgets.settings;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreNotifications;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Action1;
/* compiled from: WidgetSettingsNotificationsOs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsNotificationsOs$onViewCreated$2<T> implements Action1<Boolean> {
    public static final WidgetSettingsNotificationsOs$onViewCreated$2 INSTANCE = new WidgetSettingsNotificationsOs$onViewCreated$2();

    public final void call(Boolean bool) {
        StoreNotifications notifications = StoreStream.Companion.getNotifications();
        m.checkNotNullExpressionValue(bool, "it");
        StoreNotifications.setEnabledInApp$default(notifications, bool.booleanValue(), false, 2, null);
    }
}
