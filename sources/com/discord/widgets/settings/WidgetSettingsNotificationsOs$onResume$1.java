package com.discord.widgets.settings;

import com.discord.databinding.WidgetSettingsNotificationOsBinding;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.views.CheckedSetting;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsNotificationsOs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "settings", "", "invoke", "(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsNotificationsOs$onResume$1 extends o implements Function1<NotificationClient.SettingsV2, Unit> {
    public final /* synthetic */ WidgetSettingsNotificationsOs this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsNotificationsOs$onResume$1(WidgetSettingsNotificationsOs widgetSettingsNotificationsOs) {
        super(1);
        this.this$0 = widgetSettingsNotificationsOs;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(NotificationClient.SettingsV2 settingsV2) {
        invoke2(settingsV2);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(NotificationClient.SettingsV2 settingsV2) {
        WidgetSettingsNotificationOsBinding binding;
        WidgetSettingsNotificationOsBinding binding2;
        m.checkNotNullParameter(settingsV2, "settings");
        binding = this.this$0.getBinding();
        CheckedSetting checkedSetting = binding.d;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsNotificationSwitch");
        checkedSetting.setChecked(settingsV2.isEnabled());
        binding2 = this.this$0.getBinding();
        CheckedSetting checkedSetting2 = binding2.f2605b;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsInappNotifsSwitch");
        checkedSetting2.setChecked(settingsV2.isEnabledInApp());
    }
}
