package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsBinding;
import com.discord.models.presence.Presence;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.error.Error;
import com.discord.utilities.notifications.NotificationUtils;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.debugging.WidgetDebugging;
import com.discord.widgets.media.WidgetQRScanner;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.tabs.NavigationTab;
import com.discord.widgets.tabs.OnTabSelectedListener;
import com.discord.widgets.tabs.WidgetTabsHost;
import com.discord.widgets.user.Badge;
import com.discord.widgets.user.WidgetUserStatusSheet;
import com.discord.widgets.user.profile.UserProfileHeaderView;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action2;
import rx.functions.Func5;
import xyz.discord.R;
/* compiled from: WidgetSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u0002:\u0001$B\u0007¢\u0006\u0004\b#\u0010\u0005J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0012\u0010\u0005J\u000f\u0010\u0013\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0013\u0010\u0005R\u001d\u0010\u0019\u001a\u00020\u00148B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001d\u0010\"\u001a\u00020\u001d8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/tabs/OnTabSelectedListener;", "", "configureToolbar", "()V", "Lcom/discord/widgets/settings/WidgetSettings$Model;", "model", "configureUI", "(Lcom/discord/widgets/settings/WidgetSettings$Model;)V", "Landroid/content/Context;", "context", "showLogoutDialog", "(Landroid/content/Context;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onTabSelected", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;", "viewModelUserProfileHeader$delegate", "Lkotlin/Lazy;", "getViewModelUserProfileHeader", "()Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;", "viewModelUserProfileHeader", "Landroid/content/SharedPreferences;", "sharedPreferences", "Landroid/content/SharedPreferences;", "Lcom/discord/databinding/WidgetSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsBinding;", "binding", HookHelper.constructorName, ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettings extends AppFragment implements OnTabSelectedListener {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsBinding;", 0)};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettings$binding$2.INSTANCE, null, 2, null);
    private final SharedPreferences sharedPreferences = SharedPreferencesProvider.INSTANCE.get();
    private final Lazy viewModelUserProfileHeader$delegate;

    /* compiled from: WidgetSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0012\b\u0082\b\u0018\u0000 '2\u00020\u0001:\u0001'B/\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\u0006\u0010\u0012\u001a\u00020\u000b\u0012\u0006\u0010\u0013\u001a\u00020\u0005¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0007JB\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\b2\b\b\u0002\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\u0013\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u0019\u0010\rJ\u001a\u0010\u001b\u001a\u00020\u00052\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001d\u001a\u0004\b\u001e\u0010\rR\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001f\u001a\u0004\b \u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001f\u001a\u0004\b\u0010\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\nR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010#\u001a\u0004\b$\u0010\u0004¨\u0006("}, d2 = {"Lcom/discord/widgets/settings/WidgetSettings$Model;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "", "component2", "()Z", "Lcom/discord/models/presence/Presence;", "component3", "()Lcom/discord/models/presence/Presence;", "", "component4", "()I", "component5", "meUser", "isStaffOrAlpha", "presence", "promoCount", "pushNotificationUpsellDismissed", "copy", "(Lcom/discord/models/user/MeUser;ZLcom/discord/models/presence/Presence;IZ)Lcom/discord/widgets/settings/WidgetSettings$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getPromoCount", "Z", "getPushNotificationUpsellDismissed", "Lcom/discord/models/presence/Presence;", "getPresence", "Lcom/discord/models/user/MeUser;", "getMeUser", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;ZLcom/discord/models/presence/Presence;IZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean isStaffOrAlpha;
        private final MeUser meUser;
        private final Presence presence;
        private final int promoCount;
        private final boolean pushNotificationUpsellDismissed;

        /* compiled from: WidgetSettings.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettings$Model$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/settings/WidgetSettings$Model;", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v5, types: [com.discord.widgets.settings.WidgetSettings$sam$rx_functions_Func5$0] */
            public final Observable<Model> get() {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<MeUser> observeMe = companion.getUsers().observeMe(true);
                Observable<Boolean> experimentalAlpha = companion.getExperiments().getExperimentalAlpha();
                Observable<Presence> observeLocalPresence = companion.getPresences().observeLocalPresence();
                Observable<Integer> observeUnseenCount = companion.getOutboundPromotions().observeUnseenCount();
                Observable<Boolean> observePushNotificationUpsellDismissed = companion.getNotificationUpsells().observePushNotificationUpsellDismissed();
                final WidgetSettings$Model$Companion$get$1 widgetSettings$Model$Companion$get$1 = WidgetSettings$Model$Companion$get$1.INSTANCE;
                WidgetSettings$Model$Companion$get$1 widgetSettings$Model$Companion$get$12 = widgetSettings$Model$Companion$get$1;
                if (widgetSettings$Model$Companion$get$1 != null) {
                    widgetSettings$Model$Companion$get$12 = new Func5() { // from class: com.discord.widgets.settings.WidgetSettings$sam$rx_functions_Func5$0
                        @Override // rx.functions.Func5
                        public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
                            return Function5.this.invoke(obj, obj2, obj3, obj4, obj5);
                        }
                    };
                }
                Observable<Model> g = Observable.g(observeMe, experimentalAlpha, observeLocalPresence, observeUnseenCount, observePushNotificationUpsellDismissed, (Func5) widgetSettings$Model$Companion$get$12);
                m.checkNotNullExpressionValue(g, "Observable\n            .…    ::Model\n            )");
                return g;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(MeUser meUser, boolean z2, Presence presence, int i, boolean z3) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(presence, "presence");
            this.meUser = meUser;
            this.isStaffOrAlpha = z2;
            this.presence = presence;
            this.promoCount = i;
            this.pushNotificationUpsellDismissed = z3;
        }

        public static /* synthetic */ Model copy$default(Model model, MeUser meUser, boolean z2, Presence presence, int i, boolean z3, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                meUser = model.meUser;
            }
            if ((i2 & 2) != 0) {
                z2 = model.isStaffOrAlpha;
            }
            boolean z4 = z2;
            if ((i2 & 4) != 0) {
                presence = model.presence;
            }
            Presence presence2 = presence;
            if ((i2 & 8) != 0) {
                i = model.promoCount;
            }
            int i3 = i;
            if ((i2 & 16) != 0) {
                z3 = model.pushNotificationUpsellDismissed;
            }
            return model.copy(meUser, z4, presence2, i3, z3);
        }

        public final MeUser component1() {
            return this.meUser;
        }

        public final boolean component2() {
            return this.isStaffOrAlpha;
        }

        public final Presence component3() {
            return this.presence;
        }

        public final int component4() {
            return this.promoCount;
        }

        public final boolean component5() {
            return this.pushNotificationUpsellDismissed;
        }

        public final Model copy(MeUser meUser, boolean z2, Presence presence, int i, boolean z3) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(presence, "presence");
            return new Model(meUser, z2, presence, i, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.meUser, model.meUser) && this.isStaffOrAlpha == model.isStaffOrAlpha && m.areEqual(this.presence, model.presence) && this.promoCount == model.promoCount && this.pushNotificationUpsellDismissed == model.pushNotificationUpsellDismissed;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final Presence getPresence() {
            return this.presence;
        }

        public final int getPromoCount() {
            return this.promoCount;
        }

        public final boolean getPushNotificationUpsellDismissed() {
            return this.pushNotificationUpsellDismissed;
        }

        public int hashCode() {
            MeUser meUser = this.meUser;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            boolean z2 = this.isStaffOrAlpha;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            Presence presence = this.presence;
            if (presence != null) {
                i = presence.hashCode();
            }
            int i6 = (((i5 + i) * 31) + this.promoCount) * 31;
            boolean z3 = this.pushNotificationUpsellDismissed;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            return i6 + i2;
        }

        public final boolean isStaffOrAlpha() {
            return this.isStaffOrAlpha;
        }

        public String toString() {
            StringBuilder R = a.R("Model(meUser=");
            R.append(this.meUser);
            R.append(", isStaffOrAlpha=");
            R.append(this.isStaffOrAlpha);
            R.append(", presence=");
            R.append(this.presence);
            R.append(", promoCount=");
            R.append(this.promoCount);
            R.append(", pushNotificationUpsellDismissed=");
            return a.M(R, this.pushNotificationUpsellDismissed, ")");
        }
    }

    public WidgetSettings() {
        super(R.layout.widget_settings);
        WidgetSettings$viewModelUserProfileHeader$2 widgetSettings$viewModelUserProfileHeader$2 = WidgetSettings$viewModelUserProfileHeader$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModelUserProfileHeader$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(UserProfileHeaderViewModel.class), new WidgetSettings$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetSettings$viewModelUserProfileHeader$2));
    }

    private final void configureToolbar() {
        AppFragment.bindToolbar$default(this, null, 1, null);
        setActionBarTitle(R.string.user_settings);
        setActionBarTitleLayoutMinimumTappableArea();
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_settings, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.settings.WidgetSettings$configureToolbar$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                switch (menuItem.getItemId()) {
                    case R.id.menu_settings_debugging /* 2131364340 */:
                        WidgetDebugging.Companion companion = WidgetDebugging.Companion;
                        m.checkNotNullExpressionValue(context, "context");
                        companion.launch(context);
                        return;
                    case R.id.menu_settings_log_out /* 2131364341 */:
                        WidgetSettings widgetSettings = WidgetSettings.this;
                        m.checkNotNullExpressionValue(context, "context");
                        widgetSettings.showLogoutDialog(context);
                        return;
                    default:
                        return;
                }
            }
        }, null, 4, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        MeUser meUser = model.getMeUser();
        UserUtils userUtils = UserUtils.INSTANCE;
        boolean z2 = true;
        int i = 0;
        boolean z3 = userUtils.isStaff(meUser);
        boolean isVerified = meUser.isVerified();
        boolean hasSubscription = userUtils.getHasSubscription(meUser);
        WidgetSettingsBinding binding = getBinding();
        View view = binding.n;
        m.checkNotNullExpressionValue(view, "developerOptionsDivider");
        view.setVisibility(z3 ? 0 : 8);
        TextView textView = binding.o;
        m.checkNotNullExpressionValue(textView, "developerOptionsHeader");
        textView.setVisibility(z3 ? 0 : 8);
        TextView textView2 = binding.m;
        m.checkNotNullExpressionValue(textView2, "developerOptions");
        textView2.setVisibility(z3 ? 0 : 8);
        binding.m.setOnClickListener(WidgetSettings$configureUI$1$1.INSTANCE);
        LinearLayout linearLayout = binding.u;
        m.checkNotNullExpressionValue(linearLayout, "nitroSettingsContainer");
        linearLayout.setVisibility(isVerified ? 0 : 8);
        TextView textView3 = binding.D;
        m.checkNotNullExpressionValue(textView3, "settingsNitro");
        textView3.setText(getString(hasSubscription ? R.string.billing_manage_subscription : R.string.premium_settings_subscribe_today));
        TextView textView4 = binding.q;
        m.checkNotNullExpressionValue(textView4, "nitroBoosting");
        textView4.setText(getString(hasSubscription ? R.string.premium_guild_perks_modal_manage_your_subscriptions : R.string.premium_settings_premium_guild_subscriptions));
        Presence presence = model.getPresence();
        binding.F.setPresence(presence);
        TextView textView5 = binding.E;
        m.checkNotNullExpressionValue(textView5, "settingsPresenceText");
        textView5.setText(getString(PresenceUtils.INSTANCE.getStatusStringResForPresence(presence)));
        if (model.getPromoCount() > 0) {
            TextView textView6 = binding.r;
            m.checkNotNullExpressionValue(textView6, "nitroGiftingBadge");
            textView6.setText(String.valueOf(model.getPromoCount()));
            TextView textView7 = binding.r;
            m.checkNotNullExpressionValue(textView7, "nitroGiftingBadge");
            MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(new ShapeAppearanceModel.Builder().setAllCornerSizes(ShapeAppearanceModel.PILL).build());
            materialShapeDrawable.setFillColor(ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.status_red_500)));
            textView7.setBackground(materialShapeDrawable);
            TextView textView8 = binding.r;
            m.checkNotNullExpressionValue(textView8, "nitroGiftingBadge");
            textView8.setVisibility(0);
        } else {
            TextView textView9 = binding.r;
            m.checkNotNullExpressionValue(textView9, "nitroGiftingBadge");
            textView9.setVisibility(8);
        }
        LinearLayout linearLayout2 = binding.v;
        m.checkNotNullExpressionValue(linearLayout2, "notificationUpsell");
        if (NotificationManagerCompat.from(requireContext()).areNotificationsEnabled() || model.getPushNotificationUpsellDismissed()) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        linearLayout2.setVisibility(i);
    }

    private final WidgetSettingsBinding getBinding() {
        return (WidgetSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final UserProfileHeaderViewModel getViewModelUserProfileHeader() {
        return (UserProfileHeaderViewModel) this.viewModelUserProfileHeader$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showLogoutDialog(Context context) {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        WidgetNoticeDialog.Builder.setNegativeButton$default(new WidgetNoticeDialog.Builder(context).setTitle(R.string.logout).setMessage(R.string.user_settings_confirm_logout).setDialogAttrTheme(R.attr.notice_theme_positive_red).setPositiveButton(R.string.logout, WidgetSettings$showLogoutDialog$1.INSTANCE), (int) R.string.cancel, (Function1) null, 2, (Object) null).show(parentFragmentManager);
    }

    @Override // com.discord.widgets.tabs.OnTabSelectedListener
    public void onTabSelected() {
        configureToolbar();
        setActionBarTitleAccessibilityViewFocused();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof WidgetTabsHost)) {
            parentFragment = null;
        }
        WidgetTabsHost widgetTabsHost = (WidgetTabsHost) parentFragment;
        if (widgetTabsHost != null) {
            widgetTabsHost.registerTabSelectionListener(NavigationTab.SETTINGS, this);
        }
        final WidgetSettingsBinding binding = getBinding();
        binding.C.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetUserStatusSheet.Companion.show(WidgetSettings.this);
            }
        });
        UserProfileHeaderView userProfileHeaderView = binding.K;
        Badge.Companion companion = Badge.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        userProfileHeaderView.setOnBadgeClick(companion.onBadgeClick(parentFragmentManager, requireContext()));
        binding.K.setOnBannerPress(new WidgetSettings$onViewBound$$inlined$with$lambda$2(this));
        TextView textView = binding.f;
        m.checkNotNullExpressionValue(textView, "appInfoHeader");
        String string = getString(R.string.app_information);
        textView.setText(string + " - 112.14 - Stable (112014)");
        binding.B.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$3

            /* compiled from: WidgetSettings.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()V", "com/discord/widgets/settings/WidgetSettings$onViewBound$1$4$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$3$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function0<Unit> {
                public AnonymousClass1() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetQRScanner.Companion.launch$default(WidgetQRScanner.Companion, WidgetSettings.this.requireContext(), false, 2, null);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetSettings.this.requestCameraQRScanner(new AnonymousClass1());
            }
        });
        binding.u.setOnClickListener(WidgetSettings$onViewBound$1$5.INSTANCE);
        binding.q.setOnClickListener(WidgetSettings$onViewBound$1$6.INSTANCE);
        binding.f2581s.setOnClickListener(WidgetSettings$onViewBound$1$7.INSTANCE);
        binding.c.setOnClickListener(WidgetSettings$onViewBound$1$8.INSTANCE);
        binding.A.setOnClickListener(WidgetSettings$onViewBound$1$9.INSTANCE);
        binding.i.setOnClickListener(WidgetSettings$onViewBound$1$10.INSTANCE);
        binding.f2584z.setOnClickListener(WidgetSettings$onViewBound$1$11.INSTANCE);
        binding.l.setOnClickListener(WidgetSettings$onViewBound$1$12.INSTANCE);
        binding.h.setOnClickListener(WidgetSettings$onViewBound$1$13.INSTANCE);
        binding.f2580b.setOnClickListener(WidgetSettings$onViewBound$1$14.INSTANCE);
        binding.j.setOnClickListener(WidgetSettings$onViewBound$1$15.INSTANCE);
        binding.p.setOnClickListener(WidgetSettings$onViewBound$1$16.INSTANCE);
        binding.e.setOnClickListener(WidgetSettings$onViewBound$1$17.INSTANCE);
        binding.f2583y.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                NotificationUtils.INSTANCE.showNotificationPage(WidgetSettings.this);
            }
        });
        binding.H.setOnClickListener(WidgetSettings$onViewBound$1$19.INSTANCE);
        binding.L.setOnClickListener(WidgetSettings$onViewBound$1$20.INSTANCE);
        binding.G.setOnClickListener(WidgetSettings$onViewBound$1$21.INSTANCE);
        binding.I.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$5

            /* compiled from: WidgetSettings.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"", "isEnabled", "", "invoke", "(Z)V", "com/discord/widgets/settings/WidgetSettings$onViewBound$1$22$1", "updateUploadDebugLogsUI"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$5$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Boolean, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                public static /* synthetic */ void invoke$default(AnonymousClass1 r0, boolean z2, int i, Object obj) {
                    if ((i & 1) != 0) {
                        z2 = true;
                    }
                    r0.invoke(z2);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                    invoke(bool.booleanValue());
                    return Unit.a;
                }

                public final void invoke(boolean z2) {
                    WidgetSettings widgetSettings;
                    int i;
                    TextView textView = WidgetSettingsBinding.this.I;
                    m.checkNotNullExpressionValue(textView, "uploadDebugLogs");
                    textView.setEnabled(z2);
                    TextView textView2 = WidgetSettingsBinding.this.I;
                    m.checkNotNullExpressionValue(textView2, "uploadDebugLogs");
                    if (z2) {
                        widgetSettings = this;
                        i = R.string.upload_debug_logs;
                    } else {
                        widgetSettings = this;
                        i = R.string.working;
                    }
                    textView2.setText(widgetSettings.getString(i));
                }
            }

            /* compiled from: WidgetSettings.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "com/discord/widgets/settings/WidgetSettings$onViewBound$1$22$2", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$5$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends o implements Function1<Void, Unit> {
                public final /* synthetic */ AnonymousClass1 $updateUploadDebugLogsUI$1;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass2(AnonymousClass1 r2) {
                    super(1);
                    this.$updateUploadDebugLogsUI$1 = r2;
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
                    invoke2(r1);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Void r4) {
                    b.a.d.m.i(this, R.string.upload_debug_log_success, 0, 4);
                    AnonymousClass1.invoke$default(this.$updateUploadDebugLogsUI$1, false, 1, null);
                }
            }

            /* compiled from: WidgetSettings.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "com/discord/widgets/settings/WidgetSettings$onViewBound$1$22$3", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$5$3  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass3 extends o implements Function1<Error, Unit> {
                public final /* synthetic */ AnonymousClass1 $updateUploadDebugLogsUI$1;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass3(AnonymousClass1 r1) {
                    super(1);
                    this.$updateUploadDebugLogsUI$1 = r1;
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Error error) {
                    invoke2(error);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Error error) {
                    m.checkNotNullParameter(error, "it");
                    AnonymousClass1.invoke$default(this.$updateUploadDebugLogsUI$1, false, 1, null);
                }
            }

            /* compiled from: WidgetSettings.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()V", "com/discord/widgets/settings/WidgetSettings$onViewBound$1$22$4", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettings$onViewBound$$inlined$with$lambda$5$4  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass4 extends o implements Function0<Unit> {
                public final /* synthetic */ AnonymousClass1 $updateUploadDebugLogsUI$1;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass4(AnonymousClass1 r1) {
                    super(0);
                    this.$updateUploadDebugLogsUI$1 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    AnonymousClass1.invoke$default(this.$updateUploadDebugLogsUI$1, false, 1, null);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AnonymousClass1 r15 = new AnonymousClass1();
                r15.invoke(false);
                Observable ui$default = ObservableExtensionsKt.ui$default(RestAPI.Companion.uploadSystemLog(), this, null, 2, null);
                Context context = this.getContext();
                String name = WidgetSettingsBinding.this.getClass().getName();
                m.checkNotNullExpressionValue(name, "javaClass.name");
                ObservableExtensionsKt.appSubscribe(ui$default, (r18 & 1) != 0 ? null : context, name, (r18 & 4) != 0 ? null : null, new AnonymousClass2(r15), (r18 & 16) != 0 ? null : new AnonymousClass3(r15), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : new AnonymousClass4(r15), (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
            }
        });
        binding.d.setOnClickListener(WidgetSettings$onViewBound$1$23.INSTANCE);
        binding.k.setOnClickListener(WidgetSettings$onViewBound$1$24.INSTANCE);
        binding.f2582x.setOnClickListener(WidgetSettings$onViewBound$1$25.INSTANCE);
        binding.w.setOnClickListener(WidgetSettings$onViewBound$1$26.INSTANCE);
        for (TextView textView2 : n.listOf((Object[]) new TextView[]{binding.J, binding.t, binding.g, binding.o, binding.f})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView2, "header");
            accessibilityUtils.setViewIsHeading(textView2);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        UserProfileHeaderView.Companion companion = UserProfileHeaderView.Companion;
        UserProfileHeaderView userProfileHeaderView = getBinding().K;
        m.checkNotNullExpressionValue(userProfileHeaderView, "binding.userSettingsProfileHeaderView");
        companion.bind(userProfileHeaderView, this, getViewModelUserProfileHeader().observeViewState());
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Model.Companion.get()), this, null, 2, null), WidgetSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettings$onViewBoundOrOnResume$1(this));
    }
}
