package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.f;
import b.a.d.j;
import b.a.i.w5;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.app.AppFragment;
import com.discord.databinding.ViewDialogConfirmationBinding;
import com.discord.databinding.WidgetSettingsPrivacyBinding;
import com.discord.models.domain.Consents;
import com.discord.models.domain.Harvest;
import com.discord.models.domain.ModelUserSettings;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserConnections;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.discord.widgets.contact_sync.ContactSyncFlowAnalytics;
import com.discord.widgets.contact_sync.WidgetContactSync;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.o;
import d0.t.g0;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import j0.l.e.k;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function7;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func7;
import xyz.discord.R;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 ]2\u00020\u0001:\u0003]^_B\u0007¢\u0006\u0004\b\\\u00109J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J!\u0010\r\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J'\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ?\u0010\"\u001a\u00020\u0004*\u00020\u000f2*\u0010!\u001a&\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020\u00040\u001eH\u0002¢\u0006\u0004\b\"\u0010#J\u0017\u0010$\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b$\u0010\u0006J\u0017\u0010&\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\tH\u0002¢\u0006\u0004\b&\u0010'J\u001f\u0010)\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\t2\u0006\u0010(\u001a\u00020\tH\u0002¢\u0006\u0004\b)\u0010*J\u0017\u0010+\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b+\u0010\u0006J'\u0010/\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010,\u001a\u00020\u001a2\u0006\u0010.\u001a\u00020-H\u0002¢\u0006\u0004\b/\u00100J\u001f\u00103\u001a\u00020\u00042\u0006\u00101\u001a\u00020-2\u0006\u00102\u001a\u00020\tH\u0002¢\u0006\u0004\b3\u00104J\u001f\u00107\u001a\u00020\u00042\u0006\u00105\u001a\u00020\t2\u0006\u00106\u001a\u00020\tH\u0002¢\u0006\u0004\b7\u0010*J\u000f\u00108\u001a\u00020\u0004H\u0002¢\u0006\u0004\b8\u00109J!\u0010=\u001a\u00020\u00042\b\u0010;\u001a\u0004\u0018\u00010:2\u0006\u0010<\u001a\u00020\tH\u0002¢\u0006\u0004\b=\u0010>J\u0017\u0010A\u001a\u00020\u00042\u0006\u0010@\u001a\u00020?H\u0017¢\u0006\u0004\bA\u0010BJ\u000f\u0010C\u001a\u00020\u0004H\u0016¢\u0006\u0004\bC\u00109J\u000f\u0010D\u001a\u00020\u0004H\u0016¢\u0006\u0004\bD\u00109R#\u0010J\u001a\b\u0012\u0004\u0012\u00020\u001a0E8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bF\u0010G\u001a\u0004\bH\u0010IR#\u0010M\u001a\b\u0012\u0004\u0012\u00020\u001a0E8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bK\u0010G\u001a\u0004\bL\u0010IR\u0018\u0010N\u001a\u0004\u0018\u00010\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bN\u0010OR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u001d\u0010X\u001a\u00020S8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bT\u0010U\u001a\u0004\bV\u0010WR\u0018\u0010Z\u001a\u0004\u0018\u00010Y8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bZ\u0010[¨\u0006`"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsPrivacy;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V", "configureContactSyncOptions", "configurePrivacyControls", "", "isMeVerified", "Lcom/discord/utilities/rest/RestAPI$HarvestState;", "harvestState", "configureRequestDataButton", "(ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V", "Landroid/content/Context;", "context", "onRequestDataClick", "(Landroid/content/Context;Lcom/discord/utilities/rest/RestAPI$HarvestState;)V", "", "nextAvailableRequestMillis", "showNextAvailableRequestAlert", "(J)V", "consented", "", "consentType", "Lcom/discord/views/CheckedSetting;", "toggle", "toggleConsent", "(ZLjava/lang/String;Lcom/discord/views/CheckedSetting;)V", "Lkotlin/Function5;", "Landroidx/appcompat/app/AlertDialog;", "Landroid/widget/TextView;", "onConfigure", "confirmConsent", "(Landroid/content/Context;Lkotlin/jvm/functions/Function5;)V", "configureDefaultGuildsRestricted", "defaultGuildsRestricted", "showDefaultGuildsRestrictedExistingServers", "(Z)V", "applyToExistingGuilds", "updateDefaultGuildsRestricted", "(ZZ)V", "configureFriendSourceRadio", "radio", "", "explicitContentFilter", "configureExplicitContentRadio", "(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;Lcom/discord/views/CheckedSetting;I)V", "index", "checked", "updateFriendSourceFlags", "(IZ)V", "allowPhone", "allowEmail", "updateFriendDiscoveryFlags", "deleteContactSync", "()V", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "contactsAccount", "syncFriends", "toggleContactSync", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;Z)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onPause", "", "friendSourceRadios$delegate", "Lkotlin/Lazy;", "getFriendSourceRadios", "()Ljava/util/List;", "friendSourceRadios", "explicitContentRadios$delegate", "getExplicitContentRadios", "explicitContentRadios", "dialog", "Landroidx/appcompat/app/AlertDialog;", "Lcom/discord/stores/StoreUserSettings;", "userSettings", "Lcom/discord/stores/StoreUserSettings;", "Lcom/discord/databinding/WidgetSettingsPrivacyBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsPrivacyBinding;", "binding", "Lcom/discord/views/RadioManager;", "radioManagerExplicit", "Lcom/discord/views/RadioManager;", HookHelper.constructorName, "Companion", "LocalState", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPrivacy extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsPrivacy.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsPrivacyBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private AlertDialog dialog;
    private RadioManager radioManagerExplicit;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsPrivacy$binding$2.INSTANCE, null, 2, null);
    private final Lazy explicitContentRadios$delegate = g.lazy(new WidgetSettingsPrivacy$explicitContentRadios$2(this));
    private final Lazy friendSourceRadios$delegate = g.lazy(new WidgetSettingsPrivacy$friendSourceRadios$2(this));
    private final StoreUserSettings userSettings = StoreStream.Companion.getUserSettings();

    /* compiled from: WidgetSettingsPrivacy.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsPrivacy.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsPrivacy.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0016\b\u0082\b\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\b\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u000e\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\u001b\u001a\u00020\u0005¢\u0006\u0004\b2\u00103J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0007J\\\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00052\b\b\u0002\u0010\u0017\u001a\u00020\b2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010\u001b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b!\u0010\u0007J\u001a\u0010#\u001a\u00020\b2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u0019\u0010\u001b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010%\u001a\u0004\b&\u0010\u0007R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010'\u001a\u0004\b(\u0010\rR\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010)\u001a\u0004\b*\u0010\u0004R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010+\u001a\u0004\b,\u0010\u0013R\u0019\u0010\u0017\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010-\u001a\u0004\b.\u0010\nR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010/\u001a\u0004\b0\u0010\u0010R\u0019\u0010\u0016\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010%\u001a\u0004\b1\u0010\u0007¨\u00064"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsPrivacy$LocalState;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "", "component2", "()I", "", "component3", "()Z", "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;", "component4", "()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component5", "()Lcom/discord/api/connectedaccounts/ConnectedAccount;", "Lcom/discord/models/experiments/domain/Experiment;", "component6", "()Lcom/discord/models/experiments/domain/Experiment;", "component7", "me", "explicitContentFilter", "defaultRestrictedGuilds", "friendSourceFlags", "contactSyncConnection", "contactSyncExperiment", "userDiscoveryFlags", "copy", "(Lcom/discord/models/user/MeUser;IZLcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;Lcom/discord/api/connectedaccounts/ConnectedAccount;Lcom/discord/models/experiments/domain/Experiment;I)Lcom/discord/widgets/settings/WidgetSettingsPrivacy$LocalState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getUserDiscoveryFlags", "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;", "getFriendSourceFlags", "Lcom/discord/models/user/MeUser;", "getMe", "Lcom/discord/models/experiments/domain/Experiment;", "getContactSyncExperiment", "Z", "getDefaultRestrictedGuilds", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "getContactSyncConnection", "getExplicitContentFilter", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;IZLcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;Lcom/discord/api/connectedaccounts/ConnectedAccount;Lcom/discord/models/experiments/domain/Experiment;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class LocalState {
        private final ConnectedAccount contactSyncConnection;
        private final Experiment contactSyncExperiment;
        private final boolean defaultRestrictedGuilds;
        private final int explicitContentFilter;
        private final ModelUserSettings.FriendSourceFlags friendSourceFlags;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2840me;
        private final int userDiscoveryFlags;

        public LocalState(MeUser meUser, int i, boolean z2, ModelUserSettings.FriendSourceFlags friendSourceFlags, ConnectedAccount connectedAccount, Experiment experiment, int i2) {
            m.checkNotNullParameter(meUser, "me");
            this.f2840me = meUser;
            this.explicitContentFilter = i;
            this.defaultRestrictedGuilds = z2;
            this.friendSourceFlags = friendSourceFlags;
            this.contactSyncConnection = connectedAccount;
            this.contactSyncExperiment = experiment;
            this.userDiscoveryFlags = i2;
        }

        public static /* synthetic */ LocalState copy$default(LocalState localState, MeUser meUser, int i, boolean z2, ModelUserSettings.FriendSourceFlags friendSourceFlags, ConnectedAccount connectedAccount, Experiment experiment, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                meUser = localState.f2840me;
            }
            if ((i3 & 2) != 0) {
                i = localState.explicitContentFilter;
            }
            int i4 = i;
            if ((i3 & 4) != 0) {
                z2 = localState.defaultRestrictedGuilds;
            }
            boolean z3 = z2;
            if ((i3 & 8) != 0) {
                friendSourceFlags = localState.friendSourceFlags;
            }
            ModelUserSettings.FriendSourceFlags friendSourceFlags2 = friendSourceFlags;
            if ((i3 & 16) != 0) {
                connectedAccount = localState.contactSyncConnection;
            }
            ConnectedAccount connectedAccount2 = connectedAccount;
            if ((i3 & 32) != 0) {
                experiment = localState.contactSyncExperiment;
            }
            Experiment experiment2 = experiment;
            if ((i3 & 64) != 0) {
                i2 = localState.userDiscoveryFlags;
            }
            return localState.copy(meUser, i4, z3, friendSourceFlags2, connectedAccount2, experiment2, i2);
        }

        public final MeUser component1() {
            return this.f2840me;
        }

        public final int component2() {
            return this.explicitContentFilter;
        }

        public final boolean component3() {
            return this.defaultRestrictedGuilds;
        }

        public final ModelUserSettings.FriendSourceFlags component4() {
            return this.friendSourceFlags;
        }

        public final ConnectedAccount component5() {
            return this.contactSyncConnection;
        }

        public final Experiment component6() {
            return this.contactSyncExperiment;
        }

        public final int component7() {
            return this.userDiscoveryFlags;
        }

        public final LocalState copy(MeUser meUser, int i, boolean z2, ModelUserSettings.FriendSourceFlags friendSourceFlags, ConnectedAccount connectedAccount, Experiment experiment, int i2) {
            m.checkNotNullParameter(meUser, "me");
            return new LocalState(meUser, i, z2, friendSourceFlags, connectedAccount, experiment, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LocalState)) {
                return false;
            }
            LocalState localState = (LocalState) obj;
            return m.areEqual(this.f2840me, localState.f2840me) && this.explicitContentFilter == localState.explicitContentFilter && this.defaultRestrictedGuilds == localState.defaultRestrictedGuilds && m.areEqual(this.friendSourceFlags, localState.friendSourceFlags) && m.areEqual(this.contactSyncConnection, localState.contactSyncConnection) && m.areEqual(this.contactSyncExperiment, localState.contactSyncExperiment) && this.userDiscoveryFlags == localState.userDiscoveryFlags;
        }

        public final ConnectedAccount getContactSyncConnection() {
            return this.contactSyncConnection;
        }

        public final Experiment getContactSyncExperiment() {
            return this.contactSyncExperiment;
        }

        public final boolean getDefaultRestrictedGuilds() {
            return this.defaultRestrictedGuilds;
        }

        public final int getExplicitContentFilter() {
            return this.explicitContentFilter;
        }

        public final ModelUserSettings.FriendSourceFlags getFriendSourceFlags() {
            return this.friendSourceFlags;
        }

        public final MeUser getMe() {
            return this.f2840me;
        }

        public final int getUserDiscoveryFlags() {
            return this.userDiscoveryFlags;
        }

        public int hashCode() {
            MeUser meUser = this.f2840me;
            int i = 0;
            int hashCode = (((meUser != null ? meUser.hashCode() : 0) * 31) + this.explicitContentFilter) * 31;
            boolean z2 = this.defaultRestrictedGuilds;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode + i2) * 31;
            ModelUserSettings.FriendSourceFlags friendSourceFlags = this.friendSourceFlags;
            int hashCode2 = (i4 + (friendSourceFlags != null ? friendSourceFlags.hashCode() : 0)) * 31;
            ConnectedAccount connectedAccount = this.contactSyncConnection;
            int hashCode3 = (hashCode2 + (connectedAccount != null ? connectedAccount.hashCode() : 0)) * 31;
            Experiment experiment = this.contactSyncExperiment;
            if (experiment != null) {
                i = experiment.hashCode();
            }
            return ((hashCode3 + i) * 31) + this.userDiscoveryFlags;
        }

        public String toString() {
            StringBuilder R = a.R("LocalState(me=");
            R.append(this.f2840me);
            R.append(", explicitContentFilter=");
            R.append(this.explicitContentFilter);
            R.append(", defaultRestrictedGuilds=");
            R.append(this.defaultRestrictedGuilds);
            R.append(", friendSourceFlags=");
            R.append(this.friendSourceFlags);
            R.append(", contactSyncConnection=");
            R.append(this.contactSyncConnection);
            R.append(", contactSyncExperiment=");
            R.append(this.contactSyncExperiment);
            R.append(", userDiscoveryFlags=");
            return a.A(R, this.userDiscoveryFlags, ")");
        }
    }

    /* compiled from: WidgetSettingsPrivacy.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u001a\b\u0082\b\u0018\u0000 =2\u00020\u0001:\u0001=BU\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0005\u0012\u0006\u0010\u001b\u001a\u00020\b\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u001d\u001a\u00020\u0005\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\u001f\u001a\u00020\b\u0012\u0006\u0010 \u001a\u00020\u0013\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0016¢\u0006\u0004\b;\u0010<J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0007J\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018Jp\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00052\b\b\u0002\u0010\u001b\u001a\u00020\b2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\u001d\u001a\u00020\u00052\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010\u001f\u001a\u00020\b2\b\b\u0002\u0010 \u001a\u00020\u00132\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0016HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b'\u0010\u0007J\u001a\u0010)\u001a\u00020\b2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b)\u0010*R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010+\u001a\u0004\b,\u0010\rR\u0019\u0010\u001b\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010-\u001a\u0004\b.\u0010\nR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010/\u001a\u0004\b0\u0010\u0011R\u0019\u0010 \u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b \u00101\u001a\u0004\b2\u0010\u0015R\u0019\u0010\u001a\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00103\u001a\u0004\b4\u0010\u0007R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00105\u001a\u0004\b6\u0010\u0004R\u0019\u0010\u001f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010-\u001a\u0004\b7\u0010\nR\u001b\u0010!\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b!\u00108\u001a\u0004\b9\u0010\u0018R\u0019\u0010\u001d\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00103\u001a\u0004\b:\u0010\u0007¨\u0006>"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "", "component2", "()I", "", "component3", "()Z", "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;", "component4", "()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;", "component5", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component6", "()Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component7", "Lcom/discord/models/domain/Consents;", "component8", "()Lcom/discord/models/domain/Consents;", "Lcom/discord/utilities/rest/RestAPI$HarvestState;", "component9", "()Lcom/discord/utilities/rest/RestAPI$HarvestState;", "me", "explicitContentFilter", "defaultRestrictedGuilds", "friendSourceFlags", "userDiscoveryFlags", "contactSyncConnection", "showContactSync", "consents", "harvestState", "copy", "(Lcom/discord/models/user/MeUser;IZLcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;ILcom/discord/api/connectedaccounts/ConnectedAccount;ZLcom/discord/models/domain/Consents;Lcom/discord/utilities/rest/RestAPI$HarvestState;)Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;", "getFriendSourceFlags", "Z", "getDefaultRestrictedGuilds", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "getContactSyncConnection", "Lcom/discord/models/domain/Consents;", "getConsents", "I", "getExplicitContentFilter", "Lcom/discord/models/user/MeUser;", "getMe", "getShowContactSync", "Lcom/discord/utilities/rest/RestAPI$HarvestState;", "getHarvestState", "getUserDiscoveryFlags", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;IZLcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;ILcom/discord/api/connectedaccounts/ConnectedAccount;ZLcom/discord/models/domain/Consents;Lcom/discord/utilities/rest/RestAPI$HarvestState;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Consents consents;
        private final ConnectedAccount contactSyncConnection;
        private final boolean defaultRestrictedGuilds;
        private final int explicitContentFilter;
        private final ModelUserSettings.FriendSourceFlags friendSourceFlags;
        private final RestAPI.HarvestState harvestState;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2841me;
        private final boolean showContactSync;
        private final int userDiscoveryFlags;

        /* compiled from: WidgetSettingsPrivacy.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v8, types: [com.discord.widgets.settings.WidgetSettingsPrivacy$sam$rx_functions_Func7$0] */
            public final Observable<Model> get() {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
                Observable<Integer> observeExplicitContentFilter = companion.getUserSettings().observeExplicitContentFilter();
                Observable<Boolean> observeIsDefaultGuildsRestricted = companion.getUserSettings().observeIsDefaultGuildsRestricted();
                Observable<ModelUserSettings.FriendSourceFlags> observeFriendSourceFlags = companion.getUserSettings().observeFriendSourceFlags();
                Observable F = companion.getUserConnections().observeConnectedAccounts().F(WidgetSettingsPrivacy$Model$Companion$get$1.INSTANCE);
                Observable<Experiment> observeUserExperiment = companion.getExperiments().observeUserExperiment("2021-04_contact_sync_android_main", true);
                Observable<Integer> observeFriendDiscoveryFlags = companion.getUserSettings().observeFriendDiscoveryFlags();
                final WidgetSettingsPrivacy$Model$Companion$get$2 widgetSettingsPrivacy$Model$Companion$get$2 = WidgetSettingsPrivacy$Model$Companion$get$2.INSTANCE;
                WidgetSettingsPrivacy$Model$Companion$get$2 widgetSettingsPrivacy$Model$Companion$get$22 = widgetSettingsPrivacy$Model$Companion$get$2;
                if (widgetSettingsPrivacy$Model$Companion$get$2 != null) {
                    widgetSettingsPrivacy$Model$Companion$get$22 = new Func7() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$sam$rx_functions_Func7$0
                        @Override // rx.functions.Func7
                        public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7) {
                            return Function7.this.invoke(obj, obj2, obj3, obj4, obj5, obj6, obj7);
                        }
                    };
                }
                Observable Y = Observable.e(observeMe$default, observeExplicitContentFilter, observeIsDefaultGuildsRestricted, observeFriendSourceFlags, F, observeUserExperiment, observeFriendDiscoveryFlags, (Func7) widgetSettingsPrivacy$Model$Companion$get$22).Y(WidgetSettingsPrivacy$Model$Companion$get$3.INSTANCE);
                m.checkNotNullExpressionValue(Y, "Observable\n          .co…            }\n          }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(Y).q();
                m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(MeUser meUser, int i, boolean z2, ModelUserSettings.FriendSourceFlags friendSourceFlags, int i2, ConnectedAccount connectedAccount, boolean z3, Consents consents, RestAPI.HarvestState harvestState) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(consents, "consents");
            this.f2841me = meUser;
            this.explicitContentFilter = i;
            this.defaultRestrictedGuilds = z2;
            this.friendSourceFlags = friendSourceFlags;
            this.userDiscoveryFlags = i2;
            this.contactSyncConnection = connectedAccount;
            this.showContactSync = z3;
            this.consents = consents;
            this.harvestState = harvestState;
        }

        public final MeUser component1() {
            return this.f2841me;
        }

        public final int component2() {
            return this.explicitContentFilter;
        }

        public final boolean component3() {
            return this.defaultRestrictedGuilds;
        }

        public final ModelUserSettings.FriendSourceFlags component4() {
            return this.friendSourceFlags;
        }

        public final int component5() {
            return this.userDiscoveryFlags;
        }

        public final ConnectedAccount component6() {
            return this.contactSyncConnection;
        }

        public final boolean component7() {
            return this.showContactSync;
        }

        public final Consents component8() {
            return this.consents;
        }

        public final RestAPI.HarvestState component9() {
            return this.harvestState;
        }

        public final Model copy(MeUser meUser, int i, boolean z2, ModelUserSettings.FriendSourceFlags friendSourceFlags, int i2, ConnectedAccount connectedAccount, boolean z3, Consents consents, RestAPI.HarvestState harvestState) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(consents, "consents");
            return new Model(meUser, i, z2, friendSourceFlags, i2, connectedAccount, z3, consents, harvestState);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.f2841me, model.f2841me) && this.explicitContentFilter == model.explicitContentFilter && this.defaultRestrictedGuilds == model.defaultRestrictedGuilds && m.areEqual(this.friendSourceFlags, model.friendSourceFlags) && this.userDiscoveryFlags == model.userDiscoveryFlags && m.areEqual(this.contactSyncConnection, model.contactSyncConnection) && this.showContactSync == model.showContactSync && m.areEqual(this.consents, model.consents) && m.areEqual(this.harvestState, model.harvestState);
        }

        public final Consents getConsents() {
            return this.consents;
        }

        public final ConnectedAccount getContactSyncConnection() {
            return this.contactSyncConnection;
        }

        public final boolean getDefaultRestrictedGuilds() {
            return this.defaultRestrictedGuilds;
        }

        public final int getExplicitContentFilter() {
            return this.explicitContentFilter;
        }

        public final ModelUserSettings.FriendSourceFlags getFriendSourceFlags() {
            return this.friendSourceFlags;
        }

        public final RestAPI.HarvestState getHarvestState() {
            return this.harvestState;
        }

        public final MeUser getMe() {
            return this.f2841me;
        }

        public final boolean getShowContactSync() {
            return this.showContactSync;
        }

        public final int getUserDiscoveryFlags() {
            return this.userDiscoveryFlags;
        }

        public int hashCode() {
            MeUser meUser = this.f2841me;
            int i = 0;
            int hashCode = (((meUser != null ? meUser.hashCode() : 0) * 31) + this.explicitContentFilter) * 31;
            boolean z2 = this.defaultRestrictedGuilds;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            ModelUserSettings.FriendSourceFlags friendSourceFlags = this.friendSourceFlags;
            int hashCode2 = (((i5 + (friendSourceFlags != null ? friendSourceFlags.hashCode() : 0)) * 31) + this.userDiscoveryFlags) * 31;
            ConnectedAccount connectedAccount = this.contactSyncConnection;
            int hashCode3 = (hashCode2 + (connectedAccount != null ? connectedAccount.hashCode() : 0)) * 31;
            boolean z3 = this.showContactSync;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            int i6 = (hashCode3 + i2) * 31;
            Consents consents = this.consents;
            int hashCode4 = (i6 + (consents != null ? consents.hashCode() : 0)) * 31;
            RestAPI.HarvestState harvestState = this.harvestState;
            if (harvestState != null) {
                i = harvestState.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(me=");
            R.append(this.f2841me);
            R.append(", explicitContentFilter=");
            R.append(this.explicitContentFilter);
            R.append(", defaultRestrictedGuilds=");
            R.append(this.defaultRestrictedGuilds);
            R.append(", friendSourceFlags=");
            R.append(this.friendSourceFlags);
            R.append(", userDiscoveryFlags=");
            R.append(this.userDiscoveryFlags);
            R.append(", contactSyncConnection=");
            R.append(this.contactSyncConnection);
            R.append(", showContactSync=");
            R.append(this.showContactSync);
            R.append(", consents=");
            R.append(this.consents);
            R.append(", harvestState=");
            R.append(this.harvestState);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetSettingsPrivacy() {
        super(R.layout.widget_settings_privacy);
    }

    private final void configureContactSyncOptions(final Model model) {
        LinearLayout linearLayout = getBinding().j;
        m.checkNotNullExpressionValue(linearLayout, "binding.settingsPrivacyContactSync");
        int i = 8;
        linearLayout.setVisibility(model.getShowContactSync() ? 0 : 8);
        if (model.getShowContactSync()) {
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.contactSyncSettingInfo");
            b.m(textView, R.string.contact_sync_info_settings_2, new Object[0], new WidgetSettingsPrivacy$configureContactSyncOptions$1(this));
            TextView textView2 = getBinding().d;
            m.checkNotNullExpressionValue(textView2, "binding.contactSyncSettingInfo");
            textView2.setMovementMethod(LinkMovementMethod.getInstance());
            CheckedSetting checkedSetting = getBinding().c;
            m.checkNotNullExpressionValue(checkedSetting, "binding.contactSyncSettingEnabled");
            ConnectedAccount contactSyncConnection = model.getContactSyncConnection();
            checkedSetting.setChecked(contactSyncConnection != null && contactSyncConnection.a());
            CheckedSetting checkedSetting2 = getBinding().e;
            m.checkNotNullExpressionValue(checkedSetting2, "binding.contactSyncSettingPhone");
            checkedSetting2.setChecked((model.getUserDiscoveryFlags() & 2) == 2);
            CheckedSetting checkedSetting3 = getBinding().f2609b;
            m.checkNotNullExpressionValue(checkedSetting3, "binding.contactSyncSettingEmail");
            checkedSetting3.setChecked((model.getUserDiscoveryFlags() & 4) == 4);
            getBinding().c.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureContactSyncOptions$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetSettingsPrivacyBinding binding;
                    WidgetSettingsPrivacyBinding binding2;
                    WidgetSettingsPrivacyBinding binding3;
                    binding = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting4 = binding.c;
                    m.checkNotNullExpressionValue(checkedSetting4, "binding.contactSyncSettingEnabled");
                    boolean z2 = !checkedSetting4.isChecked();
                    if (model.getContactSyncConnection() != null) {
                        AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                        binding2 = WidgetSettingsPrivacy.this.getBinding();
                        CheckedSetting checkedSetting5 = binding2.e;
                        m.checkNotNullExpressionValue(checkedSetting5, "binding.contactSyncSettingPhone");
                        boolean isChecked = checkedSetting5.isChecked();
                        binding3 = WidgetSettingsPrivacy.this.getBinding();
                        CheckedSetting checkedSetting6 = binding3.f2609b;
                        m.checkNotNullExpressionValue(checkedSetting6, "binding.contactSyncSettingEmail");
                        analyticsTracker.contactSyncToggled(z2, isChecked, checkedSetting6.isChecked());
                        WidgetSettingsPrivacy.this.toggleContactSync(model.getContactSyncConnection(), z2);
                    } else if (z2) {
                        AnalyticsTracker.INSTANCE.openModal("Contact Sync", new Traits.Location(Traits.Location.Page.USER_SETTINGS, null, null, null, null, 30, null));
                        ContactSyncFlowAnalytics.Companion.trackStart$default(ContactSyncFlowAnalytics.Companion, false, g0.mapOf(o.to("location_page", Traits.Location.Page.USER_SETTINGS)), 1, null);
                        WidgetContactSync.Companion.launch$default(WidgetContactSync.Companion, WidgetSettingsPrivacy.this.requireContext(), null, false, false, false, 30, null);
                    }
                }
            });
            getBinding().e.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureContactSyncOptions$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetSettingsPrivacyBinding binding;
                    WidgetSettingsPrivacyBinding binding2;
                    WidgetSettingsPrivacyBinding binding3;
                    WidgetSettingsPrivacyBinding binding4;
                    binding = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting4 = binding.e;
                    m.checkNotNullExpressionValue(checkedSetting4, "binding.contactSyncSettingPhone");
                    boolean z2 = !checkedSetting4.isChecked();
                    binding2 = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting5 = binding2.f2609b;
                    m.checkNotNullExpressionValue(checkedSetting5, "binding.contactSyncSettingEmail");
                    boolean isChecked = checkedSetting5.isChecked();
                    AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                    binding3 = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting6 = binding3.c;
                    m.checkNotNullExpressionValue(checkedSetting6, "binding.contactSyncSettingEnabled");
                    boolean isChecked2 = checkedSetting6.isChecked();
                    binding4 = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting7 = binding4.f2609b;
                    m.checkNotNullExpressionValue(checkedSetting7, "binding.contactSyncSettingEmail");
                    analyticsTracker.contactSyncToggled(isChecked2, z2, checkedSetting7.isChecked());
                    WidgetSettingsPrivacy.this.updateFriendDiscoveryFlags(z2, isChecked);
                }
            });
            getBinding().f2609b.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureContactSyncOptions$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetSettingsPrivacyBinding binding;
                    WidgetSettingsPrivacyBinding binding2;
                    WidgetSettingsPrivacyBinding binding3;
                    WidgetSettingsPrivacyBinding binding4;
                    binding = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting4 = binding.f2609b;
                    m.checkNotNullExpressionValue(checkedSetting4, "binding.contactSyncSettingEmail");
                    boolean z2 = !checkedSetting4.isChecked();
                    binding2 = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting5 = binding2.e;
                    m.checkNotNullExpressionValue(checkedSetting5, "binding.contactSyncSettingPhone");
                    boolean isChecked = checkedSetting5.isChecked();
                    AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                    binding3 = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting6 = binding3.c;
                    m.checkNotNullExpressionValue(checkedSetting6, "binding.contactSyncSettingEnabled");
                    boolean isChecked2 = checkedSetting6.isChecked();
                    binding4 = WidgetSettingsPrivacy.this.getBinding();
                    CheckedSetting checkedSetting7 = binding4.e;
                    m.checkNotNullExpressionValue(checkedSetting7, "binding.contactSyncSettingPhone");
                    analyticsTracker.contactSyncToggled(isChecked2, checkedSetting7.isChecked(), z2);
                    WidgetSettingsPrivacy.this.updateFriendDiscoveryFlags(isChecked, z2);
                }
            });
            CheckedSetting checkedSetting4 = getBinding().f;
            m.checkNotNullExpressionValue(checkedSetting4, "binding.contactSyncSettingStaffOnly");
            if (UserUtils.INSTANCE.isStaff(model.getMe())) {
                i = 0;
            }
            checkedSetting4.setVisibility(i);
            CheckedSetting checkedSetting5 = getBinding().f;
            m.checkNotNullExpressionValue(checkedSetting5, "binding.contactSyncSettingStaffOnly");
            checkedSetting5.setChecked(true);
            getBinding().f.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureContactSyncOptions$5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    if (UserUtils.INSTANCE.isStaff(model.getMe())) {
                        WidgetSettingsPrivacy.this.deleteContactSync();
                    }
                }
            });
        }
    }

    private final void configureDefaultGuildsRestricted(final Model model) {
        CheckedSetting checkedSetting = getBinding().m;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsPrivacyDefaultRestrictedGuilds");
        checkedSetting.setChecked(!model.getDefaultRestrictedGuilds());
        getBinding().m.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureDefaultGuildsRestricted$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsPrivacy.this.showDefaultGuildsRestrictedExistingServers(!model.getDefaultRestrictedGuilds());
            }
        });
    }

    private final void configureExplicitContentRadio(Model model, CheckedSetting checkedSetting, final int i) {
        RadioManager radioManager;
        checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureExplicitContentRadio$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoreStream.Companion.getUserSettings().setExplicitContentFilter(WidgetSettingsPrivacy.this.getAppActivity(), i);
            }
        });
        if (this.radioManagerExplicit != null && model.getExplicitContentFilter() == i && (radioManager = this.radioManagerExplicit) != null) {
            radioManager.a(checkedSetting);
        }
    }

    private final void configureFriendSourceRadio(Model model) {
        boolean z2 = false;
        getFriendSourceRadios().get(0).setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureFriendSourceRadio$1
            public final void call(Boolean bool) {
                WidgetSettingsPrivacy widgetSettingsPrivacy = WidgetSettingsPrivacy.this;
                m.checkNotNullExpressionValue(bool, "checked");
                widgetSettingsPrivacy.updateFriendSourceFlags(0, bool.booleanValue());
            }
        });
        getFriendSourceRadios().get(1).setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureFriendSourceRadio$2
            public final void call(Boolean bool) {
                WidgetSettingsPrivacy widgetSettingsPrivacy = WidgetSettingsPrivacy.this;
                m.checkNotNullExpressionValue(bool, "checked");
                widgetSettingsPrivacy.updateFriendSourceFlags(1, bool.booleanValue());
            }
        });
        getFriendSourceRadios().get(2).setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureFriendSourceRadio$3
            public final void call(Boolean bool) {
                WidgetSettingsPrivacy widgetSettingsPrivacy = WidgetSettingsPrivacy.this;
                m.checkNotNullExpressionValue(bool, "checked");
                widgetSettingsPrivacy.updateFriendSourceFlags(2, bool.booleanValue());
            }
        });
        ModelUserSettings.FriendSourceFlags friendSourceFlags = model.getFriendSourceFlags();
        boolean isAll = friendSourceFlags != null ? friendSourceFlags.isAll() : false;
        CheckedSetting checkedSetting = getFriendSourceRadios().get(0);
        m.checkNotNullExpressionValue(checkedSetting, "friendSourceRadios[0]");
        checkedSetting.setChecked(isAll);
        CheckedSetting checkedSetting2 = getFriendSourceRadios().get(1);
        m.checkNotNullExpressionValue(checkedSetting2, "friendSourceRadios[1]");
        CheckedSetting checkedSetting3 = checkedSetting2;
        ModelUserSettings.FriendSourceFlags friendSourceFlags2 = model.getFriendSourceFlags();
        checkedSetting3.setChecked((friendSourceFlags2 != null ? friendSourceFlags2.isMutualFriends() : false) || isAll);
        CheckedSetting checkedSetting4 = getFriendSourceRadios().get(2);
        m.checkNotNullExpressionValue(checkedSetting4, "friendSourceRadios[2]");
        CheckedSetting checkedSetting5 = checkedSetting4;
        ModelUserSettings.FriendSourceFlags friendSourceFlags3 = model.getFriendSourceFlags();
        if ((friendSourceFlags3 != null ? friendSourceFlags3.isMutualGuilds() : false) || isAll) {
            z2 = true;
        }
        checkedSetting5.setChecked(z2);
    }

    private final void configurePrivacyControls(Model model) {
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            LinearLayout linearLayout = getBinding().k;
            m.checkNotNullExpressionValue(linearLayout, "binding.settingsPrivacyControls");
            linearLayout.setVisibility(0);
            CheckedSetting checkedSetting = getBinding().f2613z;
            m.checkNotNullExpressionValue(checkedSetting, "binding.settingsPrivacyStatistics");
            checkedSetting.setChecked(model.getConsents().getUsageStatistics().getConsented());
            getBinding().f2613z.setOnCheckedListener(new WidgetSettingsPrivacy$configurePrivacyControls$1(this, context));
            CheckedSetting checkedSetting2 = getBinding().v;
            m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsPrivacyPersonalization");
            checkedSetting2.setChecked(model.getConsents().getPersonalization().getConsented());
            getBinding().v.setOnCheckedListener(new WidgetSettingsPrivacy$configurePrivacyControls$2(this, context));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureRequestDataButton(final boolean z2, final RestAPI.HarvestState harvestState) {
        getBinding().w.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$configureRequestDataButton$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RestAPI.HarvestState harvestState2;
                Context context = WidgetSettingsPrivacy.this.getContext();
                if (context != null) {
                    m.checkNotNullExpressionValue(context, "context ?: return@setOnClickListener");
                    if (!z2 || (harvestState2 = harvestState) == null) {
                        b.a.d.m.i(WidgetSettingsPrivacy.this, R.string.data_privacy_controls_request_data_tooltip, 0, 4);
                    } else {
                        WidgetSettingsPrivacy.this.onRequestDataClick(context, harvestState2);
                    }
                }
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        configureDefaultGuildsRestricted(model);
        configureFriendSourceRadio(model);
        configureContactSyncOptions(model);
        CheckedSetting checkedSetting = getExplicitContentRadios().get(0);
        m.checkNotNullExpressionValue(checkedSetting, "explicitContentRadios[0]");
        configureExplicitContentRadio(model, checkedSetting, 0);
        CheckedSetting checkedSetting2 = getExplicitContentRadios().get(1);
        m.checkNotNullExpressionValue(checkedSetting2, "explicitContentRadios[1]");
        configureExplicitContentRadio(model, checkedSetting2, 1);
        CheckedSetting checkedSetting3 = getExplicitContentRadios().get(2);
        m.checkNotNullExpressionValue(checkedSetting3, "explicitContentRadios[2]");
        configureExplicitContentRadio(model, checkedSetting3, 2);
        configurePrivacyControls(model);
        configureRequestDataButton(model.getMe().isVerified(), model.getHarvestState());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void confirmConsent(Context context, Function5<? super AlertDialog, ? super TextView, ? super TextView, ? super TextView, ? super TextView, Unit> function5) {
        ViewDialogConfirmationBinding a = ViewDialogConfirmationBinding.a(getLayoutInflater().inflate(R.layout.view_dialog_confirmation, (ViewGroup) null, false));
        m.checkNotNullExpressionValue(a, "ViewDialogConfirmationBi…outInflater, null, false)");
        AlertDialog create = new AlertDialog.Builder(context).setView(a.a).setCancelable(false).create();
        m.checkNotNullExpressionValue(create, "AlertDialog.Builder(this…(false)\n        .create()");
        TextView textView = a.d;
        m.checkNotNullExpressionValue(textView, "binding.viewDialogConfirmationHeader");
        TextView textView2 = a.e;
        m.checkNotNullExpressionValue(textView2, "binding.viewDialogConfirmationText");
        MaterialButton materialButton = a.f2167b;
        m.checkNotNullExpressionValue(materialButton, "binding.viewDialogConfirmationCancel");
        MaterialButton materialButton2 = a.c;
        m.checkNotNullExpressionValue(materialButton2, "binding.viewDialogConfirmationConfirm");
        function5.invoke(create, textView, textView2, materialButton, materialButton2);
        create.show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void deleteContactSync() {
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getContactSync().clearDismissStates();
        companion.getUserConnections().deleteUserConnection("contacts", "@me");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSettingsPrivacyBinding getBinding() {
        return (WidgetSettingsPrivacyBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final List<CheckedSetting> getExplicitContentRadios() {
        return (List) this.explicitContentRadios$delegate.getValue();
    }

    private final List<CheckedSetting> getFriendSourceRadios() {
        return (List) this.friendSourceRadios$delegate.getValue();
    }

    public static final void launch(Context context) {
        Companion.launch(context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onRequestDataClick(Context context, RestAPI.HarvestState harvestState) {
        WidgetSettingsPrivacy$onRequestDataClick$4 widgetSettingsPrivacy$onRequestDataClick$4 = new WidgetSettingsPrivacy$onRequestDataClick$4(this, context, new WidgetSettingsPrivacy$onRequestDataClick$3(this, context, new WidgetSettingsPrivacy$onRequestDataClick$2(this, context), new WidgetSettingsPrivacy$onRequestDataClick$1(this, context)));
        if (harvestState instanceof RestAPI.HarvestState.NeverRequested) {
            widgetSettingsPrivacy$onRequestDataClick$4.invoke2();
        } else if (harvestState instanceof RestAPI.HarvestState.LastRequested) {
            Harvest data = ((RestAPI.HarvestState.LastRequested) harvestState).getData();
            if (Harvest.canRequest$default(data, 0L, 1, null)) {
                widgetSettingsPrivacy$onRequestDataClick$4.invoke2();
            } else {
                showNextAvailableRequestAlert(data.nextAvailableRequestInMillis());
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showDefaultGuildsRestrictedExistingServers(final boolean z2) {
        View inflate = getLayoutInflater().inflate(R.layout.widget_settings_privacy_defaults, (ViewGroup) null, false);
        int i = R.id.settings_privacy_defaults_existing_no;
        MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.settings_privacy_defaults_existing_no);
        if (materialButton != null) {
            i = R.id.settings_privacy_defaults_existing_yes;
            MaterialButton materialButton2 = (MaterialButton) inflate.findViewById(R.id.settings_privacy_defaults_existing_yes);
            if (materialButton2 != null) {
                LinearLayout linearLayout = (LinearLayout) inflate;
                m.checkNotNullExpressionValue(new w5(linearLayout, materialButton, materialButton2), "WidgetSettingsPrivacyDef…outInflater, null, false)");
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$showDefaultGuildsRestrictedExistingServers$$inlined$apply$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetSettingsPrivacy.this.updateDefaultGuildsRestricted(z2, false);
                    }
                });
                materialButton2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$showDefaultGuildsRestrictedExistingServers$$inlined$apply$lambda$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetSettingsPrivacy.this.updateDefaultGuildsRestricted(z2, true);
                    }
                });
                AlertDialog alertDialog = this.dialog;
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                m.checkNotNullExpressionValue(linearLayout, "binding.root");
                AlertDialog create = new AlertDialog.Builder(linearLayout.getContext()).setView(linearLayout).create();
                this.dialog = create;
                if (create != null) {
                    create.show();
                    return;
                }
                return;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    private final void showNextAvailableRequestAlert(long j) {
        CharSequence b2;
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            b2 = b.b(context, R.string.data_download_requested_status_note, new Object[]{TimeUtils.renderUtcDate$default(TimeUtils.INSTANCE, j, context, 0, 4, null)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            WidgetNoticeDialog.Builder positiveButton$default = WidgetNoticeDialog.Builder.setPositiveButton$default(new WidgetNoticeDialog.Builder(context).setTitle(R.string.data_privacy_rate_limit_title).setMessage(b2), (int) R.string.okay, (Function1) null, 2, (Object) null);
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            positiveButton$default.show(parentFragmentManager);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void toggleConsent(final boolean z2, String str, final CheckedSetting checkedSetting) {
        ObservableExtensionsKt.withDimmer(ObservableExtensionsKt.ui$default(RestAPI.Companion.getApi().setConsent(z2, str), this, null, 2, null), getBinding().g, 100L).k(b.a.d.o.a.g(getContext(), new WidgetSettingsPrivacy$toggleConsent$1(checkedSetting), new Action1<Error>() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$toggleConsent$2
            public final void call(Error error) {
                CheckedSetting.this.setEnabled(true);
                CheckedSetting.this.setChecked(true ^ z2);
            }
        }));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void toggleContactSync(ConnectedAccount connectedAccount, boolean z2) {
        if (connectedAccount != null) {
            StoreStream.Companion companion = StoreStream.Companion;
            StoreUserConnections userConnections = companion.getUserConnections();
            boolean f = connectedAccount.f();
            boolean z3 = true;
            if (connectedAccount.i() != 1) {
                z3 = false;
            }
            userConnections.updateUserConnection(connectedAccount, z2, f, z3);
            if (z2) {
                companion.getContactSync().backgroundUploadContacts();
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateDefaultGuildsRestricted(boolean z2, boolean z3) {
        Observable observable;
        if (!z3) {
            observable = new k(null);
        } else if (!z2) {
            observable = new k(n0.emptySet());
        } else {
            observable = StoreStream.Companion.getGuilds().observeGuilds().F(WidgetSettingsPrivacy$updateDefaultGuildsRestricted$1.INSTANCE);
        }
        Observable Z = observable.Z(1);
        m.checkNotNullExpressionValue(Z, "when {\n      !applyToExi… }\n    }\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Z), this, null, 2, null), WidgetSettingsPrivacy.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsPrivacy$updateDefaultGuildsRestricted$2(this, z2));
        AlertDialog alertDialog = this.dialog;
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateFriendDiscoveryFlags(boolean z2, boolean z3) {
        int i = z2 ? 2 : 0;
        if (z3) {
            i |= 4;
        }
        StoreStream.Companion.getUserSettings().setFriendDiscoveryFlags(getAppActivity(), i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateFriendSourceFlags(int i, boolean z2) {
        boolean z3 = false;
        CheckedSetting checkedSetting = getFriendSourceRadios().get(0);
        m.checkNotNullExpressionValue(checkedSetting, "friendSourceRadios[0]");
        boolean isChecked = checkedSetting.isChecked();
        CheckedSetting checkedSetting2 = getFriendSourceRadios().get(1);
        m.checkNotNullExpressionValue(checkedSetting2, "friendSourceRadios[1]");
        boolean isChecked2 = checkedSetting2.isChecked();
        CheckedSetting checkedSetting3 = getFriendSourceRadios().get(2);
        m.checkNotNullExpressionValue(checkedSetting3, "friendSourceRadios[2]");
        boolean isChecked3 = checkedSetting3.isChecked();
        if ((isChecked && isChecked2 && isChecked3) || (i == 0 && z2)) {
            z3 = true;
        }
        StoreStream.Companion.getUserSettings().setFriendSourceFlags(getAppActivity(), Boolean.valueOf(z3), Boolean.valueOf(isChecked3), Boolean.valueOf(isChecked2));
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        AlertDialog alertDialog = this.dialog;
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    @Override // com.discord.app.AppFragment
    @SuppressLint({"SetTextI18n"})
    public void onViewBound(View view) {
        CharSequence d;
        CharSequence d2;
        CharSequence d3;
        CharSequence g;
        CharSequence d4;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.privacy_and_safety);
        this.radioManagerExplicit = new RadioManager(getExplicitContentRadios());
        CheckedSetting checkedSetting = getBinding().v;
        CheckedSetting checkedSetting2 = getBinding().v;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsPrivacyPersonalization");
        f fVar = f.a;
        d = b.d(checkedSetting2, R.string.data_privacy_controls_personalization_note_learn_more, new Object[]{fVar.a(360004109911L, null)}, (r4 & 4) != 0 ? b.c.j : null);
        checkedSetting.h(d, true);
        CheckedSetting checkedSetting3 = getBinding().f2612y;
        CheckedSetting checkedSetting4 = getBinding().f2612y;
        m.checkNotNullExpressionValue(checkedSetting4, "binding.settingsPrivacyScreenreaderDetection");
        d2 = b.d(checkedSetting4, R.string.data_privacy_controls_allow_accessibility_detection_note, new Object[]{fVar.a(360035966492L, null)}, (r4 & 4) != 0 ? b.c.j : null);
        checkedSetting3.h(d2, true);
        final CheckedSetting checkedSetting5 = getBinding().f2612y;
        checkedSetting5.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$onViewBound$$inlined$apply$lambda$1
            public final void call(Boolean bool) {
                StoreUserSettings storeUserSettings;
                storeUserSettings = this.userSettings;
                m.checkNotNullExpressionValue(bool, "checked");
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(storeUserSettings.setIsAccessibilityDetectionAllowed(bool.booleanValue()), false, 1, null), CheckedSetting.this.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetSettingsPrivacy$onViewBound$1$1$1.INSTANCE);
            }
        });
        getBinding().i.setButtonVisibility(false);
        CheckedSetting checkedSetting6 = getBinding().i;
        CheckedSetting checkedSetting7 = getBinding().i;
        m.checkNotNullExpressionValue(checkedSetting7, "binding.settingsPrivacyBasicService");
        checkedSetting6.h(b.d(checkedSetting7, R.string.data_privacy_controls_basic_service_note, new Object[0], WidgetSettingsPrivacy$onViewBound$2.INSTANCE), true);
        getBinding().i.e(WidgetSettingsPrivacy$onViewBound$3.INSTANCE);
        final String a = fVar.a(360004027692L, null);
        TextView textView = getBinding().h;
        m.checkNotNullExpressionValue(textView, "binding.requestDataLink");
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        TextView textView2 = getBinding().h;
        m.checkNotNullExpressionValue(textView2, "binding.requestDataLink");
        d3 = b.d(textView2, R.string.notice_whats_this, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        sb.append(d3);
        sb.append("](");
        sb.append(a);
        sb.append(')');
        g = b.g(sb.toString(), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
        textView.setText(g);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                UriHandler.handle$default(UriHandler.INSTANCE, a.x(view2, "it", "it.context"), a, null, 4, null);
            }
        });
        CheckedSetting checkedSetting8 = getBinding().f2613z;
        CheckedSetting checkedSetting9 = getBinding().f2613z;
        m.checkNotNullExpressionValue(checkedSetting9, "binding.settingsPrivacyStatistics");
        d4 = b.d(checkedSetting9, R.string.data_privacy_controls_usage_statistics_note, new Object[]{fVar.a(360004109911L, null)}, (r4 & 4) != 0 ? b.c.j : null);
        checkedSetting8.h(d4, true);
        for (TextView textView3 : n.listOf((Object[]) new TextView[]{getBinding().f2611x, getBinding().n, getBinding().u, getBinding().l})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView3, "header");
            accessibilityUtils.setViewIsHeading(textView3);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(), this, null, 2, null), WidgetSettingsPrivacy.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsPrivacy$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(this.userSettings.observeIsAccessibilityDetectionAllowed(), this, null, 2, null), WidgetSettingsPrivacy.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsPrivacy$onViewBoundOrOnResume$2(this));
    }
}
