package com.discord.widgets.settings.developer;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.y.m0.b;
import b.d.b.a.a;
import com.discord.databinding.ExperimentOverridesListItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.views.experiments.ExperimentOverrideView;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: ExperimentOverridesAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0018\u0019B\u0007¢\u0006\u0004\b\u0016\u0010\u0017J\u001b\u0010\u0007\u001a\u00020\u00062\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010\u0015¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$ExperimentViewHolder;", "", "Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;", "items", "", "setData", "(Ljava/util/List;)V", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$ExperimentViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onBindViewHolder", "(Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$ExperimentViewHolder;I)V", "getItemCount", "()I", "Ljava/util/List;", HookHelper.constructorName, "()V", "ExperimentViewHolder", "Item", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ExperimentOverridesAdapter extends RecyclerView.Adapter<ExperimentViewHolder> {
    private List<? extends Item> items = n.emptyList();

    /* compiled from: ExperimentOverridesAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$ExperimentViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;", "item", "", "bind", "(Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;)V", "Lcom/discord/databinding/ExperimentOverridesListItemBinding;", "binding", "Lcom/discord/databinding/ExperimentOverridesListItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/ExperimentOverridesListItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ExperimentViewHolder extends RecyclerView.ViewHolder {
        private final ExperimentOverridesListItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ExperimentViewHolder(ExperimentOverridesListItemBinding experimentOverridesListItemBinding) {
            super(experimentOverridesListItemBinding.a);
            m.checkNotNullParameter(experimentOverridesListItemBinding, "binding");
            this.binding = experimentOverridesListItemBinding;
        }

        public final void bind(Item item) {
            m.checkNotNullParameter(item, "item");
            ExperimentOverrideView experimentOverrideView = this.binding.f2092b;
            String name = item.getName();
            String apiName = item.getApiName();
            Integer overrideBucket = item.getOverrideBucket();
            List<String> bucketDescriptions = item.getBucketDescriptions();
            Function1<Integer, Unit> onOverrideBucketSelected = item.getOnOverrideBucketSelected();
            Function0<Unit> onOverrideBucketCleared = item.getOnOverrideBucketCleared();
            Objects.requireNonNull(experimentOverrideView);
            m.checkNotNullParameter(name, "experimentName");
            m.checkNotNullParameter(apiName, "experimentApiName");
            m.checkNotNullParameter(bucketDescriptions, "bucketDescriptions");
            m.checkNotNullParameter(onOverrideBucketSelected, "onOverrideBucketSelected");
            m.checkNotNullParameter(onOverrideBucketCleared, "onOverrideBucketCleared");
            TextView textView = experimentOverrideView.j.f;
            m.checkNotNullExpressionValue(textView, "binding.experimentOverrideExperimentName");
            textView.setText(name);
            TextView textView2 = experimentOverrideView.j.e;
            m.checkNotNullExpressionValue(textView2, "binding.experimentOverrideExperimentApiName");
            textView2.setText(apiName);
            TextView textView3 = experimentOverrideView.j.f109b;
            m.checkNotNullExpressionValue(textView3, "binding.experimentOverrideBucketDescriptions");
            textView3.setText(u.joinToString$default(bucketDescriptions, "\n", null, null, 0, null, null, 62, null));
            ArrayList arrayList = new ArrayList();
            if (overrideBucket == null) {
                arrayList.add(new ExperimentOverrideView.b(null, "Select..."));
            }
            int size = bucketDescriptions.size();
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(new ExperimentOverrideView.b(Integer.valueOf(i2), a.p("Bucket ", i2)));
            }
            Spinner spinner = experimentOverrideView.j.c;
            m.checkNotNullExpressionValue(spinner, "binding.experimentOverrideBucketsSpinner");
            Context context = experimentOverrideView.getContext();
            m.checkNotNullExpressionValue(context, "context");
            spinner.setAdapter((SpinnerAdapter) new ExperimentOverrideView.a(context, arrayList));
            if (overrideBucket != null) {
                experimentOverrideView.j.c.setSelection(overrideBucket.intValue());
            }
            Spinner spinner2 = experimentOverrideView.j.c;
            m.checkNotNullExpressionValue(spinner2, "binding.experimentOverrideBucketsSpinner");
            spinner2.setOnItemSelectedListener(new b.a.y.m0.a(onOverrideBucketSelected));
            experimentOverrideView.j.d.setOnClickListener(new b(onOverrideBucketCleared));
            TextView textView4 = experimentOverrideView.j.d;
            m.checkNotNullExpressionValue(textView4, "binding.experimentOverrideClear");
            if (!(overrideBucket != null)) {
                i = 8;
            }
            textView4.setVisibility(i);
        }
    }

    /* compiled from: ExperimentOverridesAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u001c\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u001c\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u0012\u001a\u00020\f8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0014\u001a\u00020\f8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0011R\"\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u00158&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;", "", "", "getOverrideBucket", "()Ljava/lang/Integer;", "overrideBucket", "Lkotlin/Function0;", "", "getOnOverrideBucketCleared", "()Lkotlin/jvm/functions/Function0;", "onOverrideBucketCleared", "", "", "getBucketDescriptions", "()Ljava/util/List;", "bucketDescriptions", "getName", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, "getApiName", "apiName", "Lkotlin/Function1;", "getOnOverrideBucketSelected", "()Lkotlin/jvm/functions/Function1;", "onOverrideBucketSelected", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface Item {
        String getApiName();

        List<String> getBucketDescriptions();

        String getName();

        Function0<Unit> getOnOverrideBucketCleared();

        Function1<Integer, Unit> getOnOverrideBucketSelected();

        Integer getOverrideBucket();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    public final void setData(List<? extends Item> list) {
        m.checkNotNullParameter(list, "items");
        this.items = list;
        notifyDataSetChanged();
    }

    public void onBindViewHolder(ExperimentViewHolder experimentViewHolder, int i) {
        m.checkNotNullParameter(experimentViewHolder, "holder");
        experimentViewHolder.bind(this.items.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public ExperimentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.experiment_overrides_list_item, viewGroup, false);
        ExperimentOverrideView experimentOverrideView = (ExperimentOverrideView) inflate.findViewById(R.id.experiment_overrides_list_item_experiment_override_view);
        if (experimentOverrideView != null) {
            ExperimentOverridesListItemBinding experimentOverridesListItemBinding = new ExperimentOverridesListItemBinding((CardView) inflate, experimentOverrideView);
            m.checkNotNullExpressionValue(experimentOverridesListItemBinding, "ExperimentOverridesListI….context), parent, false)");
            return new ExperimentViewHolder(experimentOverridesListItemBinding);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(R.id.experiment_overrides_list_item_experiment_override_view)));
    }
}
