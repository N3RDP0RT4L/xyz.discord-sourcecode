package com.discord.widgets.settings.developer;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.databinding.IconListItemTextViewBinding;
import com.discord.databinding.WidgetSettingsDeveloperBinding;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreStream;
import com.discord.utilities.bugreports.BugReportManager;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.discord.views.CheckedSetting;
import com.hammerandchisel.libdiscord.Discord;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsDeveloper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B\u0007¢\u0006\u0004\b\u0016\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0004J\u000f\u0010\u0007\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0004J\u0017\u0010\n\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\f\u0010\u0004R\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;", "Lcom/discord/app/AppFragment;", "", "setupScreenshotDetector", "()V", "setupNoticesSection", "setupExperimentSection", "setupCrashes", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/databinding/WidgetSettingsDeveloperBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsDeveloperBinding;", "binding", "Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;", "experimentOverridesAdapter", "Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;", HookHelper.constructorName, "Companion", "NoticeViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsDeveloper extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsDeveloper.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsDeveloperBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsDeveloper$binding$2.INSTANCE, null, 2, null);
    private ExperimentOverridesAdapter experimentOverridesAdapter;

    /* compiled from: WidgetSettingsDeveloper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsDeveloper.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsDeveloper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\b\u0012\u00060\u0004j\u0002`\u00050\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u0013\u0010\u0014J'\u0010\b\u001a\u00020\u00072\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u0003\u0012\b\u0012\u00060\u0004j\u0002`\u00050\u0002H\u0017¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\"\u0010\r\u001a\u00020\u00038\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$NoticeViewHolder;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;", "Lkotlin/Pair;", "", "", "Lcom/discord/primitives/Timestamp;", "data", "", "bind", "(Lkotlin/Pair;)V", "Lcom/discord/databinding/IconListItemTextViewBinding;", "binding", "Lcom/discord/databinding/IconListItemTextViewBinding;", "noticeName", "Ljava/lang/String;", "getNoticeName", "()Ljava/lang/String;", "setNoticeName", "(Ljava/lang/String;)V", HookHelper.constructorName, "(Lcom/discord/databinding/IconListItemTextViewBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NoticeViewHolder extends SimpleRecyclerAdapter.ViewHolder<Pair<? extends String, ? extends Long>> {
        private final IconListItemTextViewBinding binding;
        public String noticeName;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public NoticeViewHolder(com.discord.databinding.IconListItemTextViewBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.TextView r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r0)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.developer.WidgetSettingsDeveloper.NoticeViewHolder.<init>(com.discord.databinding.IconListItemTextViewBinding):void");
        }

        @Override // com.discord.utilities.views.SimpleRecyclerAdapter.ViewHolder
        public /* bridge */ /* synthetic */ void bind(Pair<? extends String, ? extends Long> pair) {
            bind2((Pair<String, Long>) pair);
        }

        public final String getNoticeName() {
            String str = this.noticeName;
            if (str == null) {
                m.throwUninitializedPropertyAccessException("noticeName");
            }
            return str;
        }

        public final void setNoticeName(String str) {
            m.checkNotNullParameter(str, "<set-?>");
            this.noticeName = str;
        }

        @SuppressLint({"SetTextI18n"})
        /* renamed from: bind  reason: avoid collision after fix types in other method */
        public void bind2(Pair<String, Long> pair) {
            m.checkNotNullParameter(pair, "data");
            String component1 = pair.component1();
            long longValue = pair.component2().longValue();
            this.noticeName = component1;
            TextView textView = this.binding.a;
            m.checkNotNullExpressionValue(textView, "binding.root");
            Context context = textView.getContext();
            m.checkNotNullExpressionValue(context, "binding.root.context");
            CharSequence readableTimeString$default = TimeUtils.toReadableTimeString$default(context, longValue, null, 4, null);
            TextView textView2 = this.binding.a;
            m.checkNotNullExpressionValue(textView2, "binding.root");
            textView2.setText(component1 + " @ " + readableTimeString$default);
        }
    }

    public WidgetSettingsDeveloper() {
        super(R.layout.widget_settings_developer);
    }

    public static final /* synthetic */ ExperimentOverridesAdapter access$getExperimentOverridesAdapter$p(WidgetSettingsDeveloper widgetSettingsDeveloper) {
        ExperimentOverridesAdapter experimentOverridesAdapter = widgetSettingsDeveloper.experimentOverridesAdapter;
        if (experimentOverridesAdapter == null) {
            m.throwUninitializedPropertyAccessException("experimentOverridesAdapter");
        }
        return experimentOverridesAdapter;
    }

    private final WidgetSettingsDeveloperBinding getBinding() {
        return (WidgetSettingsDeveloperBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void launch(Context context) {
        Companion.launch(context);
    }

    private final void setupCrashes() {
        getBinding().d.setOnClickListener(WidgetSettingsDeveloper$setupCrashes$1.INSTANCE);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupCrashes$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                try {
                    throw new RuntimeException("This is a developer triggered crash (caught).");
                } catch (Exception e) {
                    Logger.e$default(AppLog.g, "setupCrashes", e, null, 4, null);
                    b.a.d.m.j(WidgetSettingsDeveloper.this, "Done.", 0, 4);
                }
            }
        });
        getBinding().f2589b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupCrashes$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Context context = WidgetSettingsDeveloper.this.getContext();
                Objects.requireNonNull(context, "null cannot be cast to non-null type android.content.Context");
                new Discord(context).crash();
            }
        });
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupCrashes$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoreStream.Companion.getGatewaySocket().simulateReconnectForTesting();
                b.a.d.m.j(WidgetSettingsDeveloper.this, "Done.", 0, 4);
            }
        });
    }

    private final void setupExperimentSection() {
        this.experimentOverridesAdapter = new ExperimentOverridesAdapter();
        RecyclerView recyclerView = getBinding().e;
        m.checkNotNullExpressionValue(recyclerView, "binding.developerSettingsExperiments");
        ExperimentOverridesAdapter experimentOverridesAdapter = this.experimentOverridesAdapter;
        if (experimentOverridesAdapter == null) {
            m.throwUninitializedPropertyAccessException("experimentOverridesAdapter");
        }
        recyclerView.setAdapter(experimentOverridesAdapter);
        StoreExperiments experiments = StoreStream.Companion.getExperiments();
        Observable<R> F = experiments.observeOverrides().F(new WidgetSettingsDeveloper$setupExperimentSection$1(experiments));
        m.checkNotNullExpressionValue(F, "experimentStore\n        …              }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(F, this, null, 2, null), WidgetSettingsDeveloper.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsDeveloper$setupExperimentSection$2(this));
    }

    private final void setupNoticesSection() {
        SimpleRecyclerAdapter simpleRecyclerAdapter = new SimpleRecyclerAdapter(null, WidgetSettingsDeveloper$setupNoticesSection$noticesAdapter$1.INSTANCE, 1, null);
        RecyclerView recyclerView = getBinding().f;
        m.checkNotNullExpressionValue(recyclerView, "binding.developerSettingsNotices");
        recyclerView.setAdapter(simpleRecyclerAdapter);
        new WidgetSettingsDeveloper$setupNoticesSection$1(this).invoke().attachToRecyclerView(getBinding().f);
        Observable<R> F = StoreStream.Companion.getNotices().observeNoticesSeen().F(WidgetSettingsDeveloper$setupNoticesSection$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "StoreStream\n        .get…      .toList()\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(F), this, null, 2, null), WidgetSettingsDeveloper.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsDeveloper$setupNoticesSection$3(simpleRecyclerAdapter));
    }

    private final void setupScreenshotDetector() {
        final BugReportManager bugReportManager = BugReportManager.Companion.get();
        CheckedSetting checkedSetting = getBinding().h;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsDeveloperScreenshotBugReporting");
        checkedSetting.setChecked(bugReportManager.isBugReportSettingEnabled());
        getBinding().h.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupScreenshotDetector$1
            public final void call(Boolean bool) {
                BugReportManager bugReportManager2 = BugReportManager.this;
                m.checkNotNullExpressionValue(bool, "isChecked");
                bugReportManager2.setBugReportingSettingEnabled(bool.booleanValue());
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarDisplayHomeAsUpEnabled(true);
        setActionBarTitle(R.string.developer_options);
        setupCrashes();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        setupScreenshotDetector();
        setupExperimentSection();
        setupNoticesSection();
    }
}
