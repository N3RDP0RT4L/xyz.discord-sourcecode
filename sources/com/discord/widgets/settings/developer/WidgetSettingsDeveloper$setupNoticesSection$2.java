package com.discord.widgets.settings.developer;

import androidx.core.app.NotificationCompat;
import d0.t.g0;
import d0.t.i0;
import d0.z.d.m;
import j0.k.b;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: WidgetSettingsDeveloper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001ab\u0012*\u0012(\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001\u0012\u0016\u0012\u0014 \u0004*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u00030\u0007 \u0004*0\u0012*\u0012(\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001\u0012\u0016\u0012\u0014 \u0004*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u00030\u0007\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\u0004\u0012\u00020\u0001\u0012\b\u0012\u00060\u0002j\u0002`\u0003 \u0004*\u0014\u0012\u0004\u0012\u00020\u0001\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Ljava/util/HashMap;", "", "", "Lcom/discord/primitives/Timestamp;", "kotlin.jvm.PlatformType", "noticesSeenMap", "", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/HashMap;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsDeveloper$setupNoticesSection$2<T, R> implements b<HashMap<String, Long>, List<? extends Pair<? extends String, ? extends Long>>> {
    public static final WidgetSettingsDeveloper$setupNoticesSection$2 INSTANCE = new WidgetSettingsDeveloper$setupNoticesSection$2();

    public final List<Pair<String, Long>> call(HashMap<String, Long> hashMap) {
        m.checkNotNullExpressionValue(hashMap, "noticesSeenMap");
        return i0.toList(g0.toSortedMap(hashMap));
    }
}
