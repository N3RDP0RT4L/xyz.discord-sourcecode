package com.discord.widgets.settings.developer;

import com.discord.utilities.views.SimpleRecyclerAdapter;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsDeveloper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072f\u0010\u0006\u001ab\u0012*\u0012(\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00050\u0004j\u0002`\u00050\u0001 \u0003*0\u0012*\u0012(\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00050\u0004j\u0002`\u00050\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "Lkotlin/Pair;", "", "kotlin.jvm.PlatformType", "", "Lcom/discord/primitives/Timestamp;", "noticesSeenMap", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsDeveloper$setupNoticesSection$3 extends o implements Function1<List<? extends Pair<? extends String, ? extends Long>>, Unit> {
    public final /* synthetic */ SimpleRecyclerAdapter $noticesAdapter;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsDeveloper$setupNoticesSection$3(SimpleRecyclerAdapter simpleRecyclerAdapter) {
        super(1);
        this.$noticesAdapter = simpleRecyclerAdapter;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends Pair<? extends String, ? extends Long>> list) {
        invoke2((List<Pair<String, Long>>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Pair<String, Long>> list) {
        SimpleRecyclerAdapter simpleRecyclerAdapter = this.$noticesAdapter;
        m.checkNotNullExpressionValue(list, "noticesSeenMap");
        simpleRecyclerAdapter.setData(list);
    }
}
