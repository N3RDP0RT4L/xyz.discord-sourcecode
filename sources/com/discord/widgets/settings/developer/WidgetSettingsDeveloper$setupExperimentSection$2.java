package com.discord.widgets.settings.developer;

import com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupExperimentSection$1;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsDeveloper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0013\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003*\u0001\u0001\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "com/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$1$1", "kotlin.jvm.PlatformType", "data", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsDeveloper$setupExperimentSection$2 extends o implements Function1<List<? extends WidgetSettingsDeveloper$setupExperimentSection$1.1.1>, Unit> {
    public final /* synthetic */ WidgetSettingsDeveloper this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsDeveloper$setupExperimentSection$2(WidgetSettingsDeveloper widgetSettingsDeveloper) {
        super(1);
        this.this$0 = widgetSettingsDeveloper;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends WidgetSettingsDeveloper$setupExperimentSection$1.1.1> list) {
        invoke2((List<WidgetSettingsDeveloper$setupExperimentSection$1.1.1>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<WidgetSettingsDeveloper$setupExperimentSection$1.1.1> list) {
        ExperimentOverridesAdapter access$getExperimentOverridesAdapter$p = WidgetSettingsDeveloper.access$getExperimentOverridesAdapter$p(this.this$0);
        m.checkNotNullExpressionValue(list, "data");
        access$getExperimentOverridesAdapter$p.setData(list);
    }
}
