package com.discord.widgets.settings.developer;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.views.SwipeableItemTouchHelper;
import com.discord.widgets.settings.developer.WidgetSettingsDeveloper;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetSettingsDeveloper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Landroidx/recyclerview/widget/ItemTouchHelper;", "invoke", "()Landroidx/recyclerview/widget/ItemTouchHelper;", "createSwipeableItemTouchHelper"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsDeveloper$setupNoticesSection$1 extends o implements Function0<ItemTouchHelper> {
    public final /* synthetic */ WidgetSettingsDeveloper this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsDeveloper$setupNoticesSection$1(WidgetSettingsDeveloper widgetSettingsDeveloper) {
        super(0);
        this.this$0 = widgetSettingsDeveloper;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ItemTouchHelper invoke() {
        final SwipeableItemTouchHelper.SwipeRevealConfiguration swipeRevealConfiguration = new SwipeableItemTouchHelper.SwipeRevealConfiguration(ColorCompat.getColor(this.this$0, (int) R.color.status_red_500), ContextCompat.getDrawable(this.this$0.requireContext(), R.drawable.ic_delete_white_24dp), DimenUtils.dpToPixels(8));
        return new ItemTouchHelper(new SwipeableItemTouchHelper(swipeRevealConfiguration, swipeRevealConfiguration) { // from class: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupNoticesSection$1.1
            @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int i) {
                m.checkNotNullParameter(viewHolder, "viewHolder");
                if (viewHolder instanceof WidgetSettingsDeveloper.NoticeViewHolder) {
                    StoreStream.Companion.getNotices().clearSeen(((WidgetSettingsDeveloper.NoticeViewHolder) viewHolder).getNoticeName());
                }
            }
        });
    }
}
