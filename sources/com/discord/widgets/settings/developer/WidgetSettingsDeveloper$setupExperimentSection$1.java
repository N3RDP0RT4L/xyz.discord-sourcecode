package com.discord.widgets.settings.developer;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreExperiments;
import com.discord.utilities.experiments.ExperimentRegistry;
import com.discord.utilities.experiments.RegisteredExperiment;
import com.discord.widgets.settings.developer.ExperimentOverridesAdapter;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsDeveloper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001b\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0004*\u0001\u0006\u0010\t\u001a\u0016\u0012\u0004\u0012\u00020\u0006 \u0003*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\u00052&\u0010\u0004\u001a\"\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002 \u0003*\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "", "kotlin.jvm.PlatformType", "allOverrides", "", "com/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$1$1", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsDeveloper$setupExperimentSection$1<T, R> implements b<Map<String, ? extends Integer>, List<? extends 1.1>> {
    public final /* synthetic */ StoreExperiments $experimentStore;

    public WidgetSettingsDeveloper$setupExperimentSection$1(StoreExperiments storeExperiments) {
        this.$experimentStore = storeExperiments;
    }

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends 1.1> call(Map<String, ? extends Integer> map) {
        return call2((Map<String, Integer>) map);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<1.1> call2(final Map<String, Integer> map) {
        Collection<RegisteredExperiment> values = ExperimentRegistry.INSTANCE.getRegisteredExperiments().values();
        m.checkNotNullExpressionValue(values, "ExperimentRegistry\n     …nts\n              .values");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(values, 10));
        for (final RegisteredExperiment registeredExperiment : values) {
            arrayList.add(new ExperimentOverridesAdapter.Item(this, map) { // from class: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1
                public final /* synthetic */ Map $allOverrides$inlined;
                private final String apiName;
                private final List<String> bucketDescriptions;
                private final String name;
                private final Integer overrideBucket;
                public final /* synthetic */ WidgetSettingsDeveloper$setupExperimentSection$1 this$0;
                private final Function1<Integer, Unit> onOverrideBucketSelected = new AnonymousClass1();
                private final Function0<Unit> onOverrideBucketCleared = new AnonymousClass2();

                /* compiled from: WidgetSettingsDeveloper.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"", "it", "", "invoke", "(I)V", "com/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$1$1$onOverrideBucketSelected$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends d0.z.d.o implements Function1<Integer, Unit> {
                    public AnonymousClass1() {
                        super(1);
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.a;
                    }

                    public final void invoke(int i) {
                        WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1 widgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1 = WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1.this;
                        widgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1.this$0.$experimentStore.setOverride(RegisteredExperiment.this.getName(), i);
                    }
                }

                /* compiled from: WidgetSettingsDeveloper.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()V", "com/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$1$1$onOverrideBucketCleared$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.settings.developer.WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1$2  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass2 extends d0.z.d.o implements Function0<Unit> {
                    public AnonymousClass2() {
                        super(0);
                    }

                    @Override // kotlin.jvm.functions.Function0
                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2() {
                        WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1 widgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1 = WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1.this;
                        widgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1.this$0.$experimentStore.clearOverride(RegisteredExperiment.this.getName());
                    }
                }

                {
                    this.this$0 = this;
                    this.$allOverrides$inlined = map;
                    this.name = RegisteredExperiment.this.getReadableName();
                    this.apiName = RegisteredExperiment.this.getName();
                    this.overrideBucket = (Integer) map.get(RegisteredExperiment.this.getName());
                    this.bucketDescriptions = RegisteredExperiment.this.getBuckets();
                }

                @Override // com.discord.widgets.settings.developer.ExperimentOverridesAdapter.Item
                public String getApiName() {
                    return this.apiName;
                }

                @Override // com.discord.widgets.settings.developer.ExperimentOverridesAdapter.Item
                public List<String> getBucketDescriptions() {
                    return this.bucketDescriptions;
                }

                @Override // com.discord.widgets.settings.developer.ExperimentOverridesAdapter.Item
                public String getName() {
                    return this.name;
                }

                @Override // com.discord.widgets.settings.developer.ExperimentOverridesAdapter.Item
                public Function0<Unit> getOnOverrideBucketCleared() {
                    return this.onOverrideBucketCleared;
                }

                @Override // com.discord.widgets.settings.developer.ExperimentOverridesAdapter.Item
                public Function1<Integer, Unit> getOnOverrideBucketSelected() {
                    return this.onOverrideBucketSelected;
                }

                @Override // com.discord.widgets.settings.developer.ExperimentOverridesAdapter.Item
                public Integer getOverrideBucket() {
                    return this.overrideBucket;
                }
            });
        }
        return arrayList;
    }
}
