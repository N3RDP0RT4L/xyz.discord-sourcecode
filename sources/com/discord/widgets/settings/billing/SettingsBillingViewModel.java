package com.discord.widgets.settings.billing;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelPaymentSource;
import com.discord.models.domain.ModelSubscription;
import com.discord.stores.StorePaymentSources;
import com.discord.stores.StoreSubscriptions;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.settings.billing.PaymentSourceAdapter;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: SettingsBillingViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001c\u001dB+\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0015\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012\u0012\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00060\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ-\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState;", "", "fetchData", "()V", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;)V", "", "Lcom/discord/models/domain/ModelPaymentSource;", "paymentSources", "Lcom/discord/models/domain/ModelSubscription;", "premiumSubscription", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "generateListItems", "(Ljava/util/List;Lcom/discord/models/domain/ModelSubscription;)Ljava/util/List;", "Lcom/discord/stores/StoreSubscriptions;", "storeSubscriptions", "Lcom/discord/stores/StoreSubscriptions;", "Lcom/discord/stores/StorePaymentSources;", "storePaymentSources", "Lcom/discord/stores/StorePaymentSources;", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lrx/Observable;)V", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsBillingViewModel extends AppViewModel<ViewState> {
    private final StorePaymentSources storePaymentSources;
    private final StoreSubscriptions storeSubscriptions;

    /* compiled from: SettingsBillingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\n \u0001*\u0004\u0018\u00010\u00050\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;", "kotlin.jvm.PlatformType", "paymentSourcesState", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "subscriptionsState", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.billing.SettingsBillingViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T1, T2, R> implements Func2<StorePaymentSources.PaymentSourcesState, StoreSubscriptions.SubscriptionsState, StoreState> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final StoreState call(StorePaymentSources.PaymentSourcesState paymentSourcesState, StoreSubscriptions.SubscriptionsState subscriptionsState) {
            m.checkNotNullExpressionValue(paymentSourcesState, "paymentSourcesState");
            m.checkNotNullExpressionValue(subscriptionsState, "subscriptionsState");
            return new StoreState(paymentSourcesState, subscriptionsState);
        }
    }

    /* compiled from: SettingsBillingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.billing.SettingsBillingViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            SettingsBillingViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: SettingsBillingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;", "", "Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;", "component1", "()Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "component2", "()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "paymentSourceState", "subscriptionsState", "copy", "(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;", "getPaymentSourceState", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "getSubscriptionsState", HookHelper.constructorName, "(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StorePaymentSources.PaymentSourcesState paymentSourceState;
        private final StoreSubscriptions.SubscriptionsState subscriptionsState;

        public StoreState(StorePaymentSources.PaymentSourcesState paymentSourcesState, StoreSubscriptions.SubscriptionsState subscriptionsState) {
            m.checkNotNullParameter(paymentSourcesState, "paymentSourceState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            this.paymentSourceState = paymentSourcesState;
            this.subscriptionsState = subscriptionsState;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, StorePaymentSources.PaymentSourcesState paymentSourcesState, StoreSubscriptions.SubscriptionsState subscriptionsState, int i, Object obj) {
            if ((i & 1) != 0) {
                paymentSourcesState = storeState.paymentSourceState;
            }
            if ((i & 2) != 0) {
                subscriptionsState = storeState.subscriptionsState;
            }
            return storeState.copy(paymentSourcesState, subscriptionsState);
        }

        public final StorePaymentSources.PaymentSourcesState component1() {
            return this.paymentSourceState;
        }

        public final StoreSubscriptions.SubscriptionsState component2() {
            return this.subscriptionsState;
        }

        public final StoreState copy(StorePaymentSources.PaymentSourcesState paymentSourcesState, StoreSubscriptions.SubscriptionsState subscriptionsState) {
            m.checkNotNullParameter(paymentSourcesState, "paymentSourceState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            return new StoreState(paymentSourcesState, subscriptionsState);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.paymentSourceState, storeState.paymentSourceState) && m.areEqual(this.subscriptionsState, storeState.subscriptionsState);
        }

        public final StorePaymentSources.PaymentSourcesState getPaymentSourceState() {
            return this.paymentSourceState;
        }

        public final StoreSubscriptions.SubscriptionsState getSubscriptionsState() {
            return this.subscriptionsState;
        }

        public int hashCode() {
            StorePaymentSources.PaymentSourcesState paymentSourcesState = this.paymentSourceState;
            int i = 0;
            int hashCode = (paymentSourcesState != null ? paymentSourcesState.hashCode() : 0) * 31;
            StoreSubscriptions.SubscriptionsState subscriptionsState = this.subscriptionsState;
            if (subscriptionsState != null) {
                i = subscriptionsState.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(paymentSourceState=");
            R.append(this.paymentSourceState);
            R.append(", subscriptionsState=");
            R.append(this.subscriptionsState);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: SettingsBillingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Failure", "Loaded", "Loading", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Failure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: SettingsBillingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Failure;", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failure extends ViewState {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: SettingsBillingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState;", "", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "component1", "()Ljava/util/List;", "paymentSourceItems", "copy", "(Ljava/util/List;)Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getPaymentSourceItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final List<PaymentSourceAdapter.Item> paymentSourceItems;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(List<? extends PaymentSourceAdapter.Item> list) {
                super(null);
                m.checkNotNullParameter(list, "paymentSourceItems");
                this.paymentSourceItems = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.paymentSourceItems;
                }
                return loaded.copy(list);
            }

            public final List<PaymentSourceAdapter.Item> component1() {
                return this.paymentSourceItems;
            }

            public final Loaded copy(List<? extends PaymentSourceAdapter.Item> list) {
                m.checkNotNullParameter(list, "paymentSourceItems");
                return new Loaded(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.paymentSourceItems, ((Loaded) obj).paymentSourceItems);
                }
                return true;
            }

            public final List<PaymentSourceAdapter.Item> getPaymentSourceItems() {
                return this.paymentSourceItems;
            }

            public int hashCode() {
                List<PaymentSourceAdapter.Item> list = this.paymentSourceItems;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("Loaded(paymentSourceItems="), this.paymentSourceItems, ")");
            }
        }

        /* compiled from: SettingsBillingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public SettingsBillingViewModel() {
        this(null, null, null, 7, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ SettingsBillingViewModel(com.discord.stores.StorePaymentSources r1, com.discord.stores.StoreSubscriptions r2, rx.Observable r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
        /*
            r0 = this;
            r5 = r4 & 1
            if (r5 == 0) goto La
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePaymentSources r1 = r1.getPaymentSources()
        La:
            r5 = r4 & 2
            if (r5 == 0) goto L14
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreSubscriptions r2 = r2.getSubscriptions()
        L14:
            r4 = r4 & 4
            if (r4 == 0) goto L35
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePaymentSources r4 = r3.getPaymentSources()
            rx.Observable r4 = r4.observePaymentSourcesState()
            com.discord.stores.StoreSubscriptions r3 = r3.getSubscriptions()
            rx.Observable r3 = r3.observeSubscriptions()
            com.discord.widgets.settings.billing.SettingsBillingViewModel$1 r5 = com.discord.widgets.settings.billing.SettingsBillingViewModel.AnonymousClass1.INSTANCE
            rx.Observable r3 = rx.Observable.j(r4, r3, r5)
            java.lang.String r4 = "Observable\n      .combin…nsState\n        )\n      }"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
        L35:
            r0.<init>(r1, r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.billing.SettingsBillingViewModel.<init>(com.discord.stores.StorePaymentSources, com.discord.stores.StoreSubscriptions, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void fetchData() {
        this.storePaymentSources.fetchPaymentSources();
        this.storeSubscriptions.fetchSubscriptions();
    }

    private final List<PaymentSourceAdapter.Item> generateListItems(List<? extends ModelPaymentSource> list, ModelSubscription modelSubscription) {
        PaymentSourceAdapter.PaymentSourceHeader.Type type;
        ArrayList arrayList = new ArrayList();
        if (!list.isEmpty()) {
            int i = 0;
            for (Object obj : list) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                ModelPaymentSource modelPaymentSource = (ModelPaymentSource) obj;
                String str = null;
                if (i != 0) {
                    type = i != 1 ? null : PaymentSourceAdapter.PaymentSourceHeader.Type.OTHER;
                } else {
                    type = PaymentSourceAdapter.PaymentSourceHeader.Type.DEFAULT;
                }
                PaymentSourceAdapter.PaymentSourceHeader paymentSourceHeader = type != null ? new PaymentSourceAdapter.PaymentSourceHeader(type) : null;
                if (paymentSourceHeader != null) {
                    arrayList.add(paymentSourceHeader);
                }
                String id2 = modelPaymentSource.getId();
                if (modelSubscription != null) {
                    str = modelSubscription.getPaymentSourceId();
                }
                arrayList.add(new PaymentSourceAdapter.PaymentSourceItem(modelPaymentSource, m.areEqual(id2, str) && !modelSubscription.getStatus().isCanceled()));
            }
            arrayList.add(new PaymentSourceAdapter.PaymentSourceAddItem());
        }
        return arrayList;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Object obj;
        boolean z2;
        StorePaymentSources.PaymentSourcesState paymentSourceState = storeState.getPaymentSourceState();
        StoreSubscriptions.SubscriptionsState subscriptionsState = storeState.getSubscriptionsState();
        if ((paymentSourceState instanceof StorePaymentSources.PaymentSourcesState.Loaded) && (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Loaded)) {
            Iterator<T> it = ((StoreSubscriptions.SubscriptionsState.Loaded) subscriptionsState).getSubscriptions().iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((ModelSubscription) obj).getType() == ModelSubscription.Type.PREMIUM) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            updateViewState(new ViewState.Loaded(generateListItems(((StorePaymentSources.PaymentSourcesState.Loaded) paymentSourceState).getPaymentSources(), (ModelSubscription) obj)));
        } else if ((paymentSourceState instanceof StorePaymentSources.PaymentSourcesState.Failure) || (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Failure)) {
            updateViewState(ViewState.Failure.INSTANCE);
        } else {
            updateViewState(ViewState.Loading.INSTANCE);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsBillingViewModel(StorePaymentSources storePaymentSources, StoreSubscriptions storeSubscriptions, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(storePaymentSources, "storePaymentSources");
        m.checkNotNullParameter(storeSubscriptions, "storeSubscriptions");
        m.checkNotNullParameter(observable, "storeObservable");
        this.storePaymentSources = storePaymentSources;
        this.storeSubscriptions = storeSubscriptions;
        fetchData();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), SettingsBillingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
