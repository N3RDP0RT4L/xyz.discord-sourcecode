package com.discord.widgets.settings.billing;

import androidx.core.app.NotificationCompat;
import com.discord.widgets.settings.billing.PaymentSourceAdapter;
import com.discord.widgets.settings.billing.SettingsBillingViewModel;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: WidgetPaymentSourceEditDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Loaded;", "kotlin.jvm.PlatformType", "it", "", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$ViewState$Loaded;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPaymentSourceEditDialog$onViewBound$1<T, R> implements b<SettingsBillingViewModel.ViewState.Loaded, List<? extends PaymentSourceAdapter.PaymentSourceItem>> {
    public static final WidgetPaymentSourceEditDialog$onViewBound$1 INSTANCE = new WidgetPaymentSourceEditDialog$onViewBound$1();

    public final List<PaymentSourceAdapter.PaymentSourceItem> call(SettingsBillingViewModel.ViewState.Loaded loaded) {
        List<PaymentSourceAdapter.Item> paymentSourceItems = loaded.getPaymentSourceItems();
        ArrayList arrayList = new ArrayList();
        for (PaymentSourceAdapter.Item item : paymentSourceItems) {
            if (!(item instanceof PaymentSourceAdapter.PaymentSourceItem)) {
                item = null;
            }
            PaymentSourceAdapter.PaymentSourceItem paymentSourceItem = (PaymentSourceAdapter.PaymentSourceItem) item;
            if (paymentSourceItem != null) {
                arrayList.add(paymentSourceItem);
            }
        }
        return arrayList;
    }
}
