package com.discord.widgets.settings.billing;

import android.os.Bundle;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetPaymentSourceEditDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPaymentSourceEditDialog$paymentSourceId$2 extends o implements Function0<String> {
    public final /* synthetic */ WidgetPaymentSourceEditDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetPaymentSourceEditDialog$paymentSourceId$2(WidgetPaymentSourceEditDialog widgetPaymentSourceEditDialog) {
        super(0);
        this.this$0 = widgetPaymentSourceEditDialog;
    }

    @Override // kotlin.jvm.functions.Function0
    public final String invoke() {
        String str;
        Bundle arguments = this.this$0.getArguments();
        if (arguments == null || (str = arguments.getString("ARG_PAYMENT_SOURCE_ID")) == null) {
            str = "";
        }
        m.checkNotNullExpressionValue(str, "arguments?.getString(ARG_PAYMENT_SOURCE_ID) ?: \"\"");
        return str;
    }
}
