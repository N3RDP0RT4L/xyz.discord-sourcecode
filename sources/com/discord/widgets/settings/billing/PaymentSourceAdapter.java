package com.discord.widgets.settings.billing;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.PaymentMethodListItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPaymentSource;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.widgets.settings.billing.PaymentSourceAdapter;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: PaymentSourceAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 \u00162\b\u0012\u0004\u0012\u00020\u00020\u0001:\b\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001dB1\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\f0\u000f\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n¢\u0006\u0004\b\u0014\u0010\u0015J)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR\"\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u001c\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\f0\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function1;", "Lcom/discord/models/domain/ModelPaymentSource;", "", "onEditPaymentSource", "Lkotlin/jvm/functions/Function1;", "Lkotlin/Function0;", "onAddClick", "Lkotlin/jvm/functions/Function0;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "Companion", "Item", "PaymentSourceAddItem", "PaymentSourceAddViewHolder", "PaymentSourceHeader", "PaymentSourceHeaderViewHolder", "PaymentSourceItem", "PaymentSourceItemViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PaymentSourceAdapter extends MGRecyclerAdapterSimple<Item> {
    public static final Companion Companion = new Companion(null);
    private static final int VIEW_TYPE_HEADER = 2;
    private static final int VIEW_TYPE_PAYMENT_ADD = 1;
    private static final int VIEW_TYPE_PAYMENT_SOURCE = 0;
    private final Function0<Unit> onAddClick;
    private final Function1<ModelPaymentSource, Unit> onEditPaymentSource;

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Companion;", "", "", "VIEW_TYPE_HEADER", "I", "VIEW_TYPE_PAYMENT_ADD", "VIEW_TYPE_PAYMENT_SOURCE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001¨\u0006\u0002"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface Item extends MGRecyclerDataPayload {
    }

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u00078\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceAddItem;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "", "type", "I", "getType", "()I", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PaymentSourceAddItem implements Item {
        private final int type = 1;
        private final String key = "AddPaymentSource";

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }
    }

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceAddViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;)V", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PaymentSourceAddViewHolder extends MGRecyclerViewHolder<PaymentSourceAdapter, Item> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PaymentSourceAddViewHolder(PaymentSourceAdapter paymentSourceAdapter) {
            super((int) R.layout.payment_method_list_add_item, paymentSourceAdapter);
            m.checkNotNullParameter(paymentSourceAdapter, "adapter");
        }

        public static final /* synthetic */ PaymentSourceAdapter access$getAdapter$p(PaymentSourceAddViewHolder paymentSourceAddViewHolder) {
            return (PaymentSourceAdapter) paymentSourceAddViewHolder.adapter;
        }

        public void onConfigure(int i, Item item) {
            m.checkNotNullParameter(item, "data");
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.billing.PaymentSourceAdapter$PaymentSourceAddViewHolder$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function0 function0;
                    function0 = PaymentSourceAdapter.PaymentSourceAddViewHolder.access$getAdapter$p(PaymentSourceAdapter.PaymentSourceAddViewHolder.this).onAddClick;
                    function0.invoke();
                }
            });
        }
    }

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001dB\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001c\u0010\u0015\u001a\u00020\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\rR\u001c\u0010\u0018\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\n¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader$Type;", "component1", "()Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader$Type;", "headerType", "copy", "(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader$Type;)Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader$Type;", "getHeaderType", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader$Type;)V", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PaymentSourceHeader implements Item {
        private final Type headerType;
        private final String key;
        private final int type = 2;

        /* compiled from: PaymentSourceAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeader$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "DEFAULT", "OTHER", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum Type {
            DEFAULT,
            OTHER
        }

        public PaymentSourceHeader(Type type) {
            m.checkNotNullParameter(type, "headerType");
            this.headerType = type;
            this.key = "headerType" + type;
        }

        public static /* synthetic */ PaymentSourceHeader copy$default(PaymentSourceHeader paymentSourceHeader, Type type, int i, Object obj) {
            if ((i & 1) != 0) {
                type = paymentSourceHeader.headerType;
            }
            return paymentSourceHeader.copy(type);
        }

        public final Type component1() {
            return this.headerType;
        }

        public final PaymentSourceHeader copy(Type type) {
            m.checkNotNullParameter(type, "headerType");
            return new PaymentSourceHeader(type);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof PaymentSourceHeader) && m.areEqual(this.headerType, ((PaymentSourceHeader) obj).headerType);
            }
            return true;
        }

        public final Type getHeaderType() {
            return this.headerType;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            Type type = this.headerType;
            if (type != null) {
                return type.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("PaymentSourceHeader(headerType=");
            R.append(this.headerType);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceHeaderViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;)V", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PaymentSourceHeaderViewHolder extends MGRecyclerViewHolder<PaymentSourceAdapter, Item> {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                PaymentSourceHeader.Type.values();
                int[] iArr = new int[2];
                $EnumSwitchMapping$0 = iArr;
                iArr[PaymentSourceHeader.Type.DEFAULT.ordinal()] = 1;
                iArr[PaymentSourceHeader.Type.OTHER.ordinal()] = 2;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PaymentSourceHeaderViewHolder(PaymentSourceAdapter paymentSourceAdapter) {
            super((int) R.layout.payment_method_list_header, paymentSourceAdapter);
            m.checkNotNullParameter(paymentSourceAdapter, "adapter");
        }

        public void onConfigure(int i, Item item) {
            int i2;
            m.checkNotNullParameter(item, "data");
            View view = this.itemView;
            Objects.requireNonNull(view, "null cannot be cast to non-null type android.widget.TextView");
            TextView textView = (TextView) view;
            int ordinal = ((PaymentSourceHeader) item).getHeaderType().ordinal();
            if (ordinal == 0) {
                i2 = R.string._default;
            } else if (ordinal == 1) {
                i2 = R.string.other_options;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            textView.setText(i2);
        }
    }

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001c\u0010\u0018\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u000eR\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001b\u001a\u0004\b\t\u0010\u0007R\u001c\u0010\u001c\u001a\u00020\u000f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u0011¨\u0006!"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "Lcom/discord/models/domain/ModelPaymentSource;", "component1", "()Lcom/discord/models/domain/ModelPaymentSource;", "", "component2", "()Z", "paymentSource", "isPremium", "copy", "(Lcom/discord/models/domain/ModelPaymentSource;Z)Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelPaymentSource;", "getPaymentSource", "key", "Ljava/lang/String;", "getKey", "Z", "type", "I", "getType", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelPaymentSource;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PaymentSourceItem implements Item {
        private final boolean isPremium;
        private final String key;
        private final ModelPaymentSource paymentSource;
        private final int type;

        public PaymentSourceItem(ModelPaymentSource modelPaymentSource, boolean z2) {
            m.checkNotNullParameter(modelPaymentSource, "paymentSource");
            this.paymentSource = modelPaymentSource;
            this.isPremium = z2;
            this.key = modelPaymentSource.getId();
        }

        public static /* synthetic */ PaymentSourceItem copy$default(PaymentSourceItem paymentSourceItem, ModelPaymentSource modelPaymentSource, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                modelPaymentSource = paymentSourceItem.paymentSource;
            }
            if ((i & 2) != 0) {
                z2 = paymentSourceItem.isPremium;
            }
            return paymentSourceItem.copy(modelPaymentSource, z2);
        }

        public final ModelPaymentSource component1() {
            return this.paymentSource;
        }

        public final boolean component2() {
            return this.isPremium;
        }

        public final PaymentSourceItem copy(ModelPaymentSource modelPaymentSource, boolean z2) {
            m.checkNotNullParameter(modelPaymentSource, "paymentSource");
            return new PaymentSourceItem(modelPaymentSource, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PaymentSourceItem)) {
                return false;
            }
            PaymentSourceItem paymentSourceItem = (PaymentSourceItem) obj;
            return m.areEqual(this.paymentSource, paymentSourceItem.paymentSource) && this.isPremium == paymentSourceItem.isPremium;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final ModelPaymentSource getPaymentSource() {
            return this.paymentSource;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            ModelPaymentSource modelPaymentSource = this.paymentSource;
            int hashCode = (modelPaymentSource != null ? modelPaymentSource.hashCode() : 0) * 31;
            boolean z2 = this.isPremium;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public final boolean isPremium() {
            return this.isPremium;
        }

        public String toString() {
            StringBuilder R = a.R("PaymentSourceItem(paymentSource=");
            R.append(this.paymentSource);
            R.append(", isPremium=");
            return a.M(R, this.isPremium, ")");
        }
    }

    /* compiled from: PaymentSourceAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItemViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/billing/PaymentSourceAdapter$Item;)V", "Lcom/discord/databinding/PaymentMethodListItemBinding;", "binding", "Lcom/discord/databinding/PaymentMethodListItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PaymentSourceItemViewHolder extends MGRecyclerViewHolder<PaymentSourceAdapter, Item> {
        private final PaymentMethodListItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PaymentSourceItemViewHolder(PaymentSourceAdapter paymentSourceAdapter) {
            super((int) R.layout.payment_method_list_item, paymentSourceAdapter);
            m.checkNotNullParameter(paymentSourceAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.payment_method_edit;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.payment_method_edit);
            if (materialButton != null) {
                i = R.id.payment_method_summary;
                PaymentSourceView paymentSourceView = (PaymentSourceView) view.findViewById(R.id.payment_method_summary);
                if (paymentSourceView != null) {
                    PaymentMethodListItemBinding paymentMethodListItemBinding = new PaymentMethodListItemBinding((RelativeLayout) view, materialButton, paymentSourceView);
                    m.checkNotNullExpressionValue(paymentMethodListItemBinding, "PaymentMethodListItemBinding.bind(itemView)");
                    this.binding = paymentMethodListItemBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ PaymentSourceAdapter access$getAdapter$p(PaymentSourceItemViewHolder paymentSourceItemViewHolder) {
            return (PaymentSourceAdapter) paymentSourceItemViewHolder.adapter;
        }

        public void onConfigure(int i, Item item) {
            m.checkNotNullParameter(item, "data");
            PaymentSourceItem paymentSourceItem = (PaymentSourceItem) item;
            final ModelPaymentSource paymentSource = paymentSourceItem.getPaymentSource();
            this.binding.c.bind(paymentSource, paymentSourceItem.isPremium());
            this.binding.f2117b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.billing.PaymentSourceAdapter$PaymentSourceItemViewHolder$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1 function1;
                    function1 = PaymentSourceAdapter.PaymentSourceItemViewHolder.access$getAdapter$p(PaymentSourceAdapter.PaymentSourceItemViewHolder.this).onEditPaymentSource;
                    function1.invoke(paymentSource);
                }
            });
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public PaymentSourceAdapter(RecyclerView recyclerView, Function0<Unit> function0, Function1<? super ModelPaymentSource, Unit> function1) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(function0, "onAddClick");
        m.checkNotNullParameter(function1, "onEditPaymentSource");
        this.onAddClick = function0;
        this.onEditPaymentSource = function1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new PaymentSourceItemViewHolder(this);
        }
        if (i == 1) {
            return new PaymentSourceAddViewHolder(this);
        }
        if (i == 2) {
            return new PaymentSourceHeaderViewHolder(this);
        }
        throw new IllegalArgumentException(a.p("unknown type ", i));
    }
}
