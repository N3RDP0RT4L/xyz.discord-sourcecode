package com.discord.widgets.settings.billing;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.n;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetPaymentSourceEditDialogBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPaymentSource;
import com.discord.models.domain.PatchPaymentSourceRaw;
import com.discord.models.domain.billing.ModelBillingAddress;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.validators.BasicTextInputValidator;
import com.discord.utilities.view.validators.Input;
import com.discord.utilities.view.validators.InputValidator;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.settings.billing.PaymentSourceAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.g0.t;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetPaymentSourceEditDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 =2\u00020\u0001:\u0002=>B\u0007¢\u0006\u0004\b<\u0010\u0016J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u001d\u0010\u000b\u001a\u00020\u00042\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0011\u0010\u0010J+\u0010\u0013\u001a\u0016\u0012\u0004\u0012\u00020\t \u0012*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b0\b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001bH\u0007¢\u0006\u0004\b\u001d\u0010\u001eR\u001d\u0010$\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R7\u0010)\u001a\u0016\u0012\u0004\u0012\u00020\t \u0012*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b0\b8B@\u0002X\u0082\u0084\u0002¢\u0006\u0012\n\u0004\b%\u0010!\u0012\u0004\b(\u0010\u0016\u001a\u0004\b&\u0010'R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b+\u0010,R\u001d\u00101\u001a\u00020-8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010!\u001a\u0004\b/\u00100R\u001d\u00107\u001a\u0002028B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b3\u00104\u001a\u0004\b5\u00106R7\u0010;\u001a\u0016\u0012\u0004\u0012\u00020\t \u0012*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b0\b8B@\u0002X\u0082\u0084\u0002¢\u0006\u0012\n\u0004\b8\u0010!\u0012\u0004\b:\u0010\u0016\u001a\u0004\b9\u0010'¨\u0006?"}, d2 = {"Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;", "Lcom/discord/app/AppDialog;", "Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;", "paymentSourceItem", "", "initValidator", "(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;)V", "initPaymentSourceInfo", "", "Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;", "states", "selectState", "([Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;)V", "Lcom/discord/models/domain/ModelPaymentSource;", "paymentSource", "updatePaymentSource", "(Lcom/discord/models/domain/ModelPaymentSource;)V", "deletePaymentSource", "kotlin.jvm.PlatformType", "getStatesFor", "(Lcom/discord/models/domain/ModelPaymentSource;)[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;", "onStart", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/utilities/error/Error;", "error", "handleError", "(Lcom/discord/utilities/error/Error;)V", "", "paymentSourceId$delegate", "Lkotlin/Lazy;", "getPaymentSourceId", "()Ljava/lang/String;", "paymentSourceId", "usStates$delegate", "getUsStates", "()[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;", "getUsStates$annotations", "usStates", "Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager", "Lcom/discord/utilities/view/validators/ValidationManager;", "Lcom/discord/widgets/settings/billing/SettingsBillingViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/settings/billing/SettingsBillingViewModel;", "viewModel", "Lcom/discord/databinding/WidgetPaymentSourceEditDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetPaymentSourceEditDialogBinding;", "binding", "caProvinces$delegate", "getCaProvinces", "getCaProvinces$annotations", "caProvinces", HookHelper.constructorName, "Companion", "StateEntry", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPaymentSourceEditDialog extends AppDialog {
    private static final String ARG_PAYMENT_SOURCE_ID = "ARG_PAYMENT_SOURCE_ID";
    private ValidationManager validationManager;
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetPaymentSourceEditDialog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetPaymentSourceEditDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetPaymentSourceEditDialog$binding$2.INSTANCE, null, 2, null);
    private final Lazy paymentSourceId$delegate = g.lazy(new WidgetPaymentSourceEditDialog$paymentSourceId$2(this));
    private final Lazy usStates$delegate = g.lazy(new WidgetPaymentSourceEditDialog$usStates$2(this));
    private final Lazy caProvinces$delegate = g.lazy(new WidgetPaymentSourceEditDialog$caProvinces$2(this));

    /* compiled from: WidgetPaymentSourceEditDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001d\u0010\n\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;", "", "Lcom/google/android/material/textfield/TextInputLayout;", "", "getTextOrEmpty", "(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "paymentSourceId", "", "launch", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V", WidgetPaymentSourceEditDialog.ARG_PAYMENT_SOURCE_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final String getTextOrEmpty(TextInputLayout textInputLayout) {
            Editable text;
            EditText editText = textInputLayout.getEditText();
            String obj = (editText == null || (text = editText.getText()) == null) ? null : text.toString();
            return obj != null ? obj : "";
        }

        public final void launch(FragmentManager fragmentManager, String str) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(str, "paymentSourceId");
            WidgetPaymentSourceEditDialog widgetPaymentSourceEditDialog = new WidgetPaymentSourceEditDialog();
            Bundle bundle = new Bundle();
            bundle.putString(WidgetPaymentSourceEditDialog.ARG_PAYMENT_SOURCE_ID, str);
            widgetPaymentSourceEditDialog.setArguments(bundle);
            widgetPaymentSourceEditDialog.show(fragmentManager, "javaClass");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetPaymentSourceEditDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;", "", "", "component1", "()Ljava/lang/String;", "component2", "label", "value", "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getValue", "getLabel", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StateEntry {
        private final String label;
        private final String value;

        public StateEntry(String str, String str2) {
            m.checkNotNullParameter(str, "label");
            m.checkNotNullParameter(str2, "value");
            this.label = str;
            this.value = str2;
        }

        public static /* synthetic */ StateEntry copy$default(StateEntry stateEntry, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = stateEntry.label;
            }
            if ((i & 2) != 0) {
                str2 = stateEntry.value;
            }
            return stateEntry.copy(str, str2);
        }

        public final String component1() {
            return this.label;
        }

        public final String component2() {
            return this.value;
        }

        public final StateEntry copy(String str, String str2) {
            m.checkNotNullParameter(str, "label");
            m.checkNotNullParameter(str2, "value");
            return new StateEntry(str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StateEntry)) {
                return false;
            }
            StateEntry stateEntry = (StateEntry) obj;
            return m.areEqual(this.label, stateEntry.label) && m.areEqual(this.value, stateEntry.value);
        }

        public final String getLabel() {
            return this.label;
        }

        public final String getValue() {
            return this.value;
        }

        public int hashCode() {
            String str = this.label;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.value;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StateEntry(label=");
            R.append(this.label);
            R.append(", value=");
            return a.H(R, this.value, ")");
        }
    }

    public WidgetPaymentSourceEditDialog() {
        super(R.layout.widget_payment_source_edit_dialog);
        WidgetPaymentSourceEditDialog$viewModel$2 widgetPaymentSourceEditDialog$viewModel$2 = WidgetPaymentSourceEditDialog$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(SettingsBillingViewModel.class), new WidgetPaymentSourceEditDialog$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetPaymentSourceEditDialog$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void deletePaymentSource(ModelPaymentSource modelPaymentSource) {
        getBinding().d.setIsLoading(true);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deletePaymentSource(modelPaymentSource.getId()), false, 1, null), this, null, 2, null), WidgetPaymentSourceEditDialog.class, (r18 & 2) != 0 ? null : requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetPaymentSourceEditDialog$deletePaymentSource$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : new WidgetPaymentSourceEditDialog$deletePaymentSource$3(this), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPaymentSourceEditDialog$deletePaymentSource$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetPaymentSourceEditDialogBinding getBinding() {
        return (WidgetPaymentSourceEditDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final StateEntry[] getCaProvinces() {
        return (StateEntry[]) this.caProvinces$delegate.getValue();
    }

    private static /* synthetic */ void getCaProvinces$annotations() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getPaymentSourceId() {
        return (String) this.paymentSourceId$delegate.getValue();
    }

    private final StateEntry[] getStatesFor(ModelPaymentSource modelPaymentSource) {
        String country = modelPaymentSource.getBillingAddress().getCountry();
        int hashCode = country.hashCode();
        if (hashCode != 2142) {
            if (hashCode == 2718 && country.equals("US")) {
                return getUsStates();
            }
        } else if (country.equals("CA")) {
            return getCaProvinces();
        }
        return new StateEntry[0];
    }

    private final StateEntry[] getUsStates() {
        return (StateEntry[]) this.usStates$delegate.getValue();
    }

    private static /* synthetic */ void getUsStates$annotations() {
    }

    private final SettingsBillingViewModel getViewModel() {
        return (SettingsBillingViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void initPaymentSourceInfo(PaymentSourceAdapter.PaymentSourceItem paymentSourceItem) {
        StateEntry stateEntry;
        String str;
        CharSequence e;
        CharSequence e2;
        initValidator(paymentSourceItem);
        final ModelPaymentSource component1 = paymentSourceItem.component1();
        boolean component2 = paymentSourceItem.component2();
        getBinding().o.bind(component1, component2);
        TextView textView = getBinding().k;
        m.checkNotNullExpressionValue(textView, "binding.paymentSourceEditHelp");
        boolean z2 = false;
        textView.setText(component1 instanceof ModelPaymentSource.ModelPaymentSourcePaypal ? b.e(this, R.string.payment_source_edit_help_paypal, new Object[]{"https://www.paypal.com"}, (r4 & 4) != 0 ? b.a.j : null) : b.e(this, R.string.payment_source_edit_help_card, new Object[0], (r4 & 4) != 0 ? b.a.j : null));
        StateEntry[] statesFor = getStatesFor(component1);
        ModelBillingAddress billingAddress = component1.getBillingAddress();
        TextInputLayout textInputLayout = getBinding().l;
        m.checkNotNullExpressionValue(textInputLayout, "binding.paymentSourceEditName");
        ViewExtensions.setText(textInputLayout, billingAddress.getName());
        TextInputLayout textInputLayout2 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.paymentSourceEditAddress1");
        ViewExtensions.setText(textInputLayout2, billingAddress.getLine_1());
        TextInputLayout textInputLayout3 = getBinding().f;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.paymentSourceEditAddress2");
        ViewExtensions.setText(textInputLayout3, billingAddress.getLine_2());
        TextInputLayout textInputLayout4 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.paymentSourceEditCity");
        ViewExtensions.setText(textInputLayout4, billingAddress.getCity());
        TextInputLayout textInputLayout5 = getBinding().m;
        m.checkNotNullExpressionValue(textInputLayout5, "binding.paymentSourceEditPostalCode");
        ViewExtensions.setText(textInputLayout5, billingAddress.getPostalCode());
        TextInputLayout textInputLayout6 = getBinding().n;
        m.checkNotNullExpressionValue(textInputLayout6, "binding.paymentSourceEditState");
        m.checkNotNullExpressionValue(statesFor, "states");
        int length = statesFor.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                stateEntry = null;
                break;
            }
            stateEntry = statesFor[i];
            if (m.areEqual(stateEntry.getValue(), billingAddress.getState())) {
                break;
            }
            i++;
        }
        if (stateEntry == null || (str = stateEntry.getLabel()) == null) {
            str = billingAddress.getState();
        }
        ViewExtensions.setText(textInputLayout6, str);
        TextInputLayout textInputLayout7 = getBinding().h;
        m.checkNotNullExpressionValue(textInputLayout7, "binding.paymentSourceEditCountry");
        ViewExtensions.setText(textInputLayout7, billingAddress.getCountry());
        CheckBox checkBox = getBinding().i;
        m.checkNotNullExpressionValue(checkBox, "binding.paymentSourceEditDefault");
        checkBox.setChecked(component1.getDefault());
        getBinding().d.setIsLoading(false);
        if (component2) {
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.billing.WidgetPaymentSourceEditDialog$initPaymentSourceInfo$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    CharSequence e3;
                    CharSequence e4;
                    CharSequence e5;
                    WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
                    FragmentManager parentFragmentManager = WidgetPaymentSourceEditDialog.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    e3 = b.e(WidgetPaymentSourceEditDialog.this, R.string.payment_source_delete, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                    e4 = b.e(WidgetPaymentSourceEditDialog.this, R.string.payment_source_delete_disabled_tooltip, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                    e5 = b.e(WidgetPaymentSourceEditDialog.this, R.string.okay, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                    WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, e3, e4, e5, "", null, null, null, null, null, null, null, 0, null, 16352, null);
                }
            });
            MaterialButton materialButton = getBinding().c;
            m.checkNotNullExpressionValue(materialButton, "binding.dialogDelete");
            materialButton.setAlpha(0.3f);
        } else {
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.billing.WidgetPaymentSourceEditDialog$initPaymentSourceInfo$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetPaymentSourceEditDialog.this.deletePaymentSource(component1);
                }
            });
        }
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.billing.WidgetPaymentSourceEditDialog$initPaymentSourceInfo$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetPaymentSourceEditDialog.this.updatePaymentSource(component1);
            }
        });
        if (m.areEqual(component1.getBillingAddress().getCountry(), "CA")) {
            TextInputLayout textInputLayout8 = getBinding().n;
            m.checkNotNullExpressionValue(textInputLayout8, "binding.paymentSourceEditState");
            e = b.e(this, R.string.billing_address_province, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            textInputLayout8.setHint(e);
            TextInputLayout textInputLayout9 = getBinding().m;
            m.checkNotNullExpressionValue(textInputLayout9, "binding.paymentSourceEditPostalCode");
            e2 = b.e(this, R.string.billing_address_postal_code, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            textInputLayout9.setHint(e2);
        }
        if (statesFor.length == 0) {
            z2 = true;
        }
        if (z2) {
            TextInputLayout textInputLayout10 = getBinding().n;
            m.checkNotNullExpressionValue(textInputLayout10, "binding.paymentSourceEditState");
            EditText editText = textInputLayout10.getEditText();
            if (editText != null) {
                editText.setInputType(1);
            }
            TextInputLayout textInputLayout11 = getBinding().n;
            m.checkNotNullExpressionValue(textInputLayout11, "binding.paymentSourceEditState");
            EditText editText2 = textInputLayout11.getEditText();
            if (editText2 != null) {
                editText2.setFocusableInTouchMode(true);
                return;
            }
            return;
        }
        TextInputLayout textInputLayout12 = getBinding().n;
        m.checkNotNullExpressionValue(textInputLayout12, "binding.paymentSourceEditState");
        ViewExtensions.setOnEditTextClickListener(textInputLayout12, new WidgetPaymentSourceEditDialog$initPaymentSourceInfo$5(this, statesFor));
        TextInputLayout textInputLayout13 = getBinding().n;
        m.checkNotNullExpressionValue(textInputLayout13, "binding.paymentSourceEditState");
        ViewExtensions.setOnEditorActionListener(textInputLayout13, new WidgetPaymentSourceEditDialog$initPaymentSourceInfo$6(this, statesFor));
    }

    private final void initValidator(PaymentSourceAdapter.PaymentSourceItem paymentSourceItem) {
        ModelPaymentSource component1 = paymentSourceItem.component1();
        Input[] inputArr = new Input[5];
        TextInputLayout textInputLayout = getBinding().l;
        m.checkNotNullExpressionValue(textInputLayout, "binding.paymentSourceEditName");
        BasicTextInputValidator.Companion companion = BasicTextInputValidator.Companion;
        inputArr[0] = new Input.TextInputLayoutInput(ModelAuditLogEntry.CHANGE_KEY_NAME, textInputLayout, companion.createRequiredInputValidator(R.string.billing_address_name_error_required));
        TextInputLayout textInputLayout2 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.paymentSourceEditAddress1");
        inputArr[1] = new Input.TextInputLayoutInput("line_1", textInputLayout2, companion.createRequiredInputValidator(R.string.billing_address_address_error_required));
        TextInputLayout textInputLayout3 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.paymentSourceEditCity");
        inputArr[2] = new Input.TextInputLayoutInput("city", textInputLayout3, companion.createRequiredInputValidator(R.string.billing_address_city_error_required));
        TextInputLayout textInputLayout4 = getBinding().n;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.paymentSourceEditState");
        InputValidator[] inputValidatorArr = new InputValidator[1];
        inputValidatorArr[0] = companion.createRequiredInputValidator(m.areEqual(component1.getBillingAddress().getCountry(), "CA") ? R.string.billing_address_province_error_required : R.string.billing_address_state_error_required);
        inputArr[3] = new Input.TextInputLayoutInput("state", textInputLayout4, inputValidatorArr);
        TextInputLayout textInputLayout5 = getBinding().m;
        m.checkNotNullExpressionValue(textInputLayout5, "binding.paymentSourceEditPostalCode");
        inputArr[4] = new Input.TextInputLayoutInput("postal_code", textInputLayout5, companion.createRequiredInputValidator(R.string.billing_address_postal_code_error_required));
        this.validationManager = new ValidationManager(inputArr);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void selectState(StateEntry[] stateEntryArr) {
        CharSequence e;
        n.a aVar = n.k;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        e = b.e(this, R.string.payment_source_edit_select_state, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        ArrayList arrayList = new ArrayList(stateEntryArr.length);
        for (StateEntry stateEntry : stateEntryArr) {
            arrayList.add(stateEntry.getLabel());
        }
        Object[] array = arrayList.toArray(new String[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        aVar.a(parentFragmentManager, e, (CharSequence[]) array, new WidgetPaymentSourceEditDialog$selectState$2(this, stateEntryArr));
        getBinding().m.requestFocus();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updatePaymentSource(ModelPaymentSource modelPaymentSource) {
        StateEntry stateEntry;
        String value;
        Companion companion = Companion;
        TextInputLayout textInputLayout = getBinding().n;
        m.checkNotNullExpressionValue(textInputLayout, "binding.paymentSourceEditState");
        String textOrEmpty = companion.getTextOrEmpty(textInputLayout);
        StateEntry[] statesFor = getStatesFor(modelPaymentSource);
        m.checkNotNullExpressionValue(statesFor, "getStatesFor(paymentSource)");
        int length = statesFor.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                stateEntry = null;
                break;
            }
            stateEntry = statesFor[i];
            if (t.equals(stateEntry.getLabel(), textOrEmpty, true)) {
                break;
            }
            i++;
        }
        String str = (stateEntry == null || (value = stateEntry.getValue()) == null) ? textOrEmpty : value;
        ValidationManager validationManager = this.validationManager;
        if (validationManager == null) {
            m.throwUninitializedPropertyAccessException("validationManager");
        }
        if (ValidationManager.validate$default(validationManager, false, 1, null)) {
            Companion companion2 = Companion;
            TextInputLayout textInputLayout2 = getBinding().l;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.paymentSourceEditName");
            String textOrEmpty2 = companion2.getTextOrEmpty(textInputLayout2);
            TextInputLayout textInputLayout3 = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout3, "binding.paymentSourceEditAddress1");
            String textOrEmpty3 = companion2.getTextOrEmpty(textInputLayout3);
            TextInputLayout textInputLayout4 = getBinding().f;
            m.checkNotNullExpressionValue(textInputLayout4, "binding.paymentSourceEditAddress2");
            String textOrEmpty4 = companion2.getTextOrEmpty(textInputLayout4);
            TextInputLayout textInputLayout5 = getBinding().g;
            m.checkNotNullExpressionValue(textInputLayout5, "binding.paymentSourceEditCity");
            String textOrEmpty5 = companion2.getTextOrEmpty(textInputLayout5);
            TextInputLayout textInputLayout6 = getBinding().m;
            m.checkNotNullExpressionValue(textInputLayout6, "binding.paymentSourceEditPostalCode");
            String textOrEmpty6 = companion2.getTextOrEmpty(textInputLayout6);
            TextInputLayout textInputLayout7 = getBinding().h;
            m.checkNotNullExpressionValue(textInputLayout7, "binding.paymentSourceEditCountry");
            ModelBillingAddress modelBillingAddress = new ModelBillingAddress(textOrEmpty2, textOrEmpty3, textOrEmpty4, textOrEmpty5, str, companion2.getTextOrEmpty(textInputLayout7), textOrEmpty6);
            CheckBox checkBox = getBinding().i;
            m.checkNotNullExpressionValue(checkBox, "binding.paymentSourceEditDefault");
            PatchPaymentSourceRaw patchPaymentSourceRaw = new PatchPaymentSourceRaw(modelBillingAddress, checkBox.isChecked());
            TextView textView = getBinding().j;
            m.checkNotNullExpressionValue(textView, "binding.paymentSourceEditError");
            textView.setVisibility(8);
            getBinding().d.setIsLoading(true);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updatePaymentSource(modelPaymentSource.getId(), patchPaymentSourceRaw), false, 1, null), this, null, 2, null), WidgetPaymentSourceEditDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetPaymentSourceEditDialog$updatePaymentSource$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : new WidgetPaymentSourceEditDialog$updatePaymentSource$3(this), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPaymentSourceEditDialog$updatePaymentSource$1(this));
        }
    }

    @MainThread
    public final void handleError(Error error) {
        m.checkNotNullParameter(error, "error");
        TextView textView = getBinding().j;
        m.checkNotNullExpressionValue(textView, "binding.paymentSourceEditError");
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        ViewExtensions.setTextAndVisibilityBy(textView, response.getMessage());
        ValidationManager validationManager = this.validationManager;
        if (validationManager == null) {
            m.throwUninitializedPropertyAccessException("validationManager");
        }
        Error.Response response2 = error.getResponse();
        m.checkNotNullExpressionValue(response2, "error.response");
        Map<String, List<String>> messages = response2.getMessages();
        m.checkNotNullExpressionValue(messages, "error.response.messages");
        validationManager.setErrors(messages);
        Error.Response response3 = error.getResponse();
        m.checkNotNullExpressionValue(response3, "error.response");
        String message = response3.getMessage();
        if (message != null) {
            if (message.length() > 0) {
                b.a.d.m.h(getContext(), message, 0, null, 8);
            }
        }
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && (window = dialog.getWindow()) != null) {
            if (!AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
                window.getAttributes().windowAnimations = R.style.UiKit_Dialog_Animation;
            }
            window.setLayout(-1, -1);
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setSoftInputMode(16);
        }
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        Observable<R> F = getViewModel().observeViewState().x(WidgetPaymentSourceEditDialog$onViewBound$$inlined$filterIs$1.INSTANCE).F(WidgetPaymentSourceEditDialog$onViewBound$$inlined$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        Observable Z = F.F(WidgetPaymentSourceEditDialog$onViewBound$1.INSTANCE).F(new j0.k.b<List<? extends PaymentSourceAdapter.PaymentSourceItem>, PaymentSourceAdapter.PaymentSourceItem>() { // from class: com.discord.widgets.settings.billing.WidgetPaymentSourceEditDialog$onViewBound$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ PaymentSourceAdapter.PaymentSourceItem call(List<? extends PaymentSourceAdapter.PaymentSourceItem> list) {
                return call2((List<PaymentSourceAdapter.PaymentSourceItem>) list);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final PaymentSourceAdapter.PaymentSourceItem call2(List<PaymentSourceAdapter.PaymentSourceItem> list) {
                T t;
                String paymentSourceId;
                m.checkNotNullExpressionValue(list, "paymentSources");
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    String id2 = ((PaymentSourceAdapter.PaymentSourceItem) t).getPaymentSource().getId();
                    paymentSourceId = WidgetPaymentSourceEditDialog.this.getPaymentSourceId();
                    if (m.areEqual(id2, paymentSourceId)) {
                        break;
                    }
                }
                return (PaymentSourceAdapter.PaymentSourceItem) t;
            }
        }).Z(1);
        m.checkNotNullExpressionValue(Z, "viewModel\n        .obser…       }\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(Z, this, null, 2, null), WidgetPaymentSourceEditDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPaymentSourceEditDialog$onViewBound$3(this));
        getBinding().f2485b.setNavigationOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.billing.WidgetPaymentSourceEditDialog$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetPaymentSourceEditDialog.this.dismiss();
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.billing.WidgetPaymentSourceEditDialog$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetPaymentSourceEditDialog.this.dismiss();
            }
        });
    }
}
