package com.discord.widgets.settings.billing;

import android.content.Context;
import android.content.res.AssetManager;
import com.adjust.sdk.Constants;
import com.discord.widgets.settings.billing.WidgetPaymentSourceEditDialog;
import com.google.gson.Gson;
import d0.z.d.o;
import java.io.InputStreamReader;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetPaymentSourceEditDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0005\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;", "kotlin.jvm.PlatformType", "invoke", "()[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPaymentSourceEditDialog$caProvinces$2 extends o implements Function0<WidgetPaymentSourceEditDialog.StateEntry[]> {
    public final /* synthetic */ WidgetPaymentSourceEditDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetPaymentSourceEditDialog$caProvinces$2(WidgetPaymentSourceEditDialog widgetPaymentSourceEditDialog) {
        super(0);
        this.this$0 = widgetPaymentSourceEditDialog;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Throwable] */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetPaymentSourceEditDialog.StateEntry[] invoke() {
        AssetManager assets;
        Context context = this.this$0.getContext();
        th = 0;
        try {
            return (WidgetPaymentSourceEditDialog.StateEntry[]) new Gson().e(new InputStreamReader((context == null || (assets = context.getAssets()) == null) ? th : assets.open("data/canadian-provinces.json"), Constants.ENCODING), WidgetPaymentSourceEditDialog.StateEntry[].class);
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
