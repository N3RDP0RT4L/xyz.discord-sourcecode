package com.discord.widgets.settings.connections;

import com.discord.databinding.WidgetConnectionsAddXboxBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.error.Error;
import com.discord.utilities.platform.Platform;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.functions.Action1;
/* compiled from: WidgetSettingsUserConnectionsAddXbox.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "verificationCode", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsUserConnectionsAddXbox$onViewBound$2 extends o implements Function1<String, Unit> {
    public final /* synthetic */ WidgetSettingsUserConnectionsAddXbox this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsUserConnectionsAddXbox$onViewBound$2(WidgetSettingsUserConnectionsAddXbox widgetSettingsUserConnectionsAddXbox) {
        super(1);
        this.this$0 = widgetSettingsUserConnectionsAddXbox;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(String str) {
        invoke2(str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        WidgetConnectionsAddXboxBinding binding;
        m.checkNotNullParameter(str, "verificationCode");
        if (str.length() == 6) {
            WidgetSettingsUserConnectionsAddXbox widgetSettingsUserConnectionsAddXbox = this.this$0;
            Platform platform = Platform.XBOX;
            binding = widgetSettingsUserConnectionsAddXbox.getBinding();
            DimmerView dimmerView = binding.d;
            WidgetSettingsUserConnectionsAddXbox widgetSettingsUserConnectionsAddXbox2 = this.this$0;
            widgetSettingsUserConnectionsAddXbox.submitPinCode(str, platform, dimmerView, widgetSettingsUserConnectionsAddXbox2, widgetSettingsUserConnectionsAddXbox2.e(), new Action1<Error>() { // from class: com.discord.widgets.settings.connections.WidgetSettingsUserConnectionsAddXbox$onViewBound$2.1
                public final void call(Error error) {
                    WidgetSettingsUserConnectionsAddXbox widgetSettingsUserConnectionsAddXbox3 = WidgetSettingsUserConnectionsAddXbox$onViewBound$2.this.this$0;
                    m.checkNotNullExpressionValue(error, "error");
                    widgetSettingsUserConnectionsAddXbox3.trackXboxLinkFailed(error);
                    if (error.getType() == Error.Type.DISCORD_REQUEST_ERROR) {
                        error.setShowErrorToasts(false);
                        WidgetSettingsUserConnectionsAddXbox$onViewBound$2.this.this$0.showPinError();
                    }
                }
            });
        }
    }
}
