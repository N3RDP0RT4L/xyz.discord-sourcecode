package com.discord.widgets.settings.connections;

import andhook.lib.HookHelper;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetConnectionsAddXboxBinding;
import com.discord.models.domain.ModelConnectionState;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.error.Error;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsUserConnectionsAddXbox.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 #2\u00020\u0001:\u0001#B\u0007¢\u0006\u0004\b\"\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rJG\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0015\u001a\u00020\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u00162\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00060\u0018¢\u0006\u0004\b\u001a\u0010\u001bR\u001d\u0010!\u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAddXbox;", "Lcom/discord/app/AppFragment;", "", "showPinError", "()V", "trackXboxLinkStep", "Lcom/discord/utilities/error/Error;", "error", "trackXboxLinkFailed", "(Lcom/discord/utilities/error/Error;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "", "verificationCode", "Lcom/discord/utilities/platform/Platform;", "platform", "Lcom/discord/utilities/dimmer/DimmerView;", "dimmer", "Lcom/discord/app/AppComponent;", "appComponent", "Landroid/app/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lrx/functions/Action1;", "errorHandler", "submitPinCode", "(Ljava/lang/String;Lcom/discord/utilities/platform/Platform;Lcom/discord/utilities/dimmer/DimmerView;Lcom/discord/app/AppComponent;Landroid/app/Activity;Lrx/functions/Action1;)V", "Lcom/discord/databinding/WidgetConnectionsAddXboxBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetConnectionsAddXboxBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsUserConnectionsAddXbox extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsUserConnectionsAddXbox.class, "binding", "getBinding()Lcom/discord/databinding/WidgetConnectionsAddXboxBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsUserConnectionsAddXbox$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetSettingsUserConnectionsAddXbox.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAddXbox$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsUserConnectionsAddXbox.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsUserConnectionsAddXbox() {
        super(R.layout.widget_connections_add_xbox);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetConnectionsAddXboxBinding getBinding() {
        return (WidgetConnectionsAddXboxBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void launch(Context context) {
        Companion.launch(context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showPinError() {
        b.a.d.m.g(getContext(), R.string.connection_invalid_pin, 0, null, 12);
        getBinding().f2335b.b();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void trackXboxLinkFailed(Error error) {
        AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
        String bodyText = error.getBodyText();
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        analyticsTracker.accountLinkFailed(bodyText, Integer.valueOf(response.getCode()), "pin", "PIN code entry", Platform.XBOX.getPlatformId());
    }

    private final void trackXboxLinkStep() {
        AnalyticsTracker.accountLinkStep$default(AnalyticsTracker.INSTANCE, "mobile connections", "PIN code entry", null, Platform.XBOX.getPlatformId(), 4, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.connections);
        trackXboxLinkStep();
        getBinding().c.setOnClickListener(WidgetSettingsUserConnectionsAddXbox$onViewBound$1.INSTANCE);
        getBinding().f2335b.setOnCodeEntered(new WidgetSettingsUserConnectionsAddXbox$onViewBound$2(this));
    }

    public final void submitPinCode(String str, final Platform platform, DimmerView dimmerView, AppComponent appComponent, Activity activity, Action1<Error> action1) {
        m.checkNotNullParameter(str, "verificationCode");
        m.checkNotNullParameter(platform, "platform");
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(action1, "errorHandler");
        Observable z2 = ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getConnectionState(platform.getPlatformId(), str), false, 1, null).z(new b<ModelConnectionState, Observable<? extends Void>>() { // from class: com.discord.widgets.settings.connections.WidgetSettingsUserConnectionsAddXbox$submitPinCode$1
            public final Observable<? extends Void> call(ModelConnectionState modelConnectionState) {
                AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                m.checkNotNullExpressionValue(modelConnectionState, "state");
                analyticsTracker.accountLinkStep("PIN code entry", "PIN success", modelConnectionState.getState(), Platform.this.getPlatformId());
                RestAPI api = RestAPI.Companion.getApi();
                String platformId = Platform.this.getPlatformId();
                String code = modelConnectionState.getCode();
                String state = modelConnectionState.getState();
                Boolean bool = Boolean.TRUE;
                return api.submitConnectionState(platformId, new RestAPIParams.ConnectionState(code, state, bool, bool));
            }
        });
        m.checkNotNullExpressionValue(z2, "RestAPI\n        .api\n   …              )\n        }");
        ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.ui$default(z2, appComponent, null, 2, null), dimmerView, 0L, 2, null).k(o.a.g(activity, new WidgetSettingsUserConnectionsAddXbox$submitPinCode$2(activity), action1));
    }
}
