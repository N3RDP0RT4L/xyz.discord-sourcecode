package com.discord.widgets.settings.connections;

import com.discord.widgets.settings.connections.WidgetSettingsUserConnectionsViewModel;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.BehaviorSubject;
/* compiled from: WidgetSettingsUserConnectionsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$2 extends o implements Function1<Void, Unit> {
    public final /* synthetic */ String $integrationId;
    public final /* synthetic */ WidgetSettingsUserConnectionsViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$2(WidgetSettingsUserConnectionsViewModel widgetSettingsUserConnectionsViewModel, String str) {
        super(1);
        this.this$0 = widgetSettingsUserConnectionsViewModel;
        this.$integrationId = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
        invoke2(r1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Void r3) {
        Map map;
        BehaviorSubject behaviorSubject;
        Map map2;
        map = this.this$0.joinStatusMap;
        map.put(this.$integrationId, WidgetSettingsUserConnectionsViewModel.JoinStatus.Joined.INSTANCE);
        behaviorSubject = this.this$0.joinStateSubject;
        map2 = this.this$0.joinStatusMap;
        behaviorSubject.onNext(map2);
    }
}
