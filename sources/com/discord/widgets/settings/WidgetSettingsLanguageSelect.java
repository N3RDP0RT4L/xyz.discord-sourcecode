package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetSettingsLanguageSelectBinding;
import com.discord.databinding.WidgetSettingsLanguageSelectItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.view.recycler.MaxHeightRecyclerView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.WidgetSettingsLanguage;
import com.discord.widgets.settings.WidgetSettingsLanguageSelect;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetSettingsLanguageSelect.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u00132\u00020\u0001:\u0003\u0014\u0013\u0015B\u0007¢\u0006\u0004\b\u0012\u0010\bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001d\u0010\u000e\u001a\u00020\t8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect;", "Lcom/discord/app/AppDialog;", "", "locale", "", "onLocaleSelected", "(Ljava/lang/String;)V", "onResume", "()V", "Lcom/discord/databinding/WidgetSettingsLanguageSelectBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsLanguageSelectBinding;", "binding", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter;", "adapter", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter;", HookHelper.constructorName, "Companion", "Adapter", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsLanguageSelect extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsLanguageSelect.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsLanguageSelectBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String REQUEST_KEY_USER_LOCALE = "REQUEST_KEY_USER_LOCALE";
    private static final String RESULT_KEY_USE_LOCALE = "INTENT_EXTRA_LOCALE";
    private Adapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsLanguageSelect$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetSettingsLanguageSelect.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B\u0017\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Model$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter$AdapterItemLocale;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter$AdapterItemLocale;", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect;", "dialog", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect;)V", "AdapterItemLocale", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Adapter extends MGRecyclerAdapterSimple<Model.Item> {
        private final WidgetSettingsLanguageSelect dialog;

        /* compiled from: WidgetSettingsLanguageSelect.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter$AdapterItemLocale;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter;", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Model$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Model$Item;)V", "Lcom/discord/databinding/WidgetSettingsLanguageSelectItemBinding;", "binding", "Lcom/discord/databinding/WidgetSettingsLanguageSelectItemBinding;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AdapterItemLocale extends MGRecyclerViewHolder<Adapter, Model.Item> {
            private final WidgetSettingsLanguageSelectItemBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AdapterItemLocale(@LayoutRes int i, Adapter adapter) {
                super(i, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                View view = this.itemView;
                int i2 = R.id.flag_icon;
                ImageView imageView = (ImageView) view.findViewById(R.id.flag_icon);
                if (imageView != null) {
                    i2 = R.id.flag_icon_barrier;
                    Barrier barrier = (Barrier) view.findViewById(R.id.flag_icon_barrier);
                    if (barrier != null) {
                        i2 = R.id.settings_language_select_item_name;
                        TextView textView = (TextView) view.findViewById(R.id.settings_language_select_item_name);
                        if (textView != null) {
                            i2 = R.id.settings_language_select_item_name_localized;
                            TextView textView2 = (TextView) view.findViewById(R.id.settings_language_select_item_name_localized);
                            if (textView2 != null) {
                                WidgetSettingsLanguageSelectItemBinding widgetSettingsLanguageSelectItemBinding = new WidgetSettingsLanguageSelectItemBinding((ConstraintLayout) view, imageView, barrier, textView, textView2);
                                m.checkNotNullExpressionValue(widgetSettingsLanguageSelectItemBinding, "WidgetSettingsLanguageSe…temBinding.bind(itemView)");
                                this.binding = widgetSettingsLanguageSelectItemBinding;
                                return;
                            }
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
            }

            public static final /* synthetic */ Adapter access$getAdapter$p(AdapterItemLocale adapterItemLocale) {
                return (Adapter) adapterItemLocale.adapter;
            }

            public void onConfigure(int i, final Model.Item item) {
                m.checkNotNullParameter(item, "data");
                super.onConfigure(i, (int) item);
                this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsLanguageSelect$Adapter$AdapterItemLocale$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetSettingsLanguageSelect widgetSettingsLanguageSelect;
                        widgetSettingsLanguageSelect = WidgetSettingsLanguageSelect.Adapter.AdapterItemLocale.access$getAdapter$p(WidgetSettingsLanguageSelect.Adapter.AdapterItemLocale.this).dialog;
                        widgetSettingsLanguageSelect.onLocaleSelected(item.getLocale());
                    }
                });
                TextView textView = this.binding.c;
                m.checkNotNullExpressionValue(textView, "binding.settingsLanguageSelectItemName");
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Context context = view.getContext();
                WidgetSettingsLanguage.Companion companion = WidgetSettingsLanguage.Companion;
                textView.setText(context.getString(companion.getLocaleResId(item.getLocale())));
                TextView textView2 = this.binding.d;
                m.checkNotNullExpressionValue(textView2, "binding.settingsLanguageSelectItemNameLocalized");
                textView2.setText(companion.getAsStringInLocale(item.getLocale()));
                this.binding.f2603b.setImageResource(companion.getLocaleFlagResId(item.getLocale()));
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Adapter(RecyclerView recyclerView, WidgetSettingsLanguageSelect widgetSettingsLanguageSelect) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recycler");
            m.checkNotNullParameter(widgetSettingsLanguageSelect, "dialog");
            this.dialog = widgetSettingsLanguageSelect;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public AdapterItemLocale onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            return new AdapterItemLocale(R.layout.widget_settings_language_select_item, this);
        }
    }

    /* compiled from: WidgetSettingsLanguageSelect.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\n\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\r¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Companion;", "", "Landroidx/fragment/app/Fragment;", "fragment", "Lkotlin/Function1;", "", "", "onLocaleSelected", "registerForResult", "(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)V", "show", "(Landroidx/fragment/app/Fragment;)V", WidgetSettingsLanguageSelect.REQUEST_KEY_USER_LOCALE, "Ljava/lang/String;", "RESULT_KEY_USE_LOCALE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void registerForResult(Fragment fragment, Function1<? super String, Unit> function1) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(function1, "onLocaleSelected");
            FragmentKt.setFragmentResultListener(fragment, WidgetSettingsLanguageSelect.REQUEST_KEY_USER_LOCALE, new WidgetSettingsLanguageSelect$Companion$registerForResult$1(function1));
        }

        public final void show(Fragment fragment) {
            m.checkNotNullParameter(fragment, "fragment");
            WidgetSettingsLanguageSelect widgetSettingsLanguageSelect = new WidgetSettingsLanguageSelect();
            widgetSettingsLanguageSelect.setArguments(new Bundle());
            FragmentManager parentFragmentManager = fragment.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "fragment.parentFragmentManager");
            widgetSettingsLanguageSelect.show(parentFragmentManager, WidgetSettingsLanguageSelect.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsLanguageSelect.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\bÂ\u0002\u0018\u00002\u00020\u0001:\u0001\tB\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Model;", "", "", "Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Model$Item;", "getLocales", "()Ljava/util/List;", "locales", HookHelper.constructorName, "()V", "Item", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Model INSTANCE = new Model();

        /* compiled from: WidgetSettingsLanguageSelect.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004R\u001c\u0010\u0013\u001a\u00020\t8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u000bR\u001c\u0010\u0016\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010\u0011\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Model$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "component1", "()Ljava/lang/String;", "locale", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Model$Item;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getLocale", "type", "I", "getType", "key", "getKey", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Item implements MGRecyclerDataPayload {
            private final String key;
            private final String locale;
            private final int type;

            public Item(String str) {
                m.checkNotNullParameter(str, "locale");
                this.locale = str;
                this.key = str;
            }

            public static /* synthetic */ Item copy$default(Item item, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = item.locale;
                }
                return item.copy(str);
            }

            public final String component1() {
                return this.locale;
            }

            public final Item copy(String str) {
                m.checkNotNullParameter(str, "locale");
                return new Item(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Item) && m.areEqual(this.locale, ((Item) obj).locale);
                }
                return true;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final String getLocale() {
                return this.locale;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                String str = this.locale;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("Item(locale="), this.locale, ")");
            }
        }

        private Model() {
        }

        public final List<Item> getLocales() {
            List<String> listOf = n.listOf((Object[]) new String[]{"da", "de", "en-GB", "en-US", "es-ES", "fr", "hr", "it", "lt", "hu", "nl", "no", "pl", "pt-BR", "ro", "fi", "sv-SE", "vi", "tr", "cs", "el", "bg", "ru", "uk", "ja", "zh-TW", "th", "zh-CN", "ko", "hi"});
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(listOf, 10));
            for (String str : listOf) {
                arrayList.add(new Item(str));
            }
            return arrayList;
        }
    }

    public WidgetSettingsLanguageSelect() {
        super(R.layout.widget_settings_language_select);
    }

    private final WidgetSettingsLanguageSelectBinding getBinding() {
        return (WidgetSettingsLanguageSelectBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onLocaleSelected(String str) {
        Bundle bundle = new Bundle();
        bundle.putString(RESULT_KEY_USE_LOCALE, str);
        FragmentKt.setFragmentResult(this, REQUEST_KEY_USER_LOCALE, bundle);
        dismiss();
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        MaxHeightRecyclerView maxHeightRecyclerView = getBinding().f2602b;
        m.checkNotNullExpressionValue(maxHeightRecyclerView, "binding.settingsLanguageSelectList");
        Adapter adapter = (Adapter) companion.configure(new Adapter(maxHeightRecyclerView, this));
        this.adapter = adapter;
        if (adapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        adapter.setData(Model.INSTANCE.getLocales());
    }
}
