package com.discord.widgets.settings;

import com.discord.app.AppViewModel;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettings$viewModelUserProfileHeader$2 extends o implements Function0<AppViewModel<UserProfileHeaderViewModel.ViewState>> {
    public static final WidgetSettings$viewModelUserProfileHeader$2 INSTANCE = new WidgetSettings$viewModelUserProfileHeader$2();

    public WidgetSettings$viewModelUserProfileHeader$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<UserProfileHeaderViewModel.ViewState> invoke() {
        return new UserProfileHeaderViewModel(-1L, null, null, null, true, null, null, 110, null);
    }
}
