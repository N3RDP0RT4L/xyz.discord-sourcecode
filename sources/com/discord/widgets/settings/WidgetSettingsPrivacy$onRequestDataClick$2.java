package com.discord.widgets.settings;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\n\b\u0002\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "responseMessage", "", "invoke", "(Ljava/lang/String;)V", "handleRequestError"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPrivacy$onRequestDataClick$2 extends o implements Function1<String, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ WidgetSettingsPrivacy this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPrivacy$onRequestDataClick$2(WidgetSettingsPrivacy widgetSettingsPrivacy, Context context) {
        super(1);
        this.this$0 = widgetSettingsPrivacy;
        this.$context = context;
    }

    public static /* synthetic */ void invoke$default(WidgetSettingsPrivacy$onRequestDataClick$2 widgetSettingsPrivacy$onRequestDataClick$2, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        widgetSettingsPrivacy$onRequestDataClick$2.invoke2(str);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(String str) {
        invoke2(str);
        return Unit.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v5, types: [java.lang.CharSequence] */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        ?? b2;
        WidgetNoticeDialog.Builder title = new WidgetNoticeDialog.Builder(this.$context).setTitle(R.string.data_privacy_controls_request_data_failure_title);
        String str2 = str;
        if (str == null) {
            b2 = b.b(this.$context, R.string.data_privacy_controls_request_data_failure_body, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            str2 = b2;
        }
        WidgetNoticeDialog.Builder positiveButton$default = WidgetNoticeDialog.Builder.setPositiveButton$default(title.setMessage(str2), (int) R.string.okay, (Function1) null, 2, (Object) null);
        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        positiveButton$default.show(parentFragmentManager);
    }
}
