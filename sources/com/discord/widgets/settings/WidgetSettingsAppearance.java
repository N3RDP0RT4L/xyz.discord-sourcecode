package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetSettingsAppearanceBinding;
import com.discord.models.domain.ModelUserSettings;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.ChatInputComponentTypes;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.ToastManager;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.views.CheckedSetting;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g;
import d0.l;
import d0.t.n;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlinx.coroutines.CoroutineScope;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetSettingsAppearance.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u0000 :2\u00020\u0001:\u0002:;B\u0007¢\u0006\u0004\b9\u0010\u0019J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u000e\u001a\u00020\r2\u0006\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001b\u0010\u0012\u001a\u00020\u0004*\u00020\u00102\u0006\u0010\u0011\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0017\u0010\u0006J\u000f\u0010\u0018\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u0004H\u0016¢\u0006\u0004\b!\u0010\u0019J\u000f\u0010\"\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\"\u0010\u0019R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(R:\u0010+\u001a&\u0012\f\u0012\n **\u0004\u0018\u00010\u00070\u0007 **\u0012\u0012\f\u0012\n **\u0004\u0018\u00010\u00070\u0007\u0018\u00010)0)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u001d\u00102\u001a\u00020-8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R\u0016\u00103\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u0010%RA\u00108\u001a&\u0012\f\u0012\n **\u0004\u0018\u00010\u000b0\u000b **\u0012\u0012\f\u0012\n **\u0004\u0018\u00010\u000b0\u000b\u0018\u00010)0)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107¨\u0006<"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAppearance;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V", "", "fontScale", "configureFontScalingUI", "(I)V", "", "useSystemFontScale", "", "getFontScaleString", "(IZ)Ljava/lang/String;", "Lcom/discord/views/CheckedSetting;", "settingTheme", "configureThemeOption", "(Lcom/discord/views/CheckedSetting;Ljava/lang/String;)V", "theme", "updateTheme", "(Ljava/lang/String;)V", "setupMessage", "showHolyLight", "()V", "enabled", "tryEnableTorchMode", "(Z)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onDestroyView", "Ljava/util/concurrent/atomic/AtomicInteger;", "holyLightEasterEggCounter", "Ljava/util/concurrent/atomic/AtomicInteger;", "Lcom/discord/utilities/view/ToastManager;", "toastManager", "Lcom/discord/utilities/view/ToastManager;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "newFontScaleSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/databinding/WidgetSettingsAppearanceBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsAppearanceBinding;", "binding", "pureEvilEasterEggCounter", "pureEvilEasterEggSubject$delegate", "Lkotlin/Lazy;", "getPureEvilEasterEggSubject", "()Lrx/subjects/BehaviorSubject;", "pureEvilEasterEggSubject", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAppearance extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsAppearance.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsAppearanceBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int EASTER_EGG_UNLOCK_TIMEOUT = 5;
    private static final int HOLY_LIGHT_UNLOCK_COUNT = 5;
    private static final int PURE_EVIL_HINT_COUNT = 3;
    private static final int PURE_EVIL_UNLOCK_COUNT = 8;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsAppearance$binding$2.INSTANCE, null, 2, null);
    private final BehaviorSubject<Integer> newFontScaleSubject = BehaviorSubject.k0();
    private final Lazy pureEvilEasterEggSubject$delegate = g.lazy(WidgetSettingsAppearance$pureEvilEasterEggSubject$2.INSTANCE);
    private final AtomicInteger pureEvilEasterEggCounter = new AtomicInteger(0);
    private final AtomicInteger holyLightEasterEggCounter = new AtomicInteger(0);
    private ToastManager toastManager = new ToastManager();

    /* compiled from: WidgetSettingsAppearance.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\tR\u0016\u0010\u000b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\tR\u0016\u0010\f\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\t¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", "", "EASTER_EGG_UNLOCK_TIMEOUT", "I", "HOLY_LIGHT_UNLOCK_COUNT", "PURE_EVIL_HINT_COUNT", "PURE_EVIL_UNLOCK_COUNT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsAppearance.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsAppearance.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0019\b\u0082\b\u0018\u0000 #2\u00020\u0001:\u0001#B'\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ8\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0004J\u0010\u0010\u0015\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0015\u0010\nJ\u001a\u0010\u0017\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0019\u001a\u0004\b\u001a\u0010\nR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b\u001e\u0010\rR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0007¨\u0006$"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()Z", "", "component3", "()I", "Lcom/discord/models/user/MeUser;", "component4", "()Lcom/discord/models/user/MeUser;", "currentTheme", "canSeePureEvil", "fontScale", "meUser", "copy", "(Ljava/lang/String;ZILcom/discord/models/user/MeUser;)Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getFontScale", "Ljava/lang/String;", "getCurrentTheme", "Lcom/discord/models/user/MeUser;", "getMeUser", "Z", "getCanSeePureEvil", HookHelper.constructorName, "(Ljava/lang/String;ZILcom/discord/models/user/MeUser;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canSeePureEvil;
        private final String currentTheme;
        private final int fontScale;
        private final MeUser meUser;

        /* compiled from: WidgetSettingsAppearance.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ!\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model$Companion;", "", "Lrx/Observable;", "", "easterEggPureEvil", "Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;", "get", "(Lrx/Observable;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(Observable<Boolean> observable) {
                m.checkNotNullParameter(observable, "easterEggPureEvil");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<Model> i = Observable.i(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getUserSettingsSystem().observeSettings(false), observable, WidgetSettingsAppearance$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(i, "Observable\n            .…          )\n            }");
                return i;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(String str, boolean z2, int i, MeUser meUser) {
            m.checkNotNullParameter(str, "currentTheme");
            m.checkNotNullParameter(meUser, "meUser");
            this.currentTheme = str;
            this.canSeePureEvil = z2;
            this.fontScale = i;
            this.meUser = meUser;
        }

        public static /* synthetic */ Model copy$default(Model model, String str, boolean z2, int i, MeUser meUser, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = model.currentTheme;
            }
            if ((i2 & 2) != 0) {
                z2 = model.canSeePureEvil;
            }
            if ((i2 & 4) != 0) {
                i = model.fontScale;
            }
            if ((i2 & 8) != 0) {
                meUser = model.meUser;
            }
            return model.copy(str, z2, i, meUser);
        }

        public final String component1() {
            return this.currentTheme;
        }

        public final boolean component2() {
            return this.canSeePureEvil;
        }

        public final int component3() {
            return this.fontScale;
        }

        public final MeUser component4() {
            return this.meUser;
        }

        public final Model copy(String str, boolean z2, int i, MeUser meUser) {
            m.checkNotNullParameter(str, "currentTheme");
            m.checkNotNullParameter(meUser, "meUser");
            return new Model(str, z2, i, meUser);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.currentTheme, model.currentTheme) && this.canSeePureEvil == model.canSeePureEvil && this.fontScale == model.fontScale && m.areEqual(this.meUser, model.meUser);
        }

        public final boolean getCanSeePureEvil() {
            return this.canSeePureEvil;
        }

        public final String getCurrentTheme() {
            return this.currentTheme;
        }

        public final int getFontScale() {
            return this.fontScale;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public int hashCode() {
            String str = this.currentTheme;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z2 = this.canSeePureEvil;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (((hashCode + i2) * 31) + this.fontScale) * 31;
            MeUser meUser = this.meUser;
            if (meUser != null) {
                i = meUser.hashCode();
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(currentTheme=");
            R.append(this.currentTheme);
            R.append(", canSeePureEvil=");
            R.append(this.canSeePureEvil);
            R.append(", fontScale=");
            R.append(this.fontScale);
            R.append(", meUser=");
            R.append(this.meUser);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetSettingsAppearance() {
        super(R.layout.widget_settings_appearance);
    }

    private final void configureFontScalingUI(int i) {
        FragmentActivity activity;
        ContentResolver contentResolver;
        int systemFontScaleInt = (i != -1 || (activity = e()) == null || (contentResolver = activity.getContentResolver()) == null) ? i : FontUtils.INSTANCE.getSystemFontScaleInt(contentResolver);
        TextView textView = getBinding().d;
        m.checkNotNullExpressionValue(textView, "binding.settingsAppearanceFontScalePlatform");
        textView.setText(getFontScaleString(systemFontScaleInt, i == -1));
        SeekBar seekBar = getBinding().f;
        m.checkNotNullExpressionValue(seekBar, "binding.settingsAppearanceFontScalingSeekbar");
        seekBar.setProgress(systemFontScaleInt - 80);
    }

    private final void configureThemeOption(CheckedSetting checkedSetting, final String str) {
        checkedSetting.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsAppearance$configureThemeOption$1
            public final void call(Boolean bool) {
                m.checkNotNullExpressionValue(bool, "checked");
                if (bool.booleanValue()) {
                    WidgetSettingsAppearance.this.updateTheme(str);
                }
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        boolean z2 = !m.areEqual(model.getCurrentTheme(), ModelUserSettings.THEME_LIGHT);
        getBinding().l.g(!z2, false);
        CheckedSetting checkedSetting = getBinding().l;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsAppearanceThemeLightRadio");
        configureThemeOption(checkedSetting, ModelUserSettings.THEME_LIGHT);
        getBinding().l.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$1

            /* compiled from: WidgetSettingsAppearance.kt */
            @e(c = "com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$1$1", f = "WidgetSettingsAppearance.kt", l = {122}, m = "invokeSuspend")
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
                public int label;

                public AnonymousClass1(Continuation continuation) {
                    super(2, continuation);
                }

                @Override // d0.w.i.a.a
                public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
                    m.checkNotNullParameter(continuation, "completion");
                    return new AnonymousClass1(continuation);
                }

                @Override // kotlin.jvm.functions.Function2
                public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
                    return ((AnonymousClass1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
                }

                @Override // d0.w.i.a.a
                public final Object invokeSuspend(Object obj) {
                    AtomicInteger atomicInteger;
                    Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
                    int i = this.label;
                    if (i == 0) {
                        l.throwOnFailure(obj);
                        this.label = 1;
                        if (f.P(5000L, this) == coroutine_suspended) {
                            return coroutine_suspended;
                        }
                    } else if (i == 1) {
                        l.throwOnFailure(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    atomicInteger = WidgetSettingsAppearance.this.holyLightEasterEggCounter;
                    atomicInteger.set(0);
                    return Unit.a;
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsAppearanceBinding binding;
                AtomicInteger atomicInteger;
                binding = WidgetSettingsAppearance.this.getBinding();
                binding.l.g(true, true);
                atomicInteger = WidgetSettingsAppearance.this.holyLightEasterEggCounter;
                int andIncrement = atomicInteger.getAndIncrement();
                if (andIncrement == 0) {
                    m.checkNotNullExpressionValue(view, "it");
                    CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(view);
                    if (coroutineScope != null) {
                        f.H0(coroutineScope, null, null, new AnonymousClass1(null), 3, null);
                    }
                } else if (andIncrement == 5) {
                    WidgetSettingsAppearance.this.showHolyLight();
                }
            }
        });
        getBinding().j.g(z2, false);
        CheckedSetting checkedSetting2 = getBinding().j;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsAppearanceThemeDarkRadio");
        configureThemeOption(checkedSetting2, ModelUserSettings.THEME_DARK);
        getBinding().j.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$2

            /* compiled from: WidgetSettingsAppearance.kt */
            @e(c = "com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$2$1", f = "WidgetSettingsAppearance.kt", l = {Opcodes.F2I}, m = "invokeSuspend")
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
                public int label;

                public AnonymousClass1(Continuation continuation) {
                    super(2, continuation);
                }

                @Override // d0.w.i.a.a
                public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
                    m.checkNotNullParameter(continuation, "completion");
                    return new AnonymousClass1(continuation);
                }

                @Override // kotlin.jvm.functions.Function2
                public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
                    return ((AnonymousClass1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
                }

                @Override // d0.w.i.a.a
                public final Object invokeSuspend(Object obj) {
                    AtomicInteger atomicInteger;
                    Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
                    int i = this.label;
                    if (i == 0) {
                        l.throwOnFailure(obj);
                        this.label = 1;
                        if (f.P(5000L, this) == coroutine_suspended) {
                            return coroutine_suspended;
                        }
                    } else if (i == 1) {
                        l.throwOnFailure(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    atomicInteger = WidgetSettingsAppearance.this.pureEvilEasterEggCounter;
                    atomicInteger.set(0);
                    return Unit.a;
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsAppearanceBinding binding;
                AtomicInteger atomicInteger;
                ToastManager toastManager;
                BehaviorSubject pureEvilEasterEggSubject;
                CharSequence e;
                ToastManager toastManager2;
                binding = WidgetSettingsAppearance.this.getBinding();
                binding.j.g(true, true);
                if (!model.getCanSeePureEvil()) {
                    atomicInteger = WidgetSettingsAppearance.this.pureEvilEasterEggCounter;
                    int andIncrement = atomicInteger.getAndIncrement();
                    if (andIncrement == 0) {
                        m.checkNotNullExpressionValue(view, "it");
                        CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(view);
                        if (coroutineScope != null) {
                            f.H0(coroutineScope, null, null, new AnonymousClass1(null), 3, null);
                        }
                    } else if (3 <= andIncrement && 8 > andIncrement) {
                        m.checkNotNullExpressionValue(view, "it");
                        Context context = view.getContext();
                        e = b.e(WidgetSettingsAppearance.this, R.string.theme_pure_evil_easter_hint, new Object[]{String.valueOf(8 - andIncrement)}, (r4 & 4) != 0 ? b.a.j : null);
                        toastManager2 = WidgetSettingsAppearance.this.toastManager;
                        b.a.d.m.h(context, e, 0, toastManager2, 4);
                    } else if (andIncrement == 8) {
                        Context context2 = WidgetSettingsAppearance.this.getContext();
                        toastManager = WidgetSettingsAppearance.this.toastManager;
                        b.a.d.m.d(context2, R.string.theme_pure_evil_easter_reveal, 0, toastManager);
                        pureEvilEasterEggSubject = WidgetSettingsAppearance.this.getPureEvilEasterEggSubject();
                        pureEvilEasterEggSubject.onNext(Boolean.TRUE);
                    }
                }
            }
        });
        if (m.areEqual(model.getCurrentTheme(), ModelUserSettings.THEME_PURE_EVIL)) {
            CheckedSetting.d(getBinding().m, null, 1);
        }
        getBinding().m.g(m.areEqual(model.getCurrentTheme(), ModelUserSettings.THEME_PURE_EVIL), false);
        CheckedSetting checkedSetting3 = getBinding().m;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.settingsAppearanceThemePureEvilSwitch");
        checkedSetting3.setVisibility(model.getCanSeePureEvil() ? 0 : 8);
        getBinding().m.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$3
            public final void call(Boolean bool) {
                m.checkNotNullExpressionValue(bool, "isChecked");
                if (bool.booleanValue() && m.areEqual(model.getCurrentTheme(), ModelUserSettings.THEME_DARK)) {
                    WidgetSettingsAppearance.this.updateTheme(ModelUserSettings.THEME_PURE_EVIL);
                } else if (!bool.booleanValue() && m.areEqual(model.getCurrentTheme(), ModelUserSettings.THEME_PURE_EVIL)) {
                    WidgetSettingsAppearance.this.updateTheme(ModelUserSettings.THEME_DARK);
                }
            }
        });
        configureFontScalingUI(model.getFontScale());
        getBinding().f.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: com.discord.widgets.settings.WidgetSettingsAppearance$configureUI$4
            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int i, boolean z3) {
                BehaviorSubject behaviorSubject;
                WidgetSettingsAppearanceBinding binding;
                String fontScaleString;
                if (z3) {
                    int i2 = i + 80;
                    behaviorSubject = WidgetSettingsAppearance.this.newFontScaleSubject;
                    behaviorSubject.onNext(Integer.valueOf(i2));
                    binding = WidgetSettingsAppearance.this.getBinding();
                    TextView textView = binding.d;
                    m.checkNotNullExpressionValue(textView, "binding.settingsAppearanceFontScalePlatform");
                    fontScaleString = WidgetSettingsAppearance.this.getFontScaleString(i2, false);
                    textView.setText(fontScaleString);
                }
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        getBinding().e.setOnClickListener(WidgetSettingsAppearance$configureUI$5.INSTANCE);
        setupMessage(model);
        for (TextView textView : n.listOf((Object[]) new TextView[]{getBinding().k, getBinding().h, getBinding().c})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView, "header");
            accessibilityUtils.setViewIsHeading(textView);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSettingsAppearanceBinding getBinding() {
        return (WidgetSettingsAppearanceBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getFontScaleString(int i, boolean z2) {
        if (z2) {
            return i + "% (" + getString(R.string.accessibility_font_scaling_use_os) + ')';
        }
        return i + "% (" + getString(R.string.accessibility_font_scaling_use_app) + ')';
    }

    public static /* synthetic */ String getFontScaleString$default(WidgetSettingsAppearance widgetSettingsAppearance, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = true;
        }
        return widgetSettingsAppearance.getFontScaleString(i, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final BehaviorSubject<Boolean> getPureEvilEasterEggSubject() {
        return (BehaviorSubject) this.pureEvilEasterEggSubject$delegate.getValue();
    }

    private final void setupMessage(Model model) {
        SimpleDraweeView simpleDraweeView = getBinding().f2576b.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.chatItem.chatListAdapterItemTextAvatar");
        IconUtils.setIcon$default(simpleDraweeView, model.getMeUser(), R.dimen.avatar_size_standard, null, null, null, 56, null);
        TextView textView = getBinding().f2576b.f;
        m.checkNotNullExpressionValue(textView, "binding.chatItem.chatListAdapterItemTextTag");
        textView.setVisibility(8);
        TextView textView2 = getBinding().f2576b.d;
        m.checkNotNullExpressionValue(textView2, "binding.chatItem.chatListAdapterItemTextName");
        textView2.setText(model.getMeUser().getUsername());
        TextView textView3 = getBinding().f2576b.g;
        m.checkNotNullExpressionValue(textView3, "binding.chatItem.chatListAdapterItemTextTimestamp");
        textView3.setText(TimeUtils.toReadableTimeString$default(requireContext(), ClockFactory.get().currentTimeMillis(), null, 4, null));
        LinkifiedTextView linkifiedTextView = getBinding().f2576b.f2319b;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.chatItem.chatListAdapterItemText");
        linkifiedTextView.setText(getString(R.string.user_settings_appearance_preview_message_1));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showHolyLight() {
        b.a.d.m.d(getContext(), R.string.theme_holy_light_reveal, 0, this.toastManager);
        tryEnableTorchMode(true);
        View view = getBinding().g;
        m.checkNotNullExpressionValue(view, "binding.settingsAppearanceHolyLight");
        view.setVisibility(0);
        Observable<Long> d02 = Observable.d0(3L, TimeUnit.SECONDS);
        m.checkNotNullExpressionValue(d02, "Observable\n        .timer(3, TimeUnit.SECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), WidgetSettingsAppearance.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsAppearance$showHolyLight$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void tryEnableTorchMode(boolean z2) {
        String str;
        if (Build.VERSION.SDK_INT >= 23) {
            Context context = getContext();
            CameraManager cameraManager = (CameraManager) (context != null ? context.getSystemService(ChatInputComponentTypes.CAMERA) : null);
            if (cameraManager != null) {
                try {
                    String[] cameraIdList = cameraManager.getCameraIdList();
                    if (cameraIdList != null && (str = cameraIdList[0]) != null) {
                        cameraManager.setTorchMode(str, z2);
                    }
                } catch (Exception e) {
                    AppLog.g.w("Unable to turn on flashlight", e);
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateTheme(String str) {
        StoreStream.Companion.getUserSettingsSystem().setTheme(str, true, new WidgetSettingsAppearance$updateTheme$1(this, str));
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.toastManager.close();
        super.onDestroyView();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarTitle(R.string.appearance);
        setActionBarSubtitle(R.string.user_settings);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        SeekBar seekBar = getBinding().f;
        m.checkNotNullExpressionValue(seekBar, "binding.settingsAppearanceFontScalingSeekbar");
        seekBar.setMax(70);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Model.Companion companion = Model.Companion;
        BehaviorSubject<Boolean> pureEvilEasterEggSubject = getPureEvilEasterEggSubject();
        m.checkNotNullExpressionValue(pureEvilEasterEggSubject, "pureEvilEasterEggSubject");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.get(pureEvilEasterEggSubject), this, null, 2, null), WidgetSettingsAppearance.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsAppearance$onViewBoundOrOnResume$1(this));
        Observable<Integer> o = this.newFontScaleSubject.o(400L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "newFontScaleSubject\n    …0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(o, this, null, 2, null), WidgetSettingsAppearance.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetSettingsAppearance$onViewBoundOrOnResume$2.INSTANCE);
        CheckedSetting checkedSetting = getBinding().i;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsAppearanceSyncSwitch");
        checkedSetting.setChecked(StoreStream.Companion.getUserSettingsSystem().getIsThemeSyncEnabled());
        getBinding().i.setOnCheckedListener(WidgetSettingsAppearance$onViewBoundOrOnResume$3.INSTANCE);
    }
}
