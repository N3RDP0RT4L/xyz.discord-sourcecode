package com.discord.widgets.settings.premium;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import b.a.a.b.a;
import b.a.a.b.e;
import com.android.billingclient.api.SkuDetails;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlaySku;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function3;
/* compiled from: WidgetChoosePlan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/utilities/billing/GooglePlaySku;", "sku", "", "oldSkuName", "Lcom/android/billingclient/api/SkuDetails;", "upgradeSkuDetails", "", "invoke", "(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChoosePlan$setUpRecycler$2 extends o implements Function3<GooglePlaySku, String, SkuDetails, Unit> {
    public final /* synthetic */ Traits.Location $locationTrait;
    public final /* synthetic */ WidgetChoosePlan this$0;

    /* compiled from: WidgetChoosePlan.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.WidgetChoosePlan$setUpRecycler$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ String $oldSkuName;
        public final /* synthetic */ GooglePlaySku $sku;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(GooglePlaySku googlePlaySku, String str) {
            super(0);
            this.$sku = googlePlaySku;
            this.$oldSkuName = str;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            ChoosePlanViewModel viewModel;
            viewModel = WidgetChoosePlan$setUpRecycler$2.this.this$0.getViewModel();
            viewModel.buy(this.$sku, this.$oldSkuName, WidgetChoosePlan$setUpRecycler$2.this.$locationTrait, "premium_upsell");
        }
    }

    /* compiled from: WidgetChoosePlan.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.WidgetChoosePlan$setUpRecycler$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function0<Unit> {
        public final /* synthetic */ String $oldSkuName;
        public final /* synthetic */ GooglePlaySku $sku;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(GooglePlaySku googlePlaySku, String str) {
            super(0);
            this.$sku = googlePlaySku;
            this.$oldSkuName = str;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            ChoosePlanViewModel viewModel;
            Traits.Location location = new Traits.Location("Premium Upsell Modal", "Premium Upsell Modal", null, null, null, 28, null);
            AnalyticsTracker.paymentFlowStarted$default(AnalyticsTracker.INSTANCE, location, Traits.Subscription.Companion.withGatewayPlanId(this.$sku.getUpgrade().getSkuName()), null, null, 12, null);
            viewModel = WidgetChoosePlan$setUpRecycler$2.this.this$0.getViewModel();
            viewModel.buy(this.$sku.getUpgrade(), this.$oldSkuName, location, "premium_upsell");
        }
    }

    /* compiled from: WidgetChoosePlan.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.WidgetChoosePlan$setUpRecycler$2$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends o implements Function0<Unit> {
        public final /* synthetic */ String $oldSkuName;
        public final /* synthetic */ GooglePlaySku $sku;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass3(GooglePlaySku googlePlaySku, String str) {
            super(0);
            this.$sku = googlePlaySku;
            this.$oldSkuName = str;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            ChoosePlanViewModel viewModel;
            viewModel = WidgetChoosePlan$setUpRecycler$2.this.this$0.getViewModel();
            viewModel.buy(this.$sku, this.$oldSkuName, WidgetChoosePlan$setUpRecycler$2.this.$locationTrait, "yearly_upsell");
        }
    }

    /* compiled from: WidgetChoosePlan.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.WidgetChoosePlan$setUpRecycler$2$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass4 extends o implements Function0<Unit> {
        public final /* synthetic */ String $oldSkuName;
        public final /* synthetic */ GooglePlaySku $sku;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass4(GooglePlaySku googlePlaySku, String str) {
            super(0);
            this.$sku = googlePlaySku;
            this.$oldSkuName = str;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            ChoosePlanViewModel viewModel;
            Traits.Location location = new Traits.Location("Yearly Upsell Modal", "Yearly Upsell Modal", null, null, null, 28, null);
            AnalyticsTracker.paymentFlowStarted$default(AnalyticsTracker.INSTANCE, location, Traits.Subscription.Companion.withGatewayPlanId(this.$sku.getUpgrade().getSkuName()), null, null, 12, null);
            viewModel = WidgetChoosePlan$setUpRecycler$2.this.this$0.getViewModel();
            viewModel.buy(this.$sku.getUpgrade(), this.$oldSkuName, location, "yearly_upsell");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChoosePlan$setUpRecycler$2(WidgetChoosePlan widgetChoosePlan, Traits.Location location) {
        super(3);
        this.this$0 = widgetChoosePlan;
        this.$locationTrait = location;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Unit invoke(GooglePlaySku googlePlaySku, String str, SkuDetails skuDetails) {
        invoke2(googlePlaySku, str, skuDetails);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GooglePlaySku googlePlaySku, String str, SkuDetails skuDetails) {
        ChoosePlanViewModel viewModel;
        m.checkNotNullParameter(googlePlaySku, "sku");
        if (googlePlaySku.getUpgrade() != null && skuDetails != null && googlePlaySku.getType() == GooglePlaySku.Type.PREMIUM_GUILD) {
            AnalyticsTracker.INSTANCE.paymentFlowStep(this.$locationTrait, (r16 & 2) != 0 ? null : Traits.Subscription.Companion.withGatewayPlanId(googlePlaySku.getSkuName()), "premium_upsell", "plan_select", (r16 & 16) != 0 ? null : null, (r16 & 32) != 0 ? null : null);
            a.b bVar = a.k;
            FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            AnonymousClass1 r7 = new AnonymousClass1(googlePlaySku, str);
            AnonymousClass2 r9 = new AnonymousClass2(googlePlaySku, str);
            String b2 = skuDetails.b();
            m.checkNotNullExpressionValue(b2, "upgradeSkuDetails.price");
            boolean areEqual = m.areEqual(googlePlaySku.getSkuName(), str);
            Objects.requireNonNull(bVar);
            m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
            m.checkNotNullParameter(r7, "onContinueClickListener");
            m.checkNotNullParameter(r9, "onUpgradeClickListener");
            m.checkNotNullParameter(b2, "upgradePrice");
            a aVar = new a();
            aVar.l = r7;
            aVar.m = r9;
            aVar.n = b2;
            aVar.o = areEqual;
            aVar.show(parentFragmentManager, a.class.getSimpleName());
        } else if (googlePlaySku.getUpgrade() == null || skuDetails == null) {
            viewModel = this.this$0.getViewModel();
            viewModel.buy(googlePlaySku, str, this.$locationTrait, "plan_select");
        } else {
            AnalyticsTracker.INSTANCE.paymentFlowStep(this.$locationTrait, (r16 & 2) != 0 ? null : Traits.Subscription.Companion.withGatewayPlanId(googlePlaySku.getSkuName()), "yearly_upsell", "plan_select", (r16 & 16) != 0 ? null : null, (r16 & 32) != 0 ? null : null);
            e.b bVar2 = e.k;
            FragmentManager parentFragmentManager2 = this.this$0.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            AnonymousClass3 r72 = new AnonymousClass3(googlePlaySku, str);
            AnonymousClass4 r92 = new AnonymousClass4(googlePlaySku, str);
            String b3 = skuDetails.b();
            m.checkNotNullExpressionValue(b3, "upgradeSkuDetails.price");
            boolean areEqual2 = m.areEqual(googlePlaySku.getSkuName(), str);
            boolean z2 = googlePlaySku.getType() == GooglePlaySku.Type.PREMIUM_TIER_1 || googlePlaySku.getType() == GooglePlaySku.Type.PREMIUM_TIER_1_AND_PREMIUM_GUILD;
            Objects.requireNonNull(bVar2);
            m.checkNotNullParameter(parentFragmentManager2, "fragmentManager");
            m.checkNotNullParameter(r72, "onMonthlyClickListener");
            m.checkNotNullParameter(r92, "onYearlyClickListener");
            m.checkNotNullParameter(b3, "upgradePrice");
            e eVar = new e();
            eVar.l = r72;
            eVar.m = r92;
            eVar.n = b3;
            eVar.o = areEqual2;
            Bundle bundle = new Bundle();
            bundle.putBoolean("ARG_IS_TIER_1", z2);
            eVar.setArguments(bundle);
            eVar.show(parentFragmentManager2, e.class.getSimpleName());
        }
    }
}
