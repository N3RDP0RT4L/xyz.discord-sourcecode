package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.a.a.c;
import b.a.a.b.f;
import b.a.a.b.g;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChoosePlanBinding;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlayBillingManager;
import com.discord.utilities.billing.GooglePlaySku;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.premium.ChoosePlanViewModel;
import d0.z.d.a0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetChoosePlan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 %2\u00020\u0001:\u0002%&B\u0007¢\u0006\u0004\b$\u0010\fJ\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\fR\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001a\u001a\u00020\u00158B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"¨\u0006'"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetChoosePlan;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;)Lkotlin/Unit;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;)V", "setUpRecycler", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;", "adapter", "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;", "viewModel", "Landroidx/recyclerview/widget/LinearLayoutManager;", "planLayoutManager", "Landroidx/recyclerview/widget/LinearLayoutManager;", "Lcom/discord/databinding/WidgetChoosePlanBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChoosePlanBinding;", "binding", HookHelper.constructorName, "Companion", "ViewType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChoosePlan extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChoosePlan.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChoosePlanBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final String RESULT_EXTRA_LOCATION_TRAIT = "result_extra_location_trait";
    public static final String RESULT_EXTRA_OLD_SKU_NAME = "result_extra_current_sku_name";
    public static final String RESULT_VIEW_TYPE = "result_view_type";
    private WidgetChoosePlanAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChoosePlan$binding$2.INSTANCE, null, 2, null);
    private LinearLayoutManager planLayoutManager;
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetChoosePlan.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001c\u0010\u001dJO\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u00022\u0010\b\u0002\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\f\u001a\u00020\u000b2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b\u0010\u0010\u0011J)\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0013\u001a\u00020\u00122\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001a\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0019¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;", "", "Landroid/content/Context;", "context", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "launcher", "Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;", "viewType", "", "oldSkuName", "Lcom/discord/utilities/analytics/Traits$Location;", "locationTrait", "Lcom/discord/utilities/analytics/Traits$Subscription;", "subscriptionTrait", "", "launch", "(Landroid/content/Context;Landroidx/activity/result/ActivityResultLauncher;Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;)V", "Lcom/discord/app/AppFragment;", "fragment", "Lkotlin/Function0;", "callback", "registerForResult", "(Lcom/discord/app/AppFragment;Lkotlin/jvm/functions/Function0;)Landroidx/activity/result/ActivityResultLauncher;", "RESULT_EXTRA_LOCATION_TRAIT", "Ljava/lang/String;", "RESULT_EXTRA_OLD_SKU_NAME", "RESULT_VIEW_TYPE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, ActivityResultLauncher<Intent> activityResultLauncher, ViewType viewType, String str, Traits.Location location, Traits.Subscription subscription) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(viewType, "viewType");
            m.checkNotNullParameter(location, "locationTrait");
            Intent intent = new Intent();
            intent.putExtra(WidgetChoosePlan.RESULT_EXTRA_OLD_SKU_NAME, str);
            intent.putExtra(WidgetChoosePlan.RESULT_EXTRA_LOCATION_TRAIT, location);
            intent.putExtra(WidgetChoosePlan.RESULT_VIEW_TYPE, viewType);
            AnalyticsTracker.paymentFlowStarted$default(AnalyticsTracker.INSTANCE, location, subscription, null, null, 12, null);
            if (activityResultLauncher != null) {
                j.g.f(context, activityResultLauncher, WidgetChoosePlan.class, intent);
            } else {
                j.d(context, WidgetChoosePlan.class, intent);
            }
        }

        public final ActivityResultLauncher<Intent> registerForResult(AppFragment appFragment, final Function0<Unit> function0) {
            m.checkNotNullParameter(appFragment, "fragment");
            m.checkNotNullParameter(function0, "callback");
            ActivityResultLauncher<Intent> registerForActivityResult = appFragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.settings.premium.WidgetChoosePlan$Companion$registerForResult$1
                public final void onActivityResult(ActivityResult activityResult) {
                    m.checkNotNullExpressionValue(activityResult, "activityResult");
                    if (activityResult.getResultCode() == -1) {
                        Function0.this.invoke();
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…k()\n          }\n        }");
            return registerForActivityResult;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChoosePlan.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "BUY_PREMIUM_TIER_2", "BUY_PREMIUM_TIER_1", "BUY_PREMIUM_GUILD", "SWITCH_PLANS", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ViewType {
        BUY_PREMIUM_TIER_2,
        BUY_PREMIUM_TIER_1,
        BUY_PREMIUM_GUILD,
        SWITCH_PLANS
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            GooglePlaySku.Type.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[GooglePlaySku.Type.PREMIUM_TIER_1.ordinal()] = 1;
            iArr[GooglePlaySku.Type.PREMIUM_TIER_2.ordinal()] = 2;
            iArr[GooglePlaySku.Type.PREMIUM_TIER_1_AND_PREMIUM_GUILD.ordinal()] = 3;
            iArr[GooglePlaySku.Type.PREMIUM_TIER_2_AND_PREMIUM_GUILD.ordinal()] = 4;
            iArr[GooglePlaySku.Type.PREMIUM_GUILD.ordinal()] = 5;
        }
    }

    public WidgetChoosePlan() {
        super(R.layout.widget_choose_plan);
        WidgetChoosePlan$viewModel$2 widgetChoosePlan$viewModel$2 = new WidgetChoosePlan$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ChoosePlanViewModel.class), new WidgetChoosePlan$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetChoosePlan$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Unit configureUI(ChoosePlanViewModel.ViewState viewState) {
        if (viewState instanceof ChoosePlanViewModel.ViewState.Loading) {
            return null;
        }
        if (viewState instanceof ChoosePlanViewModel.ViewState.Loaded) {
            WidgetChoosePlanAdapter widgetChoosePlanAdapter = this.adapter;
            if (widgetChoosePlanAdapter == null) {
                m.throwUninitializedPropertyAccessException("adapter");
            }
            ChoosePlanViewModel.ViewState.Loaded loaded = (ChoosePlanViewModel.ViewState.Loaded) viewState;
            widgetChoosePlanAdapter.setData(loaded.getItems());
            LinearLayout linearLayout = getBinding().f2330b;
            m.checkNotNullExpressionValue(linearLayout, "binding.choosePlanEmptyContainer");
            linearLayout.setVisibility(loaded.isEmpty() ? 0 : 8);
            if (m.areEqual(loaded.getPurchasesQueryState(), StoreGooglePlayPurchases.QueryState.InProgress.INSTANCE)) {
                DimmerView.setDimmed$default(getBinding().e, true, false, 2, null);
                return Unit.a;
            }
            DimmerView.setDimmed$default(getBinding().e, false, false, 2, null);
            return Unit.a;
        }
        throw new NoWhenBranchMatchedException();
    }

    private final WidgetChoosePlanBinding getBinding() {
        return (WidgetChoosePlanBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ChoosePlanViewModel getViewModel() {
        return (ChoosePlanViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(ChoosePlanViewModel.Event event) {
        if (event instanceof ChoosePlanViewModel.Event.ErrorSkuPurchase) {
            b.a.d.m.i(this, ((ChoosePlanViewModel.Event.ErrorSkuPurchase) event).getMessage(), 0, 4);
        } else if (event instanceof ChoosePlanViewModel.Event.StartSkuPurchase) {
            GooglePlayBillingManager.INSTANCE.launchBillingFlow(requireAppActivity(), ((ChoosePlanViewModel.Event.StartSkuPurchase) event).getBillingParams());
        } else if (event instanceof ChoosePlanViewModel.Event.CompleteSkuPurchase) {
            ChoosePlanViewModel.Event.CompleteSkuPurchase completeSkuPurchase = (ChoosePlanViewModel.Event.CompleteSkuPurchase) event;
            GooglePlaySku fromSkuName = GooglePlaySku.Companion.fromSkuName(completeSkuPurchase.getSkuName());
            if (fromSkuName != null) {
                WidgetChoosePlan$handleEvent$onDismiss$1 widgetChoosePlan$handleEvent$onDismiss$1 = new WidgetChoosePlan$handleEvent$onDismiss$1(this);
                int ordinal = fromSkuName.getType().ordinal();
                if (ordinal == 0) {
                    f.a aVar = f.k;
                    FragmentManager parentFragmentManager = getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    aVar.a(parentFragmentManager, widgetChoosePlan$handleEvent$onDismiss$1, false);
                } else if (ordinal == 1) {
                    f.a aVar2 = f.k;
                    FragmentManager parentFragmentManager2 = getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
                    aVar2.a(parentFragmentManager2, widgetChoosePlan$handleEvent$onDismiss$1, true);
                } else if (ordinal == 2 || ordinal == 3) {
                    g.a aVar3 = g.k;
                    FragmentManager parentFragmentManager3 = getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager3, "parentFragmentManager");
                    String planName = completeSkuPurchase.getPlanName();
                    Objects.requireNonNull(aVar3);
                    m.checkNotNullParameter(parentFragmentManager3, "fragmentManager");
                    m.checkNotNullParameter(widgetChoosePlan$handleEvent$onDismiss$1, "onDismiss");
                    m.checkNotNullParameter(planName, "planName");
                    g gVar = new g();
                    gVar.l = widgetChoosePlan$handleEvent$onDismiss$1;
                    Bundle bundle = new Bundle();
                    bundle.putString("extra_plan_text", planName);
                    gVar.setArguments(bundle);
                    gVar.show(parentFragmentManager3, g.class.getSimpleName());
                } else if (ordinal == 4) {
                    c.a aVar4 = c.l;
                    FragmentManager parentFragmentManager4 = getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager4, "parentFragmentManager");
                    aVar4.a(parentFragmentManager4, requireContext(), null, fromSkuName.getPremiumSubscriptionCount(), false, widgetChoosePlan$handleEvent$onDismiss$1);
                }
            }
        }
    }

    private final void setUpRecycler() {
        RecyclerView recyclerView = getBinding().d;
        m.checkNotNullExpressionValue(recyclerView, "binding.choosePlanRecycler");
        this.planLayoutManager = new LinearLayoutManager(recyclerView.getContext(), 1, false);
        RecyclerView recyclerView2 = getBinding().d;
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        m.checkNotNullExpressionValue(recyclerView2, "it");
        this.adapter = (WidgetChoosePlanAdapter) companion.configure(new WidgetChoosePlanAdapter(recyclerView2));
        Serializable serializableExtra = getMostRecentIntent().getSerializableExtra(RESULT_EXTRA_LOCATION_TRAIT);
        Objects.requireNonNull(serializableExtra, "null cannot be cast to non-null type com.discord.utilities.analytics.Traits.Location");
        Traits.Location location = (Traits.Location) serializableExtra;
        WidgetChoosePlanAdapter widgetChoosePlanAdapter = this.adapter;
        if (widgetChoosePlanAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChoosePlanAdapter.setOnClickPlan(new WidgetChoosePlan$setUpRecycler$2(this, location));
        RecyclerView recyclerView3 = getBinding().d;
        m.checkNotNullExpressionValue(recyclerView3, "binding.choosePlanRecycler");
        LinearLayoutManager linearLayoutManager = this.planLayoutManager;
        if (linearLayoutManager == null) {
            m.throwUninitializedPropertyAccessException("planLayoutManager");
        }
        recyclerView3.setLayoutManager(linearLayoutManager);
        RecyclerView recyclerView4 = getBinding().d;
        m.checkNotNullExpressionValue(recyclerView4, "binding.choosePlanRecycler");
        WidgetChoosePlanAdapter widgetChoosePlanAdapter2 = this.adapter;
        if (widgetChoosePlanAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        recyclerView4.setAdapter(widgetChoosePlanAdapter2);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setUpRecycler();
        LinkifiedTextView linkifiedTextView = getBinding().c;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.choosePlanEmptyDescription");
        b.m(linkifiedTextView, R.string.premium_no_plans_body, new Object[]{b.a.d.f.a.a(360055386693L, null)}, (r4 & 4) != 0 ? b.g.j : null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getViewModel().observeEvents(), this, null, 2, null), WidgetChoosePlan.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChoosePlan$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getViewModel().observeViewState(), this, null, 2, null), WidgetChoosePlan.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChoosePlan$onViewBoundOrOnResume$2(this));
    }
}
