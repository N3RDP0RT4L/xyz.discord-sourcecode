package com.discord.widgets.settings.premium;

import com.discord.api.premium.ClaimedOutboundPromotion;
import com.discord.widgets.settings.premium.ClaimOutboundPromoViewModel;
import com.discord.widgets.settings.premium.ClaimStatus;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: ClaimOutboundPromoViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/premium/ClaimedOutboundPromotion;", "claimedPromo", "", "invoke", "(Lcom/discord/api/premium/ClaimedOutboundPromotion;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ClaimOutboundPromoViewModel$claimPromo$1 extends o implements Function1<ClaimedOutboundPromotion, Unit> {
    public final /* synthetic */ ClaimOutboundPromoViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ClaimOutboundPromoViewModel$claimPromo$1(ClaimOutboundPromoViewModel claimOutboundPromoViewModel) {
        super(1);
        this.this$0 = claimOutboundPromoViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ClaimedOutboundPromotion claimedOutboundPromotion) {
        invoke2(claimedOutboundPromotion);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ClaimedOutboundPromotion claimedOutboundPromotion) {
        PublishSubject publishSubject;
        m.checkNotNullParameter(claimedOutboundPromotion, "claimedPromo");
        ClaimOutboundPromoViewModel claimOutboundPromoViewModel = this.this$0;
        ClaimStatus.Claimed claimed = new ClaimStatus.Claimed(claimedOutboundPromotion.b().b(), claimedOutboundPromotion.a(), claimedOutboundPromotion.b().c(), claimedOutboundPromotion.d(), claimedOutboundPromotion.c());
        this.this$0.updateViewState(new ClaimOutboundPromoViewModel.ViewState.Claimed(claimed));
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(new ClaimOutboundPromoViewModel.Event.Claimed(claimed));
        claimOutboundPromoViewModel.claimStatus = claimed;
    }
}
