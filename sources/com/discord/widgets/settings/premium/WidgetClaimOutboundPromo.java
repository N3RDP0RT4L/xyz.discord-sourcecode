package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetClaimOutboundPromoBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.premium.ClaimOutboundPromoViewModel;
import com.discord.widgets.settings.premium.ClaimStatus;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetClaimOutboundPromo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 -2\u00020\u0001:\u0001-B\u0007¢\u0006\u0004\b,\u0010\u0018J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\r\u001a\u00020\u00042\b\b\u0001\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u0011\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u001d\u0010'\u001a\u00020\"8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u0016\u0010+\u001a\u00020(8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b)\u0010*¨\u0006."}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetClaimOutboundPromo;", "Lcom/discord/app/AppDialog;", "Lcom/discord/widgets/settings/premium/ClaimOutboundPromoViewModel$ViewState;", "viewState", "", "configureUi", "(Lcom/discord/widgets/settings/premium/ClaimOutboundPromoViewModel$ViewState;)V", "Lcom/discord/widgets/settings/premium/ClaimOutboundPromoViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/settings/premium/ClaimOutboundPromoViewModel$Event;)V", "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "setCodeBoxColor", "(I)V", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetClaimOutboundPromoBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetClaimOutboundPromoBinding;", "binding", "Lcom/google/android/material/shape/MaterialShapeDrawable;", "codeBoxBackground", "Lcom/google/android/material/shape/MaterialShapeDrawable;", "Lcom/discord/widgets/settings/premium/ClaimOutboundPromoViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/premium/ClaimOutboundPromoViewModel;", "viewModel", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "getClaimStatus", "()Lcom/discord/widgets/settings/premium/ClaimStatus;", "claimStatus", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetClaimOutboundPromo extends AppDialog {
    private static final String ARG_CLAIM_STATUS = "ARG_CLAIM_STATUS";
    private static final int INDEX_FAILURE = 2;
    private static final int INDEX_LOADING = 0;
    private static final int INDEX_SUCCESS = 1;
    private static final String KEY_PROMO_CLAIMED = "KEY_PROMO_CLAIMED";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetClaimOutboundPromo$binding$2.INSTANCE, null, 2, null);
    private final MaterialShapeDrawable codeBoxBackground;
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetClaimOutboundPromo.class, "binding", "getBinding()Lcom/discord/databinding/WidgetClaimOutboundPromoBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetClaimOutboundPromo.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J1\u0010\n\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0011R\u0016\u0010\u0014\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u000e¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetClaimOutboundPromo$Companion;", "", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "claimStatus", "Landroidx/fragment/app/Fragment;", "fragment", "Lkotlin/Function1;", "Lcom/discord/widgets/settings/premium/ClaimStatus$Claimed;", "", "onPromoClaimed", "showAndRegisterForClaimResult", "(Lcom/discord/widgets/settings/premium/ClaimStatus;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)V", "", WidgetClaimOutboundPromo.ARG_CLAIM_STATUS, "Ljava/lang/String;", "", "INDEX_FAILURE", "I", "INDEX_LOADING", "INDEX_SUCCESS", WidgetClaimOutboundPromo.KEY_PROMO_CLAIMED, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void showAndRegisterForClaimResult(ClaimStatus claimStatus, Fragment fragment, Function1<? super ClaimStatus.Claimed, Unit> function1) {
            m.checkNotNullParameter(claimStatus, "claimStatus");
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(function1, "onPromoClaimed");
            if (fragment.getParentFragmentManager().findFragmentByTag("javaClass") == null) {
                if (claimStatus instanceof ClaimStatus.Unclaimed) {
                    FragmentKt.setFragmentResultListener(fragment, WidgetClaimOutboundPromo.KEY_PROMO_CLAIMED, new WidgetClaimOutboundPromo$Companion$showAndRegisterForClaimResult$2(function1));
                }
                WidgetClaimOutboundPromo widgetClaimOutboundPromo = new WidgetClaimOutboundPromo();
                Bundle bundle = new Bundle();
                bundle.putParcelable(WidgetClaimOutboundPromo.ARG_CLAIM_STATUS, claimStatus);
                widgetClaimOutboundPromo.setArguments(bundle);
                FragmentManager parentFragmentManager = fragment.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "fragment.parentFragmentManager");
                widgetClaimOutboundPromo.show(parentFragmentManager, "javaClass");
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetClaimOutboundPromo() {
        super(R.layout.widget_claim_outbound_promo);
        WidgetClaimOutboundPromo$viewModel$2 widgetClaimOutboundPromo$viewModel$2 = new WidgetClaimOutboundPromo$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ClaimOutboundPromoViewModel.class), new WidgetClaimOutboundPromo$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetClaimOutboundPromo$viewModel$2));
        MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(new ShapeAppearanceModel.Builder().setAllCornerSizes(DimenUtils.dpToPixels(4)).build());
        materialShapeDrawable.setFillColor(ColorStateList.valueOf(0));
        this.codeBoxBackground = materialShapeDrawable;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(ClaimOutboundPromoViewModel.ViewState viewState) {
        View view = getView();
        if (!(view instanceof ViewGroup)) {
            view = null;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        if (viewGroup != null) {
            AutoTransition autoTransition = new AutoTransition();
            autoTransition.setOrdering(0);
            autoTransition.setDuration(200L);
            TransitionManager.beginDelayedTransition(viewGroup, autoTransition);
        }
        if (viewState instanceof ClaimOutboundPromoViewModel.ViewState.ClaimInProgress) {
            AppViewFlipper appViewFlipper = getBinding().g;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.claimPromoFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (viewState instanceof ClaimOutboundPromoViewModel.ViewState.Claimed) {
            TextView textView = getBinding().f2331b;
            m.checkNotNullExpressionValue(textView, "binding.claimPromoBody");
            ClaimOutboundPromoViewModel.ViewState.Claimed claimed = (ClaimOutboundPromoViewModel.ViewState.Claimed) viewState;
            textView.setText(claimed.getClaimedStatus().getBody());
            TextView textView2 = getBinding().c;
            m.checkNotNullExpressionValue(textView2, "binding.claimPromoCode");
            textView2.setText(claimed.getClaimedStatus().getCode());
            AppViewFlipper appViewFlipper2 = getBinding().g;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.claimPromoFlipper");
            appViewFlipper2.setDisplayedChild(1);
            setCancelable(true);
        } else if (viewState instanceof ClaimOutboundPromoViewModel.ViewState.ClaimFailed) {
            AppViewFlipper appViewFlipper3 = getBinding().g;
            m.checkNotNullExpressionValue(appViewFlipper3, "binding.claimPromoFlipper");
            appViewFlipper3.setMeasureAllChildren(false);
            AppViewFlipper appViewFlipper4 = getBinding().g;
            m.checkNotNullExpressionValue(appViewFlipper4, "binding.claimPromoFlipper");
            appViewFlipper4.setDisplayedChild(2);
            setCancelable(true);
        }
    }

    private final WidgetClaimOutboundPromoBinding getBinding() {
        return (WidgetClaimOutboundPromoBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ClaimStatus getClaimStatus() {
        Parcelable parcelable = getArgumentsOrDefault().getParcelable(ARG_CLAIM_STATUS);
        m.checkNotNull(parcelable);
        return (ClaimStatus) parcelable;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ClaimOutboundPromoViewModel getViewModel() {
        return (ClaimOutboundPromoViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(ClaimOutboundPromoViewModel.Event event) {
        Unit unit;
        if (event instanceof ClaimOutboundPromoViewModel.Event.Claimed) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(KEY_PROMO_CLAIMED, ((ClaimOutboundPromoViewModel.Event.Claimed) event).getClaimedStatus());
            unit = Unit.a;
            FragmentKt.setFragmentResult(this, KEY_PROMO_CLAIMED, bundle);
        } else if (event instanceof ClaimOutboundPromoViewModel.Event.CopyCode) {
            Object systemService = requireContext().getSystemService("clipboard");
            Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
            ((ClipboardManager) systemService).setPrimaryClip(ClipData.newPlainText("Promo Code", ((ClaimOutboundPromoViewModel.Event.CopyCode) event).getCode()));
            MaterialButton materialButton = getBinding().e;
            m.checkNotNullExpressionValue(materialButton, "binding.claimPromoCopyButton");
            b.m(materialButton, R.string.copied, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            setCodeBoxColor(ContextCompat.getColor(requireContext(), R.color.status_green_600));
            unit = Unit.a;
        } else if (event instanceof ClaimOutboundPromoViewModel.Event.OpenRedemptionUrl) {
            UriHandler uriHandler = UriHandler.INSTANCE;
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            UriHandler.handle$default(uriHandler, requireContext, ((ClaimOutboundPromoViewModel.Event.OpenRedemptionUrl) event).getUrl(), null, 4, null);
            dismiss();
            unit = Unit.a;
        } else if (event instanceof ClaimOutboundPromoViewModel.Event.Dismiss) {
            dismiss();
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }

    private final void setCodeBoxColor(@ColorInt int i) {
        this.codeBoxBackground.setStroke(DimenUtils.dpToPixels(1), i);
        getBinding().d.invalidate();
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setCancelable(false);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        ViewCompat.setBackground(getBinding().d, this.codeBoxBackground);
        setCodeBoxColor(ColorCompat.getThemedColor(view, (int) R.attr.colorInteractiveNormal));
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetClaimOutboundPromo$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ClaimOutboundPromoViewModel viewModel;
                viewModel = WidgetClaimOutboundPromo.this.getViewModel();
                viewModel.copyClicked();
            }
        });
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetClaimOutboundPromo$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ClaimOutboundPromoViewModel viewModel;
                viewModel = WidgetClaimOutboundPromo.this.getViewModel();
                viewModel.redeemClicked();
            }
        });
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetClaimOutboundPromo$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ClaimOutboundPromoViewModel viewModel;
                viewModel = WidgetClaimOutboundPromo.this.getViewModel();
                viewModel.maybeLaterClicked();
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetClaimOutboundPromo$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ClaimOutboundPromoViewModel viewModel;
                viewModel = WidgetClaimOutboundPromo.this.getViewModel();
                viewModel.failureCloseClicked();
            }
        });
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetClaimOutboundPromo.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetClaimOutboundPromo$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetClaimOutboundPromo.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetClaimOutboundPromo$onViewBoundOrOnResume$2(this));
    }
}
