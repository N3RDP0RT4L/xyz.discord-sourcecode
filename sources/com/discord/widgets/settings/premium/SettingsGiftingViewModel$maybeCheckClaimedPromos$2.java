package com.discord.widgets.settings.premium;

import androidx.core.app.NotificationCompat;
import com.discord.api.premium.ClaimedOutboundPromotion;
import d0.t.n;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
/* compiled from: SettingsGiftingViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", "Lcom/discord/api/premium/ClaimedOutboundPromotion;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsGiftingViewModel$maybeCheckClaimedPromos$2<T, R> implements b<Throwable, List<? extends ClaimedOutboundPromotion>> {
    public static final SettingsGiftingViewModel$maybeCheckClaimedPromos$2 INSTANCE = new SettingsGiftingViewModel$maybeCheckClaimedPromos$2();

    public final List<ClaimedOutboundPromotion> call(Throwable th) {
        return n.emptyList();
    }
}
