package com.discord.widgets.settings.premium;

import com.discord.models.domain.ModelGift;
import com.discord.stores.StoreEntitlements;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.widgets.settings.premium.SettingsGiftingViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: SettingsGiftingViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelGift;", "it", "", "invoke", "(Lcom/discord/models/domain/ModelGift;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsGiftingViewModel$handleGooglePlayPurchaseEvent$2 extends o implements Function1<ModelGift, Unit> {
    public final /* synthetic */ StoreGooglePlayPurchases.Event $event;
    public final /* synthetic */ SettingsGiftingViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsGiftingViewModel$handleGooglePlayPurchaseEvent$2(SettingsGiftingViewModel settingsGiftingViewModel, StoreGooglePlayPurchases.Event event) {
        super(1);
        this.this$0 = settingsGiftingViewModel;
        this.$event = event;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelGift modelGift) {
        invoke2(modelGift);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelGift modelGift) {
        StoreEntitlements storeEntitlements;
        PublishSubject publishSubject;
        m.checkNotNullParameter(modelGift, "it");
        storeEntitlements = this.this$0.storeEntitlements;
        storeEntitlements.fetchMyGiftEntitlements();
        publishSubject = this.this$0.giftPurchaseEventSubject;
        publishSubject.k.onNext(new SettingsGiftingViewModel.GiftPurchaseEvent.CompleteGiftPurchase(((StoreGooglePlayPurchases.Event.PurchaseQuerySuccess) this.$event).getNewSkuName(), modelGift.getCode()));
    }
}
