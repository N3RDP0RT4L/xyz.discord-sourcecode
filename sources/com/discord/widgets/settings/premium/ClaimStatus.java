package com.discord.widgets.settings.premium;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.UtcDateTimeParceler;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ClaimStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/ClaimStatus;", "Landroid/os/Parcelable;", HookHelper.constructorName, "()V", "Claimed", "Unclaimed", "Lcom/discord/widgets/settings/premium/ClaimStatus$Unclaimed;", "Lcom/discord/widgets/settings/premium/ClaimStatus$Claimed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class ClaimStatus implements Parcelable {

    /* compiled from: ClaimStatus.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\r\b\u0087\b\u0018\u00002\u00020\u0001B3\u0012\n\u0010\u000e\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0006\u0012\u0006\u0010\u0010\u001a\u00020\u0006\u0012\u0006\u0010\u0011\u001a\u00020\u0006\u0012\u0006\u0010\u0012\u001a\u00020\u000b¢\u0006\u0004\b-\u0010.J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\n\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJF\u0010\u0013\u001a\u00020\u00002\f\b\u0002\u0010\u000e\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u000f\u001a\u00020\u00062\b\b\u0002\u0010\u0010\u001a\u00020\u00062\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0012\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0015\u0010\bJ\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0018J \u0010#\u001a\u00020\"2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b#\u0010$R\u0019\u0010\u000f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010%\u001a\u0004\b&\u0010\bR\u001d\u0010\u000e\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010'\u001a\u0004\b(\u0010\u0005R\u0019\u0010\u0010\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010%\u001a\u0004\b)\u0010\bR\u0019\u0010\u0011\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b*\u0010\bR\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010+\u001a\u0004\b,\u0010\r¨\u0006/"}, d2 = {"Lcom/discord/widgets/settings/premium/ClaimStatus$Claimed;", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "", "Lcom/discord/primitives/PromoId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "component4", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component5", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "promoId", ModelAuditLogEntry.CHANGE_KEY_CODE, "body", "link", "redeemBy", "copy", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;)Lcom/discord/widgets/settings/premium/ClaimStatus$Claimed;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Ljava/lang/String;", "getCode", "J", "getPromoId", "getBody", "getLink", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getRedeemBy", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Claimed extends ClaimStatus {
        public static final Parcelable.Creator<Claimed> CREATOR = new Creator();
        private final String body;
        private final String code;
        private final String link;
        private final long promoId;
        private final UtcDateTime redeemBy;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static class Creator implements Parcelable.Creator<Claimed> {
            /* JADX WARN: Can't rename method to resolve collision */
            @Override // android.os.Parcelable.Creator
            public final Claimed createFromParcel(Parcel parcel) {
                m.checkNotNullParameter(parcel, "in");
                return new Claimed(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString(), UtcDateTimeParceler.INSTANCE.create(parcel));
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // android.os.Parcelable.Creator
            public final Claimed[] newArray(int i) {
                return new Claimed[i];
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Claimed(long j, String str, String str2, String str3, UtcDateTime utcDateTime) {
            super(null);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
            m.checkNotNullParameter(str2, "body");
            m.checkNotNullParameter(str3, "link");
            m.checkNotNullParameter(utcDateTime, "redeemBy");
            this.promoId = j;
            this.code = str;
            this.body = str2;
            this.link = str3;
            this.redeemBy = utcDateTime;
        }

        public static /* synthetic */ Claimed copy$default(Claimed claimed, long j, String str, String str2, String str3, UtcDateTime utcDateTime, int i, Object obj) {
            if ((i & 1) != 0) {
                j = claimed.promoId;
            }
            long j2 = j;
            if ((i & 2) != 0) {
                str = claimed.code;
            }
            String str4 = str;
            if ((i & 4) != 0) {
                str2 = claimed.body;
            }
            String str5 = str2;
            if ((i & 8) != 0) {
                str3 = claimed.link;
            }
            String str6 = str3;
            if ((i & 16) != 0) {
                utcDateTime = claimed.redeemBy;
            }
            return claimed.copy(j2, str4, str5, str6, utcDateTime);
        }

        public final long component1() {
            return this.promoId;
        }

        public final String component2() {
            return this.code;
        }

        public final String component3() {
            return this.body;
        }

        public final String component4() {
            return this.link;
        }

        public final UtcDateTime component5() {
            return this.redeemBy;
        }

        public final Claimed copy(long j, String str, String str2, String str3, UtcDateTime utcDateTime) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
            m.checkNotNullParameter(str2, "body");
            m.checkNotNullParameter(str3, "link");
            m.checkNotNullParameter(utcDateTime, "redeemBy");
            return new Claimed(j, str, str2, str3, utcDateTime);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Claimed)) {
                return false;
            }
            Claimed claimed = (Claimed) obj;
            return this.promoId == claimed.promoId && m.areEqual(this.code, claimed.code) && m.areEqual(this.body, claimed.body) && m.areEqual(this.link, claimed.link) && m.areEqual(this.redeemBy, claimed.redeemBy);
        }

        public final String getBody() {
            return this.body;
        }

        public final String getCode() {
            return this.code;
        }

        public final String getLink() {
            return this.link;
        }

        public final long getPromoId() {
            return this.promoId;
        }

        public final UtcDateTime getRedeemBy() {
            return this.redeemBy;
        }

        public int hashCode() {
            int a = b.a(this.promoId) * 31;
            String str = this.code;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.body;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.link;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            UtcDateTime utcDateTime = this.redeemBy;
            if (utcDateTime != null) {
                i = utcDateTime.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Claimed(promoId=");
            R.append(this.promoId);
            R.append(", code=");
            R.append(this.code);
            R.append(", body=");
            R.append(this.body);
            R.append(", link=");
            R.append(this.link);
            R.append(", redeemBy=");
            R.append(this.redeemBy);
            R.append(")");
            return R.toString();
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            m.checkNotNullParameter(parcel, "parcel");
            parcel.writeLong(this.promoId);
            parcel.writeString(this.code);
            parcel.writeString(this.body);
            parcel.writeString(this.link);
            UtcDateTimeParceler.INSTANCE.write((UtcDateTimeParceler) this.redeemBy, parcel, i);
        }
    }

    /* compiled from: ClaimStatus.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\t\b\u0087\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b#\u0010$J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0012J \u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001f\u001a\u0004\b \u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010!\u001a\u0004\b\"\u0010\b¨\u0006%"}, d2 = {"Lcom/discord/widgets/settings/premium/ClaimStatus$Unclaimed;", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "", "Lcom/discord/primitives/PromoId;", "component1", "()J", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component2", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "promoId", "claimBy", "copy", "(JLcom/discord/api/utcdatetime/UtcDateTime;)Lcom/discord/widgets/settings/premium/ClaimStatus$Unclaimed;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "J", "getPromoId", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getClaimBy", HookHelper.constructorName, "(JLcom/discord/api/utcdatetime/UtcDateTime;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Unclaimed extends ClaimStatus {
        public static final Parcelable.Creator<Unclaimed> CREATOR = new Creator();
        private final UtcDateTime claimBy;
        private final long promoId;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static class Creator implements Parcelable.Creator<Unclaimed> {
            /* JADX WARN: Can't rename method to resolve collision */
            @Override // android.os.Parcelable.Creator
            public final Unclaimed createFromParcel(Parcel parcel) {
                m.checkNotNullParameter(parcel, "in");
                return new Unclaimed(parcel.readLong(), UtcDateTimeParceler.INSTANCE.create(parcel));
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // android.os.Parcelable.Creator
            public final Unclaimed[] newArray(int i) {
                return new Unclaimed[i];
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Unclaimed(long j, UtcDateTime utcDateTime) {
            super(null);
            m.checkNotNullParameter(utcDateTime, "claimBy");
            this.promoId = j;
            this.claimBy = utcDateTime;
        }

        public static /* synthetic */ Unclaimed copy$default(Unclaimed unclaimed, long j, UtcDateTime utcDateTime, int i, Object obj) {
            if ((i & 1) != 0) {
                j = unclaimed.promoId;
            }
            if ((i & 2) != 0) {
                utcDateTime = unclaimed.claimBy;
            }
            return unclaimed.copy(j, utcDateTime);
        }

        public final long component1() {
            return this.promoId;
        }

        public final UtcDateTime component2() {
            return this.claimBy;
        }

        public final Unclaimed copy(long j, UtcDateTime utcDateTime) {
            m.checkNotNullParameter(utcDateTime, "claimBy");
            return new Unclaimed(j, utcDateTime);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Unclaimed)) {
                return false;
            }
            Unclaimed unclaimed = (Unclaimed) obj;
            return this.promoId == unclaimed.promoId && m.areEqual(this.claimBy, unclaimed.claimBy);
        }

        public final UtcDateTime getClaimBy() {
            return this.claimBy;
        }

        public final long getPromoId() {
            return this.promoId;
        }

        public int hashCode() {
            int a = b.a(this.promoId) * 31;
            UtcDateTime utcDateTime = this.claimBy;
            return a + (utcDateTime != null ? utcDateTime.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("Unclaimed(promoId=");
            R.append(this.promoId);
            R.append(", claimBy=");
            R.append(this.claimBy);
            R.append(")");
            return R.toString();
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            m.checkNotNullParameter(parcel, "parcel");
            parcel.writeLong(this.promoId);
            UtcDateTimeParceler.INSTANCE.write((UtcDateTimeParceler) this.claimBy, parcel, i);
        }
    }

    private ClaimStatus() {
    }

    public /* synthetic */ ClaimStatus(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
