package com.discord.widgets.settings.premium;

import com.discord.models.domain.ModelSubscription;
import com.discord.utilities.premium.PremiumUtils;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetSettingsPremium.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$1 extends o implements Function0<Unit> {
    public final /* synthetic */ ModelSubscription $premiumSubscription;
    public final /* synthetic */ WidgetSettingsPremium this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$1(WidgetSettingsPremium widgetSettingsPremium, ModelSubscription modelSubscription) {
        super(0);
        this.this$0 = widgetSettingsPremium;
        this.$premiumSubscription = modelSubscription;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        PremiumUtils.INSTANCE.openGooglePlayBilling(this.this$0.requireContext(), this.$premiumSubscription.getPaymentGatewayPlanId());
    }
}
