package com.discord.widgets.settings.premium;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.api.premium.ClaimedOutboundPromotion;
import com.discord.api.premium.OutboundPromotion;
import com.discord.app.AppComponent;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelEntitlement;
import com.discord.models.domain.ModelGift;
import com.discord.models.domain.ModelUserSettings;
import com.discord.stores.StoreEntitlements;
import com.discord.stores.StoreGifting;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.stores.StoreOutboundPromotions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserSettingsSystem;
import com.discord.utilities.billing.GooglePlayBillingManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.settings.premium.ClaimStatus;
import d0.t.h0;
import d0.t.n;
import d0.t.n0;
import d0.t.o;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.functions.Func4;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: SettingsGiftingViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0090\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 {2\b\u0012\u0004\u0012\u00020\u00020\u0001:\r{|}~\u007f\u0080\u0001\u0081\u0001\u0082\u0001\u0083\u0001BS\u0012\b\b\u0002\u0010n\u001a\u00020m\u0012\b\b\u0002\u0010f\u001a\u00020e\u0012\b\b\u0002\u0010r\u001a\u00020q\u0012\b\b\u0002\u0010]\u001a\u00020\\\u0012\b\b\u0002\u0010u\u001a\u00020t\u0012\b\b\u0002\u0010w\u001a\u00020v\u0012\u000e\b\u0002\u0010x\u001a\b\u0012\u0004\u0012\u00020\"0>¢\u0006\u0004\by\u0010zJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJE\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\u001c\u0010\u0019\u001a\u0018\u0012\b\u0012\u00060\u0015j\u0002`\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u00142\u0006\u0010\u001b\u001a\u00020\u001aH\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\u00052\u0006\u0010\u001f\u001a\u00020\u001eH\u0002¢\u0006\u0004\b \u0010!J%\u0010'\u001a\u00020&2\u0006\u0010#\u001a\u00020\"2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020$0\u0017H\u0002¢\u0006\u0004\b'\u0010(J\u0017\u0010*\u001a\u00020\u00052\u0006\u0010)\u001a\u00020&H\u0003¢\u0006\u0004\b*\u0010+J1\u0010/\u001a\b\u0012\u0004\u0012\u00020.0\u00172\f\u0010-\u001a\b\u0012\u0004\u0012\u00020,0\u00172\f\u0010%\u001a\b\u0012\u0004\u0012\u00020$0\u0017H\u0002¢\u0006\u0004\b/\u00100J\u001b\u00104\u001a\u0002032\n\u00102\u001a\u00060\u0015j\u0002`1H\u0002¢\u0006\u0004\b4\u00105J!\u00108\u001a\u00020\u00052\u0012\u00107\u001a\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020\u000506¢\u0006\u0004\b8\u00109J\u0013\u0010<\u001a\b\u0012\u0004\u0012\u00020;0:¢\u0006\u0004\b<\u0010=J\u0013\u0010@\u001a\b\u0012\u0004\u0012\u00020?0>¢\u0006\u0004\b@\u0010AJ\u001f\u0010E\u001a\u00020\u00052\u0006\u0010B\u001a\u0002032\u0006\u0010D\u001a\u00020CH\u0007¢\u0006\u0004\bE\u0010FJ+\u0010K\u001a\u00020\u00052\n\u0010H\u001a\u00060\u0015j\u0002`G2\u000e\u0010J\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`IH\u0007¢\u0006\u0004\bK\u0010LJ\u0017\u0010M\u001a\u00020\u00052\u0006\u0010B\u001a\u000203H\u0007¢\u0006\u0004\bM\u0010NJ\u0015\u0010P\u001a\u00020\u00052\u0006\u0010O\u001a\u00020.¢\u0006\u0004\bP\u0010QJ\u0015\u0010R\u001a\u00020\u00052\u0006\u0010O\u001a\u00020.¢\u0006\u0004\bR\u0010QJ\u0017\u0010U\u001a\u00020\u00052\u0006\u0010T\u001a\u00020SH\u0007¢\u0006\u0004\bU\u0010VJ\u000f\u0010W\u001a\u00020\u0005H\u0014¢\u0006\u0004\bW\u0010XR:\u0010Z\u001a&\u0012\f\u0012\n Y*\u0004\u0018\u00010?0? Y*\u0012\u0012\f\u0012\n Y*\u0004\u0018\u00010?0?\u0018\u00010:0:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R\u0016\u0010]\u001a\u00020\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR:\u0010c\u001a&\u0012\f\u0012\n Y*\u0004\u0018\u00010\"0\" Y*\u0012\u0012\f\u0012\n Y*\u0004\u0018\u00010\"0\"\u0018\u00010b0b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR\u0016\u0010f\u001a\u00020e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010gR:\u0010h\u001a&\u0012\f\u0012\n Y*\u0004\u0018\u00010;0; Y*\u0012\u0012\f\u0012\n Y*\u0004\u0018\u00010;0;\u0018\u00010:0:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010[RR\u0010i\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020$ Y*\n\u0012\u0004\u0012\u00020$\u0018\u00010\u00170\u0017 Y*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020$ Y*\n\u0012\u0004\u0012\u00020$\u0018\u00010\u00170\u0017\u0018\u00010b0b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010dR\u0016\u0010k\u001a\u00020j8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bk\u0010lR\u0016\u0010n\u001a\u00020m8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bn\u0010oR\"\u00107\u001a\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020\u0005068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u0010pR\u0016\u0010r\u001a\u00020q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\br\u0010s¨\u0006\u0084\u0001"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;", "Lcom/discord/stores/StoreGifting$GiftState;", "giftState", "", "onHandleGiftCode", "(Lcom/discord/stores/StoreGifting$GiftState;)V", "Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "event", "handleGooglePlayPurchaseEvent", "(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "queryState", "handleGooglePlayQueryStateUpdate", "(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;)V", "Lcom/discord/stores/StoreEntitlements$State;", "entitlementState", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "resolvingGiftState", "", "", "Lcom/discord/primitives/Snowflake;", "", "Lcom/discord/models/domain/ModelGift;", "myPurchasedGifts", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData;", "outboundPromoData", "buildViewState", "(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;", "", "isUserPremium", "maybeCheckClaimedPromos", "(Z)V", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;", "storeState", "Lcom/discord/api/premium/ClaimedOutboundPromotion;", "claimedPromos", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", "combineData", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;Ljava/util/List;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", "data", "handleAsyncData", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;)V", "Lcom/discord/api/premium/OutboundPromotion;", "validActivePromos", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "getPromos", "(Ljava/util/List;Ljava/util/List;)Ljava/util/List;", "Lcom/discord/primitives/PromoId;", "promoId", "", "getPromoImageUrl", "(J)Ljava/lang/String;", "Lkotlin/Function1;", "onGiftCodeResolved", "setOnGiftCodeResolved", "(Lkotlin/jvm/functions/Function1;)V", "Lrx/subjects/PublishSubject;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;", "observeGiftPurchaseEvents", "()Lrx/subjects/PublishSubject;", "Lrx/Observable;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event;", "observeEvents", "()Lrx/Observable;", "giftCode", "Lcom/discord/app/AppComponent;", "appComponent", "redeemGiftCode", "(Ljava/lang/String;Lcom/discord/app/AppComponent;)V", "Lcom/discord/primitives/SkuId;", "skuId", "Lcom/discord/primitives/PlanId;", "planId", "handleSkuClicked", "(JLjava/lang/Long;)V", "handleCopyClicked", "(Ljava/lang/String;)V", "promoItem", "handlePromoMoreDetailsClicked", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;)V", "handlePromoButtonClicked", "Lcom/discord/widgets/settings/premium/ClaimStatus$Claimed;", "claimedStatus", "handleClaimedPromo", "(Lcom/discord/widgets/settings/premium/ClaimStatus$Claimed;)V", "onCleared", "()V", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreOutboundPromotions;", "storeOutboundPromotions", "Lcom/discord/stores/StoreOutboundPromotions;", "Lrx/subscriptions/CompositeSubscription;", "subscriptions", "Lrx/subscriptions/CompositeSubscription;", "Lrx/subjects/BehaviorSubject;", "storeStateSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreGifting;", "storeGifting", "Lcom/discord/stores/StoreGifting;", "giftPurchaseEventSubject", "claimedPromotionsSubject", "Ljava/util/concurrent/atomic/AtomicBoolean;", "shouldCheckClaimedPromos", "Ljava/util/concurrent/atomic/AtomicBoolean;", "Lcom/discord/stores/StoreEntitlements;", "storeEntitlements", "Lcom/discord/stores/StoreEntitlements;", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/stores/StoreUserSettingsSystem;", "storeUserSettingsSystem", "Lcom/discord/stores/StoreUserSettingsSystem;", "Lcom/discord/stores/StoreGooglePlayPurchases;", "storeGooglePlayPurchases", "Lcom/discord/utilities/billing/GooglePlayBillingManager;", "gPlayBillingManager", "storeObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StoreGifting;Lcom/discord/stores/StoreUserSettingsSystem;Lcom/discord/stores/StoreOutboundPromotions;Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/utilities/billing/GooglePlayBillingManager;Lrx/Observable;)V", "Companion", "Event", "GiftAndPromoData", "GiftPurchaseEvent", "OutboundPromoData", "OutboundPromoItem", "ResolvingGiftState", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsGiftingViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final BehaviorSubject<List<ClaimedOutboundPromotion>> claimedPromotionsSubject;
    private final PublishSubject<Event> eventSubject;
    private final PublishSubject<GiftPurchaseEvent> giftPurchaseEventSubject;
    private Function1<? super String, Unit> onGiftCodeResolved;
    private AtomicBoolean shouldCheckClaimedPromos;
    private final StoreEntitlements storeEntitlements;
    private final StoreGifting storeGifting;
    private final StoreOutboundPromotions storeOutboundPromotions;
    private final BehaviorSubject<StoreState> storeStateSubject;
    private final StoreUserSettingsSystem storeUserSettingsSystem;
    private final CompositeSubscription subscriptions;

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "p1", "", "invoke", "(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.SettingsGiftingViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreGooglePlayPurchases.Event, Unit> {
        public AnonymousClass1(SettingsGiftingViewModel settingsGiftingViewModel) {
            super(1, settingsGiftingViewModel, SettingsGiftingViewModel.class, "handleGooglePlayPurchaseEvent", "handleGooglePlayPurchaseEvent(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreGooglePlayPurchases.Event event) {
            invoke2(event);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreGooglePlayPurchases.Event event) {
            m.checkNotNullParameter(event, "p1");
            ((SettingsGiftingViewModel) this.receiver).handleGooglePlayPurchaseEvent(event);
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "p1", "", "invoke", "(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.SettingsGiftingViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<StoreGooglePlayPurchases.QueryState, Unit> {
        public AnonymousClass2(SettingsGiftingViewModel settingsGiftingViewModel) {
            super(1, settingsGiftingViewModel, SettingsGiftingViewModel.class, "handleGooglePlayQueryStateUpdate", "handleGooglePlayQueryStateUpdate(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreGooglePlayPurchases.QueryState queryState) {
            invoke2(queryState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreGooglePlayPurchases.QueryState queryState) {
            m.checkNotNullParameter(queryState, "p1");
            ((SettingsGiftingViewModel) this.receiver).handleGooglePlayQueryStateUpdate(queryState);
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;", "kotlin.jvm.PlatformType", "p1", "", "invoke", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.SettingsGiftingViewModel$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass4 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass4(BehaviorSubject behaviorSubject) {
            super(1, behaviorSubject, BehaviorSubject.class, "onNext", "onNext(Ljava/lang/Object;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            ((BehaviorSubject) this.receiver).onNext(storeState);
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u0006\u0010\u0001\u001a\u00020\u00002\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;", "p1", "", "Lcom/discord/api/premium/ClaimedOutboundPromotion;", "p2", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", "invoke", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;Ljava/util/List;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.SettingsGiftingViewModel$5  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass5 extends k implements Function2<StoreState, List<? extends ClaimedOutboundPromotion>, GiftAndPromoData> {
        public AnonymousClass5(SettingsGiftingViewModel settingsGiftingViewModel) {
            super(2, settingsGiftingViewModel, SettingsGiftingViewModel.class, "combineData", "combineData(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;Ljava/util/List;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", 0);
        }

        @Override // kotlin.jvm.functions.Function2
        public /* bridge */ /* synthetic */ GiftAndPromoData invoke(StoreState storeState, List<? extends ClaimedOutboundPromotion> list) {
            return invoke2(storeState, (List<ClaimedOutboundPromotion>) list);
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final GiftAndPromoData invoke2(StoreState storeState, List<ClaimedOutboundPromotion> list) {
            m.checkNotNullParameter(storeState, "p1");
            m.checkNotNullParameter(list, "p2");
            return ((SettingsGiftingViewModel) this.receiver).combineData(storeState, list);
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", "p1", "", "invoke", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.SettingsGiftingViewModel$6  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass6 extends k implements Function1<GiftAndPromoData, Unit> {
        public AnonymousClass6(SettingsGiftingViewModel settingsGiftingViewModel) {
            super(1, settingsGiftingViewModel, SettingsGiftingViewModel.class, "handleAsyncData", "handleAsyncData(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(GiftAndPromoData giftAndPromoData) {
            invoke2(giftAndPromoData);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(GiftAndPromoData giftAndPromoData) {
            m.checkNotNullParameter(giftAndPromoData, "p1");
            ((SettingsGiftingViewModel) this.receiver).handleAsyncData(giftAndPromoData);
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;", "observeStores", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores() {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<StoreEntitlements.State> observeEntitlementState = companion.getEntitlements().observeEntitlementState();
            Observable<R> Y = companion.getUsers().observeMeId().Y(SettingsGiftingViewModel$Companion$observeStores$1.INSTANCE);
            Observable<StoreOutboundPromotions.State> observeState = companion.getOutboundPromotions().observeState();
            Observable F = StoreUser.observeMe$default(companion.getUsers(), false, 1, null).F(SettingsGiftingViewModel$Companion$observeStores$2.INSTANCE);
            final SettingsGiftingViewModel$Companion$observeStores$3 settingsGiftingViewModel$Companion$observeStores$3 = SettingsGiftingViewModel$Companion$observeStores$3.INSTANCE;
            Object obj = settingsGiftingViewModel$Companion$observeStores$3;
            if (settingsGiftingViewModel$Companion$observeStores$3 != null) {
                obj = new Func4() { // from class: com.discord.widgets.settings.premium.SettingsGiftingViewModel$sam$rx_functions_Func4$0
                    @Override // rx.functions.Func4
                    public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                        return Function4.this.invoke(obj2, obj3, obj4, obj5);
                    }
                };
            }
            Observable<StoreState> q = Observable.h(observeEntitlementState, Y, observeState, F, (Func4) obj).q();
            m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
            return q;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event;", "", HookHelper.constructorName, "()V", "ShowPromoBottomSheet", "ShowPromoDialog", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event$ShowPromoBottomSheet;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event$ShowPromoDialog;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event$ShowPromoBottomSheet;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event;", "", "component1", "()Ljava/lang/String;", "content", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event$ShowPromoBottomSheet;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getContent", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowPromoBottomSheet extends Event {
            private final String content;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ShowPromoBottomSheet(String str) {
                super(null);
                m.checkNotNullParameter(str, "content");
                this.content = str;
            }

            public static /* synthetic */ ShowPromoBottomSheet copy$default(ShowPromoBottomSheet showPromoBottomSheet, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = showPromoBottomSheet.content;
                }
                return showPromoBottomSheet.copy(str);
            }

            public final String component1() {
                return this.content;
            }

            public final ShowPromoBottomSheet copy(String str) {
                m.checkNotNullParameter(str, "content");
                return new ShowPromoBottomSheet(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowPromoBottomSheet) && m.areEqual(this.content, ((ShowPromoBottomSheet) obj).content);
                }
                return true;
            }

            public final String getContent() {
                return this.content;
            }

            public int hashCode() {
                String str = this.content;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("ShowPromoBottomSheet(content="), this.content, ")");
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event$ShowPromoDialog;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event;", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "component1", "()Lcom/discord/widgets/settings/premium/ClaimStatus;", "claimStatus", "copy", "(Lcom/discord/widgets/settings/premium/ClaimStatus;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event$ShowPromoDialog;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "getClaimStatus", HookHelper.constructorName, "(Lcom/discord/widgets/settings/premium/ClaimStatus;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowPromoDialog extends Event {
            private final ClaimStatus claimStatus;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ShowPromoDialog(ClaimStatus claimStatus) {
                super(null);
                m.checkNotNullParameter(claimStatus, "claimStatus");
                this.claimStatus = claimStatus;
            }

            public static /* synthetic */ ShowPromoDialog copy$default(ShowPromoDialog showPromoDialog, ClaimStatus claimStatus, int i, Object obj) {
                if ((i & 1) != 0) {
                    claimStatus = showPromoDialog.claimStatus;
                }
                return showPromoDialog.copy(claimStatus);
            }

            public final ClaimStatus component1() {
                return this.claimStatus;
            }

            public final ShowPromoDialog copy(ClaimStatus claimStatus) {
                m.checkNotNullParameter(claimStatus, "claimStatus");
                return new ShowPromoDialog(claimStatus);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowPromoDialog) && m.areEqual(this.claimStatus, ((ShowPromoDialog) obj).claimStatus);
                }
                return true;
            }

            public final ClaimStatus getClaimStatus() {
                return this.claimStatus;
            }

            public int hashCode() {
                ClaimStatus claimStatus = this.claimStatus;
                if (claimStatus != null) {
                    return claimStatus.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("ShowPromoDialog(claimStatus=");
                R.append(this.claimStatus);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0082\b\u0018\u00002\u00020\u0001BK\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u001c\u0010\u0017\u001a\u0018\u0012\b\u0012\u00060\tj\u0002`\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\b\u0012\u0006\u0010\u0018\u001a\u00020\u000f\u0012\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00120\u000b¢\u0006\u0004\b0\u00101J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\r\u001a\u0018\u0012\b\u0012\u00060\tj\u0002`\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u000bHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J^\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00052\u001e\b\u0002\u0010\u0017\u001a\u0018\u0012\b\u0012\u00060\tj\u0002`\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\b2\b\b\u0002\u0010\u0018\u001a\u00020\u000f2\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00120\u000bHÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010$\u001a\u00020#2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R/\u0010\u0017\u001a\u0018\u0012\b\u0012\u00060\tj\u0002`\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\u000eR\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010(\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010*\u001a\u0004\b+\u0010\u0007R\u0019\u0010\u0018\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010,\u001a\u0004\b-\u0010\u0011R\u001f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00120\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010.\u001a\u0004\b/\u0010\u0014¨\u00062"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", "", "Lcom/discord/stores/StoreEntitlements$State;", "component1", "()Lcom/discord/stores/StoreEntitlements$State;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "component2", "()Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "", "", "Lcom/discord/primitives/Snowflake;", "", "Lcom/discord/models/domain/ModelGift;", "component3", "()Ljava/util/Map;", "Lcom/discord/stores/StoreOutboundPromotions$State;", "component4", "()Lcom/discord/stores/StoreOutboundPromotions$State;", "Lcom/discord/api/premium/ClaimedOutboundPromotion;", "component5", "()Ljava/util/List;", "entitlementState", "resolvingGiftState", "myPurchasedGifts", "outboundPromoState", "claimedOutboundPromotions", "copy", "(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;Lcom/discord/stores/StoreOutboundPromotions$State;Ljava/util/List;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftAndPromoData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getMyPurchasedGifts", "Lcom/discord/stores/StoreEntitlements$State;", "getEntitlementState", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "getResolvingGiftState", "Lcom/discord/stores/StoreOutboundPromotions$State;", "getOutboundPromoState", "Ljava/util/List;", "getClaimedOutboundPromotions", HookHelper.constructorName, "(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;Lcom/discord/stores/StoreOutboundPromotions$State;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GiftAndPromoData {
        private final List<ClaimedOutboundPromotion> claimedOutboundPromotions;
        private final StoreEntitlements.State entitlementState;
        private final Map<Long, List<ModelGift>> myPurchasedGifts;
        private final StoreOutboundPromotions.State outboundPromoState;
        private final ResolvingGiftState resolvingGiftState;

        /* JADX WARN: Multi-variable type inference failed */
        public GiftAndPromoData(StoreEntitlements.State state, ResolvingGiftState resolvingGiftState, Map<Long, ? extends List<ModelGift>> map, StoreOutboundPromotions.State state2, List<ClaimedOutboundPromotion> list) {
            m.checkNotNullParameter(state, "entitlementState");
            m.checkNotNullParameter(resolvingGiftState, "resolvingGiftState");
            m.checkNotNullParameter(map, "myPurchasedGifts");
            m.checkNotNullParameter(state2, "outboundPromoState");
            m.checkNotNullParameter(list, "claimedOutboundPromotions");
            this.entitlementState = state;
            this.resolvingGiftState = resolvingGiftState;
            this.myPurchasedGifts = map;
            this.outboundPromoState = state2;
            this.claimedOutboundPromotions = list;
        }

        public static /* synthetic */ GiftAndPromoData copy$default(GiftAndPromoData giftAndPromoData, StoreEntitlements.State state, ResolvingGiftState resolvingGiftState, Map map, StoreOutboundPromotions.State state2, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                state = giftAndPromoData.entitlementState;
            }
            if ((i & 2) != 0) {
                resolvingGiftState = giftAndPromoData.resolvingGiftState;
            }
            ResolvingGiftState resolvingGiftState2 = resolvingGiftState;
            Map<Long, List<ModelGift>> map2 = map;
            if ((i & 4) != 0) {
                map2 = giftAndPromoData.myPurchasedGifts;
            }
            Map map3 = map2;
            if ((i & 8) != 0) {
                state2 = giftAndPromoData.outboundPromoState;
            }
            StoreOutboundPromotions.State state3 = state2;
            List<ClaimedOutboundPromotion> list2 = list;
            if ((i & 16) != 0) {
                list2 = giftAndPromoData.claimedOutboundPromotions;
            }
            return giftAndPromoData.copy(state, resolvingGiftState2, map3, state3, list2);
        }

        public final StoreEntitlements.State component1() {
            return this.entitlementState;
        }

        public final ResolvingGiftState component2() {
            return this.resolvingGiftState;
        }

        public final Map<Long, List<ModelGift>> component3() {
            return this.myPurchasedGifts;
        }

        public final StoreOutboundPromotions.State component4() {
            return this.outboundPromoState;
        }

        public final List<ClaimedOutboundPromotion> component5() {
            return this.claimedOutboundPromotions;
        }

        public final GiftAndPromoData copy(StoreEntitlements.State state, ResolvingGiftState resolvingGiftState, Map<Long, ? extends List<ModelGift>> map, StoreOutboundPromotions.State state2, List<ClaimedOutboundPromotion> list) {
            m.checkNotNullParameter(state, "entitlementState");
            m.checkNotNullParameter(resolvingGiftState, "resolvingGiftState");
            m.checkNotNullParameter(map, "myPurchasedGifts");
            m.checkNotNullParameter(state2, "outboundPromoState");
            m.checkNotNullParameter(list, "claimedOutboundPromotions");
            return new GiftAndPromoData(state, resolvingGiftState, map, state2, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GiftAndPromoData)) {
                return false;
            }
            GiftAndPromoData giftAndPromoData = (GiftAndPromoData) obj;
            return m.areEqual(this.entitlementState, giftAndPromoData.entitlementState) && m.areEqual(this.resolvingGiftState, giftAndPromoData.resolvingGiftState) && m.areEqual(this.myPurchasedGifts, giftAndPromoData.myPurchasedGifts) && m.areEqual(this.outboundPromoState, giftAndPromoData.outboundPromoState) && m.areEqual(this.claimedOutboundPromotions, giftAndPromoData.claimedOutboundPromotions);
        }

        public final List<ClaimedOutboundPromotion> getClaimedOutboundPromotions() {
            return this.claimedOutboundPromotions;
        }

        public final StoreEntitlements.State getEntitlementState() {
            return this.entitlementState;
        }

        public final Map<Long, List<ModelGift>> getMyPurchasedGifts() {
            return this.myPurchasedGifts;
        }

        public final StoreOutboundPromotions.State getOutboundPromoState() {
            return this.outboundPromoState;
        }

        public final ResolvingGiftState getResolvingGiftState() {
            return this.resolvingGiftState;
        }

        public int hashCode() {
            StoreEntitlements.State state = this.entitlementState;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            ResolvingGiftState resolvingGiftState = this.resolvingGiftState;
            int hashCode2 = (hashCode + (resolvingGiftState != null ? resolvingGiftState.hashCode() : 0)) * 31;
            Map<Long, List<ModelGift>> map = this.myPurchasedGifts;
            int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
            StoreOutboundPromotions.State state2 = this.outboundPromoState;
            int hashCode4 = (hashCode3 + (state2 != null ? state2.hashCode() : 0)) * 31;
            List<ClaimedOutboundPromotion> list = this.claimedOutboundPromotions;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("GiftAndPromoData(entitlementState=");
            R.append(this.entitlementState);
            R.append(", resolvingGiftState=");
            R.append(this.resolvingGiftState);
            R.append(", myPurchasedGifts=");
            R.append(this.myPurchasedGifts);
            R.append(", outboundPromoState=");
            R.append(this.outboundPromoState);
            R.append(", claimedOutboundPromotions=");
            return a.K(R, this.claimedOutboundPromotions, ")");
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;", "", HookHelper.constructorName, "()V", "CompleteGiftPurchase", "ErrorGiftPurchase", "NotInProgress", "StartGiftPurchase", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$StartGiftPurchase;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$NotInProgress;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$ErrorGiftPurchase;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$CompleteGiftPurchase;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class GiftPurchaseEvent {

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$CompleteGiftPurchase;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;", "", "component1", "()Ljava/lang/String;", "component2", "skuName", "newGiftCode", "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$CompleteGiftPurchase;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getNewGiftCode", "getSkuName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CompleteGiftPurchase extends GiftPurchaseEvent {
            private final String newGiftCode;
            private final String skuName;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public CompleteGiftPurchase(String str, String str2) {
                super(null);
                m.checkNotNullParameter(str, "skuName");
                m.checkNotNullParameter(str2, "newGiftCode");
                this.skuName = str;
                this.newGiftCode = str2;
            }

            public static /* synthetic */ CompleteGiftPurchase copy$default(CompleteGiftPurchase completeGiftPurchase, String str, String str2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = completeGiftPurchase.skuName;
                }
                if ((i & 2) != 0) {
                    str2 = completeGiftPurchase.newGiftCode;
                }
                return completeGiftPurchase.copy(str, str2);
            }

            public final String component1() {
                return this.skuName;
            }

            public final String component2() {
                return this.newGiftCode;
            }

            public final CompleteGiftPurchase copy(String str, String str2) {
                m.checkNotNullParameter(str, "skuName");
                m.checkNotNullParameter(str2, "newGiftCode");
                return new CompleteGiftPurchase(str, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof CompleteGiftPurchase)) {
                    return false;
                }
                CompleteGiftPurchase completeGiftPurchase = (CompleteGiftPurchase) obj;
                return m.areEqual(this.skuName, completeGiftPurchase.skuName) && m.areEqual(this.newGiftCode, completeGiftPurchase.newGiftCode);
            }

            public final String getNewGiftCode() {
                return this.newGiftCode;
            }

            public final String getSkuName() {
                return this.skuName;
            }

            public int hashCode() {
                String str = this.skuName;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String str2 = this.newGiftCode;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("CompleteGiftPurchase(skuName=");
                R.append(this.skuName);
                R.append(", newGiftCode=");
                return a.H(R, this.newGiftCode, ")");
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$ErrorGiftPurchase;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;", "", "component1", "()I", "message", "copy", "(I)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$ErrorGiftPurchase;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMessage", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ErrorGiftPurchase extends GiftPurchaseEvent {
            private final int message;

            public ErrorGiftPurchase(@StringRes int i) {
                super(null);
                this.message = i;
            }

            public static /* synthetic */ ErrorGiftPurchase copy$default(ErrorGiftPurchase errorGiftPurchase, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = errorGiftPurchase.message;
                }
                return errorGiftPurchase.copy(i);
            }

            public final int component1() {
                return this.message;
            }

            public final ErrorGiftPurchase copy(@StringRes int i) {
                return new ErrorGiftPurchase(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ErrorGiftPurchase) && this.message == ((ErrorGiftPurchase) obj).message;
                }
                return true;
            }

            public final int getMessage() {
                return this.message;
            }

            public int hashCode() {
                return this.message;
            }

            public String toString() {
                return a.A(a.R("ErrorGiftPurchase(message="), this.message, ")");
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$NotInProgress;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NotInProgress extends GiftPurchaseEvent {
            public static final NotInProgress INSTANCE = new NotInProgress();

            private NotInProgress() {
                super(null);
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent$StartGiftPurchase;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class StartGiftPurchase extends GiftPurchaseEvent {
            public static final StartGiftPurchase INSTANCE = new StartGiftPurchase();

            private StartGiftPurchase() {
                super(null);
            }
        }

        private GiftPurchaseEvent() {
        }

        public /* synthetic */ GiftPurchaseEvent(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData;", "", HookHelper.constructorName, "()V", "Loaded", "Loading", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData$Loading;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class OutboundPromoData {

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData$Loaded;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData;", "", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "component1", "()Ljava/util/List;", "outboundPromos", "copy", "(Ljava/util/List;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getOutboundPromos", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends OutboundPromoData {
            private final List<OutboundPromoItem> outboundPromos;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(List<OutboundPromoItem> list) {
                super(null);
                m.checkNotNullParameter(list, "outboundPromos");
                this.outboundPromos = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.outboundPromos;
                }
                return loaded.copy(list);
            }

            public final List<OutboundPromoItem> component1() {
                return this.outboundPromos;
            }

            public final Loaded copy(List<OutboundPromoItem> list) {
                m.checkNotNullParameter(list, "outboundPromos");
                return new Loaded(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.outboundPromos, ((Loaded) obj).outboundPromos);
                }
                return true;
            }

            public final List<OutboundPromoItem> getOutboundPromos() {
                return this.outboundPromos;
            }

            public int hashCode() {
                List<OutboundPromoItem> list = this.outboundPromos;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("Loaded(outboundPromos="), this.outboundPromos, ")");
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData$Loading;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoData;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends OutboundPromoData {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private OutboundPromoData() {
        }

        public /* synthetic */ OutboundPromoData(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\n\u0010\u000f\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010\u0010\u001a\u00020\t\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u0006\u0010\u0012\u001a\u00020\t¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\r\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJF\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\f\b\u0002\u0010\u000f\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u0010\u001a\u00020\t2\b\b\u0002\u0010\u0011\u001a\u00020\t2\b\b\u0002\u0010\u0012\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0015\u0010\u000bJ\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b\u001f\u0010\u000bR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b!\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001d\u001a\u0004\b\"\u0010\u000bR\u001d\u0010\u000f\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010#\u001a\u0004\b$\u0010\b¨\u0006'"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "component1", "()Lcom/discord/widgets/settings/premium/ClaimStatus;", "", "Lcom/discord/primitives/PromoId;", "component2", "()J", "", "component3", "()Ljava/lang/String;", "component4", "component5", "claimStatus", ModelAuditLogEntry.CHANGE_KEY_ID, "title", "terms", "imageUrl", "copy", "(Lcom/discord/widgets/settings/premium/ClaimStatus;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getTitle", "getTerms", "Lcom/discord/widgets/settings/premium/ClaimStatus;", "getClaimStatus", "getImageUrl", "J", "getId", HookHelper.constructorName, "(Lcom/discord/widgets/settings/premium/ClaimStatus;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class OutboundPromoItem {
        private final ClaimStatus claimStatus;

        /* renamed from: id  reason: collision with root package name */
        private final long f2842id;
        private final String imageUrl;
        private final String terms;
        private final String title;

        public OutboundPromoItem(ClaimStatus claimStatus, long j, String str, String str2, String str3) {
            m.checkNotNullParameter(claimStatus, "claimStatus");
            m.checkNotNullParameter(str, "title");
            m.checkNotNullParameter(str2, "terms");
            m.checkNotNullParameter(str3, "imageUrl");
            this.claimStatus = claimStatus;
            this.f2842id = j;
            this.title = str;
            this.terms = str2;
            this.imageUrl = str3;
        }

        public static /* synthetic */ OutboundPromoItem copy$default(OutboundPromoItem outboundPromoItem, ClaimStatus claimStatus, long j, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                claimStatus = outboundPromoItem.claimStatus;
            }
            if ((i & 2) != 0) {
                j = outboundPromoItem.f2842id;
            }
            long j2 = j;
            if ((i & 4) != 0) {
                str = outboundPromoItem.title;
            }
            String str4 = str;
            if ((i & 8) != 0) {
                str2 = outboundPromoItem.terms;
            }
            String str5 = str2;
            if ((i & 16) != 0) {
                str3 = outboundPromoItem.imageUrl;
            }
            return outboundPromoItem.copy(claimStatus, j2, str4, str5, str3);
        }

        public final ClaimStatus component1() {
            return this.claimStatus;
        }

        public final long component2() {
            return this.f2842id;
        }

        public final String component3() {
            return this.title;
        }

        public final String component4() {
            return this.terms;
        }

        public final String component5() {
            return this.imageUrl;
        }

        public final OutboundPromoItem copy(ClaimStatus claimStatus, long j, String str, String str2, String str3) {
            m.checkNotNullParameter(claimStatus, "claimStatus");
            m.checkNotNullParameter(str, "title");
            m.checkNotNullParameter(str2, "terms");
            m.checkNotNullParameter(str3, "imageUrl");
            return new OutboundPromoItem(claimStatus, j, str, str2, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OutboundPromoItem)) {
                return false;
            }
            OutboundPromoItem outboundPromoItem = (OutboundPromoItem) obj;
            return m.areEqual(this.claimStatus, outboundPromoItem.claimStatus) && this.f2842id == outboundPromoItem.f2842id && m.areEqual(this.title, outboundPromoItem.title) && m.areEqual(this.terms, outboundPromoItem.terms) && m.areEqual(this.imageUrl, outboundPromoItem.imageUrl);
        }

        public final ClaimStatus getClaimStatus() {
            return this.claimStatus;
        }

        public final long getId() {
            return this.f2842id;
        }

        public final String getImageUrl() {
            return this.imageUrl;
        }

        public final String getTerms() {
            return this.terms;
        }

        public final String getTitle() {
            return this.title;
        }

        public int hashCode() {
            ClaimStatus claimStatus = this.claimStatus;
            int i = 0;
            int a = (b.a(this.f2842id) + ((claimStatus != null ? claimStatus.hashCode() : 0) * 31)) * 31;
            String str = this.title;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.terms;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.imageUrl;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("OutboundPromoItem(claimStatus=");
            R.append(this.claimStatus);
            R.append(", id=");
            R.append(this.f2842id);
            R.append(", title=");
            R.append(this.title);
            R.append(", terms=");
            R.append(this.terms);
            R.append(", imageUrl=");
            return a.H(R, this.imageUrl, ")");
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "", HookHelper.constructorName, "()V", "Error", "NotResolving", "Resolving", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState$NotResolving;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState$Error;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState$Resolving;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ResolvingGiftState {

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState$Error;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends ResolvingGiftState {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState$NotResolving;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NotResolving extends ResolvingGiftState {
            public static final NotResolving INSTANCE = new NotResolving();

            private NotResolving() {
                super(null);
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState$Resolving;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Resolving extends ResolvingGiftState {
            public static final Resolving INSTANCE = new Resolving();

            private Resolving() {
                super(null);
            }
        }

        private ResolvingGiftState() {
        }

        public /* synthetic */ ResolvingGiftState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u0006\u0010\u0012\u001a\u00020\f¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ>\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0011\u001a\u00020\t2\b\b\u0002\u0010\u0012\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\u001f\u0010\u000bR\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\bR\u0019\u0010\u0012\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b\u0012\u0010\u000eR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010#\u001a\u0004\b$\u0010\u0004¨\u0006'"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;", "", "Lcom/discord/stores/StoreEntitlements$State;", "component1", "()Lcom/discord/stores/StoreEntitlements$State;", "", "Lcom/discord/models/domain/ModelGift;", "component2", "()Ljava/util/List;", "Lcom/discord/stores/StoreOutboundPromotions$State;", "component3", "()Lcom/discord/stores/StoreOutboundPromotions$State;", "", "component4", "()Z", "entitlementState", "myResolvedGifts", "outboundPromoState", "isUserPremium", "copy", "(Lcom/discord/stores/StoreEntitlements$State;Ljava/util/List;Lcom/discord/stores/StoreOutboundPromotions$State;Z)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreOutboundPromotions$State;", "getOutboundPromoState", "Ljava/util/List;", "getMyResolvedGifts", "Z", "Lcom/discord/stores/StoreEntitlements$State;", "getEntitlementState", HookHelper.constructorName, "(Lcom/discord/stores/StoreEntitlements$State;Ljava/util/List;Lcom/discord/stores/StoreOutboundPromotions$State;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StoreEntitlements.State entitlementState;
        private final boolean isUserPremium;
        private final List<ModelGift> myResolvedGifts;
        private final StoreOutboundPromotions.State outboundPromoState;

        public StoreState(StoreEntitlements.State state, List<ModelGift> list, StoreOutboundPromotions.State state2, boolean z2) {
            m.checkNotNullParameter(state, "entitlementState");
            m.checkNotNullParameter(list, "myResolvedGifts");
            m.checkNotNullParameter(state2, "outboundPromoState");
            this.entitlementState = state;
            this.myResolvedGifts = list;
            this.outboundPromoState = state2;
            this.isUserPremium = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, StoreEntitlements.State state, List list, StoreOutboundPromotions.State state2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                state = storeState.entitlementState;
            }
            if ((i & 2) != 0) {
                list = storeState.myResolvedGifts;
            }
            if ((i & 4) != 0) {
                state2 = storeState.outboundPromoState;
            }
            if ((i & 8) != 0) {
                z2 = storeState.isUserPremium;
            }
            return storeState.copy(state, list, state2, z2);
        }

        public final StoreEntitlements.State component1() {
            return this.entitlementState;
        }

        public final List<ModelGift> component2() {
            return this.myResolvedGifts;
        }

        public final StoreOutboundPromotions.State component3() {
            return this.outboundPromoState;
        }

        public final boolean component4() {
            return this.isUserPremium;
        }

        public final StoreState copy(StoreEntitlements.State state, List<ModelGift> list, StoreOutboundPromotions.State state2, boolean z2) {
            m.checkNotNullParameter(state, "entitlementState");
            m.checkNotNullParameter(list, "myResolvedGifts");
            m.checkNotNullParameter(state2, "outboundPromoState");
            return new StoreState(state, list, state2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.entitlementState, storeState.entitlementState) && m.areEqual(this.myResolvedGifts, storeState.myResolvedGifts) && m.areEqual(this.outboundPromoState, storeState.outboundPromoState) && this.isUserPremium == storeState.isUserPremium;
        }

        public final StoreEntitlements.State getEntitlementState() {
            return this.entitlementState;
        }

        public final List<ModelGift> getMyResolvedGifts() {
            return this.myResolvedGifts;
        }

        public final StoreOutboundPromotions.State getOutboundPromoState() {
            return this.outboundPromoState;
        }

        public int hashCode() {
            StoreEntitlements.State state = this.entitlementState;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            List<ModelGift> list = this.myResolvedGifts;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            StoreOutboundPromotions.State state2 = this.outboundPromoState;
            if (state2 != null) {
                i = state2.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isUserPremium;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isUserPremium() {
            return this.isUserPremium;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(entitlementState=");
            R.append(this.entitlementState);
            R.append(", myResolvedGifts=");
            R.append(this.myResolvedGifts);
            R.append(", outboundPromoState=");
            R.append(this.outboundPromoState);
            R.append(", isUserPremium=");
            return a.M(R, this.isUserPremium, ")");
        }
    }

    /* compiled from: SettingsGiftingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Failure", "Loaded", "Loading", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Failure;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Failure;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failure extends ViewState {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001Bu\u0012\u001c\u0010\u0018\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0002\u0012\u0006\u0010\u0019\u001a\u00020\t\u0012\u0010\u0010\u001a\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\r0\f\u0012\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00100\u0005\u0012\u001c\u0010\u001c\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00050\u0002\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b4\u00105J&\u0010\u0007\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\r0\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u0005HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J&\u0010\u0014\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0014\u0010\bJ\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u008a\u0001\u0010\u001e\u001a\u00020\u00002\u001e\b\u0002\u0010\u0018\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00022\b\b\u0002\u0010\u0019\u001a\u00020\t2\u0012\b\u0002\u0010\u001a\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\r0\f2\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00100\u00052\u001e\b\u0002\u0010\u001c\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00050\u00022\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0015HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b \u0010\u0017J\u0010\u0010\"\u001a\u00020!HÖ\u0001¢\u0006\u0004\b\"\u0010#J\u001a\u0010'\u001a\u00020&2\b\u0010%\u001a\u0004\u0018\u00010$HÖ\u0003¢\u0006\u0004\b'\u0010(R#\u0010\u001a\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b*\u0010\u000fR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010+\u001a\u0004\b,\u0010\u0017R/\u0010\u0018\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010-\u001a\u0004\b.\u0010\bR\u0019\u0010\u0019\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010/\u001a\u0004\b0\u0010\u000bR/\u0010\u001c\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010-\u001a\u0004\b1\u0010\bR\u001f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00100\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00102\u001a\u0004\b3\u0010\u0012¨\u00066"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;", "", "", "Lcom/discord/primitives/SkuId;", "", "Lcom/discord/models/domain/ModelEntitlement;", "component1", "()Ljava/util/Map;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "component2", "()Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "", "Lcom/discord/primitives/Snowflake;", "component3", "()Ljava/util/Set;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "component4", "()Ljava/util/List;", "Lcom/discord/models/domain/ModelGift;", "component5", "", "component6", "()Ljava/lang/String;", "myEntitlements", "resolvingGiftState", "expandedSkuOrPlanIds", "outboundPromos", "myPurchasedGifts", "lastCopiedCode", "copy", "(Ljava/util/Map;Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Set;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loaded;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getExpandedSkuOrPlanIds", "Ljava/lang/String;", "getLastCopiedCode", "Ljava/util/Map;", "getMyEntitlements", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;", "getResolvingGiftState", "getMyPurchasedGifts", "Ljava/util/List;", "getOutboundPromos", HookHelper.constructorName, "(Ljava/util/Map;Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Set;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final Set<Long> expandedSkuOrPlanIds;
            private final String lastCopiedCode;
            private final Map<Long, List<ModelEntitlement>> myEntitlements;
            private final Map<Long, List<ModelGift>> myPurchasedGifts;
            private final List<OutboundPromoItem> outboundPromos;
            private final ResolvingGiftState resolvingGiftState;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(Map<Long, ? extends List<ModelEntitlement>> map, ResolvingGiftState resolvingGiftState, Set<Long> set, List<OutboundPromoItem> list, Map<Long, ? extends List<ModelGift>> map2, String str) {
                super(null);
                m.checkNotNullParameter(map, "myEntitlements");
                m.checkNotNullParameter(resolvingGiftState, "resolvingGiftState");
                m.checkNotNullParameter(set, "expandedSkuOrPlanIds");
                m.checkNotNullParameter(list, "outboundPromos");
                m.checkNotNullParameter(map2, "myPurchasedGifts");
                this.myEntitlements = map;
                this.resolvingGiftState = resolvingGiftState;
                this.expandedSkuOrPlanIds = set;
                this.outboundPromos = list;
                this.myPurchasedGifts = map2;
                this.lastCopiedCode = str;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, Map map, ResolvingGiftState resolvingGiftState, Set set, List list, Map map2, String str, int i, Object obj) {
                Map<Long, List<ModelEntitlement>> map3 = map;
                if ((i & 1) != 0) {
                    map3 = loaded.myEntitlements;
                }
                if ((i & 2) != 0) {
                    resolvingGiftState = loaded.resolvingGiftState;
                }
                ResolvingGiftState resolvingGiftState2 = resolvingGiftState;
                Set<Long> set2 = set;
                if ((i & 4) != 0) {
                    set2 = loaded.expandedSkuOrPlanIds;
                }
                Set set3 = set2;
                List<OutboundPromoItem> list2 = list;
                if ((i & 8) != 0) {
                    list2 = loaded.outboundPromos;
                }
                List list3 = list2;
                Map<Long, List<ModelGift>> map4 = map2;
                if ((i & 16) != 0) {
                    map4 = loaded.myPurchasedGifts;
                }
                Map map5 = map4;
                if ((i & 32) != 0) {
                    str = loaded.lastCopiedCode;
                }
                return loaded.copy(map3, resolvingGiftState2, set3, list3, map5, str);
            }

            public final Map<Long, List<ModelEntitlement>> component1() {
                return this.myEntitlements;
            }

            public final ResolvingGiftState component2() {
                return this.resolvingGiftState;
            }

            public final Set<Long> component3() {
                return this.expandedSkuOrPlanIds;
            }

            public final List<OutboundPromoItem> component4() {
                return this.outboundPromos;
            }

            public final Map<Long, List<ModelGift>> component5() {
                return this.myPurchasedGifts;
            }

            public final String component6() {
                return this.lastCopiedCode;
            }

            public final Loaded copy(Map<Long, ? extends List<ModelEntitlement>> map, ResolvingGiftState resolvingGiftState, Set<Long> set, List<OutboundPromoItem> list, Map<Long, ? extends List<ModelGift>> map2, String str) {
                m.checkNotNullParameter(map, "myEntitlements");
                m.checkNotNullParameter(resolvingGiftState, "resolvingGiftState");
                m.checkNotNullParameter(set, "expandedSkuOrPlanIds");
                m.checkNotNullParameter(list, "outboundPromos");
                m.checkNotNullParameter(map2, "myPurchasedGifts");
                return new Loaded(map, resolvingGiftState, set, list, map2, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.myEntitlements, loaded.myEntitlements) && m.areEqual(this.resolvingGiftState, loaded.resolvingGiftState) && m.areEqual(this.expandedSkuOrPlanIds, loaded.expandedSkuOrPlanIds) && m.areEqual(this.outboundPromos, loaded.outboundPromos) && m.areEqual(this.myPurchasedGifts, loaded.myPurchasedGifts) && m.areEqual(this.lastCopiedCode, loaded.lastCopiedCode);
            }

            public final Set<Long> getExpandedSkuOrPlanIds() {
                return this.expandedSkuOrPlanIds;
            }

            public final String getLastCopiedCode() {
                return this.lastCopiedCode;
            }

            public final Map<Long, List<ModelEntitlement>> getMyEntitlements() {
                return this.myEntitlements;
            }

            public final Map<Long, List<ModelGift>> getMyPurchasedGifts() {
                return this.myPurchasedGifts;
            }

            public final List<OutboundPromoItem> getOutboundPromos() {
                return this.outboundPromos;
            }

            public final ResolvingGiftState getResolvingGiftState() {
                return this.resolvingGiftState;
            }

            public int hashCode() {
                Map<Long, List<ModelEntitlement>> map = this.myEntitlements;
                int i = 0;
                int hashCode = (map != null ? map.hashCode() : 0) * 31;
                ResolvingGiftState resolvingGiftState = this.resolvingGiftState;
                int hashCode2 = (hashCode + (resolvingGiftState != null ? resolvingGiftState.hashCode() : 0)) * 31;
                Set<Long> set = this.expandedSkuOrPlanIds;
                int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
                List<OutboundPromoItem> list = this.outboundPromos;
                int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
                Map<Long, List<ModelGift>> map2 = this.myPurchasedGifts;
                int hashCode5 = (hashCode4 + (map2 != null ? map2.hashCode() : 0)) * 31;
                String str = this.lastCopiedCode;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode5 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(myEntitlements=");
                R.append(this.myEntitlements);
                R.append(", resolvingGiftState=");
                R.append(this.resolvingGiftState);
                R.append(", expandedSkuOrPlanIds=");
                R.append(this.expandedSkuOrPlanIds);
                R.append(", outboundPromos=");
                R.append(this.outboundPromos);
                R.append(", myPurchasedGifts=");
                R.append(this.myPurchasedGifts);
                R.append(", lastCopiedCode=");
                return a.H(R, this.lastCopiedCode, ")");
            }
        }

        /* compiled from: SettingsGiftingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public SettingsGiftingViewModel() {
        this(null, null, null, null, null, null, null, Opcodes.LAND, null);
    }

    public /* synthetic */ SettingsGiftingViewModel(StoreEntitlements storeEntitlements, StoreGifting storeGifting, StoreUserSettingsSystem storeUserSettingsSystem, StoreOutboundPromotions storeOutboundPromotions, StoreGooglePlayPurchases storeGooglePlayPurchases, GooglePlayBillingManager googlePlayBillingManager, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getEntitlements() : storeEntitlements, (i & 2) != 0 ? StoreStream.Companion.getGifting() : storeGifting, (i & 4) != 0 ? StoreStream.Companion.getUserSettingsSystem() : storeUserSettingsSystem, (i & 8) != 0 ? StoreStream.Companion.getOutboundPromotions() : storeOutboundPromotions, (i & 16) != 0 ? StoreStream.Companion.getGooglePlayPurchases() : storeGooglePlayPurchases, (i & 32) != 0 ? GooglePlayBillingManager.INSTANCE : googlePlayBillingManager, (i & 64) != 0 ? Companion.observeStores() : observable);
    }

    @MainThread
    private final ViewState buildViewState(StoreEntitlements.State state, ResolvingGiftState resolvingGiftState, Map<Long, ? extends List<ModelGift>> map, OutboundPromoData outboundPromoData) {
        Set<Long> set;
        if (!(state instanceof StoreEntitlements.State.Loaded) || !(outboundPromoData instanceof OutboundPromoData.Loaded)) {
            return state instanceof StoreEntitlements.State.Failure ? ViewState.Failure.INSTANCE : ViewState.Loading.INSTANCE;
        }
        ViewState viewState = getViewState();
        String str = null;
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded == null || (set = loaded.getExpandedSkuOrPlanIds()) == null) {
            set = n0.emptySet();
        }
        Set<Long> set2 = set;
        ViewState viewState2 = getViewState();
        if (!(viewState2 instanceof ViewState.Loaded)) {
            viewState2 = null;
        }
        ViewState.Loaded loaded2 = (ViewState.Loaded) viewState2;
        if (loaded2 != null) {
            str = loaded2.getLastCopiedCode();
        }
        return new ViewState.Loaded(((StoreEntitlements.State.Loaded) state).getGiftableEntitlements(), resolvingGiftState, set2, ((OutboundPromoData.Loaded) outboundPromoData).getOutboundPromos(), map, str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GiftAndPromoData combineData(StoreState storeState, List<ClaimedOutboundPromotion> list) {
        ResolvingGiftState resolvingGiftState;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded == null || (resolvingGiftState = loaded.getResolvingGiftState()) == null) {
            resolvingGiftState = ResolvingGiftState.NotResolving.INSTANCE;
        }
        ResolvingGiftState resolvingGiftState2 = resolvingGiftState;
        StoreEntitlements.State entitlementState = storeState.getEntitlementState();
        StoreOutboundPromotions.State outboundPromoState = storeState.getOutboundPromoState();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (entitlementState instanceof StoreEntitlements.State.Loaded) {
            for (ModelGift modelGift : storeState.getMyResolvedGifts()) {
                Long subscriptionPlanId = modelGift.getSubscriptionPlanId();
                long longValue = subscriptionPlanId != null ? subscriptionPlanId.longValue() : modelGift.getSkuId();
                if (!linkedHashMap.containsKey(Long.valueOf(longValue))) {
                    linkedHashMap.put(Long.valueOf(longValue), new ArrayList());
                }
                List list2 = (List) linkedHashMap.get(Long.valueOf(longValue));
                if (list2 != null) {
                    list2.add(modelGift);
                }
            }
        }
        return new GiftAndPromoData(entitlementState, resolvingGiftState2, linkedHashMap, outboundPromoState, list);
    }

    private final String getPromoImageUrl(long j) {
        String str = m.areEqual(this.storeUserSettingsSystem.getTheme(), ModelUserSettings.THEME_LIGHT) ? "logo-light" : "logo-dark";
        String str2 = BuildConfig.HOST_CDN;
        if (!(str2.length() > 0)) {
            str2 = null;
        }
        if (str2 == null) {
            str2 = BuildConfig.HOST_API;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append("/promotions/");
        sb.append(j);
        sb.append(MentionUtilsKt.SLASH_CHAR);
        return a.H(sb, str, "?size=256");
    }

    private final List<OutboundPromoItem> getPromos(List<OutboundPromotion> list, List<ClaimedOutboundPromotion> list2) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (OutboundPromotion outboundPromotion : list) {
            OutboundPromoItem outboundPromoItem = new OutboundPromoItem(new ClaimStatus.Unclaimed(outboundPromotion.b(), outboundPromotion.a()), outboundPromotion.b(), outboundPromotion.g(), outboundPromotion.f(), getPromoImageUrl(outboundPromotion.b()));
            linkedHashMap.put(Long.valueOf(outboundPromoItem.getId()), outboundPromoItem);
        }
        for (ClaimedOutboundPromotion claimedOutboundPromotion : list2) {
            OutboundPromoItem outboundPromoItem2 = new OutboundPromoItem(new ClaimStatus.Claimed(claimedOutboundPromotion.b().b(), claimedOutboundPromotion.a(), claimedOutboundPromotion.b().c(), claimedOutboundPromotion.d(), claimedOutboundPromotion.c()), claimedOutboundPromotion.b().b(), claimedOutboundPromotion.b().g(), claimedOutboundPromotion.b().f(), getPromoImageUrl(claimedOutboundPromotion.b().b()));
            linkedHashMap.put(Long.valueOf(outboundPromoItem2.getId()), outboundPromoItem2);
        }
        return u.toList(linkedHashMap.values());
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleAsyncData(GiftAndPromoData giftAndPromoData) {
        OutboundPromoData outboundPromoData;
        StoreOutboundPromotions.State outboundPromoState = giftAndPromoData.getOutboundPromoState();
        if (outboundPromoState instanceof StoreOutboundPromotions.State.Loaded) {
            outboundPromoData = new OutboundPromoData.Loaded(getPromos(((StoreOutboundPromotions.State.Loaded) outboundPromoState).getValidActivePromotions(), giftAndPromoData.getClaimedOutboundPromotions()));
        } else if (outboundPromoState instanceof StoreOutboundPromotions.State.Failed) {
            outboundPromoData = new OutboundPromoData.Loaded(n.emptyList());
        } else if (outboundPromoState instanceof StoreOutboundPromotions.State.Loading) {
            outboundPromoData = OutboundPromoData.Loading.INSTANCE;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        ViewState buildViewState = buildViewState(giftAndPromoData.getEntitlementState(), giftAndPromoData.getResolvingGiftState(), giftAndPromoData.getMyPurchasedGifts(), outboundPromoData);
        if (buildViewState instanceof ViewState.Loaded) {
            this.storeOutboundPromotions.markSeen();
        }
        updateViewState(buildViewState);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleGooglePlayPurchaseEvent(StoreGooglePlayPurchases.Event event) {
        if (event instanceof StoreGooglePlayPurchases.Event.PurchaseQuerySuccess) {
            StoreGooglePlayPurchases.Event.PurchaseQuerySuccess purchaseQuerySuccess = (StoreGooglePlayPurchases.Event.PurchaseQuerySuccess) event;
            if (purchaseQuerySuccess.getGiftCode() != null) {
                this.storeEntitlements.fetchMyGiftEntitlements();
                PublishSubject<GiftPurchaseEvent> publishSubject = this.giftPurchaseEventSubject;
                publishSubject.k.onNext(new GiftPurchaseEvent.CompleteGiftPurchase(purchaseQuerySuccess.getNewSkuName(), purchaseQuerySuccess.getGiftCode()));
                return;
            }
            StoreGifting storeGifting = this.storeGifting;
            Long skuId = purchaseQuerySuccess.getSkuId();
            m.checkNotNull(skuId);
            storeGifting.generateGiftCode(skuId.longValue(), purchaseQuerySuccess.getSubscriptionPlanId(), new SettingsGiftingViewModel$handleGooglePlayPurchaseEvent$2(this, event), new SettingsGiftingViewModel$handleGooglePlayPurchaseEvent$1(this));
        } else if (event instanceof StoreGooglePlayPurchases.Event.PurchaseQueryFailure) {
            PublishSubject<GiftPurchaseEvent> publishSubject2 = this.giftPurchaseEventSubject;
            publishSubject2.k.onNext(new GiftPurchaseEvent.ErrorGiftPurchase(R.string.billing_error_purchase));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleGooglePlayQueryStateUpdate(StoreGooglePlayPurchases.QueryState queryState) {
        if (m.areEqual(queryState, StoreGooglePlayPurchases.QueryState.InProgress.INSTANCE)) {
            PublishSubject<GiftPurchaseEvent> publishSubject = this.giftPurchaseEventSubject;
            publishSubject.k.onNext(GiftPurchaseEvent.StartGiftPurchase.INSTANCE);
        } else if (m.areEqual(queryState, StoreGooglePlayPurchases.QueryState.NotInProgress.INSTANCE)) {
            PublishSubject<GiftPurchaseEvent> publishSubject2 = this.giftPurchaseEventSubject;
            publishSubject2.k.onNext(GiftPurchaseEvent.NotInProgress.INSTANCE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void maybeCheckClaimedPromos(boolean z2) {
        if (!this.shouldCheckClaimedPromos.compareAndSet(true, false)) {
            return;
        }
        if (z2) {
            Observable L = ObservableExtensionsKt.restSubscribeOn$default(this.storeOutboundPromotions.fetchClaimedOutboundPromotions(), false, 1, null).s(new Action1<Throwable>() { // from class: com.discord.widgets.settings.premium.SettingsGiftingViewModel$maybeCheckClaimedPromos$1
                public final void call(Throwable th) {
                    AtomicBoolean atomicBoolean;
                    atomicBoolean = SettingsGiftingViewModel.this.shouldCheckClaimedPromos;
                    atomicBoolean.set(true);
                }
            }).L(SettingsGiftingViewModel$maybeCheckClaimedPromos$2.INSTANCE);
            m.checkNotNullExpressionValue(L, "storeOutboundPromotions.…rorReturn { emptyList() }");
            ObservableExtensionsKt.appSubscribe(L, SettingsGiftingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new SettingsGiftingViewModel$maybeCheckClaimedPromos$3(this.claimedPromotionsSubject));
            return;
        }
        this.claimedPromotionsSubject.onNext(n.emptyList());
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void onHandleGiftCode(StoreGifting.GiftState giftState) {
        ViewState viewState;
        ModelGift gift;
        ViewState viewState2 = getViewState();
        ModelGift modelGift = null;
        if (!(viewState2 instanceof ViewState.Loaded)) {
            viewState2 = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState2;
        if (loaded != null) {
            StoreEntitlements.State.Loaded loaded2 = new StoreEntitlements.State.Loaded(loaded.getMyEntitlements(), h0.emptyMap());
            OutboundPromoData.Loaded loaded3 = new OutboundPromoData.Loaded(loaded.getOutboundPromos());
            if ((giftState instanceof StoreGifting.GiftState.Loading) || (giftState instanceof StoreGifting.GiftState.Redeeming)) {
                viewState = buildViewState(loaded2, ResolvingGiftState.Resolving.INSTANCE, loaded.getMyPurchasedGifts(), loaded3);
            } else if (!(giftState instanceof StoreGifting.GiftState.LoadFailed) && !(giftState instanceof StoreGifting.GiftState.RedeemedFailed) && !(giftState instanceof StoreGifting.GiftState.Invalid)) {
                boolean z2 = giftState instanceof StoreGifting.GiftState.Revoking;
                if (!z2 && !(giftState instanceof StoreGifting.GiftState.Resolved)) {
                    throw new NoWhenBranchMatchedException();
                }
                StoreGifting.GiftState.Resolved resolved = (StoreGifting.GiftState.Resolved) (!(giftState instanceof StoreGifting.GiftState.Resolved) ? null : giftState);
                if (resolved == null || (gift = resolved.getGift()) == null) {
                    if (!z2) {
                        giftState = null;
                    }
                    StoreGifting.GiftState.Revoking revoking = (StoreGifting.GiftState.Revoking) giftState;
                    if (revoking != null) {
                        modelGift = revoking.getGift();
                    }
                } else {
                    modelGift = gift;
                }
                if (modelGift != null) {
                    if (!modelGift.isClaimedByMe()) {
                        this.onGiftCodeResolved.invoke(modelGift.getCode());
                    }
                    viewState = buildViewState(loaded2, ResolvingGiftState.NotResolving.INSTANCE, loaded.getMyPurchasedGifts(), loaded3);
                } else {
                    return;
                }
            } else {
                viewState = buildViewState(loaded2, ResolvingGiftState.Error.INSTANCE, loaded.getMyPurchasedGifts(), loaded3);
            }
            updateViewState(viewState);
        }
    }

    @MainThread
    public final void handleClaimedPromo(ClaimStatus.Claimed claimed) {
        m.checkNotNullParameter(claimed, "claimedStatus");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            List<OutboundPromoItem> outboundPromos = loaded.getOutboundPromos();
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(outboundPromos, 10));
            for (OutboundPromoItem outboundPromoItem : outboundPromos) {
                if (outboundPromoItem.getId() == claimed.getPromoId()) {
                    outboundPromoItem = OutboundPromoItem.copy$default(outboundPromoItem, claimed, 0L, null, null, null, 30, null);
                }
                arrayList.add(outboundPromoItem);
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, arrayList, null, null, 55, null));
        }
    }

    @MainThread
    public final void handleCopyClicked(String str) {
        m.checkNotNullParameter(str, "giftCode");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, str, 31, null));
        }
    }

    public final void handlePromoButtonClicked(OutboundPromoItem outboundPromoItem) {
        m.checkNotNullParameter(outboundPromoItem, "promoItem");
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowPromoDialog(outboundPromoItem.getClaimStatus()));
    }

    public final void handlePromoMoreDetailsClicked(OutboundPromoItem outboundPromoItem) {
        m.checkNotNullParameter(outboundPromoItem, "promoItem");
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowPromoBottomSheet(outboundPromoItem.getTerms()));
    }

    @MainThread
    public final void handleSkuClicked(long j, Long l) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            Set mutableSet = u.toMutableSet(loaded.getExpandedSkuOrPlanIds());
            long longValue = l != null ? l.longValue() : j;
            if (mutableSet.contains(Long.valueOf(longValue))) {
                mutableSet.remove(Long.valueOf(longValue));
            } else {
                mutableSet.add(Long.valueOf(longValue));
                this.storeGifting.fetchMyGiftsForSku(j, l);
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, mutableSet, null, null, null, 59, null));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final PublishSubject<GiftPurchaseEvent> observeGiftPurchaseEvents() {
        PublishSubject<GiftPurchaseEvent> publishSubject = this.giftPurchaseEventSubject;
        m.checkNotNullExpressionValue(publishSubject, "giftPurchaseEventSubject");
        return publishSubject;
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        this.subscriptions.b();
    }

    @MainThread
    public final void redeemGiftCode(String str, AppComponent appComponent) {
        m.checkNotNullParameter(str, "giftCode");
        m.checkNotNullParameter(appComponent, "appComponent");
        if (getViewState() instanceof ViewState.Loaded) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(this.storeGifting.requestGift(str), appComponent, null, 2, null), SettingsGiftingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new SettingsGiftingViewModel$redeemGiftCode$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new SettingsGiftingViewModel$redeemGiftCode$2(this));
        }
    }

    public final void setOnGiftCodeResolved(Function1<? super String, Unit> function1) {
        m.checkNotNullParameter(function1, "onGiftCodeResolved");
        this.onGiftCodeResolved = function1;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsGiftingViewModel(StoreEntitlements storeEntitlements, StoreGifting storeGifting, StoreUserSettingsSystem storeUserSettingsSystem, StoreOutboundPromotions storeOutboundPromotions, StoreGooglePlayPurchases storeGooglePlayPurchases, GooglePlayBillingManager googlePlayBillingManager, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(storeEntitlements, "storeEntitlements");
        m.checkNotNullParameter(storeGifting, "storeGifting");
        m.checkNotNullParameter(storeUserSettingsSystem, "storeUserSettingsSystem");
        m.checkNotNullParameter(storeOutboundPromotions, "storeOutboundPromotions");
        m.checkNotNullParameter(storeGooglePlayPurchases, "storeGooglePlayPurchases");
        m.checkNotNullParameter(googlePlayBillingManager, "gPlayBillingManager");
        m.checkNotNullParameter(observable, "storeObservable");
        this.storeEntitlements = storeEntitlements;
        this.storeGifting = storeGifting;
        this.storeUserSettingsSystem = storeUserSettingsSystem;
        this.storeOutboundPromotions = storeOutboundPromotions;
        BehaviorSubject<StoreState> k0 = BehaviorSubject.k0();
        this.storeStateSubject = k0;
        BehaviorSubject<List<ClaimedOutboundPromotion>> k02 = BehaviorSubject.k0();
        this.claimedPromotionsSubject = k02;
        this.shouldCheckClaimedPromos = new AtomicBoolean(true);
        this.giftPurchaseEventSubject = PublishSubject.k0();
        this.eventSubject = PublishSubject.k0();
        this.onGiftCodeResolved = SettingsGiftingViewModel$onGiftCodeResolved$1.INSTANCE;
        this.subscriptions = new CompositeSubscription();
        storeEntitlements.fetchMyGiftEntitlements();
        googlePlayBillingManager.queryPurchases();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(storeGooglePlayPurchases.observeEvents(), this, null, 2, null), SettingsGiftingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(storeGooglePlayPurchases.observeQueryState(), this, null, 2, null), SettingsGiftingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(this));
        Observable t = ObservableExtensionsKt.computationLatest(observable).t(new Action1<StoreState>() { // from class: com.discord.widgets.settings.premium.SettingsGiftingViewModel.3
            public final void call(StoreState storeState) {
                SettingsGiftingViewModel.this.maybeCheckClaimedPromos(storeState.isUserPremium());
            }
        });
        m.checkNotNullExpressionValue(t, "storeObservable\n        …oreState.isUserPremium) }");
        ObservableExtensionsKt.appSubscribe(t, SettingsGiftingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass4(k0));
        m.checkNotNullExpressionValue(k0, "storeStateSubject");
        Observable computationLatest = ObservableExtensionsKt.computationLatest(k0);
        m.checkNotNullExpressionValue(k02, "claimedPromotionsSubject");
        Observable computationLatest2 = ObservableExtensionsKt.computationLatest(k02);
        final AnonymousClass5 r3 = new AnonymousClass5(this);
        Observable q = Observable.j(computationLatest, computationLatest2, new Func2() { // from class: com.discord.widgets.settings.premium.SettingsGiftingViewModel$sam$rx_functions_Func2$0
            @Override // rx.functions.Func2
            public final /* synthetic */ Object call(Object obj, Object obj2) {
                return Function2.this.invoke(obj, obj2);
            }
        }).q();
        m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), SettingsGiftingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass6(this));
    }
}
