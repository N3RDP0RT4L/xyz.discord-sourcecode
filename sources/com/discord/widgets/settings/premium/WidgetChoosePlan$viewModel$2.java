package com.discord.widgets.settings.premium;

import com.discord.app.AppViewModel;
import com.discord.widgets.settings.premium.ChoosePlanViewModel;
import com.discord.widgets.settings.premium.WidgetChoosePlan;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChoosePlan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChoosePlan$viewModel$2 extends o implements Function0<AppViewModel<ChoosePlanViewModel.ViewState>> {
    public final /* synthetic */ WidgetChoosePlan this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChoosePlan$viewModel$2(WidgetChoosePlan widgetChoosePlan) {
        super(0);
        this.this$0 = widgetChoosePlan;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ChoosePlanViewModel.ViewState> invoke() {
        String stringExtra = this.this$0.getMostRecentIntent().getStringExtra(WidgetChoosePlan.RESULT_EXTRA_OLD_SKU_NAME);
        Serializable serializableExtra = this.this$0.getMostRecentIntent().getSerializableExtra(WidgetChoosePlan.RESULT_VIEW_TYPE);
        Objects.requireNonNull(serializableExtra, "null cannot be cast to non-null type com.discord.widgets.settings.premium.WidgetChoosePlan.ViewType");
        return new ChoosePlanViewModel((WidgetChoosePlan.ViewType) serializableExtra, stringExtra, null, 4, null);
    }
}
