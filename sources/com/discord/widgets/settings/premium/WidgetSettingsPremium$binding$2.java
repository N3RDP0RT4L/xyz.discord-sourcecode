package com.discord.widgets.settings.premium;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import b.a.i.d0;
import b.a.i.q0;
import b.a.i.v2;
import b.a.i.v5;
import b.a.i.w2;
import b.a.i.x2;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetSettingsPremiumBinding;
import com.discord.views.ActiveSubscriptionView;
import com.discord.views.premium.AccountCreditView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsPremium.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSettingsPremiumBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsPremiumBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettingsPremium$binding$2 extends k implements Function1<View, WidgetSettingsPremiumBinding> {
    public static final WidgetSettingsPremium$binding$2 INSTANCE = new WidgetSettingsPremium$binding$2();

    public WidgetSettingsPremium$binding$2() {
        super(1, WidgetSettingsPremiumBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsPremiumBinding;", 0);
    }

    public final WidgetSettingsPremiumBinding invoke(View view) {
        int i;
        m.checkNotNullParameter(view, "p1");
        View findViewById = view.findViewById(R.id.account_credits);
        int i2 = R.id.premium_settings_grandfathered;
        if (findViewById != null) {
            AccountCreditView accountCreditView = (AccountCreditView) findViewById.findViewById(R.id.credit_nitro);
            if (accountCreditView != null) {
                AccountCreditView accountCreditView2 = (AccountCreditView) findViewById.findViewById(R.id.credit_nitro_classic);
                if (accountCreditView2 != null) {
                    View findViewById2 = findViewById.findViewById(R.id.credit_nitro_divider);
                    if (findViewById2 != null) {
                        d0 d0Var = new d0((CardView) findViewById, accountCreditView, accountCreditView2, findViewById2);
                        View findViewById3 = view.findViewById(R.id.payment);
                        if (findViewById3 != null) {
                            int i3 = R.id.active_subscriptions_bottom_button;
                            MaterialButton materialButton = (MaterialButton) findViewById3.findViewById(R.id.active_subscriptions_bottom_button);
                            if (materialButton != null) {
                                i3 = R.id.premium_settings_billing_divider;
                                View findViewById4 = findViewById3.findViewById(R.id.premium_settings_billing_divider);
                                if (findViewById4 != null) {
                                    i3 = R.id.premium_settings_billing_information;
                                    TextView textView = (TextView) findViewById3.findViewById(R.id.premium_settings_billing_information);
                                    if (textView != null) {
                                        i3 = R.id.premium_settings_google_play_manage;
                                        TextView textView2 = (TextView) findViewById3.findViewById(R.id.premium_settings_google_play_manage);
                                        if (textView2 != null) {
                                            LinearLayout linearLayout = (LinearLayout) findViewById3;
                                            i3 = R.id.premium_settings_payment_header;
                                            TextView textView3 = (TextView) findViewById3.findViewById(R.id.premium_settings_payment_header);
                                            if (textView3 != null) {
                                                q0 q0Var = new q0(linearLayout, materialButton, findViewById4, textView, textView2, linearLayout, textView3);
                                                View findViewById5 = view.findViewById(R.id.premium_perks);
                                                if (findViewById5 != null) {
                                                    int i4 = R.id.guild_subscription_perk;
                                                    TextView textView4 = (TextView) findViewById5.findViewById(R.id.guild_subscription_perk);
                                                    if (textView4 != null) {
                                                        i4 = R.id.identity_perk;
                                                        TextView textView5 = (TextView) findViewById5.findViewById(R.id.identity_perk);
                                                        if (textView5 != null) {
                                                            i4 = R.id.max_guilds_perk;
                                                            TextView textView6 = (TextView) findViewById5.findViewById(R.id.max_guilds_perk);
                                                            if (textView6 != null) {
                                                                i4 = R.id.max_message_length_perk;
                                                                TextView textView7 = (TextView) findViewById5.findViewById(R.id.max_message_length_perk);
                                                                if (textView7 != null) {
                                                                    i4 = R.id.upload_size_perk;
                                                                    TextView textView8 = (TextView) findViewById5.findViewById(R.id.upload_size_perk);
                                                                    if (textView8 != null) {
                                                                        v2 v2Var = new v2((LinearLayout) findViewById5, textView4, textView5, textView6, textView7, textView8);
                                                                        ActiveSubscriptionView activeSubscriptionView = (ActiveSubscriptionView) view.findViewById(R.id.premium_settings_active_guild_subscription);
                                                                        if (activeSubscriptionView != null) {
                                                                            ActiveSubscriptionView activeSubscriptionView2 = (ActiveSubscriptionView) view.findViewById(R.id.premium_settings_active_subscription);
                                                                            if (activeSubscriptionView2 != null) {
                                                                                LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.premium_settings_credit_container);
                                                                                if (linearLayout2 != null) {
                                                                                    TextView textView9 = (TextView) view.findViewById(R.id.premium_settings_credit_header);
                                                                                    if (textView9 != null) {
                                                                                        TextView textView10 = (TextView) view.findViewById(R.id.premium_settings_grandfathered);
                                                                                        if (textView10 != null) {
                                                                                            TextView textView11 = (TextView) view.findViewById(R.id.premium_settings_legalese);
                                                                                            if (textView11 != null) {
                                                                                                View findViewById6 = view.findViewById(R.id.premium_settings_price_change_notice);
                                                                                                if (findViewById6 != null) {
                                                                                                    CardView cardView = (CardView) findViewById6;
                                                                                                    TextView textView12 = (TextView) findViewById6.findViewById(R.id.textview);
                                                                                                    if (textView12 != null) {
                                                                                                        v5 v5Var = new v5(cardView, cardView, textView12);
                                                                                                        MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.premium_settings_retry);
                                                                                                        if (materialButton2 != null) {
                                                                                                            ScrollView scrollView = (ScrollView) view.findViewById(R.id.premium_settings_scrollview);
                                                                                                            if (scrollView != null) {
                                                                                                                Button button = (Button) view.findViewById(R.id.premium_settings_status_notice_button);
                                                                                                                if (button != null) {
                                                                                                                    CardView cardView2 = (CardView) view.findViewById(R.id.premium_settings_status_notice_container);
                                                                                                                    if (cardView2 != null) {
                                                                                                                        TextView textView13 = (TextView) view.findViewById(R.id.premium_settings_status_notice_tv);
                                                                                                                        if (textView13 != null) {
                                                                                                                            LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.premium_settings_subscription_container);
                                                                                                                            if (linearLayout3 != null) {
                                                                                                                                TextView textView14 = (TextView) view.findViewById(R.id.premium_settings_subscriptions_header);
                                                                                                                                if (textView14 != null) {
                                                                                                                                    AppViewFlipper appViewFlipper = (AppViewFlipper) view.findViewById(R.id.premium_settings_view_flipper);
                                                                                                                                    if (appViewFlipper != null) {
                                                                                                                                        View findViewById7 = view.findViewById(R.id.premium_tier_1);
                                                                                                                                        if (findViewById7 != null) {
                                                                                                                                            int i5 = R.id.premium_settings_premium_classic;
                                                                                                                                            MaterialButton materialButton3 = (MaterialButton) findViewById7.findViewById(R.id.premium_settings_premium_classic);
                                                                                                                                            if (materialButton3 != null) {
                                                                                                                                                i5 = R.id.tier_1_guild_subscription_perk;
                                                                                                                                                TextView textView15 = (TextView) findViewById7.findViewById(R.id.tier_1_guild_subscription_perk);
                                                                                                                                                if (textView15 != null) {
                                                                                                                                                    i5 = R.id.tier1_upload_size_perk;
                                                                                                                                                    TextView textView16 = (TextView) findViewById7.findViewById(R.id.tier1_upload_size_perk);
                                                                                                                                                    if (textView16 != null) {
                                                                                                                                                        w2 w2Var = new w2((LinearLayout) findViewById7, materialButton3, textView15, textView16);
                                                                                                                                                        View findViewById8 = view.findViewById(R.id.premium_tier_2);
                                                                                                                                                        if (findViewById8 != null) {
                                                                                                                                                            int i6 = R.id.premium_settings_nitro;
                                                                                                                                                            ImageView imageView = (ImageView) findViewById8.findViewById(R.id.premium_settings_nitro);
                                                                                                                                                            if (imageView != null) {
                                                                                                                                                                i6 = R.id.premium_settings_nitro_wumpus;
                                                                                                                                                                ImageView imageView2 = (ImageView) findViewById8.findViewById(R.id.premium_settings_nitro_wumpus);
                                                                                                                                                                if (imageView2 != null) {
                                                                                                                                                                    i6 = R.id.premium_settings_premium;
                                                                                                                                                                    MaterialButton materialButton4 = (MaterialButton) findViewById8.findViewById(R.id.premium_settings_premium);
                                                                                                                                                                    if (materialButton4 != null) {
                                                                                                                                                                        return new WidgetSettingsPremiumBinding((CoordinatorLayout) view, d0Var, q0Var, v2Var, activeSubscriptionView, activeSubscriptionView2, linearLayout2, textView9, textView10, textView11, v5Var, materialButton2, scrollView, button, cardView2, textView13, linearLayout3, textView14, appViewFlipper, w2Var, new x2((LinearLayout) findViewById8, imageView, imageView2, materialButton4));
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                            throw new NullPointerException("Missing required view with ID: ".concat(findViewById8.getResources().getResourceName(i6)));
                                                                                                                                                        }
                                                                                                                                                        i2 = R.id.premium_tier_2;
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            throw new NullPointerException("Missing required view with ID: ".concat(findViewById7.getResources().getResourceName(i5)));
                                                                                                                                        }
                                                                                                                                        i2 = R.id.premium_tier_1;
                                                                                                                                    } else {
                                                                                                                                        i2 = R.id.premium_settings_view_flipper;
                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    i2 = R.id.premium_settings_subscriptions_header;
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                i2 = R.id.premium_settings_subscription_container;
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            i2 = R.id.premium_settings_status_notice_tv;
                                                                                                                        }
                                                                                                                    } else {
                                                                                                                        i2 = R.id.premium_settings_status_notice_container;
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    i2 = R.id.premium_settings_status_notice_button;
                                                                                                                }
                                                                                                            } else {
                                                                                                                i2 = R.id.premium_settings_scrollview;
                                                                                                            }
                                                                                                        } else {
                                                                                                            i2 = R.id.premium_settings_retry;
                                                                                                        }
                                                                                                    } else {
                                                                                                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById6.getResources().getResourceName(R.id.textview)));
                                                                                                    }
                                                                                                } else {
                                                                                                    i2 = R.id.premium_settings_price_change_notice;
                                                                                                }
                                                                                            } else {
                                                                                                i2 = R.id.premium_settings_legalese;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        i2 = R.id.premium_settings_credit_header;
                                                                                    }
                                                                                } else {
                                                                                    i2 = R.id.premium_settings_credit_container;
                                                                                }
                                                                            } else {
                                                                                i2 = R.id.premium_settings_active_subscription;
                                                                            }
                                                                        } else {
                                                                            i2 = R.id.premium_settings_active_guild_subscription;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById5.getResources().getResourceName(i4)));
                                                }
                                                i2 = R.id.premium_perks;
                                            }
                                        }
                                    }
                                }
                            }
                            throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(i3)));
                        }
                        i2 = R.id.payment;
                    } else {
                        i = R.id.credit_nitro_divider;
                    }
                } else {
                    i = R.id.credit_nitro_classic;
                }
            } else {
                i = R.id.credit_nitro;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(i)));
        }
        i2 = R.id.account_credits;
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
    }
}
