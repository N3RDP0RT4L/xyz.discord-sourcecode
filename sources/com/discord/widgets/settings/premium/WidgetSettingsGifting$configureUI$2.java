package com.discord.widgets.settings.premium;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import com.discord.utilities.gifting.GiftingUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsGifting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "giftCode", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGifting$configureUI$2 extends o implements Function1<String, Unit> {
    public final /* synthetic */ WidgetSettingsGifting this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsGifting$configureUI$2(WidgetSettingsGifting widgetSettingsGifting) {
        super(1);
        this.this$0 = widgetSettingsGifting;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(String str) {
        invoke2(str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        SettingsGiftingViewModel viewModel;
        m.checkNotNullParameter(str, "giftCode");
        Context context = this.this$0.getContext();
        ClipboardManager clipboardManager = null;
        Object systemService = context != null ? context.getSystemService("clipboard") : null;
        if (systemService instanceof ClipboardManager) {
            clipboardManager = systemService;
        }
        ClipboardManager clipboardManager2 = clipboardManager;
        if (clipboardManager2 != null) {
            clipboardManager2.setPrimaryClip(ClipData.newPlainText("", GiftingUtils.INSTANCE.generateGiftUrl(str)));
        }
        viewModel = this.this$0.getViewModel();
        viewModel.handleCopyClicked(str);
    }
}
