package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.stores.StoreGooglePlaySkuDetails;
import com.discord.stores.StoreSubscriptions;
import com.discord.widgets.settings.premium.ChoosePlanViewModel;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function4;
/* compiled from: ChoosePlanViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\n"}, d2 = {"Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "p1", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "p2", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "p3", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "p4", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;", "invoke", "(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class ChoosePlanViewModel$Companion$observeStores$1 extends k implements Function4<StoreGooglePlaySkuDetails.State, StoreGooglePlayPurchases.State, StoreGooglePlayPurchases.QueryState, StoreSubscriptions.SubscriptionsState, ChoosePlanViewModel.StoreState> {
    public static final ChoosePlanViewModel$Companion$observeStores$1 INSTANCE = new ChoosePlanViewModel$Companion$observeStores$1();

    public ChoosePlanViewModel$Companion$observeStores$1() {
        super(4, ChoosePlanViewModel.StoreState.class, HookHelper.constructorName, "<init>(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V", 0);
    }

    public final ChoosePlanViewModel.StoreState invoke(StoreGooglePlaySkuDetails.State state, StoreGooglePlayPurchases.State state2, StoreGooglePlayPurchases.QueryState queryState, StoreSubscriptions.SubscriptionsState subscriptionsState) {
        m.checkNotNullParameter(state, "p1");
        m.checkNotNullParameter(state2, "p2");
        m.checkNotNullParameter(queryState, "p3");
        m.checkNotNullParameter(subscriptionsState, "p4");
        return new ChoosePlanViewModel.StoreState(state, state2, queryState, subscriptionsState);
    }
}
