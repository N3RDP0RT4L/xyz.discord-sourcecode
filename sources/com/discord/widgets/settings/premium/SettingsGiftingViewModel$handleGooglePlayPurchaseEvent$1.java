package com.discord.widgets.settings.premium;

import com.discord.stores.StoreEntitlements;
import com.discord.utilities.error.Error;
import com.discord.widgets.settings.premium.SettingsGiftingViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: SettingsGiftingViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsGiftingViewModel$handleGooglePlayPurchaseEvent$1 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ SettingsGiftingViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsGiftingViewModel$handleGooglePlayPurchaseEvent$1(SettingsGiftingViewModel settingsGiftingViewModel) {
        super(1);
        this.this$0 = settingsGiftingViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        StoreEntitlements storeEntitlements;
        PublishSubject publishSubject;
        m.checkNotNullParameter(error, "it");
        storeEntitlements = this.this$0.storeEntitlements;
        storeEntitlements.fetchMyGiftEntitlements();
        publishSubject = this.this$0.giftPurchaseEventSubject;
        publishSubject.k.onNext(new SettingsGiftingViewModel.GiftPurchaseEvent.ErrorGiftPurchase(R.string.billing_error_purchase));
    }
}
