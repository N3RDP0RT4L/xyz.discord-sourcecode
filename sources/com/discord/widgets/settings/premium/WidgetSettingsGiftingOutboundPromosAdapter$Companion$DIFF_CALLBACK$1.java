package com.discord.widgets.settings.premium;

import com.discord.widgets.settings.premium.SettingsGiftingViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsGiftingOutboundPromosAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "it", "", "invoke", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGiftingOutboundPromosAdapter$Companion$DIFF_CALLBACK$1 extends o implements Function1<SettingsGiftingViewModel.OutboundPromoItem, Object> {
    public static final WidgetSettingsGiftingOutboundPromosAdapter$Companion$DIFF_CALLBACK$1 INSTANCE = new WidgetSettingsGiftingOutboundPromosAdapter$Companion$DIFF_CALLBACK$1();

    public WidgetSettingsGiftingOutboundPromosAdapter$Companion$DIFF_CALLBACK$1() {
        super(1);
    }

    public final Object invoke(SettingsGiftingViewModel.OutboundPromoItem outboundPromoItem) {
        m.checkNotNullParameter(outboundPromoItem, "it");
        return Long.valueOf(outboundPromoItem.getId());
    }
}
