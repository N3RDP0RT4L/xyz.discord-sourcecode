package com.discord.widgets.settings.premium;

import com.discord.i18n.RenderContext;
import d0.z.d.m;
import d0.z.d.o;
import java.text.NumberFormat;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsPremium.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPremium$onViewBound$3 extends o implements Function1<RenderContext, Unit> {
    public static final WidgetSettingsPremium$onViewBound$3 INSTANCE = new WidgetSettingsPremium$onViewBound$3();

    public WidgetSettingsPremium$onViewBound$3() {
        super(1);
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        renderContext.a.put("numBoosts", String.valueOf(2));
        Map<String, String> map = renderContext.a;
        String format = NumberFormat.getPercentInstance().format(Float.valueOf(0.3f));
        m.checkNotNullExpressionValue(format, "NumberFormat.getPercentI…_DISCOUNT_PERCENT\n      )");
        map.put("discountPercentage", format);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }
}
