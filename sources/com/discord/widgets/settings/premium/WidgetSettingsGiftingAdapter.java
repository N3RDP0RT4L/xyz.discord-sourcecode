package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.premium.SubscriptionInterval;
import com.discord.api.premium.SubscriptionPlan;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.ViewGiftEntitlementListItemBinding;
import com.discord.databinding.ViewGiftSkuListItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelEntitlement;
import com.discord.models.domain.ModelGift;
import com.discord.models.domain.ModelSku;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.gifting.GiftingUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.widgets.settings.premium.WidgetSettingsGiftingAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetSettingsGiftingAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004 !\"#B\u000f\u0012\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u008f\u0001\u0010\u0018\u001a\u00020\u00102\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\n2$\u0010\u0011\u001a \u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\f\u0012\n\u0018\u00010\rj\u0004\u0018\u0001`\u000f\u0012\u0004\u0012\u00020\u00100\f2\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00100\u00122\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00100\u00122$\u0010\u0017\u001a \u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\f\u0012\n\u0018\u00010\rj\u0004\u0018\u0001`\u000f\u0012\u0004\u0012\u00020\u00100\f¢\u0006\u0004\b\u0018\u0010\u0019R\"\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00100\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u001aR\"\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00100\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u001aR4\u0010\u0011\u001a \u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\f\u0012\n\u0018\u00010\rj\u0004\u0018\u0001`\u000f\u0012\u0004\u0012\u00020\u00100\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u001bR4\u0010\u0017\u001a \u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\f\u0012\n\u0018\u00010\rj\u0004\u0018\u0001`\u000f\u0012\u0004\u0012\u00020\u00100\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u001b¨\u0006$"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "", "giftItems", "Lkotlin/Function2;", "", "Lcom/discord/primitives/SkuId;", "Lcom/discord/primitives/PlanId;", "", "onClickSkuListener", "Lkotlin/Function1;", "", "onClickCopyListener", "Lcom/discord/models/domain/ModelGift;", "onRevokeClickListener", "onGenerateClickListener", "configure", "(Ljava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V", "Lkotlin/jvm/functions/Function1;", "Lkotlin/jvm/functions/Function2;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "EntitlementListItem", "GiftItem", "NoGiftsListItem", "SkuListItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGiftingAdapter extends MGRecyclerAdapterSimple<GiftItem> {
    private Function2<? super Long, ? super Long, Unit> onClickSkuListener = WidgetSettingsGiftingAdapter$onClickSkuListener$1.INSTANCE;
    private Function1<? super String, Unit> onClickCopyListener = WidgetSettingsGiftingAdapter$onClickCopyListener$1.INSTANCE;
    private Function1<? super ModelGift, Unit> onRevokeClickListener = WidgetSettingsGiftingAdapter$onRevokeClickListener$1.INSTANCE;
    private Function2<? super Long, ? super Long, Unit> onGenerateClickListener = WidgetSettingsGiftingAdapter$onGenerateClickListener$1.INSTANCE;

    /* compiled from: WidgetSettingsGiftingAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u00102\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u0010B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$EntitlementListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;)V", "Lcom/discord/databinding/ViewGiftEntitlementListItemBinding;", "binding", "Lcom/discord/databinding/ViewGiftEntitlementListItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EntitlementListItem extends MGRecyclerViewHolder<WidgetSettingsGiftingAdapter, GiftItem> {
        public static final Companion Companion = new Companion(null);
        private static final int VIEW_INDEX_CODE = 0;
        private static final int VIEW_INDEX_GENERATE = 1;
        private final ViewGiftEntitlementListItemBinding binding;

        /* compiled from: WidgetSettingsGiftingAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$EntitlementListItem$Companion;", "", "", "VIEW_INDEX_CODE", "I", "VIEW_INDEX_GENERATE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public EntitlementListItem(WidgetSettingsGiftingAdapter widgetSettingsGiftingAdapter) {
            super((int) R.layout.view_gift_entitlement_list_item, widgetSettingsGiftingAdapter);
            m.checkNotNullParameter(widgetSettingsGiftingAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.gift_entitlement_code;
            TextView textView = (TextView) view.findViewById(R.id.gift_entitlement_code);
            if (textView != null) {
                i = R.id.gift_entitlement_code_container;
                RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.gift_entitlement_code_container);
                if (relativeLayout != null) {
                    FrameLayout frameLayout = (FrameLayout) view;
                    i = R.id.gift_entitlement_copy;
                    MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.gift_entitlement_copy);
                    if (materialButton != null) {
                        i = R.id.gift_entitlement_divider;
                        View findViewById = view.findViewById(R.id.gift_entitlement_divider);
                        if (findViewById != null) {
                            i = R.id.gift_entitlement_flipper;
                            AppViewFlipper appViewFlipper = (AppViewFlipper) view.findViewById(R.id.gift_entitlement_flipper);
                            if (appViewFlipper != null) {
                                i = R.id.gift_entitlement_generate;
                                MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.gift_entitlement_generate);
                                if (materialButton2 != null) {
                                    i = R.id.gift_entitlement_revoke;
                                    LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.gift_entitlement_revoke);
                                    if (linkifiedTextView != null) {
                                        ViewGiftEntitlementListItemBinding viewGiftEntitlementListItemBinding = new ViewGiftEntitlementListItemBinding(frameLayout, textView, relativeLayout, frameLayout, materialButton, findViewById, appViewFlipper, materialButton2, linkifiedTextView);
                                        m.checkNotNullExpressionValue(viewGiftEntitlementListItemBinding, "ViewGiftEntitlementListItemBinding.bind(itemView)");
                                        this.binding = viewGiftEntitlementListItemBinding;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetSettingsGiftingAdapter access$getAdapter$p(EntitlementListItem entitlementListItem) {
            return (WidgetSettingsGiftingAdapter) entitlementListItem.adapter;
        }

        public void onConfigure(int i, final GiftItem giftItem) {
            CharSequence d;
            int i2;
            m.checkNotNullParameter(giftItem, "data");
            super.onConfigure(i, (int) giftItem);
            if (m.areEqual(giftItem.getExpanded(), Boolean.FALSE)) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                layoutParams.height = 0;
                view.setLayoutParams(layoutParams);
                return;
            }
            View view3 = this.itemView;
            m.checkNotNullExpressionValue(view3, "itemView");
            View view4 = this.itemView;
            m.checkNotNullExpressionValue(view4, "itemView");
            ViewGroup.LayoutParams layoutParams2 = view4.getLayoutParams();
            layoutParams2.height = -2;
            view3.setLayoutParams(layoutParams2);
            View view5 = this.itemView;
            m.checkNotNullExpressionValue(view5, "itemView");
            Context context = view5.getContext();
            if (giftItem.getEntitlement() != null && giftItem.isLastItem() != null) {
                if (giftItem.getGift() != null) {
                    AppViewFlipper appViewFlipper = this.binding.g;
                    m.checkNotNullExpressionValue(appViewFlipper, "binding.giftEntitlementFlipper");
                    appViewFlipper.setDisplayedChild(0);
                    boolean areEqual = m.areEqual(giftItem.getWasCopied(), Boolean.TRUE);
                    MaterialButton materialButton = this.binding.e;
                    m.checkNotNullExpressionValue(materialButton, "binding.giftEntitlementCopy");
                    View view6 = this.itemView;
                    m.checkNotNullExpressionValue(view6, "itemView");
                    d = b.d(view6, areEqual ? R.string.copied : R.string.copy, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                    materialButton.setText(d);
                    RelativeLayout relativeLayout = this.binding.c;
                    if (areEqual) {
                        m.checkNotNullExpressionValue(context, "context");
                        i2 = DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.gift_code_copied_outline, 0, 2, (Object) null);
                    } else {
                        m.checkNotNullExpressionValue(context, "context");
                        i2 = DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.primary_660_bg_outline, 0, 2, (Object) null);
                    }
                    relativeLayout.setBackgroundResource(i2);
                    this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$3
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view7) {
                            Function1 function1;
                            function1 = WidgetSettingsGiftingAdapter.EntitlementListItem.access$getAdapter$p(WidgetSettingsGiftingAdapter.EntitlementListItem.this).onClickCopyListener;
                            function1.invoke(giftItem.getGift().getCode());
                        }
                    });
                    GiftingUtils giftingUtils = GiftingUtils.INSTANCE;
                    CharSequence timeString = giftingUtils.getTimeString(giftItem.getGift().getExpiresDiff(ClockFactory.get().currentTimeMillis()), context);
                    LinkifiedTextView linkifiedTextView = this.binding.i;
                    m.checkNotNullExpressionValue(linkifiedTextView, "binding.giftEntitlementRevoke");
                    b.m(linkifiedTextView, R.string.gift_inventory_expires_in_mobile, new Object[]{timeString}, new WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$4(this, giftItem));
                    TextView textView = this.binding.f2168b;
                    m.checkNotNullExpressionValue(textView, "binding.giftEntitlementCode");
                    textView.setText(giftingUtils.generateGiftUrl(giftItem.getGift().getCode()));
                } else {
                    AppViewFlipper appViewFlipper2 = this.binding.g;
                    m.checkNotNullExpressionValue(appViewFlipper2, "binding.giftEntitlementFlipper");
                    appViewFlipper2.setDisplayedChild(1);
                    this.binding.h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view7) {
                            Function2 function2;
                            function2 = WidgetSettingsGiftingAdapter.EntitlementListItem.access$getAdapter$p(WidgetSettingsGiftingAdapter.EntitlementListItem.this).onGenerateClickListener;
                            Long valueOf = Long.valueOf(giftItem.getEntitlement().getSkuId());
                            SubscriptionPlan subscriptionPlan = giftItem.getEntitlement().getSubscriptionPlan();
                            function2.invoke(valueOf, subscriptionPlan != null ? Long.valueOf(subscriptionPlan.a()) : null);
                        }
                    });
                }
                View view7 = this.binding.f;
                m.checkNotNullExpressionValue(view7, "binding.giftEntitlementDivider");
                view7.setVisibility(giftItem.isLastItem().booleanValue() ^ true ? 0 : 8);
                if (giftItem.isLastItem().booleanValue()) {
                    FrameLayout frameLayout = this.binding.d;
                    View view8 = this.itemView;
                    m.checkNotNullExpressionValue(view8, "itemView");
                    Context context2 = view8.getContext();
                    m.checkNotNullExpressionValue(context2, "itemView.context");
                    frameLayout.setBackgroundResource(DrawableCompat.getThemedDrawableRes$default(context2, (int) R.attr.primary_630_bg_bottom_corners, 0, 2, (Object) null));
                    return;
                }
                FrameLayout frameLayout2 = this.binding.d;
                View view9 = this.itemView;
                m.checkNotNullExpressionValue(view9, "itemView");
                frameLayout2.setBackgroundColor(ColorCompat.getThemedColor(view9.getContext(), (int) R.attr.primary_630));
            }
        }
    }

    /* compiled from: WidgetSettingsGiftingAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$NoGiftsListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NoGiftsListItem extends MGRecyclerViewHolder<WidgetSettingsGiftingAdapter, GiftItem> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public NoGiftsListItem(WidgetSettingsGiftingAdapter widgetSettingsGiftingAdapter) {
            super((int) R.layout.view_no_gifts_list_item, widgetSettingsGiftingAdapter);
            m.checkNotNullParameter(widgetSettingsGiftingAdapter, "adapter");
        }
    }

    /* compiled from: WidgetSettingsGiftingAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$SkuListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;)V", "Lcom/discord/databinding/ViewGiftSkuListItemBinding;", "binding", "Lcom/discord/databinding/ViewGiftSkuListItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SkuListItem extends MGRecyclerViewHolder<WidgetSettingsGiftingAdapter, GiftItem> {
        private final ViewGiftSkuListItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public SkuListItem(WidgetSettingsGiftingAdapter widgetSettingsGiftingAdapter) {
            super((int) R.layout.view_gift_sku_list_item, widgetSettingsGiftingAdapter);
            m.checkNotNullParameter(widgetSettingsGiftingAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.gift_sku_arrow;
            ImageView imageView = (ImageView) view.findViewById(R.id.gift_sku_arrow);
            if (imageView != null) {
                i = R.id.gift_sku_copies;
                TextView textView = (TextView) view.findViewById(R.id.gift_sku_copies);
                if (textView != null) {
                    i = R.id.gift_sku_icon;
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.gift_sku_icon);
                    if (simpleDraweeView != null) {
                        i = R.id.gift_sku_name;
                        TextView textView2 = (TextView) view.findViewById(R.id.gift_sku_name);
                        if (textView2 != null) {
                            ViewGiftSkuListItemBinding viewGiftSkuListItemBinding = new ViewGiftSkuListItemBinding((CardView) view, imageView, textView, simpleDraweeView, textView2);
                            m.checkNotNullExpressionValue(viewGiftSkuListItemBinding, "ViewGiftSkuListItemBinding.bind(itemView)");
                            this.binding = viewGiftSkuListItemBinding;
                            return;
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetSettingsGiftingAdapter access$getAdapter$p(SkuListItem skuListItem) {
            return (WidgetSettingsGiftingAdapter) skuListItem.adapter;
        }

        public void onConfigure(int i, final GiftItem giftItem) {
            CharSequence charSequence;
            String f;
            Pair pair;
            m.checkNotNullParameter(giftItem, "data");
            super.onConfigure(i, (int) giftItem);
            if (giftItem.getSku() != null && giftItem.getCopies() != null && giftItem.getExpanded() != null) {
                TextView textView = this.binding.e;
                m.checkNotNullExpressionValue(textView, "binding.giftSkuName");
                textView.setText(giftItem.getSku().getName());
                String str = null;
                SubscriptionPlanType from = giftItem.getPlanId() != null ? SubscriptionPlanType.Companion.from(giftItem.getPlanId().longValue()) : null;
                TextView textView2 = this.binding.e;
                m.checkNotNullExpressionValue(textView2, "binding.giftSkuName");
                if (from != null) {
                    if (from.getInterval() == SubscriptionInterval.MONTHLY) {
                        pair = new Pair(Integer.valueOf((int) R.string.gift_inventory_subscription_months), Integer.valueOf((int) R.plurals.gift_inventory_subscription_months_intervalCount));
                    } else {
                        pair = new Pair(Integer.valueOf((int) R.string.gift_inventory_subscription_years), Integer.valueOf((int) R.plurals.gift_inventory_subscription_years_intervalCount));
                    }
                    int intValue = ((Number) pair.component1()).intValue();
                    int intValue2 = ((Number) pair.component2()).intValue();
                    View view = this.itemView;
                    m.checkNotNullExpressionValue(view, "itemView");
                    charSequence = b.d(view, intValue, new Object[0], new WidgetSettingsGiftingAdapter$SkuListItem$onConfigure$1(this, giftItem, intValue2, 1));
                } else {
                    charSequence = giftItem.getSku().getName();
                }
                textView2.setText(charSequence);
                TextView textView3 = this.binding.c;
                m.checkNotNullExpressionValue(textView3, "binding.giftSkuCopies");
                textView3.setText(StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.gift_inventory_copies_copies, giftItem.getCopies().intValue(), giftItem.getCopies()));
                this.binding.f2170b.setImageResource(giftItem.getExpanded().booleanValue() ? R.drawable.ic_chevron_down_primary_300_12dp : R.drawable.ic_chevron_right_primary_300_12dp);
                PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
                if (premiumUtils.isNitroSku(giftItem.getSku())) {
                    this.binding.d.setImageResource(premiumUtils.getNitroGiftIcon(giftItem.getSku()));
                } else {
                    Application application = giftItem.getSku().getApplication();
                    if (!(application == null || (f = application.f()) == null)) {
                        str = IconUtils.getApplicationIcon$default(giftItem.getSku().getApplicationId(), f, 0, 4, (Object) null);
                    }
                    this.binding.d.setImageURI(str);
                }
                this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsGiftingAdapter$SkuListItem$onConfigure$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        Function2 function2;
                        function2 = WidgetSettingsGiftingAdapter.SkuListItem.access$getAdapter$p(WidgetSettingsGiftingAdapter.SkuListItem.this).onClickSkuListener;
                        function2.invoke(Long.valueOf(giftItem.getSku().getId()), giftItem.getPlanId());
                    }
                });
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsGiftingAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
    }

    public final void configure(List<GiftItem> list, Function2<? super Long, ? super Long, Unit> function2, Function1<? super String, Unit> function1, Function1<? super ModelGift, Unit> function12, Function2<? super Long, ? super Long, Unit> function22) {
        m.checkNotNullParameter(list, "giftItems");
        m.checkNotNullParameter(function2, "onClickSkuListener");
        m.checkNotNullParameter(function1, "onClickCopyListener");
        m.checkNotNullParameter(function12, "onRevokeClickListener");
        m.checkNotNullParameter(function22, "onGenerateClickListener");
        setData(list);
        this.onClickSkuListener = function2;
        this.onClickCopyListener = function1;
        this.onRevokeClickListener = function12;
        this.onGenerateClickListener = function22;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<WidgetSettingsGiftingAdapter, GiftItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new NoGiftsListItem(this);
        }
        if (i == 1) {
            return new SkuListItem(this);
        }
        if (i == 2) {
            return new EntitlementListItem(this);
        }
        throw invalidViewTypeException(i);
    }

    /* compiled from: WidgetSettingsGiftingAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u001b\b\u0086\b\u0018\u0000 B2\u00020\u0001:\u0001BBu\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u000e\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u0014\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b@\u0010AJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0018\u0010\u0015\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u0017\u0010\rJ\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u0018\u0010\rJ\u0080\u0001\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u00142\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u000bHÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b'\u0010\u0004J\u001a\u0010*\u001a\u00020\u000b2\b\u0010)\u001a\u0004\u0018\u00010(HÖ\u0003¢\u0006\u0004\b*\u0010+R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b-\u0010\u0007R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010.\u001a\u0004\b/\u0010\u0010R\u001b\u0010!\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00100\u001a\u0004\b1\u0010\rR\u001c\u00102\u001a\u00020$8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u0010&R\u001b\u0010 \u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u00100\u001a\u0004\b \u0010\rR!\u0010\u001f\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00105\u001a\u0004\b6\u0010\u0016R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00107\u001a\u0004\b8\u0010\u0012R\u001c\u00109\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b9\u0010:\u001a\u0004\b;\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010:\u001a\u0004\b<\u0010\u0004R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010=\u001a\u0004\b>\u0010\nR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00100\u001a\u0004\b?\u0010\r¨\u0006C"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "component1", "()I", "Lcom/discord/models/domain/ModelGift;", "component2", "()Lcom/discord/models/domain/ModelGift;", "Lcom/discord/models/domain/ModelEntitlement;", "component3", "()Lcom/discord/models/domain/ModelEntitlement;", "", "component4", "()Ljava/lang/Boolean;", "Lcom/discord/models/domain/ModelSku;", "component5", "()Lcom/discord/models/domain/ModelSku;", "component6", "()Ljava/lang/Integer;", "", "Lcom/discord/primitives/PlanId;", "component7", "()Ljava/lang/Long;", "component8", "component9", "typeInt", "gift", "entitlement", "expanded", "sku", "copies", "planId", "isLastItem", "wasCopied", "copy", "(ILcom/discord/models/domain/ModelGift;Lcom/discord/models/domain/ModelEntitlement;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelSku;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGift;", "getGift", "Lcom/discord/models/domain/ModelSku;", "getSku", "Ljava/lang/Boolean;", "getWasCopied", "key", "Ljava/lang/String;", "getKey", "Ljava/lang/Long;", "getPlanId", "Ljava/lang/Integer;", "getCopies", "type", "I", "getType", "getTypeInt", "Lcom/discord/models/domain/ModelEntitlement;", "getEntitlement", "getExpanded", HookHelper.constructorName, "(ILcom/discord/models/domain/ModelGift;Lcom/discord/models/domain/ModelEntitlement;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelSku;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GiftItem implements MGRecyclerDataPayload {
        public static final Companion Companion = new Companion(null);
        public static final int TYPE_GIFT = 2;
        public static final int TYPE_NO_GIFTS = 0;
        public static final int TYPE_SKU = 1;
        private final Integer copies;
        private final ModelEntitlement entitlement;
        private final Boolean expanded;
        private final ModelGift gift;
        private final Boolean isLastItem;
        private final String key;
        private final Long planId;
        private final ModelSku sku;
        private final int type;
        private final int typeInt;
        private final Boolean wasCopied;

        /* compiled from: WidgetSettingsGiftingAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem$Companion;", "", "", "TYPE_GIFT", "I", "TYPE_NO_GIFTS", "TYPE_SKU", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public GiftItem(int i, ModelGift modelGift, ModelEntitlement modelEntitlement, Boolean bool, ModelSku modelSku, Integer num, Long l, Boolean bool2, Boolean bool3) {
            this.typeInt = i;
            this.gift = modelGift;
            this.entitlement = modelEntitlement;
            this.expanded = bool;
            this.sku = modelSku;
            this.copies = num;
            this.planId = l;
            this.isLastItem = bool2;
            this.wasCopied = bool3;
            this.type = i;
            int type = getType();
            String str = "";
            if (type != 0) {
                Long l2 = null;
                if (type == 1) {
                    str = String.valueOf(modelSku != null ? Long.valueOf(modelSku.getId()) : l2);
                } else if (type == 2) {
                    str = String.valueOf(modelEntitlement != null ? Long.valueOf(modelEntitlement.getId()) : l2);
                }
            }
            this.key = str;
        }

        public final int component1() {
            return this.typeInt;
        }

        public final ModelGift component2() {
            return this.gift;
        }

        public final ModelEntitlement component3() {
            return this.entitlement;
        }

        public final Boolean component4() {
            return this.expanded;
        }

        public final ModelSku component5() {
            return this.sku;
        }

        public final Integer component6() {
            return this.copies;
        }

        public final Long component7() {
            return this.planId;
        }

        public final Boolean component8() {
            return this.isLastItem;
        }

        public final Boolean component9() {
            return this.wasCopied;
        }

        public final GiftItem copy(int i, ModelGift modelGift, ModelEntitlement modelEntitlement, Boolean bool, ModelSku modelSku, Integer num, Long l, Boolean bool2, Boolean bool3) {
            return new GiftItem(i, modelGift, modelEntitlement, bool, modelSku, num, l, bool2, bool3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GiftItem)) {
                return false;
            }
            GiftItem giftItem = (GiftItem) obj;
            return this.typeInt == giftItem.typeInt && m.areEqual(this.gift, giftItem.gift) && m.areEqual(this.entitlement, giftItem.entitlement) && m.areEqual(this.expanded, giftItem.expanded) && m.areEqual(this.sku, giftItem.sku) && m.areEqual(this.copies, giftItem.copies) && m.areEqual(this.planId, giftItem.planId) && m.areEqual(this.isLastItem, giftItem.isLastItem) && m.areEqual(this.wasCopied, giftItem.wasCopied);
        }

        public final Integer getCopies() {
            return this.copies;
        }

        public final ModelEntitlement getEntitlement() {
            return this.entitlement;
        }

        public final Boolean getExpanded() {
            return this.expanded;
        }

        public final ModelGift getGift() {
            return this.gift;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final Long getPlanId() {
            return this.planId;
        }

        public final ModelSku getSku() {
            return this.sku;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public final int getTypeInt() {
            return this.typeInt;
        }

        public final Boolean getWasCopied() {
            return this.wasCopied;
        }

        public int hashCode() {
            int i = this.typeInt * 31;
            ModelGift modelGift = this.gift;
            int i2 = 0;
            int hashCode = (i + (modelGift != null ? modelGift.hashCode() : 0)) * 31;
            ModelEntitlement modelEntitlement = this.entitlement;
            int hashCode2 = (hashCode + (modelEntitlement != null ? modelEntitlement.hashCode() : 0)) * 31;
            Boolean bool = this.expanded;
            int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
            ModelSku modelSku = this.sku;
            int hashCode4 = (hashCode3 + (modelSku != null ? modelSku.hashCode() : 0)) * 31;
            Integer num = this.copies;
            int hashCode5 = (hashCode4 + (num != null ? num.hashCode() : 0)) * 31;
            Long l = this.planId;
            int hashCode6 = (hashCode5 + (l != null ? l.hashCode() : 0)) * 31;
            Boolean bool2 = this.isLastItem;
            int hashCode7 = (hashCode6 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Boolean bool3 = this.wasCopied;
            if (bool3 != null) {
                i2 = bool3.hashCode();
            }
            return hashCode7 + i2;
        }

        public final Boolean isLastItem() {
            return this.isLastItem;
        }

        public String toString() {
            StringBuilder R = a.R("GiftItem(typeInt=");
            R.append(this.typeInt);
            R.append(", gift=");
            R.append(this.gift);
            R.append(", entitlement=");
            R.append(this.entitlement);
            R.append(", expanded=");
            R.append(this.expanded);
            R.append(", sku=");
            R.append(this.sku);
            R.append(", copies=");
            R.append(this.copies);
            R.append(", planId=");
            R.append(this.planId);
            R.append(", isLastItem=");
            R.append(this.isLastItem);
            R.append(", wasCopied=");
            return a.C(R, this.wasCopied, ")");
        }

        public /* synthetic */ GiftItem(int i, ModelGift modelGift, ModelEntitlement modelEntitlement, Boolean bool, ModelSku modelSku, Integer num, Long l, Boolean bool2, Boolean bool3, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, (i2 & 2) != 0 ? null : modelGift, (i2 & 4) != 0 ? null : modelEntitlement, (i2 & 8) != 0 ? null : bool, (i2 & 16) != 0 ? null : modelSku, (i2 & 32) != 0 ? null : num, (i2 & 64) != 0 ? null : l, (i2 & 128) != 0 ? null : bool2, (i2 & 256) == 0 ? bool3 : null);
        }
    }
}
