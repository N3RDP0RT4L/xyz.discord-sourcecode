package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.databinding.ViewGiftOutboundPromoListItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.recycler.SimpleItemCallback;
import com.discord.widgets.settings.premium.ClaimStatus;
import com.discord.widgets.settings.premium.SettingsGiftingViewModel;
import com.discord.widgets.settings.premium.WidgetSettingsGiftingOutboundPromosAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetSettingsGiftingOutboundPromosAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00152\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0015\u0016B/\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\f0\u000f\u0012\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\f0\u000f¢\u0006\u0004\b\u0013\u0010\u0014J\u001f\u0010\b\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\r\u001a\u00020\f2\u0006\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\r\u0010\u000eR\"\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\f0\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\"\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\f0\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter;", "Landroidx/recyclerview/widget/ListAdapter;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter$OutboundPromoViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter$OutboundPromoViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter$OutboundPromoViewHolder;I)V", "Lkotlin/Function1;", "onMoreDetailsClick", "Lkotlin/jvm/functions/Function1;", "onButtonClick", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Companion", "OutboundPromoViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGiftingOutboundPromosAdapter extends ListAdapter<SettingsGiftingViewModel.OutboundPromoItem, OutboundPromoViewHolder> {
    public static final Companion Companion = new Companion(null);
    private static final SimpleItemCallback<SettingsGiftingViewModel.OutboundPromoItem> DIFF_CALLBACK = new SimpleItemCallback<>(WidgetSettingsGiftingOutboundPromosAdapter$Companion$DIFF_CALLBACK$1.INSTANCE);
    private final Function1<SettingsGiftingViewModel.OutboundPromoItem, Unit> onButtonClick;
    private final Function1<SettingsGiftingViewModel.OutboundPromoItem, Unit> onMoreDetailsClick;

    /* compiled from: WidgetSettingsGiftingOutboundPromosAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter$Companion;", "", "Lcom/discord/utilities/view/recycler/SimpleItemCallback;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "DIFF_CALLBACK", "Lcom/discord/utilities/view/recycler/SimpleItemCallback;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsGiftingOutboundPromosAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f\u0012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f¢\u0006\u0004\b\u0012\u0010\u0013J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\u00020\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\"\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\"\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter$OutboundPromoViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "item", "", "bindTo", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;)V", "boundItem", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$OutboundPromoItem;", "Lcom/discord/databinding/ViewGiftOutboundPromoListItemBinding;", "binding", "Lcom/discord/databinding/ViewGiftOutboundPromoListItemBinding;", "Lkotlin/Function1;", "onMoreDetailsClick", "Lkotlin/jvm/functions/Function1;", "onButtonClick", "Landroid/view/ViewGroup;", "parent", HookHelper.constructorName, "(Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class OutboundPromoViewHolder extends RecyclerView.ViewHolder {
        private final ViewGiftOutboundPromoListItemBinding binding;
        private SettingsGiftingViewModel.OutboundPromoItem boundItem;
        private final Function1<SettingsGiftingViewModel.OutboundPromoItem, Unit> onButtonClick;
        private final Function1<SettingsGiftingViewModel.OutboundPromoItem, Unit> onMoreDetailsClick;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public OutboundPromoViewHolder(ViewGroup viewGroup, Function1<? super SettingsGiftingViewModel.OutboundPromoItem, Unit> function1, Function1<? super SettingsGiftingViewModel.OutboundPromoItem, Unit> function12) {
            super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_gift_outbound_promo_list_item, viewGroup, false));
            m.checkNotNullParameter(viewGroup, "parent");
            m.checkNotNullParameter(function1, "onMoreDetailsClick");
            m.checkNotNullParameter(function12, "onButtonClick");
            this.onMoreDetailsClick = function1;
            this.onButtonClick = function12;
            View view = this.itemView;
            int i = R.id.giftPromoButton;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.giftPromoButton);
            if (materialButton != null) {
                i = R.id.giftPromoDescription;
                TextView textView = (TextView) view.findViewById(R.id.giftPromoDescription);
                if (textView != null) {
                    i = R.id.giftPromoImage;
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.giftPromoImage);
                    if (simpleDraweeView != null) {
                        i = R.id.giftPromoMoreDetails;
                        TextView textView2 = (TextView) view.findViewById(R.id.giftPromoMoreDetails);
                        if (textView2 != null) {
                            i = R.id.giftPromoTitle;
                            TextView textView3 = (TextView) view.findViewById(R.id.giftPromoTitle);
                            if (textView3 != null) {
                                ViewGiftOutboundPromoListItemBinding viewGiftOutboundPromoListItemBinding = new ViewGiftOutboundPromoListItemBinding((MaterialCardView) view, materialButton, textView, simpleDraweeView, textView2, textView3);
                                textView2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsGiftingOutboundPromosAdapter$OutboundPromoViewHolder$$special$$inlined$also$lambda$1
                                    @Override // android.view.View.OnClickListener
                                    public final void onClick(View view2) {
                                        Function1 function13;
                                        function13 = WidgetSettingsGiftingOutboundPromosAdapter.OutboundPromoViewHolder.this.onMoreDetailsClick;
                                        function13.invoke(WidgetSettingsGiftingOutboundPromosAdapter.OutboundPromoViewHolder.access$getBoundItem$p(WidgetSettingsGiftingOutboundPromosAdapter.OutboundPromoViewHolder.this));
                                    }
                                });
                                materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsGiftingOutboundPromosAdapter$OutboundPromoViewHolder$$special$$inlined$also$lambda$2
                                    @Override // android.view.View.OnClickListener
                                    public final void onClick(View view2) {
                                        Function1 function13;
                                        function13 = WidgetSettingsGiftingOutboundPromosAdapter.OutboundPromoViewHolder.this.onButtonClick;
                                        function13.invoke(WidgetSettingsGiftingOutboundPromosAdapter.OutboundPromoViewHolder.access$getBoundItem$p(WidgetSettingsGiftingOutboundPromosAdapter.OutboundPromoViewHolder.this));
                                    }
                                });
                                m.checkNotNullExpressionValue(viewGiftOutboundPromoListItemBinding, "ViewGiftOutboundPromoLis…nClick(boundItem) }\n    }");
                                this.binding = viewGiftOutboundPromoListItemBinding;
                                return;
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ SettingsGiftingViewModel.OutboundPromoItem access$getBoundItem$p(OutboundPromoViewHolder outboundPromoViewHolder) {
            SettingsGiftingViewModel.OutboundPromoItem outboundPromoItem = outboundPromoViewHolder.boundItem;
            if (outboundPromoItem == null) {
                m.throwUninitializedPropertyAccessException("boundItem");
            }
            return outboundPromoItem;
        }

        public final void bindTo(SettingsGiftingViewModel.OutboundPromoItem outboundPromoItem) {
            m.checkNotNullParameter(outboundPromoItem, "item");
            this.boundItem = outboundPromoItem;
            SimpleDraweeView simpleDraweeView = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.giftPromoImage");
            IconUtils.setIcon$default(simpleDraweeView, outboundPromoItem.getImageUrl(), 0, (Function1) null, new MGImages.DistinctChangeDetector(), 12, (Object) null);
            TextView textView = this.binding.f;
            m.checkNotNullExpressionValue(textView, "binding.giftPromoTitle");
            textView.setText(outboundPromoItem.getTitle());
            ClaimStatus claimStatus = outboundPromoItem.getClaimStatus();
            if (claimStatus instanceof ClaimStatus.Unclaimed) {
                long g = ((ClaimStatus.Unclaimed) outboundPromoItem.getClaimStatus()).getClaimBy().g();
                TimeUtils timeUtils = TimeUtils.INSTANCE;
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "itemView.context");
                String renderUtcDate = timeUtils.renderUtcDate(g, context, 1);
                TextView textView2 = this.binding.c;
                m.checkNotNullExpressionValue(textView2, "binding.giftPromoDescription");
                b.m(textView2, R.string.mobile_outbound_promotion_card_unclaimed_body, new Object[]{renderUtcDate}, (r4 & 4) != 0 ? b.g.j : null);
                MaterialButton materialButton = this.binding.f2169b;
                m.checkNotNullExpressionValue(materialButton, "binding.giftPromoButton");
                b.m(materialButton, R.string.promotion_card_action_claim, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            } else if (claimStatus instanceof ClaimStatus.Claimed) {
                long g2 = ((ClaimStatus.Claimed) outboundPromoItem.getClaimStatus()).getRedeemBy().g();
                TimeUtils timeUtils2 = TimeUtils.INSTANCE;
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                Context context2 = view2.getContext();
                m.checkNotNullExpressionValue(context2, "itemView.context");
                String renderUtcDate2 = timeUtils2.renderUtcDate(g2, context2, 1);
                TextView textView3 = this.binding.c;
                m.checkNotNullExpressionValue(textView3, "binding.giftPromoDescription");
                b.m(textView3, R.string.mobile_outbound_promotion_card_claimed_body, new Object[]{renderUtcDate2}, (r4 & 4) != 0 ? b.g.j : null);
                MaterialButton materialButton2 = this.binding.f2169b;
                m.checkNotNullExpressionValue(materialButton2, "binding.giftPromoButton");
                b.m(materialButton2, R.string.outbound_promotion_see_code, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public WidgetSettingsGiftingOutboundPromosAdapter(Function1<? super SettingsGiftingViewModel.OutboundPromoItem, Unit> function1, Function1<? super SettingsGiftingViewModel.OutboundPromoItem, Unit> function12) {
        super(DIFF_CALLBACK);
        m.checkNotNullParameter(function1, "onMoreDetailsClick");
        m.checkNotNullParameter(function12, "onButtonClick");
        this.onMoreDetailsClick = function1;
        this.onButtonClick = function12;
    }

    public void onBindViewHolder(OutboundPromoViewHolder outboundPromoViewHolder, int i) {
        m.checkNotNullParameter(outboundPromoViewHolder, "holder");
        SettingsGiftingViewModel.OutboundPromoItem item = getItem(i);
        m.checkNotNullExpressionValue(item, "getItem(position)");
        outboundPromoViewHolder.bindTo(item);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public OutboundPromoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        return new OutboundPromoViewHolder(viewGroup, this.onMoreDetailsClick, this.onButtonClick);
    }
}
