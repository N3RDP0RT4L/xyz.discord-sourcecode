package com.discord.widgets.settings.premium;

import com.discord.stores.StoreGifting;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: SettingsGiftingViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState;", "p1", "", "invoke", "(Lcom/discord/stores/StoreGifting$GiftState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class SettingsGiftingViewModel$redeemGiftCode$2 extends k implements Function1<StoreGifting.GiftState, Unit> {
    public SettingsGiftingViewModel$redeemGiftCode$2(SettingsGiftingViewModel settingsGiftingViewModel) {
        super(1, settingsGiftingViewModel, SettingsGiftingViewModel.class, "onHandleGiftCode", "onHandleGiftCode(Lcom/discord/stores/StoreGifting$GiftState;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreGifting.GiftState giftState) {
        invoke2(giftState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreGifting.GiftState giftState) {
        m.checkNotNullParameter(giftState, "p1");
        ((SettingsGiftingViewModel) this.receiver).onHandleGiftCode(giftState);
    }
}
