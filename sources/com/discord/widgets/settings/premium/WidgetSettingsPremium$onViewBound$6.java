package com.discord.widgets.settings.premium;

import com.discord.i18n.RenderContext;
import com.discord.utilities.rest.SendUtils;
import d0.z.d.m;
import d0.z.d.o;
import java.text.NumberFormat;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsPremium.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPremium$onViewBound$6 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ NumberFormat $numberFormat;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPremium$onViewBound$6(NumberFormat numberFormat) {
        super(1);
        this.$numberFormat = numberFormat;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        Map<String, String> map = renderContext.a;
        String format = this.$numberFormat.format((Object) 2000);
        m.checkNotNullExpressionValue(format, "numberFormat.format(Send…_MESSAGE_CHARACTER_COUNT)");
        map.put("nonPremiumMaxMessageLength", format);
        Map<String, String> map2 = renderContext.a;
        String format2 = this.$numberFormat.format(Integer.valueOf((int) SendUtils.MAX_MESSAGE_CHARACTER_COUNT_PREMIUM));
        m.checkNotNullExpressionValue(format2, "numberFormat.format(Send…_CHARACTER_COUNT_PREMIUM)");
        map2.put("premiumMaxMessageLength", format2);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }
}
