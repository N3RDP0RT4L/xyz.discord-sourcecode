package com.discord.widgets.settings.premium;

import com.discord.widgets.settings.premium.ChoosePlanViewModel;
import d0.z.d.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChoosePlan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;", "p1", "", "invoke", "(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetChoosePlan$onViewBoundOrOnResume$2 extends a implements Function1<ChoosePlanViewModel.ViewState, Unit> {
    public WidgetChoosePlan$onViewBoundOrOnResume$2(WidgetChoosePlan widgetChoosePlan) {
        super(1, widgetChoosePlan, WidgetChoosePlan.class, "configureUI", "configureUI(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;)Lkotlin/Unit;", 8);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ChoosePlanViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ChoosePlanViewModel.ViewState viewState) {
        m.checkNotNullParameter(viewState, "p1");
        ((WidgetChoosePlan) this.receiver).configureUI(viewState);
    }
}
