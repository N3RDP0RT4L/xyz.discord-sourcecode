package com.discord.widgets.settings.premium;

import com.discord.app.AppViewModel;
import com.discord.widgets.settings.premium.ClaimOutboundPromoViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetClaimOutboundPromo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/premium/ClaimOutboundPromoViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetClaimOutboundPromo$viewModel$2 extends o implements Function0<AppViewModel<ClaimOutboundPromoViewModel.ViewState>> {
    public final /* synthetic */ WidgetClaimOutboundPromo this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetClaimOutboundPromo$viewModel$2(WidgetClaimOutboundPromo widgetClaimOutboundPromo) {
        super(0);
        this.this$0 = widgetClaimOutboundPromo;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ClaimOutboundPromoViewModel.ViewState> invoke() {
        ClaimStatus claimStatus;
        claimStatus = this.this$0.getClaimStatus();
        return new ClaimOutboundPromoViewModel(claimStatus, null, 2, null);
    }
}
