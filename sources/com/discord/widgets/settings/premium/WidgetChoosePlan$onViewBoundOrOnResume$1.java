package com.discord.widgets.settings.premium;

import com.discord.widgets.settings.premium.ChoosePlanViewModel;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChoosePlan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;", "p1", "", "invoke", "(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetChoosePlan$onViewBoundOrOnResume$1 extends k implements Function1<ChoosePlanViewModel.Event, Unit> {
    public WidgetChoosePlan$onViewBoundOrOnResume$1(WidgetChoosePlan widgetChoosePlan) {
        super(1, widgetChoosePlan, WidgetChoosePlan.class, "handleEvent", "handleEvent(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ChoosePlanViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ChoosePlanViewModel.Event event) {
        m.checkNotNullParameter(event, "p1");
        ((WidgetChoosePlan) this.receiver).handleEvent(event);
    }
}
