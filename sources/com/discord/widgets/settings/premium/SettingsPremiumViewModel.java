package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelEntitlement;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.models.domain.ModelPaymentSource;
import com.discord.models.domain.ModelSubscription;
import com.discord.models.domain.billing.ModelInvoicePreview;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreEntitlements;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.stores.StoreGooglePlaySkuDetails;
import com.discord.stores.StoreGuildBoost;
import com.discord.stores.StorePaymentSources;
import com.discord.stores.StoreSubscriptions;
import com.discord.utilities.billing.GooglePlayBillingManager;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.settings.premium.SettingsPremiumViewModel;
import d0.g0.t;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.a.d;
import j0.l.e.k;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: SettingsPremiumViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 -2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0006-./012BI\u0012\b\b\u0002\u0010$\u001a\u00020#\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u001a\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0017\u0012\b\b\u0002\u0010!\u001a\u00020 \u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d\u0012\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00060\u0011¢\u0006\u0004\b+\u0010,J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u000f\u0010\u0005J\u000f\u0010\u0010\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0010\u0010\u0005J\u0015\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0015\u0010\u0005J\u000f\u0010\u0016\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0016\u0010\u0005R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R:\u0010(\u001a&\u0012\f\u0012\n '*\u0004\u0018\u00010\u00120\u0012 '*\u0012\u0012\f\u0012\n '*\u0004\u0018\u00010\u00120\u0012\u0018\u00010&0&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)¨\u00063"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;", "", "onCancelError", "()V", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;)V", "Lcom/discord/models/domain/ModelSubscription;", "sub", "", "getPastDueGracePeriodDays", "(Lcom/discord/models/domain/ModelSubscription;)I", "markBusy", "fetchData", "Lrx/Observable;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;", "getEventSubject", "()Lrx/Observable;", "onRetryClicked", "cancelSubscription", "Lcom/discord/stores/StoreEntitlements;", "storeEntitlements", "Lcom/discord/stores/StoreEntitlements;", "Lcom/discord/stores/StoreSubscriptions;", "storeSubscriptions", "Lcom/discord/stores/StoreSubscriptions;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreGuildBoost;", "storeGuildBoost", "Lcom/discord/stores/StoreGuildBoost;", "Lcom/discord/stores/StorePaymentSources;", "storePaymentsSources", "Lcom/discord/stores/StorePaymentSources;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StoreGuildBoost;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V", "Companion", "Event", "InvoicePreviewFetch", "StoreState", "SubscriptionAndInvoice", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsPremiumViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final RestAPI restAPI;
    private final StoreEntitlements storeEntitlements;
    private final StoreGuildBoost storeGuildBoost;
    private final StorePaymentSources storePaymentsSources;
    private final StoreSubscriptions storeSubscriptions;

    /* compiled from: SettingsPremiumViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.SettingsPremiumViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            SettingsPremiumViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: SettingsPremiumViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJU\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J-\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00122\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Companion;", "", "Lcom/discord/stores/StorePaymentSources;", "storePaymentsSources", "Lcom/discord/stores/StoreSubscriptions;", "storeSubscriptions", "Lcom/discord/stores/StoreEntitlements;", "storeEntitlements", "Lcom/discord/stores/StoreGuildBoost;", "storeGuildBoost", "Lcom/discord/stores/StoreGooglePlaySkuDetails;", "storeGooglePlaySkuDetails", "Lcom/discord/stores/StoreGooglePlayPurchases;", "storeGooglePlayPurchases", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lrx/Observable;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;", "observeStores", "(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StoreGuildBoost;Lcom/discord/stores/StoreGooglePlaySkuDetails;Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/rest/RestAPI;)Lrx/Observable;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "state", "", "applyEntitlements", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;", "getSubscriptionsAndInvoicePreview", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Z)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<SubscriptionAndInvoice> getSubscriptionsAndInvoicePreview(RestAPI restAPI, final StoreSubscriptions.SubscriptionsState subscriptionsState, boolean z2) {
            Object obj;
            String id2;
            boolean z3;
            if (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Loaded) {
                Iterator<T> it = ((StoreSubscriptions.SubscriptionsState.Loaded) subscriptionsState).getSubscriptions().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (((ModelSubscription) obj).getType() == ModelSubscription.Type.PREMIUM) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        break;
                    }
                }
                ModelSubscription modelSubscription = (ModelSubscription) obj;
                if (modelSubscription == null || (id2 = modelSubscription.getId()) == null) {
                    k kVar = new k(new SubscriptionAndInvoice(subscriptionsState, new InvoicePreviewFetch.Invoice(null)));
                    m.checkNotNullExpressionValue(kVar, "Observable.just(\n       …nvoice(null))\n          )");
                    return kVar;
                }
                Observable<SubscriptionAndInvoice> L = ObservableExtensionsKt.restSubscribeOn$default(restAPI.getInvoicePreview(new RestAPIParams.InvoicePreviewBody(id2, true, z2 && !modelSubscription.isGoogleSubscription())), false, 1, null).F(new b<ModelInvoicePreview, SubscriptionAndInvoice>() { // from class: com.discord.widgets.settings.premium.SettingsPremiumViewModel$Companion$getSubscriptionsAndInvoicePreview$1
                    public final SettingsPremiumViewModel.SubscriptionAndInvoice call(ModelInvoicePreview modelInvoicePreview) {
                        return new SettingsPremiumViewModel.SubscriptionAndInvoice(StoreSubscriptions.SubscriptionsState.this, new SettingsPremiumViewModel.InvoicePreviewFetch.Invoice(modelInvoicePreview));
                    }
                }).L(new b<Throwable, SubscriptionAndInvoice>() { // from class: com.discord.widgets.settings.premium.SettingsPremiumViewModel$Companion$getSubscriptionsAndInvoicePreview$2
                    public final SettingsPremiumViewModel.SubscriptionAndInvoice call(Throwable th) {
                        return new SettingsPremiumViewModel.SubscriptionAndInvoice(StoreSubscriptions.SubscriptionsState.this, SettingsPremiumViewModel.InvoicePreviewFetch.Error.INSTANCE);
                    }
                });
                m.checkNotNullExpressionValue(L, "restAPI\n              .g…ch.Error)\n              }");
                return L;
            }
            Observable observable = d.k;
            m.checkNotNullExpressionValue(observable, "Observable.never()");
            return observable;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(StorePaymentSources storePaymentSources, StoreSubscriptions storeSubscriptions, StoreEntitlements storeEntitlements, StoreGuildBoost storeGuildBoost, StoreGooglePlaySkuDetails storeGooglePlaySkuDetails, StoreGooglePlayPurchases storeGooglePlayPurchases, StoreExperiments storeExperiments, final RestAPI restAPI) {
            Observable<StoreState> e = Observable.e(storePaymentSources.observePaymentSourcesState(), storeSubscriptions.observeSubscriptions().Y(new b<StoreSubscriptions.SubscriptionsState, Observable<? extends SubscriptionAndInvoice>>() { // from class: com.discord.widgets.settings.premium.SettingsPremiumViewModel$Companion$observeStores$1
                public final Observable<? extends SettingsPremiumViewModel.SubscriptionAndInvoice> call(StoreSubscriptions.SubscriptionsState subscriptionsState) {
                    Observable<? extends SettingsPremiumViewModel.SubscriptionAndInvoice> subscriptionsAndInvoicePreview;
                    SettingsPremiumViewModel.Companion companion = SettingsPremiumViewModel.Companion;
                    RestAPI restAPI2 = RestAPI.this;
                    m.checkNotNullExpressionValue(subscriptionsState, "state");
                    subscriptionsAndInvoicePreview = companion.getSubscriptionsAndInvoicePreview(restAPI2, subscriptionsState, true);
                    return subscriptionsAndInvoicePreview;
                }
            }), storeSubscriptions.observeSubscriptions().Y(new b<StoreSubscriptions.SubscriptionsState, Observable<? extends SubscriptionAndInvoice>>() { // from class: com.discord.widgets.settings.premium.SettingsPremiumViewModel$Companion$observeStores$2
                public final Observable<? extends SettingsPremiumViewModel.SubscriptionAndInvoice> call(StoreSubscriptions.SubscriptionsState subscriptionsState) {
                    Observable<? extends SettingsPremiumViewModel.SubscriptionAndInvoice> subscriptionsAndInvoicePreview;
                    SettingsPremiumViewModel.Companion companion = SettingsPremiumViewModel.Companion;
                    RestAPI restAPI2 = RestAPI.this;
                    m.checkNotNullExpressionValue(subscriptionsState, "state");
                    subscriptionsAndInvoicePreview = companion.getSubscriptionsAndInvoicePreview(restAPI2, subscriptionsState, false);
                    return subscriptionsAndInvoicePreview;
                }
            }), storeEntitlements.observeEntitlementState(), StoreGuildBoost.observeGuildBoostState$default(storeGuildBoost, null, 1, null), storeGooglePlaySkuDetails.observeState(), storeGooglePlayPurchases.observeState(), SettingsPremiumViewModel$Companion$observeStores$3.INSTANCE);
            m.checkNotNullExpressionValue(e, "Observable\n          .co…            )\n          }");
            return e;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsPremiumViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;", "", HookHelper.constructorName, "()V", "ErrorToast", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: SettingsPremiumViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;", "", "component1", "()I", "errorStringResId", "copy", "(I)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getErrorStringResId", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ErrorToast extends Event {
            private final int errorStringResId;

            public ErrorToast(@StringRes int i) {
                super(null);
                this.errorStringResId = i;
            }

            public static /* synthetic */ ErrorToast copy$default(ErrorToast errorToast, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = errorToast.errorStringResId;
                }
                return errorToast.copy(i);
            }

            public final int component1() {
                return this.errorStringResId;
            }

            public final ErrorToast copy(@StringRes int i) {
                return new ErrorToast(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ErrorToast) && this.errorStringResId == ((ErrorToast) obj).errorStringResId;
                }
                return true;
            }

            public final int getErrorStringResId() {
                return this.errorStringResId;
            }

            public int hashCode() {
                return this.errorStringResId;
            }

            public String toString() {
                return a.A(a.R("ErrorToast(errorStringResId="), this.errorStringResId, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsPremiumViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "", HookHelper.constructorName, "()V", "Error", "Invoice", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class InvoicePreviewFetch {

        /* compiled from: SettingsPremiumViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Error;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends InvoicePreviewFetch {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: SettingsPremiumViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "Lcom/discord/models/domain/billing/ModelInvoicePreview;", "component1", "()Lcom/discord/models/domain/billing/ModelInvoicePreview;", "modelInvoicePreview", "copy", "(Lcom/discord/models/domain/billing/ModelInvoicePreview;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/billing/ModelInvoicePreview;", "getModelInvoicePreview", HookHelper.constructorName, "(Lcom/discord/models/domain/billing/ModelInvoicePreview;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invoice extends InvoicePreviewFetch {
            private final ModelInvoicePreview modelInvoicePreview;

            public Invoice(ModelInvoicePreview modelInvoicePreview) {
                super(null);
                this.modelInvoicePreview = modelInvoicePreview;
            }

            public static /* synthetic */ Invoice copy$default(Invoice invoice, ModelInvoicePreview modelInvoicePreview, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelInvoicePreview = invoice.modelInvoicePreview;
                }
                return invoice.copy(modelInvoicePreview);
            }

            public final ModelInvoicePreview component1() {
                return this.modelInvoicePreview;
            }

            public final Invoice copy(ModelInvoicePreview modelInvoicePreview) {
                return new Invoice(modelInvoicePreview);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Invoice) && m.areEqual(this.modelInvoicePreview, ((Invoice) obj).modelInvoicePreview);
                }
                return true;
            }

            public final ModelInvoicePreview getModelInvoicePreview() {
                return this.modelInvoicePreview;
            }

            public int hashCode() {
                ModelInvoicePreview modelInvoicePreview = this.modelInvoicePreview;
                if (modelInvoicePreview != null) {
                    return modelInvoicePreview.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Invoice(modelInvoicePreview=");
                R.append(this.modelInvoicePreview);
                R.append(")");
                return R.toString();
            }
        }

        private InvoicePreviewFetch() {
        }

        public /* synthetic */ InvoicePreviewFetch(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsPremiumViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001BG\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0006\u0010\u0019\u001a\u00020\u0005\u0012\u0006\u0010\u001a\u001a\u00020\b\u0012\u0006\u0010\u001b\u001a\u00020\u000b\u0012\u0006\u0010\u001c\u001a\u00020\u000e\u0012\u0006\u0010\u001d\u001a\u00020\u000e\u0012\u0006\u0010\u001e\u001a\u00020\u0012\u0012\u0006\u0010\u001f\u001a\u00020\u0015¢\u0006\u0004\b;\u0010<J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J`\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00022\b\b\u0002\u0010\u0019\u001a\u00020\u00052\b\b\u0002\u0010\u001a\u001a\u00020\b2\b\b\u0002\u0010\u001b\u001a\u00020\u000b2\b\b\u0002\u0010\u001c\u001a\u00020\u000e2\b\b\u0002\u0010\u001d\u001a\u00020\u000e2\b\b\u0002\u0010\u001e\u001a\u00020\u00122\b\b\u0002\u0010\u001f\u001a\u00020\u0015HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u001a\u0010*\u001a\u00020)2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b*\u0010+R\u0019\u0010\u001c\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010,\u001a\u0004\b-\u0010\u0010R\u0019\u0010\u001d\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010,\u001a\u0004\b.\u0010\u0010R\u0019\u0010\u001a\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010/\u001a\u0004\b0\u0010\nR\u0019\u0010\u0019\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00101\u001a\u0004\b2\u0010\u0007R\u0019\u0010\u001b\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00103\u001a\u0004\b4\u0010\rR\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00105\u001a\u0004\b6\u0010\u0004R\u0019\u0010\u001f\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00107\u001a\u0004\b8\u0010\u0017R\u0019\u0010\u001e\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00109\u001a\u0004\b:\u0010\u0014¨\u0006="}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;", "", "Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;", "component1", "()Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "component2", "()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "Lcom/discord/stores/StoreEntitlements$State;", "component3", "()Lcom/discord/stores/StoreEntitlements$State;", "Lcom/discord/stores/StoreGuildBoost$State;", "component4", "()Lcom/discord/stores/StoreGuildBoost$State;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "component5", "()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "component6", "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "component7", "()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "component8", "()Lcom/discord/stores/StoreGooglePlayPurchases$State;", "paymentSourcesState", "subscriptionsState", "entitlementState", "guildBoostState", "renewalInvoicePreviewFetch", "currentInvoicePreviewFetch", "skuDetailsState", "purchaseState", "copy", "(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StoreGuildBoost$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "getRenewalInvoicePreviewFetch", "getCurrentInvoicePreviewFetch", "Lcom/discord/stores/StoreEntitlements$State;", "getEntitlementState", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "getSubscriptionsState", "Lcom/discord/stores/StoreGuildBoost$State;", "getGuildBoostState", "Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;", "getPaymentSourcesState", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "getPurchaseState", "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "getSkuDetailsState", HookHelper.constructorName, "(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StoreGuildBoost$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final InvoicePreviewFetch currentInvoicePreviewFetch;
        private final StoreEntitlements.State entitlementState;
        private final StoreGuildBoost.State guildBoostState;
        private final StorePaymentSources.PaymentSourcesState paymentSourcesState;
        private final StoreGooglePlayPurchases.State purchaseState;
        private final InvoicePreviewFetch renewalInvoicePreviewFetch;
        private final StoreGooglePlaySkuDetails.State skuDetailsState;
        private final StoreSubscriptions.SubscriptionsState subscriptionsState;

        public StoreState(StorePaymentSources.PaymentSourcesState paymentSourcesState, StoreSubscriptions.SubscriptionsState subscriptionsState, StoreEntitlements.State state, StoreGuildBoost.State state2, InvoicePreviewFetch invoicePreviewFetch, InvoicePreviewFetch invoicePreviewFetch2, StoreGooglePlaySkuDetails.State state3, StoreGooglePlayPurchases.State state4) {
            m.checkNotNullParameter(paymentSourcesState, "paymentSourcesState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            m.checkNotNullParameter(state, "entitlementState");
            m.checkNotNullParameter(state2, "guildBoostState");
            m.checkNotNullParameter(invoicePreviewFetch, "renewalInvoicePreviewFetch");
            m.checkNotNullParameter(invoicePreviewFetch2, "currentInvoicePreviewFetch");
            m.checkNotNullParameter(state3, "skuDetailsState");
            m.checkNotNullParameter(state4, "purchaseState");
            this.paymentSourcesState = paymentSourcesState;
            this.subscriptionsState = subscriptionsState;
            this.entitlementState = state;
            this.guildBoostState = state2;
            this.renewalInvoicePreviewFetch = invoicePreviewFetch;
            this.currentInvoicePreviewFetch = invoicePreviewFetch2;
            this.skuDetailsState = state3;
            this.purchaseState = state4;
        }

        public final StorePaymentSources.PaymentSourcesState component1() {
            return this.paymentSourcesState;
        }

        public final StoreSubscriptions.SubscriptionsState component2() {
            return this.subscriptionsState;
        }

        public final StoreEntitlements.State component3() {
            return this.entitlementState;
        }

        public final StoreGuildBoost.State component4() {
            return this.guildBoostState;
        }

        public final InvoicePreviewFetch component5() {
            return this.renewalInvoicePreviewFetch;
        }

        public final InvoicePreviewFetch component6() {
            return this.currentInvoicePreviewFetch;
        }

        public final StoreGooglePlaySkuDetails.State component7() {
            return this.skuDetailsState;
        }

        public final StoreGooglePlayPurchases.State component8() {
            return this.purchaseState;
        }

        public final StoreState copy(StorePaymentSources.PaymentSourcesState paymentSourcesState, StoreSubscriptions.SubscriptionsState subscriptionsState, StoreEntitlements.State state, StoreGuildBoost.State state2, InvoicePreviewFetch invoicePreviewFetch, InvoicePreviewFetch invoicePreviewFetch2, StoreGooglePlaySkuDetails.State state3, StoreGooglePlayPurchases.State state4) {
            m.checkNotNullParameter(paymentSourcesState, "paymentSourcesState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            m.checkNotNullParameter(state, "entitlementState");
            m.checkNotNullParameter(state2, "guildBoostState");
            m.checkNotNullParameter(invoicePreviewFetch, "renewalInvoicePreviewFetch");
            m.checkNotNullParameter(invoicePreviewFetch2, "currentInvoicePreviewFetch");
            m.checkNotNullParameter(state3, "skuDetailsState");
            m.checkNotNullParameter(state4, "purchaseState");
            return new StoreState(paymentSourcesState, subscriptionsState, state, state2, invoicePreviewFetch, invoicePreviewFetch2, state3, state4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.paymentSourcesState, storeState.paymentSourcesState) && m.areEqual(this.subscriptionsState, storeState.subscriptionsState) && m.areEqual(this.entitlementState, storeState.entitlementState) && m.areEqual(this.guildBoostState, storeState.guildBoostState) && m.areEqual(this.renewalInvoicePreviewFetch, storeState.renewalInvoicePreviewFetch) && m.areEqual(this.currentInvoicePreviewFetch, storeState.currentInvoicePreviewFetch) && m.areEqual(this.skuDetailsState, storeState.skuDetailsState) && m.areEqual(this.purchaseState, storeState.purchaseState);
        }

        public final InvoicePreviewFetch getCurrentInvoicePreviewFetch() {
            return this.currentInvoicePreviewFetch;
        }

        public final StoreEntitlements.State getEntitlementState() {
            return this.entitlementState;
        }

        public final StoreGuildBoost.State getGuildBoostState() {
            return this.guildBoostState;
        }

        public final StorePaymentSources.PaymentSourcesState getPaymentSourcesState() {
            return this.paymentSourcesState;
        }

        public final StoreGooglePlayPurchases.State getPurchaseState() {
            return this.purchaseState;
        }

        public final InvoicePreviewFetch getRenewalInvoicePreviewFetch() {
            return this.renewalInvoicePreviewFetch;
        }

        public final StoreGooglePlaySkuDetails.State getSkuDetailsState() {
            return this.skuDetailsState;
        }

        public final StoreSubscriptions.SubscriptionsState getSubscriptionsState() {
            return this.subscriptionsState;
        }

        public int hashCode() {
            StorePaymentSources.PaymentSourcesState paymentSourcesState = this.paymentSourcesState;
            int i = 0;
            int hashCode = (paymentSourcesState != null ? paymentSourcesState.hashCode() : 0) * 31;
            StoreSubscriptions.SubscriptionsState subscriptionsState = this.subscriptionsState;
            int hashCode2 = (hashCode + (subscriptionsState != null ? subscriptionsState.hashCode() : 0)) * 31;
            StoreEntitlements.State state = this.entitlementState;
            int hashCode3 = (hashCode2 + (state != null ? state.hashCode() : 0)) * 31;
            StoreGuildBoost.State state2 = this.guildBoostState;
            int hashCode4 = (hashCode3 + (state2 != null ? state2.hashCode() : 0)) * 31;
            InvoicePreviewFetch invoicePreviewFetch = this.renewalInvoicePreviewFetch;
            int hashCode5 = (hashCode4 + (invoicePreviewFetch != null ? invoicePreviewFetch.hashCode() : 0)) * 31;
            InvoicePreviewFetch invoicePreviewFetch2 = this.currentInvoicePreviewFetch;
            int hashCode6 = (hashCode5 + (invoicePreviewFetch2 != null ? invoicePreviewFetch2.hashCode() : 0)) * 31;
            StoreGooglePlaySkuDetails.State state3 = this.skuDetailsState;
            int hashCode7 = (hashCode6 + (state3 != null ? state3.hashCode() : 0)) * 31;
            StoreGooglePlayPurchases.State state4 = this.purchaseState;
            if (state4 != null) {
                i = state4.hashCode();
            }
            return hashCode7 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(paymentSourcesState=");
            R.append(this.paymentSourcesState);
            R.append(", subscriptionsState=");
            R.append(this.subscriptionsState);
            R.append(", entitlementState=");
            R.append(this.entitlementState);
            R.append(", guildBoostState=");
            R.append(this.guildBoostState);
            R.append(", renewalInvoicePreviewFetch=");
            R.append(this.renewalInvoicePreviewFetch);
            R.append(", currentInvoicePreviewFetch=");
            R.append(this.currentInvoicePreviewFetch);
            R.append(", skuDetailsState=");
            R.append(this.skuDetailsState);
            R.append(", purchaseState=");
            R.append(this.purchaseState);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: SettingsPremiumViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;", "", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "component1", "()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "component2", "()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "subscriptionsState", "invoicePreviewFetch", "copy", "(Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "getSubscriptionsState", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;", "getInvoicePreviewFetch", HookHelper.constructorName, "(Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SubscriptionAndInvoice {
        private final InvoicePreviewFetch invoicePreviewFetch;
        private final StoreSubscriptions.SubscriptionsState subscriptionsState;

        public SubscriptionAndInvoice(StoreSubscriptions.SubscriptionsState subscriptionsState, InvoicePreviewFetch invoicePreviewFetch) {
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            m.checkNotNullParameter(invoicePreviewFetch, "invoicePreviewFetch");
            this.subscriptionsState = subscriptionsState;
            this.invoicePreviewFetch = invoicePreviewFetch;
        }

        public static /* synthetic */ SubscriptionAndInvoice copy$default(SubscriptionAndInvoice subscriptionAndInvoice, StoreSubscriptions.SubscriptionsState subscriptionsState, InvoicePreviewFetch invoicePreviewFetch, int i, Object obj) {
            if ((i & 1) != 0) {
                subscriptionsState = subscriptionAndInvoice.subscriptionsState;
            }
            if ((i & 2) != 0) {
                invoicePreviewFetch = subscriptionAndInvoice.invoicePreviewFetch;
            }
            return subscriptionAndInvoice.copy(subscriptionsState, invoicePreviewFetch);
        }

        public final StoreSubscriptions.SubscriptionsState component1() {
            return this.subscriptionsState;
        }

        public final InvoicePreviewFetch component2() {
            return this.invoicePreviewFetch;
        }

        public final SubscriptionAndInvoice copy(StoreSubscriptions.SubscriptionsState subscriptionsState, InvoicePreviewFetch invoicePreviewFetch) {
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            m.checkNotNullParameter(invoicePreviewFetch, "invoicePreviewFetch");
            return new SubscriptionAndInvoice(subscriptionsState, invoicePreviewFetch);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SubscriptionAndInvoice)) {
                return false;
            }
            SubscriptionAndInvoice subscriptionAndInvoice = (SubscriptionAndInvoice) obj;
            return m.areEqual(this.subscriptionsState, subscriptionAndInvoice.subscriptionsState) && m.areEqual(this.invoicePreviewFetch, subscriptionAndInvoice.invoicePreviewFetch);
        }

        public final InvoicePreviewFetch getInvoicePreviewFetch() {
            return this.invoicePreviewFetch;
        }

        public final StoreSubscriptions.SubscriptionsState getSubscriptionsState() {
            return this.subscriptionsState;
        }

        public int hashCode() {
            StoreSubscriptions.SubscriptionsState subscriptionsState = this.subscriptionsState;
            int i = 0;
            int hashCode = (subscriptionsState != null ? subscriptionsState.hashCode() : 0) * 31;
            InvoicePreviewFetch invoicePreviewFetch = this.invoicePreviewFetch;
            if (invoicePreviewFetch != null) {
                i = invoicePreviewFetch.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("SubscriptionAndInvoice(subscriptionsState=");
            R.append(this.subscriptionsState);
            R.append(", invoicePreviewFetch=");
            R.append(this.invoicePreviewFetch);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: SettingsPremiumViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Failure", "Loaded", "Loading", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Failure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: SettingsPremiumViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Failure;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failure extends ViewState {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: SettingsPremiumViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001B\u0093\u0001\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010#\u001a\u00020\t\u0012\f\u0010$\u001a\b\u0012\u0004\u0012\u00020\f0\u0005\u0012\u0016\u0010%\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000e\u0012\u0006\u0010&\u001a\u00020\t\u0012\b\u0010'\u001a\u0004\u0018\u00010\u0015\u0012\b\u0010(\u001a\u0004\u0018\u00010\u0015\u0012\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u000e\u0012\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0005\u0012\u0006\u0010+\u001a\u00020\u001e¢\u0006\u0004\bE\u0010FJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u0005HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ \u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000eHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u000bJ\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0017J\u001c\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u000eHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0013J\u0016\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0005HÆ\u0003¢\u0006\u0004\b\u001d\u0010\bJ\u0010\u0010\u001f\u001a\u00020\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J²\u0001\u0010,\u001a\u00020\u00002\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010#\u001a\u00020\t2\u000e\b\u0002\u0010$\u001a\b\u0012\u0004\u0012\u00020\f0\u00052\u0018\b\u0002\u0010%\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000e2\b\b\u0002\u0010&\u001a\u00020\t2\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u00152\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00152\u0014\b\u0002\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u000e2\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00052\b\b\u0002\u0010+\u001a\u00020\u001eHÆ\u0001¢\u0006\u0004\b,\u0010-J\u0010\u0010.\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b.\u0010/J\u0010\u00100\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b0\u0010 J\u001a\u00103\u001a\u00020\t2\b\u00102\u001a\u0004\u0018\u000101HÖ\u0003¢\u0006\u0004\b3\u00104R\u001b\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u00105\u001a\u0004\b6\u0010\u0004R\u001f\u0010$\u001a\b\u0012\u0004\u0012\u00020\f0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b$\u00107\u001a\u0004\b8\u0010\bR\u0019\u0010&\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00109\u001a\u0004\b:\u0010\u000bR%\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010;\u001a\u0004\b<\u0010\u0013R\u001f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00107\u001a\u0004\b=\u0010\bR\u0019\u0010#\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00109\u001a\u0004\b#\u0010\u000bR)\u0010%\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010;\u001a\u0004\b>\u0010\u0013R\u001b\u0010(\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010?\u001a\u0004\b@\u0010\u0017R\u001b\u0010'\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010?\u001a\u0004\bA\u0010\u0017R\u001f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b*\u00107\u001a\u0004\bB\u0010\bR\u0019\u0010+\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010C\u001a\u0004\bD\u0010 ¨\u0006G"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;", "Lcom/discord/models/domain/ModelSubscription;", "component1", "()Lcom/discord/models/domain/ModelSubscription;", "", "Lcom/discord/models/domain/ModelPaymentSource;", "component2", "()Ljava/util/List;", "", "component3", "()Z", "Lcom/discord/models/domain/ModelEntitlement;", "component4", "", "", "Lcom/discord/primitives/GuildBoostSlotId;", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "component5", "()Ljava/util/Map;", "component6", "Lcom/discord/models/domain/billing/ModelInvoicePreview;", "component7", "()Lcom/discord/models/domain/billing/ModelInvoicePreview;", "component8", "", "Lcom/android/billingclient/api/SkuDetails;", "component9", "Lcom/android/billingclient/api/Purchase;", "component10", "", "component11", "()I", "premiumSubscription", "paymentSources", "isBusy", "entitlements", "guildSubscriptions", "hasAnyGuildBoosts", "renewalInvoicePreview", "currentInvoicePreview", "skuDetails", "purchases", "pastDueGracePeriodDays", "copy", "(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;I)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelSubscription;", "getPremiumSubscription", "Ljava/util/List;", "getEntitlements", "Z", "getHasAnyGuildBoosts", "Ljava/util/Map;", "getSkuDetails", "getPaymentSources", "getGuildSubscriptions", "Lcom/discord/models/domain/billing/ModelInvoicePreview;", "getCurrentInvoicePreview", "getRenewalInvoicePreview", "getPurchases", "I", "getPastDueGracePeriodDays", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final ModelInvoicePreview currentInvoicePreview;
            private final List<ModelEntitlement> entitlements;
            private final Map<Long, ModelGuildBoostSlot> guildSubscriptions;
            private final boolean hasAnyGuildBoosts;
            private final boolean isBusy;
            private final int pastDueGracePeriodDays;
            private final List<ModelPaymentSource> paymentSources;
            private final ModelSubscription premiumSubscription;
            private final List<Purchase> purchases;
            private final ModelInvoicePreview renewalInvoicePreview;
            private final Map<String, SkuDetails> skuDetails;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(ModelSubscription modelSubscription, List<? extends ModelPaymentSource> list, boolean z2, List<ModelEntitlement> list2, Map<Long, ModelGuildBoostSlot> map, boolean z3, ModelInvoicePreview modelInvoicePreview, ModelInvoicePreview modelInvoicePreview2, Map<String, ? extends SkuDetails> map2, List<? extends Purchase> list3, int i) {
                super(null);
                m.checkNotNullParameter(list, "paymentSources");
                m.checkNotNullParameter(list2, "entitlements");
                m.checkNotNullParameter(map, "guildSubscriptions");
                m.checkNotNullParameter(map2, "skuDetails");
                m.checkNotNullParameter(list3, "purchases");
                this.premiumSubscription = modelSubscription;
                this.paymentSources = list;
                this.isBusy = z2;
                this.entitlements = list2;
                this.guildSubscriptions = map;
                this.hasAnyGuildBoosts = z3;
                this.renewalInvoicePreview = modelInvoicePreview;
                this.currentInvoicePreview = modelInvoicePreview2;
                this.skuDetails = map2;
                this.purchases = list3;
                this.pastDueGracePeriodDays = i;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, ModelSubscription modelSubscription, List list, boolean z2, List list2, Map map, boolean z3, ModelInvoicePreview modelInvoicePreview, ModelInvoicePreview modelInvoicePreview2, Map map2, List list3, int i, int i2, Object obj) {
                return loaded.copy((i2 & 1) != 0 ? loaded.premiumSubscription : modelSubscription, (i2 & 2) != 0 ? loaded.paymentSources : list, (i2 & 4) != 0 ? loaded.isBusy : z2, (i2 & 8) != 0 ? loaded.entitlements : list2, (i2 & 16) != 0 ? loaded.guildSubscriptions : map, (i2 & 32) != 0 ? loaded.hasAnyGuildBoosts : z3, (i2 & 64) != 0 ? loaded.renewalInvoicePreview : modelInvoicePreview, (i2 & 128) != 0 ? loaded.currentInvoicePreview : modelInvoicePreview2, (i2 & 256) != 0 ? loaded.skuDetails : map2, (i2 & 512) != 0 ? loaded.purchases : list3, (i2 & 1024) != 0 ? loaded.pastDueGracePeriodDays : i);
            }

            public final ModelSubscription component1() {
                return this.premiumSubscription;
            }

            public final List<Purchase> component10() {
                return this.purchases;
            }

            public final int component11() {
                return this.pastDueGracePeriodDays;
            }

            public final List<ModelPaymentSource> component2() {
                return this.paymentSources;
            }

            public final boolean component3() {
                return this.isBusy;
            }

            public final List<ModelEntitlement> component4() {
                return this.entitlements;
            }

            public final Map<Long, ModelGuildBoostSlot> component5() {
                return this.guildSubscriptions;
            }

            public final boolean component6() {
                return this.hasAnyGuildBoosts;
            }

            public final ModelInvoicePreview component7() {
                return this.renewalInvoicePreview;
            }

            public final ModelInvoicePreview component8() {
                return this.currentInvoicePreview;
            }

            public final Map<String, SkuDetails> component9() {
                return this.skuDetails;
            }

            public final Loaded copy(ModelSubscription modelSubscription, List<? extends ModelPaymentSource> list, boolean z2, List<ModelEntitlement> list2, Map<Long, ModelGuildBoostSlot> map, boolean z3, ModelInvoicePreview modelInvoicePreview, ModelInvoicePreview modelInvoicePreview2, Map<String, ? extends SkuDetails> map2, List<? extends Purchase> list3, int i) {
                m.checkNotNullParameter(list, "paymentSources");
                m.checkNotNullParameter(list2, "entitlements");
                m.checkNotNullParameter(map, "guildSubscriptions");
                m.checkNotNullParameter(map2, "skuDetails");
                m.checkNotNullParameter(list3, "purchases");
                return new Loaded(modelSubscription, list, z2, list2, map, z3, modelInvoicePreview, modelInvoicePreview2, map2, list3, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.premiumSubscription, loaded.premiumSubscription) && m.areEqual(this.paymentSources, loaded.paymentSources) && this.isBusy == loaded.isBusy && m.areEqual(this.entitlements, loaded.entitlements) && m.areEqual(this.guildSubscriptions, loaded.guildSubscriptions) && this.hasAnyGuildBoosts == loaded.hasAnyGuildBoosts && m.areEqual(this.renewalInvoicePreview, loaded.renewalInvoicePreview) && m.areEqual(this.currentInvoicePreview, loaded.currentInvoicePreview) && m.areEqual(this.skuDetails, loaded.skuDetails) && m.areEqual(this.purchases, loaded.purchases) && this.pastDueGracePeriodDays == loaded.pastDueGracePeriodDays;
            }

            public final ModelInvoicePreview getCurrentInvoicePreview() {
                return this.currentInvoicePreview;
            }

            public final List<ModelEntitlement> getEntitlements() {
                return this.entitlements;
            }

            public final Map<Long, ModelGuildBoostSlot> getGuildSubscriptions() {
                return this.guildSubscriptions;
            }

            public final boolean getHasAnyGuildBoosts() {
                return this.hasAnyGuildBoosts;
            }

            public final int getPastDueGracePeriodDays() {
                return this.pastDueGracePeriodDays;
            }

            public final List<ModelPaymentSource> getPaymentSources() {
                return this.paymentSources;
            }

            public final ModelSubscription getPremiumSubscription() {
                return this.premiumSubscription;
            }

            public final List<Purchase> getPurchases() {
                return this.purchases;
            }

            public final ModelInvoicePreview getRenewalInvoicePreview() {
                return this.renewalInvoicePreview;
            }

            public final Map<String, SkuDetails> getSkuDetails() {
                return this.skuDetails;
            }

            public int hashCode() {
                ModelSubscription modelSubscription = this.premiumSubscription;
                int i = 0;
                int hashCode = (modelSubscription != null ? modelSubscription.hashCode() : 0) * 31;
                List<ModelPaymentSource> list = this.paymentSources;
                int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
                boolean z2 = this.isBusy;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode2 + i3) * 31;
                List<ModelEntitlement> list2 = this.entitlements;
                int hashCode3 = (i5 + (list2 != null ? list2.hashCode() : 0)) * 31;
                Map<Long, ModelGuildBoostSlot> map = this.guildSubscriptions;
                int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
                boolean z3 = this.hasAnyGuildBoosts;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                int i6 = (hashCode4 + i2) * 31;
                ModelInvoicePreview modelInvoicePreview = this.renewalInvoicePreview;
                int hashCode5 = (i6 + (modelInvoicePreview != null ? modelInvoicePreview.hashCode() : 0)) * 31;
                ModelInvoicePreview modelInvoicePreview2 = this.currentInvoicePreview;
                int hashCode6 = (hashCode5 + (modelInvoicePreview2 != null ? modelInvoicePreview2.hashCode() : 0)) * 31;
                Map<String, SkuDetails> map2 = this.skuDetails;
                int hashCode7 = (hashCode6 + (map2 != null ? map2.hashCode() : 0)) * 31;
                List<Purchase> list3 = this.purchases;
                if (list3 != null) {
                    i = list3.hashCode();
                }
                return ((hashCode7 + i) * 31) + this.pastDueGracePeriodDays;
            }

            public final boolean isBusy() {
                return this.isBusy;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(premiumSubscription=");
                R.append(this.premiumSubscription);
                R.append(", paymentSources=");
                R.append(this.paymentSources);
                R.append(", isBusy=");
                R.append(this.isBusy);
                R.append(", entitlements=");
                R.append(this.entitlements);
                R.append(", guildSubscriptions=");
                R.append(this.guildSubscriptions);
                R.append(", hasAnyGuildBoosts=");
                R.append(this.hasAnyGuildBoosts);
                R.append(", renewalInvoicePreview=");
                R.append(this.renewalInvoicePreview);
                R.append(", currentInvoicePreview=");
                R.append(this.currentInvoicePreview);
                R.append(", skuDetails=");
                R.append(this.skuDetails);
                R.append(", purchases=");
                R.append(this.purchases);
                R.append(", pastDueGracePeriodDays=");
                return a.A(R, this.pastDueGracePeriodDays, ")");
            }
        }

        /* compiled from: SettingsPremiumViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public SettingsPremiumViewModel() {
        this(null, null, null, null, null, null, 63, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ SettingsPremiumViewModel(com.discord.stores.StorePaymentSources r16, com.discord.stores.StoreSubscriptions r17, com.discord.stores.StoreEntitlements r18, com.discord.stores.StoreGuildBoost r19, com.discord.utilities.rest.RestAPI r20, rx.Observable r21, int r22, kotlin.jvm.internal.DefaultConstructorMarker r23) {
        /*
            r15 = this;
            r0 = r22 & 1
            if (r0 == 0) goto Lb
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePaymentSources r0 = r0.getPaymentSources()
            goto Ld
        Lb:
            r0 = r16
        Ld:
            r1 = r22 & 2
            if (r1 == 0) goto L18
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreSubscriptions r1 = r1.getSubscriptions()
            goto L1a
        L18:
            r1 = r17
        L1a:
            r2 = r22 & 4
            if (r2 == 0) goto L25
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreEntitlements r2 = r2.getEntitlements()
            goto L27
        L25:
            r2 = r18
        L27:
            r3 = r22 & 8
            if (r3 == 0) goto L32
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildBoost r3 = r3.getGuildBoosts()
            goto L34
        L32:
            r3 = r19
        L34:
            r4 = r22 & 16
            if (r4 == 0) goto L3f
            com.discord.utilities.rest.RestAPI$Companion r4 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r4 = r4.getApi()
            goto L41
        L3f:
            r4 = r20
        L41:
            r5 = r22 & 32
            if (r5 == 0) goto L70
            com.discord.widgets.settings.premium.SettingsPremiumViewModel$Companion r6 = com.discord.widgets.settings.premium.SettingsPremiumViewModel.Companion
            com.discord.stores.StoreStream$Companion r5 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePaymentSources r7 = r5.getPaymentSources()
            com.discord.stores.StoreSubscriptions r8 = r5.getSubscriptions()
            com.discord.stores.StoreEntitlements r9 = r5.getEntitlements()
            com.discord.stores.StoreGuildBoost r10 = r5.getGuildBoosts()
            com.discord.stores.StoreGooglePlaySkuDetails r11 = r5.getGooglePlaySkuDetails()
            com.discord.stores.StoreGooglePlayPurchases r12 = r5.getGooglePlayPurchases()
            com.discord.stores.StoreExperiments r13 = r5.getExperiments()
            com.discord.utilities.rest.RestAPI$Companion r5 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r14 = r5.getApi()
            rx.Observable r5 = com.discord.widgets.settings.premium.SettingsPremiumViewModel.Companion.access$observeStores(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            goto L72
        L70:
            r5 = r21
        L72:
            r16 = r15
            r17 = r0
            r18 = r1
            r19 = r2
            r20 = r3
            r21 = r4
            r22 = r5
            r16.<init>(r17, r18, r19, r20, r21, r22)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.premium.SettingsPremiumViewModel.<init>(com.discord.stores.StorePaymentSources, com.discord.stores.StoreSubscriptions, com.discord.stores.StoreEntitlements, com.discord.stores.StoreGuildBoost, com.discord.utilities.rest.RestAPI, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void fetchData() {
        this.storePaymentsSources.fetchPaymentSources();
        this.storeSubscriptions.fetchSubscriptions();
        this.storeEntitlements.fetchMyEntitlementsForApplication(521842831262875670L);
        this.storeGuildBoost.fetchUserGuildBoostState();
        GooglePlayBillingManager.INSTANCE.querySkuDetails();
    }

    private final int getPastDueGracePeriodDays(ModelSubscription modelSubscription) {
        if (modelSubscription != null && !modelSubscription.isMobileManaged()) {
            String paymentSourceId = modelSubscription.getPaymentSourceId();
            if (!(paymentSourceId == null || t.isBlank(paymentSourceId))) {
                return 7;
            }
        }
        return 3;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r9v10 */
    /* JADX WARN: Type inference failed for: r9v16, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r9v9 */
    public final void handleStoreState(StoreState storeState) {
        Object obj;
        ?? r9;
        boolean z2;
        StorePaymentSources.PaymentSourcesState paymentSourcesState = storeState.getPaymentSourcesState();
        StoreSubscriptions.SubscriptionsState subscriptionsState = storeState.getSubscriptionsState();
        StoreEntitlements.State entitlementState = storeState.getEntitlementState();
        StoreGuildBoost.State guildBoostState = storeState.getGuildBoostState();
        InvoicePreviewFetch renewalInvoicePreviewFetch = storeState.getRenewalInvoicePreviewFetch();
        InvoicePreviewFetch currentInvoicePreviewFetch = storeState.getCurrentInvoicePreviewFetch();
        StoreGooglePlaySkuDetails.State skuDetailsState = storeState.getSkuDetailsState();
        StoreGooglePlayPurchases.State purchaseState = storeState.getPurchaseState();
        if ((paymentSourcesState instanceof StorePaymentSources.PaymentSourcesState.Loaded) && (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Loaded) && (entitlementState instanceof StoreEntitlements.State.Loaded) && (guildBoostState instanceof StoreGuildBoost.State.Loaded) && (renewalInvoicePreviewFetch instanceof InvoicePreviewFetch.Invoice) && (currentInvoicePreviewFetch instanceof InvoicePreviewFetch.Invoice) && (skuDetailsState instanceof StoreGooglePlaySkuDetails.State.Loaded) && (purchaseState instanceof StoreGooglePlayPurchases.State.Loaded)) {
            Iterator it = ((StoreSubscriptions.SubscriptionsState.Loaded) subscriptionsState).getSubscriptions().iterator();
            while (true) {
                if (!it.hasNext()) {
                    r9 = 0;
                    break;
                }
                r9 = it.next();
                if (((ModelSubscription) r9).getType() == ModelSubscription.Type.PREMIUM) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            ModelSubscription modelSubscription = r9;
            boolean hasAnyOfPlans = modelSubscription != null ? modelSubscription.hasAnyOfPlans(n0.setOf((Object[]) new Long[]{Long.valueOf(SubscriptionPlanType.PREMIUM_GUILD_MONTH.getPlanId()), Long.valueOf(SubscriptionPlanType.PREMIUM_GUILD_YEAR.getPlanId())})) : false;
            List<ModelPaymentSource> paymentSources = ((StorePaymentSources.PaymentSourcesState.Loaded) paymentSourcesState).getPaymentSources();
            List<ModelEntitlement> list = ((StoreEntitlements.State.Loaded) entitlementState).getOwnedEntitlements().get(521842831262875670L);
            if (list == null) {
                list = n.emptyList();
            }
            obj = new ViewState.Loaded(modelSubscription, paymentSources, false, list, ((StoreGuildBoost.State.Loaded) guildBoostState).getBoostSlotMap(), hasAnyOfPlans, ((InvoicePreviewFetch.Invoice) renewalInvoicePreviewFetch).getModelInvoicePreview(), ((InvoicePreviewFetch.Invoice) currentInvoicePreviewFetch).getModelInvoicePreview(), ((StoreGooglePlaySkuDetails.State.Loaded) skuDetailsState).getSkuDetails(), ((StoreGooglePlayPurchases.State.Loaded) purchaseState).getPurchases(), getPastDueGracePeriodDays(modelSubscription));
        } else if ((paymentSourcesState instanceof StorePaymentSources.PaymentSourcesState.Failure) || (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Failure) || (guildBoostState instanceof StoreGuildBoost.State.Failure) || (renewalInvoicePreviewFetch instanceof InvoicePreviewFetch.Error) || (skuDetailsState instanceof StoreGooglePlaySkuDetails.State.Failure)) {
            obj = ViewState.Failure.INSTANCE;
        } else {
            obj = ViewState.Loading.INSTANCE;
        }
        updateViewState(obj);
    }

    private final void markBusy() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, true, null, null, false, null, null, null, null, 0, 2043, null));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onCancelError() {
        if (getViewState() instanceof ViewState.Loaded) {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new Event.ErrorToast(R.string.premium_alert_error_title));
        }
    }

    @MainThread
    public final void cancelSubscription() {
        ModelSubscription premiumSubscription;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (premiumSubscription = loaded.getPremiumSubscription()) != null) {
            markBusy();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.deleteSubscription(premiumSubscription.getId()), false, 1, null), this, null, 2, null), SettingsPremiumViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new SettingsPremiumViewModel$cancelSubscription$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new SettingsPremiumViewModel$cancelSubscription$1(this));
        }
    }

    @MainThread
    public final Observable<Event> getEventSubject() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @MainThread
    public final void onRetryClicked() {
        fetchData();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsPremiumViewModel(StorePaymentSources storePaymentSources, StoreSubscriptions storeSubscriptions, StoreEntitlements storeEntitlements, StoreGuildBoost storeGuildBoost, RestAPI restAPI, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(storePaymentSources, "storePaymentsSources");
        m.checkNotNullParameter(storeSubscriptions, "storeSubscriptions");
        m.checkNotNullParameter(storeEntitlements, "storeEntitlements");
        m.checkNotNullParameter(storeGuildBoost, "storeGuildBoost");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observable, "storeObservable");
        this.storePaymentsSources = storePaymentSources;
        this.storeSubscriptions = storeSubscriptions;
        this.storeEntitlements = storeEntitlements;
        this.storeGuildBoost = storeGuildBoost;
        this.restAPI = restAPI;
        this.eventSubject = PublishSubject.k0();
        fetchData();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), SettingsPremiumViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
