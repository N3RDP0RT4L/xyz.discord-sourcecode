package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.a.z.a;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.android.billingclient.api.SkuDetails;
import com.discord.api.premium.SubscriptionPlan;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetSettingsGiftingBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelEntitlement;
import com.discord.models.domain.ModelGift;
import com.discord.models.domain.ModelSku;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlayInAppSku;
import com.discord.utilities.billing.GooglePlayInAppSkuKt;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.gifting.GiftingUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.recycler.SpaceBetweenItemDecoration;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.premium.GiftSelectView;
import com.discord.widgets.settings.premium.SettingsGiftingViewModel;
import com.discord.widgets.settings.premium.WidgetOutboundPromoTerms;
import com.discord.widgets.settings.premium.WidgetSettingsGiftingAdapter;
import com.google.android.material.textfield.TextInputLayout;
import d0.o;
import d0.t.n;
import d0.t.p;
import d0.t.u;
import d0.t.z;
import d0.z.d.a0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetSettingsGifting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 82\u00020\u0001:\u00018B\u0007¢\u0006\u0004\b7\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\r\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J3\u0010\u0019\u001a\u00020\u00042\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00112\u0006\u0010\r\u001a\u00020\u00102\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00120\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001bH\u0016¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001f\u0010\u000bR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b!\u0010\"R\u001d\u0010(\u001a\u00020#8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b*\u0010+R\u001d\u00101\u001a\u00020,8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R\"\u00105\u001a\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u000204028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b5\u00106¨\u00069"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGifting;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;", "event", "", "handleGiftPurchaseEvent", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$GiftPurchaseEvent;)V", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event;", "handleEvent", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$Event;)V", "enableGiftingButtons", "()V", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;", "viewState", "configureUI", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState;)V", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loaded;", "", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter$GiftItem;", "generateListItems", "(Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loaded;)Ljava/util/List;", "Lcom/discord/models/domain/ModelEntitlement;", "entries", "", "listItems", "addGiftItems", "(Ljava/util/List;Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel$ViewState$Loaded;Ljava/util/List;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter;", "promosAdapter", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingOutboundPromosAdapter;", "Lcom/discord/databinding/WidgetSettingsGiftingBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsGiftingBinding;", "binding", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;", "giftingAdapter", "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingAdapter;", "Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/premium/SettingsGiftingViewModel;", "viewModel", "", "Lcom/discord/views/premium/GiftSelectView;", "Lcom/discord/utilities/billing/GooglePlayInAppSku;", "chooseGiftViews", "Ljava/util/Map;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsGifting extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsGifting.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsGiftingBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final int VIEW_INDEX_FAILURE = 1;
    public static final int VIEW_INDEX_LOADED = 2;
    public static final int VIEW_INDEX_LOADING = 0;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsGifting$binding$2.INSTANCE, null, 2, null);
    private Map<GiftSelectView, GooglePlayInAppSku> chooseGiftViews;
    private WidgetSettingsGiftingAdapter giftingAdapter;
    private WidgetSettingsGiftingOutboundPromosAdapter promosAdapter;
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetSettingsGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ#\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0007¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u000bR\u0016\u0010\r\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsGifting$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/utilities/analytics/Traits$Location;", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "", "launch", "(Landroid/content/Context;Lcom/discord/utilities/analytics/Traits$Location;)V", "", "VIEW_INDEX_FAILURE", "I", "VIEW_INDEX_LOADED", "VIEW_INDEX_LOADING", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, Traits.Location location, int i, Object obj) {
            if ((i & 2) != 0) {
                location = null;
            }
            companion.launch(context, location);
        }

        public final void launch(Context context, Traits.Location location) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra("com.discord.intent.extra.EXTRA_LOCATION", location);
            j.d(context, WidgetSettingsGifting.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsGifting() {
        super(R.layout.widget_settings_gifting);
        WidgetSettingsGifting$viewModel$2 widgetSettingsGifting$viewModel$2 = WidgetSettingsGifting$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(SettingsGiftingViewModel.class), new WidgetSettingsGifting$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetSettingsGifting$viewModel$2));
    }

    private final void addGiftItems(List<ModelEntitlement> list, SettingsGiftingViewModel.ViewState.Loaded loaded, List<WidgetSettingsGiftingAdapter.GiftItem> list2) {
        if (!list.isEmpty()) {
            ModelEntitlement modelEntitlement = (ModelEntitlement) u.first((List<? extends Object>) list);
            Set<Long> expandedSkuOrPlanIds = loaded.getExpandedSkuOrPlanIds();
            SubscriptionPlan subscriptionPlan = modelEntitlement.getSubscriptionPlan();
            boolean contains = expandedSkuOrPlanIds.contains(Long.valueOf(subscriptionPlan != null ? subscriptionPlan.a() : modelEntitlement.getSku().getId()));
            ModelSku sku = modelEntitlement.getSku();
            SubscriptionPlan subscriptionPlan2 = modelEntitlement.getSubscriptionPlan();
            list2.add(new WidgetSettingsGiftingAdapter.GiftItem(1, null, null, Boolean.valueOf(contains), sku, Integer.valueOf(list.size()), subscriptionPlan2 != null ? Long.valueOf(subscriptionPlan2.a()) : null, null, null, 390, null));
            Iterator withIndex = p.withIndex(list.iterator());
            boolean z2 = false;
            while (withIndex.hasNext()) {
                z zVar = (z) withIndex.next();
                int component1 = zVar.component1();
                ModelEntitlement modelEntitlement2 = (ModelEntitlement) zVar.component2();
                Map<Long, List<ModelGift>> myPurchasedGifts = loaded.getMyPurchasedGifts();
                SubscriptionPlan subscriptionPlan3 = modelEntitlement2.getSubscriptionPlan();
                List<ModelGift> list3 = myPurchasedGifts.get(Long.valueOf(subscriptionPlan3 != null ? subscriptionPlan3.a() : modelEntitlement2.getSkuId()));
                ModelGift modelGift = (component1 < (list3 != null ? list3.size() : -1) && list3 != null) ? list3.get(component1) : null;
                boolean z3 = true;
                if (modelGift == null) {
                    if (!z2) {
                        z2 = true;
                    }
                }
                SubscriptionPlan subscriptionPlan4 = modelEntitlement2.getSubscriptionPlan();
                Long valueOf = subscriptionPlan4 != null ? Long.valueOf(subscriptionPlan4.a()) : null;
                Boolean valueOf2 = Boolean.valueOf(contains);
                Boolean valueOf3 = Boolean.valueOf(component1 == list.size() - 1);
                if (modelGift == null || !m.areEqual(modelGift.getCode(), loaded.getLastCopiedCode())) {
                    z3 = false;
                }
                list2.add(new WidgetSettingsGiftingAdapter.GiftItem(2, modelGift, modelEntitlement2, valueOf2, null, null, valueOf, valueOf3, Boolean.valueOf(z3), 48, null));
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(SettingsGiftingViewModel.ViewState viewState) {
        CharSequence charSequence;
        int i = 0;
        if (viewState instanceof SettingsGiftingViewModel.ViewState.Loading) {
            AppViewFlipper appViewFlipper = getBinding().i;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.settingsGiftingFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (viewState instanceof SettingsGiftingViewModel.ViewState.Failure) {
            AppViewFlipper appViewFlipper2 = getBinding().i;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.settingsGiftingFlipper");
            appViewFlipper2.setDisplayedChild(1);
        } else {
            if (viewState instanceof SettingsGiftingViewModel.ViewState.Loaded) {
                AppViewFlipper appViewFlipper3 = getBinding().i;
                m.checkNotNullExpressionValue(appViewFlipper3, "binding.settingsGiftingFlipper");
                appViewFlipper3.setDisplayedChild(2);
            }
            SettingsGiftingViewModel.ViewState.Loaded loaded = (SettingsGiftingViewModel.ViewState.Loaded) viewState;
            boolean z2 = loaded.getResolvingGiftState() instanceof SettingsGiftingViewModel.ResolvingGiftState.Resolving;
            if (loaded.getResolvingGiftState() instanceof SettingsGiftingViewModel.ResolvingGiftState.Error) {
                charSequence = b.e(this, R.string.application_entitlement_code_redemption_invalid, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            } else {
                charSequence = null;
            }
            ProgressBar progressBar = getBinding().j;
            m.checkNotNullExpressionValue(progressBar, "binding.settingsGiftingGiftCodeInputProgress");
            progressBar.setVisibility(z2 ? 0 : 8);
            TextInputLayout textInputLayout = getBinding().k;
            m.checkNotNullExpressionValue(textInputLayout, "binding.settingsGiftingGiftCodeInputWrap");
            ViewExtensions.setEnabledAlpha$default(textInputLayout, !z2, 0.0f, 2, null);
            TextInputLayout textInputLayout2 = getBinding().k;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.settingsGiftingGiftCodeInputWrap");
            EditText editText = textInputLayout2.getEditText();
            if (editText != null) {
                editText.setInputType(z2 ? 0 : 524288);
            }
            TextInputLayout textInputLayout3 = getBinding().k;
            m.checkNotNullExpressionValue(textInputLayout3, "binding.settingsGiftingGiftCodeInputWrap");
            textInputLayout3.setError(charSequence);
            boolean z3 = !loaded.getOutboundPromos().isEmpty();
            TextView textView = getBinding().r;
            m.checkNotNullExpressionValue(textView, "binding.settingsGiftingYourGiftsHeader");
            textView.setVisibility(z3 ? 0 : 8);
            RecyclerView recyclerView = getBinding().f2595s;
            m.checkNotNullExpressionValue(recyclerView, "binding.settingsGiftingYourGiftsRecycler");
            if (!z3) {
                i = 8;
            }
            recyclerView.setVisibility(i);
            WidgetSettingsGiftingOutboundPromosAdapter widgetSettingsGiftingOutboundPromosAdapter = this.promosAdapter;
            if (widgetSettingsGiftingOutboundPromosAdapter == null) {
                m.throwUninitializedPropertyAccessException("promosAdapter");
            }
            widgetSettingsGiftingOutboundPromosAdapter.submitList(loaded.getOutboundPromos());
            WidgetSettingsGiftingAdapter widgetSettingsGiftingAdapter = this.giftingAdapter;
            if (widgetSettingsGiftingAdapter == null) {
                m.throwUninitializedPropertyAccessException("giftingAdapter");
            }
            widgetSettingsGiftingAdapter.configure(generateListItems(loaded), new WidgetSettingsGifting$configureUI$1(this), new WidgetSettingsGifting$configureUI$2(this), WidgetSettingsGifting$configureUI$3.INSTANCE, WidgetSettingsGifting$configureUI$4.INSTANCE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void enableGiftingButtons() {
        Map<GiftSelectView, GooglePlayInAppSku> map = this.chooseGiftViews;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("chooseGiftViews");
        }
        for (GiftSelectView giftSelectView : map.keySet()) {
            giftSelectView.j.c.setOnClickListener(new b.a.y.n0.a(giftSelectView));
        }
    }

    private final List<WidgetSettingsGiftingAdapter.GiftItem> generateListItems(SettingsGiftingViewModel.ViewState.Loaded loaded) {
        ArrayList arrayList = new ArrayList();
        if (loaded.getMyEntitlements().isEmpty()) {
            arrayList.add(new WidgetSettingsGiftingAdapter.GiftItem(0, null, null, null, null, null, null, null, null, 510, null));
        } else {
            for (Map.Entry<Long, List<ModelEntitlement>> entry : loaded.getMyEntitlements().entrySet()) {
                entry.getKey().longValue();
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Object obj : entry.getValue()) {
                    SubscriptionPlan subscriptionPlan = ((ModelEntitlement) obj).getSubscriptionPlan();
                    Long valueOf = subscriptionPlan != null ? Long.valueOf(subscriptionPlan.a()) : null;
                    Object obj2 = linkedHashMap.get(valueOf);
                    if (obj2 == null) {
                        obj2 = new ArrayList();
                        linkedHashMap.put(valueOf, obj2);
                    }
                    ((List) obj2).add(obj);
                }
                for (Map.Entry entry2 : linkedHashMap.entrySet()) {
                    Long l = (Long) entry2.getKey();
                    addGiftItems((List) entry2.getValue(), loaded, arrayList);
                }
            }
        }
        return arrayList;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSettingsGiftingBinding getBinding() {
        return (WidgetSettingsGiftingBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final SettingsGiftingViewModel getViewModel() {
        return (SettingsGiftingViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(SettingsGiftingViewModel.Event event) {
        Unit unit;
        if (event instanceof SettingsGiftingViewModel.Event.ShowPromoBottomSheet) {
            WidgetOutboundPromoTerms.Companion companion = WidgetOutboundPromoTerms.Companion;
            String content = ((SettingsGiftingViewModel.Event.ShowPromoBottomSheet) event).getContent();
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(content, parentFragmentManager);
            unit = Unit.a;
        } else if (event instanceof SettingsGiftingViewModel.Event.ShowPromoDialog) {
            WidgetClaimOutboundPromo.Companion.showAndRegisterForClaimResult(((SettingsGiftingViewModel.Event.ShowPromoDialog) event).getClaimStatus(), this, new WidgetSettingsGifting$handleEvent$1(this));
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleGiftPurchaseEvent(SettingsGiftingViewModel.GiftPurchaseEvent giftPurchaseEvent) {
        enableGiftingButtons();
        if (giftPurchaseEvent instanceof SettingsGiftingViewModel.GiftPurchaseEvent.StartGiftPurchase) {
            DimmerView.setDimmed$default(getBinding().f2594b, true, false, 2, null);
        } else if (giftPurchaseEvent instanceof SettingsGiftingViewModel.GiftPurchaseEvent.NotInProgress) {
            DimmerView.setDimmed$default(getBinding().f2594b, false, false, 2, null);
        } else if (giftPurchaseEvent instanceof SettingsGiftingViewModel.GiftPurchaseEvent.ErrorGiftPurchase) {
            DimmerView.setDimmed$default(getBinding().f2594b, false, false, 2, null);
            b.a.d.m.i(this, ((SettingsGiftingViewModel.GiftPurchaseEvent.ErrorGiftPurchase) giftPurchaseEvent).getMessage(), 0, 4);
        } else if (giftPurchaseEvent instanceof SettingsGiftingViewModel.GiftPurchaseEvent.CompleteGiftPurchase) {
            DimmerView.setDimmed$default(getBinding().f2594b, false, false, 2, null);
            a.C0025a aVar = b.a.a.z.a.k;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            WidgetSettingsGifting$handleGiftPurchaseEvent$1 widgetSettingsGifting$handleGiftPurchaseEvent$1 = new WidgetSettingsGifting$handleGiftPurchaseEvent$1(this);
            SettingsGiftingViewModel.GiftPurchaseEvent.CompleteGiftPurchase completeGiftPurchase = (SettingsGiftingViewModel.GiftPurchaseEvent.CompleteGiftPurchase) giftPurchaseEvent;
            String skuName = completeGiftPurchase.getSkuName();
            String newGiftCode = completeGiftPurchase.getNewGiftCode();
            Objects.requireNonNull(aVar);
            m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
            m.checkNotNullParameter(widgetSettingsGifting$handleGiftPurchaseEvent$1, "onDismiss");
            m.checkNotNullParameter(skuName, "skuName");
            m.checkNotNullParameter(newGiftCode, "giftCode");
            b.a.a.z.a aVar2 = new b.a.a.z.a();
            aVar2.l = widgetSettingsGifting$handleGiftPurchaseEvent$1;
            Bundle bundle = new Bundle();
            bundle.putString("ARG_SKU_NAME", skuName);
            bundle.putString("ARG_GIFT_CODE", newGiftCode);
            aVar2.setArguments(bundle);
            aVar2.show(parentFragmentManager, b.a.a.z.a.class.getSimpleName());
        }
    }

    public static final void launch(Context context, Traits.Location location) {
        Companion.launch(context, location);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        this.chooseGiftViews = d0.t.h0.mapOf(o.to(getBinding().c, GooglePlayInAppSkuKt.getPremiumTier1Month()), o.to(getBinding().d, GooglePlayInAppSkuKt.getPremiumTier1Year()), o.to(getBinding().e, GooglePlayInAppSkuKt.getPremiumTier2Month()), o.to(getBinding().f, GooglePlayInAppSkuKt.getPremiumTier2Year()));
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        getBinding().o.setOnClickListener(WidgetSettingsGifting$onViewBound$1.INSTANCE);
        TextInputLayout textInputLayout = getBinding().k;
        m.checkNotNullExpressionValue(textInputLayout, "binding.settingsGiftingGiftCodeInputWrap");
        ViewExtensions.setOnImeActionDone$default(textInputLayout, false, new WidgetSettingsGifting$onViewBound$2(this), 1, null);
        getBinding().k.setErrorTextColor(ColorStateList.valueOf(ColorCompat.getColor(requireContext(), (int) R.color.status_red_500)));
        WidgetSettingsGiftingOutboundPromosAdapter widgetSettingsGiftingOutboundPromosAdapter = new WidgetSettingsGiftingOutboundPromosAdapter(new WidgetSettingsGifting$onViewBound$3(getViewModel()), new WidgetSettingsGifting$onViewBound$4(getViewModel()));
        RecyclerView recyclerView = getBinding().f2595s;
        m.checkNotNullExpressionValue(recyclerView, "binding.settingsGiftingYourGiftsRecycler");
        recyclerView.setAdapter(widgetSettingsGiftingOutboundPromosAdapter);
        this.promosAdapter = widgetSettingsGiftingOutboundPromosAdapter;
        RecyclerView recyclerView2 = getBinding().f2595s;
        WidgetSettingsGiftingOutboundPromosAdapter widgetSettingsGiftingOutboundPromosAdapter2 = this.promosAdapter;
        if (widgetSettingsGiftingOutboundPromosAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("promosAdapter");
        }
        recyclerView2.setAdapter(widgetSettingsGiftingOutboundPromosAdapter2);
        Context context = recyclerView2.getContext();
        m.checkNotNullExpressionValue(context, "context");
        RecyclerView.LayoutManager layoutManager = recyclerView2.getLayoutManager();
        Objects.requireNonNull(layoutManager, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        int orientation = ((LinearLayoutManager) layoutManager).getOrientation();
        WidgetSettingsGiftingOutboundPromosAdapter widgetSettingsGiftingOutboundPromosAdapter3 = this.promosAdapter;
        if (widgetSettingsGiftingOutboundPromosAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("promosAdapter");
        }
        recyclerView2.addItemDecoration(new SpaceBetweenItemDecoration(context, orientation, widgetSettingsGiftingOutboundPromosAdapter3, DimenUtils.dpToPixels(16), 0, 16, null));
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView3 = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView3, "binding.settingsGiftingGiftsYouPurchasedRecycler");
        this.giftingAdapter = (WidgetSettingsGiftingAdapter) companion.configure(new WidgetSettingsGiftingAdapter(recyclerView3));
        LinkifiedTextView linkifiedTextView = getBinding().g;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.premiumTier1GiftLearnMore");
        b.m(linkifiedTextView, R.string.premium_classic_payment_gift_blurb_mobile, new Object[0], WidgetSettingsGifting$onViewBound$7.INSTANCE);
        LinkifiedTextView linkifiedTextView2 = getBinding().h;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.premiumTier2GiftLearnMore");
        b.m(linkifiedTextView2, R.string.premium_payment_gift_blurb_mobile, new Object[0], WidgetSettingsGifting$onViewBound$8.INSTANCE);
        Serializable serializableExtra = getMostRecentIntent().getSerializableExtra("com.discord.intent.extra.EXTRA_LOCATION");
        if (!(serializableExtra instanceof Traits.Location)) {
            serializableExtra = null;
        }
        WidgetSettingsGifting$onViewBound$chooseGiftCallback$1 widgetSettingsGifting$onViewBound$chooseGiftCallback$1 = new WidgetSettingsGifting$onViewBound$chooseGiftCallback$1(this, (Traits.Location) serializableExtra);
        Map<GiftSelectView, GooglePlayInAppSku> map = this.chooseGiftViews;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("chooseGiftViews");
        }
        for (Map.Entry<GiftSelectView, GooglePlayInAppSku> entry : map.entrySet()) {
            GiftSelectView key = entry.getKey();
            GooglePlayInAppSku value = entry.getValue();
            Objects.requireNonNull(key);
            m.checkNotNullParameter(value, "inAppSku");
            m.checkNotNullParameter(widgetSettingsGifting$onViewBound$chooseGiftCallback$1, "onClickPlan");
            key.k = widgetSettingsGifting$onViewBound$chooseGiftCallback$1;
            key.l = value;
            SkuDetails skuDetails = value.getSkuDetails();
            if (skuDetails != null) {
                TextView textView = key.j.f;
                m.checkNotNullExpressionValue(textView, "binding.planItemName");
                textView.setText(skuDetails.a());
                TextView textView2 = key.j.g;
                m.checkNotNullExpressionValue(textView2, "binding.planItemPrice");
                textView2.setText(skuDetails.b());
                TextView textView3 = key.j.d;
                m.checkNotNullExpressionValue(textView3, "binding.planItemCurrentPlan");
                textView3.setVisibility(8);
                Integer iconForSku = GiftingUtils.INSTANCE.getIconForSku(value);
                if (iconForSku != null) {
                    key.j.e.setImageResource(iconForSku.intValue());
                }
                TextView textView4 = key.j.g;
                m.checkNotNullExpressionValue(textView4, "binding.planItemPrice");
                Context context2 = textView4.getContext();
                m.checkNotNullExpressionValue(context2, "binding.planItemPrice.context");
                int themedDrawableRes$default = DrawableCompat.getThemedDrawableRes$default(context2, (int) R.attr.ic_navigate_next, 0, 2, (Object) null);
                TextView textView5 = key.j.g;
                m.checkNotNullExpressionValue(textView5, "binding.planItemPrice");
                DrawableCompat.setCompoundDrawablesCompat$default(textView5, 0, 0, themedDrawableRes$default, 0, 11, (Object) null);
                key.j.c.setOnClickListener(new b.a.y.n0.a(key));
            }
        }
        RecyclerView recyclerView4 = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView4, "binding.settingsGiftingGiftsYouPurchasedRecycler");
        recyclerView4.setItemAnimator(null);
        getBinding().m.setHasFixedSize(false);
        for (TextView textView6 : n.listOf((Object[]) new TextView[]{getBinding().n, getBinding().l, getBinding().q, getBinding().p})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView6, "header");
            accessibilityUtils.setViewIsHeading(textView6);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<SettingsGiftingViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetSettingsGifting.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsGifting$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getViewModel().observeGiftPurchaseEvents(), this, null, 2, null), WidgetSettingsGifting.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsGifting$onViewBoundOrOnResume$2(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetSettingsGifting.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsGifting$onViewBoundOrOnResume$3(this));
        getViewModel().setOnGiftCodeResolved(WidgetSettingsGifting$onViewBoundOrOnResume$4.INSTANCE);
        enableGiftingButtons();
    }
}
