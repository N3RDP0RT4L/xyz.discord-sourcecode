package com.discord.widgets.settings.premium;

import b.a.k.b;
import com.discord.i18n.RenderContext;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsPremium.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPremium$onViewBound$2 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ WidgetSettingsPremium this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPremium$onViewBound$2(WidgetSettingsPremium widgetSettingsPremium) {
        super(1);
        this.this$0 = widgetSettingsPremium;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        CharSequence e;
        CharSequence e2;
        m.checkNotNullParameter(renderContext, "$receiver");
        Map<String, String> map = renderContext.a;
        e = b.e(this.this$0, R.string.file_upload_limit_standard, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        map.put("maxUploadStandard", e.toString());
        Map<String, String> map2 = renderContext.a;
        e2 = b.e(this.this$0, R.string.file_upload_limit_premium_tier_1, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        map2.put("maxUploadPremium", e2.toString());
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }
}
