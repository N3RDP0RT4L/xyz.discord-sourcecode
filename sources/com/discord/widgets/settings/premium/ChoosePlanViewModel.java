package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import android.text.TextUtils;
import androidx.annotation.StringRes;
import b.d.a.a.c;
import b.d.b.a.a;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.discord.api.premium.SubscriptionInterval;
import com.discord.app.AppLog;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelSubscription;
import com.discord.stores.PendingDowngrade;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.stores.StoreGooglePlaySkuDetails;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreSubscriptions;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlayBillingManager;
import com.discord.utilities.billing.GooglePlaySku;
import com.discord.utilities.billing.GooglePlaySkuKt;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.settings.premium.WidgetChoosePlan;
import com.discord.widgets.settings.premium.WidgetChoosePlanAdapter;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func4;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: ChoosePlanViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 D2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004DEFGB)\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\b\u00104\u001a\u0004\u0018\u00010\u0015\u0012\u000e\b\u0002\u0010A\u001a\b\u0012\u0004\u0012\u00020\u000600¢\u0006\u0004\bB\u0010CJ\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J)\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000e0\u0014H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ=\u0010\u001f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180\u00172\u0006\u0010\u001c\u001a\u00020\u001b2\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000e0\u00142\b\b\u0002\u0010\u001e\u001a\u00020\u001dH\u0002¢\u0006\u0004\b\u001f\u0010 J3\u0010!\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180\u00172\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000e0\u00142\u0006\u0010\u001e\u001a\u00020\u001dH\u0002¢\u0006\u0004\b!\u0010\"J7\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\f\u0010$\u001a\b\u0012\u0004\u0012\u00020#0\u00172\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000e0\u0014H\u0002¢\u0006\u0004\b%\u0010&J\u0017\u0010*\u001a\u00020)2\u0006\u0010(\u001a\u00020'H\u0002¢\u0006\u0004\b*\u0010+J-\u0010.\u001a\u0004\u0018\u00010-2\u0006\u0010,\u001a\u00020#2\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000e0\u0014H\u0002¢\u0006\u0004\b.\u0010/J\u0013\u00102\u001a\b\u0012\u0004\u0012\u00020100¢\u0006\u0004\b2\u00103J1\u00108\u001a\u00020\u00032\u0006\u0010,\u001a\u00020#2\n\b\u0002\u00104\u001a\u0004\u0018\u00010\u00152\u0006\u00106\u001a\u0002052\u0006\u00107\u001a\u00020\u0015¢\u0006\u0004\b8\u00109R\u0018\u00106\u001a\u0004\u0018\u0001058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u0010:R\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010;R\u0018\u00104\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u0010<R:\u0010?\u001a&\u0012\f\u0012\n >*\u0004\u0018\u00010101 >*\u0012\u0012\f\u0012\n >*\u0004\u0018\u00010101\u0018\u00010=0=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@¨\u0006H"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;", "", "fetchData", "()V", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;)V", "Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "event", "handleEvent", "(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V", "Lcom/android/billingclient/api/SkuDetails;", "oldSkuDetails", "newSkuDetails", "", "getProrationMode", "(Lcom/android/billingclient/api/SkuDetails;Lcom/android/billingclient/api/SkuDetails;)I", "", "", "skuDetailsMap", "", "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;", "getCurrentPlanItems", "(Ljava/util/Map;)Ljava/util/List;", "Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;", "viewType", "Lcom/discord/api/premium/SubscriptionInterval;", "skuInterval", "getItemsForViewType", "(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/util/Map;Lcom/discord/api/premium/SubscriptionInterval;)Ljava/util/List;", "getGuildBoostPlans", "(Ljava/util/Map;Lcom/discord/api/premium/SubscriptionInterval;)Ljava/util/List;", "Lcom/discord/utilities/billing/GooglePlaySku;", "skus", "getPlansWithHeaders", "(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;", "Lcom/discord/utilities/billing/GooglePlaySku$Section;", "section", "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;", "getHeaderForSkuSection", "(Lcom/discord/utilities/billing/GooglePlaySku$Section;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;", "sku", "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;", "getPlanForSku", "(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;", "Lrx/Observable;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;", "observeEvents", "()Lrx/Observable;", "oldSkuName", "Lcom/discord/utilities/analytics/Traits$Location;", "locationTrait", "fromStep", "buy", "(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Ljava/lang/String;)V", "Lcom/discord/utilities/analytics/Traits$Location;", "Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;", "Ljava/lang/String;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChoosePlanViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private Traits.Location locationTrait;
    private final String oldSkuName;
    private final WidgetChoosePlan.ViewType viewType;

    /* compiled from: ChoosePlanViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.ChoosePlanViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(ChoosePlanViewModel choosePlanViewModel) {
            super(1, choosePlanViewModel, ChoosePlanViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((ChoosePlanViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: ChoosePlanViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "p1", "", "invoke", "(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.premium.ChoosePlanViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<StoreGooglePlayPurchases.Event, Unit> {
        public AnonymousClass2(ChoosePlanViewModel choosePlanViewModel) {
            super(1, choosePlanViewModel, ChoosePlanViewModel.class, "handleEvent", "handleEvent(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreGooglePlayPurchases.Event event) {
            invoke2(event);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreGooglePlayPurchases.Event event) {
            m.checkNotNullParameter(event, "p1");
            ((ChoosePlanViewModel) this.receiver).handleEvent(event);
        }
    }

    /* compiled from: ChoosePlanViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;", "observeStores", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores() {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<StoreGooglePlaySkuDetails.State> observeState = companion.getGooglePlaySkuDetails().observeState();
            Observable<StoreGooglePlayPurchases.State> observeState2 = companion.getGooglePlayPurchases().observeState();
            Observable<StoreGooglePlayPurchases.QueryState> observeQueryState = companion.getGooglePlayPurchases().observeQueryState();
            Observable<StoreSubscriptions.SubscriptionsState> observeSubscriptions = companion.getSubscriptions().observeSubscriptions();
            final ChoosePlanViewModel$Companion$observeStores$1 choosePlanViewModel$Companion$observeStores$1 = ChoosePlanViewModel$Companion$observeStores$1.INSTANCE;
            Object obj = choosePlanViewModel$Companion$observeStores$1;
            if (choosePlanViewModel$Companion$observeStores$1 != null) {
                obj = new Func4() { // from class: com.discord.widgets.settings.premium.ChoosePlanViewModel$sam$rx_functions_Func4$0
                    @Override // rx.functions.Func4
                    public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                        return Function4.this.invoke(obj2, obj3, obj4, obj5);
                    }
                };
            }
            Observable<StoreState> h = Observable.h(observeState, observeState2, observeQueryState, observeSubscriptions, (Func4) obj);
            m.checkNotNullExpressionValue(h, "Observable.combineLatest…     ::StoreState\n      )");
            return h;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ChoosePlanViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;", "", HookHelper.constructorName, "()V", "CompleteSkuPurchase", "ErrorSkuPurchase", "StartSkuPurchase", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: ChoosePlanViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;", "", "component1", "()Ljava/lang/String;", "component2", "skuName", "planName", "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSkuName", "getPlanName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CompleteSkuPurchase extends Event {
            private final String planName;
            private final String skuName;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public CompleteSkuPurchase(String str, String str2) {
                super(null);
                m.checkNotNullParameter(str, "skuName");
                m.checkNotNullParameter(str2, "planName");
                this.skuName = str;
                this.planName = str2;
            }

            public static /* synthetic */ CompleteSkuPurchase copy$default(CompleteSkuPurchase completeSkuPurchase, String str, String str2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = completeSkuPurchase.skuName;
                }
                if ((i & 2) != 0) {
                    str2 = completeSkuPurchase.planName;
                }
                return completeSkuPurchase.copy(str, str2);
            }

            public final String component1() {
                return this.skuName;
            }

            public final String component2() {
                return this.planName;
            }

            public final CompleteSkuPurchase copy(String str, String str2) {
                m.checkNotNullParameter(str, "skuName");
                m.checkNotNullParameter(str2, "planName");
                return new CompleteSkuPurchase(str, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof CompleteSkuPurchase)) {
                    return false;
                }
                CompleteSkuPurchase completeSkuPurchase = (CompleteSkuPurchase) obj;
                return m.areEqual(this.skuName, completeSkuPurchase.skuName) && m.areEqual(this.planName, completeSkuPurchase.planName);
            }

            public final String getPlanName() {
                return this.planName;
            }

            public final String getSkuName() {
                return this.skuName;
            }

            public int hashCode() {
                String str = this.skuName;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String str2 = this.planName;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("CompleteSkuPurchase(skuName=");
                R.append(this.skuName);
                R.append(", planName=");
                return a.H(R, this.planName, ")");
            }
        }

        /* compiled from: ChoosePlanViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;", "", "component1", "()I", "message", "copy", "(I)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMessage", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ErrorSkuPurchase extends Event {
            private final int message;

            public ErrorSkuPurchase(@StringRes int i) {
                super(null);
                this.message = i;
            }

            public static /* synthetic */ ErrorSkuPurchase copy$default(ErrorSkuPurchase errorSkuPurchase, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = errorSkuPurchase.message;
                }
                return errorSkuPurchase.copy(i);
            }

            public final int component1() {
                return this.message;
            }

            public final ErrorSkuPurchase copy(@StringRes int i) {
                return new ErrorSkuPurchase(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ErrorSkuPurchase) && this.message == ((ErrorSkuPurchase) obj).message;
                }
                return true;
            }

            public final int getMessage() {
                return this.message;
            }

            public int hashCode() {
                return this.message;
            }

            public String toString() {
                return a.A(a.R("ErrorSkuPurchase(message="), this.message, ")");
            }
        }

        /* compiled from: ChoosePlanViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;", "Lcom/android/billingclient/api/BillingFlowParams;", "component1", "()Lcom/android/billingclient/api/BillingFlowParams;", "billingParams", "copy", "(Lcom/android/billingclient/api/BillingFlowParams;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/android/billingclient/api/BillingFlowParams;", "getBillingParams", HookHelper.constructorName, "(Lcom/android/billingclient/api/BillingFlowParams;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class StartSkuPurchase extends Event {
            private final BillingFlowParams billingParams;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public StartSkuPurchase(BillingFlowParams billingFlowParams) {
                super(null);
                m.checkNotNullParameter(billingFlowParams, "billingParams");
                this.billingParams = billingFlowParams;
            }

            public static /* synthetic */ StartSkuPurchase copy$default(StartSkuPurchase startSkuPurchase, BillingFlowParams billingFlowParams, int i, Object obj) {
                if ((i & 1) != 0) {
                    billingFlowParams = startSkuPurchase.billingParams;
                }
                return startSkuPurchase.copy(billingFlowParams);
            }

            public final BillingFlowParams component1() {
                return this.billingParams;
            }

            public final StartSkuPurchase copy(BillingFlowParams billingFlowParams) {
                m.checkNotNullParameter(billingFlowParams, "billingParams");
                return new StartSkuPurchase(billingFlowParams);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof StartSkuPurchase) && m.areEqual(this.billingParams, ((StartSkuPurchase) obj).billingParams);
                }
                return true;
            }

            public final BillingFlowParams getBillingParams() {
                return this.billingParams;
            }

            public int hashCode() {
                BillingFlowParams billingFlowParams = this.billingParams;
                if (billingFlowParams != null) {
                    return billingFlowParams.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("StartSkuPurchase(billingParams=");
                R.append(this.billingParams);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ChoosePlanViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ8\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\rR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\"\u001a\u0004\b#\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010$\u001a\u0004\b%\u0010\n¨\u0006("}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;", "", "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "component1", "()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "component2", "()Lcom/discord/stores/StoreGooglePlayPurchases$State;", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "component3", "()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "component4", "()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "skuDetailsState", "purchasesState", "purchasesQueryState", "subscriptionsState", "copy", "(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "getPurchasesState", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "getSubscriptionsState", "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "getSkuDetailsState", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "getPurchasesQueryState", HookHelper.constructorName, "(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StoreGooglePlayPurchases.QueryState purchasesQueryState;
        private final StoreGooglePlayPurchases.State purchasesState;
        private final StoreGooglePlaySkuDetails.State skuDetailsState;
        private final StoreSubscriptions.SubscriptionsState subscriptionsState;

        public StoreState(StoreGooglePlaySkuDetails.State state, StoreGooglePlayPurchases.State state2, StoreGooglePlayPurchases.QueryState queryState, StoreSubscriptions.SubscriptionsState subscriptionsState) {
            m.checkNotNullParameter(state, "skuDetailsState");
            m.checkNotNullParameter(state2, "purchasesState");
            m.checkNotNullParameter(queryState, "purchasesQueryState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            this.skuDetailsState = state;
            this.purchasesState = state2;
            this.purchasesQueryState = queryState;
            this.subscriptionsState = subscriptionsState;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, StoreGooglePlaySkuDetails.State state, StoreGooglePlayPurchases.State state2, StoreGooglePlayPurchases.QueryState queryState, StoreSubscriptions.SubscriptionsState subscriptionsState, int i, Object obj) {
            if ((i & 1) != 0) {
                state = storeState.skuDetailsState;
            }
            if ((i & 2) != 0) {
                state2 = storeState.purchasesState;
            }
            if ((i & 4) != 0) {
                queryState = storeState.purchasesQueryState;
            }
            if ((i & 8) != 0) {
                subscriptionsState = storeState.subscriptionsState;
            }
            return storeState.copy(state, state2, queryState, subscriptionsState);
        }

        public final StoreGooglePlaySkuDetails.State component1() {
            return this.skuDetailsState;
        }

        public final StoreGooglePlayPurchases.State component2() {
            return this.purchasesState;
        }

        public final StoreGooglePlayPurchases.QueryState component3() {
            return this.purchasesQueryState;
        }

        public final StoreSubscriptions.SubscriptionsState component4() {
            return this.subscriptionsState;
        }

        public final StoreState copy(StoreGooglePlaySkuDetails.State state, StoreGooglePlayPurchases.State state2, StoreGooglePlayPurchases.QueryState queryState, StoreSubscriptions.SubscriptionsState subscriptionsState) {
            m.checkNotNullParameter(state, "skuDetailsState");
            m.checkNotNullParameter(state2, "purchasesState");
            m.checkNotNullParameter(queryState, "purchasesQueryState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionsState");
            return new StoreState(state, state2, queryState, subscriptionsState);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.skuDetailsState, storeState.skuDetailsState) && m.areEqual(this.purchasesState, storeState.purchasesState) && m.areEqual(this.purchasesQueryState, storeState.purchasesQueryState) && m.areEqual(this.subscriptionsState, storeState.subscriptionsState);
        }

        public final StoreGooglePlayPurchases.QueryState getPurchasesQueryState() {
            return this.purchasesQueryState;
        }

        public final StoreGooglePlayPurchases.State getPurchasesState() {
            return this.purchasesState;
        }

        public final StoreGooglePlaySkuDetails.State getSkuDetailsState() {
            return this.skuDetailsState;
        }

        public final StoreSubscriptions.SubscriptionsState getSubscriptionsState() {
            return this.subscriptionsState;
        }

        public int hashCode() {
            StoreGooglePlaySkuDetails.State state = this.skuDetailsState;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            StoreGooglePlayPurchases.State state2 = this.purchasesState;
            int hashCode2 = (hashCode + (state2 != null ? state2.hashCode() : 0)) * 31;
            StoreGooglePlayPurchases.QueryState queryState = this.purchasesQueryState;
            int hashCode3 = (hashCode2 + (queryState != null ? queryState.hashCode() : 0)) * 31;
            StoreSubscriptions.SubscriptionsState subscriptionsState = this.subscriptionsState;
            if (subscriptionsState != null) {
                i = subscriptionsState.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(skuDetailsState=");
            R.append(this.skuDetailsState);
            R.append(", purchasesState=");
            R.append(this.purchasesState);
            R.append(", purchasesQueryState=");
            R.append(this.purchasesQueryState);
            R.append(", subscriptionsState=");
            R.append(this.subscriptionsState);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: ChoosePlanViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Loading", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: ChoosePlanViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001BU\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u0012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005\u0012\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00100\u0005\u0012\u0006\u0010\u001a\u001a\u00020\u0012¢\u0006\u0004\b/\u00100J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001c\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005HÆ\u0003¢\u0006\u0004\b\u000f\u0010\bJ\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u0005HÆ\u0003¢\u0006\u0004\b\u0011\u0010\bJ\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014Jj\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0014\b\u0002\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t2\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00052\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00100\u00052\b\b\u0002\u0010\u001a\u001a\u00020\u0012HÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010$\u001a\u00020\u00122\b\u0010#\u001a\u0004\u0018\u00010\"HÖ\u0003¢\u0006\u0004\b$\u0010%R%\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\rR\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\bR\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b*\u0010\bR\u001f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00100\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010(\u001a\u0004\b+\u0010\bR\u0019\u0010\u001a\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b\u001a\u0010\u0014R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010-\u001a\u0004\b.\u0010\u0004¨\u00061"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "component1", "()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "", "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;", "component2", "()Ljava/util/List;", "", "", "Lcom/android/billingclient/api/SkuDetails;", "component3", "()Ljava/util/Map;", "Lcom/android/billingclient/api/Purchase;", "component4", "Lcom/discord/models/domain/ModelSubscription;", "component5", "", "component6", "()Z", "purchasesQueryState", "items", "skuDetails", "purchases", "subscriptions", "isEmpty", "copy", "(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Z)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getSkuDetails", "Ljava/util/List;", "getItems", "getPurchases", "getSubscriptions", "Z", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "getPurchasesQueryState", HookHelper.constructorName, "(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean isEmpty;
            private final List<WidgetChoosePlanAdapter.Item> items;
            private final List<Purchase> purchases;
            private final StoreGooglePlayPurchases.QueryState purchasesQueryState;
            private final Map<String, SkuDetails> skuDetails;
            private final List<ModelSubscription> subscriptions;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(StoreGooglePlayPurchases.QueryState queryState, List<? extends WidgetChoosePlanAdapter.Item> list, Map<String, ? extends SkuDetails> map, List<? extends Purchase> list2, List<ModelSubscription> list3, boolean z2) {
                super(null);
                m.checkNotNullParameter(queryState, "purchasesQueryState");
                m.checkNotNullParameter(list, "items");
                m.checkNotNullParameter(map, "skuDetails");
                m.checkNotNullParameter(list2, "purchases");
                m.checkNotNullParameter(list3, "subscriptions");
                this.purchasesQueryState = queryState;
                this.items = list;
                this.skuDetails = map;
                this.purchases = list2;
                this.subscriptions = list3;
                this.isEmpty = z2;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, StoreGooglePlayPurchases.QueryState queryState, List list, Map map, List list2, List list3, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    queryState = loaded.purchasesQueryState;
                }
                List<WidgetChoosePlanAdapter.Item> list4 = list;
                if ((i & 2) != 0) {
                    list4 = loaded.items;
                }
                List list5 = list4;
                Map<String, SkuDetails> map2 = map;
                if ((i & 4) != 0) {
                    map2 = loaded.skuDetails;
                }
                Map map3 = map2;
                List<Purchase> list6 = list2;
                if ((i & 8) != 0) {
                    list6 = loaded.purchases;
                }
                List list7 = list6;
                List<ModelSubscription> list8 = list3;
                if ((i & 16) != 0) {
                    list8 = loaded.subscriptions;
                }
                List list9 = list8;
                if ((i & 32) != 0) {
                    z2 = loaded.isEmpty;
                }
                return loaded.copy(queryState, list5, map3, list7, list9, z2);
            }

            public final StoreGooglePlayPurchases.QueryState component1() {
                return this.purchasesQueryState;
            }

            public final List<WidgetChoosePlanAdapter.Item> component2() {
                return this.items;
            }

            public final Map<String, SkuDetails> component3() {
                return this.skuDetails;
            }

            public final List<Purchase> component4() {
                return this.purchases;
            }

            public final List<ModelSubscription> component5() {
                return this.subscriptions;
            }

            public final boolean component6() {
                return this.isEmpty;
            }

            public final Loaded copy(StoreGooglePlayPurchases.QueryState queryState, List<? extends WidgetChoosePlanAdapter.Item> list, Map<String, ? extends SkuDetails> map, List<? extends Purchase> list2, List<ModelSubscription> list3, boolean z2) {
                m.checkNotNullParameter(queryState, "purchasesQueryState");
                m.checkNotNullParameter(list, "items");
                m.checkNotNullParameter(map, "skuDetails");
                m.checkNotNullParameter(list2, "purchases");
                m.checkNotNullParameter(list3, "subscriptions");
                return new Loaded(queryState, list, map, list2, list3, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.purchasesQueryState, loaded.purchasesQueryState) && m.areEqual(this.items, loaded.items) && m.areEqual(this.skuDetails, loaded.skuDetails) && m.areEqual(this.purchases, loaded.purchases) && m.areEqual(this.subscriptions, loaded.subscriptions) && this.isEmpty == loaded.isEmpty;
            }

            public final List<WidgetChoosePlanAdapter.Item> getItems() {
                return this.items;
            }

            public final List<Purchase> getPurchases() {
                return this.purchases;
            }

            public final StoreGooglePlayPurchases.QueryState getPurchasesQueryState() {
                return this.purchasesQueryState;
            }

            public final Map<String, SkuDetails> getSkuDetails() {
                return this.skuDetails;
            }

            public final List<ModelSubscription> getSubscriptions() {
                return this.subscriptions;
            }

            public int hashCode() {
                StoreGooglePlayPurchases.QueryState queryState = this.purchasesQueryState;
                int i = 0;
                int hashCode = (queryState != null ? queryState.hashCode() : 0) * 31;
                List<WidgetChoosePlanAdapter.Item> list = this.items;
                int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
                Map<String, SkuDetails> map = this.skuDetails;
                int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
                List<Purchase> list2 = this.purchases;
                int hashCode4 = (hashCode3 + (list2 != null ? list2.hashCode() : 0)) * 31;
                List<ModelSubscription> list3 = this.subscriptions;
                if (list3 != null) {
                    i = list3.hashCode();
                }
                int i2 = (hashCode4 + i) * 31;
                boolean z2 = this.isEmpty;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public final boolean isEmpty() {
                return this.isEmpty;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(purchasesQueryState=");
                R.append(this.purchasesQueryState);
                R.append(", items=");
                R.append(this.items);
                R.append(", skuDetails=");
                R.append(this.skuDetails);
                R.append(", purchases=");
                R.append(this.purchases);
                R.append(", subscriptions=");
                R.append(this.subscriptions);
                R.append(", isEmpty=");
                return a.M(R, this.isEmpty, ")");
            }
        }

        /* compiled from: ChoosePlanViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loading;", "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            WidgetChoosePlan.ViewType.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[WidgetChoosePlan.ViewType.BUY_PREMIUM_TIER_2.ordinal()] = 1;
            iArr[WidgetChoosePlan.ViewType.BUY_PREMIUM_TIER_1.ordinal()] = 2;
            iArr[WidgetChoosePlan.ViewType.BUY_PREMIUM_GUILD.ordinal()] = 3;
            iArr[WidgetChoosePlan.ViewType.SWITCH_PLANS.ordinal()] = 4;
        }
    }

    public /* synthetic */ ChoosePlanViewModel(WidgetChoosePlan.ViewType viewType, String str, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(viewType, str, (i & 4) != 0 ? Companion.observeStores() : observable);
    }

    public static /* synthetic */ void buy$default(ChoosePlanViewModel choosePlanViewModel, GooglePlaySku googlePlaySku, String str, Traits.Location location, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        choosePlanViewModel.buy(googlePlaySku, str, location, str2);
    }

    private final void fetchData() {
        GooglePlayBillingManager googlePlayBillingManager = GooglePlayBillingManager.INSTANCE;
        googlePlayBillingManager.queryPurchases();
        googlePlayBillingManager.querySkuDetails();
    }

    private final List<WidgetChoosePlanAdapter.Item> getCurrentPlanItems(Map<String, ? extends SkuDetails> map) {
        GooglePlaySku.Companion companion;
        GooglePlaySku fromSkuName;
        String str = this.oldSkuName;
        if (str == null || (fromSkuName = (companion = GooglePlaySku.Companion).fromSkuName(str)) == null) {
            return n.emptyList();
        }
        SkuDetails skuDetails = map.get(this.oldSkuName);
        if (skuDetails == null) {
            return n.emptyList();
        }
        GooglePlaySku upgrade = fromSkuName.getUpgrade();
        GooglePlaySku upgrade2 = fromSkuName.getUpgrade();
        GooglePlaySku googlePlaySku = null;
        SkuDetails skuDetails2 = map.get(upgrade2 != null ? upgrade2.getSkuName() : null);
        GooglePlaySku downgrade = companion.getDowngrade(this.oldSkuName);
        SkuDetails skuDetails3 = map.get(downgrade != null ? downgrade.getSkuName() : null);
        if (upgrade != null && skuDetails2 != null) {
            googlePlaySku = fromSkuName;
        } else if (!(downgrade == null || skuDetails3 == null)) {
            googlePlaySku = downgrade;
        }
        return n.listOf((Object[]) new WidgetChoosePlanAdapter.Item[]{new WidgetChoosePlanAdapter.Item.Header(R.string.billing_switch_plan_current_plan), new WidgetChoosePlanAdapter.Item.Plan(fromSkuName, skuDetails, googlePlaySku, this.oldSkuName, skuDetails2, true), new WidgetChoosePlanAdapter.Item.Divider()});
    }

    private final List<WidgetChoosePlanAdapter.Item> getGuildBoostPlans(Map<String, ? extends SkuDetails> map, SubscriptionInterval subscriptionInterval) {
        GooglePlaySku fromSkuName;
        String str = this.oldSkuName;
        if (str == null || (fromSkuName = GooglePlaySku.Companion.fromSkuName(str)) == null) {
            GooglePlaySku[] values = GooglePlaySku.values();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 19; i++) {
                GooglePlaySku googlePlaySku = values[i];
                if (googlePlaySku.getInterval() == subscriptionInterval && googlePlaySku.getPremiumSubscriptionCount() > 0) {
                    arrayList.add(googlePlaySku);
                }
            }
            return getPlansWithHeaders(arrayList, map);
        }
        GooglePlaySku[] values2 = GooglePlaySku.values();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < 19; i2++) {
            GooglePlaySku googlePlaySku2 = values2[i2];
            if (googlePlaySku2.getInterval() == fromSkuName.getInterval() && googlePlaySku2.getPremiumSubscriptionCount() > fromSkuName.getPremiumSubscriptionCount()) {
                arrayList2.add(googlePlaySku2);
            }
        }
        return getPlansWithHeaders(arrayList2, map);
    }

    private final WidgetChoosePlanAdapter.Item.Header getHeaderForSkuSection(GooglePlaySku.Section section) {
        return new WidgetChoosePlanAdapter.Item.Header(GooglePlaySku.Section.Companion.getHeaderResource(section));
    }

    private final List<WidgetChoosePlanAdapter.Item> getItemsForViewType(WidgetChoosePlan.ViewType viewType, Map<String, ? extends SkuDetails> map, SubscriptionInterval subscriptionInterval) {
        int ordinal = viewType.ordinal();
        if (ordinal == 0) {
            GooglePlaySku[] values = GooglePlaySku.values();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 19; i++) {
                GooglePlaySku googlePlaySku = values[i];
                if (googlePlaySku.getInterval() == subscriptionInterval && GooglePlaySkuKt.isTier2(googlePlaySku)) {
                    arrayList.add(googlePlaySku);
                }
            }
            return getPlansWithHeaders(arrayList, map);
        } else if (ordinal == 1) {
            GooglePlaySku[] values2 = GooglePlaySku.values();
            ArrayList arrayList2 = new ArrayList();
            for (int i2 = 0; i2 < 19; i2++) {
                GooglePlaySku googlePlaySku2 = values2[i2];
                if (googlePlaySku2.getInterval() == subscriptionInterval && GooglePlaySkuKt.isTier1(googlePlaySku2)) {
                    arrayList2.add(googlePlaySku2);
                }
            }
            return getPlansWithHeaders(arrayList2, map);
        } else if (ordinal == 2) {
            return getGuildBoostPlans(map, subscriptionInterval);
        } else {
            if (ordinal == 3) {
                GooglePlaySku[] values3 = GooglePlaySku.values();
                ArrayList arrayList3 = new ArrayList();
                for (int i3 = 0; i3 < 19; i3++) {
                    GooglePlaySku googlePlaySku3 = values3[i3];
                    if (googlePlaySku3.getInterval() == subscriptionInterval) {
                        arrayList3.add(googlePlaySku3);
                    }
                }
                return getPlansWithHeaders(arrayList3, map);
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    public static /* synthetic */ List getItemsForViewType$default(ChoosePlanViewModel choosePlanViewModel, WidgetChoosePlan.ViewType viewType, Map map, SubscriptionInterval subscriptionInterval, int i, Object obj) {
        if ((i & 4) != 0) {
            subscriptionInterval = SubscriptionInterval.MONTHLY;
        }
        return choosePlanViewModel.getItemsForViewType(viewType, map, subscriptionInterval);
    }

    private final WidgetChoosePlanAdapter.Item.Plan getPlanForSku(GooglePlaySku googlePlaySku, Map<String, ? extends SkuDetails> map) {
        SkuDetails skuDetails = map.get(googlePlaySku.getSkuName());
        if (skuDetails == null) {
            return null;
        }
        GooglePlaySku upgrade = googlePlaySku.getUpgrade();
        SkuDetails skuDetails2 = map.get(upgrade != null ? upgrade.getSkuName() : null);
        boolean areEqual = m.areEqual(this.oldSkuName, googlePlaySku.getSkuName());
        String str = this.oldSkuName;
        GooglePlaySku upgrade2 = googlePlaySku.getUpgrade();
        boolean z2 = m.areEqual(str, upgrade2 != null ? upgrade2.getSkuName() : null) && googlePlaySku.getType() != GooglePlaySku.Type.PREMIUM_GUILD;
        if (this.oldSkuName == null || (!areEqual && !z2)) {
            return new WidgetChoosePlanAdapter.Item.Plan(googlePlaySku, skuDetails, googlePlaySku, this.oldSkuName, skuDetails2, false, 32, null);
        }
        return null;
    }

    private final List<WidgetChoosePlanAdapter.Item> getPlansWithHeaders(List<? extends GooglePlaySku> list, Map<String, ? extends SkuDetails> map) {
        List<GooglePlaySku.Section> listOf = n.listOf((Object[]) new GooglePlaySku.Section[]{GooglePlaySku.Section.PREMIUM, GooglePlaySku.Section.PREMIUM_AND_PREMIUM_GUILD, GooglePlaySku.Section.PREMIUM_GUILD});
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(listOf, 10));
        for (GooglePlaySku.Section section : listOf) {
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : list) {
                if (GooglePlaySkuKt.getSection((GooglePlaySku) obj) == section) {
                    arrayList2.add(obj);
                }
            }
            arrayList.add(arrayList2);
        }
        ArrayList<List> arrayList3 = new ArrayList();
        for (Object obj2 : arrayList) {
            if (!((List) obj2).isEmpty()) {
                arrayList3.add(obj2);
            }
        }
        ArrayList arrayList4 = new ArrayList(o.collectionSizeOrDefault(arrayList3, 10));
        for (List<GooglePlaySku> list2 : arrayList3) {
            List listOf2 = d0.t.m.listOf(getHeaderForSkuSection(GooglePlaySkuKt.getSection((GooglePlaySku) u.first((List<? extends Object>) list2))));
            ArrayList arrayList5 = new ArrayList();
            for (GooglePlaySku googlePlaySku : list2) {
                WidgetChoosePlanAdapter.Item.Plan planForSku = getPlanForSku(googlePlaySku, map);
                if (planForSku != null) {
                    arrayList5.add(planForSku);
                }
            }
            arrayList4.add(u.plus((Collection) listOf2, (Iterable) arrayList5));
        }
        return o.flatten(arrayList4);
    }

    private final int getProrationMode(SkuDetails skuDetails, SkuDetails skuDetails2) {
        return (skuDetails2.c() > skuDetails.c() ? 1 : (skuDetails2.c() == skuDetails.c() ? 0 : -1)) < 0 ? 4 : 2;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(StoreGooglePlayPurchases.Event event) {
        String a;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded == null) {
            return;
        }
        if (event instanceof StoreGooglePlayPurchases.Event.PurchaseQuerySuccess) {
            StoreGooglePlayPurchases.Event.PurchaseQuerySuccess purchaseQuerySuccess = (StoreGooglePlayPurchases.Event.PurchaseQuerySuccess) event;
            SkuDetails skuDetails = loaded.getSkuDetails().get(purchaseQuerySuccess.getNewSkuName());
            if (skuDetails != null && (a = skuDetails.a()) != null) {
                m.checkNotNullExpressionValue(a, "loadedViewState.skuDetai…e]?.description ?: return");
                AnalyticsTracker.INSTANCE.paymentFlowCompleted(this.locationTrait, (r13 & 2) != 0 ? null : Traits.Subscription.Companion.withGatewayPlanId(purchaseQuerySuccess.getNewSkuName()), (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? null : null);
                this.eventSubject.k.onNext(new Event.CompleteSkuPurchase(purchaseQuerySuccess.getNewSkuName(), a));
            }
        } else if (event instanceof StoreGooglePlayPurchases.Event.PurchaseQueryFailure) {
            StoreGooglePlayPurchases.Event.PurchaseQueryFailure purchaseQueryFailure = (StoreGooglePlayPurchases.Event.PurchaseQueryFailure) event;
            AnalyticsTracker.paymentFlowFailed$default(AnalyticsTracker.INSTANCE, this.locationTrait, Traits.Subscription.Companion.withGatewayPlanId(purchaseQueryFailure.getNewSkuName()), null, null, 12, null);
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("Purchase query failure. ");
            R.append(purchaseQueryFailure.getNewSkuName());
            Logger.e$default(appLog, R.toString(), new Exception(), null, 4, null);
            this.eventSubject.k.onNext(new Event.ErrorSkuPurchase(R.string.billing_error_purchase));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Object obj;
        if (!(storeState.getPurchasesState() instanceof StoreGooglePlayPurchases.State.Loaded) || !(storeState.getSkuDetailsState() instanceof StoreGooglePlaySkuDetails.State.Loaded) || !(storeState.getSubscriptionsState() instanceof StoreSubscriptions.SubscriptionsState.Loaded)) {
            obj = ViewState.Loading.INSTANCE;
        } else {
            List<WidgetChoosePlanAdapter.Item> currentPlanItems = getCurrentPlanItems(((StoreGooglePlaySkuDetails.State.Loaded) storeState.getSkuDetailsState()).getSkuDetails());
            List itemsForViewType$default = getItemsForViewType$default(this, this.viewType, ((StoreGooglePlaySkuDetails.State.Loaded) storeState.getSkuDetailsState()).getSkuDetails(), null, 4, null);
            obj = new ViewState.Loaded(storeState.getPurchasesQueryState(), u.filterNotNull(u.plus((Collection) currentPlanItems, (Iterable) itemsForViewType$default)), ((StoreGooglePlaySkuDetails.State.Loaded) storeState.getSkuDetailsState()).getSkuDetails(), ((StoreGooglePlayPurchases.State.Loaded) storeState.getPurchasesState()).getPurchases(), ((StoreSubscriptions.SubscriptionsState.Loaded) storeState.getSubscriptionsState()).getSubscriptions(), itemsForViewType$default.isEmpty());
        }
        updateViewState(obj);
    }

    public final void buy(GooglePlaySku googlePlaySku, String str, Traits.Location location, String str2) {
        m.checkNotNullParameter(googlePlaySku, "sku");
        m.checkNotNullParameter(location, "locationTrait");
        m.checkNotNullParameter(str2, "fromStep");
        ViewState viewState = getViewState();
        String str3 = null;
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            Map<String, SkuDetails> skuDetails = loaded.getSkuDetails();
            SkuDetails skuDetails2 = skuDetails.get(googlePlaySku.getSkuName());
            SkuDetails skuDetails3 = skuDetails.get(str);
            Traits.Subscription withGatewayPlanId = Traits.Subscription.Companion.withGatewayPlanId(googlePlaySku.getSkuName());
            this.locationTrait = location;
            boolean z2 = true;
            if (skuDetails2 == null || (str != null && skuDetails3 == null)) {
                AnalyticsTracker.paymentFlowFailed$default(AnalyticsTracker.INSTANCE, location, withGatewayPlanId, null, null, 12, null);
                AppLog appLog = AppLog.g;
                StringBuilder sb = new StringBuilder();
                sb.append("Purchase details not found.");
                sb.append("oldSku: ");
                sb.append(str);
                sb.append("; hasOldSkuDetails: ");
                sb.append(skuDetails3 == null);
                sb.append("; ");
                sb.append("hasNewSkuDetails: ");
                if (skuDetails2 != null) {
                    z2 = false;
                }
                sb.append(z2);
                Logger.e$default(appLog, sb.toString(), new Exception(), null, 4, null);
                this.eventSubject.k.onNext(new Event.ErrorSkuPurchase(R.string.billing_error_purchase_details_not_found));
                return;
            }
            BillingFlowParams.a aVar = new BillingFlowParams.a();
            ArrayList<SkuDetails> arrayList = new ArrayList<>();
            arrayList.add(skuDetails2);
            aVar.d = arrayList;
            m.checkNotNullExpressionValue(aVar, "BillingFlowParams.newBui…SkuDetails(newSkuDetails)");
            if (!(str == null || skuDetails3 == null || !(!m.areEqual(googlePlaySku.getSkuName(), str)))) {
                PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
                Purchase findPurchaseForSkuName = premiumUtils.findPurchaseForSkuName(loaded.getPurchases(), str);
                if (findPurchaseForSkuName == null) {
                    AnalyticsTracker.paymentFlowFailed$default(AnalyticsTracker.INSTANCE, location, withGatewayPlanId, null, null, 12, null);
                    AppLog appLog2 = AppLog.g;
                    StringBuilder W = a.W("Subscription without matching purchase. oldSkuName: ", str, "; skuName: ");
                    W.append(googlePlaySku.getSkuName());
                    Logger.e$default(appLog2, W.toString(), new Exception(), null, 4, null);
                    this.eventSubject.k.onNext(new Event.ErrorSkuPurchase(R.string.billing_error_purchase));
                    return;
                }
                int prorationMode = getProrationMode(skuDetails3, skuDetails2);
                c cVar = new c();
                m.checkNotNullExpressionValue(cVar, "BillingFlowParams.Subscr…UpdateParams.newBuilder()");
                String a = findPurchaseForSkuName.a();
                cVar.a = a;
                cVar.f445b = prorationMode;
                if (!TextUtils.isEmpty(a) || !TextUtils.isEmpty(null)) {
                    String str4 = cVar.a;
                    int i = cVar.f445b;
                    aVar.f1998b = str4;
                    aVar.c = i;
                    if (prorationMode == 4) {
                        ModelSubscription findSubscriptionForSku = premiumUtils.findSubscriptionForSku(loaded.getSubscriptions(), str);
                        if (findSubscriptionForSku != null) {
                            str3 = findSubscriptionForSku.getId();
                        }
                        if (str3 == null) {
                            AnalyticsTracker.paymentFlowFailed$default(AnalyticsTracker.INSTANCE, location, withGatewayPlanId, null, null, 12, null);
                            AppLog appLog3 = AppLog.g;
                            StringBuilder W2 = a.W("No premium subscription for downgrade found. oldSkuName: ", str, "; skuName: ");
                            W2.append(googlePlaySku.getSkuName());
                            Logger.e$default(appLog3, W2.toString(), new Exception(), null, 4, null);
                            this.eventSubject.k.onNext(new Event.ErrorSkuPurchase(R.string.billing_error_purchase));
                            return;
                        }
                        StoreGooglePlayPurchases googlePlayPurchases = StoreStream.Companion.getGooglePlayPurchases();
                        String a2 = findPurchaseForSkuName.a();
                        m.checkNotNullExpressionValue(a2, "purchase.purchaseToken");
                        googlePlayPurchases.updatePendingDowngrade(new PendingDowngrade(a2, str3, googlePlaySku.getSkuName()));
                    }
                } else {
                    throw new IllegalArgumentException("Old SKU purchase token/id must be provided.");
                }
            }
            String obfuscatedUserId = UserUtils.INSTANCE.getObfuscatedUserId(Long.valueOf(StoreStream.Companion.getUsers().getMeInternal$app_productionGoogleRelease().getId()));
            if (obfuscatedUserId != null) {
                aVar.a = obfuscatedUserId;
            }
            AnalyticsTracker.INSTANCE.paymentFlowStep(location, (r16 & 2) != 0 ? null : withGatewayPlanId, "external_payment", str2, (r16 & 16) != 0 ? null : null, (r16 & 32) != 0 ? null : null);
            PublishSubject<Event> publishSubject = this.eventSubject;
            BillingFlowParams a3 = aVar.a();
            m.checkNotNullExpressionValue(a3, "builder.build()");
            publishSubject.k.onNext(new Event.StartSkuPurchase(a3));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChoosePlanViewModel(WidgetChoosePlan.ViewType viewType, String str, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(viewType, "viewType");
        m.checkNotNullParameter(observable, "storeObservable");
        this.viewType = viewType;
        this.oldSkuName = str;
        this.eventSubject = PublishSubject.k0();
        fetchData();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), ChoosePlanViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreStream.Companion.getGooglePlayPurchases().observeEvents(), this, null, 2, null), ChoosePlanViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(this));
    }
}
