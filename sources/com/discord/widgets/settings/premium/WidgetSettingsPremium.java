package com.discord.widgets.settings.premium;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.i.w2;
import b.a.i.x2;
import b.a.k.b;
import b.d.b.a.a;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.discord.api.premium.SubscriptionInterval;
import com.discord.api.premium.SubscriptionPlan;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetSettingsPremiumBinding;
import com.discord.models.domain.ModelEntitlement;
import com.discord.models.domain.ModelSubscription;
import com.discord.models.domain.billing.ModelInvoiceItem;
import com.discord.models.domain.billing.ModelInvoicePreview;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.stores.StoreStream;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlaySku;
import com.discord.utilities.billing.PremiumUtilsKt;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.settings.premium.SettingsPremiumViewModel;
import com.discord.widgets.settings.premium.WidgetChoosePlan;
import com.google.android.material.button.MaterialButton;
import d0.o;
import d0.t.g0;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetSettingsPremium.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 ^2\u00020\u0001:\u0001^B\u0007¢\u0006\u0004\b]\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0019\u0010\u0007\u001a\u00020\u00022\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J'\u0010\u0014\u001a\u00020\u00022\u0016\b\u0002\u0010\u000e\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\r\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J/\u0010\u0018\u001a\u00020\u00022\b\u0010\u0017\u001a\u0004\u0018\u00010\u00162\u0014\u0010\u000e\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\r\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001a\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u001a\u0010\fJ\u0019\u0010\u001b\u001a\u00020\u00022\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ7\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0017\u001a\u0004\u0018\u00010\u00162\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\r0\u00122\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dH\u0002¢\u0006\u0004\b\u0010\u0010\u001fJ-\u0010&\u001a\u00020%2\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010!\u001a\u00020 2\f\u0010$\u001a\b\u0012\u0004\u0012\u00020#0\"H\u0002¢\u0006\u0004\b&\u0010'J!\u0010*\u001a\u00020%2\u0006\u0010(\u001a\u00020 2\b\u0010)\u001a\u0004\u0018\u00010\u0013H\u0002¢\u0006\u0004\b*\u0010+J\u0017\u0010,\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b,\u0010\fJ'\u00100\u001a\u00020\u00022\f\u0010.\u001a\b\u0012\u0004\u0012\u00020-0\"2\b\u0010/\u001a\u0004\u0018\u00010\u0016H\u0002¢\u0006\u0004\b0\u00101J\u000f\u00102\u001a\u00020\u0002H\u0002¢\u0006\u0004\b2\u0010\u0004J\u000f\u00103\u001a\u00020\u0002H\u0002¢\u0006\u0004\b3\u0010\u0004J\u0017\u00106\u001a\u00020\u00022\u0006\u00105\u001a\u000204H\u0002¢\u0006\u0004\b6\u00107J\u000f\u00108\u001a\u00020\u0002H\u0002¢\u0006\u0004\b8\u0010\u0004J\u0017\u0010;\u001a\u00020\u00022\u0006\u0010:\u001a\u000209H\u0002¢\u0006\u0004\b;\u0010<J\u0017\u0010=\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b=\u0010\fJ-\u0010?\u001a\u0004\u0018\u00010\u00132\u0006\u0010>\u001a\u00020\u00162\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\r0\u0012H\u0002¢\u0006\u0004\b?\u0010@J/\u0010B\u001a\u00020\u00022\b\u0010>\u001a\u0004\u0018\u00010\u00162\u0014\u0010A\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\r\u0018\u00010\u0012H\u0002¢\u0006\u0004\bB\u0010\u0019J/\u0010C\u001a\u00020\u00022\b\u0010>\u001a\u0004\u0018\u00010\u00162\u0014\u0010A\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\r\u0018\u00010\u0012H\u0002¢\u0006\u0004\bC\u0010\u0019J\u0019\u0010F\u001a\u00020\u000f2\b\u0010E\u001a\u0004\u0018\u00010DH\u0002¢\u0006\u0004\bF\u0010GJ\u0017\u0010J\u001a\u00020\u00022\u0006\u0010I\u001a\u00020HH\u0016¢\u0006\u0004\bJ\u0010KJ\u000f\u0010L\u001a\u00020\u0002H\u0016¢\u0006\u0004\bL\u0010\u0004R\u001d\u0010:\u001a\u0002098B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bM\u0010N\u001a\u0004\bO\u0010PR\u0016\u0010R\u001a\u00020Q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bR\u0010SR\u0016\u0010V\u001a\u00020\u00138B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bT\u0010UR\u001d\u0010\\\u001a\u00020W8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bX\u0010Y\u001a\u0004\bZ\u0010[¨\u0006_"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;", "Lcom/discord/app/AppFragment;", "", "scrollToTop", "()V", "", "section", "scrollToSection", "(Ljava/lang/Integer;)V", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;", "model", "showContent", "(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V", "Lcom/android/billingclient/api/SkuDetails;", "skuDetails", "", "getPriceText", "(Lcom/android/billingclient/api/SkuDetails;)Ljava/lang/CharSequence;", "", "", "configureButtonText", "(Ljava/util/Map;)V", "Lcom/discord/models/domain/ModelSubscription;", "premiumSubscription", "configureButtons", "(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)V", "configureLegalese", "configureGrandfatheredHeader", "(Lcom/discord/models/domain/ModelSubscription;)V", "Lcom/discord/models/domain/billing/ModelInvoiceItem;", "invoiceItem", "(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;Lcom/discord/models/domain/billing/ModelInvoiceItem;)Ljava/lang/CharSequence;", "", "canManageGuildBoosts", "", "Lcom/android/billingclient/api/Purchase;", "purchases", "Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;", "getPremiumSubscriptionViewCallbacks", "(Lcom/discord/models/domain/ModelSubscription;ZLjava/util/List;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;", "isGoogleGuildBoost", "skuName", "getGuildBoostViewCallbacks", "(ZLjava/lang/String;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;", "configureActiveSubscriptionView", "Lcom/discord/models/domain/ModelEntitlement;", "entitlements", "currentSubscription", "configureAccountCredit", "(Ljava/util/List;Lcom/discord/models/domain/ModelSubscription;)V", "showLoadingUI", "showFailureUI", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;)V", "showDesktopManageAlert", "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;", "viewModel", "showCancelConfirmationAlert", "(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V", "configurePaymentInfo", Traits.Payment.Type.SUBSCRIPTION, "getGoogleSubscriptionRenewalPrice", "(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)Ljava/lang/String;", "skuDetailsMap", "configurePriceChangeNotice", "configureStatusNotice", "Lcom/discord/models/domain/premium/SubscriptionPlanType;", "planType", "getPlanString", "(Lcom/discord/models/domain/premium/SubscriptionPlanType;)Ljava/lang/CharSequence;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;", "Lcom/discord/utilities/locale/LocaleManager;", "localeManager", "Lcom/discord/utilities/locale/LocaleManager;", "getAnalyticsLocationSection", "()Ljava/lang/String;", "analyticsLocationSection", "Lcom/discord/databinding/WidgetSettingsPremiumBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsPremiumBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPremium extends AppFragment {
    private static final String ANALYTICS_LOCATION_SECTION = "analytics_location_section";
    private static final String INTENT_SCROLL_TO_SECTION = "intent_section";
    public static final int SECTION_NITRO = 1;
    public static final int SECTION_NITRO_CLASSIC = 0;
    private static final int VIEW_INDEX_CONTENT = 0;
    private static final int VIEW_INDEX_ERROR = 2;
    private static final int VIEW_INDEX_LOADING = 1;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsPremium$binding$2.INSTANCE, null, 2, null);
    private final LocaleManager localeManager = new LocaleManager();
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsPremium.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsPremiumBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetSettingsPremium.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u000f\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0016B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J-\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\fR\u0016\u0010\u000e\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000fR\u0016\u0010\u0012\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u000fR\u0016\u0010\u0013\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u000f¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;", "", "Landroid/content/Context;", "context", "", "scrollToSection", "", "locationSection", "", "launch", "(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/String;)V", "ANALYTICS_LOCATION_SECTION", "Ljava/lang/String;", "INTENT_SCROLL_TO_SECTION", "SECTION_NITRO", "I", "SECTION_NITRO_CLASSIC", "VIEW_INDEX_CONTENT", "VIEW_INDEX_ERROR", "VIEW_INDEX_LOADING", HookHelper.constructorName, "()V", "SubscriptionViewCallbacks", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetSettingsPremium.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0082\b\u0018\u00002\u00020\u0001BW\u0012\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\u000e\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\u000e\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002¢\u0006\u0004\b!\u0010\"J\u0018\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0018\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0005J\u0018\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0005Jj\u0010\u000f\u001a\u00020\u00002\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\u0010\b\u0002\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\u0010\b\u0002\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR!\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R!\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001d\u0010\u0005R!\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001b\u001a\u0004\b\u001e\u0010\u0005R!\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001f\u0010\u0005R!\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b \u0010\u0005¨\u0006#"}, d2 = {"Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;", "", "Lkotlin/Function0;", "", "component1", "()Lkotlin/jvm/functions/Function0;", "component2", "component3", "component4", "component5", "restoreCallback", "managePlanCallback", "cancelCallback", "manageGuildBoostCallback", "manageBillingCallback", "copy", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/jvm/functions/Function0;", "getCancelCallback", "getManageGuildBoostCallback", "getRestoreCallback", "getManagePlanCallback", "getManageBillingCallback", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SubscriptionViewCallbacks {
            private final Function0<Unit> cancelCallback;
            private final Function0<Unit> manageBillingCallback;
            private final Function0<Unit> manageGuildBoostCallback;
            private final Function0<Unit> managePlanCallback;
            private final Function0<Unit> restoreCallback;

            public SubscriptionViewCallbacks(Function0<Unit> function0, Function0<Unit> function02, Function0<Unit> function03, Function0<Unit> function04, Function0<Unit> function05) {
                this.restoreCallback = function0;
                this.managePlanCallback = function02;
                this.cancelCallback = function03;
                this.manageGuildBoostCallback = function04;
                this.manageBillingCallback = function05;
            }

            public static /* synthetic */ SubscriptionViewCallbacks copy$default(SubscriptionViewCallbacks subscriptionViewCallbacks, Function0 function0, Function0 function02, Function0 function03, Function0 function04, Function0 function05, int i, Object obj) {
                Function0<Unit> function06 = function0;
                if ((i & 1) != 0) {
                    function06 = subscriptionViewCallbacks.restoreCallback;
                }
                Function0<Unit> function07 = function02;
                if ((i & 2) != 0) {
                    function07 = subscriptionViewCallbacks.managePlanCallback;
                }
                Function0 function08 = function07;
                Function0<Unit> function09 = function03;
                if ((i & 4) != 0) {
                    function09 = subscriptionViewCallbacks.cancelCallback;
                }
                Function0 function010 = function09;
                Function0<Unit> function011 = function04;
                if ((i & 8) != 0) {
                    function011 = subscriptionViewCallbacks.manageGuildBoostCallback;
                }
                Function0 function012 = function011;
                Function0<Unit> function013 = function05;
                if ((i & 16) != 0) {
                    function013 = subscriptionViewCallbacks.manageBillingCallback;
                }
                return subscriptionViewCallbacks.copy(function06, function08, function010, function012, function013);
            }

            public final Function0<Unit> component1() {
                return this.restoreCallback;
            }

            public final Function0<Unit> component2() {
                return this.managePlanCallback;
            }

            public final Function0<Unit> component3() {
                return this.cancelCallback;
            }

            public final Function0<Unit> component4() {
                return this.manageGuildBoostCallback;
            }

            public final Function0<Unit> component5() {
                return this.manageBillingCallback;
            }

            public final SubscriptionViewCallbacks copy(Function0<Unit> function0, Function0<Unit> function02, Function0<Unit> function03, Function0<Unit> function04, Function0<Unit> function05) {
                return new SubscriptionViewCallbacks(function0, function02, function03, function04, function05);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SubscriptionViewCallbacks)) {
                    return false;
                }
                SubscriptionViewCallbacks subscriptionViewCallbacks = (SubscriptionViewCallbacks) obj;
                return m.areEqual(this.restoreCallback, subscriptionViewCallbacks.restoreCallback) && m.areEqual(this.managePlanCallback, subscriptionViewCallbacks.managePlanCallback) && m.areEqual(this.cancelCallback, subscriptionViewCallbacks.cancelCallback) && m.areEqual(this.manageGuildBoostCallback, subscriptionViewCallbacks.manageGuildBoostCallback) && m.areEqual(this.manageBillingCallback, subscriptionViewCallbacks.manageBillingCallback);
            }

            public final Function0<Unit> getCancelCallback() {
                return this.cancelCallback;
            }

            public final Function0<Unit> getManageBillingCallback() {
                return this.manageBillingCallback;
            }

            public final Function0<Unit> getManageGuildBoostCallback() {
                return this.manageGuildBoostCallback;
            }

            public final Function0<Unit> getManagePlanCallback() {
                return this.managePlanCallback;
            }

            public final Function0<Unit> getRestoreCallback() {
                return this.restoreCallback;
            }

            public int hashCode() {
                Function0<Unit> function0 = this.restoreCallback;
                int i = 0;
                int hashCode = (function0 != null ? function0.hashCode() : 0) * 31;
                Function0<Unit> function02 = this.managePlanCallback;
                int hashCode2 = (hashCode + (function02 != null ? function02.hashCode() : 0)) * 31;
                Function0<Unit> function03 = this.cancelCallback;
                int hashCode3 = (hashCode2 + (function03 != null ? function03.hashCode() : 0)) * 31;
                Function0<Unit> function04 = this.manageGuildBoostCallback;
                int hashCode4 = (hashCode3 + (function04 != null ? function04.hashCode() : 0)) * 31;
                Function0<Unit> function05 = this.manageBillingCallback;
                if (function05 != null) {
                    i = function05.hashCode();
                }
                return hashCode4 + i;
            }

            public String toString() {
                StringBuilder R = a.R("SubscriptionViewCallbacks(restoreCallback=");
                R.append(this.restoreCallback);
                R.append(", managePlanCallback=");
                R.append(this.managePlanCallback);
                R.append(", cancelCallback=");
                R.append(this.cancelCallback);
                R.append(", manageGuildBoostCallback=");
                R.append(this.manageGuildBoostCallback);
                R.append(", manageBillingCallback=");
                R.append(this.manageBillingCallback);
                R.append(")");
                return R.toString();
            }
        }

        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, Integer num, String str, int i, Object obj) {
            if ((i & 2) != 0) {
                num = null;
            }
            if ((i & 4) != 0) {
                str = null;
            }
            companion.launch(context, num, str);
        }

        public final void launch(Context context, Integer num, String str) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion.getAnalytics().onUserSettingsPaneViewed("Discord Nitro", str);
            j.d(context, WidgetSettingsPremium.class, new Intent().putExtra(WidgetSettingsPremium.INTENT_SCROLL_TO_SECTION, num).putExtra(WidgetSettingsPremium.ANALYTICS_LOCATION_SECTION, str));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;
        public static final /* synthetic */ int[] $EnumSwitchMapping$3;
        public static final /* synthetic */ int[] $EnumSwitchMapping$4;
        public static final /* synthetic */ int[] $EnumSwitchMapping$5;

        static {
            SubscriptionInterval.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[SubscriptionInterval.YEARLY.ordinal()] = 1;
            iArr[SubscriptionInterval.MONTHLY.ordinal()] = 2;
            SubscriptionPlanType.values();
            int[] iArr2 = new int[10];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[SubscriptionPlanType.PREMIUM_MONTH_LEGACY.ordinal()] = 1;
            iArr2[SubscriptionPlanType.PREMIUM_YEAR_LEGACY.ordinal()] = 2;
            ModelSubscription.Status.values();
            int[] iArr3 = new int[5];
            $EnumSwitchMapping$2 = iArr3;
            ModelSubscription.Status status = ModelSubscription.Status.PAST_DUE;
            iArr3[status.ordinal()] = 1;
            ModelSubscription.Status status2 = ModelSubscription.Status.ACCOUNT_HOLD;
            iArr3[status2.ordinal()] = 2;
            ModelSubscription.Status.values();
            int[] iArr4 = new int[5];
            $EnumSwitchMapping$3 = iArr4;
            iArr4[status.ordinal()] = 1;
            iArr4[status2.ordinal()] = 2;
            ModelSubscription.Status.values();
            int[] iArr5 = new int[5];
            $EnumSwitchMapping$4 = iArr5;
            iArr5[ModelSubscription.Status.ACTIVE.ordinal()] = 1;
            iArr5[ModelSubscription.Status.CANCELED.ordinal()] = 2;
            iArr5[status.ordinal()] = 3;
            iArr5[status2.ordinal()] = 4;
            SubscriptionPlanType.values();
            int[] iArr6 = new int[10];
            $EnumSwitchMapping$5 = iArr6;
            iArr6[SubscriptionPlanType.PREMIUM_MONTH_TIER_1.ordinal()] = 1;
            iArr6[SubscriptionPlanType.PREMIUM_YEAR_TIER_1.ordinal()] = 2;
            iArr6[SubscriptionPlanType.PREMIUM_MONTH_TIER_2.ordinal()] = 3;
            iArr6[SubscriptionPlanType.PREMIUM_YEAR_TIER_2.ordinal()] = 4;
        }
    }

    public WidgetSettingsPremium() {
        super(R.layout.widget_settings_premium);
        WidgetSettingsPremium$viewModel$2 widgetSettingsPremium$viewModel$2 = WidgetSettingsPremium$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(SettingsPremiumViewModel.class), new WidgetSettingsPremium$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetSettingsPremium$viewModel$2));
    }

    private final void configureAccountCredit(List<ModelEntitlement> list, ModelSubscription modelSubscription) {
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        for (ModelEntitlement modelEntitlement : list) {
            if (modelEntitlement.getParentId() != null) {
                SubscriptionPlan subscriptionPlan = modelEntitlement.getSubscriptionPlan();
                Long valueOf = subscriptionPlan != null ? Long.valueOf(subscriptionPlan.a()) : null;
                long planId = SubscriptionPlanType.PREMIUM_MONTH_TIER_1.getPlanId();
                if (valueOf != null && valueOf.longValue() == planId) {
                    i2++;
                } else {
                    long planId2 = SubscriptionPlanType.PREMIUM_MONTH_TIER_2.getPlanId();
                    if (valueOf != null && valueOf.longValue() == planId2) {
                        i3++;
                    }
                }
            }
        }
        LinearLayout linearLayout = getBinding().g;
        m.checkNotNullExpressionValue(linearLayout, "binding.premiumSettingsCreditContainer");
        boolean z2 = true;
        linearLayout.setVisibility(i2 > 0 || i3 > 0 ? 0 : 8);
        View view = getBinding().f2607b.d;
        m.checkNotNullExpressionValue(view, "binding.accountCredits.creditNitroDivider");
        if (i2 <= 0 || i3 <= 0) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        view.setVisibility(i);
        getBinding().f2607b.c.a(SubscriptionPlanType.PREMIUM_MONTH_TIER_1.getPlanId(), i2, modelSubscription);
        getBinding().f2607b.f95b.a(SubscriptionPlanType.PREMIUM_MONTH_TIER_2.getPlanId(), i3, modelSubscription);
    }

    /* JADX WARN: Removed duplicated region for block: B:200:0x00b3 A[EDGE_INSN: B:200:0x00b3->B:50:0x00b3 ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0102  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0107  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x0128  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x016b  */
    /* JADX WARN: Removed duplicated region for block: B:95:0x016f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureActiveSubscriptionView(com.discord.widgets.settings.premium.SettingsPremiumViewModel.ViewState.Loaded r25) {
        /*
            Method dump skipped, instructions count: 726
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.premium.WidgetSettingsPremium.configureActiveSubscriptionView(com.discord.widgets.settings.premium.SettingsPremiumViewModel$ViewState$Loaded):void");
    }

    private final void configureButtonText(Map<String, ? extends SkuDetails> map) {
        MaterialButton materialButton = getBinding().t.f218b;
        m.checkNotNullExpressionValue(materialButton, "binding.premiumTier1.premiumSettingsPremiumClassic");
        SkuDetails skuDetails = null;
        materialButton.setText(getPriceText(map != null ? map.get(GooglePlaySku.PREMIUM_TIER_1_MONTHLY.getSkuName()) : null));
        MaterialButton materialButton2 = getBinding().u.f223b;
        m.checkNotNullExpressionValue(materialButton2, "binding.premiumTier2.premiumSettingsPremium");
        if (map != null) {
            skuDetails = map.get(GooglePlaySku.PREMIUM_TIER_2_MONTHLY.getSkuName());
        }
        materialButton2.setText(getPriceText(skuDetails));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void configureButtonText$default(WidgetSettingsPremium widgetSettingsPremium, Map map, int i, Object obj) {
        if ((i & 1) != 0) {
            map = null;
        }
        widgetSettingsPremium.configureButtonText(map);
    }

    private final void configureButtons(final ModelSubscription modelSubscription, Map<String, ? extends SkuDetails> map) {
        configureButtonText(map);
        if (modelSubscription == null || !modelSubscription.isAppleSubscription()) {
            for (final MaterialButton materialButton : n.listOf((Object[]) new MaterialButton[]{getBinding().t.f218b, getBinding().u.f223b})) {
                m.checkNotNullExpressionValue(materialButton, "button");
                materialButton.setEnabled(modelSubscription == null);
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetSettingsPremiumBinding binding;
                        WidgetChoosePlan.ViewType viewType;
                        String analyticsLocationSection;
                        WidgetSettingsPremiumBinding binding2;
                        String str;
                        WidgetChoosePlan.Companion companion = WidgetChoosePlan.Companion;
                        Context requireContext = this.requireContext();
                        MaterialButton materialButton2 = MaterialButton.this;
                        binding = this.getBinding();
                        if (m.areEqual(materialButton2, binding.t.f218b)) {
                            viewType = WidgetChoosePlan.ViewType.BUY_PREMIUM_TIER_1;
                        } else {
                            viewType = WidgetChoosePlan.ViewType.BUY_PREMIUM_TIER_2;
                        }
                        WidgetChoosePlan.ViewType viewType2 = viewType;
                        ModelSubscription modelSubscription2 = modelSubscription;
                        String paymentGatewayPlanId = modelSubscription2 != null ? modelSubscription2.getPaymentGatewayPlanId() : null;
                        analyticsLocationSection = this.getAnalyticsLocationSection();
                        Traits.Location location = new Traits.Location(Traits.Location.Page.USER_SETTINGS, analyticsLocationSection, Traits.Location.Obj.BUTTON_CTA, "buy", null, 16, null);
                        Traits.Subscription.Companion companion2 = Traits.Subscription.Companion;
                        MaterialButton materialButton3 = MaterialButton.this;
                        binding2 = this.getBinding();
                        if (m.areEqual(materialButton3, binding2.t.f218b)) {
                            str = GooglePlaySku.PREMIUM_TIER_1_MONTHLY.getSkuName();
                        } else {
                            str = GooglePlaySku.PREMIUM_TIER_2_MONTHLY.getSkuName();
                        }
                        companion.launch(requireContext, (r16 & 2) != 0 ? null : null, viewType2, (r16 & 8) != 0 ? null : paymentGatewayPlanId, location, (r16 & 32) != 0 ? null : companion2.withGatewayPlanId(str));
                    }
                });
            }
            return;
        }
        for (MaterialButton materialButton2 : n.listOf((Object[]) new MaterialButton[]{getBinding().t.f218b, getBinding().u.f223b})) {
            m.checkNotNullExpressionValue(materialButton2, "button");
            ViewExtensions.setEnabledAlpha$default(materialButton2, true, 0.0f, 2, null);
            materialButton2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    PremiumUtils.INSTANCE.openAppleBilling(WidgetSettingsPremium.this.requireContext());
                }
            });
        }
    }

    private final void configureGrandfatheredHeader(ModelSubscription modelSubscription) {
        CharSequence charSequence = null;
        SubscriptionPlanType planType = modelSubscription != null ? modelSubscription.getPlanType() : null;
        if (planType != null) {
            int ordinal = planType.ordinal();
            if (ordinal == 2) {
                charSequence = b.e(this, R.string.premium_grandfathered_monthly, new Object[]{DateFormat.getMediumDateFormat(requireContext()).format(PremiumUtilsKt.getGRANDFATHERED_MONTHLY_END_DATE())}, (r4 & 4) != 0 ? b.a.j : null);
            } else if (ordinal == 3) {
                charSequence = b.e(this, R.string.premium_grandfathered_yearly, new Object[]{DateFormat.getMediumDateFormat(requireContext()).format(PremiumUtilsKt.getGRANDFATHERED_YEARLY_END_DATE())}, (r4 & 4) != 0 ? b.a.j : null);
            }
        }
        TextView textView = getBinding().i;
        m.checkNotNullExpressionValue(textView, "binding.premiumSettingsGrandfathered");
        ViewExtensions.setTextAndVisibilityBy(textView, charSequence);
    }

    private final void configureLegalese(SettingsPremiumViewModel.ViewState.Loaded loaded) {
        int i;
        CharSequence e;
        CharSequence e2;
        ModelSubscription premiumSubscription = loaded.getPremiumSubscription();
        Map<String, SkuDetails> skuDetails = loaded.getSkuDetails();
        ModelInvoicePreview renewalInvoicePreview = loaded.getRenewalInvoicePreview();
        if (premiumSubscription == null || !premiumSubscription.getPlanType().isPremiumSubscription()) {
            TextView textView = getBinding().j;
            m.checkNotNullExpressionValue(textView, "binding.premiumSettingsLegalese");
            textView.setVisibility(8);
            return;
        }
        int ordinal = premiumSubscription.getPlanType().getInterval().ordinal();
        if (ordinal == 0) {
            i = R.string.billing_payment_premium_legalese_monthly;
        } else if (ordinal == 1) {
            i = R.string.billing_payment_premium_legalese_yearly;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        Object googleSubscriptionRenewalPrice = getGoogleSubscriptionRenewalPrice(premiumSubscription, skuDetails);
        if (googleSubscriptionRenewalPrice == null) {
            googleSubscriptionRenewalPrice = PremiumUtilsKt.getFormattedPriceUsd(renewalInvoicePreview != null ? renewalInvoicePreview.getTotal() : 0, requireContext());
        }
        TextView textView2 = getBinding().j;
        m.checkNotNullExpressionValue(textView2, "binding.premiumSettingsLegalese");
        textView2.setVisibility(0);
        TextView textView3 = getBinding().j;
        m.checkNotNullExpressionValue(textView3, "binding.premiumSettingsLegalese");
        e = b.e(this, R.string.terms_of_service_url, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e2 = b.e(this, R.string.privacy_policy_url, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        b.m(textView3, i, new Object[]{e, e2, googleSubscriptionRenewalPrice}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView4 = getBinding().j;
        m.checkNotNullExpressionValue(textView4, "binding.premiumSettingsLegalese");
        textView4.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* JADX WARN: Code restructure failed: missing block: B:33:0x0076, code lost:
        if (r7 != null) goto L35;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configurePaymentInfo(com.discord.widgets.settings.premium.SettingsPremiumViewModel.ViewState.Loaded r22) {
        /*
            Method dump skipped, instructions count: 493
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.premium.WidgetSettingsPremium.configurePaymentInfo(com.discord.widgets.settings.premium.SettingsPremiumViewModel$ViewState$Loaded):void");
    }

    private final void configurePriceChangeNotice(ModelSubscription modelSubscription, Map<String, ? extends SkuDetails> map) {
        String str;
        String str2;
        CharSequence e;
        SkuDetails skuDetails = map != null ? map.get(GooglePlaySku.PREMIUM_TIER_2_MONTHLY.getSkuName()) : null;
        if (skuDetails != null) {
            str = skuDetails.f2002b.optString("price_currency_code");
        } else {
            str = null;
        }
        Map mapOf = g0.mapOf(o.to("PLN", Integer.valueOf((int) R.string.country_name_pl)));
        Objects.requireNonNull(mapOf, "null cannot be cast to non-null type kotlin.collections.Map<K, *>");
        boolean z2 = mapOf.containsKey(str) && (modelSubscription == null || modelSubscription.isGoogleSubscription());
        CardView cardView = getBinding().k.f214b;
        m.checkNotNullExpressionValue(cardView, "binding.premiumSettingsPriceChangeNotice.card");
        cardView.setVisibility(z2 ? 0 : 8);
        if (z2) {
            int i = modelSubscription == null ? R.string.localized_pricing_mobile_price_change_notice_no_sub : R.string.localized_pricing_mobile_price_change_notice_has_sub;
            TextView textView = getBinding().k.c;
            m.checkNotNullExpressionValue(textView, "binding.premiumSettingsPriceChangeNotice.textview");
            Object[] objArr = new Object[3];
            Integer num = (Integer) mapOf.get(str);
            objArr[0] = num != null ? b.e(this, num.intValue(), new Object[0], (r4 & 4) != 0 ? b.a.j : null) : null;
            if (skuDetails != null) {
                str2 = skuDetails.b();
            } else {
                str2 = null;
            }
            objArr[1] = str2;
            objArr[2] = f.a.a(4407269525911L, null);
            e = b.e(this, i, objArr, (r4 & 4) != 0 ? b.a.j : null);
            textView.setText(e);
            getBinding().k.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsPremium$configurePriceChangeNotice$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetSettingsPremiumBinding binding;
                    UriHandler uriHandler = UriHandler.INSTANCE;
                    binding = WidgetSettingsPremium.this.getBinding();
                    TextView textView2 = binding.k.c;
                    m.checkNotNullExpressionValue(textView2, "binding.premiumSettingsPriceChangeNotice.textview");
                    Context context = textView2.getContext();
                    m.checkNotNullExpressionValue(context, "binding.premiumSettingsP…geNotice.textview.context");
                    UriHandler.handle$default(uriHandler, context, f.a.a(4407269525911L, null), null, 4, null);
                }
            });
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:64:0x00ea, code lost:
        if (r2 != null) goto L72;
     */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0046  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0048  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureStatusNotice(final com.discord.models.domain.ModelSubscription r22, java.util.Map<java.lang.String, ? extends com.android.billingclient.api.SkuDetails> r23) {
        /*
            Method dump skipped, instructions count: 374
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.premium.WidgetSettingsPremium.configureStatusNotice(com.discord.models.domain.ModelSubscription, java.util.Map):void");
    }

    public final String getAnalyticsLocationSection() {
        String string;
        Bundle extras = getMostRecentIntent().getExtras();
        return (extras == null || (string = extras.getString(ANALYTICS_LOCATION_SECTION)) == null) ? "Discord Nitro" : string;
    }

    public final WidgetSettingsPremiumBinding getBinding() {
        return (WidgetSettingsPremiumBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final String getGoogleSubscriptionRenewalPrice(ModelSubscription modelSubscription, Map<String, ? extends SkuDetails> map) {
        if (!modelSubscription.isGoogleSubscription()) {
            return null;
        }
        SkuDetails skuDetails = map.get(modelSubscription.getPaymentGatewayPlanId());
        ModelSubscription.SubscriptionRenewalMutations renewalMutations = modelSubscription.getRenewalMutations();
        SkuDetails skuDetails2 = map.get(renewalMutations != null ? renewalMutations.getPaymentGatewayPlanId() : null);
        if (modelSubscription.getRenewalMutations() != null && skuDetails2 != null) {
            return skuDetails2.b();
        }
        if (modelSubscription.getRenewalMutations() != null || skuDetails == null) {
            return null;
        }
        return skuDetails.b();
    }

    private final Companion.SubscriptionViewCallbacks getGuildBoostViewCallbacks(boolean z2, String str) {
        if (!z2) {
            return new Companion.SubscriptionViewCallbacks(null, null, null, new WidgetSettingsPremium$getGuildBoostViewCallbacks$6(this), null);
        }
        return new Companion.SubscriptionViewCallbacks(new WidgetSettingsPremium$getGuildBoostViewCallbacks$2(this, str), new WidgetSettingsPremium$getGuildBoostViewCallbacks$3(this, str), new WidgetSettingsPremium$getGuildBoostViewCallbacks$4(this, str), new WidgetSettingsPremium$getGuildBoostViewCallbacks$1(this), new WidgetSettingsPremium$getGuildBoostViewCallbacks$5(this, str));
    }

    private final CharSequence getPlanString(SubscriptionPlanType subscriptionPlanType) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        CharSequence e4;
        if (subscriptionPlanType != null) {
            int ordinal = subscriptionPlanType.ordinal();
            if (ordinal == 4) {
                e = b.e(this, R.string.premium_plan_month_tier_1, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                return e;
            } else if (ordinal == 5) {
                e2 = b.e(this, R.string.premium_plan_year_tier_1, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                return e2;
            } else if (ordinal == 6) {
                e3 = b.e(this, R.string.premium_plan_month_tier_2, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                return e3;
            } else if (ordinal == 7) {
                e4 = b.e(this, R.string.premium_plan_year_tier_2, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                return e4;
            }
        }
        return "";
    }

    private final Companion.SubscriptionViewCallbacks getPremiumSubscriptionViewCallbacks(ModelSubscription modelSubscription, boolean z2, List<? extends Purchase> list) {
        Function0 function0;
        if (modelSubscription.isGoogleSubscription()) {
            WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$manageBundledGuildBoostCallback$1 widgetSettingsPremium$getPremiumSubscriptionViewCallbacks$manageBundledGuildBoostCallback$1 = z2 ? new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$manageBundledGuildBoostCallback$1(this) : null;
            boolean z3 = true;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                for (Purchase purchase : list) {
                    if (!purchase.c()) {
                        break;
                    }
                }
            }
            z3 = false;
            if (z3) {
                function0 = new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$managePlanCallback$2(this);
            } else {
                function0 = new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$managePlanCallback$3(this, modelSubscription);
            }
            return new Companion.SubscriptionViewCallbacks(new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$1(this, modelSubscription), function0, new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$2(this, modelSubscription), widgetSettingsPremium$getPremiumSubscriptionViewCallbacks$manageBundledGuildBoostCallback$1, new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$3(this, modelSubscription));
        } else if (modelSubscription.isAppleSubscription()) {
            return new Companion.SubscriptionViewCallbacks(null, null, null, null, null);
        } else {
            return new Companion.SubscriptionViewCallbacks(null, new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$4(this), new WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$5(this), null, null);
        }
    }

    private final CharSequence getPriceText(SkuDetails skuDetails) {
        CharSequence e;
        CharSequence e2;
        if (skuDetails != null) {
            e2 = b.e(this, R.string.premium_settings_starting_at_per_month, new Object[]{skuDetails.b()}, (r4 & 4) != 0 ? b.a.j : null);
            return e2;
        }
        e = b.e(this, R.string.stream_premium_upsell_cta, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        return e;
    }

    public final SettingsPremiumViewModel getViewModel() {
        return (SettingsPremiumViewModel) this.viewModel$delegate.getValue();
    }

    public final void handleEvent(SettingsPremiumViewModel.Event event) {
        if (event instanceof SettingsPremiumViewModel.Event.ErrorToast) {
            b.a.d.m.i(this, ((SettingsPremiumViewModel.Event.ErrorToast) event).getErrorStringResId(), 0, 4);
        }
    }

    public final void scrollToSection(Integer num) {
        int i;
        if (num != null && num.intValue() == 0) {
            w2 w2Var = getBinding().t;
            m.checkNotNullExpressionValue(w2Var, "binding.premiumTier1");
            LinearLayout linearLayout = w2Var.a;
            m.checkNotNullExpressionValue(linearLayout, "binding.premiumTier1.root");
            int top = linearLayout.getTop();
            ScrollView scrollView = getBinding().m;
            m.checkNotNullExpressionValue(scrollView, "binding.premiumSettingsScrollview");
            i = scrollView.getHeight() + top;
        } else if (num != null && num.intValue() == 1) {
            x2 x2Var = getBinding().u;
            m.checkNotNullExpressionValue(x2Var, "binding.premiumTier2");
            LinearLayout linearLayout2 = x2Var.a;
            m.checkNotNullExpressionValue(linearLayout2, "binding.premiumTier2.root");
            i = linearLayout2.getTop();
        } else {
            i = 0;
        }
        getBinding().m.scrollTo(0, i);
    }

    private final void scrollToTop() {
        getBinding().m.scrollTo(0, 0);
    }

    public final void showCancelConfirmationAlert(SettingsPremiumViewModel settingsPremiumViewModel) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        CharSequence e4;
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        e = b.e(this, R.string.premium_cancel_confirm_header, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e2 = b.e(this, R.string.premium_cancel_confirm_body, new Object[]{f.a.a(360055386693L, null)}, (r4 & 4) != 0 ? b.a.j : null);
        e3 = b.e(this, R.string.premium_cancel_confirm_button, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e4 = b.e(this, R.string.nevermind, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, e, e2, e3, e4, g0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new WidgetSettingsPremium$showCancelConfirmationAlert$1(settingsPremiumViewModel))), null, null, null, Integer.valueOf((int) R.attr.notice_theme_positive_red), null, null, 0, null, 15808, null);
    }

    public final void showContent(SettingsPremiumViewModel.ViewState.Loaded loaded) {
        SubscriptionPlanType planType;
        AppViewFlipper appViewFlipper = getBinding().f2608s;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.premiumSettingsViewFlipper");
        appViewFlipper.setDisplayedChild(0);
        ModelSubscription premiumSubscription = loaded.getPremiumSubscription();
        Boolean valueOf = (premiumSubscription == null || (planType = premiumSubscription.getPlanType()) == null) ? null : Boolean.valueOf(planType.isPremiumSubscription());
        ModelSubscription premiumSubscription2 = loaded.getPremiumSubscription();
        Boolean valueOf2 = premiumSubscription2 != null ? Boolean.valueOf(premiumSubscription2.isNonePlan()) : null;
        Boolean bool = Boolean.FALSE;
        if (!m.areEqual(valueOf, bool) || !m.areEqual(valueOf2, bool)) {
            configureActiveSubscriptionView(loaded);
            configureGrandfatheredHeader(loaded.getPremiumSubscription());
            configureLegalese(loaded);
            configureButtons(loaded.getPremiumSubscription(), loaded.getSkuDetails());
            configureAccountCredit(loaded.getEntitlements(), loaded.getPremiumSubscription());
            configurePaymentInfo(loaded);
            configureStatusNotice(loaded.getPremiumSubscription(), loaded.getSkuDetails());
            configurePriceChangeNotice(loaded.getPremiumSubscription(), loaded.getSkuDetails());
            Bundle extras = getMostRecentIntent().getExtras();
            Integer valueOf3 = extras != null ? Integer.valueOf(extras.getInt(INTENT_SCROLL_TO_SECTION, -1)) : null;
            if (valueOf3 == null || valueOf3.intValue() != -1) {
                try {
                    Observable<Long> d02 = Observable.d0(300L, TimeUnit.MILLISECONDS);
                    m.checkNotNullExpressionValue(d02, "Observable\n            .…0, TimeUnit.MILLISECONDS)");
                    ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsPremium$showContent$1(this, valueOf3));
                } catch (Exception e) {
                    Logger.e$default(AppLog.g, "Error Scrolling to section", e, null, 4, null);
                }
                getMostRecentIntent().removeExtra(INTENT_SCROLL_TO_SECTION);
                return;
            }
            return;
        }
        StringBuilder R = a.R("Attempting to open WidgetSettingsPremium with non-Premium ");
        StringBuilder R2 = a.R("and non-Guild Boost subscription: ");
        R2.append(loaded.getPremiumSubscription().getId());
        R.append(R2.toString());
        String sb = R.toString();
        m.checkNotNullExpressionValue(sb, "StringBuilder()\n        …}\")\n          .toString()");
        Logger.e$default(AppLog.g, sb, null, null, 6, null);
        FragmentActivity activity = e();
        if (activity != null) {
            activity.finish();
        }
    }

    public final void showDesktopManageAlert() {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        e = b.e(this, R.string.billing_manage_subscription, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e2 = b.e(this, R.string.premium_manage_via_desktop, new Object[]{f.a.a(360055386693L, null)}, (r4 & 4) != 0 ? b.a.j : null);
        e3 = b.e(this, R.string.premium_guild_subscription_header_subscribe_tooltip_close, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, e, e2, e3, null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    public final void showFailureUI() {
        AppViewFlipper appViewFlipper = getBinding().f2608s;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.premiumSettingsViewFlipper");
        appViewFlipper.setDisplayedChild(2);
        getBinding().l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.premium.WidgetSettingsPremium$showFailureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SettingsPremiumViewModel viewModel;
                viewModel = WidgetSettingsPremium.this.getViewModel();
                viewModel.onRetryClicked();
            }
        });
    }

    public final void showLoadingUI() {
        AppViewFlipper appViewFlipper = getBinding().f2608s;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.premiumSettingsViewFlipper");
        appViewFlipper.setDisplayedChild(1);
        scrollToTop();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.premium_title);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        TextView textView = getBinding().d.e;
        m.checkNotNullExpressionValue(textView, "binding.premiumPerks.uploadSizePerk");
        textView.setText(b.d(view, R.string.premium_perks_upload_size, new Object[0], new WidgetSettingsPremium$onViewBound$1(this)));
        TextView textView2 = getBinding().t.d;
        m.checkNotNullExpressionValue(textView2, "binding.premiumTier1.tier1UploadSizePerk");
        textView2.setText(b.d(view, R.string.premium_perks_upload_size, new Object[0], new WidgetSettingsPremium$onViewBound$2(this)));
        TextView textView3 = getBinding().d.f211b;
        m.checkNotNullExpressionValue(textView3, "binding.premiumPerks.guildSubscriptionPerk");
        textView3.setText(b.d(view, R.string.premium_perks_tier_2_guild_subscription, new Object[0], WidgetSettingsPremium$onViewBound$3.INSTANCE));
        TextView textView4 = getBinding().t.c;
        m.checkNotNullExpressionValue(textView4, "binding.premiumTier1.tier1GuildSubscriptionPerk");
        textView4.setText(b.d(view, R.string.premium_perks_tier_1_guild_subscription, new Object[0], WidgetSettingsPremium$onViewBound$4.INSTANCE));
        NumberFormat numberFormat = NumberFormat.getInstance(this.localeManager.getPrimaryLocale(requireContext()));
        TextView textView5 = getBinding().d.c;
        m.checkNotNullExpressionValue(textView5, "binding.premiumPerks.maxGuildsPerk");
        textView5.setText(b.d(view, R.string.premium_perks_tier_2_max_guilds, new Object[0], new WidgetSettingsPremium$onViewBound$5(numberFormat)));
        TextView textView6 = getBinding().d.d;
        m.checkNotNullExpressionValue(textView6, "binding.premiumPerks.maxMessageLengthPerk");
        textView6.setText(b.e(this, R.string.premium_chat_perks_max_message_length, new Object[0], new WidgetSettingsPremium$onViewBound$6(numberFormat)));
        configureButtonText$default(this, null, 1, null);
        for (TextView textView7 : n.listOf((Object[]) new TextView[]{getBinding().r, getBinding().h, getBinding().c.g})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView7, "header");
            accessibilityUtils.setViewIsHeading(textView7);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<SettingsPremiumViewModel.ViewState> q = getViewModel().observeViewState().p(200L, TimeUnit.MILLISECONDS).q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), WidgetSettingsPremium.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsPremium$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getViewModel().getEventSubject(), this, null, 2, null), WidgetSettingsPremium.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsPremium$onViewBoundOrOnResume$2(this));
    }

    private final CharSequence getPriceText(ModelSubscription modelSubscription, Map<String, ? extends SkuDetails> map, ModelInvoiceItem modelInvoiceItem) {
        SubscriptionPlanType subscriptionPlanType;
        CharSequence e;
        CharSequence e2;
        SkuDetails skuDetails;
        boolean isGoogleSubscription = modelSubscription != null ? modelSubscription.isGoogleSubscription() : false;
        if (modelInvoiceItem == null || (subscriptionPlanType = SubscriptionPlanType.Companion.from(modelInvoiceItem.getSubscriptionPlanId())) == null) {
            subscriptionPlanType = modelSubscription != null ? modelSubscription.getPlanType() : null;
        }
        int i = subscriptionPlanType != null ? subscriptionPlanType.isMonthlyInterval() : false ? R.string.billing_price_per_month : R.string.billing_price_per_year;
        String b2 = (modelSubscription == null || (skuDetails = map.get(modelSubscription.getPaymentGatewayPlanId())) == null) ? null : skuDetails.b();
        if (!isGoogleSubscription || b2 == null) {
            Object[] objArr = new Object[1];
            objArr[0] = PremiumUtilsKt.getFormattedPriceUsd(modelInvoiceItem != null ? modelInvoiceItem.getAmount() : 0, requireContext());
            e = b.e(this, i, objArr, (r4 & 4) != 0 ? b.a.j : null);
            return e;
        }
        e2 = b.e(this, i, new Object[]{b2}, (r4 & 4) != 0 ? b.a.j : null);
        return e2;
    }
}
