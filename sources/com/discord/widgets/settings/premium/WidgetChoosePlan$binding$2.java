package com.discord.widgets.settings.premium;

import android.view.View;
import android.widget.LinearLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChoosePlanBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.view.text.LinkifiedTextView;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChoosePlan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetChoosePlanBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetChoosePlanBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetChoosePlan$binding$2 extends k implements Function1<View, WidgetChoosePlanBinding> {
    public static final WidgetChoosePlan$binding$2 INSTANCE = new WidgetChoosePlan$binding$2();

    public WidgetChoosePlan$binding$2() {
        super(1, WidgetChoosePlanBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetChoosePlanBinding;", 0);
    }

    public final WidgetChoosePlanBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.choose_plan_empty_container;
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.choose_plan_empty_container);
        if (linearLayout != null) {
            i = R.id.choose_plan_empty_description;
            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.choose_plan_empty_description);
            if (linkifiedTextView != null) {
                i = R.id.choose_plan_recycler;
                RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.choose_plan_recycler);
                if (recyclerView != null) {
                    i = R.id.dimmer_view;
                    DimmerView dimmerView = (DimmerView) view.findViewById(R.id.dimmer_view);
                    if (dimmerView != null) {
                        return new WidgetChoosePlanBinding((CoordinatorLayout) view, linearLayout, linkifiedTextView, recyclerView, dimmerView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
