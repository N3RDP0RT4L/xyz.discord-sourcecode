package com.discord.widgets.settings;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.models.domain.Harvest;
import com.discord.utilities.rest.RestAPI;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/Harvest;", "requestedHarvest", "", "invoke", "(Lcom/discord/models/domain/Harvest;)V", "handleRequestSuccess"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPrivacy$onRequestDataClick$1 extends o implements Function1<Harvest, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ WidgetSettingsPrivacy this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPrivacy$onRequestDataClick$1(WidgetSettingsPrivacy widgetSettingsPrivacy, Context context) {
        super(1);
        this.this$0 = widgetSettingsPrivacy;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Harvest harvest) {
        invoke2(harvest);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Harvest harvest) {
        m.checkNotNullParameter(harvest, "requestedHarvest");
        WidgetNoticeDialog.Builder positiveButton$default = WidgetNoticeDialog.Builder.setPositiveButton$default(new WidgetNoticeDialog.Builder(this.$context).setTitle(R.string.data_privacy_controls_request_data_success_title).setMessage(R.string.data_privacy_controls_request_data_success_body), (int) R.string.okay, (Function1) null, 2, (Object) null);
        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        positiveButton$default.show(parentFragmentManager);
        this.this$0.configureRequestDataButton(true, new RestAPI.HarvestState.LastRequested(harvest));
    }
}
