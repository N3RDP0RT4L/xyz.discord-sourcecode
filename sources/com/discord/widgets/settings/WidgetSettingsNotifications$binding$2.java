package com.discord.widgets.settings;

import android.view.View;
import android.widget.LinearLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetSettingsNotificationsBinding;
import com.discord.views.CheckedSetting;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSettingsNotificationsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsNotificationsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettingsNotifications$binding$2 extends k implements Function1<View, WidgetSettingsNotificationsBinding> {
    public static final WidgetSettingsNotifications$binding$2 INSTANCE = new WidgetSettingsNotifications$binding$2();

    public WidgetSettingsNotifications$binding$2() {
        super(1, WidgetSettingsNotificationsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsNotificationsBinding;", 0);
    }

    public final WidgetSettingsNotificationsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.settings_inapp_notifs_switch;
        CheckedSetting checkedSetting = (CheckedSetting) view.findViewById(R.id.settings_inapp_notifs_switch);
        if (checkedSetting != null) {
            i = R.id.settings_notifications_blink;
            CheckedSetting checkedSetting2 = (CheckedSetting) view.findViewById(R.id.settings_notifications_blink);
            if (checkedSetting2 != null) {
                i = R.id.settings_notifications_enabled;
                CheckedSetting checkedSetting3 = (CheckedSetting) view.findViewById(R.id.settings_notifications_enabled);
                if (checkedSetting3 != null) {
                    i = R.id.settings_notifications_mute_all;
                    CheckedSetting checkedSetting4 = (CheckedSetting) view.findViewById(R.id.settings_notifications_mute_all);
                    if (checkedSetting4 != null) {
                        i = R.id.settings_notifications_vibrations;
                        CheckedSetting checkedSetting5 = (CheckedSetting) view.findViewById(R.id.settings_notifications_vibrations);
                        if (checkedSetting5 != null) {
                            i = R.id.settings_notifications_wrap;
                            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.settings_notifications_wrap);
                            if (linearLayout != null) {
                                return new WidgetSettingsNotificationsBinding((CoordinatorLayout) view, checkedSetting, checkedSetting2, checkedSetting3, checkedSetting4, checkedSetting5, linearLayout);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
