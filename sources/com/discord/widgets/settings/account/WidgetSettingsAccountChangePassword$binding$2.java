package com.discord.widgets.settings.account;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetSettingsAccountChangePasswordBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccountChangePassword.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSettingsAccountChangePasswordBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsAccountChangePasswordBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettingsAccountChangePassword$binding$2 extends k implements Function1<View, WidgetSettingsAccountChangePasswordBinding> {
    public static final WidgetSettingsAccountChangePassword$binding$2 INSTANCE = new WidgetSettingsAccountChangePassword$binding$2();

    public WidgetSettingsAccountChangePassword$binding$2() {
        super(1, WidgetSettingsAccountChangePasswordBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsAccountChangePasswordBinding;", 0);
    }

    public final WidgetSettingsAccountChangePasswordBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.change_password_current_password_input;
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.change_password_current_password_input);
        if (textInputLayout != null) {
            i = R.id.change_password_new_password_input;
            TextInputLayout textInputLayout2 = (TextInputLayout) view.findViewById(R.id.change_password_new_password_input);
            if (textInputLayout2 != null) {
                i = R.id.change_password_save;
                FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.change_password_save);
                if (floatingActionButton != null) {
                    i = R.id.change_password_two_factor;
                    TextInputLayout textInputLayout3 = (TextInputLayout) view.findViewById(R.id.change_password_two_factor);
                    if (textInputLayout3 != null) {
                        i = R.id.dimmer_view;
                        DimmerView dimmerView = (DimmerView) view.findViewById(R.id.dimmer_view);
                        if (dimmerView != null) {
                            return new WidgetSettingsAccountChangePasswordBinding((CoordinatorLayout) view, textInputLayout, textInputLayout2, floatingActionButton, textInputLayout3, dimmerView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
