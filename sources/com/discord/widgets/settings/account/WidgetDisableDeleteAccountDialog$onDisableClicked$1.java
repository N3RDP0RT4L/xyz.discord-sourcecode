package com.discord.widgets.settings.account;

import android.content.Context;
import com.discord.databinding.WidgetDisableDeleteAccountDialogBinding;
import com.discord.utilities.error.Error;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetDisableDeleteAccountDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDisableDeleteAccountDialog$onDisableClicked$1 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ WidgetDisableDeleteAccountDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetDisableDeleteAccountDialog$onDisableClicked$1(WidgetDisableDeleteAccountDialog widgetDisableDeleteAccountDialog) {
        super(1);
        this.this$0 = widgetDisableDeleteAccountDialog;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        WidgetDisableDeleteAccountDialogBinding binding;
        WidgetDisableDeleteAccountDialogBinding binding2;
        WidgetDisableDeleteAccountDialogBinding binding3;
        m.checkNotNullParameter(error, "it");
        binding = this.this$0.getBinding();
        binding.e.setIsLoading(false);
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "it.response");
        int code = response.getCode();
        if (code == 50018) {
            binding2 = this.this$0.getBinding();
            TextInputLayout textInputLayout = binding2.g;
            m.checkNotNullExpressionValue(textInputLayout, "binding.disableDeletePasswordWrap");
            Error.Response response2 = error.getResponse();
            m.checkNotNullExpressionValue(response2, "it.response");
            textInputLayout.setError(response2.getMessage());
        } else if (code != 60008) {
            Context context = this.this$0.getContext();
            Error.Response response3 = error.getResponse();
            m.checkNotNullExpressionValue(response3, "it.response");
            b.a.d.m.h(context, response3.getMessage(), 0, null, 12);
        } else {
            binding3 = this.this$0.getBinding();
            TextInputLayout textInputLayout2 = binding3.d;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.disableDeleteCodeWrap");
            Error.Response response4 = error.getResponse();
            m.checkNotNullExpressionValue(response4, "it.response");
            textInputLayout2.setError(response4.getMessage());
        }
    }
}
