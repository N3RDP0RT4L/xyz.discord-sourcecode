package com.discord.widgets.settings.account;

import com.discord.utilities.user.UserUtils;
import com.discord.widgets.settings.account.WidgetSettingsBlockedUsersViewModel;
import java.util.Comparator;
import kotlin.Metadata;
/* compiled from: WidgetSettingsBlockedUsersViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;", "kotlin.jvm.PlatformType", "lhs", "rhs", "", "compare", "(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;)I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsBlockedUsersViewModel$getItems$2<T> implements Comparator<WidgetSettingsBlockedUsersViewModel.Item> {
    public static final WidgetSettingsBlockedUsersViewModel$getItems$2 INSTANCE = new WidgetSettingsBlockedUsersViewModel$getItems$2();

    public final int compare(WidgetSettingsBlockedUsersViewModel.Item item, WidgetSettingsBlockedUsersViewModel.Item item2) {
        return UserUtils.INSTANCE.compareUserNames(item.getUser(), item2.getUser());
    }
}
