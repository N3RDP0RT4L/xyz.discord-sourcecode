package com.discord.widgets.settings.account;

import android.content.Context;
import com.discord.api.auth.mfa.DisableMfaRequestBody;
import com.discord.api.auth.mfa.DisableMfaResponse;
import com.discord.databinding.WidgetSettingsAccountBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreMFA;
import com.discord.stores.StoreStream;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccount.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/content/Context;", "<anonymous parameter 0>", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "", "invoke", "(Landroid/content/Context;Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccount$showRemove2FAModal$1 extends o implements Function2<Context, String, Unit> {
    public final /* synthetic */ WidgetSettingsAccount this$0;

    /* compiled from: WidgetSettingsAccount.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/auth/mfa/DisableMfaResponse;", "it", "", "invoke", "(Lcom/discord/api/auth/mfa/DisableMfaResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.account.WidgetSettingsAccount$showRemove2FAModal$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<DisableMfaResponse, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(DisableMfaResponse disableMfaResponse) {
            invoke2(disableMfaResponse);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(DisableMfaResponse disableMfaResponse) {
            m.checkNotNullParameter(disableMfaResponse, "it");
            b.a.d.m.h(WidgetSettingsAccount$showRemove2FAModal$1.this.this$0.requireContext(), WidgetSettingsAccount$showRemove2FAModal$1.this.this$0.requireContext().getString(R.string.user_settings_mfa_removed), 0, null, 12);
            StoreStream.Companion companion = StoreStream.Companion;
            companion.getAuthentication().setAuthed(disableMfaResponse.a());
            companion.getMFA().updatePendingMFAState(StoreMFA.MFAActivationState.PENDING_DISABLED);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsAccount$showRemove2FAModal$1(WidgetSettingsAccount widgetSettingsAccount) {
        super(2);
        this.this$0 = widgetSettingsAccount;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Context context, String str) {
        invoke2(context, str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Context context, String str) {
        WidgetSettingsAccountBinding binding;
        m.checkNotNullParameter(context, "<anonymous parameter 0>");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
        Observable ui$default = ObservableExtensionsKt.ui$default(RestAPI.Companion.getApi().disableMFA(new DisableMfaRequestBody(str)), this.this$0, null, 2, null);
        binding = this.this$0.getBinding();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(ObservableExtensionsKt.withDimmer$default(ui$default, binding.f2565b, 0L, 2, null), false, 1, null), WidgetSettingsAccount.class, (r18 & 2) != 0 ? null : this.this$0.requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
