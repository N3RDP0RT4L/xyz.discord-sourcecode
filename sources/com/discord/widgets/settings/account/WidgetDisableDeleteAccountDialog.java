package com.discord.widgets.settings.account;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetDisableDeleteAccountDialogBinding;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetDisableDeleteAccountDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00182\u00020\u0001:\u0002\u0018\u0019B\u0007¢\u0006\u0004\b\u0017\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0016\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetDisableDeleteAccountDialog;", "Lcom/discord/app/AppDialog;", "Lcom/discord/models/user/MeUser;", "meUser", "", "configureUI", "(Lcom/discord/models/user/MeUser;)V", "Lcom/discord/widgets/settings/account/WidgetDisableDeleteAccountDialog$Mode;", "mode", "onDisableClicked", "(Lcom/discord/widgets/settings/account/WidgetDisableDeleteAccountDialog$Mode;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "()V", "Lcom/discord/databinding/WidgetDisableDeleteAccountDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetDisableDeleteAccountDialogBinding;", "binding", HookHelper.constructorName, "Companion", "Mode", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDisableDeleteAccountDialog extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetDisableDeleteAccountDialog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetDisableDeleteAccountDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_MODE = "extra_mode";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetDisableDeleteAccountDialog$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetDisableDeleteAccountDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetDisableDeleteAccountDialog$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/widgets/settings/account/WidgetDisableDeleteAccountDialog$Mode;", "mode", "", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/settings/account/WidgetDisableDeleteAccountDialog$Mode;)V", "", "EXTRA_MODE", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, Mode mode) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(mode, "mode");
            WidgetDisableDeleteAccountDialog widgetDisableDeleteAccountDialog = new WidgetDisableDeleteAccountDialog();
            Bundle bundle = new Bundle();
            bundle.putInt(WidgetDisableDeleteAccountDialog.EXTRA_MODE, mode.ordinal());
            widgetDisableDeleteAccountDialog.setArguments(bundle);
            String tag = widgetDisableDeleteAccountDialog.getTag();
            if (tag == null) {
                tag = "";
            }
            widgetDisableDeleteAccountDialog.show(fragmentManager, tag);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetDisableDeleteAccountDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\r\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B'\b\u0002\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0002\u0012\b\b\u0001\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0004\u001a\u0004\b\n\u0010\u0006j\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetDisableDeleteAccountDialog$Mode;", "", "", "headerStringId", "I", "getHeaderStringId", "()I", "bodyStringId", "getBodyStringId", "confirmStringId", "getConfirmStringId", HookHelper.constructorName, "(Ljava/lang/String;IIII)V", "DISABLE", "DELETE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum Mode {
        DISABLE(R.string.disable_account, R.string.disable_account_body, R.string.disable),
        DELETE(R.string.delete_account, R.string.delete_account_body, R.string.delete);
        
        private final int bodyStringId;
        private final int confirmStringId;
        private final int headerStringId;

        Mode(@StringRes int i, @StringRes int i2, @StringRes int i3) {
            this.headerStringId = i;
            this.bodyStringId = i2;
            this.confirmStringId = i3;
        }

        public final int getBodyStringId() {
            return this.bodyStringId;
        }

        public final int getConfirmStringId() {
            return this.confirmStringId;
        }

        public final int getHeaderStringId() {
            return this.headerStringId;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            Mode.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[Mode.DISABLE.ordinal()] = 1;
            iArr[Mode.DELETE.ordinal()] = 2;
        }
    }

    public WidgetDisableDeleteAccountDialog() {
        super(R.layout.widget_disable_delete_account_dialog);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(MeUser meUser) {
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.disableDeleteCodeWrap");
        textInputLayout.setVisibility(meUser.getMfaEnabled() ? 0 : 8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetDisableDeleteAccountDialogBinding getBinding() {
        return (WidgetDisableDeleteAccountDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onDisableClicked(Mode mode) {
        String str;
        Observable<Void> observable;
        getBinding().e.setIsLoading(true);
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.disableDeleteCodeWrap");
        if (textInputLayout.getVisibility() == 0) {
            TextInputLayout textInputLayout2 = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.disableDeleteCodeWrap");
            str = ViewExtensions.getTextOrEmpty(textInputLayout2);
        } else {
            str = null;
        }
        TextInputLayout textInputLayout3 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.disableDeletePasswordWrap");
        RestAPIParams.DisableAccount disableAccount = new RestAPIParams.DisableAccount(ViewExtensions.getTextOrEmpty(textInputLayout3), str);
        int ordinal = mode.ordinal();
        if (ordinal == 0) {
            observable = RestAPI.Companion.getApi().disableAccount(disableAccount);
        } else if (ordinal == 1) {
            observable = RestAPI.Companion.getApi().deleteAccount(disableAccount);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(observable, false, 1, null), this, null, 2, null), WidgetDisableDeleteAccountDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetDisableDeleteAccountDialog$onDisableClicked$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetDisableDeleteAccountDialog$onDisableClicked$2.INSTANCE);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreUser.observeMe$default(StoreStream.Companion.getUsers(), false, 1, null), this, null, 2, null), WidgetDisableDeleteAccountDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDisableDeleteAccountDialog$onResume$1(this));
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        final Mode mode;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setCancelable(false);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mode = Mode.values()[arguments.getInt(EXTRA_MODE)];
        } else {
            mode = null;
        }
        if (mode == null) {
            Logger.e$default(AppLog.g, "Disable/Delete Dialog shown with null mode", null, null, 6, null);
            dismiss();
            return;
        }
        getBinding().e.setIsLoading(false);
        TextView textView = getBinding().f;
        m.checkNotNullExpressionValue(textView, "binding.disableDeleteHeader");
        textView.setText(getString(mode.getHeaderStringId()));
        TextView textView2 = getBinding().f2346b;
        m.checkNotNullExpressionValue(textView2, "binding.disableDeleteBody");
        textView2.setText(getString(mode.getBodyStringId()));
        getBinding().e.setText(getString(mode.getConfirmStringId()));
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetDisableDeleteAccountDialog$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetDisableDeleteAccountDialog.this.dismiss();
            }
        });
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.disableDeleteCodeWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetDisableDeleteAccountDialog$onViewBound$2(this));
        TextInputLayout textInputLayout2 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.disableDeletePasswordWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, new WidgetDisableDeleteAccountDialog$onViewBound$3(this));
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetDisableDeleteAccountDialog$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetDisableDeleteAccountDialog.this.onDisableClicked(mode);
            }
        });
    }
}
