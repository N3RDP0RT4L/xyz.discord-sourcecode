package com.discord.widgets.settings.account;

import android.content.Context;
import com.discord.app.AppFragment;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetSettingsAccountBackupCodes.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\b\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Landroid/content/Context;", "<anonymous parameter 0>", "", "newPassword", "", "invoke", "(Landroid/content/Context;Ljava/lang/String;)V", "com/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$showPasswordModal$1$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccountBackupCodes$showPasswordModal$$inlined$let$lambda$1 extends o implements Function2<Context, String, Unit> {
    public final /* synthetic */ WidgetSettingsAccountBackupCodes this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsAccountBackupCodes$showPasswordModal$$inlined$let$lambda$1(WidgetSettingsAccountBackupCodes widgetSettingsAccountBackupCodes) {
        super(2);
        this.this$0 = widgetSettingsAccountBackupCodes;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Context context, String str) {
        invoke2(context, str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Context context, String str) {
        m.checkNotNullParameter(context, "<anonymous parameter 0>");
        m.checkNotNullParameter(str, "newPassword");
        this.this$0.password = str;
        this.this$0.getBackupCodes(false);
        AppFragment.hideKeyboard$default(this.this$0, null, 1, null);
    }
}
