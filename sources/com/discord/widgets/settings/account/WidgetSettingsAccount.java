package com.discord.widgets.settings.account;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.airbnb.lottie.LottieAnimationView;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetSettingsAccountBinding;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreMFA;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.settings.account.WidgetDisableDeleteAccountDialog;
import com.discord.widgets.settings.account.WidgetEnableSMSBackupDialog;
import com.discord.widgets.settings.account.WidgetSettingsAccountChangePassword;
import com.discord.widgets.settings.account.WidgetSettingsAccountEmailEdit;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import com.discord.widgets.user.phone.WidgetUserPhoneManage;
import com.google.android.material.button.MaterialButton;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlinx.coroutines.CoroutineScope;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccount.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 )2\u00020\u0001:\u0003)*+B\u0007¢\u0006\u0004\b(\u0010\u0014J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0015\u0010\u0014J\u0017\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001a\u0010\u0014J)\u0010 \u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001b2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001eH\u0016¢\u0006\u0004\b \u0010!R\u001d\u0010'\u001a\u00020\"8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&¨\u0006,"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccount;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;)V", "Lcom/discord/models/user/MeUser;", "user", "configureAccountVerificationBanner", "(Lcom/discord/models/user/MeUser;)V", "", "getSMSBackupDisabledMessage", "(Lcom/discord/models/user/MeUser;)Ljava/lang/String;", "", "enabled", "pending", "configureMFA", "(ZZ)V", "showRemove2FAModal", "()V", "showOwnsGuildModal", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "", "requestCode", "resultCode", "Landroid/content/Intent;", "data", "onActivityResult", "(IILandroid/content/Intent;)V", "Lcom/discord/databinding/WidgetSettingsAccountBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsAccountBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "Redirect", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccount extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsAccount.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsAccountBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_HINT_DATA_MANAGEMENT = "com.discord.extra.HINT_DATA_MANAGEMENT";
    private static final String EXTRA_REDIRECT = "extra_redirect";
    private static final int MFA_DISABLED_VIEW_INDEX = 1;
    private static final int MFA_ENABLED_VIEW_INDEX = 0;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsAccount$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetSettingsAccount.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J-\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\rR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Companion;", "", "Landroid/content/Context;", "context", "", "hintDataManagement", "Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Redirect;", "redirect", "", "launch", "(Landroid/content/Context;ZLcom/discord/widgets/settings/account/WidgetSettingsAccount$Redirect;)V", "", "EXTRA_HINT_DATA_MANAGEMENT", "Ljava/lang/String;", "EXTRA_REDIRECT", "", "MFA_DISABLED_VIEW_INDEX", "I", "MFA_ENABLED_VIEW_INDEX", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, boolean z2, Redirect redirect, int i, Object obj) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            if ((i & 4) != 0) {
                redirect = null;
            }
            companion.launch(context, z2, redirect);
        }

        public final void launch(Context context) {
            launch$default(this, context, false, null, 6, null);
        }

        public final void launch(Context context, boolean z2) {
            launch$default(this, context, z2, null, 4, null);
        }

        public final void launch(Context context, boolean z2, Redirect redirect) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra(WidgetSettingsAccount.EXTRA_HINT_DATA_MANAGEMENT, z2);
            intent.putExtra(WidgetSettingsAccount.EXTRA_REDIRECT, redirect);
            if (redirect != null) {
                intent.addFlags(268435456);
                intent.addFlags(67108864);
            }
            j.d(context, WidgetSettingsAccount.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsAccount.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0011\b\u0086\b\u0018\u0000 '2\u00020\u0001:\u0001'B)\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ:\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001b\u001a\u00020\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001f\u001a\u0004\b \u0010\nR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010!\u001a\u0004\b\"\u0010\u0007R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b$\u0010\r¨\u0006("}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/stores/StoreMFA$State;", "component2", "()Lcom/discord/stores/StoreMFA$State;", "", "component3", "()Z", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component4", "()Lcom/discord/api/connectedaccounts/ConnectedAccount;", "meUser", "pendingMFAState", "ownsAnyGuilds", "contactSyncConnection", "copy", "(Lcom/discord/models/user/MeUser;Lcom/discord/stores/StoreMFA$State;ZLcom/discord/api/connectedaccounts/ConnectedAccount;)Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/user/MeUser;", "getMeUser", "Z", "getOwnsAnyGuilds", "Lcom/discord/stores/StoreMFA$State;", "getPendingMFAState", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "getContactSyncConnection", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/stores/StoreMFA$State;ZLcom/discord/api/connectedaccounts/ConnectedAccount;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final ConnectedAccount contactSyncConnection;
        private final MeUser meUser;
        private final boolean ownsAnyGuilds;
        private final StoreMFA.State pendingMFAState;

        /* compiled from: WidgetSettingsAccount.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get() {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<Model> g = Observable.g(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getMFA().observeState(), companion.getGuilds().observeGuilds(), companion.getUserConnections().observeConnectedAccounts(), companion.getExperiments().observeUserExperiment("2021-04_contact_sync_android_main", true), WidgetSettingsAccount$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(g, "Observable.combineLatest…            )\n          }");
                return g;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(MeUser meUser, StoreMFA.State state, boolean z2, ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(state, "pendingMFAState");
            this.meUser = meUser;
            this.pendingMFAState = state;
            this.ownsAnyGuilds = z2;
            this.contactSyncConnection = connectedAccount;
        }

        public static /* synthetic */ Model copy$default(Model model, MeUser meUser, StoreMFA.State state, boolean z2, ConnectedAccount connectedAccount, int i, Object obj) {
            if ((i & 1) != 0) {
                meUser = model.meUser;
            }
            if ((i & 2) != 0) {
                state = model.pendingMFAState;
            }
            if ((i & 4) != 0) {
                z2 = model.ownsAnyGuilds;
            }
            if ((i & 8) != 0) {
                connectedAccount = model.contactSyncConnection;
            }
            return model.copy(meUser, state, z2, connectedAccount);
        }

        public final MeUser component1() {
            return this.meUser;
        }

        public final StoreMFA.State component2() {
            return this.pendingMFAState;
        }

        public final boolean component3() {
            return this.ownsAnyGuilds;
        }

        public final ConnectedAccount component4() {
            return this.contactSyncConnection;
        }

        public final Model copy(MeUser meUser, StoreMFA.State state, boolean z2, ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(state, "pendingMFAState");
            return new Model(meUser, state, z2, connectedAccount);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.meUser, model.meUser) && m.areEqual(this.pendingMFAState, model.pendingMFAState) && this.ownsAnyGuilds == model.ownsAnyGuilds && m.areEqual(this.contactSyncConnection, model.contactSyncConnection);
        }

        public final ConnectedAccount getContactSyncConnection() {
            return this.contactSyncConnection;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final boolean getOwnsAnyGuilds() {
            return this.ownsAnyGuilds;
        }

        public final StoreMFA.State getPendingMFAState() {
            return this.pendingMFAState;
        }

        public int hashCode() {
            MeUser meUser = this.meUser;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            StoreMFA.State state = this.pendingMFAState;
            int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
            boolean z2 = this.ownsAnyGuilds;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode2 + i2) * 31;
            ConnectedAccount connectedAccount = this.contactSyncConnection;
            if (connectedAccount != null) {
                i = connectedAccount.hashCode();
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(meUser=");
            R.append(this.meUser);
            R.append(", pendingMFAState=");
            R.append(this.pendingMFAState);
            R.append(", ownsAnyGuilds=");
            R.append(this.ownsAnyGuilds);
            R.append(", contactSyncConnection=");
            R.append(this.contactSyncConnection);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetSettingsAccount.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Redirect;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "SMS_BACKUP", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum Redirect {
        SMS_BACKUP
    }

    public WidgetSettingsAccount() {
        super(R.layout.widget_settings_account);
    }

    private final void configureAccountVerificationBanner(MeUser meUser) {
        RelativeLayout relativeLayout = getBinding().C;
        m.checkNotNullExpressionValue(relativeLayout, "binding.settingsAccountVerification");
        relativeLayout.setVisibility(meUser.isVerified() ^ true ? 0 : 8);
        if (meUser.getEmail() == null) {
            TextView textView = getBinding().E;
            m.checkNotNullExpressionValue(textView, "binding.settingsAccountVerificationTitle");
            textView.setText(getString(R.string.add_email_banner_title));
            MaterialButton materialButton = getBinding().D;
            m.checkNotNullExpressionValue(materialButton, "binding.settingsAccountVerificationButton");
            materialButton.setText(getString(R.string.add_email_short));
        } else {
            TextView textView2 = getBinding().E;
            m.checkNotNullExpressionValue(textView2, "binding.settingsAccountVerificationTitle");
            textView2.setText(getString(R.string.verify_your_email));
            MaterialButton materialButton2 = getBinding().D;
            m.checkNotNullExpressionValue(materialButton2, "binding.settingsAccountVerificationButton");
            materialButton2.setText(getString(R.string.verify));
        }
        getBinding().D.setOnClickListener(WidgetSettingsAccount$configureAccountVerificationBanner$1.INSTANCE);
    }

    private final void configureMFA(boolean z2, boolean z3) {
        TextView textView = getBinding().l;
        m.checkNotNullExpressionValue(textView, "binding.settingsAccountMfaEnabledHeader");
        int i = 0;
        textView.setVisibility(z2 ? 0 : 8);
        AppViewFlipper appViewFlipper = getBinding().m;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.settingsAccountMfaFlipper");
        appViewFlipper.setDisplayedChild(!z2 ? 1 : 0);
        LottieAnimationView lottieAnimationView = getBinding().n;
        m.checkNotNullExpressionValue(lottieAnimationView, "binding.settingsAccountMfaLottie");
        lottieAnimationView.setVisibility(z2 ^ true ? 0 : 8);
        TextView textView2 = getBinding().o;
        m.checkNotNullExpressionValue(textView2, "binding.settingsAccountMfaSalesPitch");
        textView2.setVisibility(z2 ^ true ? 0 : 8);
        MaterialButton materialButton = getBinding().j;
        m.checkNotNullExpressionValue(materialButton, "binding.settingsAccountMfaEnable");
        if (!(!z2)) {
            i = 8;
        }
        materialButton.setVisibility(i);
        MaterialButton materialButton2 = getBinding().j;
        m.checkNotNullExpressionValue(materialButton2, "binding.settingsAccountMfaEnable");
        materialButton2.setEnabled(!z3);
        TextView textView3 = getBinding().F;
        m.checkNotNullExpressionValue(textView3, "binding.settingsAccountViewBackupCodes");
        textView3.setEnabled(!z3);
        TextView textView4 = getBinding().w;
        m.checkNotNullExpressionValue(textView4, "binding.settingsAccountRemoveTwoFa");
        textView4.setEnabled(!z3);
        LinearLayout linearLayout = getBinding().i;
        m.checkNotNullExpressionValue(linearLayout, "binding.settingsAccountMfaDisabledContainer");
        ViewExtensions.setEnabledAlpha$default(linearLayout, !z3, 0.0f, 2, null);
        LinearLayout linearLayout2 = getBinding().k;
        m.checkNotNullExpressionValue(linearLayout2, "binding.settingsAccountMfaEnabledContainer");
        ViewExtensions.setEnabledAlpha$default(linearLayout2, !z3, 0.0f, 2, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        String str;
        final MeUser component1 = model.component1();
        StoreMFA.State component2 = model.component2();
        ConnectedAccount component4 = model.component4();
        configureAccountVerificationBanner(component1);
        LinearLayout linearLayout = getBinding().p;
        m.checkNotNullExpressionValue(linearLayout, "binding.settingsAccountNameContainer");
        int i = 8;
        linearLayout.setVisibility(component4 != null ? 0 : 8);
        TextView textView = getBinding().q;
        m.checkNotNullExpressionValue(textView, "binding.settingsAccountNameText");
        if (component4 == null || (str = component4.d()) == null) {
            str = "";
        }
        textView.setText(str);
        TextView textView2 = getBinding().B;
        m.checkNotNullExpressionValue(textView2, "binding.settingsAccountTagText");
        UserUtils userUtils = UserUtils.INSTANCE;
        textView2.setText(UserUtils.getUserNameWithDiscriminator$default(userUtils, component1, null, null, 3, null));
        TextView textView3 = getBinding().f;
        m.checkNotNullExpressionValue(textView3, "binding.settingsAccountEmailText");
        textView3.setText(component1.getEmail());
        TextView textView4 = getBinding().f2566s;
        m.checkNotNullExpressionValue(textView4, "binding.settingsAccountPhoneText");
        textView4.setText(component1.getPhoneNumber());
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccount$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsAccountBinding binding;
                WidgetSettingsAccountChangePassword.Companion companion = WidgetSettingsAccountChangePassword.Companion;
                binding = WidgetSettingsAccount.this.getBinding();
                TextView textView5 = binding.d;
                m.checkNotNullExpressionValue(textView5, "binding.settingsAccountChangePassword");
                Context context = textView5.getContext();
                m.checkNotNullExpressionValue(context, "binding.settingsAccountChangePassword.context");
                companion.launch(context);
            }
        });
        getBinding().A.setOnClickListener(WidgetSettingsAccount$configureUI$2.INSTANCE);
        getBinding().p.setOnClickListener(WidgetSettingsAccount$configureUI$3.INSTANCE);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccount$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                boolean isVerified = MeUser.this.isVerified();
                Experiment userExperiment = StoreStream.Companion.getExperiments().getUserExperiment("2022-01_email_change_confirmation", isVerified);
                boolean z2 = true;
                if (userExperiment == null || userExperiment.getBucket() != 1 || !isVerified) {
                    z2 = false;
                }
                WidgetSettingsAccountEmailEdit.Companion companion = WidgetSettingsAccountEmailEdit.Companion;
                m.checkNotNullExpressionValue(view, "it");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                companion.launch(context, z2);
            }
        });
        getBinding().r.setOnClickListener(WidgetSettingsAccount$configureUI$5.INSTANCE);
        TextView textView5 = getBinding().l;
        m.checkNotNullExpressionValue(textView5, "binding.settingsAccountMfaEnabledHeader");
        textView5.setVisibility(component1.getMfaEnabled() ? 0 : 8);
        AppViewFlipper appViewFlipper = getBinding().m;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.settingsAccountMfaFlipper");
        appViewFlipper.setDisplayedChild(!component1.getMfaEnabled());
        LottieAnimationView lottieAnimationView = getBinding().n;
        m.checkNotNullExpressionValue(lottieAnimationView, "binding.settingsAccountMfaLottie");
        lottieAnimationView.setVisibility(component1.getMfaEnabled() ^ true ? 0 : 8);
        TextView textView6 = getBinding().o;
        m.checkNotNullExpressionValue(textView6, "binding.settingsAccountMfaSalesPitch");
        textView6.setVisibility(component1.getMfaEnabled() ^ true ? 0 : 8);
        MaterialButton materialButton = getBinding().j;
        m.checkNotNullExpressionValue(materialButton, "binding.settingsAccountMfaEnable");
        if (!component1.getMfaEnabled()) {
            i = 0;
        }
        materialButton.setVisibility(i);
        if (component2.getActivationState() != StoreMFA.MFAActivationState.NONE) {
            configureMFA(component2.getActivationState() == StoreMFA.MFAActivationState.PENDING_ENABLED, true);
        } else {
            configureMFA(component1.getMfaEnabled(), false);
        }
        getBinding().j.setOnClickListener(WidgetSettingsAccount$configureUI$6.INSTANCE);
        getBinding().F.setOnClickListener(WidgetSettingsAccount$configureUI$7.INSTANCE);
        getBinding().w.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccount$configureUI$8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsAccount.this.showRemove2FAModal();
            }
        });
        getBinding().G.setOnClickListener(WidgetSettingsAccount$configureUI$9.INSTANCE);
        getBinding().u.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccount$configureUI$10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (model.getOwnsAnyGuilds()) {
                    WidgetSettingsAccount.this.showOwnsGuildModal();
                    return;
                }
                WidgetDisableDeleteAccountDialog.Companion companion = WidgetDisableDeleteAccountDialog.Companion;
                FragmentManager parentFragmentManager = WidgetSettingsAccount.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                companion.show(parentFragmentManager, WidgetDisableDeleteAccountDialog.Mode.DISABLE);
            }
        });
        getBinding().t.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccount$configureUI$11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (model.getOwnsAnyGuilds()) {
                    WidgetSettingsAccount.this.showOwnsGuildModal();
                    return;
                }
                WidgetDisableDeleteAccountDialog.Companion companion = WidgetDisableDeleteAccountDialog.Companion;
                FragmentManager parentFragmentManager = WidgetSettingsAccount.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                companion.show(parentFragmentManager, WidgetDisableDeleteAccountDialog.Mode.DELETE);
            }
        });
        String sMSBackupDisabledMessage = getSMSBackupDisabledMessage(component1);
        CharSequence charSequence = null;
        if (sMSBackupDisabledMessage != null || component2.isTogglingSMSBackup()) {
            CheckedSetting.d(getBinding().f2568y, null, 1);
            TextView textView7 = getBinding().f2569z;
            m.checkNotNullExpressionValue(textView7, "binding.settingsAccountSmsPhone");
            ViewExtensions.setEnabledAlpha$default(textView7, false, 0.0f, 2, null);
        } else {
            TextView textView8 = getBinding().f2569z;
            m.checkNotNullExpressionValue(textView8, "binding.settingsAccountSmsPhone");
            ViewExtensions.setEnabledAlpha$default(textView8, true, 0.0f, 2, null);
            getBinding().f2568y.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccount$configureUI$12
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    UserUtils userUtils2 = UserUtils.INSTANCE;
                    if (userUtils2.isMfaSMSEnabled(component1)) {
                        WidgetEnableSMSBackupDialog.Companion companion = WidgetEnableSMSBackupDialog.Companion;
                        FragmentManager parentFragmentManager = WidgetSettingsAccount.this.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                        companion.show(parentFragmentManager, false);
                    } else if (userUtils2.isMfaSMSEnabled(component1) || !userUtils2.getHasPhone(component1)) {
                        WidgetUserPhoneManage.Companion.launch(a.x(view, "it", "it.context"), WidgetUserAccountVerifyBase.Mode.NO_HISTORY_FROM_USER_SETTINGS, WidgetUserPhoneManage.Companion.Source.MFA_PHONE_UPDATE);
                    } else {
                        WidgetEnableSMSBackupDialog.Companion companion2 = WidgetEnableSMSBackupDialog.Companion;
                        FragmentManager parentFragmentManager2 = WidgetSettingsAccount.this.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
                        companion2.show(parentFragmentManager2, true);
                    }
                }
            });
        }
        TextView textView9 = getBinding().f2569z;
        m.checkNotNullExpressionValue(textView9, "binding.settingsAccountSmsPhone");
        if (component1.getPhoneNumber() != null) {
            TextView textView10 = getBinding().f2569z;
            m.checkNotNullExpressionValue(textView10, "binding.settingsAccountSmsPhone");
            Context context = textView10.getContext();
            m.checkNotNullExpressionValue(context, "binding.settingsAccountSmsPhone.context");
            charSequence = b.b(context, R.string.mfa_sms_auth_current_phone, new Object[]{component1.getPhoneNumber()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        }
        ViewExtensions.setTextAndVisibilityBy(textView9, charSequence);
        CheckedSetting checkedSetting = getBinding().f2568y;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsAccountSmsBackup");
        checkedSetting.setChecked(userUtils.isMfaSMSEnabled(component1));
        CheckedSetting checkedSetting2 = getBinding().f2568y;
        if (sMSBackupDisabledMessage == null) {
            sMSBackupDisabledMessage = getString(R.string.mfa_sms_auth_sales_pitch);
            m.checkNotNullExpressionValue(sMSBackupDisabledMessage, "getString(R.string.mfa_sms_auth_sales_pitch)");
        }
        CheckedSetting.i(checkedSetting2, sMSBackupDisabledMessage, false, 2);
        if (getMostRecentIntent().getSerializableExtra(EXTRA_REDIRECT) == Redirect.SMS_BACKUP) {
            getMostRecentIntent().removeExtra(EXTRA_REDIRECT);
            WidgetEnableSMSBackupDialog.Companion companion = WidgetEnableSMSBackupDialog.Companion;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(parentFragmentManager, true);
        }
        for (TextView textView11 : n.listOf((Object[]) new TextView[]{getBinding().g, getBinding().c, getBinding().h})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView11, "header");
            accessibilityUtils.setViewIsHeading(textView11);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSettingsAccountBinding getBinding() {
        return (WidgetSettingsAccountBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final String getSMSBackupDisabledMessage(MeUser meUser) {
        UserUtils userUtils = UserUtils.INSTANCE;
        if (userUtils.isPartner(meUser) || userUtils.isStaff(meUser)) {
            return getString(R.string.mfa_sms_disabled_partner);
        }
        if (meUser.getEmail() == null) {
            return getString(R.string.mfa_sms_disabled_no_email);
        }
        return null;
    }

    public static final void launch(Context context) {
        Companion.launch$default(Companion, context, false, null, 6, null);
    }

    public static final void launch(Context context, boolean z2) {
        Companion.launch$default(Companion, context, z2, null, 4, null);
    }

    public static final void launch(Context context, boolean z2, Redirect redirect) {
        Companion.launch(context, z2, redirect);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showOwnsGuildModal() {
        WidgetNoticeDialog.Builder positiveButton$default = WidgetNoticeDialog.Builder.setPositiveButton$default(new WidgetNoticeDialog.Builder(requireContext()).setTitle(R.string.delete_account_transfer_ownership).setMessage(R.string.delete_account_transfer_ownership_body), (int) R.string.okay, (Function1) null, 2, (Object) null);
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        positiveButton$default.show(parentFragmentManager);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showRemove2FAModal() {
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            String string = requireContext().getString(R.string.two_fa_remove);
            m.checkNotNullExpressionValue(string, "requireContext().getString(R.string.two_fa_remove)");
            String string2 = requireContext().getString(R.string.user_settings_mfa_enable_code_body);
            m.checkNotNullExpressionValue(string2, "requireContext().getStri…ngs_mfa_enable_code_body)");
            String string3 = requireContext().getString(R.string.two_fa_auth_code);
            m.checkNotNullExpressionValue(string3, "requireContext().getStri….string.two_fa_auth_code)");
            companion.showInputModal(appActivity, string, string2, string3, new WidgetSettingsAccount$showRemove2FAModal$1(this), (r18 & 32) != 0 ? null : null, (r18 & 64) != 0 ? null : null);
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 4008) {
            GoogleSmartLockManager.Companion.handleResult(i2, intent);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.user_settings_my_account);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setRetainInstance(true);
        if (getMostRecentIntent().getBooleanExtra(EXTRA_HINT_DATA_MANAGEMENT, false)) {
            LinearLayout linearLayout = getBinding().v;
            m.checkNotNullExpressionValue(linearLayout, "binding.settingsAccountPrivateDataWrap");
            CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(linearLayout);
            if (coroutineScope != null) {
                f.H0(coroutineScope, null, null, new WidgetSettingsAccount$onViewBound$1(this, null), 3, null);
            }
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<Model> q = Model.Companion.get().q();
        m.checkNotNullExpressionValue(q, "Model\n        .get()\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), WidgetSettingsAccount.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsAccount$onViewBoundOrOnResume$1(this));
    }
}
