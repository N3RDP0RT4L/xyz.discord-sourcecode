package com.discord.widgets.settings.account;

import b.a.k.b;
import com.discord.api.auth.mfa.BackupCode;
import com.discord.api.auth.mfa.GetBackupCodesResponse;
import com.discord.widgets.settings.account.WidgetSettingsAccountBackupCodes;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccountBackupCodes.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/auth/mfa/GetBackupCodesResponse;", "kotlin.jvm.PlatformType", "response", "", "invoke", "(Lcom/discord/api/auth/mfa/GetBackupCodesResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccountBackupCodes$getBackupCodes$1 extends o implements Function1<GetBackupCodesResponse, Unit> {
    public final /* synthetic */ WidgetSettingsAccountBackupCodes this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsAccountBackupCodes$getBackupCodes$1(WidgetSettingsAccountBackupCodes widgetSettingsAccountBackupCodes) {
        super(1);
        this.this$0 = widgetSettingsAccountBackupCodes;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GetBackupCodesResponse getBackupCodesResponse) {
        invoke2(getBackupCodesResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GetBackupCodesResponse getBackupCodesResponse) {
        CharSequence b2;
        CharSequence b3;
        List<BackupCode> a = getBackupCodesResponse.a();
        ArrayList<BackupCode> arrayList = new ArrayList();
        for (Object obj : a) {
            if (!((BackupCode) obj).b()) {
                arrayList.add(obj);
            }
        }
        ArrayList<BackupCode> arrayList2 = new ArrayList();
        for (Object obj2 : a) {
            if (((BackupCode) obj2).b()) {
                arrayList2.add(obj2);
            }
        }
        ArrayList arrayList3 = new ArrayList();
        if (!arrayList.isEmpty()) {
            b3 = b.b(this.this$0.requireContext(), R.string.user_settings_available_codes, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            arrayList3.add(new WidgetSettingsAccountBackupCodes.BackupCodeItemHeader(b3));
            ArrayList arrayList4 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
            for (BackupCode backupCode : arrayList) {
                arrayList4.add(new WidgetSettingsAccountBackupCodes.BackupCodeItem(backupCode));
            }
            arrayList3.addAll(arrayList4);
        }
        if (!arrayList2.isEmpty()) {
            b2 = b.b(this.this$0.requireContext(), R.string.user_settings_used_backup_codes, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            arrayList3.add(new WidgetSettingsAccountBackupCodes.BackupCodeItemHeader(b2));
            ArrayList arrayList5 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList2, 10));
            for (BackupCode backupCode2 : arrayList2) {
                arrayList5.add(new WidgetSettingsAccountBackupCodes.BackupCodeItem(backupCode2));
            }
            arrayList3.addAll(arrayList5);
        }
        this.this$0.configureUI(arrayList3);
    }
}
