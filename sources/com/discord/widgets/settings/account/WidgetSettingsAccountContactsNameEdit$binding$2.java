package com.discord.widgets.settings.account;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetSettingsAccountContactsNameEditBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccountContactsNameEdit.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSettingsAccountContactsNameEditBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsAccountContactsNameEditBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettingsAccountContactsNameEdit$binding$2 extends k implements Function1<View, WidgetSettingsAccountContactsNameEditBinding> {
    public static final WidgetSettingsAccountContactsNameEdit$binding$2 INSTANCE = new WidgetSettingsAccountContactsNameEdit$binding$2();

    public WidgetSettingsAccountContactsNameEdit$binding$2() {
        super(1, WidgetSettingsAccountContactsNameEditBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsAccountContactsNameEditBinding;", 0);
    }

    public final WidgetSettingsAccountContactsNameEditBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.edit_account_name_wrap;
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.edit_account_name_wrap);
        if (textInputLayout != null) {
            i = R.id.settings_account_name_clear;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.settings_account_name_clear);
            if (materialButton != null) {
                i = R.id.settings_account_save;
                FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.settings_account_save);
                if (floatingActionButton != null) {
                    return new WidgetSettingsAccountContactsNameEditBinding((CoordinatorLayout) view, textInputLayout, materialButton, floatingActionButton);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
