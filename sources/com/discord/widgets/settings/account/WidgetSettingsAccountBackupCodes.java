package com.discord.widgets.settings.account;

import andhook.lib.HookHelper;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.d.o;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.auth.mfa.BackupCode;
import com.discord.api.auth.mfa.GetBackupCodesRequestBody;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsAccountBackupCodesBinding;
import com.discord.databinding.WidgetSettingsItemBackupCodeBinding;
import com.discord.databinding.WidgetSettingsItemBackupCodeHeaderBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.error.Error;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.settings.account.WidgetSettingsAccountBackupCodes;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccountBackupCodes.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001f2\u00020\u0001:\u0004 !\"\u001fB\u0007¢\u0006\u0004\b\u001e\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\f\u001a\u00020\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001a\u001a\u00020\u00158B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001c\u0010\u001d¨\u0006#"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes;", "Lcom/discord/app/AppFragment;", "", "regenerate", "", "getBackupCodes", "(Z)V", "showPasswordModal", "()V", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "data", "configureUI", "(Ljava/util/List;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "", "password", "Ljava/lang/String;", "Lcom/discord/databinding/WidgetSettingsAccountBackupCodesBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsAccountBackupCodesBinding;", "binding", "Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter;", "backupCodesAdapter", "Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter;", HookHelper.constructorName, "Companion", "Adapter", "BackupCodeItem", "BackupCodeItemHeader", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccountBackupCodes extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsAccountBackupCodes.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsAccountBackupCodesBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private Adapter backupCodesAdapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsAccountBackupCodes$binding$2.INSTANCE, null, 2, null);
    private String password = "";

    /* compiled from: WidgetSettingsAccountBackupCodes.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u000e\u000fB\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rJ+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\t¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "BackupCodeHeaderViewHolder", "BackupCodeViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Adapter extends MGRecyclerAdapterSimple<MGRecyclerDataPayload> {

        /* compiled from: WidgetSettingsAccountBackupCodes.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter$BackupCodeHeaderViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Lcom/discord/databinding/WidgetSettingsItemBackupCodeHeaderBinding;", "binding", "Lcom/discord/databinding/WidgetSettingsItemBackupCodeHeaderBinding;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class BackupCodeHeaderViewHolder extends MGRecyclerViewHolder<Adapter, MGRecyclerDataPayload> {
            private final WidgetSettingsItemBackupCodeHeaderBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public BackupCodeHeaderViewHolder(@LayoutRes int i, Adapter adapter) {
                super(i, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                View view = this.itemView;
                Objects.requireNonNull(view, "rootView");
                TextView textView = (TextView) view;
                WidgetSettingsItemBackupCodeHeaderBinding widgetSettingsItemBackupCodeHeaderBinding = new WidgetSettingsItemBackupCodeHeaderBinding(textView, textView);
                m.checkNotNullExpressionValue(widgetSettingsItemBackupCodeHeaderBinding, "WidgetSettingsItemBackup…derBinding.bind(itemView)");
                this.binding = widgetSettingsItemBackupCodeHeaderBinding;
            }

            public void onConfigure(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
                m.checkNotNullParameter(mGRecyclerDataPayload, "data");
                super.onConfigure(i, (int) mGRecyclerDataPayload);
                TextView textView = this.binding.f2598b;
                m.checkNotNullExpressionValue(textView, "binding.itemHeader");
                textView.setText(((BackupCodeItemHeader) mGRecyclerDataPayload).getHeaderText());
            }
        }

        /* compiled from: WidgetSettingsAccountBackupCodes.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter$BackupCodeViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Lcom/discord/databinding/WidgetSettingsItemBackupCodeBinding;", "binding", "Lcom/discord/databinding/WidgetSettingsItemBackupCodeBinding;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class BackupCodeViewHolder extends MGRecyclerViewHolder<Adapter, MGRecyclerDataPayload> {
            private final WidgetSettingsItemBackupCodeBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public BackupCodeViewHolder(@LayoutRes int i, Adapter adapter) {
                super(i, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                View view = this.itemView;
                Objects.requireNonNull(view, "rootView");
                TextView textView = (TextView) view;
                WidgetSettingsItemBackupCodeBinding widgetSettingsItemBackupCodeBinding = new WidgetSettingsItemBackupCodeBinding(textView, textView);
                m.checkNotNullExpressionValue(widgetSettingsItemBackupCodeBinding, "WidgetSettingsItemBackupCodeBinding.bind(itemView)");
                this.binding = widgetSettingsItemBackupCodeBinding;
            }

            public void onConfigure(int i, final MGRecyclerDataPayload mGRecyclerDataPayload) {
                m.checkNotNullParameter(mGRecyclerDataPayload, "data");
                super.onConfigure(i, (int) mGRecyclerDataPayload);
                BackupCodeItem backupCodeItem = (BackupCodeItem) mGRecyclerDataPayload;
                TextView textView = this.binding.f2597b;
                m.checkNotNullExpressionValue(textView, "binding.itemBackupCodeTv");
                String a = backupCodeItem.getBackupCode().a();
                Objects.requireNonNull(a, "null cannot be cast to non-null type java.lang.String");
                String substring = a.substring(0, 4);
                m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                String a2 = backupCodeItem.getBackupCode().a();
                Objects.requireNonNull(a2, "null cannot be cast to non-null type java.lang.String");
                String substring2 = a2.substring(4);
                m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
                b.m(textView, R.string.backup_codes_dash, new Object[]{substring, substring2}, (r4 & 4) != 0 ? b.g.j : null);
                if (backupCodeItem.getBackupCode().b()) {
                    this.binding.f2597b.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_brand_24dp, 0);
                    this.binding.f2597b.setOnClickListener(null);
                    return;
                }
                this.binding.f2597b.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.binding.f2597b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccountBackupCodes$Adapter$BackupCodeViewHolder$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        m.checkNotNullExpressionValue(view, "it");
                        ClipboardManager clipboardManager = (ClipboardManager) view.getContext().getSystemService("clipboard");
                        ClipData newPlainText = ClipData.newPlainText("backup code", ((WidgetSettingsAccountBackupCodes.BackupCodeItem) MGRecyclerDataPayload.this).getBackupCode().a());
                        if (clipboardManager != null) {
                            clipboardManager.setPrimaryClip(newPlainText);
                        }
                        b.a.d.m.g(view.getContext(), R.string.copied_text, 0, null, 12);
                    }
                });
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Adapter(RecyclerView recyclerView) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recyclerView");
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public MGRecyclerViewHolder<Adapter, MGRecyclerDataPayload> onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            if (i == 0) {
                return new BackupCodeHeaderViewHolder(R.layout.widget_settings_item_backup_code_header, this);
            }
            if (i != 1) {
                return new MGRecyclerViewHolder<>(0, this);
            }
            return new BackupCodeViewHolder(R.layout.widget_settings_item_backup_code, this);
        }
    }

    /* compiled from: WidgetSettingsAccountBackupCodes.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001c\u0010\u0015\u001a\u00020\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\rR\u001c\u0010\u0018\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\n¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$BackupCodeItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/api/auth/mfa/BackupCode;", "component1", "()Lcom/discord/api/auth/mfa/BackupCode;", "backupCode", "copy", "(Lcom/discord/api/auth/mfa/BackupCode;)Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$BackupCodeItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/auth/mfa/BackupCode;", "getBackupCode", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/api/auth/mfa/BackupCode;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BackupCodeItem implements MGRecyclerDataPayload {
        private final BackupCode backupCode;
        private final String key;
        private final int type = 1;

        public BackupCodeItem(BackupCode backupCode) {
            m.checkNotNullParameter(backupCode, "backupCode");
            this.backupCode = backupCode;
            this.key = backupCode.a();
        }

        public static /* synthetic */ BackupCodeItem copy$default(BackupCodeItem backupCodeItem, BackupCode backupCode, int i, Object obj) {
            if ((i & 1) != 0) {
                backupCode = backupCodeItem.backupCode;
            }
            return backupCodeItem.copy(backupCode);
        }

        public final BackupCode component1() {
            return this.backupCode;
        }

        public final BackupCodeItem copy(BackupCode backupCode) {
            m.checkNotNullParameter(backupCode, "backupCode");
            return new BackupCodeItem(backupCode);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof BackupCodeItem) && m.areEqual(this.backupCode, ((BackupCodeItem) obj).backupCode);
            }
            return true;
        }

        public final BackupCode getBackupCode() {
            return this.backupCode;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            BackupCode backupCode = this.backupCode;
            if (backupCode != null) {
                return backupCode.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("BackupCodeItem(backupCode=");
            R.append(this.backupCode);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetSettingsAccountBackupCodes.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001c\u0010\u0015\u001a\u00020\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\rR\u001c\u0010\u0018\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\n¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$BackupCodeItemHeader;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "component1", "()Ljava/lang/CharSequence;", "headerText", "copy", "(Ljava/lang/CharSequence;)Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$BackupCodeItemHeader;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/CharSequence;", "getHeaderText", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Ljava/lang/CharSequence;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BackupCodeItemHeader implements MGRecyclerDataPayload {
        private final CharSequence headerText;
        private final String key;
        private final int type;

        public BackupCodeItemHeader(CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, "headerText");
            this.headerText = charSequence;
            this.key = charSequence.toString();
        }

        public static /* synthetic */ BackupCodeItemHeader copy$default(BackupCodeItemHeader backupCodeItemHeader, CharSequence charSequence, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = backupCodeItemHeader.headerText;
            }
            return backupCodeItemHeader.copy(charSequence);
        }

        public final CharSequence component1() {
            return this.headerText;
        }

        public final BackupCodeItemHeader copy(CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, "headerText");
            return new BackupCodeItemHeader(charSequence);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof BackupCodeItemHeader) && m.areEqual(this.headerText, ((BackupCodeItemHeader) obj).headerText);
            }
            return true;
        }

        public final CharSequence getHeaderText() {
            return this.headerText;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            CharSequence charSequence = this.headerText;
            if (charSequence != null) {
                return charSequence.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.D(a.R("BackupCodeItemHeader(headerText="), this.headerText, ")");
        }
    }

    /* compiled from: WidgetSettingsAccountBackupCodes.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetSettingsAccountBackupCodes.class, new Intent());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsAccountBackupCodes() {
        super(R.layout.widget_settings_account_backup_codes);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(List<? extends MGRecyclerDataPayload> list) {
        Adapter adapter = this.backupCodesAdapter;
        if (adapter == null) {
            m.throwUninitializedPropertyAccessException("backupCodesAdapter");
        }
        adapter.setData(list);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void getBackupCodes(boolean z2) {
        ObservableExtensionsKt.restSubscribeOn$default(ObservableExtensionsKt.withDimmer(ObservableExtensionsKt.ui$default(RestAPI.Companion.getApi().getBackupCodes(new GetBackupCodesRequestBody(this.password, z2)), this, null, 2, null), getBinding().f2564b, 100L), false, 1, null).k(o.a.g(getContext(), new WidgetSettingsAccountBackupCodes$getBackupCodes$1(this), new Action1<Error>() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccountBackupCodes$getBackupCodes$2
            public final void call(Error error) {
                WidgetSettingsAccountBackupCodes.this.showPasswordModal();
            }
        }));
    }

    private final WidgetSettingsAccountBackupCodesBinding getBinding() {
        return (WidgetSettingsAccountBackupCodesBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showPasswordModal() {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            getBinding().f2564b.setDimmed(true, true);
            WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
            b2 = b.b(requireContext(), R.string.user_settings_enter_password_view_codes, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            b3 = b.b(requireContext(), R.string.form_label_password, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            b4 = b.b(requireContext(), R.string.two_fa_backup_codes_label, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            companion.showInputModal(appActivity, b4, b2, b3, new WidgetSettingsAccountBackupCodes$showPasswordModal$$inlined$let$lambda$1(this), new WidgetSettingsAccountBackupCodes$showPasswordModal$$inlined$let$lambda$2(this), Boolean.FALSE);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.two_fa_backup_codes_label);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        TextView textView = getBinding().d;
        m.checkNotNullExpressionValue(textView, "binding.settingsBackupCodesInfo");
        b.m(textView, R.string.two_fa_backup_codes_body, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        RecyclerView recyclerView = getBinding().e;
        m.checkNotNullExpressionValue(recyclerView, "binding.settingsBackupCodesRv");
        this.backupCodesAdapter = new Adapter(recyclerView);
        if (e() != null) {
            MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
            RecyclerView recyclerView2 = getBinding().e;
            m.checkNotNullExpressionValue(recyclerView2, "binding.settingsBackupCodesRv");
            this.backupCodesAdapter = (Adapter) companion.configure(new Adapter(recyclerView2));
        }
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccountBackupCodes$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetSettingsAccountBackupCodes.this.getBackupCodes(true);
            }
        });
        showPasswordModal();
    }
}
