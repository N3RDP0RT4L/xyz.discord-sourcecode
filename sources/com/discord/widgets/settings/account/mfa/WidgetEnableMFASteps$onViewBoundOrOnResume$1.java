package com.discord.widgets.settings.account.mfa;

import com.discord.databinding.WidgetEnableMfaStepsBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.widgets.settings.account.mfa.WidgetEnableMFAViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEnableMFASteps.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;", "it", "", "invoke", "(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEnableMFASteps$onViewBoundOrOnResume$1 extends o implements Function1<WidgetEnableMFAViewModel.ViewState, Unit> {
    public final /* synthetic */ WidgetEnableMFASteps this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEnableMFASteps$onViewBoundOrOnResume$1(WidgetEnableMFASteps widgetEnableMFASteps) {
        super(1);
        this.this$0 = widgetEnableMFASteps;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetEnableMFAViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetEnableMFAViewModel.ViewState viewState) {
        WidgetEnableMfaStepsBinding binding;
        WidgetEnableMfaStepsBinding binding2;
        m.checkNotNullParameter(viewState, "it");
        binding = this.this$0.getBinding();
        DimmerView.setDimmed$default(binding.f2361b, viewState.isLoading(), false, 2, null);
        Integer screenIndex = viewState.getScreenIndex();
        if (screenIndex != null) {
            int intValue = screenIndex.intValue();
            binding2 = this.this$0.getBinding();
            binding2.c.b(intValue);
        }
    }
}
