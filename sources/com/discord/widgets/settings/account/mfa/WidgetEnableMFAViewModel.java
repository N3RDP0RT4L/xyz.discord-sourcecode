package com.discord.widgets.settings.account.mfa;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.auth.mfa.EnableMfaResponse;
import com.discord.app.AppViewModel;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreMFA;
import com.discord.stores.StoreStream;
import com.discord.utilities.auth.AuthUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
/* compiled from: WidgetEnableMFAViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\"B\u0007¢\u0006\u0004\b!\u0010\tJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\n\u0010\tJ\u0011\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u0007¢\u0006\u0004\b\u000b\u0010\fJ\u0011\u0010\r\u001a\u0004\u0018\u00010\u0003H\u0007¢\u0006\u0004\b\r\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u000f\u0010\u0007J\u0017\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0011\u0010\u0007J\u001f\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0017H\u0007¢\u0006\u0004\b\u0019\u0010\u001aR\u0018\u0010\u001b\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001cR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u001c¨\u0006#"}, d2 = {"Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;", "", "token", "", "handleMFASuccess", "(Ljava/lang/String;)V", "handleMFAFailure", "()V", "onCleared", "getTotpSecret", "()Ljava/lang/String;", "getPassword", "secret", "setTotpSecret", "password", "setPassword", "Landroid/content/Context;", "context", "mfaCode", "enableMFA", "(Landroid/content/Context;Ljava/lang/String;)V", "", "currentPage", "updateScreenIndex", "(I)V", "totpSecret", "Ljava/lang/String;", "encodedTotpSecret", "Lrx/subscriptions/CompositeSubscription;", "subs", "Lrx/subscriptions/CompositeSubscription;", HookHelper.constructorName, "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEnableMFAViewModel extends AppViewModel<ViewState> {
    private String encodedTotpSecret;
    private String password;
    private CompositeSubscription subs = new CompositeSubscription();
    private String totpSecret;

    /* compiled from: WidgetEnableMFAViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\b\b\u0002\u0010\b\u001a\u00020\u0002\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0012\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\b\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;", "", "", "component1", "()Z", "", "component2", "()Ljava/lang/Integer;", "isLoading", "screenIndex", "copy", "(ZLjava/lang/Integer;)Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/Integer;", "getScreenIndex", HookHelper.constructorName, "(ZLjava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean isLoading;
        private final Integer screenIndex;

        public ViewState() {
            this(false, null, 3, null);
        }

        public ViewState(boolean z2, Integer num) {
            this.isLoading = z2;
            this.screenIndex = num;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, boolean z2, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = viewState.isLoading;
            }
            if ((i & 2) != 0) {
                num = viewState.screenIndex;
            }
            return viewState.copy(z2, num);
        }

        public final boolean component1() {
            return this.isLoading;
        }

        public final Integer component2() {
            return this.screenIndex;
        }

        public final ViewState copy(boolean z2, Integer num) {
            return new ViewState(z2, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return this.isLoading == viewState.isLoading && m.areEqual(this.screenIndex, viewState.screenIndex);
        }

        public final Integer getScreenIndex() {
            return this.screenIndex;
        }

        public int hashCode() {
            boolean z2 = this.isLoading;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            int i3 = i * 31;
            Integer num = this.screenIndex;
            return i3 + (num != null ? num.hashCode() : 0);
        }

        public final boolean isLoading() {
            return this.isLoading;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(isLoading=");
            R.append(this.isLoading);
            R.append(", screenIndex=");
            return a.E(R, this.screenIndex, ")");
        }

        public /* synthetic */ ViewState(boolean z2, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? null : num);
        }
    }

    public WidgetEnableMFAViewModel() {
        super(new ViewState(false, 0));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleMFAFailure() {
        updateViewState(new ViewState(false, 2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleMFASuccess(String str) {
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getAuthentication().setAuthed(str);
        updateViewState(new ViewState(false, 3));
        companion.getMFA().updatePendingMFAState(StoreMFA.MFAActivationState.PENDING_ENABLED);
    }

    @MainThread
    public final void enableMFA(Context context, String str) {
        String str2;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, "mfaCode");
        updateViewState(new ViewState(true, null));
        RestAPI api = RestAPI.Companion.getApi();
        String str3 = this.encodedTotpSecret;
        if (str3 != null && (str2 = this.password) != null) {
            Observable<EnableMfaResponse> p = api.enableMFA(new RestAPIParams.EnableMFA(str, str3, str2)).p(2000L, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(p, "RestAPI\n        .api\n   …0, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(p, false, 1, null), this, null, 2, null), WidgetEnableMFAViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : new WidgetEnableMFAViewModel$enableMFA$3(this), (r18 & 8) != 0 ? null : new WidgetEnableMFAViewModel$enableMFA$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEnableMFAViewModel$enableMFA$1(this));
        }
    }

    @MainThread
    public final String getPassword() {
        return this.password;
    }

    @MainThread
    public final String getTotpSecret() {
        return this.totpSecret;
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        this.subs.b();
    }

    @MainThread
    public final void setPassword(String str) {
        m.checkNotNullParameter(str, "password");
        this.password = str;
    }

    @MainThread
    public final void setTotpSecret(String str) {
        m.checkNotNullParameter(str, "secret");
        this.totpSecret = str;
        AuthUtils authUtils = AuthUtils.INSTANCE;
        if (str != null) {
            this.encodedTotpSecret = authUtils.encodeTotpSecret(str);
        }
    }

    @MainThread
    public final void updateScreenIndex(int i) {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, Integer.valueOf(i), 1, null));
        }
    }
}
