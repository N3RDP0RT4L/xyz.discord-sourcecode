package com.discord.widgets.settings.account.mfa;

import andhook.lib.HookHelper;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.e0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsEnableMfaKeyBinding;
import com.discord.utilities.auth.AuthUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetEnableMFAKey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0007¢\u0006\u0004\b\u0019\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAKey;", "Lcom/discord/app/AppFragment;", "", "showLaunchTexts", "()V", "Landroid/content/Context;", "context", "copyCodeToClipboard", "(Landroid/content/Context;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;", "viewModel", "Lcom/discord/databinding/WidgetSettingsEnableMfaKeyBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsEnableMfaKeyBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEnableMFAKey extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetEnableMFAKey.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsEnableMfaKeyBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetEnableMFAKey$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetEnableMFAViewModel.class), new WidgetEnableMFAKey$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetEnableMFAKey$viewModel$2.INSTANCE));

    /* compiled from: WidgetEnableMFAKey.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAKey$Companion;", "", "", "packageName", "Landroid/content/pm/PackageManager;", "packageManager", "", "isPackageInstalled", "(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final boolean isPackageInstalled(String str, PackageManager packageManager) {
            try {
                packageManager.getPackageInfo(str, 0);
                return true;
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetEnableMFAKey() {
        super(R.layout.widget_settings_enable_mfa_key);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void copyCodeToClipboard(Context context) {
        AuthUtils authUtils = AuthUtils.INSTANCE;
        TextView textView = getBinding().f2592b;
        m.checkNotNullExpressionValue(textView, "binding.enableMfaKeyCode");
        String encodeTotpSecret = authUtils.encodeTotpSecret(textView.getText().toString());
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService("clipboard");
        ClipData newPlainText = ClipData.newPlainText("two fa code", encodeTotpSecret);
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(newPlainText);
        }
        b.a.d.m.g(context, R.string.copied_text, 0, null, 12);
    }

    private final WidgetSettingsEnableMfaKeyBinding getBinding() {
        return (WidgetSettingsEnableMfaKeyBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final WidgetEnableMFAViewModel getViewModel() {
        return (WidgetEnableMFAViewModel) this.viewModel$delegate.getValue();
    }

    private final void showLaunchTexts() {
        final PackageManager packageManager;
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        CharSequence e4;
        FragmentActivity activity = e();
        if (activity != null && (packageManager = activity.getPackageManager()) != null) {
            Companion companion = Companion;
            if (companion.isPackageInstalled(AuthUtils.AUTHY_PACKAGE, packageManager)) {
                TextView textView = getBinding().c;
                m.checkNotNullExpressionValue(textView, "binding.enableMfaKeyLaunchAuthy");
                e3 = b.e(this, R.string.two_fa_app_name_authy, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                e4 = b.e(this, R.string.launch_app, new Object[]{e3}, (r4 & 4) != 0 ? b.a.j : null);
                textView.setText(e4);
                TextView textView2 = getBinding().c;
                m.checkNotNullExpressionValue(textView2, "binding.enableMfaKeyLaunchAuthy");
                textView2.setVisibility(0);
                getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.mfa.WidgetEnableMFAKey$showLaunchTexts$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        m.checkNotNullExpressionValue(view, "it");
                        Context context = view.getContext();
                        Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(AuthUtils.AUTHY_PACKAGE);
                        if (launchIntentForPackage != null) {
                            context.startActivity(launchIntentForPackage);
                        }
                    }
                });
            }
            if (companion.isPackageInstalled(AuthUtils.GOOGLE_AUTHENTICATOR_PACKAGE, packageManager)) {
                TextView textView3 = getBinding().d;
                m.checkNotNullExpressionValue(textView3, "binding.enableMfaKeyLaunchGoogleAuth");
                e = b.e(this, R.string.two_fa_app_name_google_authenticator, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                e2 = b.e(this, R.string.launch_app, new Object[]{e}, (r4 & 4) != 0 ? b.a.j : null);
                textView3.setText(e2);
                TextView textView4 = getBinding().d;
                m.checkNotNullExpressionValue(textView4, "binding.enableMfaKeyLaunchGoogleAuth");
                textView4.setVisibility(0);
                getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.mfa.WidgetEnableMFAKey$showLaunchTexts$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        m.checkNotNullExpressionValue(view, "it");
                        Context context = view.getContext();
                        Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(AuthUtils.GOOGLE_AUTHENTICATOR_PACKAGE);
                        if (launchIntentForPackage != null) {
                            context.startActivity(launchIntentForPackage);
                        }
                    }
                });
            }
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        TextView textView = getBinding().f2592b;
        m.checkNotNullExpressionValue(textView, "binding.enableMfaKeyCode");
        textView.setText(getViewModel().getTotpSecret());
        getBinding().f2592b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.mfa.WidgetEnableMFAKey$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetEnableMFAKey widgetEnableMFAKey = WidgetEnableMFAKey.this;
                m.checkNotNullExpressionValue(view2, "it");
                Context context = view2.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                widgetEnableMFAKey.copyCodeToClipboard(context);
            }
        });
        showLaunchTexts();
    }
}
