package com.discord.widgets.settings.account.mfa;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetSettingsEnableMfaKeyBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetEnableMFAKey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSettingsEnableMfaKeyBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsEnableMfaKeyBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetEnableMFAKey$binding$2 extends k implements Function1<View, WidgetSettingsEnableMfaKeyBinding> {
    public static final WidgetEnableMFAKey$binding$2 INSTANCE = new WidgetEnableMFAKey$binding$2();

    public WidgetEnableMFAKey$binding$2() {
        super(1, WidgetSettingsEnableMfaKeyBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsEnableMfaKeyBinding;", 0);
    }

    public final WidgetSettingsEnableMfaKeyBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.enable_mfa_key_code;
        TextView textView = (TextView) view.findViewById(R.id.enable_mfa_key_code);
        if (textView != null) {
            i = R.id.enable_mfa_key_launch_authy;
            TextView textView2 = (TextView) view.findViewById(R.id.enable_mfa_key_launch_authy);
            if (textView2 != null) {
                i = R.id.enable_mfa_key_launch_google_auth;
                TextView textView3 = (TextView) view.findViewById(R.id.enable_mfa_key_launch_google_auth);
                if (textView3 != null) {
                    return new WidgetSettingsEnableMfaKeyBinding((LinearLayout) view, textView, textView2, textView3);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
