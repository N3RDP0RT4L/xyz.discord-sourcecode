package com.discord.widgets.settings.account.mfa;

import com.discord.api.auth.mfa.EnableMfaResponse;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEnableMFAViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/auth/mfa/EnableMfaResponse;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/api/auth/mfa/EnableMfaResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEnableMFAViewModel$enableMFA$1 extends o implements Function1<EnableMfaResponse, Unit> {
    public final /* synthetic */ WidgetEnableMFAViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEnableMFAViewModel$enableMFA$1(WidgetEnableMFAViewModel widgetEnableMFAViewModel) {
        super(1);
        this.this$0 = widgetEnableMFAViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(EnableMfaResponse enableMfaResponse) {
        invoke2(enableMfaResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(EnableMfaResponse enableMfaResponse) {
        this.this$0.handleMFASuccess(enableMfaResponse.a());
    }
}
