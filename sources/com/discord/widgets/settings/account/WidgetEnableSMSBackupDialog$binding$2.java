package com.discord.widgets.settings.account;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetEnableSmsBackupDialogBinding;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetEnableSMSBackupDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetEnableSmsBackupDialogBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetEnableSmsBackupDialogBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetEnableSMSBackupDialog$binding$2 extends k implements Function1<View, WidgetEnableSmsBackupDialogBinding> {
    public static final WidgetEnableSMSBackupDialog$binding$2 INSTANCE = new WidgetEnableSMSBackupDialog$binding$2();

    public WidgetEnableSMSBackupDialog$binding$2() {
        super(1, WidgetEnableSmsBackupDialogBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetEnableSmsBackupDialogBinding;", 0);
    }

    public final WidgetEnableSmsBackupDialogBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.enable_sms_backup_body;
        TextView textView = (TextView) view.findViewById(R.id.enable_sms_backup_body);
        if (textView != null) {
            i = R.id.enable_sms_backup_cancel;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.enable_sms_backup_cancel);
            if (materialButton != null) {
                i = R.id.enable_sms_backup_confirm;
                LoadingButton loadingButton = (LoadingButton) view.findViewById(R.id.enable_sms_backup_confirm);
                if (loadingButton != null) {
                    i = R.id.enable_sms_backup_header;
                    TextView textView2 = (TextView) view.findViewById(R.id.enable_sms_backup_header);
                    if (textView2 != null) {
                        i = R.id.enable_sms_backup_password_wrap;
                        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.enable_sms_backup_password_wrap);
                        if (textInputLayout != null) {
                            i = R.id.notice_header_container;
                            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.notice_header_container);
                            if (linearLayout != null) {
                                return new WidgetEnableSmsBackupDialogBinding((LinearLayout) view, textView, materialButton, loadingButton, textView2, textInputLayout, linearLayout);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
