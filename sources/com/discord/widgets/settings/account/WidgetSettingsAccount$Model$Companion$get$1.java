package com.discord.widgets.settings.account;

import androidx.core.app.NotificationCompat;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreMFA;
import com.discord.stores.StoreUserConnections;
import com.discord.widgets.settings.account.WidgetSettingsAccount;
import d0.z.d.m;
import java.util.Collection;
import java.util.ListIterator;
import java.util.Map;
import kotlin.Metadata;
import rx.functions.Func5;
/* compiled from: WidgetSettingsAccount.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0011\u001a\n \u0001*\u0004\u0018\u00010\u000e0\u000e2\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032.\u0010\t\u001a*\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b \u0001*\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u00050\u00052\u000e\u0010\u000b\u001a\n \u0001*\u0004\u0018\u00010\n0\n2\b\u0010\r\u001a\u0004\u0018\u00010\fH\n¢\u0006\u0004\b\u000f\u0010\u0010"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "meUser", "Lcom/discord/stores/StoreMFA$State;", "pendingMFAState", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "guilds", "Lcom/discord/stores/StoreUserConnections$State;", "connectedAccounts", "Lcom/discord/models/experiments/domain/Experiment;", "contactSyncExperiment", "Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/user/MeUser;Lcom/discord/stores/StoreMFA$State;Ljava/util/Map;Lcom/discord/stores/StoreUserConnections$State;Lcom/discord/models/experiments/domain/Experiment;)Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccount$Model$Companion$get$1<T1, T2, T3, T4, T5, R> implements Func5<MeUser, StoreMFA.State, Map<Long, ? extends Guild>, StoreUserConnections.State, Experiment, WidgetSettingsAccount.Model> {
    public static final WidgetSettingsAccount$Model$Companion$get$1 INSTANCE = new WidgetSettingsAccount$Model$Companion$get$1();

    @Override // rx.functions.Func5
    public /* bridge */ /* synthetic */ WidgetSettingsAccount.Model call(MeUser meUser, StoreMFA.State state, Map<Long, ? extends Guild> map, StoreUserConnections.State state2, Experiment experiment) {
        return call2(meUser, state, (Map<Long, Guild>) map, state2, experiment);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final WidgetSettingsAccount.Model call2(MeUser meUser, StoreMFA.State state, Map<Long, Guild> map, StoreUserConnections.State state2, Experiment experiment) {
        ConnectedAccount connectedAccount;
        ConnectedAccount connectedAccount2;
        boolean z2;
        m.checkNotNullExpressionValue(state2, "connectedAccounts");
        ListIterator<ConnectedAccount> listIterator = state2.listIterator(state2.size());
        while (true) {
            connectedAccount = null;
            if (!listIterator.hasPrevious()) {
                connectedAccount2 = null;
                break;
            }
            connectedAccount2 = listIterator.previous();
            if (m.areEqual(connectedAccount2.g(), "contacts")) {
                break;
            }
        }
        connectedAccount = connectedAccount2;
        boolean z3 = true;
        if (experiment != null && experiment.getBucket() == 1) {
        }
        m.checkNotNullExpressionValue(meUser, "meUser");
        m.checkNotNullExpressionValue(state, "pendingMFAState");
        Collection<Guild> values = map.values();
        if (!(values instanceof Collection) || !values.isEmpty()) {
            for (Guild guild : values) {
                if (guild.getOwnerId() == meUser.getId()) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
        }
        z3 = false;
        return new WidgetSettingsAccount.Model(meUser, state, z3, connectedAccount);
    }
}
