package com.discord.widgets.settings.account;

import com.discord.models.user.User;
import com.discord.widgets.settings.account.WidgetSettingsBlockedUsersViewModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsBlockedUsersViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010&\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u0016\u0010\u0004\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "userEntry", "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;", "invoke", "(Ljava/util/Map$Entry;)Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsBlockedUsersViewModel$getItems$1 extends o implements Function1<Map.Entry<? extends Long, ? extends User>, WidgetSettingsBlockedUsersViewModel.Item> {
    public static final WidgetSettingsBlockedUsersViewModel$getItems$1 INSTANCE = new WidgetSettingsBlockedUsersViewModel$getItems$1();

    public WidgetSettingsBlockedUsersViewModel$getItems$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ WidgetSettingsBlockedUsersViewModel.Item invoke(Map.Entry<? extends Long, ? extends User> entry) {
        return invoke2((Map.Entry<Long, ? extends User>) entry);
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final WidgetSettingsBlockedUsersViewModel.Item invoke2(Map.Entry<Long, ? extends User> entry) {
        m.checkNotNullParameter(entry, "userEntry");
        return new WidgetSettingsBlockedUsersViewModel.Item(entry.getValue());
    }
}
