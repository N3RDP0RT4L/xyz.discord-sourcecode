package com.discord.widgets.settings.account;

import android.view.View;
import android.widget.Button;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetSettingsAccountEmailEditBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccountEmailEdit.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSettingsAccountEmailEditBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsAccountEmailEditBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettingsAccountEmailEdit$binding$2 extends k implements Function1<View, WidgetSettingsAccountEmailEditBinding> {
    public static final WidgetSettingsAccountEmailEdit$binding$2 INSTANCE = new WidgetSettingsAccountEmailEdit$binding$2();

    public WidgetSettingsAccountEmailEdit$binding$2() {
        super(1, WidgetSettingsAccountEmailEditBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSettingsAccountEmailEditBinding;", 0);
    }

    public final WidgetSettingsAccountEmailEditBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.dimmer_view;
        DimmerView dimmerView = (DimmerView) view.findViewById(R.id.dimmer_view);
        if (dimmerView != null) {
            i = R.id.edit_account_email_description;
            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.edit_account_email_description);
            if (linkifiedTextView != null) {
                i = R.id.edit_account_email_wrap;
                TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.edit_account_email_wrap);
                if (textInputLayout != null) {
                    i = R.id.settings_account_next;
                    Button button = (Button) view.findViewById(R.id.settings_account_next);
                    if (button != null) {
                        i = R.id.settings_account_save;
                        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.settings_account_save);
                        if (floatingActionButton != null) {
                            return new WidgetSettingsAccountEmailEditBinding((CoordinatorLayout) view, dimmerView, linkifiedTextView, textInputLayout, button, floatingActionButton);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
