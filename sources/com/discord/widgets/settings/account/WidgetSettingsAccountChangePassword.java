package com.discord.widgets.settings.account;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import b.a.d.j;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsAccountChangePasswordBinding;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.auth.AuthUtils;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.discord.utilities.auth.GoogleSmartLockManagerKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccountChangePassword.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0007¢\u0006\u0004\b\u001a\u0010\rJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\f\u0010\rR\u001d\u0010\u0013\u001a\u00020\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0018\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountChangePassword;", "Lcom/discord/app/AppFragment;", "Lcom/discord/models/user/MeUser;", "meUser", "", "configureUI", "(Lcom/discord/models/user/MeUser;)V", "saveNewPassword", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetSettingsAccountChangePasswordBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsAccountChangePasswordBinding;", "binding", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "Lcom/discord/utilities/auth/GoogleSmartLockManager;", "googleSmartLockManager", "Lcom/discord/utilities/auth/GoogleSmartLockManager;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccountChangePassword extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsAccountChangePassword.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsAccountChangePasswordBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private GoogleSmartLockManager googleSmartLockManager;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsAccountChangePassword$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.change_password_new_password_input);

    /* compiled from: WidgetSettingsAccountChangePassword.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/account/WidgetSettingsAccountChangePassword$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsAccountChangePassword.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsAccountChangePassword() {
        super(R.layout.widget_settings_account_change_password);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final MeUser meUser) {
        TextInputLayout textInputLayout = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.changePasswordTwoFactor");
        textInputLayout.setVisibility(meUser.getMfaEnabled() ? 0 : 8);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccountChangePassword$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsAccountChangePassword.this.saveNewPassword(meUser);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSettingsAccountChangePasswordBinding getBinding() {
        return (WidgetSettingsAccountChangePasswordBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void saveNewPassword(MeUser meUser) {
        String str;
        if (meUser.getMfaEnabled()) {
            TextInputLayout textInputLayout = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout, "binding.changePasswordTwoFactor");
            str = ViewExtensions.getTextOrEmpty(textInputLayout);
        } else {
            str = null;
        }
        TextInputLayout textInputLayout2 = getBinding().f2570b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.changePasswordCurrentPasswordInput");
        String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout2);
        TextInputLayout textInputLayout3 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.changePasswordNewPasswordInput");
        final String textOrEmpty2 = ViewExtensions.getTextOrEmpty(textInputLayout3);
        if (textOrEmpty.length() == 0) {
            getBinding().f2570b.requestFocus();
            TextInputLayout textInputLayout4 = getBinding().f2570b;
            m.checkNotNullExpressionValue(textInputLayout4, "binding.changePasswordCurrentPasswordInput");
            TextInputLayout textInputLayout5 = getBinding().f2570b;
            m.checkNotNullExpressionValue(textInputLayout5, "binding.changePasswordCurrentPasswordInput");
            textInputLayout4.setError(textInputLayout5.getContext().getString(R.string.password_required));
        } else if (!AuthUtils.INSTANCE.isValidPasswordLength(textOrEmpty2)) {
            getBinding().c.requestFocus();
            TextInputLayout textInputLayout6 = getBinding().c;
            m.checkNotNullExpressionValue(textInputLayout6, "binding.changePasswordNewPasswordInput");
            TextInputLayout textInputLayout7 = getBinding().c;
            m.checkNotNullExpressionValue(textInputLayout7, "binding.changePasswordNewPasswordInput");
            textInputLayout6.setError(textInputLayout7.getContext().getString(R.string.password_length_error));
        } else {
            if (meUser.getMfaEnabled()) {
                if (str == null || str.length() == 0) {
                    getBinding().e.requestFocus();
                    TextInputLayout textInputLayout8 = getBinding().e;
                    m.checkNotNullExpressionValue(textInputLayout8, "binding.changePasswordTwoFactor");
                    TextInputLayout textInputLayout9 = getBinding().e;
                    m.checkNotNullExpressionValue(textInputLayout9, "binding.changePasswordTwoFactor");
                    textInputLayout8.setError(textInputLayout9.getContext().getString(R.string.two_fa_token_required));
                    return;
                }
            }
            RestAPIParams.UserInfo userInfo = new RestAPIParams.UserInfo(null, null, null, textOrEmpty, textOrEmpty2, null, StoreStream.Companion.getNotifications().getPushToken(), str, null, null, null, 1831, null);
            AppFragment.hideKeyboard$default(this, null, 1, null);
            ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().patchUser(userInfo), false, 1, null), this, null, 2, null), getBinding().f, 0L, 2, null).k(o.j(new Action1<User>() { // from class: com.discord.widgets.settings.account.WidgetSettingsAccountChangePassword$saveNewPassword$1
                public final void call(User user) {
                    GoogleSmartLockManager googleSmartLockManager;
                    b.a.d.m.i(WidgetSettingsAccountChangePassword.this, R.string.saved_settings, 0, 4);
                    StoreStream.Companion.getAuthentication().setAuthed(user.q());
                    Context context = WidgetSettingsAccountChangePassword.this.getContext();
                    if (!(context == null || (googleSmartLockManager = GoogleSmartLockManagerKt.googleSmartLockManager(context)) == null)) {
                        googleSmartLockManager.updateAccountInfo(null, textOrEmpty2);
                    }
                    FragmentActivity activity = WidgetSettingsAccountChangePassword.this.e();
                    if (activity != null) {
                        activity.finish();
                    }
                }
            }, requireContext(), null, 4));
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "view.context");
        this.googleSmartLockManager = new GoogleSmartLockManager(context, null, 2, null);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.change_password);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        FloatingActionButton floatingActionButton = getBinding().d;
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.changePasswordNewPasswordInput");
        statefulViews.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout);
        TextInputLayout textInputLayout2 = getBinding().f2570b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.changePasswordCurrentPasswordInput");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, new WidgetSettingsAccountChangePassword$onViewBound$1(this));
        TextInputLayout textInputLayout3 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.changePasswordNewPasswordInput");
        ViewExtensions.addBindedTextWatcher(textInputLayout3, this, new WidgetSettingsAccountChangePassword$onViewBound$2(this));
        TextInputLayout textInputLayout4 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.changePasswordTwoFactor");
        ViewExtensions.addBindedTextWatcher(textInputLayout4, this, new WidgetSettingsAccountChangePassword$onViewBound$3(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreUser.observeMe$default(StoreStream.Companion.getUsers(), false, 1, null), this, null, 2, null), WidgetSettingsAccountChangePassword.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsAccountChangePassword$onViewBoundOrOnResume$1(this));
    }
}
