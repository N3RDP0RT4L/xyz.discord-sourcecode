package com.discord.widgets.settings.account;

import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import b.i.a.f.e.o.f;
import com.discord.databinding.WidgetSettingsAccountBinding;
import com.discord.utilities.view.extensions.ViewExtensions;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: WidgetSettingsAccount.kt */
@e(c = "com.discord.widgets.settings.account.WidgetSettingsAccount$onViewBound$1", f = "WidgetSettingsAccount.kt", l = {65}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccount$onViewBound$1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public int label;
    public final /* synthetic */ WidgetSettingsAccount this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsAccount$onViewBound$1(WidgetSettingsAccount widgetSettingsAccount, Continuation continuation) {
        super(2, continuation);
        this.this$0 = widgetSettingsAccount;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new WidgetSettingsAccount$onViewBound$1(this.this$0, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((WidgetSettingsAccount$onViewBound$1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        WidgetSettingsAccountBinding binding;
        WidgetSettingsAccountBinding binding2;
        WidgetSettingsAccountBinding binding3;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            l.throwOnFailure(obj);
            this.label = 1;
            if (f.P(500L, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            l.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        binding = this.this$0.getBinding();
        NestedScrollView nestedScrollView = binding.f2567x;
        binding2 = this.this$0.getBinding();
        LinearLayout linearLayout = binding2.v;
        m.checkNotNullExpressionValue(linearLayout, "binding.settingsAccountPrivateDataWrap");
        nestedScrollView.smoothScrollTo(0, linearLayout.getBottom());
        binding3 = this.this$0.getBinding();
        TextView textView = binding3.u;
        m.checkNotNullExpressionValue(textView, "binding.settingsAccountPrivateDataDisable");
        ViewExtensions.hintWithRipple$default(textView, 0L, 1, null);
        return Unit.a;
    }
}
