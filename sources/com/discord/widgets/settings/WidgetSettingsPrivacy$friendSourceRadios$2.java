package com.discord.widgets.settings;

import com.discord.databinding.WidgetSettingsPrivacyBinding;
import com.discord.views.CheckedSetting;
import d0.t.n;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/views/CheckedSetting;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPrivacy$friendSourceRadios$2 extends o implements Function0<List<? extends CheckedSetting>> {
    public final /* synthetic */ WidgetSettingsPrivacy this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPrivacy$friendSourceRadios$2(WidgetSettingsPrivacy widgetSettingsPrivacy) {
        super(0);
        this.this$0 = widgetSettingsPrivacy;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends CheckedSetting> invoke() {
        WidgetSettingsPrivacyBinding binding;
        WidgetSettingsPrivacyBinding binding2;
        WidgetSettingsPrivacyBinding binding3;
        binding = this.this$0.getBinding();
        binding2 = this.this$0.getBinding();
        binding3 = this.this$0.getBinding();
        return n.listOf((Object[]) new CheckedSetting[]{binding.r, binding2.f2610s, binding3.t});
    }
}
