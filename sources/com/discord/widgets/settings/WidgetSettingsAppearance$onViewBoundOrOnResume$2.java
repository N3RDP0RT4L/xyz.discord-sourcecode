package com.discord.widgets.settings;

import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettingsSystem;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsAppearance.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "fontScale", "", "invoke", "(Ljava/lang/Integer;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAppearance$onViewBoundOrOnResume$2 extends o implements Function1<Integer, Unit> {
    public static final WidgetSettingsAppearance$onViewBoundOrOnResume$2 INSTANCE = new WidgetSettingsAppearance$onViewBoundOrOnResume$2();

    public WidgetSettingsAppearance$onViewBoundOrOnResume$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke2(num);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Integer num) {
        StoreUserSettingsSystem userSettingsSystem = StoreStream.Companion.getUserSettingsSystem();
        m.checkNotNullExpressionValue(num, "fontScale");
        userSettingsSystem.setFontScale(num.intValue());
    }
}
