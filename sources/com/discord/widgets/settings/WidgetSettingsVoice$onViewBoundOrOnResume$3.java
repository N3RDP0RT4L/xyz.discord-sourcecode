package com.discord.widgets.settings;

import com.discord.widgets.settings.WidgetSettingsVoice;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetSettingsVoice.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsVoice$Model;", "it", "", "invoke", "(Lcom/discord/widgets/settings/WidgetSettingsVoice$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsVoice$onViewBoundOrOnResume$3 extends o implements Function1<WidgetSettingsVoice.Model, Unit> {
    public final /* synthetic */ WidgetSettingsVoice this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsVoice$onViewBoundOrOnResume$3(WidgetSettingsVoice widgetSettingsVoice) {
        super(1);
        this.this$0 = widgetSettingsVoice;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetSettingsVoice.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetSettingsVoice.Model model) {
        m.checkNotNullParameter(model, "it");
        this.this$0.configureUI(model);
    }
}
