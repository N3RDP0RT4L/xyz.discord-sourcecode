package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsMediaBinding;
import com.discord.stores.StoreAccessibility;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsMedia.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\b\u0010\tR\u001d\u0010\u000f\u001a\u00020\n8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsMedia;", "Lcom/discord/app/AppFragment;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/stores/StoreAccessibility;", "accessibilitySettings", "Lcom/discord/stores/StoreAccessibility;", "Lcom/discord/databinding/WidgetSettingsMediaBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsMediaBinding;", "binding", "Lcom/discord/stores/StoreUserSettings;", "userSettings", "Lcom/discord/stores/StoreUserSettings;", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsMedia extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsMedia.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsMediaBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String PREVIEW_MAX_SIZE_MB = "10";
    private StoreAccessibility accessibilitySettings;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsMedia$binding$2.INSTANCE, null, 2, null);
    private StoreUserSettings userSettings;

    /* compiled from: WidgetSettingsMedia.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", "", "PREVIEW_MAX_SIZE_MB", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsMedia.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsMedia() {
        super(R.layout.widget_settings_media);
    }

    public static final /* synthetic */ StoreUserSettings access$getUserSettings$p(WidgetSettingsMedia widgetSettingsMedia) {
        StoreUserSettings storeUserSettings = widgetSettingsMedia.userSettings;
        if (storeUserSettings == null) {
            m.throwUninitializedPropertyAccessException("userSettings");
        }
        return storeUserSettings;
    }

    private final WidgetSettingsMediaBinding getBinding() {
        return (WidgetSettingsMediaBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void launch(Context context) {
        Companion.launch(context);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        CharSequence e;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.text_and_images);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        StoreStream.Companion companion = StoreStream.Companion;
        this.userSettings = companion.getUserSettings();
        this.accessibilitySettings = companion.getAccessibility();
        CheckedSetting checkedSetting = getBinding().f2604b;
        m.checkNotNullExpressionValue(checkedSetting, "binding.attachmentsToggle");
        StoreUserSettings storeUserSettings = this.userSettings;
        if (storeUserSettings == null) {
            m.throwUninitializedPropertyAccessException("userSettings");
        }
        checkedSetting.setChecked(storeUserSettings.getIsAttachmentMediaInline());
        CheckedSetting checkedSetting2 = getBinding().f2604b;
        e = b.e(this, R.string.inline_attachment_media_help, new Object[]{PREVIEW_MAX_SIZE_MB}, (r4 & 4) != 0 ? b.a.j : null);
        CheckedSetting.i(checkedSetting2, e, false, 2);
        getBinding().f2604b.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsMedia$onViewBound$1
            public final void call(Boolean bool) {
                StoreUserSettings access$getUserSettings$p = WidgetSettingsMedia.access$getUserSettings$p(WidgetSettingsMedia.this);
                AppActivity appActivity = WidgetSettingsMedia.this.getAppActivity();
                m.checkNotNullExpressionValue(bool, "checked");
                access$getUserSettings$p.setIsAttachmentMediaInline(appActivity, bool.booleanValue());
            }
        });
        getBinding().c.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsMedia$onViewBound$2
            public final void call(Boolean bool) {
                StoreUserSettings access$getUserSettings$p = WidgetSettingsMedia.access$getUserSettings$p(WidgetSettingsMedia.this);
                m.checkNotNullExpressionValue(bool, "checked");
                access$getUserSettings$p.setIsAutoImageCompressionEnabled(bool.booleanValue());
            }
        });
        CheckedSetting checkedSetting3 = getBinding().c;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.compressionToggle");
        StoreUserSettings storeUserSettings2 = this.userSettings;
        if (storeUserSettings2 == null) {
            m.throwUninitializedPropertyAccessException("userSettings");
        }
        checkedSetting3.setChecked(storeUserSettings2.getIsAutoImageCompressionEnabled());
        LinkifiedTextView linkifiedTextView = getBinding().d;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.compressionToggleSubtext");
        b.m(linkifiedTextView, R.string.image_compression_nitro_upsell, new Object[]{"getNitro"}, new WidgetSettingsMedia$onViewBound$3(this));
        CheckedSetting checkedSetting4 = getBinding().g;
        m.checkNotNullExpressionValue(checkedSetting4, "binding.settingsTextImagesEmbedsToggle");
        StoreUserSettings storeUserSettings3 = this.userSettings;
        if (storeUserSettings3 == null) {
            m.throwUninitializedPropertyAccessException("userSettings");
        }
        checkedSetting4.setChecked(storeUserSettings3.getIsEmbedMediaInlined());
        getBinding().g.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsMedia$onViewBound$4
            public final void call(Boolean bool) {
                StoreUserSettings access$getUserSettings$p = WidgetSettingsMedia.access$getUserSettings$p(WidgetSettingsMedia.this);
                AppActivity appActivity = WidgetSettingsMedia.this.getAppActivity();
                m.checkNotNullExpressionValue(bool, "checked");
                access$getUserSettings$p.setIsEmbedMediaInlined(appActivity, bool.booleanValue());
            }
        });
        CheckedSetting checkedSetting5 = getBinding().h;
        m.checkNotNullExpressionValue(checkedSetting5, "binding.settingsTextImagesLinksToggle");
        StoreUserSettings storeUserSettings4 = this.userSettings;
        if (storeUserSettings4 == null) {
            m.throwUninitializedPropertyAccessException("userSettings");
        }
        checkedSetting5.setChecked(storeUserSettings4.getIsRenderEmbedsEnabled());
        getBinding().h.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsMedia$onViewBound$5
            public final void call(Boolean bool) {
                StoreUserSettings access$getUserSettings$p = WidgetSettingsMedia.access$getUserSettings$p(WidgetSettingsMedia.this);
                AppActivity appActivity = WidgetSettingsMedia.this.getAppActivity();
                m.checkNotNullExpressionValue(bool, "checked");
                access$getUserSettings$p.setIsRenderEmbedsEnabled(appActivity, bool.booleanValue());
            }
        });
        CheckedSetting checkedSetting6 = getBinding().l;
        m.checkNotNullExpressionValue(checkedSetting6, "binding.settingsTextImagesSyncToggle");
        StoreUserSettings storeUserSettings5 = this.userSettings;
        if (storeUserSettings5 == null) {
            m.throwUninitializedPropertyAccessException("userSettings");
        }
        checkedSetting6.setChecked(storeUserSettings5.getIsSyncTextAndImagesEnabled());
        getBinding().l.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsMedia$onViewBound$6
            public final void call(Boolean bool) {
                StoreUserSettings access$getUserSettings$p = WidgetSettingsMedia.access$getUserSettings$p(WidgetSettingsMedia.this);
                m.checkNotNullExpressionValue(bool, "checked");
                access$getUserSettings$p.getIsSyncTextAndImagesEnabled(bool.booleanValue());
            }
        });
        CheckedSetting checkedSetting7 = getBinding().m;
        m.checkNotNullExpressionValue(checkedSetting7, "binding.stickersSuggestions");
        StoreUserSettings storeUserSettings6 = this.userSettings;
        if (storeUserSettings6 == null) {
            m.throwUninitializedPropertyAccessException("userSettings");
        }
        checkedSetting7.setChecked(storeUserSettings6.getIsStickerSuggestionsEnabled());
        getBinding().m.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsMedia$onViewBound$7
            public final void call(Boolean bool) {
                AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                m.checkNotNullExpressionValue(bool, "checked");
                analyticsTracker.stickerSuggestionsEnabledToggled(bool.booleanValue(), new Traits.Location(null, Traits.Location.Section.SETTINGS_TEXT_AND_IMAGES, null, null, null, 29, null));
                WidgetSettingsMedia.access$getUserSettings$p(WidgetSettingsMedia.this).setIsStickerSuggestionsEnabled(bool.booleanValue());
            }
        });
        for (TextView textView : n.listOf((Object[]) new TextView[]{getBinding().f, getBinding().j, getBinding().e, getBinding().i, getBinding().k})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView, "header");
            accessibilityUtils.setViewIsHeading(textView);
        }
    }
}
