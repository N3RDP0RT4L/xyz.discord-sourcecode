package com.discord.widgets.settings;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import com.discord.databinding.WidgetSettingsPrivacyBinding;
import com.discord.restapi.RestAPIParams;
import com.discord.views.CheckedSetting;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function5;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "consented", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPrivacy$configurePrivacyControls$1<T> implements Action1<Boolean> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ WidgetSettingsPrivacy this$0;

    /* compiled from: WidgetSettingsPrivacy.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0002H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Landroidx/appcompat/app/AlertDialog;", "dialog", "Landroid/widget/TextView;", "dialogHeader", "dialogBody", "dialogCancel", "dialogConfirm", "", "invoke", "(Landroidx/appcompat/app/AlertDialog;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.WidgetSettingsPrivacy$configurePrivacyControls$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function5<AlertDialog, TextView, TextView, TextView, TextView, Unit> {
        public final /* synthetic */ Boolean $consented;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Boolean bool) {
            super(5);
            this.$consented = bool;
        }

        @Override // kotlin.jvm.functions.Function5
        public /* bridge */ /* synthetic */ Unit invoke(AlertDialog alertDialog, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
            invoke2(alertDialog, textView, textView2, textView3, textView4);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(final AlertDialog alertDialog, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
            m.checkNotNullParameter(alertDialog, "dialog");
            m.checkNotNullParameter(textView, "dialogHeader");
            m.checkNotNullParameter(textView2, "dialogBody");
            m.checkNotNullParameter(textView3, "dialogCancel");
            m.checkNotNullParameter(textView4, "dialogConfirm");
            textView.setText(R.string.usage_statistics_disable_modal_title);
            textView2.setText(R.string.usage_statistics_disable_modal_body);
            textView3.setText(R.string.usage_statistics_disable_modal_cancel);
            textView3.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy.configurePrivacyControls.1.1.1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetSettingsPrivacyBinding binding;
                    WidgetSettingsPrivacyBinding binding2;
                    alertDialog.dismiss();
                    binding = WidgetSettingsPrivacy$configurePrivacyControls$1.this.this$0.getBinding();
                    CheckedSetting checkedSetting = binding.f2613z;
                    m.checkNotNullExpressionValue(checkedSetting, "binding.settingsPrivacyStatistics");
                    checkedSetting.setChecked(true);
                    binding2 = WidgetSettingsPrivacy$configurePrivacyControls$1.this.this$0.getBinding();
                    CheckedSetting checkedSetting2 = binding2.f2613z;
                    m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsPrivacyStatistics");
                    checkedSetting2.setEnabled(true);
                }
            });
            textView4.setText(R.string.usage_statistics_disable_modal_confirm);
            textView4.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsPrivacy.configurePrivacyControls.1.1.2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetSettingsPrivacyBinding binding;
                    alertDialog.dismiss();
                    AnonymousClass1 r4 = AnonymousClass1.this;
                    WidgetSettingsPrivacy widgetSettingsPrivacy = WidgetSettingsPrivacy$configurePrivacyControls$1.this.this$0;
                    Boolean bool = r4.$consented;
                    m.checkNotNullExpressionValue(bool, "consented");
                    boolean booleanValue = bool.booleanValue();
                    binding = WidgetSettingsPrivacy$configurePrivacyControls$1.this.this$0.getBinding();
                    CheckedSetting checkedSetting = binding.f2613z;
                    m.checkNotNullExpressionValue(checkedSetting, "binding.settingsPrivacyStatistics");
                    widgetSettingsPrivacy.toggleConsent(booleanValue, RestAPIParams.Consents.Type.USAGE_STATS, checkedSetting);
                }
            });
        }
    }

    public WidgetSettingsPrivacy$configurePrivacyControls$1(WidgetSettingsPrivacy widgetSettingsPrivacy, Context context) {
        this.this$0 = widgetSettingsPrivacy;
        this.$context = context;
    }

    public final void call(Boolean bool) {
        WidgetSettingsPrivacyBinding binding;
        WidgetSettingsPrivacyBinding binding2;
        binding = this.this$0.getBinding();
        CheckedSetting checkedSetting = binding.f2613z;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsPrivacyStatistics");
        checkedSetting.setEnabled(false);
        m.checkNotNullExpressionValue(bool, "consented");
        if (bool.booleanValue()) {
            WidgetSettingsPrivacy widgetSettingsPrivacy = this.this$0;
            boolean booleanValue = bool.booleanValue();
            binding2 = this.this$0.getBinding();
            CheckedSetting checkedSetting2 = binding2.f2613z;
            m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsPrivacyStatistics");
            widgetSettingsPrivacy.toggleConsent(booleanValue, RestAPIParams.Consents.Type.USAGE_STATS, checkedSetting2);
            return;
        }
        this.this$0.confirmConsent(this.$context, new AnonymousClass1(bool));
    }
}
