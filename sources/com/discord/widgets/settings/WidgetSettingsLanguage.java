package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsLanguageBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetSettingsLanguage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0007¢\u0006\u0004\b\u0013\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguage;", "Lcom/discord/app/AppFragment;", "", "locale", "", "configureUI", "(Ljava/lang/String;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetSettingsLanguageBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsLanguageBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsLanguage extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsLanguage.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsLanguageBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsLanguage$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetSettingsLanguage.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\f\u001a\u00020\u000b2\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000e\u001a\u00020\u000b2\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\u000e\u0010\r¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsLanguage$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", "", "locale", "getAsStringInLocale", "(Ljava/lang/String;)Ljava/lang/String;", "", "getLocaleResId", "(Ljava/lang/String;)I", "getLocaleFlagResId", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final String getAsStringInLocale(String str) {
            if (str == null) {
                return "English, USA";
            }
            switch (str.hashCode()) {
                case 3141:
                    return str.equals("bg") ? "български" : "English, USA";
                case 3184:
                    return str.equals("cs") ? "Čeština" : "English, USA";
                case 3197:
                    return str.equals("da") ? "Dansk" : "English, USA";
                case 3201:
                    return str.equals("de") ? "Deutsch" : "English, USA";
                case 3239:
                    return str.equals("el") ? "Ελληνικά" : "English, USA";
                case 3267:
                    return str.equals("fi") ? "Suomi" : "English, USA";
                case 3276:
                    return str.equals("fr") ? "Français" : "English, USA";
                case 3329:
                    return str.equals("hi") ? "हिंदी" : "English, USA";
                case 3338:
                    return str.equals("hr") ? "Hrvatski" : "English, USA";
                case 3341:
                    return str.equals("hu") ? "Magyar" : "English, USA";
                case 3371:
                    return str.equals("it") ? "Italiano" : "English, USA";
                case 3383:
                    return str.equals("ja") ? "日本語" : "English, USA";
                case 3428:
                    return str.equals("ko") ? "한국어" : "English, USA";
                case 3464:
                    return str.equals("lt") ? "Lietuviškai" : "English, USA";
                case 3518:
                    return str.equals("nl") ? "Nederlands" : "English, USA";
                case 3521:
                    return str.equals("no") ? "Norsk" : "English, USA";
                case 3580:
                    return str.equals("pl") ? "Polski" : "English, USA";
                case 3645:
                    return str.equals("ro") ? "Română" : "English, USA";
                case 3651:
                    return str.equals("ru") ? "Русский" : "English, USA";
                case 3700:
                    return str.equals("th") ? "ไทย" : "English, USA";
                case 3710:
                    return str.equals("tr") ? "Türkçe" : "English, USA";
                case 3734:
                    return str.equals("uk") ? "Українська" : "English, USA";
                case 3763:
                    return str.equals("vi") ? "Tiếng Việt" : "English, USA";
                case 96598143:
                    return str.equals("en-GB") ? "English, UK" : "English, USA";
                case 96598594:
                    str.equals("en-US");
                    return "English, USA";
                case 96747053:
                    return str.equals("es-ES") ? "Español" : "English, USA";
                case 106935481:
                    return str.equals("pt-BR") ? "Português do Brasil" : "English, USA";
                case 109766140:
                    return str.equals("sv-SE") ? "Svenska" : "English, USA";
                case 115813226:
                    return str.equals("zh-CN") ? "中文" : "English, USA";
                case 115813762:
                    return str.equals("zh-TW") ? "繁體中文" : "English, USA";
                default:
                    return "English, USA";
            }
        }

        @DrawableRes
        public final int getLocaleFlagResId(String str) {
            if (str == null) {
                return R.drawable.icon_flag_en_us;
            }
            switch (str.hashCode()) {
                case 3141:
                    return str.equals("bg") ? R.drawable.icon_flag_bg : R.drawable.icon_flag_en_us;
                case 3184:
                    return str.equals("cs") ? R.drawable.icon_flag_cs : R.drawable.icon_flag_en_us;
                case 3197:
                    return str.equals("da") ? R.drawable.icon_flag_da : R.drawable.icon_flag_en_us;
                case 3201:
                    return str.equals("de") ? R.drawable.icon_flag_de : R.drawable.icon_flag_en_us;
                case 3239:
                    return str.equals("el") ? R.drawable.icon_flag_el : R.drawable.icon_flag_en_us;
                case 3267:
                    return str.equals("fi") ? R.drawable.icon_flag_fi : R.drawable.icon_flag_en_us;
                case 3276:
                    return str.equals("fr") ? R.drawable.icon_flag_fr : R.drawable.icon_flag_en_us;
                case 3329:
                    return str.equals("hi") ? R.drawable.icon_flag_hi : R.drawable.icon_flag_en_us;
                case 3338:
                    return str.equals("hr") ? R.drawable.icon_flag_hr : R.drawable.icon_flag_en_us;
                case 3341:
                    return str.equals("hu") ? R.drawable.icon_flag_hu : R.drawable.icon_flag_en_us;
                case 3371:
                    return str.equals("it") ? R.drawable.icon_flag_it : R.drawable.icon_flag_en_us;
                case 3383:
                    return str.equals("ja") ? R.drawable.icon_flag_ja : R.drawable.icon_flag_en_us;
                case 3428:
                    return str.equals("ko") ? R.drawable.icon_flag_ko : R.drawable.icon_flag_en_us;
                case 3464:
                    return str.equals("lt") ? R.drawable.icon_flag_lt : R.drawable.icon_flag_en_us;
                case 3518:
                    return str.equals("nl") ? R.drawable.icon_flag_nl : R.drawable.icon_flag_en_us;
                case 3521:
                    return str.equals("no") ? R.drawable.icon_flag_no : R.drawable.icon_flag_en_us;
                case 3580:
                    return str.equals("pl") ? R.drawable.icon_flag_pl : R.drawable.icon_flag_en_us;
                case 3645:
                    return str.equals("ro") ? R.drawable.icon_flag_ro : R.drawable.icon_flag_en_us;
                case 3651:
                    return str.equals("ru") ? R.drawable.icon_flag_ru : R.drawable.icon_flag_en_us;
                case 3700:
                    return str.equals("th") ? R.drawable.icon_flag_th : R.drawable.icon_flag_en_us;
                case 3710:
                    return str.equals("tr") ? R.drawable.icon_flag_tr : R.drawable.icon_flag_en_us;
                case 3734:
                    return str.equals("uk") ? R.drawable.icon_flag_uk : R.drawable.icon_flag_en_us;
                case 3763:
                    return str.equals("vi") ? R.drawable.icon_flag_vi : R.drawable.icon_flag_en_us;
                case 96598143:
                    return str.equals("en-GB") ? R.drawable.icon_flag_en_gb : R.drawable.icon_flag_en_us;
                case 96598594:
                    str.equals("en-US");
                    return R.drawable.icon_flag_en_us;
                case 96747053:
                    return str.equals("es-ES") ? R.drawable.icon_flag_es_es : R.drawable.icon_flag_en_us;
                case 106935481:
                    return str.equals("pt-BR") ? R.drawable.icon_flag_pt_br : R.drawable.icon_flag_en_us;
                case 109766140:
                    return str.equals("sv-SE") ? R.drawable.icon_flag_sv_se : R.drawable.icon_flag_en_us;
                case 115813226:
                    return str.equals("zh-CN") ? R.drawable.icon_flag_zh_cn : R.drawable.icon_flag_en_us;
                case 115813762:
                    return str.equals("zh-TW") ? R.drawable.icon_flag_zh_tw : R.drawable.icon_flag_en_us;
                default:
                    return R.drawable.icon_flag_en_us;
            }
        }

        @StringRes
        public final int getLocaleResId(String str) {
            if (str == null) {
                return R.string.en_us;
            }
            switch (str.hashCode()) {
                case 3141:
                    return str.equals("bg") ? R.string.bg : R.string.en_us;
                case 3184:
                    return str.equals("cs") ? R.string.cs : R.string.en_us;
                case 3197:
                    return str.equals("da") ? R.string.da : R.string.en_us;
                case 3201:
                    return str.equals("de") ? R.string.de : R.string.en_us;
                case 3239:
                    return str.equals("el") ? R.string.el : R.string.en_us;
                case 3267:
                    return str.equals("fi") ? R.string.fi : R.string.en_us;
                case 3276:
                    return str.equals("fr") ? R.string.fr : R.string.en_us;
                case 3329:
                    return str.equals("hi") ? R.string.hi : R.string.en_us;
                case 3338:
                    return str.equals("hr") ? R.string.hr : R.string.en_us;
                case 3341:
                    return str.equals("hu") ? R.string.hu : R.string.en_us;
                case 3371:
                    return str.equals("it") ? R.string.it : R.string.en_us;
                case 3383:
                    return str.equals("ja") ? R.string.ja : R.string.en_us;
                case 3428:
                    return str.equals("ko") ? R.string.ko : R.string.en_us;
                case 3464:
                    return str.equals("lt") ? R.string.lt : R.string.en_us;
                case 3518:
                    return str.equals("nl") ? R.string.nl : R.string.en_us;
                case 3521:
                    return str.equals("no") ? R.string.no : R.string.en_us;
                case 3580:
                    return str.equals("pl") ? R.string.pl : R.string.en_us;
                case 3645:
                    return str.equals("ro") ? R.string.ro : R.string.en_us;
                case 3651:
                    return str.equals("ru") ? R.string.ru : R.string.en_us;
                case 3700:
                    return str.equals("th") ? R.string.th : R.string.en_us;
                case 3710:
                    return str.equals("tr") ? R.string.tr : R.string.en_us;
                case 3734:
                    return str.equals("uk") ? R.string.uk : R.string.en_us;
                case 3763:
                    return str.equals("vi") ? R.string.vi : R.string.en_us;
                case 96598143:
                    return str.equals("en-GB") ? R.string.en_gb : R.string.en_us;
                case 96598594:
                    str.equals("en-US");
                    return R.string.en_us;
                case 96747053:
                    return str.equals("es-ES") ? R.string.es_es : R.string.en_us;
                case 106935481:
                    return str.equals("pt-BR") ? R.string.pt_br : R.string.en_us;
                case 109766140:
                    return str.equals("sv-SE") ? R.string.sv_se : R.string.en_us;
                case 115813226:
                    return str.equals("zh-CN") ? R.string.zh_cn : R.string.en_us;
                case 115813762:
                    return str.equals("zh-TW") ? R.string.zh_tw : R.string.en_us;
                default:
                    return R.string.en_us;
            }
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsLanguage.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetSettingsLanguage() {
        super(R.layout.widget_settings_language);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(String str) {
        TextView textView = getBinding().d;
        m.checkNotNullExpressionValue(textView, "binding.settingsLanguageCurrentText");
        Companion companion = Companion;
        textView.setText(getString(companion.getLocaleResId(str)));
        getBinding().c.setImageResource(companion.getLocaleFlagResId(str));
    }

    private final WidgetSettingsLanguageBinding getBinding() {
        return (WidgetSettingsLanguageBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarSubtitle(R.string.user_settings);
        setActionBarTitle(R.string.language);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        WidgetSettingsLanguageSelect.Companion.registerForResult(this, new WidgetSettingsLanguage$onViewBound$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        StoreStream.Companion companion = StoreStream.Companion;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.getUserSettingsSystem().observeSettings(false), this, null, 2, null), WidgetSettingsLanguage.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsLanguage$onViewBoundOrOnResume$1(this));
        CheckedSetting checkedSetting = getBinding().f;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsLanguageSyncCheck");
        checkedSetting.setChecked(companion.getUserSettingsSystem().getIsLocaleSyncEnabled());
        getBinding().f.setOnCheckedListener(WidgetSettingsLanguage$onViewBoundOrOnResume$2.INSTANCE);
        getBinding().f2601b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsLanguage$onViewBoundOrOnResume$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsLanguageSelect.Companion.show(WidgetSettingsLanguage.this);
            }
        });
        for (TextView textView : n.listOf((Object[]) new TextView[]{getBinding().e, getBinding().g})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView, "header");
            accessibilityUtils.setViewIsHeading(textView);
        }
    }
}
