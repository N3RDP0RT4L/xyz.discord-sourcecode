package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import com.discord.models.presence.Presence;
import com.discord.models.user.MeUser;
import com.discord.widgets.settings.WidgetSettings;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function5;
/* compiled from: WidgetSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\f\u001a\u00020\t2\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0002¢\u0006\u0004\b\n\u0010\u000b"}, d2 = {"Lcom/discord/models/user/MeUser;", "p1", "", "p2", "Lcom/discord/models/presence/Presence;", "p3", "", "p4", "p5", "Lcom/discord/widgets/settings/WidgetSettings$Model;", "invoke", "(Lcom/discord/models/user/MeUser;ZLcom/discord/models/presence/Presence;IZ)Lcom/discord/widgets/settings/WidgetSettings$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetSettings$Model$Companion$get$1 extends k implements Function5<MeUser, Boolean, Presence, Integer, Boolean, WidgetSettings.Model> {
    public static final WidgetSettings$Model$Companion$get$1 INSTANCE = new WidgetSettings$Model$Companion$get$1();

    public WidgetSettings$Model$Companion$get$1() {
        super(5, WidgetSettings.Model.class, HookHelper.constructorName, "<init>(Lcom/discord/models/user/MeUser;ZLcom/discord/models/presence/Presence;IZ)V", 0);
    }

    @Override // kotlin.jvm.functions.Function5
    public /* bridge */ /* synthetic */ WidgetSettings.Model invoke(MeUser meUser, Boolean bool, Presence presence, Integer num, Boolean bool2) {
        return invoke(meUser, bool.booleanValue(), presence, num.intValue(), bool2.booleanValue());
    }

    public final WidgetSettings.Model invoke(MeUser meUser, boolean z2, Presence presence, int i, boolean z3) {
        m.checkNotNullParameter(meUser, "p1");
        m.checkNotNullParameter(presence, "p3");
        return new WidgetSettings.Model(meUser, z2, presence, i, z3);
    }
}
