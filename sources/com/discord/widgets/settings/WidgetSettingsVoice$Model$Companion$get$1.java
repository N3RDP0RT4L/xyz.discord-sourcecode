package com.discord.widgets.settings;

import androidx.core.app.NotificationCompat;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: WidgetSettingsVoice.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "kotlin.jvm.PlatformType", "isListeningForSensitivity", "Lrx/Observable;", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsVoice$Model$Companion$get$1<T, R> implements b<Boolean, Observable<? extends MediaEngine.LocalVoiceStatus>> {
    public static final WidgetSettingsVoice$Model$Companion$get$1 INSTANCE = new WidgetSettingsVoice$Model$Companion$get$1();

    public final Observable<? extends MediaEngine.LocalVoiceStatus> call(Boolean bool) {
        MediaEngine.LocalVoiceStatus localVoiceStatus;
        m.checkNotNullExpressionValue(bool, "isListeningForSensitivity");
        if (bool.booleanValue()) {
            return StoreStream.Companion.getMediaEngine().getLocalVoiceStatus();
        }
        localVoiceStatus = WidgetSettingsVoice.LOCAL_VOICE_STATUS_ENGINE_UNINITIALIZED;
        return new k(localVoiceStatus);
    }
}
