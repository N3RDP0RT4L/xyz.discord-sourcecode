package com.discord.widgets.settings;

import android.content.Context;
import android.view.View;
import com.discord.utilities.navigation.SystemSettingNavigators;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/view/View;", "kotlin.jvm.PlatformType", "it", "", "onClick", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettings$onViewBound$1$25 implements View.OnClickListener {
    public static final WidgetSettings$onViewBound$1$25 INSTANCE = new WidgetSettings$onViewBound$1$25();

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        SystemSettingNavigators systemSettingNavigators = SystemSettingNavigators.INSTANCE;
        m.checkNotNullExpressionValue(view, "it");
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "it.context");
        systemSettingNavigators.openSystemNotification(context);
    }
}
