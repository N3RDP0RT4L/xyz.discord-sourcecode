package com.discord.widgets.settings;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreThreadsJoined;
import com.discord.widgets.settings.MuteSettingsSheetViewModel;
import java.util.Map;
import kotlin.Metadata;
import rx.functions.Func4;
/* compiled from: MuteSettingsSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000f\u001a\n \b*\u0004\u0018\u00010\f0\f2\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022.\u0010\t\u001a*\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007 \b*\u0014\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00040\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\n¢\u0006\u0004\b\r\u0010\u000e"}, d2 = {"Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/channel/Channel;", "channel", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/domain/ModelNotificationSettings;", "kotlin.jvm.PlatformType", "guildNotificationSettings", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "joinedThread", "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Map;Lcom/discord/stores/StoreThreadsJoined$JoinedThread;)Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MuteSettingsSheetViewModel$Companion$observeStoreState$1<T1, T2, T3, T4, R> implements Func4<Guild, Channel, Map<Long, ? extends ModelNotificationSettings>, StoreThreadsJoined.JoinedThread, MuteSettingsSheetViewModel.StoreState> {
    public static final MuteSettingsSheetViewModel$Companion$observeStoreState$1 INSTANCE = new MuteSettingsSheetViewModel$Companion$observeStoreState$1();

    /* JADX WARN: Removed duplicated region for block: B:10:0x0017  */
    /* JADX WARN: Removed duplicated region for block: B:11:0x001c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.settings.MuteSettingsSheetViewModel.StoreState call(com.discord.models.guild.Guild r4, com.discord.api.channel.Channel r5, java.util.Map<java.lang.Long, ? extends com.discord.models.domain.ModelNotificationSettings> r6, com.discord.stores.StoreThreadsJoined.JoinedThread r7) {
        /*
            r3 = this;
            com.discord.widgets.settings.MuteSettingsSheetViewModel$StoreState r0 = new com.discord.widgets.settings.MuteSettingsSheetViewModel$StoreState
            if (r4 == 0) goto Ld
            long r1 = r4.getId()
        L8:
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            goto L15
        Ld:
            if (r5 == 0) goto L14
            long r1 = r5.f()
            goto L8
        L14:
            r1 = 0
        L15:
            if (r1 == 0) goto L1c
            long r1 = r1.longValue()
            goto L1e
        L1c:
            r1 = 0
        L1e:
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            java.lang.Object r6 = r6.get(r1)
            com.discord.models.domain.ModelNotificationSettings r6 = (com.discord.models.domain.ModelNotificationSettings) r6
            r0.<init>(r4, r5, r6, r7)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.MuteSettingsSheetViewModel$Companion$observeStoreState$1.call(com.discord.models.guild.Guild, com.discord.api.channel.Channel, java.util.Map, com.discord.stores.StoreThreadsJoined$JoinedThread):com.discord.widgets.settings.MuteSettingsSheetViewModel$StoreState");
    }
}
