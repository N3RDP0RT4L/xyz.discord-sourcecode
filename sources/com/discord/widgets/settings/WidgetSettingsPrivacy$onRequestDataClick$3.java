package com.discord.widgets.settings;

import android.content.Context;
import com.discord.databinding.WidgetSettingsPrivacyBinding;
import com.discord.models.domain.Harvest;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: WidgetSettingsPrivacy.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "requestHarvest"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsPrivacy$onRequestDataClick$3 extends o implements Function0<Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ WidgetSettingsPrivacy$onRequestDataClick$2 $handleRequestError$2;
    public final /* synthetic */ WidgetSettingsPrivacy$onRequestDataClick$1 $handleRequestSuccess$1;
    public final /* synthetic */ WidgetSettingsPrivacy this$0;

    /* compiled from: WidgetSettingsPrivacy.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.WidgetSettingsPrivacy$onRequestDataClick$3$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Error, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            m.checkNotNullParameter(error, "it");
            WidgetSettingsPrivacy$onRequestDataClick$2 widgetSettingsPrivacy$onRequestDataClick$2 = WidgetSettingsPrivacy$onRequestDataClick$3.this.$handleRequestError$2;
            Error.Response response = error.getResponse();
            m.checkNotNullExpressionValue(response, "it.response");
            widgetSettingsPrivacy$onRequestDataClick$2.invoke2(response.getMessage());
            error.setShowErrorToasts(false);
        }
    }

    /* compiled from: WidgetSettingsPrivacy.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/Harvest;", "harvest", "", "invoke", "(Lcom/discord/models/domain/Harvest;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.WidgetSettingsPrivacy$onRequestDataClick$3$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<Harvest, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Harvest harvest) {
            invoke2(harvest);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Harvest harvest) {
            if (harvest != null) {
                WidgetSettingsPrivacy$onRequestDataClick$3.this.$handleRequestSuccess$1.invoke2(harvest);
            } else {
                WidgetSettingsPrivacy$onRequestDataClick$2.invoke$default(WidgetSettingsPrivacy$onRequestDataClick$3.this.$handleRequestError$2, null, 1, null);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetSettingsPrivacy$onRequestDataClick$3(WidgetSettingsPrivacy widgetSettingsPrivacy, Context context, WidgetSettingsPrivacy$onRequestDataClick$2 widgetSettingsPrivacy$onRequestDataClick$2, WidgetSettingsPrivacy$onRequestDataClick$1 widgetSettingsPrivacy$onRequestDataClick$1) {
        super(0);
        this.this$0 = widgetSettingsPrivacy;
        this.$context = context;
        this.$handleRequestError$2 = widgetSettingsPrivacy$onRequestDataClick$2;
        this.$handleRequestSuccess$1 = widgetSettingsPrivacy$onRequestDataClick$1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetSettingsPrivacyBinding binding;
        Observable ui = ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn(RestAPI.Companion.getApi().requestHarvest(), false));
        binding = this.this$0.getBinding();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.withDimmer(ui, binding.g, 100L), this.this$0.getClass(), (r18 & 2) != 0 ? null : this.$context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass1(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
