package com.discord.widgets.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import b.a.d.f;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetSettingsAccessibilityBinding;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetSettingsAccessibility.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001a2\u00020\u0001:\u0002\u001a\u001bB\u0007¢\u0006\u0004\b\u0019\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\f\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0015\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0018\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAccessibility;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/WidgetSettingsAccessibility$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/settings/WidgetSettingsAccessibility$Model;)V", "", "currentStickerAnimationSettings", "Lcom/discord/views/CheckedSetting;", "radio", "stickerAnimationSetting", "configureStickerAnimationRadio", "(ILcom/discord/views/CheckedSetting;I)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetSettingsAccessibilityBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsAccessibilityBinding;", "binding", "Lcom/discord/views/RadioManager;", "stickersAnimationRadioManager", "Lcom/discord/views/RadioManager;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetSettingsAccessibility extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetSettingsAccessibility.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsAccessibilityBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetSettingsAccessibility$binding$2.INSTANCE, null, 2, null);
    private RadioManager stickersAnimationRadioManager;

    /* compiled from: WidgetSettingsAccessibility.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAccessibility$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetSettingsAccessibility.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetSettingsAccessibility.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0010\b\u0082\b\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB'\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ8\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u0013\u0010\tJ\u001a\u0010\u0015\u001a\u00020\u00022\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\r\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0017\u001a\u0004\b\u0018\u0010\tR\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001c\u0010\u0004¨\u0006 "}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAccessibility$Model;", "", "", "component1", "()Z", "component2", "component3", "", "component4", "()I", "reducedMotionEnabled", "allowAnimatedEmoji", "autoPlayGifs", "currentStickerAnimationSettings", "copy", "(ZZZI)Lcom/discord/widgets/settings/WidgetSettingsAccessibility$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getCurrentStickerAnimationSettings", "Z", "getReducedMotionEnabled", "getAllowAnimatedEmoji", "getAutoPlayGifs", HookHelper.constructorName, "(ZZZI)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean allowAnimatedEmoji;
        private final boolean autoPlayGifs;
        private final int currentStickerAnimationSettings;
        private final boolean reducedMotionEnabled;

        /* compiled from: WidgetSettingsAccessibility.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/settings/WidgetSettingsAccessibility$Model$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/settings/WidgetSettingsAccessibility$Model;", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get() {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<Model> h = Observable.h(companion.getAccessibility().observeReducedMotionEnabled(), companion.getUserSettings().observeIsAnimatedEmojisEnabled(false), companion.getUserSettings().observeIsAutoPlayGifsEnabled(false), companion.getUserSettings().observeStickerAnimationSettings(false), WidgetSettingsAccessibility$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(h, "Observable.combineLatest…ngs\n          )\n        }");
                return h;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(boolean z2, boolean z3, boolean z4, int i) {
            this.reducedMotionEnabled = z2;
            this.allowAnimatedEmoji = z3;
            this.autoPlayGifs = z4;
            this.currentStickerAnimationSettings = i;
        }

        public static /* synthetic */ Model copy$default(Model model, boolean z2, boolean z3, boolean z4, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                z2 = model.reducedMotionEnabled;
            }
            if ((i2 & 2) != 0) {
                z3 = model.allowAnimatedEmoji;
            }
            if ((i2 & 4) != 0) {
                z4 = model.autoPlayGifs;
            }
            if ((i2 & 8) != 0) {
                i = model.currentStickerAnimationSettings;
            }
            return model.copy(z2, z3, z4, i);
        }

        public final boolean component1() {
            return this.reducedMotionEnabled;
        }

        public final boolean component2() {
            return this.allowAnimatedEmoji;
        }

        public final boolean component3() {
            return this.autoPlayGifs;
        }

        public final int component4() {
            return this.currentStickerAnimationSettings;
        }

        public final Model copy(boolean z2, boolean z3, boolean z4, int i) {
            return new Model(z2, z3, z4, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return this.reducedMotionEnabled == model.reducedMotionEnabled && this.allowAnimatedEmoji == model.allowAnimatedEmoji && this.autoPlayGifs == model.autoPlayGifs && this.currentStickerAnimationSettings == model.currentStickerAnimationSettings;
        }

        public final boolean getAllowAnimatedEmoji() {
            return this.allowAnimatedEmoji;
        }

        public final boolean getAutoPlayGifs() {
            return this.autoPlayGifs;
        }

        public final int getCurrentStickerAnimationSettings() {
            return this.currentStickerAnimationSettings;
        }

        public final boolean getReducedMotionEnabled() {
            return this.reducedMotionEnabled;
        }

        public int hashCode() {
            boolean z2 = this.reducedMotionEnabled;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.allowAnimatedEmoji;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.autoPlayGifs;
            if (!z4) {
                i = z4 ? 1 : 0;
            }
            return ((i7 + i) * 31) + this.currentStickerAnimationSettings;
        }

        public String toString() {
            StringBuilder R = a.R("Model(reducedMotionEnabled=");
            R.append(this.reducedMotionEnabled);
            R.append(", allowAnimatedEmoji=");
            R.append(this.allowAnimatedEmoji);
            R.append(", autoPlayGifs=");
            R.append(this.autoPlayGifs);
            R.append(", currentStickerAnimationSettings=");
            return a.A(R, this.currentStickerAnimationSettings, ")");
        }
    }

    public WidgetSettingsAccessibility() {
        super(R.layout.widget_settings_accessibility);
    }

    private final void configureStickerAnimationRadio(int i, CheckedSetting checkedSetting, final int i2) {
        checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsAccessibility$configureStickerAnimationRadio$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoreStream.Companion.getUserSettings().setStickerAnimationSettings(WidgetSettingsAccessibility.this.getAppActivity(), i2);
            }
        });
        RadioManager radioManager = this.stickersAnimationRadioManager;
        if (radioManager != null && i == i2 && radioManager != null) {
            radioManager.a(checkedSetting);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        CheckedSetting checkedSetting = getBinding().g;
        m.checkNotNullExpressionValue(checkedSetting, "binding.settingsAccessibilityReducedMotionSwitch");
        checkedSetting.setChecked(model.getReducedMotionEnabled());
        if (model.getReducedMotionEnabled()) {
            getBinding().f2563b.b(R.string.accessibility_reduced_motion_settings_override);
            CheckedSetting checkedSetting2 = getBinding().f2563b;
            m.checkNotNullExpressionValue(checkedSetting2, "binding.settingsAccessib…tyAllowAnimateEmojiSwitch");
            checkedSetting2.setChecked(false);
            for (CheckedSetting checkedSetting3 : n.listOf((Object[]) new CheckedSetting[]{getBinding().i, getBinding().j, getBinding().k})) {
                checkedSetting3.b(R.string.stickers_auto_play_help_disabled);
            }
            if (model.getCurrentStickerAnimationSettings() != 2) {
                RadioManager radioManager = this.stickersAnimationRadioManager;
                if (radioManager != null) {
                    CheckedSetting checkedSetting4 = getBinding().j;
                    m.checkNotNullExpressionValue(checkedSetting4, "binding.stickersAnimateOnInteraction");
                    radioManager.a(checkedSetting4);
                }
            } else {
                RadioManager radioManager2 = this.stickersAnimationRadioManager;
                if (radioManager2 != null) {
                    CheckedSetting checkedSetting5 = getBinding().k;
                    m.checkNotNullExpressionValue(checkedSetting5, "binding.stickersNeverAnimate");
                    radioManager2.a(checkedSetting5);
                }
            }
            CheckedSetting checkedSetting6 = getBinding().c;
            m.checkNotNullExpressionValue(checkedSetting6, "binding.settingsAccessib…ityAllowAutoplayGifSwitch");
            checkedSetting6.setChecked(false);
            getBinding().c.b(R.string.accessibility_reduced_motion_settings_override);
            return;
        }
        CheckedSetting checkedSetting7 = getBinding().f2563b;
        m.checkNotNullExpressionValue(checkedSetting7, "binding.settingsAccessib…tyAllowAnimateEmojiSwitch");
        checkedSetting7.setChecked(model.getAllowAnimatedEmoji());
        int currentStickerAnimationSettings = model.getCurrentStickerAnimationSettings();
        CheckedSetting checkedSetting8 = getBinding().i;
        m.checkNotNullExpressionValue(checkedSetting8, "binding.stickersAlwaysAnimate");
        configureStickerAnimationRadio(currentStickerAnimationSettings, checkedSetting8, 0);
        int currentStickerAnimationSettings2 = model.getCurrentStickerAnimationSettings();
        CheckedSetting checkedSetting9 = getBinding().j;
        m.checkNotNullExpressionValue(checkedSetting9, "binding.stickersAnimateOnInteraction");
        configureStickerAnimationRadio(currentStickerAnimationSettings2, checkedSetting9, 1);
        int currentStickerAnimationSettings3 = model.getCurrentStickerAnimationSettings();
        CheckedSetting checkedSetting10 = getBinding().k;
        m.checkNotNullExpressionValue(checkedSetting10, "binding.stickersNeverAnimate");
        configureStickerAnimationRadio(currentStickerAnimationSettings3, checkedSetting10, 2);
        CheckedSetting checkedSetting11 = getBinding().c;
        m.checkNotNullExpressionValue(checkedSetting11, "binding.settingsAccessib…ityAllowAutoplayGifSwitch");
        checkedSetting11.setChecked(model.getAutoPlayGifs());
        getBinding().f2563b.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsAccessibility$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsAccessibilityBinding binding;
                WidgetSettingsAccessibilityBinding binding2;
                binding = WidgetSettingsAccessibility.this.getBinding();
                CheckedSetting checkedSetting12 = binding.f2563b;
                binding2 = WidgetSettingsAccessibility.this.getBinding();
                CheckedSetting checkedSetting13 = binding2.f2563b;
                m.checkNotNullExpressionValue(checkedSetting13, "binding.settingsAccessib…tyAllowAnimateEmojiSwitch");
                checkedSetting12.g(!checkedSetting13.isChecked(), true);
            }
        });
        getBinding().c.e(new View.OnClickListener() { // from class: com.discord.widgets.settings.WidgetSettingsAccessibility$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsAccessibilityBinding binding;
                WidgetSettingsAccessibilityBinding binding2;
                binding = WidgetSettingsAccessibility.this.getBinding();
                CheckedSetting checkedSetting12 = binding.c;
                binding2 = WidgetSettingsAccessibility.this.getBinding();
                CheckedSetting checkedSetting13 = binding2.c;
                m.checkNotNullExpressionValue(checkedSetting13, "binding.settingsAccessib…ityAllowAutoplayGifSwitch");
                checkedSetting12.g(!checkedSetting13.isChecked(), true);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetSettingsAccessibilityBinding getBinding() {
        return (WidgetSettingsAccessibilityBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(), this, null, 2, null), WidgetSettingsAccessibility.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetSettingsAccessibility$onViewBoundOrOnResume$1(this));
        setActionBarTitle(R.string.accessibility);
        setActionBarSubtitle(R.string.user_settings);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        getBinding().g.setOnCheckedListener(WidgetSettingsAccessibility$onViewBoundOrOnResume$2.INSTANCE);
        LinkifiedTextView linkifiedTextView = getBinding().e;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.settingsAccessib…yReducedMotionDescription");
        b.m(linkifiedTextView, R.string.accessibility_prefers_reduced_motion_description, new Object[]{f.a.a(360040613412L, null)}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().f2563b.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.settings.WidgetSettingsAccessibility$onViewBoundOrOnResume$3
            public final void call(Boolean bool) {
                StoreUserSettings userSettings = StoreStream.Companion.getUserSettings();
                AppActivity appActivity = WidgetSettingsAccessibility.this.getAppActivity();
                m.checkNotNullExpressionValue(bool, "checked");
                userSettings.setIsAnimatedEmojisEnabled(appActivity, bool.booleanValue());
            }
        });
        getBinding().c.setOnCheckedListener(WidgetSettingsAccessibility$onViewBoundOrOnResume$4.INSTANCE);
        this.stickersAnimationRadioManager = new RadioManager(n.listOf((Object[]) new CheckedSetting[]{getBinding().i, getBinding().j, getBinding().k}));
        for (TextView textView : n.listOf((Object[]) new TextView[]{getBinding().f, getBinding().d, getBinding().h})) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            m.checkNotNullExpressionValue(textView, "header");
            accessibilityUtils.setViewIsHeading(textView);
        }
    }
}
