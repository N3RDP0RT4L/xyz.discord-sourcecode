package com.discord.widgets.settings.profile;

import androidx.core.app.NotificationCompat;
import com.discord.api.user.UserProfile;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.nullserializable.NullSerializable;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.settings.profile.SettingsUserProfileViewModel;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import j0.k.b;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState;", "kotlin.jvm.PlatformType", "viewState", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState;)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsUserProfileViewModel$observeHeaderViewState$1<T, R> implements b<SettingsUserProfileViewModel.ViewState, UserProfileHeaderViewModel.ViewState> {
    public static final SettingsUserProfileViewModel$observeHeaderViewState$1 INSTANCE = new SettingsUserProfileViewModel$observeHeaderViewState$1();

    public final UserProfileHeaderViewModel.ViewState call(SettingsUserProfileViewModel.ViewState viewState) {
        MeUser meUser;
        String str;
        GuildMember guildMember;
        if (viewState == null) {
            return UserProfileHeaderViewModel.ViewState.Uninitialized.INSTANCE;
        }
        if (viewState instanceof SettingsUserProfileViewModel.ViewState.Loaded) {
            SettingsUserProfileViewModel.ViewState.Loaded loaded = (SettingsUserProfileViewModel.ViewState.Loaded) viewState;
            if (loaded.getCurrentAvatar() == null || loaded.getMeMember() != null) {
                meUser = loaded.getUser();
            } else {
                meUser = r3.copy((r38 & 1) != 0 ? r3.getId() : 0L, (r38 & 2) != 0 ? r3.getUsername() : null, (r38 & 4) != 0 ? r3.getAvatar() : loaded.getCurrentAvatar().a(), (r38 & 8) != 0 ? r3.getBanner() : null, (r38 & 16) != 0 ? r3.isBot() : false, (r38 & 32) != 0 ? r3.isSystemUser() : false, (r38 & 64) != 0 ? r3.getDiscriminator() : 0, (r38 & 128) != 0 ? r3.getPremiumTier() : null, (r38 & 256) != 0 ? r3.email : null, (r38 & 512) != 0 ? r3.mfaEnabled : false, (r38 & 1024) != 0 ? r3.isVerified : false, (r38 & 2048) != 0 ? r3.token : null, (r38 & 4096) != 0 ? r3.getFlags() : 0, (r38 & 8192) != 0 ? r3.getPublicFlags() : 0, (r38 & 16384) != 0 ? r3.phoneNumber : null, (r38 & 32768) != 0 ? r3.nsfwAllowance : null, (r38 & 65536) != 0 ? r3.getBio() : null, (r38 & 131072) != 0 ? loaded.getUser().getBannerColor() : null);
            }
            MeUser meUser2 = meUser;
            UserProfile userProfile = loaded.getUserProfile();
            if (loaded.getCurrentBannerImage() == null) {
                GuildMember meMember = loaded.getMeMember();
                if (meMember == null || (str = meMember.getBannerHash()) == null) {
                    str = loaded.getUser().getBanner();
                }
            } else if (loaded.getCurrentBannerImage() instanceof NullSerializable.a) {
                str = loaded.getMeMember() != null ? loaded.getUser().getBanner() : null;
            } else {
                str = loaded.getCurrentBannerImage().a();
            }
            String str2 = str;
            NullSerializable<String> currentBannerColorHex = loaded.getCurrentBannerColorHex();
            String bannerColor = (!(currentBannerColorHex instanceof NullSerializable.b) && !(currentBannerColorHex instanceof NullSerializable.a)) ? loaded.getUser().getBannerColor() : currentBannerColorHex.a();
            boolean isPremium = UserUtils.INSTANCE.isPremium(loaded.getUser());
            boolean isVerified = loaded.getUser().isVerified();
            if (loaded.getMeMember() != null) {
                GuildMember meMember2 = loaded.getMeMember();
                if (loaded.getCurrentAvatar() != null) {
                    meMember2 = meMember2.copy((r33 & 1) != 0 ? meMember2.color : 0, (r33 & 2) != 0 ? meMember2.hoistRoleId : 0L, (r33 & 4) != 0 ? meMember2.roles : null, (r33 & 8) != 0 ? meMember2.nick : null, (r33 & 16) != 0 ? meMember2.premiumSince : null, (r33 & 32) != 0 ? meMember2.pending : false, (r33 & 64) != 0 ? meMember2.joinedAt : null, (r33 & 128) != 0 ? meMember2.guildId : 0L, (r33 & 256) != 0 ? meMember2.userId : 0L, (r33 & 512) != 0 ? meMember2.avatarHash : loaded.getCurrentAvatar().a(), (r33 & 1024) != 0 ? meMember2.bannerHash : null, (r33 & 2048) != 0 ? meMember2.bio : null, (r33 & 4096) != 0 ? meMember2.communicationDisabledUntil : null);
                }
                GuildMember guildMember2 = meMember2;
                if (loaded.getCurrentBannerImage() != null) {
                    guildMember2 = guildMember2.copy((r33 & 1) != 0 ? guildMember2.color : 0, (r33 & 2) != 0 ? guildMember2.hoistRoleId : 0L, (r33 & 4) != 0 ? guildMember2.roles : null, (r33 & 8) != 0 ? guildMember2.nick : null, (r33 & 16) != 0 ? guildMember2.premiumSince : null, (r33 & 32) != 0 ? guildMember2.pending : false, (r33 & 64) != 0 ? guildMember2.joinedAt : null, (r33 & 128) != 0 ? guildMember2.guildId : 0L, (r33 & 256) != 0 ? guildMember2.userId : 0L, (r33 & 512) != 0 ? guildMember2.avatarHash : null, (r33 & 1024) != 0 ? guildMember2.bannerHash : loaded.getCurrentBannerImage().a(), (r33 & 2048) != 0 ? guildMember2.bio : null, (r33 & 4096) != 0 ? guildMember2.communicationDisabledUntil : null);
                }
                guildMember = guildMember2;
            } else {
                guildMember = loaded.getMeMember();
            }
            return new UserProfileHeaderViewModel.ViewState.Loaded(meUser2, str2, bannerColor, guildMember, null, null, null, userProfile, isPremium, isVerified, false, false, true, false, false, false, 58480, null);
        }
        throw new NoWhenBranchMatchedException();
    }
}
