package com.discord.widgets.settings.profile;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.ColorInt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.b.d;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import b.k.a.a.f;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetEditProfileBannerSheetBinding;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.profile.EditProfileBannerSheetViewModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetEditProfileBannerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 ?2\u00020\u0001:\u0001?B\u0007¢\u0006\u0004\b>\u0010\bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\t\u0010\bJ\u0019\u0010\f\u001a\u00020\u00042\b\b\u0001\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\nH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ!\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0016\u0010\bJ\u000f\u0010\u0017\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0017\u0010\bR\u001d\u0010\u001d\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u001d\u0010(\u001a\u00020&8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010 \u001a\u0004\b(\u0010)R(\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00040*8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u0018\u00102\u001a\u0004\u0018\u0001018\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u00103R(\u00104\u001a\b\u0012\u0004\u0012\u00020\u00040*8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b4\u0010,\u001a\u0004\b5\u0010.\"\u0004\b6\u00100R0\u00108\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\n\u0012\u0004\u0012\u00020\u0004078\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b8\u00109\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=¨\u0006@"}, d2 = {"Lcom/discord/widgets/settings/profile/WidgetEditProfileBannerSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/settings/profile/EditProfileBannerSheetViewModel$ViewState;", "viewState", "", "configureUi", "(Lcom/discord/widgets/settings/profile/EditProfileBannerSheetViewModel$ViewState;)V", "configureStringsForGuild", "()V", "navigateToUpsellModal", "", "initialColor", "launchColorPicker", "(I)V", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "onPause", "Lcom/discord/databinding/WidgetEditProfileBannerSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetEditProfileBannerSheetBinding;", "binding", "Lcom/discord/widgets/settings/profile/EditProfileBannerSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/profile/EditProfileBannerSheetViewModel;", "viewModel", "defaultBannerColor", "I", "", "isGuildContext$delegate", "isGuildContext", "()Z", "Lkotlin/Function0;", "onRemoveProfileBannerImage", "Lkotlin/jvm/functions/Function0;", "getOnRemoveProfileBannerImage", "()Lkotlin/jvm/functions/Function0;", "setOnRemoveProfileBannerImage", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/jaredrummler/android/colorpicker/ColorPickerDialog;", "colorPickerDialog", "Lcom/jaredrummler/android/colorpicker/ColorPickerDialog;", "onChangeProfileBannerImage", "getOnChangeProfileBannerImage", "setOnChangeProfileBannerImage", "Lkotlin/Function1;", "onColorSelected", "Lkotlin/jvm/functions/Function1;", "getOnColorSelected", "()Lkotlin/jvm/functions/Function1;", "setOnColorSelected", "(Lkotlin/jvm/functions/Function1;)V", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEditProfileBannerSheet extends AppBottomSheet {
    private static final String ARG_DEFAULT_BANNER_COLOR_HEX = "ARG_DEFAULT_BANNER_COLOR_HEX";
    private static final String ARG_HAS_BANNER_IMAGE = "ARG_HAS_BANNER_IMAGE";
    private static final String ARG_INITIAL_COLOR_PREVIEW_HEX = "ARG_INITIAL_COLOR_PREVIEW_HEX";
    private static final String DIALOG_TAG_COLOR_PICKER = "DIALOG_TAG_COLOR_PICKER";
    private ColorPickerDialog colorPickerDialog;
    private int defaultBannerColor;
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetEditProfileBannerSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetEditProfileBannerSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private Function0<Unit> onChangeProfileBannerImage = WidgetEditProfileBannerSheet$onChangeProfileBannerImage$1.INSTANCE;
    private Function0<Unit> onRemoveProfileBannerImage = WidgetEditProfileBannerSheet$onRemoveProfileBannerImage$1.INSTANCE;
    private Function1<? super Integer, Unit> onColorSelected = WidgetEditProfileBannerSheet$onColorSelected$1.INSTANCE;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetEditProfileBannerSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy isGuildContext$delegate = g.lazy(new WidgetEditProfileBannerSheet$isGuildContext$2(this));

    /* compiled from: WidgetEditProfileBannerSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001a\u0010\u001bJy\u0010\u0013\u001a\u00020\r2\u0010\b\u0002\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0016\b\u0002\u0010\u0012\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0011\u0012\u0004\u0012\u00020\r0\u0010¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0016R\u0016\u0010\u0019\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0016¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/settings/profile/WidgetEditProfileBannerSheet$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "defaultBannerColorHex", "initialColorPreviewHex", "", "hasBannerImage", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lkotlin/Function0;", "", "onChangeProfileBannerImage", "onRemoveProfileBannerImage", "Lkotlin/Function1;", "", "onColorSelected", "show", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLandroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", WidgetEditProfileBannerSheet.ARG_DEFAULT_BANNER_COLOR_HEX, "Ljava/lang/String;", WidgetEditProfileBannerSheet.ARG_HAS_BANNER_IMAGE, WidgetEditProfileBannerSheet.ARG_INITIAL_COLOR_PREVIEW_HEX, WidgetEditProfileBannerSheet.DIALOG_TAG_COLOR_PICKER, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Long l, String str, String str2, boolean z2, FragmentManager fragmentManager, Function0<Unit> function0, Function0<Unit> function02, Function1<? super Integer, Unit> function1) {
            m.checkNotNullParameter(str, "defaultBannerColorHex");
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(function0, "onChangeProfileBannerImage");
            m.checkNotNullParameter(function02, "onRemoveProfileBannerImage");
            m.checkNotNullParameter(function1, "onColorSelected");
            WidgetEditProfileBannerSheet widgetEditProfileBannerSheet = new WidgetEditProfileBannerSheet();
            Bundle bundle = new Bundle();
            bundle.putString(WidgetEditProfileBannerSheet.ARG_DEFAULT_BANNER_COLOR_HEX, str);
            bundle.putString(WidgetEditProfileBannerSheet.ARG_INITIAL_COLOR_PREVIEW_HEX, str2);
            bundle.putBoolean(WidgetEditProfileBannerSheet.ARG_HAS_BANNER_IMAGE, z2);
            if (l != null) {
                bundle.putLong("com.discord.intent.extra.EXTRA_GUILD_ID", l.longValue());
            }
            widgetEditProfileBannerSheet.setArguments(bundle);
            widgetEditProfileBannerSheet.setOnChangeProfileBannerImage(function0);
            widgetEditProfileBannerSheet.setOnRemoveProfileBannerImage(function02);
            widgetEditProfileBannerSheet.setOnColorSelected(function1);
            widgetEditProfileBannerSheet.show(fragmentManager, WidgetEditProfileBannerSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetEditProfileBannerSheet() {
        super(false, 1, null);
        WidgetEditProfileBannerSheet$viewModel$2 widgetEditProfileBannerSheet$viewModel$2 = new WidgetEditProfileBannerSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(EditProfileBannerSheetViewModel.class), new WidgetEditProfileBannerSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetEditProfileBannerSheet$viewModel$2));
    }

    private final void configureStringsForGuild() {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        MaterialTextView materialTextView = getBinding().e;
        m.checkNotNullExpressionValue(materialTextView, "binding.bannerColorItemTitle");
        e = b.e(this, R.string.change_identity_profile_color, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        materialTextView.setText(e);
        MaterialTextView materialTextView2 = getBinding().c;
        m.checkNotNullExpressionValue(materialTextView2, "binding.bannerChangeImageItemTitle");
        e2 = b.e(this, R.string.change_identity_profile_banner, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        materialTextView2.setText(e2);
        MaterialTextView materialTextView3 = getBinding().g;
        m.checkNotNullExpressionValue(materialTextView3, "binding.bannerRemoveImageItem");
        e3 = b.e(this, R.string.change_identity_reset_banner, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        materialTextView3.setText(e3);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Type inference failed for: r2v5, types: [T, java.lang.Integer] */
    public final void configureUi(final EditProfileBannerSheetViewModel.ViewState viewState) {
        if (viewState instanceof EditProfileBannerSheetViewModel.ViewState) {
            String bannerColorHex = viewState.getBannerColorHex();
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            if (isGuildContext()) {
                configureStringsForGuild();
            }
            if (bannerColorHex != null) {
                try {
                    ref$ObjectRef.element = Integer.valueOf(Color.parseColor(bannerColorHex));
                } catch (IllegalArgumentException e) {
                    Logger.e$default(AppLog.g, a.v("failed to parse banner color string: ", bannerColorHex), e, null, 4, null);
                }
            }
            if (((Integer) ref$ObjectRef.element) != null) {
                ImageView imageView = getBinding().f;
                m.checkNotNullExpressionValue(imageView, "binding.bannerColorPreview");
                ColorCompatKt.tintWithColor(imageView, ((Integer) ref$ObjectRef.element).intValue());
            }
            ImageView imageView2 = getBinding().f;
            m.checkNotNullExpressionValue(imageView2, "binding.bannerColorPreview");
            boolean z2 = false;
            imageView2.setVisibility(((Integer) ref$ObjectRef.element) == null ? 4 : 0);
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditProfileBannerSheet$configureUi$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetEditProfileBannerSheet widgetEditProfileBannerSheet = WidgetEditProfileBannerSheet.this;
                    Integer num = (Integer) ref$ObjectRef.element;
                    widgetEditProfileBannerSheet.launchColorPicker(num != null ? num.intValue() : widgetEditProfileBannerSheet.defaultBannerColor);
                }
            });
            MaterialButton materialButton = getBinding().h;
            m.checkNotNullExpressionValue(materialButton, "binding.premiumUpsellButton");
            materialButton.setVisibility(viewState.getShowPremiumUpsell() ? 0 : 8);
            MaterialButton materialButton2 = getBinding().h;
            m.checkNotNullExpressionValue(materialButton2, "binding.premiumUpsellButton");
            if (materialButton2.getVisibility() == 0) {
                z2 = true;
            }
            if (z2 && !getViewModel().getUpsellViewedTracked()) {
                AnalyticsTracker.premiumUpsellViewed$default(AnalyticsTracker.INSTANCE, AnalyticsTracker.PremiumUpsellType.CustomProfileBannerUpsell, new Traits.Location(Traits.Location.Page.USER_SETTINGS, "User Profile", Traits.Location.Obj.EDIT_PROFILE_BANNER, null, null, 24, null), null, null, 12, null);
                getViewModel().setUpsellViewedTracked(true);
            }
            getBinding().f2348b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditProfileBannerSheet$configureUi$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    if (viewState.getShowPremiumUpsell()) {
                        WidgetEditProfileBannerSheet.this.navigateToUpsellModal();
                        return;
                    }
                    WidgetEditProfileBannerSheet.this.getOnChangeProfileBannerImage().invoke();
                    WidgetEditProfileBannerSheet.this.dismiss();
                }
            });
        }
        KotlinExtensionsKt.getExhaustive(Unit.a);
    }

    private final WidgetEditProfileBannerSheetBinding getBinding() {
        return (WidgetEditProfileBannerSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final EditProfileBannerSheetViewModel getViewModel() {
        return (EditProfileBannerSheetViewModel) this.viewModel$delegate.getValue();
    }

    private final boolean isGuildContext() {
        return ((Boolean) this.isGuildContext$delegate.getValue()).booleanValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchColorPicker(@ColorInt int i) {
        ColorPickerDialog.k kVar = new ColorPickerDialog.k();
        kVar.h = i;
        kVar.f3120s = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundPrimary);
        kVar.i = false;
        kVar.j = false;
        kVar.f = 0;
        kVar.a = R.string.user_settings_banner_color_title;
        kVar.r = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorHeaderPrimary);
        FontUtils fontUtils = FontUtils.INSTANCE;
        kVar.f3121x = fontUtils.getThemedFontResId(getContext(), R.attr.font_display_bold);
        kVar.l = true;
        kVar.e = R.string.color_picker_use_default;
        kVar.p = ColorCompat.getThemedColor(getContext(), (int) R.attr.color_brand);
        kVar.d = R.string.select;
        kVar.w = ColorCompat.getColor(getContext(), (int) R.color.white);
        kVar.f3122y = fontUtils.getThemedFontResId(getContext(), R.attr.font_primary_semibold);
        kVar.t = ColorCompat.getThemedColor(getContext(), (int) R.attr.colorTextMuted);
        kVar.u = R.drawable.drawable_cpv_edit_text_background;
        kVar.f3123z = fontUtils.getThemedFontResId(getContext(), R.attr.font_primary_normal);
        ColorPickerDialog a = kVar.a();
        a.k = new f() { // from class: com.discord.widgets.settings.profile.WidgetEditProfileBannerSheet$launchColorPicker$1
            @Override // b.k.a.a.f
            public void onColorReset(int i2) {
                EditProfileBannerSheetViewModel viewModel;
                viewModel = WidgetEditProfileBannerSheet.this.getViewModel();
                viewModel.updateColorPreview(null);
                WidgetEditProfileBannerSheet.this.getOnColorSelected().invoke(null);
            }

            @Override // b.k.a.a.f
            public void onColorSelected(int i2, int i3) {
                EditProfileBannerSheetViewModel viewModel;
                viewModel = WidgetEditProfileBannerSheet.this.getViewModel();
                ColorCompat colorCompat = ColorCompat.INSTANCE;
                viewModel.updateColorPreview(colorCompat.getColorHexFromColorInt(i3));
                WidgetEditProfileBannerSheet.this.getOnColorSelected().invoke(Integer.valueOf(colorCompat.removeAlphaComponent(i3)));
            }

            @Override // b.k.a.a.f
            public void onDialogDismissed(int i2) {
            }
        };
        AppBottomSheet.hideKeyboard$default(this, null, 1, null);
        a.show(getParentFragmentManager(), DIALOG_TAG_COLOR_PICKER);
        this.colorPickerDialog = a;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void navigateToUpsellModal() {
        CharSequence c;
        CharSequence c2;
        d.b bVar = d.k;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        AnalyticsTracker.PremiumUpsellType premiumUpsellType = AnalyticsTracker.PremiumUpsellType.CustomProfileUpsellModal;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        c = b.c(resources, R.string.premium_profile_customization_upsell_header, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        String obj = c.toString();
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        c2 = b.c(resources2, R.string.premium_profile_customization_upsell_body, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        bVar.a(parentFragmentManager, premiumUpsellType, R.drawable.img_profile_banner_value_prop, obj, c2.toString(), Traits.Location.Page.USER_SETTINGS, "User Profile", Traits.Location.Obj.EDIT_PROFILE_BANNER);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_edit_profile_banner_sheet;
    }

    public final Function0<Unit> getOnChangeProfileBannerImage() {
        return this.onChangeProfileBannerImage;
    }

    public final Function1<Integer, Unit> getOnColorSelected() {
        return this.onColorSelected;
    }

    public final Function0<Unit> getOnRemoveProfileBannerImage() {
        return this.onRemoveProfileBannerImage;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
        ColorPickerDialog colorPickerDialog = this.colorPickerDialog;
        if (colorPickerDialog != null) {
            colorPickerDialog.dismiss();
        }
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetEditProfileBannerSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEditProfileBannerSheet$onResume$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        CharSequence charSequence;
        Resources resources;
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditProfileBannerSheet$onViewCreated$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetEditProfileBannerSheet.this.navigateToUpsellModal();
            }
        });
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditProfileBannerSheet$onViewCreated$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetEditProfileBannerSheet.this.getOnRemoveProfileBannerImage().invoke();
                WidgetEditProfileBannerSheet.this.dismiss();
            }
        });
        boolean z2 = getArgumentsOrDefault().getBoolean(ARG_HAS_BANNER_IMAGE);
        MaterialTextView materialTextView = getBinding().c;
        m.checkNotNullExpressionValue(materialTextView, "binding.bannerChangeImageItemTitle");
        int i = 0;
        if (z2) {
            Resources resources2 = getResources();
            m.checkNotNullExpressionValue(resources2, "resources");
            charSequence = b.c(resources2, R.string.user_settings_change_profile_banner, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        } else {
            resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            charSequence = b.c(resources, R.string.user_settings_upload_banner, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
        }
        materialTextView.setText(charSequence);
        MaterialTextView materialTextView2 = getBinding().g;
        m.checkNotNullExpressionValue(materialTextView2, "binding.bannerRemoveImageItem");
        if (!z2) {
            i = 8;
        }
        materialTextView2.setVisibility(i);
    }

    public final void setOnChangeProfileBannerImage(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onChangeProfileBannerImage = function0;
    }

    public final void setOnColorSelected(Function1<? super Integer, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onColorSelected = function1;
    }

    public final void setOnRemoveProfileBannerImage(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onRemoveProfileBannerImage = function0;
    }
}
