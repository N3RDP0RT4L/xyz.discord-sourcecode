package com.discord.widgets.settings.profile;

import com.discord.api.guildmember.GuildMember;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreGuilds;
import com.discord.widgets.settings.profile.SettingsUserProfileViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guildmember/GuildMember;", "apiMember", "", "invoke", "(Lcom/discord/api/guildmember/GuildMember;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsUserProfileViewModel$saveGuildMemberChanges$2 extends o implements Function1<GuildMember, Unit> {
    public final /* synthetic */ SettingsUserProfileViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ SettingsUserProfileViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsUserProfileViewModel$saveGuildMemberChanges$2(SettingsUserProfileViewModel settingsUserProfileViewModel, SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = settingsUserProfileViewModel;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GuildMember guildMember) {
        invoke2(guildMember);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GuildMember guildMember) {
        StoreGuilds storeGuilds;
        StoreGuilds storeGuilds2;
        com.discord.models.member.GuildMember from;
        PublishSubject publishSubject;
        m.checkNotNullParameter(guildMember, "apiMember");
        storeGuilds = this.this$0.storeGuilds;
        storeGuilds.handleGuildMember(GuildMember.a(guildMember, this.this$0.guildId.longValue(), null, null, null, null, null, false, null, null, null, null, null, null, 8190), this.this$0.guildId.longValue(), true);
        GuildMember.Companion companion = com.discord.models.member.GuildMember.Companion;
        long longValue = this.this$0.guildId.longValue();
        storeGuilds2 = this.this$0.storeGuilds;
        from = companion.from(guildMember, longValue, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : storeGuilds2);
        this.this$0.updateViewState(SettingsUserProfileViewModel.ViewState.Loaded.copy$default(this.$viewState, null, null, from, null, null, null, null, null, null, null, false, false, 3979, null));
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(SettingsUserProfileViewModel.Event.UserUpdateRequestCompleted.INSTANCE);
    }
}
