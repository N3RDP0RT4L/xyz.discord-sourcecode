package com.discord.widgets.settings.profile;

import android.graphics.Color;
import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.settings.profile.EditProfileBannerSheetViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetEditProfileBannerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/profile/EditProfileBannerSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEditProfileBannerSheet$viewModel$2 extends o implements Function0<AppViewModel<EditProfileBannerSheetViewModel.ViewState>> {
    public final /* synthetic */ WidgetEditProfileBannerSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEditProfileBannerSheet$viewModel$2(WidgetEditProfileBannerSheet widgetEditProfileBannerSheet) {
        super(0);
        this.this$0 = widgetEditProfileBannerSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<EditProfileBannerSheetViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        String string = argumentsOrDefault.getString("ARG_DEFAULT_BANNER_COLOR_HEX");
        m.checkNotNull(string);
        m.checkNotNullExpressionValue(string, "argumentsOrDefault.getSt…FAULT_BANNER_COLOR_HEX)!!");
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        String string2 = argumentsOrDefault2.getString("ARG_INITIAL_COLOR_PREVIEW_HEX");
        this.this$0.defaultBannerColor = Color.parseColor(string);
        return new EditProfileBannerSheetViewModel(string, string2, null, 4, null);
    }
}
