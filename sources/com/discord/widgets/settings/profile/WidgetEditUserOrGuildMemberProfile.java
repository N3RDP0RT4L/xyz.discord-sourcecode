package com.discord.widgets.settings.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Editable;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.a.y.d0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.AppTransitionActivity;
import com.discord.databinding.ViewDialogConfirmationBinding;
import com.discord.databinding.WidgetSettingsUserProfileBinding;
import com.discord.simpleast.core.node.Node;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.file.FileUtilsKt;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.AstRenderer;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.profile.SettingsUserProfileViewModel;
import com.discord.widgets.user.profile.UserProfileHeaderView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.g0.t;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 B2\u00020\u0001:\u0001BB\u0007¢\u0006\u0004\bA\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0013\u0010\rJ\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0014\u0010\rJ\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0015\u0010\rJ\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0016\u0010\rJ\u001f\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u001f\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\u0016¢\u0006\u0004\b\u001d\u0010\u001cJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\u0016¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\"\u0010\u000fR\"\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00040#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u001d\u0010+\u001a\u00020&8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\"\u0010,\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00040#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010%R\"\u0010-\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00040#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010%R:\u00100\u001a&\u0012\f\u0012\n /*\u0004\u0018\u00010\u00190\u0019 /*\u0012\u0012\f\u0012\n /*\u0004\u0018\u00010\u00190\u0019\u0018\u00010.0.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u001d\u00107\u001a\u0002028B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b3\u00104\u001a\u0004\b5\u00106R%\u0010=\u001a\n\u0018\u000108j\u0004\u0018\u0001`98B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b:\u0010(\u001a\u0004\b;\u0010<R\u0016\u0010?\u001a\u00020>8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@¨\u0006C"}, d2 = {"Lcom/discord/widgets/settings/profile/WidgetEditUserOrGuildMemberProfile;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState;)V", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Event;)V", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState$Loaded;", "configureBio", "(Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState$Loaded;)V", "setCurrentBioFromEditor", "()V", "", "handleBackPressed", "(Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState$Loaded;)Z", "configureNick", "configureAvatarSelect", "configureBannerSelect", "configureFab", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "mimeType", "onImageChosen", "(Landroid/net/Uri;Ljava/lang/String;)V", "onImageCropped", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lkotlin/Function1;", "bannerSelectedResult", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel;", "viewModel", "avatarSelectedResult", "imageSelectedResult", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "avatarRepresentativeColorHexSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/databinding/WidgetSettingsUserProfileBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetSettingsUserProfileBinding;", "binding", "", "Lcom/discord/primitives/GuildId;", "guildId$delegate", "getGuildId", "()Ljava/lang/Long;", "guildId", "Ljava/util/concurrent/atomic/AtomicBoolean;", "discardConfirmed", "Ljava/util/concurrent/atomic/AtomicBoolean;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEditUserOrGuildMemberProfile extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetEditUserOrGuildMemberProfile.class, "binding", "getBinding()Lcom/discord/databinding/WidgetSettingsUserProfileBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final int MAX_AVATAR_SIZE = 1024;
    public static final int MAX_BANNER_FILE_SIZE_MB = 30;
    public static final int MAX_BANNER_IMAGE_SIZE = 1080;
    private final Lazy viewModel$delegate;
    private Function1<? super String, Unit> avatarSelectedResult = WidgetEditUserOrGuildMemberProfile$avatarSelectedResult$1.INSTANCE;
    private Function1<? super String, Unit> bannerSelectedResult = WidgetEditUserOrGuildMemberProfile$bannerSelectedResult$1.INSTANCE;
    private Function1<? super String, Unit> imageSelectedResult = WidgetEditUserOrGuildMemberProfile$imageSelectedResult$1.INSTANCE;
    private final Lazy guildId$delegate = g.lazy(new WidgetEditUserOrGuildMemberProfile$guildId$2(this));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetEditUserOrGuildMemberProfile$binding$2.INSTANCE, null, 2, null);
    private final AtomicBoolean discardConfirmed = new AtomicBoolean(false);
    private final BehaviorSubject<String> avatarRepresentativeColorHexSubject = BehaviorSubject.k0();

    /* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012J3\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0010\b\u0002\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000e¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/settings/profile/WidgetEditUserOrGuildMemberProfile$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/app/AppTransitionActivity$Transition;", "transition", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "launch", "(Landroid/content/Context;Lcom/discord/app/AppTransitionActivity$Transition;Ljava/lang/Long;)V", "", "MAX_AVATAR_SIZE", "I", "MAX_BANNER_FILE_SIZE_MB", "MAX_BANNER_IMAGE_SIZE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, AppTransitionActivity.Transition transition, Long l, int i, Object obj) {
            if ((i & 2) != 0) {
                transition = null;
            }
            if ((i & 4) != 0) {
                l = null;
            }
            companion.launch(context, transition, l);
        }

        public final void launch(Context context, AppTransitionActivity.Transition transition, Long l) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            if (transition != null) {
                intent.putExtra("transition", transition);
            }
            if (l != null) {
                intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", l.longValue());
            }
            j.d(context, WidgetEditUserOrGuildMemberProfile.class, intent);
            StoreAnalytics.onUserSettingsPaneViewed$default(StoreStream.Companion.getAnalytics(), "User Profile", null, 2, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetEditUserOrGuildMemberProfile() {
        super(R.layout.widget_settings_user_profile);
        WidgetEditUserOrGuildMemberProfile$viewModel$2 widgetEditUserOrGuildMemberProfile$viewModel$2 = new WidgetEditUserOrGuildMemberProfile$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(SettingsUserProfileViewModel.class), new WidgetEditUserOrGuildMemberProfile$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetEditUserOrGuildMemberProfile$viewModel$2));
    }

    private final void configureAvatarSelect(SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        String str;
        String str2;
        this.avatarSelectedResult = new WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$1(this, loaded);
        d0[] d0VarArr = new d0[1];
        if (loaded.getMeMember() != null) {
            if (loaded.getHasAvatarForDisplay()) {
                str = getString(R.string.change_guild_member_avatar);
            } else {
                str = getString(R.string.upload_guild_member_avatar);
            }
        } else if (loaded.getHasAvatarForDisplay()) {
            str = getString(R.string.user_settings_change_avatar);
        } else {
            str = getString(R.string.user_settings_upload_avatar);
        }
        d0VarArr[0] = new d0(str, null, null, null, null, null, null, 116);
        List mutableListOf = n.mutableListOf(d0VarArr);
        if (loaded.getHasAvatarForDisplay()) {
            if (loaded.getMeMember() != null) {
                str2 = getString(R.string.change_identity_modal_reset_primary_avatar);
            } else {
                str2 = getString(R.string.user_settings_remove_avatar);
            }
            mutableListOf.add(new d0(str2, null, null, null, null, Integer.valueOf(ColorCompat.getColor(requireContext(), (int) R.color.status_red_500)), null, 84));
        }
        getBinding().o.setOnAvatarEdit(new WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$2(this, mutableListOf));
    }

    private final void configureBannerSelect(SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        this.bannerSelectedResult = new WidgetEditUserOrGuildMemberProfile$configureBannerSelect$1(this);
        getBinding().o.setOnBannerPress(new WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2(this, loaded));
    }

    private final void configureBio(SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        Drawable drawable;
        List<Node<MessageRenderContext>> bioAst = loaded.getBioAst();
        boolean showBioEditor = loaded.getShowBioEditor();
        CardView cardView = getBinding().f2614b;
        m.checkNotNullExpressionValue(cardView, "binding.bioEditorCard");
        boolean z2 = true;
        int i = 0;
        boolean z3 = cardView.getVisibility() == 0;
        TextView textView = getBinding().f;
        m.checkNotNullExpressionValue(textView, "binding.bioHelpText");
        if (loaded.getMeMember() == null) {
            z2 = false;
        }
        textView.setVisibility(z2 ? 0 : 8);
        TextView textView2 = getBinding().e;
        Drawable drawable2 = null;
        textView2.setText(loaded.getMeMember() != null ? b.d(textView2, R.string.change_identity_bio_header, new Object[0], (r4 & 4) != 0 ? b.c.j : null) : b.d(textView2, R.string.user_profile_about_me, new Object[0], (r4 & 4) != 0 ? b.c.j : null));
        if (!(loaded.getMeMember() == null || (drawable = ContextCompat.getDrawable(textView2.getContext(), R.drawable.ic_nitro_wheel_16dp)) == null)) {
            TextView textView3 = getBinding().e;
            m.checkNotNullExpressionValue(textView3, "binding.bioHeader");
            DrawableCompat.setTint(drawable, ColorCompat.getThemedColor(textView3, (int) R.attr.colorHeaderSecondary));
            drawable2 = drawable;
        }
        com.discord.utilities.drawable.DrawableCompat.setCompoundDrawablesCompat$default(textView2, (Drawable) null, (Drawable) null, drawable2, (Drawable) null, 11, (Object) null);
        CardView cardView2 = getBinding().g;
        m.checkNotNullExpressionValue(cardView2, "binding.bioPreviewCard");
        cardView2.setVisibility(showBioEditor ^ true ? 0 : 8);
        CardView cardView3 = getBinding().f2614b;
        m.checkNotNullExpressionValue(cardView3, "binding.bioEditorCard");
        if (!showBioEditor) {
            i = 8;
        }
        cardView3.setVisibility(i);
        if (showBioEditor) {
            TextInputLayout textInputLayout = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout, "binding.bioEditorTextInputFieldWrap");
            ViewExtensions.setText(textInputLayout, loaded.getCurrentBio());
            if (!z3) {
                getBinding().d.requestFocus();
                TextInputLayout textInputLayout2 = getBinding().d;
                m.checkNotNullExpressionValue(textInputLayout2, "binding.bioEditorTextInputFieldWrap");
                ViewExtensions.moveCursorToEnd(textInputLayout2);
                TextInputLayout textInputLayout3 = getBinding().d;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.bioEditorTextInputFieldWrap");
                showKeyboard(textInputLayout3);
            }
        } else if (bioAst != null) {
            LinkifiedTextView linkifiedTextView = getBinding().h;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.bioPreviewText");
            Context context = linkifiedTextView.getContext();
            m.checkNotNullExpressionValue(context, "binding.bioPreviewText.context");
            getBinding().h.setDraweeSpanStringBuilder(AstRenderer.render(bioAst, new MessageRenderContext(context, 0L, false, null, null, null, 0, null, null, 0, 0, new WidgetEditUserOrGuildMemberProfile$configureBio$renderContext$1(getViewModel()), null, null, 14328, null)));
        }
    }

    private final void configureFab(SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        FloatingActionButton floatingActionButton = getBinding().m;
        m.checkNotNullExpressionValue(floatingActionButton, "binding.saveFab");
        floatingActionButton.setVisibility(loaded.getShowSaveFab() ? 0 : 8);
    }

    private final void configureNick(final SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        String str;
        boolean z2 = false;
        boolean z3 = loaded.getGuild() != null;
        LinearLayout linearLayout = getBinding().l;
        m.checkNotNullExpressionValue(linearLayout, "binding.nickContainer");
        linearLayout.setVisibility(z3 ? 0 : 8);
        TextInputLayout textInputLayout = getBinding().n;
        m.checkNotNullExpressionValue(textInputLayout, "textInputLayout");
        String currentNickname = loaded.getCurrentNickname();
        textInputLayout.setEndIconVisible(!(currentNickname == null || currentNickname.length() == 0));
        Editable editable = null;
        ViewExtensions.setEnabledAndAlpha$default(textInputLayout, loaded.getCanEditNickname(), 0.0f, 2, null);
        if (loaded.getCanEditNickname()) {
            str = getString(R.string.nickname);
        } else {
            str = getString(R.string.change_identity_modal_change_nickname_disabled);
        }
        textInputLayout.setHint(str);
        textInputLayout.setPlaceholderText(loaded.getUser().getUsername());
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetEditUserOrGuildMemberProfile$configureNick$$inlined$also$lambda$1(this, loaded));
        textInputLayout.setEndIconOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$configureNick$$inlined$also$lambda$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SettingsUserProfileViewModel viewModel;
                viewModel = WidgetEditUserOrGuildMemberProfile.this.getViewModel();
                viewModel.updateNickname("");
            }
        });
        TextInputLayout textInputLayout2 = getBinding().n;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.setNicknameText");
        String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout2);
        if (!m.areEqual(loaded.getCurrentNickname(), textOrEmpty)) {
            ViewExtensions.setText(textInputLayout, loaded.getCurrentNickname());
            if (textOrEmpty.length() == 0) {
                z2 = true;
            }
            if (z2) {
                EditText editText = textInputLayout.getEditText();
                if (editText != null) {
                    editable = editText.getText();
                }
                Selection.setSelection(editable, ViewExtensions.getTextOrEmpty(textInputLayout).length());
            }
        }
    }

    public final void configureUI(final SettingsUserProfileViewModel.ViewState viewState) {
        CharSequence e;
        if (viewState instanceof SettingsUserProfileViewModel.ViewState.Loaded) {
            SettingsUserProfileViewModel.ViewState.Loaded loaded = (SettingsUserProfileViewModel.ViewState.Loaded) viewState;
            if (loaded.getGuild() != null) {
                setActionBarSubtitle(loaded.getGuild().getName());
            }
            configureNick(loaded);
            getBinding().k.setVisibility(loaded.getGuild() != null ? 0 : 8);
            if (loaded.getGuild() != null) {
                TextView textView = getBinding().j;
                m.checkNotNullExpressionValue(textView, "binding.guildMemberProfileHelpTextOverall");
                e = b.e(this, R.string.change_identity_help_text_overall, new Object[]{loaded.getGuild().getName()}, (r4 & 4) != 0 ? b.a.j : null);
                textView.setText(e);
            }
            configureBio(loaded);
            configureAvatarSelect(loaded);
            configureBannerSelect(loaded);
            configureFab(loaded);
            AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$configureUI$3
                @Override // rx.functions.Func0, java.util.concurrent.Callable
                public final Boolean call() {
                    boolean handleBackPressed;
                    handleBackPressed = WidgetEditUserOrGuildMemberProfile.this.handleBackPressed((SettingsUserProfileViewModel.ViewState.Loaded) viewState);
                    return Boolean.valueOf(handleBackPressed);
                }
            }, 0, 2, null);
        }
    }

    public final WidgetSettingsUserProfileBinding getBinding() {
        return (WidgetSettingsUserProfileBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final Long getGuildId() {
        return (Long) this.guildId$delegate.getValue();
    }

    public final SettingsUserProfileViewModel getViewModel() {
        return (SettingsUserProfileViewModel) this.viewModel$delegate.getValue();
    }

    public final boolean handleBackPressed(SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        if (loaded.isEditingBio()) {
            setCurrentBioFromEditor();
            return true;
        } else if (!loaded.isDirty() || this.discardConfirmed.get()) {
            return false;
        } else {
            ViewDialogConfirmationBinding b2 = ViewDialogConfirmationBinding.b(LayoutInflater.from(e()));
            m.checkNotNullExpressionValue(b2, "ViewDialogConfirmationBi…tInflater.from(activity))");
            final AlertDialog create = new AlertDialog.Builder(requireContext()).setView(b2.a).create();
            m.checkNotNullExpressionValue(create, "AlertDialog.Builder(requ…logBinding.root).create()");
            b2.d.setText(R.string.discard_changes);
            b2.e.setText(R.string.discard_changes_description);
            b2.f2167b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$handleBackPressed$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AlertDialog.this.dismiss();
                }
            });
            b2.c.setText(R.string.okay);
            b2.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$handleBackPressed$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AtomicBoolean atomicBoolean;
                    atomicBoolean = WidgetEditUserOrGuildMemberProfile.this.discardConfirmed;
                    atomicBoolean.set(true);
                    create.dismiss();
                    FragmentActivity activity = WidgetEditUserOrGuildMemberProfile.this.e();
                    if (activity != null) {
                        activity.onBackPressed();
                    }
                }
            });
            create.show();
            return true;
        }
    }

    public final void handleEvent(SettingsUserProfileViewModel.Event event) {
        if (m.areEqual(event, SettingsUserProfileViewModel.Event.UserUpdateRequestCompleted.INSTANCE)) {
            DimmerView.setDimmed$default(getBinding().i, false, false, 2, null);
        }
    }

    public final void setCurrentBioFromEditor() {
        TextInputEditText textInputEditText = getBinding().c;
        m.checkNotNullExpressionValue(textInputEditText, "binding.bioEditorTextInputField");
        getViewModel().updateBio(String.valueOf(textInputEditText.getText()));
        getViewModel().updateIsEditingBio(false);
        getBinding().d.clearFocus();
        hideKeyboard(getBinding().d);
    }

    @Override // com.discord.app.AppFragment
    public void onImageChosen(Uri uri, String str) {
        CharSequence e;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageChosen(uri, str);
        if (!t.startsWith$default(str, "image", false, 2, null)) {
            b.a.d.m.g(getContext(), R.string.user_settings_image_upload_filetype_error, 0, null, 12);
        } else if (m.areEqual(str, "image/gif")) {
            Long fileSizeBytes = FileUtilsKt.getFileSizeBytes(requireContext(), uri);
            if (fileSizeBytes == null || fileSizeBytes.longValue() < 31457280) {
                Context context = getContext();
                Function1<? super String, Unit> function1 = this.imageSelectedResult;
                Object obj = function1;
                if (function1 != null) {
                    obj = new WidgetEditUserOrGuildMemberProfile$sam$rx_functions_Action1$0(function1);
                }
                MGImages.requestDataUrl(context, uri, str, (Action1) obj);
                return;
            }
            Context requireContext = requireContext();
            e = b.e(this, R.string.user_settings_image_upload_file_too_large, new Object[]{30}, (r4 & 4) != 0 ? b.a.j : null);
            b.a.d.m.h(requireContext, e, 0, null, 12);
        } else {
            MGImages.requestImageCrop(requireContext(), this, uri, m.areEqual(this.imageSelectedResult, this.bannerSelectedResult) ? 5.0f : 1.0f, m.areEqual(this.imageSelectedResult, this.bannerSelectedResult) ? 2.0f : 1.0f, m.areEqual(this.imageSelectedResult, this.bannerSelectedResult) ? MAX_BANNER_IMAGE_SIZE : 1024);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onImageCropped(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageCropped(uri, str);
        Context context = getContext();
        Function1<? super String, Unit> function1 = this.imageSelectedResult;
        Object obj = function1;
        if (function1 != null) {
            obj = new WidgetEditUserOrGuildMemberProfile$sam$rx_functions_Action1$0(function1);
        }
        MGImages.requestDataUrl(context, uri, str, (Action1) obj);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(final View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        if (getGuildId() != null) {
            setActionBarTitle(R.string.change_identity);
        } else {
            setActionBarTitle(R.string.user_settings_user_profile);
            setActionBarSubtitle(R.string.user_settings);
        }
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        getBinding().m.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SettingsUserProfileViewModel viewModel;
                WidgetSettingsUserProfileBinding binding;
                viewModel = WidgetEditUserOrGuildMemberProfile.this.getViewModel();
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "view.context");
                viewModel.saveChanges(context);
                binding = WidgetEditUserOrGuildMemberProfile.this.getBinding();
                DimmerView.setDimmed$default(binding.i, true, false, 2, null);
            }
        });
        ((TouchInterceptingCoordinatorLayout) view).setOnInterceptTouchEvent(new WidgetEditUserOrGuildMemberProfile$onViewBound$2(this));
        getBinding().c.setRawInputType(1);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SettingsUserProfileViewModel viewModel;
                viewModel = WidgetEditUserOrGuildMemberProfile.this.getViewModel();
                viewModel.updateIsEditingBio(true);
            }
        });
        getBinding().o.setOnAvatarRepresentativeColorUpdated(new WidgetEditUserOrGuildMemberProfile$onViewBound$4(this));
        getBinding().o.setOnBadgeClick(new WidgetEditUserOrGuildMemberProfile$onViewBound$5(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<SettingsUserProfileViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel.observeViewSta…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetEditUserOrGuildMemberProfile.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEditUserOrGuildMemberProfile$onViewBoundOrOnResume$1(this));
        UserProfileHeaderView.Companion companion = UserProfileHeaderView.Companion;
        UserProfileHeaderView userProfileHeaderView = getBinding().o;
        m.checkNotNullExpressionValue(userProfileHeaderView, "binding.userSettingsProfileHeaderView");
        companion.bind(userProfileHeaderView, this, getViewModel().observeHeaderViewState());
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetEditUserOrGuildMemberProfile.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEditUserOrGuildMemberProfile$onViewBoundOrOnResume$2(this));
    }
}
