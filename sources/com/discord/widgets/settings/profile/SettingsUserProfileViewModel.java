package com.discord.widgets.settings.profile;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.guildmember.PatchGuildMemberBody;
import com.discord.api.user.PatchUserBody;
import com.discord.api.user.UserProfile;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.nullserializable.NullSerializable;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.Parser;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserProfile;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.DiscordParser;
import com.discord.utilities.textprocessing.MessageParseState;
import com.discord.utilities.textprocessing.MessagePreprocessor;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.widgets.settings.profile.SettingsUserProfileViewModel;
import com.discord.widgets.user.profile.UserProfileHeaderViewModel;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
import rx.subjects.PublishSubject;
/* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¶\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010#\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 P2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004PQRSBu\u0012\u000e\u0010;\u001a\n\u0018\u000109j\u0004\u0018\u0001`:\u0012\b\b\u0002\u0010L\u001a\u00020K\u0012\b\b\u0002\u0010>\u001a\u00020=\u0012\b\b\u0002\u0010I\u001a\u00020H\u0012\b\b\u0002\u0010A\u001a\u00020@\u0012$\b\u0002\u0010F\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f\u0012\u0004\u0012\u00020D0Cj\u0002`E\u0012\u000e\b\u0002\u0010M\u001a\b\u0012\u0004\u0012\u00020\u00140\u0019¢\u0006\u0004\bN\u0010OJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\b\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\b\u0010\u0007J'\u0010\u000f\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\u000bj\u0002`\u000e2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0014H\u0003¢\u0006\u0004\b\u0016\u0010\u0017J#\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\u000b2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0018\u0010\u0010J\u0013\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0019\u0010\u001e\u001a\u00020\u00052\b\u0010\u001d\u001a\u0004\u0018\u00010\tH\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u0019\u0010 \u001a\u00020\u00052\b\u0010\u001d\u001a\u0004\u0018\u00010\tH\u0007¢\u0006\u0004\b \u0010\u001fJ\u0019\u0010\"\u001a\u00020\u00052\b\u0010!\u001a\u0004\u0018\u00010\tH\u0007¢\u0006\u0004\b\"\u0010\u001fJ\u0017\u0010$\u001a\u00020\u00052\u0006\u0010#\u001a\u00020\tH\u0007¢\u0006\u0004\b$\u0010\u001fJ\u0017\u0010%\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\tH\u0007¢\u0006\u0004\b%\u0010\u001fJ\u0017\u0010(\u001a\u00020\u00052\u0006\u0010'\u001a\u00020&H\u0007¢\u0006\u0004\b(\u0010)J\u0013\u0010+\u001a\b\u0012\u0004\u0012\u00020*0\u0019¢\u0006\u0004\b+\u0010\u001cJ\u001b\u0010.\u001a\u00020\u00052\n\u0010-\u001a\u0006\u0012\u0002\b\u00030,H\u0007¢\u0006\u0004\b.\u0010/J\u0017\u00100\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0007¢\u0006\u0004\b0\u0010\u0007R\u001c\u00103\u001a\b\u0012\u0004\u0012\u000202018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R:\u00107\u001a&\u0012\f\u0012\n 6*\u0004\u0018\u00010\u001a0\u001a 6*\u0012\u0012\f\u0012\n 6*\u0004\u0018\u00010\u001a0\u001a\u0018\u000105058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108R\u001e\u0010;\u001a\n\u0018\u000109j\u0004\u0018\u0001`:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR2\u0010F\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f\u0012\u0004\u0012\u00020D0Cj\u0002`E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010J¨\u0006T"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState;", "Landroid/content/Context;", "context", "", "saveGuildMemberChanges", "(Landroid/content/Context;)V", "saveUserChanges", "", "bio", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/settings/profile/AST;", "parseBio", "(Ljava/lang/String;)Ljava/util/List;", "Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "createMessagePreprocessor", "()Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$StoreState;)V", "createAndProcessBioAstFromText", "Lrx/Observable;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Event;", "observeEvents", "()Lrx/Observable;", "dataUrl", "updateAvatar", "(Ljava/lang/String;)V", "updateBannerImage", "colorHex", "updateBannerColor", ModelAuditLogEntry.CHANGE_KEY_NICK, "updateNickname", "updateBio", "", "isEditing", "updateIsEditingBio", "(Z)V", "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;", "observeHeaderViewState", "Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "spoilerNode", "handleBioIndexClicked", "(Lcom/discord/utilities/textprocessing/node/SpoilerNode;)V", "saveChanges", "", "", "revealedBioIndices", "Ljava/util/Set;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/Long;", "Lcom/discord/stores/StoreUserProfile;", "storeUserProfile", "Lcom/discord/stores/StoreUserProfile;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/utilities/textprocessing/MessageParseState;", "Lcom/discord/widgets/settings/profile/BioParser;", "bioParser", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreUser;", "storeUser", "storeStateObservable", HookHelper.constructorName, "(Ljava/lang/Long;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreGuilds;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/simpleast/core/parser/Parser;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsUserProfileViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> bioParser;
    private final PublishSubject<Event> eventSubject;
    private final Long guildId;
    private final RestAPI restAPI;
    private final Set<Integer> revealedBioIndices;
    private final StoreGuilds storeGuilds;
    private final StoreUserProfile storeUserProfile;

    /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "meUser", "", "invoke", "(Lcom/discord/models/user/MeUser;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.profile.SettingsUserProfileViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<MeUser, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(MeUser meUser) {
            invoke2(meUser);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(MeUser meUser) {
            SettingsUserProfileViewModel.this.storeUserProfile.fetchProfile(meUser.getId(), (r13 & 2) != 0 ? null : SettingsUserProfileViewModel.this.guildId, (r13 & 4) != 0 ? false : false, (r13 & 8) != 0 ? null : null);
        }
    }

    /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.profile.SettingsUserProfileViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            SettingsUserProfileViewModel settingsUserProfileViewModel = SettingsUserProfileViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            settingsUserProfileViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010JC\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreUserProfile;", "storeUserProfile", "Lrx/Observable;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$StoreState;", "observeStoreState", "(Ljava/lang/Long;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserProfile;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStoreState(final Long l, StoreUser storeUser, final StoreGuilds storeGuilds, final StoreUserProfile storeUserProfile) {
            Observable<StoreState> Y = Observable.j(StoreUser.observeMe$default(storeUser, false, 1, null), storeGuilds.observeGuild(l != null ? l.longValue() : -1L), SettingsUserProfileViewModel$Companion$observeStoreState$1.INSTANCE).Y(new b<Pair<? extends MeUser, ? extends Guild>, Observable<? extends StoreState>>() { // from class: com.discord.widgets.settings.profile.SettingsUserProfileViewModel$Companion$observeStoreState$2
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends SettingsUserProfileViewModel.StoreState> call(Pair<? extends MeUser, ? extends Guild> pair) {
                    return call2((Pair<MeUser, Guild>) pair);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends SettingsUserProfileViewModel.StoreState> call2(Pair<MeUser, Guild> pair) {
                    final MeUser component1 = pair.component1();
                    final Guild component2 = pair.component2();
                    Observable<UserProfile> observeUserProfile = StoreUserProfile.this.observeUserProfile(component1.getId());
                    StoreGuilds storeGuilds2 = storeGuilds;
                    Long l2 = l;
                    long j = -1;
                    Observable<GuildMember> observeGuildMember = storeGuilds2.observeGuildMember(l2 != null ? l2.longValue() : -1L, component1.getId());
                    GuildChannelsInfo.Companion companion = GuildChannelsInfo.Companion;
                    Long l3 = l;
                    if (l3 != null) {
                        j = l3.longValue();
                    }
                    return Observable.i(observeUserProfile, observeGuildMember, companion.get(j), new Func3<UserProfile, GuildMember, GuildChannelsInfo, SettingsUserProfileViewModel.StoreState>() { // from class: com.discord.widgets.settings.profile.SettingsUserProfileViewModel$Companion$observeStoreState$2.1
                        public final SettingsUserProfileViewModel.StoreState call(UserProfile userProfile, GuildMember guildMember, GuildChannelsInfo guildChannelsInfo) {
                            MeUser.Companion companion2 = MeUser.Companion;
                            MeUser meUser = MeUser.this;
                            m.checkNotNullExpressionValue(meUser, "meUser");
                            MeUser merge = companion2.merge(meUser, userProfile.g());
                            Guild guild = component2;
                            m.checkNotNullExpressionValue(userProfile, "userProfile");
                            m.checkNotNullExpressionValue(guildChannelsInfo, "guildChannelsInfo");
                            return new SettingsUserProfileViewModel.StoreState(merge, guild, userProfile, guildMember, guildChannelsInfo);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "Observable.combineLatest…      )\n        }\n      }");
            return Y;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, Long l, StoreUser storeUser, StoreGuilds storeGuilds, StoreUserProfile storeUserProfile, int i, Object obj) {
            if ((i & 2) != 0) {
                storeUser = StoreStream.Companion.getUsers();
            }
            if ((i & 4) != 0) {
                storeGuilds = StoreStream.Companion.getGuilds();
            }
            if ((i & 8) != 0) {
                storeUserProfile = StoreStream.Companion.getUserProfile();
            }
            return companion.observeStoreState(l, storeUser, storeGuilds, storeUserProfile);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Event;", "", HookHelper.constructorName, "()V", "UserUpdateRequestCompleted", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Event$UserUpdateRequestCompleted;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Event$UserUpdateRequestCompleted;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UserUpdateRequestCompleted extends Event {
            public static final UserUpdateRequestCompleted INSTANCE = new UserUpdateRequestCompleted();

            private UserUpdateRequestCompleted() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0013\u001a\u00020\b\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u0015\u001a\u00020\u000e¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JF\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0013\u001a\u00020\b2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\u0015\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010 \u001a\u00020\u001f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0013\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b#\u0010\nR\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b%\u0010\u0004R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b'\u0010\rR\u0019\u0010\u0015\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010(\u001a\u0004\b)\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010*\u001a\u0004\b+\u0010\u0007¨\u0006."}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$StoreState;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/user/UserProfile;", "component3", "()Lcom/discord/api/user/UserProfile;", "Lcom/discord/models/member/GuildMember;", "component4", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "component5", "()Lcom/discord/utilities/channel/GuildChannelsInfo;", "user", "guild", "userProfile", "meMember", "guildChannelsInfo", "copy", "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/api/user/UserProfile;Lcom/discord/models/member/GuildMember;Lcom/discord/utilities/channel/GuildChannelsInfo;)Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/user/UserProfile;", "getUserProfile", "Lcom/discord/models/user/MeUser;", "getUser", "Lcom/discord/models/member/GuildMember;", "getMeMember", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "getGuildChannelsInfo", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/api/user/UserProfile;Lcom/discord/models/member/GuildMember;Lcom/discord/utilities/channel/GuildChannelsInfo;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Guild guild;
        private final GuildChannelsInfo guildChannelsInfo;
        private final GuildMember meMember;
        private final MeUser user;
        private final UserProfile userProfile;

        public StoreState(MeUser meUser, Guild guild, UserProfile userProfile, GuildMember guildMember, GuildChannelsInfo guildChannelsInfo) {
            m.checkNotNullParameter(meUser, "user");
            m.checkNotNullParameter(userProfile, "userProfile");
            m.checkNotNullParameter(guildChannelsInfo, "guildChannelsInfo");
            this.user = meUser;
            this.guild = guild;
            this.userProfile = userProfile;
            this.meMember = guildMember;
            this.guildChannelsInfo = guildChannelsInfo;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, MeUser meUser, Guild guild, UserProfile userProfile, GuildMember guildMember, GuildChannelsInfo guildChannelsInfo, int i, Object obj) {
            if ((i & 1) != 0) {
                meUser = storeState.user;
            }
            if ((i & 2) != 0) {
                guild = storeState.guild;
            }
            Guild guild2 = guild;
            if ((i & 4) != 0) {
                userProfile = storeState.userProfile;
            }
            UserProfile userProfile2 = userProfile;
            if ((i & 8) != 0) {
                guildMember = storeState.meMember;
            }
            GuildMember guildMember2 = guildMember;
            if ((i & 16) != 0) {
                guildChannelsInfo = storeState.guildChannelsInfo;
            }
            return storeState.copy(meUser, guild2, userProfile2, guildMember2, guildChannelsInfo);
        }

        public final MeUser component1() {
            return this.user;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final UserProfile component3() {
            return this.userProfile;
        }

        public final GuildMember component4() {
            return this.meMember;
        }

        public final GuildChannelsInfo component5() {
            return this.guildChannelsInfo;
        }

        public final StoreState copy(MeUser meUser, Guild guild, UserProfile userProfile, GuildMember guildMember, GuildChannelsInfo guildChannelsInfo) {
            m.checkNotNullParameter(meUser, "user");
            m.checkNotNullParameter(userProfile, "userProfile");
            m.checkNotNullParameter(guildChannelsInfo, "guildChannelsInfo");
            return new StoreState(meUser, guild, userProfile, guildMember, guildChannelsInfo);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.user, storeState.user) && m.areEqual(this.guild, storeState.guild) && m.areEqual(this.userProfile, storeState.userProfile) && m.areEqual(this.meMember, storeState.meMember) && m.areEqual(this.guildChannelsInfo, storeState.guildChannelsInfo);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final GuildChannelsInfo getGuildChannelsInfo() {
            return this.guildChannelsInfo;
        }

        public final GuildMember getMeMember() {
            return this.meMember;
        }

        public final MeUser getUser() {
            return this.user;
        }

        public final UserProfile getUserProfile() {
            return this.userProfile;
        }

        public int hashCode() {
            MeUser meUser = this.user;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            Guild guild = this.guild;
            int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
            UserProfile userProfile = this.userProfile;
            int hashCode3 = (hashCode2 + (userProfile != null ? userProfile.hashCode() : 0)) * 31;
            GuildMember guildMember = this.meMember;
            int hashCode4 = (hashCode3 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
            GuildChannelsInfo guildChannelsInfo = this.guildChannelsInfo;
            if (guildChannelsInfo != null) {
                i = guildChannelsInfo.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(user=");
            R.append(this.user);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", userProfile=");
            R.append(this.userProfile);
            R.append(", meMember=");
            R.append(this.meMember);
            R.append(", guildChannelsInfo=");
            R.append(this.guildChannelsInfo);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b)\b\u0086\b\u0018\u00002\u00020\u0001B£\u0001\u0012\u0006\u0010!\u001a\u00020\u0002\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010#\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010$\u001a\u00020\u000b\u0012\u0010\b\u0002\u0010%\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e\u0012\u0010\b\u0002\u0010&\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e\u0012\u0010\b\u0002\u0010'\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e\u0012\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u000f\u0012\b\u0010)\u001a\u0004\u0018\u00010\u000f\u0012\u001a\u0010*\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u0018\u0018\u00010\u0017j\u0004\u0018\u0001`\u001a\u0012\u0006\u0010+\u001a\u00020\u001d\u0012\u0006\u0010,\u001a\u00020\u001d¢\u0006\u0004\bZ\u0010[J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0018\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0018\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\u0018\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0011J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0015J$\u0010\u001b\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u0018\u0018\u00010\u0017j\u0004\u0018\u0001`\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u001dHÆ\u0003¢\u0006\u0004\b \u0010\u001fJ¼\u0001\u0010-\u001a\u00020\u00002\b\b\u0002\u0010!\u001a\u00020\u00022\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010$\u001a\u00020\u000b2\u0010\b\u0002\u0010%\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e2\u0010\b\u0002\u0010&\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e2\u0010\b\u0002\u0010'\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u000f2\u001c\b\u0002\u0010*\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u0018\u0018\u00010\u0017j\u0004\u0018\u0001`\u001a2\b\b\u0002\u0010+\u001a\u00020\u001d2\b\b\u0002\u0010,\u001a\u00020\u001dHÆ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u0010/\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b/\u0010\u0015J\u0010\u00101\u001a\u000200HÖ\u0001¢\u0006\u0004\b1\u00102J\u001a\u00105\u001a\u00020\u001d2\b\u00104\u001a\u0004\u0018\u000103HÖ\u0003¢\u0006\u0004\b5\u00106R\u0019\u0010$\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00107\u001a\u0004\b8\u0010\rR\u001b\u0010#\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00109\u001a\u0004\b:\u0010\nR\u0016\u0010;\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R-\u0010*\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u0018\u0018\u00010\u0017j\u0004\u0018\u0001`\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010=\u001a\u0004\b>\u0010\u001cR\u0016\u0010?\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010<R\u001b\u0010)\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010@\u001a\u0004\bA\u0010\u0015R\u0019\u0010B\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010<\u001a\u0004\bC\u0010\u001fR\u0016\u0010D\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010<R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010E\u001a\u0004\bF\u0010\u0007R\u001b\u0010(\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010@\u001a\u0004\bG\u0010\u0015R\u0019\u0010H\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010<\u001a\u0004\bH\u0010\u001fR!\u0010'\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010I\u001a\u0004\bJ\u0010\u0011R\u0019\u0010K\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010<\u001a\u0004\bL\u0010\u001fR!\u0010&\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010I\u001a\u0004\bM\u0010\u0011R\u0019\u0010,\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010<\u001a\u0004\bN\u0010\u001fR\u0019\u0010O\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010<\u001a\u0004\bO\u0010\u001fR\u0019\u0010!\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010P\u001a\u0004\bQ\u0010\u0004R\u0019\u0010R\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010<\u001a\u0004\bS\u0010\u001fR\u001b\u0010T\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010@\u001a\u0004\bU\u0010\u0015R!\u0010%\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010I\u001a\u0004\bV\u0010\u0011R\u0016\u0010W\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010<R\u0019\u0010X\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010<\u001a\u0004\bY\u0010\u001fR\u0019\u0010+\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010<\u001a\u0004\b+\u0010\u001f¨\u0006\\"}, d2 = {"Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState$Loaded;", "Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState;", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/api/user/UserProfile;", "component4", "()Lcom/discord/api/user/UserProfile;", "Lcom/discord/nullserializable/NullSerializable;", "", "component5", "()Lcom/discord/nullserializable/NullSerializable;", "component6", "component7", "component8", "()Ljava/lang/String;", "component9", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/settings/profile/AST;", "component10", "()Ljava/util/List;", "", "component11", "()Z", "component12", "user", "guild", "meMember", "userProfile", "currentAvatar", "currentBannerImage", "currentBannerColorHex", "currentNickname", "currentBio", "bioAst", "isEditingBio", "canEditNickname", "copy", "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/models/member/GuildMember;Lcom/discord/api/user/UserProfile;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/nullserializable/NullSerializable;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZ)Lcom/discord/widgets/settings/profile/SettingsUserProfileViewModel$ViewState$Loaded;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/user/UserProfile;", "getUserProfile", "Lcom/discord/models/member/GuildMember;", "getMeMember", "hasMemberBannerForDisplay", "Z", "Ljava/util/List;", "getBioAst", "hasUserAvatarForDisplay", "Ljava/lang/String;", "getCurrentBio", "showBioEditor", "getShowBioEditor", "hasMemberAvatarForDisplay", "Lcom/discord/models/guild/Guild;", "getGuild", "getCurrentNickname", "isDirty", "Lcom/discord/nullserializable/NullSerializable;", "getCurrentBannerColorHex", "hasBannerImageForDisplay", "getHasBannerImageForDisplay", "getCurrentBannerImage", "getCanEditNickname", "isBioChanged", "Lcom/discord/models/user/MeUser;", "getUser", "hasAvatarForDisplay", "getHasAvatarForDisplay", "nonDefaultColorPreviewHex", "getNonDefaultColorPreviewHex", "getCurrentAvatar", "hasUserBannerForDisplay", "showSaveFab", "getShowSaveFab", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/models/member/GuildMember;Lcom/discord/api/user/UserProfile;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/nullserializable/NullSerializable;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final List<Node<MessageRenderContext>> bioAst;
            private final boolean canEditNickname;
            private final NullSerializable<String> currentAvatar;
            private final NullSerializable<String> currentBannerColorHex;
            private final NullSerializable<String> currentBannerImage;
            private final String currentBio;
            private final String currentNickname;
            private final Guild guild;
            private final boolean hasAvatarForDisplay;
            private final boolean hasBannerImageForDisplay;
            private final boolean hasMemberAvatarForDisplay;
            private final boolean hasMemberBannerForDisplay;
            private final boolean hasUserAvatarForDisplay;
            private final boolean hasUserBannerForDisplay;
            private final boolean isBioChanged;
            private final boolean isDirty;
            private final boolean isEditingBio;
            private final GuildMember meMember;
            private final String nonDefaultColorPreviewHex;
            private final boolean showBioEditor;
            private final boolean showSaveFab;
            private final MeUser user;
            private final UserProfile userProfile;

            public /* synthetic */ Loaded(MeUser meUser, Guild guild, GuildMember guildMember, UserProfile userProfile, NullSerializable nullSerializable, NullSerializable nullSerializable2, NullSerializable nullSerializable3, String str, String str2, List list, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(meUser, guild, guildMember, userProfile, (i & 16) != 0 ? null : nullSerializable, (i & 32) != 0 ? null : nullSerializable2, (i & 64) != 0 ? null : nullSerializable3, (i & 128) != 0 ? null : str, str2, list, z2, z3);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, MeUser meUser, Guild guild, GuildMember guildMember, UserProfile userProfile, NullSerializable nullSerializable, NullSerializable nullSerializable2, NullSerializable nullSerializable3, String str, String str2, List list, boolean z2, boolean z3, int i, Object obj) {
                return loaded.copy((i & 1) != 0 ? loaded.user : meUser, (i & 2) != 0 ? loaded.guild : guild, (i & 4) != 0 ? loaded.meMember : guildMember, (i & 8) != 0 ? loaded.userProfile : userProfile, (i & 16) != 0 ? loaded.currentAvatar : nullSerializable, (i & 32) != 0 ? loaded.currentBannerImage : nullSerializable2, (i & 64) != 0 ? loaded.currentBannerColorHex : nullSerializable3, (i & 128) != 0 ? loaded.currentNickname : str, (i & 256) != 0 ? loaded.currentBio : str2, (i & 512) != 0 ? loaded.bioAst : list, (i & 1024) != 0 ? loaded.isEditingBio : z2, (i & 2048) != 0 ? loaded.canEditNickname : z3);
            }

            public final MeUser component1() {
                return this.user;
            }

            public final List<Node<MessageRenderContext>> component10() {
                return this.bioAst;
            }

            public final boolean component11() {
                return this.isEditingBio;
            }

            public final boolean component12() {
                return this.canEditNickname;
            }

            public final Guild component2() {
                return this.guild;
            }

            public final GuildMember component3() {
                return this.meMember;
            }

            public final UserProfile component4() {
                return this.userProfile;
            }

            public final NullSerializable<String> component5() {
                return this.currentAvatar;
            }

            public final NullSerializable<String> component6() {
                return this.currentBannerImage;
            }

            public final NullSerializable<String> component7() {
                return this.currentBannerColorHex;
            }

            public final String component8() {
                return this.currentNickname;
            }

            public final String component9() {
                return this.currentBio;
            }

            public final Loaded copy(MeUser meUser, Guild guild, GuildMember guildMember, UserProfile userProfile, NullSerializable<String> nullSerializable, NullSerializable<String> nullSerializable2, NullSerializable<String> nullSerializable3, String str, String str2, List<Node<MessageRenderContext>> list, boolean z2, boolean z3) {
                m.checkNotNullParameter(meUser, "user");
                m.checkNotNullParameter(userProfile, "userProfile");
                return new Loaded(meUser, guild, guildMember, userProfile, nullSerializable, nullSerializable2, nullSerializable3, str, str2, list, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.user, loaded.user) && m.areEqual(this.guild, loaded.guild) && m.areEqual(this.meMember, loaded.meMember) && m.areEqual(this.userProfile, loaded.userProfile) && m.areEqual(this.currentAvatar, loaded.currentAvatar) && m.areEqual(this.currentBannerImage, loaded.currentBannerImage) && m.areEqual(this.currentBannerColorHex, loaded.currentBannerColorHex) && m.areEqual(this.currentNickname, loaded.currentNickname) && m.areEqual(this.currentBio, loaded.currentBio) && m.areEqual(this.bioAst, loaded.bioAst) && this.isEditingBio == loaded.isEditingBio && this.canEditNickname == loaded.canEditNickname;
            }

            public final List<Node<MessageRenderContext>> getBioAst() {
                return this.bioAst;
            }

            public final boolean getCanEditNickname() {
                return this.canEditNickname;
            }

            public final NullSerializable<String> getCurrentAvatar() {
                return this.currentAvatar;
            }

            public final NullSerializable<String> getCurrentBannerColorHex() {
                return this.currentBannerColorHex;
            }

            public final NullSerializable<String> getCurrentBannerImage() {
                return this.currentBannerImage;
            }

            public final String getCurrentBio() {
                return this.currentBio;
            }

            public final String getCurrentNickname() {
                return this.currentNickname;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final boolean getHasAvatarForDisplay() {
                return this.hasAvatarForDisplay;
            }

            public final boolean getHasBannerImageForDisplay() {
                return this.hasBannerImageForDisplay;
            }

            public final GuildMember getMeMember() {
                return this.meMember;
            }

            public final String getNonDefaultColorPreviewHex() {
                return this.nonDefaultColorPreviewHex;
            }

            public final boolean getShowBioEditor() {
                return this.showBioEditor;
            }

            public final boolean getShowSaveFab() {
                return this.showSaveFab;
            }

            public final MeUser getUser() {
                return this.user;
            }

            public final UserProfile getUserProfile() {
                return this.userProfile;
            }

            public int hashCode() {
                MeUser meUser = this.user;
                int i = 0;
                int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
                Guild guild = this.guild;
                int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
                GuildMember guildMember = this.meMember;
                int hashCode3 = (hashCode2 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
                UserProfile userProfile = this.userProfile;
                int hashCode4 = (hashCode3 + (userProfile != null ? userProfile.hashCode() : 0)) * 31;
                NullSerializable<String> nullSerializable = this.currentAvatar;
                int hashCode5 = (hashCode4 + (nullSerializable != null ? nullSerializable.hashCode() : 0)) * 31;
                NullSerializable<String> nullSerializable2 = this.currentBannerImage;
                int hashCode6 = (hashCode5 + (nullSerializable2 != null ? nullSerializable2.hashCode() : 0)) * 31;
                NullSerializable<String> nullSerializable3 = this.currentBannerColorHex;
                int hashCode7 = (hashCode6 + (nullSerializable3 != null ? nullSerializable3.hashCode() : 0)) * 31;
                String str = this.currentNickname;
                int hashCode8 = (hashCode7 + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.currentBio;
                int hashCode9 = (hashCode8 + (str2 != null ? str2.hashCode() : 0)) * 31;
                List<Node<MessageRenderContext>> list = this.bioAst;
                if (list != null) {
                    i = list.hashCode();
                }
                int i2 = (hashCode9 + i) * 31;
                boolean z2 = this.isEditingBio;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.canEditNickname;
                if (!z3) {
                    i3 = z3 ? 1 : 0;
                }
                return i6 + i3;
            }

            public final boolean isBioChanged() {
                return this.isBioChanged;
            }

            public final boolean isDirty() {
                return this.isDirty;
            }

            public final boolean isEditingBio() {
                return this.isEditingBio;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(user=");
                R.append(this.user);
                R.append(", guild=");
                R.append(this.guild);
                R.append(", meMember=");
                R.append(this.meMember);
                R.append(", userProfile=");
                R.append(this.userProfile);
                R.append(", currentAvatar=");
                R.append(this.currentAvatar);
                R.append(", currentBannerImage=");
                R.append(this.currentBannerImage);
                R.append(", currentBannerColorHex=");
                R.append(this.currentBannerColorHex);
                R.append(", currentNickname=");
                R.append(this.currentNickname);
                R.append(", currentBio=");
                R.append(this.currentBio);
                R.append(", bioAst=");
                R.append(this.bioAst);
                R.append(", isEditingBio=");
                R.append(this.isEditingBio);
                R.append(", canEditNickname=");
                return a.M(R, this.canEditNickname, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Code restructure failed: missing block: B:22:0x005b, code lost:
                if ((!d0.z.d.m.areEqual(r13 == null ? "" : r13, r9 == null ? "" : r9)) != false) goto L24;
             */
            /* JADX WARN: Removed duplicated region for block: B:15:0x0048  */
            /* JADX WARN: Removed duplicated region for block: B:32:0x0079  */
            /* JADX WARN: Removed duplicated region for block: B:42:0x0092  */
            /* JADX WARN: Removed duplicated region for block: B:48:0x00a5  */
            /* JADX WARN: Removed duplicated region for block: B:54:0x00b2  */
            /* JADX WARN: Removed duplicated region for block: B:60:0x00c1  */
            /* JADX WARN: Removed duplicated region for block: B:68:0x00d0  */
            /* JADX WARN: Removed duplicated region for block: B:74:0x00dd  */
            /* JADX WARN: Removed duplicated region for block: B:80:0x00ec  */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public Loaded(com.discord.models.user.MeUser r2, com.discord.models.guild.Guild r3, com.discord.models.member.GuildMember r4, com.discord.api.user.UserProfile r5, com.discord.nullserializable.NullSerializable<java.lang.String> r6, com.discord.nullserializable.NullSerializable<java.lang.String> r7, com.discord.nullserializable.NullSerializable<java.lang.String> r8, java.lang.String r9, java.lang.String r10, java.util.List<com.discord.simpleast.core.node.Node<com.discord.utilities.textprocessing.MessageRenderContext>> r11, boolean r12, boolean r13) {
                /*
                    Method dump skipped, instructions count: 248
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.profile.SettingsUserProfileViewModel.ViewState.Loaded.<init>(com.discord.models.user.MeUser, com.discord.models.guild.Guild, com.discord.models.member.GuildMember, com.discord.api.user.UserProfile, com.discord.nullserializable.NullSerializable, com.discord.nullserializable.NullSerializable, com.discord.nullserializable.NullSerializable, java.lang.String, java.lang.String, java.util.List, boolean, boolean):void");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ SettingsUserProfileViewModel(Long l, StoreUser storeUser, StoreUserProfile storeUserProfile, StoreGuilds storeGuilds, RestAPI restAPI, Parser parser, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(l, (i & 2) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 4) != 0 ? StoreStream.Companion.getUserProfile() : storeUserProfile, (i & 8) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 16) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 32) != 0 ? DiscordParser.createParser$default(false, false, false, 4, null) : parser, (i & 64) != 0 ? Companion.observeStoreState$default(Companion, l, null, null, null, 14, null) : observable);
    }

    private final List<Node<MessageRenderContext>> createAndProcessBioAstFromText(String str) {
        List<Node<MessageRenderContext>> parseBio = parseBio(str);
        createMessagePreprocessor().process(parseBio);
        return parseBio;
    }

    private final MessagePreprocessor createMessagePreprocessor() {
        return new MessagePreprocessor(-1L, this.revealedBioIndices, null, false, null, 28, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0074  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x007f  */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.settings.profile.SettingsUserProfileViewModel.StoreState r18) {
        /*
            r17 = this;
            r0 = r17
            com.discord.models.user.MeUser r2 = r18.getUser()
            java.lang.Object r1 = r17.getViewState()
            boolean r3 = r1 instanceof com.discord.widgets.settings.profile.SettingsUserProfileViewModel.ViewState.Loaded
            r4 = 0
            if (r3 != 0) goto L10
            r1 = r4
        L10:
            com.discord.widgets.settings.profile.SettingsUserProfileViewModel$ViewState$Loaded r1 = (com.discord.widgets.settings.profile.SettingsUserProfileViewModel.ViewState.Loaded) r1
            if (r1 == 0) goto L1b
            java.lang.String r1 = r1.getCurrentBio()
            if (r1 == 0) goto L1b
            goto L2c
        L1b:
            com.discord.models.member.GuildMember r1 = r18.getMeMember()
            if (r1 == 0) goto L2b
            java.lang.String r1 = r1.getBio()
            if (r1 == 0) goto L28
            goto L2c
        L28:
            java.lang.String r1 = ""
            goto L2c
        L2b:
            r1 = r4
        L2c:
            if (r1 == 0) goto L2f
            goto L33
        L2f:
            java.lang.String r1 = r2.getBio()
        L33:
            r10 = r1
            if (r10 == 0) goto L3c
            java.util.List r1 = r0.createAndProcessBioAstFromText(r10)
            r11 = r1
            goto L3d
        L3c:
            r11 = r4
        L3d:
            java.lang.Object r1 = r17.getViewState()
            boolean r3 = r1 instanceof com.discord.widgets.settings.profile.SettingsUserProfileViewModel.ViewState.Loaded
            if (r3 != 0) goto L46
            r1 = r4
        L46:
            com.discord.widgets.settings.profile.SettingsUserProfileViewModel$ViewState$Loaded r1 = (com.discord.widgets.settings.profile.SettingsUserProfileViewModel.ViewState.Loaded) r1
            if (r1 == 0) goto L51
            java.lang.String r1 = r1.getCurrentNickname()
            if (r1 == 0) goto L51
            goto L5b
        L51:
            com.discord.models.member.GuildMember r1 = r18.getMeMember()
            if (r1 == 0) goto L5d
            java.lang.String r1 = r1.getNick()
        L5b:
            r9 = r1
            goto L5e
        L5d:
            r9 = r4
        L5e:
            com.discord.widgets.settings.profile.SettingsUserProfileViewModel$ViewState$Loaded r15 = new com.discord.widgets.settings.profile.SettingsUserProfileViewModel$ViewState$Loaded
            com.discord.models.guild.Guild r3 = r18.getGuild()
            com.discord.api.user.UserProfile r5 = r18.getUserProfile()
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.Object r1 = r17.getViewState()
            boolean r12 = r1 instanceof com.discord.widgets.settings.profile.SettingsUserProfileViewModel.ViewState.Loaded
            if (r12 != 0) goto L74
            goto L75
        L74:
            r4 = r1
        L75:
            com.discord.widgets.settings.profile.SettingsUserProfileViewModel$ViewState$Loaded r4 = (com.discord.widgets.settings.profile.SettingsUserProfileViewModel.ViewState.Loaded) r4
            if (r4 == 0) goto L7f
            boolean r1 = r4.isEditingBio()
            r12 = r1
            goto L81
        L7f:
            r1 = 0
            r12 = 0
        L81:
            com.discord.models.member.GuildMember r4 = r18.getMeMember()
            com.discord.utilities.channel.GuildChannelsInfo r1 = r18.getGuildChannelsInfo()
            boolean r13 = r1.getCanChangeNickname()
            r14 = 112(0x70, float:1.57E-43)
            r16 = 0
            r1 = r15
            r0 = r15
            r15 = r16
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            r1 = r0
            r0 = r17
            r0.updateViewState(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.settings.profile.SettingsUserProfileViewModel.handleStoreState(com.discord.widgets.settings.profile.SettingsUserProfileViewModel$StoreState):void");
    }

    private final List<Node<MessageRenderContext>> parseBio(String str) {
        return Parser.parse$default(this.bioParser, str, MessageParseState.Companion.getInitialState(), null, 4, null);
    }

    @MainThread
    private final void saveGuildMemberChanges(Context context) {
        Long l;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (l = this.guildId) != null) {
            l.longValue();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.updateMeGuildMember(this.guildId.longValue(), new PatchGuildMemberBody(loaded.getCurrentNickname(), loaded.getCurrentAvatar(), loaded.getCurrentBannerImage(), loaded.getCurrentBio())), false, 1, null), this, null, 2, null), SettingsUserProfileViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new SettingsUserProfileViewModel$saveGuildMemberChanges$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new SettingsUserProfileViewModel$saveGuildMemberChanges$2(this, loaded));
        }
    }

    @MainThread
    private final void saveUserChanges(Context context) {
        NullSerializable nullSerializable;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            MeUser user = loaded.getUser();
            String currentBio = loaded.getCurrentBio();
            if (!loaded.isBioChanged()) {
                nullSerializable = null;
            } else if (currentBio != null) {
                nullSerializable = new NullSerializable.b(currentBio);
            } else {
                nullSerializable = new NullSerializable.a(null, 1);
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.patchUser(new PatchUserBody(loaded.getCurrentAvatar(), loaded.getCurrentBannerImage(), loaded.getCurrentBannerColorHex(), nullSerializable)), false, 1, null), this, null, 2, null), SettingsUserProfileViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new SettingsUserProfileViewModel$saveUserChanges$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new SettingsUserProfileViewModel$saveUserChanges$2(this, user, loaded));
        }
    }

    @MainThread
    public final void handleBioIndexClicked(SpoilerNode<?> spoilerNode) {
        String currentBio;
        m.checkNotNullParameter(spoilerNode, "spoilerNode");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (currentBio = loaded.getCurrentBio()) != null) {
            this.revealedBioIndices.add(Integer.valueOf(spoilerNode.getId()));
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, null, null, null, null, createAndProcessBioAstFromText(currentBio), false, false, 3583, null));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final Observable<UserProfileHeaderViewModel.ViewState> observeHeaderViewState() {
        Observable<UserProfileHeaderViewModel.ViewState> q = observeViewState().F(SettingsUserProfileViewModel$observeHeaderViewState$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeViewState().map {… }.distinctUntilChanged()");
        return q;
    }

    @MainThread
    public final void saveChanges(Context context) {
        m.checkNotNullParameter(context, "context");
        if (this.guildId != null) {
            saveGuildMemberChanges(context);
        } else {
            saveUserChanges(context);
        }
    }

    @MainThread
    public final void updateAvatar(String str) {
        NullSerializable.b bVar;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            if (str != null) {
                bVar = new NullSerializable.b(str);
            } else {
                bVar = new NullSerializable.a(null, 1);
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, bVar, null, null, null, null, null, false, false, 4079, null));
        }
    }

    @MainThread
    public final void updateBannerColor(String str) {
        NullSerializable.b bVar;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            if (str != null) {
                bVar = new NullSerializable.b(str);
            } else {
                bVar = new NullSerializable.a(null, 1);
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, null, bVar, null, null, null, false, false, 4031, null));
        }
    }

    @MainThread
    public final void updateBannerImage(String str) {
        NullSerializable.b bVar;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            if (str != null) {
                bVar = new NullSerializable.b(str);
            } else {
                bVar = new NullSerializable.a(null, 1);
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, bVar, null, null, null, null, false, false, 4063, null));
        }
    }

    @MainThread
    public final void updateBio(String str) {
        m.checkNotNullParameter(str, "bio");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && (!m.areEqual(str, loaded.getCurrentBio()))) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, null, null, null, str, createAndProcessBioAstFromText(str), false, false, 3327, null));
        }
    }

    @MainThread
    public final void updateIsEditingBio(boolean z2) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, null, null, null, null, null, z2, false, 3071, null));
        }
    }

    @MainThread
    public final void updateNickname(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NICK);
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, null, null, null, null, null, null, null, str, null, null, false, false, 3967, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsUserProfileViewModel(Long l, StoreUser storeUser, StoreUserProfile storeUserProfile, StoreGuilds storeGuilds, RestAPI restAPI, Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser, Observable<StoreState> observable) {
        super(null);
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeUserProfile, "storeUserProfile");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(parser, "bioParser");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.guildId = l;
        this.storeUserProfile = storeUserProfile;
        this.storeGuilds = storeGuilds;
        this.restAPI = restAPI;
        this.bioParser = parser;
        this.eventSubject = PublishSubject.k0();
        Observable Z = ObservableExtensionsKt.computationLatest(storeUser.observeMe(false)).Z(1);
        m.checkNotNullExpressionValue(Z, "storeUser.observeMe(emit…Latest()\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Z, this, null, 2, null), SettingsUserProfileViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        Observable<StoreState> q = observable.q();
        m.checkNotNullExpressionValue(q, "storeStateObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), SettingsUserProfileViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
        this.revealedBioIndices = new LinkedHashSet();
    }
}
