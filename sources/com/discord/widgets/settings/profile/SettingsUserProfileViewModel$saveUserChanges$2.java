package com.discord.widgets.settings.profile;

import com.discord.api.user.User;
import com.discord.models.user.MeUser;
import com.discord.widgets.settings.profile.SettingsUserProfileViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: EditUserOrGuildMemberProfileViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/user/User;", "apiUser", "", "invoke", "(Lcom/discord/api/user/User;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsUserProfileViewModel$saveUserChanges$2 extends o implements Function1<User, Unit> {
    public final /* synthetic */ MeUser $meUser;
    public final /* synthetic */ SettingsUserProfileViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ SettingsUserProfileViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsUserProfileViewModel$saveUserChanges$2(SettingsUserProfileViewModel settingsUserProfileViewModel, MeUser meUser, SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = settingsUserProfileViewModel;
        this.$meUser = meUser;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(User user) {
        invoke2(user);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(User user) {
        PublishSubject publishSubject;
        m.checkNotNullParameter(user, "apiUser");
        this.this$0.storeUserProfile.updateUser(user);
        this.this$0.updateViewState(SettingsUserProfileViewModel.ViewState.Loaded.copy$default(this.$viewState, MeUser.Companion.merge(this.$meUser, user), null, null, null, null, null, null, null, null, null, false, false, 3982, null));
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(SettingsUserProfileViewModel.Event.UserUpdateRequestCompleted.INSTANCE);
    }
}
