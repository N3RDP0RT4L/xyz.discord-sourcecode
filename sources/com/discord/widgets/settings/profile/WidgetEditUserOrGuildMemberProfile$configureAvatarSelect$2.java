package com.discord.widgets.settings.profile;

import androidx.fragment.app.FragmentManager;
import b.a.y.c0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$2 extends o implements Function0<Unit> {
    public final /* synthetic */ List $avatarSheetOptions;
    public final /* synthetic */ WidgetEditUserOrGuildMemberProfile this$0;

    /* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "selectedItemPosition", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Integer, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
            invoke(num.intValue());
            return Unit.a;
        }

        public final void invoke(int i) {
            Function1 function1;
            SettingsUserProfileViewModel viewModel;
            if (i == 0) {
                WidgetEditUserOrGuildMemberProfile widgetEditUserOrGuildMemberProfile = WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$2.this.this$0;
                function1 = widgetEditUserOrGuildMemberProfile.avatarSelectedResult;
                widgetEditUserOrGuildMemberProfile.imageSelectedResult = function1;
                WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$2.this.this$0.openMediaChooser();
            } else if (i == 1) {
                viewModel = WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$2.this.this$0.getViewModel();
                viewModel.updateAvatar(null);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEditUserOrGuildMemberProfile$configureAvatarSelect$2(WidgetEditUserOrGuildMemberProfile widgetEditUserOrGuildMemberProfile, List list) {
        super(0);
        this.this$0 = widgetEditUserOrGuildMemberProfile;
        this.$avatarSheetOptions = list;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        c0.a aVar = c0.k;
        FragmentManager childFragmentManager = this.this$0.getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        aVar.a(childFragmentManager, "", this.$avatarSheetOptions, false, new AnonymousClass1());
    }
}
