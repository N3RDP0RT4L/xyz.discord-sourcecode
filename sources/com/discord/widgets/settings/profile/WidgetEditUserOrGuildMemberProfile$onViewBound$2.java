package com.discord.widgets.settings.profile;

import android.view.MotionEvent;
import androidx.cardview.widget.CardView;
import com.discord.databinding.WidgetSettingsUserProfileBinding;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/MotionEvent;", "event", "", "invoke", "(Landroid/view/MotionEvent;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEditUserOrGuildMemberProfile$onViewBound$2 extends o implements Function1<MotionEvent, Boolean> {
    public final /* synthetic */ WidgetEditUserOrGuildMemberProfile this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEditUserOrGuildMemberProfile$onViewBound$2(WidgetEditUserOrGuildMemberProfile widgetEditUserOrGuildMemberProfile) {
        super(1);
        this.this$0 = widgetEditUserOrGuildMemberProfile;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(MotionEvent motionEvent) {
        return Boolean.valueOf(invoke2(motionEvent));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(MotionEvent motionEvent) {
        WidgetSettingsUserProfileBinding binding;
        WidgetSettingsUserProfileBinding binding2;
        WidgetSettingsUserProfileBinding binding3;
        WidgetSettingsUserProfileBinding binding4;
        if (motionEvent == null) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        boolean z2 = true;
        if (actionMasked == 1 || actionMasked == 3) {
            float rawX = motionEvent.getRawX();
            float rawY = motionEvent.getRawY();
            binding = this.this$0.getBinding();
            TextInputLayout textInputLayout = binding.d;
            m.checkNotNullExpressionValue(textInputLayout, "binding.bioEditorTextInputFieldWrap");
            int width = textInputLayout.getWidth();
            binding2 = this.this$0.getBinding();
            TextInputLayout textInputLayout2 = binding2.d;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.bioEditorTextInputFieldWrap");
            int height = textInputLayout2.getHeight();
            int[] iArr = new int[2];
            binding3 = this.this$0.getBinding();
            binding3.d.getLocationOnScreen(iArr);
            int i = iArr[0];
            int i2 = iArr[1];
            if (rawX < ((float) i) || rawX > ((float) (i + width)) || rawY < ((float) i2) || rawY > ((float) (i2 + height))) {
                binding4 = this.this$0.getBinding();
                CardView cardView = binding4.f2614b;
                m.checkNotNullExpressionValue(cardView, "binding.bioEditorCard");
                if (cardView.getVisibility() != 0) {
                    z2 = false;
                }
                if (z2) {
                    this.this$0.setCurrentBioFromEditor();
                }
            }
        }
        return false;
    }
}
