package com.discord.widgets.settings.profile;

import androidx.fragment.app.FragmentManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.settings.profile.SettingsUserProfileViewModel;
import com.discord.widgets.settings.profile.WidgetEditProfileBannerSheet;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2 extends o implements Function0<Unit> {
    public final /* synthetic */ SettingsUserProfileViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ WidgetEditUserOrGuildMemberProfile this$0;

    /* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "avatarRepresentativeColorHex", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<String, Unit> {

        /* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02601 extends o implements Function0<Unit> {
            public C02601() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                Function1 function1;
                WidgetEditUserOrGuildMemberProfile widgetEditUserOrGuildMemberProfile = WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.this$0;
                function1 = widgetEditUserOrGuildMemberProfile.bannerSelectedResult;
                widgetEditUserOrGuildMemberProfile.imageSelectedResult = function1;
                WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.this$0.openMediaChooser();
            }
        }

        /* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends o implements Function0<Unit> {
            public AnonymousClass2() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                SettingsUserProfileViewModel viewModel;
                viewModel = WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.this$0.getViewModel();
                viewModel.updateBannerImage(null);
            }
        }

        /* compiled from: WidgetEditUserOrGuildMemberProfile.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "colorInt", "", "invoke", "(Ljava/lang/Integer;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2$1$3  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass3 extends o implements Function1<Integer, Unit> {
            public AnonymousClass3() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                invoke2(num);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Integer num) {
                String str;
                SettingsUserProfileViewModel viewModel;
                if (num != null) {
                    str = ColorCompat.INSTANCE.getColorHexFromColorInt(num.intValue());
                } else {
                    str = null;
                }
                viewModel = WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.this$0.getViewModel();
                viewModel.updateBannerColor(str);
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            Long guildId;
            WidgetEditProfileBannerSheet.Companion companion = WidgetEditProfileBannerSheet.Companion;
            guildId = WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.this$0.getGuildId();
            m.checkNotNullExpressionValue(str, "avatarRepresentativeColorHex");
            String nonDefaultColorPreviewHex = WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.$viewState.getNonDefaultColorPreviewHex();
            boolean hasBannerImageForDisplay = WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.$viewState.getHasBannerImageForDisplay();
            FragmentManager parentFragmentManager = WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2.this.this$0.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(guildId, str, nonDefaultColorPreviewHex, hasBannerImageForDisplay, parentFragmentManager, new C02601(), new AnonymousClass2(), new AnonymousClass3());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEditUserOrGuildMemberProfile$configureBannerSelect$2(WidgetEditUserOrGuildMemberProfile widgetEditUserOrGuildMemberProfile, SettingsUserProfileViewModel.ViewState.Loaded loaded) {
        super(0);
        this.this$0 = widgetEditUserOrGuildMemberProfile;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        BehaviorSubject behaviorSubject;
        behaviorSubject = this.this$0.avatarRepresentativeColorHexSubject;
        Observable K = behaviorSubject.Z(1).K();
        m.checkNotNullExpressionValue(K, "avatarRepresentativeColo…  .onBackpressureLatest()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(K, this.this$0, null, 2, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
