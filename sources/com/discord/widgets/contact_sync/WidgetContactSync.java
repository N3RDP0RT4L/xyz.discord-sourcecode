package com.discord.widgets.contact_sync;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.e0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.analytics.generated.traits.TrackImpressionMetadata;
import com.discord.analytics.utils.ImpressionGroups;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetContactSyncBinding;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.contacts.ContactsProviderUtils;
import com.discord.utilities.error.Error;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.LoadingButton;
import com.discord.widgets.captcha.WidgetCaptcha;
import com.discord.widgets.contact_sync.AddFriendsFailed;
import com.discord.widgets.contact_sync.WidgetContactSyncViewModel;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.t;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetContactSync.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 G2\u00020\u0001:\u0001GB\u0007¢\u0006\u0004\bF\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\t\u001a\u00020\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0019\u0010\fJ\u000f\u0010\u001a\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001a\u0010\fJ\u000f\u0010\u001b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\fJ\u0017\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001cH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010\"\u001a\u00020\u00042\u0006\u0010!\u001a\u00020 H\u0016¢\u0006\u0004\b\"\u0010#J\u000f\u0010$\u001a\u00020\u0004H\u0016¢\u0006\u0004\b$\u0010\fJ\u000f\u0010%\u001a\u00020\u0004H\u0016¢\u0006\u0004\b%\u0010\fR\u0018\u0010'\u001a\u0004\u0018\u00010&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(R\u001d\u0010.\u001a\u00020)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u001c\u00103\u001a\u0002028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b3\u00104\u001a\u0004\b5\u00106R\u0016\u00108\u001a\u0002078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109R\u0018\u0010:\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u0010;R\u001d\u0010A\u001a\u00020<8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@R\u001c\u0010D\u001a\b\u0012\u0004\u0012\u00020C0B8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010E¨\u0006H"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSync;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ViewState;)V", "", "captchaKey", "submitPhoneNumber", "(Ljava/lang/String;)V", "handlePhoneNumberTextChanged", "()V", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "toolbarConfig", "configureToolbar", "(Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;)V", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;", "displayedChild", "configureViewFlipper", "(Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;)V", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;)V", "requestContactsPermissions", "onPermissionsDenied", "onPermissionsGranted", "Lcom/discord/utilities/error/Error;", "error", "launchCaptchaFlow", "(Lcom/discord/utilities/error/Error;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onResume", "Lcom/discord/widgets/contact_sync/ContactSyncFriendSuggestionListAdapter;", "friendSuggestionsAdapter", "Lcom/discord/widgets/contact_sync/ContactSyncFriendSuggestionListAdapter;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel;", "viewModel", "Lcom/discord/analytics/generated/traits/TrackImpressionMetadata;", "contactSyncFlowMetadata", "Lcom/discord/analytics/generated/traits/TrackImpressionMetadata;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "", "displayedChildIndex", "I", "phoneNumber", "Ljava/lang/String;", "Lcom/discord/databinding/WidgetContactSyncBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetContactSyncBinding;", "binding", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "captchaLauncher", "Landroidx/activity/result/ActivityResultLauncher;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetContactSync extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetContactSync.class, "binding", "getBinding()Lcom/discord/databinding/WidgetContactSyncBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_CONTACT_SYNC_ALLOW_EMAIL = "INTENT_EXTRA_CONTACT_SYNC_ALLOW_EMAIL";
    private static final String INTENT_EXTRA_CONTACT_SYNC_ALLOW_PHONE = "INTENT_EXTRA_CONTACT_SYNC_ALLOW_PHONE";
    private static final String INTENT_EXTRA_CONTACT_SYNC_IMMEDIATELY_PROCEED = "INTENT_EXTRA_CONTACT_SYNC_IMMEDIATELY_PROCEED";
    private static final String INTENT_EXTRA_CONTACT_SYNC_MODE = "INTENT_EXTRA_CONTACT_SYNC_MODE";
    private int displayedChildIndex;
    private ContactSyncFriendSuggestionListAdapter friendSuggestionsAdapter;
    private String phoneNumber;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetContactSync$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetContactSyncViewModel.class), new WidgetContactSync$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(new WidgetContactSync$viewModel$2(this)));
    private final TrackImpressionMetadata contactSyncFlowMetadata = new TrackImpressionMetadata(null, null, null, ImpressionGroups.CONTACT_SYNC_FLOW, 7);
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetCaptcha.Companion.registerForResult(this, new WidgetContactSync$captchaLauncher$1(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, new WidgetContactSync$loggingConfig$1(this), 3);

    /* compiled from: WidgetContactSync.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\n\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\n\u0010\tJ=\u0010\u0012\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\r\u001a\u00020\u00042\b\b\u0002\u0010\u000e\u001a\u00020\u00072\b\b\u0002\u0010\u000f\u001a\u00020\u00072\b\b\u0002\u0010\u0010\u001a\u00020\u0007¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0016R\u0016\u0010\u0019\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0016¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSync$Companion;", "", "Lcom/discord/app/AppActivity;", "appActivity", "Lcom/discord/widgets/contact_sync/ContactSyncMode;", "getContactSyncModeFromIntent", "(Lcom/discord/app/AppActivity;)Lcom/discord/widgets/contact_sync/ContactSyncMode;", "", "getPhoneDiscoverableFromIntent", "(Lcom/discord/app/AppActivity;)Z", "getEmailDiscoverableFromIntent", "Landroid/content/Context;", "context", "mode", "immediatelyProceed", "discoverByPhone", "discoverByEmail", "", "launch", "(Landroid/content/Context;Lcom/discord/widgets/contact_sync/ContactSyncMode;ZZZ)V", "", WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_ALLOW_EMAIL, "Ljava/lang/String;", WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_ALLOW_PHONE, WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_IMMEDIATELY_PROCEED, WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_MODE, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, ContactSyncMode contactSyncMode, boolean z2, boolean z3, boolean z4, int i, Object obj) {
            if ((i & 2) != 0) {
                contactSyncMode = ContactSyncMode.DEFAULT;
            }
            companion.launch(context, contactSyncMode, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? true : z3, (i & 16) != 0 ? true : z4);
        }

        public final ContactSyncMode getContactSyncModeFromIntent(AppActivity appActivity) {
            m.checkNotNullParameter(appActivity, "appActivity");
            Serializable serializableExtra = appActivity.c().getSerializableExtra(WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_MODE);
            Objects.requireNonNull(serializableExtra, "null cannot be cast to non-null type com.discord.widgets.contact_sync.ContactSyncMode");
            return (ContactSyncMode) serializableExtra;
        }

        public final boolean getEmailDiscoverableFromIntent(AppActivity appActivity) {
            m.checkNotNullParameter(appActivity, "appActivity");
            return appActivity.c().getBooleanExtra(WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_ALLOW_EMAIL, true);
        }

        public final boolean getPhoneDiscoverableFromIntent(AppActivity appActivity) {
            m.checkNotNullParameter(appActivity, "appActivity");
            return appActivity.c().getBooleanExtra(WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_ALLOW_PHONE, true);
        }

        public final void launch(Context context, ContactSyncMode contactSyncMode, boolean z2, boolean z3, boolean z4) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(contactSyncMode, "mode");
            Intent intent = new Intent();
            intent.putExtra(WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_MODE, contactSyncMode);
            intent.putExtra(WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_IMMEDIATELY_PROCEED, z2);
            intent.putExtra(WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_ALLOW_PHONE, z3);
            intent.putExtra(WidgetContactSync.INTENT_EXTRA_CONTACT_SYNC_ALLOW_EMAIL, z4);
            j.d(context, WidgetContactSync.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetContactSync() {
        super(R.layout.widget_contact_sync);
    }

    private final void configureToolbar(final WidgetContactSyncViewModel.ToolbarConfig toolbarConfig) {
        setActionBarDisplayHomeAsUpEnabled(toolbarConfig.getShowBackButton());
        setActionBarOptionsMenu(R.menu.menu_contact_sync, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureToolbar$1
            public final void call(MenuItem menuItem, Context context) {
                WidgetContactSyncViewModel viewModel;
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_contact_sync_skip) {
                    viewModel = WidgetContactSync.this.getViewModel();
                    viewModel.skip();
                }
            }
        }, new Action1<Menu>() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureToolbar$2
            public final void call(Menu menu) {
                MenuItem findItem = menu.findItem(R.id.menu_contact_sync_skip);
                m.checkNotNullExpressionValue(findItem, "menu.findItem(R.id.menu_contact_sync_skip)");
                findItem.setVisible(WidgetContactSyncViewModel.ToolbarConfig.this.getShowSkip());
            }
        });
        AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureToolbar$3
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                WidgetContactSyncViewModel viewModel;
                if (!toolbarConfig.getShowBackButton()) {
                    return Boolean.FALSE;
                }
                viewModel = WidgetContactSync.this.getViewModel();
                viewModel.skip();
                return Boolean.TRUE;
            }
        }, 0, 2, null);
    }

    public final void configureUI(final WidgetContactSyncViewModel.ViewState viewState) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        this.phoneNumber = viewState.getPhoneNumber();
        configureViewFlipper(viewState.getDisplayedChild());
        configureToolbar(viewState.getToolbarConfig());
        LoadingButton loadingButton = getBinding().e.c;
        m.checkNotNullExpressionValue(loadingButton, "binding.contactSyncLandi…tactSyncLandingNextButton");
        loadingButton.setEnabled(viewState.getLandingNextEnabled());
        getBinding().e.c.setIsLoading(viewState.isSubmitting());
        getBinding().f.d.setIsLoading(viewState.isSubmitting());
        getBinding().c.c.setIsLoading(viewState.isSubmitting());
        if (viewState.getPermissionsDenied()) {
            TextView textView = getBinding().e.f127b;
            m.checkNotNullExpressionValue(textView, "binding.contactSyncLandi…ncLandingNeedsPermissions");
            textView.setVisibility(0);
            View view = getBinding().e.d;
            m.checkNotNullExpressionValue(view, "binding.contactSyncLandi…LandingPermissionsDivider");
            view.setVisibility(0);
            LoadingButton loadingButton2 = getBinding().e.c;
            e3 = b.e(this, R.string.password_manager_open_settings, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            loadingButton2.setText(e3);
            getBinding().e.c.setOnClickListener(WidgetContactSync$configureUI$1.INSTANCE);
        } else {
            TextView textView2 = getBinding().e.f127b;
            m.checkNotNullExpressionValue(textView2, "binding.contactSyncLandi…ncLandingNeedsPermissions");
            textView2.setVisibility(8);
            View view2 = getBinding().e.d;
            m.checkNotNullExpressionValue(view2, "binding.contactSyncLandi…LandingPermissionsDivider");
            view2.setVisibility(8);
            LoadingButton loadingButton3 = getBinding().e.c;
            e2 = b.e(this, R.string.get_started, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            loadingButton3.setText(e2);
            getBinding().e.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    WidgetContactSyncViewModel viewModel;
                    viewModel = WidgetContactSync.this.getViewModel();
                    viewModel.onLandingNext();
                }
            });
        }
        CheckedSetting checkedSetting = getBinding().e.e.c;
        m.checkNotNullExpressionValue(checkedSetting, "binding.contactSyncLandi…ontactSyncDiscoveryToggle");
        checkedSetting.setChecked(viewState.getAllowPhone() || viewState.getAllowEmail());
        getBinding().e.e.c.e(new View.OnClickListener() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                WidgetContactSyncBinding binding;
                WidgetContactSyncViewModel viewModel;
                binding = WidgetContactSync.this.getBinding();
                CheckedSetting checkedSetting2 = binding.e.e.c;
                m.checkNotNullExpressionValue(checkedSetting2, "binding.contactSyncLandi…ontactSyncDiscoveryToggle");
                boolean isChecked = checkedSetting2.isChecked();
                viewModel = WidgetContactSync.this.getViewModel();
                viewModel.onPermissionsToggle(!isChecked, !isChecked);
            }
        });
        LinkifiedTextView linkifiedTextView = getBinding().e.e.f155b;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.contactSyncLandi…contactSyncDiscoveryInfo2");
        b.m(linkifiedTextView, R.string.contact_sync_permissions_description_android, new Object[0], new WidgetContactSync$configureUI$4(this));
        getBinding().f.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                WidgetContactSyncViewModel viewModel;
                WidgetContactSyncBinding binding;
                viewModel = WidgetContactSync.this.getViewModel();
                binding = WidgetContactSync.this.getBinding();
                TextInputLayout textInputLayout = binding.f.c;
                m.checkNotNullExpressionValue(textInputLayout, "binding.contactSyncName.contactSyncNameInputWrap");
                viewModel.onNameSubmitted(ViewExtensions.getTextOrEmpty(textInputLayout));
            }
        });
        ContactSyncFriendSuggestionListAdapter contactSyncFriendSuggestionListAdapter = this.friendSuggestionsAdapter;
        if (contactSyncFriendSuggestionListAdapter != null) {
            contactSyncFriendSuggestionListAdapter.setData(viewState.getFriendSuggestions());
        }
        LoadingButton loadingButton4 = getBinding().c.c;
        e = b.e(this, R.string.next, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        loadingButton4.setText(e);
        getBinding().c.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureUI$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                WidgetContactSyncViewModel viewModel;
                viewModel = WidgetContactSync.this.getViewModel();
                viewModel.onBulkAddFriends();
            }
        });
        getBinding().f2336b.f120b.b(this);
        getBinding().f2336b.f120b.setCountryCode(viewState.getCountryCode());
        getBinding().f2336b.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureUI$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                WidgetContactSync.submitPhoneNumber$default(WidgetContactSync.this, null, 1, null);
            }
        });
        getBinding().f2336b.f120b.a(this, new WidgetContactSync$configureUI$8(this));
        getBinding().g.f161b.setOnCodeEntered(new WidgetContactSync$configureUI$9(this));
        getBinding().d.f148b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureUI$10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                CharSequence e4;
                CharSequence e5;
                AnalyticsTracker.INSTANCE.friendAddViewed("Invite");
                m.checkNotNullExpressionValue(view3, "it");
                Context context = view3.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                e4 = b.e(WidgetContactSync.this, R.string.friends_share_tabbar_title, new Object[]{BuildConfig.HOST, viewState.getUsername()}, (r4 & 4) != 0 ? b.a.j : null);
                String obj = e4.toString();
                e5 = b.e(WidgetContactSync.this, R.string.tip_instant_invite_title3, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                IntentUtils.performChooserSendIntent(context, obj, e5);
            }
        });
        getBinding().d.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.contact_sync.WidgetContactSync$configureUI$11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                WidgetContactSyncViewModel viewModel;
                viewModel = WidgetContactSync.this.getViewModel();
                viewModel.skip();
            }
        });
    }

    private final void configureViewFlipper(WidgetContactSyncViewModel.Views views) {
        int i;
        int ordinal = views.ordinal();
        boolean z2 = true;
        if (ordinal != this.displayedChildIndex) {
            AppFragment.hideKeyboard$default(this, null, 1, null);
        }
        if (!(views == WidgetContactSyncViewModel.Views.VIEW_LANDING && ((i = this.displayedChildIndex) == 2 || i == 3))) {
            z2 = false;
        }
        if (AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
            AppViewFlipper appViewFlipper = getBinding().h;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.contactSyncViewFlipper");
            appViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.anim_fade_in_fast));
            AppViewFlipper appViewFlipper2 = getBinding().h;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.contactSyncViewFlipper");
            appViewFlipper2.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.anim_fade_out_fast));
        } else {
            int i2 = this.displayedChildIndex;
            if (ordinal > i2 || z2) {
                AppViewFlipper appViewFlipper3 = getBinding().h;
                m.checkNotNullExpressionValue(appViewFlipper3, "binding.contactSyncViewFlipper");
                appViewFlipper3.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.activity_slide_horizontal_open_in));
                AppViewFlipper appViewFlipper4 = getBinding().h;
                m.checkNotNullExpressionValue(appViewFlipper4, "binding.contactSyncViewFlipper");
                appViewFlipper4.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.activity_slide_horizontal_open_out));
            } else if (ordinal < i2) {
                AppViewFlipper appViewFlipper5 = getBinding().h;
                m.checkNotNullExpressionValue(appViewFlipper5, "binding.contactSyncViewFlipper");
                appViewFlipper5.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.activity_slide_horizontal_close_in));
                AppViewFlipper appViewFlipper6 = getBinding().h;
                m.checkNotNullExpressionValue(appViewFlipper6, "binding.contactSyncViewFlipper");
                appViewFlipper6.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.activity_slide_horizontal_close_out));
            }
        }
        AppViewFlipper appViewFlipper7 = getBinding().h;
        m.checkNotNullExpressionValue(appViewFlipper7, "binding.contactSyncViewFlipper");
        appViewFlipper7.setDisplayedChild(ordinal);
        this.displayedChildIndex = ordinal;
        getAppLogger().a(null);
    }

    public final WidgetContactSyncBinding getBinding() {
        return (WidgetContactSyncBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final WidgetContactSyncViewModel getViewModel() {
        return (WidgetContactSyncViewModel) this.viewModel$delegate.getValue();
    }

    public final void handleEvent(WidgetContactSyncViewModel.Event event) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        CharSequence e4;
        CharSequence e5;
        CharSequence e6;
        if (m.areEqual(event, WidgetContactSyncViewModel.Event.MaybeProceedFromLanding.INSTANCE)) {
            if (getMostRecentIntent().getBooleanExtra(INTENT_EXTRA_CONTACT_SYNC_IMMEDIATELY_PROCEED, false)) {
                getViewModel().onLandingNext();
            }
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.PermissionsNeeded.INSTANCE)) {
            requestContactsPermissions();
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.ContactsEnabled.INSTANCE)) {
            getViewModel().onContactsFetched(ContactsProviderUtils.INSTANCE.getAllContactPhoneNumbers(requireContext()));
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.RateLimited.INSTANCE)) {
            e6 = b.e(this, R.string.contact_sync_failed_alert_title, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            b.a.d.m.f(this, e6, 1);
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.UploadFailed.INSTANCE)) {
            e5 = b.e(this, R.string.contact_sync_failed_alert_message, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            b.a.d.m.f(this, e5, 1);
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.ContactsEnableFailed.INSTANCE)) {
            e4 = b.e(this, R.string.contact_sync_failed_alert_title, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            b.a.d.m.f(this, e4, 1);
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.AddFriendsFailed.INSTANCE)) {
            AddFriendsFailed.Companion companion = AddFriendsFailed.Companion;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(parentFragmentManager).setOnClose(new WidgetContactSync$handleEvent$$inlined$apply$lambda$1(this));
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.AddFriendsFailedPartial.INSTANCE)) {
            AddFriendsFailed.Companion companion2 = AddFriendsFailed.Companion;
            FragmentManager parentFragmentManager2 = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            companion2.show(parentFragmentManager2).setOnClose(new WidgetContactSync$handleEvent$$inlined$apply$lambda$2(this));
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.PhoneInvalid.INSTANCE)) {
            e3 = b.e(this, R.string.phone_invalid, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            b.a.d.m.j(this, e3, 0, 4);
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.VerificationCodeInvalid.INSTANCE)) {
            e2 = b.e(this, R.string.application_entitlement_code_redemption_invalid, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            b.a.d.m.j(this, e2, 0, 4);
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.VerificationFailed.INSTANCE)) {
            e = b.e(this, R.string.phone_failed_to_add, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            b.a.d.m.j(this, e, 0, 4);
        } else if (m.areEqual(event, WidgetContactSyncViewModel.Event.Completed.INSTANCE)) {
            requireAppActivity().finish();
        }
    }

    public final void handlePhoneNumberTextChanged() {
        String textOrEmpty = getBinding().f2336b.f120b.getTextOrEmpty();
        MaterialButton materialButton = getBinding().f2336b.c;
        m.checkNotNullExpressionValue(materialButton, "binding.contactSyncAddPh…e.contactSyncAddPhoneNext");
        boolean z2 = true;
        if (!(textOrEmpty.length() > 0) || !t.startsWith$default(textOrEmpty, BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX, false, 2, null)) {
            z2 = false;
        }
        materialButton.setEnabled(z2);
    }

    public final void launchCaptchaFlow(Error error) {
        WidgetCaptcha.Companion companion = WidgetCaptcha.Companion;
        Context requireContext = requireContext();
        ActivityResultLauncher<Intent> activityResultLauncher = this.captchaLauncher;
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        companion.processErrorsForCaptcha(requireContext, activityResultLauncher, u.toMutableList((Collection) response.getMessages().keySet()), error);
    }

    public final void onPermissionsDenied() {
        getViewModel().onPermissionsDenied();
    }

    public final void onPermissionsGranted() {
        getViewModel().onPermissionsGranted();
        if (this.phoneNumber != null) {
            ContactsProviderUtils contactsProviderUtils = ContactsProviderUtils.INSTANCE;
            Context requireContext = requireContext();
            String str = this.phoneNumber;
            m.checkNotNull(str);
            String ownName = contactsProviderUtils.getOwnName(requireContext, str);
            if (ownName != null) {
                getBinding().f.f134b.setText(ownName);
                TextView textView = getBinding().f.e;
                m.checkNotNullExpressionValue(textView, "binding.contactSyncName.contactSyncNamePrefillHint");
                textView.setVisibility(0);
            }
        }
    }

    private final void requestContactsPermissions() {
        if (!ContactsProviderUtils.INSTANCE.hasContactPermissions(requireContext())) {
            AnalyticsTracker.INSTANCE.permissionsRequested("contacts");
            getViewModel().requestingPermissions();
        }
        requestContacts(new WidgetContactSync$requestContactsPermissions$1(this), new WidgetContactSync$requestContactsPermissions$2(this));
    }

    public final void submitPhoneNumber(String str) {
        getViewModel().onPhoneNumberSubmitted(getBinding().f2336b.f120b.getTextOrEmpty(), str);
    }

    public static /* synthetic */ void submitPhoneNumber$default(WidgetContactSync widgetContactSync, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        widgetContactSync.submitPhoneNumber(str);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (ContactsProviderUtils.INSTANCE.hasContactPermissions(requireContext())) {
            getViewModel().onPermissionsBecameAvailable();
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RecyclerView recyclerView = getBinding().c.f141b;
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        m.checkNotNullExpressionValue(recyclerView, "it");
        ContactSyncFriendSuggestionListAdapter contactSyncFriendSuggestionListAdapter = (ContactSyncFriendSuggestionListAdapter) companion.configure(new ContactSyncFriendSuggestionListAdapter(recyclerView));
        this.friendSuggestionsAdapter = contactSyncFriendSuggestionListAdapter;
        if (contactSyncFriendSuggestionListAdapter != null) {
            contactSyncFriendSuggestionListAdapter.setOnClickFriendSuggestion(new WidgetContactSync$onViewBound$2(this));
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetContactSync.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSync$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetContactSync.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSync$onViewBoundOrOnResume$2(this));
    }
}
