package com.discord.widgets.contact_sync;

import com.discord.analytics.generated.events.impression.TrackImpressionContactSyncInputName;
import com.discord.analytics.generated.events.impression.TrackImpressionContactSyncStart;
import com.discord.analytics.generated.events.impression.TrackImpressionContactSyncSuggestions;
import com.discord.analytics.generated.events.impression.TrackImpressionUserAddPhone;
import com.discord.analytics.generated.traits.TrackImpressionMetadata;
import com.discord.api.science.AnalyticsSchema;
import com.discord.widgets.contact_sync.WidgetContactSyncViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetContactSync.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/science/AnalyticsSchema;", "invoke", "()Lcom/discord/api/science/AnalyticsSchema;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetContactSync$loggingConfig$1 extends o implements Function0<AnalyticsSchema> {
    public final /* synthetic */ WidgetContactSync this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetContactSync$loggingConfig$1(WidgetContactSync widgetContactSync) {
        super(0);
        this.this$0 = widgetContactSync;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AnalyticsSchema invoke() {
        int i;
        TrackImpressionMetadata trackImpressionMetadata;
        TrackImpressionMetadata trackImpressionMetadata2;
        TrackImpressionMetadata trackImpressionMetadata3;
        TrackImpressionMetadata trackImpressionMetadata4;
        TrackImpressionMetadata trackImpressionMetadata5;
        i = this.this$0.displayedChildIndex;
        if (i == WidgetContactSyncViewModel.Views.VIEW_LANDING.ordinal()) {
            TrackImpressionContactSyncStart trackImpressionContactSyncStart = new TrackImpressionContactSyncStart();
            trackImpressionMetadata5 = this.this$0.contactSyncFlowMetadata;
            trackImpressionContactSyncStart.c(trackImpressionMetadata5);
            return trackImpressionContactSyncStart;
        } else if (i == WidgetContactSyncViewModel.Views.VIEW_ADD_PHONE.ordinal()) {
            TrackImpressionUserAddPhone trackImpressionUserAddPhone = new TrackImpressionUserAddPhone();
            trackImpressionMetadata4 = this.this$0.contactSyncFlowMetadata;
            trackImpressionUserAddPhone.c(trackImpressionMetadata4);
            return trackImpressionUserAddPhone;
        } else if (i == WidgetContactSyncViewModel.Views.VIEW_NAME_INPUT.ordinal()) {
            TrackImpressionContactSyncInputName trackImpressionContactSyncInputName = new TrackImpressionContactSyncInputName();
            trackImpressionMetadata3 = this.this$0.contactSyncFlowMetadata;
            trackImpressionContactSyncInputName.c(trackImpressionMetadata3);
            return trackImpressionContactSyncInputName;
        } else if (i == WidgetContactSyncViewModel.Views.VIEW_SUGGESTIONS.ordinal()) {
            TrackImpressionContactSyncSuggestions trackImpressionContactSyncSuggestions = new TrackImpressionContactSyncSuggestions();
            trackImpressionMetadata2 = this.this$0.contactSyncFlowMetadata;
            trackImpressionContactSyncSuggestions.c(trackImpressionMetadata2);
            return trackImpressionContactSyncSuggestions;
        } else if (i != WidgetContactSyncViewModel.Views.VIEW_SUGGESTIONS_EMPTY.ordinal()) {
            return null;
        } else {
            TrackImpressionContactSyncSuggestions trackImpressionContactSyncSuggestions2 = new TrackImpressionContactSyncSuggestions();
            trackImpressionMetadata = this.this$0.contactSyncFlowMetadata;
            trackImpressionContactSyncSuggestions2.c(trackImpressionMetadata);
            return trackImpressionContactSyncSuggestions2;
        }
    }
}
