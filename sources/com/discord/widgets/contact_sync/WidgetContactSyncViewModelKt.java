package com.discord.widgets.contact_sync;

import com.discord.widgets.contact_sync.WidgetContactSyncViewModel;
import kotlin.Metadata;
/* compiled from: WidgetContactSyncViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002\"\u0016\u0010\u0003\u001a\u00020\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0002¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "TOOLBAR_CONFIG_ONBOARDING", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "TOOLBAR_CONFIG_DEFAULT", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetContactSyncViewModelKt {
    private static final WidgetContactSyncViewModel.ToolbarConfig TOOLBAR_CONFIG_DEFAULT = new WidgetContactSyncViewModel.ToolbarConfig(true, false);
    private static final WidgetContactSyncViewModel.ToolbarConfig TOOLBAR_CONFIG_ONBOARDING = new WidgetContactSyncViewModel.ToolbarConfig(false, true);
}
