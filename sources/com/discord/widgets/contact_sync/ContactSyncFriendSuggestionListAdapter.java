package com.discord.widgets.contact_sync;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.friendsuggestions.FriendSuggestion;
import com.discord.api.friendsuggestions.FriendSuggestionReason;
import com.discord.api.user.User;
import com.discord.databinding.ViewSelectableFriendSuggestionBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.nullserializable.NullSerializable;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.contact_sync.ContactSyncFriendSuggestionListAdapter;
import com.discord.widgets.contact_sync.WidgetContactSyncViewModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.checkbox.MaterialCheckBox;
import d0.g0.s;
import d0.g0.t;
import d0.z.d.m;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: ContactSyncFriendSuggestionListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB\u000f\u0012\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0018\u0010\u0019J)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR4\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/contact_sync/ContactSyncFriendSuggestionListAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "spacing", "I", "Lkotlin/Function2;", "", "", "", "onClickFriendSuggestion", "Lkotlin/jvm/functions/Function2;", "getOnClickFriendSuggestion", "()Lkotlin/jvm/functions/Function2;", "setOnClickFriendSuggestion", "(Lkotlin/jvm/functions/Function2;)V", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "ItemFriendSuggestion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ContactSyncFriendSuggestionListAdapter extends MGRecyclerAdapterSimple<WidgetContactSyncViewModel.Item> {
    private Function2<? super Long, ? super Boolean, Unit> onClickFriendSuggestion = ContactSyncFriendSuggestionListAdapter$onClickFriendSuggestion$1.INSTANCE;
    private final int spacing;

    /* compiled from: ContactSyncFriendSuggestionListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0015¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/contact_sync/ContactSyncFriendSuggestionListAdapter$ItemFriendSuggestion;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/contact_sync/ContactSyncFriendSuggestionListAdapter;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item;)V", "Lcom/discord/databinding/ViewSelectableFriendSuggestionBinding;", "binding", "Lcom/discord/databinding/ViewSelectableFriendSuggestionBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/contact_sync/ContactSyncFriendSuggestionListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemFriendSuggestion extends MGRecyclerViewHolder<ContactSyncFriendSuggestionListAdapter, WidgetContactSyncViewModel.Item> {
        private final ViewSelectableFriendSuggestionBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemFriendSuggestion(ContactSyncFriendSuggestionListAdapter contactSyncFriendSuggestionListAdapter) {
            super((int) R.layout.view_selectable_friend_suggestion, contactSyncFriendSuggestionListAdapter);
            m.checkNotNullParameter(contactSyncFriendSuggestionListAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.friend_suggestion_avatar;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.friend_suggestion_avatar);
            if (simpleDraweeView != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                i = R.id.friend_suggestion_discriminator;
                TextView textView = (TextView) view.findViewById(R.id.friend_suggestion_discriminator);
                if (textView != null) {
                    i = R.id.friend_suggestion_name;
                    TextView textView2 = (TextView) view.findViewById(R.id.friend_suggestion_name);
                    if (textView2 != null) {
                        i = R.id.friend_suggestion_nickname;
                        TextView textView3 = (TextView) view.findViewById(R.id.friend_suggestion_nickname);
                        if (textView3 != null) {
                            i = R.id.friend_suggestion_selected;
                            MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.friend_suggestion_selected);
                            if (materialCheckBox != null) {
                                ViewSelectableFriendSuggestionBinding viewSelectableFriendSuggestionBinding = new ViewSelectableFriendSuggestionBinding(constraintLayout, simpleDraweeView, constraintLayout, textView, textView2, textView3, materialCheckBox);
                                m.checkNotNullExpressionValue(viewSelectableFriendSuggestionBinding, "ViewSelectableFriendSugg…ionBinding.bind(itemView)");
                                this.binding = viewSelectableFriendSuggestionBinding;
                                return;
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ ContactSyncFriendSuggestionListAdapter access$getAdapter$p(ItemFriendSuggestion itemFriendSuggestion) {
            return (ContactSyncFriendSuggestionListAdapter) itemFriendSuggestion.adapter;
        }

        @SuppressLint({"SetTextI18n"})
        public void onConfigure(int i, WidgetContactSyncViewModel.Item item) {
            String str;
            Object obj;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            WidgetContactSyncViewModel.Item.FriendSuggestionItem friendSuggestionItem = (WidgetContactSyncViewModel.Item.FriendSuggestionItem) item;
            FriendSuggestion suggestion = friendSuggestionItem.getSuggestion();
            User b2 = suggestion.b();
            ConstraintLayout constraintLayout = this.binding.c;
            m.checkNotNullExpressionValue(constraintLayout, "binding.friendSuggestionContainer");
            int paddingLeft = constraintLayout.getPaddingLeft();
            ConstraintLayout constraintLayout2 = this.binding.c;
            m.checkNotNullExpressionValue(constraintLayout2, "binding.friendSuggestionContainer");
            int i2 = 0;
            constraintLayout.setPadding(paddingLeft, 0, constraintLayout2.getPaddingRight(), ((ContactSyncFriendSuggestionListAdapter) this.adapter).spacing);
            TextView textView = this.binding.e;
            m.checkNotNullExpressionValue(textView, "binding.friendSuggestionName");
            textView.setText(b2.r());
            TextView textView2 = this.binding.d;
            m.checkNotNullExpressionValue(textView2, "binding.friendSuggestionDiscriminator");
            textView2.setText(MentionUtilsKt.CHANNELS_CHAR + b2.f());
            Iterator<T> it = suggestion.a().iterator();
            while (true) {
                str = null;
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (m.areEqual(((FriendSuggestionReason) obj).b(), "contacts")) {
                    break;
                }
            }
            FriendSuggestionReason friendSuggestionReason = (FriendSuggestionReason) obj;
            String a = friendSuggestionReason != null ? friendSuggestionReason.a() : null;
            if (a == null) {
                TextView textView3 = this.binding.f;
                m.checkNotNullExpressionValue(textView3, "binding.friendSuggestionNickname");
                textView3.setVisibility(8);
            } else {
                TextView textView4 = this.binding.f;
                m.checkNotNullExpressionValue(textView4, "binding.friendSuggestionNickname");
                textView4.setText(a);
                TextView textView5 = this.binding.f;
                m.checkNotNullExpressionValue(textView5, "binding.friendSuggestionNickname");
                if (!(!t.isBlank(a))) {
                    i2 = 8;
                }
                textView5.setVisibility(i2);
            }
            MaterialCheckBox materialCheckBox = this.binding.g;
            m.checkNotNullExpressionValue(materialCheckBox, "binding.friendSuggestionSelected");
            materialCheckBox.setChecked(friendSuggestionItem.getSelected());
            SimpleDraweeView simpleDraweeView = this.binding.f2191b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.friendSuggestionAvatar");
            Long valueOf = Long.valueOf(b2.i());
            NullSerializable<String> a2 = b2.a();
            if (a2 != null) {
                str = a2.a();
            }
            IconUtils.setIcon$default(simpleDraweeView, IconUtils.getForUser$default(valueOf, str, s.toIntOrNull(b2.f()), false, null, 16, null), (int) R.dimen.avatar_size_standard, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
            final long i3 = suggestion.b().i();
            this.binding.g.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: com.discord.widgets.contact_sync.ContactSyncFriendSuggestionListAdapter$ItemFriendSuggestion$onConfigure$1
                @Override // android.widget.CompoundButton.OnCheckedChangeListener
                public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
                    ContactSyncFriendSuggestionListAdapter.ItemFriendSuggestion.access$getAdapter$p(ContactSyncFriendSuggestionListAdapter.ItemFriendSuggestion.this).getOnClickFriendSuggestion().invoke(Long.valueOf(i3), Boolean.valueOf(z2));
                }
            });
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContactSyncFriendSuggestionListAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        Context context = recyclerView.getContext();
        m.checkNotNullExpressionValue(context, "recycler.context");
        this.spacing = context.getResources().getDimensionPixelSize(R.dimen.suggestion_spacing);
    }

    public final Function2<Long, Boolean, Unit> getOnClickFriendSuggestion() {
        return this.onClickFriendSuggestion;
    }

    public final void setOnClickFriendSuggestion(Function2<? super Long, ? super Boolean, Unit> function2) {
        m.checkNotNullParameter(function2, "<set-?>");
        this.onClickFriendSuggestion = function2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, WidgetContactSyncViewModel.Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        return new ItemFriendSuggestion(this);
    }
}
