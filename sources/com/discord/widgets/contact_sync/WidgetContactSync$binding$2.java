package com.discord.widgets.contact_sync;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.h0;
import b.a.i.i0;
import b.a.i.j0;
import b.a.i.k0;
import b.a.i.l0;
import b.a.i.m0;
import b.a.i.n0;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetContactSyncBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.CodeVerificationView;
import com.discord.views.LoadingButton;
import com.discord.views.phone.PhoneOrEmailInputView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetContactSync.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetContactSyncBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetContactSyncBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetContactSync$binding$2 extends k implements Function1<View, WidgetContactSyncBinding> {
    public static final WidgetContactSync$binding$2 INSTANCE = new WidgetContactSync$binding$2();

    public WidgetContactSync$binding$2() {
        super(1, WidgetContactSyncBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetContactSyncBinding;", 0);
    }

    public final WidgetContactSyncBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.contact_sync_add_phone;
        View findViewById = view.findViewById(R.id.contact_sync_add_phone);
        if (findViewById != null) {
            int i2 = R.id.contact_sync_add_phone_input;
            PhoneOrEmailInputView phoneOrEmailInputView = (PhoneOrEmailInputView) findViewById.findViewById(R.id.contact_sync_add_phone_input);
            if (phoneOrEmailInputView != null) {
                i2 = R.id.contact_sync_add_phone_next;
                MaterialButton materialButton = (MaterialButton) findViewById.findViewById(R.id.contact_sync_add_phone_next);
                if (materialButton != null) {
                    i2 = R.id.contact_sync_add_phone_subtitle;
                    TextView textView = (TextView) findViewById.findViewById(R.id.contact_sync_add_phone_subtitle);
                    if (textView != null) {
                        i2 = R.id.contact_sync_add_phone_title;
                        TextView textView2 = (TextView) findViewById.findViewById(R.id.contact_sync_add_phone_title);
                        if (textView2 != null) {
                            h0 h0Var = new h0((ConstraintLayout) findViewById, phoneOrEmailInputView, materialButton, textView, textView2);
                            i = R.id.contact_sync_friend_suggestions;
                            View findViewById2 = view.findViewById(R.id.contact_sync_friend_suggestions);
                            if (findViewById2 != null) {
                                int i3 = R.id.contact_sync_suggestions_list_recycler;
                                RecyclerView recyclerView = (RecyclerView) findViewById2.findViewById(R.id.contact_sync_suggestions_list_recycler);
                                if (recyclerView != null) {
                                    i3 = R.id.contact_sync_suggestions_submit_button;
                                    LoadingButton loadingButton = (LoadingButton) findViewById2.findViewById(R.id.contact_sync_suggestions_submit_button);
                                    if (loadingButton != null) {
                                        i3 = R.id.contact_sync_suggestions_subtitle;
                                        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) findViewById2.findViewById(R.id.contact_sync_suggestions_subtitle);
                                        if (linkifiedTextView != null) {
                                            i3 = R.id.contact_sync_suggestions_title;
                                            TextView textView3 = (TextView) findViewById2.findViewById(R.id.contact_sync_suggestions_title);
                                            if (textView3 != null) {
                                                k0 k0Var = new k0((ConstraintLayout) findViewById2, recyclerView, loadingButton, linkifiedTextView, textView3);
                                                i = R.id.contact_sync_friend_suggestions_empty;
                                                View findViewById3 = view.findViewById(R.id.contact_sync_friend_suggestions_empty);
                                                if (findViewById3 != null) {
                                                    int i4 = R.id.contact_sync_suggestions_empty_subtitle;
                                                    TextView textView4 = (TextView) findViewById3.findViewById(R.id.contact_sync_suggestions_empty_subtitle);
                                                    if (textView4 != null) {
                                                        i4 = R.id.contact_sync_suggestions_empty_title;
                                                        TextView textView5 = (TextView) findViewById3.findViewById(R.id.contact_sync_suggestions_empty_title);
                                                        if (textView5 != null) {
                                                            i4 = R.id.contact_sync_suggestions_invite_button;
                                                            MaterialButton materialButton2 = (MaterialButton) findViewById3.findViewById(R.id.contact_sync_suggestions_invite_button);
                                                            if (materialButton2 != null) {
                                                                i4 = R.id.contact_sync_suggestions_skip_button;
                                                                MaterialButton materialButton3 = (MaterialButton) findViewById3.findViewById(R.id.contact_sync_suggestions_skip_button);
                                                                if (materialButton3 != null) {
                                                                    l0 l0Var = new l0((ConstraintLayout) findViewById3, textView4, textView5, materialButton2, materialButton3);
                                                                    i = R.id.contact_sync_landing;
                                                                    View findViewById4 = view.findViewById(R.id.contact_sync_landing);
                                                                    if (findViewById4 != null) {
                                                                        int i5 = R.id.contact_sync_landing_cta;
                                                                        LinearLayout linearLayout = (LinearLayout) findViewById4.findViewById(R.id.contact_sync_landing_cta);
                                                                        if (linearLayout != null) {
                                                                            i5 = R.id.contact_sync_landing_needs_permissions;
                                                                            TextView textView6 = (TextView) findViewById4.findViewById(R.id.contact_sync_landing_needs_permissions);
                                                                            if (textView6 != null) {
                                                                                i5 = R.id.contact_sync_landing_next_button;
                                                                                LoadingButton loadingButton2 = (LoadingButton) findViewById4.findViewById(R.id.contact_sync_landing_next_button);
                                                                                if (loadingButton2 != null) {
                                                                                    i5 = R.id.contact_sync_landing_permissions_divider;
                                                                                    View findViewById5 = findViewById4.findViewById(R.id.contact_sync_landing_permissions_divider);
                                                                                    if (findViewById5 != null) {
                                                                                        i5 = R.id.contact_sync_landing_subtitle;
                                                                                        TextView textView7 = (TextView) findViewById4.findViewById(R.id.contact_sync_landing_subtitle);
                                                                                        if (textView7 != null) {
                                                                                            i5 = R.id.contact_sync_landing_title;
                                                                                            TextView textView8 = (TextView) findViewById4.findViewById(R.id.contact_sync_landing_title);
                                                                                            if (textView8 != null) {
                                                                                                i5 = R.id.contact_sync_landing_toggle_info;
                                                                                                View findViewById6 = findViewById4.findViewById(R.id.contact_sync_landing_toggle_info);
                                                                                                if (findViewById6 != null) {
                                                                                                    i0 i0Var = new i0((ConstraintLayout) findViewById4, linearLayout, textView6, loadingButton2, findViewById5, textView7, textView8, m0.a(findViewById6));
                                                                                                    View findViewById7 = view.findViewById(R.id.contact_sync_name);
                                                                                                    if (findViewById7 != null) {
                                                                                                        int i6 = R.id.contact_sync_name_input;
                                                                                                        TextInputEditText textInputEditText = (TextInputEditText) findViewById7.findViewById(R.id.contact_sync_name_input);
                                                                                                        if (textInputEditText != null) {
                                                                                                            i6 = R.id.contact_sync_name_input_wrap;
                                                                                                            TextInputLayout textInputLayout = (TextInputLayout) findViewById7.findViewById(R.id.contact_sync_name_input_wrap);
                                                                                                            if (textInputLayout != null) {
                                                                                                                i6 = R.id.contact_sync_name_next_button;
                                                                                                                LoadingButton loadingButton3 = (LoadingButton) findViewById7.findViewById(R.id.contact_sync_name_next_button);
                                                                                                                if (loadingButton3 != null) {
                                                                                                                    i6 = R.id.contact_sync_name_prefill_hint;
                                                                                                                    TextView textView9 = (TextView) findViewById7.findViewById(R.id.contact_sync_name_prefill_hint);
                                                                                                                    if (textView9 != null) {
                                                                                                                        i6 = R.id.contact_sync_name_subtitle;
                                                                                                                        TextView textView10 = (TextView) findViewById7.findViewById(R.id.contact_sync_name_subtitle);
                                                                                                                        if (textView10 != null) {
                                                                                                                            i6 = R.id.contact_sync_name_title;
                                                                                                                            TextView textView11 = (TextView) findViewById7.findViewById(R.id.contact_sync_name_title);
                                                                                                                            if (textView11 != null) {
                                                                                                                                j0 j0Var = new j0((ConstraintLayout) findViewById7, textInputEditText, textInputLayout, loadingButton3, textView9, textView10, textView11);
                                                                                                                                View findViewById8 = view.findViewById(R.id.contact_sync_verify_phone);
                                                                                                                                if (findViewById8 != null) {
                                                                                                                                    int i7 = R.id.contact_sync_verify_phone_code;
                                                                                                                                    CodeVerificationView codeVerificationView = (CodeVerificationView) findViewById8.findViewById(R.id.contact_sync_verify_phone_code);
                                                                                                                                    if (codeVerificationView != null) {
                                                                                                                                        i7 = R.id.contact_sync_verify_phone_subtitle;
                                                                                                                                        TextView textView12 = (TextView) findViewById8.findViewById(R.id.contact_sync_verify_phone_subtitle);
                                                                                                                                        if (textView12 != null) {
                                                                                                                                            i7 = R.id.contact_sync_verify_phone_title;
                                                                                                                                            TextView textView13 = (TextView) findViewById8.findViewById(R.id.contact_sync_verify_phone_title);
                                                                                                                                            if (textView13 != null) {
                                                                                                                                                n0 n0Var = new n0((ConstraintLayout) findViewById8, codeVerificationView, textView12, textView13);
                                                                                                                                                AppViewFlipper appViewFlipper = (AppViewFlipper) view.findViewById(R.id.contact_sync_view_flipper);
                                                                                                                                                if (appViewFlipper != null) {
                                                                                                                                                    return new WidgetContactSyncBinding((CoordinatorLayout) view, h0Var, k0Var, l0Var, i0Var, j0Var, n0Var, appViewFlipper);
                                                                                                                                                }
                                                                                                                                                i = R.id.contact_sync_view_flipper;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById8.getResources().getResourceName(i7)));
                                                                                                                                }
                                                                                                                                i = R.id.contact_sync_verify_phone;
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById7.getResources().getResourceName(i6)));
                                                                                                    }
                                                                                                    i = R.id.contact_sync_name;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById4.getResources().getResourceName(i5)));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(i4)));
                                                }
                                            }
                                        }
                                    }
                                }
                                throw new NullPointerException("Missing required view with ID: ".concat(findViewById2.getResources().getResourceName(i3)));
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(i2)));
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
