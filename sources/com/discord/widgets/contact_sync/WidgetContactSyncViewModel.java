package com.discord.widgets.contact_sync;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.api.friendsuggestions.AllowedInSuggestionsType;
import com.discord.api.friendsuggestions.BulkAddFriendsResponse;
import com.discord.api.friendsuggestions.BulkFriendSuggestions;
import com.discord.api.friendsuggestions.FriendSuggestion;
import com.discord.api.friendsuggestions.FriendSuggestionReason;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.phone.PhoneCountryCode;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StorePhone;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserConnections;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.error.Error;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.contact_sync.WidgetContactSyncViewModel;
import com.discord.widgets.user.phone.WidgetUserPhoneManage;
import d0.g0.t;
import d0.g0.w;
import d0.t.g0;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: WidgetContactSyncViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u0000 [2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0007[\\]^_`aBe\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\b\b\u0002\u0010,\u001a\u00020+\u0012\b\b\u0002\u0010-\u001a\u00020+\u0012\b\b\u0002\u0010Q\u001a\u00020P\u0012\u000e\b\u0002\u0010X\u001a\b\u0012\u0004\u0012\u00020\u00180\"\u0012\b\b\u0002\u0010T\u001a\u00020S\u0012\b\b\u0002\u0010K\u001a\u00020+\u0012\u0012\u0010N\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030M¢\u0006\u0004\bY\u0010ZJ\u000f\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\nH\u0003¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\u000e\u0010\tJ\u0017\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u000fH\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\u0013\u0010\tJ\u000f\u0010\u0014\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0014\u0010\u0005J\u000f\u0010\u0015\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0015\u0010\u0005J\u000f\u0010\u0016\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0016\u0010\u0005J\u0017\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\u0017\u0010\tJ\u0017\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u001c\u0010\u0005J\u0017\u0010 \u001a\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001dH\u0002¢\u0006\u0004\b \u0010!J\u0013\u0010$\u001a\b\u0012\u0004\u0012\u00020#0\"¢\u0006\u0004\b$\u0010%J\u000f\u0010&\u001a\u00020\u0003H\u0007¢\u0006\u0004\b&\u0010\u0005J\u000f\u0010'\u001a\u00020\u0003H\u0007¢\u0006\u0004\b'\u0010\u0005J\u000f\u0010(\u001a\u00020\u0003H\u0007¢\u0006\u0004\b(\u0010\u0005J\u000f\u0010)\u001a\u00020\u0003H\u0007¢\u0006\u0004\b)\u0010\u0005J\u000f\u0010*\u001a\u00020\u0003H\u0007¢\u0006\u0004\b*\u0010\u0005J\u001f\u0010.\u001a\u00020\u00032\u0006\u0010,\u001a\u00020+2\u0006\u0010-\u001a\u00020+H\u0007¢\u0006\u0004\b.\u0010/J#\u00104\u001a\u00020\u00032\n\u00102\u001a\u000600j\u0002`12\u0006\u00103\u001a\u00020+H\u0007¢\u0006\u0004\b4\u00105J\u0017\u00108\u001a\u00020\u00032\u0006\u00107\u001a\u000206H\u0007¢\u0006\u0004\b8\u00109J\u001d\u0010<\u001a\u00020\u00032\f\u0010;\u001a\b\u0012\u0004\u0012\u0002060:H\u0007¢\u0006\u0004\b<\u0010=J\u000f\u0010>\u001a\u00020\u0003H\u0007¢\u0006\u0004\b>\u0010\u0005J\u000f\u0010?\u001a\u00020\u0003H\u0007¢\u0006\u0004\b?\u0010\u0005J!\u0010B\u001a\u00020\u00032\u0006\u0010@\u001a\u0002062\b\u0010A\u001a\u0004\u0018\u000106H\u0007¢\u0006\u0004\bB\u0010CJ\u0017\u0010E\u001a\u00020\u00032\u0006\u0010D\u001a\u000206H\u0007¢\u0006\u0004\bE\u00109J\r\u0010F\u001a\u00020\u0003¢\u0006\u0004\bF\u0010\u0005R:\u0010I\u001a&\u0012\f\u0012\n H*\u0004\u0018\u00010#0# H*\u0012\u0012\f\u0012\n H*\u0004\u0018\u00010#0#\u0018\u00010G0G8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR\u0016\u0010K\u001a\u00020+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bK\u0010LR\"\u0010N\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030M8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u0019\u0010T\u001a\u00020S8\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010U\u001a\u0004\bV\u0010W¨\u0006b"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ViewState;", "", "handleContactsEnabled", "()V", "Lcom/discord/utilities/error/Error;", "error", "handleContactsEnableError", "(Lcom/discord/utilities/error/Error;)V", "Lcom/discord/api/friendsuggestions/BulkFriendSuggestions;", "suggestions", "handleFriendSuggestions", "(Lcom/discord/api/friendsuggestions/BulkFriendSuggestions;)V", "handleUploadError", "Lcom/discord/api/friendsuggestions/BulkAddFriendsResponse;", "result", "handleFriendsAdded", "(Lcom/discord/api/friendsuggestions/BulkAddFriendsResponse;)V", "handleFriendsAddedError", "handlePhoneSubmitted", "handlePhoneSubmittedError", "handlePhoneVerified", "handlePhoneVerifiedError", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$StoreState;)V", "handleComplete", "Lcom/discord/widgets/contact_sync/ContactSyncMode;", "mode", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "getLandingToolbarConfig", "(Lcom/discord/widgets/contact_sync/ContactSyncMode;)Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "Lrx/Observable;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", "observeEvents", "()Lrx/Observable;", "onLandingNext", "requestingPermissions", "onPermissionsGranted", "onPermissionsBecameAvailable", "onPermissionsDenied", "", "allowPhone", "allowEmail", "onPermissionsToggle", "(ZZ)V", "", "Lcom/discord/primitives/UserId;", "userId", "isSelected", "handleToggleFriendSuggestionSelected", "(JZ)V", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "onNameSubmitted", "(Ljava/lang/String;)V", "", "contactNumbers", "onContactsFetched", "(Ljava/util/Set;)V", "onBulkAddFriends", "dismissUpsell", "phoneNumber", "captchaKey", "onPhoneNumberSubmitted", "(Ljava/lang/String;Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_CODE, "onVerifyPhone", "skip", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventsSubject", "Lrx/subjects/PublishSubject;", "initialized", "Z", "Lkotlin/Function1;", "captchaLauncher", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/widgets/contact_sync/ContactSyncFlowAnalytics;", "tracker", "Lcom/discord/widgets/contact_sync/ContactSyncFlowAnalytics;", "getTracker", "()Lcom/discord/widgets/contact_sync/ContactSyncFlowAnalytics;", "storeObservable", HookHelper.constructorName, "(Lcom/discord/widgets/contact_sync/ContactSyncMode;ZZLcom/discord/utilities/rest/RestAPI;Lrx/Observable;Lcom/discord/widgets/contact_sync/ContactSyncFlowAnalytics;ZLkotlin/jvm/functions/Function1;)V", "Companion", "Event", "Item", "StoreState", "ToolbarConfig", "ViewState", "Views", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetContactSyncViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final Function1<Error, Unit> captchaLauncher;
    private final PublishSubject<Event> eventsSubject;
    private boolean initialized;
    private final RestAPI restAPI;
    private final ContactSyncFlowAnalytics tracker;

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.contact_sync.WidgetContactSyncViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetContactSyncViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$StoreState;", "observeStores", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<StoreState> observeStores() {
            StoreStream.Companion companion = StoreStream.Companion;
            final StoreUser users = companion.getUsers();
            final StorePhone phone = companion.getPhone();
            final StoreUserConnections userConnections = companion.getUserConnections();
            Observable<StoreState> F = ObservableExtensionsKt.leadingEdgeThrottle(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{users, phone, userConnections}, false, null, null, 14, null), 1L, TimeUnit.SECONDS).F(new b<Unit, StoreState>() { // from class: com.discord.widgets.contact_sync.WidgetContactSyncViewModel$Companion$observeStores$1
                public final WidgetContactSyncViewModel.StoreState call(Unit unit) {
                    ConnectedAccount connectedAccount;
                    MeUser me2 = StoreUser.this.getMe();
                    PhoneCountryCode countryCode = phone.getCountryCode();
                    StoreUserConnections.State connectedAccounts = userConnections.getConnectedAccounts();
                    ListIterator<ConnectedAccount> listIterator = connectedAccounts.listIterator(connectedAccounts.size());
                    while (true) {
                        if (!listIterator.hasPrevious()) {
                            connectedAccount = null;
                            break;
                        }
                        connectedAccount = listIterator.previous();
                        if (m.areEqual(connectedAccount.g(), "contacts")) {
                            break;
                        }
                    }
                    return new WidgetContactSyncViewModel.StoreState(me2.getPhoneNumber(), UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, me2, null, null, 3, null).toString(), countryCode, connectedAccount);
                }
            });
            m.checkNotNullExpressionValue(F, "ObservationDeckProvider\n…            )\n          }");
            return F;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\f\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000fB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", "", HookHelper.constructorName, "()V", "AddFriendsFailed", "AddFriendsFailedPartial", "Completed", "ContactsEnableFailed", "ContactsEnabled", "MaybeProceedFromLanding", "PermissionsNeeded", "PhoneInvalid", "RateLimited", "UploadFailed", "VerificationCodeInvalid", "VerificationFailed", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$MaybeProceedFromLanding;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$PermissionsNeeded;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$ContactsEnabled;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$Completed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$RateLimited;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$ContactsEnableFailed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$UploadFailed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$AddFriendsFailed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$AddFriendsFailedPartial;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$PhoneInvalid;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$VerificationCodeInvalid;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$VerificationFailed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$AddFriendsFailed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AddFriendsFailed extends Event {
            public static final AddFriendsFailed INSTANCE = new AddFriendsFailed();

            private AddFriendsFailed() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$AddFriendsFailedPartial;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AddFriendsFailedPartial extends Event {
            public static final AddFriendsFailedPartial INSTANCE = new AddFriendsFailedPartial();

            private AddFriendsFailedPartial() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$Completed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Completed extends Event {
            public static final Completed INSTANCE = new Completed();

            private Completed() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$ContactsEnableFailed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ContactsEnableFailed extends Event {
            public static final ContactsEnableFailed INSTANCE = new ContactsEnableFailed();

            private ContactsEnableFailed() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$ContactsEnabled;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ContactsEnabled extends Event {
            public static final ContactsEnabled INSTANCE = new ContactsEnabled();

            private ContactsEnabled() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$MaybeProceedFromLanding;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MaybeProceedFromLanding extends Event {
            public static final MaybeProceedFromLanding INSTANCE = new MaybeProceedFromLanding();

            private MaybeProceedFromLanding() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$PermissionsNeeded;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PermissionsNeeded extends Event {
            public static final PermissionsNeeded INSTANCE = new PermissionsNeeded();

            private PermissionsNeeded() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$PhoneInvalid;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PhoneInvalid extends Event {
            public static final PhoneInvalid INSTANCE = new PhoneInvalid();

            private PhoneInvalid() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$RateLimited;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RateLimited extends Event {
            public static final RateLimited INSTANCE = new RateLimited();

            private RateLimited() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$UploadFailed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UploadFailed extends Event {
            public static final UploadFailed INSTANCE = new UploadFailed();

            private UploadFailed() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$VerificationCodeInvalid;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class VerificationCodeInvalid extends Event {
            public static final VerificationCodeInvalid INSTANCE = new VerificationCodeInvalid();

            private VerificationCodeInvalid() {
                super(null);
            }
        }

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event$VerificationFailed;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class VerificationFailed extends Event {
            public static final VerificationFailed INSTANCE = new VerificationFailed();

            private VerificationFailed() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\tB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0001\n¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "type", "I", "getType", "()I", HookHelper.constructorName, "(I)V", "FriendSuggestionItem", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item$FriendSuggestionItem;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item implements MGRecyclerDataPayload {
        private final int type;

        /* compiled from: WidgetContactSyncViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001c\u0010\u0018\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u000eR\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item$FriendSuggestionItem;", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item;", "Lcom/discord/api/friendsuggestions/FriendSuggestion;", "component1", "()Lcom/discord/api/friendsuggestions/FriendSuggestion;", "", "component2", "()Z", "suggestion", "selected", "copy", "(Lcom/discord/api/friendsuggestions/FriendSuggestion;Z)Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item$FriendSuggestionItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/friendsuggestions/FriendSuggestion;", "getSuggestion", "key", "Ljava/lang/String;", "getKey", "Z", "getSelected", HookHelper.constructorName, "(Lcom/discord/api/friendsuggestions/FriendSuggestion;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FriendSuggestionItem extends Item {
            private final String key;
            private final boolean selected;
            private final FriendSuggestion suggestion;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public FriendSuggestionItem(FriendSuggestion friendSuggestion, boolean z2) {
                super(0, null);
                m.checkNotNullParameter(friendSuggestion, "suggestion");
                this.suggestion = friendSuggestion;
                this.selected = z2;
                this.key = String.valueOf(friendSuggestion.b().i());
            }

            public static /* synthetic */ FriendSuggestionItem copy$default(FriendSuggestionItem friendSuggestionItem, FriendSuggestion friendSuggestion, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    friendSuggestion = friendSuggestionItem.suggestion;
                }
                if ((i & 2) != 0) {
                    z2 = friendSuggestionItem.selected;
                }
                return friendSuggestionItem.copy(friendSuggestion, z2);
            }

            public final FriendSuggestion component1() {
                return this.suggestion;
            }

            public final boolean component2() {
                return this.selected;
            }

            public final FriendSuggestionItem copy(FriendSuggestion friendSuggestion, boolean z2) {
                m.checkNotNullParameter(friendSuggestion, "suggestion");
                return new FriendSuggestionItem(friendSuggestion, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof FriendSuggestionItem)) {
                    return false;
                }
                FriendSuggestionItem friendSuggestionItem = (FriendSuggestionItem) obj;
                return m.areEqual(this.suggestion, friendSuggestionItem.suggestion) && this.selected == friendSuggestionItem.selected;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final boolean getSelected() {
                return this.selected;
            }

            public final FriendSuggestion getSuggestion() {
                return this.suggestion;
            }

            public int hashCode() {
                FriendSuggestion friendSuggestion = this.suggestion;
                int hashCode = (friendSuggestion != null ? friendSuggestion.hashCode() : 0) * 31;
                boolean z2 = this.selected;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("FriendSuggestionItem(suggestion=");
                R.append(this.suggestion);
                R.append(", selected=");
                return a.M(R, this.selected, ")");
            }
        }

        private Item(int i) {
            this.type = i;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public /* synthetic */ Item(int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(i);
        }
    }

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0006\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b!\u0010\"J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ<\u0010\u0010\u001a\u00020\u00002\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00062\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\bR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u000b¨\u0006#"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$StoreState;", "", "", "component1", "()Ljava/lang/String;", "component2", "Lcom/discord/models/phone/PhoneCountryCode;", "component3", "()Lcom/discord/models/phone/PhoneCountryCode;", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component4", "()Lcom/discord/api/connectedaccounts/ConnectedAccount;", "userPhone", "username", "countryCode", "contactsConnection", "copy", "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/phone/PhoneCountryCode;Lcom/discord/api/connectedaccounts/ConnectedAccount;)Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$StoreState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getUsername", "getUserPhone", "Lcom/discord/models/phone/PhoneCountryCode;", "getCountryCode", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "getContactsConnection", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/phone/PhoneCountryCode;Lcom/discord/api/connectedaccounts/ConnectedAccount;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final ConnectedAccount contactsConnection;
        private final PhoneCountryCode countryCode;
        private final String userPhone;
        private final String username;

        public StoreState(String str, String str2, PhoneCountryCode phoneCountryCode, ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(str2, "username");
            m.checkNotNullParameter(phoneCountryCode, "countryCode");
            this.userPhone = str;
            this.username = str2;
            this.countryCode = phoneCountryCode;
            this.contactsConnection = connectedAccount;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, String str, String str2, PhoneCountryCode phoneCountryCode, ConnectedAccount connectedAccount, int i, Object obj) {
            if ((i & 1) != 0) {
                str = storeState.userPhone;
            }
            if ((i & 2) != 0) {
                str2 = storeState.username;
            }
            if ((i & 4) != 0) {
                phoneCountryCode = storeState.countryCode;
            }
            if ((i & 8) != 0) {
                connectedAccount = storeState.contactsConnection;
            }
            return storeState.copy(str, str2, phoneCountryCode, connectedAccount);
        }

        public final String component1() {
            return this.userPhone;
        }

        public final String component2() {
            return this.username;
        }

        public final PhoneCountryCode component3() {
            return this.countryCode;
        }

        public final ConnectedAccount component4() {
            return this.contactsConnection;
        }

        public final StoreState copy(String str, String str2, PhoneCountryCode phoneCountryCode, ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(str2, "username");
            m.checkNotNullParameter(phoneCountryCode, "countryCode");
            return new StoreState(str, str2, phoneCountryCode, connectedAccount);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.userPhone, storeState.userPhone) && m.areEqual(this.username, storeState.username) && m.areEqual(this.countryCode, storeState.countryCode) && m.areEqual(this.contactsConnection, storeState.contactsConnection);
        }

        public final ConnectedAccount getContactsConnection() {
            return this.contactsConnection;
        }

        public final PhoneCountryCode getCountryCode() {
            return this.countryCode;
        }

        public final String getUserPhone() {
            return this.userPhone;
        }

        public final String getUsername() {
            return this.username;
        }

        public int hashCode() {
            String str = this.userPhone;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.username;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            PhoneCountryCode phoneCountryCode = this.countryCode;
            int hashCode3 = (hashCode2 + (phoneCountryCode != null ? phoneCountryCode.hashCode() : 0)) * 31;
            ConnectedAccount connectedAccount = this.contactsConnection;
            if (connectedAccount != null) {
                i = connectedAccount.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(userPhone=");
            R.append(this.userPhone);
            R.append(", username=");
            R.append(this.username);
            R.append(", countryCode=");
            R.append(this.countryCode);
            R.append(", contactsConnection=");
            R.append(this.contactsConnection);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0011\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "", "", "component1", "()Z", "component2", "showBackButton", "showSkip", "copy", "(ZZ)Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowBackButton", "getShowSkip", HookHelper.constructorName, "(ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ToolbarConfig {
        private final boolean showBackButton;
        private final boolean showSkip;

        public ToolbarConfig(boolean z2, boolean z3) {
            this.showBackButton = z2;
            this.showSkip = z3;
        }

        public static /* synthetic */ ToolbarConfig copy$default(ToolbarConfig toolbarConfig, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = toolbarConfig.showBackButton;
            }
            if ((i & 2) != 0) {
                z3 = toolbarConfig.showSkip;
            }
            return toolbarConfig.copy(z2, z3);
        }

        public final boolean component1() {
            return this.showBackButton;
        }

        public final boolean component2() {
            return this.showSkip;
        }

        public final ToolbarConfig copy(boolean z2, boolean z3) {
            return new ToolbarConfig(z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ToolbarConfig)) {
                return false;
            }
            ToolbarConfig toolbarConfig = (ToolbarConfig) obj;
            return this.showBackButton == toolbarConfig.showBackButton && this.showSkip == toolbarConfig.showSkip;
        }

        public final boolean getShowBackButton() {
            return this.showBackButton;
        }

        public final boolean getShowSkip() {
            return this.showSkip;
        }

        public int hashCode() {
            boolean z2 = this.showBackButton;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.showSkip;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ToolbarConfig(showBackButton=");
            R.append(this.showBackButton);
            R.append(", showSkip=");
            return a.M(R, this.showSkip, ")");
        }
    }

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\u001f\b\u0086\b\u0018\u00002\u00020\u0001B\u009f\u0001\u0012\u0006\u0010%\u001a\u00020\u0002\u0012\u0006\u0010&\u001a\u00020\u0005\u0012\b\u0010'\u001a\u0004\u0018\u00010\b\u0012\b\u0010(\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010)\u001a\u00020\u000e\u0012\u0006\u0010*\u001a\u00020\b\u0012\b\u0010+\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010,\u001a\u00020\u0002\u0012\u0006\u0010-\u001a\u00020\u0002\u0012\u0006\u0010.\u001a\u00020\u0015\u0012\u0006\u0010/\u001a\u00020\u0002\u0012\u0006\u00100\u001a\u00020\u0002\u0012\b\u00101\u001a\u0004\u0018\u00010\b\u0012\f\u00102\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b\u0012\u0010\u00103\u001a\f\u0012\b\u0012\u00060\u001fj\u0002` 0\u001b\u0012\u0006\u00104\u001a\u00020\"¢\u0006\u0004\bU\u0010VJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0011\u0010\nJ\u0012\u0010\u0012\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\nJ\u0010\u0010\u0013\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0004J\u0010\u0010\u0016\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0004J\u0010\u0010\u0019\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0004J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u001a\u0010\nJ\u0016\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010!\u001a\f\u0012\b\u0012\u00060\u001fj\u0002` 0\u001bHÆ\u0003¢\u0006\u0004\b!\u0010\u001eJ\u0010\u0010#\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b#\u0010$JÈ\u0001\u00105\u001a\u00020\u00002\b\b\u0002\u0010%\u001a\u00020\u00022\b\b\u0002\u0010&\u001a\u00020\u00052\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010)\u001a\u00020\u000e2\b\b\u0002\u0010*\u001a\u00020\b2\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010,\u001a\u00020\u00022\b\b\u0002\u0010-\u001a\u00020\u00022\b\b\u0002\u0010.\u001a\u00020\u00152\b\b\u0002\u0010/\u001a\u00020\u00022\b\b\u0002\u00100\u001a\u00020\u00022\n\b\u0002\u00101\u001a\u0004\u0018\u00010\b2\u000e\b\u0002\u00102\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b2\u0012\b\u0002\u00103\u001a\f\u0012\b\u0012\u00060\u001fj\u0002` 0\u001b2\b\b\u0002\u00104\u001a\u00020\"HÆ\u0001¢\u0006\u0004\b5\u00106J\u0010\u00107\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b7\u0010\nJ\u0010\u00109\u001a\u000208HÖ\u0001¢\u0006\u0004\b9\u0010:J\u001a\u0010<\u001a\u00020\u00022\b\u0010;\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b<\u0010=R\u0019\u00100\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010>\u001a\u0004\b?\u0010\u0004R\u001b\u0010(\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010@\u001a\u0004\bA\u0010\rR\u0019\u0010-\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010>\u001a\u0004\bB\u0010\u0004R\u0019\u0010.\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010C\u001a\u0004\bD\u0010\u0017R\u0019\u00104\u001a\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010E\u001a\u0004\bF\u0010$R\u0019\u0010%\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010>\u001a\u0004\bG\u0010\u0004R#\u00103\u001a\f\u0012\b\u0012\u00060\u001fj\u0002` 0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010H\u001a\u0004\bI\u0010\u001eR\u001b\u0010'\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010J\u001a\u0004\bK\u0010\nR\u0019\u0010/\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010>\u001a\u0004\bL\u0010\u0004R\u001b\u00101\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010J\u001a\u0004\bM\u0010\nR\u001f\u00102\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010H\u001a\u0004\bN\u0010\u001eR\u0019\u0010&\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010O\u001a\u0004\bP\u0010\u0007R\u0019\u0010*\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010J\u001a\u0004\bQ\u0010\nR\u0019\u0010,\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010>\u001a\u0004\b,\u0010\u0004R\u001b\u0010+\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010J\u001a\u0004\bR\u0010\nR\u0019\u0010)\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010S\u001a\u0004\bT\u0010\u0010¨\u0006W"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ViewState;", "", "", "component1", "()Z", "Lcom/discord/widgets/contact_sync/ContactSyncMode;", "component2", "()Lcom/discord/widgets/contact_sync/ContactSyncMode;", "", "component3", "()Ljava/lang/String;", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component4", "()Lcom/discord/api/connectedaccounts/ConnectedAccount;", "Lcom/discord/models/phone/PhoneCountryCode;", "component5", "()Lcom/discord/models/phone/PhoneCountryCode;", "component6", "component7", "component8", "component9", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;", "component10", "()Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;", "component11", "component12", "component13", "", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Item;", "component14", "()Ljava/util/List;", "", "Lcom/discord/primitives/UserId;", "component15", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "component16", "()Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "landingNextEnabled", "mode", "phoneNumber", "existingConnection", "countryCode", "username", ModelAuditLogEntry.CHANGE_KEY_NAME, "isSubmitting", "permissionsDenied", "displayedChild", "allowPhone", "allowEmail", "bulkAddToken", "friendSuggestions", "selectedFriendIds", "toolbarConfig", "copy", "(ZLcom/discord/widgets/contact_sync/ContactSyncMode;Ljava/lang/String;Lcom/discord/api/connectedaccounts/ConnectedAccount;Lcom/discord/models/phone/PhoneCountryCode;Ljava/lang/String;Ljava/lang/String;ZZLcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;ZZLjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;)Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ViewState;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getAllowEmail", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "getExistingConnection", "getPermissionsDenied", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;", "getDisplayedChild", "Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;", "getToolbarConfig", "getLandingNextEnabled", "Ljava/util/List;", "getSelectedFriendIds", "Ljava/lang/String;", "getPhoneNumber", "getAllowPhone", "getBulkAddToken", "getFriendSuggestions", "Lcom/discord/widgets/contact_sync/ContactSyncMode;", "getMode", "getUsername", "getName", "Lcom/discord/models/phone/PhoneCountryCode;", "getCountryCode", HookHelper.constructorName, "(ZLcom/discord/widgets/contact_sync/ContactSyncMode;Ljava/lang/String;Lcom/discord/api/connectedaccounts/ConnectedAccount;Lcom/discord/models/phone/PhoneCountryCode;Ljava/lang/String;Ljava/lang/String;ZZLcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;ZZLjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$ToolbarConfig;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean allowEmail;
        private final boolean allowPhone;
        private final String bulkAddToken;
        private final PhoneCountryCode countryCode;
        private final Views displayedChild;
        private final ConnectedAccount existingConnection;
        private final List<Item> friendSuggestions;
        private final boolean isSubmitting;
        private final boolean landingNextEnabled;
        private final ContactSyncMode mode;
        private final String name;
        private final boolean permissionsDenied;
        private final String phoneNumber;
        private final List<Long> selectedFriendIds;
        private final ToolbarConfig toolbarConfig;
        private final String username;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(boolean z2, ContactSyncMode contactSyncMode, String str, ConnectedAccount connectedAccount, PhoneCountryCode phoneCountryCode, String str2, String str3, boolean z3, boolean z4, Views views, boolean z5, boolean z6, String str4, List<? extends Item> list, List<Long> list2, ToolbarConfig toolbarConfig) {
            m.checkNotNullParameter(contactSyncMode, "mode");
            m.checkNotNullParameter(phoneCountryCode, "countryCode");
            m.checkNotNullParameter(str2, "username");
            m.checkNotNullParameter(views, "displayedChild");
            m.checkNotNullParameter(list, "friendSuggestions");
            m.checkNotNullParameter(list2, "selectedFriendIds");
            m.checkNotNullParameter(toolbarConfig, "toolbarConfig");
            this.landingNextEnabled = z2;
            this.mode = contactSyncMode;
            this.phoneNumber = str;
            this.existingConnection = connectedAccount;
            this.countryCode = phoneCountryCode;
            this.username = str2;
            this.name = str3;
            this.isSubmitting = z3;
            this.permissionsDenied = z4;
            this.displayedChild = views;
            this.allowPhone = z5;
            this.allowEmail = z6;
            this.bulkAddToken = str4;
            this.friendSuggestions = list;
            this.selectedFriendIds = list2;
            this.toolbarConfig = toolbarConfig;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, boolean z2, ContactSyncMode contactSyncMode, String str, ConnectedAccount connectedAccount, PhoneCountryCode phoneCountryCode, String str2, String str3, boolean z3, boolean z4, Views views, boolean z5, boolean z6, String str4, List list, List list2, ToolbarConfig toolbarConfig, int i, Object obj) {
            return viewState.copy((i & 1) != 0 ? viewState.landingNextEnabled : z2, (i & 2) != 0 ? viewState.mode : contactSyncMode, (i & 4) != 0 ? viewState.phoneNumber : str, (i & 8) != 0 ? viewState.existingConnection : connectedAccount, (i & 16) != 0 ? viewState.countryCode : phoneCountryCode, (i & 32) != 0 ? viewState.username : str2, (i & 64) != 0 ? viewState.name : str3, (i & 128) != 0 ? viewState.isSubmitting : z3, (i & 256) != 0 ? viewState.permissionsDenied : z4, (i & 512) != 0 ? viewState.displayedChild : views, (i & 1024) != 0 ? viewState.allowPhone : z5, (i & 2048) != 0 ? viewState.allowEmail : z6, (i & 4096) != 0 ? viewState.bulkAddToken : str4, (i & 8192) != 0 ? viewState.friendSuggestions : list, (i & 16384) != 0 ? viewState.selectedFriendIds : list2, (i & 32768) != 0 ? viewState.toolbarConfig : toolbarConfig);
        }

        public final boolean component1() {
            return this.landingNextEnabled;
        }

        public final Views component10() {
            return this.displayedChild;
        }

        public final boolean component11() {
            return this.allowPhone;
        }

        public final boolean component12() {
            return this.allowEmail;
        }

        public final String component13() {
            return this.bulkAddToken;
        }

        public final List<Item> component14() {
            return this.friendSuggestions;
        }

        public final List<Long> component15() {
            return this.selectedFriendIds;
        }

        public final ToolbarConfig component16() {
            return this.toolbarConfig;
        }

        public final ContactSyncMode component2() {
            return this.mode;
        }

        public final String component3() {
            return this.phoneNumber;
        }

        public final ConnectedAccount component4() {
            return this.existingConnection;
        }

        public final PhoneCountryCode component5() {
            return this.countryCode;
        }

        public final String component6() {
            return this.username;
        }

        public final String component7() {
            return this.name;
        }

        public final boolean component8() {
            return this.isSubmitting;
        }

        public final boolean component9() {
            return this.permissionsDenied;
        }

        public final ViewState copy(boolean z2, ContactSyncMode contactSyncMode, String str, ConnectedAccount connectedAccount, PhoneCountryCode phoneCountryCode, String str2, String str3, boolean z3, boolean z4, Views views, boolean z5, boolean z6, String str4, List<? extends Item> list, List<Long> list2, ToolbarConfig toolbarConfig) {
            m.checkNotNullParameter(contactSyncMode, "mode");
            m.checkNotNullParameter(phoneCountryCode, "countryCode");
            m.checkNotNullParameter(str2, "username");
            m.checkNotNullParameter(views, "displayedChild");
            m.checkNotNullParameter(list, "friendSuggestions");
            m.checkNotNullParameter(list2, "selectedFriendIds");
            m.checkNotNullParameter(toolbarConfig, "toolbarConfig");
            return new ViewState(z2, contactSyncMode, str, connectedAccount, phoneCountryCode, str2, str3, z3, z4, views, z5, z6, str4, list, list2, toolbarConfig);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return this.landingNextEnabled == viewState.landingNextEnabled && m.areEqual(this.mode, viewState.mode) && m.areEqual(this.phoneNumber, viewState.phoneNumber) && m.areEqual(this.existingConnection, viewState.existingConnection) && m.areEqual(this.countryCode, viewState.countryCode) && m.areEqual(this.username, viewState.username) && m.areEqual(this.name, viewState.name) && this.isSubmitting == viewState.isSubmitting && this.permissionsDenied == viewState.permissionsDenied && m.areEqual(this.displayedChild, viewState.displayedChild) && this.allowPhone == viewState.allowPhone && this.allowEmail == viewState.allowEmail && m.areEqual(this.bulkAddToken, viewState.bulkAddToken) && m.areEqual(this.friendSuggestions, viewState.friendSuggestions) && m.areEqual(this.selectedFriendIds, viewState.selectedFriendIds) && m.areEqual(this.toolbarConfig, viewState.toolbarConfig);
        }

        public final boolean getAllowEmail() {
            return this.allowEmail;
        }

        public final boolean getAllowPhone() {
            return this.allowPhone;
        }

        public final String getBulkAddToken() {
            return this.bulkAddToken;
        }

        public final PhoneCountryCode getCountryCode() {
            return this.countryCode;
        }

        public final Views getDisplayedChild() {
            return this.displayedChild;
        }

        public final ConnectedAccount getExistingConnection() {
            return this.existingConnection;
        }

        public final List<Item> getFriendSuggestions() {
            return this.friendSuggestions;
        }

        public final boolean getLandingNextEnabled() {
            return this.landingNextEnabled;
        }

        public final ContactSyncMode getMode() {
            return this.mode;
        }

        public final String getName() {
            return this.name;
        }

        public final boolean getPermissionsDenied() {
            return this.permissionsDenied;
        }

        public final String getPhoneNumber() {
            return this.phoneNumber;
        }

        public final List<Long> getSelectedFriendIds() {
            return this.selectedFriendIds;
        }

        public final ToolbarConfig getToolbarConfig() {
            return this.toolbarConfig;
        }

        public final String getUsername() {
            return this.username;
        }

        public int hashCode() {
            boolean z2 = this.landingNextEnabled;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            ContactSyncMode contactSyncMode = this.mode;
            int i5 = 0;
            int hashCode = (i4 + (contactSyncMode != null ? contactSyncMode.hashCode() : 0)) * 31;
            String str = this.phoneNumber;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            ConnectedAccount connectedAccount = this.existingConnection;
            int hashCode3 = (hashCode2 + (connectedAccount != null ? connectedAccount.hashCode() : 0)) * 31;
            PhoneCountryCode phoneCountryCode = this.countryCode;
            int hashCode4 = (hashCode3 + (phoneCountryCode != null ? phoneCountryCode.hashCode() : 0)) * 31;
            String str2 = this.username;
            int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.name;
            int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
            boolean z3 = this.isSubmitting;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (hashCode6 + i6) * 31;
            boolean z4 = this.permissionsDenied;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (i8 + i9) * 31;
            Views views = this.displayedChild;
            int hashCode7 = (i11 + (views != null ? views.hashCode() : 0)) * 31;
            boolean z5 = this.allowPhone;
            if (z5) {
                z5 = true;
            }
            int i12 = z5 ? 1 : 0;
            int i13 = z5 ? 1 : 0;
            int i14 = (hashCode7 + i12) * 31;
            boolean z6 = this.allowEmail;
            if (!z6) {
                i = z6 ? 1 : 0;
            }
            int i15 = (i14 + i) * 31;
            String str4 = this.bulkAddToken;
            int hashCode8 = (i15 + (str4 != null ? str4.hashCode() : 0)) * 31;
            List<Item> list = this.friendSuggestions;
            int hashCode9 = (hashCode8 + (list != null ? list.hashCode() : 0)) * 31;
            List<Long> list2 = this.selectedFriendIds;
            int hashCode10 = (hashCode9 + (list2 != null ? list2.hashCode() : 0)) * 31;
            ToolbarConfig toolbarConfig = this.toolbarConfig;
            if (toolbarConfig != null) {
                i5 = toolbarConfig.hashCode();
            }
            return hashCode10 + i5;
        }

        public final boolean isSubmitting() {
            return this.isSubmitting;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(landingNextEnabled=");
            R.append(this.landingNextEnabled);
            R.append(", mode=");
            R.append(this.mode);
            R.append(", phoneNumber=");
            R.append(this.phoneNumber);
            R.append(", existingConnection=");
            R.append(this.existingConnection);
            R.append(", countryCode=");
            R.append(this.countryCode);
            R.append(", username=");
            R.append(this.username);
            R.append(", name=");
            R.append(this.name);
            R.append(", isSubmitting=");
            R.append(this.isSubmitting);
            R.append(", permissionsDenied=");
            R.append(this.permissionsDenied);
            R.append(", displayedChild=");
            R.append(this.displayedChild);
            R.append(", allowPhone=");
            R.append(this.allowPhone);
            R.append(", allowEmail=");
            R.append(this.allowEmail);
            R.append(", bulkAddToken=");
            R.append(this.bulkAddToken);
            R.append(", friendSuggestions=");
            R.append(this.friendSuggestions);
            R.append(", selectedFriendIds=");
            R.append(this.selectedFriendIds);
            R.append(", toolbarConfig=");
            R.append(this.toolbarConfig);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetContactSyncViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\u000e\n\u0002\b\r\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/contact_sync/WidgetContactSyncViewModel$Views;", "", "", "trackingStep", "Ljava/lang/String;", "getTrackingStep", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;)V", "VIEW_LANDING", "VIEW_ADD_PHONE", "VIEW_VERIFY_PHONE", "VIEW_NAME_INPUT", "VIEW_SUGGESTIONS", "VIEW_SUGGESTIONS_EMPTY", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum Views {
        VIEW_LANDING("Landing"),
        VIEW_ADD_PHONE("Add Phone Number"),
        VIEW_VERIFY_PHONE("Verify Phone Number"),
        VIEW_NAME_INPUT("Name Input"),
        VIEW_SUGGESTIONS("Suggestions Results"),
        VIEW_SUGGESTIONS_EMPTY("Suggestions Results");
        
        private final String trackingStep;

        Views(String str) {
            this.trackingStep = str;
        }

        public final String getTrackingStep() {
            return this.trackingStep;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            ContactSyncMode.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            ContactSyncMode contactSyncMode = ContactSyncMode.ONBOARDING;
            iArr[contactSyncMode.ordinal()] = 1;
            ContactSyncMode contactSyncMode2 = ContactSyncMode.DEFAULT;
            iArr[contactSyncMode2.ordinal()] = 2;
            Views.values();
            int[] iArr2 = new int[6];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[Views.VIEW_LANDING.ordinal()] = 1;
            iArr2[Views.VIEW_NAME_INPUT.ordinal()] = 2;
            iArr2[Views.VIEW_SUGGESTIONS.ordinal()] = 3;
            iArr2[Views.VIEW_SUGGESTIONS_EMPTY.ordinal()] = 4;
            iArr2[Views.VIEW_ADD_PHONE.ordinal()] = 5;
            iArr2[Views.VIEW_VERIFY_PHONE.ordinal()] = 6;
            ContactSyncMode.values();
            int[] iArr3 = new int[2];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[contactSyncMode.ordinal()] = 1;
            iArr3[contactSyncMode2.ordinal()] = 2;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetContactSyncViewModel(com.discord.widgets.contact_sync.ContactSyncMode r12, boolean r13, boolean r14, com.discord.utilities.rest.RestAPI r15, rx.Observable r16, com.discord.widgets.contact_sync.ContactSyncFlowAnalytics r17, boolean r18, kotlin.jvm.functions.Function1 r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r11 = this;
            r0 = r20 & 2
            r1 = 1
            if (r0 == 0) goto L7
            r4 = 1
            goto L8
        L7:
            r4 = r13
        L8:
            r0 = r20 & 4
            if (r0 == 0) goto Le
            r5 = 1
            goto Lf
        Le:
            r5 = r14
        Lf:
            r0 = r20 & 8
            if (r0 == 0) goto L1b
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            r6 = r0
            goto L1c
        L1b:
            r6 = r15
        L1c:
            r0 = r20 & 16
            if (r0 == 0) goto L28
            com.discord.widgets.contact_sync.WidgetContactSyncViewModel$Companion r0 = com.discord.widgets.contact_sync.WidgetContactSyncViewModel.Companion
            rx.Observable r0 = r0.observeStores()
            r7 = r0
            goto L2a
        L28:
            r7 = r16
        L2a:
            r0 = r20 & 32
            r2 = 0
            if (r0 == 0) goto L40
            com.discord.widgets.contact_sync.ContactSyncFlowAnalytics r0 = new com.discord.widgets.contact_sync.ContactSyncFlowAnalytics
            com.discord.widgets.contact_sync.ContactSyncMode r3 = com.discord.widgets.contact_sync.ContactSyncMode.ONBOARDING
            r8 = r12
            if (r8 != r3) goto L37
            goto L38
        L37:
            r1 = 0
        L38:
            com.discord.utilities.time.Clock r3 = com.discord.utilities.time.ClockFactory.get()
            r0.<init>(r1, r3)
            goto L43
        L40:
            r8 = r12
            r0 = r17
        L43:
            r1 = r20 & 64
            if (r1 == 0) goto L49
            r9 = 0
            goto L4b
        L49:
            r9 = r18
        L4b:
            r2 = r11
            r3 = r12
            r8 = r0
            r10 = r19
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.contact_sync.WidgetContactSyncViewModel.<init>(com.discord.widgets.contact_sync.ContactSyncMode, boolean, boolean, com.discord.utilities.rest.RestAPI, rx.Observable, com.discord.widgets.contact_sync.ContactSyncFlowAnalytics, boolean, kotlin.jvm.functions.Function1, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final ToolbarConfig getLandingToolbarConfig(ContactSyncMode contactSyncMode) {
        ToolbarConfig toolbarConfig;
        ToolbarConfig toolbarConfig2;
        int ordinal = contactSyncMode.ordinal();
        if (ordinal == 0) {
            toolbarConfig = WidgetContactSyncViewModelKt.TOOLBAR_CONFIG_ONBOARDING;
            return toolbarConfig;
        } else if (ordinal == 1) {
            toolbarConfig2 = WidgetContactSyncViewModelKt.TOOLBAR_CONFIG_DEFAULT;
            return toolbarConfig2;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void handleComplete() {
        ViewState viewState = getViewState();
        if (viewState != null) {
            if (viewState.getMode() == ContactSyncMode.ONBOARDING) {
                StoreStream.Companion.getNux().setContactSyncCompleted(true);
            }
            PublishSubject<Event> publishSubject = this.eventsSubject;
            publishSubject.k.onNext(Event.Completed.INSTANCE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleContactsEnableError(Error error) {
        if (error.getType() == Error.Type.RATE_LIMITED) {
            PublishSubject<Event> publishSubject = this.eventsSubject;
            publishSubject.k.onNext(Event.RateLimited.INSTANCE);
        } else {
            PublishSubject<Event> publishSubject2 = this.eventsSubject;
            publishSubject2.k.onNext(Event.ContactsEnableFailed.INSTANCE);
        }
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, Views.VIEW_LANDING, false, false, null, null, null, getLandingToolbarConfig(viewState.getMode()), 32255, null));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleContactsEnabled() {
        ViewState viewState = getViewState();
        if (viewState != null) {
            AnalyticsTracker.INSTANCE.contactSyncToggled(true, viewState.getAllowPhone(), viewState.getAllowEmail());
            PublishSubject<Event> publishSubject = this.eventsSubject;
            publishSubject.k.onNext(Event.ContactsEnabled.INSTANCE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleFriendSuggestions(BulkFriendSuggestions bulkFriendSuggestions) {
        ViewState viewState = getViewState();
        if (viewState == null) {
            return;
        }
        if (bulkFriendSuggestions.b().isEmpty()) {
            ContactSyncFlowAnalytics contactSyncFlowAnalytics = this.tracker;
            Views views = Views.VIEW_SUGGESTIONS_EMPTY;
            contactSyncFlowAnalytics.trackFlowStep(views.getTrackingStep(), false, false, g0.mapOf(d0.o.to("num_contacts_found", 0)));
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, views, false, false, null, null, null, new ToolbarConfig(false, false), 32127, null));
            return;
        }
        StoreStream.Companion.getFriendSuggestions().updateFriendSuggestions(bulkFriendSuggestions.b());
        List<FriendSuggestion> b2 = bulkFriendSuggestions.b();
        ArrayList<FriendSuggestion> arrayList = new ArrayList();
        Iterator<T> it = b2.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            List<FriendSuggestionReason> a = ((FriendSuggestion) next).a();
            if (!(a instanceof Collection) || !a.isEmpty()) {
                for (FriendSuggestionReason friendSuggestionReason : a) {
                    if (m.areEqual(friendSuggestionReason.b(), "contacts")) {
                        break;
                    }
                }
            }
            z2 = false;
            if (z2) {
                arrayList.add(next);
            }
        }
        this.tracker.trackFlowStep(Views.VIEW_SUGGESTIONS.getTrackingStep(), false, false, g0.mapOf(d0.o.to("num_contacts_found", Integer.valueOf(arrayList.size()))));
        String a2 = bulkFriendSuggestions.a();
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        for (FriendSuggestion friendSuggestion : arrayList) {
            arrayList2.add(new Item.FriendSuggestionItem(friendSuggestion, true));
        }
        ArrayList arrayList3 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        for (FriendSuggestion friendSuggestion2 : arrayList) {
            arrayList3.add(Long.valueOf(friendSuggestion2.b().i()));
        }
        updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, Views.VIEW_SUGGESTIONS, false, false, a2, arrayList2, arrayList3, new ToolbarConfig(false, true), 3455, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleFriendsAdded(BulkAddFriendsResponse bulkAddFriendsResponse) {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, null, false, false, null, null, null, null, 65407, null));
            this.tracker.trackEnd(false, h0.mapOf(d0.o.to("num_contacts_found", Integer.valueOf(viewState.getFriendSuggestions().size())), d0.o.to("num_contacts_added", Integer.valueOf(bulkAddFriendsResponse.b().size()))));
            if (!bulkAddFriendsResponse.a().isEmpty()) {
                this.eventsSubject.k.onNext(Event.AddFriendsFailedPartial.INSTANCE);
                return;
            }
            handleComplete();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleFriendsAddedError(Error error) {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, null, false, false, null, null, null, null, 65407, null));
            this.tracker.trackEnd(false, h0.mapOf(d0.o.to("num_contacts_found", Integer.valueOf(viewState.getFriendSuggestions().size())), d0.o.to("num_contacts_added", 0)));
            Error.Response response = error.getResponse();
            m.checkNotNullExpressionValue(response, "error.response");
            Map<String, List<String>> messages = response.getMessages();
            m.checkNotNullExpressionValue(messages, "error.response.messages");
            if (!messages.isEmpty()) {
                this.eventsSubject.k.onNext(Event.AddFriendsFailed.INSTANCE);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handlePhoneSubmitted() {
        ViewState viewState = getViewState();
        if (viewState != null) {
            ContactSyncFlowAnalytics contactSyncFlowAnalytics = this.tracker;
            Views views = Views.VIEW_VERIFY_PHONE;
            ContactSyncFlowAnalytics.trackFlowStep$default(contactSyncFlowAnalytics, views.getTrackingStep(), false, false, null, 8, null);
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, views, false, false, null, null, null, null, 65023, null));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handlePhoneSubmittedError() {
        PublishSubject<Event> publishSubject = this.eventsSubject;
        publishSubject.k.onNext(Event.PhoneInvalid.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handlePhoneVerified() {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, Views.VIEW_LANDING, false, false, null, null, null, getLandingToolbarConfig(viewState.getMode()), 32255, null));
            PublishSubject<Event> publishSubject = this.eventsSubject;
            publishSubject.k.onNext(Event.PermissionsNeeded.INSTANCE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handlePhoneVerifiedError(Error error) {
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        Map<String, List<String>> messages = response.getMessages();
        m.checkNotNullExpressionValue(messages, "error.response.messages");
        if (!messages.isEmpty()) {
            PublishSubject<Event> publishSubject = this.eventsSubject;
            publishSubject.k.onNext(Event.VerificationCodeInvalid.INSTANCE);
            return;
        }
        PublishSubject<Event> publishSubject2 = this.eventsSubject;
        publishSubject2.k.onNext(Event.VerificationFailed.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, true, null, storeState.getUserPhone(), storeState.getContactsConnection(), storeState.getCountryCode(), storeState.getUsername(), null, false, false, null, false, false, null, null, null, null, 65474, null));
            if (!this.initialized) {
                this.initialized = true;
                PublishSubject<Event> publishSubject = this.eventsSubject;
                publishSubject.k.onNext(Event.MaybeProceedFromLanding.INSTANCE);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleUploadError(Error error) {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, Views.VIEW_LANDING, false, false, null, null, null, getLandingToolbarConfig(viewState.getMode()), 32127, null));
            if (error.getType() == Error.Type.RATE_LIMITED) {
                PublishSubject<Event> publishSubject = this.eventsSubject;
                publishSubject.k.onNext(Event.RateLimited.INSTANCE);
                return;
            }
            PublishSubject<Event> publishSubject2 = this.eventsSubject;
            publishSubject2.k.onNext(Event.UploadFailed.INSTANCE);
        }
    }

    @MainThread
    public final void dismissUpsell() {
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getContactSync().dismissUpsell();
        companion.getUserSettings().updateContactSyncShown();
    }

    public final ContactSyncFlowAnalytics getTracker() {
        return this.tracker;
    }

    @MainThread
    public final void handleToggleFriendSuggestionSelected(long j, boolean z2) {
        ViewState viewState = getViewState();
        if (viewState != null) {
            List mutableList = u.toMutableList((Collection) viewState.getSelectedFriendIds());
            if (z2) {
                mutableList.add(Long.valueOf(j));
            } else {
                mutableList.remove(Long.valueOf(j));
            }
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, null, false, false, null, null, mutableList, null, 49151, null));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventsSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventsSubject");
        return publishSubject;
    }

    @MainThread
    public final void onBulkAddFriends() {
        String bulkAddToken;
        ViewState viewState = getViewState();
        if (viewState != null && (bulkAddToken = viewState.getBulkAddToken()) != null) {
            List<Long> selectedFriendIds = viewState.getSelectedFriendIds();
            if (selectedFriendIds.isEmpty()) {
                this.tracker.trackEnd(false, h0.mapOf(d0.o.to("num_contacts_found", Integer.valueOf(viewState.getFriendSuggestions().size())), d0.o.to("num_contacts_added", 0)));
                handleComplete();
                return;
            }
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, true, false, null, false, false, null, null, null, null, 65407, null));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.bulkAddRelationships(new RestAPIParams.UserBulkRelationship(selectedFriendIds, bulkAddToken)), false, 1, null), WidgetContactSyncViewModel$onBulkAddFriends$1.INSTANCE), this, null, 2, null), WidgetContactSyncViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetContactSyncViewModel$onBulkAddFriends$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSyncViewModel$onBulkAddFriends$2(this));
        }
    }

    @MainThread
    public final void onContactsFetched(Set<String> set) {
        m.checkNotNullParameter(set, "contactNumbers");
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, true, false, null, false, false, null, null, null, null, 65407, null));
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(set, 10));
            for (String str : set) {
                arrayList.add(new RestAPIParams.ContactEntry(str, str, g0.mapOf(new Pair("number", str))));
            }
            RestAPIParams.UploadContacts uploadContacts = new RestAPIParams.UploadContacts(arrayList, false, AllowedInSuggestionsType.ANYONE_WITH_CONTACT_INFO);
            StoreStream.Companion.getContactSync().setContactSyncUploadTimestamp(ClockFactory.get().currentTimeMillis());
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(this.restAPI.uploadContacts(uploadContacts), WidgetContactSyncViewModel$onContactsFetched$1.INSTANCE), false, 1, null), this, null, 2, null), WidgetContactSyncViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetContactSyncViewModel$onContactsFetched$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSyncViewModel$onContactsFetched$2(this));
        }
    }

    @MainThread
    public final void onLandingNext() {
        ToolbarConfig toolbarConfig;
        ViewState viewState = getViewState();
        if (viewState == null) {
            return;
        }
        if (viewState.getPhoneNumber() == null) {
            ContactSyncFlowAnalytics contactSyncFlowAnalytics = this.tracker;
            Views views = Views.VIEW_ADD_PHONE;
            ContactSyncFlowAnalytics.trackFlowStep$default(contactSyncFlowAnalytics, views.getTrackingStep(), false, false, null, 8, null);
            toolbarConfig = WidgetContactSyncViewModelKt.TOOLBAR_CONFIG_DEFAULT;
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, views, false, false, null, null, null, toolbarConfig, 32255, null));
            return;
        }
        PublishSubject<Event> publishSubject = this.eventsSubject;
        publishSubject.k.onNext(Event.PermissionsNeeded.INSTANCE);
    }

    @MainThread
    public final void onNameSubmitted(String str) {
        MGRecyclerAdapterSimple mGRecyclerAdapterSimple;
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        AnalyticsTracker.INSTANCE.nameSubmitted(w.split$default((CharSequence) str, new String[]{" "}, false, 0, 6, (Object) null).size(), str.length());
        String str2 = null;
        if (!t.isBlank(str)) {
            str2 = w.trim(str).toString();
        }
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, str, false, false, null, false, false, null, null, null, null, 65471, null));
            ConnectedAccount existingConnection = viewState.getExistingConnection();
            if (existingConnection != null) {
                mGRecyclerAdapterSimple = null;
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.updateConnection(existingConnection.g(), existingConnection.b(), new RestAPIParams.ConnectedAccount(true, existingConnection.b(), str2, existingConnection.e(), existingConnection.f(), existingConnection.g(), existingConnection.h(), existingConnection.i())), false, 1, null), new WidgetContactSyncViewModel$onNameSubmitted$1(str2)), this, null, 2, null), WidgetContactSyncViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetContactSyncViewModel$onNameSubmitted$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSyncViewModel$onNameSubmitted$2(this));
            } else {
                mGRecyclerAdapterSimple = null;
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.createConnectionContacts(new RestAPIParams.ConnectedAccountContacts(str2, true)), false, 1, null), this, null, 2, null), WidgetContactSyncViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetContactSyncViewModel$onNameSubmitted$5(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSyncViewModel$onNameSubmitted$4(this));
            }
            int i = viewState.getAllowPhone() ? 2 : 0;
            if (viewState.getAllowEmail()) {
                i |= 4;
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.updateUserSettings(RestAPIParams.UserSettings.Companion.createWithFriendDiscoveryFlags(Integer.valueOf(i))), false, 1, mGRecyclerAdapterSimple), this, mGRecyclerAdapterSimple, 2, mGRecyclerAdapterSimple), WidgetContactSyncViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetContactSyncViewModel$onNameSubmitted$7(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetContactSyncViewModel$onNameSubmitted$6.INSTANCE);
        }
    }

    @MainThread
    public final void onPermissionsBecameAvailable() {
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, null, false, false, null, null, null, null, 65279, null));
        }
    }

    @MainThread
    public final void onPermissionsDenied() {
        this.tracker.trackFlowStep(Views.VIEW_LANDING.getTrackingStep(), true, false, g0.mapOf(d0.o.to("mobile_contacts_permission", "denied")));
        AnalyticsTracker.INSTANCE.permissionsAcked("contacts", false);
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, true, null, false, false, null, null, null, null, 65151, null));
        }
    }

    @MainThread
    public final void onPermissionsGranted() {
        ViewState viewState = getViewState();
        if (viewState != null && viewState.getDisplayedChild() != Views.VIEW_SUGGESTIONS && viewState.getDisplayedChild() != Views.VIEW_SUGGESTIONS_EMPTY) {
            AnalyticsTracker.INSTANCE.permissionsAcked("contacts", true);
            ContactSyncFlowAnalytics contactSyncFlowAnalytics = this.tracker;
            Views views = Views.VIEW_NAME_INPUT;
            ContactSyncFlowAnalytics.trackFlowStep$default(contactSyncFlowAnalytics, views.getTrackingStep(), false, false, null, 8, null);
            if (viewState.getName() != null) {
                onNameSubmitted(viewState.getName());
            } else {
                updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, views, false, false, null, null, null, new ToolbarConfig(false, true), 31871, null));
            }
        }
    }

    @MainThread
    public final void onPermissionsToggle(boolean z2, boolean z3) {
        AnalyticsTracker.INSTANCE.contactSyncToggled(false, z2, z3);
        ViewState viewState = getViewState();
        if (viewState != null) {
            updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, null, z2, z3, null, null, null, null, 62463, null));
        }
    }

    @MainThread
    public final void onPhoneNumberSubmitted(String str, String str2) {
        m.checkNotNullParameter(str, "phoneNumber");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.userAddPhone(new RestAPIParams.Phone(str, WidgetUserPhoneManage.Companion.Source.CONTACT_SYNC.getSource(), str2)), false, 1, null), this, null, 2, null), WidgetContactSyncViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetContactSyncViewModel$onPhoneNumberSubmitted$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSyncViewModel$onPhoneNumberSubmitted$1(this));
    }

    @MainThread
    public final void onVerifyPhone(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.userAddPhoneNoPassword(new RestAPIParams.VerificationCodeOnly(str)), false, 1, null), this, null, 2, null), WidgetContactSyncViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetContactSyncViewModel$onVerifyPhone$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetContactSyncViewModel$onVerifyPhone$1(this));
    }

    @MainThread
    public final void requestingPermissions() {
        ContactSyncFlowAnalytics.trackFlowStep$default(this.tracker, "Contacts Permission Requested", false, false, null, 8, null);
    }

    public final void skip() {
        ViewState viewState = getViewState();
        if (viewState != null) {
            int ordinal = viewState.getDisplayedChild().ordinal();
            if (ordinal == 0) {
                ContactSyncFlowAnalytics.trackEnd$default(this.tracker, true, null, 2, null);
                handleComplete();
            } else if (ordinal == 1) {
                ContactSyncFlowAnalytics contactSyncFlowAnalytics = this.tracker;
                Views views = Views.VIEW_LANDING;
                ContactSyncFlowAnalytics.trackFlowStep$default(contactSyncFlowAnalytics, views.getTrackingStep(), false, true, null, 8, null);
                updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, views, false, false, null, null, null, getLandingToolbarConfig(viewState.getMode()), 32255, null));
            } else if (ordinal == 2) {
                ContactSyncFlowAnalytics contactSyncFlowAnalytics2 = this.tracker;
                Views views2 = Views.VIEW_ADD_PHONE;
                ContactSyncFlowAnalytics.trackFlowStep$default(contactSyncFlowAnalytics2, views2.getTrackingStep(), false, true, null, 8, null);
                updateViewState(ViewState.copy$default(viewState, false, null, null, null, null, null, null, false, false, views2, false, false, null, null, null, null, 65023, null));
            } else if (ordinal == 3) {
                onNameSubmitted("");
            } else if (ordinal == 4) {
                this.tracker.trackEnd(false, h0.mapOf(d0.o.to("num_contacts_found", Integer.valueOf(viewState.getFriendSuggestions().size())), d0.o.to("num_contacts_added", 0)));
                handleComplete();
            } else if (ordinal == 5) {
                this.tracker.trackEnd(false, h0.mapOf(d0.o.to("num_contacts_found", Integer.valueOf(viewState.getFriendSuggestions().size())), d0.o.to("num_contacts_added", 0)));
                handleComplete();
            }
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public WidgetContactSyncViewModel(com.discord.widgets.contact_sync.ContactSyncMode r24, boolean r25, boolean r26, com.discord.utilities.rest.RestAPI r27, rx.Observable<com.discord.widgets.contact_sync.WidgetContactSyncViewModel.StoreState> r28, com.discord.widgets.contact_sync.ContactSyncFlowAnalytics r29, boolean r30, kotlin.jvm.functions.Function1<? super com.discord.utilities.error.Error, kotlin.Unit> r31) {
        /*
            r23 = this;
            r0 = r23
            r1 = r27
            r2 = r28
            r3 = r29
            r4 = r31
            java.lang.String r5 = "mode"
            r8 = r24
            d0.z.d.m.checkNotNullParameter(r8, r5)
            java.lang.String r5 = "restAPI"
            d0.z.d.m.checkNotNullParameter(r1, r5)
            java.lang.String r5 = "storeObservable"
            d0.z.d.m.checkNotNullParameter(r2, r5)
            java.lang.String r5 = "tracker"
            d0.z.d.m.checkNotNullParameter(r3, r5)
            java.lang.String r5 = "captchaLauncher"
            d0.z.d.m.checkNotNullParameter(r4, r5)
            com.discord.widgets.contact_sync.WidgetContactSyncViewModel$ViewState r5 = new com.discord.widgets.contact_sync.WidgetContactSyncViewModel$ViewState
            com.discord.models.phone.PhoneCountryCode$Companion r6 = com.discord.models.phone.PhoneCountryCode.Companion
            com.discord.models.phone.PhoneCountryCode r11 = r6.getDEFAULT_COUNTRY_CODE()
            com.discord.widgets.contact_sync.WidgetContactSyncViewModel$Views r16 = com.discord.widgets.contact_sync.WidgetContactSyncViewModel.Views.VIEW_LANDING
            java.util.List r20 = d0.t.n.emptyList()
            java.util.List r21 = d0.t.n.emptyList()
            int r6 = r24.ordinal()
            if (r6 == 0) goto L4b
            r7 = 1
            if (r6 != r7) goto L45
            com.discord.widgets.contact_sync.WidgetContactSyncViewModel$ToolbarConfig r6 = com.discord.widgets.contact_sync.WidgetContactSyncViewModelKt.access$getTOOLBAR_CONFIG_DEFAULT$p()
            goto L4f
        L45:
            kotlin.NoWhenBranchMatchedException r1 = new kotlin.NoWhenBranchMatchedException
            r1.<init>()
            throw r1
        L4b:
            com.discord.widgets.contact_sync.WidgetContactSyncViewModel$ToolbarConfig r6 = com.discord.widgets.contact_sync.WidgetContactSyncViewModelKt.access$getTOOLBAR_CONFIG_ONBOARDING$p()
        L4f:
            r22 = r6
            r7 = 0
            r9 = 0
            r10 = 0
            java.lang.String r12 = ""
            r13 = 0
            r14 = 0
            r15 = 0
            r19 = 0
            r6 = r5
            r8 = r24
            r17 = r25
            r18 = r26
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22)
            r0.<init>(r5)
            r0.restAPI = r1
            r0.tracker = r3
            r1 = r30
            r0.initialized = r1
            r0.captchaLauncher = r4
            rx.subjects.PublishSubject r1 = rx.subjects.PublishSubject.k0()
            r0.eventsSubject = r1
            r1 = 2
            r3 = 0
            rx.Observable r4 = com.discord.utilities.rx.ObservableExtensionsKt.ui$default(r2, r0, r3, r1, r3)
            java.lang.Class<com.discord.widgets.contact_sync.WidgetContactSyncViewModel> r5 = com.discord.widgets.contact_sync.WidgetContactSyncViewModel.class
            r6 = 0
            r7 = 0
            r8 = 0
            com.discord.widgets.contact_sync.WidgetContactSyncViewModel$1 r11 = new com.discord.widgets.contact_sync.WidgetContactSyncViewModel$1
            r11.<init>()
            r12 = 62
            com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.contact_sync.WidgetContactSyncViewModel.<init>(com.discord.widgets.contact_sync.ContactSyncMode, boolean, boolean, com.discord.utilities.rest.RestAPI, rx.Observable, com.discord.widgets.contact_sync.ContactSyncFlowAnalytics, boolean, kotlin.jvm.functions.Function1):void");
    }
}
