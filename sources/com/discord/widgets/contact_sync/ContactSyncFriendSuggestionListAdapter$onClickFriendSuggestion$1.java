package com.discord.widgets.contact_sync;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: ContactSyncFriendSuggestionListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "<anonymous parameter 0>", "", "<anonymous parameter 1>", "", "invoke", "(JZ)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ContactSyncFriendSuggestionListAdapter$onClickFriendSuggestion$1 extends o implements Function2<Long, Boolean, Unit> {
    public static final ContactSyncFriendSuggestionListAdapter$onClickFriendSuggestion$1 INSTANCE = new ContactSyncFriendSuggestionListAdapter$onClickFriendSuggestion$1();

    public ContactSyncFriendSuggestionListAdapter$onClickFriendSuggestion$1() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Long l, Boolean bool) {
        invoke(l.longValue(), bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(long j, boolean z2) {
    }
}
