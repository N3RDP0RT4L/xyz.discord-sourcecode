package com.discord.widgets.contact_sync;

import com.discord.analytics.generated.events.network_action.TrackNetworkActionUserBulkRelationshipsUpdate;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.friendsuggestions.BulkAddFriendsResponse;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetContactSyncViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/friendsuggestions/BulkAddFriendsResponse;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/api/friendsuggestions/BulkAddFriendsResponse;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetContactSyncViewModel$onBulkAddFriends$1 extends o implements Function1<BulkAddFriendsResponse, TrackNetworkMetadataReceiver> {
    public static final WidgetContactSyncViewModel$onBulkAddFriends$1 INSTANCE = new WidgetContactSyncViewModel$onBulkAddFriends$1();

    public WidgetContactSyncViewModel$onBulkAddFriends$1() {
        super(1);
    }

    public final TrackNetworkMetadataReceiver invoke(BulkAddFriendsResponse bulkAddFriendsResponse) {
        return new TrackNetworkActionUserBulkRelationshipsUpdate();
    }
}
