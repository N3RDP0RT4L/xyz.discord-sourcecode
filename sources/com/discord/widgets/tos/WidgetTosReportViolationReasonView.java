package com.discord.widgets.tos;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.discord.api.report.ReportReason;
import com.discord.databinding.WidgetTosReportViolationReasonBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.material.radiobutton.MaterialRadioButton;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$BooleanRef;
import xyz.discord.R;
/* compiled from: WidgetTosReportViolationReasonView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001d\u0012\b\b\u0002\u0010 \u001a\u00020\u001f¢\u0006\u0004\b!\u0010\"J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nR$\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\nR.\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\b\u0010\u000b\u001a\u0004\u0018\u00010\u000f8\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0017\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a¨\u0006#"}, d2 = {"Lcom/discord/widgets/tos/WidgetTosReportViolationReasonView;", "Landroid/widget/RelativeLayout;", "Landroid/view/View$OnClickListener;", "onClickListener", "", "setOnClickListener", "(Landroid/view/View$OnClickListener;)V", "", "enabled", "setEnabled", "(Z)V", "value", "isChecked", "()Z", "setChecked", "Lcom/discord/api/report/ReportReason;", ModelAuditLogEntry.CHANGE_KEY_REASON, "Lcom/discord/api/report/ReportReason;", "getReason", "()Lcom/discord/api/report/ReportReason;", "setReason", "(Lcom/discord/api/report/ReportReason;)V", "Lcom/discord/databinding/WidgetTosReportViolationReasonBinding;", "binding", "Lcom/discord/databinding/WidgetTosReportViolationReasonBinding;", "getBinding", "()Lcom/discord/databinding/WidgetTosReportViolationReasonBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTosReportViolationReasonView extends RelativeLayout {
    private final WidgetTosReportViolationReasonBinding binding;
    private ReportReason reason;

    public WidgetTosReportViolationReasonView(Context context) {
        this(context, null, 0, 6, null);
    }

    public WidgetTosReportViolationReasonView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ WidgetTosReportViolationReasonView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    public final WidgetTosReportViolationReasonBinding getBinding() {
        return this.binding;
    }

    public final ReportReason getReason() {
        return this.reason;
    }

    public final boolean isChecked() {
        MaterialRadioButton materialRadioButton = this.binding.d;
        m.checkNotNullExpressionValue(materialRadioButton, "binding.reportReasonRadio");
        return materialRadioButton.isChecked();
    }

    public final void setChecked(boolean z2) {
        MaterialRadioButton materialRadioButton = this.binding.d;
        m.checkNotNullExpressionValue(materialRadioButton, "binding.reportReasonRadio");
        materialRadioButton.setChecked(z2);
    }

    @Override // android.view.View
    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        RelativeLayout relativeLayout = this.binding.a;
        m.checkNotNullExpressionValue(relativeLayout, "binding.root");
        relativeLayout.setEnabled(z2);
        MaterialRadioButton materialRadioButton = this.binding.d;
        m.checkNotNullExpressionValue(materialRadioButton, "binding.reportReasonRadio");
        materialRadioButton.setEnabled(z2);
    }

    @Override // android.view.View
    public void setOnClickListener(final View.OnClickListener onClickListener) {
        final Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ref$BooleanRef.element = false;
        this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tos.WidgetTosReportViolationReasonView$setOnClickListener$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ref$BooleanRef.element = true;
                View.OnClickListener onClickListener2 = onClickListener;
                if (onClickListener2 != null) {
                    onClickListener2.onClick(WidgetTosReportViolationReasonView.this);
                }
                ref$BooleanRef.element = false;
            }
        });
        this.binding.d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: com.discord.widgets.tos.WidgetTosReportViolationReasonView$setOnClickListener$2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
                View.OnClickListener onClickListener2;
                if (!ref$BooleanRef.element && z2 && (onClickListener2 = onClickListener) != null) {
                    onClickListener2.onClick(WidgetTosReportViolationReasonView.this);
                }
            }
        });
    }

    public final void setReason(ReportReason reportReason) {
        this.reason = reportReason;
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.reportReasonHeader");
        String str = null;
        textView.setText(reportReason != null ? reportReason.b() : null);
        TextView textView2 = this.binding.f2649b;
        m.checkNotNullExpressionValue(textView2, "binding.reportReasonDescriptipn");
        if (reportReason != null) {
            str = reportReason.a();
        }
        textView2.setText(str);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetTosReportViolationReasonView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.widget_tos_report_violation_reason, (ViewGroup) this, false);
        addView(inflate);
        int i2 = R.id.report_reason_descriptipn;
        TextView textView = (TextView) inflate.findViewById(R.id.report_reason_descriptipn);
        if (textView != null) {
            i2 = R.id.report_reason_header;
            TextView textView2 = (TextView) inflate.findViewById(R.id.report_reason_header);
            if (textView2 != null) {
                i2 = R.id.report_reason_radio;
                MaterialRadioButton materialRadioButton = (MaterialRadioButton) inflate.findViewById(R.id.report_reason_radio);
                if (materialRadioButton != null) {
                    WidgetTosReportViolationReasonBinding widgetTosReportViolationReasonBinding = new WidgetTosReportViolationReasonBinding((RelativeLayout) inflate, textView, textView2, materialRadioButton);
                    m.checkNotNullExpressionValue(widgetTosReportViolationReasonBinding, "WidgetTosReportViolation…rom(context), this, true)");
                    this.binding = widgetTosReportViolationReasonBinding;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }
}
