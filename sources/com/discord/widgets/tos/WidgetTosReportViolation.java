package com.discord.widgets.tos;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewGroupKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.report.ReportReason;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetTosReportViolationBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.tos.WidgetTosReportViolationViewModel;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetTosReportViolation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 +2\u00020\u0001:\u0001+B\u0007¢\u0006\u0004\b*\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\f\u001a\u00020\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0012\u0010\bJ\u000f\u0010\u0013\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0013\u0010\bJ\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\bR\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001d\u0010#\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b \u0010\u001b\u001a\u0004\b!\u0010\"R\u001d\u0010)\u001a\u00020$8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(¨\u0006,"}, d2 = {"Lcom/discord/widgets/tos/WidgetTosReportViolation;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/tos/WidgetTosReportViolationViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/tos/WidgetTosReportViolationViewModel$ViewState;)V", "handleLoading", "()V", "", "Lcom/discord/api/report/ReportReason;", "reasons", "handleLoaded", "(Ljava/util/List;)V", "", ModelAuditLogEntry.CHANGE_KEY_REASON, "handleReportSubmitting", "(I)V", "handleReportSubmitted", "handleReportSubmissionError", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/tos/WidgetTosReportViolationViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/tos/WidgetTosReportViolationViewModel;", "viewModel", "Lcom/discord/widgets/tos/WidgetTosReportViolation$Companion$Arguments;", "args$delegate", "getArgs", "()Lcom/discord/widgets/tos/WidgetTosReportViolation$Companion$Arguments;", "args", "Lcom/discord/databinding/WidgetTosReportViolationBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetTosReportViolationBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTosReportViolation extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetTosReportViolation.class, "binding", "getBinding()Lcom/discord/databinding/WidgetTosReportViolationBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_CHANNEL_ID = "EXTRA_CHANNEL_ID";
    private static final String EXTRA_MESSAGE_ID = "EXTRA_MESSAGE_ID";
    private static final String EXTRA_TARGET = "EXTRA_TARGET";
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetTosReportViolation$binding$2.INSTANCE, null, 2, null);
    private final Lazy args$delegate = g.lazy(new WidgetTosReportViolation$args$2(this));

    /* compiled from: WidgetTosReportViolation.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0014B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013JA\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0010\b\u0002\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\t¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000f¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/tos/WidgetTosReportViolation$Companion;", "", "Landroid/content/Context;", "context", "", "target", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "", "show", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V", WidgetTosReportViolation.EXTRA_CHANNEL_ID, "Ljava/lang/String;", WidgetTosReportViolation.EXTRA_MESSAGE_ID, WidgetTosReportViolation.EXTRA_TARGET, HookHelper.constructorName, "()V", "Arguments", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetTosReportViolation.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\n\u0010\f\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010\r\u001a\u00060\u0005j\u0002`\t¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0005j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\bJ6\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\f\b\u0002\u0010\f\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010\r\u001a\u00060\u0005j\u0002`\tHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001d\u0010\f\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u001d\u0010\r\u001a\u00060\u0005j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0018\u001a\u0004\b\u001c\u0010\b¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/tos/WidgetTosReportViolation$Companion$Arguments;", "", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/ChannelId;", "component2", "()J", "Lcom/discord/primitives/MessageId;", "component3", "target", "channelId", "messageId", "copy", "(Ljava/lang/String;JJ)Lcom/discord/widgets/tos/WidgetTosReportViolation$Companion$Arguments;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Ljava/lang/String;", "getTarget", "getMessageId", HookHelper.constructorName, "(Ljava/lang/String;JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Arguments {
            private final long channelId;
            private final long messageId;
            private final String target;

            public Arguments(String str, long j, long j2) {
                m.checkNotNullParameter(str, "target");
                this.target = str;
                this.channelId = j;
                this.messageId = j2;
            }

            public static /* synthetic */ Arguments copy$default(Arguments arguments, String str, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = arguments.target;
                }
                if ((i & 2) != 0) {
                    j = arguments.channelId;
                }
                long j3 = j;
                if ((i & 4) != 0) {
                    j2 = arguments.messageId;
                }
                return arguments.copy(str, j3, j2);
            }

            public final String component1() {
                return this.target;
            }

            public final long component2() {
                return this.channelId;
            }

            public final long component3() {
                return this.messageId;
            }

            public final Arguments copy(String str, long j, long j2) {
                m.checkNotNullParameter(str, "target");
                return new Arguments(str, j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Arguments)) {
                    return false;
                }
                Arguments arguments = (Arguments) obj;
                return m.areEqual(this.target, arguments.target) && this.channelId == arguments.channelId && this.messageId == arguments.messageId;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final long getMessageId() {
                return this.messageId;
            }

            public final String getTarget() {
                return this.target;
            }

            public int hashCode() {
                String str = this.target;
                int hashCode = str != null ? str.hashCode() : 0;
                return b.a(this.messageId) + ((b.a(this.channelId) + (hashCode * 31)) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("Arguments(target=");
                R.append(this.target);
                R.append(", channelId=");
                R.append(this.channelId);
                R.append(", messageId=");
                return a.B(R, this.messageId, ")");
            }
        }

        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, Context context, String str, Long l, Long l2, int i, Object obj) {
            if ((i & 4) != 0) {
                l = null;
            }
            if ((i & 8) != 0) {
                l2 = null;
            }
            companion.show(context, str, l, l2);
        }

        public final void show(Context context, String str, Long l, Long l2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "target");
            Intent intent = new Intent();
            intent.putExtra(WidgetTosReportViolation.EXTRA_CHANNEL_ID, l);
            intent.putExtra(WidgetTosReportViolation.EXTRA_MESSAGE_ID, l2);
            intent.putExtra(WidgetTosReportViolation.EXTRA_TARGET, str);
            j.d(context, WidgetTosReportViolation.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetTosReportViolation() {
        super(R.layout.widget_tos_report_violation);
        WidgetTosReportViolation$viewModel$2 widgetTosReportViolation$viewModel$2 = new WidgetTosReportViolation$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetTosReportViolationViewModel.class), new WidgetTosReportViolation$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetTosReportViolation$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetTosReportViolationViewModel.ViewState viewState) {
        if (viewState instanceof WidgetTosReportViolationViewModel.ViewState.Loading) {
            handleLoading();
        } else if (viewState instanceof WidgetTosReportViolationViewModel.ViewState.Loaded) {
            handleLoaded(((WidgetTosReportViolationViewModel.ViewState.Loaded) viewState).getReasons());
        } else if (viewState instanceof WidgetTosReportViolationViewModel.ViewState.Submitting) {
            handleReportSubmitting(((WidgetTosReportViolationViewModel.ViewState.Submitting) viewState).getReason());
        } else if (viewState instanceof WidgetTosReportViolationViewModel.ViewState.Submitted) {
            handleReportSubmitted();
        } else if (viewState instanceof WidgetTosReportViolationViewModel.ViewState.SubmissionError) {
            handleReportSubmissionError();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Companion.Arguments getArgs() {
        return (Companion.Arguments) this.args$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetTosReportViolationBinding getBinding() {
        return (WidgetTosReportViolationBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetTosReportViolationViewModel getViewModel() {
        return (WidgetTosReportViolationViewModel) this.viewModel$delegate.getValue();
    }

    private final void handleLoaded(List<ReportReason> list) {
        ProgressBar progressBar = getBinding().f;
        m.checkNotNullExpressionValue(progressBar, "binding.reportReasonsLoading");
        progressBar.setVisibility(8);
        LoadingButton loadingButton = getBinding().f2648b;
        m.checkNotNullExpressionValue(loadingButton, "binding.reportButton");
        loadingButton.setEnabled(getViewModel().getReasonSelected() != null);
        getBinding().f2648b.setIsLoading(false);
        getBinding().f2648b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tos.WidgetTosReportViolation$handleLoaded$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetTosReportViolation.this.getViewModel().sendReport();
            }
        });
        TextView textView = getBinding().e;
        m.checkNotNullExpressionValue(textView, "binding.reportReasonsHeader");
        textView.setVisibility(0);
        getBinding().d.removeAllViews();
        for (final ReportReason reportReason : list) {
            final WidgetTosReportViolationReasonView widgetTosReportViolationReasonView = new WidgetTosReportViolationReasonView(requireContext(), null, 0, 6, null);
            if (!ViewCompat.isLaidOut(widgetTosReportViolationReasonView) || widgetTosReportViolationReasonView.isLayoutRequested()) {
                widgetTosReportViolationReasonView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.tos.WidgetTosReportViolation$handleLoaded$$inlined$forEach$lambda$2
                    @Override // android.view.View.OnLayoutChangeListener
                    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                        m.checkNotNullParameter(view, "view");
                        view.removeOnLayoutChangeListener(this);
                        WidgetTosReportViolationReasonView.this.setReason(reportReason);
                        WidgetTosReportViolationReasonView.this.setChecked(m.areEqual(reportReason, this.getViewModel().getReasonSelected()));
                        WidgetTosReportViolationReasonView.this.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tos.WidgetTosReportViolation$handleLoaded$$inlined$forEach$lambda$2.1
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view2) {
                                WidgetTosReportViolationBinding binding;
                                WidgetTosReportViolationBinding binding2;
                                WidgetTosReportViolationBinding binding3;
                                boolean z2 = true;
                                if (!m.areEqual(this.getViewModel().getReasonSelected(), WidgetTosReportViolationReasonView.this.getReason())) {
                                    this.getViewModel().setReasonSelected(WidgetTosReportViolationReasonView.this.getReason());
                                    binding2 = this.getBinding();
                                    LoadingButton loadingButton2 = binding2.f2648b;
                                    m.checkNotNullExpressionValue(loadingButton2, "binding.reportButton");
                                    loadingButton2.setEnabled(false);
                                    binding3 = this.getBinding();
                                    LinearLayout linearLayout = binding3.d;
                                    m.checkNotNullExpressionValue(linearLayout, "binding.reportReasonsContainer");
                                    for (View view3 : ViewGroupKt.getChildren(linearLayout)) {
                                        Objects.requireNonNull(view3, "null cannot be cast to non-null type com.discord.widgets.tos.WidgetTosReportViolationReasonView");
                                        ((WidgetTosReportViolationReasonView) view3).setChecked(false);
                                    }
                                    WidgetTosReportViolationReasonView.this.setChecked(true);
                                }
                                binding = this.getBinding();
                                LoadingButton loadingButton3 = binding.f2648b;
                                m.checkNotNullExpressionValue(loadingButton3, "binding.reportButton");
                                if (this.getViewModel().getReasonSelected() == null) {
                                    z2 = false;
                                }
                                loadingButton3.setEnabled(z2);
                            }
                        });
                    }
                });
            } else {
                widgetTosReportViolationReasonView.setReason(reportReason);
                widgetTosReportViolationReasonView.setChecked(m.areEqual(reportReason, getViewModel().getReasonSelected()));
                widgetTosReportViolationReasonView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tos.WidgetTosReportViolation$handleLoaded$$inlined$forEach$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetTosReportViolationBinding binding;
                        WidgetTosReportViolationBinding binding2;
                        WidgetTosReportViolationBinding binding3;
                        boolean z2 = true;
                        if (!m.areEqual(this.getViewModel().getReasonSelected(), WidgetTosReportViolationReasonView.this.getReason())) {
                            this.getViewModel().setReasonSelected(WidgetTosReportViolationReasonView.this.getReason());
                            binding2 = this.getBinding();
                            LoadingButton loadingButton2 = binding2.f2648b;
                            m.checkNotNullExpressionValue(loadingButton2, "binding.reportButton");
                            loadingButton2.setEnabled(false);
                            binding3 = this.getBinding();
                            LinearLayout linearLayout = binding3.d;
                            m.checkNotNullExpressionValue(linearLayout, "binding.reportReasonsContainer");
                            for (View view2 : ViewGroupKt.getChildren(linearLayout)) {
                                Objects.requireNonNull(view2, "null cannot be cast to non-null type com.discord.widgets.tos.WidgetTosReportViolationReasonView");
                                ((WidgetTosReportViolationReasonView) view2).setChecked(false);
                            }
                            WidgetTosReportViolationReasonView.this.setChecked(true);
                        }
                        binding = this.getBinding();
                        LoadingButton loadingButton3 = binding.f2648b;
                        m.checkNotNullExpressionValue(loadingButton3, "binding.reportButton");
                        if (this.getViewModel().getReasonSelected() == null) {
                            z2 = false;
                        }
                        loadingButton3.setEnabled(z2);
                    }
                });
            }
            getBinding().d.addView(widgetTosReportViolationReasonView);
        }
    }

    private final void handleLoading() {
        ProgressBar progressBar = getBinding().f;
        m.checkNotNullExpressionValue(progressBar, "binding.reportReasonsLoading");
        progressBar.setVisibility(0);
        LoadingButton loadingButton = getBinding().f2648b;
        m.checkNotNullExpressionValue(loadingButton, "binding.reportButton");
        loadingButton.setEnabled(false);
        getBinding().d.removeAllViews();
    }

    private final void handleReportSubmissionError() {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        LoadingButton loadingButton = getBinding().f2648b;
        m.checkNotNullExpressionValue(loadingButton, "binding.reportButton");
        loadingButton.setEnabled(false);
        getBinding().f2648b.setIsLoading(false);
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        e = b.a.k.b.e(this, R.string.notice_dispatch_error, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e2 = b.a.k.b.e(this, R.string.report_modal_error, new Object[]{"https://dis.gd/request"}, (r4 & 4) != 0 ? b.a.j : null);
        e3 = b.a.k.b.e(this, R.string.okay, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, e, e2, e3, null, null, null, null, null, null, null, null, 0, new WidgetTosReportViolation$handleReportSubmissionError$1(this), 8176, null);
    }

    private final void handleReportSubmitted() {
        b.a.d.m.i(this, R.string.report_modal_submitted, 0, 4);
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.finish();
        }
    }

    private final void handleReportSubmitting(int i) {
        LinearLayout linearLayout = getBinding().d;
        m.checkNotNullExpressionValue(linearLayout, "binding.reportReasonsContainer");
        for (View view : ViewGroupKt.getChildren(linearLayout)) {
            view.setEnabled(false);
        }
        LoadingButton loadingButton = getBinding().f2648b;
        m.checkNotNullExpressionValue(loadingButton, "binding.reportButton");
        loadingButton.setEnabled(false);
        getBinding().f2648b.setIsLoading(true);
        getViewModel().sendReportAPICall(i, getArgs().getChannelId(), getArgs().getMessageId());
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarTitle(R.string.report);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.reportHeader");
        b.a.k.b.m(textView, R.string.report_message, new Object[]{getArgs().getTarget()}, (r4 & 4) != 0 ? b.g.j : null);
        LinkifiedTextView linkifiedTextView = getBinding().g;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.reportTooltip");
        b.a.k.b.m(linkifiedTextView, R.string.form_report_help_text, new Object[]{"https://discord.com/guidelines"}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().f2648b.setIsLoading(false);
        LoadingButton loadingButton = getBinding().f2648b;
        m.checkNotNullExpressionValue(loadingButton, "binding.reportButton");
        loadingButton.setEnabled(false);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetTosReportViolation.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetTosReportViolation$onViewBoundOrOnResume$1(this));
    }
}
