package com.discord.widgets.tos;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetTosAcceptBinding;
import com.discord.models.requiredaction.RequiredAction;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetTosAccept.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0007¢\u0006\u0004\b\u0013\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/tos/WidgetTosAccept;", "Lcom/discord/app/AppFragment;", "Lcom/discord/models/requiredaction/RequiredAction;", "requiredAction", "", "configureUI", "(Lcom/discord/models/requiredaction/RequiredAction;)V", "onViewBoundOrOnResume", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetTosAcceptBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetTosAcceptBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTosAccept extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetTosAccept.class, "binding", "getBinding()Lcom/discord/databinding/WidgetTosAcceptBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetTosAccept$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetTosAccept.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/tos/WidgetTosAccept$Companion;", "", "Landroid/content/Context;", "context", "", "show", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetTosAccept.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetTosAccept() {
        super(R.layout.widget_tos_accept);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(RequiredAction requiredAction) {
        if (requiredAction == RequiredAction.AGREEMENTS) {
            LinkifiedTextView linkifiedTextView = getBinding().e;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.alertTosTermsOfService");
            b.m(linkifiedTextView, R.string.terms_of_service_url, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            LinkifiedTextView linkifiedTextView2 = getBinding().d;
            m.checkNotNullExpressionValue(linkifiedTextView2, "binding.alertTosPrivacyPolicy");
            b.m(linkifiedTextView2, R.string.privacy_policy_url, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            getBinding().f2647b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: com.discord.widgets.tos.WidgetTosAccept$configureUI$1
                @Override // android.widget.CompoundButton.OnCheckedChangeListener
                public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
                    WidgetTosAcceptBinding binding;
                    binding = WidgetTosAccept.this.getBinding();
                    MaterialButton materialButton = binding.c;
                    m.checkNotNullExpressionValue(materialButton, "binding.alertTosAckContinue");
                    materialButton.setEnabled(z2);
                }
            });
            MaterialButton materialButton = getBinding().c;
            m.checkNotNullExpressionValue(materialButton, "binding.alertTosAckContinue");
            CheckBox checkBox = getBinding().f2647b;
            m.checkNotNullExpressionValue(checkBox, "binding.alertTosAck");
            materialButton.setEnabled(checkBox.isChecked());
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tos.WidgetTosAccept$configureUI$2

                /* compiled from: WidgetTosAccept.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.tos.WidgetTosAccept$configureUI$2$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function1<Void, Unit> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    public AnonymousClass1() {
                        super(1);
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
                        invoke2(r1);
                        return Unit.a;
                    }

                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2(Void r1) {
                    }
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().userAgreements(new RestAPIParams.UserAgreements()), false, 1, null), WidgetTosAccept.this, null, 2, null).k(b.a.d.o.a.g(WidgetTosAccept.this.getContext(), AnonymousClass1.INSTANCE, null));
                }
            });
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetTosAcceptBinding getBinding() {
        return (WidgetTosAcceptBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setOnBackPressed$default(this, WidgetTosAccept$onViewBound$1.INSTANCE, 0, 2, null);
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_settings_logout, WidgetTosAccept$onViewBound$2.INSTANCE, null, 4, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreStream.Companion.getUserRequiredActions().observeUserRequiredAction(), this, null, 2, null), WidgetTosAccept.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetTosAccept$onViewBoundOrOnResume$1(this));
    }
}
