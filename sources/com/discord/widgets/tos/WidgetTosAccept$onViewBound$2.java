package com.discord.widgets.tos;

import android.content.Context;
import android.view.MenuItem;
import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetTosAccept.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Landroid/view/MenuItem;", "kotlin.jvm.PlatformType", "menuItem", "Landroid/content/Context;", "<anonymous parameter 1>", "", NotificationCompat.CATEGORY_CALL, "(Landroid/view/MenuItem;Landroid/content/Context;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTosAccept$onViewBound$2<T1, T2> implements Action2<MenuItem, Context> {
    public static final WidgetTosAccept$onViewBound$2 INSTANCE = new WidgetTosAccept$onViewBound$2();

    public final void call(MenuItem menuItem, Context context) {
        m.checkNotNullExpressionValue(menuItem, "menuItem");
        if (menuItem.getItemId() == R.id.menu_settings_log_out) {
            StoreStream.Companion.getAuthentication().logout();
        }
    }
}
