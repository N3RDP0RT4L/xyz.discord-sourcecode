package com.discord.widgets.tos;

import com.discord.app.AppViewModel;
import com.discord.widgets.tos.WidgetTosReportViolation;
import com.discord.widgets.tos.WidgetTosReportViolationViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetTosReportViolation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/tos/WidgetTosReportViolationViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTosReportViolation$viewModel$2 extends o implements Function0<AppViewModel<WidgetTosReportViolationViewModel.ViewState>> {
    public final /* synthetic */ WidgetTosReportViolation this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetTosReportViolation$viewModel$2(WidgetTosReportViolation widgetTosReportViolation) {
        super(0);
        this.this$0 = widgetTosReportViolation;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetTosReportViolationViewModel.ViewState> invoke() {
        WidgetTosReportViolation.Companion.Arguments args;
        WidgetTosReportViolation.Companion.Arguments args2;
        args = this.this$0.getArgs();
        long channelId = args.getChannelId();
        args2 = this.this$0.getArgs();
        return new WidgetTosReportViolationViewModel(channelId, args2.getMessageId());
    }
}
