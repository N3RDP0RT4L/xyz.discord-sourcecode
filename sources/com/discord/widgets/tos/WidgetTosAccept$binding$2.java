package com.discord.widgets.tos;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetTosAcceptBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetTosAccept.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetTosAcceptBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetTosAcceptBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetTosAccept$binding$2 extends k implements Function1<View, WidgetTosAcceptBinding> {
    public static final WidgetTosAccept$binding$2 INSTANCE = new WidgetTosAccept$binding$2();

    public WidgetTosAccept$binding$2() {
        super(1, WidgetTosAcceptBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetTosAcceptBinding;", 0);
    }

    public final WidgetTosAcceptBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.alert_tos_ack;
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.alert_tos_ack);
        if (checkBox != null) {
            i = R.id.alert_tos_ack_continue;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.alert_tos_ack_continue);
            if (materialButton != null) {
                i = R.id.alert_tos_ack_wrap;
                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.alert_tos_ack_wrap);
                if (linearLayout != null) {
                    i = R.id.alert_tos_privacy_policy;
                    LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.alert_tos_privacy_policy);
                    if (linkifiedTextView != null) {
                        i = R.id.alert_tos_terms_of_service;
                        LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) view.findViewById(R.id.alert_tos_terms_of_service);
                        if (linkifiedTextView2 != null) {
                            i = R.id.alert_tos_text_description;
                            TextView textView = (TextView) view.findViewById(R.id.alert_tos_text_description);
                            if (textView != null) {
                                i = R.id.alert_tos_text_title;
                                TextView textView2 = (TextView) view.findViewById(R.id.alert_tos_text_title);
                                if (textView2 != null) {
                                    return new WidgetTosAcceptBinding((RelativeLayout) view, checkBox, materialButton, linearLayout, linkifiedTextView, linkifiedTextView2, textView, textView2);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
