package com.discord.widgets.tos;

import androidx.core.app.NotificationCompat;
import com.discord.api.report.ReportReason;
import com.discord.widgets.tos.WidgetTosReportViolationViewModel;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
/* compiled from: WidgetTosReportViolationViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/report/ReportReason;", "kotlin.jvm.PlatformType", "reportReasons", "Lcom/discord/widgets/tos/WidgetTosReportViolationViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Lcom/discord/widgets/tos/WidgetTosReportViolationViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTosReportViolationViewModel$observeStoreState$1<T, R> implements b<List<? extends ReportReason>, WidgetTosReportViolationViewModel.StoreState> {
    public static final WidgetTosReportViolationViewModel$observeStoreState$1 INSTANCE = new WidgetTosReportViolationViewModel$observeStoreState$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ WidgetTosReportViolationViewModel.StoreState call(List<? extends ReportReason> list) {
        return call2((List<ReportReason>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final WidgetTosReportViolationViewModel.StoreState call2(List<ReportReason> list) {
        m.checkNotNullExpressionValue(list, "reportReasons");
        return new WidgetTosReportViolationViewModel.StoreState.ReportReasons(list);
    }
}
