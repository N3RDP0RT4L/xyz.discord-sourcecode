package com.discord.widgets.media;

import android.os.Handler;
import com.discord.databinding.WidgetQrScannerBinding;
import d0.z.d.o;
import e0.a.a.a.b;
import e0.a.a.a.c;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
/* compiled from: WidgetQRScanner.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetQRScanner$onViewBoundOrOnResume$1 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetQRScanner this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetQRScanner$onViewBoundOrOnResume$1(WidgetQRScanner widgetQRScanner) {
        super(0);
        this.this$0 = widgetQRScanner;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetQrScannerBinding binding;
        binding = this.this$0.getBinding();
        ZXingScannerView zXingScannerView = binding.f2492b;
        if (zXingScannerView.n == null) {
            zXingScannerView.n = new c(zXingScannerView);
        }
        c cVar = zXingScannerView.n;
        Objects.requireNonNull(cVar);
        new Handler(cVar.getLooper()).post(new b(cVar, 0));
    }
}
