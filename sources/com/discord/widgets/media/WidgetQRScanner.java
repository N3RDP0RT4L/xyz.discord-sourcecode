package com.discord.widgets.media;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetQrScannerBinding;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.chip.Chip;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import xyz.discord.R;
/* compiled from: WidgetQRScanner.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u00182\u00020\u00012\u00020\u0002:\u0001\u0018B\u0007¢\u0006\u0004\b\u0017\u0010\tJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\n\u0010\tJ\u0017\u0010\r\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u0011\u001a\u00020\u00052\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012R\u001d\u0010\u0004\u001a\u00020\u00038B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/media/WidgetQRScanner;", "Lcom/discord/app/AppFragment;", "Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;", "Lcom/discord/databinding/WidgetQrScannerBinding;", "binding", "", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetQrScannerBinding;)V", "onViewBoundOrOnResume", "()V", "onPause", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/google/zxing/Result;", "rawResult", "handleResult", "(Lcom/google/zxing/Result;)V", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetQrScannerBinding;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetQRScanner extends AppFragment implements ZXingScannerView.b {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetQRScanner.class, "binding", "getBinding()Lcom/discord/databinding/WidgetQrScannerBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_SHOW_HELP_CHIP = "SHOW_HELP_CHIP";
    private static final int MAIN_BACK_CAMERA = 0;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetQRScanner$binding$2.INSTANCE, new WidgetQRScanner$binding$3(this));

    /* compiled from: WidgetQRScanner.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/media/WidgetQRScanner$Companion;", "", "Landroid/content/Context;", "context", "", "showHelpChip", "", "launch", "(Landroid/content/Context;Z)V", "", "EXTRA_SHOW_HELP_CHIP", "Ljava/lang/String;", "", "MAIN_BACK_CAMERA", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            companion.launch(context, z2);
        }

        public final void launch(Context context, boolean z2) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent("android.intent.action.VIEW").putExtra(WidgetQRScanner.EXTRA_SHOW_HELP_CHIP, z2);
            m.checkNotNullExpressionValue(putExtra, "Intent(Intent.ACTION_VIE…_HELP_CHIP, showHelpChip)");
            j.d(context, WidgetQRScanner.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetQRScanner() {
        super(R.layout.widget_qr_scanner);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetQrScannerBinding getBinding() {
        return (WidgetQrScannerBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onViewBindingDestroy(WidgetQrScannerBinding widgetQrScannerBinding) {
        widgetQrScannerBinding.f2492b.a();
    }

    /* JADX WARN: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r0.a(r7.getHost()) == false) goto L9;
     */
    @Override // me.dm7.barcodescanner.zxing.ZXingScannerView.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void handleResult(com.google.zxing.Result r7) {
        /*
            r6 = this;
            if (r7 == 0) goto L73
            java.lang.String r2 = r7.a
            if (r2 == 0) goto L73
            android.net.Uri r7 = android.net.Uri.parse(r2)
            boolean r0 = android.webkit.URLUtil.isValidUrl(r2)
            java.lang.String r1 = "uri"
            if (r0 == 0) goto L21
            b.a.d.l0.a r0 = b.a.d.l0.a.F
            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
            java.lang.String r3 = r7.getHost()
            boolean r0 = r0.a(r3)
            if (r0 != 0) goto L2c
        L21:
            com.discord.utilities.intent.IntentUtils r0 = com.discord.utilities.intent.IntentUtils.INSTANCE
            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
            boolean r0 = r0.isDiscordAppUri(r7)
            if (r0 == 0) goto L64
        L2c:
            java.lang.String r7 = r7.getPath()
            if (r7 == 0) goto L40
            b.a.d.l0.a r0 = b.a.d.l0.a.F
            kotlin.text.Regex r0 = b.a.d.l0.a.C
            java.lang.String r1 = "it"
            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
            kotlin.text.MatchResult r7 = r0.matchEntire(r7)
            goto L41
        L40:
            r7 = 0
        L41:
            if (r7 == 0) goto L57
            java.util.List r7 = r7.getGroupValues()
            java.lang.Object r7 = d0.t.u.last(r7)
            java.lang.String r7 = (java.lang.String) r7
            com.discord.widgets.auth.WidgetRemoteAuth$Companion r0 = com.discord.widgets.auth.WidgetRemoteAuth.Companion
            android.content.Context r1 = r6.requireContext()
            r0.launch(r1, r7)
            goto L6c
        L57:
            com.discord.utilities.uri.UriHandler r0 = com.discord.utilities.uri.UriHandler.INSTANCE
            android.content.Context r1 = r6.requireContext()
            r3 = 0
            r4 = 4
            r5 = 0
            com.discord.utilities.uri.UriHandler.handle$default(r0, r1, r2, r3, r4, r5)
            goto L6c
        L64:
            r7 = 2131893342(0x7f121c5e, float:1.9421458E38)
            r0 = 0
            r1 = 4
            b.a.d.m.i(r6, r7, r0, r1)
        L6c:
            androidx.fragment.app.FragmentActivity r7 = r6.requireActivity()
            r7.finish()
        L73:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.media.WidgetQRScanner.handleResult(com.google.zxing.Result):void");
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        getBinding().f2492b.a();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        getBinding().f2492b.setFormats(ZXingScannerView.D);
        getBinding().f2492b.setResultHandler(this);
        ZXingScannerView zXingScannerView = getBinding().f2492b;
        m.checkNotNullExpressionValue(zXingScannerView, "binding.qrScanner");
        zXingScannerView.setVisibility(0);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        int i = 0;
        ColorCompat.setStatusBarColor$default((Fragment) this, ColorCompat.getColor(this, (int) R.color.black), false, 4, (Object) null);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.login_with_qr);
        boolean booleanExtra = getMostRecentIntent().getBooleanExtra(EXTRA_SHOW_HELP_CHIP, false);
        Chip chip = getBinding().c;
        m.checkNotNullExpressionValue(chip, "binding.qrScannerChip");
        if (!booleanExtra) {
            i = 8;
        }
        chip.setVisibility(i);
        requestCameraQRScanner(new WidgetQRScanner$onViewBoundOrOnResume$1(this), new WidgetQRScanner$onViewBoundOrOnResume$2(this));
    }
}
