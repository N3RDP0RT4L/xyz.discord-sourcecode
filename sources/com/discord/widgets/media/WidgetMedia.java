package com.discord.widgets.media;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Parcelable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.p.i;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.f.g.c.c;
import b.f.g.e.v;
import b.f.l.b.e;
import b.i.a.c.k2;
import b.i.a.c.u0;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.api.message.attachment.MessageAttachmentType;
import com.discord.api.message.embed.EmbedType;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetMediaBinding;
import com.discord.player.AppMediaPlayer;
import com.discord.player.MediaSource;
import com.discord.player.MediaType;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.display.DisplayUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.samples.zoomable.ZoomableDraweeView;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.appbar.AppBarLayout;
import d0.g0.t;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetMedia.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 M2\u00020\u0001:\u0002MNB\u0007¢\u0006\u0004\bL\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0004J\u0017\u0010\t\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000e\u0010\u0004J\u000f\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0016\u001a\u00020\u0015*\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u001c\u0010\u0004J\u000f\u0010\u001d\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u001d\u0010\u0004J\u000f\u0010\u001e\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u001e\u0010\u0004J\u0017\u0010!\u001a\u00020\u00022\u0006\u0010 \u001a\u00020\u001fH\u0002¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\u00022\u0006\u0010$\u001a\u00020#H\u0016¢\u0006\u0004\b%\u0010&J\u000f\u0010'\u001a\u00020\u0002H\u0016¢\u0006\u0004\b'\u0010\u0004J\u000f\u0010(\u001a\u00020\u0002H\u0016¢\u0006\u0004\b(\u0010\u0004J\u000f\u0010)\u001a\u00020\u0002H\u0016¢\u0006\u0004\b)\u0010\u0004R\u0018\u0010*\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u001d\u00104\u001a\u00020/8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R\u0016\u00105\u001a\u00020\u00138\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b5\u00106R\u0018\u00108\u001a\u0004\u0018\u0001078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109R\u0018\u0010;\u001a\u0004\u0018\u00010:8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b;\u0010<R\u001d\u0010 \u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@R\u0018\u0010A\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010C\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010.R\u0018\u0010E\u001a\u0004\u0018\u00010D8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bE\u0010FR\u0016\u0010H\u001a\u00020G8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bH\u0010IR\u0016\u0010J\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bJ\u0010K¨\u0006O"}, d2 = {"Lcom/discord/widgets/media/WidgetMedia;", "Lcom/discord/app/AppFragment;", "", "onMediaClick", "()V", "showControls", "hideControls", "Landroid/animation/ValueAnimator;", "animator", "configureAndStartControlsAnimation", "(Landroid/animation/ValueAnimator;)V", "", "getToolbarTranslationY", "()F", "configureMediaImage", "", "isVideo", "()Z", "Landroid/content/Context;", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "getFormattedUrl", "(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;", "Lcom/discord/player/AppMediaPlayer$Event;", "event", "handlePlayerEvent", "(Lcom/discord/player/AppMediaPlayer$Event;)V", "handleImageProgressComplete", "showLoadingIndicator", "hideLoadingIndicator", "Lcom/discord/databinding/WidgetMediaBinding;", "binding", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetMediaBinding;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onPause", "onDestroy", "videoUrl", "Ljava/lang/String;", "", "playerControlsHeight", "I", "Lcom/discord/widgets/media/WidgetMediaViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/media/WidgetMediaViewModel;", "viewModel", "imageUri", "Landroid/net/Uri;", "Lcom/discord/player/MediaSource;", "mediaSource", "Lcom/discord/player/MediaSource;", "Lrx/Subscription;", "controlsVisibilitySubscription", "Lrx/Subscription;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetMediaBinding;", "controlsAnimator", "Landroid/animation/ValueAnimator;", "toolbarHeight", "Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;", "controlsAnimationAction", "Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;", "Lcom/discord/player/AppMediaPlayer;", "appMediaPlayer", "Lcom/discord/player/AppMediaPlayer;", "playerPausedByFragmentLifecycle", "Z", HookHelper.constructorName, "Companion", "ControlsAnimationAction", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMedia extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetMedia.class, "binding", "getBinding()Lcom/discord/databinding/WidgetMediaBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_HEIGHT = "INTENT_MEDIA_HEIGHT";
    private static final String INTENT_IMAGE_URL = "INTENT_IMAGE_URL";
    private static final String INTENT_MEDIA_SOURCE = "INTENT_MEDIA_SOURCE";
    private static final String INTENT_TITLE = "INTENT_TITLE";
    private static final String INTENT_URL = "INTENT_MEDIA_URL";
    private static final String INTENT_WIDTH = "INTENT_MEDIA_WIDTH";
    private static final long SHOW_CONTROLS_TIMEOUT_MS = 3000;
    private static final long VERTICAL_CONTROLS_ANIMATION_DURATION_MS = 200;
    private AppMediaPlayer appMediaPlayer;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetMedia$binding$2.INSTANCE, new WidgetMedia$binding$3(this));
    private ControlsAnimationAction controlsAnimationAction;
    private ValueAnimator controlsAnimator;
    private Subscription controlsVisibilitySubscription;
    private Uri imageUri;
    private MediaSource mediaSource;
    private int playerControlsHeight;
    private boolean playerPausedByFragmentLifecycle;
    private int toolbarHeight;
    private String videoUrl;
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetMedia.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\"\u0010#J]\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00042\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\u000b\u001a\u0004\u0018\u00010\t2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001d\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u000f\u0010\u0013J\u001d\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u000f\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0018R\u0016\u0010\u001b\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0018R\u0016\u0010\u001c\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u0018R\u0016\u0010\u001d\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u0018R\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010!\u001a\u00020\u001e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/widgets/media/WidgetMedia$Companion;", "", "Landroid/content/Context;", "context", "", "title", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "progressiveMediaUri", "previewImageUri", "", "width", "height", "Lcom/discord/player/MediaType;", "mediaType", "", "launch", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/player/MediaType;)V", "Lcom/discord/api/message/attachment/MessageAttachment;", "attachment", "(Landroid/content/Context;Lcom/discord/api/message/attachment/MessageAttachment;)V", "Lcom/discord/api/message/embed/MessageEmbed;", "embed", "(Landroid/content/Context;Lcom/discord/api/message/embed/MessageEmbed;)V", "INTENT_HEIGHT", "Ljava/lang/String;", WidgetMedia.INTENT_IMAGE_URL, WidgetMedia.INTENT_MEDIA_SOURCE, WidgetMedia.INTENT_TITLE, "INTENT_URL", "INTENT_WIDTH", "", "SHOW_CONTROLS_TIMEOUT_MS", "J", "VERTICAL_CONTROLS_ANIMATION_DURATION_MS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;
            public static final /* synthetic */ int[] $EnumSwitchMapping$1;

            static {
                MessageAttachmentType.values();
                int[] iArr = new int[3];
                $EnumSwitchMapping$0 = iArr;
                iArr[MessageAttachmentType.VIDEO.ordinal()] = 1;
                EmbedType.values();
                int[] iArr2 = new int[11];
                $EnumSwitchMapping$1 = iArr2;
                iArr2[EmbedType.VIDEO.ordinal()] = 1;
                iArr2[EmbedType.GIFV.ordinal()] = 2;
            }
        }

        private Companion() {
        }

        public final void launch(Context context, MessageAttachment messageAttachment) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(messageAttachment, "attachment");
            MessageAttachmentType e = messageAttachment.e();
            MediaType mediaType = null;
            String c = e.ordinal() != 0 ? null : messageAttachment.c();
            String a = messageAttachment.a();
            String c2 = messageAttachment.c();
            String c3 = messageAttachment.c();
            Integer g = messageAttachment.g();
            Integer b2 = messageAttachment.b();
            if (e == MessageAttachmentType.VIDEO) {
                mediaType = MediaType.VIDEO;
            }
            launch(context, a, c2, c, c3, g, b2, mediaType);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* JADX WARN: Removed duplicated region for block: B:19:0x0044  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x0047  */
        /* JADX WARN: Removed duplicated region for block: B:23:0x0054  */
        /* JADX WARN: Removed duplicated region for block: B:24:0x0058  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x005b  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x005f  */
        /* JADX WARN: Removed duplicated region for block: B:29:0x0062  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final void launch(android.content.Context r12, com.discord.api.message.embed.MessageEmbed r13) {
            /*
                r11 = this;
                java.lang.String r0 = "context"
                d0.z.d.m.checkNotNullParameter(r12, r0)
                java.lang.String r0 = "embed"
                d0.z.d.m.checkNotNullParameter(r13, r0)
                com.discord.api.message.embed.EmbedType r0 = r13.k()
                r1 = 0
                if (r0 != 0) goto L12
                goto L1c
            L12:
                int r0 = r0.ordinal()
                r2 = 2
                if (r0 == r2) goto L1e
                r2 = 7
                if (r0 == r2) goto L1e
            L1c:
                r6 = r1
                goto L36
            L1e:
                com.discord.api.message.embed.EmbedVideo r0 = r13.m()
                if (r0 == 0) goto L2b
                java.lang.String r0 = r0.b()
                if (r0 == 0) goto L2b
                goto L35
            L2b:
                com.discord.api.message.embed.EmbedVideo r0 = r13.m()
                if (r0 == 0) goto L1c
                java.lang.String r0 = r0.c()
            L35:
                r6 = r0
            L36:
                com.discord.utilities.embed.EmbedResourceUtils r0 = com.discord.utilities.embed.EmbedResourceUtils.INSTANCE
                com.discord.embed.RenderableEmbedMedia r0 = r0.getPreviewImage(r13)
                com.discord.api.message.embed.EmbedType r2 = r13.k()
                com.discord.api.message.embed.EmbedType r3 = com.discord.api.message.embed.EmbedType.GIFV
                if (r2 != r3) goto L47
                com.discord.player.MediaType r2 = com.discord.player.MediaType.GIFV
                goto L49
            L47:
                com.discord.player.MediaType r2 = com.discord.player.MediaType.VIDEO
            L49:
                r10 = r2
                java.lang.String r4 = r13.j()
                java.lang.String r5 = r13.l()
                if (r0 == 0) goto L58
                java.lang.String r13 = r0.a
                r7 = r13
                goto L59
            L58:
                r7 = r1
            L59:
                if (r0 == 0) goto L5f
                java.lang.Integer r13 = r0.f2679b
                r8 = r13
                goto L60
            L5f:
                r8 = r1
            L60:
                if (r0 == 0) goto L64
                java.lang.Integer r1 = r0.c
            L64:
                r9 = r1
                r2 = r11
                r3 = r12
                r2.launch(r3, r4, r5, r6, r7, r8, r9, r10)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.media.WidgetMedia.Companion.launch(android.content.Context, com.discord.api.message.embed.MessageEmbed):void");
        }

        private final void launch(Context context, String str, String str2, String str3, String str4, Integer num, Integer num2, MediaType mediaType) {
            MediaSource P = (str3 == null || mediaType == null) ? null : d.P(mediaType, str3, "javaClass");
            Intent putExtra = new Intent().putExtra(WidgetMedia.INTENT_TITLE, str);
            if (str2 == null) {
                str2 = str4;
            }
            Intent putExtra2 = putExtra.putExtra(WidgetMedia.INTENT_URL, str2).putExtra(WidgetMedia.INTENT_IMAGE_URL, str4).putExtra(WidgetMedia.INTENT_WIDTH, num).putExtra(WidgetMedia.INTENT_HEIGHT, num2).putExtra(WidgetMedia.INTENT_MEDIA_SOURCE, P);
            m.checkNotNullExpressionValue(putExtra2, "Intent()\n          .putE…EDIA_SOURCE, mediaSource)");
            j.d(context, WidgetMedia.class, putExtra2);
        }
    }

    /* compiled from: WidgetMedia.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "SHOW", "HIDE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ControlsAnimationAction {
        SHOW,
        HIDE
    }

    public WidgetMedia() {
        super(R.layout.widget_media);
        WidgetMedia$viewModel$2 widgetMedia$viewModel$2 = WidgetMedia$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetMediaViewModel.class), new WidgetMedia$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetMedia$viewModel$2));
    }

    private final void configureAndStartControlsAnimation(ValueAnimator valueAnimator) {
        valueAnimator.setInterpolator(new FastOutSlowInInterpolator());
        valueAnimator.setDuration(VERTICAL_CONTROLS_ANIMATION_DURATION_MS);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: com.discord.widgets.media.WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                WidgetMediaBinding binding;
                boolean isVideo;
                int i;
                int i2;
                int i3;
                WidgetMediaBinding binding2;
                m.checkNotNullExpressionValue(valueAnimator2, "animator");
                Object animatedValue = valueAnimator2.getAnimatedValue();
                Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Float");
                float floatValue = ((Float) animatedValue).floatValue();
                binding = WidgetMedia.this.getBinding();
                AppBarLayout appBarLayout = binding.f2463b;
                m.checkNotNullExpressionValue(appBarLayout, "binding.actionBarToolbarLayout");
                appBarLayout.setTranslationY(floatValue);
                isVideo = WidgetMedia.this.isVideo();
                if (isVideo) {
                    i = WidgetMedia.this.playerControlsHeight;
                    if (i > 0) {
                        i2 = WidgetMedia.this.toolbarHeight;
                        i3 = WidgetMedia.this.playerControlsHeight;
                        float f = (-floatValue) / (i2 / i3);
                        binding2 = WidgetMedia.this.getBinding();
                        PlayerControlView playerControlView = binding2.f;
                        m.checkNotNullExpressionValue(playerControlView, "binding.mediaPlayerControlView");
                        playerControlView.setTranslationY(f);
                    }
                }
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() { // from class: com.discord.widgets.media.WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$2
            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                m.checkNotNullParameter(animator, "animation");
                WidgetMedia.this.controlsAnimationAction = null;
            }
        });
        valueAnimator.start();
    }

    private final void configureMediaImage() {
        getBinding().d.setIsLongpressEnabled(false);
        getBinding().d.setTapListener(new GestureDetector.SimpleOnGestureListener() { // from class: com.discord.widgets.media.WidgetMedia$configureMediaImage$1
            private boolean mDoubleTapScroll;
            private final long DURATION_MS = 300;
            private final long DOUBLE_TAP_SCROLL_THRESHOLD = 20;
            private final PointF mDoubleTapViewPoint = new PointF();
            private final PointF mDoubleTapImagePoint = new PointF();
            private float mDoubleTapScale = 1.0f;

            private final float calcScale(PointF pointF) {
                float f = pointF.y - this.mDoubleTapViewPoint.y;
                float abs = (Math.abs(f) * 0.001f) + 1;
                if (f < 0) {
                    return this.mDoubleTapScale / abs;
                }
                return this.mDoubleTapScale * abs;
            }

            private final boolean shouldStartDoubleTapScroll(PointF pointF) {
                float f = pointF.x;
                PointF pointF2 = this.mDoubleTapViewPoint;
                return ((float) Math.hypot((double) (f - pointF2.x), (double) (pointF.y - pointF2.y))) > ((float) this.DOUBLE_TAP_SCROLL_THRESHOLD);
            }

            public final PointF getMDoubleTapImagePoint() {
                return this.mDoubleTapImagePoint;
            }

            public final float getMDoubleTapScale() {
                return this.mDoubleTapScale;
            }

            public final boolean getMDoubleTapScroll() {
                return this.mDoubleTapScroll;
            }

            public final PointF getMDoubleTapViewPoint() {
                return this.mDoubleTapViewPoint;
            }

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
            public boolean onDoubleTapEvent(MotionEvent motionEvent) {
                WidgetMediaBinding binding;
                m.checkNotNullParameter(motionEvent, "e");
                binding = WidgetMedia.this.getBinding();
                ZoomableDraweeView zoomableDraweeView = binding.d;
                m.checkNotNullExpressionValue(zoomableDraweeView, "binding.mediaImage");
                e zoomableController = zoomableDraweeView.getZoomableController();
                Objects.requireNonNull(zoomableController, "null cannot be cast to non-null type com.facebook.samples.zoomable.AbstractAnimatedZoomableController");
                b.f.l.b.a aVar = (b.f.l.b.a) zoomableController;
                PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
                float[] fArr = aVar.j;
                boolean z2 = false;
                fArr[0] = pointF.x;
                fArr[1] = pointF.y;
                aVar.h.invert(aVar.i);
                aVar.i.mapPoints(fArr, 0, fArr, 0, 1);
                for (int i = 0; i < 1; i++) {
                    int i2 = i * 2;
                    int i3 = i2 + 0;
                    float f = fArr[i3];
                    RectF rectF = aVar.e;
                    fArr[i3] = (f - rectF.left) / rectF.width();
                    int i4 = i2 + 1;
                    float f2 = fArr[i4];
                    RectF rectF2 = aVar.e;
                    fArr[i4] = (f2 - rectF2.top) / rectF2.height();
                }
                PointF pointF2 = new PointF(fArr[0], fArr[1]);
                int actionMasked = motionEvent.getActionMasked();
                if (actionMasked == 0) {
                    this.mDoubleTapViewPoint.set(pointF);
                    this.mDoubleTapImagePoint.set(pointF2);
                    this.mDoubleTapScale = aVar.e();
                } else if (actionMasked == 1) {
                    if (this.mDoubleTapScroll) {
                        aVar.p(calcScale(pointF), this.mDoubleTapImagePoint, this.mDoubleTapViewPoint, 7, 0L, null);
                    } else if (aVar.e() < 3.0f / 2) {
                        aVar.p(2.0f, pointF2, pointF, 7, this.DURATION_MS, null);
                    } else {
                        aVar.p(1.0f, pointF2, pointF, 7, this.DURATION_MS, null);
                    }
                    this.mDoubleTapScroll = false;
                } else if (actionMasked == 2) {
                    if (this.mDoubleTapScroll || shouldStartDoubleTapScroll(pointF)) {
                        z2 = true;
                    }
                    this.mDoubleTapScroll = z2;
                    if (z2) {
                        aVar.p(calcScale(pointF), this.mDoubleTapImagePoint, this.mDoubleTapViewPoint, 7, 0L, null);
                    }
                }
                return true;
            }

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
            public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
                m.checkNotNullParameter(motionEvent, "e");
                WidgetMedia.this.onMediaClick();
                return true;
            }

            public final void setMDoubleTapScale(float f) {
                this.mDoubleTapScale = f;
            }

            public final void setMDoubleTapScroll(boolean z2) {
                this.mDoubleTapScroll = z2;
            }
        });
        ZoomableDraweeView zoomableDraweeView = getBinding().d;
        m.checkNotNullExpressionValue(zoomableDraweeView, "binding.mediaImage");
        ScalingUtils$ScaleType scalingUtils$ScaleType = ScalingUtils$ScaleType.a;
        ScalingUtils$ScaleType scalingUtils$ScaleType2 = v.l;
        m.checkNotNullExpressionValue(scalingUtils$ScaleType2, "ScalingUtils.ScaleType.FIT_CENTER");
        MGImages.setScaleType(zoomableDraweeView, scalingUtils$ScaleType2);
        ZoomableDraweeView zoomableDraweeView2 = getBinding().d;
        m.checkNotNullExpressionValue(zoomableDraweeView2, "binding.mediaImage");
        ZoomableDraweeView zoomableDraweeView3 = getBinding().d;
        m.checkNotNullExpressionValue(zoomableDraweeView3, "binding.mediaImage");
        Context context = zoomableDraweeView3.getContext();
        m.checkNotNullExpressionValue(context, "binding.mediaImage.context");
        Uri uri = this.imageUri;
        if (uri == null) {
            m.throwUninitializedPropertyAccessException("imageUri");
        }
        MGImages.setImage$default(zoomableDraweeView2, d0.t.m.listOf(getFormattedUrl(context, uri)), 0, 0, false, null, null, new c<ImageInfo>() { // from class: com.discord.widgets.media.WidgetMedia$configureMediaImage$2
            @Override // b.f.g.c.c, com.facebook.drawee.controller.ControllerListener
            public void onFailure(String str, Throwable th) {
                super.onFailure(str, th);
                WidgetMedia.this.handleImageProgressComplete();
            }

            public void onFinalImageSet(String str, ImageInfo imageInfo, Animatable animatable) {
                super.onFinalImageSet(str, (String) imageInfo, animatable);
                WidgetMedia.this.handleImageProgressComplete();
            }
        }, 124, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetMediaBinding getBinding() {
        return (WidgetMediaBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final String getFormattedUrl(Context context, Uri uri) {
        String str;
        Rect resizeToFitScreen = DisplayUtils.resizeToFitScreen(context, new Rect(0, 0, getMostRecentIntent().getIntExtra(INTENT_WIDTH, 0), getMostRecentIntent().getIntExtra(INTENT_HEIGHT, 0)));
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment == null || !t.endsWith$default(lastPathSegment, ".gif", false, 2, null)) {
            StringBuilder R = a.R("&format=");
            R.append(StringUtilsKt.getSTATIC_IMAGE_EXTENSION());
            str = R.toString();
        } else {
            str = "";
        }
        return uri + "?width=" + resizeToFitScreen.width() + "&height=" + resizeToFitScreen.height() + str;
    }

    private final float getToolbarTranslationY() {
        AppBarLayout appBarLayout = getBinding().f2463b;
        m.checkNotNullExpressionValue(appBarLayout, "binding.actionBarToolbarLayout");
        return appBarLayout.getTranslationY();
    }

    private final WidgetMediaViewModel getViewModel() {
        return (WidgetMediaViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleImageProgressComplete() {
        if (this.videoUrl == null) {
            hideLoadingIndicator();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handlePlayerEvent(AppMediaPlayer.Event event) {
        if (m.areEqual(event, AppMediaPlayer.Event.b.a)) {
            showLoadingIndicator();
        } else if (m.areEqual(event, AppMediaPlayer.Event.a.a)) {
            ZoomableDraweeView zoomableDraweeView = getBinding().d;
            m.checkNotNullExpressionValue(zoomableDraweeView, "binding.mediaImage");
            zoomableDraweeView.setVisibility(8);
            getViewModel().setShowCoverFrame(false);
            hideLoadingIndicator();
        } else if (event instanceof AppMediaPlayer.Event.c) {
            getViewModel().setCurrentPlayerPositionMs(((AppMediaPlayer.Event.c) event).a);
        } else if (m.areEqual(event, AppMediaPlayer.Event.d.a)) {
            if (!this.playerPausedByFragmentLifecycle) {
                getViewModel().setPlaying(false);
            }
        } else if (m.areEqual(event, AppMediaPlayer.Event.f.a)) {
            getViewModel().setPlaying(true);
        } else if (m.areEqual(event, AppMediaPlayer.Event.e.a)) {
            hideLoadingIndicator();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void hideControls() {
        ControlsAnimationAction controlsAnimationAction = this.controlsAnimationAction;
        ControlsAnimationAction controlsAnimationAction2 = ControlsAnimationAction.HIDE;
        if (controlsAnimationAction != controlsAnimationAction2) {
            this.controlsAnimationAction = controlsAnimationAction2;
            ValueAnimator valueAnimator = this.controlsAnimator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(getToolbarTranslationY(), -this.toolbarHeight);
            m.checkNotNullExpressionValue(ofFloat, "this");
            configureAndStartControlsAnimation(ofFloat);
            this.controlsAnimator = ofFloat;
        }
    }

    private final void hideLoadingIndicator() {
        ProgressBar progressBar = getBinding().e;
        m.checkNotNullExpressionValue(progressBar, "binding.mediaLoadingIndicator");
        progressBar.setVisibility(8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean isVideo() {
        MediaSource mediaSource = this.mediaSource;
        return (mediaSource != null ? mediaSource.l : null) == MediaType.VIDEO;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onMediaClick() {
        if (getToolbarTranslationY() < 0.0f) {
            showControls();
        } else {
            hideControls();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onViewBindingDestroy(WidgetMediaBinding widgetMediaBinding) {
        ValueAnimator valueAnimator = this.controlsAnimator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        widgetMediaBinding.d.setTapListener(null);
        MGImages mGImages = MGImages.INSTANCE;
        ZoomableDraweeView zoomableDraweeView = widgetMediaBinding.d;
        m.checkNotNullExpressionValue(zoomableDraweeView, "binding.mediaImage");
        mGImages.cancelImageRequests(zoomableDraweeView);
    }

    private final void showControls() {
        if (isVideo()) {
            getBinding().f.i();
            Subscription subscription = this.controlsVisibilitySubscription;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            Observable<Long> d02 = Observable.d0(SHOW_CONTROLS_TIMEOUT_MS, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable.timer(SHOW_CO…S, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), WidgetMedia.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetMedia$showControls$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMedia$showControls$2(this));
        } else {
            getBinding().f.c();
        }
        ControlsAnimationAction controlsAnimationAction = this.controlsAnimationAction;
        ControlsAnimationAction controlsAnimationAction2 = ControlsAnimationAction.SHOW;
        if (controlsAnimationAction != controlsAnimationAction2) {
            this.controlsAnimationAction = controlsAnimationAction2;
            ValueAnimator valueAnimator = this.controlsAnimator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(getToolbarTranslationY(), 0.0f);
            m.checkNotNullExpressionValue(ofFloat, "this");
            configureAndStartControlsAnimation(ofFloat);
            this.controlsAnimator = ofFloat;
        }
    }

    private final void showLoadingIndicator() {
        ProgressBar progressBar = getBinding().e;
        m.checkNotNullExpressionValue(progressBar, "binding.mediaLoadingIndicator");
        progressBar.setVisibility(0);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        AppMediaPlayer appMediaPlayer = this.appMediaPlayer;
        if (appMediaPlayer == null) {
            m.throwUninitializedPropertyAccessException("appMediaPlayer");
        }
        appMediaPlayer.c();
        super.onDestroy();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        AppMediaPlayer appMediaPlayer = this.appMediaPlayer;
        if (appMediaPlayer == null) {
            m.throwUninitializedPropertyAccessException("appMediaPlayer");
        }
        if (((u0) appMediaPlayer.f).z()) {
            this.playerPausedByFragmentLifecycle = true;
            AppMediaPlayer appMediaPlayer2 = this.appMediaPlayer;
            if (appMediaPlayer2 == null) {
                m.throwUninitializedPropertyAccessException("appMediaPlayer");
            }
            ((k2) appMediaPlayer2.f).u(false);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        this.appMediaPlayer = i.a(requireContext());
        ColorCompat.getThemedColor(this, (int) R.attr.primary_900);
        ColorCompat.setStatusBarColor$default((Fragment) this, ColorCompat.getThemedColor(this, (int) R.attr.primary_900), false, 4, (Object) null);
        PlayerView playerView = getBinding().g;
        m.checkNotNullExpressionValue(playerView, "binding.mediaPlayerView");
        View videoSurfaceView = playerView.getVideoSurfaceView();
        if (videoSurfaceView != null) {
            videoSurfaceView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.media.WidgetMedia$onViewBound$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WidgetMedia.this.onMediaClick();
                }
            });
        }
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.media.WidgetMedia$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetMedia.this.onMediaClick();
            }
        });
        AppBarLayout appBarLayout = getBinding().f2463b;
        m.checkNotNullExpressionValue(appBarLayout, "binding.actionBarToolbarLayout");
        ViewExtensions.addOnHeightChangedListener(appBarLayout, new WidgetMedia$onViewBound$3(this));
        PlayerControlView playerControlView = getBinding().f;
        m.checkNotNullExpressionValue(playerControlView, "binding.mediaPlayerControlView");
        ViewExtensions.addOnHeightChangedListener(playerControlView, new WidgetMedia$onViewBound$4(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        Uri uri;
        String str;
        Uri uri2;
        super.onViewBoundOrOnResume();
        ZoomableDraweeView zoomableDraweeView = getBinding().d;
        m.checkNotNullExpressionValue(zoomableDraweeView, "binding.mediaImage");
        int i = 8;
        boolean z2 = false;
        zoomableDraweeView.setVisibility(getViewModel().getShowCoverFrame() ? 0 : 8);
        ProgressBar progressBar = getBinding().e;
        m.checkNotNullExpressionValue(progressBar, "binding.mediaLoadingIndicator");
        if (getViewModel().getShowCoverFrame()) {
            i = 0;
        }
        progressBar.setVisibility(i);
        Uri parse = Uri.parse(getMostRecentIntent().getStringExtra(INTENT_URL));
        Uri parse2 = Uri.parse(getMostRecentIntent().getStringExtra(INTENT_IMAGE_URL));
        m.checkNotNullExpressionValue(parse2, "Uri.parse(mostRecentInte…gExtra(INTENT_IMAGE_URL))");
        this.imageUri = parse2;
        Parcelable parcelableExtra = getMostRecentIntent().getParcelableExtra(INTENT_MEDIA_SOURCE);
        if (!(parcelableExtra instanceof MediaSource)) {
            parcelableExtra = null;
        }
        MediaSource mediaSource = (MediaSource) parcelableExtra;
        this.mediaSource = mediaSource;
        String uri3 = (mediaSource == null || (uri2 = mediaSource.j) == null) ? null : uri2.toString();
        this.videoUrl = uri3;
        if (uri3 != null) {
            uri = Uri.parse(uri3);
        } else {
            uri = this.imageUri;
            if (uri == null) {
                m.throwUninitializedPropertyAccessException("imageUri");
            }
        }
        Uri uri4 = uri;
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_TITLE);
        if (stringExtra == null || t.isBlank(stringExtra)) {
            String uri5 = parse.toString();
            m.checkNotNullExpressionValue(uri5, "sourceUri.toString()");
            str = uri5;
        } else {
            str = stringExtra;
        }
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.view_embed);
        setActionBarSubtitle(str);
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_media, new WidgetMedia$onViewBoundOrOnResume$1(this, parse, uri4, stringExtra, str), null, 4, null);
        configureMediaImage();
        showControls();
        this.playerPausedByFragmentLifecycle = false;
        MediaSource mediaSource2 = this.mediaSource;
        if (mediaSource2 != null) {
            if (mediaSource2.l == MediaType.GIFV) {
                z2 = true;
            }
            AppMediaPlayer appMediaPlayer = this.appMediaPlayer;
            if (appMediaPlayer == null) {
                m.throwUninitializedPropertyAccessException("appMediaPlayer");
            }
            boolean isPlaying = getViewModel().isPlaying();
            long currentPlayerPositionMs = getViewModel().getCurrentPlayerPositionMs();
            PlayerView playerView = getBinding().g;
            m.checkNotNullExpressionValue(playerView, "binding.mediaPlayerView");
            appMediaPlayer.a(mediaSource2, isPlaying, z2, currentPlayerPositionMs, playerView, getBinding().f);
            if (z2) {
                AppMediaPlayer appMediaPlayer2 = this.appMediaPlayer;
                if (appMediaPlayer2 == null) {
                    m.throwUninitializedPropertyAccessException("appMediaPlayer");
                }
                appMediaPlayer2.d(0.0f);
            } else {
                AppMediaPlayer appMediaPlayer3 = this.appMediaPlayer;
                if (appMediaPlayer3 == null) {
                    m.throwUninitializedPropertyAccessException("appMediaPlayer");
                }
                appMediaPlayer3.d(1.0f);
            }
            AppMediaPlayer appMediaPlayer4 = this.appMediaPlayer;
            if (appMediaPlayer4 == null) {
                m.throwUninitializedPropertyAccessException("appMediaPlayer");
            }
            Observable<AppMediaPlayer.Event> J = appMediaPlayer4.a.J();
            m.checkNotNullExpressionValue(J, "eventSubject.onBackpressureBuffer()");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(J, this, null, 2, null), WidgetMedia.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMedia$onViewBoundOrOnResume$$inlined$let$lambda$1(this));
        }
    }
}
