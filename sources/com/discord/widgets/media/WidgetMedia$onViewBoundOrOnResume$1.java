package com.discord.widgets.media;

import android.content.Context;
import android.net.Uri;
import android.view.MenuItem;
import androidx.core.app.NotificationCompat;
import b.a.k.b;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.io.NetworkUtils;
import com.discord.utilities.uri.UriHandler;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetMedia.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Landroid/view/MenuItem;", "kotlin.jvm.PlatformType", "menuItem", "Landroid/content/Context;", "context", "", NotificationCompat.CATEGORY_CALL, "(Landroid/view/MenuItem;Landroid/content/Context;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMedia$onViewBoundOrOnResume$1<T1, T2> implements Action2<MenuItem, Context> {
    public final /* synthetic */ Uri $downloadUri;
    public final /* synthetic */ Uri $sourceUri;
    public final /* synthetic */ String $title;
    public final /* synthetic */ String $titleSubtext;
    public final /* synthetic */ WidgetMedia this$0;

    /* compiled from: WidgetMedia.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.media.WidgetMedia$onViewBoundOrOnResume$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Context $context;

        /* compiled from: WidgetMedia.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.media.WidgetMedia$onViewBoundOrOnResume$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02531 extends o implements Function1<String, Unit> {
            public C02531() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(String str) {
                invoke2(str);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(String str) {
                CharSequence b2;
                m.checkNotNullParameter(str, "it");
                if (WidgetMedia$onViewBoundOrOnResume$1.this.this$0.isAdded()) {
                    AnonymousClass1 r0 = AnonymousClass1.this;
                    WidgetMedia widgetMedia = WidgetMedia$onViewBoundOrOnResume$1.this.this$0;
                    Context context = r0.$context;
                    m.checkNotNullExpressionValue(context, "context");
                    b2 = b.b(context, R.string.download_file_complete, new Object[]{str}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    b.a.d.m.j(widgetMedia, b2, 0, 4);
                }
            }
        }

        /* compiled from: WidgetMedia.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.media.WidgetMedia$onViewBoundOrOnResume$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends o implements Function1<Throwable, Unit> {
            public AnonymousClass2() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke2(th);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Throwable th) {
                m.checkNotNullParameter(th, "it");
                if (WidgetMedia$onViewBoundOrOnResume$1.this.this$0.isAdded()) {
                    WidgetMedia widgetMedia = WidgetMedia$onViewBoundOrOnResume$1.this.this$0;
                    b.a.d.m.j(widgetMedia, widgetMedia.getString(R.string.download_failed), 0, 4);
                }
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Context context) {
            super(0);
            this.$context = context;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Context context = this.$context;
            Uri uri = WidgetMedia$onViewBoundOrOnResume$1.this.$downloadUri;
            m.checkNotNullExpressionValue(uri, "downloadUri");
            WidgetMedia$onViewBoundOrOnResume$1 widgetMedia$onViewBoundOrOnResume$1 = WidgetMedia$onViewBoundOrOnResume$1.this;
            NetworkUtils.downloadFile(context, uri, widgetMedia$onViewBoundOrOnResume$1.$title, widgetMedia$onViewBoundOrOnResume$1.$titleSubtext, new C02531(), new AnonymousClass2());
        }
    }

    public WidgetMedia$onViewBoundOrOnResume$1(WidgetMedia widgetMedia, Uri uri, Uri uri2, String str, String str2) {
        this.this$0 = widgetMedia;
        this.$sourceUri = uri;
        this.$downloadUri = uri2;
        this.$title = str;
        this.$titleSubtext = str2;
    }

    public final void call(MenuItem menuItem, Context context) {
        m.checkNotNullExpressionValue(menuItem, "menuItem");
        switch (menuItem.getItemId()) {
            case R.id.menu_media_browser /* 2131364327 */:
                m.checkNotNullExpressionValue(context, "context");
                String uri = this.$sourceUri.toString();
                m.checkNotNullExpressionValue(uri, "sourceUri.toString()");
                UriHandler.handleOrUntrusted$default(context, uri, null, 4, null);
                return;
            case R.id.menu_media_download /* 2131364328 */:
                this.this$0.requestMediaDownload(new AnonymousClass1(context));
                return;
            case R.id.menu_media_share /* 2131364329 */:
                m.checkNotNullExpressionValue(context, "context");
                String uri2 = this.$sourceUri.toString();
                m.checkNotNullExpressionValue(uri2, "sourceUri.toString()");
                IntentUtils.performChooserSendIntent$default(context, uri2, null, 4, null);
                return;
            default:
                return;
        }
    }
}
