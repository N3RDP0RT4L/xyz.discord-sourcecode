package com.discord.widgets.tabs;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.panels.PanelState;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMentions;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreTabsNavigation;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserRelationships;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.k;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: TabsHostViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 *2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004*+,-B5\u0012\b\b\u0002\u0010&\u001a\u00020%\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u001e\u0012\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00030\u000f¢\u0006\u0004\b(\u0010)J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000e\u0010\rJ\u0013\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0013H\u0007¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0018\u001a\u00020\u0017H\u0007¢\u0006\u0004\b\u0018\u0010\u0019R:\u0010\u001c\u001a&\u0012\f\u0012\n \u001b*\u0004\u0018\u00010\u00100\u0010 \u001b*\u0012\u0012\f\u0012\n \u001b*\u0004\u0018\u00010\u00100\u0010\u0018\u00010\u001a0\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0018\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0004\u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006."}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;", "Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;)V", "", "height", "handleBottomNavHeight", "(I)V", "emitTrackFriendsListShown", "()V", "dismissSearchDialog", "Lrx/Observable;", "Lcom/discord/widgets/tabs/TabsHostViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/widgets/tabs/NavigationTab;", "tab", "selectTab", "(Lcom/discord/widgets/tabs/NavigationTab;)V", "", "handleBackPress", "()Z", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreNavigation;", "Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;", "Lcom/discord/stores/StoreTabsNavigation;", "storeTabsNavigation", "Lcom/discord/stores/StoreTabsNavigation;", "Lcom/discord/widgets/tabs/BottomNavViewObserver;", "bottomNavViewObserver", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/widgets/tabs/BottomNavViewObserver;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreNavigation;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class TabsHostViewModel extends AppViewModel<ViewState> {
    private static final Set<NavigationTab> AT_LEAST_ONE_GUILD_TABS;
    public static final Companion Companion = new Companion(null);
    private static final List<NavigationTab> NON_HOME_TAB_DESTINATIONS;
    private static final Set<NavigationTab> NO_GUILD_TABS;
    private static final List<NavigationTab> TAB_DESTINATIONS;
    private final PublishSubject<Event> eventSubject;
    private final StoreNavigation storeNavigation;
    private StoreState storeState;
    private final StoreTabsNavigation storeTabsNavigation;

    /* compiled from: TabsHostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.tabs.TabsHostViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            TabsHostViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: TabsHostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "bottomNavHeight", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.tabs.TabsHostViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<Integer, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
            invoke(num.intValue());
            return Unit.a;
        }

        public final void invoke(int i) {
            TabsHostViewModel.this.handleBottomNavHeight(i);
        }
    }

    /* compiled from: TabsHostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "it", "invoke", "(Lkotlin/Unit;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.tabs.TabsHostViewModel$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends o implements Function1<Unit, Unit> {
        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Unit unit) {
            invoke2(unit);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Unit unit) {
            m.checkNotNullParameter(unit, "it");
            TabsHostViewModel.this.dismissSearchDialog();
        }
    }

    /* compiled from: TabsHostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b!\u0010\"JE\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00130\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0015\u001a\u0004\b\u001e\u0010\u0017R\u001f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00130\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u001a\u001a\u0004\b \u0010\u001c¨\u0006#"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;", "", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreTabsNavigation;", "storeTabsNavigation", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreMentions;", "storeMentions", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lrx/Observable;", "Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreUserRelationships;)Lrx/Observable;", "", "Lcom/discord/widgets/tabs/NavigationTab;", "TAB_DESTINATIONS", "Ljava/util/List;", "getTAB_DESTINATIONS", "()Ljava/util/List;", "", "AT_LEAST_ONE_GUILD_TABS", "Ljava/util/Set;", "getAT_LEAST_ONE_GUILD_TABS", "()Ljava/util/Set;", "NON_HOME_TAB_DESTINATIONS", "getNON_HOME_TAB_DESTINATIONS", "NO_GUILD_TABS", "getNO_GUILD_TABS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(StoreNavigation storeNavigation, StoreTabsNavigation storeTabsNavigation, StoreUser storeUser, StoreGuilds storeGuilds, StoreMentions storeMentions, StoreUserRelationships storeUserRelationships) {
            Observable<StoreState> f = Observable.f(storeNavigation.observeLeftPanelState(), storeTabsNavigation.observeSelectedTab(), storeUser.observeMeId(), storeGuilds.observeGuilds(), storeMentions.observeTotalMentions(), storeUserRelationships.observe(), TabsHostViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(f, "Observable.combineLatest…nships,\n        )\n      }");
            return f;
        }

        public final Set<NavigationTab> getAT_LEAST_ONE_GUILD_TABS() {
            return TabsHostViewModel.AT_LEAST_ONE_GUILD_TABS;
        }

        public final List<NavigationTab> getNON_HOME_TAB_DESTINATIONS() {
            return TabsHostViewModel.NON_HOME_TAB_DESTINATIONS;
        }

        public final Set<NavigationTab> getNO_GUILD_TABS() {
            return TabsHostViewModel.NO_GUILD_TABS;
        }

        public final List<NavigationTab> getTAB_DESTINATIONS() {
            return TabsHostViewModel.TAB_DESTINATIONS;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: TabsHostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$Event;", "", HookHelper.constructorName, "()V", "DismissSearchDialog", "TrackFriendsListShown", "Lcom/discord/widgets/tabs/TabsHostViewModel$Event$TrackFriendsListShown;", "Lcom/discord/widgets/tabs/TabsHostViewModel$Event$DismissSearchDialog;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: TabsHostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$Event$DismissSearchDialog;", "Lcom/discord/widgets/tabs/TabsHostViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DismissSearchDialog extends Event {
            public static final DismissSearchDialog INSTANCE = new DismissSearchDialog();

            private DismissSearchDialog() {
                super(null);
            }
        }

        /* compiled from: TabsHostViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$Event$TrackFriendsListShown;", "Lcom/discord/widgets/tabs/TabsHostViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class TrackFriendsListShown extends Event {
            public static final TrackFriendsListShown INSTANCE = new TrackFriendsListShown();

            private TrackFriendsListShown() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: TabsHostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\n\u0010\u0018\u001a\u00060\bj\u0002`\t\u0012\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\f\u0012\u0006\u0010\u001a\u001a\u00020\u0011\u0012\u001a\u0010\u001b\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\u0011j\u0002`\u00140\f¢\u0006\u0004\b1\u00102J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0014\u0010\n\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J$\u0010\u0015\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\u0011j\u0002`\u00140\fHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0010Jt\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0016\u001a\u00020\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u00052\f\b\u0002\u0010\u0018\u001a\u00060\bj\u0002`\t2\u0018\b\u0002\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\f2\b\b\u0002\u0010\u001a\u001a\u00020\u00112\u001c\b\u0002\u0010\u001b\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\u0011j\u0002`\u00140\fHÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b!\u0010\u0013J\u001a\u0010$\u001a\u00020#2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010&\u001a\u0004\b'\u0010\u0010R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\u0004R\u001d\u0010\u0018\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b+\u0010\u000bR\u0019\u0010\u001a\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b-\u0010\u0013R\u0019\u0010\u0017\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010.\u001a\u0004\b/\u0010\u0007R-\u0010\u001b\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\u0011j\u0002`\u00140\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010&\u001a\u0004\b0\u0010\u0010¨\u00063"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;", "", "Lcom/discord/panels/PanelState;", "component1", "()Lcom/discord/panels/PanelState;", "Lcom/discord/widgets/tabs/NavigationTab;", "component2", "()Lcom/discord/widgets/tabs/NavigationTab;", "", "Lcom/discord/primitives/UserId;", "component3", "()J", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "component4", "()Ljava/util/Map;", "", "component5", "()I", "Lcom/discord/primitives/RelationshipType;", "component6", "leftPanelState", "selectedTab", "myUserId", "guildIdToGuildMap", "numTotalMentions", "userRelationships", "copy", "(Lcom/discord/panels/PanelState;Lcom/discord/widgets/tabs/NavigationTab;JLjava/util/Map;ILjava/util/Map;)Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getGuildIdToGuildMap", "Lcom/discord/panels/PanelState;", "getLeftPanelState", "J", "getMyUserId", "I", "getNumTotalMentions", "Lcom/discord/widgets/tabs/NavigationTab;", "getSelectedTab", "getUserRelationships", HookHelper.constructorName, "(Lcom/discord/panels/PanelState;Lcom/discord/widgets/tabs/NavigationTab;JLjava/util/Map;ILjava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, Guild> guildIdToGuildMap;
        private final PanelState leftPanelState;
        private final long myUserId;
        private final int numTotalMentions;
        private final NavigationTab selectedTab;
        private final Map<Long, Integer> userRelationships;

        public StoreState(PanelState panelState, NavigationTab navigationTab, long j, Map<Long, Guild> map, int i, Map<Long, Integer> map2) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(navigationTab, "selectedTab");
            m.checkNotNullParameter(map, "guildIdToGuildMap");
            m.checkNotNullParameter(map2, "userRelationships");
            this.leftPanelState = panelState;
            this.selectedTab = navigationTab;
            this.myUserId = j;
            this.guildIdToGuildMap = map;
            this.numTotalMentions = i;
            this.userRelationships = map2;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, PanelState panelState, NavigationTab navigationTab, long j, Map map, int i, Map map2, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                panelState = storeState.leftPanelState;
            }
            if ((i2 & 2) != 0) {
                navigationTab = storeState.selectedTab;
            }
            NavigationTab navigationTab2 = navigationTab;
            if ((i2 & 4) != 0) {
                j = storeState.myUserId;
            }
            long j2 = j;
            Map<Long, Guild> map3 = map;
            if ((i2 & 8) != 0) {
                map3 = storeState.guildIdToGuildMap;
            }
            Map map4 = map3;
            if ((i2 & 16) != 0) {
                i = storeState.numTotalMentions;
            }
            int i3 = i;
            Map<Long, Integer> map5 = map2;
            if ((i2 & 32) != 0) {
                map5 = storeState.userRelationships;
            }
            return storeState.copy(panelState, navigationTab2, j2, map4, i3, map5);
        }

        public final PanelState component1() {
            return this.leftPanelState;
        }

        public final NavigationTab component2() {
            return this.selectedTab;
        }

        public final long component3() {
            return this.myUserId;
        }

        public final Map<Long, Guild> component4() {
            return this.guildIdToGuildMap;
        }

        public final int component5() {
            return this.numTotalMentions;
        }

        public final Map<Long, Integer> component6() {
            return this.userRelationships;
        }

        public final StoreState copy(PanelState panelState, NavigationTab navigationTab, long j, Map<Long, Guild> map, int i, Map<Long, Integer> map2) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(navigationTab, "selectedTab");
            m.checkNotNullParameter(map, "guildIdToGuildMap");
            m.checkNotNullParameter(map2, "userRelationships");
            return new StoreState(panelState, navigationTab, j, map, i, map2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.leftPanelState, storeState.leftPanelState) && m.areEqual(this.selectedTab, storeState.selectedTab) && this.myUserId == storeState.myUserId && m.areEqual(this.guildIdToGuildMap, storeState.guildIdToGuildMap) && this.numTotalMentions == storeState.numTotalMentions && m.areEqual(this.userRelationships, storeState.userRelationships);
        }

        public final Map<Long, Guild> getGuildIdToGuildMap() {
            return this.guildIdToGuildMap;
        }

        public final PanelState getLeftPanelState() {
            return this.leftPanelState;
        }

        public final long getMyUserId() {
            return this.myUserId;
        }

        public final int getNumTotalMentions() {
            return this.numTotalMentions;
        }

        public final NavigationTab getSelectedTab() {
            return this.selectedTab;
        }

        public final Map<Long, Integer> getUserRelationships() {
            return this.userRelationships;
        }

        public int hashCode() {
            PanelState panelState = this.leftPanelState;
            int i = 0;
            int hashCode = (panelState != null ? panelState.hashCode() : 0) * 31;
            NavigationTab navigationTab = this.selectedTab;
            int a = (b.a(this.myUserId) + ((hashCode + (navigationTab != null ? navigationTab.hashCode() : 0)) * 31)) * 31;
            Map<Long, Guild> map = this.guildIdToGuildMap;
            int hashCode2 = (((a + (map != null ? map.hashCode() : 0)) * 31) + this.numTotalMentions) * 31;
            Map<Long, Integer> map2 = this.userRelationships;
            if (map2 != null) {
                i = map2.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(leftPanelState=");
            R.append(this.leftPanelState);
            R.append(", selectedTab=");
            R.append(this.selectedTab);
            R.append(", myUserId=");
            R.append(this.myUserId);
            R.append(", guildIdToGuildMap=");
            R.append(this.guildIdToGuildMap);
            R.append(", numTotalMentions=");
            R.append(this.numTotalMentions);
            R.append(", userRelationships=");
            return a.L(R, this.userRelationships, ")");
        }
    }

    /* compiled from: TabsHostViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\n\u0010\u0017\u001a\u00060\u000bj\u0002`\f\u0012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f\u0012\u0006\u0010\u0019\u001a\u00020\b\u0012\u0006\u0010\u001a\u001a\u00020\b¢\u0006\u0004\b0\u00101J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\r\u001a\u00060\u000bj\u0002`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\nJ\u0010\u0010\u0013\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0013\u0010\nJ`\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\b2\f\b\u0002\u0010\u0017\u001a\u00060\u000bj\u0002`\f2\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f2\b\b\u0002\u0010\u0019\u001a\u00020\b2\b\b\u0002\u0010\u001a\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\bHÖ\u0001¢\u0006\u0004\b \u0010\nJ\u001a\u0010\"\u001a\u00020\u00052\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010$\u001a\u0004\b%\u0010\u0011R\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\u0007R\u0019\u0010\u0016\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\nR\u0019\u0010\u001a\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010(\u001a\u0004\b*\u0010\nR\u001d\u0010\u0017\u001a\u00060\u000bj\u0002`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010+\u001a\u0004\b,\u0010\u000eR\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010-\u001a\u0004\b.\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010(\u001a\u0004\b/\u0010\n¨\u00062"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;", "", "Lcom/discord/widgets/tabs/NavigationTab;", "component1", "()Lcom/discord/widgets/tabs/NavigationTab;", "", "component2", "()Z", "", "component3", "()I", "", "Lcom/discord/primitives/UserId;", "component4", "()J", "", "component5", "()Ljava/util/Set;", "component6", "component7", "selectedTab", "showBottomNav", "bottomNavHeight", "myUserId", "visibleTabs", "numHomeNotifications", "numFriendsNotifications", "copy", "(Lcom/discord/widgets/tabs/NavigationTab;ZIJLjava/util/Set;II)Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getVisibleTabs", "Z", "getShowBottomNav", "I", "getBottomNavHeight", "getNumFriendsNotifications", "J", "getMyUserId", "Lcom/discord/widgets/tabs/NavigationTab;", "getSelectedTab", "getNumHomeNotifications", HookHelper.constructorName, "(Lcom/discord/widgets/tabs/NavigationTab;ZIJLjava/util/Set;II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final int bottomNavHeight;
        private final long myUserId;
        private final int numFriendsNotifications;
        private final int numHomeNotifications;
        private final NavigationTab selectedTab;
        private final boolean showBottomNav;
        private final Set<NavigationTab> visibleTabs;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(NavigationTab navigationTab, boolean z2, int i, long j, Set<? extends NavigationTab> set, int i2, int i3) {
            m.checkNotNullParameter(navigationTab, "selectedTab");
            m.checkNotNullParameter(set, "visibleTabs");
            this.selectedTab = navigationTab;
            this.showBottomNav = z2;
            this.bottomNavHeight = i;
            this.myUserId = j;
            this.visibleTabs = set;
            this.numHomeNotifications = i2;
            this.numFriendsNotifications = i3;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, NavigationTab navigationTab, boolean z2, int i, long j, Set set, int i2, int i3, int i4, Object obj) {
            return viewState.copy((i4 & 1) != 0 ? viewState.selectedTab : navigationTab, (i4 & 2) != 0 ? viewState.showBottomNav : z2, (i4 & 4) != 0 ? viewState.bottomNavHeight : i, (i4 & 8) != 0 ? viewState.myUserId : j, (i4 & 16) != 0 ? viewState.visibleTabs : set, (i4 & 32) != 0 ? viewState.numHomeNotifications : i2, (i4 & 64) != 0 ? viewState.numFriendsNotifications : i3);
        }

        public final NavigationTab component1() {
            return this.selectedTab;
        }

        public final boolean component2() {
            return this.showBottomNav;
        }

        public final int component3() {
            return this.bottomNavHeight;
        }

        public final long component4() {
            return this.myUserId;
        }

        public final Set<NavigationTab> component5() {
            return this.visibleTabs;
        }

        public final int component6() {
            return this.numHomeNotifications;
        }

        public final int component7() {
            return this.numFriendsNotifications;
        }

        public final ViewState copy(NavigationTab navigationTab, boolean z2, int i, long j, Set<? extends NavigationTab> set, int i2, int i3) {
            m.checkNotNullParameter(navigationTab, "selectedTab");
            m.checkNotNullParameter(set, "visibleTabs");
            return new ViewState(navigationTab, z2, i, j, set, i2, i3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.selectedTab, viewState.selectedTab) && this.showBottomNav == viewState.showBottomNav && this.bottomNavHeight == viewState.bottomNavHeight && this.myUserId == viewState.myUserId && m.areEqual(this.visibleTabs, viewState.visibleTabs) && this.numHomeNotifications == viewState.numHomeNotifications && this.numFriendsNotifications == viewState.numFriendsNotifications;
        }

        public final int getBottomNavHeight() {
            return this.bottomNavHeight;
        }

        public final long getMyUserId() {
            return this.myUserId;
        }

        public final int getNumFriendsNotifications() {
            return this.numFriendsNotifications;
        }

        public final int getNumHomeNotifications() {
            return this.numHomeNotifications;
        }

        public final NavigationTab getSelectedTab() {
            return this.selectedTab;
        }

        public final boolean getShowBottomNav() {
            return this.showBottomNav;
        }

        public final Set<NavigationTab> getVisibleTabs() {
            return this.visibleTabs;
        }

        public int hashCode() {
            NavigationTab navigationTab = this.selectedTab;
            int i = 0;
            int hashCode = (navigationTab != null ? navigationTab.hashCode() : 0) * 31;
            boolean z2 = this.showBottomNav;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int a = (b.a(this.myUserId) + ((((hashCode + i2) * 31) + this.bottomNavHeight) * 31)) * 31;
            Set<NavigationTab> set = this.visibleTabs;
            if (set != null) {
                i = set.hashCode();
            }
            return ((((a + i) * 31) + this.numHomeNotifications) * 31) + this.numFriendsNotifications;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(selectedTab=");
            R.append(this.selectedTab);
            R.append(", showBottomNav=");
            R.append(this.showBottomNav);
            R.append(", bottomNavHeight=");
            R.append(this.bottomNavHeight);
            R.append(", myUserId=");
            R.append(this.myUserId);
            R.append(", visibleTabs=");
            R.append(this.visibleTabs);
            R.append(", numHomeNotifications=");
            R.append(this.numHomeNotifications);
            R.append(", numFriendsNotifications=");
            return a.A(R, this.numFriendsNotifications, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            NavigationTab.values();
            int[] iArr = new int[6];
            $EnumSwitchMapping$0 = iArr;
            iArr[NavigationTab.HOME.ordinal()] = 1;
            iArr[NavigationTab.FRIENDS.ordinal()] = 2;
        }
    }

    static {
        List<NavigationTab> list = k.toList(NavigationTab.values());
        TAB_DESTINATIONS = list;
        NavigationTab navigationTab = NavigationTab.HOME;
        NON_HOME_TAB_DESTINATIONS = u.minus(list, navigationTab);
        NavigationTab navigationTab2 = NavigationTab.FRIENDS;
        NavigationTab navigationTab3 = NavigationTab.SETTINGS;
        NO_GUILD_TABS = n0.setOf((Object[]) new NavigationTab[]{navigationTab, navigationTab2, navigationTab3});
        AT_LEAST_ONE_GUILD_TABS = n0.setOf((Object[]) new NavigationTab[]{navigationTab, navigationTab2, NavigationTab.SEARCH, NavigationTab.MENTIONS, navigationTab3});
    }

    public TabsHostViewModel() {
        this(null, null, null, null, 15, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ TabsHostViewModel(com.discord.widgets.tabs.BottomNavViewObserver r8, com.discord.stores.StoreTabsNavigation r9, com.discord.stores.StoreNavigation r10, rx.Observable r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r7 = this;
            r13 = r12 & 1
            if (r13 == 0) goto La
            com.discord.widgets.tabs.BottomNavViewObserver$Companion r8 = com.discord.widgets.tabs.BottomNavViewObserver.Companion
            com.discord.widgets.tabs.BottomNavViewObserver r8 = r8.getINSTANCE()
        La:
            r13 = r12 & 2
            if (r13 == 0) goto L14
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreTabsNavigation r9 = r9.getTabsNavigation()
        L14:
            r13 = r12 & 4
            if (r13 == 0) goto L1e
            com.discord.stores.StoreStream$Companion r10 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreNavigation r10 = r10.getNavigation()
        L1e:
            r12 = r12 & 8
            if (r12 == 0) goto L3c
            com.discord.widgets.tabs.TabsHostViewModel$Companion r0 = com.discord.widgets.tabs.TabsHostViewModel.Companion
            com.discord.stores.StoreStream$Companion r11 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r3 = r11.getUsers()
            com.discord.stores.StoreGuilds r4 = r11.getGuilds()
            com.discord.stores.StoreMentions r5 = r11.getMentions()
            com.discord.stores.StoreUserRelationships r6 = r11.getUserRelationships()
            r1 = r10
            r2 = r9
            rx.Observable r11 = com.discord.widgets.tabs.TabsHostViewModel.Companion.access$observeStoreState(r0, r1, r2, r3, r4, r5, r6)
        L3c:
            r7.<init>(r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.tabs.TabsHostViewModel.<init>(com.discord.widgets.tabs.BottomNavViewObserver, com.discord.stores.StoreTabsNavigation, com.discord.stores.StoreNavigation, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void dismissSearchDialog() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.DismissSearchDialog.INSTANCE);
    }

    private final void emitTrackFriendsListShown() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.TrackFriendsListShown.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleBottomNavHeight(int i) {
        updateViewState(ViewState.copy$default(requireViewState(), null, false, i, 0L, null, 0, 0, 123, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        this.storeState = storeState;
        NavigationTab selectedTab = storeState.getSelectedTab();
        boolean z2 = NON_HOME_TAB_DESTINATIONS.contains(selectedTab) || (selectedTab == NavigationTab.HOME && (m.areEqual(storeState.getLeftPanelState(), PanelState.c.a) || m.areEqual(storeState.getLeftPanelState(), PanelState.d.a)));
        Set<NavigationTab> set = storeState.getGuildIdToGuildMap().values().isEmpty() ^ true ? AT_LEAST_ONE_GUILD_TABS : NO_GUILD_TABS;
        Map<Long, Integer> userRelationships = storeState.getUserRelationships();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Integer> entry : userRelationships.entrySet()) {
            if (entry.getValue().intValue() == 3) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        updateViewState(ViewState.copy$default(requireViewState(), selectedTab, z2, 0, storeState.getMyUserId(), set, storeState.getNumTotalMentions(), linkedHashMap.size(), 4, null));
    }

    @MainThread
    public final boolean handleBackPress() {
        NavigationTab navigationTab;
        NavigationTab selectedTab = requireViewState().getSelectedTab();
        if (!TAB_DESTINATIONS.contains(selectedTab) || selectedTab == (navigationTab = NavigationTab.HOME)) {
            return false;
        }
        selectTab(navigationTab);
        return true;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @MainThread
    public final void selectTab(NavigationTab navigationTab) {
        StoreNavigation.PanelAction panelAction;
        m.checkNotNullParameter(navigationTab, "tab");
        NavigationTab selectedTab = requireViewState().getSelectedTab();
        StoreTabsNavigation.selectTab$default(this.storeTabsNavigation, navigationTab, false, 2, null);
        int ordinal = navigationTab.ordinal();
        if (ordinal == 0) {
            if (selectedTab == NavigationTab.HOME) {
                StoreState storeState = this.storeState;
                if (m.areEqual(storeState != null ? storeState.getLeftPanelState() : null, PanelState.c.a)) {
                    panelAction = StoreNavigation.PanelAction.CLOSE;
                    StoreNavigation.setNavigationPanelAction$default(this.storeNavigation, panelAction, null, 2, null);
                }
            }
            panelAction = StoreNavigation.PanelAction.OPEN;
            StoreNavigation.setNavigationPanelAction$default(this.storeNavigation, panelAction, null, 2, null);
        } else if (ordinal == 1) {
            emitTrackFriendsListShown();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TabsHostViewModel(BottomNavViewObserver bottomNavViewObserver, StoreTabsNavigation storeTabsNavigation, StoreNavigation storeNavigation, Observable<StoreState> observable) {
        super(new ViewState(NavigationTab.HOME, false, 0, 0L, NO_GUILD_TABS, 0, 0));
        m.checkNotNullParameter(bottomNavViewObserver, "bottomNavViewObserver");
        m.checkNotNullParameter(storeTabsNavigation, "storeTabsNavigation");
        m.checkNotNullParameter(storeNavigation, "storeNavigation");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.storeTabsNavigation = storeTabsNavigation;
        this.storeNavigation = storeNavigation;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), TabsHostViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(bottomNavViewObserver.observeHeight(), this, null, 2, null), TabsHostViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(storeTabsNavigation.observeDismissTabsDialogEvent(), this, null, 2, null), TabsHostViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3());
    }
}
