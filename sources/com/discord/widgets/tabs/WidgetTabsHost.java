package com.discord.widgets.tabs;

import andhook.lib.HookHelper;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.core.graphics.Insets;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewGroupKt;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetTabsHostBinding;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorState;
import com.discord.widgets.tabs.TabsHostViewModel;
import com.discord.widgets.user.WidgetUserStatusSheet;
import com.discord.widgets.user.search.WidgetGlobalSearchDialog;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlin.sequences.Sequence;
import rx.Observable;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetTabsHost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 E2\u00020\u0001:\u0001EB\u0007¢\u0006\u0004\bD\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\r\u0010\fJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000f\u0010\u0006J\u0019\u0010\u0012\u001a\u00020\u00042\b\b\u0002\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0014\u0010\fJ\u0017\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u0019H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001d\u0010\fJ)\u0010#\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u001e2\b\u0010\"\u001a\u0004\u0018\u00010!H\u0016¢\u0006\u0004\b#\u0010$J\u000f\u0010%\u001a\u00020\u0004H\u0016¢\u0006\u0004\b%\u0010\fJ\u001d\u0010(\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b(\u0010)R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010*R\u0018\u0010+\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R\"\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020&0-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R\u001d\u00105\u001a\u0002008B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0016\u00106\u001a\u00020\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u00107R\u0018\u00109\u001a\u0004\u0018\u0001088\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b9\u0010:R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u001d\u0010C\u001a\u00020>8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010B¨\u0006F"}, d2 = {"Lcom/discord/widgets/tabs/WidgetTabsHost;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;", "viewState", "", "updateViews", "(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V", "Lcom/discord/widgets/tabs/TabsHostViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/tabs/TabsHostViewModel$Event;)V", "onSearchClick", "()V", "onSettingsLongPress", "handleFriendsListShown", "updateNavHostMargins", "", "isCallStatusVisible", "setPanelWindowInsetsListeners", "(Z)V", "configureSystemStatusBar", "Lcom/discord/widgets/tabs/NavigationTab;", "navigationTab", "navigateToTab", "(Lcom/discord/widgets/tabs/NavigationTab;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "", "requestCode", "resultCode", "Landroid/content/Intent;", "data", "onActivityResult", "(IILandroid/content/Intent;)V", "onDestroyView", "Lcom/discord/widgets/tabs/OnTabSelectedListener;", "onTabSelectedListener", "registerTabSelectionListener", "(Lcom/discord/widgets/tabs/NavigationTab;Lcom/discord/widgets/tabs/OnTabSelectedListener;)V", "Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;", "previousShowBottomNav", "Ljava/lang/Boolean;", "", "tabToTabSelectionListenerMap", "Ljava/util/Map;", "Lcom/discord/widgets/tabs/TabsHostViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/tabs/TabsHostViewModel;", "viewModel", "previousBottomNavHeight", "I", "Landroid/animation/ValueAnimator;", "bottomNavAnimator", "Landroid/animation/ValueAnimator;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "globalStatusIndicatorStateObserver", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "Lcom/discord/databinding/WidgetTabsHostBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetTabsHostBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTabsHost extends AppFragment {
    private static final long BOTTOM_TABS_DOWNWARD_ANIMATION_DURATION_MS = 200;
    private static final long BOTTOM_TABS_UPWARD_ANIMATION_DURATION_MS = 250;
    private ValueAnimator bottomNavAnimator;
    private int previousBottomNavHeight;
    private Boolean previousShowBottomNav;
    private final Lazy viewModel$delegate;
    private TabsHostViewModel.ViewState viewState;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetTabsHost.class, "binding", "getBinding()Lcom/discord/databinding/WidgetTabsHostBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetTabsHost$binding$2.INSTANCE, null, 2, null);
    private final Map<NavigationTab, OnTabSelectedListener> tabToTabSelectionListenerMap = new LinkedHashMap();
    private final WidgetGlobalStatusIndicatorState globalStatusIndicatorStateObserver = WidgetGlobalStatusIndicatorState.Provider.get();

    /* compiled from: WidgetTabsHost.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/tabs/WidgetTabsHost$Companion;", "", "", "BOTTOM_TABS_DOWNWARD_ANIMATION_DURATION_MS", "J", "BOTTOM_TABS_UPWARD_ANIMATION_DURATION_MS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetTabsHost() {
        super(R.layout.widget_tabs_host);
        WidgetTabsHost$viewModel$2 widgetTabsHost$viewModel$2 = WidgetTabsHost$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(TabsHostViewModel.class), new WidgetTabsHost$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetTabsHost$viewModel$2));
    }

    private final void configureSystemStatusBar() {
        int themedColor = ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundTertiary);
        ColorCompat.setStatusBarColor$default((Fragment) this, themedColor, false, 4, (Object) null);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(this.globalStatusIndicatorStateObserver.observeState(), this, null, 2, null), WidgetTabsHost.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetTabsHost$configureSystemStatusBar$1(this, themedColor));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetTabsHostBinding getBinding() {
        return (WidgetTabsHostBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final TabsHostViewModel getViewModel() {
        return (TabsHostViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(TabsHostViewModel.Event event) {
        if (m.areEqual(event, TabsHostViewModel.Event.TrackFriendsListShown.INSTANCE)) {
            handleFriendsListShown();
        } else if (m.areEqual(event, TabsHostViewModel.Event.DismissSearchDialog.INSTANCE)) {
            WidgetGlobalSearchDialog.Companion companion = WidgetGlobalSearchDialog.Companion;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.dismiss(parentFragmentManager);
        }
    }

    private final void handleFriendsListShown() {
        AnalyticsTracker.INSTANCE.friendsListViewed();
    }

    private final void navigateToTab(NavigationTab navigationTab) {
        FragmentContainerView fragmentContainerView = getBinding().e;
        m.checkNotNullExpressionValue(fragmentContainerView, "binding.widgetTabsHostHome");
        boolean z2 = true;
        int i = 0;
        fragmentContainerView.setVisibility(navigationTab == NavigationTab.HOME ? 0 : 8);
        FragmentContainerView fragmentContainerView2 = getBinding().d;
        m.checkNotNullExpressionValue(fragmentContainerView2, "binding.widgetTabsHostFriends");
        fragmentContainerView2.setVisibility(navigationTab == NavigationTab.FRIENDS ? 0 : 8);
        FragmentContainerView fragmentContainerView3 = getBinding().f;
        m.checkNotNullExpressionValue(fragmentContainerView3, "binding.widgetTabsHostMentions");
        fragmentContainerView3.setVisibility(navigationTab == NavigationTab.MENTIONS ? 0 : 8);
        FragmentContainerView fragmentContainerView4 = getBinding().i;
        m.checkNotNullExpressionValue(fragmentContainerView4, "binding.widgetTabsHostUserSettings");
        if (navigationTab != NavigationTab.SETTINGS) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        fragmentContainerView4.setVisibility(i);
        OnTabSelectedListener onTabSelectedListener = this.tabToTabSelectionListenerMap.get(navigationTab);
        if (onTabSelectedListener != null) {
            onTabSelectedListener.onTabSelected();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSearchClick() {
        WidgetGlobalSearchDialog.Companion companion = WidgetGlobalSearchDialog.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        WidgetGlobalSearchDialog.Companion.show$default(companion, parentFragmentManager, null, 2, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSettingsLongPress() {
        WidgetUserStatusSheet.Companion.show(this);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setPanelWindowInsetsListeners(final boolean z2) {
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().c, WidgetTabsHost$setPanelWindowInsetsListeners$1.INSTANCE);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().e, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.tabs.WidgetTabsHost$setPanelWindowInsetsListeners$2
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                Sequence<View> children;
                m.checkNotNullParameter(view, "view");
                m.checkNotNullParameter(windowInsetsCompat, "insets");
                if (z2) {
                    view.setPadding(view.getPaddingLeft(), 0, view.getPaddingRight(), view.getPaddingBottom());
                } else {
                    view.setPadding(view.getPaddingLeft(), windowInsetsCompat.getSystemWindowInsetTop(), view.getPaddingRight(), view.getPaddingBottom());
                    windowInsetsCompat = new WindowInsetsCompat.Builder().setSystemWindowInsets(Insets.of(windowInsetsCompat.getSystemWindowInsetLeft(), 0, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom())).build();
                    m.checkNotNullExpressionValue(windowInsetsCompat, "WindowInsetsCompat.Build…      )\n        ).build()");
                }
                if (!(view instanceof ViewGroup)) {
                    view = null;
                }
                ViewGroup viewGroup = (ViewGroup) view;
                if (!(viewGroup == null || (children = ViewGroupKt.getChildren(viewGroup)) == null)) {
                    for (View view2 : children) {
                        ViewCompat.dispatchApplyWindowInsets(view2, windowInsetsCompat);
                    }
                }
                return windowInsetsCompat;
            }
        });
        FrameLayout frameLayout = getBinding().g;
        m.checkNotNullExpressionValue(frameLayout, "binding.widgetTabsHostNavHost");
        ViewExtensions.setForwardingWindowInsetsListener(frameLayout);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().h, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.tabs.WidgetTabsHost$setPanelWindowInsetsListeners$3
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                int i;
                m.checkNotNullExpressionValue(view, "v");
                if (z2) {
                    i = 0;
                } else {
                    m.checkNotNullExpressionValue(windowInsetsCompat, "insets");
                    i = windowInsetsCompat.getSystemWindowInsetTop();
                }
                view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), view.getPaddingBottom());
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().f2638b, WidgetTabsHost$setPanelWindowInsetsListeners$4.INSTANCE);
        getBinding().c.requestApplyInsets();
    }

    public static /* synthetic */ void setPanelWindowInsetsListeners$default(WidgetTabsHost widgetTabsHost, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        widgetTabsHost.setPanelWindowInsetsListeners(z2);
    }

    private final void updateNavHostMargins(TabsHostViewModel.ViewState viewState) {
        NavigationTab selectedTab = viewState.getSelectedTab();
        FrameLayout frameLayout = getBinding().g;
        m.checkNotNullExpressionValue(frameLayout, "binding.widgetTabsHostNavHost");
        ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, selectedTab == NavigationTab.HOME ? 0 : viewState.getBottomNavHeight());
        FrameLayout frameLayout2 = getBinding().g;
        m.checkNotNullExpressionValue(frameLayout2, "binding.widgetTabsHostNavHost");
        frameLayout2.setLayoutParams(marginLayoutParams);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateViews(TabsHostViewModel.ViewState viewState) {
        this.viewState = viewState;
        NavigationTab selectedTab = viewState.getSelectedTab();
        boolean showBottomNav = viewState.getShowBottomNav();
        int bottomNavHeight = viewState.getBottomNavHeight();
        navigateToTab(selectedTab);
        getBinding().f2638b.updateView(selectedTab, new WidgetTabsHost$updateViews$1(getViewModel()), showBottomNav, viewState.getMyUserId(), viewState.getVisibleTabs(), viewState.getNumHomeNotifications(), viewState.getNumFriendsNotifications(), new WidgetTabsHost$updateViews$2(this), new WidgetTabsHost$updateViews$3(this), new WidgetTabsHost$updateViews$4(this));
        updateNavHostMargins(viewState);
        boolean z2 = !m.areEqual(Boolean.valueOf(showBottomNav), this.previousShowBottomNav);
        boolean z3 = bottomNavHeight != this.previousBottomNavHeight;
        if (z2 || z3) {
            ValueAnimator valueAnimator = this.bottomNavAnimator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            TabsHostBottomNavigationView tabsHostBottomNavigationView = getBinding().f2638b;
            m.checkNotNullExpressionValue(tabsHostBottomNavigationView, "binding.widgetTabsHostBottomNavigationView");
            float translationY = tabsHostBottomNavigationView.getTranslationY();
            if (showBottomNav) {
                TabsHostBottomNavigationView tabsHostBottomNavigationView2 = getBinding().f2638b;
                m.checkNotNullExpressionValue(tabsHostBottomNavigationView2, "binding.widgetTabsHostBottomNavigationView");
                tabsHostBottomNavigationView2.setVisibility(0);
                ValueAnimator ofFloat = ValueAnimator.ofFloat(translationY, 0.0f);
                ofFloat.setInterpolator(new FastOutSlowInInterpolator());
                ofFloat.setDuration(BOTTOM_TABS_UPWARD_ANIMATION_DURATION_MS);
                ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: com.discord.widgets.tabs.WidgetTabsHost$updateViews$$inlined$apply$lambda$1
                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                        WidgetTabsHostBinding binding;
                        binding = WidgetTabsHost.this.getBinding();
                        TabsHostBottomNavigationView tabsHostBottomNavigationView3 = binding.f2638b;
                        m.checkNotNullExpressionValue(tabsHostBottomNavigationView3, "binding.widgetTabsHostBottomNavigationView");
                        m.checkNotNullExpressionValue(valueAnimator2, "animator");
                        Object animatedValue = valueAnimator2.getAnimatedValue();
                        Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Float");
                        tabsHostBottomNavigationView3.setTranslationY(((Float) animatedValue).floatValue());
                    }
                });
                ofFloat.start();
                this.bottomNavAnimator = ofFloat;
            } else if (bottomNavHeight > 0) {
                ValueAnimator ofFloat2 = ValueAnimator.ofFloat(translationY, bottomNavHeight);
                ofFloat2.setInterpolator(new FastOutSlowInInterpolator());
                ofFloat2.setDuration(BOTTOM_TABS_DOWNWARD_ANIMATION_DURATION_MS);
                ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: com.discord.widgets.tabs.WidgetTabsHost$updateViews$$inlined$apply$lambda$2
                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                        WidgetTabsHostBinding binding;
                        binding = WidgetTabsHost.this.getBinding();
                        TabsHostBottomNavigationView tabsHostBottomNavigationView3 = binding.f2638b;
                        m.checkNotNullExpressionValue(tabsHostBottomNavigationView3, "binding.widgetTabsHostBottomNavigationView");
                        m.checkNotNullExpressionValue(valueAnimator2, "animator");
                        Object animatedValue = valueAnimator2.getAnimatedValue();
                        Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Float");
                        tabsHostBottomNavigationView3.setTranslationY(((Float) animatedValue).floatValue());
                    }
                });
                ofFloat2.start();
                this.bottomNavAnimator = ofFloat2;
            } else {
                TabsHostBottomNavigationView tabsHostBottomNavigationView3 = getBinding().f2638b;
                m.checkNotNullExpressionValue(tabsHostBottomNavigationView3, "binding.widgetTabsHostBottomNavigationView");
                tabsHostBottomNavigationView3.setVisibility(4);
            }
        }
        this.previousBottomNavHeight = bottomNavHeight;
        this.previousShowBottomNav = Boolean.valueOf(showBottomNav);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 4008) {
            GoogleSmartLockManager.Companion.handleResult(i2, intent);
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        ValueAnimator valueAnimator = this.bottomNavAnimator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        super.onDestroyView();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        getBinding().f2638b.addHeightChangedListener(BottomNavViewObserver.Companion.getINSTANCE());
        setPanelWindowInsetsListeners$default(this, false, 1, null);
        AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.tabs.WidgetTabsHost$onViewBound$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                TabsHostViewModel viewModel;
                viewModel = WidgetTabsHost.this.getViewModel();
                return Boolean.valueOf(viewModel.handleBackPress());
            }
        }, 0, 2, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<TabsHostViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel.observeViewSta…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetTabsHost.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetTabsHost$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetTabsHost.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetTabsHost$onViewBoundOrOnResume$2(this));
        configureSystemStatusBar();
    }

    public final void registerTabSelectionListener(NavigationTab navigationTab, OnTabSelectedListener onTabSelectedListener) {
        m.checkNotNullParameter(navigationTab, "navigationTab");
        m.checkNotNullParameter(onTabSelectedListener, "onTabSelectedListener");
        this.tabToTabSelectionListenerMap.put(navigationTab, onTabSelectedListener);
        TabsHostViewModel.ViewState viewState = this.viewState;
        NavigationTab selectedTab = viewState != null ? viewState.getSelectedTab() : null;
        if (selectedTab != null) {
            navigateToTab(selectedTab);
        }
    }
}
