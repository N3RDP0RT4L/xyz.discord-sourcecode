package com.discord.widgets.tabs;

import android.view.View;
import android.view.ViewGroup;
import androidx.core.graphics.Insets;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewGroupKt;
import androidx.core.view.WindowInsetsCompat;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.sequences.Sequence;
/* compiled from: WidgetTabsHost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\u0007\u001a\n \u0004*\u0004\u0018\u00010\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/view/View;", "v", "Landroidx/core/view/WindowInsetsCompat;", "insets", "kotlin.jvm.PlatformType", "onApplyWindowInsets", "(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTabsHost$setPanelWindowInsetsListeners$1 implements OnApplyWindowInsetsListener {
    public static final WidgetTabsHost$setPanelWindowInsetsListeners$1 INSTANCE = new WidgetTabsHost$setPanelWindowInsetsListeners$1();

    @Override // androidx.core.view.OnApplyWindowInsetsListener
    public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
        Sequence<View> children;
        m.checkNotNullParameter(view, "v");
        m.checkNotNullParameter(windowInsetsCompat, "insets");
        view.setPadding(windowInsetsCompat.getSystemWindowInsetLeft(), view.getPaddingTop(), windowInsetsCompat.getSystemWindowInsetRight(), view.getPaddingBottom());
        WindowInsetsCompat build = new WindowInsetsCompat.Builder().setSystemWindowInsets(Insets.of(0, windowInsetsCompat.getSystemWindowInsetTop(), 0, windowInsetsCompat.getSystemWindowInsetBottom())).build();
        m.checkNotNullExpressionValue(build, "WindowInsetsCompat.Build…        )\n      ).build()");
        if (!(view instanceof ViewGroup)) {
            view = null;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        if (!(viewGroup == null || (children = ViewGroupKt.getChildren(viewGroup)) == null)) {
            for (View view2 : children) {
                ViewCompat.dispatchApplyWindowInsets(view2, build);
            }
        }
        return build;
    }
}
