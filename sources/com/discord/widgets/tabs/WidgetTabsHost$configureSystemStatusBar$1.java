package com.discord.widgets.tabs;

import androidx.fragment.app.Fragment;
import com.discord.utilities.color.ColorCompat;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorState;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetTabsHost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;", "state", "", "invoke", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTabsHost$configureSystemStatusBar$1 extends o implements Function1<WidgetGlobalStatusIndicatorState.State, Unit> {
    public final /* synthetic */ int $defaultStatusBarColor;
    public final /* synthetic */ WidgetTabsHost this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetTabsHost$configureSystemStatusBar$1(WidgetTabsHost widgetTabsHost, int i) {
        super(1);
        this.this$0 = widgetTabsHost;
        this.$defaultStatusBarColor = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetGlobalStatusIndicatorState.State state) {
        invoke2(state);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetGlobalStatusIndicatorState.State state) {
        m.checkNotNullParameter(state, "state");
        this.this$0.setPanelWindowInsetsListeners(state.isVisible());
        int color = ColorCompat.getColor(this.this$0, (int) R.color.transparent);
        if (!state.isVisible()) {
            color = this.$defaultStatusBarColor;
        }
        ColorCompat.setStatusBarColor$default((Fragment) this.this$0, color, false, 4, (Object) null);
    }
}
