package com.discord.widgets.tabs;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.k.b;
import com.discord.databinding.TabsHostBottomNavigationViewBinding;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.user.UserAvatarPresenceView;
import com.discord.views.user.UserAvatarPresenceViewController;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: TabsHostBottomNavigationView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001>B\u0011\b\u0016\u0012\u0006\u00106\u001a\u000205¢\u0006\u0004\b7\u00108B\u001d\b\u0016\u0012\u0006\u00106\u001a\u000205\u0012\n\b\u0002\u0010:\u001a\u0004\u0018\u000109¢\u0006\u0004\b7\u0010;B'\b\u0016\u0012\u0006\u00106\u001a\u000205\u0012\n\b\u0002\u0010:\u001a\u0004\u0018\u000109\u0012\b\b\u0002\u0010<\u001a\u00020\u0005¢\u0006\u0004\b7\u0010=J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u001f\u0010\b\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0085\u0001\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\n2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u00112\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\u00132\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00052\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00020\u00152\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00020\u00152\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u0015¢\u0006\u0004\b\u0019\u0010\u001aJ\u0015\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ/\u0010#\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0006\u0010!\u001a\u00020\u00052\u0006\u0010\"\u001a\u00020\u0005H\u0014¢\u0006\u0004\b#\u0010$R\"\u0010'\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020&0%8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b*\u0010+R\"\u0010,\u001a\u000e\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020\n0%8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b,\u0010(R\"\u0010.\u001a\u000e\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u00020\n0%8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b.\u0010(R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u001c\u00103\u001a\b\u0012\u0004\u0012\u00020\u001b028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u0006?"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;", "Landroid/widget/LinearLayout;", "", "initialize", "()V", "", "homeNotificationsCount", "friendsNotificationsCount", "updateNotificationBadges", "(II)V", "Lcom/discord/widgets/tabs/NavigationTab;", "selectedTab", "Lkotlin/Function1;", "onTabSelected", "", "buttonsEnabled", "", "Lcom/discord/primitives/UserId;", "myUserId", "", "visibleTabs", "Lkotlin/Function0;", "onSearchClick", "onSettingsLongPress", "onMentionsLongPress", "updateView", "(Lcom/discord/widgets/tabs/NavigationTab;Lkotlin/jvm/functions/Function1;ZJLjava/util/Set;IILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;", "heightChangedListener", "addHeightChangedListener", "(Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;)V", "w", "h", "oldw", "oldh", "onSizeChanged", "(IIII)V", "", "Landroid/view/View;", "navigationTabToViewMap", "Ljava/util/Map;", "Lcom/discord/views/user/UserAvatarPresenceViewController;", "userAvatarPresenceViewController", "Lcom/discord/views/user/UserAvatarPresenceViewController;", "iconToNavigationTabMap", "Landroid/widget/ImageView;", "tintableIconToNavigationTabMap", "Lcom/discord/databinding/TabsHostBottomNavigationViewBinding;", "binding", "Lcom/discord/databinding/TabsHostBottomNavigationViewBinding;", "", "heightChangedListeners", "Ljava/util/Set;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "HeightChangedListener", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class TabsHostBottomNavigationView extends LinearLayout {
    private final TabsHostBottomNavigationViewBinding binding;
    private final Set<HeightChangedListener> heightChangedListeners;
    private Map<View, ? extends NavigationTab> iconToNavigationTabMap;
    private Map<NavigationTab, ? extends View> navigationTabToViewMap;
    private Map<ImageView, ? extends NavigationTab> tintableIconToNavigationTabMap;
    private UserAvatarPresenceViewController userAvatarPresenceViewController;

    /* compiled from: TabsHostBottomNavigationView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;", "", "", "height", "", "onHeightChanged", "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface HeightChangedListener {
        void onHeightChanged(int i);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TabsHostBottomNavigationView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        TabsHostBottomNavigationViewBinding a = TabsHostBottomNavigationViewBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "TabsHostBottomNavigation…rom(context), this, true)");
        this.binding = a;
        this.heightChangedListeners = new LinkedHashSet();
    }

    private final void initialize() {
        int i;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        if (resources.getConfiguration().orientation == 1) {
            Resources resources2 = getResources();
            m.checkNotNullExpressionValue(resources2, "resources");
            i = resources2.getDisplayMetrics().widthPixels;
        } else {
            Resources resources3 = getResources();
            m.checkNotNullExpressionValue(resources3, "resources");
            i = resources3.getDisplayMetrics().heightPixels;
        }
        LinearLayout linearLayout = this.binding.l;
        m.checkNotNullExpressionValue(linearLayout, "binding.tabsHostBottomNavTabsContainer");
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        layoutParams.width = i;
        LinearLayout linearLayout2 = this.binding.l;
        m.checkNotNullExpressionValue(linearLayout2, "binding.tabsHostBottomNavTabsContainer");
        linearLayout2.setLayoutParams(layoutParams);
        ImageView imageView = this.binding.e;
        NavigationTab navigationTab = NavigationTab.HOME;
        ImageView imageView2 = this.binding.f2140b;
        NavigationTab navigationTab2 = NavigationTab.FRIENDS;
        ImageView imageView3 = this.binding.j;
        NavigationTab navigationTab3 = NavigationTab.SEARCH;
        ImageView imageView4 = this.binding.h;
        NavigationTab navigationTab4 = NavigationTab.MENTIONS;
        this.tintableIconToNavigationTabMap = h0.mapOf(o.to(imageView, navigationTab), o.to(imageView2, navigationTab2), o.to(imageView3, navigationTab3), o.to(imageView4, navigationTab4));
        UserAvatarPresenceView userAvatarPresenceView = this.binding.m;
        NavigationTab navigationTab5 = NavigationTab.SETTINGS;
        this.iconToNavigationTabMap = h0.mapOf(o.to(this.binding.e, navigationTab), o.to(this.binding.f2140b, navigationTab2), o.to(this.binding.j, navigationTab3), o.to(this.binding.h, navigationTab4), o.to(userAvatarPresenceView, navigationTab5));
        this.navigationTabToViewMap = h0.mapOf(o.to(navigationTab, this.binding.f), o.to(navigationTab2, this.binding.c), o.to(navigationTab3, this.binding.k), o.to(navigationTab4, this.binding.i), o.to(navigationTab5, this.binding.n));
        UserAvatarPresenceView userAvatarPresenceView2 = this.binding.m;
        m.checkNotNullExpressionValue(userAvatarPresenceView2, "binding.tabsHostBottomNavUserAvatarPresenceView");
        this.userAvatarPresenceViewController = new UserAvatarPresenceViewController(userAvatarPresenceView2, null, null, null, 14);
    }

    private final void updateNotificationBadges(int i, int i2) {
        CharSequence c;
        CharSequence c2;
        TextView textView = this.binding.g;
        m.checkNotNullExpressionValue(textView, "binding.tabsHostBottomNavHomeNotificationsBadge");
        textView.setText(String.valueOf(i));
        TextView textView2 = this.binding.g;
        m.checkNotNullExpressionValue(textView2, "binding.tabsHostBottomNavHomeNotificationsBadge");
        int i3 = 8;
        textView2.setVisibility(i > 0 ? 0 : 8);
        TextView textView3 = this.binding.g;
        m.checkNotNullExpressionValue(textView3, "binding.tabsHostBottomNavHomeNotificationsBadge");
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        c = b.c(resources, R.string.mentions_count, new Object[]{String.valueOf(i)}, (r4 & 4) != 0 ? b.d.j : null);
        textView3.setContentDescription(c);
        TextView textView4 = this.binding.d;
        m.checkNotNullExpressionValue(textView4, "binding.tabsHostBottomNavFriendsNotificationsBadge");
        textView4.setText(String.valueOf(i2));
        TextView textView5 = this.binding.d;
        m.checkNotNullExpressionValue(textView5, "binding.tabsHostBottomNavFriendsNotificationsBadge");
        if (i2 > 0) {
            i3 = 0;
        }
        textView5.setVisibility(i3);
        TextView textView6 = this.binding.d;
        m.checkNotNullExpressionValue(textView6, "binding.tabsHostBottomNavFriendsNotificationsBadge");
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        c2 = b.c(resources2, R.string.incoming_friend_requests_count, new Object[]{String.valueOf(i2)}, (r4 & 4) != 0 ? b.d.j : null);
        textView6.setContentDescription(c2);
    }

    public final void addHeightChangedListener(HeightChangedListener heightChangedListener) {
        m.checkNotNullParameter(heightChangedListener, "heightChangedListener");
        this.heightChangedListeners.add(heightChangedListener);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        for (HeightChangedListener heightChangedListener : this.heightChangedListeners) {
            heightChangedListener.onHeightChanged(i2);
        }
    }

    public final void updateView(NavigationTab navigationTab, final Function1<? super NavigationTab, Unit> function1, boolean z2, long j, Set<? extends NavigationTab> set, int i, int i2, final Function0<Unit> function0, Function0<Unit> function02, Function0<Unit> function03) {
        m.checkNotNullParameter(navigationTab, "selectedTab");
        m.checkNotNullParameter(function1, "onTabSelected");
        m.checkNotNullParameter(set, "visibleTabs");
        m.checkNotNullParameter(function0, "onSearchClick");
        m.checkNotNullParameter(function02, "onSettingsLongPress");
        m.checkNotNullParameter(function03, "onMentionsLongPress");
        LinearLayout linearLayout = this.binding.l;
        m.checkNotNullExpressionValue(linearLayout, "binding.tabsHostBottomNavTabsContainer");
        linearLayout.setWeightSum(set.size());
        ConstraintLayout constraintLayout = this.binding.f;
        m.checkNotNullExpressionValue(constraintLayout, "binding.tabsHostBottomNavHomeItem");
        int i3 = 8;
        constraintLayout.setVisibility(set.contains(NavigationTab.HOME) ? 0 : 8);
        ConstraintLayout constraintLayout2 = this.binding.c;
        m.checkNotNullExpressionValue(constraintLayout2, "binding.tabsHostBottomNavFriendsItem");
        constraintLayout2.setVisibility(set.contains(NavigationTab.FRIENDS) ? 0 : 8);
        FrameLayout frameLayout = this.binding.k;
        m.checkNotNullExpressionValue(frameLayout, "binding.tabsHostBottomNavSearchItem");
        frameLayout.setVisibility(set.contains(NavigationTab.SEARCH) ? 0 : 8);
        FrameLayout frameLayout2 = this.binding.i;
        m.checkNotNullExpressionValue(frameLayout2, "binding.tabsHostBottomNavMentionsItem");
        frameLayout2.setVisibility(set.contains(NavigationTab.MENTIONS) ? 0 : 8);
        FrameLayout frameLayout3 = this.binding.n;
        m.checkNotNullExpressionValue(frameLayout3, "binding.tabsHostBottomNavUserSettingsItem");
        if (set.contains(NavigationTab.SETTINGS)) {
            i3 = 0;
        }
        frameLayout3.setVisibility(i3);
        Map<ImageView, ? extends NavigationTab> map = this.tintableIconToNavigationTabMap;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("tintableIconToNavigationTabMap");
        }
        Iterator<T> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            ColorCompatKt.tintWithColor((ImageView) entry.getKey(), ColorCompat.getThemedColor(getContext(), ((NavigationTab) entry.getValue()) == navigationTab ? R.attr.colorTabsIconActive : R.attr.colorInteractiveNormal));
        }
        Map<View, ? extends NavigationTab> map2 = this.iconToNavigationTabMap;
        if (map2 == null) {
            m.throwUninitializedPropertyAccessException("iconToNavigationTabMap");
        }
        Iterator<T> it2 = map2.entrySet().iterator();
        while (it2.hasNext()) {
            Map.Entry entry2 = (Map.Entry) it2.next();
            ((View) entry2.getKey()).setAlpha(navigationTab == ((NavigationTab) entry2.getValue()) ? 1.0f : 0.5f);
        }
        Map<NavigationTab, ? extends View> map3 = this.navigationTabToViewMap;
        if (map3 == null) {
            m.throwUninitializedPropertyAccessException("navigationTabToViewMap");
        }
        Iterator<T> it3 = map3.entrySet().iterator();
        while (it3.hasNext()) {
            Map.Entry entry3 = (Map.Entry) it3.next();
            ((View) entry3.getValue()).setSelected(navigationTab == ((NavigationTab) entry3.getKey()));
        }
        UserAvatarPresenceViewController userAvatarPresenceViewController = this.userAvatarPresenceViewController;
        if (userAvatarPresenceViewController == null) {
            m.throwUninitializedPropertyAccessException("userAvatarPresenceViewController");
        }
        long j2 = userAvatarPresenceViewController.a;
        userAvatarPresenceViewController.a = j;
        if (j2 != j) {
            userAvatarPresenceViewController.bind();
        }
        updateNotificationBadges(i, i2);
        if (z2) {
            this.binding.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tabs.TabsHostBottomNavigationView$updateView$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1.this.invoke(NavigationTab.HOME);
                }
            });
            this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tabs.TabsHostBottomNavigationView$updateView$5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1.this.invoke(NavigationTab.FRIENDS);
                }
            });
            this.binding.k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tabs.TabsHostBottomNavigationView$updateView$6
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function0.this.invoke();
                }
            });
            this.binding.i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tabs.TabsHostBottomNavigationView$updateView$7
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1.this.invoke(NavigationTab.MENTIONS);
                }
            });
            this.binding.n.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.tabs.TabsHostBottomNavigationView$updateView$8
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1.this.invoke(NavigationTab.SETTINGS);
                }
            });
            FrameLayout frameLayout4 = this.binding.n;
            m.checkNotNullExpressionValue(frameLayout4, "binding.tabsHostBottomNavUserSettingsItem");
            ViewExtensions.setOnLongClickListenerConsumeClick(frameLayout4, new TabsHostBottomNavigationView$updateView$9(function02));
            return;
        }
        this.binding.f.setOnClickListener(TabsHostBottomNavigationView$updateView$10.INSTANCE);
        this.binding.c.setOnClickListener(TabsHostBottomNavigationView$updateView$11.INSTANCE);
        this.binding.k.setOnClickListener(TabsHostBottomNavigationView$updateView$12.INSTANCE);
        this.binding.i.setOnClickListener(TabsHostBottomNavigationView$updateView$13.INSTANCE);
        this.binding.n.setOnClickListener(TabsHostBottomNavigationView$updateView$14.INSTANCE);
        this.binding.n.setOnLongClickListener(TabsHostBottomNavigationView$updateView$15.INSTANCE);
        this.binding.i.setOnLongClickListener(TabsHostBottomNavigationView$updateView$16.INSTANCE);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TabsHostBottomNavigationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        TabsHostBottomNavigationViewBinding a = TabsHostBottomNavigationViewBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "TabsHostBottomNavigation…rom(context), this, true)");
        this.binding = a;
        this.heightChangedListeners = new LinkedHashSet();
        initialize();
    }

    public /* synthetic */ TabsHostBottomNavigationView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TabsHostBottomNavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        TabsHostBottomNavigationViewBinding a = TabsHostBottomNavigationViewBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "TabsHostBottomNavigation…rom(context), this, true)");
        this.binding = a;
        this.heightChangedListeners = new LinkedHashSet();
        initialize();
    }

    public /* synthetic */ TabsHostBottomNavigationView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
