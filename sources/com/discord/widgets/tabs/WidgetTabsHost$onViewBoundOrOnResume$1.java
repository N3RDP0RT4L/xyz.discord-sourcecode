package com.discord.widgets.tabs;

import com.discord.widgets.tabs.TabsHostViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetTabsHost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;", "kotlin.jvm.PlatformType", "viewState", "", "invoke", "(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTabsHost$onViewBoundOrOnResume$1 extends o implements Function1<TabsHostViewModel.ViewState, Unit> {
    public final /* synthetic */ WidgetTabsHost this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetTabsHost$onViewBoundOrOnResume$1(WidgetTabsHost widgetTabsHost) {
        super(1);
        this.this$0 = widgetTabsHost;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(TabsHostViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(TabsHostViewModel.ViewState viewState) {
        WidgetTabsHost widgetTabsHost = this.this$0;
        m.checkNotNullExpressionValue(viewState, "viewState");
        widgetTabsHost.updateViews(viewState);
    }
}
