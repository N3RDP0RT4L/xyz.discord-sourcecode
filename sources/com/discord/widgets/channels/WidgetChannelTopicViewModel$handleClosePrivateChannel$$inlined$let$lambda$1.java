package com.discord.widgets.channels;

import android.content.Context;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelTopicViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "com/discord/widgets/channels/WidgetChannelTopicViewModel$handleClosePrivateChannel$1$2", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelTopicViewModel$handleClosePrivateChannel$$inlined$let$lambda$1 extends o implements Function1<Channel, Unit> {
    public final /* synthetic */ Context $context$inlined;
    public final /* synthetic */ WidgetChannelTopicViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelTopicViewModel$handleClosePrivateChannel$$inlined$let$lambda$1(WidgetChannelTopicViewModel widgetChannelTopicViewModel, Context context) {
        super(1);
        this.this$0 = widgetChannelTopicViewModel;
        this.$context$inlined = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
        invoke2(channel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        Integer b2 = ChannelUtils.b(channel);
        if (b2 != null) {
            b.a.d.m.g(this.$context$inlined, b2.intValue(), 0, null, 12);
        }
    }
}
