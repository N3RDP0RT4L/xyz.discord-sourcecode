package com.discord.widgets.channels;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreGuildsNsfw;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserGuildSettings;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetChannelSidebarActionsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\f\r\u000eB\u0017\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\b¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;)V", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSidebarActionsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.WidgetChannelSidebarActionsViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetChannelSidebarActionsViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011JG\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\b\b\u0002\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Companion;", "", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreUserGuildSettings;", "storeUserGuildSettings", "Lcom/discord/stores/StoreGuildsNsfw;", "storeGuildNSFW", "Lcom/discord/stores/StoreUser;", "storeUser", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStoreState(StoreNavigation storeNavigation, StoreChannelsSelected storeChannelsSelected, StoreUserGuildSettings storeUserGuildSettings, StoreGuildsNsfw storeGuildsNsfw, StoreUser storeUser) {
            Observable Y = storeNavigation.observeRightPanelState().Y(new WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1(storeChannelsSelected, storeUserGuildSettings, storeUser, storeGuildsNsfw));
            m.checkNotNullExpressionValue(Y, "storeNavigation\n        …          }\n            }");
            return Y;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, StoreNavigation storeNavigation, StoreChannelsSelected storeChannelsSelected, StoreUserGuildSettings storeUserGuildSettings, StoreGuildsNsfw storeGuildsNsfw, StoreUser storeUser, int i, Object obj) {
            if ((i & 1) != 0) {
                storeNavigation = StoreStream.Companion.getNavigation();
            }
            if ((i & 2) != 0) {
                storeChannelsSelected = StoreStream.Companion.getChannelsSelected();
            }
            StoreChannelsSelected storeChannelsSelected2 = storeChannelsSelected;
            if ((i & 4) != 0) {
                storeUserGuildSettings = StoreStream.Companion.getUserGuildSettings();
            }
            StoreUserGuildSettings storeUserGuildSettings2 = storeUserGuildSettings;
            if ((i & 8) != 0) {
                storeGuildsNsfw = StoreStream.Companion.getGuildsNsfw();
            }
            StoreGuildsNsfw storeGuildsNsfw2 = storeGuildsNsfw;
            if ((i & 16) != 0) {
                storeUser = StoreStream.Companion.getUsers();
            }
            return companion.observeStoreState(storeNavigation, storeChannelsSelected2, storeUserGuildSettings2, storeGuildsNsfw2, storeUser);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", "", HookHelper.constructorName, "()V", "ChannelFound", "ChannelNotFound", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelNotFound;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\u0007R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004¨\u0006\""}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/domain/ModelNotificationSettings;", "component2", "()Lcom/discord/models/domain/ModelNotificationSettings;", "", "component3", "()Z", "channel", "guildNotificationSettings", "disablePins", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/domain/ModelNotificationSettings;Z)Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelNotificationSettings;", "getGuildNotificationSettings", "Z", "getDisablePins", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/domain/ModelNotificationSettings;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ChannelFound extends StoreState {
            private final Channel channel;
            private final boolean disablePins;
            private final ModelNotificationSettings guildNotificationSettings;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ChannelFound(Channel channel, ModelNotificationSettings modelNotificationSettings, boolean z2) {
                super(null);
                m.checkNotNullParameter(channel, "channel");
                this.channel = channel;
                this.guildNotificationSettings = modelNotificationSettings;
                this.disablePins = z2;
            }

            public static /* synthetic */ ChannelFound copy$default(ChannelFound channelFound, Channel channel, ModelNotificationSettings modelNotificationSettings, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = channelFound.channel;
                }
                if ((i & 2) != 0) {
                    modelNotificationSettings = channelFound.guildNotificationSettings;
                }
                if ((i & 4) != 0) {
                    z2 = channelFound.disablePins;
                }
                return channelFound.copy(channel, modelNotificationSettings, z2);
            }

            public final Channel component1() {
                return this.channel;
            }

            public final ModelNotificationSettings component2() {
                return this.guildNotificationSettings;
            }

            public final boolean component3() {
                return this.disablePins;
            }

            public final ChannelFound copy(Channel channel, ModelNotificationSettings modelNotificationSettings, boolean z2) {
                m.checkNotNullParameter(channel, "channel");
                return new ChannelFound(channel, modelNotificationSettings, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ChannelFound)) {
                    return false;
                }
                ChannelFound channelFound = (ChannelFound) obj;
                return m.areEqual(this.channel, channelFound.channel) && m.areEqual(this.guildNotificationSettings, channelFound.guildNotificationSettings) && this.disablePins == channelFound.disablePins;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final boolean getDisablePins() {
                return this.disablePins;
            }

            public final ModelNotificationSettings getGuildNotificationSettings() {
                return this.guildNotificationSettings;
            }

            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                ModelNotificationSettings modelNotificationSettings = this.guildNotificationSettings;
                if (modelNotificationSettings != null) {
                    i = modelNotificationSettings.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.disablePins;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("ChannelFound(channel=");
                R.append(this.channel);
                R.append(", guildNotificationSettings=");
                R.append(this.guildNotificationSettings);
                R.append(", disablePins=");
                return a.M(R, this.disablePins, ")");
            }
        }

        /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelNotFound;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ChannelNotFound extends StoreState {
            public static final ChannelNotFound INSTANCE = new ChannelNotFound();

            private ChannelNotFound() {
                super(null);
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Guild", "Private", "Uninitialized", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001BG\u0012\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\b\u0012\u0006\u0010\u0014\u001a\u00020\b\u0012\u0006\u0010\u0015\u001a\u00020\b¢\u0006\u0004\b)\u0010*J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0010\u0010\f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0010\u0010\r\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\r\u0010\nJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ^\u0010\u0016\u001a\u00020\u00002\f\b\u0002\u0010\u000f\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\u0011\u001a\u00020\b2\b\b\u0002\u0010\u0012\u001a\u00020\b2\b\b\u0002\u0010\u0013\u001a\u00020\b2\b\b\u0002\u0010\u0014\u001a\u00020\b2\b\b\u0002\u0010\u0015\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010 \u001a\u00020\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b\u0011\u0010\nR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010#\u001a\u0004\b$\u0010\u0005R\u001d\u0010\u000f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010#\u001a\u0004\b%\u0010\u0005R\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b&\u0010\nR\u0019\u0010\u0013\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b'\u0010\nR\u0019\u0010\u0015\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\"\u001a\u0004\b(\u0010\nR\u0019\u0010\u0014\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\"\u001a\u0004\b\u0014\u0010\n¨\u0006+"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "", "component3", "()Z", "component4", "component5", "component6", "component7", "channelId", "guildId", "isMuted", "hasUnreadPins", "disablePins", "isThread", "shouldHideChannelSidebar", "copy", "(JJZZZZZ)Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "J", "getGuildId", "getChannelId", "getHasUnreadPins", "getDisablePins", "getShouldHideChannelSidebar", HookHelper.constructorName, "(JJZZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Guild extends ViewState {
            private final long channelId;
            private final boolean disablePins;
            private final long guildId;
            private final boolean hasUnreadPins;
            private final boolean isMuted;
            private final boolean isThread;
            private final boolean shouldHideChannelSidebar;

            public Guild(long j, long j2, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
                super(null);
                this.channelId = j;
                this.guildId = j2;
                this.isMuted = z2;
                this.hasUnreadPins = z3;
                this.disablePins = z4;
                this.isThread = z5;
                this.shouldHideChannelSidebar = z6;
            }

            public final long component1() {
                return this.channelId;
            }

            public final long component2() {
                return this.guildId;
            }

            public final boolean component3() {
                return this.isMuted;
            }

            public final boolean component4() {
                return this.hasUnreadPins;
            }

            public final boolean component5() {
                return this.disablePins;
            }

            public final boolean component6() {
                return this.isThread;
            }

            public final boolean component7() {
                return this.shouldHideChannelSidebar;
            }

            public final Guild copy(long j, long j2, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
                return new Guild(j, j2, z2, z3, z4, z5, z6);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Guild)) {
                    return false;
                }
                Guild guild = (Guild) obj;
                return this.channelId == guild.channelId && this.guildId == guild.guildId && this.isMuted == guild.isMuted && this.hasUnreadPins == guild.hasUnreadPins && this.disablePins == guild.disablePins && this.isThread == guild.isThread && this.shouldHideChannelSidebar == guild.shouldHideChannelSidebar;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final boolean getDisablePins() {
                return this.disablePins;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public final boolean getHasUnreadPins() {
                return this.hasUnreadPins;
            }

            public final boolean getShouldHideChannelSidebar() {
                return this.shouldHideChannelSidebar;
            }

            public int hashCode() {
                int a = (b.a(this.guildId) + (b.a(this.channelId) * 31)) * 31;
                boolean z2 = this.isMuted;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (a + i2) * 31;
                boolean z3 = this.hasUnreadPins;
                if (z3) {
                    z3 = true;
                }
                int i5 = z3 ? 1 : 0;
                int i6 = z3 ? 1 : 0;
                int i7 = (i4 + i5) * 31;
                boolean z4 = this.disablePins;
                if (z4) {
                    z4 = true;
                }
                int i8 = z4 ? 1 : 0;
                int i9 = z4 ? 1 : 0;
                int i10 = (i7 + i8) * 31;
                boolean z5 = this.isThread;
                if (z5) {
                    z5 = true;
                }
                int i11 = z5 ? 1 : 0;
                int i12 = z5 ? 1 : 0;
                int i13 = (i10 + i11) * 31;
                boolean z6 = this.shouldHideChannelSidebar;
                if (!z6) {
                    i = z6 ? 1 : 0;
                }
                return i13 + i;
            }

            public final boolean isMuted() {
                return this.isMuted;
            }

            public final boolean isThread() {
                return this.isThread;
            }

            public String toString() {
                StringBuilder R = a.R("Guild(channelId=");
                R.append(this.channelId);
                R.append(", guildId=");
                R.append(this.guildId);
                R.append(", isMuted=");
                R.append(this.isMuted);
                R.append(", hasUnreadPins=");
                R.append(this.hasUnreadPins);
                R.append(", disablePins=");
                R.append(this.disablePins);
                R.append(", isThread=");
                R.append(this.isThread);
                R.append(", shouldHideChannelSidebar=");
                return a.M(R, this.shouldHideChannelSidebar, ")");
            }
        }

        /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00062\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\n\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "component2", "()Z", "channelId", "isMuted", "copy", "(JZ)Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Z", HookHelper.constructorName, "(JZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Private extends ViewState {
            private final long channelId;
            private final boolean isMuted;

            public Private(long j, boolean z2) {
                super(null);
                this.channelId = j;
                this.isMuted = z2;
            }

            public static /* synthetic */ Private copy$default(Private r0, long j, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = r0.channelId;
                }
                if ((i & 2) != 0) {
                    z2 = r0.isMuted;
                }
                return r0.copy(j, z2);
            }

            public final long component1() {
                return this.channelId;
            }

            public final boolean component2() {
                return this.isMuted;
            }

            public final Private copy(long j, boolean z2) {
                return new Private(j, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Private)) {
                    return false;
                }
                Private r6 = (Private) obj;
                return this.channelId == r6.channelId && this.isMuted == r6.isMuted;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                int a = b.a(this.channelId) * 31;
                boolean z2 = this.isMuted;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return a + i;
            }

            public final boolean isMuted() {
                return this.isMuted;
            }

            public String toString() {
                StringBuilder R = a.R("Private(channelId=");
                R.append(this.channelId);
                R.append(", isMuted=");
                return a.M(R, this.isMuted, ")");
            }
        }

        /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChannelSidebarActionsViewModel() {
        this(null, 1, null);
    }

    public /* synthetic */ WidgetChannelSidebarActionsViewModel(Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? Companion.observeStoreState$default(Companion, null, null, null, null, null, 31, null) : observable);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        ModelNotificationSettings.ChannelOverride channelOverride;
        List<ModelNotificationSettings.ChannelOverride> channelOverrides;
        Object obj;
        boolean z2;
        if (m.areEqual(storeState, StoreState.ChannelNotFound.INSTANCE)) {
            updateViewState(ViewState.Uninitialized.INSTANCE);
        } else if (storeState instanceof StoreState.ChannelFound) {
            StoreState.ChannelFound channelFound = (StoreState.ChannelFound) storeState;
            boolean z3 = false;
            if (ChannelUtils.x(channelFound.getChannel())) {
                ModelNotificationSettings guildNotificationSettings = channelFound.getGuildNotificationSettings();
                long h = channelFound.getChannel().h();
                if (!(guildNotificationSettings == null || (channelOverrides = guildNotificationSettings.getChannelOverrides()) == null)) {
                    Iterator<T> it = channelOverrides.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it.next();
                        ModelNotificationSettings.ChannelOverride channelOverride2 = (ModelNotificationSettings.ChannelOverride) obj;
                        m.checkNotNullExpressionValue(channelOverride2, "channelOverride");
                        if (channelOverride2.getChannelId() == h) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            break;
                        }
                    }
                    ModelNotificationSettings.ChannelOverride channelOverride3 = (ModelNotificationSettings.ChannelOverride) obj;
                    if (channelOverride3 != null && channelOverride3.isMuted()) {
                        z3 = true;
                    }
                }
                updateViewState(new ViewState.Private(channelFound.getChannel().h(), z3));
                return;
            }
            long h2 = channelFound.getChannel().h();
            long f = channelFound.getChannel().f();
            ModelNotificationSettings guildNotificationSettings2 = channelFound.getGuildNotificationSettings();
            updateViewState(new ViewState.Guild(h2, f, (guildNotificationSettings2 == null || (channelOverride = guildNotificationSettings2.getChannelOverride(h2)) == null || !channelOverride.isMuted()) ? false : true, false, channelFound.getDisablePins(), ChannelUtils.C(channelFound.getChannel()), ChannelUtils.o(channelFound.getChannel())));
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSidebarActionsViewModel(Observable<StoreState> observable) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(observable, "storeStateObservable");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetChannelSidebarActionsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
