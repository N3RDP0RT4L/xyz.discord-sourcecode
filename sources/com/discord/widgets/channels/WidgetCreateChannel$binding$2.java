package com.discord.widgets.channels;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetCreateChannelBinding;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetCreateChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetCreateChannelBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetCreateChannelBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetCreateChannel$binding$2 extends k implements Function1<View, WidgetCreateChannelBinding> {
    public static final WidgetCreateChannel$binding$2 INSTANCE = new WidgetCreateChannel$binding$2();

    public WidgetCreateChannel$binding$2() {
        super(1, WidgetCreateChannelBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetCreateChannelBinding;", 0);
    }

    public final WidgetCreateChannelBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.create_channel_announcements_container;
        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.create_channel_announcements_container);
        if (constraintLayout != null) {
            i = R.id.create_channel_announcements_icon;
            ImageView imageView = (ImageView) view.findViewById(R.id.create_channel_announcements_icon);
            if (imageView != null) {
                i = R.id.create_channel_announcements_radio;
                RadioButton radioButton = (RadioButton) view.findViewById(R.id.create_channel_announcements_radio);
                if (radioButton != null) {
                    i = R.id.create_channel_announcements_title;
                    TextView textView = (TextView) view.findViewById(R.id.create_channel_announcements_title);
                    if (textView != null) {
                        i = R.id.create_channel_name_layout;
                        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.create_channel_name_layout);
                        if (textInputLayout != null) {
                            i = R.id.create_channel_private_container;
                            RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.create_channel_private_container);
                            if (relativeLayout != null) {
                                i = R.id.create_channel_private_info;
                                TextView textView2 = (TextView) view.findViewById(R.id.create_channel_private_info);
                                if (textView2 != null) {
                                    i = R.id.create_channel_private_switch;
                                    SwitchCompat switchCompat = (SwitchCompat) view.findViewById(R.id.create_channel_private_switch);
                                    if (switchCompat != null) {
                                        i = R.id.create_channel_private_title;
                                        TextView textView3 = (TextView) view.findViewById(R.id.create_channel_private_title);
                                        if (textView3 != null) {
                                            i = R.id.create_channel_role_access_recycler;
                                            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.create_channel_role_access_recycler);
                                            if (recyclerView != null) {
                                                i = R.id.create_channel_role_divider;
                                                View findViewById = view.findViewById(R.id.create_channel_role_divider);
                                                if (findViewById != null) {
                                                    i = R.id.create_channel_role_header;
                                                    TextView textView4 = (TextView) view.findViewById(R.id.create_channel_role_header);
                                                    if (textView4 != null) {
                                                        i = R.id.create_channel_stage_container;
                                                        ConstraintLayout constraintLayout2 = (ConstraintLayout) view.findViewById(R.id.create_channel_stage_container);
                                                        if (constraintLayout2 != null) {
                                                            i = R.id.create_channel_stage_icon;
                                                            ImageView imageView2 = (ImageView) view.findViewById(R.id.create_channel_stage_icon);
                                                            if (imageView2 != null) {
                                                                i = R.id.create_channel_stage_radio;
                                                                RadioButton radioButton2 = (RadioButton) view.findViewById(R.id.create_channel_stage_radio);
                                                                if (radioButton2 != null) {
                                                                    i = R.id.create_channel_stage_title;
                                                                    TextView textView5 = (TextView) view.findViewById(R.id.create_channel_stage_title);
                                                                    if (textView5 != null) {
                                                                        i = R.id.create_channel_text_container;
                                                                        ConstraintLayout constraintLayout3 = (ConstraintLayout) view.findViewById(R.id.create_channel_text_container);
                                                                        if (constraintLayout3 != null) {
                                                                            i = R.id.create_channel_text_icon;
                                                                            ImageView imageView3 = (ImageView) view.findViewById(R.id.create_channel_text_icon);
                                                                            if (imageView3 != null) {
                                                                                i = R.id.create_channel_text_radio;
                                                                                RadioButton radioButton3 = (RadioButton) view.findViewById(R.id.create_channel_text_radio);
                                                                                if (radioButton3 != null) {
                                                                                    i = R.id.create_channel_text_title;
                                                                                    TextView textView6 = (TextView) view.findViewById(R.id.create_channel_text_title);
                                                                                    if (textView6 != null) {
                                                                                        i = R.id.create_channel_type_container;
                                                                                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.create_channel_type_container);
                                                                                        if (linearLayout != null) {
                                                                                            i = R.id.create_channel_voice_container;
                                                                                            ConstraintLayout constraintLayout4 = (ConstraintLayout) view.findViewById(R.id.create_channel_voice_container);
                                                                                            if (constraintLayout4 != null) {
                                                                                                i = R.id.create_channel_voice_icon;
                                                                                                ImageView imageView4 = (ImageView) view.findViewById(R.id.create_channel_voice_icon);
                                                                                                if (imageView4 != null) {
                                                                                                    i = R.id.create_channel_voice_radio;
                                                                                                    RadioButton radioButton4 = (RadioButton) view.findViewById(R.id.create_channel_voice_radio);
                                                                                                    if (radioButton4 != null) {
                                                                                                        i = R.id.create_channel_voice_title;
                                                                                                        TextView textView7 = (TextView) view.findViewById(R.id.create_channel_voice_title);
                                                                                                        if (textView7 != null) {
                                                                                                            return new WidgetCreateChannelBinding((CoordinatorLayout) view, constraintLayout, imageView, radioButton, textView, textInputLayout, relativeLayout, textView2, switchCompat, textView3, recyclerView, findViewById, textView4, constraintLayout2, imageView2, radioButton2, textView5, constraintLayout3, imageView3, radioButton3, textView6, linearLayout, constraintLayout4, imageView4, radioButton4, textView7);
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
