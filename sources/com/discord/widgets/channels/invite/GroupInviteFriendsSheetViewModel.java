package com.discord.widgets.channels.invite;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppViewModel;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserRelationships;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.channels.invite.GroupInviteFriendsSheetAdapter;
import d0.g0.w;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
/* compiled from: GroupInviteFriendsSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 ;2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004;<=>B7\u0012\n\u0010+\u001a\u00060\tj\u0002`*\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010(\u001a\u00020'\u0012\u000e\b\u0002\u00108\u001a\b\u0012\u0004\u0012\u00020\u00030\u001e¢\u0006\u0004\b9\u0010:J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J7\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000f2\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u001d\u0010\u001b\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u001d\u0010\u0015J\u0013\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e¢\u0006\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u001a\u0010+\u001a\u00060\tj\u0002`*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R&\u0010/\u001a\u0012\u0012\u0004\u0012\u00020\u000b0-j\b\u0012\u0004\u0012\u00020\u000b`.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R:\u00103\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00120\u0012 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00120\u0012\u0018\u000101018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R:\u00106\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\u001f0\u001f 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\u001f0\u001f\u0018\u000105058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107¨\u0006?"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$ViewState;", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;)V", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "users", "Lcom/discord/api/channel/Channel;", "channel", "", "excludeUsersAlreadyInChannel", "(Ljava/util/Map;Lcom/discord/api/channel/Channel;)Ljava/util/List;", "", "searchText", "onSearch", "(Ljava/lang/CharSequence;)V", "emitChannelFullEvent", "()V", "user", "", "isChecked", "onChangeUserChecked", "(Lcom/discord/models/user/User;Z)V", "onSearchTextChanged", "Lrx/Observable;", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "currentStoreState", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;", "", "maxNumMembers", "I", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "checkedUsers", "Ljava/util/HashSet;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "searchTextChangedPublisher", "Lrx/subjects/BehaviorSubject;", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/subjects/PublishSubject;", "storeStateObservable", HookHelper.constructorName, "(JLcom/discord/stores/StoreUser;ILrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GroupInviteFriendsSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_GROUP_MEMBERS = 10;
    private static final int MAX_GROUP_MEMBERS_STAFF = 25;
    private static final long SEARCH_DEBOUNCE_MILLIS = 150;
    private final long channelId;
    private final HashSet<User> checkedUsers;
    private StoreState currentStoreState;
    private final PublishSubject<Event> eventSubject;
    private final int maxNumMembers;
    private final BehaviorSubject<CharSequence> searchTextChangedPublisher;
    private final StoreUser storeUser;

    /* compiled from: GroupInviteFriendsSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GroupInviteFriendsSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GroupInviteFriendsSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "searchText", "", "invoke", "(Ljava/lang/CharSequence;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<CharSequence, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(CharSequence charSequence) {
            invoke2(charSequence);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(CharSequence charSequence) {
            GroupInviteFriendsSheetViewModel groupInviteFriendsSheetViewModel = GroupInviteFriendsSheetViewModel.this;
            m.checkNotNullExpressionValue(charSequence, "searchText");
            groupInviteFriendsSheetViewModel.onSearch(charSequence);
        }
    }

    /* compiled from: GroupInviteFriendsSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ9\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\r\u0010\u000eJ5\u0010\u0012\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000f0\u000b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0015\u001a\u00020\u00142\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lrx/Observable;", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;)Lrx/Observable;", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "observeFriends", "(Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreUser;)Lrx/Observable;", "", "getMaxNumMembers", "(Lcom/discord/stores/StoreUser;)I", "MAX_GROUP_MEMBERS", "I", "MAX_GROUP_MEMBERS_STAFF", "SEARCH_DEBOUNCE_MILLIS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final int getMaxNumMembers(StoreUser storeUser) {
            return UserUtils.INSTANCE.isStaff(storeUser.getMe()) ? 25 : 10;
        }

        private final Observable<Map<Long, User>> observeFriends(StoreUserRelationships storeUserRelationships, final StoreUser storeUser) {
            Observable Y = storeUserRelationships.observeForType(1).Y(new b<Map<Long, ? extends Integer>, Observable<? extends Map<Long, ? extends User>>>() { // from class: com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel$Companion$observeFriends$1
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends Map<Long, ? extends User>> call(Map<Long, ? extends Integer> map) {
                    return call2((Map<Long, Integer>) map);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends Map<Long, User>> call2(Map<Long, Integer> map) {
                    return StoreUser.this.observeUsers(map.keySet());
                }
            });
            m.checkNotNullExpressionValue(Y, "storeUserRelationships.o…ships.keys)\n            }");
            return Y;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, StoreUserRelationships storeUserRelationships, StoreUser storeUser, StoreChannels storeChannels) {
            if (j == -1) {
                Observable F = observeFriends(storeUserRelationships, storeUser).F(GroupInviteFriendsSheetViewModel$Companion$observeStoreState$1.INSTANCE);
                m.checkNotNullExpressionValue(F, "observeFriends(storeUser…          )\n            }");
                return F;
            }
            Observable<StoreState> j2 = Observable.j(storeChannels.observeChannel(j), observeFriends(storeUserRelationships, storeUser), GroupInviteFriendsSheetViewModel$Companion$observeStoreState$2.INSTANCE);
            m.checkNotNullExpressionValue(j2, "Observable\n             …      )\n                }");
            return j2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GroupInviteFriendsSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "ChannelFull", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$Event$ChannelFull;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: GroupInviteFriendsSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$Event$ChannelFull;", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ChannelFull extends Event {
            public static final ChannelFull INSTANCE = new ChannelFull();

            private ChannelFull() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GroupInviteFriendsSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0018\b\u0002\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b\u001d\u0010\u001eJ \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ6\u0010\r\u001a\u00020\u00002\u0018\b\u0002\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001b\u0010\f\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\nR)\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;", "", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component1", "()Ljava/util/Map;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "friendUsersMap", "channel", "copy", "(Ljava/util/Map;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/Map;", "getFriendUsersMap", HookHelper.constructorName, "(Ljava/util/Map;Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Channel channel;
        private final Map<Long, User> friendUsersMap;

        public StoreState() {
            this(null, null, 3, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(Map<Long, ? extends User> map, Channel channel) {
            m.checkNotNullParameter(map, "friendUsersMap");
            this.friendUsersMap = map;
            this.channel = channel;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, Map map, Channel channel, int i, Object obj) {
            if ((i & 1) != 0) {
                map = storeState.friendUsersMap;
            }
            if ((i & 2) != 0) {
                channel = storeState.channel;
            }
            return storeState.copy(map, channel);
        }

        public final Map<Long, User> component1() {
            return this.friendUsersMap;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final StoreState copy(Map<Long, ? extends User> map, Channel channel) {
            m.checkNotNullParameter(map, "friendUsersMap");
            return new StoreState(map, channel);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.friendUsersMap, storeState.friendUsersMap) && m.areEqual(this.channel, storeState.channel);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Map<Long, User> getFriendUsersMap() {
            return this.friendUsersMap;
        }

        public int hashCode() {
            Map<Long, User> map = this.friendUsersMap;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            Channel channel = this.channel;
            if (channel != null) {
                i = channel.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(friendUsersMap=");
            R.append(this.friendUsersMap);
            R.append(", channel=");
            R.append(this.channel);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ StoreState(Map map, Channel channel, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? h0.emptyMap() : map, (i & 2) != 0 ? null : channel);
        }
    }

    /* compiled from: GroupInviteFriendsSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0016\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u0004\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000b¢\u0006\u0004\b\"\u0010#J \u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u0004HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJD\u0010\u0011\u001a\u00020\u00002\u0018\b\u0002\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u00042\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\b\b\u0002\u0010\u0010\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001a\u001a\u00020\u000b2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR)\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u001d\u0010\u0006R\u0019\u0010\u0010\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\rR\u001f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\n¨\u0006$"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$ViewState;", "", "Ljava/util/HashSet;", "Lcom/discord/models/user/User;", "Lkotlin/collections/HashSet;", "component1", "()Ljava/util/HashSet;", "", "Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetAdapter$FriendItem;", "component2", "()Ljava/util/List;", "", "component3", "()Z", "checkedUsers", "friendItems", "showSearchIcon", "copy", "(Ljava/util/HashSet;Ljava/util/List;Z)Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheetViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/HashSet;", "getCheckedUsers", "Z", "getShowSearchIcon", "Ljava/util/List;", "getFriendItems", HookHelper.constructorName, "(Ljava/util/HashSet;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final HashSet<User> checkedUsers;
        private final List<GroupInviteFriendsSheetAdapter.FriendItem> friendItems;
        private final boolean showSearchIcon;

        public ViewState(HashSet<User> hashSet, List<GroupInviteFriendsSheetAdapter.FriendItem> list, boolean z2) {
            m.checkNotNullParameter(hashSet, "checkedUsers");
            m.checkNotNullParameter(list, "friendItems");
            this.checkedUsers = hashSet;
            this.friendItems = list;
            this.showSearchIcon = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, HashSet hashSet, List list, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                hashSet = viewState.checkedUsers;
            }
            if ((i & 2) != 0) {
                list = viewState.friendItems;
            }
            if ((i & 4) != 0) {
                z2 = viewState.showSearchIcon;
            }
            return viewState.copy(hashSet, list, z2);
        }

        public final HashSet<User> component1() {
            return this.checkedUsers;
        }

        public final List<GroupInviteFriendsSheetAdapter.FriendItem> component2() {
            return this.friendItems;
        }

        public final boolean component3() {
            return this.showSearchIcon;
        }

        public final ViewState copy(HashSet<User> hashSet, List<GroupInviteFriendsSheetAdapter.FriendItem> list, boolean z2) {
            m.checkNotNullParameter(hashSet, "checkedUsers");
            m.checkNotNullParameter(list, "friendItems");
            return new ViewState(hashSet, list, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.checkedUsers, viewState.checkedUsers) && m.areEqual(this.friendItems, viewState.friendItems) && this.showSearchIcon == viewState.showSearchIcon;
        }

        public final HashSet<User> getCheckedUsers() {
            return this.checkedUsers;
        }

        public final List<GroupInviteFriendsSheetAdapter.FriendItem> getFriendItems() {
            return this.friendItems;
        }

        public final boolean getShowSearchIcon() {
            return this.showSearchIcon;
        }

        public int hashCode() {
            HashSet<User> hashSet = this.checkedUsers;
            int i = 0;
            int hashCode = (hashSet != null ? hashSet.hashCode() : 0) * 31;
            List<GroupInviteFriendsSheetAdapter.FriendItem> list = this.friendItems;
            if (list != null) {
                i = list.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.showSearchIcon;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(checkedUsers=");
            R.append(this.checkedUsers);
            R.append(", friendItems=");
            R.append(this.friendItems);
            R.append(", showSearchIcon=");
            return a.M(R, this.showSearchIcon, ")");
        }

        public /* synthetic */ ViewState(HashSet hashSet, List list, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(hashSet, list, (i & 4) != 0 ? true : z2);
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GroupInviteFriendsSheetViewModel(long r7, com.discord.stores.StoreUser r9, int r10, rx.Observable r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r6 = this;
            r13 = r12 & 2
            if (r13 == 0) goto La
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r9 = r9.getUsers()
        La:
            r13 = r12 & 4
            if (r13 == 0) goto L14
            com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel$Companion r10 = com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel.Companion
            int r10 = com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel.Companion.access$getMaxNumMembers(r10, r9)
        L14:
            r12 = r12 & 8
            if (r12 == 0) goto L2a
            com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel$Companion r0 = com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel.Companion
            com.discord.stores.StoreStream$Companion r11 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUserRelationships r3 = r11.getUserRelationships()
            com.discord.stores.StoreChannels r5 = r11.getChannels()
            r1 = r7
            r4 = r9
            rx.Observable r11 = com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel.Companion.access$observeStoreState(r0, r1, r3, r4, r5)
        L2a:
            r5 = r11
            r0 = r6
            r1 = r7
            r3 = r9
            r4 = r10
            r0.<init>(r1, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.invite.GroupInviteFriendsSheetViewModel.<init>(long, com.discord.stores.StoreUser, int, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void emitChannelFullEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ChannelFull.INSTANCE);
    }

    private final List<User> excludeUsersAlreadyInChannel(Map<Long, ? extends User> map, Channel channel) {
        Set set;
        List<com.discord.api.user.User> w;
        if (channel == null || (w = channel.w()) == null) {
            set = null;
        } else {
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(w, 10));
            for (com.discord.api.user.User user : w) {
                arrayList.add(Long.valueOf(user.i()));
            }
            set = u.toSet(arrayList);
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, ? extends User> entry : map.entrySet()) {
            boolean z2 = true;
            if (set != null && set.contains(entry.getKey())) {
                z2 = false;
            }
            if (z2) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return u.toList(linkedHashMap.values());
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        this.currentStoreState = storeState;
        List<User> excludeUsersAlreadyInChannel = excludeUsersAlreadyInChannel(storeState.getFriendUsersMap(), storeState.getChannel());
        HashSet<User> hashSet = this.checkedUsers;
        ArrayList<GroupInviteFriendsSheetAdapter.FriendItem> createItems = GroupInviteFriendsSheetAdapter.FriendItem.Companion.createItems(excludeUsersAlreadyInChannel, hashSet);
        ViewState viewState = getViewState();
        updateViewState(new ViewState(hashSet, createItems, viewState != null ? viewState.getShowSearchIcon() : true));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSearch(CharSequence charSequence) {
        boolean z2;
        Collection<User> values = this.currentStoreState.getFriendUsersMap().values();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (true) {
            z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (w.contains((CharSequence) ((User) next).getUsername(), charSequence, true)) {
                arrayList.add(next);
            }
        }
        HashSet<User> hashSet = this.checkedUsers;
        ArrayList<GroupInviteFriendsSheetAdapter.FriendItem> createItems = GroupInviteFriendsSheetAdapter.FriendItem.Companion.createItems(arrayList, hashSet);
        if (!(charSequence.length() == 0) || !this.checkedUsers.isEmpty()) {
            z2 = false;
        }
        updateViewState(new ViewState(hashSet, createItems, z2));
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void onChangeUserChecked(User user, boolean z2) {
        m.checkNotNullParameter(user, "user");
        ViewState viewState = getViewState();
        if (viewState != null) {
            if (!z2) {
                this.checkedUsers.remove(user);
            } else if (this.checkedUsers.size() < this.maxNumMembers) {
                this.checkedUsers.add(user);
            } else {
                emitChannelFullEvent();
            }
            List<GroupInviteFriendsSheetAdapter.FriendItem> friendItems = viewState.getFriendItems();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(friendItems, 10));
            for (GroupInviteFriendsSheetAdapter.FriendItem friendItem : friendItems) {
                arrayList.add(friendItem.getUser());
            }
            HashSet<User> hashSet = this.checkedUsers;
            updateViewState(ViewState.copy$default(viewState, hashSet, GroupInviteFriendsSheetAdapter.FriendItem.Companion.createItems(arrayList, hashSet), false, 4, null));
        }
    }

    public final void onSearchTextChanged(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "searchText");
        this.searchTextChangedPublisher.onNext(charSequence);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GroupInviteFriendsSheetViewModel(long j, StoreUser storeUser, int i, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.channelId = j;
        this.storeUser = storeUser;
        this.maxNumMembers = i;
        this.eventSubject = PublishSubject.k0();
        this.currentStoreState = new StoreState(null, null, 3, null);
        this.checkedUsers = new HashSet<>();
        BehaviorSubject<CharSequence> k0 = BehaviorSubject.k0();
        this.searchTextChangedPublisher = k0;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), GroupInviteFriendsSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        Observable<CharSequence> o = k0.o(150L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "searchTextChangedPublish…S, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(o, this, null, 2, null), GroupInviteFriendsSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
