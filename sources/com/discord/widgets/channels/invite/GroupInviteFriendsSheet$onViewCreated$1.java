package com.discord.widgets.channels.invite;

import com.discord.models.user.User;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: GroupInviteFriendsSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/models/user/User;", "user", "", "isChecked", "", "invoke", "(Lcom/discord/models/user/User;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GroupInviteFriendsSheet$onViewCreated$1 extends o implements Function2<User, Boolean, Unit> {
    public final /* synthetic */ GroupInviteFriendsSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GroupInviteFriendsSheet$onViewCreated$1(GroupInviteFriendsSheet groupInviteFriendsSheet) {
        super(2);
        this.this$0 = groupInviteFriendsSheet;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(User user, Boolean bool) {
        invoke(user, bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(User user, boolean z2) {
        GroupInviteFriendsSheetViewModel viewModel;
        m.checkNotNullParameter(user, "user");
        viewModel = this.this$0.getViewModel();
        viewModel.onChangeUserChecked(user, z2);
    }
}
