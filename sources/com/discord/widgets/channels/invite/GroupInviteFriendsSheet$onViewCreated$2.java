package com.discord.widgets.channels.invite;

import com.discord.models.user.User;
import com.discord.widgets.channels.invite.GroupInviteFriendsSheet;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GroupInviteFriendsSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheet$UserDataContract;", "deletedChip", "", "invoke", "(Lcom/discord/widgets/channels/invite/GroupInviteFriendsSheet$UserDataContract;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GroupInviteFriendsSheet$onViewCreated$2 extends o implements Function1<GroupInviteFriendsSheet.UserDataContract, Unit> {
    public final /* synthetic */ GroupInviteFriendsSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GroupInviteFriendsSheet$onViewCreated$2(GroupInviteFriendsSheet groupInviteFriendsSheet) {
        super(1);
        this.this$0 = groupInviteFriendsSheet;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GroupInviteFriendsSheet.UserDataContract userDataContract) {
        invoke2(userDataContract);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GroupInviteFriendsSheet.UserDataContract userDataContract) {
        User modelUser;
        GroupInviteFriendsSheetViewModel viewModel;
        if (userDataContract != null && (modelUser = userDataContract.getModelUser()) != null) {
            viewModel = this.this$0.getViewModel();
            viewModel.onChangeUserChecked(modelUser, false);
        }
    }
}
