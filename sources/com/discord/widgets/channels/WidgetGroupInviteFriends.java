package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.chips_view.ChipsView;
import com.discord.databinding.WidgetGroupInviteFriendsBinding;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.presence.Presence;
import com.discord.models.user.CoreUser;
import com.discord.models.user.User;
import com.discord.stores.StoreCalls;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreTabsNavigation;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.WidgetGroupInviteFriends;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.internal.LinkedTreeMap;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetGroupInviteFriends.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 12\u00020\u0001:\u0003123B\u0007¢\u0006\u0004\b0\u0010\u001bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J/\u0010\u000f\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\t2\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0014J\u0017\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001a\u0010\u001bRR\u0010\u001f\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0011 \u001e*\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u001d0\u001d \u001e*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0011 \u001e*\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u001d0\u001d\u0018\u00010\u001c0\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R:\u0010\"\u001a&\u0012\f\u0012\n \u001e*\u0004\u0018\u00010!0! \u001e*\u0012\u0012\f\u0012\n \u001e*\u0004\u0018\u00010!0!\u0018\u00010\u001c0\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010 R\"\u0010%\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\u00110#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u001d\u0010,\u001a\u00020'8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+R\u0018\u0010.\u001a\u0004\u0018\u00010-8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/¨\u00064"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;", "data", "", "configureUI", "(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V", "setupFAB", "setupToolbar", "", "maxGroupSize", "numRecipients", "", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;", "potentialAdditions", "getChildToDisplay", "(IILjava/util/List;)I", "Lcom/discord/models/user/User;", "user", "selectUser", "(Lcom/discord/models/user/User;)V", "unselectUser", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lrx/subjects/BehaviorSubject;", "", "kotlin.jvm.PlatformType", "addedUsersPublisher", "Lrx/subjects/BehaviorSubject;", "", "filterPublisher", "Lcom/google/gson/internal/LinkedTreeMap;", "", "addedUsers", "Lcom/google/gson/internal/LinkedTreeMap;", "Lcom/discord/databinding/WidgetGroupInviteFriendsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGroupInviteFriendsBinding;", "binding", "Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;", "adapter", "Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "UserDataContract", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGroupInviteFriends extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGroupInviteFriends.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGroupInviteFriendsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private static final int MAX_GROUP_MEMBERS = 10;
    private static final int MAX_GROUP_MEMBERS_STAFF = 25;
    private static final long NO_CHANNEL_ID = -1;
    private static final int VIEW_INDEX_FRIENDS_LIST = 0;
    private static final int VIEW_INDEX_GROUP_FULL = 2;
    private static final int VIEW_INDEX_NO_FRIENDS = 1;
    private WidgetGroupInviteFriendsAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGroupInviteFriends$binding$2.INSTANCE, null, 2, null);
    private final LinkedTreeMap<Long, User> addedUsers = new LinkedTreeMap<>();
    private final BehaviorSubject<String> filterPublisher = BehaviorSubject.l0("");
    private final BehaviorSubject<Collection<User>> addedUsersPublisher = BehaviorSubject.l0(new ArrayList());

    /* compiled from: WidgetGroupInviteFriends.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ)\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\fR\u0016\u0010\r\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011R\u001a\u0010\u0013\u001a\u00060\tj\u0002`\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0011R\u0016\u0010\u0016\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0011R\u0016\u0010\u0017\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0011¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Companion;", "", "Landroid/content/Context;", "context", "", "source", "", "launch", "(Landroid/content/Context;Ljava/lang/String;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "(Landroid/content/Context;JLjava/lang/String;)V", WidgetGroupInviteFriends.INTENT_EXTRA_CHANNEL_ID, "Ljava/lang/String;", "", "MAX_GROUP_MEMBERS", "I", "MAX_GROUP_MEMBERS_STAFF", "NO_CHANNEL_ID", "J", "VIEW_INDEX_FRIENDS_LIST", "VIEW_INDEX_GROUP_FULL", "VIEW_INDEX_NO_FRIENDS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, String str) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "source");
            AnalyticsTracker.openModal$default("DM Group Create", str, null, 4, null);
            j.e(context, WidgetGroupInviteFriends.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final void launch(Context context, long j, String str) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "source");
            AnalyticsTracker.openModal$default("DM Group Add", str, null, 4, null);
            Intent intent = new Intent();
            intent.putExtra(WidgetGroupInviteFriends.INTENT_EXTRA_CHANNEL_ID, j);
            j.d(context, WidgetGroupInviteFriends.class, intent);
        }
    }

    /* compiled from: WidgetGroupInviteFriends.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0015\b\u0086\b\u0018\u0000 12\u00020\u0001:\u0003123BM\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0010\u0012\u0006\u0010\u0019\u001a\u00020\u0010¢\u0006\u0004\b/\u00100J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0012J\\\u0010\u001a\u001a\u00020\u00002\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f2\b\b\u0002\u0010\u0018\u001a\u00020\u00102\b\b\u0002\u0010\u0019\u001a\u00020\u0010HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001c\u0010\u0007J\u0010\u0010\u001d\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u001d\u0010\u0012J\u001a\u0010 \u001a\u00020\u001f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\"\u001a\u0004\b#\u0010\u000bR\u0019\u0010\u0019\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010$\u001a\u0004\b%\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b'\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010$\u001a\u0004\b(\u0010\u0012R\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b*\u0010\u000fR\u0013\u0010,\u001a\u00020\u00108F@\u0006¢\u0006\u0006\u001a\u0004\b+\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010-\u001a\u0004\b.\u0010\u0007¨\u00064"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Ljava/lang/String;", "", "Lcom/discord/models/user/User;", "component3", "()Ljava/util/Collection;", "", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;", "component4", "()Ljava/util/List;", "", "component5", "()I", "component6", "channel", "filterText", "selectedUsers", "potentialAdditions", "mode", "maxGroupMemberCount", "copy", "(Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/Collection;Ljava/util/List;II)Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Collection;", "getSelectedUsers", "I", "getMaxGroupMemberCount", "Lcom/discord/api/channel/Channel;", "getChannel", "getMode", "Ljava/util/List;", "getPotentialAdditions", "getTotalNumRecipients", "totalNumRecipients", "Ljava/lang/String;", "getFilterText", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/Collection;Ljava/util/List;II)V", "Companion", "FriendItem", "ModelAppUserRelationship", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        public static final int MODE_ADD = 1;
        public static final int MODE_CREATE = 0;
        private final Channel channel;
        private final String filterText;
        private final int maxGroupMemberCount;
        private final int mode;
        private final List<FriendItem> potentialAdditions;
        private final Collection<User> selectedUsers;

        /* compiled from: WidgetGroupInviteFriends.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u001fB\t\b\u0002¢\u0006\u0004\b\u001e\u0010\u001dJ7\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00022\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u00022\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002H\u0002¢\u0006\u0004\b\t\u0010\nJ?\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u00022\u0006\u0010\f\u001a\u00020\u000b2\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u00022\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002H\u0002¢\u0006\u0004\b\r\u0010\u000eJ/\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00022\u000e\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006H\u0003¢\u0006\u0004\b\u0012\u0010\u0013J=\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u00022\u0006\u0010\f\u001a\u00020\u000b2\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u00022\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002¢\u0006\u0004\b\u0014\u0010\u000eJ/\u0010\u0016\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u00022\u0012\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u00020\u00188\u0006@\u0006X\u0086T¢\u0006\f\n\u0004\b\u001b\u0010\u001a\u0012\u0004\b\u001c\u0010\u001d¨\u0006 "}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion;", "", "Lrx/Observable;", "", "Lcom/discord/models/user/User;", "addedUsersPublisher", "", "filterPublisher", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;", "getForCreate", "(Lrx/Observable;Lrx/Observable;)Lrx/Observable;", "", "channelId", "getForAdd", "(JLrx/Observable;Lrx/Observable;)Lrx/Observable;", "oldExcludeUsers", "nameFilter", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$ModelAppUserRelationship;", "getFilteredFriends", "(Ljava/util/Collection;Ljava/lang/String;)Lrx/Observable;", "get", "addedUsers", "getFriendChanges", "(Lrx/Observable;)Lrx/Observable;", "", "MODE_ADD", "I", "MODE_CREATE", "getMODE_CREATE$annotations", "()V", HookHelper.constructorName, "AddedUsersInput", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {

            /* compiled from: WidgetGroupInviteFriends.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\b¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion$AddedUsersInput;", "", "", "Lcom/discord/models/user/User;", "component1", "()Ljava/util/Collection;", "", "component2", "()Ljava/lang/String;", "addedUsers", "filter", "copy", "(Ljava/util/Collection;Ljava/lang/String;)Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion$AddedUsersInput;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Collection;", "getAddedUsers", "Ljava/lang/String;", "getFilter", HookHelper.constructorName, "(Ljava/util/Collection;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class AddedUsersInput {
                private final Collection<User> addedUsers;
                private final String filter;

                /* JADX WARN: Multi-variable type inference failed */
                public AddedUsersInput(Collection<? extends User> collection, String str) {
                    m.checkNotNullParameter(collection, "addedUsers");
                    m.checkNotNullParameter(str, "filter");
                    this.addedUsers = collection;
                    this.filter = str;
                }

                /* JADX WARN: Multi-variable type inference failed */
                public static /* synthetic */ AddedUsersInput copy$default(AddedUsersInput addedUsersInput, Collection collection, String str, int i, Object obj) {
                    if ((i & 1) != 0) {
                        collection = addedUsersInput.addedUsers;
                    }
                    if ((i & 2) != 0) {
                        str = addedUsersInput.filter;
                    }
                    return addedUsersInput.copy(collection, str);
                }

                public final Collection<User> component1() {
                    return this.addedUsers;
                }

                public final String component2() {
                    return this.filter;
                }

                public final AddedUsersInput copy(Collection<? extends User> collection, String str) {
                    m.checkNotNullParameter(collection, "addedUsers");
                    m.checkNotNullParameter(str, "filter");
                    return new AddedUsersInput(collection, str);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof AddedUsersInput)) {
                        return false;
                    }
                    AddedUsersInput addedUsersInput = (AddedUsersInput) obj;
                    return m.areEqual(this.addedUsers, addedUsersInput.addedUsers) && m.areEqual(this.filter, addedUsersInput.filter);
                }

                public final Collection<User> getAddedUsers() {
                    return this.addedUsers;
                }

                public final String getFilter() {
                    return this.filter;
                }

                public int hashCode() {
                    Collection<User> collection = this.addedUsers;
                    int i = 0;
                    int hashCode = (collection != null ? collection.hashCode() : 0) * 31;
                    String str = this.filter;
                    if (str != null) {
                        i = str.hashCode();
                    }
                    return hashCode + i;
                }

                public String toString() {
                    StringBuilder R = a.R("AddedUsersInput(addedUsers=");
                    R.append(this.addedUsers);
                    R.append(", filter=");
                    return a.H(R, this.filter, ")");
                }
            }

            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            @SuppressLint({"DefaultLocale"})
            public final Observable<ModelAppUserRelationship> getFilteredFriends(Collection<? extends User> collection, String str) {
                if (collection == null) {
                    collection = n.emptyList();
                }
                Observable<ModelAppUserRelationship> Y = Observable.A(collection).F(WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$1.INSTANCE).f0().z(new WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2(str)).Y(WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$3.INSTANCE);
                m.checkNotNullExpressionValue(Y, "Observable\n            .…          }\n            }");
                return Y;
            }

            private final Observable<Model> getForAdd(long j, final Observable<Collection<User>> observable, final Observable<String> observable2) {
                Observable Y = StoreStream.Companion.getChannels().observeChannel(j).Y(new b<Channel, Observable<? extends Model>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getForAdd$1

                    /* compiled from: WidgetGroupInviteFriends.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u00002\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/models/user/User;", "p1", "", "p2", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion$AddedUsersInput;", "invoke", "(Ljava/util/Collection;Ljava/lang/String;)Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion$AddedUsersInput;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getForAdd$1$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final /* synthetic */ class AnonymousClass1 extends k implements Function2<Collection<? extends User>, String, WidgetGroupInviteFriends.Model.Companion.AddedUsersInput> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        public AnonymousClass1() {
                            super(2, WidgetGroupInviteFriends.Model.Companion.AddedUsersInput.class, HookHelper.constructorName, "<init>(Ljava/util/Collection;Ljava/lang/String;)V", 0);
                        }

                        public final WidgetGroupInviteFriends.Model.Companion.AddedUsersInput invoke(Collection<? extends User> collection, String str) {
                            m.checkNotNullParameter(collection, "p1");
                            m.checkNotNullParameter(str, "p2");
                            return new WidgetGroupInviteFriends.Model.Companion.AddedUsersInput(collection, str);
                        }
                    }

                    public final Observable<? extends WidgetGroupInviteFriends.Model> call(final Channel channel) {
                        if (channel == null) {
                            return new j0.l.e.k(null);
                        }
                        Observable observable3 = Observable.this;
                        Observable observable4 = observable2;
                        AnonymousClass1 r2 = AnonymousClass1.INSTANCE;
                        Object obj = r2;
                        if (r2 != null) {
                            obj = new WidgetGroupInviteFriends$sam$rx_functions_Func2$0(r2);
                        }
                        return Observable.j(observable3, observable4, (Func2) obj).Y(new b<WidgetGroupInviteFriends.Model.Companion.AddedUsersInput, Observable<? extends WidgetGroupInviteFriends.Model>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getForAdd$1.2
                            public final Observable<? extends WidgetGroupInviteFriends.Model> call(final WidgetGroupInviteFriends.Model.Companion.AddedUsersInput addedUsersInput) {
                                ArrayList arrayList;
                                Observable filteredFriends;
                                WidgetGroupInviteFriends.Model.Companion companion = WidgetGroupInviteFriends.Model.Companion;
                                List<com.discord.api.user.User> w = Channel.this.w();
                                if (w != null) {
                                    arrayList = new ArrayList(o.collectionSizeOrDefault(w, 10));
                                    for (com.discord.api.user.User user : w) {
                                        arrayList.add(new CoreUser(user));
                                    }
                                } else {
                                    arrayList = null;
                                }
                                filteredFriends = companion.getFilteredFriends(arrayList, addedUsersInput.getFilter());
                                return (Observable<R>) filteredFriends.F(new b<WidgetGroupInviteFriends.Model.ModelAppUserRelationship, List<? extends WidgetGroupInviteFriends.Model.FriendItem>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends.Model.Companion.getForAdd.1.2.2
                                    public final List<WidgetGroupInviteFriends.Model.FriendItem> call(WidgetGroupInviteFriends.Model.ModelAppUserRelationship modelAppUserRelationship) {
                                        WidgetGroupInviteFriends.Model.FriendItem.Companion companion2 = WidgetGroupInviteFriends.Model.FriendItem.Companion;
                                        m.checkNotNullExpressionValue(modelAppUserRelationship, "friends");
                                        return companion2.createData(modelAppUserRelationship, WidgetGroupInviteFriends.Model.Companion.AddedUsersInput.this.getAddedUsers());
                                    }
                                }).Y(new b<List<? extends WidgetGroupInviteFriends.Model.FriendItem>, Observable<? extends WidgetGroupInviteFriends.Model>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends.Model.Companion.getForAdd.1.2.3
                                    @Override // j0.k.b
                                    public /* bridge */ /* synthetic */ Observable<? extends WidgetGroupInviteFriends.Model> call(List<? extends WidgetGroupInviteFriends.Model.FriendItem> list) {
                                        return call2((List<WidgetGroupInviteFriends.Model.FriendItem>) list);
                                    }

                                    /* renamed from: call  reason: avoid collision after fix types in other method */
                                    public final Observable<? extends WidgetGroupInviteFriends.Model> call2(List<WidgetGroupInviteFriends.Model.FriendItem> list) {
                                        Channel channel2 = Channel.this;
                                        String filter = addedUsersInput.getFilter();
                                        Collection<User> addedUsers = addedUsersInput.getAddedUsers();
                                        m.checkNotNullExpressionValue(list, "friendItems");
                                        return new j0.l.e.k(new WidgetGroupInviteFriends.Model(channel2, filter, addedUsers, list, 1, UserUtils.INSTANCE.isStaff(StoreStream.Companion.getUsers().getMe()) ? 25 : 10));
                                    }
                                });
                            }
                        });
                    }
                });
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …          }\n            }");
                return Y;
            }

            private final Observable<Model> getForCreate(Observable<Collection<User>> observable, Observable<String> observable2) {
                WidgetGroupInviteFriends$Model$Companion$getForCreate$1 widgetGroupInviteFriends$Model$Companion$getForCreate$1 = WidgetGroupInviteFriends$Model$Companion$getForCreate$1.INSTANCE;
                Object obj = widgetGroupInviteFriends$Model$Companion$getForCreate$1;
                if (widgetGroupInviteFriends$Model$Companion$getForCreate$1 != null) {
                    obj = new WidgetGroupInviteFriends$sam$rx_functions_Func2$0(widgetGroupInviteFriends$Model$Companion$getForCreate$1);
                }
                Observable<Model> Y = Observable.j(observable, observable2, (Func2) obj).Y(WidgetGroupInviteFriends$Model$Companion$getForCreate$2.INSTANCE);
                m.checkNotNullExpressionValue(Y, "Observable\n            .…          }\n            }");
                return Y;
            }

            public static /* synthetic */ void getMODE_CREATE$annotations() {
            }

            public final Observable<Model> get(long j, Observable<Collection<User>> observable, Observable<String> observable2) {
                m.checkNotNullParameter(observable, "addedUsersPublisher");
                m.checkNotNullParameter(observable2, "filterPublisher");
                if (j == -1) {
                    return getForCreate(observable, observable2);
                }
                return getForAdd(j, observable, observable2);
            }

            public final Observable<? extends Collection<User>> getFriendChanges(final Observable<Collection<User>> observable) {
                m.checkNotNullParameter(observable, "addedUsers");
                Observable<? extends Collection<User>> q = StoreStream.Companion.getUserRelationships().observeForType(1).Y(new b<Map<Long, ? extends Integer>, Observable<? extends List<User>>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFriendChanges$1
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Observable<? extends List<User>> call(Map<Long, ? extends Integer> map) {
                        return call2((Map<Long, Integer>) map);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Observable<? extends List<User>> call2(final Map<Long, Integer> map) {
                        return Observable.this.Y(new b<Collection<? extends User>, Observable<? extends List<User>>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFriendChanges$1.1
                            public final Observable<? extends List<User>> call(Collection<? extends User> collection) {
                                return Observable.A(collection).x(new b<User, Boolean>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends.Model.Companion.getFriendChanges.1.1.1
                                    public final Boolean call(User user) {
                                        return Boolean.valueOf(map.containsKey(Long.valueOf(user.getId())));
                                    }
                                }).f0();
                            }
                        });
                    }
                }).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetGroupInviteFriends.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001Bg\u0012\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0002\u0012\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u0002\u0012\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\f0\u0002¢\u0006\u0004\b\"\u0010#J \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J \u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J \u0010\r\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\f0\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0007Jx\u0010\u0012\u001a\u00020\u00002\u0018\b\u0002\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0018\b\u0002\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u00022\u0018\b\u0002\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u00022\u0018\b\u0002\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\f0\u0002HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR)\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R)\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001f\u0010\u0007R)\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\f0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b \u0010\u0007R)\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b!\u0010\u0007¨\u0006$"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$ModelAppUserRelationship;", "", "", "", "Lcom/discord/primitives/UserId;", "", "component1", "()Ljava/util/Map;", "Lcom/discord/models/presence/Presence;", "component2", "Lcom/discord/models/user/User;", "component3", "Lcom/discord/models/domain/ModelApplicationStream;", "component4", "relationships", "presences", "users", "streams", "copy", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$ModelAppUserRelationship;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getRelationships", "getUsers", "getStreams", "getPresences", HookHelper.constructorName, "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ModelAppUserRelationship {
            private final Map<Long, Presence> presences;
            private final Map<Long, Integer> relationships;
            private final Map<Long, ModelApplicationStream> streams;
            private final Map<Long, User> users;

            /* JADX WARN: Multi-variable type inference failed */
            public ModelAppUserRelationship(Map<Long, Integer> map, Map<Long, Presence> map2, Map<Long, ? extends User> map3, Map<Long, ? extends ModelApplicationStream> map4) {
                m.checkNotNullParameter(map, "relationships");
                m.checkNotNullParameter(map2, "presences");
                m.checkNotNullParameter(map3, "users");
                m.checkNotNullParameter(map4, "streams");
                this.relationships = map;
                this.presences = map2;
                this.users = map3;
                this.streams = map4;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ModelAppUserRelationship copy$default(ModelAppUserRelationship modelAppUserRelationship, Map map, Map map2, Map map3, Map map4, int i, Object obj) {
                if ((i & 1) != 0) {
                    map = modelAppUserRelationship.relationships;
                }
                if ((i & 2) != 0) {
                    map2 = modelAppUserRelationship.presences;
                }
                if ((i & 4) != 0) {
                    map3 = modelAppUserRelationship.users;
                }
                if ((i & 8) != 0) {
                    map4 = modelAppUserRelationship.streams;
                }
                return modelAppUserRelationship.copy(map, map2, map3, map4);
            }

            public final Map<Long, Integer> component1() {
                return this.relationships;
            }

            public final Map<Long, Presence> component2() {
                return this.presences;
            }

            public final Map<Long, User> component3() {
                return this.users;
            }

            public final Map<Long, ModelApplicationStream> component4() {
                return this.streams;
            }

            public final ModelAppUserRelationship copy(Map<Long, Integer> map, Map<Long, Presence> map2, Map<Long, ? extends User> map3, Map<Long, ? extends ModelApplicationStream> map4) {
                m.checkNotNullParameter(map, "relationships");
                m.checkNotNullParameter(map2, "presences");
                m.checkNotNullParameter(map3, "users");
                m.checkNotNullParameter(map4, "streams");
                return new ModelAppUserRelationship(map, map2, map3, map4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ModelAppUserRelationship)) {
                    return false;
                }
                ModelAppUserRelationship modelAppUserRelationship = (ModelAppUserRelationship) obj;
                return m.areEqual(this.relationships, modelAppUserRelationship.relationships) && m.areEqual(this.presences, modelAppUserRelationship.presences) && m.areEqual(this.users, modelAppUserRelationship.users) && m.areEqual(this.streams, modelAppUserRelationship.streams);
            }

            public final Map<Long, Presence> getPresences() {
                return this.presences;
            }

            public final Map<Long, Integer> getRelationships() {
                return this.relationships;
            }

            public final Map<Long, ModelApplicationStream> getStreams() {
                return this.streams;
            }

            public final Map<Long, User> getUsers() {
                return this.users;
            }

            public int hashCode() {
                Map<Long, Integer> map = this.relationships;
                int i = 0;
                int hashCode = (map != null ? map.hashCode() : 0) * 31;
                Map<Long, Presence> map2 = this.presences;
                int hashCode2 = (hashCode + (map2 != null ? map2.hashCode() : 0)) * 31;
                Map<Long, User> map3 = this.users;
                int hashCode3 = (hashCode2 + (map3 != null ? map3.hashCode() : 0)) * 31;
                Map<Long, ModelApplicationStream> map4 = this.streams;
                if (map4 != null) {
                    i = map4.hashCode();
                }
                return hashCode3 + i;
            }

            public String toString() {
                StringBuilder R = a.R("ModelAppUserRelationship(relationships=");
                R.append(this.relationships);
                R.append(", presences=");
                R.append(this.presences);
                R.append(", users=");
                R.append(this.users);
                R.append(", streams=");
                return a.L(R, this.streams, ")");
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(Channel channel, String str, Collection<? extends User> collection, List<FriendItem> list, int i, int i2) {
            m.checkNotNullParameter(collection, "selectedUsers");
            m.checkNotNullParameter(list, "potentialAdditions");
            this.channel = channel;
            this.filterText = str;
            this.selectedUsers = collection;
            this.potentialAdditions = list;
            this.mode = i;
            this.maxGroupMemberCount = i2;
        }

        public static /* synthetic */ Model copy$default(Model model, Channel channel, String str, Collection collection, List list, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                channel = model.channel;
            }
            if ((i3 & 2) != 0) {
                str = model.filterText;
            }
            String str2 = str;
            Collection<User> collection2 = collection;
            if ((i3 & 4) != 0) {
                collection2 = model.selectedUsers;
            }
            Collection collection3 = collection2;
            List<FriendItem> list2 = list;
            if ((i3 & 8) != 0) {
                list2 = model.potentialAdditions;
            }
            List list3 = list2;
            if ((i3 & 16) != 0) {
                i = model.mode;
            }
            int i4 = i;
            if ((i3 & 32) != 0) {
                i2 = model.maxGroupMemberCount;
            }
            return model.copy(channel, str2, collection3, list3, i4, i2);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final String component2() {
            return this.filterText;
        }

        public final Collection<User> component3() {
            return this.selectedUsers;
        }

        public final List<FriendItem> component4() {
            return this.potentialAdditions;
        }

        public final int component5() {
            return this.mode;
        }

        public final int component6() {
            return this.maxGroupMemberCount;
        }

        public final Model copy(Channel channel, String str, Collection<? extends User> collection, List<FriendItem> list, int i, int i2) {
            m.checkNotNullParameter(collection, "selectedUsers");
            m.checkNotNullParameter(list, "potentialAdditions");
            return new Model(channel, str, collection, list, i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.channel, model.channel) && m.areEqual(this.filterText, model.filterText) && m.areEqual(this.selectedUsers, model.selectedUsers) && m.areEqual(this.potentialAdditions, model.potentialAdditions) && this.mode == model.mode && this.maxGroupMemberCount == model.maxGroupMemberCount;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final String getFilterText() {
            return this.filterText;
        }

        public final int getMaxGroupMemberCount() {
            return this.maxGroupMemberCount;
        }

        public final int getMode() {
            return this.mode;
        }

        public final List<FriendItem> getPotentialAdditions() {
            return this.potentialAdditions;
        }

        public final Collection<User> getSelectedUsers() {
            return this.selectedUsers;
        }

        public final int getTotalNumRecipients() {
            int size = this.selectedUsers.size() + 1;
            Channel channel = this.channel;
            if (channel == null) {
                return size;
            }
            List<com.discord.api.user.User> w = channel.w();
            return size + (w != null ? w.size() : 0);
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            String str = this.filterText;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            Collection<User> collection = this.selectedUsers;
            int hashCode3 = (hashCode2 + (collection != null ? collection.hashCode() : 0)) * 31;
            List<FriendItem> list = this.potentialAdditions;
            if (list != null) {
                i = list.hashCode();
            }
            return ((((hashCode3 + i) * 31) + this.mode) * 31) + this.maxGroupMemberCount;
        }

        public String toString() {
            StringBuilder R = a.R("Model(channel=");
            R.append(this.channel);
            R.append(", filterText=");
            R.append(this.filterText);
            R.append(", selectedUsers=");
            R.append(this.selectedUsers);
            R.append(", potentialAdditions=");
            R.append(this.potentialAdditions);
            R.append(", mode=");
            R.append(this.mode);
            R.append(", maxGroupMemberCount=");
            return a.A(R, this.maxGroupMemberCount, ")");
        }

        /* compiled from: WidgetGroupInviteFriends.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u0000 )2\u00020\u0001:\u0001)B3\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u000e\u001a\u00020\b\u0012\b\b\u0002\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b'\u0010(J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ<\u0010\u0010\u001a\u00020\u00002\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u000f\u0010\nR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001f\u001a\u0004\b \u0010\u0004R\u0019\u0010\u000e\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u000e\u0010\nR\u001c\u0010!\u001a\u00020\u00158\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\u0017R\u001c\u0010$\u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010\u0014¨\u0006*"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/presence/Presence;", "component2", "()Lcom/discord/models/presence/Presence;", "", "component3", "()Z", "component4", "user", "presence", "isSelected", "isApplicationStreaming", "copy", "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;ZZ)Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/models/presence/Presence;", "getPresence", "Lcom/discord/models/user/User;", "getUser", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;ZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FriendItem implements MGRecyclerDataPayload {
            public static final Companion Companion = new Companion(null);
            public static final int TYPE_FRIEND = 0;
            private final boolean isApplicationStreaming;
            private final boolean isSelected;
            private final String key;
            private final Presence presence;
            private final int type;
            private final User user;

            /* compiled from: WidgetGroupInviteFriends.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ)\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem$Companion;", "", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$ModelAppUserRelationship;", "friends", "", "Lcom/discord/models/user/User;", "alreadyAddedUsers", "", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;", "createData", "(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$ModelAppUserRelationship;Ljava/util/Collection;)Ljava/util/List;", "", "TYPE_FRIEND", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Companion {
                private Companion() {
                }

                public final List<FriendItem> createData(ModelAppUserRelationship modelAppUserRelationship, Collection<? extends User> collection) {
                    m.checkNotNullParameter(modelAppUserRelationship, "friends");
                    m.checkNotNullParameter(collection, "alreadyAddedUsers");
                    ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(collection, 10));
                    for (User user : collection) {
                        arrayList.add(Long.valueOf(user.getId()));
                    }
                    Set set = u.toSet(arrayList);
                    Set<Long> keySet = modelAppUserRelationship.getUsers().keySet();
                    ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(keySet, 10));
                    for (Number number : keySet) {
                        long longValue = number.longValue();
                        arrayList2.add(new FriendItem(modelAppUserRelationship.getUsers().get(Long.valueOf(longValue)), modelAppUserRelationship.getPresences().get(Long.valueOf(longValue)), set.contains(Long.valueOf(longValue)), modelAppUserRelationship.getStreams().containsKey(Long.valueOf(longValue))));
                    }
                    return arrayList2;
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            public FriendItem() {
                this(null, null, false, false, 15, null);
            }

            public FriendItem(User user, Presence presence, boolean z2, boolean z3) {
                this.user = user;
                this.presence = presence;
                this.isSelected = z2;
                this.isApplicationStreaming = z3;
                this.key = String.valueOf(user != null ? Long.valueOf(user.getId()) : null);
            }

            public static /* synthetic */ FriendItem copy$default(FriendItem friendItem, User user, Presence presence, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    user = friendItem.user;
                }
                if ((i & 2) != 0) {
                    presence = friendItem.presence;
                }
                if ((i & 4) != 0) {
                    z2 = friendItem.isSelected;
                }
                if ((i & 8) != 0) {
                    z3 = friendItem.isApplicationStreaming;
                }
                return friendItem.copy(user, presence, z2, z3);
            }

            public final User component1() {
                return this.user;
            }

            public final Presence component2() {
                return this.presence;
            }

            public final boolean component3() {
                return this.isSelected;
            }

            public final boolean component4() {
                return this.isApplicationStreaming;
            }

            public final FriendItem copy(User user, Presence presence, boolean z2, boolean z3) {
                return new FriendItem(user, presence, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof FriendItem)) {
                    return false;
                }
                FriendItem friendItem = (FriendItem) obj;
                return m.areEqual(this.user, friendItem.user) && m.areEqual(this.presence, friendItem.presence) && this.isSelected == friendItem.isSelected && this.isApplicationStreaming == friendItem.isApplicationStreaming;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final Presence getPresence() {
                return this.presence;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public final User getUser() {
                return this.user;
            }

            public int hashCode() {
                User user = this.user;
                int i = 0;
                int hashCode = (user != null ? user.hashCode() : 0) * 31;
                Presence presence = this.presence;
                if (presence != null) {
                    i = presence.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.isSelected;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.isApplicationStreaming;
                if (!z3) {
                    i3 = z3 ? 1 : 0;
                }
                return i6 + i3;
            }

            public final boolean isApplicationStreaming() {
                return this.isApplicationStreaming;
            }

            public final boolean isSelected() {
                return this.isSelected;
            }

            public String toString() {
                StringBuilder R = a.R("FriendItem(user=");
                R.append(this.user);
                R.append(", presence=");
                R.append(this.presence);
                R.append(", isSelected=");
                R.append(this.isSelected);
                R.append(", isApplicationStreaming=");
                return a.M(R, this.isApplicationStreaming, ")");
            }

            public /* synthetic */ FriendItem(User user, Presence presence, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? null : user, (i & 2) != 0 ? null : presence, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? false : z3);
            }
        }

        public /* synthetic */ Model(Channel channel, String str, Collection collection, List list, int i, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
            this((i3 & 1) != 0 ? null : channel, (i3 & 2) != 0 ? null : str, collection, list, (i3 & 16) != 0 ? 0 : i, i2);
        }
    }

    /* compiled from: WidgetGroupInviteFriends.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001e\u0010\b\u001a\u0004\u0018\u00010\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$UserDataContract;", "Lcom/discord/chips_view/ChipsView$a;", "Lcom/discord/models/user/User;", "modelUser", "Lcom/discord/models/user/User;", "getModelUser", "()Lcom/discord/models/user/User;", "", "displayString", "Ljava/lang/String;", "getDisplayString", "()Ljava/lang/String;", HookHelper.constructorName, "(Lcom/discord/models/user/User;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserDataContract implements ChipsView.a {
        private final String displayString;
        private final User modelUser;

        public UserDataContract(User user) {
            this.modelUser = user;
            this.displayString = user != null ? user.getUsername() : null;
        }

        @Override // com.discord.chips_view.ChipsView.a
        public String getDisplayString() {
            return this.displayString;
        }

        public final User getModelUser() {
            return this.modelUser;
        }
    }

    public WidgetGroupInviteFriends() {
        super(R.layout.widget_group_invite_friends);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        int i;
        List<com.discord.api.user.User> w;
        CharSequence e;
        if (model == null || (model.getMode() == 1 && model.getChannel() == null)) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        HashSet hashSet = new HashSet();
        Iterator<User> it = model.getSelectedUsers().iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            User next = it.next();
            ChipsView chipsView = getBinding().f2384b;
            String username = next.getUsername();
            e = b.a.k.b.e(this, R.string.remove_role_or_user, new Object[]{next.getUsername()}, (r4 & 4) != 0 ? b.a.j : null);
            chipsView.b(username, e.toString(), Long.valueOf(next.getId()), new UserDataContract(next));
            hashSet.add(Long.valueOf(next.getId()));
        }
        String filterText = model.getFilterText();
        if (filterText != null) {
            if (filterText.length() > 0) {
                if (getBinding().f2384b.getText().length() == 0) {
                    getBinding().f2384b.setText(model.getFilterText());
                }
            }
        }
        getBinding().f2384b.d(hashSet);
        List<Model.FriendItem> potentialAdditions = model.getPotentialAdditions();
        Channel channel = model.getChannel();
        if (!(channel == null || (w = channel.w()) == null)) {
            i = w.size();
        }
        AppViewFlipper appViewFlipper = getBinding().f;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.groupInviteFriendsViewFlipper");
        appViewFlipper.setDisplayedChild(getChildToDisplay(model.getMaxGroupMemberCount(), 1 + i, potentialAdditions));
        WidgetGroupInviteFriendsAdapter widgetGroupInviteFriendsAdapter = this.adapter;
        if (widgetGroupInviteFriendsAdapter != null) {
            widgetGroupInviteFriendsAdapter.setData(potentialAdditions, new WidgetGroupInviteFriends$configureUI$1(this, model));
        }
        setupFAB(model);
        setupToolbar(model);
    }

    private final WidgetGroupInviteFriendsBinding getBinding() {
        return (WidgetGroupInviteFriendsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final int getChildToDisplay(int i, int i2, List<Model.FriendItem> list) {
        if (i2 >= i) {
            return 2;
        }
        return list == null || list.isEmpty() ? 1 : 0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void selectUser(User user) {
        CharSequence e;
        ChipsView chipsView = getBinding().f2384b;
        String username = user.getUsername();
        e = b.a.k.b.e(this, R.string.remove_role_or_user, new Object[]{user.getUsername()}, (r4 & 4) != 0 ? b.a.j : null);
        chipsView.b(username, e.toString(), Long.valueOf(user.getId()), new UserDataContract(user));
        this.addedUsers.put(Long.valueOf(user.getId()), user);
        this.addedUsersPublisher.onNext(new ArrayList(this.addedUsers.values()));
    }

    @SuppressLint({"RestrictedApi"})
    private final void setupFAB(final Model model) {
        if (!model.getSelectedUsers().isEmpty()) {
            FloatingActionButton floatingActionButton = getBinding().e;
            m.checkNotNullExpressionValue(floatingActionButton, "binding.groupInviteFriendsSaveFab");
            floatingActionButton.setVisibility(0);
            if (model.getTotalNumRecipients() > model.getMaxGroupMemberCount()) {
                getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$setupFAB$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        b.a.d.m.g(WidgetGroupInviteFriends.this.getContext(), R.string.group_dm_invite_full_sub, 0, null, 12);
                    }
                });
            } else {
                getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$setupFAB$2

                    /* compiled from: WidgetGroupInviteFriends.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001aV\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\u0016\u0012\u0014 \u0001*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004 \u0001**\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\u0016\u0012\u0014 \u0001*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004\u0018\u00010\u00060\u00062\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u0018\u0010\u0005\u001a\u0014 \u0001*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channelId", "", "Lcom/discord/primitives/ChannelId;", "selectedVoiceChannelId", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.channels.WidgetGroupInviteFriends$setupFAB$2$2  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass2<T1, T2, R> implements Func2<Channel, Long, Pair<? extends Channel, ? extends Long>> {
                        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                        public final Pair<Channel, Long> call(Channel channel, Long l) {
                            return d0.o.to(channel, l);
                        }
                    }

                    /* compiled from: WidgetGroupInviteFriends.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062Z\u0010\u0005\u001aV\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\u0016\u0012\u0014 \u0002*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004 \u0002**\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\u0016\u0012\u0014 \u0002*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lkotlin/Pair;", "Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "", "Lcom/discord/primitives/ChannelId;", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.channels.WidgetGroupInviteFriends$setupFAB$2$3  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass3 extends d0.z.d.o implements Function1<Pair<? extends Channel, ? extends Long>, Unit> {

                        /* compiled from: WidgetGroupInviteFriends.kt */
                        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                        /* renamed from: com.discord.widgets.channels.WidgetGroupInviteFriends$setupFAB$2$3$2  reason: invalid class name */
                        /* loaded from: classes2.dex */
                        public static final class AnonymousClass2 extends d0.z.d.o implements Function0<Unit> {
                            public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                            public AnonymousClass2() {
                                super(0);
                            }

                            @Override // kotlin.jvm.functions.Function0
                            /* renamed from: invoke  reason: avoid collision after fix types in other method */
                            public final void invoke2() {
                                StoreStream.Companion.getMediaEngine().selectVideoInputDevice(null);
                            }
                        }

                        public AnonymousClass3() {
                            super(1);
                        }

                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends Channel, ? extends Long> pair) {
                            invoke2((Pair<Channel, Long>) pair);
                            return Unit.a;
                        }

                        /* renamed from: invoke  reason: avoid collision after fix types in other method */
                        public final void invoke2(Pair<Channel, Long> pair) {
                            Channel component1 = pair.component1();
                            Long component2 = pair.component2();
                            ChannelSelector.Companion.getInstance().selectChannel(0L, component1.h(), (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null);
                            StoreStream.Companion companion = StoreStream.Companion;
                            Long l = null;
                            StoreTabsNavigation.selectHomeTab$default(companion.getTabsNavigation(), StoreNavigation.PanelAction.CLOSE, false, 2, null);
                            Channel channel = model.getChannel();
                            if (channel != null) {
                                l = Long.valueOf(channel.h());
                            }
                            if (m.areEqual(component2, l)) {
                                Channel channel2 = model.getChannel();
                                if (channel2 == null || !ChannelUtils.w(channel2)) {
                                    companion.getVoiceChannelSelected().clear();
                                    StoreCalls calls = companion.getCalls();
                                    WidgetGroupInviteFriends widgetGroupInviteFriends = WidgetGroupInviteFriends.this;
                                    Context requireContext = widgetGroupInviteFriends.requireContext();
                                    FragmentManager parentFragmentManager = WidgetGroupInviteFriends.this.getParentFragmentManager();
                                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                                    calls.call(widgetGroupInviteFriends, requireContext, parentFragmentManager, component1.h(), AnonymousClass2.INSTANCE);
                                } else {
                                    StoreCalls calls2 = companion.getCalls();
                                    long h = component1.h();
                                    Collection<User> selectedUsers = model.getSelectedUsers();
                                    ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(selectedUsers, 10));
                                    for (User user : selectedUsers) {
                                        arrayList.add(Long.valueOf(user.getId()));
                                    }
                                    calls2.ring(h, arrayList);
                                }
                            }
                            AppActivity appActivity = WidgetGroupInviteFriends.this.getAppActivity();
                            if (appActivity != null) {
                                appActivity.onBackPressed();
                            }
                        }
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        Observable<Channel> observable;
                        if (model.getChannel() != null) {
                            observable = RestAPI.Companion.getApi().addGroupRecipients(model.getChannel().h(), u.toList(model.getSelectedUsers()));
                        } else if (model.getSelectedUsers().size() == 1) {
                            observable = RestAPI.Companion.getApi().createOrFetchDM(((User) u.first(model.getSelectedUsers())).getId());
                        } else {
                            RestAPI api = RestAPI.Companion.getApi();
                            Collection<User> selectedUsers = model.getSelectedUsers();
                            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(selectedUsers, 10));
                            for (User user : selectedUsers) {
                                arrayList.add(Long.valueOf(user.getId()));
                            }
                            observable = api.createGroupDM(arrayList);
                        }
                        Observable j = Observable.j(observable, ObservableExtensionsKt.takeSingleUntilTimeout$default(StoreStream.Companion.getVoiceChannelSelected().observeSelectedVoiceChannelId(), 0L, false, 3, null), AnonymousClass2.INSTANCE);
                        m.checkNotNullExpressionValue(j, "Observable.combineLatest…dVoiceChannelId\n        }");
                        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(j, WidgetGroupInviteFriends.this, null, 2, null), WidgetGroupInviteFriends.this.getClass(), (r18 & 2) != 0 ? null : WidgetGroupInviteFriends.this.requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3());
                    }
                });
            }
        } else {
            FloatingActionButton floatingActionButton2 = getBinding().e;
            m.checkNotNullExpressionValue(floatingActionButton2, "binding.groupInviteFriendsSaveFab");
            floatingActionButton2.setVisibility(8);
        }
    }

    private final void setupToolbar(Model model) {
        CharSequence charSequence;
        Resources resources;
        CharSequence e;
        if (model.getChannel() != null) {
            e = b.a.k.b.e(this, R.string.group_dm_invite_with_name, new Object[]{ChannelUtils.e(model.getChannel(), requireContext(), false, 2)}, (r4 & 4) != 0 ? b.a.j : null);
            setActionBarTitle(e);
        } else {
            setActionBarTitle(getString(R.string.invite_friend_modal_title));
        }
        if (model.getChannel() != null) {
            List<com.discord.api.user.User> w = model.getChannel().w();
            if ((w != null ? w.size() : 1) >= model.getMaxGroupMemberCount()) {
                charSequence = getString(R.string.group_dm_invite_full_main);
                m.checkNotNullExpressionValue(charSequence, "getString(R.string.group_dm_invite_full_main)");
                RelativeLayout relativeLayout = getBinding().c;
                m.checkNotNullExpressionValue(relativeLayout, "binding.groupInviteFriendsRecipientsContainer");
                relativeLayout.setVisibility(8);
                setActionBarSubtitle(charSequence);
            }
        }
        int maxGroupMemberCount = model.getMaxGroupMemberCount() - model.getTotalNumRecipients();
        if (maxGroupMemberCount > 0) {
            Resources resources2 = getResources();
            m.checkNotNullExpressionValue(resources2, "resources");
            charSequence = StringResourceUtilsKt.getQuantityString(resources2, requireContext(), (int) R.plurals.group_dm_invite_remaining_number, maxGroupMemberCount, Integer.valueOf(maxGroupMemberCount));
        } else if (maxGroupMemberCount == 0) {
            charSequence = getString(R.string.group_dm_invite_will_fill_mobile);
            m.checkNotNullExpressionValue(charSequence, "getString(R.string.group…_invite_will_fill_mobile)");
        } else {
            int i = maxGroupMemberCount * (-1);
            resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            charSequence = StringResourceUtilsKt.getQuantityString(resources, requireContext(), (int) R.plurals.group_dm_invite_unselect_users_number, i, Integer.valueOf(i));
        }
        RelativeLayout relativeLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(relativeLayout2, "binding.groupInviteFriendsRecipientsContainer");
        relativeLayout2.setVisibility(0);
        setActionBarSubtitle(charSequence);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void unselectUser(User user) {
        this.addedUsers.remove(Long.valueOf(user.getId()));
        this.addedUsersPublisher.onNext(new ArrayList(this.addedUsers.values()));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().d;
        m.checkNotNullExpressionValue(recyclerView, "binding.groupInviteFriendsRecycler");
        this.adapter = (WidgetGroupInviteFriendsAdapter) companion.configure(new WidgetGroupInviteFriendsAdapter(recyclerView));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        ChipsView chipsView = getBinding().f2384b;
        Objects.requireNonNull(chipsView, "null cannot be cast to non-null type com.discord.chips_view.ChipsView<kotlin.Long, com.discord.widgets.channels.WidgetGroupInviteFriends.UserDataContract>");
        chipsView.setChipDeletedListener(new WidgetGroupInviteFriends$onViewBoundOrOnResume$1(this));
        getBinding().f2384b.setTextChangedListener(new WidgetGroupInviteFriends$onViewBoundOrOnResume$2(this));
        Model.Companion companion = Model.Companion;
        BehaviorSubject<Collection<User>> behaviorSubject = this.addedUsersPublisher;
        m.checkNotNullExpressionValue(behaviorSubject, "addedUsersPublisher");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(companion.getFriendChanges(behaviorSubject), this, this.adapter), WidgetGroupInviteFriends.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGroupInviteFriends$onViewBoundOrOnResume$3(this.addedUsersPublisher));
        long longExtra = getMostRecentIntent().getLongExtra(INTENT_EXTRA_CHANNEL_ID, -1L);
        BehaviorSubject<Collection<User>> behaviorSubject2 = this.addedUsersPublisher;
        m.checkNotNullExpressionValue(behaviorSubject2, "addedUsersPublisher");
        BehaviorSubject<String> behaviorSubject3 = this.filterPublisher;
        m.checkNotNullExpressionValue(behaviorSubject3, "filterPublisher");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(companion.get(longExtra, behaviorSubject2, behaviorSubject3), this, this.adapter), WidgetGroupInviteFriends.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGroupInviteFriends$onViewBoundOrOnResume$4(this));
    }
}
