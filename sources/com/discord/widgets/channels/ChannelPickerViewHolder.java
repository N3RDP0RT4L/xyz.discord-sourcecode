package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.channel.ChannelUtils;
import com.discord.databinding.ViewGuildRoleSubscriptionChannelItemBinding;
import com.discord.utilities.channel.GuildChannelIconUtilsKt;
import com.discord.widgets.channels.ChannelPickerAdapterItem;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetChannelPickerAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0006\u0007B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005\u0082\u0001\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/channels/ChannelPickerViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;)V", "ChannelItemViewHolder", "CreateChannelViewHolder", "Lcom/discord/widgets/channels/ChannelPickerViewHolder$CreateChannelViewHolder;", "Lcom/discord/widgets/channels/ChannelPickerViewHolder$ChannelItemViewHolder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class ChannelPickerViewHolder extends RecyclerView.ViewHolder {

    /* compiled from: WidgetChannelPickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\r¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/ChannelPickerViewHolder$ChannelItemViewHolder;", "Lcom/discord/widgets/channels/ChannelPickerViewHolder;", "Lcom/discord/widgets/channels/ChannelPickerAdapterItem;", "adapterItem", "", "configure", "(Lcom/discord/widgets/channels/ChannelPickerAdapterItem;)V", "Lcom/discord/widgets/channels/ChannelPickerAdapterItem$ChannelItem;", "item", "Lcom/discord/widgets/channels/ChannelPickerAdapterItem$ChannelItem;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionChannelItemBinding;", "binding", "Lcom/discord/databinding/ViewGuildRoleSubscriptionChannelItemBinding;", "Lkotlin/Function1;", "onItemClickListener", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionChannelItemBinding;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelItemViewHolder extends ChannelPickerViewHolder {
        private final ViewGuildRoleSubscriptionChannelItemBinding binding;
        private ChannelPickerAdapterItem.ChannelItem item;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public ChannelItemViewHolder(com.discord.databinding.ViewGuildRoleSubscriptionChannelItemBinding r3, final kotlin.jvm.functions.Function1<? super com.discord.widgets.channels.ChannelPickerAdapterItem.ChannelItem, kotlin.Unit> r4) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                java.lang.String r0 = "onItemClickListener"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                android.widget.LinearLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                android.view.View r3 = r2.itemView
                com.discord.widgets.channels.ChannelPickerViewHolder$ChannelItemViewHolder$1 r0 = new com.discord.widgets.channels.ChannelPickerViewHolder$ChannelItemViewHolder$1
                r0.<init>()
                r3.setOnClickListener(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.ChannelPickerViewHolder.ChannelItemViewHolder.<init>(com.discord.databinding.ViewGuildRoleSubscriptionChannelItemBinding, kotlin.jvm.functions.Function1):void");
        }

        public final void configure(ChannelPickerAdapterItem channelPickerAdapterItem) {
            m.checkNotNullParameter(channelPickerAdapterItem, "adapterItem");
            ChannelPickerAdapterItem.ChannelItem channelItem = (ChannelPickerAdapterItem.ChannelItem) channelPickerAdapterItem;
            this.item = channelItem;
            if (channelItem != null) {
                TextView textView = this.binding.f2175b;
                m.checkNotNullExpressionValue(textView, "binding.channelItemName");
                textView.setText(ChannelUtils.c(channelItem.getChannel()));
                this.binding.d.setImageResource(GuildChannelIconUtilsKt.guildChannelIcon(channelItem.getChannel()));
                ImageView imageView = this.binding.c;
                m.checkNotNullExpressionValue(imageView, "binding.channelItemSelected");
                imageView.setVisibility(channelItem.isSelected() ? 0 : 8);
            }
        }
    }

    /* compiled from: WidgetChannelPickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/ChannelPickerViewHolder$CreateChannelViewHolder;", "Lcom/discord/widgets/channels/ChannelPickerViewHolder;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionChannelItemBinding;", "binding", "Lkotlin/Function0;", "", "onItemClickListener", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionChannelItemBinding;Lkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    @SuppressLint({"SetTextI18n"})
    /* loaded from: classes2.dex */
    public static final class CreateChannelViewHolder extends ChannelPickerViewHolder {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public CreateChannelViewHolder(com.discord.databinding.ViewGuildRoleSubscriptionChannelItemBinding r5, final kotlin.jvm.functions.Function0<kotlin.Unit> r6) {
            /*
                r4 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                java.lang.String r0 = "onItemClickListener"
                d0.z.d.m.checkNotNullParameter(r6, r0)
                android.widget.LinearLayout r0 = r5.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r4.<init>(r0, r1)
                android.widget.TextView r0 = r5.f2175b
                r2 = 2131889851(0x7f120ebb, float:1.9414377E38)
                r0.setText(r2)
                android.view.View r0 = r4.itemView
                java.lang.String r2 = "itemView"
                d0.z.d.m.checkNotNullExpressionValue(r0, r2)
                r3 = 2130968995(0x7f0401a3, float:1.754666E38)
                int r0 = com.discord.utilities.color.ColorCompat.getThemedColor(r0, r3)
                android.widget.TextView r3 = r5.f2175b
                r3.setTextColor(r0)
                android.view.View r3 = r4.itemView
                d0.z.d.m.checkNotNullExpressionValue(r3, r2)
                android.content.Context r2 = r3.getContext()
                r3 = 2131231455(0x7f0802df, float:1.8078991E38)
                android.graphics.drawable.Drawable r2 = androidx.core.content.ContextCompat.getDrawable(r2, r3)
                if (r2 == 0) goto L4c
                java.lang.String r1 = "drawable"
                d0.z.d.m.checkNotNullExpressionValue(r2, r1)
                r1 = 0
                com.discord.utilities.color.ColorCompatKt.setTint(r2, r0, r1)
                r1 = r2
            L4c:
                android.widget.ImageView r0 = r5.d
                r0.setImageDrawable(r1)
                android.widget.ImageView r5 = r5.c
                java.lang.String r0 = "binding.channelItemSelected"
                d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                r0 = 8
                r5.setVisibility(r0)
                android.view.View r5 = r4.itemView
                com.discord.widgets.channels.ChannelPickerViewHolder$CreateChannelViewHolder$1 r0 = new com.discord.widgets.channels.ChannelPickerViewHolder$CreateChannelViewHolder$1
                r0.<init>()
                r5.setOnClickListener(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.ChannelPickerViewHolder.CreateChannelViewHolder.<init>(com.discord.databinding.ViewGuildRoleSubscriptionChannelItemBinding, kotlin.jvm.functions.Function0):void");
        }
    }

    private ChannelPickerViewHolder(View view) {
        super(view);
    }

    public /* synthetic */ ChannelPickerViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
