package com.discord.widgets.channels.threads.browser;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.databinding.ThreadBrowserItemHeaderBinding;
import com.discord.databinding.ThreadBrowserItemThreadBinding;
import com.discord.databinding.ThreadBrowserItemWarningBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserAdapter;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetThreadBrowserAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0006\u0018\u0019\u001a\u001b\u001c\u001dB7\u0012\u0006\u0010\u0015\u001a\u00020\u0014\u0012\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\f0\u0010\u0012\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\f0\u0010¢\u0006\u0004\b\u0016\u0010\u0017J)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u000f\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u000eR\"\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\f0\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\"\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\f0\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0012¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/api/channel/Channel;", "channel", "", "onThreadClicked", "(Lcom/discord/api/channel/Channel;)V", "onThreadLongClicked", "Lkotlin/Function1;", "onThreadSettings", "Lkotlin/jvm/functions/Function1;", "onOpenThread", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "HeaderItem", "Item", "ItemType", "LoadingItem", "ThreadItem", "WarningItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadBrowserAdapter extends MGRecyclerAdapterSimple<Item> {
    private final Function1<Channel, Unit> onOpenThread;
    private final Function1<Channel, Unit> onThreadSettings;

    /* compiled from: WidgetThreadBrowserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$HeaderItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;)V", "Lcom/discord/databinding/ThreadBrowserItemHeaderBinding;", "binding", "Lcom/discord/databinding/ThreadBrowserItemHeaderBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class HeaderItem extends MGRecyclerViewHolder<WidgetThreadBrowserAdapter, Item> {
        private final ThreadBrowserItemHeaderBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public HeaderItem(WidgetThreadBrowserAdapter widgetThreadBrowserAdapter) {
            super((int) R.layout.thread_browser_item_header, widgetThreadBrowserAdapter);
            m.checkNotNullParameter(widgetThreadBrowserAdapter, "adapter");
            View view = this.itemView;
            TextView textView = (TextView) view.findViewById(R.id.header_name);
            if (textView != null) {
                ThreadBrowserItemHeaderBinding threadBrowserItemHeaderBinding = new ThreadBrowserItemHeaderBinding((ConstraintLayout) view, textView);
                m.checkNotNullExpressionValue(threadBrowserItemHeaderBinding, "ThreadBrowserItemHeaderBinding.bind(itemView)");
                this.binding = threadBrowserItemHeaderBinding;
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.header_name)));
        }

        public void onConfigure(int i, Item item) {
            CharSequence b2;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            Item.Header header = (Item.Header) item;
            TextView textView = this.binding.f2141b;
            m.checkNotNullExpressionValue(textView, "binding.headerName");
            b2 = b.b(((WidgetThreadBrowserAdapter) this.adapter).getContext(), header.getStringResId(), new Object[]{Integer.valueOf(header.getCount())}, (r4 & 4) != 0 ? b.C0034b.j : null);
            textView.setText(b2.toString());
        }
    }

    /* compiled from: WidgetThreadBrowserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", Traits.Location.Section.HEADER, "Loading", "Thread", "Warning", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Warning;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Thread;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Header;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Loading;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item implements MGRecyclerDataPayload {

        /* compiled from: WidgetThreadBrowserAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0007J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u001c\u0010\t\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u001c\u0010\u0019\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010\u0015\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0015\u001a\u0004\b\u001b\u0010\u0007¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Header;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", "component3", "key", "stringResId", "count", "copy", "(Ljava/lang/String;II)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Header;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getStringResId", "Ljava/lang/String;", "getKey", "type", "getType", "getCount", HookHelper.constructorName, "(Ljava/lang/String;II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Header extends Item {
            private final int count;
            private final String key;
            private final int stringResId;
            private final int type = ItemType.HEADER.ordinal();

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Header(String str, int i, int i2) {
                super(null);
                m.checkNotNullParameter(str, "key");
                this.key = str;
                this.stringResId = i;
                this.count = i2;
            }

            public static /* synthetic */ Header copy$default(Header header, String str, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    str = header.getKey();
                }
                if ((i3 & 2) != 0) {
                    i = header.stringResId;
                }
                if ((i3 & 4) != 0) {
                    i2 = header.count;
                }
                return header.copy(str, i, i2);
            }

            public final String component1() {
                return getKey();
            }

            public final int component2() {
                return this.stringResId;
            }

            public final int component3() {
                return this.count;
            }

            public final Header copy(String str, int i, int i2) {
                m.checkNotNullParameter(str, "key");
                return new Header(str, i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Header)) {
                    return false;
                }
                Header header = (Header) obj;
                return m.areEqual(getKey(), header.getKey()) && this.stringResId == header.stringResId && this.count == header.count;
            }

            public final int getCount() {
                return this.count;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final int getStringResId() {
                return this.stringResId;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                String key = getKey();
                return ((((key != null ? key.hashCode() : 0) * 31) + this.stringResId) * 31) + this.count;
            }

            public String toString() {
                StringBuilder R = a.R("Header(key=");
                R.append(getKey());
                R.append(", stringResId=");
                R.append(this.stringResId);
                R.append(", count=");
                return a.A(R, this.count, ")");
            }
        }

        /* compiled from: WidgetThreadBrowserAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u000bR\u001c\u0010\u0005\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Loading;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "", "component1", "()Ljava/lang/String;", "key", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Loading;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends Item {
            private final String key;
            private final int type;

            public Loading() {
                this(null, 1, null);
            }

            public /* synthetic */ Loading(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? "loading" : str);
            }

            public static /* synthetic */ Loading copy$default(Loading loading, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = loading.getKey();
                }
                return loading.copy(str);
            }

            public final String component1() {
                return getKey();
            }

            public final Loading copy(String str) {
                m.checkNotNullParameter(str, "key");
                return new Loading(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loading) && m.areEqual(getKey(), ((Loading) obj).getKey());
                }
                return true;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                String key = getKey();
                if (key != null) {
                    return key.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Loading(key=");
                R.append(getKey());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loading(String str) {
                super(null);
                m.checkNotNullParameter(str, "key");
                this.key = str;
                this.type = ItemType.LOADING.ordinal();
            }
        }

        /* compiled from: WidgetThreadBrowserAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\rR\u001c\u0010\u0016\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Thread;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;", "component1", "()Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;", "threadData", "copy", "(Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Thread;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;", "getThreadData", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Thread extends Item {
            private final String key;
            private final ThreadBrowserThreadView.ThreadData threadData;
            private final int type = ItemType.THREAD.ordinal();

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Thread(ThreadBrowserThreadView.ThreadData threadData) {
                super(null);
                m.checkNotNullParameter(threadData, "threadData");
                this.threadData = threadData;
                this.key = String.valueOf(threadData.getChannel().h());
            }

            public static /* synthetic */ Thread copy$default(Thread thread, ThreadBrowserThreadView.ThreadData threadData, int i, Object obj) {
                if ((i & 1) != 0) {
                    threadData = thread.threadData;
                }
                return thread.copy(threadData);
            }

            public final ThreadBrowserThreadView.ThreadData component1() {
                return this.threadData;
            }

            public final Thread copy(ThreadBrowserThreadView.ThreadData threadData) {
                m.checkNotNullParameter(threadData, "threadData");
                return new Thread(threadData);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Thread) && m.areEqual(this.threadData, ((Thread) obj).threadData);
                }
                return true;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final ThreadBrowserThreadView.ThreadData getThreadData() {
                return this.threadData;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                ThreadBrowserThreadView.ThreadData threadData = this.threadData;
                if (threadData != null) {
                    return threadData.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Thread(threadData=");
                R.append(this.threadData);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetThreadBrowserAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0005\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004R\u001c\u0010\u0013\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u000b¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Warning;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "", "component1", "()Ljava/lang/String;", "key", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item$Warning;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getKey", "type", "I", "getType", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Warning extends Item {
            private final String key;
            private final int type;

            public Warning() {
                this(null, 1, null);
            }

            public /* synthetic */ Warning(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? "warning" : str);
            }

            public static /* synthetic */ Warning copy$default(Warning warning, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = warning.getKey();
                }
                return warning.copy(str);
            }

            public final String component1() {
                return getKey();
            }

            public final Warning copy(String str) {
                m.checkNotNullParameter(str, "key");
                return new Warning(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Warning) && m.areEqual(getKey(), ((Warning) obj).getKey());
                }
                return true;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                String key = getKey();
                if (key != null) {
                    return key.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Warning(key=");
                R.append(getKey());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Warning(String str) {
                super(null);
                m.checkNotNullParameter(str, "key");
                this.key = str;
                this.type = ItemType.WARNING.ordinal();
            }
        }

        private Item() {
        }

        public /* synthetic */ Item(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadBrowserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$ItemType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "WARNING", "THREAD", "HEADER", "LOADING", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ItemType {
        WARNING,
        THREAD,
        HEADER,
        LOADING;
        
        public static final Companion Companion = new Companion(null);
        private static final ItemType[] cachedValues = values();

        /* compiled from: WidgetThreadBrowserAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$ItemType$Companion;", "", "", "ordinal", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$ItemType;", "fromOrdinal", "(I)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$ItemType;", "", "cachedValues", "[Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$ItemType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final ItemType fromOrdinal(int i) {
                return ItemType.cachedValues[i];
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    /* compiled from: WidgetThreadBrowserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$LoadingItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class LoadingItem extends MGRecyclerViewHolder<WidgetThreadBrowserAdapter, Item> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public LoadingItem(WidgetThreadBrowserAdapter widgetThreadBrowserAdapter) {
            super((int) R.layout.thread_browser_item_loading, widgetThreadBrowserAdapter);
            m.checkNotNullParameter(widgetThreadBrowserAdapter, "adapter");
        }
    }

    /* compiled from: WidgetThreadBrowserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$ThreadItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;)V", "Lcom/discord/databinding/ThreadBrowserItemThreadBinding;", "binding", "Lcom/discord/databinding/ThreadBrowserItemThreadBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ThreadItem extends MGRecyclerViewHolder<WidgetThreadBrowserAdapter, Item> {
        private final ThreadBrowserItemThreadBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ThreadItem(WidgetThreadBrowserAdapter widgetThreadBrowserAdapter) {
            super((int) R.layout.thread_browser_item_thread, widgetThreadBrowserAdapter);
            m.checkNotNullParameter(widgetThreadBrowserAdapter, "adapter");
            View view = this.itemView;
            Objects.requireNonNull(view, "rootView");
            ThreadBrowserThreadView threadBrowserThreadView = (ThreadBrowserThreadView) view;
            ThreadBrowserItemThreadBinding threadBrowserItemThreadBinding = new ThreadBrowserItemThreadBinding(threadBrowserThreadView, threadBrowserThreadView);
            m.checkNotNullExpressionValue(threadBrowserItemThreadBinding, "ThreadBrowserItemThreadBinding.bind(itemView)");
            this.binding = threadBrowserItemThreadBinding;
        }

        public static final /* synthetic */ WidgetThreadBrowserAdapter access$getAdapter$p(ThreadItem threadItem) {
            return (WidgetThreadBrowserAdapter) threadItem.adapter;
        }

        public void onConfigure(int i, Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            final Item.Thread thread = (Item.Thread) item;
            this.binding.f2142b.setThreadData(thread.getThreadData());
            this.binding.f2142b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserAdapter$ThreadItem$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetThreadBrowserAdapter.ThreadItem.access$getAdapter$p(WidgetThreadBrowserAdapter.ThreadItem.this).onThreadClicked(thread.getThreadData().getChannel());
                }
            });
            this.binding.f2142b.setOnLongClickListener(new View.OnLongClickListener() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserAdapter$ThreadItem$onConfigure$2
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    WidgetThreadBrowserAdapter.ThreadItem.access$getAdapter$p(WidgetThreadBrowserAdapter.ThreadItem.this).onThreadLongClicked(thread.getThreadData().getChannel());
                    return true;
                }
            });
        }
    }

    /* compiled from: WidgetThreadBrowserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$WarningItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;)V", "Lcom/discord/databinding/ThreadBrowserItemWarningBinding;", "binding", "Lcom/discord/databinding/ThreadBrowserItemWarningBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class WarningItem extends MGRecyclerViewHolder<WidgetThreadBrowserAdapter, Item> {
        private final ThreadBrowserItemWarningBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public WarningItem(WidgetThreadBrowserAdapter widgetThreadBrowserAdapter) {
            super((int) R.layout.thread_browser_item_warning, widgetThreadBrowserAdapter);
            m.checkNotNullParameter(widgetThreadBrowserAdapter, "adapter");
            View view = this.itemView;
            TextView textView = (TextView) view.findViewById(R.id.warning);
            if (textView != null) {
                ThreadBrowserItemWarningBinding threadBrowserItemWarningBinding = new ThreadBrowserItemWarningBinding((ConstraintLayout) view, textView);
                m.checkNotNullExpressionValue(threadBrowserItemWarningBinding, "ThreadBrowserItemWarningBinding.bind(itemView)");
                this.binding = threadBrowserItemWarningBinding;
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.warning)));
        }

        public void onConfigure(int i, Item item) {
            CharSequence b2;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            TextView textView = this.binding.f2143b;
            m.checkNotNullExpressionValue(textView, "binding.warning");
            b2 = b.b(((WidgetThreadBrowserAdapter) this.adapter).getContext(), R.string.forum_android_warning, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            textView.setText(b2.toString());
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ItemType.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[ItemType.WARNING.ordinal()] = 1;
            iArr[ItemType.THREAD.ordinal()] = 2;
            iArr[ItemType.HEADER.ordinal()] = 3;
            iArr[ItemType.LOADING.ordinal()] = 4;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public WidgetThreadBrowserAdapter(RecyclerView recyclerView, Function1<? super Channel, Unit> function1, Function1<? super Channel, Unit> function12) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(function1, "onOpenThread");
        m.checkNotNullParameter(function12, "onThreadSettings");
        this.onOpenThread = function1;
        this.onThreadSettings = function12;
    }

    public final void onThreadClicked(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.onOpenThread.invoke(channel);
    }

    public final void onThreadLongClicked(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.onThreadSettings.invoke(channel);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        int ordinal = ItemType.Companion.fromOrdinal(i).ordinal();
        if (ordinal == 0) {
            return new WarningItem(this);
        }
        if (ordinal == 1) {
            return new ThreadItem(this);
        }
        if (ordinal == 2) {
            return new HeaderItem(this);
        }
        if (ordinal == 3) {
            return new LoadingItem(this);
        }
        throw new NoWhenBranchMatchedException();
    }
}
