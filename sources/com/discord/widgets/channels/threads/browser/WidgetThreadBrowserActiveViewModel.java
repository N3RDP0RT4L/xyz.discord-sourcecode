package com.discord.widgets.channels.threads.browser;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildMemberRequester;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadMessages;
import com.discord.stores.StoreThreadsActive;
import com.discord.stores.StoreThreadsActiveJoined;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserAdapter;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetThreadBrowserActiveViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 *2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003*+,B\u007f\u0012\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f\u0012\n\u0010\u0013\u001a\u00060\u000ej\u0002`\u0012\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0017\u0012\b\b\u0002\u0010\t\u001a\u00020\b\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u001a\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0014\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d\u0012\b\b\u0002\u0010$\u001a\u00020#\u0012\b\b\u0002\u0010!\u001a\u00020 \u0012\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00030&¢\u0006\u0004\b(\u0010)J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u001a\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0013\u001a\u00060\u000ej\u0002`\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0011R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%¨\u0006-"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$ViewState;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;)V", "Lcom/discord/stores/StoreGuildMemberRequester;", "storeGuildMemberRequester", "Lcom/discord/stores/StoreGuildMemberRequester;", "Lcom/discord/stores/StoreThreadsActiveJoined;", "storeThreadsActiveJoined", "Lcom/discord/stores/StoreThreadsActiveJoined;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreThreadMessages;", "storeThreadMessages", "Lcom/discord/stores/StoreThreadMessages;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreThreadsActive;", "storeThreadsActive", "Lcom/discord/stores/StoreThreadsActive;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(JJLcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuildMemberRequester;Lcom/discord/stores/StoreThreadsActive;Lcom/discord/stores/StoreThreadsActiveJoined;Lcom/discord/stores/StoreThreadMessages;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadBrowserActiveViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final long channelId;
    private final long guildId;
    private final StoreChannels storeChannels;
    private final StoreGuildMemberRequester storeGuildMemberRequester;
    private final StoreGuilds storeGuilds;
    private final StorePermissions storePermissions;
    private final StoreThreadMessages storeThreadMessages;
    private final StoreThreadsActive storeThreadsActive;
    private final StoreThreadsActiveJoined storeThreadsActiveJoined;
    private final StoreUser storeUser;

    /* compiled from: WidgetThreadBrowserActiveViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetThreadBrowserActiveViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetThreadBrowserActiveViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJe\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreThreadsActive;", "storeThreadsActive", "Lcom/discord/stores/StoreThreadsActiveJoined;", "storeThreadsActiveJoined", "Lcom/discord/stores/StoreThreadMessages;", "storeThreadMessages", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lrx/Observable;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;", "observeStoreState", "(JJLcom/discord/stores/StoreUser;Lcom/discord/stores/StoreThreadsActive;Lcom/discord/stores/StoreThreadsActiveJoined;Lcom/discord/stores/StoreThreadMessages;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(final long j, final long j2, final StoreUser storeUser, StoreThreadsActive storeThreadsActive, StoreThreadsActiveJoined storeThreadsActiveJoined, final StoreThreadMessages storeThreadMessages, final StoreGuilds storeGuilds, final StoreChannels storeChannels, final StorePermissions storePermissions) {
            Observable<StoreState> Y = Observable.j(storeThreadsActiveJoined.observeActiveJoinedThreadsForChannel(j, j2).F(WidgetThreadBrowserActiveViewModel$Companion$observeStoreState$1.INSTANCE), storeThreadsActive.observeActiveThreadsForChannel(j, Long.valueOf(j2)), WidgetThreadBrowserActiveViewModel$Companion$observeStoreState$2.INSTANCE).Y(new b<Pair<? extends Map<Long, ? extends Channel>, ? extends Map<Long, ? extends Channel>>, Observable<? extends StoreState>>() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel$Companion$observeStoreState$3

                /* compiled from: WidgetThreadBrowserActiveViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u0001\u001a\u00020\u00002\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0016\u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u000226\u0010\r\u001a2\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\nj\u0002`\u000b \f*\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\nj\u0002`\u000b\u0018\u00010\u00020\u00022\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\u00022\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0007\u0012\u0004\u0012\u00020\u00110\u00022\u000e\u0010\u0014\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00132\u001a\u0010\u0017\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0015j\u0002`\u00160\u00022\b\u0010\u0019\u001a\u0004\u0018\u00010\u00182\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aH\n¢\u0006\u0004\b\u001d\u0010\u001e"}, d2 = {"Lcom/discord/models/user/MeUser;", "meUser", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "users", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/stores/StoreThreadMessages$ThreadState;", "threadStates", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "kotlin.jvm.PlatformType", "guildMembers", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "", "channelNames", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "", "Lcom/discord/primitives/RelationshipType;", "blockedUsers", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;", "invoke", "(Lcom/discord/models/user/MeUser;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel$Companion$observeStoreState$3$2  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass2 extends o implements Function10<MeUser, Map<Long, ? extends User>, Map<Long, ? extends StoreThreadMessages.ThreadState>, Map<Long, ? extends GuildMember>, Map<Long, ? extends GuildRole>, Map<Long, ? extends String>, Long, Map<Long, ? extends Integer>, Channel, Guild, WidgetThreadBrowserActiveViewModel.StoreState> {
                    public final /* synthetic */ Map $activeJoinedThreads;
                    public final /* synthetic */ Map $activeThreads;

                    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                    public AnonymousClass2(Map map, Map map2) {
                        super(10);
                        this.$activeJoinedThreads = map;
                        this.$activeThreads = map2;
                    }

                    @Override // kotlin.jvm.functions.Function10
                    public /* bridge */ /* synthetic */ WidgetThreadBrowserActiveViewModel.StoreState invoke(MeUser meUser, Map<Long, ? extends User> map, Map<Long, ? extends StoreThreadMessages.ThreadState> map2, Map<Long, ? extends GuildMember> map3, Map<Long, ? extends GuildRole> map4, Map<Long, ? extends String> map5, Long l, Map<Long, ? extends Integer> map6, Channel channel, Guild guild) {
                        return invoke2(meUser, map, (Map<Long, StoreThreadMessages.ThreadState>) map2, (Map<Long, GuildMember>) map3, (Map<Long, GuildRole>) map4, (Map<Long, String>) map5, l, (Map<Long, Integer>) map6, channel, guild);
                    }

                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final WidgetThreadBrowserActiveViewModel.StoreState invoke2(MeUser meUser, Map<Long, ? extends User> map, Map<Long, StoreThreadMessages.ThreadState> map2, Map<Long, GuildMember> map3, Map<Long, GuildRole> map4, Map<Long, String> map5, Long l, Map<Long, Integer> map6, Channel channel, Guild guild) {
                        m.checkNotNullParameter(meUser, "meUser");
                        m.checkNotNullParameter(map, "users");
                        m.checkNotNullParameter(map2, "threadStates");
                        m.checkNotNullParameter(map4, "guildRoles");
                        m.checkNotNullParameter(map5, "channelNames");
                        m.checkNotNullParameter(map6, "blockedUsers");
                        Map map7 = this.$activeJoinedThreads;
                        m.checkNotNullExpressionValue(map7, "activeJoinedThreads");
                        Map map8 = this.$activeThreads;
                        m.checkNotNullExpressionValue(map8, "activeThreads");
                        m.checkNotNullExpressionValue(map3, "guildMembers");
                        return new WidgetThreadBrowserActiveViewModel.StoreState(meUser, map7, map8, map2, map3, map, map4, map5, l, map6, channel, guild);
                    }
                }

                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends WidgetThreadBrowserActiveViewModel.StoreState> call(Pair<? extends Map<Long, ? extends Channel>, ? extends Map<Long, ? extends Channel>> pair) {
                    return call2((Pair<? extends Map<Long, Channel>, ? extends Map<Long, Channel>>) pair);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends WidgetThreadBrowserActiveViewModel.StoreState> call2(Pair<? extends Map<Long, Channel>, ? extends Map<Long, Channel>> pair) {
                    Map<Long, Channel> component1 = pair.component1();
                    Map<Long, Channel> component2 = pair.component2();
                    Observable observeMe$default = StoreUser.observeMe$default(StoreUser.this, false, 1, null);
                    StoreUser storeUser2 = StoreUser.this;
                    HashSet hashSet = new HashSet();
                    m.checkNotNullExpressionValue(component1, "activeJoinedThreads");
                    for (Map.Entry<Long, Channel> entry : component1.entrySet()) {
                        hashSet.add(Long.valueOf(entry.getValue().q()));
                    }
                    m.checkNotNullExpressionValue(component2, "activeThreads");
                    for (Map.Entry<Long, Channel> entry2 : component2.entrySet()) {
                        hashSet.add(Long.valueOf(entry2.getValue().q()));
                    }
                    Observable<Map<Long, User>> observeUsers = storeUser2.observeUsers(hashSet);
                    Observable<Map<Long, StoreThreadMessages.ThreadState>> observeThreadCountAndLatestMessage = storeThreadMessages.observeThreadCountAndLatestMessage();
                    Observable<Map<Long, GuildMember>> observeGuildMembers = storeGuilds.observeGuildMembers(j);
                    m.checkNotNullExpressionValue(observeGuildMembers, "storeGuilds\n            …erveGuildMembers(guildId)");
                    Observable<T> q = ObservableExtensionsKt.leadingEdgeThrottle(observeGuildMembers, 1L, TimeUnit.SECONDS).q();
                    m.checkNotNullExpressionValue(q, "storeGuilds\n            …  .distinctUntilChanged()");
                    return ObservableCombineLatestOverloadsKt.combineLatest(observeMe$default, observeUsers, observeThreadCountAndLatestMessage, q, storeGuilds.observeRoles(j), storeChannels.observeNames(), storePermissions.observePermissionsForChannel(j2), StoreStream.Companion.getUserRelationships().observeForType(2), storeChannels.observeChannel(j2), storeGuilds.observeGuild(j), new AnonymousClass2(component1, component2));
                }
            });
            m.checkNotNullExpressionValue(Y, "Observable.combineLatest…            }\n          }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadBrowserActiveViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001B÷\u0001\u0012\u0006\u0010#\u001a\u00020\u0002\u0012\u0016\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0016\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0016\u0010&\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\f0\u0005\u0012\u0016\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\u0005\u0012\u0016\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00110\u0005\u0012\u0016\u0010)\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u0005\u0012\u0016\u0010*\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u0005\u0012\u000e\u0010+\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0018\u0012\u001a\u0010,\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\b\u0012\u00060\u001bj\u0002`\u001c0\u0005\u0012\b\u0010-\u001a\u0004\u0018\u00010\b\u0012\b\u0010.\u001a\u0004\u0018\u00010 ¢\u0006\u0004\bJ\u0010KJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ \u0010\r\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\f0\u0005HÆ\u0003¢\u0006\u0004\b\r\u0010\nJ \u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\u0005HÆ\u0003¢\u0006\u0004\b\u0010\u0010\nJ \u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00110\u0005HÆ\u0003¢\u0006\u0004\b\u0012\u0010\nJ \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u0005HÆ\u0003¢\u0006\u0004\b\u0015\u0010\nJ \u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u0005HÆ\u0003¢\u0006\u0004\b\u0017\u0010\nJ\u0018\u0010\u0019\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ$\u0010\u001d\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\b\u0012\u00060\u001bj\u0002`\u001c0\u0005HÆ\u0003¢\u0006\u0004\b\u001d\u0010\nJ\u0012\u0010\u001e\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ\u0012\u0010!\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b!\u0010\"J\u0098\u0002\u0010/\u001a\u00020\u00002\b\b\u0002\u0010#\u001a\u00020\u00022\u0018\b\u0002\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\u0018\b\u0002\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\u0018\b\u0002\u0010&\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\f0\u00052\u0018\b\u0002\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\u00052\u0018\b\u0002\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00110\u00052\u0018\b\u0002\u0010)\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u00052\u0018\b\u0002\u0010*\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u00052\u0010\b\u0002\u0010+\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00182\u001c\b\u0002\u0010,\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\b\u0012\u00060\u001bj\u0002`\u001c0\u00052\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010.\u001a\u0004\u0018\u00010 HÆ\u0001¢\u0006\u0004\b/\u00100J\u0010\u00101\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b1\u00102J\u0010\u00103\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b3\u00104J\u001a\u00107\u001a\u0002062\b\u00105\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b7\u00108R)\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b$\u00109\u001a\u0004\b:\u0010\nR)\u0010*\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00160\u00058\u0006@\u0006¢\u0006\f\n\u0004\b*\u00109\u001a\u0004\b;\u0010\nR-\u0010,\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\b\u0012\u00060\u001bj\u0002`\u001c0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b,\u00109\u001a\u0004\b<\u0010\nR\u001b\u0010.\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010=\u001a\u0004\b>\u0010\"R)\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b%\u00109\u001a\u0004\b?\u0010\nR\u0019\u0010#\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010@\u001a\u0004\bA\u0010\u0004R)\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00110\u00058\u0006@\u0006¢\u0006\f\n\u0004\b(\u00109\u001a\u0004\bB\u0010\nR)\u0010)\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u00058\u0006@\u0006¢\u0006\f\n\u0004\b)\u00109\u001a\u0004\bC\u0010\nR)\u0010&\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\f0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b&\u00109\u001a\u0004\bD\u0010\nR\u001b\u0010-\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010E\u001a\u0004\bF\u0010\u001fR)\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b'\u00109\u001a\u0004\bG\u0010\nR!\u0010+\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00188\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010H\u001a\u0004\bI\u0010\u001a¨\u0006L"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component2", "()Ljava/util/Map;", "component3", "Lcom/discord/stores/StoreThreadMessages$ThreadState;", "component4", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "component5", "Lcom/discord/models/user/User;", "component6", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component7", "", "component8", "Lcom/discord/api/permission/PermissionBit;", "component9", "()Ljava/lang/Long;", "", "Lcom/discord/primitives/RelationshipType;", "component10", "component11", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component12", "()Lcom/discord/models/guild/Guild;", "meUser", "activeJoinedThreads", "activeThreads", "threadStates", "guildMembers", "users", "guildRoles", "channelNames", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "blockedUsers", "channel", "guild", "copy", "(Lcom/discord/models/user/MeUser;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$StoreState;", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getActiveJoinedThreads", "getChannelNames", "getBlockedUsers", "Lcom/discord/models/guild/Guild;", "getGuild", "getActiveThreads", "Lcom/discord/models/user/MeUser;", "getMeUser", "getUsers", "getGuildRoles", "getThreadStates", "Lcom/discord/api/channel/Channel;", "getChannel", "getGuildMembers", "Ljava/lang/Long;", "getPermissions", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, Channel> activeJoinedThreads;
        private final Map<Long, Channel> activeThreads;
        private final Map<Long, Integer> blockedUsers;
        private final Channel channel;
        private final Map<Long, String> channelNames;
        private final Guild guild;
        private final Map<Long, GuildMember> guildMembers;
        private final Map<Long, GuildRole> guildRoles;
        private final MeUser meUser;
        private final Long permissions;
        private final Map<Long, StoreThreadMessages.ThreadState> threadStates;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(MeUser meUser, Map<Long, Channel> map, Map<Long, Channel> map2, Map<Long, StoreThreadMessages.ThreadState> map3, Map<Long, GuildMember> map4, Map<Long, ? extends User> map5, Map<Long, GuildRole> map6, Map<Long, String> map7, Long l, Map<Long, Integer> map8, Channel channel, Guild guild) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(map, "activeJoinedThreads");
            m.checkNotNullParameter(map2, "activeThreads");
            m.checkNotNullParameter(map3, "threadStates");
            m.checkNotNullParameter(map4, "guildMembers");
            m.checkNotNullParameter(map5, "users");
            m.checkNotNullParameter(map6, "guildRoles");
            m.checkNotNullParameter(map7, "channelNames");
            m.checkNotNullParameter(map8, "blockedUsers");
            this.meUser = meUser;
            this.activeJoinedThreads = map;
            this.activeThreads = map2;
            this.threadStates = map3;
            this.guildMembers = map4;
            this.users = map5;
            this.guildRoles = map6;
            this.channelNames = map7;
            this.permissions = l;
            this.blockedUsers = map8;
            this.channel = channel;
            this.guild = guild;
        }

        public final MeUser component1() {
            return this.meUser;
        }

        public final Map<Long, Integer> component10() {
            return this.blockedUsers;
        }

        public final Channel component11() {
            return this.channel;
        }

        public final Guild component12() {
            return this.guild;
        }

        public final Map<Long, Channel> component2() {
            return this.activeJoinedThreads;
        }

        public final Map<Long, Channel> component3() {
            return this.activeThreads;
        }

        public final Map<Long, StoreThreadMessages.ThreadState> component4() {
            return this.threadStates;
        }

        public final Map<Long, GuildMember> component5() {
            return this.guildMembers;
        }

        public final Map<Long, User> component6() {
            return this.users;
        }

        public final Map<Long, GuildRole> component7() {
            return this.guildRoles;
        }

        public final Map<Long, String> component8() {
            return this.channelNames;
        }

        public final Long component9() {
            return this.permissions;
        }

        public final StoreState copy(MeUser meUser, Map<Long, Channel> map, Map<Long, Channel> map2, Map<Long, StoreThreadMessages.ThreadState> map3, Map<Long, GuildMember> map4, Map<Long, ? extends User> map5, Map<Long, GuildRole> map6, Map<Long, String> map7, Long l, Map<Long, Integer> map8, Channel channel, Guild guild) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(map, "activeJoinedThreads");
            m.checkNotNullParameter(map2, "activeThreads");
            m.checkNotNullParameter(map3, "threadStates");
            m.checkNotNullParameter(map4, "guildMembers");
            m.checkNotNullParameter(map5, "users");
            m.checkNotNullParameter(map6, "guildRoles");
            m.checkNotNullParameter(map7, "channelNames");
            m.checkNotNullParameter(map8, "blockedUsers");
            return new StoreState(meUser, map, map2, map3, map4, map5, map6, map7, l, map8, channel, guild);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.meUser, storeState.meUser) && m.areEqual(this.activeJoinedThreads, storeState.activeJoinedThreads) && m.areEqual(this.activeThreads, storeState.activeThreads) && m.areEqual(this.threadStates, storeState.threadStates) && m.areEqual(this.guildMembers, storeState.guildMembers) && m.areEqual(this.users, storeState.users) && m.areEqual(this.guildRoles, storeState.guildRoles) && m.areEqual(this.channelNames, storeState.channelNames) && m.areEqual(this.permissions, storeState.permissions) && m.areEqual(this.blockedUsers, storeState.blockedUsers) && m.areEqual(this.channel, storeState.channel) && m.areEqual(this.guild, storeState.guild);
        }

        public final Map<Long, Channel> getActiveJoinedThreads() {
            return this.activeJoinedThreads;
        }

        public final Map<Long, Channel> getActiveThreads() {
            return this.activeThreads;
        }

        public final Map<Long, Integer> getBlockedUsers() {
            return this.blockedUsers;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Map<Long, String> getChannelNames() {
            return this.channelNames;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Map<Long, GuildMember> getGuildMembers() {
            return this.guildMembers;
        }

        public final Map<Long, GuildRole> getGuildRoles() {
            return this.guildRoles;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final Long getPermissions() {
            return this.permissions;
        }

        public final Map<Long, StoreThreadMessages.ThreadState> getThreadStates() {
            return this.threadStates;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            MeUser meUser = this.meUser;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            Map<Long, Channel> map = this.activeJoinedThreads;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Channel> map2 = this.activeThreads;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, StoreThreadMessages.ThreadState> map3 = this.threadStates;
            int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, GuildMember> map4 = this.guildMembers;
            int hashCode5 = (hashCode4 + (map4 != null ? map4.hashCode() : 0)) * 31;
            Map<Long, User> map5 = this.users;
            int hashCode6 = (hashCode5 + (map5 != null ? map5.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map6 = this.guildRoles;
            int hashCode7 = (hashCode6 + (map6 != null ? map6.hashCode() : 0)) * 31;
            Map<Long, String> map7 = this.channelNames;
            int hashCode8 = (hashCode7 + (map7 != null ? map7.hashCode() : 0)) * 31;
            Long l = this.permissions;
            int hashCode9 = (hashCode8 + (l != null ? l.hashCode() : 0)) * 31;
            Map<Long, Integer> map8 = this.blockedUsers;
            int hashCode10 = (hashCode9 + (map8 != null ? map8.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode11 = (hashCode10 + (channel != null ? channel.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            if (guild != null) {
                i = guild.hashCode();
            }
            return hashCode11 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(meUser=");
            R.append(this.meUser);
            R.append(", activeJoinedThreads=");
            R.append(this.activeJoinedThreads);
            R.append(", activeThreads=");
            R.append(this.activeThreads);
            R.append(", threadStates=");
            R.append(this.threadStates);
            R.append(", guildMembers=");
            R.append(this.guildMembers);
            R.append(", users=");
            R.append(this.users);
            R.append(", guildRoles=");
            R.append(this.guildRoles);
            R.append(", channelNames=");
            R.append(this.channelNames);
            R.append(", permissions=");
            R.append(this.permissions);
            R.append(", blockedUsers=");
            R.append(this.blockedUsers);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(this.guild);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetThreadBrowserActiveViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0014\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\bR\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$ViewState;", "", "", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "component1", "()Ljava/util/List;", "", "component2", "()Z", "listItems", "canCreateThread", "copy", "(Ljava/util/List;Z)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanCreateThread", "Ljava/util/List;", "getListItems", HookHelper.constructorName, "(Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean canCreateThread;
        private final List<WidgetThreadBrowserAdapter.Item> listItems;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(List<? extends WidgetThreadBrowserAdapter.Item> list, boolean z2) {
            m.checkNotNullParameter(list, "listItems");
            this.listItems = list;
            this.canCreateThread = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, List list, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = viewState.listItems;
            }
            if ((i & 2) != 0) {
                z2 = viewState.canCreateThread;
            }
            return viewState.copy(list, z2);
        }

        public final List<WidgetThreadBrowserAdapter.Item> component1() {
            return this.listItems;
        }

        public final boolean component2() {
            return this.canCreateThread;
        }

        public final ViewState copy(List<? extends WidgetThreadBrowserAdapter.Item> list, boolean z2) {
            m.checkNotNullParameter(list, "listItems");
            return new ViewState(list, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.listItems, viewState.listItems) && this.canCreateThread == viewState.canCreateThread;
        }

        public final boolean getCanCreateThread() {
            return this.canCreateThread;
        }

        public final List<WidgetThreadBrowserAdapter.Item> getListItems() {
            return this.listItems;
        }

        public int hashCode() {
            List<WidgetThreadBrowserAdapter.Item> list = this.listItems;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            boolean z2 = this.canCreateThread;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(listItems=");
            R.append(this.listItems);
            R.append(", canCreateThread=");
            return a.M(R, this.canCreateThread, ")");
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetThreadBrowserActiveViewModel(long r22, long r24, com.discord.stores.StoreUser r26, com.discord.stores.StoreGuildMemberRequester r27, com.discord.stores.StoreThreadsActive r28, com.discord.stores.StoreThreadsActiveJoined r29, com.discord.stores.StoreThreadMessages r30, com.discord.stores.StoreGuilds r31, com.discord.stores.StoreChannels r32, com.discord.stores.StorePermissions r33, rx.Observable r34, int r35, kotlin.jvm.internal.DefaultConstructorMarker r36) {
        /*
            r21 = this;
            r0 = r35
            r1 = r0 & 4
            if (r1 == 0) goto Ld
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r1 = r1.getUsers()
            goto Lf
        Ld:
            r1 = r26
        Lf:
            r2 = r0 & 8
            if (r2 == 0) goto L1b
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildMemberRequester r2 = r2.getGuildMemberRequester()
            r14 = r2
            goto L1d
        L1b:
            r14 = r27
        L1d:
            r2 = r0 & 16
            if (r2 == 0) goto L29
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreThreadsActive r2 = r2.getThreadsActive()
            r15 = r2
            goto L2b
        L29:
            r15 = r28
        L2b:
            r2 = r0 & 32
            if (r2 == 0) goto L38
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreThreadsActiveJoined r2 = r2.getThreadsActiveJoined()
            r16 = r2
            goto L3a
        L38:
            r16 = r29
        L3a:
            r2 = r0 & 64
            if (r2 == 0) goto L47
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreThreadMessages r2 = r2.getThreadMessages()
            r17 = r2
            goto L49
        L47:
            r17 = r30
        L49:
            r2 = r0 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L56
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r2 = r2.getGuilds()
            r18 = r2
            goto L58
        L56:
            r18 = r31
        L58:
            r2 = r0 & 256(0x100, float:3.59E-43)
            if (r2 == 0) goto L65
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r2 = r2.getChannels()
            r19 = r2
            goto L67
        L65:
            r19 = r32
        L67:
            r2 = r0 & 512(0x200, float:7.175E-43)
            if (r2 == 0) goto L74
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePermissions r2 = r2.getPermissions()
            r20 = r2
            goto L76
        L74:
            r20 = r33
        L76:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L91
            com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel$Companion r2 = com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel.Companion
            r3 = r22
            r5 = r24
            r7 = r1
            r8 = r15
            r9 = r16
            r10 = r17
            r11 = r18
            r12 = r19
            r13 = r20
            rx.Observable r0 = com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel.Companion.access$observeStoreState(r2, r3, r5, r7, r8, r9, r10, r11, r12, r13)
            goto L93
        L91:
            r0 = r34
        L93:
            r2 = r21
            r3 = r22
            r5 = r24
            r7 = r1
            r8 = r14
            r9 = r15
            r10 = r16
            r11 = r17
            r12 = r18
            r13 = r19
            r14 = r20
            r15 = r0
            r2.<init>(r3, r5, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel.<init>(long, long, com.discord.stores.StoreUser, com.discord.stores.StoreGuildMemberRequester, com.discord.stores.StoreThreadsActive, com.discord.stores.StoreThreadsActiveJoined, com.discord.stores.StoreThreadMessages, com.discord.stores.StoreGuilds, com.discord.stores.StoreChannels, com.discord.stores.StorePermissions, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        int i;
        Iterator it;
        Iterator it2;
        com.discord.api.user.User author;
        com.discord.api.user.User author2;
        ArrayList arrayList = new ArrayList();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        boolean z2 = true;
        List listOf = n.listOf((Object[]) new Triple[]{new Triple("joined", Integer.valueOf((int) R.string.thread_browser_joined_header), storeState.getActiveJoinedThreads()), new Triple("other", Integer.valueOf((int) R.string.thread_browser_other_header), storeState.getActiveThreads())});
        Channel channel = storeState.getChannel();
        Message message = null;
        if (channel != null && channel.A() == 15) {
            arrayList.add(new WidgetThreadBrowserAdapter.Item.Warning(null, 1, null));
        }
        Iterator it3 = listOf.iterator();
        while (it3.hasNext()) {
            Triple triple = (Triple) it3.next();
            String str = (String) triple.component1();
            int intValue = ((Number) triple.component2()).intValue();
            Map map = (Map) triple.component3();
            if (map.isEmpty()) {
                i = 0;
            } else {
                i = 0;
                for (Map.Entry entry : map.entrySet()) {
                    if (linkedHashSet.contains(Long.valueOf(((Number) entry.getKey()).longValue())) ^ z2) {
                        i++;
                    }
                }
            }
            if (i != 0) {
                arrayList.add(new WidgetThreadBrowserAdapter.Item.Header(str, intValue, i));
                Iterator it4 = map.entrySet().iterator();
                while (it4.hasNext()) {
                    Map.Entry entry2 = (Map.Entry) it4.next();
                    long longValue = ((Number) entry2.getKey()).longValue();
                    Channel channel2 = (Channel) entry2.getValue();
                    if (linkedHashSet.contains(Long.valueOf(longValue))) {
                        it2 = it3;
                        it = it4;
                    } else {
                        User user = storeState.getUsers().get(Long.valueOf(channel2.q()));
                        if (user == null || !storeState.getGuildMembers().containsKey(Long.valueOf(channel2.q()))) {
                            this.storeGuildMemberRequester.queueRequest(channel2.f(), channel2.q());
                        }
                        StoreThreadMessages.ThreadState threadState = storeState.getThreadStates().get(Long.valueOf(longValue));
                        Message mostRecentMessage = threadState != null ? threadState.getMostRecentMessage() : message;
                        if (mostRecentMessage == null || (author2 = mostRecentMessage.getAuthor()) == null || storeState.getGuildMembers().containsKey(Long.valueOf(author2.i()))) {
                            it2 = it3;
                            it = it4;
                        } else {
                            it2 = it3;
                            it = it4;
                            this.storeGuildMemberRequester.queueRequest(channel2.f(), author2.i());
                        }
                        arrayList.add(new WidgetThreadBrowserAdapter.Item.Thread(new ThreadBrowserThreadView.ThreadData.ActiveThread(channel2, user, mostRecentMessage, storeState.getMeUser().getId(), storeState.getGuildMembers(), storeState.getGuildRoles(), storeState.getChannelNames(), storeState.getBlockedUsers().containsKey((mostRecentMessage == null || (author = mostRecentMessage.getAuthor()) == null) ? null : Long.valueOf(author.i())))));
                        linkedHashSet.add(Long.valueOf(longValue));
                    }
                    it3 = it2;
                    it4 = it;
                    message = null;
                }
            }
            it3 = it3;
            z2 = true;
            message = null;
        }
        if (linkedHashSet.isEmpty()) {
            arrayList.clear();
        }
        this.storeGuildMemberRequester.performQueuedRequests();
        updateViewState(new ViewState(arrayList, ThreadUtils.INSTANCE.canCreateThread(storeState.getPermissions(), storeState.getChannel(), null, storeState.getGuild())));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetThreadBrowserActiveViewModel(long j, long j2, StoreUser storeUser, StoreGuildMemberRequester storeGuildMemberRequester, StoreThreadsActive storeThreadsActive, StoreThreadsActiveJoined storeThreadsActiveJoined, StoreThreadMessages storeThreadMessages, StoreGuilds storeGuilds, StoreChannels storeChannels, StorePermissions storePermissions, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeGuildMemberRequester, "storeGuildMemberRequester");
        m.checkNotNullParameter(storeThreadsActive, "storeThreadsActive");
        m.checkNotNullParameter(storeThreadsActiveJoined, "storeThreadsActiveJoined");
        m.checkNotNullParameter(storeThreadMessages, "storeThreadMessages");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.guildId = j;
        this.channelId = j2;
        this.storeUser = storeUser;
        this.storeGuildMemberRequester = storeGuildMemberRequester;
        this.storeThreadsActive = storeThreadsActive;
        this.storeThreadsActiveJoined = storeThreadsActiveJoined;
        this.storeThreadMessages = storeThreadMessages;
        this.storeGuilds = storeGuilds;
        this.storeChannels = storeChannels;
        this.storePermissions = storePermissions;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetThreadBrowserActiveViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
