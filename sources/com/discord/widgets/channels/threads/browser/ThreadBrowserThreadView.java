package com.discord.widgets.channels.threads.browser;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.Typeface;
import android.text.format.DateUtils;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.role.GuildRole;
import com.discord.api.thread.ThreadMetadata;
import com.discord.databinding.ThreadBrowserThreadViewBinding;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.CoreUser;
import com.discord.models.user.User;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.spans.TypefaceSpanCompat;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildWelcomeKt;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: ThreadBrowserThreadView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002*+B'\b\u0007\u0012\u0006\u0010#\u001a\u00020\"\u0012\n\b\u0002\u0010%\u001a\u0004\u0018\u00010$\u0012\b\b\u0002\u0010'\u001a\u00020&¢\u0006\u0004\b(\u0010)J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ%\u0010\u0013\u001a\u00020\u00042\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\nH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u001b\u0010\u0019\u001a\u00020\u00182\n\u0010\u0017\u001a\u00060\u0015j\u0002`\u0016H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u001b\u0010\u001b\u001a\u00020\u00182\n\u0010\u0017\u001a\u00060\u0015j\u0002`\u0016H\u0002¢\u0006\u0004\b\u001b\u0010\u001aJ\u0015\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u001c¢\u0006\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!¨\u0006,"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView;", "Landroid/widget/FrameLayout;", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ActiveThread;", "threadData", "", "configureActiveThreadUI", "(Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ActiveThread;)V", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ArchivedThread;", "configureArchivedThreadUI", "(Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ArchivedThread;)V", "Lcom/discord/models/member/GuildMember;", "member", "", "Landroid/text/style/CharacterStyle;", "getMemberCharacterStyles", "(Lcom/discord/models/member/GuildMember;)Ljava/util/List;", "Lcom/discord/models/user/User;", "user", "guildMember", "configureAvatar", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)V", "", "Lcom/discord/primitives/Timestamp;", "timestamp", "", "formatActivityTimestamp", "(J)Ljava/lang/CharSequence;", "formatDateTimestamp", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;", "setThreadData", "(Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;)V", "Lcom/discord/databinding/ThreadBrowserThreadViewBinding;", "binding", "Lcom/discord/databinding/ThreadBrowserThreadViewBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "ThreadData", "TimestampMode", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ThreadBrowserThreadView extends FrameLayout {
    private final ThreadBrowserThreadViewBinding binding;

    /* compiled from: ThreadBrowserThreadView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0016\u0017B3\b\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\n\u0012\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002¢\u0006\u0004\b\u0014\u0010\u0015R,\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR\u001e\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u001c\u0010\u0010\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\u0082\u0001\u0002\u0018\u0019¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;", "", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "guildMembers", "Ljava/util/Map;", "getGuildMembers", "()Ljava/util/Map;", "Lcom/discord/models/user/User;", "owner", "Lcom/discord/models/user/User;", "getOwner", "()Lcom/discord/models/user/User;", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/user/User;Ljava/util/Map;)V", "ActiveThread", "ArchivedThread", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ActiveThread;", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ArchivedThread;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ThreadData {
        private final Channel channel;
        private final Map<Long, GuildMember> guildMembers;
        private final User owner;

        /* compiled from: ThreadBrowserThreadView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B\u007f\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\b\u0012\n\u0010\u001f\u001a\u00060\u000bj\u0002`\f\u0012\u0016\u0010 \u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0016\u0010!\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000f\u0012\u0016\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0016\u0012\u0004\u0012\u00020\u00170\u000f\u0012\u0006\u0010#\u001a\u00020\u0019¢\u0006\u0004\b<\u0010=J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\r\u001a\u00060\u000bj\u0002`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ \u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u00100\u000fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000fHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0012J \u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0016\u0012\u0004\u0012\u00020\u00170\u000fHÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0012J\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0098\u0001\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001c\u001a\u00020\u00022\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\b2\f\b\u0002\u0010\u001f\u001a\u00060\u000bj\u0002`\f2\u0018\b\u0002\u0010 \u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u00100\u000f2\u0018\b\u0002\u0010!\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000f2\u0018\b\u0002\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0016\u0012\u0004\u0012\u00020\u00170\u000f2\b\b\u0002\u0010#\u001a\u00020\u0019HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010&\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010)\u001a\u00020(HÖ\u0001¢\u0006\u0004\b)\u0010*J\u001a\u0010-\u001a\u00020\u00192\b\u0010,\u001a\u0004\u0018\u00010+HÖ\u0003¢\u0006\u0004\b-\u0010.R\u001d\u0010\u001f\u001a\u00060\u000bj\u0002`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010/\u001a\u0004\b0\u0010\u000eR\u001c\u0010\u001c\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u00101\u001a\u0004\b2\u0010\u0004R)\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0016\u0012\u0004\u0012\u00020\u00170\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00103\u001a\u0004\b4\u0010\u0012R)\u0010!\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00103\u001a\u0004\b5\u0010\u0012R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00106\u001a\u0004\b7\u0010\nR\u0019\u0010#\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b#\u00108\u001a\u0004\b#\u0010\u001bR\u001e\u0010\u001d\u001a\u0004\u0018\u00010\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u00109\u001a\u0004\b:\u0010\u0007R,\u0010 \u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u00100\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u00103\u001a\u0004\b;\u0010\u0012¨\u0006>"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ActiveThread;", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/user/User;", "component2", "()Lcom/discord/models/user/User;", "Lcom/discord/models/message/Message;", "component3", "()Lcom/discord/models/message/Message;", "", "Lcom/discord/primitives/UserId;", "component4", "()J", "", "Lcom/discord/models/member/GuildMember;", "component5", "()Ljava/util/Map;", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component6", "Lcom/discord/primitives/ChannelId;", "", "component7", "", "component8", "()Z", "channel", "owner", "message", "myUserId", "guildMembers", "guildRoles", "channelNames", "isMessageBlocked", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/user/User;Lcom/discord/models/message/Message;JLjava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ActiveThread;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getMyUserId", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/Map;", "getChannelNames", "getGuildRoles", "Lcom/discord/models/message/Message;", "getMessage", "Z", "Lcom/discord/models/user/User;", "getOwner", "getGuildMembers", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/user/User;Lcom/discord/models/message/Message;JLjava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ActiveThread extends ThreadData {
            private final Channel channel;
            private final Map<Long, String> channelNames;
            private final Map<Long, GuildMember> guildMembers;
            private final Map<Long, GuildRole> guildRoles;
            private final boolean isMessageBlocked;
            private final Message message;
            private final long myUserId;
            private final User owner;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ActiveThread(Channel channel, User user, Message message, long j, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, String> map3, boolean z2) {
                super(channel, user, map, null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "guildMembers");
                m.checkNotNullParameter(map2, "guildRoles");
                m.checkNotNullParameter(map3, "channelNames");
                this.channel = channel;
                this.owner = user;
                this.message = message;
                this.myUserId = j;
                this.guildMembers = map;
                this.guildRoles = map2;
                this.channelNames = map3;
                this.isMessageBlocked = z2;
            }

            public final Channel component1() {
                return getChannel();
            }

            public final User component2() {
                return getOwner();
            }

            public final Message component3() {
                return this.message;
            }

            public final long component4() {
                return this.myUserId;
            }

            public final Map<Long, GuildMember> component5() {
                return getGuildMembers();
            }

            public final Map<Long, GuildRole> component6() {
                return this.guildRoles;
            }

            public final Map<Long, String> component7() {
                return this.channelNames;
            }

            public final boolean component8() {
                return this.isMessageBlocked;
            }

            public final ActiveThread copy(Channel channel, User user, Message message, long j, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, String> map3, boolean z2) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "guildMembers");
                m.checkNotNullParameter(map2, "guildRoles");
                m.checkNotNullParameter(map3, "channelNames");
                return new ActiveThread(channel, user, message, j, map, map2, map3, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ActiveThread)) {
                    return false;
                }
                ActiveThread activeThread = (ActiveThread) obj;
                return m.areEqual(getChannel(), activeThread.getChannel()) && m.areEqual(getOwner(), activeThread.getOwner()) && m.areEqual(this.message, activeThread.message) && this.myUserId == activeThread.myUserId && m.areEqual(getGuildMembers(), activeThread.getGuildMembers()) && m.areEqual(this.guildRoles, activeThread.guildRoles) && m.areEqual(this.channelNames, activeThread.channelNames) && this.isMessageBlocked == activeThread.isMessageBlocked;
            }

            @Override // com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.ThreadData
            public Channel getChannel() {
                return this.channel;
            }

            public final Map<Long, String> getChannelNames() {
                return this.channelNames;
            }

            @Override // com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.ThreadData
            public Map<Long, GuildMember> getGuildMembers() {
                return this.guildMembers;
            }

            public final Map<Long, GuildRole> getGuildRoles() {
                return this.guildRoles;
            }

            public final Message getMessage() {
                return this.message;
            }

            public final long getMyUserId() {
                return this.myUserId;
            }

            @Override // com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.ThreadData
            public User getOwner() {
                return this.owner;
            }

            public int hashCode() {
                Channel channel = getChannel();
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                User owner = getOwner();
                int hashCode2 = (hashCode + (owner != null ? owner.hashCode() : 0)) * 31;
                Message message = this.message;
                int a = (b.a(this.myUserId) + ((hashCode2 + (message != null ? message.hashCode() : 0)) * 31)) * 31;
                Map<Long, GuildMember> guildMembers = getGuildMembers();
                int hashCode3 = (a + (guildMembers != null ? guildMembers.hashCode() : 0)) * 31;
                Map<Long, GuildRole> map = this.guildRoles;
                int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
                Map<Long, String> map2 = this.channelNames;
                if (map2 != null) {
                    i = map2.hashCode();
                }
                int i2 = (hashCode4 + i) * 31;
                boolean z2 = this.isMessageBlocked;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public final boolean isMessageBlocked() {
                return this.isMessageBlocked;
            }

            public String toString() {
                StringBuilder R = a.R("ActiveThread(channel=");
                R.append(getChannel());
                R.append(", owner=");
                R.append(getOwner());
                R.append(", message=");
                R.append(this.message);
                R.append(", myUserId=");
                R.append(this.myUserId);
                R.append(", guildMembers=");
                R.append(getGuildMembers());
                R.append(", guildRoles=");
                R.append(this.guildRoles);
                R.append(", channelNames=");
                R.append(this.channelNames);
                R.append(", isMessageBlocked=");
                return a.M(R, this.isMessageBlocked, ")");
            }
        }

        /* compiled from: ThreadBrowserThreadView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u0014\u001a\u00020\u000e¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JJ\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\u0018\b\u0002\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\u0014\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010 \u001a\u00020\u001f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÖ\u0003¢\u0006\u0004\b \u0010!R\u001c\u0010\u0011\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\u0004R\u001e\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b%\u0010\rR,\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0012\u0010&\u001a\u0004\b'\u0010\nR\u0019\u0010\u0014\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010(\u001a\u0004\b)\u0010\u0010¨\u0006,"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ArchivedThread;", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "component2", "()Ljava/util/Map;", "Lcom/discord/models/user/User;", "component3", "()Lcom/discord/models/user/User;", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$TimestampMode;", "component4", "()Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$TimestampMode;", "channel", "guildMembers", "owner", "timestampMode", "copy", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Lcom/discord/models/user/User;Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$TimestampMode;)Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$ThreadData$ArchivedThread;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/user/User;", "getOwner", "Ljava/util/Map;", "getGuildMembers", "Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$TimestampMode;", "getTimestampMode", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Lcom/discord/models/user/User;Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$TimestampMode;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ArchivedThread extends ThreadData {
            private final Channel channel;
            private final Map<Long, GuildMember> guildMembers;
            private final User owner;
            private final TimestampMode timestampMode;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ArchivedThread(Channel channel, Map<Long, GuildMember> map, User user, TimestampMode timestampMode) {
                super(channel, user, map, null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "guildMembers");
                m.checkNotNullParameter(timestampMode, "timestampMode");
                this.channel = channel;
                this.guildMembers = map;
                this.owner = user;
                this.timestampMode = timestampMode;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ArchivedThread copy$default(ArchivedThread archivedThread, Channel channel, Map map, User user, TimestampMode timestampMode, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = archivedThread.getChannel();
                }
                if ((i & 2) != 0) {
                    map = archivedThread.getGuildMembers();
                }
                if ((i & 4) != 0) {
                    user = archivedThread.getOwner();
                }
                if ((i & 8) != 0) {
                    timestampMode = archivedThread.timestampMode;
                }
                return archivedThread.copy(channel, map, user, timestampMode);
            }

            public final Channel component1() {
                return getChannel();
            }

            public final Map<Long, GuildMember> component2() {
                return getGuildMembers();
            }

            public final User component3() {
                return getOwner();
            }

            public final TimestampMode component4() {
                return this.timestampMode;
            }

            public final ArchivedThread copy(Channel channel, Map<Long, GuildMember> map, User user, TimestampMode timestampMode) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "guildMembers");
                m.checkNotNullParameter(timestampMode, "timestampMode");
                return new ArchivedThread(channel, map, user, timestampMode);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ArchivedThread)) {
                    return false;
                }
                ArchivedThread archivedThread = (ArchivedThread) obj;
                return m.areEqual(getChannel(), archivedThread.getChannel()) && m.areEqual(getGuildMembers(), archivedThread.getGuildMembers()) && m.areEqual(getOwner(), archivedThread.getOwner()) && m.areEqual(this.timestampMode, archivedThread.timestampMode);
            }

            @Override // com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.ThreadData
            public Channel getChannel() {
                return this.channel;
            }

            @Override // com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.ThreadData
            public Map<Long, GuildMember> getGuildMembers() {
                return this.guildMembers;
            }

            @Override // com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.ThreadData
            public User getOwner() {
                return this.owner;
            }

            public final TimestampMode getTimestampMode() {
                return this.timestampMode;
            }

            public int hashCode() {
                Channel channel = getChannel();
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Map<Long, GuildMember> guildMembers = getGuildMembers();
                int hashCode2 = (hashCode + (guildMembers != null ? guildMembers.hashCode() : 0)) * 31;
                User owner = getOwner();
                int hashCode3 = (hashCode2 + (owner != null ? owner.hashCode() : 0)) * 31;
                TimestampMode timestampMode = this.timestampMode;
                if (timestampMode != null) {
                    i = timestampMode.hashCode();
                }
                return hashCode3 + i;
            }

            public String toString() {
                StringBuilder R = a.R("ArchivedThread(channel=");
                R.append(getChannel());
                R.append(", guildMembers=");
                R.append(getGuildMembers());
                R.append(", owner=");
                R.append(getOwner());
                R.append(", timestampMode=");
                R.append(this.timestampMode);
                R.append(")");
                return R.toString();
            }
        }

        private ThreadData(Channel channel, User user, Map<Long, GuildMember> map) {
            this.channel = channel;
            this.owner = user;
            this.guildMembers = map;
        }

        public Channel getChannel() {
            return this.channel;
        }

        public Map<Long, GuildMember> getGuildMembers() {
            return this.guildMembers;
        }

        public User getOwner() {
            return this.owner;
        }

        public /* synthetic */ ThreadData(Channel channel, User user, Map map, DefaultConstructorMarker defaultConstructorMarker) {
            this(channel, user, map);
        }
    }

    /* compiled from: ThreadBrowserThreadView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/ThreadBrowserThreadView$TimestampMode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ArchivedAt", "CreatedAt", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum TimestampMode {
        ArchivedAt,
        CreatedAt
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            TimestampMode.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            TimestampMode timestampMode = TimestampMode.ArchivedAt;
            iArr[timestampMode.ordinal()] = 1;
            TimestampMode timestampMode2 = TimestampMode.CreatedAt;
            iArr[timestampMode2.ordinal()] = 2;
            TimestampMode.values();
            int[] iArr2 = new int[2];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[timestampMode.ordinal()] = 1;
            iArr2[timestampMode2.ordinal()] = 2;
        }
    }

    public ThreadBrowserThreadView(Context context) {
        this(context, null, 0, 6, null);
    }

    public ThreadBrowserThreadView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ ThreadBrowserThreadView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x0109  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureActiveThreadUI(com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.ThreadData.ActiveThread r42) {
        /*
            Method dump skipped, instructions count: 477
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView.configureActiveThreadUI(com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView$ThreadData$ActiveThread):void");
    }

    private final void configureArchivedThreadUI(ThreadData.ArchivedThread archivedThread) {
        long j;
        int i;
        CharSequence b2;
        ThreadMetadata y2 = archivedThread.getChannel().y();
        if (y2 != null) {
            int ordinal = archivedThread.getTimestampMode().ordinal();
            if (ordinal == 0) {
                j = TimeUtils.parseUTCDate(y2.a());
            } else if (ordinal == 1) {
                j = (archivedThread.getChannel().h() >>> 22) + SnowflakeUtils.DISCORD_EPOCH;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            int ordinal2 = archivedThread.getTimestampMode().ordinal();
            if (ordinal2 == 0) {
                i = R.string.thread_browser_archive_time;
            } else if (ordinal2 == 1) {
                i = R.string.thread_browser_creation_time;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            TextView textView = this.binding.f;
            m.checkNotNullExpressionValue(textView, "binding.threadTimestamp");
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            Long l = null;
            b2 = b.a.k.b.b(context, i, new Object[]{formatDateTimestamp(j)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            textView.setText(b2);
            User owner = archivedThread.getOwner();
            Map<Long, GuildMember> guildMembers = archivedThread.getGuildMembers();
            User owner2 = archivedThread.getOwner();
            if (owner2 != null) {
                l = Long.valueOf(owner2.getId());
            }
            configureAvatar(owner, guildMembers.get(l));
        }
    }

    private final void configureAvatar(User user, GuildMember guildMember) {
        if (user == null) {
            ImageView imageView = this.binding.c;
            m.checkNotNullExpressionValue(imageView, "binding.threadIcon");
            imageView.setVisibility(0);
            SimpleDraweeView simpleDraweeView = this.binding.f2144b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.threadAvatar");
            simpleDraweeView.setVisibility(8);
            return;
        }
        ImageView imageView2 = this.binding.c;
        m.checkNotNullExpressionValue(imageView2, "binding.threadIcon");
        imageView2.setVisibility(8);
        SimpleDraweeView simpleDraweeView2 = this.binding.f2144b;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.threadAvatar");
        simpleDraweeView2.setVisibility(0);
        SimpleDraweeView simpleDraweeView3 = this.binding.f2144b;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.threadAvatar");
        IconUtils.setIcon$default(simpleDraweeView3, user, R.dimen.avatar_size_small, null, null, guildMember, 24, null);
    }

    public static /* synthetic */ void configureAvatar$default(ThreadBrowserThreadView threadBrowserThreadView, User user, GuildMember guildMember, int i, Object obj) {
        if ((i & 2) != 0) {
            guildMember = null;
        }
        threadBrowserThreadView.configureAvatar(user, guildMember);
    }

    private final CharSequence formatActivityTimestamp(long j) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        long currentTimeMillis = ClockFactory.get().currentTimeMillis() - j;
        if (currentTimeMillis < 60000) {
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            b5 = b.a.k.b.b(context, R.string.thread_browser_timestamp_minutes, new Object[]{1}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b5;
        } else if (currentTimeMillis < 3600000) {
            Context context2 = getContext();
            m.checkNotNullExpressionValue(context2, "context");
            b4 = b.a.k.b.b(context2, R.string.thread_browser_timestamp_minutes, new Object[]{Long.valueOf(currentTimeMillis / 60000)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (currentTimeMillis < 86400000) {
            Context context3 = getContext();
            m.checkNotNullExpressionValue(context3, "context");
            b3 = b.a.k.b.b(context3, R.string.thread_browser_timestamp_hours, new Object[]{Long.valueOf(currentTimeMillis / 3600000)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else if (currentTimeMillis < WidgetChatListAdapterItemGuildWelcomeKt.OLD_GUILD_AGE_THRESHOLD) {
            Context context4 = getContext();
            m.checkNotNullExpressionValue(context4, "context");
            b2 = b.a.k.b.b(context4, R.string.thread_browser_timestamp_days, new Object[]{Long.valueOf(currentTimeMillis / 86400000)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else {
            String string = getContext().getString(R.string.thread_browser_timestamp_more_than_month);
            m.checkNotNullExpressionValue(string, "context.getString(R.stri…imestamp_more_than_month)");
            return string;
        }
    }

    private final CharSequence formatDateTimestamp(long j) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        long currentTimeMillis = ClockFactory.get().currentTimeMillis() - j;
        if (currentTimeMillis < 60000) {
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            b4 = b.a.k.b.b(context, R.string.thread_browser_timestamp_minutes, new Object[]{1}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (currentTimeMillis < 3600000) {
            Context context2 = getContext();
            m.checkNotNullExpressionValue(context2, "context");
            b3 = b.a.k.b.b(context2, R.string.thread_browser_timestamp_minutes, new Object[]{Long.valueOf(currentTimeMillis / 60000)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else if (currentTimeMillis < 86400000) {
            Context context3 = getContext();
            m.checkNotNullExpressionValue(context3, "context");
            b2 = b.a.k.b.b(context3, R.string.thread_browser_timestamp_hours, new Object[]{Long.valueOf(currentTimeMillis / 3600000)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else {
            String formatDateTime = DateUtils.formatDateTime(getContext(), j, 131076);
            m.checkNotNullExpressionValue(formatDateTime, "DateUtils.formatDateTime…teUtils.FORMAT_SHOW_YEAR)");
            return formatDateTime;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<CharacterStyle> getMemberCharacterStyles(GuildMember guildMember) {
        ArrayList arrayList = new ArrayList();
        FontUtils fontUtils = FontUtils.INSTANCE;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        Typeface themedFont = fontUtils.getThemedFont(context, R.attr.font_primary_semibold);
        if (themedFont != null) {
            arrayList.add(new TypefaceSpanCompat(themedFont));
        }
        if (guildMember != null) {
            arrayList.add(new ForegroundColorSpan(GuildMember.Companion.getColor(guildMember, ColorCompat.getThemedColor(getContext(), (int) R.attr.colorHeaderPrimary))));
        }
        return arrayList;
    }

    public final void setThreadData(ThreadData threadData) {
        m.checkNotNullParameter(threadData, "threadData");
        TextView textView = this.binding.e;
        m.checkNotNullExpressionValue(textView, "binding.threadName");
        Channel channel = threadData.getChannel();
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        textView.setText(ChannelUtils.d(channel, context, false));
        User owner = threadData.getOwner();
        if (owner == null) {
            owner = new CoreUser(threadData.getChannel().q(), null, null, null, false, false, 0, null, 0, 0, null, null, 4094, null);
        }
        GuildMember guildMember = (GuildMember) a.e(owner, threadData.getGuildMembers());
        String nickOrUsername$default = GuildMember.Companion.getNickOrUsername$default(GuildMember.Companion, owner, guildMember, threadData.getChannel(), null, 8, null);
        SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.threadMessage");
        b.a.k.b.m(simpleDraweeSpanTextView, R.string.thread_browser_started_by, new Object[0], new ThreadBrowserThreadView$setThreadData$1(this, nickOrUsername$default, guildMember));
        if (threadData instanceof ThreadData.ActiveThread) {
            configureActiveThreadUI((ThreadData.ActiveThread) threadData);
        } else if (threadData instanceof ThreadData.ArchivedThread) {
            configureArchivedThreadUI((ThreadData.ArchivedThread) threadData);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ThreadBrowserThreadView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.thread_browser_thread_view, this);
        int i2 = R.id.thread_avatar;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.thread_avatar);
        if (simpleDraweeView != null) {
            i2 = R.id.thread_icon;
            ImageView imageView = (ImageView) findViewById(R.id.thread_icon);
            if (imageView != null) {
                i2 = R.id.thread_image;
                FrameLayout frameLayout = (FrameLayout) findViewById(R.id.thread_image);
                if (frameLayout != null) {
                    i2 = R.id.thread_message;
                    SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) findViewById(R.id.thread_message);
                    if (simpleDraweeSpanTextView != null) {
                        i2 = R.id.thread_name;
                        TextView textView = (TextView) findViewById(R.id.thread_name);
                        if (textView != null) {
                            i2 = R.id.thread_timestamp;
                            TextView textView2 = (TextView) findViewById(R.id.thread_timestamp);
                            if (textView2 != null) {
                                i2 = R.id.thread_timestamp_separator;
                                TextView textView3 = (TextView) findViewById(R.id.thread_timestamp_separator);
                                if (textView3 != null) {
                                    ThreadBrowserThreadViewBinding threadBrowserThreadViewBinding = new ThreadBrowserThreadViewBinding(this, simpleDraweeView, imageView, frameLayout, simpleDraweeSpanTextView, textView, textView2, textView3);
                                    m.checkNotNullExpressionValue(threadBrowserThreadViewBinding, "ThreadBrowserThreadViewB…ater.from(context), this)");
                                    this.binding = threadBrowserThreadViewBinding;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i2)));
    }
}
