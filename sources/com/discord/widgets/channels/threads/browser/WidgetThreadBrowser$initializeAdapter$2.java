package com.discord.widgets.channels.threads.browser;

import androidx.fragment.app.Fragment;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserArchived;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetThreadBrowser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Landroidx/fragment/app/Fragment;", "invoke", "()Landroidx/fragment/app/Fragment;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadBrowser$initializeAdapter$2 extends o implements Function0<Fragment> {
    public final /* synthetic */ WidgetThreadBrowser this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetThreadBrowser$initializeAdapter$2(WidgetThreadBrowser widgetThreadBrowser) {
        super(0);
        this.this$0 = widgetThreadBrowser;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Fragment invoke() {
        WidgetThreadBrowserViewModel viewModel;
        WidgetThreadBrowserViewModel viewModel2;
        WidgetThreadBrowserArchived.Companion companion = WidgetThreadBrowserArchived.Companion;
        viewModel = this.this$0.getViewModel();
        long guildId = viewModel.getGuildId();
        viewModel2 = this.this$0.getViewModel();
        return companion.create(guildId, viewModel2.getChannelId());
    }
}
