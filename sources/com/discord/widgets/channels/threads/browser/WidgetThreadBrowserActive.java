package com.discord.widgets.channels.threads.browser;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.e0;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetThreadBrowserActiveBinding;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActiveViewModel;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetThreadBrowserActive.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 ,2\u00020\u0001:\u0001,B\u0007¢\u0006\u0004\b+\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0017\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u000f\u001a\u0004\b\u0015\u0010\u0016R!\u0010\u001d\u001a\u00060\u0018j\u0002`\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u000f\u001a\u0004\b\u001b\u0010\u001cR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b%\u0010&R!\u0010*\u001a\u00060\u0018j\u0002`'8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b(\u0010\u000f\u001a\u0004\b)\u0010\u001c¨\u0006-"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActive;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$ViewState;", "viewState", "", "updateView", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel$ViewState;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "()V", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActiveViewModel;", "viewModel", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel;", "browserViewModel$delegate", "getBrowserViewModel", "()Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel;", "browserViewModel", "", "Lcom/discord/primitives/GuildId;", "guildId$delegate", "getGuildId", "()J", "guildId", "Lcom/discord/databinding/WidgetThreadBrowserActiveBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetThreadBrowserActiveBinding;", "binding", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;", "adapter", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter;", "Lcom/discord/primitives/ChannelId;", "channelId$delegate", "getChannelId", "channelId", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadBrowserActive extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetThreadBrowserActive.class, "binding", "getBinding()Lcom/discord/databinding/WidgetThreadBrowserActiveBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private WidgetThreadBrowserAdapter adapter;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetThreadBrowserActive$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetThreadBrowserActive$guildId$2(this));
    private final Lazy channelId$delegate = g.lazy(new WidgetThreadBrowserActive$channelId$2(this));
    private final Lazy browserViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetThreadBrowserViewModel.class), new WidgetThreadBrowserActive$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(new WidgetThreadBrowserActive$browserViewModel$2(this)));

    /* compiled from: WidgetThreadBrowserActive.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ%\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActive$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActive;", "create", "(JJ)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserActive;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final WidgetThreadBrowserActive create(long j, long j2) {
            WidgetThreadBrowserActive widgetThreadBrowserActive = new WidgetThreadBrowserActive();
            Bundle I = a.I("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            I.putLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", j2);
            widgetThreadBrowserActive.setArguments(I);
            return widgetThreadBrowserActive;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetThreadBrowserActive() {
        super(R.layout.widget_thread_browser_active);
        WidgetThreadBrowserActive$viewModel$2 widgetThreadBrowserActive$viewModel$2 = new WidgetThreadBrowserActive$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetThreadBrowserActiveViewModel.class), new WidgetThreadBrowserActive$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetThreadBrowserActive$viewModel$2));
    }

    private final WidgetThreadBrowserActiveBinding getBinding() {
        return (WidgetThreadBrowserActiveBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final WidgetThreadBrowserViewModel getBrowserViewModel() {
        return (WidgetThreadBrowserViewModel) this.browserViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getChannelId() {
        return ((Number) this.channelId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    private final WidgetThreadBrowserActiveViewModel getViewModel() {
        return (WidgetThreadBrowserActiveViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateView(WidgetThreadBrowserActiveViewModel.ViewState viewState) {
        WidgetThreadBrowserAdapter widgetThreadBrowserAdapter = this.adapter;
        if (widgetThreadBrowserAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetThreadBrowserAdapter.setData(viewState.getListItems());
        TextView textView = getBinding().f2641b.d;
        m.checkNotNullExpressionValue(textView, "binding.emptyView.threadBrowserEmptyHeading");
        textView.setText(requireContext().getString(R.string.thread_browser_empty_state_active_header));
        ConstraintLayout constraintLayout = getBinding().f2641b.f180b;
        m.checkNotNullExpressionValue(constraintLayout, "binding.emptyView.threadBrowserEmpty");
        int i = 0;
        constraintLayout.setVisibility(viewState.getListItems().isEmpty() ? 0 : 8);
        MaterialButton materialButton = getBinding().f2641b.c;
        m.checkNotNullExpressionValue(materialButton, "binding.emptyView.threadBrowserEmptyCreateButton");
        if (!viewState.getCanCreateThread()) {
            i = 8;
        }
        materialButton.setVisibility(i);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetThreadBrowserActive.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadBrowserActive$onResume$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.recyclerView");
        this.adapter = (WidgetThreadBrowserAdapter) companion.configure(new WidgetThreadBrowserAdapter(recyclerView, new WidgetThreadBrowserActive$onViewBound$1(getBrowserViewModel()), new WidgetThreadBrowserActive$onViewBound$2(this)));
        getBinding().f2641b.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserActive$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                long guildId;
                long channelId;
                ChannelSelector companion2 = ChannelSelector.Companion.getInstance();
                guildId = WidgetThreadBrowserActive.this.getGuildId();
                channelId = WidgetThreadBrowserActive.this.getChannelId();
                ChannelSelector.openCreateThread$default(companion2, guildId, channelId, null, "Thread Browser Empty State", 4, null);
                WidgetThreadBrowserActive.this.requireAppActivity().finish();
            }
        });
    }
}
