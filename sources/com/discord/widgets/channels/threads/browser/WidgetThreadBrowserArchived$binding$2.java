package com.discord.widgets.channels.threads.browser;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.q1;
import com.discord.databinding.WidgetThreadBrowserArchivedBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetThreadBrowserArchived.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetThreadBrowserArchivedBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetThreadBrowserArchivedBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetThreadBrowserArchived$binding$2 extends k implements Function1<View, WidgetThreadBrowserArchivedBinding> {
    public static final WidgetThreadBrowserArchived$binding$2 INSTANCE = new WidgetThreadBrowserArchived$binding$2();

    public WidgetThreadBrowserArchived$binding$2() {
        super(1, WidgetThreadBrowserArchivedBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetThreadBrowserArchivedBinding;", 0);
    }

    public final WidgetThreadBrowserArchivedBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.empty_view;
        View findViewById = view.findViewById(R.id.empty_view);
        if (findViewById != null) {
            q1 a = q1.a(findViewById);
            i = R.id.recycler_view;
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            if (recyclerView != null) {
                i = R.id.thread_browser_error_msg;
                TextView textView = (TextView) view.findViewById(R.id.thread_browser_error_msg);
                if (textView != null) {
                    i = R.id.thread_browser_error_overlay;
                    ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.thread_browser_error_overlay);
                    if (constraintLayout != null) {
                        i = R.id.thread_browser_try_again;
                        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.thread_browser_try_again);
                        if (materialButton != null) {
                            return new WidgetThreadBrowserArchivedBinding((ConstraintLayout) view, a, recyclerView, textView, constraintLayout, materialButton);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
