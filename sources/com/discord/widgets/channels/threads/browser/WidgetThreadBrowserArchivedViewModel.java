package com.discord.widgets.channels.threads.browser;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.ArchivedThreadsStore;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildMemberRequester;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserAdapter;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserArchivedViewModel;
import d0.t.h0;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func4;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetThreadBrowserArchivedViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 72\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005789:;B[\u0012\n\u0010\u001b\u001a\u00060\u0019j\u0002`\u001a\u0012\n\u0010!\u001a\u00060\u0019j\u0002` \u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u00103\u001a\u000202\u0012\b\b\u0002\u0010)\u001a\u00020(\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d\u0012\b\b\u0002\u0010&\u001a\u00020%¢\u0006\u0004\b5\u00106J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0011\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\fH\u0007¢\u0006\u0004\b\u0011\u0010\u000fJ\r\u0010\u0012\u001a\u00020\u0005¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u001a\u0010\u001b\u001a\u00060\u0019j\u0002`\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001a\u0010!\u001a\u00060\u0019j\u0002` 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\u001cR\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u001c\u0010-\u001a\b\u0012\u0004\u0012\u00020,0+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u001c\u00100\u001a\b\u0012\u0004\u0012\u00020\u00030/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u0006<"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewState;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$StoreState;)V", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;", "visibility", "onVisibilityChanged", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;)V", "", "isModeratorMode", "onModeratorModeChanged", "(Z)V", "force", "maybeLoadMore", "trackTabChanged", "()V", "canLoadMore", "Z", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreGuildMemberRequester;", "storeGuildMemberRequester", "Lcom/discord/stores/StoreGuildMemberRequester;", "Lcom/discord/stores/ArchivedThreadsStore;", "storeArchivedThreads", "Lcom/discord/stores/ArchivedThreadsStore;", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "viewModeSubject", "Lrx/subjects/BehaviorSubject;", "Lrx/Observable;", "storeStateObservable", "Lrx/Observable;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(JJLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/ArchivedThreadsStore;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuildMemberRequester;)V", "Companion", "StoreState", "ViewMode", "ViewState", "VisibilityMode", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadBrowserArchivedViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private boolean canLoadMore;
    private final long channelId;
    private final long guildId;
    private final ArchivedThreadsStore storeArchivedThreads;
    private final StoreChannels storeChannels;
    private final StoreGuildMemberRequester storeGuildMemberRequester;
    private final StoreGuilds storeGuilds;
    private final StorePermissions storePermissions;
    private final Observable<StoreState> storeStateObservable;
    private final StoreUser storeUser;
    private final BehaviorSubject<ViewMode> viewModeSubject;

    /* compiled from: WidgetThreadBrowserArchivedViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserArchivedViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetThreadBrowserArchivedViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetThreadBrowserArchivedViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018J]\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$Companion;", "", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "viewMode", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/ArchivedThreadsStore;", "storeArchivedThreads", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lrx/Observable;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$StoreState;", "observeStoreState", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;JJLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/ArchivedThreadsStore;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(final ViewMode viewMode, final long j, long j2, final StoreGuilds storeGuilds, StoreChannels storeChannels, final StoreUser storeUser, ArchivedThreadsStore archivedThreadsStore, final StorePermissions storePermissions) {
            Observable<StoreState> Y = Observable.j(archivedThreadsStore.loadAndObserveThreadListing(j2, viewMode.getThreadListingType()), storeChannels.observeChannel(j2), WidgetThreadBrowserArchivedViewModel$Companion$observeStoreState$1.INSTANCE).Y(new b<Pair<? extends ArchivedThreadsStore.ThreadListingState, ? extends Channel>, Observable<? extends StoreState>>() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserArchivedViewModel$Companion$observeStoreState$2
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends WidgetThreadBrowserArchivedViewModel.StoreState> call(Pair<? extends ArchivedThreadsStore.ThreadListingState, ? extends Channel> pair) {
                    return call2((Pair<? extends ArchivedThreadsStore.ThreadListingState, Channel>) pair);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends WidgetThreadBrowserArchivedViewModel.StoreState> call2(Pair<? extends ArchivedThreadsStore.ThreadListingState, Channel> pair) {
                    Observable<Map<Long, User>> observable;
                    final ArchivedThreadsStore.ThreadListingState component1 = pair.component1();
                    final Channel component2 = pair.component2();
                    if (component1 instanceof ArchivedThreadsStore.ThreadListingState.Listing) {
                        StoreUser storeUser2 = StoreUser.this;
                        List<Channel> threads = ((ArchivedThreadsStore.ThreadListingState.Listing) component1).getThreads();
                        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(threads, 10));
                        for (Channel channel : threads) {
                            arrayList.add(Long.valueOf(channel.q()));
                        }
                        observable = storeUser2.observeUsers(arrayList);
                    } else {
                        observable = new k(h0.emptyMap());
                    }
                    Observable<R> F = storeGuilds.observeComputed().F(new b<Map<Long, ? extends Map<Long, ? extends GuildMember>>, Map<Long, ? extends GuildMember>>() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserArchivedViewModel$Companion$observeStoreState$2.1
                        @Override // j0.k.b
                        public /* bridge */ /* synthetic */ Map<Long, ? extends GuildMember> call(Map<Long, ? extends Map<Long, ? extends GuildMember>> map) {
                            return call2((Map<Long, ? extends Map<Long, GuildMember>>) map);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final Map<Long, GuildMember> call2(Map<Long, ? extends Map<Long, GuildMember>> map) {
                            Map<Long, GuildMember> map2 = map.get(Long.valueOf(j));
                            return map2 != null ? map2 : h0.emptyMap();
                        }
                    });
                    m.checkNotNullExpressionValue(F, "storeGuilds\n            …[guildId] ?: emptyMap() }");
                    Observable<T> q = ObservableExtensionsKt.leadingEdgeThrottle(F, 1L, TimeUnit.SECONDS).q();
                    Observable<Guild> observeGuild = storeGuilds.observeGuild(j);
                    StorePermissions storePermissions2 = storePermissions;
                    Long valueOf = component2 != null ? Long.valueOf(component2.r()) : null;
                    Objects.requireNonNull(valueOf, "null cannot be cast to non-null type com.discord.primitives.Snowflake /* = kotlin.Long */");
                    return Observable.h(q, observeGuild, observable, storePermissions2.observePermissionsForChannel(valueOf.longValue()), new Func4<Map<Long, ? extends GuildMember>, Guild, Map<Long, ? extends User>, Long, WidgetThreadBrowserArchivedViewModel.StoreState>() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserArchivedViewModel$Companion$observeStoreState$2.2
                        @Override // rx.functions.Func4
                        public /* bridge */ /* synthetic */ WidgetThreadBrowserArchivedViewModel.StoreState call(Map<Long, ? extends GuildMember> map, Guild guild, Map<Long, ? extends User> map2, Long l) {
                            return call2((Map<Long, GuildMember>) map, guild, map2, l);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final WidgetThreadBrowserArchivedViewModel.StoreState call2(Map<Long, GuildMember> map, Guild guild, Map<Long, ? extends User> map2, Long l) {
                            boolean canViewAllPrivateThreads = ThreadUtils.INSTANCE.canViewAllPrivateThreads(l);
                            WidgetThreadBrowserArchivedViewModel.ViewMode viewMode2 = viewMode;
                            m.checkNotNullExpressionValue(map, "guildMembers");
                            m.checkNotNullExpressionValue(map2, "users");
                            ArchivedThreadsStore.ThreadListingState threadListingState = component1;
                            m.checkNotNullExpressionValue(threadListingState, "listingState");
                            return new WidgetThreadBrowserArchivedViewModel.StoreState(viewMode2, map, map2, threadListingState, canViewAllPrivateThreads, component2, guild, l);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "Observable.combineLatest…            }\n          }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadBrowserArchivedViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001Bq\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0016\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0016\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u0005\u0012\u0006\u0010\u001f\u001a\u00020\r\u0012\u0006\u0010 \u001a\u00020\u0010\u0012\u0006\u0010!\u001a\u00020\u0013\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0016\u0012\u000e\u0010#\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0019¢\u0006\u0004\b=\u0010>J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0018\u0010\u001a\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u008a\u0001\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001c\u001a\u00020\u00022\u0018\b\u0002\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\u0018\b\u0002\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u00052\b\b\u0002\u0010\u001f\u001a\u00020\r2\b\b\u0002\u0010 \u001a\u00020\u00102\b\b\u0002\u0010!\u001a\u00020\u00132\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00162\u0010\b\u0002\u0010#\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0019HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010'\u001a\u00020&HÖ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010*\u001a\u00020)HÖ\u0001¢\u0006\u0004\b*\u0010+J\u001a\u0010-\u001a\u00020\u00102\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b-\u0010.R\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010/\u001a\u0004\b0\u0010\u0004R)\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b2\u0010\nR!\u0010#\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00198\u0006@\u0006¢\u0006\f\n\u0004\b#\u00103\u001a\u0004\b4\u0010\u001bR)\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b5\u0010\nR\u001b\u0010\"\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00106\u001a\u0004\b7\u0010\u0018R\u0019\u0010!\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b!\u00108\u001a\u0004\b9\u0010\u0015R\u0019\u0010\u001f\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010:\u001a\u0004\b;\u0010\u000fR\u0019\u0010 \u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b \u0010<\u001a\u0004\b \u0010\u0012¨\u0006?"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$StoreState;", "", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "component1", "()Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "component2", "()Ljava/util/Map;", "Lcom/discord/models/user/User;", "component3", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "component4", "()Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "", "component5", "()Z", "Lcom/discord/api/channel/Channel;", "component6", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component7", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/permission/PermissionBit;", "component8", "()Ljava/lang/Long;", "viewMode", "guildMembers", "users", "listingState", "isModerator", "channel", "guild", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "copy", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;ZLcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Ljava/lang/Long;)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "getViewMode", "Ljava/util/Map;", "getUsers", "Ljava/lang/Long;", "getPermissions", "getGuildMembers", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "getListingState", "Z", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;ZLcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Ljava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Channel channel;
        private final Guild guild;
        private final Map<Long, GuildMember> guildMembers;
        private final boolean isModerator;
        private final ArchivedThreadsStore.ThreadListingState listingState;
        private final Long permissions;
        private final Map<Long, User> users;
        private final ViewMode viewMode;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(ViewMode viewMode, Map<Long, GuildMember> map, Map<Long, ? extends User> map2, ArchivedThreadsStore.ThreadListingState threadListingState, boolean z2, Channel channel, Guild guild, Long l) {
            m.checkNotNullParameter(viewMode, "viewMode");
            m.checkNotNullParameter(map, "guildMembers");
            m.checkNotNullParameter(map2, "users");
            m.checkNotNullParameter(threadListingState, "listingState");
            m.checkNotNullParameter(channel, "channel");
            this.viewMode = viewMode;
            this.guildMembers = map;
            this.users = map2;
            this.listingState = threadListingState;
            this.isModerator = z2;
            this.channel = channel;
            this.guild = guild;
            this.permissions = l;
        }

        public final ViewMode component1() {
            return this.viewMode;
        }

        public final Map<Long, GuildMember> component2() {
            return this.guildMembers;
        }

        public final Map<Long, User> component3() {
            return this.users;
        }

        public final ArchivedThreadsStore.ThreadListingState component4() {
            return this.listingState;
        }

        public final boolean component5() {
            return this.isModerator;
        }

        public final Channel component6() {
            return this.channel;
        }

        public final Guild component7() {
            return this.guild;
        }

        public final Long component8() {
            return this.permissions;
        }

        public final StoreState copy(ViewMode viewMode, Map<Long, GuildMember> map, Map<Long, ? extends User> map2, ArchivedThreadsStore.ThreadListingState threadListingState, boolean z2, Channel channel, Guild guild, Long l) {
            m.checkNotNullParameter(viewMode, "viewMode");
            m.checkNotNullParameter(map, "guildMembers");
            m.checkNotNullParameter(map2, "users");
            m.checkNotNullParameter(threadListingState, "listingState");
            m.checkNotNullParameter(channel, "channel");
            return new StoreState(viewMode, map, map2, threadListingState, z2, channel, guild, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.viewMode, storeState.viewMode) && m.areEqual(this.guildMembers, storeState.guildMembers) && m.areEqual(this.users, storeState.users) && m.areEqual(this.listingState, storeState.listingState) && this.isModerator == storeState.isModerator && m.areEqual(this.channel, storeState.channel) && m.areEqual(this.guild, storeState.guild) && m.areEqual(this.permissions, storeState.permissions);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Map<Long, GuildMember> getGuildMembers() {
            return this.guildMembers;
        }

        public final ArchivedThreadsStore.ThreadListingState getListingState() {
            return this.listingState;
        }

        public final Long getPermissions() {
            return this.permissions;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public final ViewMode getViewMode() {
            return this.viewMode;
        }

        public int hashCode() {
            ViewMode viewMode = this.viewMode;
            int i = 0;
            int hashCode = (viewMode != null ? viewMode.hashCode() : 0) * 31;
            Map<Long, GuildMember> map = this.guildMembers;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, User> map2 = this.users;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            ArchivedThreadsStore.ThreadListingState threadListingState = this.listingState;
            int hashCode4 = (hashCode3 + (threadListingState != null ? threadListingState.hashCode() : 0)) * 31;
            boolean z2 = this.isModerator;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode4 + i2) * 31;
            Channel channel = this.channel;
            int hashCode5 = (i4 + (channel != null ? channel.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            int hashCode6 = (hashCode5 + (guild != null ? guild.hashCode() : 0)) * 31;
            Long l = this.permissions;
            if (l != null) {
                i = l.hashCode();
            }
            return hashCode6 + i;
        }

        public final boolean isModerator() {
            return this.isModerator;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(viewMode=");
            R.append(this.viewMode);
            R.append(", guildMembers=");
            R.append(this.guildMembers);
            R.append(", users=");
            R.append(this.users);
            R.append(", listingState=");
            R.append(this.listingState);
            R.append(", isModerator=");
            R.append(this.isModerator);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", permissions=");
            return a.F(R, this.permissions, ")");
        }
    }

    /* compiled from: WidgetThreadBrowserArchivedViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0016\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001c\u001a\u0004\b\t\u0010\u0007¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;", "component1", "()Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;", "", "component2", "()Z", "visibility", "isModeratorMode", "copy", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;Z)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "threadListingType", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "getThreadListingType", "()Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;", "getVisibility", "Z", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewMode {
        private final boolean isModeratorMode;
        private final ArchivedThreadsStore.ThreadListingType threadListingType;
        private final VisibilityMode visibility;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                VisibilityMode.values();
                int[] iArr = new int[2];
                $EnumSwitchMapping$0 = iArr;
                iArr[VisibilityMode.PublicThreads.ordinal()] = 1;
            }
        }

        public ViewMode(VisibilityMode visibilityMode, boolean z2) {
            ArchivedThreadsStore.ThreadListingType threadListingType;
            m.checkNotNullParameter(visibilityMode, "visibility");
            this.visibility = visibilityMode;
            this.isModeratorMode = z2;
            if (visibilityMode.ordinal() == 0) {
                threadListingType = ArchivedThreadsStore.ThreadListingType.ALL_ARCHIVED_PUBLIC_THREADS;
            } else if (!z2) {
                threadListingType = ArchivedThreadsStore.ThreadListingType.MY_ARCHIVED_PRIVATE_THREADS;
            } else if (z2) {
                threadListingType = ArchivedThreadsStore.ThreadListingType.ALL_ARCHIVED_PRIVATE_THREADS;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            this.threadListingType = threadListingType;
        }

        public static /* synthetic */ ViewMode copy$default(ViewMode viewMode, VisibilityMode visibilityMode, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                visibilityMode = viewMode.visibility;
            }
            if ((i & 2) != 0) {
                z2 = viewMode.isModeratorMode;
            }
            return viewMode.copy(visibilityMode, z2);
        }

        public final VisibilityMode component1() {
            return this.visibility;
        }

        public final boolean component2() {
            return this.isModeratorMode;
        }

        public final ViewMode copy(VisibilityMode visibilityMode, boolean z2) {
            m.checkNotNullParameter(visibilityMode, "visibility");
            return new ViewMode(visibilityMode, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewMode)) {
                return false;
            }
            ViewMode viewMode = (ViewMode) obj;
            return m.areEqual(this.visibility, viewMode.visibility) && this.isModeratorMode == viewMode.isModeratorMode;
        }

        public final ArchivedThreadsStore.ThreadListingType getThreadListingType() {
            return this.threadListingType;
        }

        public final VisibilityMode getVisibility() {
            return this.visibility;
        }

        public int hashCode() {
            VisibilityMode visibilityMode = this.visibility;
            int hashCode = (visibilityMode != null ? visibilityMode.hashCode() : 0) * 31;
            boolean z2 = this.isModeratorMode;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public final boolean isModeratorMode() {
            return this.isModeratorMode;
        }

        public String toString() {
            StringBuilder R = a.R("ViewMode(visibility=");
            R.append(this.visibility);
            R.append(", isModeratorMode=");
            return a.M(R, this.isModeratorMode, ")");
        }
    }

    /* compiled from: WidgetThreadBrowserArchivedViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0010\u001a\u00020\t\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u0006\u0010\u0012\u001a\u00020\t¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\r\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJH\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0010\u001a\u00020\t2\b\b\u0002\u0010\u0011\u001a\u00020\t2\b\b\u0002\u0010\u0012\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\t2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b!\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\u000bR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b\u0010\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b\u0011\u0010\u000b¨\u0006&"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewState;", "", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "component1", "()Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserAdapter$Item;", "component2", "()Ljava/util/List;", "", "component3", "()Z", "component4", "component5", "viewMode", "listItems", "isModerator", "isError", "canCreateThread", "copy", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;Ljava/util/List;ZZZ)Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getListItems", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;", "getViewMode", "Z", "getCanCreateThread", HookHelper.constructorName, "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$ViewMode;Ljava/util/List;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean canCreateThread;
        private final boolean isError;
        private final boolean isModerator;
        private final List<WidgetThreadBrowserAdapter.Item> listItems;
        private final ViewMode viewMode;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(ViewMode viewMode, List<? extends WidgetThreadBrowserAdapter.Item> list, boolean z2, boolean z3, boolean z4) {
            m.checkNotNullParameter(viewMode, "viewMode");
            m.checkNotNullParameter(list, "listItems");
            this.viewMode = viewMode;
            this.listItems = list;
            this.isModerator = z2;
            this.isError = z3;
            this.canCreateThread = z4;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, ViewMode viewMode, List list, boolean z2, boolean z3, boolean z4, int i, Object obj) {
            if ((i & 1) != 0) {
                viewMode = viewState.viewMode;
            }
            List<WidgetThreadBrowserAdapter.Item> list2 = list;
            if ((i & 2) != 0) {
                list2 = viewState.listItems;
            }
            List list3 = list2;
            if ((i & 4) != 0) {
                z2 = viewState.isModerator;
            }
            boolean z5 = z2;
            if ((i & 8) != 0) {
                z3 = viewState.isError;
            }
            boolean z6 = z3;
            if ((i & 16) != 0) {
                z4 = viewState.canCreateThread;
            }
            return viewState.copy(viewMode, list3, z5, z6, z4);
        }

        public final ViewMode component1() {
            return this.viewMode;
        }

        public final List<WidgetThreadBrowserAdapter.Item> component2() {
            return this.listItems;
        }

        public final boolean component3() {
            return this.isModerator;
        }

        public final boolean component4() {
            return this.isError;
        }

        public final boolean component5() {
            return this.canCreateThread;
        }

        public final ViewState copy(ViewMode viewMode, List<? extends WidgetThreadBrowserAdapter.Item> list, boolean z2, boolean z3, boolean z4) {
            m.checkNotNullParameter(viewMode, "viewMode");
            m.checkNotNullParameter(list, "listItems");
            return new ViewState(viewMode, list, z2, z3, z4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.viewMode, viewState.viewMode) && m.areEqual(this.listItems, viewState.listItems) && this.isModerator == viewState.isModerator && this.isError == viewState.isError && this.canCreateThread == viewState.canCreateThread;
        }

        public final boolean getCanCreateThread() {
            return this.canCreateThread;
        }

        public final List<WidgetThreadBrowserAdapter.Item> getListItems() {
            return this.listItems;
        }

        public final ViewMode getViewMode() {
            return this.viewMode;
        }

        public int hashCode() {
            ViewMode viewMode = this.viewMode;
            int i = 0;
            int hashCode = (viewMode != null ? viewMode.hashCode() : 0) * 31;
            List<WidgetThreadBrowserAdapter.Item> list = this.listItems;
            if (list != null) {
                i = list.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.isModerator;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.isError;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.canCreateThread;
            if (!z4) {
                i3 = z4 ? 1 : 0;
            }
            return i9 + i3;
        }

        public final boolean isError() {
            return this.isError;
        }

        public final boolean isModerator() {
            return this.isModerator;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(viewMode=");
            R.append(this.viewMode);
            R.append(", listItems=");
            R.append(this.listItems);
            R.append(", isModerator=");
            R.append(this.isModerator);
            R.append(", isError=");
            R.append(this.isError);
            R.append(", canCreateThread=");
            return a.M(R, this.canCreateThread, ")");
        }
    }

    /* compiled from: WidgetThreadBrowserArchivedViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel$VisibilityMode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "PublicThreads", "PrivateThreads", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum VisibilityMode {
        PublicThreads,
        PrivateThreads
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            VisibilityMode.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[VisibilityMode.PublicThreads.ordinal()] = 1;
            iArr[VisibilityMode.PrivateThreads.ordinal()] = 2;
            ArchivedThreadsStore.ThreadListingType.values();
            int[] iArr2 = new int[3];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[ArchivedThreadsStore.ThreadListingType.ALL_ARCHIVED_PUBLIC_THREADS.ordinal()] = 1;
            iArr2[ArchivedThreadsStore.ThreadListingType.MY_ARCHIVED_PRIVATE_THREADS.ordinal()] = 2;
            iArr2[ArchivedThreadsStore.ThreadListingType.ALL_ARCHIVED_PRIVATE_THREADS.ordinal()] = 3;
        }
    }

    public /* synthetic */ WidgetThreadBrowserArchivedViewModel(long j, long j2, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreUser storeUser, ArchivedThreadsStore archivedThreadsStore, StorePermissions storePermissions, StoreGuildMemberRequester storeGuildMemberRequester, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, (i & 4) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 8) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 16) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 32) != 0 ? StoreStream.Companion.getArchivedThreads() : archivedThreadsStore, (i & 64) != 0 ? StoreStream.Companion.getPermissions() : storePermissions, (i & 128) != 0 ? StoreStream.Companion.getGuildMemberRequester() : storeGuildMemberRequester);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        boolean z2;
        int i;
        ThreadBrowserThreadView.TimestampMode timestampMode;
        ViewMode viewMode = storeState.getViewMode();
        ArrayList arrayList = new ArrayList();
        boolean isModerator = storeState.isModerator();
        ArchivedThreadsStore.ThreadListingState listingState = storeState.getListingState();
        boolean z3 = true;
        if (listingState instanceof ArchivedThreadsStore.ThreadListingState.Listing) {
            Channel channel = storeState.getChannel();
            if (channel != null && channel.A() == 15) {
                arrayList.add(new WidgetThreadBrowserAdapter.Item.Warning(null, 1, null));
            }
            ArchivedThreadsStore.ThreadListingState.Listing listing = (ArchivedThreadsStore.ThreadListingState.Listing) listingState;
            if (!listing.getThreads().isEmpty()) {
                int ordinal = viewMode.getVisibility().ordinal();
                if (ordinal == 0) {
                    i = R.string.thread_browser_public_header;
                } else if (ordinal == 1) {
                    i = R.string.thread_browser_private_header;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                arrayList.add(new WidgetThreadBrowserAdapter.Item.Header("header", i, listing.getThreads().size()));
                for (Channel channel2 : listing.getThreads()) {
                    User user = storeState.getUsers().get(Long.valueOf(channel2.q()));
                    if (user == null || !storeState.getGuildMembers().containsKey(Long.valueOf(channel2.q()))) {
                        this.storeGuildMemberRequester.queueRequest(channel2.f(), channel2.q());
                    }
                    Map<Long, GuildMember> guildMembers = storeState.getGuildMembers();
                    boolean z4 = viewMode.getThreadListingType() == ArchivedThreadsStore.ThreadListingType.MY_ARCHIVED_PRIVATE_THREADS;
                    if (z4) {
                        timestampMode = ThreadBrowserThreadView.TimestampMode.CreatedAt;
                    } else if (!z4) {
                        timestampMode = ThreadBrowserThreadView.TimestampMode.ArchivedAt;
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                    arrayList.add(new WidgetThreadBrowserAdapter.Item.Thread(new ThreadBrowserThreadView.ThreadData.ArchivedThread(channel2, guildMembers, user, timestampMode)));
                }
            }
            if (listing.isLoadingMore()) {
                arrayList.add(new WidgetThreadBrowserAdapter.Item.Loading(null, 1, null));
            }
            if (listing.isLoadingMore() || !listing.getHasMore()) {
                z3 = false;
            }
            this.canLoadMore = z3;
        } else if (listingState instanceof ArchivedThreadsStore.ThreadListingState.Uninitialized) {
            arrayList.add(new WidgetThreadBrowserAdapter.Item.Loading(null, 1, null));
        } else if (listingState instanceof ArchivedThreadsStore.ThreadListingState.Error) {
            z2 = true;
            this.storeGuildMemberRequester.performQueuedRequests();
            updateViewState(new ViewState(viewMode, arrayList, isModerator, z2, ThreadUtils.INSTANCE.canCreateThread(storeState.getPermissions(), storeState.getChannel(), null, storeState.getGuild())));
        }
        z2 = false;
        this.storeGuildMemberRequester.performQueuedRequests();
        updateViewState(new ViewState(viewMode, arrayList, isModerator, z2, ThreadUtils.INSTANCE.canCreateThread(storeState.getPermissions(), storeState.getChannel(), null, storeState.getGuild())));
    }

    public static /* synthetic */ void maybeLoadMore$default(WidgetThreadBrowserArchivedViewModel widgetThreadBrowserArchivedViewModel, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        widgetThreadBrowserArchivedViewModel.maybeLoadMore(z2);
    }

    @MainThread
    public final void maybeLoadMore(boolean z2) {
        if (this.canLoadMore || z2) {
            ArchivedThreadsStore.fetchListing$default(this.storeArchivedThreads, this.channelId, this.viewModeSubject.n0().getThreadListingType(), false, 4, null);
        }
    }

    @MainThread
    public final void onModeratorModeChanged(boolean z2) {
        BehaviorSubject<ViewMode> behaviorSubject = this.viewModeSubject;
        behaviorSubject.onNext(ViewMode.copy$default(behaviorSubject.n0(), null, z2, 1, null));
        trackTabChanged();
    }

    @MainThread
    public final void onVisibilityChanged(VisibilityMode visibilityMode) {
        m.checkNotNullParameter(visibilityMode, "visibility");
        BehaviorSubject<ViewMode> behaviorSubject = this.viewModeSubject;
        behaviorSubject.onNext(ViewMode.copy$default(behaviorSubject.n0(), visibilityMode, false, 2, null));
        trackTabChanged();
    }

    public final void trackTabChanged() {
        String str;
        ViewMode n0 = this.viewModeSubject.n0();
        if (n0 != null) {
            int ordinal = n0.getThreadListingType().ordinal();
            if (ordinal == 0) {
                str = "My Private Archived Threads";
            } else if (ordinal == 1) {
                str = "Public Archived Threads";
            } else if (ordinal == 2) {
                str = "All Private Archived Threads";
            } else {
                throw new NoWhenBranchMatchedException();
            }
            StoreStream.Companion.getAnalytics().trackThreadBrowserTabChanged(this.channelId, str);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetThreadBrowserArchivedViewModel(long j, long j2, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreUser storeUser, ArchivedThreadsStore archivedThreadsStore, StorePermissions storePermissions, StoreGuildMemberRequester storeGuildMemberRequester) {
        super(null, 1, null);
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(archivedThreadsStore, "storeArchivedThreads");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(storeGuildMemberRequester, "storeGuildMemberRequester");
        this.guildId = j;
        this.channelId = j2;
        this.storeGuilds = storeGuilds;
        this.storeChannels = storeChannels;
        this.storeUser = storeUser;
        this.storeArchivedThreads = archivedThreadsStore;
        this.storePermissions = storePermissions;
        this.storeGuildMemberRequester = storeGuildMemberRequester;
        BehaviorSubject<ViewMode> l0 = BehaviorSubject.l0(new ViewMode(VisibilityMode.PublicThreads, false));
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(\n…rMode = false\n      )\n  )");
        this.viewModeSubject = l0;
        Observable Y = l0.Y(new b<ViewMode, Observable<? extends StoreState>>() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowserArchivedViewModel$storeStateObservable$1
            public final Observable<? extends WidgetThreadBrowserArchivedViewModel.StoreState> call(WidgetThreadBrowserArchivedViewModel.ViewMode viewMode) {
                long j3;
                long j4;
                StoreGuilds storeGuilds2;
                StoreChannels storeChannels2;
                StoreUser storeUser2;
                ArchivedThreadsStore archivedThreadsStore2;
                StorePermissions storePermissions2;
                Observable<? extends WidgetThreadBrowserArchivedViewModel.StoreState> observeStoreState;
                WidgetThreadBrowserArchivedViewModel.Companion companion = WidgetThreadBrowserArchivedViewModel.Companion;
                m.checkNotNullExpressionValue(viewMode, "viewMode");
                j3 = WidgetThreadBrowserArchivedViewModel.this.guildId;
                j4 = WidgetThreadBrowserArchivedViewModel.this.channelId;
                storeGuilds2 = WidgetThreadBrowserArchivedViewModel.this.storeGuilds;
                storeChannels2 = WidgetThreadBrowserArchivedViewModel.this.storeChannels;
                storeUser2 = WidgetThreadBrowserArchivedViewModel.this.storeUser;
                archivedThreadsStore2 = WidgetThreadBrowserArchivedViewModel.this.storeArchivedThreads;
                storePermissions2 = WidgetThreadBrowserArchivedViewModel.this.storePermissions;
                observeStoreState = companion.observeStoreState(viewMode, j3, j4, storeGuilds2, storeChannels2, storeUser2, archivedThreadsStore2, storePermissions2);
                return observeStoreState;
            }
        });
        m.checkNotNullExpressionValue(Y, "viewModeSubject\n        …            )\n          }");
        this.storeStateObservable = Y;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Y, this, null, 2, null), WidgetThreadBrowserArchivedViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
