package com.discord.widgets.channels.threads.browser;

import com.discord.i18n.Hook;
import com.discord.i18n.RenderContext;
import com.discord.models.member.GuildMember;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ThreadBrowserThreadView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ThreadBrowserThreadView$setThreadData$1 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ GuildMember $creatorMember;
    public final /* synthetic */ String $creatorName;
    public final /* synthetic */ ThreadBrowserThreadView this$0;

    /* compiled from: ThreadBrowserThreadView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/Hook;", "", "invoke", "(Lcom/discord/i18n/Hook;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView$setThreadData$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Hook, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Hook hook) {
            invoke2(hook);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Hook hook) {
            List memberCharacterStyles;
            m.checkNotNullParameter(hook, "$receiver");
            ThreadBrowserThreadView$setThreadData$1 threadBrowserThreadView$setThreadData$1 = ThreadBrowserThreadView$setThreadData$1.this;
            hook.f2681b = threadBrowserThreadView$setThreadData$1.$creatorName;
            List<Object> list = hook.a;
            memberCharacterStyles = threadBrowserThreadView$setThreadData$1.this$0.getMemberCharacterStyles(threadBrowserThreadView$setThreadData$1.$creatorMember);
            list.addAll(memberCharacterStyles);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ThreadBrowserThreadView$setThreadData$1(ThreadBrowserThreadView threadBrowserThreadView, String str, GuildMember guildMember) {
        super(1);
        this.this$0 = threadBrowserThreadView;
        this.$creatorName = str;
        this.$creatorMember = guildMember;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        renderContext.a("authorHook", new AnonymousClass1());
    }
}
