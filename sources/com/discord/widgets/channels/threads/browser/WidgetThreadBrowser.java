package com.discord.widgets.channels.threads.browser;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.viewpager.widget.ViewPager;
import b.a.d.e0;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetThreadBrowserBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.simple_pager.SimplePager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserFilterSheet;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowserViewModel;
import com.google.android.material.tabs.TabLayout;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetThreadBrowser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 :2\u00020\u0001:\u0001:B\u0007¢\u0006\u0004\b9\u0010\u0011J!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000e\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0016\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u001d\u0010\u001d\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u00028\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR!\u0010%\u001a\u00060 j\u0002`!8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010\u001a\u001a\u0004\b#\u0010$R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001d\u0010-\u001a\u00020)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010\u001a\u001a\u0004\b+\u0010,R\u001d\u00103\u001a\u00020.8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R\u0016\u00104\u001a\u00020\u00028\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b4\u0010\u001fR!\u00108\u001a\u00060 j\u0002`58B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b6\u0010\u001a\u001a\u0004\b7\u0010$¨\u0006;"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowser;", "Lcom/discord/app/AppFragment;", "", "selectedPage", "", "canCreateThread", "", "updateMenu", "(ILjava/lang/Boolean;)V", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel$ViewState;", "viewState", "updateView", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel$ViewState;)V", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel$ViewState$Browser;", "initializeAdapter", "(Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel$ViewState$Browser;)V", "onResume", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "previousViewState", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel$ViewState;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel;", "archivedViewModel$delegate", "Lkotlin/Lazy;", "getArchivedViewModel", "()Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserArchivedViewModel;", "archivedViewModel", "ARCHIVED_PAGE", "I", "", "Lcom/discord/primitives/GuildId;", "guildId$delegate", "getGuildId", "()J", "guildId", "Lcom/discord/utilities/channel/ChannelSelector;", "channelSelector", "Lcom/discord/utilities/channel/ChannelSelector;", "Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowserViewModel;", "viewModel", "Lcom/discord/databinding/WidgetThreadBrowserBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetThreadBrowserBinding;", "binding", "ACTIVE_PAGE", "Lcom/discord/primitives/ChannelId;", "channelId$delegate", "getChannelId", "channelId", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadBrowser extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetThreadBrowser.class, "binding", "getBinding()Lcom/discord/databinding/WidgetThreadBrowserBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final int ACTIVE_PAGE;
    private WidgetThreadBrowserViewModel.ViewState previousViewState;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetThreadBrowser$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetThreadBrowser$guildId$2(this));
    private final Lazy channelId$delegate = g.lazy(new WidgetThreadBrowser$channelId$2(this));
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetThreadBrowserViewModel.class), new WidgetThreadBrowser$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(new WidgetThreadBrowser$viewModel$2(this)));
    private final Lazy archivedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetThreadBrowserArchivedViewModel.class), new WidgetThreadBrowser$appActivityViewModels$$inlined$activityViewModels$3(this), new e0(new WidgetThreadBrowser$archivedViewModel$2(this)));
    private final ChannelSelector channelSelector = ChannelSelector.Companion.getInstance();
    private final int ARCHIVED_PAGE = 1;

    /* compiled from: WidgetThreadBrowser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ9\u0010\f\u001a\u00020\u000b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u00072\u0006\u0010\n\u001a\u00020\tH\u0007¢\u0006\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/channels/threads/browser/WidgetThreadBrowser$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "", "show", "(Landroid/content/Context;JJLjava/lang/String;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Context context, long j, long j2, String str) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            if (context != null) {
                Intent intent = new Intent();
                intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", j);
                intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", j2);
                j.d(context, WidgetThreadBrowser.class, intent);
                AnalyticsTracker.openModal("Thread Browser", str, Long.valueOf(j));
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetThreadBrowser() {
        super(R.layout.widget_thread_browser);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetThreadBrowserArchivedViewModel getArchivedViewModel() {
        return (WidgetThreadBrowserArchivedViewModel) this.archivedViewModel$delegate.getValue();
    }

    private final WidgetThreadBrowserBinding getBinding() {
        return (WidgetThreadBrowserBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getChannelId() {
        return ((Number) this.channelId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetThreadBrowserViewModel getViewModel() {
        return (WidgetThreadBrowserViewModel) this.viewModel$delegate.getValue();
    }

    private final void initializeAdapter(WidgetThreadBrowserViewModel.ViewState.Browser browser) {
        SimplePager simplePager = getBinding().c;
        m.checkNotNullExpressionValue(simplePager, "binding.threadBrowserViewPager");
        if (simplePager.getAdapter() == null) {
            ArrayList arrayList = new ArrayList();
            String string = getString(R.string.thread_browser_active);
            m.checkNotNullExpressionValue(string, "getString(R.string.thread_browser_active)");
            arrayList.add(new SimplePager.Adapter.Item(string, new WidgetThreadBrowser$initializeAdapter$1(this)));
            if (browser.getCanViewArchivedThreads()) {
                String string2 = getString(R.string.thread_browser_archived);
                m.checkNotNullExpressionValue(string2, "getString(R.string.thread_browser_archived)");
                arrayList.add(new SimplePager.Adapter.Item(string2, new WidgetThreadBrowser$initializeAdapter$2(this)));
            }
            SimplePager simplePager2 = getBinding().c;
            m.checkNotNullExpressionValue(simplePager2, "binding.threadBrowserViewPager");
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            int i = 0;
            Object[] array = arrayList.toArray(new SimplePager.Adapter.Item[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            SimplePager.Adapter.Item[] itemArr = (SimplePager.Adapter.Item[]) array;
            simplePager2.setAdapter(new SimplePager.Adapter(parentFragmentManager, (SimplePager.Adapter.Item[]) Arrays.copyOf(itemArr, itemArr.length)));
            getBinding().c.addOnPageChangeListener(new ViewPager.OnPageChangeListener() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowser$initializeAdapter$3
                @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
                public void onPageScrollStateChanged(int i2) {
                }

                @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
                public void onPageScrolled(int i2, float f, int i3) {
                }

                @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
                public void onPageSelected(int i2) {
                    WidgetThreadBrowserViewModel.ViewState viewState;
                    int i3;
                    int i4;
                    WidgetThreadBrowserArchivedViewModel archivedViewModel;
                    long channelId;
                    WidgetThreadBrowser widgetThreadBrowser = WidgetThreadBrowser.this;
                    viewState = widgetThreadBrowser.previousViewState;
                    Boolean bool = null;
                    if (!(viewState instanceof WidgetThreadBrowserViewModel.ViewState.Browser)) {
                        viewState = null;
                    }
                    WidgetThreadBrowserViewModel.ViewState.Browser browser2 = (WidgetThreadBrowserViewModel.ViewState.Browser) viewState;
                    if (browser2 != null) {
                        bool = Boolean.valueOf(browser2.getCanCreateThread());
                    }
                    widgetThreadBrowser.updateMenu(i2, bool);
                    i3 = WidgetThreadBrowser.this.ACTIVE_PAGE;
                    if (i2 == i3) {
                        StoreAnalytics analytics = StoreStream.Companion.getAnalytics();
                        channelId = WidgetThreadBrowser.this.getChannelId();
                        analytics.trackThreadBrowserTabChanged(channelId, "Active Threads");
                        return;
                    }
                    i4 = WidgetThreadBrowser.this.ARCHIVED_PAGE;
                    if (i2 == i4) {
                        archivedViewModel = WidgetThreadBrowser.this.getArchivedViewModel();
                        archivedViewModel.trackTabChanged();
                    }
                }
            });
            TabLayout tabLayout = getBinding().f2643b;
            m.checkNotNullExpressionValue(tabLayout, "binding.actionBarTabs");
            if (!browser.getCanViewArchivedThreads()) {
                i = 8;
            }
            tabLayout.setVisibility(i);
        }
    }

    public static final void show(Context context, long j, long j2, String str) {
        Companion.show(context, j, j2, str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateMenu(final int i, final Boolean bool) {
        setActionBarOptionsMenu(R.menu.menu_thread_browser, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowser$updateMenu$1
            public final void call(MenuItem menuItem, Context context) {
                long guildId;
                long channelId;
                long guildId2;
                long channelId2;
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                switch (menuItem.getItemId()) {
                    case R.id.menu_thread_browser_create_thread /* 2131364348 */:
                        ChannelSelector companion = ChannelSelector.Companion.getInstance();
                        guildId = WidgetThreadBrowser.this.getGuildId();
                        channelId = WidgetThreadBrowser.this.getChannelId();
                        ChannelSelector.openCreateThread$default(companion, guildId, channelId, null, "Thread Browser Toolbar", 4, null);
                        WidgetThreadBrowser.this.requireAppActivity().finish();
                        return;
                    case R.id.menu_thread_browser_filters /* 2131364349 */:
                        WidgetThreadBrowserFilterSheet.Companion companion2 = WidgetThreadBrowserFilterSheet.Companion;
                        FragmentManager parentFragmentManager = WidgetThreadBrowser.this.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                        guildId2 = WidgetThreadBrowser.this.getGuildId();
                        channelId2 = WidgetThreadBrowser.this.getChannelId();
                        companion2.show(parentFragmentManager, guildId2, channelId2);
                        return;
                    default:
                        return;
                }
            }
        }, new Action1<Menu>() { // from class: com.discord.widgets.channels.threads.browser.WidgetThreadBrowser$updateMenu$2
            public final void call(Menu menu) {
                int i2;
                int i3;
                MenuItem findItem = menu.findItem(R.id.menu_thread_browser_filters);
                boolean z2 = true;
                if (findItem != null) {
                    int i4 = i;
                    i3 = WidgetThreadBrowser.this.ARCHIVED_PAGE;
                    findItem.setVisible(i4 == i3);
                }
                MenuItem findItem2 = menu.findItem(R.id.menu_thread_browser_create_thread);
                if (findItem2 != null) {
                    int i5 = i;
                    i2 = WidgetThreadBrowser.this.ACTIVE_PAGE;
                    if (i5 != i2 || !m.areEqual(bool, Boolean.TRUE)) {
                        z2 = false;
                    }
                    findItem2.setVisible(z2);
                }
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateView(WidgetThreadBrowserViewModel.ViewState viewState) {
        this.previousViewState = viewState;
        if (viewState instanceof WidgetThreadBrowserViewModel.ViewState.GoToChannel) {
            ChannelSelector.selectChannel$default(this.channelSelector, ((WidgetThreadBrowserViewModel.ViewState.GoToChannel) viewState).getChannel(), null, SelectedChannelAnalyticsLocation.THREAD_BROWSER, 2, null);
            requireActivity().onBackPressed();
        } else if (viewState instanceof WidgetThreadBrowserViewModel.ViewState.Browser) {
            WidgetThreadBrowserViewModel.ViewState.Browser browser = (WidgetThreadBrowserViewModel.ViewState.Browser) viewState;
            initializeAdapter(browser);
            if (browser.getChannelName() != null && !browser.isForumChannel()) {
                StringBuilder R = a.R("#");
                R.append(browser.getChannelName());
                setActionBarSubtitle(R.toString());
            }
            if (browser.getChannelName() != null && browser.isForumChannel()) {
                StringBuilder R2 = a.R("#");
                R2.append(browser.getChannelName());
                setActionBarTitle(R2.toString());
            }
            SimplePager simplePager = getBinding().c;
            m.checkNotNullExpressionValue(simplePager, "binding.threadBrowserViewPager");
            updateMenu(simplePager.getCurrentItem(), Boolean.valueOf(browser.getCanCreateThread()));
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetThreadBrowser.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadBrowser$onResume$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.thread_browser_title);
    }
}
