package com.discord.widgets.channels.threads;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildFeature;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetThreadArchiveActionsSheetBinding;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadDraft;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.radiobutton.MaterialRadioButton;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetThreadDraftArchiveSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001f B\u0007¢\u0006\u0004\b\u001e\u0010\u0014J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\tH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u001d\u0010\u001d\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006!"}, d2 = {"Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet$Model;)V", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "draftState", "", "minutes", "setAutoArchiveDuration", "(Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;I)V", "getContentViewResId", "()I", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "onPause", "()V", "", "hasFiredAnalytics", "Z", "Lcom/discord/databinding/WidgetThreadArchiveActionsSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetThreadArchiveActionsSheetBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadDraftArchiveSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetThreadDraftArchiveSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetThreadArchiveActionsSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetThreadDraftArchiveSheet$binding$2.INSTANCE, null, 2, null);
    private boolean hasFiredAnalytics;

    /* compiled from: WidgetThreadDraftArchiveSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "show", "(Landroidx/fragment/app/FragmentManager;J)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, long j) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetThreadDraftArchiveSheet widgetThreadDraftArchiveSheet = new WidgetThreadDraftArchiveSheet();
            Bundle bundle = new Bundle();
            bundle.putLong("INTENT_EXTRA_GUILD_ID", j);
            widgetThreadDraftArchiveSheet.setArguments(bundle);
            widgetThreadDraftArchiveSheet.show(fragmentManager, WidgetThreadDraftArchiveSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadDraftArchiveSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0082\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B#\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ2\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0007R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\n¨\u0006#"}, d2 = {"Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet$Model;", "", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "component1", "()Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "draftState", "guild", "channel", "copy", "(Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "getDraftState", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Channel channel;
        private final StoreThreadDraft.ThreadDraftState draftState;
        private final Guild guild;

        /* compiled from: WidgetThreadDraftArchiveSheet.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/channels/threads/WidgetThreadDraftArchiveSheet$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable i = Observable.i(companion.getThreadDraft().observeDraftState(), companion.getGuilds().observeGuild(j), companion.getChannelsSelected().observeSelectedChannel(), WidgetThreadDraftArchiveSheet$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(i, "Observable.combineLatest…, guild, channel)\n      }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(i).q();
                m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(StoreThreadDraft.ThreadDraftState threadDraftState, Guild guild, Channel channel) {
            m.checkNotNullParameter(threadDraftState, "draftState");
            this.draftState = threadDraftState;
            this.guild = guild;
            this.channel = channel;
        }

        public static /* synthetic */ Model copy$default(Model model, StoreThreadDraft.ThreadDraftState threadDraftState, Guild guild, Channel channel, int i, Object obj) {
            if ((i & 1) != 0) {
                threadDraftState = model.draftState;
            }
            if ((i & 2) != 0) {
                guild = model.guild;
            }
            if ((i & 4) != 0) {
                channel = model.channel;
            }
            return model.copy(threadDraftState, guild, channel);
        }

        public final StoreThreadDraft.ThreadDraftState component1() {
            return this.draftState;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final Channel component3() {
            return this.channel;
        }

        public final Model copy(StoreThreadDraft.ThreadDraftState threadDraftState, Guild guild, Channel channel) {
            m.checkNotNullParameter(threadDraftState, "draftState");
            return new Model(threadDraftState, guild, channel);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.draftState, model.draftState) && m.areEqual(this.guild, model.guild) && m.areEqual(this.channel, model.channel);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final StoreThreadDraft.ThreadDraftState getDraftState() {
            return this.draftState;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public int hashCode() {
            StoreThreadDraft.ThreadDraftState threadDraftState = this.draftState;
            int i = 0;
            int hashCode = (threadDraftState != null ? threadDraftState.hashCode() : 0) * 31;
            Guild guild = this.guild;
            int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            if (channel != null) {
                i = channel.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(draftState=");
            R.append(this.draftState);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", channel=");
            R.append(this.channel);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetThreadDraftArchiveSheet() {
        super(false, 1, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        final StoreThreadDraft.ThreadDraftState draftState = model.getDraftState();
        Guild guild = model.getGuild();
        Channel channel = model.getChannel();
        Long l = null;
        Long valueOf = channel != null ? Long.valueOf(channel.h()) : null;
        Integer autoArchiveDuration = draftState.getAutoArchiveDuration();
        int intValue = autoArchiveDuration != null ? autoArchiveDuration.intValue() : 1440;
        Traits.Location location = new Traits.Location(Traits.Location.Page.GUILD_CHANNEL, Traits.Location.Section.THREAD_ARCHIVAL_DURATION_SHEET, Traits.Location.Obj.LIST_ITEM, null, null, 24, null);
        if (!this.hasFiredAnalytics) {
            AnalyticsTracker.GuildBoostUpsellType threadTypeForGuild = AnalyticsTracker.GuildBoostUpsellType.Companion.getThreadTypeForGuild(guild);
            if (!(guild == null || threadTypeForGuild == null)) {
                AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                long id2 = guild.getId();
                Channel channel2 = model.getChannel();
                if (channel2 != null) {
                    l = Long.valueOf(channel2.h());
                }
                analyticsTracker.guildBoostUpsellViewed(threadTypeForGuild, id2, l, new Traits.Location(null, Traits.Location.Section.THREAD_ARCHIVAL_DURATION_SHEET, null, null, null, 29, null));
                this.hasFiredAnalytics = true;
            }
        }
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.threads.WidgetThreadDraftArchiveSheet$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadDraftArchiveSheet.this.setAutoArchiveDuration(draftState, 60);
            }
        });
        MaterialRadioButton materialRadioButton = getBinding().e;
        m.checkNotNullExpressionValue(materialRadioButton, "binding.optionOneHourRadio");
        materialRadioButton.setChecked(intValue == 60);
        getBinding().p.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.threads.WidgetThreadDraftArchiveSheet$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadDraftArchiveSheet.this.setAutoArchiveDuration(draftState, 1440);
            }
        });
        MaterialRadioButton materialRadioButton2 = getBinding().q;
        m.checkNotNullExpressionValue(materialRadioButton2, "binding.optionTwentyFourHoursRadio");
        materialRadioButton2.setChecked(intValue == 1440);
        PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
        GuildFeature guildFeature = GuildFeature.THREE_DAY_THREAD_ARCHIVE;
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        PremiumUtils.BoostFeatureBadgeData boostFeatureBadgeDataForGuildFeature = premiumUtils.getBoostFeatureBadgeDataForGuildFeature(guild, valueOf, guildFeature, requireContext, parentFragmentManager, new WidgetThreadDraftArchiveSheet$configureUI$threeDayArchiveOption$1(this, draftState), location);
        ConstraintLayout constraintLayout = getBinding().k;
        final Function1<View, Unit> onClickListener = boostFeatureBadgeDataForGuildFeature.getOnClickListener();
        Object obj = onClickListener;
        if (onClickListener != null) {
            obj = new View.OnClickListener() { // from class: com.discord.widgets.channels.threads.WidgetThreadDraftArchiveSheet$sam$android_view_View_OnClickListener$0
                @Override // android.view.View.OnClickListener
                public final /* synthetic */ void onClick(View view) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(view), "invoke(...)");
                }
            };
        }
        constraintLayout.setOnClickListener((View.OnClickListener) obj);
        TextView textView = getBinding().n;
        m.checkNotNullExpressionValue(textView, "binding.optionThreeDaysLabel");
        textView.setText(boostFeatureBadgeDataForGuildFeature.getText());
        getBinding().l.setTextColor(boostFeatureBadgeDataForGuildFeature.getTextColor());
        ImageView imageView = getBinding().m;
        m.checkNotNullExpressionValue(imageView, "binding.optionThreeDaysIcon");
        ColorCompatKt.tintWithColor(imageView, boostFeatureBadgeDataForGuildFeature.getIconColor());
        MaterialRadioButton materialRadioButton3 = getBinding().o;
        m.checkNotNullExpressionValue(materialRadioButton3, "binding.optionThreeDaysRadio");
        materialRadioButton3.setChecked(intValue == 4320);
        GuildFeature guildFeature2 = GuildFeature.SEVEN_DAY_THREAD_ARCHIVE;
        Context requireContext2 = requireContext();
        m.checkNotNullExpressionValue(requireContext2, "requireContext()");
        FragmentManager parentFragmentManager2 = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
        PremiumUtils.BoostFeatureBadgeData boostFeatureBadgeDataForGuildFeature2 = premiumUtils.getBoostFeatureBadgeDataForGuildFeature(guild, valueOf, guildFeature2, requireContext2, parentFragmentManager2, new WidgetThreadDraftArchiveSheet$configureUI$sevenDayArchiveOption$1(this, draftState), location);
        ConstraintLayout constraintLayout2 = getBinding().f;
        final Function1<View, Unit> onClickListener2 = boostFeatureBadgeDataForGuildFeature2.getOnClickListener();
        Object obj2 = onClickListener2;
        if (onClickListener2 != null) {
            obj2 = new View.OnClickListener() { // from class: com.discord.widgets.channels.threads.WidgetThreadDraftArchiveSheet$sam$android_view_View_OnClickListener$0
                @Override // android.view.View.OnClickListener
                public final /* synthetic */ void onClick(View view) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(view), "invoke(...)");
                }
            };
        }
        constraintLayout2.setOnClickListener((View.OnClickListener) obj2);
        TextView textView2 = getBinding().i;
        m.checkNotNullExpressionValue(textView2, "binding.optionSevenDaysLabel");
        textView2.setText(boostFeatureBadgeDataForGuildFeature2.getText());
        getBinding().g.setTextColor(boostFeatureBadgeDataForGuildFeature2.getTextColor());
        ImageView imageView2 = getBinding().h;
        m.checkNotNullExpressionValue(imageView2, "binding.optionSevenDaysIcon");
        ColorCompatKt.tintWithColor(imageView2, boostFeatureBadgeDataForGuildFeature2.getIconColor());
        MaterialRadioButton materialRadioButton4 = getBinding().j;
        m.checkNotNullExpressionValue(materialRadioButton4, "binding.optionSevenDaysRadio");
        materialRadioButton4.setChecked(intValue == 10080);
    }

    private final WidgetThreadArchiveActionsSheetBinding getBinding() {
        return (WidgetThreadArchiveActionsSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setAutoArchiveDuration(StoreThreadDraft.ThreadDraftState threadDraftState, int i) {
        StoreStream.Companion.getThreadDraft().setDraftState(StoreThreadDraft.ThreadDraftState.copy$default(threadDraftState, false, Integer.valueOf(i), null, false, false, 29, null));
        dismiss();
    }

    public static final void show(FragmentManager fragmentManager, long j) {
        Companion.show(fragmentManager, j);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        super.bindSubscriptions(compositeSubscription);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getArgumentsOrDefault().getLong("INTENT_EXTRA_GUILD_ID", -1L)), this, null, 2, null), WidgetThreadDraftArchiveSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadDraftArchiveSheet$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_thread_archive_actions_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
