package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.app.AppComponent;
import com.discord.databinding.ViewGuildRoleSubscriptionChannelItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.recycler.DiffCreator;
import com.discord.widgets.channels.ChannelPickerAdapterItem;
import com.discord.widgets.channels.ChannelPickerViewHolder;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetChannelPickerAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 #2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002#$B3\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\u001a\b\u0002\u0010\u0018\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u0017¢\u0006\u0004\b!\u0010\"J\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0015\u001a\u00020\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012¢\u0006\u0004\b\u0015\u0010\u0016R(\u0010\u0018\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006%"}, d2 = {"Lcom/discord/widgets/channels/WidgetchannelPickerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/channels/ChannelPickerViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/channels/ChannelPickerViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/channels/ChannelPickerViewHolder;I)V", "getItemViewType", "(I)I", "getItemCount", "()I", "", "Lcom/discord/widgets/channels/ChannelPickerAdapterItem;", "newItems", "setItems", "(Ljava/util/List;)V", "Lcom/discord/utilities/recycler/DiffCreator;", "diffCreator", "Lcom/discord/utilities/recycler/DiffCreator;", "Lcom/discord/widgets/channels/WidgetchannelPickerAdapter$OnItemClickListener;", "listener", "Lcom/discord/widgets/channels/WidgetchannelPickerAdapter$OnItemClickListener;", "items", "Ljava/util/List;", "Lcom/discord/app/AppComponent;", "appComponent", HookHelper.constructorName, "(Lcom/discord/widgets/channels/WidgetchannelPickerAdapter$OnItemClickListener;Lcom/discord/app/AppComponent;Lcom/discord/utilities/recycler/DiffCreator;)V", "Companion", "OnItemClickListener", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetchannelPickerAdapter extends RecyclerView.Adapter<ChannelPickerViewHolder> {
    public static final Companion Companion = new Companion(null);
    private static final int VIEW_TYPE_CHANNEL_ITEM = 1;
    private static final int VIEW_TYPE_CREATE_CHANNEL = 0;
    private final DiffCreator<List<ChannelPickerAdapterItem>, ChannelPickerViewHolder> diffCreator;
    private List<? extends ChannelPickerAdapterItem> items;
    private final OnItemClickListener listener;

    /* compiled from: WidgetChannelPickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/WidgetchannelPickerAdapter$Companion;", "", "", "VIEW_TYPE_CHANNEL_ITEM", "I", "VIEW_TYPE_CREATE_CHANNEL", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelPickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H&¢\u0006\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/WidgetchannelPickerAdapter$OnItemClickListener;", "", "Lcom/discord/widgets/channels/ChannelPickerAdapterItem$ChannelItem;", "channelItem", "", "onChannelItemClick", "(Lcom/discord/widgets/channels/ChannelPickerAdapterItem$ChannelItem;)V", "onCreateChannelClick", "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface OnItemClickListener {
        void onChannelItemClick(ChannelPickerAdapterItem.ChannelItem channelItem);

        void onCreateChannelClick();
    }

    public /* synthetic */ WidgetchannelPickerAdapter(OnItemClickListener onItemClickListener, AppComponent appComponent, DiffCreator diffCreator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(onItemClickListener, appComponent, (i & 4) != 0 ? new DiffCreator(appComponent) : diffCreator);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        ChannelPickerAdapterItem channelPickerAdapterItem = this.items.get(i);
        if (channelPickerAdapterItem instanceof ChannelPickerAdapterItem.CreateChannelItem) {
            return 0;
        }
        if (channelPickerAdapterItem instanceof ChannelPickerAdapterItem.ChannelItem) {
            return 1;
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void setItems(List<? extends ChannelPickerAdapterItem> list) {
        m.checkNotNullParameter(list, "newItems");
        this.diffCreator.dispatchDiffUpdatesAsync(this, new WidgetchannelPickerAdapter$setItems$1(this), this.items, list);
    }

    public WidgetchannelPickerAdapter(OnItemClickListener onItemClickListener, AppComponent appComponent, DiffCreator<List<ChannelPickerAdapterItem>, ChannelPickerViewHolder> diffCreator) {
        m.checkNotNullParameter(onItemClickListener, "listener");
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(diffCreator, "diffCreator");
        this.listener = onItemClickListener;
        this.diffCreator = diffCreator;
        this.items = n.emptyList();
    }

    public void onBindViewHolder(ChannelPickerViewHolder channelPickerViewHolder, int i) {
        m.checkNotNullParameter(channelPickerViewHolder, "holder");
        if (channelPickerViewHolder instanceof ChannelPickerViewHolder.ChannelItemViewHolder) {
            ((ChannelPickerViewHolder.ChannelItemViewHolder) channelPickerViewHolder).configure(this.items.get(i));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public ChannelPickerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        CharSequence b2;
        m.checkNotNullParameter(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_guild_role_subscription_channel_item, viewGroup, false);
        int i2 = R.id.channel_item_name;
        TextView textView = (TextView) inflate.findViewById(R.id.channel_item_name);
        if (textView != null) {
            i2 = R.id.channel_item_selected;
            ImageView imageView = (ImageView) inflate.findViewById(R.id.channel_item_selected);
            if (imageView != null) {
                i2 = R.id.channel_item_type_icon;
                ImageView imageView2 = (ImageView) inflate.findViewById(R.id.channel_item_type_icon);
                if (imageView2 != null) {
                    ViewGuildRoleSubscriptionChannelItemBinding viewGuildRoleSubscriptionChannelItemBinding = new ViewGuildRoleSubscriptionChannelItemBinding((LinearLayout) inflate, textView, imageView, imageView2);
                    m.checkNotNullExpressionValue(viewGuildRoleSubscriptionChannelItemBinding, "ViewGuildRoleSubscriptio…rent,\n        false\n    )");
                    if (i == 0) {
                        return new ChannelPickerViewHolder.CreateChannelViewHolder(viewGuildRoleSubscriptionChannelItemBinding, new WidgetchannelPickerAdapter$onCreateViewHolder$1(this.listener));
                    }
                    if (i == 1) {
                        return new ChannelPickerViewHolder.ChannelItemViewHolder(viewGuildRoleSubscriptionChannelItemBinding, new WidgetchannelPickerAdapter$onCreateViewHolder$2(this.listener));
                    }
                    Context context = viewGroup.getContext();
                    m.checkNotNullExpressionValue(context, "parent.context");
                    b2 = b.b(context, R.string.android_unknown_view_holder, new Object[]{Integer.valueOf(i)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    throw new IllegalArgumentException(b2.toString());
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }
}
