package com.discord.widgets.channels;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.user.NsfwAllowance;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.user.MeUser;
import com.discord.panels.PanelState;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreGuildsNsfw;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserGuildSettings;
import com.discord.widgets.channels.WidgetChannelSidebarActionsViewModel;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: WidgetChannelSidebarActionsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/panels/PanelState;", "kotlin.jvm.PlatformType", "panelState", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/panels/PanelState;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1<T, R> implements b<PanelState, Observable<? extends WidgetChannelSidebarActionsViewModel.StoreState>> {
    public final /* synthetic */ StoreChannelsSelected $storeChannelsSelected;
    public final /* synthetic */ StoreGuildsNsfw $storeGuildNSFW;
    public final /* synthetic */ StoreUser $storeUser;
    public final /* synthetic */ StoreUserGuildSettings $storeUserGuildSettings;

    /* compiled from: WidgetChannelSidebarActionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0014\u0012\u000e\b\u0001\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T, R> implements b<Channel, Observable<? extends WidgetChannelSidebarActionsViewModel.StoreState>> {
        public final /* synthetic */ PanelState $panelState;

        public AnonymousClass1(PanelState panelState) {
            this.$panelState = panelState;
        }

        public final Observable<? extends WidgetChannelSidebarActionsViewModel.StoreState> call(final Channel channel) {
            Observable<? extends WidgetChannelSidebarActionsViewModel.StoreState> observable;
            if (channel == null) {
                observable = new k<>(WidgetChannelSidebarActionsViewModel.StoreState.ChannelNotFound.INSTANCE);
            } else {
                observable = Observable.j(WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1.this.$storeUserGuildSettings.observeGuildSettings(), StoreUser.observeMe$default(WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1.this.$storeUser, false, 1, null), new Func2<Map<Long, ? extends ModelNotificationSettings>, MeUser, WidgetChannelSidebarActionsViewModel.StoreState.ChannelFound>() { // from class: com.discord.widgets.channels.WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1$1$storeState$1
                    public final WidgetChannelSidebarActionsViewModel.StoreState.ChannelFound call(Map<Long, ? extends ModelNotificationSettings> map, MeUser meUser) {
                        boolean isGuildNsfwGateAgreed = WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1.this.$storeGuildNSFW.isGuildNsfwGateAgreed(channel.f());
                        Channel channel2 = channel;
                        return new WidgetChannelSidebarActionsViewModel.StoreState.ChannelFound(channel2, (ModelNotificationSettings) a.u0(channel2, map), channel.o() && (!isGuildNsfwGateAgreed || meUser.getNsfwAllowance() == NsfwAllowance.DISALLOWED));
                    }
                });
            }
            return m.areEqual(this.$panelState, PanelState.c.a) ? observable : observable.Z(1);
        }
    }

    public WidgetChannelSidebarActionsViewModel$Companion$observeStoreState$1(StoreChannelsSelected storeChannelsSelected, StoreUserGuildSettings storeUserGuildSettings, StoreUser storeUser, StoreGuildsNsfw storeGuildsNsfw) {
        this.$storeChannelsSelected = storeChannelsSelected;
        this.$storeUserGuildSettings = storeUserGuildSettings;
        this.$storeUser = storeUser;
        this.$storeGuildNSFW = storeGuildsNsfw;
    }

    public final Observable<? extends WidgetChannelSidebarActionsViewModel.StoreState> call(PanelState panelState) {
        return (Observable<R>) this.$storeChannelsSelected.observeSelectedChannel().Y(new AnonymousClass1(panelState));
    }
}
