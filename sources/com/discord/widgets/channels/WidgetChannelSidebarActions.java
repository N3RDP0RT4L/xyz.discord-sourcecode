package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import androidx.core.view.ViewKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelSidebarActionsBinding;
import com.discord.utilities.device.DeviceUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.channelsidebar.GuildChannelSideBarActionsView;
import com.discord.views.channelsidebar.PrivateChannelSideBarActionsView;
import com.discord.widgets.channels.WidgetChannelSidebarActionsViewModel;
import com.discord.widgets.search.WidgetSearch;
import com.discord.widgets.settings.WidgetMuteSettingsSheet;
import com.discord.widgets.user.calls.PrivateCallLauncher;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetChannelSidebarActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u001c\u0010\u000fJ#\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0015\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001b\u001a\u00020\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSidebarActions;", "Lcom/discord/app/AppFragment;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "useVideo", "", "startPrivateCall", "(JZ)V", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;", "viewState", "configureUI", "(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetChannelSidebarActionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelSidebarActionsBinding;", "binding", "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;", "viewModel", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSidebarActions extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelSidebarActions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelSidebarActionsBinding;", 0)};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelSidebarActions$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    public WidgetChannelSidebarActions() {
        super(R.layout.widget_channel_sidebar_actions);
        WidgetChannelSidebarActions$viewModel$2 widgetChannelSidebarActions$viewModel$2 = WidgetChannelSidebarActions$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetChannelSidebarActionsViewModel.class), new WidgetChannelSidebarActions$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetChannelSidebarActions$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetChannelSidebarActionsViewModel.ViewState viewState) {
        DeviceUtils deviceUtils = DeviceUtils.INSTANCE;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        boolean isSmallScreen = deviceUtils.isSmallScreen(resources);
        if (m.areEqual(viewState, WidgetChannelSidebarActionsViewModel.ViewState.Uninitialized.INSTANCE)) {
            View view = getView();
            if (view != null) {
                ViewKt.setVisible(view, false);
            }
        } else if (viewState instanceof WidgetChannelSidebarActionsViewModel.ViewState.Private) {
            View view2 = getView();
            if (view2 != null) {
                ViewKt.setVisible(view2, true);
            }
            GuildChannelSideBarActionsView guildChannelSideBarActionsView = getBinding().f2263b;
            m.checkNotNullExpressionValue(guildChannelSideBarActionsView, "binding.widgetChannelSidebarActionsGuildView");
            guildChannelSideBarActionsView.setVisibility(8);
            PrivateChannelSideBarActionsView privateChannelSideBarActionsView = getBinding().c;
            m.checkNotNullExpressionValue(privateChannelSideBarActionsView, "binding.widgetChannelSidebarActionsPrivateView");
            privateChannelSideBarActionsView.setVisibility(0);
            WidgetChannelSidebarActionsViewModel.ViewState.Private r2 = (WidgetChannelSidebarActionsViewModel.ViewState.Private) viewState;
            final long channelId = r2.getChannelId();
            final Context requireContext = requireContext();
            getBinding().c.a(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelSidebarActions$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    WidgetChannelSidebarActions.this.startPrivateCall(((WidgetChannelSidebarActionsViewModel.ViewState.Private) viewState).getChannelId(), false);
                }
            }, new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelSidebarActions$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    WidgetChannelSidebarActions.this.startPrivateCall(((WidgetChannelSidebarActionsViewModel.ViewState.Private) viewState).getChannelId(), true);
                }
            }, new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelSidebarActions$configureUI$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    WidgetMuteSettingsSheet.Companion companion = WidgetMuteSettingsSheet.Companion;
                    long j = channelId;
                    FragmentManager parentFragmentManager = WidgetChannelSidebarActions.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion.showForChannel(j, parentFragmentManager);
                }
            }, new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelSidebarActions$configureUI$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    WidgetSearch.Companion.launchForChannel(channelId, requireContext);
                }
            }, r2.isMuted());
        } else if (viewState instanceof WidgetChannelSidebarActionsViewModel.ViewState.Guild) {
            WidgetChannelSidebarActionsViewModel.ViewState.Guild guild = (WidgetChannelSidebarActionsViewModel.ViewState.Guild) viewState;
            long channelId2 = guild.getChannelId();
            Context requireContext2 = requireContext();
            getBinding().f2263b.a(new WidgetChannelSidebarActions$configureUI$5(viewState, requireContext2), new WidgetChannelSidebarActions$configureUI$6(requireContext2, viewState), new WidgetChannelSidebarActions$configureUI$8(this, channelId2), new WidgetChannelSidebarActions$configureUI$7(requireContext2, channelId2), new WidgetChannelSidebarActions$configureUI$9(viewState, channelId2, requireContext2), guild.getHasUnreadPins(), guild.isMuted(), guild.getDisablePins(), isSmallScreen);
            View view3 = getView();
            if (view3 != null) {
                ViewKt.setVisible(view3, true);
            }
            PrivateChannelSideBarActionsView privateChannelSideBarActionsView2 = getBinding().c;
            m.checkNotNullExpressionValue(privateChannelSideBarActionsView2, "binding.widgetChannelSidebarActionsPrivateView");
            privateChannelSideBarActionsView2.setVisibility(8);
            GuildChannelSideBarActionsView guildChannelSideBarActionsView2 = getBinding().f2263b;
            m.checkNotNullExpressionValue(guildChannelSideBarActionsView2, "binding.widgetChannelSidebarActionsGuildView");
            guildChannelSideBarActionsView2.setVisibility(guild.getShouldHideChannelSidebar() ^ true ? 0 : 8);
        }
    }

    private final WidgetChannelSidebarActionsBinding getBinding() {
        return (WidgetChannelSidebarActionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final WidgetChannelSidebarActionsViewModel getViewModel() {
        return (WidgetChannelSidebarActionsViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void startPrivateCall(long j, boolean z2) {
        Context requireContext = requireContext();
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        PrivateCallLauncher privateCallLauncher = new PrivateCallLauncher(this, this, requireContext, parentFragmentManager);
        if (z2) {
            privateCallLauncher.launchVideoCall(j);
        } else {
            privateCallLauncher.launchVoiceCall(j);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetChannelSidebarActions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelSidebarActions$onViewBoundOrOnResume$1(this));
    }
}
