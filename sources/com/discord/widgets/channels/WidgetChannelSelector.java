package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetChannelSelectorBinding;
import com.discord.databinding.WidgetChannelSelectorItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreStream;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.WidgetChannelSelector;
import d0.g;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetChannelSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 %2\u00020\u0001:\t&'%()*+,-B\u0007¢\u0006\u0004\b#\u0010$J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rJ!\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0019\u001a\u00020\u00148B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001f\u001a\u00020\u001a8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006."}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/api/channel/Channel;", "channel", "", "onChannelSelected", "(Lcom/discord/api/channel/Channel;)V", "", "getContentViewResId", "()I", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "", "requestCode$delegate", "Lkotlin/Lazy;", "getRequestCode", "()Ljava/lang/String;", "requestCode", "Lcom/discord/databinding/WidgetChannelSelectorBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelSelectorBinding;", "binding", "Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;", "adapter", "Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;", HookHelper.constructorName, "()V", "Companion", "Adapter", "BaseFilterFunction", "FilterFunction", "InactiveStageChannelFilterFunction", ExifInterface.TAG_MODEL, "SetFilterFunction", "TypeFilterFunction", "VocalChannelFilterFunction", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSelector extends AppBottomSheet {
    private static final String ARG_FILTER_FUNCTION = "INTENT_EXTRA_FILTER_FUNCTION";
    private static final String ARG_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String ARG_INCLUDE_NO_CHANNEL = "INTENT_EXTRA_INCLUDE_NO_CHANNEL";
    private static final String ARG_NO_CHANNEL_STRING_ID = "INTENT_EXTRA_NO_CHANNEL_STRING_ID";
    private static final String ARG_REQUEST_KEY = "INTENT_EXTRA_REQUEST_CODE";
    private static final String RESULT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private static final String RESULT_EXTRA_CHANNEL_NAME = "INTENT_EXTRA_CHANNEL_NAME";
    private Adapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelSelector$binding$2.INSTANCE, null, 2, null);
    private final Lazy requestCode$delegate = g.lazy(new WidgetChannelSelector$requestCode$2(this));
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelSelector.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelSelectorBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B\u001f\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\n\u001a\u00020\u0005¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter$ItemChannel;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter$ItemChannel;", "noChannelStringId", "I", "Lcom/discord/widgets/channels/WidgetChannelSelector;", "dialog", "Lcom/discord/widgets/channels/WidgetChannelSelector;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/widgets/channels/WidgetChannelSelector;I)V", "ItemChannel", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Adapter extends MGRecyclerAdapterSimple<Model.Item> {
        private final WidgetChannelSelector dialog;
        private final int noChannelStringId;

        /* compiled from: WidgetChannelSelector.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B!\u0012\b\b\u0001\u0010\u0011\u001a\u00020\u0004\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0004¢\u0006\u0004\b\u0013\u0010\u0014J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\n\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter$ItemChannel;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;", "Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;)V", "noChannelStringId", "I", "getNoChannelStringId", "()I", "Lcom/discord/databinding/WidgetChannelSelectorItemBinding;", "binding", "Lcom/discord/databinding/WidgetChannelSelectorItemBinding;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/WidgetChannelSelector$Adapter;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ItemChannel extends MGRecyclerViewHolder<Adapter, Model.Item> {
            private final WidgetChannelSelectorItemBinding binding;
            private final int noChannelStringId;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ItemChannel(@LayoutRes int i, Adapter adapter, int i2) {
                super(i, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                this.noChannelStringId = i2;
                View view = this.itemView;
                Objects.requireNonNull(view, "rootView");
                TextView textView = (TextView) view;
                WidgetChannelSelectorItemBinding widgetChannelSelectorItemBinding = new WidgetChannelSelectorItemBinding(textView, textView);
                m.checkNotNullExpressionValue(widgetChannelSelectorItemBinding, "WidgetChannelSelectorItemBinding.bind(itemView)");
                this.binding = widgetChannelSelectorItemBinding;
            }

            public static final /* synthetic */ Adapter access$getAdapter$p(ItemChannel itemChannel) {
                return (Adapter) itemChannel.adapter;
            }

            public final int getNoChannelStringId() {
                return this.noChannelStringId;
            }

            public void onConfigure(int i, final Model.Item item) {
                CharSequence charSequence;
                int i2;
                TextView textView;
                m.checkNotNullParameter(item, "data");
                super.onConfigure(i, (int) item);
                this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelSelector$Adapter$ItemChannel$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetChannelSelector widgetChannelSelector;
                        widgetChannelSelector = WidgetChannelSelector.Adapter.ItemChannel.access$getAdapter$p(WidgetChannelSelector.Adapter.ItemChannel.this).dialog;
                        widgetChannelSelector.onChannelSelected(item.getChannel());
                    }
                });
                TextView textView2 = this.binding.f2251b;
                m.checkNotNullExpressionValue(textView2, "binding.itemName");
                Channel channel = item.getChannel();
                if (channel != null) {
                    charSequence = ChannelUtils.c(channel);
                } else {
                    TextView textView3 = this.binding.f2251b;
                    m.checkNotNullExpressionValue(textView3, "binding.itemName");
                    charSequence = b.d(textView3, this.noChannelStringId, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                }
                textView2.setText(charSequence);
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Channel channel2 = item.getChannel();
                Integer valueOf = channel2 != null ? Integer.valueOf(channel2.A()) : null;
                if (valueOf != null && valueOf.intValue() == 2) {
                    i2 = R.attr.ic_volume_up;
                } else if (valueOf != null && valueOf.intValue() == 13) {
                    i2 = R.attr.ic_channel_stage;
                } else {
                    i2 = (valueOf != null && valueOf.intValue() == 0) ? R.attr.ic_channel_text : 0;
                }
                int themedDrawableRes$default = DrawableCompat.getThemedDrawableRes$default(view, i2, 0, 2, (Object) null);
                textView = this.binding.f2251b;
                m.checkNotNullExpressionValue(textView, "binding.itemName");
                DrawableCompat.setCompoundDrawablesCompat$default(textView, themedDrawableRes$default, 0, 0, 0, 14, (Object) null);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Adapter(RecyclerView recyclerView, WidgetChannelSelector widgetChannelSelector, int i) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recycler");
            m.checkNotNullParameter(widgetChannelSelector, "dialog");
            this.dialog = widgetChannelSelector;
            this.noChannelStringId = i;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public ItemChannel onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            return new ItemChannel(R.layout.widget_channel_selector_item, this, this.noChannelStringId);
        }
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$BaseFilterFunction;", "Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BaseFilterFunction implements FilterFunction {
        @Override // com.discord.widgets.channels.WidgetChannelSelector.FilterFunction
        public boolean includeChannel(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            return FilterFunction.DefaultImpls.includeChannel(this, channel);
        }
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b%\u0010&JE\u0010\r\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\u001c\u0010\f\u001a\u0018\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000b0\b¢\u0006\u0004\b\r\u0010\u000eJ=\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0010\u001a\u00060\tj\u0002`\u000f2\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J=\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0010\u001a\u00060\tj\u0002`\u000f2\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0016\u0010\u0015J=\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0010\u001a\u00060\tj\u0002`\u000f2\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0017\u0010\u0015J=\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0010\u001a\u00060\tj\u0002`\u000f2\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0018\u0010\u0015JI\u0010\u001b\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0010\u001a\u00060\tj\u0002`\u000f2\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u00122\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0019¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u001eR\u0016\u0010 \u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b \u0010\u001eR\u0016\u0010!\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\u001eR\u0016\u0010\"\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010\u001eR\u0016\u0010#\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010\u001eR\u0016\u0010$\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010\u001e¨\u0006'"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;", "", "Landroidx/fragment/app/Fragment;", "fragment", "", "requestKey", "", "allowNullChannel", "Lkotlin/Function2;", "", "Lcom/discord/primitives/ChannelId;", "", "onChannelSelected", "registerForResult", "(Landroidx/fragment/app/Fragment;Ljava/lang/String;ZLkotlin/jvm/functions/Function2;)V", "Lcom/discord/primitives/GuildId;", "guildId", "includeNoChannel", "", "noChannelStringId", "launchForVocal", "(Landroidx/fragment/app/Fragment;JLjava/lang/String;ZI)V", "launchForInactiveStages", "launchForVoice", "launchForText", "Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", "filterFunction", "launch", "(Landroidx/fragment/app/Fragment;JLjava/lang/String;ZILcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;)V", "ARG_FILTER_FUNCTION", "Ljava/lang/String;", "ARG_GUILD_ID", "ARG_INCLUDE_NO_CHANNEL", "ARG_NO_CHANNEL_STRING_ID", "ARG_REQUEST_KEY", "RESULT_EXTRA_CHANNEL_ID", "RESULT_EXTRA_CHANNEL_NAME", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launchForVocal$default(Companion companion, Fragment fragment, long j, String str, boolean z2, int i, int i2, Object obj) {
            companion.launchForVocal(fragment, j, str, (i2 & 8) != 0 ? false : z2, (i2 & 16) != 0 ? R.string.none : i);
        }

        public static /* synthetic */ void registerForResult$default(Companion companion, Fragment fragment, String str, boolean z2, Function2 function2, int i, Object obj) {
            if ((i & 4) != 0) {
                z2 = false;
            }
            companion.registerForResult(fragment, str, z2, function2);
        }

        public final void launch(Fragment fragment, long j, String str, boolean z2, int i, FilterFunction filterFunction) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            WidgetChannelSelector widgetChannelSelector = new WidgetChannelSelector();
            Bundle bundle = new Bundle();
            bundle.putString(WidgetChannelSelector.ARG_REQUEST_KEY, str);
            bundle.putLong("INTENT_EXTRA_GUILD_ID", j);
            bundle.putBoolean(WidgetChannelSelector.ARG_INCLUDE_NO_CHANNEL, z2);
            bundle.putInt(WidgetChannelSelector.ARG_NO_CHANNEL_STRING_ID, i);
            bundle.putSerializable(WidgetChannelSelector.ARG_FILTER_FUNCTION, filterFunction);
            widgetChannelSelector.setArguments(bundle);
            FragmentManager parentFragmentManager = fragment.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "fragment.parentFragmentManager");
            widgetChannelSelector.show(parentFragmentManager, WidgetChannelSelector.class.getName());
        }

        public final void launchForInactiveStages(Fragment fragment, long j, String str, boolean z2, int i) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            launch(fragment, j, str, z2, i, InactiveStageChannelFilterFunction.INSTANCE);
        }

        public final void launchForText(Fragment fragment, long j, String str, boolean z2, int i) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            launch(fragment, j, str, z2, i, new TypeFilterFunction(0));
        }

        public final void launchForVocal(Fragment fragment, long j, String str, boolean z2, int i) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            launch(fragment, j, str, z2, i, VocalChannelFilterFunction.INSTANCE);
        }

        public final void launchForVoice(Fragment fragment, long j, String str, boolean z2, int i) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            launch(fragment, j, str, z2, i, new TypeFilterFunction(2));
        }

        public final void registerForResult(Fragment fragment, String str, boolean z2, Function2<? super Long, ? super String, Unit> function2) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            m.checkNotNullParameter(function2, "onChannelSelected");
            FragmentKt.setFragmentResultListener(fragment, str, new WidgetChannelSelector$Companion$registerForResult$1(str, z2, function2));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", "Ljava/io/Serializable;", "Lcom/discord/api/channel/Channel;", "channel", "", "includeChannel", "(Lcom/discord/api/channel/Channel;)Z", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface FilterFunction extends Serializable {

        /* compiled from: WidgetChannelSelector.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DefaultImpls {
            public static boolean includeChannel(FilterFunction filterFunction, Channel channel) {
                m.checkNotNullParameter(channel, "channel");
                return true;
            }
        }

        boolean includeChannel(Channel channel);
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$InactiveStageChannelFilterFunction;", "Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", "Lcom/discord/api/channel/Channel;", "channel", "", "includeChannel", "(Lcom/discord/api/channel/Channel;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class InactiveStageChannelFilterFunction implements FilterFunction {
        public static final InactiveStageChannelFilterFunction INSTANCE = new InactiveStageChannelFilterFunction();

        private InactiveStageChannelFilterFunction() {
        }

        @Override // com.discord.widgets.channels.WidgetChannelSelector.FilterFunction
        public boolean includeChannel(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            return ChannelUtils.z(channel) && StoreStream.Companion.getStageInstances().getStageInstanceForChannel(channel.h()) == null;
        }
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\u0018\u0000 \u00042\u00020\u0001:\u0002\u0004\u0005B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$Model;", "", HookHelper.constructorName, "()V", "Companion", "Item", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);

        /* compiled from: WidgetChannelSelector.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ5\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "includeNoChannel", "Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", "filterFunction", "Lrx/Observable;", "", "Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;", "get", "(JZLcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<List<Item>> get(long j, final boolean z2, final FilterFunction filterFunction) {
                m.checkNotNullParameter(filterFunction, "filterFunction");
                Observable F = StoreChannels.observeChannelsForGuild$default(StoreStream.Companion.getChannels(), j, null, 2, null).F(new j0.k.b<Map<Long, ? extends Channel>, List<? extends Item>>() { // from class: com.discord.widgets.channels.WidgetChannelSelector$Model$Companion$get$1
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ List<? extends WidgetChannelSelector.Model.Item> call(Map<Long, ? extends Channel> map) {
                        return call2((Map<Long, Channel>) map);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final List<WidgetChannelSelector.Model.Item> call2(Map<Long, Channel> map) {
                        Collection<Channel> values = map.values();
                        WidgetChannelSelector.FilterFunction filterFunction2 = WidgetChannelSelector.FilterFunction.this;
                        ArrayList arrayList = new ArrayList();
                        for (T t : values) {
                            if (filterFunction2.includeChannel((Channel) t)) {
                                arrayList.add(t);
                            }
                        }
                        List listOf = z2 ? d0.t.m.listOf(new WidgetChannelSelector.Model.Item(null, 0, null, 6, null)) : n.emptyList();
                        List<Channel> sortedWith = u.sortedWith(arrayList, ChannelUtils.h(Channel.Companion));
                        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(sortedWith, 10));
                        for (Channel channel : sortedWith) {
                            arrayList2.add(new WidgetChannelSelector.Model.Item(channel, 0, null, 6, null));
                        }
                        return u.plus((Collection) listOf, (Iterable) arrayList2);
                    }
                });
                m.checkNotNullExpressionValue(F, "StoreStream.getChannels(… { Item(it) }\n          }");
                Observable<List<Item>> q = ObservableExtensionsKt.computationLatest(F).q();
                m.checkNotNullExpressionValue(q, "StoreStream.getChannels(…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetChannelSelector.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\f\u001a\u00020\u0005\u0012\b\b\u0002\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0010\u0010\nJ\u0010\u0010\u0011\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0007J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u001c\u0010\r\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\nR\u001c\u0010\f\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()I", "", "component3", "()Ljava/lang/String;", "channel", "type", "key", "copy", "(Lcom/discord/api/channel/Channel;ILjava/lang/String;)Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/lang/String;", "getKey", "I", "getType", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ILjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Item implements MGRecyclerDataPayload {
            private final Channel channel;
            private final String key;
            private final int type;

            public Item(Channel channel, int i, String str) {
                m.checkNotNullParameter(str, "key");
                this.channel = channel;
                this.type = i;
                this.key = str;
            }

            public static /* synthetic */ Item copy$default(Item item, Channel channel, int i, String str, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    channel = item.channel;
                }
                if ((i2 & 2) != 0) {
                    i = item.getType();
                }
                if ((i2 & 4) != 0) {
                    str = item.getKey();
                }
                return item.copy(channel, i, str);
            }

            public final Channel component1() {
                return this.channel;
            }

            public final int component2() {
                return getType();
            }

            public final String component3() {
                return getKey();
            }

            public final Item copy(Channel channel, int i, String str) {
                m.checkNotNullParameter(str, "key");
                return new Item(channel, i, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Item)) {
                    return false;
                }
                Item item = (Item) obj;
                return m.areEqual(this.channel, item.channel) && getType() == item.getType() && m.areEqual(getKey(), item.getKey());
            }

            public final Channel getChannel() {
                return this.channel;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int type = (getType() + ((channel != null ? channel.hashCode() : 0) * 31)) * 31;
                String key = getKey();
                if (key != null) {
                    i = key.hashCode();
                }
                return type + i;
            }

            public String toString() {
                StringBuilder R = a.R("Item(channel=");
                R.append(this.channel);
                R.append(", type=");
                R.append(getType());
                R.append(", key=");
                R.append(getKey());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public /* synthetic */ Item(com.discord.api.channel.Channel r1, int r2, java.lang.String r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
                /*
                    r0 = this;
                    r5 = r4 & 2
                    if (r5 == 0) goto Lc
                    if (r1 == 0) goto Lb
                    int r2 = r1.A()
                    goto Lc
                Lb:
                    r2 = -1
                Lc:
                    r4 = r4 & 4
                    if (r4 == 0) goto L1f
                    if (r1 == 0) goto L1d
                    long r3 = r1.h()
                    java.lang.String r3 = java.lang.String.valueOf(r3)
                    if (r3 == 0) goto L1d
                    goto L1f
                L1d:
                    java.lang.String r3 = ""
                L1f:
                    r0.<init>(r1, r2, r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.WidgetChannelSelector.Model.Item.<init>(com.discord.api.channel.Channel, int, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
            }
        }
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0010\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÂ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bJ$\u0010\r\u001a\u00020\u00002\u0012\b\u0002\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\t2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R \u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$SetFilterFunction;", "Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()Ljava/util/Set;", "Lcom/discord/api/channel/Channel;", "channel", "", "includeChannel", "(Lcom/discord/api/channel/Channel;)Z", "channelIds", "copy", "(Ljava/util/Set;)Lcom/discord/widgets/channels/WidgetChannelSelector$SetFilterFunction;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", HookHelper.constructorName, "(Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SetFilterFunction implements FilterFunction {
        private final Set<Long> channelIds;

        public SetFilterFunction(Set<Long> set) {
            m.checkNotNullParameter(set, "channelIds");
            this.channelIds = set;
        }

        private final Set<Long> component1() {
            return this.channelIds;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ SetFilterFunction copy$default(SetFilterFunction setFilterFunction, Set set, int i, Object obj) {
            if ((i & 1) != 0) {
                set = setFilterFunction.channelIds;
            }
            return setFilterFunction.copy(set);
        }

        public final SetFilterFunction copy(Set<Long> set) {
            m.checkNotNullParameter(set, "channelIds");
            return new SetFilterFunction(set);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof SetFilterFunction) && m.areEqual(this.channelIds, ((SetFilterFunction) obj).channelIds);
            }
            return true;
        }

        public int hashCode() {
            Set<Long> set = this.channelIds;
            if (set != null) {
                return set.hashCode();
            }
            return 0;
        }

        @Override // com.discord.widgets.channels.WidgetChannelSelector.FilterFunction
        public boolean includeChannel(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            return this.channelIds.contains(Long.valueOf(channel.h()));
        }

        public String toString() {
            StringBuilder R = a.R("SetFilterFunction(channelIds=");
            R.append(this.channelIds);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u001a\u0010\u0013\u001a\u00020\u00072\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\n\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$TypeFilterFunction;", "Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", "", "component1", "()I", "Lcom/discord/api/channel/Channel;", "channel", "", "includeChannel", "(Lcom/discord/api/channel/Channel;)Z", "type", "copy", "(I)Lcom/discord/widgets/channels/WidgetChannelSelector$TypeFilterFunction;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TypeFilterFunction implements FilterFunction {
        private final int type;

        public TypeFilterFunction(int i) {
            this.type = i;
        }

        private final int component1() {
            return this.type;
        }

        public static /* synthetic */ TypeFilterFunction copy$default(TypeFilterFunction typeFilterFunction, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = typeFilterFunction.type;
            }
            return typeFilterFunction.copy(i);
        }

        public final TypeFilterFunction copy(int i) {
            return new TypeFilterFunction(i);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof TypeFilterFunction) && this.type == ((TypeFilterFunction) obj).type;
            }
            return true;
        }

        public int hashCode() {
            return this.type;
        }

        @Override // com.discord.widgets.channels.WidgetChannelSelector.FilterFunction
        public boolean includeChannel(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            return this.type == channel.A();
        }

        public String toString() {
            return a.A(a.R("TypeFilterFunction(type="), this.type, ")");
        }
    }

    /* compiled from: WidgetChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelSelector$VocalChannelFilterFunction;", "Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;", "Lcom/discord/api/channel/Channel;", "channel", "", "includeChannel", "(Lcom/discord/api/channel/Channel;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class VocalChannelFilterFunction implements FilterFunction {
        public static final VocalChannelFilterFunction INSTANCE = new VocalChannelFilterFunction();

        private VocalChannelFilterFunction() {
        }

        @Override // com.discord.widgets.channels.WidgetChannelSelector.FilterFunction
        public boolean includeChannel(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            return ChannelUtils.t(channel);
        }
    }

    public WidgetChannelSelector() {
        super(false, 1, null);
    }

    public static final /* synthetic */ Adapter access$getAdapter$p(WidgetChannelSelector widgetChannelSelector) {
        Adapter adapter = widgetChannelSelector.adapter;
        if (adapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        return adapter;
    }

    private final WidgetChannelSelectorBinding getBinding() {
        return (WidgetChannelSelectorBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final String getRequestCode() {
        return (String) this.requestCode$delegate.getValue();
    }

    public final void onChannelSelected(Channel channel) {
        String requestCode = getRequestCode();
        Bundle bundle = new Bundle();
        bundle.putLong(RESULT_EXTRA_CHANNEL_ID, channel != null ? channel.h() : -1L);
        bundle.putString(RESULT_EXTRA_CHANNEL_NAME, channel != null ? ChannelUtils.c(channel) : null);
        FragmentKt.setFragmentResult(this, requestCode, bundle);
        dismiss();
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        super.bindSubscriptions(compositeSubscription);
        Model.Companion companion = Model.Companion;
        long j = getArgumentsOrDefault().getLong("INTENT_EXTRA_GUILD_ID", -1L);
        boolean z2 = getArgumentsOrDefault().getBoolean(ARG_INCLUDE_NO_CHANNEL, false);
        Serializable serializable = getArgumentsOrDefault().getSerializable(ARG_FILTER_FUNCTION);
        if (!(serializable instanceof FilterFunction)) {
            serializable = null;
        }
        FilterFunction filterFunction = (FilterFunction) serializable;
        if (filterFunction == null) {
            filterFunction = new BaseFilterFunction();
        }
        Observable<List<Model.Item>> observable = companion.get(j, z2, filterFunction);
        Adapter adapter = this.adapter;
        if (adapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(observable, this, adapter), WidgetChannelSelector.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelSelector$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_channel_selector;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2250b;
        m.checkNotNullExpressionValue(recyclerView, "binding.channelSelectorList");
        this.adapter = (Adapter) companion.configure(new Adapter(recyclerView, this, getArgumentsOrDefault().getInt(ARG_NO_CHANNEL_STRING_ID)));
    }
}
