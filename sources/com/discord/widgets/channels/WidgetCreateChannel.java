package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.analytics.generated.events.network_action.TrackNetworkActionChannelCreate;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.ViewCheckableRoleListItemThemedBinding;
import com.discord.databinding.WidgetCreateChannelBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.channel.GuildChannelIconUtilsKt;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.mg_recycler.SingleTypePayload;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stage.StageChannelUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.discord.widgets.channels.WidgetCreateChannel;
import com.discord.widgets.channels.permissions.WidgetCreateChannelAddMember;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.g0.t;
import d0.t.n;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetCreateChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 G2\u00020\u0001:\u0003GHIB\u0007¢\u0006\u0004\bF\u0010\bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0011\u0010\bJ\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0016\u0010\bR\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR!\u0010\"\u001a\u00060\u001dj\u0002`\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010\u0019\u001a\u0004\b \u0010!R\u001d\u0010(\u001a\u00020#8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001d\u0010-\u001a\u00020)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010\u0019\u001a\u0004\b+\u0010,R\u0018\u0010/\u001a\u0004\u0018\u00010.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R\u001c\u00102\u001a\u0002018\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105R\u0016\u00107\u001a\u00020)8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b6\u0010,R\u001d\u0010:\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b8\u0010\u0019\u001a\u0004\b9\u0010\u001bR\u001d\u0010=\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b;\u0010\u0019\u001a\u0004\b<\u0010\u001bR%\u0010B\u001a\n\u0018\u00010\u001dj\u0004\u0018\u0001`>8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b?\u0010\u0019\u001a\u0004\b@\u0010AR\u001d\u0010E\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bC\u0010\u0019\u001a\u0004\bD\u0010\u001b¨\u0006J"}, d2 = {"Lcom/discord/widgets/channels/WidgetCreateChannel;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/WidgetCreateChannel$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/channels/WidgetCreateChannel$Model;)V", "configurePrivateRoles", "()V", "", "Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", "getPermissionOverwrites", "()Ljava/util/List;", "Lcom/discord/api/channel/Channel;", "channel", "onChannelCreated", "(Lcom/discord/api/channel/Channel;)V", "finishActivity", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "", "disablePrivateSwitch$delegate", "Lkotlin/Lazy;", "getDisablePrivateSwitch", "()Z", "disablePrivateSwitch", "", "Lcom/discord/primitives/GuildId;", "guildId$delegate", "getGuildId", "()J", "guildId", "Lcom/discord/databinding/WidgetCreateChannelBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetCreateChannelBinding;", "binding", "", "type$delegate", "getType", "()I", "type", "Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;", "rolesAdapter", "Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "getChannelType", "channelType", "shouldChannelDefaultPrivate$delegate", "getShouldChannelDefaultPrivate", "shouldChannelDefaultPrivate", "provideResultOnly$delegate", "getProvideResultOnly", "provideResultOnly", "Lcom/discord/primitives/ChannelId;", "categoryId$delegate", "getCategoryId", "()Ljava/lang/Long;", "categoryId", "disableAnnouncementChannelType$delegate", "getDisableAnnouncementChannelType", "disableAnnouncementChannelType", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "RolesAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCreateChannel extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetCreateChannel.class, "binding", "getBinding()Lcom/discord/databinding/WidgetCreateChannelBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_CATEGORY_ID = "INTENT_CATEGORY_ID";
    private static final String INTENT_DISABLE_ANNOUNCEMENT_CHANNEL_TYPE = "INTENT_DISABLE_ANNOUNCEMENT_CHANNEL_TYPE";
    private static final String INTENT_DISABLE_PRIVATE_SWITCH = "INTENT_DISABLE_PRIVATE_SWITCH";
    private static final String INTENT_GUILD_ID = "INTENT_GUILD_ID";
    private static final String INTENT_PROVIDE_RESULT_ONLY = "INTENT_PROVIDE_RESULT_ONLY";
    private static final String INTENT_SHOULD_CHANNEL_DEFAULT_PRIVATE = "INTENT_SHOULD_CHANNEL_DEFAULT_PRIVATE";
    private static final String INTENT_TYPE = "INTENT_TYPE";
    private static final String RESULT_EXTRA_CHANNEL_ICON_RES_ID = "RESULT_EXTRA_CHANNEL_ICON_RES_ID";
    private static final String RESULT_EXTRA_CHANNEL_ID = "RESULT_EXTRA_CHANNEL_ID";
    private static final String RESULT_EXTRA_CHANNEL_NAME = "RESULT_EXTRA_CHANNEL_NAME";
    private RolesAdapter rolesAdapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetCreateChannel$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetCreateChannel$guildId$2(this));
    private final Lazy type$delegate = g.lazy(new WidgetCreateChannel$type$2(this));
    private final Lazy categoryId$delegate = g.lazy(new WidgetCreateChannel$categoryId$2(this));
    private final Lazy provideResultOnly$delegate = g.lazy(new WidgetCreateChannel$provideResultOnly$2(this));
    private final Lazy shouldChannelDefaultPrivate$delegate = g.lazy(new WidgetCreateChannel$shouldChannelDefaultPrivate$2(this));
    private final Lazy disablePrivateSwitch$delegate = g.lazy(new WidgetCreateChannel$disablePrivateSwitch$2(this));
    private final Lazy disableAnnouncementChannelType$delegate = g.lazy(new WidgetCreateChannel$disableAnnouncementChannelType$2(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetCreateChannel$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetCreateChannel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0011\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b*\u0010+J=\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\t¢\u0006\u0004\b\f\u0010\rJs\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\t2\b\b\u0002\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u0013\u001a\u00020\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u00112\b\b\u0002\u0010\u0015\u001a\u00020\u0011¢\u0006\u0004\b\u0016\u0010\u0017J?\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0019\u001a\u00020\u00182\"\u0010\u001c\u001a\u001e\u0012\b\u0012\u00060\u0004j\u0002`\t\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000b0\u001a¢\u0006\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010!\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010 R\u0016\u0010\"\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010 R\u0016\u0010#\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010 R\u0016\u0010$\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010 R\u0016\u0010%\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010 R\u0016\u0010&\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b&\u0010 R\u0016\u0010'\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b'\u0010 R\u0016\u0010(\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b(\u0010 R\u0016\u0010)\u001a\u00020\u001b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b)\u0010 ¨\u0006,"}, d2 = {"Lcom/discord/widgets/channels/WidgetCreateChannel$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "type", "Lcom/discord/primitives/ChannelId;", "categoryId", "", "show", "(Landroid/content/Context;JILjava/lang/Long;)V", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "launcher", "", "shouldChannelDefaultPrivate", "provideResultOnly", "disablePrivateSwitch", "disableAnnouncementChannelType", "launch", "(Landroid/content/Context;Landroidx/activity/result/ActivityResultLauncher;JILjava/lang/Long;ZZZZ)V", "Lcom/discord/app/AppFragment;", "fragment", "Lkotlin/Function3;", "", "callback", "registerForResult", "(Lcom/discord/app/AppFragment;Lkotlin/jvm/functions/Function3;)Landroidx/activity/result/ActivityResultLauncher;", WidgetCreateChannel.INTENT_CATEGORY_ID, "Ljava/lang/String;", WidgetCreateChannel.INTENT_DISABLE_ANNOUNCEMENT_CHANNEL_TYPE, WidgetCreateChannel.INTENT_DISABLE_PRIVATE_SWITCH, WidgetCreateChannel.INTENT_GUILD_ID, WidgetCreateChannel.INTENT_PROVIDE_RESULT_ONLY, WidgetCreateChannel.INTENT_SHOULD_CHANNEL_DEFAULT_PRIVATE, WidgetCreateChannel.INTENT_TYPE, WidgetCreateChannel.RESULT_EXTRA_CHANNEL_ICON_RES_ID, WidgetCreateChannel.RESULT_EXTRA_CHANNEL_ID, WidgetCreateChannel.RESULT_EXTRA_CHANNEL_NAME, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, Context context, long j, int i, Long l, int i2, Object obj) {
            int i3 = (i2 & 4) != 0 ? 0 : i;
            if ((i2 & 8) != 0) {
                l = null;
            }
            companion.show(context, j, i3, l);
        }

        public final void launch(Context context, ActivityResultLauncher<Intent> activityResultLauncher, long j, int i, Long l, boolean z2, boolean z3, boolean z4, boolean z5) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(activityResultLauncher, "launcher");
            j jVar = j.g;
            Intent intent = new Intent();
            intent.putExtra(WidgetCreateChannel.INTENT_GUILD_ID, j);
            intent.putExtra(WidgetCreateChannel.INTENT_TYPE, i);
            intent.putExtra(WidgetCreateChannel.INTENT_CATEGORY_ID, l);
            intent.putExtra(WidgetCreateChannel.INTENT_PROVIDE_RESULT_ONLY, z3);
            intent.putExtra(WidgetCreateChannel.INTENT_SHOULD_CHANNEL_DEFAULT_PRIVATE, z2);
            intent.putExtra(WidgetCreateChannel.INTENT_DISABLE_PRIVATE_SWITCH, z4);
            intent.putExtra(WidgetCreateChannel.INTENT_DISABLE_ANNOUNCEMENT_CHANNEL_TYPE, z5);
            jVar.f(context, activityResultLauncher, WidgetCreateChannel.class, intent);
        }

        public final ActivityResultLauncher<Intent> registerForResult(AppFragment appFragment, final Function3<? super Long, ? super String, ? super Integer, Unit> function3) {
            m.checkNotNullParameter(appFragment, "fragment");
            m.checkNotNullParameter(function3, "callback");
            ActivityResultLauncher<Intent> registerForActivityResult = appFragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.channels.WidgetCreateChannel$Companion$registerForResult$1
                public final void onActivityResult(ActivityResult activityResult) {
                    Intent data;
                    m.checkNotNullExpressionValue(activityResult, "activityResult");
                    if (activityResult.getResultCode() == -1 && (data = activityResult.getData()) != null) {
                        long longExtra = data.getLongExtra("RESULT_EXTRA_CHANNEL_ID", -1L);
                        String stringExtra = data.getStringExtra("RESULT_EXTRA_CHANNEL_NAME");
                        int intExtra = data.getIntExtra("RESULT_EXTRA_CHANNEL_ICON_RES_ID", -1);
                        if (longExtra != -1) {
                            if (!(stringExtra == null || t.isBlank(stringExtra)) && intExtra != -1) {
                                Function3.this.invoke(Long.valueOf(longExtra), stringExtra, Integer.valueOf(intExtra));
                            }
                        }
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…  }\n          }\n        }");
            return registerForActivityResult;
        }

        public final void show(Context context, long j, int i, Long l) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra(WidgetCreateChannel.INTENT_GUILD_ID, j);
            intent.putExtra(WidgetCreateChannel.INTENT_TYPE, i);
            intent.putExtra(WidgetCreateChannel.INTENT_CATEGORY_ID, l);
            j.d(context, WidgetCreateChannel.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetCreateChannel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\f\b\u0002\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B5\b\u0002\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u0010\u001a\u00020\t\u0012\u0006\u0010\u000e\u001a\u00020\t\u0012\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002¢\u0006\u0004\b\u0012\u0010\u0013R%\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\bR\u0019\u0010\n\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000b\u001a\u0004\b\u000f\u0010\rR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u000b\u001a\u0004\b\u0011\u0010\r¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/channels/WidgetCreateChannel$Model;", "", "", "Lcom/discord/utilities/mg_recycler/SingleTypePayload;", "Lcom/discord/api/role/GuildRole;", "roleItems", "Ljava/util/List;", "getRoleItems", "()Ljava/util/List;", "", "canCreate", "Z", "getCanCreate", "()Z", "canCreateCommunityChannels", "getCanCreateCommunityChannels", "canManageRoles", "getCanManageRoles", HookHelper.constructorName, "(ZZZLjava/util/List;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canCreate;
        private final boolean canCreateCommunityChannels;
        private final boolean canManageRoles;
        private final List<SingleTypePayload<GuildRole>> roleItems;

        /* compiled from: WidgetCreateChannel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ1\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0005¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/channels/WidgetCreateChannel$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "categoryId", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetCreateChannel$Model;", "get", "(JLjava/lang/Long;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j, Long l) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable g = Observable.g(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getGuilds().observeGuild(j), companion.getPermissions().observePermissionsForGuild(j), companion.getPermissions().observePermissionsForChannel(l != null ? l.longValue() : 0L), companion.getGuilds().observeSortedRoles(j), WidgetCreateChannel$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(g, "Observable\n            .…          }\n            }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(g).q();
                m.checkNotNullExpressionValue(q, "Observable\n            .…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        private Model(boolean z2, boolean z3, boolean z4, List<SingleTypePayload<GuildRole>> list) {
            this.canCreate = z2;
            this.canManageRoles = z3;
            this.canCreateCommunityChannels = z4;
            this.roleItems = list;
        }

        public final boolean getCanCreate() {
            return this.canCreate;
        }

        public final boolean getCanCreateCommunityChannels() {
            return this.canCreateCommunityChannels;
        }

        public final boolean getCanManageRoles() {
            return this.canManageRoles;
        }

        public final List<SingleTypePayload<GuildRole>> getRoleItems() {
            return this.roleItems;
        }

        public /* synthetic */ Model(boolean z2, boolean z3, boolean z4, List list, DefaultConstructorMarker defaultConstructorMarker) {
            this(z2, z3, z4, list);
        }
    }

    /* compiled from: WidgetCreateChannel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001:\u0001!B\u000f\u0012\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 J+\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\n\u0010\n\u001a\u00060\bj\u0002`\tH\u0002¢\u0006\u0004\b\f\u0010\rJ#\u0010\u0010\u001a\u00020\u000b2\u0012\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J/\u0010\u0016\u001a\u0012\u0012\u0002\b\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u00152\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0016\u0010\u0017R)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00060\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006\""}, d2 = {"Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/utilities/mg_recycler/SingleTypePayload;", "Lcom/discord/api/role/GuildRole;", "", "checked", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "Lcom/discord/primitives/RoleId;", "roleId", "", "onRoleClicked", "(ZIJ)V", "", "data", "setData", "(Ljava/util/List;)V", "Landroid/view/ViewGroup;", "parent", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "", "checkedRoles", "Ljava/util/Map;", "getCheckedRoles", "()Ljava/util/Map;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "RoleListItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RolesAdapter extends MGRecyclerAdapterSimple<SingleTypePayload<GuildRole>> {
        private final Map<Long, Integer> checkedRoles = new HashMap();

        /* compiled from: WidgetCreateChannel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0001B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J%\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0014¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter$RoleListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;", "Lcom/discord/utilities/mg_recycler/SingleTypePayload;", "Lcom/discord/api/role/GuildRole;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/SingleTypePayload;)V", "Lcom/discord/databinding/ViewCheckableRoleListItemThemedBinding;", "binding", "Lcom/discord/databinding/ViewCheckableRoleListItemThemedBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RoleListItem extends MGRecyclerViewHolder<RolesAdapter, SingleTypePayload<GuildRole>> {
            private final ViewCheckableRoleListItemThemedBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public RoleListItem(RolesAdapter rolesAdapter) {
                super((int) R.layout.view_checkable_role_list_item_themed, rolesAdapter);
                m.checkNotNullParameter(rolesAdapter, "adapter");
                View view = this.itemView;
                Objects.requireNonNull(view, "rootView");
                CheckedSetting checkedSetting = (CheckedSetting) view;
                ViewCheckableRoleListItemThemedBinding viewCheckableRoleListItemThemedBinding = new ViewCheckableRoleListItemThemedBinding(checkedSetting, checkedSetting);
                m.checkNotNullExpressionValue(viewCheckableRoleListItemThemedBinding, "ViewCheckableRoleListIte…medBinding.bind(itemView)");
                this.binding = viewCheckableRoleListItemThemedBinding;
            }

            public static final /* synthetic */ RolesAdapter access$getAdapter$p(RoleListItem roleListItem) {
                return (RolesAdapter) roleListItem.adapter;
            }

            public void onConfigure(int i, SingleTypePayload<GuildRole> singleTypePayload) {
                m.checkNotNullParameter(singleTypePayload, "data");
                super.onConfigure(i, (int) singleTypePayload);
                final GuildRole data = singleTypePayload.getData();
                boolean containsKey = ((RolesAdapter) this.adapter).getCheckedRoles().containsKey(Long.valueOf(data.getId()));
                CheckedSetting checkedSetting = this.binding.f2163b;
                m.checkNotNullExpressionValue(checkedSetting, "binding.roleItemCheckedSetting");
                checkedSetting.setChecked(containsKey);
                this.binding.f2163b.e(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetCreateChannel$RolesAdapter$RoleListItem$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ViewCheckableRoleListItemThemedBinding viewCheckableRoleListItemThemedBinding;
                        WidgetCreateChannel.RolesAdapter access$getAdapter$p = WidgetCreateChannel.RolesAdapter.RoleListItem.access$getAdapter$p(WidgetCreateChannel.RolesAdapter.RoleListItem.this);
                        viewCheckableRoleListItemThemedBinding = WidgetCreateChannel.RolesAdapter.RoleListItem.this.binding;
                        CheckedSetting checkedSetting2 = viewCheckableRoleListItemThemedBinding.f2163b;
                        m.checkNotNullExpressionValue(checkedSetting2, "binding.roleItemCheckedSetting");
                        access$getAdapter$p.onRoleClicked(!checkedSetting2.isChecked(), WidgetCreateChannel.RolesAdapter.RoleListItem.this.getAdapterPosition(), data.getId());
                    }
                });
                this.binding.f2163b.setText(data.g());
                CheckedSetting checkedSetting2 = this.binding.f2163b;
                m.checkNotNullExpressionValue(checkedSetting2, "binding.roleItemCheckedSetting");
                Context context = checkedSetting2.getContext();
                m.checkNotNullExpressionValue(context, "binding.roleItemCheckedSetting.context");
                checkedSetting2.setTextColor(RoleUtils.getRoleColor$default(data, context, null, 2, null));
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public RolesAdapter(RecyclerView recyclerView) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recycler");
        }

        public final void onRoleClicked(boolean z2, int i, long j) {
            ArrayList<Number> arrayList = new ArrayList();
            arrayList.add(Integer.valueOf(i));
            if (z2) {
                this.checkedRoles.put(Long.valueOf(j), Integer.valueOf(i));
            } else {
                this.checkedRoles.remove(Long.valueOf(j));
            }
            for (Number number : arrayList) {
                int intValue = number.intValue();
                if (intValue < getItemCount()) {
                    notifyItemChanged(intValue);
                }
            }
        }

        public final Map<Long, Integer> getCheckedRoles() {
            return this.checkedRoles;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple
        public void setData(List<? extends SingleTypePayload<GuildRole>> list) {
            m.checkNotNullParameter(list, "data");
            super.setData(list);
            this.checkedRoles.clear();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public MGRecyclerViewHolder<?, SingleTypePayload<GuildRole>> onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            return new RoleListItem(this);
        }
    }

    public WidgetCreateChannel() {
        super(R.layout.widget_create_channel);
    }

    public final void configurePrivateRoles() {
        boolean z2 = true;
        int i = 0;
        boolean z3 = getChannelType() != 13 && !getDisablePrivateSwitch();
        SwitchCompat switchCompat = getBinding().g;
        m.checkNotNullExpressionValue(switchCompat, "binding.createChannelPrivateSwitch");
        boolean isChecked = switchCompat.isChecked();
        RelativeLayout relativeLayout = getBinding().e;
        m.checkNotNullExpressionValue(relativeLayout, "binding.createChannelPrivateContainer");
        relativeLayout.setVisibility(z3 ? 0 : 8);
        View view = getBinding().j;
        m.checkNotNullExpressionValue(view, "binding.createChannelRoleDivider");
        view.setVisibility(z3 ? 0 : 8);
        TextView textView = getBinding().k;
        m.checkNotNullExpressionValue(textView, "binding.createChannelRoleHeader");
        textView.setVisibility(z3 && isChecked ? 0 : 8);
        RecyclerView recyclerView = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView, "binding.createChannelRoleAccessRecycler");
        if (!z3 || !isChecked) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        recyclerView.setVisibility(i);
    }

    public final void configureUI(Model model) {
        if (model == null || !model.getCanCreate()) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        boolean z2 = true;
        setActionBarDisplayHomeAsUpEnabled(true);
        setActionBarTitle(getType() == 4 ? R.string.create_category : R.string.create_channel);
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_channel_create, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.channels.WidgetCreateChannel$configureUI$1

            /* compiled from: WidgetCreateChannel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/api/channel/Channel;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.channels.WidgetCreateChannel$configureUI$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Channel, TrackNetworkMetadataReceiver> {
                public AnonymousClass1() {
                    super(1);
                }

                public final TrackNetworkMetadataReceiver invoke(Channel channel) {
                    long guildId;
                    List<PermissionOverwrite> s2;
                    Boolean valueOf = (channel == null || (s2 = channel.s()) == null) ? null : Boolean.valueOf(!s2.isEmpty());
                    Long valueOf2 = channel != null ? Long.valueOf(channel.A()) : null;
                    Long valueOf3 = channel != null ? Long.valueOf(channel.h()) : null;
                    Long valueOf4 = channel != null ? Long.valueOf(channel.r()) : null;
                    guildId = WidgetCreateChannel.this.getGuildId();
                    return new TrackNetworkActionChannelCreate(valueOf, valueOf2, valueOf3, valueOf4, Long.valueOf(guildId));
                }
            }

            /* compiled from: WidgetCreateChannel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "p1", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.channels.WidgetCreateChannel$configureUI$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<Channel, Unit> {
                public AnonymousClass2(WidgetCreateChannel widgetCreateChannel) {
                    super(1, widgetCreateChannel, WidgetCreateChannel.class, "onChannelCreated", "onChannelCreated(Lcom/discord/api/channel/Channel;)V", 0);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
                    invoke2(channel);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Channel channel) {
                    m.checkNotNullParameter(channel, "p1");
                    ((WidgetCreateChannel) this.receiver).onChannelCreated(channel);
                }
            }

            public final void call(MenuItem menuItem, Context context) {
                long guildId;
                int channelType;
                WidgetCreateChannelBinding binding;
                Long categoryId;
                List permissionOverwrites;
                m.checkNotNullParameter(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_sort_channel) {
                    RestAPI api = RestAPI.Companion.getApi();
                    guildId = WidgetCreateChannel.this.getGuildId();
                    channelType = WidgetCreateChannel.this.getChannelType();
                    binding = WidgetCreateChannel.this.getBinding();
                    TextInputLayout textInputLayout = binding.d;
                    m.checkNotNullExpressionValue(textInputLayout, "binding.createChannelNameLayout");
                    String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
                    categoryId = WidgetCreateChannel.this.getCategoryId();
                    permissionOverwrites = WidgetCreateChannel.this.getPermissionOverwrites();
                    ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(api.createGuildChannel(guildId, new RestAPIParams.CreateGuildChannel(channelType, null, textOrEmpty, categoryId, permissionOverwrites, null)), new AnonymousClass1()), false, 1, null), WidgetCreateChannel.this, null, 2, null), WidgetCreateChannel.this.getClass(), (r18 & 2) != 0 ? null : WidgetCreateChannel.this.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(WidgetCreateChannel.this));
                }
            }
        }, null, 4, null);
        ConstraintLayout constraintLayout = getBinding().f2338b;
        m.checkNotNullExpressionValue(constraintLayout, "binding.createChannelAnnouncementsContainer");
        int i = 0;
        constraintLayout.setVisibility(model.getCanCreateCommunityChannels() && !getDisableAnnouncementChannelType() ? 0 : 8);
        ConstraintLayout constraintLayout2 = getBinding().l;
        m.checkNotNullExpressionValue(constraintLayout2, "binding.createChannelStageContainer");
        if (!model.getCanCreateCommunityChannels() || !model.getCanManageRoles()) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        constraintLayout2.setVisibility(i);
        RolesAdapter rolesAdapter = this.rolesAdapter;
        if (rolesAdapter != null) {
            rolesAdapter.setData(model.getRoleItems());
        }
    }

    private final void finishActivity() {
        AppFragment.hideKeyboard$default(this, null, 1, null);
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.finish();
        }
    }

    public final WidgetCreateChannelBinding getBinding() {
        return (WidgetCreateChannelBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final Long getCategoryId() {
        return (Long) this.categoryId$delegate.getValue();
    }

    public final int getChannelType() {
        if (getType() == 4) {
            return 4;
        }
        RadioButton radioButton = getBinding().r;
        m.checkNotNullExpressionValue(radioButton, "binding.createChannelVoiceRadio");
        if (radioButton.isChecked()) {
            return 2;
        }
        RadioButton radioButton2 = getBinding().c;
        m.checkNotNullExpressionValue(radioButton2, "binding.createChannelAnnouncementsRadio");
        if (radioButton2.isChecked()) {
            return 5;
        }
        RadioButton radioButton3 = getBinding().m;
        m.checkNotNullExpressionValue(radioButton3, "binding.createChannelStageRadio");
        return radioButton3.isChecked() ? 13 : 0;
    }

    private final boolean getDisableAnnouncementChannelType() {
        return ((Boolean) this.disableAnnouncementChannelType$delegate.getValue()).booleanValue();
    }

    private final boolean getDisablePrivateSwitch() {
        return ((Boolean) this.disablePrivateSwitch$delegate.getValue()).booleanValue();
    }

    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x001e, code lost:
        if (r0.isChecked() == false) goto L10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<com.discord.restapi.RestAPIParams.ChannelPermissionOverwrites> getPermissionOverwrites() {
        /*
            r11 = this;
            int r0 = r11.getChannelType()
            r1 = 1
            r2 = 13
            if (r0 == r2) goto Lb
            r0 = 1
            goto Lc
        Lb:
            r0 = 0
        Lc:
            r3 = 0
            if (r0 == 0) goto L20
            com.discord.databinding.WidgetCreateChannelBinding r0 = r11.getBinding()
            androidx.appcompat.widget.SwitchCompat r0 = r0.g
            java.lang.String r4 = "binding.createChannelPrivateSwitch"
            d0.z.d.m.checkNotNullExpressionValue(r0, r4)
            boolean r0 = r0.isChecked()
            if (r0 != 0) goto L2c
        L20:
            boolean r0 = r11.getDisablePrivateSwitch()
            if (r0 == 0) goto Lb2
            boolean r0 = r11.getShouldChannelDefaultPrivate()
            if (r0 == 0) goto Lb2
        L2c:
            com.discord.widgets.channels.WidgetCreateChannel$RolesAdapter r0 = r11.rolesAdapter
            if (r0 == 0) goto L4b
            java.util.Map r0 = r0.getCheckedRoles()
            if (r0 == 0) goto L4b
            java.util.Set r0 = r0.keySet()
            if (r0 == 0) goto L4b
            long r4 = r11.getGuildId()
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            boolean r0 = r0.contains(r4)
            if (r0 != r1) goto L4b
            return r3
        L4b:
            int r0 = r11.getChannelType()
            r1 = 2
            if (r0 == r1) goto L5c
            int r0 = r11.getChannelType()
            if (r0 != r2) goto L59
            goto L5c
        L59:
            r0 = 1024(0x400, double:5.06E-321)
            goto L5f
        L5c:
            r0 = 1048576(0x100000, double:5.180654E-318)
        L5f:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            com.discord.restapi.RestAPIParams$ChannelPermissionOverwrites$Companion r3 = com.discord.restapi.RestAPIParams.ChannelPermissionOverwrites.Companion
            long r4 = r11.getGuildId()
            r6 = 0
            java.lang.Long r8 = java.lang.Long.valueOf(r6)
            java.lang.Long r9 = java.lang.Long.valueOf(r0)
            com.discord.restapi.RestAPIParams$ChannelPermissionOverwrites r3 = r3.createForRole(r4, r8, r9)
            r2.add(r3)
            com.discord.widgets.channels.WidgetCreateChannel$RolesAdapter r3 = r11.rolesAdapter
            if (r3 == 0) goto Lb1
            java.util.Map r3 = r3.getCheckedRoles()
            if (r3 == 0) goto Lb1
            java.util.Set r3 = r3.keySet()
            if (r3 == 0) goto Lb1
            java.util.Iterator r3 = r3.iterator()
        L8f:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto Lb1
            java.lang.Object r4 = r3.next()
            java.lang.Number r4 = (java.lang.Number) r4
            long r4 = r4.longValue()
            com.discord.restapi.RestAPIParams$ChannelPermissionOverwrites$Companion r8 = com.discord.restapi.RestAPIParams.ChannelPermissionOverwrites.Companion
            java.lang.Long r9 = java.lang.Long.valueOf(r0)
            java.lang.Long r10 = java.lang.Long.valueOf(r6)
            com.discord.restapi.RestAPIParams$ChannelPermissionOverwrites r4 = r8.createForRole(r4, r9, r10)
            r2.add(r4)
            goto L8f
        Lb1:
            return r2
        Lb2:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.WidgetCreateChannel.getPermissionOverwrites():java.util.List");
    }

    private final boolean getProvideResultOnly() {
        return ((Boolean) this.provideResultOnly$delegate.getValue()).booleanValue();
    }

    private final boolean getShouldChannelDefaultPrivate() {
        return ((Boolean) this.shouldChannelDefaultPrivate$delegate.getValue()).booleanValue();
    }

    private final int getType() {
        return ((Number) this.type$delegate.getValue()).intValue();
    }

    public final void onChannelCreated(Channel channel) {
        if (getProvideResultOnly()) {
            FragmentActivity requireActivity = requireActivity();
            Intent intent = new Intent();
            intent.putExtra(RESULT_EXTRA_CHANNEL_ID, channel.h());
            intent.putExtra(RESULT_EXTRA_CHANNEL_NAME, ChannelUtils.c(channel));
            intent.putExtra(RESULT_EXTRA_CHANNEL_ICON_RES_ID, GuildChannelIconUtilsKt.guildChannelIcon(channel));
            requireActivity.setResult(-1, intent);
            finishActivity();
            return;
        }
        if (channel.A() == 0) {
            ChannelSelector.selectChannel$default(ChannelSelector.Companion.getInstance(), channel, null, null, 6, null);
        }
        StoreStream.Companion companion = StoreStream.Companion;
        boolean computeCanEditStageModerators = StageChannelUtils.computeCanEditStageModerators(companion.getPermissions(), companion.getGuilds(), companion.getUsers().getMe().getId(), getGuildId(), channel);
        if (channel.A() == 13 && computeCanEditStageModerators) {
            WidgetCreateChannelAddMember.Companion.create(requireContext(), channel.h());
        }
        finishActivity();
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView, "binding.createChannelRoleAccessRecycler");
        this.rolesAdapter = (RolesAdapter) companion.configure(new RolesAdapter(recyclerView));
        boolean z2 = false;
        getBinding().i.setHasFixedSize(false);
        RecyclerView recyclerView2 = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView2, "binding.createChannelRoleAccessRecycler");
        recyclerView2.setNestedScrollingEnabled(false);
        SwitchCompat switchCompat = getBinding().g;
        m.checkNotNullExpressionValue(switchCompat, "binding.createChannelPrivateSwitch");
        if (getShouldChannelDefaultPrivate() || getDisablePrivateSwitch()) {
            z2 = true;
        }
        switchCompat.setChecked(z2);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getGuildId(), getCategoryId()), this, null, 2, null), WidgetCreateChannel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCreateChannel$onViewBoundOrOnResume$1(this));
        int i = 0;
        boolean z2 = true;
        final RadioManager radioManager = new RadioManager(n.listOf((Object[]) new RadioButton[]{getBinding().o, getBinding().r, getBinding().c, getBinding().m}));
        getBinding().n.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetCreateChannel$onViewBoundOrOnResume$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannelBinding binding;
                RadioManager radioManager2 = radioManager;
                binding = WidgetCreateChannel.this.getBinding();
                RadioButton radioButton = binding.o;
                m.checkNotNullExpressionValue(radioButton, "binding.createChannelTextRadio");
                radioManager2.a(radioButton);
                WidgetCreateChannel.this.configurePrivateRoles();
            }
        });
        getBinding().q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetCreateChannel$onViewBoundOrOnResume$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannelBinding binding;
                RadioManager radioManager2 = radioManager;
                binding = WidgetCreateChannel.this.getBinding();
                RadioButton radioButton = binding.r;
                m.checkNotNullExpressionValue(radioButton, "binding.createChannelVoiceRadio");
                radioManager2.a(radioButton);
                WidgetCreateChannel.this.configurePrivateRoles();
            }
        });
        getBinding().f2338b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetCreateChannel$onViewBoundOrOnResume$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannelBinding binding;
                RadioManager radioManager2 = radioManager;
                binding = WidgetCreateChannel.this.getBinding();
                RadioButton radioButton = binding.c;
                m.checkNotNullExpressionValue(radioButton, "binding.createChannelAnnouncementsRadio");
                radioManager2.a(radioButton);
                WidgetCreateChannel.this.configurePrivateRoles();
            }
        });
        getBinding().l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetCreateChannel$onViewBoundOrOnResume$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannelBinding binding;
                RadioManager radioManager2 = radioManager;
                binding = WidgetCreateChannel.this.getBinding();
                RadioButton radioButton = binding.m;
                m.checkNotNullExpressionValue(radioButton, "binding.createChannelStageRadio");
                radioManager2.a(radioButton);
                WidgetCreateChannel.this.configurePrivateRoles();
            }
        });
        int type = getType();
        if (type == 0) {
            RadioButton radioButton = getBinding().o;
            m.checkNotNullExpressionValue(radioButton, "binding.createChannelTextRadio");
            radioManager.a(radioButton);
        } else if (type == 2) {
            RadioButton radioButton2 = getBinding().r;
            m.checkNotNullExpressionValue(radioButton2, "binding.createChannelVoiceRadio");
            radioManager.a(radioButton2);
        } else if (type == 5) {
            RadioButton radioButton3 = getBinding().c;
            m.checkNotNullExpressionValue(radioButton3, "binding.createChannelAnnouncementsRadio");
            radioManager.a(radioButton3);
        } else if (type == 13) {
            RadioButton radioButton4 = getBinding().m;
            m.checkNotNullExpressionValue(radioButton4, "binding.createChannelStageRadio");
            radioManager.a(radioButton4);
        }
        configurePrivateRoles();
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetCreateChannel$onViewBoundOrOnResume$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannelBinding binding;
                WidgetCreateChannelBinding binding2;
                binding = WidgetCreateChannel.this.getBinding();
                SwitchCompat switchCompat = binding.g;
                m.checkNotNullExpressionValue(switchCompat, "binding.createChannelPrivateSwitch");
                binding2 = WidgetCreateChannel.this.getBinding();
                SwitchCompat switchCompat2 = binding2.g;
                m.checkNotNullExpressionValue(switchCompat2, "binding.createChannelPrivateSwitch");
                switchCompat.setChecked(!switchCompat2.isChecked());
                WidgetCreateChannel.this.configurePrivateRoles();
            }
        });
        LinearLayout linearLayout = getBinding().p;
        m.checkNotNullExpressionValue(linearLayout, "binding.createChannelTypeContainer");
        if (getType() == 4) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        linearLayout.setVisibility(i);
        getBinding().h.setText(getType() == 4 ? R.string.private_category : R.string.private_channel);
        getBinding().f.setText(getType() == 4 ? R.string.private_category_note : R.string.private_channel_note);
        getBinding().k.setText(getType() == 4 ? R.string.form_label_category_permissions : R.string.form_label_channel_permissions);
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.createChannelNameLayout");
        textInputLayout.setHint(getString(getType() == 4 ? R.string.category_name : R.string.form_label_channel_name));
    }
}
