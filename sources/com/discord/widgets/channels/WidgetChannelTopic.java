package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.core.view.ViewKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelTopicBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.member.GuildMember;
import com.discord.utilities.channel.GuildChannelIconType;
import com.discord.utilities.channel.GuildChannelIconUtilsKt;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.AstRenderer;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.WidgetChannelTopicViewModel;
import com.discord.widgets.channels.settings.WidgetChannelGroupDMSettings;
import com.discord.widgets.chat.pins.WidgetChannelPinnedMessages;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.user.WidgetUserMutualGuilds;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.badge.BadgeDrawable;
import d0.g0.t;
import d0.t.g0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetChannelTopic.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001BB\u0007¢\u0006\u0004\bA\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0019\u0010\u0017\u001a\u00020\u00042\b\b\u0001\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u0019H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u001cH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ9\u0010'\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u001f2\n\u0010#\u001a\u00060!j\u0002`\"2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010$2\b\b\u0002\u0010&\u001a\u00020\u001fH\u0002¢\u0006\u0004\b'\u0010(J\u000f\u0010)\u001a\u00020\u0004H\u0002¢\u0006\u0004\b)\u0010\u0010J\u0017\u0010*\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b*\u0010\u0006J\u0017\u0010,\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020+H\u0002¢\u0006\u0004\b,\u0010-J\u0017\u00100\u001a\u00020\u00042\u0006\u0010/\u001a\u00020.H\u0016¢\u0006\u0004\b0\u00101J\u000f\u00102\u001a\u00020\u0004H\u0016¢\u0006\u0004\b2\u0010\u0010R\u0016\u00103\u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u001d\u0010:\u001a\u0002058B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109R\u001d\u0010@\u001a\u00020;8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010?¨\u0006C"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopic;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;)V", "Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;", "renderedTopic", "configureChannelTopicTitle", "(Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;)V", "configureEllipsis", "()V", "Lcom/discord/api/channel/Channel;", "channel", "setChannelIconForGDM", "(Lcom/discord/api/channel/Channel;)V", "", "channelIconResource", "setChannelIcon", "(I)V", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;", "getRenderedTopicForDefaultTopic", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;", "getRenderedTopicForTopic", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;", "", "isGroup", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "channelTitle", "developerModeEnabled", "showContextMenu", "(ZJLjava/lang/CharSequence;Z)V", "onToggleTopicExpansionState", "onClickMore", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;", "getRenderedTopicForGDM", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "isDm", "Z", "Lcom/discord/databinding/WidgetChannelTopicBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelTopicBinding;", "binding", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;", "viewModel", HookHelper.constructorName, "RenderedTopic", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelTopic extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelTopic.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelTopicBinding;", 0)};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelTopic$binding$2.INSTANCE, null, 2, null);
    private boolean isDm;
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetChannelTopic.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB)\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ2\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\bJ\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\b¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;", "", "", "component1", "()Ljava/lang/CharSequence;", "component2", "", "component3", "()I", "channelName", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "autoLinkMask", "copy", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/CharSequence;", "getChannelName", "getTopic", "I", "getAutoLinkMask", HookHelper.constructorName, "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RenderedTopic {
        public static final Companion Companion = new Companion(null);
        public static final int MAX_LINES = 40;
        public static final int MIN_LINES = 2;
        private final int autoLinkMask;
        private final CharSequence channelName;
        private final CharSequence topic;

        /* compiled from: WidgetChannelTopic.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic$Companion;", "", "", "MAX_LINES", "I", "MIN_LINES", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public RenderedTopic() {
            this(null, null, 0, 7, null);
        }

        public RenderedTopic(CharSequence charSequence, CharSequence charSequence2, int i) {
            this.channelName = charSequence;
            this.topic = charSequence2;
            this.autoLinkMask = i;
        }

        public static /* synthetic */ RenderedTopic copy$default(RenderedTopic renderedTopic, CharSequence charSequence, CharSequence charSequence2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                charSequence = renderedTopic.channelName;
            }
            if ((i2 & 2) != 0) {
                charSequence2 = renderedTopic.topic;
            }
            if ((i2 & 4) != 0) {
                i = renderedTopic.autoLinkMask;
            }
            return renderedTopic.copy(charSequence, charSequence2, i);
        }

        public final CharSequence component1() {
            return this.channelName;
        }

        public final CharSequence component2() {
            return this.topic;
        }

        public final int component3() {
            return this.autoLinkMask;
        }

        public final RenderedTopic copy(CharSequence charSequence, CharSequence charSequence2, int i) {
            return new RenderedTopic(charSequence, charSequence2, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RenderedTopic)) {
                return false;
            }
            RenderedTopic renderedTopic = (RenderedTopic) obj;
            return m.areEqual(this.channelName, renderedTopic.channelName) && m.areEqual(this.topic, renderedTopic.topic) && this.autoLinkMask == renderedTopic.autoLinkMask;
        }

        public final int getAutoLinkMask() {
            return this.autoLinkMask;
        }

        public final CharSequence getChannelName() {
            return this.channelName;
        }

        public final CharSequence getTopic() {
            return this.topic;
        }

        public int hashCode() {
            CharSequence charSequence = this.channelName;
            int i = 0;
            int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
            CharSequence charSequence2 = this.topic;
            if (charSequence2 != null) {
                i = charSequence2.hashCode();
            }
            return ((hashCode + i) * 31) + this.autoLinkMask;
        }

        public String toString() {
            StringBuilder R = a.R("RenderedTopic(channelName=");
            R.append(this.channelName);
            R.append(", topic=");
            R.append(this.topic);
            R.append(", autoLinkMask=");
            return a.A(R, this.autoLinkMask, ")");
        }

        public /* synthetic */ RenderedTopic(String str, CharSequence charSequence, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? null : charSequence, (i2 & 4) != 0 ? 0 : i);
        }
    }

    public WidgetChannelTopic() {
        super(R.layout.widget_channel_topic);
        WidgetChannelTopic$viewModel$2 widgetChannelTopic$viewModel$2 = WidgetChannelTopic$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetChannelTopicViewModel.class), new WidgetChannelTopic$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetChannelTopic$viewModel$2));
    }

    /* JADX WARN: Code restructure failed: missing block: B:8:0x001c, code lost:
        if ((r2.length() > 0) == true) goto L10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureChannelTopicTitle(com.discord.widgets.channels.WidgetChannelTopic.RenderedTopic r6) {
        /*
            r5 = this;
            com.discord.databinding.WidgetChannelTopicBinding r0 = r5.getBinding()
            com.discord.utilities.view.text.LinkifiedTextView r0 = r0.g
            java.lang.String r1 = "binding.channelTopicTitle"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            java.lang.CharSequence r2 = r6.getTopic()
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L1f
            int r2 = r2.length()
            if (r2 <= 0) goto L1b
            r2 = 1
            goto L1c
        L1b:
            r2 = 0
        L1c:
            if (r2 != r3) goto L1f
            goto L20
        L1f:
            r3 = 0
        L20:
            if (r3 == 0) goto L23
            goto L25
        L23:
            r4 = 8
        L25:
            r0.setVisibility(r4)
            com.discord.databinding.WidgetChannelTopicBinding r0 = r5.getBinding()
            com.discord.utilities.view.text.LinkifiedTextView r0 = r0.g
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            int r2 = r6.getAutoLinkMask()
            r0.setAutoLinkMask(r2)
            java.lang.CharSequence r6 = r6.getTopic()
            boolean r0 = r6 instanceof com.facebook.drawee.span.DraweeSpanStringBuilder
            if (r0 == 0) goto L4c
            com.discord.databinding.WidgetChannelTopicBinding r0 = r5.getBinding()
            com.discord.utilities.view.text.LinkifiedTextView r0 = r0.g
            com.facebook.drawee.span.DraweeSpanStringBuilder r6 = (com.facebook.drawee.span.DraweeSpanStringBuilder) r6
            r0.setDraweeSpanStringBuilder(r6)
            goto L58
        L4c:
            com.discord.databinding.WidgetChannelTopicBinding r0 = r5.getBinding()
            com.discord.utilities.view.text.LinkifiedTextView r0 = r0.g
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r0.setText(r6)
        L58:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.WidgetChannelTopic.configureChannelTopicTitle(com.discord.widgets.channels.WidgetChannelTopic$RenderedTopic):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:13:0x005c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureEllipsis() {
        /*
            r5 = this;
            androidx.lifecycle.Lifecycle r0 = r5.getLifecycle()
            java.lang.String r1 = "lifecycle"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            androidx.lifecycle.Lifecycle$State r0 = r0.getCurrentState()
            androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.INITIALIZED
            boolean r0 = r0.isAtLeast(r1)
            if (r0 == 0) goto L61
            com.discord.databinding.WidgetChannelTopicBinding r0 = r5.getBinding()
            android.widget.ImageView r0 = r0.d
            com.discord.widgets.channels.WidgetChannelTopic$configureEllipsis$1 r1 = new com.discord.widgets.channels.WidgetChannelTopic$configureEllipsis$1
            r1.<init>()
            r0.setOnClickListener(r1)
            com.discord.databinding.WidgetChannelTopicBinding r0 = r5.getBinding()
            android.widget.ImageView r0 = r0.d
            java.lang.String r1 = "binding.channelTopicEllipsis"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            boolean r1 = r5.isDm
            r2 = 0
            if (r1 != 0) goto L58
            com.discord.databinding.WidgetChannelTopicBinding r1 = r5.getBinding()
            com.discord.utilities.view.text.LinkifiedTextView r1 = r1.g
            java.lang.String r3 = "binding.channelTopicTitle"
            d0.z.d.m.checkNotNullExpressionValue(r1, r3)
            int r1 = r1.getLineCount()
            r4 = 2
            if (r1 <= r4) goto L58
            com.discord.databinding.WidgetChannelTopicBinding r1 = r5.getBinding()
            com.discord.utilities.view.text.LinkifiedTextView r1 = r1.g
            d0.z.d.m.checkNotNullExpressionValue(r1, r3)
            int r1 = r1.getMaxLines()
            r3 = 40
            if (r1 == r3) goto L58
            r1 = 1
            goto L59
        L58:
            r1 = 0
        L59:
            if (r1 == 0) goto L5c
            goto L5e
        L5c:
            r2 = 8
        L5e:
            r0.setVisibility(r2)
        L61:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.WidgetChannelTopic.configureEllipsis():void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetChannelTopicViewModel.ViewState viewState) {
        RenderedTopic renderedTopic;
        boolean z2;
        int i = 8;
        boolean z3 = true;
        if (viewState instanceof WidgetChannelTopicViewModel.ViewState.NoChannel) {
            View view = getView();
            if (view != null) {
                ViewKt.setVisible(view, false);
            }
            ImageView imageView = getBinding().e;
            m.checkNotNullExpressionValue(imageView, "binding.channelTopicMoreIcon");
            imageView.setVisibility(8);
            setChannelIcon(GuildChannelIconUtilsKt.mapGuildChannelTypeToIcon(GuildChannelIconType.Text.INSTANCE));
            renderedTopic = new RenderedTopic(null, null, 0, 7, null);
        } else if (viewState instanceof WidgetChannelTopicViewModel.ViewState.Guild) {
            View view2 = getView();
            if (view2 != null) {
                ViewKt.setVisible(view2, true);
            }
            ImageView imageView2 = getBinding().e;
            m.checkNotNullExpressionValue(imageView2, "binding.channelTopicMoreIcon");
            imageView2.setVisibility(8);
            setChannelIcon(GuildChannelIconUtilsKt.mapGuildChannelTypeToIcon(((WidgetChannelTopicViewModel.ViewState.Guild) viewState).getChannelIconType()));
            if (viewState instanceof WidgetChannelTopicViewModel.ViewState.Guild.DefaultTopic) {
                renderedTopic = getRenderedTopicForDefaultTopic((WidgetChannelTopicViewModel.ViewState.Guild.DefaultTopic) viewState);
            } else {
                Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild.Topic");
                renderedTopic = getRenderedTopicForTopic((WidgetChannelTopicViewModel.ViewState.Guild.Topic) viewState);
            }
        } else if (viewState instanceof WidgetChannelTopicViewModel.ViewState.DM) {
            View view3 = getView();
            if (view3 != null) {
                ViewKt.setVisible(view3, true);
            }
            ImageView imageView3 = getBinding().e;
            m.checkNotNullExpressionValue(imageView3, "binding.channelTopicMoreIcon");
            imageView3.setVisibility(0);
            setChannelIcon(R.drawable.ic_direct_message_header);
            WidgetChannelTopicViewModel.ViewState.DM dm = (WidgetChannelTopicViewModel.ViewState.DM) viewState;
            getBinding().f2264b.configure(dm.getGuildMembers());
            renderedTopic = new RenderedTopic(dm.getRecipientName(), null, 0, 6, null);
        } else if (viewState instanceof WidgetChannelTopicViewModel.ViewState.GDM) {
            View view4 = getView();
            if (view4 != null) {
                ViewKt.setVisible(view4, true);
            }
            ImageView imageView4 = getBinding().e;
            m.checkNotNullExpressionValue(imageView4, "binding.channelTopicMoreIcon");
            imageView4.setVisibility(0);
            WidgetChannelTopicViewModel.ViewState.GDM gdm = (WidgetChannelTopicViewModel.ViewState.GDM) viewState;
            setChannelIconForGDM(gdm.getChannel());
            renderedTopic = getRenderedTopicForGDM(gdm);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        configureChannelTopicTitle(renderedTopic);
        boolean z4 = viewState instanceof WidgetChannelTopicViewModel.ViewState.DM;
        if (z4) {
            Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.DM");
            WidgetChannelTopicViewModel.ViewState.DM dm2 = (WidgetChannelTopicViewModel.ViewState.DM) viewState;
            Set<String> recipientNicknames = dm2.getRecipientNicknames();
            List<GuildMember> guildMembers = dm2.getGuildMembers();
            if (!(guildMembers instanceof Collection) || !guildMembers.isEmpty()) {
                for (GuildMember guildMember : guildMembers) {
                    if (guildMember.hasAvatar()) {
                        z2 = true;
                        break;
                    }
                }
            }
            z2 = false;
            getBinding().f2264b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view5) {
                    Long recipientUserId = ((WidgetChannelTopicViewModel.ViewState.DM) viewState).getRecipientUserId();
                    if (recipientUserId != null) {
                        WidgetUserMutualGuilds.Companion.show(WidgetChannelTopic.this.requireContext(), recipientUserId.longValue());
                    }
                }
            });
            UserAkaView userAkaView = getBinding().f2264b;
            m.checkNotNullExpressionValue(userAkaView, "binding.channelAka");
            userAkaView.setVisibility(!(recipientNicknames == null || recipientNicknames.isEmpty()) || z2 ? 0 : 8);
        } else {
            UserAkaView userAkaView2 = getBinding().f2264b;
            m.checkNotNullExpressionValue(userAkaView2, "binding.channelAka");
            userAkaView2.setVisibility(8);
        }
        this.isDm = z4;
        LinkifiedTextView linkifiedTextView = getBinding().g;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.channelTopicTitle");
        CharSequence topic = renderedTopic.getTopic();
        if ((topic == null || t.isBlank(topic)) || z4) {
            z3 = false;
        }
        if (z3) {
            i = 0;
        }
        linkifiedTextView.setVisibility(i);
        configureEllipsis();
        TextView textView = getBinding().f;
        m.checkNotNullExpressionValue(textView, "binding.channelTopicName");
        textView.setText(renderedTopic.getChannelName());
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view5) {
                WidgetChannelTopic.this.onClickMore(viewState);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChannelTopicBinding getBinding() {
        return (WidgetChannelTopicBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final RenderedTopic getRenderedTopicForDefaultTopic(WidgetChannelTopicViewModel.ViewState.Guild.DefaultTopic defaultTopic) {
        return new RenderedTopic(ChannelUtils.d(defaultTopic.getChannel(), requireContext(), false), "", 0);
    }

    private final RenderedTopic getRenderedTopicForGDM(WidgetChannelTopicViewModel.ViewState.GDM gdm) {
        return new RenderedTopic(ChannelUtils.d(gdm.getChannel(), requireContext(), false), null, 0, 4, null);
    }

    private final RenderedTopic getRenderedTopicForTopic(WidgetChannelTopicViewModel.ViewState.Guild.Topic topic) {
        LinkifiedTextView linkifiedTextView = getBinding().g;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.channelTopicTitle");
        Context context = linkifiedTextView.getContext();
        m.checkNotNullExpressionValue(context, "binding.channelTopicTitle.context");
        DraweeSpanStringBuilder render = AstRenderer.render(topic.getAst(), new MessageRenderContext(context, 0L, topic.getAllowAnimatedEmojis(), topic.getUserNames(), topic.getChannelNames(), topic.getRoles(), 0, null, WidgetChannelTopic$getRenderedTopicForTopic$renderContext$1.INSTANCE, 0, 0, new WidgetChannelTopic$getRenderedTopicForTopic$renderContext$2(getViewModel()), null, null, 14016, null));
        return new RenderedTopic(ChannelUtils.d(topic.getChannel(), requireContext(), false), render, (render.length() > 200 || topic.isLinkifyConflicting()) ? 0 : 15);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChannelTopicViewModel getViewModel() {
        return (WidgetChannelTopicViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(WidgetChannelTopicViewModel.Event event) {
        if (event instanceof WidgetChannelTopicViewModel.Event.FocusFirstElement) {
            getBinding().f.sendAccessibilityEvent(8);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onClickMore(WidgetChannelTopicViewModel.ViewState viewState) {
        if (viewState instanceof WidgetChannelTopicViewModel.ViewState.GDM) {
            WidgetChannelTopicViewModel.ViewState.GDM gdm = (WidgetChannelTopicViewModel.ViewState.GDM) viewState;
            showContextMenu(true, gdm.getChannelId(), ChannelUtils.d(gdm.getChannel(), requireContext(), false), gdm.getDeveloperModeEnabled());
        } else if (viewState instanceof WidgetChannelTopicViewModel.ViewState.DM) {
            WidgetChannelTopicViewModel.ViewState.DM dm = (WidgetChannelTopicViewModel.ViewState.DM) viewState;
            showContextMenu$default(this, false, dm.getChannelId(), null, dm.getDeveloperModeEnabled(), 4, null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onToggleTopicExpansionState() {
        WidgetChannelTopic$onToggleTopicExpansionState$1 widgetChannelTopic$onToggleTopicExpansionState$1 = new WidgetChannelTopic$onToggleTopicExpansionState$1(this);
        LinkifiedTextView linkifiedTextView = getBinding().g;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.channelTopicTitle");
        if (linkifiedTextView.getMaxLines() != 40) {
            widgetChannelTopic$onToggleTopicExpansionState$1.invoke(40);
        } else {
            widgetChannelTopic$onToggleTopicExpansionState$1.invoke(2);
        }
    }

    private final void setChannelIcon(@DrawableRes int i) {
        getBinding().c.setImageResource(i);
        SimpleDraweeView simpleDraweeView = getBinding().c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.channelTopicChannelIcon");
        ViewGroup.LayoutParams layoutParams = simpleDraweeView.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, DimenUtils.dpToPixels(4), marginLayoutParams.bottomMargin);
    }

    private final void setChannelIconForGDM(Channel channel) {
        SimpleDraweeView simpleDraweeView = getBinding().c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.channelTopicChannelIcon");
        IconUtils.setIcon$default(simpleDraweeView, IconUtils.getForChannel$default(channel, null, 2, null), (int) R.dimen.avatar_size_large, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        SimpleDraweeView simpleDraweeView2 = getBinding().c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.channelTopicChannelIcon");
        ViewGroup.LayoutParams layoutParams = simpleDraweeView2.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, DimenUtils.dpToPixels(8), marginLayoutParams.bottomMargin);
    }

    private final void showContextMenu(boolean z2, final long j, final CharSequence charSequence, boolean z3) {
        ImageView imageView = getBinding().e;
        m.checkNotNullExpressionValue(imageView, "binding.channelTopicMoreIcon");
        PopupMenu popupMenu = new PopupMenu(imageView.getContext(), getBinding().e, BadgeDrawable.BOTTOM_START);
        popupMenu.inflate(R.menu.menu_private_channel_sidebar);
        popupMenu.getMenu().findItem(R.id.menu_private_channel_sidebar_pinned_messages).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$showContextMenu$1
            @Override // android.view.MenuItem.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                WidgetChannelPinnedMessages.Companion.show(WidgetChannelTopic.this.requireContext(), j);
                return true;
            }
        });
        MenuItem findItem = popupMenu.getMenu().findItem(R.id.menu_private_channel_sidebar_copy_id);
        m.checkNotNullExpressionValue(findItem, "copyChannelIdAction");
        findItem.setVisible(z3);
        findItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$showContextMenu$2
            @Override // android.view.MenuItem.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                b.a.d.m.c(WidgetChannelTopic.this.requireContext(), String.valueOf(j), 0, 4);
                return true;
            }
        });
        MenuItem findItem2 = popupMenu.getMenu().findItem(R.id.menu_private_channel_sidebar_customize_gorup);
        m.checkNotNullExpressionValue(findItem2, "customizeGroupAction");
        findItem2.setVisible(z2);
        findItem2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$showContextMenu$3
            @Override // android.view.MenuItem.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                WidgetChannelGroupDMSettings.Companion.create(j, WidgetChannelTopic.this.requireContext());
                return true;
            }
        });
        MenuItem findItem3 = popupMenu.getMenu().findItem(R.id.menu_private_channel_sidebar_close);
        if (z2) {
            findItem3.setTitle(R.string.leave_group_dm);
            findItem3.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$showContextMenu$4

                /* compiled from: WidgetChannelTopic.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "v", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.WidgetChannelTopic$showContextMenu$4$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function1<View, Unit> {
                    public AnonymousClass1() {
                        super(1);
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(View view) {
                        invoke2(view);
                        return Unit.a;
                    }

                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2(View view) {
                        WidgetChannelTopicViewModel viewModel;
                        m.checkNotNullParameter(view, "v");
                        viewModel = WidgetChannelTopic.this.getViewModel();
                        Context context = view.getContext();
                        m.checkNotNullExpressionValue(context, "v.context");
                        viewModel.handleClosePrivateChannel(context);
                    }
                }

                @Override // android.view.MenuItem.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    CharSequence e;
                    CharSequence e2;
                    CharSequence e3;
                    CharSequence e4;
                    WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
                    FragmentManager parentFragmentManager = WidgetChannelTopic.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    e = b.e(WidgetChannelTopic.this, R.string.leave_group_dm_title, new Object[]{charSequence}, (r4 & 4) != 0 ? b.a.j : null);
                    e2 = b.e(WidgetChannelTopic.this, R.string.leave_group_dm_body, new Object[]{charSequence}, (r4 & 4) != 0 ? b.a.j : null);
                    e3 = b.e(WidgetChannelTopic.this, R.string.leave_group_dm, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                    e4 = b.e(WidgetChannelTopic.this, R.string.cancel, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                    WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, e, e2, e3, e4, g0.mapOf(d0.o.to(Integer.valueOf((int) R.id.OK_BUTTON), new AnonymousClass1())), null, null, null, Integer.valueOf((int) R.attr.notice_theme_positive_red), null, null, 0, null, 15808, null);
                    return true;
                }
            });
        } else {
            findItem3.setTitle(R.string.close_dm);
            findItem3.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$showContextMenu$5
                @Override // android.view.MenuItem.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    WidgetChannelTopicViewModel viewModel;
                    viewModel = WidgetChannelTopic.this.getViewModel();
                    viewModel.handleClosePrivateChannel(WidgetChannelTopic.this.requireContext());
                    return true;
                }
            });
        }
        popupMenu.show();
    }

    public static /* synthetic */ void showContextMenu$default(WidgetChannelTopic widgetChannelTopic, boolean z2, long j, CharSequence charSequence, boolean z3, int i, Object obj) {
        if ((i & 4) != 0) {
            charSequence = null;
        }
        widgetChannelTopic.showContextMenu(z2, j, charSequence, (i & 8) != 0 ? false : z3);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChannelTopic.this.onToggleTopicExpansionState();
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().listenForEvents(), this, null, 2, null), WidgetChannelTopic.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelTopic$onViewBoundOrOnResume$1(this));
        Observable<WidgetChannelTopicViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetChannelTopic.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelTopic$onViewBoundOrOnResume$2(this));
        LinkifiedTextView linkifiedTextView = getBinding().g;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.channelTopicTitle");
        linkifiedTextView.setMaxLines(2);
    }
}
