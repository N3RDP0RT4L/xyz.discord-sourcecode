package com.discord.widgets.channels;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import com.discord.databinding.WidgetChannelTopicBinding;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelTopic.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "maxLines", "", "invoke", "(I)V", "animateMaxLines"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelTopic$onToggleTopicExpansionState$1 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ WidgetChannelTopic this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelTopic$onToggleTopicExpansionState$1(WidgetChannelTopic widgetChannelTopic) {
        super(1);
        this.this$0 = widgetChannelTopic;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.a;
    }

    public final void invoke(int i) {
        WidgetChannelTopicBinding binding;
        binding = this.this$0.getBinding();
        ObjectAnimator ofInt = ObjectAnimator.ofInt(binding.g, "maxLines", i);
        m.checkNotNullExpressionValue(ofInt, "animation");
        ofInt.setDuration(150L);
        ofInt.addListener(new Animator.AnimatorListener() { // from class: com.discord.widgets.channels.WidgetChannelTopic$onToggleTopicExpansionState$1$animateMaxLines$$inlined$doOnEnd$1
            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
                WidgetChannelTopic$onToggleTopicExpansionState$1.this.this$0.configureEllipsis();
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
            }
        });
        ofInt.start();
    }
}
