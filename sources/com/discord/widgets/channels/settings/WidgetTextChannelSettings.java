package com.discord.widgets.channels.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.a.d.f;
import b.a.d.j;
import b.a.i.m4;
import b.a.i.n4;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildFeature;
import com.discord.api.permission.Permission;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetTextChannelSettingsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserGuildSettings;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.DurationUnit;
import com.discord.utilities.resources.DurationUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsOverview;
import com.discord.widgets.channels.settings.WidgetTextChannelSettings;
import com.discord.widgets.chat.pins.WidgetChannelPinnedMessages;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.o;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.functions.Func3;
import xyz.discord.R;
/* compiled from: WidgetTextChannelSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 52\u00020\u0001:\u000256B\u0007¢\u0006\u0004\b4\u0010(J\u0015\u0010\u0004\u001a\u00020\u0003*\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J+\u0010\u000b\u001a\u00020\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00062\u0006\u0010\n\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J_\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00152\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010!\u001a\u00020\u00032\u0006\u0010 \u001a\u00020\bH\u0002¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\u00032\u0006\u0010$\u001a\u00020#H\u0016¢\u0006\u0004\b%\u0010&J\u000f\u0010'\u001a\u00020\u0003H\u0016¢\u0006\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010,\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R\u001d\u00103\u001a\u00020.8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102¨\u00067"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings$Model;", "", "configureUI", "(Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings$Model;)V", "", "Lcom/google/android/material/radiobutton/MaterialRadioButton;", "", "durationMap", "newDuration", "updateRadioState", "(Ljava/util/Map;I)V", "Lcom/discord/api/channel/Channel;", "channel", "confirmDelete", "(Lcom/discord/api/channel/Channel;)V", "", "isPublicGuildRulesChannel", "cannotDeleteWarn", "(Z)V", "", ModelAuditLogEntry.CHANGE_KEY_ID, "", ModelAuditLogEntry.CHANGE_KEY_NAME, "type", ModelAuditLogEntry.CHANGE_KEY_TOPIC, ModelAuditLogEntry.CHANGE_KEY_NSFW, "rateLimit", "defaultAutoArchiveDuration", "saveChannel", "(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;)V", "cooldownSecs", "setSlowmodeLabel", "(I)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "hasFiredAnalytics", "Z", "Lcom/discord/databinding/WidgetTextChannelSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetTextChannelSettingsBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetTextChannelSettings extends AppFragment {
    public static final int ONE_DAY = 86400;
    public static final int ONE_HOUR = 3600;
    public static final int ONE_MINUTE = 60;
    private boolean hasFiredAnalytics;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetTextChannelSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetTextChannelSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final List<Integer> SLOWMODE_COOLDOWN_VALUES = n.listOf((Object[]) new Integer[]{0, 5, 10, 15, 30, 60, 120, 300, 600, 900, Integer.valueOf((int) ModelInvite.Settings.HALF_HOUR), 3600, 7200, Integer.valueOf((int) ModelInvite.Settings.SIX_HOURS)});
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetTextChannelSettings$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.channel_settings_edit_name, R.id.channel_settings_edit_topic, R.id.channel_settings_slow_mode_cooldown_slider, R.id.duration_selector, R.id.channel_settings_announcement);

    /* compiled from: WidgetTextChannelSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u0004¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u000e\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u0006H\u0007¢\u0006\u0004\b\u000e\u0010\u000fR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0016¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings$Companion;", "", "Landroid/widget/TextView;", "textView", "", "seconds", "Landroid/content/Context;", "context", "zeroLabel", "", "setDurationSecondsLabel", "(Landroid/widget/TextView;ILandroid/content/Context;I)V", "", "channelId", "launch", "(JLandroid/content/Context;)V", "", "SLOWMODE_COOLDOWN_VALUES", "Ljava/util/List;", "getSLOWMODE_COOLDOWN_VALUES", "()Ljava/util/List;", "ONE_DAY", "I", "ONE_HOUR", "ONE_MINUTE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void setDurationSecondsLabel$default(Companion companion, TextView textView, int i, Context context, int i2, int i3, Object obj) {
            if ((i3 & 8) != 0) {
                i2 = R.string.form_label_slowmode_off;
            }
            companion.setDurationSecondsLabel(textView, i, context, i2);
        }

        public final List<Integer> getSLOWMODE_COOLDOWN_VALUES() {
            return WidgetTextChannelSettings.SLOWMODE_COOLDOWN_VALUES;
        }

        public final void launch(long j, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", j);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…RA_CHANNEL_ID, channelId)");
            j.d(context, WidgetTextChannelSettings.class, putExtra);
        }

        public final void setDurationSecondsLabel(TextView textView, int i, Context context, int i2) {
            m.checkNotNullParameter(textView, "textView");
            m.checkNotNullParameter(context, "context");
            if (i == 0) {
                textView.setText(context.getString(i2));
            } else if (1 <= i && 60 > i) {
                DurationUtilsKt.setDurationText(textView, DurationUnit.SECONDS, i);
            } else if (60 <= i && 3600 > i) {
                DurationUtilsKt.setDurationText(textView, DurationUnit.MINS, i / 60);
            } else if (3600 <= i && 86400 > i) {
                DurationUtilsKt.setDurationText(textView, DurationUnit.HOURS, i / 3600);
            } else {
                DurationUtilsKt.setDurationText(textView, DurationUnit.DAYS, i / 86400);
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetTextChannelSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0082\b\u0018\u0000 ,2\u00020\u0001:\u0001,BG\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\b\u0012\u0006\u0010\u0014\u001a\u00020\b\u0012\u0006\u0010\u0015\u001a\u00020\b\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\u0006\u0010\u0017\u001a\u00020\b¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0010\u0010\f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0010\u0010\r\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\r\u0010\nJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0010\u0010\u000f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\nJ`\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00052\b\b\u0002\u0010\u0012\u001a\u00020\b2\b\b\u0002\u0010\u0013\u001a\u00020\b2\b\b\u0002\u0010\u0014\u001a\u00020\b2\b\b\u0002\u0010\u0015\u001a\u00020\b2\b\b\u0002\u0010\u0016\u001a\u00020\b2\b\b\u0002\u0010\u0017\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010!\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R\u0019\u0010\u0017\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010#\u001a\u0004\b\u0017\u0010\nR\u0019\u0010\u0013\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b$\u0010\nR\u0019\u0010\u0016\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b\u0016\u0010\nR\u0019\u0010\u0014\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b\u0014\u0010\nR\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b%\u0010\nR\u0019\u0010\u0011\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010&\u001a\u0004\b'\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010(\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010#\u001a\u0004\b\u0015\u0010\n¨\u0006-"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings$Model;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "component3", "()Z", "component4", "component5", "component6", "component7", "component8", "guild", "channel", "canManageChannel", "canManagePermissions", "isPinsEnabled", "isPublicGuildRulesChannel", "isPublicGuildUpdatesChannel", "isCommunityGuild", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;ZZZZZZ)Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanManagePermissions", "getCanManageChannel", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;ZZZZZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canManageChannel;
        private final boolean canManagePermissions;
        private final Channel channel;
        private final Guild guild;
        private final boolean isCommunityGuild;
        private final boolean isPinsEnabled;
        private final boolean isPublicGuildRulesChannel;
        private final boolean isPublicGuildUpdatesChannel;

        /* compiled from: WidgetTextChannelSettings.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings$Model$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/widgets/channels/settings/WidgetTextChannelSettings$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final long j) {
                Observable<R> Y = StoreStream.Companion.getChannels().observeChannel(j).Y(new b<Channel, Observable<? extends Model>>() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$Model$Companion$get$1
                    public final Observable<? extends WidgetTextChannelSettings.Model> call(final Channel channel) {
                        if (channel == null || ChannelUtils.C(channel)) {
                            return new k(null);
                        }
                        StoreStream.Companion companion = StoreStream.Companion;
                        return Observable.i(companion.getGuilds().observeGuild(channel.f()), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getPermissions().observePermissionsForChannel(j), new Func3<Guild, MeUser, Long, WidgetTextChannelSettings.Model>() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$Model$Companion$get$1.1
                            public final WidgetTextChannelSettings.Model call(Guild guild, MeUser meUser, Long l) {
                                Long publicUpdatesChannelId;
                                Long rulesChannelId;
                                if (guild == null) {
                                    return null;
                                }
                                boolean canAndIsElevated = PermissionUtils.canAndIsElevated(16L, l, meUser.getMfaEnabled(), guild.getMfaLevel());
                                boolean canAndIsElevated2 = PermissionUtils.canAndIsElevated(Permission.MANAGE_ROLES, l, meUser.getMfaEnabled(), guild.getMfaLevel());
                                boolean z2 = !channel.o() || StoreStream.Companion.getGuildsNsfw().isGuildNsfwGateAgreed(channel.f());
                                boolean contains = guild.getFeatures().contains(GuildFeature.COMMUNITY);
                                return new WidgetTextChannelSettings.Model(guild, channel, canAndIsElevated, canAndIsElevated2, z2, contains && (rulesChannelId = guild.getRulesChannelId()) != null && rulesChannelId.longValue() == j, contains && (publicUpdatesChannelId = guild.getPublicUpdatesChannelId()) != null && publicUpdatesChannelId.longValue() == j, contains);
                            }
                        });
                    }
                });
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …        }\n              }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(Y).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Guild guild, Channel channel, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(channel, "channel");
            this.guild = guild;
            this.channel = channel;
            this.canManageChannel = z2;
            this.canManagePermissions = z3;
            this.isPinsEnabled = z4;
            this.isPublicGuildRulesChannel = z5;
            this.isPublicGuildUpdatesChannel = z6;
            this.isCommunityGuild = z7;
        }

        public final Guild component1() {
            return this.guild;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final boolean component3() {
            return this.canManageChannel;
        }

        public final boolean component4() {
            return this.canManagePermissions;
        }

        public final boolean component5() {
            return this.isPinsEnabled;
        }

        public final boolean component6() {
            return this.isPublicGuildRulesChannel;
        }

        public final boolean component7() {
            return this.isPublicGuildUpdatesChannel;
        }

        public final boolean component8() {
            return this.isCommunityGuild;
        }

        public final Model copy(Guild guild, Channel channel, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(channel, "channel");
            return new Model(guild, channel, z2, z3, z4, z5, z6, z7);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.guild, model.guild) && m.areEqual(this.channel, model.channel) && this.canManageChannel == model.canManageChannel && this.canManagePermissions == model.canManagePermissions && this.isPinsEnabled == model.isPinsEnabled && this.isPublicGuildRulesChannel == model.isPublicGuildRulesChannel && this.isPublicGuildUpdatesChannel == model.isPublicGuildUpdatesChannel && this.isCommunityGuild == model.isCommunityGuild;
        }

        public final boolean getCanManageChannel() {
            return this.canManageChannel;
        }

        public final boolean getCanManagePermissions() {
            return this.canManagePermissions;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Channel channel = this.channel;
            if (channel != null) {
                i = channel.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.canManageChannel;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.canManagePermissions;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.isPinsEnabled;
            if (z4) {
                z4 = true;
            }
            int i10 = z4 ? 1 : 0;
            int i11 = z4 ? 1 : 0;
            int i12 = (i9 + i10) * 31;
            boolean z5 = this.isPublicGuildRulesChannel;
            if (z5) {
                z5 = true;
            }
            int i13 = z5 ? 1 : 0;
            int i14 = z5 ? 1 : 0;
            int i15 = (i12 + i13) * 31;
            boolean z6 = this.isPublicGuildUpdatesChannel;
            if (z6) {
                z6 = true;
            }
            int i16 = z6 ? 1 : 0;
            int i17 = z6 ? 1 : 0;
            int i18 = (i15 + i16) * 31;
            boolean z7 = this.isCommunityGuild;
            if (!z7) {
                i3 = z7 ? 1 : 0;
            }
            return i18 + i3;
        }

        public final boolean isCommunityGuild() {
            return this.isCommunityGuild;
        }

        public final boolean isPinsEnabled() {
            return this.isPinsEnabled;
        }

        public final boolean isPublicGuildRulesChannel() {
            return this.isPublicGuildRulesChannel;
        }

        public final boolean isPublicGuildUpdatesChannel() {
            return this.isPublicGuildUpdatesChannel;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guild=");
            R.append(this.guild);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", canManageChannel=");
            R.append(this.canManageChannel);
            R.append(", canManagePermissions=");
            R.append(this.canManagePermissions);
            R.append(", isPinsEnabled=");
            R.append(this.isPinsEnabled);
            R.append(", isPublicGuildRulesChannel=");
            R.append(this.isPublicGuildRulesChannel);
            R.append(", isPublicGuildUpdatesChannel=");
            R.append(this.isPublicGuildUpdatesChannel);
            R.append(", isCommunityGuild=");
            return a.M(R, this.isCommunityGuild, ")");
        }
    }

    public WidgetTextChannelSettings() {
        super(R.layout.widget_text_channel_settings);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void cannotDeleteWarn(boolean z2) {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.widget_channel_settings_cannot_delete, (ViewGroup) null, false);
        int i = R.id.channel_settings_cannot_delete_body;
        TextView textView = (TextView) inflate.findViewById(R.id.channel_settings_cannot_delete_body);
        if (textView != null) {
            i = R.id.channel_settings_cannot_delete_confirm;
            MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.channel_settings_cannot_delete_confirm);
            if (materialButton != null) {
                i = R.id.channel_settings_cannot_delete_title;
                TextView textView2 = (TextView) inflate.findViewById(R.id.channel_settings_cannot_delete_title);
                if (textView2 != null) {
                    LinearLayout linearLayout = (LinearLayout) inflate;
                    m.checkNotNullExpressionValue(new m4(linearLayout, textView, materialButton, textView2), "WidgetChannelSettingsCan…om(context), null, false)");
                    m.checkNotNullExpressionValue(linearLayout, "binding.root");
                    final AlertDialog create = new AlertDialog.Builder(linearLayout.getContext()).setView(linearLayout).create();
                    m.checkNotNullExpressionValue(create, "AlertDialog.Builder(bind…ew(binding.root).create()");
                    textView.setText(z2 ? R.string.delete_rules_channel_body : R.string.delete_updates_channel_body);
                    materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$cannotDeleteWarn$1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            AlertDialog.this.dismiss();
                        }
                    });
                    create.show();
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        AnalyticsTracker.GuildBoostUpsellType threadTypeForGuild;
        if (model == null) {
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        CharSequence charSequence = null;
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(ChannelUtils.B(model.getChannel()) ? R.string.channel_settings : R.string.category_settings);
        setActionBarSubtitle(ChannelUtils.e(model.getChannel(), requireContext(), false, 2));
        setActionBarOptionsMenu(ChannelUtils.B(model.getChannel()) ? R.menu.menu_text_channel_settings : R.menu.menu_category_settings, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                switch (menuItem.getItemId()) {
                    case R.id.menu_channel_settings_delete /* 2131364302 */:
                        if (model.isPublicGuildRulesChannel() || model.isPublicGuildUpdatesChannel()) {
                            WidgetTextChannelSettings.this.cannotDeleteWarn(model.isPublicGuildRulesChannel());
                            return;
                        } else {
                            WidgetTextChannelSettings.this.confirmDelete(model.getChannel());
                            return;
                        }
                    case R.id.menu_channel_settings_reset /* 2131364303 */:
                        StoreUserGuildSettings userGuildSettings = StoreStream.Companion.getUserGuildSettings();
                        m.checkNotNullExpressionValue(context, "context");
                        userGuildSettings.setChannelNotificationsDefault(context, model.getChannel());
                        return;
                    default:
                        return;
                }
            }
        }, new Action1<Menu>() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$2
            public final void call(Menu menu) {
                MenuItem findItem = menu.findItem(R.id.menu_channel_settings_delete);
                m.checkNotNullExpressionValue(findItem, "it.findItem(R.id.menu_channel_settings_delete)");
                findItem.setVisible(WidgetTextChannelSettings.Model.this.getCanManageChannel());
            }
        });
        if (!this.hasFiredAnalytics && (threadTypeForGuild = AnalyticsTracker.GuildBoostUpsellType.Companion.getThreadTypeForGuild(model.getGuild())) != null) {
            AnalyticsTracker.INSTANCE.guildBoostUpsellViewed(threadTypeForGuild, model.getGuild().getId(), Long.valueOf(model.getChannel().h()), new Traits.Location(Traits.Location.Page.CHANNEL_SETTINGS, null, null, null, null, 30, null));
            this.hasFiredAnalytics = true;
        }
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.channelSettingsEditName");
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.channelSettingsEditName");
        ViewExtensions.setText(textInputLayout, (CharSequence) statefulViews.get(textInputLayout2.getId(), ChannelUtils.c(model.getChannel())));
        getBinding().c.setHint(ChannelUtils.B(model.getChannel()) ? R.string.form_label_channel_name : R.string.category_name);
        TextInputLayout textInputLayout3 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.channelSettingsEditTopic");
        StatefulViews statefulViews2 = this.state;
        TextInputLayout textInputLayout4 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.channelSettingsEditTopic");
        int id2 = textInputLayout4.getId();
        String z2 = model.getChannel().z();
        if (z2 == null) {
            z2 = "";
        }
        ViewExtensions.setText(textInputLayout3, (CharSequence) statefulViews2.get(id2, z2));
        TextInputLayout textInputLayout5 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout5, "binding.channelSettingsEditTopic");
        int i = 8;
        textInputLayout5.setVisibility(ChannelUtils.B(model.getChannel()) ? 0 : 8);
        LinearLayout linearLayout = getBinding().e;
        m.checkNotNullExpressionValue(linearLayout, "binding.channelSettingsEditWrap");
        linearLayout.setVisibility(model.getCanManageChannel() ? 0 : 8);
        getBinding().k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StatefulViews statefulViews3;
                WidgetTextChannelSettingsBinding binding;
                StatefulViews statefulViews4;
                WidgetTextChannelSettingsBinding binding2;
                StatefulViews statefulViews5;
                WidgetTextChannelSettingsBinding binding3;
                StatefulViews statefulViews6;
                WidgetTextChannelSettings widgetTextChannelSettings = WidgetTextChannelSettings.this;
                long h = model.getChannel().h();
                statefulViews3 = WidgetTextChannelSettings.this.state;
                binding = WidgetTextChannelSettings.this.getBinding();
                TextInputLayout textInputLayout6 = binding.c;
                m.checkNotNullExpressionValue(textInputLayout6, "binding.channelSettingsEditName");
                String str = (String) statefulViews3.getIfChanged(textInputLayout6.getId());
                statefulViews4 = WidgetTextChannelSettings.this.state;
                binding2 = WidgetTextChannelSettings.this.getBinding();
                TextInputLayout textInputLayout7 = binding2.d;
                m.checkNotNullExpressionValue(textInputLayout7, "binding.channelSettingsEditTopic");
                String str2 = (String) statefulViews4.getIfChanged(textInputLayout7.getId());
                statefulViews5 = WidgetTextChannelSettings.this.state;
                binding3 = WidgetTextChannelSettings.this.getBinding();
                SeekBar seekBar = binding3.q;
                m.checkNotNullExpressionValue(seekBar, "binding.channelSettingsSlowModeCooldownSlider");
                Integer num = (Integer) statefulViews5.getIfChanged(seekBar.getId());
                statefulViews6 = WidgetTextChannelSettings.this.state;
                widgetTextChannelSettings.saveChannel(h, (r20 & 2) != 0 ? null : str, (r20 & 4) != 0 ? null : null, (r20 & 8) != 0 ? null : str2, (r20 & 16) != 0 ? null : null, (r20 & 32) != 0 ? null : num, (r20 & 64) != 0 ? null : (Integer) statefulViews6.getIfChanged(R.id.duration_selector));
            }
        });
        this.state.configureSaveActionView(getBinding().k);
        LinearLayout linearLayout2 = getBinding().i;
        m.checkNotNullExpressionValue(linearLayout2, "binding.channelSettingsPinnedMessagesContainer");
        linearLayout2.setVisibility(ChannelUtils.B(model.getChannel()) ? 0 : 8);
        TextView textView = getBinding().h;
        m.checkNotNullExpressionValue(textView, "binding.channelSettingsPinnedMessages");
        textView.setEnabled(model.isPinsEnabled());
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelPinnedMessages.Companion.show(a.x(view, "it", "it.context"), WidgetTextChannelSettings.Model.this.getChannel().h());
            }
        });
        View view = getBinding().j;
        m.checkNotNullExpressionValue(view, "binding.channelSettingsP…edMessagesDisabledOverlay");
        view.setVisibility(model.isPinsEnabled() ^ true ? 0 : 8);
        getBinding().j.setOnClickListener(WidgetTextChannelSettings$configureUI$6.INSTANCE);
        TextView textView2 = getBinding().g;
        m.checkNotNullExpressionValue(textView2, "binding.channelSettingsPermissions");
        textView2.setVisibility(model.getCanManagePermissions() ? 0 : 8);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChannelSettingsPermissionsOverview.Companion.launch(a.x(view2, "it", "it.context"), WidgetTextChannelSettings.Model.this.getChannel().h());
            }
        });
        CheckedSetting checkedSetting = getBinding().f2639b;
        m.checkNotNullExpressionValue(checkedSetting, "binding.channelSettingsAnnouncement");
        checkedSetting.setVisibility((ChannelUtils.r(model.getChannel()) || ChannelUtils.i(model.getChannel())) && model.getCanManageChannel() && model.isCommunityGuild() && !model.isPublicGuildRulesChannel() && !model.isPublicGuildUpdatesChannel() ? 0 : 8);
        getBinding().f2639b.g(ChannelUtils.i(model.getChannel()), false);
        CheckedSetting checkedSetting2 = getBinding().f2639b;
        Context context = getContext();
        if (context != null) {
            charSequence = b.a.k.b.b(context, R.string.form_help_news_android, new Object[]{f.a.a(360032008192L, null)}, (r4 & 4) != 0 ? b.C0034b.j : null);
        }
        checkedSetting2.h(charSequence, true);
        getBinding().f2639b.e(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                if (ChannelUtils.i(model.getChannel())) {
                    WidgetTextChannelSettings.this.saveChannel(model.getChannel().h(), (r20 & 2) != 0 ? null : null, (r20 & 4) != 0 ? null : 0, (r20 & 8) != 0 ? null : null, (r20 & 16) != 0 ? null : null, (r20 & 32) != 0 ? null : null, (r20 & 64) != 0 ? null : null);
                } else {
                    WidgetTextChannelSettings.this.saveChannel(model.getChannel().h(), (r20 & 2) != 0 ? null : null, (r20 & 4) != 0 ? null : 5, (r20 & 8) != 0 ? null : null, (r20 & 16) != 0 ? null : null, (r20 & 32) != 0 ? null : null, (r20 & 64) != 0 ? null : null);
                }
            }
        });
        LinearLayout linearLayout3 = getBinding().o;
        m.checkNotNullExpressionValue(linearLayout3, "binding.channelSettingsSectionUserManagement");
        linearLayout3.setVisibility(model.getCanManageChannel() || model.getCanManagePermissions() ? 0 : 8);
        LinearLayout linearLayout4 = getBinding().m;
        m.checkNotNullExpressionValue(linearLayout4, "binding.channelSettingsSectionPrivacySafety");
        linearLayout4.setVisibility(model.getCanManageChannel() && ChannelUtils.B(model.getChannel()) ? 0 : 8);
        CheckedSetting checkedSetting3 = getBinding().f;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.channelSettingsNsfw");
        checkedSetting3.setVisibility(model.getCanManageChannel() ? 0 : 8);
        getBinding().f.g(model.getChannel().o(), false);
        getBinding().f.e(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetTextChannelSettingsBinding binding;
                WidgetTextChannelSettings widgetTextChannelSettings = WidgetTextChannelSettings.this;
                long h = model.getChannel().h();
                binding = WidgetTextChannelSettings.this.getBinding();
                CheckedSetting checkedSetting4 = binding.f;
                m.checkNotNullExpressionValue(checkedSetting4, "binding.channelSettingsNsfw");
                widgetTextChannelSettings.saveChannel(h, (r20 & 2) != 0 ? null : null, (r20 & 4) != 0 ? null : null, (r20 & 8) != 0 ? null : null, (r20 & 16) != 0 ? null : Boolean.valueOf(!checkedSetting4.isChecked()), (r20 & 32) != 0 ? null : null, (r20 & 64) != 0 ? null : null);
            }
        });
        LinearLayout linearLayout5 = getBinding().n;
        m.checkNotNullExpressionValue(linearLayout5, "binding.channelSettingsSectionSlowMode");
        linearLayout5.setVisibility(model.getCanManageChannel() && ChannelUtils.B(model.getChannel()) && !ChannelUtils.i(model.getChannel()) ? 0 : 8);
        int intValue = ((Number) this.state.get(R.id.channel_settings_slow_mode_cooldown_slider, Integer.valueOf(model.getChannel().u()))).intValue();
        setSlowmodeLabel(intValue);
        Iterator<Integer> it = SLOWMODE_COOLDOWN_VALUES.iterator();
        int i2 = 0;
        while (true) {
            if (!it.hasNext()) {
                i2 = -1;
                break;
            }
            if (it.next().intValue() >= intValue) {
                break;
            }
            i2++;
        }
        SeekBar seekBar = getBinding().q;
        m.checkNotNullExpressionValue(seekBar, "binding.channelSettingsSlowModeCooldownSlider");
        seekBar.setProgress(i2);
        this.state.configureSaveActionView(getBinding().k);
        SeekBar seekBar2 = getBinding().q;
        m.checkNotNullExpressionValue(seekBar2, "binding.channelSettingsSlowModeCooldownSlider");
        TextView textView3 = getBinding().p;
        m.checkNotNullExpressionValue(textView3, "binding.channelSettingsSlowModeCooldownLabel");
        seekBar2.setContentDescription(textView3.getText());
        LinearLayout linearLayout6 = getBinding().l;
        m.checkNotNullExpressionValue(linearLayout6, "binding.channelSettingsS…efaultAutoArchiveDuration");
        if (model.getCanManageChannel() && ChannelUtils.D(model.getChannel()) && ThreadUtils.INSTANCE.isThreadsEnabled(model.getGuild().getId())) {
            i = 0;
        }
        linearLayout6.setVisibility(i);
        Integer num = (Integer) this.state.get(R.id.duration_selector, model.getChannel().d());
        int intValue2 = num != null ? num.intValue() : 1440;
        this.state.configureSaveActionView(getBinding().k);
        final Map<MaterialRadioButton, Integer> mapOf = h0.mapOf(o.to(getBinding().r.e, 60), o.to(getBinding().r.q, 1440), o.to(getBinding().r.o, Integer.valueOf((int) ThreadUtils.ThreadArchiveDurations.THREE_DAYS_IN_MINUTES)), o.to(getBinding().r.j, Integer.valueOf((int) ThreadUtils.ThreadArchiveDurations.SEVEN_DAYS_IN_MINUTES)));
        getBinding().r.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                StatefulViews statefulViews3;
                StatefulViews statefulViews4;
                WidgetTextChannelSettingsBinding binding;
                statefulViews3 = WidgetTextChannelSettings.this.state;
                statefulViews3.put(R.id.duration_selector, 60);
                statefulViews4 = WidgetTextChannelSettings.this.state;
                binding = WidgetTextChannelSettings.this.getBinding();
                statefulViews4.configureSaveActionView(binding.k);
                WidgetTextChannelSettings.this.updateRadioState(mapOf, 60);
            }
        });
        getBinding().r.p.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$configureUI$11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                StatefulViews statefulViews3;
                StatefulViews statefulViews4;
                WidgetTextChannelSettingsBinding binding;
                statefulViews3 = WidgetTextChannelSettings.this.state;
                statefulViews3.put(R.id.duration_selector, 1440);
                statefulViews4 = WidgetTextChannelSettings.this.state;
                binding = WidgetTextChannelSettings.this.getBinding();
                statefulViews4.configureSaveActionView(binding.k);
                WidgetTextChannelSettings.this.updateRadioState(mapOf, 1440);
            }
        });
        PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
        Guild guild = model.getGuild();
        Long valueOf = Long.valueOf(model.getChannel().h());
        GuildFeature guildFeature = GuildFeature.THREE_DAY_THREAD_ARCHIVE;
        Context requireContext = requireContext();
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        PremiumUtils.BoostFeatureBadgeData boostFeatureBadgeDataForGuildFeature = premiumUtils.getBoostFeatureBadgeDataForGuildFeature(guild, valueOf, guildFeature, requireContext, parentFragmentManager, new WidgetTextChannelSettings$configureUI$threeDayArchiveOption$1(this, mapOf), new Traits.Location(Traits.Location.Page.CHANNEL_SETTINGS, null, Traits.Location.Obj.LIST_ITEM, null, null, 26, null));
        ConstraintLayout constraintLayout = getBinding().r.k;
        final Function1<View, Unit> onClickListener = boostFeatureBadgeDataForGuildFeature.getOnClickListener();
        Object obj = onClickListener;
        if (onClickListener != null) {
            obj = new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$sam$android_view_View_OnClickListener$0
                @Override // android.view.View.OnClickListener
                public final /* synthetic */ void onClick(View view2) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(view2), "invoke(...)");
                }
            };
        }
        constraintLayout.setOnClickListener((View.OnClickListener) obj);
        TextView textView4 = getBinding().r.n;
        m.checkNotNullExpressionValue(textView4, "binding.durationSelector.optionThreeDaysLabel");
        textView4.setText(boostFeatureBadgeDataForGuildFeature.getText());
        getBinding().r.l.setTextColor(boostFeatureBadgeDataForGuildFeature.getTextColor());
        ImageView imageView = getBinding().r.m;
        m.checkNotNullExpressionValue(imageView, "binding.durationSelector.optionThreeDaysIcon");
        ColorCompatKt.tintWithColor(imageView, boostFeatureBadgeDataForGuildFeature.getIconColor());
        Guild guild2 = model.getGuild();
        Long valueOf2 = Long.valueOf(model.getChannel().h());
        GuildFeature guildFeature2 = GuildFeature.SEVEN_DAY_THREAD_ARCHIVE;
        Context requireContext2 = requireContext();
        FragmentManager parentFragmentManager2 = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
        PremiumUtils.BoostFeatureBadgeData boostFeatureBadgeDataForGuildFeature2 = premiumUtils.getBoostFeatureBadgeDataForGuildFeature(guild2, valueOf2, guildFeature2, requireContext2, parentFragmentManager2, new WidgetTextChannelSettings$configureUI$sevenDayArchiveOption$1(this, mapOf), new Traits.Location(Traits.Location.Page.CHANNEL_SETTINGS, null, Traits.Location.Obj.LIST_ITEM, null, null, 26, null));
        ConstraintLayout constraintLayout2 = getBinding().r.f;
        final Function1<View, Unit> onClickListener2 = boostFeatureBadgeDataForGuildFeature2.getOnClickListener();
        Object obj2 = onClickListener2;
        if (onClickListener2 != null) {
            obj2 = new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$sam$android_view_View_OnClickListener$0
                @Override // android.view.View.OnClickListener
                public final /* synthetic */ void onClick(View view2) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(view2), "invoke(...)");
                }
            };
        }
        constraintLayout2.setOnClickListener((View.OnClickListener) obj2);
        TextView textView5 = getBinding().r.i;
        m.checkNotNullExpressionValue(textView5, "binding.durationSelector.optionSevenDaysLabel");
        textView5.setText(boostFeatureBadgeDataForGuildFeature2.getText());
        getBinding().r.g.setTextColor(boostFeatureBadgeDataForGuildFeature2.getTextColor());
        ImageView imageView2 = getBinding().r.h;
        m.checkNotNullExpressionValue(imageView2, "binding.durationSelector.optionSevenDaysIcon");
        ColorCompatKt.tintWithColor(imageView2, boostFeatureBadgeDataForGuildFeature2.getIconColor());
        updateRadioState(mapOf, intValue2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void confirmDelete(final Channel channel) {
        n4 a = n4.a(LayoutInflater.from(getContext()), null, false);
        m.checkNotNullExpressionValue(a, "WidgetChannelSettingsDel…om(context), null, false)");
        LinearLayout linearLayout = a.a;
        m.checkNotNullExpressionValue(linearLayout, "binding.root");
        final AlertDialog create = new AlertDialog.Builder(linearLayout.getContext()).setView(a.a).create();
        m.checkNotNullExpressionValue(create, "AlertDialog.Builder(bind…ew(binding.root).create()");
        a.e.setText(ChannelUtils.B(channel) ? R.string.delete_channel : R.string.delete_category);
        a.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$confirmDelete$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AlertDialog.this.dismiss();
            }
        });
        a.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$confirmDelete$2

            /* compiled from: WidgetTextChannelSettings.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.channels.settings.WidgetTextChannelSettings$confirmDelete$2$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends d0.z.d.o implements Function1<Channel, Unit> {
                public AnonymousClass2() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
                    invoke2(channel);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Channel channel) {
                    m.checkNotNullParameter(channel, "channel");
                    Integer b2 = ChannelUtils.b(channel);
                    if (b2 != null) {
                        b.a.d.m.i(WidgetTextChannelSettings.this, b2.intValue(), 0, 4);
                    }
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Observable ui$default = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteChannel(channel.h()), false, 1, null), WidgetTextChannelSettings.this, null, 2, null);
                m.checkNotNullExpressionValue(view, "v");
                ObservableExtensionsKt.appSubscribe(ui$default, (r18 & 1) != 0 ? null : view.getContext(), "javaClass", (r18 & 4) != 0 ? null : null, new AnonymousClass2(), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
            }
        });
        TextView textView = a.f164b;
        m.checkNotNullExpressionValue(textView, "binding.channelSettingsDeleteBody");
        b.a.k.b.m(textView, R.string.delete_channel_body, new Object[]{ChannelUtils.e(channel, requireContext(), false, 2)}, (r4 & 4) != 0 ? b.g.j : null);
        create.show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetTextChannelSettingsBinding getBinding() {
        return (WidgetTextChannelSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void launch(long j, Context context) {
        Companion.launch(j, context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void saveChannel(long j, String str, Integer num, String str2, Boolean bool, Integer num2, Integer num3) {
        ObservableExtensionsKt.ui$default(RestAPI.Companion.getApi().editTextChannel(j, str, num, str2, bool, num2, num3), this, null, 2, null).k(b.a.d.o.a.g(getContext(), new WidgetTextChannelSettings$saveChannel$1(this), null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setSlowmodeLabel(int i) {
        Companion companion = Companion;
        TextView textView = getBinding().p;
        m.checkNotNullExpressionValue(textView, "binding.channelSettingsSlowModeCooldownLabel");
        companion.setDurationSecondsLabel(textView, i, requireContext(), R.string.form_label_slowmode_off);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateRadioState(Map<MaterialRadioButton, Integer> map, int i) {
        for (Map.Entry<MaterialRadioButton, Integer> entry : map.entrySet()) {
            entry.getKey().setChecked(entry.getValue().intValue() == i);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setRetainInstance(true);
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.channelSettingsEditTopic");
        statefulViews.addOptionalFields(textInputLayout);
        StatefulViews statefulViews2 = this.state;
        FloatingActionButton floatingActionButton = getBinding().k;
        TextInputLayout textInputLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.channelSettingsEditName");
        TextInputLayout textInputLayout3 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.channelSettingsEditTopic");
        statefulViews2.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout2, textInputLayout3);
        SeekBar seekBar = getBinding().q;
        m.checkNotNullExpressionValue(seekBar, "binding.channelSettingsSlowModeCooldownSlider");
        seekBar.setMax(n.getLastIndex(SLOWMODE_COOLDOWN_VALUES));
        getBinding().q.setOnSeekBarChangeListener(new b.a.y.j() { // from class: com.discord.widgets.channels.settings.WidgetTextChannelSettings$onViewBound$1
            @Override // b.a.y.j, android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar2, int i, boolean z2) {
                StatefulViews statefulViews3;
                StatefulViews statefulViews4;
                WidgetTextChannelSettingsBinding binding;
                WidgetTextChannelSettingsBinding binding2;
                m.checkNotNullParameter(seekBar2, "seekBar");
                super.onProgressChanged(seekBar2, i, z2);
                if (z2) {
                    int intValue = WidgetTextChannelSettings.Companion.getSLOWMODE_COOLDOWN_VALUES().get(i).intValue();
                    WidgetTextChannelSettings.this.setSlowmodeLabel(intValue);
                    statefulViews3 = WidgetTextChannelSettings.this.state;
                    statefulViews3.put(R.id.channel_settings_slow_mode_cooldown_slider, Integer.valueOf(intValue));
                    statefulViews4 = WidgetTextChannelSettings.this.state;
                    binding = WidgetTextChannelSettings.this.getBinding();
                    statefulViews4.configureSaveActionView(binding.k);
                    binding2 = WidgetTextChannelSettings.this.getBinding();
                    TextView textView = binding2.p;
                    m.checkNotNullExpressionValue(textView, "binding.channelSettingsSlowModeCooldownLabel");
                    seekBar2.setContentDescription(textView.getText());
                }
            }
        });
        TextInputLayout textInputLayout4 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.channelSettingsEditTopic");
        ViewExtensions.interceptScrollWhenInsideScrollable(textInputLayout4);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", -1L)), this, null, 2, null), WidgetTextChannelSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetTextChannelSettings$onViewBoundOrOnResume$1(this));
    }
}
