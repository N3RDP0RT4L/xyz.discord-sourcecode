package com.discord.widgets.channels.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelNotificationSettingsBinding;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.stores.StoreStream;
import com.discord.utilities.notifications.NotificationUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.discord.widgets.servers.NotificationMuteSettingsView;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetChannelNotificationSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B\u0007¢\u0006\u0004\b\u0016\u0010\fJ\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\u0006\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0005J\u0017\u0010\t\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;", "", "configureUI", "(Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;)V", "configureNotificationRadios", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetChannelNotificationSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelNotificationSettingsBinding;", "binding", "Lcom/discord/views/RadioManager;", "notificationSettingsRadioManager", "Lcom/discord/views/RadioManager;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelNotificationSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelNotificationSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelNotificationSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_SHOW_SYSTEM_SETTINGS = "SHOW_SYSTEM_SETTING";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelNotificationSettings$binding$2.INSTANCE, null, 2, null);
    private RadioManager notificationSettingsRadioManager;

    /* compiled from: WidgetChannelNotificationSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J+\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "showSystemSetting", "", "launch", "(Landroid/content/Context;JZ)V", "", "INTENT_SHOW_SYSTEM_SETTINGS", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, long j, boolean z2, int i, Object obj) {
            if ((i & 4) != 0) {
                z2 = false;
            }
            companion.launch(context, j, z2);
        }

        public final void launch(Context context, long j, boolean z2) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", j).putExtra(WidgetChannelNotificationSettings.INTENT_SHOW_SYSTEM_SETTINGS, z2);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…TINGS, showSystemSetting)");
            j.d(context, WidgetChannelNotificationSettings.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelNotificationSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u001f\b\u0082\b\u0018\u0000 +2\u00020\u0001:\u0001+BG\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u000e\u0010\u0014\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\r\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0004\b)\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\u0010\u0010\u0011\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0007J^\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00052\u0010\b\u0002\u0010\u0014\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\r2\b\b\u0002\u0010\u0017\u001a\u00020\u00052\b\b\u0002\u0010\u0018\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u000bJ\u0010\u0010\u001c\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u000fJ\u001a\u0010\u001e\u001a\u00020\u00052\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b!\u0010\u0007R!\u0010\u0014\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\"\u001a\u0004\b#\u0010\u000bR\u0019\u0010\u0017\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010 \u001a\u0004\b$\u0010\u0007R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010%\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010 \u001a\u0004\b\u0018\u0010\u0007R\u0019\u0010\u0016\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010'\u001a\u0004\b(\u0010\u000fR\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010 \u001a\u0004\b\u0015\u0010\u0007¨\u0006,"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "", "Lcom/discord/primitives/UtcTimestamp;", "component3", "()Ljava/lang/String;", "component4", "", "component5", "()I", "component6", "component7", "channel", "channelIsMuted", "channelMuteEndTime", "isGuildMuted", "notificationSetting", "notificationSettingIsInherited", "isAboveNotifyAllSize", "copy", "(Lcom/discord/api/channel/Channel;ZLjava/lang/String;ZIZZ)Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getChannelIsMuted", "Ljava/lang/String;", "getChannelMuteEndTime", "getNotificationSettingIsInherited", "Lcom/discord/api/channel/Channel;", "getChannel", "I", "getNotificationSetting", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZLjava/lang/String;ZIZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        public static final float RADIO_DIMMED_ALPHA = 0.3f;
        public static final float RADIO_OPAQUE_ALPHA = 1.0f;
        private final Channel channel;
        private final boolean channelIsMuted;
        private final String channelMuteEndTime;
        private final boolean isAboveNotifyAllSize;
        private final boolean isGuildMuted;
        private final int notificationSetting;
        private final boolean notificationSettingIsInherited;

        /* compiled from: WidgetChannelNotificationSettings.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u000b¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;", "get", "(J)Lrx/Observable;", "", "RADIO_DIMMED_ALPHA", "F", "RADIO_OPAQUE_ALPHA", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j) {
                Observable<R> Y = StoreStream.Companion.getChannels().observeChannel(j).Y(WidgetChannelNotificationSettings$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …ust(null)\n              }");
                Observable<Model> q = ObservableExtensionsKt.computationBuffered(Y).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Channel channel, boolean z2, String str, boolean z3, int i, boolean z4, boolean z5) {
            m.checkNotNullParameter(channel, "channel");
            this.channel = channel;
            this.channelIsMuted = z2;
            this.channelMuteEndTime = str;
            this.isGuildMuted = z3;
            this.notificationSetting = i;
            this.notificationSettingIsInherited = z4;
            this.isAboveNotifyAllSize = z5;
        }

        public static /* synthetic */ Model copy$default(Model model, Channel channel, boolean z2, String str, boolean z3, int i, boolean z4, boolean z5, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                channel = model.channel;
            }
            if ((i2 & 2) != 0) {
                z2 = model.channelIsMuted;
            }
            boolean z6 = z2;
            if ((i2 & 4) != 0) {
                str = model.channelMuteEndTime;
            }
            String str2 = str;
            if ((i2 & 8) != 0) {
                z3 = model.isGuildMuted;
            }
            boolean z7 = z3;
            if ((i2 & 16) != 0) {
                i = model.notificationSetting;
            }
            int i3 = i;
            if ((i2 & 32) != 0) {
                z4 = model.notificationSettingIsInherited;
            }
            boolean z8 = z4;
            if ((i2 & 64) != 0) {
                z5 = model.isAboveNotifyAllSize;
            }
            return model.copy(channel, z6, str2, z7, i3, z8, z5);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final boolean component2() {
            return this.channelIsMuted;
        }

        public final String component3() {
            return this.channelMuteEndTime;
        }

        public final boolean component4() {
            return this.isGuildMuted;
        }

        public final int component5() {
            return this.notificationSetting;
        }

        public final boolean component6() {
            return this.notificationSettingIsInherited;
        }

        public final boolean component7() {
            return this.isAboveNotifyAllSize;
        }

        public final Model copy(Channel channel, boolean z2, String str, boolean z3, int i, boolean z4, boolean z5) {
            m.checkNotNullParameter(channel, "channel");
            return new Model(channel, z2, str, z3, i, z4, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.channel, model.channel) && this.channelIsMuted == model.channelIsMuted && m.areEqual(this.channelMuteEndTime, model.channelMuteEndTime) && this.isGuildMuted == model.isGuildMuted && this.notificationSetting == model.notificationSetting && this.notificationSettingIsInherited == model.notificationSettingIsInherited && this.isAboveNotifyAllSize == model.isAboveNotifyAllSize;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final boolean getChannelIsMuted() {
            return this.channelIsMuted;
        }

        public final String getChannelMuteEndTime() {
            return this.channelMuteEndTime;
        }

        public final int getNotificationSetting() {
            return this.notificationSetting;
        }

        public final boolean getNotificationSettingIsInherited() {
            return this.notificationSettingIsInherited;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            boolean z2 = this.channelIsMuted;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            String str = this.channelMuteEndTime;
            if (str != null) {
                i = str.hashCode();
            }
            int i6 = (i5 + i) * 31;
            boolean z3 = this.isGuildMuted;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (((i6 + i7) * 31) + this.notificationSetting) * 31;
            boolean z4 = this.notificationSettingIsInherited;
            if (z4) {
                z4 = true;
            }
            int i10 = z4 ? 1 : 0;
            int i11 = z4 ? 1 : 0;
            int i12 = (i9 + i10) * 31;
            boolean z5 = this.isAboveNotifyAllSize;
            if (!z5) {
                i2 = z5 ? 1 : 0;
            }
            return i12 + i2;
        }

        public final boolean isAboveNotifyAllSize() {
            return this.isAboveNotifyAllSize;
        }

        public final boolean isGuildMuted() {
            return this.isGuildMuted;
        }

        public String toString() {
            StringBuilder R = a.R("Model(channel=");
            R.append(this.channel);
            R.append(", channelIsMuted=");
            R.append(this.channelIsMuted);
            R.append(", channelMuteEndTime=");
            R.append(this.channelMuteEndTime);
            R.append(", isGuildMuted=");
            R.append(this.isGuildMuted);
            R.append(", notificationSetting=");
            R.append(this.notificationSetting);
            R.append(", notificationSettingIsInherited=");
            R.append(this.notificationSettingIsInherited);
            R.append(", isAboveNotifyAllSize=");
            return a.M(R, this.isAboveNotifyAllSize, ")");
        }
    }

    public WidgetChannelNotificationSettings() {
        super(R.layout.widget_channel_notification_settings);
    }

    public static final /* synthetic */ RadioManager access$getNotificationSettingsRadioManager$p(WidgetChannelNotificationSettings widgetChannelNotificationSettings) {
        RadioManager radioManager = widgetChannelNotificationSettings.notificationSettingsRadioManager;
        if (radioManager == null) {
            m.throwUninitializedPropertyAccessException("notificationSettingsRadioManager");
        }
        return radioManager;
    }

    private final void configureNotificationRadios(Model model) {
        CharSequence g;
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = model.getNotificationSetting();
        if (ChannelUtils.z(model.getChannel()) && ref$IntRef.element == ModelNotificationSettings.FREQUENCY_ALL) {
            ref$IntRef.element = ModelNotificationSettings.FREQUENCY_MENTIONS;
        }
        WidgetChannelNotificationSettings$configureNotificationRadios$1 widgetChannelNotificationSettings$configureNotificationRadios$1 = new WidgetChannelNotificationSettings$configureNotificationRadios$1(this, ref$IntRef);
        CheckedSetting checkedSetting = getBinding().c;
        m.checkNotNullExpressionValue(checkedSetting, "binding.frequencyRadioAll");
        checkedSetting.setVisibility(ChannelUtils.B(model.getChannel()) || ChannelUtils.k(model.getChannel()) ? 0 : 8);
        CheckedSetting checkedSetting2 = getBinding().d;
        String string = requireContext().getString(ChannelUtils.z(model.getChannel()) ? R.string.form_label_live_stages_only : R.string.form_label_only_mentions);
        m.checkNotNullExpressionValue(string, "requireContext().getStri…s\n            }\n        )");
        g = b.g(string, new Object[0], (r3 & 2) != 0 ? b.e.j : null);
        checkedSetting2.setText(g);
        CheckedSetting checkedSetting3 = getBinding().c;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.frequencyRadioAll");
        widgetChannelNotificationSettings$configureNotificationRadios$1.invoke(model, checkedSetting3, ModelNotificationSettings.FREQUENCY_ALL);
        CheckedSetting checkedSetting4 = getBinding().d;
        m.checkNotNullExpressionValue(checkedSetting4, "binding.frequencyRadioMentions");
        widgetChannelNotificationSettings$configureNotificationRadios$1.invoke(model, checkedSetting4, ModelNotificationSettings.FREQUENCY_MENTIONS);
        CheckedSetting checkedSetting5 = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting5, "binding.frequencyRadioNothing");
        widgetChannelNotificationSettings$configureNotificationRadios$1.invoke(model, checkedSetting5, ModelNotificationSettings.FREQUENCY_NOTHING);
    }

    public final void configureUI(Model model) {
        CharSequence b2;
        int i = 0;
        setActionBarSubtitle(ChannelUtils.e(model.getChannel(), requireContext(), false, 2));
        boolean z2 = true;
        CharSequence b3 = ChannelUtils.B(model.getChannel()) ? b.b(requireContext(), R.string.mute_channel, new Object[]{ChannelUtils.e(model.getChannel(), requireContext(), false, 2)}, (r4 & 4) != 0 ? b.C0034b.j : null) : b.b(requireContext(), R.string.mute_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        CharSequence b4 = ChannelUtils.B(model.getChannel()) ? b.b(requireContext(), R.string.unmute_channel, new Object[]{ChannelUtils.e(model.getChannel(), requireContext(), false, 2)}, (r4 & 4) != 0 ? b.C0034b.j : null) : b.b(requireContext(), R.string.unmute_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        int i2 = ChannelUtils.B(model.getChannel()) ? R.string.form_label_mobile_channel_muted_until : R.string.form_label_mobile_category_muted_until;
        CharSequence b5 = ChannelUtils.B(model.getChannel()) ? b.b(requireContext(), R.string.form_label_mobile_channel_override_mute, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null) : b.b(requireContext(), R.string.form_label_mobile_category_override_mute, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        boolean channelIsMuted = model.getChannelIsMuted();
        String channelMuteEndTime = model.getChannelMuteEndTime();
        b2 = b.b(requireContext(), R.string.form_label_mobile_channel_muted, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        NotificationMuteSettingsView.ViewState viewState = new NotificationMuteSettingsView.ViewState(channelIsMuted, channelMuteEndTime, b3, b4, b2, i2, b5);
        WidgetChannelNotificationSettings$configureUI$onMute$1 widgetChannelNotificationSettings$configureUI$onMute$1 = new WidgetChannelNotificationSettings$configureUI$onMute$1(this, model);
        WidgetChannelNotificationSettings$configureUI$onUnmute$1 widgetChannelNotificationSettings$configureUI$onUnmute$1 = new WidgetChannelNotificationSettings$configureUI$onUnmute$1(this, model);
        if (!ChannelUtils.B(model.getChannel()) && !ChannelUtils.k(model.getChannel())) {
            z2 = false;
        }
        NotificationMuteSettingsView notificationMuteSettingsView = getBinding().g;
        notificationMuteSettingsView.setVisibility(z2 ? 0 : 8);
        notificationMuteSettingsView.updateView(viewState, widgetChannelNotificationSettings$configureUI$onMute$1, widgetChannelNotificationSettings$configureUI$onUnmute$1);
        View view = getBinding().f;
        m.checkNotNullExpressionValue(view, "binding.frequencyTopDivider");
        if (!z2) {
            i = 8;
        }
        view.setVisibility(i);
        configureNotificationRadios(model);
    }

    private final WidgetChannelNotificationSettingsBinding getBinding() {
        return (WidgetChannelNotificationSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setRetainInstance(true);
        int i = 0;
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.notification_settings);
        this.notificationSettingsRadioManager = new RadioManager(n.listOf((Object[]) new CheckedSetting[]{getBinding().c, getBinding().d, getBinding().e}));
        LinearLayout linearLayout = getBinding().f2245b;
        m.checkNotNullExpressionValue(linearLayout, "binding.channelNotificationSettingsSystem");
        if (!getMostRecentIntent().getBooleanExtra(INTENT_SHOW_SYSTEM_SETTINGS, false)) {
            i = 8;
        }
        linearLayout.setVisibility(i);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        long longExtra = getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", -1L);
        getBinding().f2245b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetChannelNotificationSettings$onViewBoundOrOnResume$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                NotificationUtils.INSTANCE.showNotificationPage(WidgetChannelNotificationSettings.this);
            }
        });
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(longExtra), this, null, 2, null), WidgetChannelNotificationSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelNotificationSettings$onViewBoundOrOnResume$2(this));
    }
}
