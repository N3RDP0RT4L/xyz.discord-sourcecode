package com.discord.widgets.channels.settings;

import com.discord.app.AppViewModel;
import com.discord.widgets.channels.settings.ChannelGroupDMSettingsViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelGroupDMSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelGroupDMSettings$viewModel$2 extends o implements Function0<AppViewModel<ChannelGroupDMSettingsViewModel.ViewState>> {
    public final /* synthetic */ WidgetChannelGroupDMSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelGroupDMSettings$viewModel$2(WidgetChannelGroupDMSettings widgetChannelGroupDMSettings) {
        super(0);
        this.this$0 = widgetChannelGroupDMSettings;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ChannelGroupDMSettingsViewModel.ViewState> invoke() {
        return new ChannelGroupDMSettingsViewModel(this.this$0.getMostRecentIntent().getLongExtra("INTENT_EXTRA_CHANNEL_ID", 0L), null, null, null, 14, null);
    }
}
