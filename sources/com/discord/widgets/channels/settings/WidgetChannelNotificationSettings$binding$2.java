package com.discord.widgets.channels.settings;

import android.view.View;
import android.widget.LinearLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import com.discord.databinding.WidgetChannelNotificationSettingsBinding;
import com.discord.views.CheckedSetting;
import com.discord.widgets.servers.NotificationMuteSettingsView;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChannelNotificationSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetChannelNotificationSettingsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetChannelNotificationSettingsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetChannelNotificationSettings$binding$2 extends k implements Function1<View, WidgetChannelNotificationSettingsBinding> {
    public static final WidgetChannelNotificationSettings$binding$2 INSTANCE = new WidgetChannelNotificationSettings$binding$2();

    public WidgetChannelNotificationSettings$binding$2() {
        super(1, WidgetChannelNotificationSettingsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetChannelNotificationSettingsBinding;", 0);
    }

    public final WidgetChannelNotificationSettingsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.channel_notification_settings_system;
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.channel_notification_settings_system);
        if (linearLayout != null) {
            i = R.id.channel_settings_notifications_frequency_wrap;
            LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.channel_settings_notifications_frequency_wrap);
            if (linearLayout2 != null) {
                i = R.id.frequency_radio_all;
                CheckedSetting checkedSetting = (CheckedSetting) view.findViewById(R.id.frequency_radio_all);
                if (checkedSetting != null) {
                    i = R.id.frequency_radio_mentions;
                    CheckedSetting checkedSetting2 = (CheckedSetting) view.findViewById(R.id.frequency_radio_mentions);
                    if (checkedSetting2 != null) {
                        i = R.id.frequency_radio_nothing;
                        CheckedSetting checkedSetting3 = (CheckedSetting) view.findViewById(R.id.frequency_radio_nothing);
                        if (checkedSetting3 != null) {
                            i = R.id.frequency_top_divider;
                            View findViewById = view.findViewById(R.id.frequency_top_divider);
                            if (findViewById != null) {
                                i = R.id.mute_settings;
                                NotificationMuteSettingsView notificationMuteSettingsView = (NotificationMuteSettingsView) view.findViewById(R.id.mute_settings);
                                if (notificationMuteSettingsView != null) {
                                    i = R.id.scroll_view;
                                    NestedScrollView nestedScrollView = (NestedScrollView) view.findViewById(R.id.scroll_view);
                                    if (nestedScrollView != null) {
                                        return new WidgetChannelNotificationSettingsBinding((CoordinatorLayout) view, linearLayout, linearLayout2, checkedSetting, checkedSetting2, checkedSetting3, findViewById, notificationMuteSettingsView, nestedScrollView);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
