package com.discord.widgets.channels.settings;

import android.view.View;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.stores.StoreStream;
import com.discord.views.CheckedSetting;
import com.discord.widgets.channels.settings.WidgetChannelNotificationSettings;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Ref$IntRef;
import xyz.discord.R;
/* compiled from: WidgetChannelNotificationSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;", "Lcom/discord/views/CheckedSetting;", "radio", "", "type", "", "invoke", "(Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;Lcom/discord/views/CheckedSetting;I)V", "configureNotificationRadio"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelNotificationSettings$configureNotificationRadios$1 extends o implements Function3<WidgetChannelNotificationSettings.Model, CheckedSetting, Integer, Unit> {
    public final /* synthetic */ Ref$IntRef $notificationSetting;
    public final /* synthetic */ WidgetChannelNotificationSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelNotificationSettings$configureNotificationRadios$1(WidgetChannelNotificationSettings widgetChannelNotificationSettings, Ref$IntRef ref$IntRef) {
        super(3);
        this.this$0 = widgetChannelNotificationSettings;
        this.$notificationSetting = ref$IntRef;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Unit invoke(WidgetChannelNotificationSettings.Model model, CheckedSetting checkedSetting, Integer num) {
        invoke(model, checkedSetting, num.intValue());
        return Unit.a;
    }

    public final void invoke(final WidgetChannelNotificationSettings.Model model, CheckedSetting checkedSetting, final int i) {
        CharSequence e;
        m.checkNotNullParameter(model, "$this$configureNotificationRadio");
        m.checkNotNullParameter(checkedSetting, "radio");
        if (model.isGuildMuted() || model.getChannelIsMuted()) {
            checkedSetting.b(R.string.channel_or_guild_muted);
        } else {
            checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetChannelNotificationSettings$configureNotificationRadios$1.1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StoreStream.Companion.getUserGuildSettings().setChannelFrequency(a.x(view, "it", "it.context"), WidgetChannelNotificationSettings.Model.this.getChannel(), i);
                }
            });
        }
        boolean z2 = false;
        if (!model.isAboveNotifyAllSize() || i != ModelNotificationSettings.FREQUENCY_ALL) {
            int i2 = CheckedSetting.j;
            checkedSetting.h(null, false);
        } else {
            e = b.e(this.this$0, R.string.large_guild_notify_all_messages_description, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            int i3 = CheckedSetting.j;
            checkedSetting.h(e, false);
        }
        if (this.$notificationSetting.element == i) {
            z2 = true;
        }
        checkedSetting.setButtonAlpha((!z2 || !model.getNotificationSettingIsInherited()) ? 1.0f : 0.3f);
        if (z2) {
            WidgetChannelNotificationSettings.access$getNotificationSettingsRadioManager$p(this.this$0).a(checkedSetting);
        }
    }
}
