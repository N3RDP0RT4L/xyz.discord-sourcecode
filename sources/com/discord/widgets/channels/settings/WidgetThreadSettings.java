package com.discord.widgets.channels.settings;

import andhook.lib.HookHelper;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.i.n4;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetThreadSettingsBinding;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.settings.WidgetTextChannelSettings;
import com.discord.widgets.channels.settings.WidgetThreadSettingsViewModel;
import com.discord.widgets.chat.pins.WidgetChannelPinnedMessages;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Iterator;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetThreadSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 &2\u00020\u0001:\u0001&B\u0007¢\u0006\u0004\b%\u0010\u0018J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001d\u0010$\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#¨\u0006'"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState;)V", "Lcom/discord/api/channel/Channel;", "channel", "confirmDelete", "(Lcom/discord/api/channel/Channel;)V", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event;)V", "", "cooldownSecs", "setSlowmodeLabel", "(I)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel;", "viewModel", "Lcom/discord/databinding/WidgetThreadSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetThreadSettingsBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetThreadSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetThreadSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetThreadSettings$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetThreadSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettings$Companion;", "", "", "channelId", "Landroid/content/Context;", "context", "", "launch", "(JLandroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", j);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…RA_CHANNEL_ID, channelId)");
            j.d(context, WidgetThreadSettings.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetThreadSettings() {
        super(R.layout.widget_thread_settings);
        WidgetThreadSettings$viewModel$2 widgetThreadSettings$viewModel$2 = new WidgetThreadSettings$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetThreadSettingsViewModel.class), new WidgetThreadSettings$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetThreadSettings$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetThreadSettingsViewModel.ViewState viewState) {
        if (!(viewState instanceof WidgetThreadSettingsViewModel.ViewState.Valid)) {
            requireActivity().finish();
            return;
        }
        int i = 0;
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.thread_settings);
        WidgetThreadSettingsViewModel.ViewState.Valid valid = (WidgetThreadSettingsViewModel.ViewState.Valid) viewState;
        setActionBarSubtitle(ChannelUtils.e(valid.getChannel(), requireContext(), false, 2));
        setActionBarOptionsMenu(R.menu.menu_text_channel_settings, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.channels.settings.WidgetThreadSettings$configureUI$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_channel_settings_delete) {
                    WidgetThreadSettings.this.confirmDelete(((WidgetThreadSettingsViewModel.ViewState.Valid) viewState).getChannel());
                }
            }
        }, new Action1<Menu>() { // from class: com.discord.widgets.channels.settings.WidgetThreadSettings$configureUI$2
            public final void call(Menu menu) {
                MenuItem findItem = menu.findItem(R.id.menu_channel_settings_delete);
                m.checkNotNullExpressionValue(findItem, "it.findItem(R.id.menu_channel_settings_delete)");
                findItem.setVisible(((WidgetThreadSettingsViewModel.ViewState.Valid) WidgetThreadSettingsViewModel.ViewState.this).getCanManageThread());
                menu.findItem(R.id.menu_channel_settings_delete).setTitle(R.string.delete_thread);
                MenuItem findItem2 = menu.findItem(R.id.menu_channel_settings_reset);
                m.checkNotNullExpressionValue(findItem2, "it.findItem(R.id.menu_channel_settings_reset)");
                findItem2.setVisible(false);
            }
        });
        LinearLayout linearLayout = getBinding().e;
        m.checkNotNullExpressionValue(linearLayout, "binding.threadSettingsEditWrap");
        linearLayout.setVisibility(valid.getCanManageThread() ? 0 : 8);
        String channelNameDraft = valid.getChannelNameDraft();
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.threadSettingsEditName");
        if (!m.areEqual(channelNameDraft, ViewExtensions.getTextOrEmpty(textInputLayout))) {
            TextInputLayout textInputLayout2 = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.threadSettingsEditName");
            ViewExtensions.setText(textInputLayout2, valid.getChannelNameDraft());
        }
        LinearLayout linearLayout2 = getBinding().g;
        m.checkNotNullExpressionValue(linearLayout2, "binding.threadSettingsSectionSlowMode");
        linearLayout2.setVisibility(valid.getCanManageThread() ? 0 : 8);
        int slowModeCooldownDraft = valid.getSlowModeCooldownDraft();
        setSlowmodeLabel(slowModeCooldownDraft);
        Iterator<Integer> it = WidgetTextChannelSettings.Companion.getSLOWMODE_COOLDOWN_VALUES().iterator();
        int i2 = 0;
        while (true) {
            if (!it.hasNext()) {
                i2 = -1;
                break;
            }
            if (it.next().intValue() >= slowModeCooldownDraft) {
                break;
            }
            i2++;
        }
        SeekBar seekBar = getBinding().i;
        m.checkNotNullExpressionValue(seekBar, "binding.threadSettingsSlowModeCooldownSlider");
        seekBar.setProgress(i2);
        SeekBar seekBar2 = getBinding().i;
        m.checkNotNullExpressionValue(seekBar2, "binding.threadSettingsSlowModeCooldownSlider");
        TextView textView = getBinding().h;
        m.checkNotNullExpressionValue(textView, "binding.threadSettingsSlowModeCooldownLabel");
        seekBar2.setContentDescription(textView.getText());
        TextView textView2 = getBinding().f2645b;
        m.checkNotNullExpressionValue(textView2, "binding.channelSettingsPinnedMessages");
        textView2.setEnabled(valid.isPinsEnabled());
        getBinding().f2645b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetThreadSettings$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelPinnedMessages.Companion.show(a.x(view, "it", "it.context"), ((WidgetThreadSettingsViewModel.ViewState.Valid) WidgetThreadSettingsViewModel.ViewState.this).getChannel().h());
            }
        });
        View view = getBinding().c;
        m.checkNotNullExpressionValue(view, "binding.channelSettingsP…edMessagesDisabledOverlay");
        view.setVisibility(true ^ valid.isPinsEnabled() ? 0 : 8);
        getBinding().c.setOnClickListener(WidgetThreadSettings$configureUI$4.INSTANCE);
        FloatingActionButton floatingActionButton = getBinding().f;
        m.checkNotNullExpressionValue(floatingActionButton, "binding.threadSettingsSave");
        if (!valid.getHasUnsavedChanges()) {
            i = 8;
        }
        floatingActionButton.setVisibility(i);
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetThreadSettings$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetThreadSettingsViewModel viewModel;
                viewModel = WidgetThreadSettings.this.getViewModel();
                viewModel.saveThread();
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void confirmDelete(final Channel channel) {
        n4 a = n4.a(LayoutInflater.from(getContext()), null, false);
        m.checkNotNullExpressionValue(a, "WidgetChannelSettingsDel…om(context), null, false)");
        LinearLayout linearLayout = a.a;
        m.checkNotNullExpressionValue(linearLayout, "binding.root");
        final AlertDialog create = new AlertDialog.Builder(linearLayout.getContext()).setView(a.a).create();
        a.e.setText(R.string.delete_thread);
        a.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetThreadSettings$confirmDelete$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                create.dismiss();
            }
        });
        a.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetThreadSettings$confirmDelete$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadSettingsViewModel viewModel;
                viewModel = WidgetThreadSettings.this.getViewModel();
                viewModel.onThreadDeleted(channel.h());
            }
        });
        TextView textView = a.f164b;
        m.checkNotNullExpressionValue(textView, "binding.channelSettingsDeleteBody");
        b.m(textView, R.string.delete_channel_body, new Object[]{ChannelUtils.e(channel, requireContext(), false, 2)}, (r4 & 4) != 0 ? b.g.j : null);
        create.show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetThreadSettingsBinding getBinding() {
        return (WidgetThreadSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetThreadSettingsViewModel getViewModel() {
        return (WidgetThreadSettingsViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(WidgetThreadSettingsViewModel.Event event) {
        if (event instanceof WidgetThreadSettingsViewModel.Event.ShowToast) {
            b.a.d.m.i(this, ((WidgetThreadSettingsViewModel.Event.ShowToast) event).getMessageStringRes(), 0, 4);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setSlowmodeLabel(int i) {
        WidgetTextChannelSettings.Companion companion = WidgetTextChannelSettings.Companion;
        TextView textView = getBinding().h;
        m.checkNotNullExpressionValue(textView, "binding.threadSettingsSlowModeCooldownLabel");
        companion.setDurationSecondsLabel(textView, i, requireContext(), R.string.form_label_slowmode_off);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.threadSettingsEditName");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetThreadSettings$onViewBound$1(this));
        SeekBar seekBar = getBinding().i;
        m.checkNotNullExpressionValue(seekBar, "binding.threadSettingsSlowModeCooldownSlider");
        seekBar.setMax(n.getLastIndex(WidgetTextChannelSettings.Companion.getSLOWMODE_COOLDOWN_VALUES()));
        getBinding().i.setOnSeekBarChangeListener(new b.a.y.j() { // from class: com.discord.widgets.channels.settings.WidgetThreadSettings$onViewBound$2
            @Override // b.a.y.j, android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar2, int i, boolean z2) {
                WidgetThreadSettingsViewModel viewModel;
                WidgetThreadSettingsBinding binding;
                m.checkNotNullParameter(seekBar2, "seekBar");
                super.onProgressChanged(seekBar2, i, z2);
                if (z2) {
                    int intValue = WidgetTextChannelSettings.Companion.getSLOWMODE_COOLDOWN_VALUES().get(i).intValue();
                    WidgetThreadSettings.this.setSlowmodeLabel(intValue);
                    viewModel = WidgetThreadSettings.this.getViewModel();
                    viewModel.onSlowModeInputChanged(intValue);
                    binding = WidgetThreadSettings.this.getBinding();
                    TextView textView = binding.h;
                    m.checkNotNullExpressionValue(textView, "binding.threadSettingsSlowModeCooldownLabel");
                    seekBar2.setContentDescription(textView.getText());
                }
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<WidgetThreadSettingsViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetThreadSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadSettings$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getViewModel().observeEvents(), this, null, 2, null), WidgetThreadSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadSettings$onViewBoundOrOnResume$2(this));
    }
}
