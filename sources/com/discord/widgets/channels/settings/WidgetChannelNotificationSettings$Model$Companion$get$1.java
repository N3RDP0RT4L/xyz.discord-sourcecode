package com.discord.widgets.channels.settings;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.preview.GuildPreview;
import com.discord.models.domain.ModelMuteConfig;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.stores.NotificationTextUtils;
import com.discord.stores.StoreGuildProfiles;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadsJoined;
import com.discord.utilities.channel.ChannelNotificationSettingsUtils;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.channels.settings.WidgetChannelNotificationSettings;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func5;
/* compiled from: WidgetChannelNotificationSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003 \u0004*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/channels/settings/WidgetChannelNotificationSettings$Model;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelNotificationSettings$Model$Companion$get$1<T, R> implements b<Channel, Observable<? extends WidgetChannelNotificationSettings.Model>> {
    public static final WidgetChannelNotificationSettings$Model$Companion$get$1 INSTANCE = new WidgetChannelNotificationSettings$Model$Companion$get$1();

    public final Observable<? extends WidgetChannelNotificationSettings.Model> call(final Channel channel) {
        if (channel == null) {
            return new k(null);
        }
        StoreStream.Companion companion = StoreStream.Companion;
        return Observable.g(companion.getThreadsJoined().observeJoinedThread(channel.h()), companion.getChannels().observeChannel(channel.r()), companion.getGuilds().observeGuild(channel.f()), companion.getUserGuildSettings().observeGuildSettings(channel.f()), companion.getGuildProfiles().observeGuildProfile(channel.f()), new Func5<StoreThreadsJoined.JoinedThread, Channel, Guild, ModelNotificationSettings, StoreGuildProfiles.GuildProfileData, WidgetChannelNotificationSettings.Model>() { // from class: com.discord.widgets.channels.settings.WidgetChannelNotificationSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1
            public final WidgetChannelNotificationSettings.Model call(StoreThreadsJoined.JoinedThread joinedThread, Channel channel2, Guild guild, ModelNotificationSettings modelNotificationSettings, StoreGuildProfiles.GuildProfileData guildProfileData) {
                int i;
                String str;
                GuildPreview data;
                String str2;
                ModelMuteConfig muteConfig;
                Integer num = null;
                if (guild == null) {
                    return null;
                }
                if (ChannelUtils.C(Channel.this)) {
                    NotificationTextUtils notificationTextUtils = NotificationTextUtils.INSTANCE;
                    int computeThreadNotificationSetting = ThreadUtils.INSTANCE.computeThreadNotificationSetting(joinedThread, notificationTextUtils.isGuildOrCategoryOrChannelMuted(modelNotificationSettings, channel2), notificationTextUtils.channelMessageNotificationLevel(modelNotificationSettings, channel2, guild));
                    if (computeThreadNotificationSetting == 4) {
                        i = ModelNotificationSettings.FREQUENCY_MENTIONS;
                    } else if (computeThreadNotificationSetting != 8) {
                        i = ModelNotificationSettings.FREQUENCY_ALL;
                    } else {
                        i = ModelNotificationSettings.FREQUENCY_NOTHING;
                    }
                } else {
                    ChannelNotificationSettingsUtils channelNotificationSettingsUtils = ChannelNotificationSettingsUtils.INSTANCE;
                    Channel channel3 = Channel.this;
                    m.checkNotNullExpressionValue(modelNotificationSettings, "guildSettings");
                    i = channelNotificationSettingsUtils.computeNotificationSetting(guild, channel3, modelNotificationSettings);
                }
                int i2 = i;
                ModelNotificationSettings.ChannelOverride channelOverride = modelNotificationSettings.getChannelOverride(Channel.this.h());
                boolean z2 = !ChannelUtils.C(Channel.this) ? !(channelOverride == null || !channelOverride.isMuted()) : !(joinedThread == null || !joinedThread.getMuted());
                if (ChannelUtils.C(Channel.this)) {
                    if (!(joinedThread == null || (muteConfig = joinedThread.getMuteConfig()) == null)) {
                        str2 = muteConfig.getEndTime();
                        str = str2;
                    }
                    str = null;
                } else {
                    if (channelOverride != null) {
                        str2 = channelOverride.getMuteEndTime();
                        str = str2;
                    }
                    str = null;
                }
                boolean z3 = i2 == ModelNotificationSettings.FREQUENCY_UNSET;
                if (!(guildProfileData == null || (data = guildProfileData.getData()) == null)) {
                    num = data.a();
                }
                boolean z4 = num != null && num.intValue() > 2500;
                Channel channel4 = Channel.this;
                m.checkNotNullExpressionValue(modelNotificationSettings, "guildSettings");
                return new WidgetChannelNotificationSettings.Model(channel4, z2, str, modelNotificationSettings.isMuted(), i2, z3, z4);
            }
        });
    }
}
