package com.discord.widgets.channels.settings;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelNotificationSettings;
import d0.z.d.m;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ChannelSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB!\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0010\u0010\nJ\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0015\u001a\u00020\u00052\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\f\u0010\u0007R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\n¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/channels/settings/ChannelSettings;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "", "component3", "()Ljava/lang/String;", "channel", "isMuted", "muteEndTime", "copy", "(Lcom/discord/api/channel/Channel;ZLjava/lang/String;)Lcom/discord/widgets/channels/settings/ChannelSettings;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Z", "Ljava/lang/String;", "getMuteEndTime", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZLjava/lang/String;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelSettings {
    public static final Companion Companion = new Companion(null);
    private final Channel channel;
    private final boolean isMuted;
    private final String muteEndTime;

    /* compiled from: ChannelSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/settings/ChannelSettings$Companion;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/models/domain/ModelNotificationSettings;", "notificationSettings", "Lcom/discord/widgets/channels/settings/ChannelSettings;", "createFromNotificationSettings", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/domain/ModelNotificationSettings;)Lcom/discord/widgets/channels/settings/ChannelSettings;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ChannelSettings createFromNotificationSettings(Channel channel, ModelNotificationSettings modelNotificationSettings) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(modelNotificationSettings, "notificationSettings");
            boolean isMuted = modelNotificationSettings.isMuted();
            String muteEndTime = modelNotificationSettings.getMuteEndTime();
            Iterator<ModelNotificationSettings.ChannelOverride> it = modelNotificationSettings.getChannelOverrides().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ModelNotificationSettings.ChannelOverride next = it.next();
                m.checkNotNullExpressionValue(next, "override");
                if (next.getChannelId() == channel.h()) {
                    isMuted = next.isMuted();
                    muteEndTime = next.getMuteEndTime();
                    break;
                }
            }
            return new ChannelSettings(channel, isMuted, muteEndTime);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ChannelSettings(Channel channel, boolean z2, String str) {
        m.checkNotNullParameter(channel, "channel");
        this.channel = channel;
        this.isMuted = z2;
        this.muteEndTime = str;
    }

    public static /* synthetic */ ChannelSettings copy$default(ChannelSettings channelSettings, Channel channel, boolean z2, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            channel = channelSettings.channel;
        }
        if ((i & 2) != 0) {
            z2 = channelSettings.isMuted;
        }
        if ((i & 4) != 0) {
            str = channelSettings.muteEndTime;
        }
        return channelSettings.copy(channel, z2, str);
    }

    public final Channel component1() {
        return this.channel;
    }

    public final boolean component2() {
        return this.isMuted;
    }

    public final String component3() {
        return this.muteEndTime;
    }

    public final ChannelSettings copy(Channel channel, boolean z2, String str) {
        m.checkNotNullParameter(channel, "channel");
        return new ChannelSettings(channel, z2, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChannelSettings)) {
            return false;
        }
        ChannelSettings channelSettings = (ChannelSettings) obj;
        return m.areEqual(this.channel, channelSettings.channel) && this.isMuted == channelSettings.isMuted && m.areEqual(this.muteEndTime, channelSettings.muteEndTime);
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final String getMuteEndTime() {
        return this.muteEndTime;
    }

    public int hashCode() {
        Channel channel = this.channel;
        int i = 0;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        boolean z2 = this.isMuted;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode + i2) * 31;
        String str = this.muteEndTime;
        if (str != null) {
            i = str.hashCode();
        }
        return i4 + i;
    }

    public final boolean isMuted() {
        return this.isMuted;
    }

    public String toString() {
        StringBuilder R = a.R("ChannelSettings(channel=");
        R.append(this.channel);
        R.append(", isMuted=");
        R.append(this.isMuted);
        R.append(", muteEndTime=");
        return a.H(R, this.muteEndTime, ")");
    }
}
