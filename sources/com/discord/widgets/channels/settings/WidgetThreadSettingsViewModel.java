package com.discord.widgets.channels.settings;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppViewModel;
import com.discord.stores.StoreChannels;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.threads.ThreadUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: WidgetThreadSettingsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003#$%B\u0013\u0012\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u0011¢\u0006\u0004\b\"\u0010\u0014J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0013\u001a\u00020\u00052\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u0011¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\r\u0010\u0019\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aR\u001d\u0010\u0012\u001a\u00060\u0010j\u0002`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR:\u0010 \u001a&\u0012\f\u0012\n \u001f*\u0004\u0018\u00010\t0\t \u001f*\u0012\u0012\f\u0012\n \u001f*\u0004\u0018\u00010\t0\t\u0018\u00010\u001e0\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!¨\u0006&"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;)V", "Lrx/Observable;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event;", "observeEvents", "()Lrx/Observable;", "", "value", "onChannelNameInputChanged", "(Ljava/lang/String;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "onThreadDeleted", "(J)V", "", "cooldown", "onSlowModeInputChanged", "(I)V", "saveThread", "()V", "J", "getChannelId", "()J", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadSettingsViewModel extends AppViewModel<ViewState> {
    private final long channelId;
    private final PublishSubject<Event> eventSubject = PublishSubject.k0();

    /* compiled from: WidgetThreadSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;", "invoke", "()Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.settings.WidgetThreadSettingsViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<StoreState> {
        public final /* synthetic */ StoreChannels $channelStore;
        public final /* synthetic */ StorePermissions $permissionStore;
        public final /* synthetic */ StoreUser $userStore;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(StoreChannels storeChannels, StoreUser storeUser, StorePermissions storePermissions) {
            super(0);
            this.$channelStore = storeChannels;
            this.$userStore = storeUser;
            this.$permissionStore = storePermissions;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final StoreState invoke() {
            Channel channel = this.$channelStore.getChannel(WidgetThreadSettingsViewModel.this.getChannelId());
            return new StoreState(channel, channel != null ? ThreadUtils.INSTANCE.canManageThread(this.$userStore.getMe(), channel, (Long) a.c(channel, this.$permissionStore.getPermissionsByChannel())) : false);
        }
    }

    /* compiled from: WidgetThreadSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.settings.WidgetThreadSettingsViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            WidgetThreadSettingsViewModel widgetThreadSettingsViewModel = WidgetThreadSettingsViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            widgetThreadSettingsViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetThreadSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event;", "", HookHelper.constructorName, "()V", "ShowToast", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event$ShowToast;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetThreadSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event$ShowToast;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event;", "", "component1", "()I", "messageStringRes", "copy", "(I)Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$Event$ShowToast;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMessageStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowToast extends Event {
            private final int messageStringRes;

            public ShowToast(@StringRes int i) {
                super(null);
                this.messageStringRes = i;
            }

            public static /* synthetic */ ShowToast copy$default(ShowToast showToast, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showToast.messageStringRes;
                }
                return showToast.copy(i);
            }

            public final int component1() {
                return this.messageStringRes;
            }

            public final ShowToast copy(@StringRes int i) {
                return new ShowToast(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowToast) && this.messageStringRes == ((ShowToast) obj).messageStringRes;
                }
                return true;
            }

            public final int getMessageStringRes() {
                return this.messageStringRes;
            }

            public int hashCode() {
                return this.messageStringRes;
            }

            public String toString() {
                return a.A(a.R("ShowToast(messageStringRes="), this.messageStringRes, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "channel", "canManageThread", "copy", "(Lcom/discord/api/channel/Channel;Z)Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Z", "getCanManageThread", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final boolean canManageThread;
        private final Channel channel;

        public StoreState(Channel channel, boolean z2) {
            this.channel = channel;
            this.canManageThread = z2;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Channel channel, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                channel = storeState.channel;
            }
            if ((i & 2) != 0) {
                z2 = storeState.canManageThread;
            }
            return storeState.copy(channel, z2);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final boolean component2() {
            return this.canManageThread;
        }

        public final StoreState copy(Channel channel, boolean z2) {
            return new StoreState(channel, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.channel, storeState.channel) && this.canManageThread == storeState.canManageThread;
        }

        public final boolean getCanManageThread() {
            return this.canManageThread;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            boolean z2 = this.canManageThread;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(channel=");
            R.append(this.channel);
            R.append(", canManageThread=");
            return a.M(R, this.canManageThread, ")");
        }
    }

    /* compiled from: WidgetThreadSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState$Valid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetThreadSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetThreadSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\u000b\u0012\u0006\u0010\u0014\u001a\u00020\u000b\u0012\u0006\u0010\u0015\u001a\u00020\u000b¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJN\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0010\u001a\u00020\u00022\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0012\u001a\u00020\b2\b\b\u0002\u0010\u0013\u001a\u00020\u000b2\b\b\u0002\u0010\u0014\u001a\u00020\u000b2\b\b\u0002\u0010\u0015\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0007J\u0010\u0010\u0019\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0019\u0010\nJ\u001a\u0010\u001c\u001a\u00020\u000b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\u0007R\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\nR\u0019\u0010\u0014\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010$\u001a\u0004\b%\u0010\rR\u0019\u0010\u0013\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b&\u0010\rR\u0019\u0010\u0015\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b\u0015\u0010\r¨\u0006)"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState$Valid;", "Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Ljava/lang/String;", "", "component3", "()I", "", "component4", "()Z", "component5", "component6", "channel", "channelNameDraft", "slowModeCooldownDraft", "hasUnsavedChanges", "canManageThread", "isPinsEnabled", "copy", "(Lcom/discord/api/channel/Channel;Ljava/lang/String;IZZZ)Lcom/discord/widgets/channels/settings/WidgetThreadSettingsViewModel$ViewState$Valid;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/lang/String;", "getChannelNameDraft", "I", "getSlowModeCooldownDraft", "Z", "getCanManageThread", "getHasUnsavedChanges", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/lang/String;IZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends ViewState {
            private final boolean canManageThread;
            private final Channel channel;
            private final String channelNameDraft;
            private final boolean hasUnsavedChanges;
            private final boolean isPinsEnabled;
            private final int slowModeCooldownDraft;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Valid(Channel channel, String str, int i, boolean z2, boolean z3, boolean z4) {
                super(null);
                m.checkNotNullParameter(channel, "channel");
                this.channel = channel;
                this.channelNameDraft = str;
                this.slowModeCooldownDraft = i;
                this.hasUnsavedChanges = z2;
                this.canManageThread = z3;
                this.isPinsEnabled = z4;
            }

            public static /* synthetic */ Valid copy$default(Valid valid, Channel channel, String str, int i, boolean z2, boolean z3, boolean z4, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    channel = valid.channel;
                }
                if ((i2 & 2) != 0) {
                    str = valid.channelNameDraft;
                }
                String str2 = str;
                if ((i2 & 4) != 0) {
                    i = valid.slowModeCooldownDraft;
                }
                int i3 = i;
                if ((i2 & 8) != 0) {
                    z2 = valid.hasUnsavedChanges;
                }
                boolean z5 = z2;
                if ((i2 & 16) != 0) {
                    z3 = valid.canManageThread;
                }
                boolean z6 = z3;
                if ((i2 & 32) != 0) {
                    z4 = valid.isPinsEnabled;
                }
                return valid.copy(channel, str2, i3, z5, z6, z4);
            }

            public final Channel component1() {
                return this.channel;
            }

            public final String component2() {
                return this.channelNameDraft;
            }

            public final int component3() {
                return this.slowModeCooldownDraft;
            }

            public final boolean component4() {
                return this.hasUnsavedChanges;
            }

            public final boolean component5() {
                return this.canManageThread;
            }

            public final boolean component6() {
                return this.isPinsEnabled;
            }

            public final Valid copy(Channel channel, String str, int i, boolean z2, boolean z3, boolean z4) {
                m.checkNotNullParameter(channel, "channel");
                return new Valid(channel, str, i, z2, z3, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.channel, valid.channel) && m.areEqual(this.channelNameDraft, valid.channelNameDraft) && this.slowModeCooldownDraft == valid.slowModeCooldownDraft && this.hasUnsavedChanges == valid.hasUnsavedChanges && this.canManageThread == valid.canManageThread && this.isPinsEnabled == valid.isPinsEnabled;
            }

            public final boolean getCanManageThread() {
                return this.canManageThread;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final String getChannelNameDraft() {
                return this.channelNameDraft;
            }

            public final boolean getHasUnsavedChanges() {
                return this.hasUnsavedChanges;
            }

            public final int getSlowModeCooldownDraft() {
                return this.slowModeCooldownDraft;
            }

            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                String str = this.channelNameDraft;
                if (str != null) {
                    i = str.hashCode();
                }
                int i2 = (((hashCode + i) * 31) + this.slowModeCooldownDraft) * 31;
                boolean z2 = this.hasUnsavedChanges;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.canManageThread;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.isPinsEnabled;
                if (!z4) {
                    i3 = z4 ? 1 : 0;
                }
                return i9 + i3;
            }

            public final boolean isPinsEnabled() {
                return this.isPinsEnabled;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(channel=");
                R.append(this.channel);
                R.append(", channelNameDraft=");
                R.append(this.channelNameDraft);
                R.append(", slowModeCooldownDraft=");
                R.append(this.slowModeCooldownDraft);
                R.append(", hasUnsavedChanges=");
                R.append(this.hasUnsavedChanges);
                R.append(", canManageThread=");
                R.append(this.canManageThread);
                R.append(", isPinsEnabled=");
                return a.M(R, this.isPinsEnabled, ")");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetThreadSettingsViewModel(long j) {
        super(null, 1, null);
        this.channelId = j;
        StoreStream.Companion companion = StoreStream.Companion;
        StoreChannels channels = companion.getChannels();
        StorePermissions permissions = companion.getPermissions();
        StoreUser users = companion.getUsers();
        Observable q = ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{channels, permissions, users}, false, null, null, new AnonymousClass1(channels, users, permissions), 14, null).q();
        m.checkNotNullExpressionValue(q, "ObservationDeckProvider.…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), WidgetThreadSettingsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        Channel channel = storeState.getChannel();
        String str = null;
        if ((channel != null ? channel.m() : null) == null || !ChannelUtils.C(channel)) {
            updateViewState(ViewState.Invalid.INSTANCE);
            return;
        }
        boolean z2 = !channel.o() || StoreStream.Companion.getGuildsNsfw().isGuildNsfwGateAgreed(channel.f());
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            str = valid.getChannelNameDraft();
        }
        if (str != null) {
            updateViewState(ViewState.Valid.copy$default(valid, channel, null, 0, false, false, false, 62, null));
        } else {
            updateViewState(new ViewState.Valid(channel, channel.m(), channel.u(), false, storeState.getCanManageThread(), z2));
        }
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @MainThread
    public final void onChannelNameInputChanged(String str) {
        m.checkNotNullParameter(str, "value");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            updateViewState(ViewState.Valid.copy$default(valid, null, str, 0, true, false, false, 53, null));
        }
    }

    @MainThread
    public final void onSlowModeInputChanged(int i) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            updateViewState(ViewState.Valid.copy$default(valid, null, null, i, true, false, false, 51, null));
        }
    }

    public final void onThreadDeleted(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteChannel(j), false, 1, null), this, null, 2, null), WidgetThreadSettingsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetThreadSettingsViewModel$onThreadDeleted$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadSettingsViewModel$onThreadDeleted$1(this));
    }

    public final void saveThread() {
        Observable editTextChannel;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            editTextChannel = RestAPI.Companion.getApi().editTextChannel(this.channelId, (r20 & 2) != 0 ? null : m.areEqual(valid.getChannel().m(), valid.getChannelNameDraft()) ^ true ? valid.getChannelNameDraft() : null, (r20 & 4) != 0 ? null : null, (r20 & 8) != 0 ? null : null, (r20 & 16) != 0 ? null : null, (r20 & 32) != 0 ? null : valid.getChannel().u() != valid.getSlowModeCooldownDraft() ? Integer.valueOf(valid.getSlowModeCooldownDraft()) : null, (r20 & 64) != 0 ? null : null);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(editTextChannel, this, null, 2, null), WidgetThreadSettingsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetThreadSettingsViewModel$saveThread$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadSettingsViewModel$saveThread$1(this));
        }
    }
}
