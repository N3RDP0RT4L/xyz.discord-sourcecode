package com.discord.widgets.channels.settings;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.widgets.channels.settings.WidgetThreadSettingsViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: WidgetThreadSettingsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadSettingsViewModel$onThreadDeleted$1 extends o implements Function1<Channel, Unit> {
    public final /* synthetic */ WidgetThreadSettingsViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetThreadSettingsViewModel$onThreadDeleted$1(WidgetThreadSettingsViewModel widgetThreadSettingsViewModel) {
        super(1);
        this.this$0 = widgetThreadSettingsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
        invoke2(channel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Channel channel) {
        PublishSubject publishSubject;
        m.checkNotNullParameter(channel, "channel");
        Integer b2 = ChannelUtils.b(channel);
        if (b2 != null) {
            publishSubject = this.this$0.eventSubject;
            publishSubject.k.onNext(new WidgetThreadSettingsViewModel.Event.ShowToast(b2.intValue()));
        }
    }
}
