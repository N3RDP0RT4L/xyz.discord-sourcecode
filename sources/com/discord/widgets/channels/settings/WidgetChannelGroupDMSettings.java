package com.discord.widgets.channels.settings;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelGroupDmSettingsBinding;
import com.discord.dialogs.ImageUploadDialog;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.settings.ChannelGroupDMSettingsViewModel;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.servers.NotificationMuteSettingsView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetChannelGroupDMSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 72\u00020\u0001:\u00017B\u0007¢\u0006\u0004\b6\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\n\u0010\u000bJ-\u0010\u0012\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\u000bJ\u0015\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u001f\u0010!\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u001fH\u0016¢\u0006\u0004\b!\u0010\"J\u001f\u0010#\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u001fH\u0016¢\u0006\u0004\b#\u0010\"R\"\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020\u00040$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u001d\u0010/\u001a\u00020*8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u001d\u00105\u001a\u0002008B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104¨\u00068"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetChannelGroupDMSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel$ViewState;", "viewState", "", "configureUi", "(Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel$ViewState;)V", "Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel$ViewState$Valid;", "configureIcon", "(Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel$ViewState$Valid;)V", "handleSettingsSaved", "()V", "Landroid/content/Context;", "context", "Lkotlin/Function0;", "confirmed", "", "groupName", "confirmLeave", "(Landroid/content/Context;Lkotlin/jvm/functions/Function0;Ljava/lang/CharSequence;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel$Event;)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "mimeType", "onImageChosen", "(Landroid/net/Uri;Ljava/lang/String;)V", "onImageCropped", "Lkotlin/Function1;", "iconEditedResult", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/channels/settings/ChannelGroupDMSettingsViewModel;", "viewModel", "Lcom/discord/databinding/WidgetChannelGroupDmSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelGroupDmSettingsBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelGroupDMSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelGroupDMSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelGroupDmSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelGroupDMSettings$binding$2.INSTANCE, null, 2, null);
    private Function1<? super String, Unit> iconEditedResult = WidgetChannelGroupDMSettings$iconEditedResult$1.INSTANCE;
    private final StatefulViews state = new StatefulViews(R.id.channel_settings_edit_name, R.id.settings_group_icon);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetChannelGroupDMSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/channels/settings/WidgetChannelGroupDMSettings$Companion;", "", "", "channelId", "Landroid/content/Context;", "context", "", "create", "(JLandroid/content/Context;)V", "", WidgetChannelGroupDMSettings.INTENT_EXTRA_CHANNEL_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(long j, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra(WidgetChannelGroupDMSettings.INTENT_EXTRA_CHANNEL_ID, j);
            m.checkNotNullExpressionValue(putExtra, "Intent().putExtra(INTENT…RA_CHANNEL_ID, channelId)");
            j.d(context, WidgetChannelGroupDMSettings.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChannelGroupDMSettings() {
        super(R.layout.widget_channel_group_dm_settings);
        WidgetChannelGroupDMSettings$viewModel$2 widgetChannelGroupDMSettings$viewModel$2 = new WidgetChannelGroupDMSettings$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ChannelGroupDMSettingsViewModel.class), new WidgetChannelGroupDMSettings$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetChannelGroupDMSettings$viewModel$2));
    }

    private final void configureIcon(ChannelGroupDMSettingsViewModel.ViewState.Valid valid) {
        String currentIconUrl = valid.getCurrentIconUrl();
        String str = "";
        if (valid.getHasUnsavedIconChange()) {
            StatefulViews statefulViews = this.state;
            SimpleDraweeView simpleDraweeView = getBinding().f;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.settingsGroupIcon");
            int id2 = simpleDraweeView.getId();
            if (currentIconUrl != null) {
                str = currentIconUrl;
            }
            statefulViews.put(id2, str);
        } else {
            StatefulViews statefulViews2 = this.state;
            SimpleDraweeView simpleDraweeView2 = getBinding().f;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.settingsGroupIcon");
            int id3 = simpleDraweeView2.getId();
            if (currentIconUrl != null) {
                str = currentIconUrl;
            }
            statefulViews2.get(id3, str);
        }
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetChannelGroupDMSettings$configureIcon$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelGroupDMSettings.this.openMediaChooser();
            }
        });
        this.iconEditedResult = new WidgetChannelGroupDMSettings$configureIcon$2(this);
        SimpleDraweeView simpleDraweeView3 = getBinding().f;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.settingsGroupIcon");
        IconUtils.setIcon$default(simpleDraweeView3, currentIconUrl, (int) R.dimen.avatar_size_xxlarge, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        TextView textView = getBinding().g;
        m.checkNotNullExpressionValue(textView, "binding.settingsGroupIconLabel");
        int i = 0;
        textView.setVisibility(valid.isDefaultPhoto() ? 0 : 8);
        TextView textView2 = getBinding().h;
        m.checkNotNullExpressionValue(textView2, "binding.settingsGroupIconRemove");
        if (!(!valid.isDefaultPhoto())) {
            i = 8;
        }
        textView2.setVisibility(i);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetChannelGroupDMSettings$configureIcon$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChannelGroupDMSettingsViewModel viewModel;
                viewModel = WidgetChannelGroupDMSettings.this.getViewModel();
                viewModel.removeEditedIcon();
            }
        });
        this.state.configureSaveActionView(getBinding().c);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(ChannelGroupDMSettingsViewModel.ViewState viewState) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        if (m.areEqual(viewState, ChannelGroupDMSettingsViewModel.ViewState.Invalid.INSTANCE)) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
            }
        } else if (viewState instanceof ChannelGroupDMSettingsViewModel.ViewState.Valid) {
            ChannelGroupDMSettingsViewModel.ViewState.Valid valid = (ChannelGroupDMSettingsViewModel.ViewState.Valid) viewState;
            ChannelSettings channelSettings = valid.getChannelSettings();
            final Channel channel = channelSettings.getChannel();
            final String d = ChannelUtils.d(channel, requireContext(), true);
            setActionBarTitle(R.string.channel_settings);
            setActionBarSubtitle(d);
            AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
            AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_main_group_settings, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.channels.settings.WidgetChannelGroupDMSettings$configureUi$1

                /* compiled from: WidgetChannelGroupDMSettings.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.settings.WidgetChannelGroupDMSettings$configureUi$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function0<Unit> {
                    public AnonymousClass1() {
                        super(0);
                    }

                    @Override // kotlin.jvm.functions.Function0
                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2() {
                        ChannelGroupDMSettingsViewModel viewModel;
                        viewModel = WidgetChannelGroupDMSettings.this.getViewModel();
                        viewModel.leaveGroup();
                    }
                }

                public final void call(MenuItem menuItem, Context context) {
                    m.checkNotNullExpressionValue(menuItem, "menuItem");
                    if (menuItem.getItemId() == R.id.menu_leave_group) {
                        WidgetChannelGroupDMSettings widgetChannelGroupDMSettings = WidgetChannelGroupDMSettings.this;
                        widgetChannelGroupDMSettings.confirmLeave(widgetChannelGroupDMSettings.requireContext(), new AnonymousClass1(), d);
                    }
                }
            }, null, 4, null);
            TextInputLayout textInputLayout = getBinding().f2240b;
            m.checkNotNullExpressionValue(textInputLayout, "binding.channelSettingsEditName");
            StatefulViews statefulViews = this.state;
            TextInputLayout textInputLayout2 = getBinding().f2240b;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.channelSettingsEditName");
            ViewExtensions.setText(textInputLayout, (CharSequence) statefulViews.get(textInputLayout2.getId(), d));
            TextInputLayout textInputLayout3 = getBinding().f2240b;
            m.checkNotNullExpressionValue(textInputLayout3, "binding.channelSettingsEditName");
            ViewExtensions.setSelectionEnd(textInputLayout3);
            configureIcon(valid);
            this.state.configureSaveActionView(getBinding().c);
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.settings.WidgetChannelGroupDMSettings$configureUi$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ChannelGroupDMSettingsViewModel viewModel;
                    StatefulViews statefulViews2;
                    WidgetChannelGroupDmSettingsBinding binding;
                    viewModel = WidgetChannelGroupDMSettings.this.getViewModel();
                    long h = channel.h();
                    statefulViews2 = WidgetChannelGroupDMSettings.this.state;
                    binding = WidgetChannelGroupDMSettings.this.getBinding();
                    TextInputLayout textInputLayout4 = binding.f2240b;
                    m.checkNotNullExpressionValue(textInputLayout4, "binding.channelSettingsEditName");
                    viewModel.editGroup(h, ((String) statefulViews2.get(textInputLayout4.getId(), d)).toString());
                }
            });
            long h = channel.h();
            boolean isMuted = channelSettings.isMuted();
            String muteEndTime = channelSettings.getMuteEndTime();
            e = b.e(this, R.string.mute_conversation, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            e2 = b.e(this, R.string.unmute_conversation, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            e3 = b.e(this, R.string.form_label_mobile_dm_muted, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            getBinding().d.updateView(new NotificationMuteSettingsView.ViewState(isMuted, muteEndTime, e, e2, e3, R.string.form_label_mobile_dm_muted_until, null), new WidgetChannelGroupDMSettings$configureUi$3(this, h), new WidgetChannelGroupDMSettings$configureUi$4(this));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void confirmLeave(Context context, Function0<Unit> function0, CharSequence charSequence) {
        CharSequence e;
        CharSequence e2;
        WidgetNoticeDialog.Builder builder = new WidgetNoticeDialog.Builder(context);
        e = b.e(this, R.string.leave_group_dm_title, new Object[]{charSequence}, (r4 & 4) != 0 ? b.a.j : null);
        WidgetNoticeDialog.Builder title = builder.setTitle(e);
        e2 = b.e(this, R.string.leave_group_dm_body, new Object[]{charSequence}, (r4 & 4) != 0 ? b.a.j : null);
        WidgetNoticeDialog.Builder negativeButton$default = WidgetNoticeDialog.Builder.setNegativeButton$default(title.setMessage(e2).setDialogAttrTheme(R.attr.notice_theme_positive_red).setPositiveButton(R.string.leave_group_dm, new WidgetChannelGroupDMSettings$confirmLeave$1(function0)), (int) R.string.cancel, (Function1) null, 2, (Object) null);
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        negativeButton$default.show(parentFragmentManager);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChannelGroupDmSettingsBinding getBinding() {
        return (WidgetChannelGroupDmSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ChannelGroupDMSettingsViewModel getViewModel() {
        return (ChannelGroupDMSettingsViewModel) this.viewModel$delegate.getValue();
    }

    private final void handleSettingsSaved() {
        b.a.d.m.i(this, R.string.saved_settings, 0, 4);
        StatefulViews.clear$default(this.state, false, 1, null);
        AppFragment.hideKeyboard$default(this, null, 1, null);
        getBinding().e.fullScroll(33);
    }

    public final void handleEvent(ChannelGroupDMSettingsViewModel.Event event) {
        m.checkNotNullParameter(event, "event");
        if (m.areEqual(event, ChannelGroupDMSettingsViewModel.Event.LeaveGroupSuccess.INSTANCE)) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
            }
        } else if (m.areEqual(event, ChannelGroupDMSettingsViewModel.Event.SettingsSaved.INSTANCE)) {
            handleSettingsSaved();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v5, types: [com.discord.widgets.channels.settings.WidgetChannelGroupDMSettings$sam$rx_functions_Action1$0] */
    @Override // com.discord.app.AppFragment
    public void onImageChosen(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageChosen(uri, str);
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Function1<? super String, Unit> function1 = this.iconEditedResult;
        Function1<? super String, Unit> function12 = function1;
        if (function1 != null) {
            function12 = new WidgetChannelGroupDMSettings$sam$rx_functions_Action1$0(function1);
        }
        MGImages.prepareImageUpload(uri, str, parentFragmentManager, this, (Action1) function12, ImageUploadDialog.PreviewType.GUILD_AVATAR);
    }

    @Override // com.discord.app.AppFragment
    public void onImageCropped(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageCropped(uri, str);
        Context context = getContext();
        Function1<? super String, Unit> function1 = this.iconEditedResult;
        Object obj = function1;
        if (function1 != null) {
            obj = new WidgetChannelGroupDMSettings$sam$rx_functions_Action1$0(function1);
        }
        MGImages.requestDataUrl(context, uri, str, (Action1) obj);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        CharSequence e;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        TextView textView = getBinding().g;
        m.checkNotNullExpressionValue(textView, "binding.settingsGroupIconLabel");
        e = b.e(this, R.string.minimum_size, new Object[]{"128", "128"}, (r4 & 4) != 0 ? b.a.j : null);
        textView.setText(e);
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        FloatingActionButton floatingActionButton = getBinding().c;
        TextInputLayout textInputLayout = getBinding().f2240b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.channelSettingsEditName");
        statefulViews.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetChannelGroupDMSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelGroupDMSettings$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetChannelGroupDMSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelGroupDMSettings$onViewBoundOrOnResume$2(this));
    }
}
