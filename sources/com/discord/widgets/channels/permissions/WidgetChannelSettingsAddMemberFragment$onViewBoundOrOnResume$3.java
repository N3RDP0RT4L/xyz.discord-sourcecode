package com.discord.widgets.channels.permissions;

import com.discord.api.permission.PermissionOverwrite;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetChannelSettingsAddMemberFragment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", ModelAuditLogEntry.CHANGE_KEY_ID, "Lcom/discord/api/permission/PermissionOverwrite$Type;", "type", "", "invoke", "(JLcom/discord/api/permission/PermissionOverwrite$Type;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsAddMemberFragment$onViewBoundOrOnResume$3 extends o implements Function2<Long, PermissionOverwrite.Type, Unit> {
    public final /* synthetic */ WidgetChannelSettingsAddMemberFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsAddMemberFragment$onViewBoundOrOnResume$3(WidgetChannelSettingsAddMemberFragment widgetChannelSettingsAddMemberFragment) {
        super(2);
        this.this$0 = widgetChannelSettingsAddMemberFragment;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Long l, PermissionOverwrite.Type type) {
        invoke(l.longValue(), type);
        return Unit.a;
    }

    public final void invoke(long j, PermissionOverwrite.Type type) {
        WidgetChannelSettingsAddMemberFragmentViewModel viewModel;
        m.checkNotNullParameter(type, "type");
        viewModel = this.this$0.getViewModel();
        viewModel.toggleItem(j, type);
    }
}
