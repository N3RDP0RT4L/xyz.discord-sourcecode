package com.discord.widgets.channels.permissions;

import com.discord.api.channel.Channel;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.widgets.channels.SimpleMembersAdapter;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAdvanced;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetChannelSettingsPermissionsAdvanced.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model;", "invoke", "()Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1 extends o implements Function0<WidgetChannelSettingsPermissionsAdvanced.Model> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreChannels $storeChannels;
    public final /* synthetic */ StoreGuilds $storeGuilds;
    public final /* synthetic */ StorePermissions $storePermissions;
    public final /* synthetic */ StoreUser $storeUser;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1(StoreChannels storeChannels, long j, StoreGuilds storeGuilds, StoreUser storeUser, StorePermissions storePermissions) {
        super(0);
        this.$storeChannels = storeChannels;
        this.$channelId = j;
        this.$storeGuilds = storeGuilds;
        this.$storeUser = storeUser;
        this.$storePermissions = storePermissions;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetChannelSettingsPermissionsAdvanced.Model invoke() {
        Channel channel = this.$storeChannels.getChannel(this.$channelId);
        if (channel == null) {
            return null;
        }
        Collection overwriteIds = WidgetChannelSettingsPermissionsAdvanced.Model.Companion.getOverwriteIds(channel, PermissionOverwrite.Type.MEMBER);
        long f = channel.f();
        Guild guild = this.$storeGuilds.getGuild(f);
        Map<Long, GuildRole> map = this.$storeGuilds.getRoles().get(Long.valueOf(f));
        if (map == null) {
            map = h0.emptyMap();
        }
        Map<Long, GuildRole> map2 = map;
        MeUser me2 = this.$storeUser.getMe();
        Long l = this.$storePermissions.getPermissionsByChannel().get(Long.valueOf(this.$channelId));
        Map<Long, User> users = this.$storeUser.getUsers();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, User> entry : users.entrySet()) {
            if (overwriteIds.contains(Long.valueOf(entry.getKey().longValue()))) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        Collection values = linkedHashMap.values();
        final WidgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1$sortedUsers$2 widgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1$sortedUsers$2 = WidgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1$sortedUsers$2.INSTANCE;
        Object obj = widgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1$sortedUsers$2;
        if (widgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1$sortedUsers$2 != null) {
            obj = new Comparator() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAdvanced$sam$java_util_Comparator$0
                @Override // java.util.Comparator
                public final /* synthetic */ int compare(Object obj2, Object obj3) {
                    Object invoke = Function2.this.invoke(obj2, obj3);
                    m.checkNotNullExpressionValue(invoke, "invoke(...)");
                    return ((Number) invoke).intValue();
                }
            };
        }
        List<User> sortedWith = u.sortedWith(values, (Comparator) obj);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(sortedWith, 10));
        for (User user : sortedWith) {
            arrayList.add(new SimpleMembersAdapter.MemberItem(user, this.$storeGuilds.getMember(f, user.getId())));
        }
        if (!WidgetChannelSettingsPermissionsAdvanced.Model.Companion.isValid(me2, guild, channel, l)) {
            return null;
        }
        m.checkNotNull(guild);
        m.checkNotNull(l);
        return new WidgetChannelSettingsPermissionsAdvanced.Model(me2, guild, channel, l.longValue(), map2, arrayList);
    }
}
