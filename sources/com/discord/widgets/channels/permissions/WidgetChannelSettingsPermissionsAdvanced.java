package com.discord.widgets.channels.permissions;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.view.View;
import android.widget.LinearLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelSettingsPermissionsAdvancedBinding;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.SimpleMembersAdapter;
import com.discord.widgets.channels.SimpleRolesAdapter;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetChannelSettingsPermissionsAdvanced.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001cB\u0007¢\u0006\u0004\b\u001b\u0010\u000eJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001a\u001a\u00020\u00158B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model;)V", "configureRoles", "configureMembers", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/channels/SimpleRolesAdapter;", "rolesAdapter", "Lcom/discord/widgets/channels/SimpleRolesAdapter;", "Lcom/discord/widgets/channels/SimpleMembersAdapter;", "membersAdapter", "Lcom/discord/widgets/channels/SimpleMembersAdapter;", "Lcom/discord/databinding/WidgetChannelSettingsPermissionsAdvancedBinding;", "viewBinding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getViewBinding", "()Lcom/discord/databinding/WidgetChannelSettingsPermissionsAdvancedBinding;", "viewBinding", HookHelper.constructorName, ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsPermissionsAdvanced extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelSettingsPermissionsAdvanced.class, "viewBinding", "getViewBinding()Lcom/discord/databinding/WidgetChannelSettingsPermissionsAdvancedBinding;", 0)};
    private SimpleMembersAdapter membersAdapter;
    private SimpleRolesAdapter rolesAdapter;
    private final FragmentViewBindingDelegate viewBinding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelSettingsPermissionsAdvanced$viewBinding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetChannelSettingsPermissionsAdvanced.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u001a\b\u0082\b\u0018\u0000 @2\u00020\u0001:\u0001@BI\u0012\u0006\u0010\u0018\u001a\u00020\u0006\u0012\u0006\u0010\u0019\u001a\u00020\t\u0012\u0006\u0010\u001a\u001a\u00020\f\u0012\u0006\u0010\u001b\u001a\u00020\u000f\u0012\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00130\u0012\u0012\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00160\u0002¢\u0006\u0004\b>\u0010?J\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u001c\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00130\u0012HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0002HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0005J^\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00062\b\b\u0002\u0010\u0019\u001a\u00020\t2\b\b\u0002\u0010\u001a\u001a\u00020\f2\b\b\u0002\u0010\u001b\u001a\u00020\u000f2\u0014\b\u0002\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00130\u00122\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00160\u0002HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010(\u001a\u00020'2\b\u0010&\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b(\u0010)R\u0019\u0010\u0019\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b+\u0010\u000bR\u0019\u0010\u0018\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010,\u001a\u0004\b-\u0010\bR%\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010.\u001a\u0004\b/\u0010\u0015R\u0019\u0010\u001b\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00100\u001a\u0004\b1\u0010\u0011R\u001f\u00102\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u0010\u0005R\u001f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00160\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00103\u001a\u0004\b5\u0010\u0005R\u0019\u0010\u001a\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00106\u001a\u0004\b7\u0010\u000eR\u0019\u00108\u001a\u00020'8\u0006@\u0006¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010;R\u0019\u0010<\u001a\u00020'8\u0006@\u0006¢\u0006\f\n\u0004\b<\u00109\u001a\u0004\b=\u0010;¨\u0006A"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model;", "", "", "Lcom/discord/widgets/channels/SimpleRolesAdapter$RoleItem;", "buildRoleItems", "()Ljava/util/List;", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "", "component4", "()J", "", "Lcom/discord/api/role/GuildRole;", "component5", "()Ljava/util/Map;", "Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;", "component6", "me", "guild", "channel", "myPermissions", "guildRoles", "memberItems", "copy", "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;JLjava/util/Map;Ljava/util/List;)Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/user/MeUser;", "getMe", "Ljava/util/Map;", "getGuildRoles", "J", "getMyPermissions", "roleItems", "Ljava/util/List;", "getRoleItems", "getMemberItems", "Lcom/discord/api/channel/Channel;", "getChannel", "canAddRole", "Z", "getCanAddRole", "()Z", "canManage", "getCanManage", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;JLjava/util/Map;Ljava/util/List;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canAddRole;
        private final boolean canManage;
        private final Channel channel;
        private final Guild guild;
        private final Map<Long, GuildRole> guildRoles;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2821me;
        private final List<SimpleMembersAdapter.MemberItem> memberItems;
        private final long myPermissions;
        private final List<SimpleRolesAdapter.RoleItem> roleItems;

        /* compiled from: WidgetChannelSettingsPermissionsAdvanced.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J%\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ$\u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\f2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\nH\u0086\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ5\u0010\u0016\u001a\u00020\u00152\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0014\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model$Companion;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/permission/PermissionOverwrite$Type;", "type", "", "", "getOverwriteIds", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/permission/PermissionOverwrite$Type;)Ljava/util/Collection;", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsPermissionsAdvanced$Model;", "get", "(J)Lrx/Observable;", "Lcom/discord/models/user/MeUser;", "me", "Lcom/discord/models/guild/Guild;", "guild", "myPermissionsForChannel", "", "isValid", "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Collection<Long> getOverwriteIds(Channel channel, PermissionOverwrite.Type type) {
                List<PermissionOverwrite> s2 = channel.s();
                if (s2 == null) {
                    return n.emptyList();
                }
                ArrayList<PermissionOverwrite> arrayList = new ArrayList();
                for (Object obj : s2) {
                    if (((PermissionOverwrite) obj).f() == type) {
                        arrayList.add(obj);
                    }
                }
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
                for (PermissionOverwrite permissionOverwrite : arrayList) {
                    arrayList2.add(Long.valueOf(permissionOverwrite.e()));
                }
                return arrayList2;
            }

            public final Observable<Model> get(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                StoreChannels channels = companion.getChannels();
                StoreUser users = companion.getUsers();
                StorePermissions permissions = companion.getPermissions();
                StoreGuilds guilds = companion.getGuilds();
                return ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{channels, users, permissions, guilds}, false, null, null, new WidgetChannelSettingsPermissionsAdvanced$Model$Companion$get$1(channels, j, guilds, users, permissions), 14, null);
            }

            public final boolean isValid(MeUser meUser, Guild guild, Channel channel, Long l) {
                return (meUser == null || guild == null || channel == null || l == null) ? false : true;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(MeUser meUser, Guild guild, Channel channel, long j, Map<Long, GuildRole> map, List<SimpleMembersAdapter.MemberItem> list) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "guildRoles");
            m.checkNotNullParameter(list, "memberItems");
            this.f2821me = meUser;
            this.guild = guild;
            this.channel = channel;
            this.myPermissions = j;
            this.guildRoles = map;
            this.memberItems = list;
            boolean z2 = false;
            this.canManage = guild.getOwnerId() == meUser.getId() || PermissionUtils.canAndIsElevated(Permission.MANAGE_ROLES, Long.valueOf(j), meUser.getMfaEnabled(), guild.getMfaLevel()) || PermissionUtils.canAndIsElevated(8L, Long.valueOf(j), meUser.getMfaEnabled(), guild.getMfaLevel());
            List<SimpleRolesAdapter.RoleItem> buildRoleItems = buildRoleItems();
            this.roleItems = buildRoleItems;
            this.canAddRole = map.size() > buildRoleItems.size() ? true : z2;
        }

        private final List<SimpleRolesAdapter.RoleItem> buildRoleItems() {
            ArrayList arrayList = new ArrayList();
            for (Number number : Companion.getOverwriteIds(this.channel, PermissionOverwrite.Type.ROLE)) {
                GuildRole guildRole = this.guildRoles.get(Long.valueOf(number.longValue()));
                if (guildRole != null) {
                    arrayList.add(new SimpleRolesAdapter.RoleItem(guildRole));
                }
            }
            return u.sorted(arrayList);
        }

        public static /* synthetic */ Model copy$default(Model model, MeUser meUser, Guild guild, Channel channel, long j, Map map, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                meUser = model.f2821me;
            }
            if ((i & 2) != 0) {
                guild = model.guild;
            }
            Guild guild2 = guild;
            if ((i & 4) != 0) {
                channel = model.channel;
            }
            Channel channel2 = channel;
            if ((i & 8) != 0) {
                j = model.myPermissions;
            }
            long j2 = j;
            Map<Long, GuildRole> map2 = map;
            if ((i & 16) != 0) {
                map2 = model.guildRoles;
            }
            Map map3 = map2;
            List<SimpleMembersAdapter.MemberItem> list2 = list;
            if ((i & 32) != 0) {
                list2 = model.memberItems;
            }
            return model.copy(meUser, guild2, channel2, j2, map3, list2);
        }

        public final MeUser component1() {
            return this.f2821me;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final Channel component3() {
            return this.channel;
        }

        public final long component4() {
            return this.myPermissions;
        }

        public final Map<Long, GuildRole> component5() {
            return this.guildRoles;
        }

        public final List<SimpleMembersAdapter.MemberItem> component6() {
            return this.memberItems;
        }

        public final Model copy(MeUser meUser, Guild guild, Channel channel, long j, Map<Long, GuildRole> map, List<SimpleMembersAdapter.MemberItem> list) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "guildRoles");
            m.checkNotNullParameter(list, "memberItems");
            return new Model(meUser, guild, channel, j, map, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.f2821me, model.f2821me) && m.areEqual(this.guild, model.guild) && m.areEqual(this.channel, model.channel) && this.myPermissions == model.myPermissions && m.areEqual(this.guildRoles, model.guildRoles) && m.areEqual(this.memberItems, model.memberItems);
        }

        public final boolean getCanAddRole() {
            return this.canAddRole;
        }

        public final boolean getCanManage() {
            return this.canManage;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Map<Long, GuildRole> getGuildRoles() {
            return this.guildRoles;
        }

        public final MeUser getMe() {
            return this.f2821me;
        }

        public final List<SimpleMembersAdapter.MemberItem> getMemberItems() {
            return this.memberItems;
        }

        public final long getMyPermissions() {
            return this.myPermissions;
        }

        public final List<SimpleRolesAdapter.RoleItem> getRoleItems() {
            return this.roleItems;
        }

        public int hashCode() {
            MeUser meUser = this.f2821me;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            Guild guild = this.guild;
            int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int a = (b.a(this.myPermissions) + ((hashCode2 + (channel != null ? channel.hashCode() : 0)) * 31)) * 31;
            Map<Long, GuildRole> map = this.guildRoles;
            int hashCode3 = (a + (map != null ? map.hashCode() : 0)) * 31;
            List<SimpleMembersAdapter.MemberItem> list = this.memberItems;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(me=");
            R.append(this.f2821me);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", myPermissions=");
            R.append(this.myPermissions);
            R.append(", guildRoles=");
            R.append(this.guildRoles);
            R.append(", memberItems=");
            return a.K(R, this.memberItems, ")");
        }
    }

    public WidgetChannelSettingsPermissionsAdvanced() {
        super(R.layout.widget_channel_settings_permissions_advanced);
    }

    private final void configureMembers(Model model) {
        if (model.getMemberItems().isEmpty()) {
            LinearLayout linearLayout = getViewBinding().d;
            m.checkNotNullExpressionValue(linearLayout, "viewBinding.membersContainer");
            linearLayout.setVisibility(8);
            return;
        }
        SimpleMembersAdapter simpleMembersAdapter = this.membersAdapter;
        if (simpleMembersAdapter == null) {
            m.throwUninitializedPropertyAccessException("membersAdapter");
        }
        simpleMembersAdapter.setData(model.getMemberItems(), new WidgetChannelSettingsPermissionsAdvanced$configureMembers$1(this, model));
        LinearLayout linearLayout2 = getViewBinding().d;
        m.checkNotNullExpressionValue(linearLayout2, "viewBinding.membersContainer");
        linearLayout2.setVisibility(0);
    }

    private final void configureRoles(Model model) {
        if (model.getRoleItems().isEmpty()) {
            LinearLayout linearLayout = getViewBinding().f;
            m.checkNotNullExpressionValue(linearLayout, "viewBinding.rolesContainer");
            linearLayout.setVisibility(8);
            return;
        }
        SimpleRolesAdapter simpleRolesAdapter = this.rolesAdapter;
        if (simpleRolesAdapter == null) {
            m.throwUninitializedPropertyAccessException("rolesAdapter");
        }
        simpleRolesAdapter.setData(model.getRoleItems(), new WidgetChannelSettingsPermissionsAdvanced$configureRoles$1(this, model));
        LinearLayout linearLayout2 = getViewBinding().f;
        m.checkNotNullExpressionValue(linearLayout2, "viewBinding.rolesContainer");
        linearLayout2.setVisibility(0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        if (model != null && model.getCanManage()) {
            getViewBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAdvanced$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    if (model.getCanAddRole()) {
                        WidgetChannelSettingsPermissionsAddRole.create(WidgetChannelSettingsPermissionsAdvanced.this.getContext(), model.getChannel().h());
                    } else {
                        b.a.d.m.i(WidgetChannelSettingsPermissionsAdvanced.this, R.string.overwrite_no_role_to_add, 0, 4);
                    }
                }
            });
            getViewBinding().f2261b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAdvanced$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChannelSettingsPermissionsAddMember.create(WidgetChannelSettingsPermissionsAdvanced.this.getContext(), model.getGuild().getId(), model.getChannel().h());
                }
            });
            configureRoles(model);
            configureMembers(model);
        }
    }

    private final WidgetChannelSettingsPermissionsAdvancedBinding getViewBinding() {
        return (WidgetChannelSettingsPermissionsAdvancedBinding) this.viewBinding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getViewBinding().g;
        m.checkNotNullExpressionValue(recyclerView, "viewBinding.rolesRecycler");
        this.rolesAdapter = (SimpleRolesAdapter) companion.configure(new SimpleRolesAdapter(recyclerView));
        RecyclerView recyclerView2 = getViewBinding().g;
        m.checkNotNullExpressionValue(recyclerView2, "viewBinding.rolesRecycler");
        recyclerView2.setNestedScrollingEnabled(false);
        getViewBinding().g.setHasFixedSize(false);
        RecyclerView recyclerView3 = getViewBinding().e;
        m.checkNotNullExpressionValue(recyclerView3, "viewBinding.membersRecycler");
        this.membersAdapter = (SimpleMembersAdapter) companion.configure(new SimpleMembersAdapter(recyclerView3));
        RecyclerView recyclerView4 = getViewBinding().e;
        m.checkNotNullExpressionValue(recyclerView4, "viewBinding.membersRecycler");
        recyclerView4.setNestedScrollingEnabled(false);
        getViewBinding().e.setHasFixedSize(false);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<Model> q = Model.Companion.get(getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", -1L)).o(1L, TimeUnit.SECONDS).q();
        m.checkNotNullExpressionValue(q, "Model[channelId]\n       …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), WidgetChannelSettingsPermissionsAdvanced.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelSettingsPermissionsAdvanced$onViewBoundOrOnResume$1(this));
    }
}
