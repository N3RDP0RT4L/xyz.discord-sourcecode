package com.discord.widgets.channels.permissions;

import b.d.b.a.a;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsOverviewViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelSettingsPermissionsOverview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "index", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsPermissionsOverview$configureTabs$1 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ WidgetChannelSettingsPermissionsOverview this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsPermissionsOverview$configureTabs$1(WidgetChannelSettingsPermissionsOverview widgetChannelSettingsPermissionsOverview) {
        super(1);
        this.this$0 = widgetChannelSettingsPermissionsOverview;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.a;
    }

    public final void invoke(int i) {
        WidgetChannelSettingsPermissionsOverviewViewModel.Tab tab;
        WidgetChannelSettingsPermissionsOverviewViewModel viewModel;
        if (i == 0) {
            tab = WidgetChannelSettingsPermissionsOverviewViewModel.Tab.MODERATOR;
        } else if (i == 1) {
            tab = WidgetChannelSettingsPermissionsOverviewViewModel.Tab.ADVANCED;
        } else {
            throw new IllegalArgumentException(a.p("illegal index: ", i));
        }
        viewModel = this.this$0.getViewModel();
        viewModel.selectTab(tab);
    }
}
