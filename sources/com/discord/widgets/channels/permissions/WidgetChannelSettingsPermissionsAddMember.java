package com.discord.widgets.channels.permissions;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.view.View;
import android.widget.ViewFlipper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.z.a.a.o;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.app.AppFragment;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.channels.SimpleMembersAdapter;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAddMember;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.n;
import j0.l.a.o2;
import j0.l.a.r;
import j0.l.e.i;
import j0.l.e.k;
import j0.l.e.m;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.functions.Func4;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
import xyz.discord.R;
/* loaded from: classes2.dex */
public class WidgetChannelSettingsPermissionsAddMember extends AppFragment {
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final int VIEW_INDEX_MEMBER_LIST = 0;
    private static final int VIEW_INDEX_NO_RESULTS = 1;
    public static final /* synthetic */ int j = 0;
    private SimpleMembersAdapter membersAdapter;
    private RecyclerView membersRecycler;
    private final Subject<String, String> nameFilterPublisher = BehaviorSubject.l0("");
    private TextInputLayout searchBox;
    private ViewFlipper viewFlipper;

    /* loaded from: classes2.dex */
    public static class Model {
        private final boolean canManage;
        private final Channel channel;
        private final List<SimpleMembersAdapter.MemberItem> memberItems;

        private Model(MeUser meUser, Guild guild, Channel channel, Long l, List<SimpleMembersAdapter.MemberItem> list) {
            this.channel = channel;
            this.memberItems = list;
            boolean mfaEnabled = meUser.getMfaEnabled();
            this.canManage = guild.getOwnerId() == meUser.getId() || PermissionUtils.canAndIsElevated(Permission.MANAGE_ROLES, l, mfaEnabled, guild.getMfaLevel()) || PermissionUtils.canAndIsElevated(8L, l, mfaEnabled, guild.getMfaLevel());
        }

        public static Observable<Model> get(final long j, final long j2, final Observable<String> observable) {
            return StoreStream.getChannels().observeChannel(j2).Y(new b() { // from class: b.a.z.a.a.e
                @Override // j0.k.b
                public final Object call(Object obj) {
                    final long j3 = j;
                    long j4 = j2;
                    Observable observable2 = observable;
                    final Channel channel = (Channel) obj;
                    if (channel == null) {
                        return new k(null);
                    }
                    Observable h = Observable.h(StoreStream.getUsers().observeMe(), StoreStream.getGuilds().observeGuild(j3), StoreStream.getPermissions().observePermissionsForChannel(j4), observable2.o(300L, TimeUnit.MILLISECONDS).Y(new b() { // from class: b.a.z.a.a.g
                        @Override // j0.k.b
                        public final Object call(Object obj2) {
                            Observable memberItems;
                            memberItems = WidgetChannelSettingsPermissionsAddMember.Model.getMemberItems(j3, channel.s(), (String) obj2);
                            return memberItems;
                        }
                    }).q(), new Func4() { // from class: b.a.z.a.a.j
                        @Override // rx.functions.Func4
                        public final Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                            return WidgetChannelSettingsPermissionsAddMember.Model.lambda$null$1(Channel.this, (MeUser) obj2, (Guild) obj3, (Long) obj4, (List) obj5);
                        }
                    });
                    m.checkNotNullExpressionValue(h, "observable");
                    return ObservableExtensionsKt.computationBuffered(h).q();
                }
            });
        }

        /* JADX INFO: Access modifiers changed from: private */
        public static Observable<List<SimpleMembersAdapter.MemberItem>> getMemberItems(long j, @Nullable final List<PermissionOverwrite> list, String str) {
            final String lowerCase = str.toLowerCase(Locale.getDefault());
            return StoreStream.getGuilds().observeComputed(j).Y(new b() { // from class: b.a.z.a.a.i
                @Override // j0.k.b
                public final Object call(Object obj) {
                    final List list2 = list;
                    final String str2 = lowerCase;
                    final Map map = (Map) obj;
                    return StoreStream.getUsers().observeUsers(map.keySet()).Y(new b() { // from class: b.a.z.a.a.h
                        @Override // j0.k.b
                        public final Object call(Object obj2) {
                            Observable observable;
                            final List list3 = list2;
                            final Map map2 = map;
                            final String str3 = str2;
                            Observable x2 = Observable.A(((Map) obj2).values()).x(new b() { // from class: b.a.z.a.a.l
                                @Override // j0.k.b
                                public final Object call(Object obj3) {
                                    List list4 = list3;
                                    User user = (User) obj3;
                                    if (list4 == null) {
                                        return Boolean.TRUE;
                                    }
                                    for (int i = 0; i < list4.size(); i++) {
                                        if (((PermissionOverwrite) list4.get(i)).e() == user.getId()) {
                                            return Boolean.FALSE;
                                        }
                                    }
                                    return Boolean.TRUE;
                                }
                            }).x(new b() { // from class: b.a.z.a.a.m
                                @Override // j0.k.b
                                public final Object call(Object obj3) {
                                    return Boolean.valueOf(a.e((User) obj3, map2) != null);
                                }
                            }).x(new b() { // from class: b.a.z.a.a.k
                                @Override // j0.k.b
                                public final Object call(Object obj3) {
                                    return Boolean.valueOf(((User) obj3).getUsername().toLowerCase(Locale.ROOT).contains(str3));
                                }
                            });
                            Observable h02 = Observable.h0(new r(x2.j, new o2(new Func2() { // from class: b.a.z.a.a.f
                                @Override // rx.functions.Func2
                                public final Object call(Object obj3, Object obj4) {
                                    Map map3 = map2;
                                    User user = (User) obj3;
                                    User user2 = (User) obj4;
                                    return Integer.valueOf(GuildMember.compareUserNames(user, user2, (GuildMember) a.e(user, map3), (GuildMember) a.e(user2, map3)));
                                }
                            }, 10)));
                            m.a aVar = m.a.INSTANCE;
                            int i = i.j;
                            if (h02 instanceof k) {
                                observable = Observable.h0(new n.b(((k) h02).l, aVar));
                            } else {
                                observable = Observable.h0(new n(h02, aVar, i));
                            }
                            return observable.F(new b() { // from class: b.a.z.a.a.d
                                @Override // j0.k.b
                                public final Object call(Object obj3) {
                                    User user = (User) obj3;
                                    return new SimpleMembersAdapter.MemberItem(user, (GuildMember) a.e(user, map2));
                                }
                            }).f0();
                        }
                    });
                }
            });
        }

        private static boolean isValid(MeUser meUser, Guild guild, Channel channel, List<SimpleMembersAdapter.MemberItem> list) {
            return (channel == null || guild == null || meUser == null || list == null) ? false : true;
        }

        public static /* synthetic */ Model lambda$null$1(Channel channel, MeUser meUser, Guild guild, Long l, List list) {
            if (isValid(meUser, guild, channel, list)) {
                return new Model(meUser, guild, channel, l, list);
            }
            return null;
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Model;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            if (!model.canEqual(this) || this.canManage != model.canManage) {
                return false;
            }
            Channel channel = this.channel;
            Channel channel2 = model.channel;
            if (channel != null ? !channel.equals(channel2) : channel2 != null) {
                return false;
            }
            List<SimpleMembersAdapter.MemberItem> list = this.memberItems;
            List<SimpleMembersAdapter.MemberItem> list2 = model.memberItems;
            return list != null ? list.equals(list2) : list2 == null;
        }

        public int hashCode() {
            int i = this.canManage ? 79 : 97;
            Channel channel = this.channel;
            int i2 = 43;
            int hashCode = ((i + 59) * 59) + (channel == null ? 43 : channel.hashCode());
            List<SimpleMembersAdapter.MemberItem> list = this.memberItems;
            int i3 = hashCode * 59;
            if (list != null) {
                i2 = list.hashCode();
            }
            return i3 + i2;
        }

        public String toString() {
            StringBuilder R = a.R("WidgetChannelSettingsPermissionsAddMember.Model(channel=");
            R.append(this.channel);
            R.append(", memberItems=");
            R.append(this.memberItems);
            R.append(", canManage=");
            return a.M(R, this.canManage, ")");
        }
    }

    public WidgetChannelSettingsPermissionsAddMember() {
        super(R.layout.widget_channel_settings_permissions_add_member);
    }

    private void configureToolbar(Channel channel) {
        setActionBarTitle(R.string.add_a_member);
        setActionBarSubtitle(ChannelUtils.d(channel, requireContext(), true));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void configureUI(final Model model) {
        if (model != null && model.canManage) {
            configureToolbar(model.channel);
            this.membersAdapter.setData(model.memberItems, new Function1() { // from class: b.a.z.a.a.c
                @Override // kotlin.jvm.functions.Function1
                public final Object invoke(Object obj) {
                    WidgetChannelSettingsPermissionsAddMember widgetChannelSettingsPermissionsAddMember = WidgetChannelSettingsPermissionsAddMember.this;
                    WidgetChannelSettingsPermissionsAddMember.Model model2 = model;
                    WidgetChannelSettingsEditPermissions.createForUser(widgetChannelSettingsPermissionsAddMember.getContext(), model2.channel.f(), model2.channel.h(), ((User) obj).getId());
                    return Unit.a;
                }
            });
            ViewFlipper viewFlipper = this.viewFlipper;
            if (viewFlipper != null) {
                viewFlipper.setDisplayedChild(model.memberItems.isEmpty() ? 1 : 0);
            }
        } else if (e() != null) {
            e().onBackPressed();
        }
    }

    public static void create(Context context, long j2, long j3) {
        Intent intent = new Intent();
        intent.putExtra("INTENT_EXTRA_GUILD_ID", j2);
        intent.putExtra(INTENT_EXTRA_CHANNEL_ID, j3);
        j.d(context, WidgetChannelSettingsPermissionsAddMember.class, intent);
    }

    public /* synthetic */ Unit h(Editable editable) {
        this.nameFilterPublisher.onNext(editable.toString());
        return null;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(@NonNull View view) {
        super.onViewBound(view);
        this.membersRecycler = (RecyclerView) view.findViewById(R.id.channel_settings_permissions_add_member_recycler);
        this.searchBox = (TextInputLayout) view.findViewById(R.id.channel_settings_permissions_add_member_name_search);
        this.viewFlipper = (ViewFlipper) view.findViewById(R.id.channel_settings_permissions_add_member_view_flipper);
        setActionBarDisplayHomeAsUpEnabled();
        this.membersAdapter = (SimpleMembersAdapter) MGRecyclerAdapter.configure(new SimpleMembersAdapter(this.membersRecycler));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        final long longExtra = getMostRecentIntent().getLongExtra("INTENT_EXTRA_GUILD_ID", -1L);
        long longExtra2 = getMostRecentIntent().getLongExtra(INTENT_EXTRA_CHANNEL_ID, -1L);
        ViewExtensions.addBindedTextWatcher(this.searchBox, this, new Function1() { // from class: b.a.z.a.a.n
            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                WidgetChannelSettingsPermissionsAddMember.this.h((Editable) obj);
                return null;
            }
        });
        this.nameFilterPublisher.onNext(ViewExtensions.getTextOrEmpty(this.searchBox));
        this.nameFilterPublisher.o(750L, TimeUnit.MILLISECONDS).x(o.j).k(b.a.d.o.e(new Action1() { // from class: b.a.z.a.a.b
            @Override // rx.functions.Action1
            public final void call(Object obj) {
                int i = WidgetChannelSettingsPermissionsAddMember.j;
                StoreStream.getGatewaySocket().requestGuildMembers(longExtra, (String) obj);
            }
        }, getClass()));
        Observable<Model> observable = Model.get(longExtra, longExtra2, this.nameFilterPublisher);
        d0.z.d.m.checkNotNullParameter(this, "appComponent");
        d0.z.d.m.checkNotNullExpressionValue(observable, "it");
        ObservableExtensionsKt.ui(observable, this, null).k(b.a.d.o.e(new Action1() { // from class: b.a.z.a.a.a
            @Override // rx.functions.Action1
            public final void call(Object obj) {
                WidgetChannelSettingsPermissionsAddMember.this.configureUI((WidgetChannelSettingsPermissionsAddMember.Model) obj);
            }
        }, getClass()));
    }
}
