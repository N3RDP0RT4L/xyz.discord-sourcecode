package com.discord.widgets.channels.permissions;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.AttrRes;
import androidx.annotation.IdRes;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelSettingsEditPermissionsBinding;
import com.discord.utilities.channel.ChannelPermissionUtilsKt;
import com.discord.utilities.channel.PermissionLabelOverrides;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.spans.TypefaceSpanCompat;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.TernaryCheckBox;
import com.discord.views.user.SettingsMemberView;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import d0.g;
import d0.t.m0;
import d0.t.n0;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetChannelSettingsEditPermissions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\r\u0018\u0000 C2\u00020\u0001:\u0001CB\u0007¢\u0006\u0004\bB\u0010'J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J1\u0010\u000f\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J+\u0010\u0014\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J!\u0010\u0018\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J#\u0010\u001d\u001a\u00020\u00042\n\u0010\u001b\u001a\u00060\u0011j\u0002`\u001a2\u0006\u0010\u001c\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ+\u0010 \u001a\u00020\u00042\n\u0010\u001b\u001a\u00060\u0011j\u0002`\u001a2\u0006\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u001f\u001a\u00020\tH\u0002¢\u0006\u0004\b \u0010!J\u0017\u0010$\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\"H\u0016¢\u0006\u0004\b$\u0010%J\u000f\u0010&\u001a\u00020\u0004H\u0016¢\u0006\u0004\b&\u0010'R\u001d\u0010-\u001a\u00020(8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001d\u0010\u001c\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R\u0018\u00103\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u001d\u0010\u001b\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b5\u0010/\u001a\u0004\b6\u00101R#\u0010;\u001a\b\u0012\u0004\u0012\u00020\u0007078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b8\u0010/\u001a\u0004\b9\u0010:R\u001d\u0010>\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b<\u0010/\u001a\u0004\b=\u00101R\u001d\u0010\u001f\u001a\u00020\t8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b?\u0010/\u001a\u0004\b@\u0010A¨\u0006D"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissions;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;", "model", "", "configureUI", "(Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;)V", "Lcom/discord/views/TernaryCheckBox;", "checkBox", "", "channelType", "", "isEveryoneRole", "Lcom/discord/utilities/channel/PermissionLabelOverrides;", "labels", "updateCheckboxLabels", "(Lcom/discord/views/TernaryCheckBox;IZLcom/discord/utilities/channel/PermissionLabelOverrides;)V", "", "Lcom/discord/api/permission/PermissionBit;", "permission", "setupPermissionEnabledState", "(Lcom/discord/views/TernaryCheckBox;JLcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;)V", "Lcom/discord/api/permission/PermissionOverwrite;", "permissionOverwrite", "setupPermissionCheckedState", "(Lcom/discord/views/TernaryCheckBox;Lcom/discord/api/permission/PermissionOverwrite;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "targetId", "deletePermissionOverwrites", "(JJ)V", "type", "updatePermissionOverwrites", "(JJI)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetChannelSettingsEditPermissionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelSettingsEditPermissionsBinding;", "binding", "targetId$delegate", "Lkotlin/Lazy;", "getTargetId", "()J", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "channelId$delegate", "getChannelId", "", "permissionCheckboxes$delegate", "getPermissionCheckboxes", "()Ljava/util/List;", "permissionCheckboxes", "guildId$delegate", "getGuildId", "guildId", "type$delegate", "getType", "()I", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsEditPermissions extends AppFragment {
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String INTENT_EXTRA_TARGET_ID = "INTENT_EXTRA_TARGET_ID";
    private static final String INTENT_EXTRA_TYPE = "INTENT_EXTRA_TYPE";
    public static final int TYPE_ROLE = 1;
    public static final int TYPE_USER = 0;
    private StatefulViews state;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelSettingsEditPermissions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelSettingsEditPermissionsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final Set<Long> STAGE_HIDDEN_PERMISSIONS = n0.setOf((Object[]) new Long[]{512L, 256L, Long.valueOf((long) Permission.SPEAK), Long.valueOf((long) Permission.USE_VAD)});
    private static final Set<Long> STAGE_DISABLED_PERMISSIONS = m0.setOf(Long.valueOf((long) Permission.REQUEST_TO_SPEAK));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelSettingsEditPermissions$binding$2.INSTANCE, null, 2, null);
    private final Lazy permissionCheckboxes$delegate = g.lazy(new WidgetChannelSettingsEditPermissions$permissionCheckboxes$2(this));
    private final Lazy guildId$delegate = g.lazy(new WidgetChannelSettingsEditPermissions$guildId$2(this));
    private final Lazy channelId$delegate = g.lazy(new WidgetChannelSettingsEditPermissions$channelId$2(this));
    private final Lazy targetId$delegate = g.lazy(new WidgetChannelSettingsEditPermissions$targetId$2(this));
    private final Lazy type$delegate = g.lazy(new WidgetChannelSettingsEditPermissions$type$2(this));

    /* compiled from: WidgetChannelSettingsEditPermissions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\"\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b%\u0010&J7\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\u000e\u001a\u00020\u00042\b\b\u0001\u0010\r\u001a\u00020\bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ%\u0010\u0014\u001a\u00020\n*\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00112\b\b\u0001\u0010\u0013\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0014\u0010\u0015J/\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0016\u0010\u0017J/\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0018\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u001aR\u0016\u0010\u001d\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001aR\u001c\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00040\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00040\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010 R\u0016\u0010\"\u001a\u00020\b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010$\u001a\u00020\b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b$\u0010#¨\u0006'"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissions$Companion;", "", "Landroid/content/Context;", "context", "", "guildId", "channelId", "targetId", "", "type", "", "create", "(Landroid/content/Context;JJJI)V", "permissionSettingId", "getPermission", "(I)J", "Landroid/widget/TextView;", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "fontResId", "setTextWithFont", "(Landroid/widget/TextView;Ljava/lang/String;I)V", "createForRole", "(Landroid/content/Context;JJJ)V", "createForUser", WidgetChannelSettingsEditPermissions.INTENT_EXTRA_CHANNEL_ID, "Ljava/lang/String;", "INTENT_EXTRA_GUILD_ID", "INTENT_EXTRA_TARGET_ID", WidgetChannelSettingsEditPermissions.INTENT_EXTRA_TYPE, "", "STAGE_DISABLED_PERMISSIONS", "Ljava/util/Set;", "STAGE_HIDDEN_PERMISSIONS", "TYPE_ROLE", "I", "TYPE_USER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final void create(Context context, long j, long j2, long j3, int i) {
            Intent intent = new Intent();
            intent.putExtra("INTENT_EXTRA_GUILD_ID", j);
            intent.putExtra(WidgetChannelSettingsEditPermissions.INTENT_EXTRA_CHANNEL_ID, j2);
            intent.putExtra("INTENT_EXTRA_TARGET_ID", j3);
            intent.putExtra(WidgetChannelSettingsEditPermissions.INTENT_EXTRA_TYPE, i);
            j.d(context, WidgetChannelSettingsEditPermissions.class, intent);
        }

        /* JADX INFO: Access modifiers changed from: private */
        @SuppressLint({"NonConstantResourceId"})
        public final long getPermission(@IdRes int i) {
            switch (i) {
                case R.id.channel_permission_events_manage_events /* 2131362372 */:
                    return Permission.MANAGE_EVENTS;
                case R.id.channel_permission_general_create_instant_invite /* 2131362373 */:
                    return 1L;
                case R.id.channel_permission_general_manage_channel /* 2131362374 */:
                    return 16L;
                case R.id.channel_permission_general_manage_permissions /* 2131362375 */:
                    return Permission.MANAGE_ROLES;
                case R.id.channel_permission_general_manage_threads /* 2131362376 */:
                    return Permission.MANAGE_THREADS;
                case R.id.channel_permission_general_manage_webhooks /* 2131362377 */:
                    return Permission.MANAGE_WEBHOOKS;
                case R.id.channel_permission_owner_view /* 2131362378 */:
                default:
                    throw new IllegalArgumentException(a.p("Invalid ID: ", i));
                case R.id.channel_permission_stage_request_to_speak /* 2131362379 */:
                    return Permission.REQUEST_TO_SPEAK;
                case R.id.channel_permission_text_add_reactions /* 2131362380 */:
                    return 64L;
                case R.id.channel_permission_text_attach_files /* 2131362381 */:
                    return Permission.ATTACH_FILES;
                case R.id.channel_permission_text_create_private_threads /* 2131362382 */:
                    return Permission.CREATE_PRIVATE_THREADS;
                case R.id.channel_permission_text_create_public_threads /* 2131362383 */:
                    return Permission.CREATE_PUBLIC_THREADS;
                case R.id.channel_permission_text_embed_links /* 2131362384 */:
                    return Permission.EMBED_LINKS;
                case R.id.channel_permission_text_manage_messages /* 2131362385 */:
                    return Permission.MANAGE_MESSAGES;
                case R.id.channel_permission_text_mention_everyone /* 2131362386 */:
                    return Permission.MENTION_EVERYONE;
                case R.id.channel_permission_text_read_message_history /* 2131362387 */:
                    return Permission.READ_MESSAGE_HISTORY;
                case R.id.channel_permission_text_read_messages /* 2131362388 */:
                    return Permission.VIEW_CHANNEL;
                case R.id.channel_permission_text_send_messages /* 2131362389 */:
                    return Permission.SEND_MESSAGES;
                case R.id.channel_permission_text_send_messages_in_threads /* 2131362390 */:
                    return Permission.SEND_MESSAGES_IN_THREADS;
                case R.id.channel_permission_text_send_tts_messages /* 2131362391 */:
                    return Permission.SEND_TTS_MESSAGES;
                case R.id.channel_permission_text_use_external_emojis /* 2131362392 */:
                    return Permission.USE_EXTERNAL_EMOJIS;
                case R.id.channel_permission_text_use_external_stickers /* 2131362393 */:
                    return Permission.USE_EXTERNAL_STICKERS;
                case R.id.channel_permission_use_application_commands /* 2131362394 */:
                    return Permission.USE_APPLICATION_COMMANDS;
                case R.id.channel_permission_voice_connect /* 2131362395 */:
                    return Permission.CONNECT;
                case R.id.channel_permission_voice_deafen_members /* 2131362396 */:
                    return Permission.DEAFEN_MEMBERS;
                case R.id.channel_permission_voice_move_members /* 2131362397 */:
                    return Permission.MOVE_MEMBERS;
                case R.id.channel_permission_voice_mute_members /* 2131362398 */:
                    return Permission.MUTE_MEMBERS;
                case R.id.channel_permission_voice_priority_speaker /* 2131362399 */:
                    return 256L;
                case R.id.channel_permission_voice_speak /* 2131362400 */:
                    return Permission.SPEAK;
                case R.id.channel_permission_voice_use_vad /* 2131362401 */:
                    return Permission.USE_VAD;
                case R.id.channel_permission_voice_video /* 2131362402 */:
                    return 512L;
            }
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void setTextWithFont(TextView textView, String str, @AttrRes int i) {
            FontUtils fontUtils = FontUtils.INSTANCE;
            Context context = textView.getContext();
            m.checkNotNullExpressionValue(context, "context");
            Typeface themedFont = fontUtils.getThemedFont(context, i);
            if (themedFont != null) {
                TypefaceSpanCompat typefaceSpanCompat = new TypefaceSpanCompat(themedFont);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
                spannableStringBuilder.setSpan(typefaceSpanCompat, 0, spannableStringBuilder.length(), 33);
                textView.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
            }
        }

        public final void createForRole(Context context, long j, long j2, long j3) {
            m.checkNotNullParameter(context, "context");
            create(context, j, j2, j3, 1);
        }

        public final void createForUser(Context context, long j, long j2, long j3) {
            m.checkNotNullParameter(context, "context");
            create(context, j, j2, j3, 0);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChannelSettingsEditPermissions() {
        super(R.layout.widget_channel_settings_edit_permissions);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetChannelSettingsEditPermissionsModel widgetChannelSettingsEditPermissionsModel) {
        PermissionOverwrite permissionOverwrite;
        PermissionOverwrite permissionOverwrite2;
        CharSequence d;
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        Object obj;
        boolean z2;
        if (widgetChannelSettingsEditPermissionsModel == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        List<PermissionOverwrite> s2 = widgetChannelSettingsEditPermissionsModel.getChannel().s();
        if (s2 != null) {
            Iterator<T> it = s2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((PermissionOverwrite) obj).a() == getTargetId()) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            permissionOverwrite = (PermissionOverwrite) obj;
        } else {
            permissionOverwrite = null;
        }
        int i = permissionOverwrite != null ? R.menu.menu_edit_permission_overwrite : R.menu.menu_empty;
        setActionBarTitle(ChannelUtils.k(widgetChannelSettingsEditPermissionsModel.getChannel()) ? R.string.category_settings : R.string.channel_settings);
        setActionBarSubtitle(ChannelUtils.d(widgetChannelSettingsEditPermissionsModel.getChannel(), requireContext(), true));
        AppFragment.setActionBarOptionsMenu$default(this, i, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions$configureUI$1
            public final void call(MenuItem menuItem, Context context) {
                long targetId;
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_edit_overwrite_delete) {
                    WidgetChannelSettingsEditPermissions widgetChannelSettingsEditPermissions = WidgetChannelSettingsEditPermissions.this;
                    long h = widgetChannelSettingsEditPermissionsModel.getChannel().h();
                    targetId = WidgetChannelSettingsEditPermissions.this.getTargetId();
                    widgetChannelSettingsEditPermissions.deletePermissionOverwrites(h, targetId);
                }
            }
        }, null, 4, null);
        if (widgetChannelSettingsEditPermissionsModel instanceof WidgetChannelSettingsEditPermissionsModel.ModelForRole) {
            SettingsMemberView settingsMemberView = getBinding().M;
            m.checkNotNullExpressionValue(settingsMemberView, "binding.memberView");
            settingsMemberView.setVisibility(8);
            TextView textView = getBinding().J;
            m.checkNotNullExpressionValue(textView, "binding.channelPermissionsTargetName");
            textView.setVisibility(0);
            Companion companion = Companion;
            TextView textView2 = getBinding().J;
            m.checkNotNullExpressionValue(textView2, "binding.channelPermissionsTargetName");
            WidgetChannelSettingsEditPermissionsModel.ModelForRole modelForRole = (WidgetChannelSettingsEditPermissionsModel.ModelForRole) widgetChannelSettingsEditPermissionsModel;
            companion.setTextWithFont(textView2, modelForRole.getGuildRole().g(), R.attr.font_primary_semibold);
            getBinding().J.setTextColor(RoleUtils.getRoleColor$default(modelForRole.getGuildRole(), requireContext(), null, 2, null));
        } else if (widgetChannelSettingsEditPermissionsModel instanceof WidgetChannelSettingsEditPermissionsModel.ModelForUser) {
            SettingsMemberView settingsMemberView2 = getBinding().M;
            m.checkNotNullExpressionValue(settingsMemberView2, "binding.memberView");
            settingsMemberView2.setVisibility(0);
            TextView textView3 = getBinding().J;
            m.checkNotNullExpressionValue(textView3, "binding.channelPermissionsTargetName");
            textView3.setVisibility(8);
            WidgetChannelSettingsEditPermissionsModel.ModelForUser modelForUser = (WidgetChannelSettingsEditPermissionsModel.ModelForUser) widgetChannelSettingsEditPermissionsModel;
            getBinding().M.a(modelForUser.getUser(), modelForUser.getGuildMember());
            Companion companion2 = Companion;
            TextView textView4 = getBinding().J;
            m.checkNotNullExpressionValue(textView4, "binding.channelPermissionsTargetName");
            companion2.setTextWithFont(textView4, modelForUser.getUser().getUsername(), R.attr.font_primary_normal);
        }
        TextView textView5 = getBinding().F;
        m.checkNotNullExpressionValue(textView5, "binding.channelPermissionsChannelName");
        textView5.setText(ChannelUtils.d(widgetChannelSettingsEditPermissionsModel.getChannel(), requireContext(), true));
        boolean z3 = ChannelUtils.E(widgetChannelSettingsEditPermissionsModel.getChannel()) && TextInVoiceFeatureFlag.Companion.getINSTANCE().isEnabled(Long.valueOf(widgetChannelSettingsEditPermissionsModel.getChannel().f()));
        LinearLayout linearLayout = getBinding().K;
        m.checkNotNullExpressionValue(linearLayout, "binding.channelPermissionsTextContainer");
        linearLayout.setVisibility(ChannelUtils.s(widgetChannelSettingsEditPermissionsModel.getChannel()) || ChannelUtils.k(widgetChannelSettingsEditPermissionsModel.getChannel()) || z3 ? 0 : 8);
        LinearLayout linearLayout2 = getBinding().L;
        m.checkNotNullExpressionValue(linearLayout2, "binding.channelPermissionsVoiceContainer");
        linearLayout2.setVisibility(ChannelUtils.t(widgetChannelSettingsEditPermissionsModel.getChannel()) || ChannelUtils.k(widgetChannelSettingsEditPermissionsModel.getChannel()) ? 0 : 8);
        LinearLayout linearLayout3 = getBinding().I;
        m.checkNotNullExpressionValue(linearLayout3, "binding.channelPermissionsStageContainer");
        linearLayout3.setVisibility(ChannelUtils.z(widgetChannelSettingsEditPermissionsModel.getChannel()) || ChannelUtils.k(widgetChannelSettingsEditPermissionsModel.getChannel()) ? 0 : 8);
        LinearLayout linearLayout4 = getBinding().G;
        m.checkNotNullExpressionValue(linearLayout4, "binding.channelPermissionsEventsContainer");
        linearLayout4.setVisibility(widgetChannelSettingsEditPermissionsModel.getHasEventFeature() && (ChannelUtils.t(widgetChannelSettingsEditPermissionsModel.getChannel()) || ChannelUtils.k(widgetChannelSettingsEditPermissionsModel.getChannel())) ? 0 : 8);
        TernaryCheckBox ternaryCheckBox = getBinding().g;
        m.checkNotNullExpressionValue(ternaryCheckBox, "binding.channelPermissionGeneralManageWebhooks");
        ternaryCheckBox.setVisibility(ChannelUtils.s(widgetChannelSettingsEditPermissionsModel.getChannel()) || ChannelUtils.k(widgetChannelSettingsEditPermissionsModel.getChannel()) ? 0 : 8);
        if (ChannelUtils.k(widgetChannelSettingsEditPermissionsModel.getChannel())) {
            getBinding().N.setText(R.string.role_permissions_section_general_category);
        }
        Map<Integer, PermissionLabelOverrides> categoryLabels = ChannelPermissionUtilsKt.getCategoryLabels(widgetChannelSettingsEditPermissionsModel.getUseNewThreadsPermissions(), requireContext());
        boolean z4 = widgetChannelSettingsEditPermissionsModel.getType() == 1 && widgetChannelSettingsEditPermissionsModel.getTargetId() == widgetChannelSettingsEditPermissionsModel.getChannel().f();
        List<PermissionOverwrite> s3 = widgetChannelSettingsEditPermissionsModel.getChannel().s();
        m.checkNotNull(s3);
        Iterator<PermissionOverwrite> it2 = s3.iterator();
        while (true) {
            if (!it2.hasNext()) {
                permissionOverwrite2 = null;
                break;
            }
            permissionOverwrite2 = it2.next();
            if (permissionOverwrite2.e() == widgetChannelSettingsEditPermissionsModel.getTargetId()) {
                break;
            }
        }
        if (!widgetChannelSettingsEditPermissionsModel.getUseNewThreadsPermissions()) {
            getBinding().f2257s.setLabel(getString(R.string.interim_send_messages_in_threads));
            TernaryCheckBox ternaryCheckBox2 = getBinding().f2257s;
            e = b.e(this, R.string.interim_role_permissions_send_messages_in_threads_description_text, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            ternaryCheckBox2.setSubtext(e);
            getBinding().l.setLabel(getString(R.string.interim_create_public_threads));
            TernaryCheckBox ternaryCheckBox3 = getBinding().l;
            e2 = b.e(this, R.string.interim_role_permissions_create_public_threads_description_text, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            ternaryCheckBox3.setSubtext(e2);
            getBinding().k.setLabel(getString(R.string.interim_create_private_threads));
            TernaryCheckBox ternaryCheckBox4 = getBinding().k;
            e3 = b.e(this, R.string.interim_role_permissions_create_private_threads_description_text, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            ternaryCheckBox4.setSubtext(e3);
        }
        for (TernaryCheckBox ternaryCheckBox5 : getPermissionCheckboxes()) {
            int id2 = ternaryCheckBox5.getId();
            long permission = Companion.getPermission(id2);
            if (widgetChannelSettingsEditPermissionsModel.getChannel().A() == 13 && STAGE_HIDDEN_PERMISSIONS.contains(Long.valueOf(permission))) {
                ternaryCheckBox5.setVisibility(8);
            }
            setupPermissionEnabledState(ternaryCheckBox5, permission, widgetChannelSettingsEditPermissionsModel);
            setupPermissionCheckedState(ternaryCheckBox5, permissionOverwrite2);
            if (id2 == R.id.channel_permission_voice_priority_speaker) {
                d = b.d(ternaryCheckBox5, R.string.role_permissions_priority_speaker_description_voice_mobile, new Object[]{getString(R.string.keybind_push_to_talk_priority)}, (r4 & 4) != 0 ? b.c.j : null);
                ternaryCheckBox5.setSubtext(d);
            }
            if (id2 == R.id.channel_permission_text_create_private_threads) {
                ternaryCheckBox5.setVisibility(ChannelUtils.i(widgetChannelSettingsEditPermissionsModel.getChannel()) ^ true ? 0 : 8);
            }
            if (id2 == R.id.channel_permission_general_manage_threads) {
                ternaryCheckBox5.setVisibility(ChannelUtils.t(widgetChannelSettingsEditPermissionsModel.getChannel()) ^ true ? 0 : 8);
            }
            updateCheckboxLabels(ternaryCheckBox5, widgetChannelSettingsEditPermissionsModel.getChannel().A(), z4, categoryLabels.get(Integer.valueOf(id2)));
        }
        getBinding().H.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelSettingsEditPermissions.this.updatePermissionOverwrites(widgetChannelSettingsEditPermissionsModel.getChannel().h(), widgetChannelSettingsEditPermissionsModel.getTargetId(), widgetChannelSettingsEditPermissionsModel.getType());
            }
        });
        StatefulViews statefulViews = this.state;
        if (statefulViews != null) {
            statefulViews.configureSaveActionView(getBinding().H);
        }
    }

    public static final void createForRole(Context context, long j, long j2, long j3) {
        Companion.createForRole(context, j, j2, j3);
    }

    public static final void createForUser(Context context, long j, long j2, long j3) {
        Companion.createForUser(context, j, j2, j3);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void deletePermissionOverwrites(long j, long j2) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deletePermissionOverwrites(j, j2), false, 1, null), this, null, 2, null), WidgetChannelSettingsEditPermissions.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelSettingsEditPermissions$deletePermissionOverwrites$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChannelSettingsEditPermissionsBinding getBinding() {
        return (WidgetChannelSettingsEditPermissionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final long getChannelId() {
        return ((Number) this.channelId$delegate.getValue()).longValue();
    }

    private final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<TernaryCheckBox> getPermissionCheckboxes() {
        return (List) this.permissionCheckboxes$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getTargetId() {
        return ((Number) this.targetId$delegate.getValue()).longValue();
    }

    private final int getType() {
        return ((Number) this.type$delegate.getValue()).intValue();
    }

    private final void setupPermissionCheckedState(TernaryCheckBox ternaryCheckBox, PermissionOverwrite permissionOverwrite) {
        long j;
        long j2 = 0;
        if (permissionOverwrite != null) {
            j2 = permissionOverwrite.c();
            j = permissionOverwrite.d();
        } else {
            j = 0;
        }
        final int id2 = ternaryCheckBox.getId();
        long permission = Companion.getPermission(id2);
        if (permission == (j2 & permission)) {
            ternaryCheckBox.f();
        } else if (permission == (permission & j)) {
            ternaryCheckBox.e();
        } else {
            ternaryCheckBox.d();
        }
        StatefulViews statefulViews = this.state;
        if (statefulViews != null) {
            Integer num = (Integer) statefulViews.get(id2, Integer.valueOf(ternaryCheckBox.getSwitchStatus()));
        }
        ternaryCheckBox.setOnSwitchStatusChangedListener(new TernaryCheckBox.b() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions$setupPermissionCheckedState$1
            @Override // com.discord.views.TernaryCheckBox.b
            public void onSwitchStatusChanged(int i) {
                StatefulViews statefulViews2;
                StatefulViews statefulViews3;
                WidgetChannelSettingsEditPermissionsBinding binding;
                statefulViews2 = WidgetChannelSettingsEditPermissions.this.state;
                if (statefulViews2 != null) {
                    statefulViews2.put(id2, Integer.valueOf(i));
                }
                statefulViews3 = WidgetChannelSettingsEditPermissions.this.state;
                if (statefulViews3 != null) {
                    binding = WidgetChannelSettingsEditPermissions.this.getBinding();
                    statefulViews3.configureSaveActionView(binding.H);
                }
            }
        });
    }

    private final void setupPermissionEnabledState(TernaryCheckBox ternaryCheckBox, long j, WidgetChannelSettingsEditPermissionsModel widgetChannelSettingsEditPermissionsModel) {
        if (ChannelUtils.z(widgetChannelSettingsEditPermissionsModel.getChannel()) && STAGE_DISABLED_PERMISSIONS.contains(Long.valueOf(j))) {
            ternaryCheckBox.setDisabled(R.string.stage_channel_cannot_overwrite_permission);
        } else if (widgetChannelSettingsEditPermissionsModel instanceof WidgetChannelSettingsEditPermissionsModel.ModelForUser) {
            WidgetChannelSettingsEditPermissionsModel.ModelForUser modelForUser = (WidgetChannelSettingsEditPermissionsModel.ModelForUser) widgetChannelSettingsEditPermissionsModel;
            if (modelForUser.isMe()) {
                if (ternaryCheckBox.b()) {
                    ternaryCheckBox.setDisabled(R.string.cannot_deny_self_simple);
                } else {
                    ternaryCheckBox.setOffDisabled(R.string.cannot_deny_self_simple);
                }
            } else if ((modelForUser.getMyPermissionsForChannel() & j) == j) {
                ternaryCheckBox.c();
            } else {
                ternaryCheckBox.setDisabled(R.string.cannot_deny_missing_permission);
            }
        } else if (widgetChannelSettingsEditPermissionsModel instanceof WidgetChannelSettingsEditPermissionsModel.ModelForRole) {
            WidgetChannelSettingsEditPermissionsModel.ModelForRole modelForRole = (WidgetChannelSettingsEditPermissionsModel.ModelForRole) widgetChannelSettingsEditPermissionsModel;
            boolean z2 = true;
            if (!modelForRole.getMeHasRole()) {
                if ((modelForRole.getMyPermissionsForChannel() & j) != j) {
                    z2 = false;
                }
                ternaryCheckBox.setEnabled(z2);
                return;
            }
            int i = ternaryCheckBox.o;
            if (!(i == 0)) {
                if (i != -1) {
                    z2 = false;
                }
                if (z2) {
                    ternaryCheckBox.c();
                } else if (!ternaryCheckBox.b()) {
                } else {
                    if (!modelForRole.canNeutralizeRolePermission(j)) {
                        ternaryCheckBox.setDisabled(R.string.cannot_deny_singular_permission);
                    } else if (modelForRole.canNeutralizeRolePermission(j) && !modelForRole.canDenyRolePermission(j)) {
                        ternaryCheckBox.setOffDisabled(R.string.cannot_deny_singular_permission);
                    } else if (modelForRole.canNeutralizeRolePermission(j) && modelForRole.canDenyRolePermission(j)) {
                        ternaryCheckBox.c();
                    }
                }
            } else if (modelForRole.canDenyRolePermission(j)) {
                ternaryCheckBox.c();
            } else if ((modelForRole.getMyPermissionsForChannel() & j) == j) {
                ternaryCheckBox.setOffDisabled(R.string.cannot_deny_singular_permission);
            } else {
                ternaryCheckBox.setOffDisabled(R.string.cannot_deny_missing_permission);
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:38:0x0070  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void updateCheckboxLabels(com.discord.views.TernaryCheckBox r4, int r5, boolean r6, com.discord.utilities.channel.PermissionLabelOverrides r7) {
        /*
            r3 = this;
            if (r7 == 0) goto L78
            r0 = 0
            if (r5 == 0) goto L66
            r1 = 2
            if (r5 == r1) goto L57
            r1 = 13
            if (r5 == r1) goto L3c
            r1 = 4
            if (r5 == r1) goto L28
            r6 = 5
            if (r5 == r6) goto L13
            goto L6d
        L13:
            java.lang.CharSequence r5 = r7.getAnnouncementChannelSubtext()
            b.a.i.w3 r6 = r4.k
            com.discord.utilities.view.text.LinkifiedTextView r6 = r6.f
            java.lang.String r7 = "binding.settingSubtext"
            d0.z.d.m.checkNotNullExpressionValue(r6, r7)
            android.text.method.MovementMethod r7 = android.text.method.LinkMovementMethod.getInstance()
            r6.setMovementMethod(r7)
            goto L6e
        L28:
            java.lang.String r5 = r7.getCategoryLabel()
            if (r6 == 0) goto L32
            java.lang.CharSequence r0 = r7.getCategoryEveryoneSubtext()
        L32:
            if (r0 != 0) goto L38
            java.lang.CharSequence r0 = r7.getCategorySubtext()
        L38:
            r2 = r0
            r0 = r5
            r5 = r2
            goto L6e
        L3c:
            if (r6 == 0) goto L49
            java.lang.String r5 = r7.getStageChannelEveryoneSubtext()
            if (r5 != 0) goto L4a
            java.lang.String r5 = r7.getVoiceChannelEveryoneSubtext()
            goto L4a
        L49:
            r5 = r0
        L4a:
            if (r5 != 0) goto L50
            java.lang.CharSequence r5 = r7.getStageChannelSubtext()
        L50:
            if (r5 != 0) goto L6e
            java.lang.String r5 = r7.getVoiceChannelSubtext()
            goto L6e
        L57:
            if (r6 == 0) goto L5e
            java.lang.String r5 = r7.getVoiceChannelEveryoneSubtext()
            goto L5f
        L5e:
            r5 = r0
        L5f:
            if (r5 != 0) goto L6e
            java.lang.String r5 = r7.getVoiceChannelSubtext()
            goto L6e
        L66:
            if (r6 == 0) goto L6d
            java.lang.String r5 = r7.getTextChannelEveryoneSubtext()
            goto L6e
        L6d:
            r5 = r0
        L6e:
            if (r0 == 0) goto L73
            r4.setLabel(r0)
        L73:
            if (r5 == 0) goto L78
            r4.setSubtext(r5)
        L78:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions.updateCheckboxLabels(com.discord.views.TernaryCheckBox, int, boolean, com.discord.utilities.channel.PermissionLabelOverrides):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updatePermissionOverwrites(long j, long j2, int i) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updatePermissionOverwrites(j, j2, new WidgetChannelSettingsEditPermissions$updatePermissionOverwrites$1(this, i, j2).invoke()), false, 1, null), this, null, 2, null), WidgetChannelSettingsEditPermissions.class, (r18 & 2) != 0 ? null : requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetChannelSettingsEditPermissions$updatePermissionOverwrites$2.INSTANCE);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        StatefulViews statefulViews = new StatefulViews(getPermissionCheckboxes());
        this.state = statefulViews;
        if (statefulViews != null) {
            statefulViews.setupUnsavedChangesConfirmation(this);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(WidgetChannelSettingsEditPermissionsModel.Companion.get(getGuildId(), getChannelId(), getTargetId(), getType()), this, null, 2, null), WidgetChannelSettingsEditPermissions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelSettingsEditPermissions$onViewBoundOrOnResume$1(this));
    }
}
