package com.discord.widgets.channels.permissions;

import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChannelSettingsAddMemberBinding;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelSettingsAddMemberFragment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter;", "invoke", "()Lcom/discord/widgets/channels/permissions/AddMemberAdapter;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsAddMemberFragment$adapter$2 extends o implements Function0<AddMemberAdapter> {
    public final /* synthetic */ WidgetChannelSettingsAddMemberFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsAddMemberFragment$adapter$2(WidgetChannelSettingsAddMemberFragment widgetChannelSettingsAddMemberFragment) {
        super(0);
        this.this$0 = widgetChannelSettingsAddMemberFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AddMemberAdapter invoke() {
        WidgetChannelSettingsAddMemberBinding binding;
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        binding = this.this$0.getBinding();
        RecyclerView recyclerView = binding.f2252b;
        m.checkNotNullExpressionValue(recyclerView, "binding.recycler");
        return (AddMemberAdapter) companion.configure(new AddMemberAdapter(recyclerView));
    }
}
