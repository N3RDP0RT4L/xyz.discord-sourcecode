package com.discord.widgets.channels.permissions;

import android.content.Context;
import com.discord.api.role.GuildRole;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAdvanced;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelSettingsPermissionsAdvanced.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/role/GuildRole;", "role", "", "invoke", "(Lcom/discord/api/role/GuildRole;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsPermissionsAdvanced$configureRoles$1 extends o implements Function1<GuildRole, Unit> {
    public final /* synthetic */ WidgetChannelSettingsPermissionsAdvanced.Model $model;
    public final /* synthetic */ WidgetChannelSettingsPermissionsAdvanced this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsPermissionsAdvanced$configureRoles$1(WidgetChannelSettingsPermissionsAdvanced widgetChannelSettingsPermissionsAdvanced, WidgetChannelSettingsPermissionsAdvanced.Model model) {
        super(1);
        this.this$0 = widgetChannelSettingsPermissionsAdvanced;
        this.$model = model;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GuildRole guildRole) {
        invoke2(guildRole);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GuildRole guildRole) {
        WidgetChannelSettingsEditPermissions.Companion companion = WidgetChannelSettingsEditPermissions.Companion;
        Context requireContext = this.this$0.requireContext();
        long id2 = this.$model.getGuild().getId();
        long h = this.$model.getChannel().h();
        m.checkNotNull(guildRole);
        companion.createForRole(requireContext, id2, h, guildRole.getId());
    }
}
