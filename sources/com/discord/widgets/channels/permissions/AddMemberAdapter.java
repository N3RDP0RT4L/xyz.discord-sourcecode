package com.discord.widgets.channels.permissions;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.databinding.WidgetChannelSettingsAddMemberCategoryBinding;
import com.discord.databinding.WidgetChannelSettingsAddMemberItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.permissions.ChannelPermissionOwnerView;
import com.discord.widgets.channels.permissions.AddMemberAdapter;
import com.discord.widgets.channels.permissions.PermissionOwner;
import com.google.android.material.checkbox.MaterialCheckBox;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: AddMemberAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0017\u0018\u0019B\u000f\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016J+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ/\u0010\u0010\u001a\u00020\r2 \u0010\u000f\u001a\u001c\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r\u0018\u00010\nj\u0004\u0018\u0001`\u000e¢\u0006\u0004\b\u0010\u0010\u0011R0\u0010\u000f\u001a\u001c\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r\u0018\u00010\nj\u0004\u0018\u0001`\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000f\u0010\u0012¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function2;", "", "Lcom/discord/api/permission/PermissionOverwrite$Type;", "", "Lcom/discord/widgets/channels/permissions/OnClickListener;", "onClickListener", "setOnClickListener", "(Lkotlin/jvm/functions/Function2;)V", "Lkotlin/jvm/functions/Function2;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "AddMemberAdapterCategoryItem", "AddMemberAdapterItemItem", "Item", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AddMemberAdapter extends MGRecyclerAdapterSimple<Item> {
    private Function2<? super Long, ? super PermissionOverwrite.Type, Unit> onClickListener;

    /* compiled from: AddMemberAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$AddMemberAdapterCategoryItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;)V", "Lcom/discord/databinding/WidgetChannelSettingsAddMemberCategoryBinding;", "binding", "Lcom/discord/databinding/WidgetChannelSettingsAddMemberCategoryBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/channels/permissions/AddMemberAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AddMemberAdapterCategoryItem extends MGRecyclerViewHolder<AddMemberAdapter, Item> {
        private final WidgetChannelSettingsAddMemberCategoryBinding binding;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                Item.CategoryItem.Companion.CategoryType.values();
                int[] iArr = new int[2];
                $EnumSwitchMapping$0 = iArr;
                iArr[Item.CategoryItem.Companion.CategoryType.ROLE.ordinal()] = 1;
                iArr[Item.CategoryItem.Companion.CategoryType.MEMBER.ordinal()] = 2;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AddMemberAdapterCategoryItem(AddMemberAdapter addMemberAdapter) {
            super((int) R.layout.widget_channel_settings_add_member_category, addMemberAdapter);
            m.checkNotNullParameter(addMemberAdapter, "adapter");
            View view = this.itemView;
            TextView textView = (TextView) view.findViewById(R.id.label);
            if (textView != null) {
                WidgetChannelSettingsAddMemberCategoryBinding widgetChannelSettingsAddMemberCategoryBinding = new WidgetChannelSettingsAddMemberCategoryBinding((LinearLayout) view, textView);
                m.checkNotNullExpressionValue(widgetChannelSettingsAddMemberCategoryBinding, "WidgetChannelSettingsAdd…oryBinding.bind(itemView)");
                this.binding = widgetChannelSettingsAddMemberCategoryBinding;
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.label)));
        }

        public void onConfigure(int i, Item item) {
            String str;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            TextView textView = this.binding.f2253b;
            m.checkNotNullExpressionValue(textView, "binding.label");
            int ordinal = ((Item.CategoryItem) item).getCategoryType().ordinal();
            if (ordinal == 0) {
                str = ((AddMemberAdapter) this.adapter).getRecycler().getContext().getString(R.string.roles);
            } else if (ordinal == 1) {
                str = ((AddMemberAdapter) this.adapter).getRecycler().getContext().getString(R.string.members);
            } else {
                throw new NoWhenBranchMatchedException();
            }
            textView.setText(str);
        }
    }

    /* compiled from: AddMemberAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0015\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$AddMemberAdapterItemItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem;", "item", "", "handleClick", "(Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem;)V", "Lcom/discord/widgets/channels/permissions/PermissionOwner;", "permissionOwner", "handleOnClickForPermissionOwner", "(Lcom/discord/widgets/channels/permissions/PermissionOwner;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "onConfigure", "(ILcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;)V", "Lcom/discord/databinding/WidgetChannelSettingsAddMemberItemBinding;", "binding", "Lcom/discord/databinding/WidgetChannelSettingsAddMemberItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/channels/permissions/AddMemberAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AddMemberAdapterItemItem extends MGRecyclerViewHolder<AddMemberAdapter, Item> {
        private final WidgetChannelSettingsAddMemberItemBinding binding;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                Item.PermissionOwnerItem.Companion.AddStatus.CannotAdd.Reason.values();
                int[] iArr = new int[1];
                $EnumSwitchMapping$0 = iArr;
                iArr[Item.PermissionOwnerItem.Companion.AddStatus.CannotAdd.Reason.HAS_GUILD_PERMISSIONS.ordinal()] = 1;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AddMemberAdapterItemItem(AddMemberAdapter addMemberAdapter) {
            super((int) R.layout.widget_channel_settings_add_member_item, addMemberAdapter);
            m.checkNotNullParameter(addMemberAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.channel_permission_owner_view;
            ChannelPermissionOwnerView channelPermissionOwnerView = (ChannelPermissionOwnerView) view.findViewById(R.id.channel_permission_owner_view);
            if (channelPermissionOwnerView != null) {
                i = R.id.checkbox;
                MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.checkbox);
                if (materialCheckBox != null) {
                    i = R.id.container;
                    ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.container);
                    if (constraintLayout != null) {
                        WidgetChannelSettingsAddMemberItemBinding widgetChannelSettingsAddMemberItemBinding = new WidgetChannelSettingsAddMemberItemBinding((FrameLayout) view, channelPermissionOwnerView, materialCheckBox, constraintLayout);
                        m.checkNotNullExpressionValue(widgetChannelSettingsAddMemberItemBinding, "WidgetChannelSettingsAdd…temBinding.bind(itemView)");
                        this.binding = widgetChannelSettingsAddMemberItemBinding;
                        return;
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void handleClick(Item.PermissionOwnerItem permissionOwnerItem) {
            PermissionOwner permissionOwner = permissionOwnerItem.getPermissionOwner();
            Item.PermissionOwnerItem.Companion.AddStatus addStatus = permissionOwnerItem.getAddStatus();
            if (m.areEqual(addStatus, Item.PermissionOwnerItem.Companion.AddStatus.CanAdd.INSTANCE)) {
                handleOnClickForPermissionOwner(permissionOwner);
            } else if ((addStatus instanceof Item.PermissionOwnerItem.Companion.AddStatus.CannotAdd) && ((Item.PermissionOwnerItem.Companion.AddStatus.CannotAdd) permissionOwnerItem.getAddStatus()).getReason().ordinal() == 0) {
                FrameLayout frameLayout = this.binding.a;
                m.checkNotNullExpressionValue(frameLayout, "binding.root");
                Toast.makeText(frameLayout.getContext(), (int) R.string.channel_permissions_add_has_guild_permissions, 0).show();
            }
        }

        private final void handleOnClickForPermissionOwner(PermissionOwner permissionOwner) {
            Function2 function2 = ((AddMemberAdapter) this.adapter).onClickListener;
            if (function2 == null) {
                return;
            }
            if (permissionOwner instanceof PermissionOwner.Member) {
                function2.invoke(Long.valueOf(((PermissionOwner.Member) permissionOwner).getUser().getId()), PermissionOverwrite.Type.MEMBER);
            } else if (permissionOwner instanceof PermissionOwner.Role) {
                function2.invoke(Long.valueOf(((PermissionOwner.Role) permissionOwner).getRole().getId()), PermissionOverwrite.Type.ROLE);
            }
        }

        public void onConfigure(int i, Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            final Item.PermissionOwnerItem permissionOwnerItem = (Item.PermissionOwnerItem) item;
            this.binding.f2254b.a(permissionOwnerItem.getPermissionOwner());
            MaterialCheckBox materialCheckBox = this.binding.c;
            m.checkNotNullExpressionValue(materialCheckBox, "binding.checkbox");
            materialCheckBox.setChecked(permissionOwnerItem.getChecked());
            this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.permissions.AddMemberAdapter$AddMemberAdapterItemItem$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AddMemberAdapter.AddMemberAdapterItemItem.this.handleClick(permissionOwnerItem);
                }
            });
            this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.permissions.AddMemberAdapter$AddMemberAdapterItemItem$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AddMemberAdapter.AddMemberAdapterItemItem.this.handleClick(permissionOwnerItem);
                }
            });
            ConstraintLayout constraintLayout = this.binding.d;
            m.checkNotNullExpressionValue(constraintLayout, "binding.container");
            ViewExtensions.setEnabledAlpha$default(constraintLayout, m.areEqual(permissionOwnerItem.getAddStatus(), Item.PermissionOwnerItem.Companion.AddStatus.CanAdd.INSTANCE), 0.0f, 2, null);
        }
    }

    /* compiled from: AddMemberAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0003\u0005\u0004\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", "Companion", "CategoryItem", "PermissionOwnerItem", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item implements MGRecyclerDataPayload {
        public static final Companion Companion = new Companion(null);
        public static final int TYPE_CATEGORY = 0;
        public static final int TYPE_PERMISSION_OWNER = 1;

        /* compiled from: AddMemberAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\rR\u001c\u0010\u0016\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem$Companion$CategoryType;", "component1", "()Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem$Companion$CategoryType;", "categoryType", "copy", "(Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem$Companion$CategoryType;)Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem$Companion$CategoryType;", "getCategoryType", HookHelper.constructorName, "(Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem$Companion$CategoryType;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CategoryItem extends Item {
            public static final Companion Companion = new Companion(null);
            private final Companion.CategoryType categoryType;
            private final String key;
            private final int type;

            /* compiled from: AddMemberAdapter.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem$Companion;", "", HookHelper.constructorName, "()V", "CategoryType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Companion {

                /* compiled from: AddMemberAdapter.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$CategoryItem$Companion$CategoryType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ROLE", "MEMBER", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                /* loaded from: classes2.dex */
                public enum CategoryType {
                    ROLE,
                    MEMBER
                }

                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public CategoryItem(Companion.CategoryType categoryType) {
                super(null);
                m.checkNotNullParameter(categoryType, "categoryType");
                this.categoryType = categoryType;
                this.key = categoryType.toString();
            }

            public static /* synthetic */ CategoryItem copy$default(CategoryItem categoryItem, Companion.CategoryType categoryType, int i, Object obj) {
                if ((i & 1) != 0) {
                    categoryType = categoryItem.categoryType;
                }
                return categoryItem.copy(categoryType);
            }

            public final Companion.CategoryType component1() {
                return this.categoryType;
            }

            public final CategoryItem copy(Companion.CategoryType categoryType) {
                m.checkNotNullParameter(categoryType, "categoryType");
                return new CategoryItem(categoryType);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof CategoryItem) && m.areEqual(this.categoryType, ((CategoryItem) obj).categoryType);
                }
                return true;
            }

            public final Companion.CategoryType getCategoryType() {
                return this.categoryType;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                Companion.CategoryType categoryType = this.categoryType;
                if (categoryType != null) {
                    return categoryType.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("CategoryItem(categoryType=");
                R.append(this.categoryType);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: AddMemberAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$Companion;", "", "", "TYPE_CATEGORY", "I", "TYPE_PERMISSION_OWNER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: AddMemberAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u0000 (2\u00020\u0001:\u0001(B\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00052\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\u0007R\u001c\u0010\u001c\u001a\u00020\u00108\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u0012R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001f\u001a\u0004\b \u0010\nR\u001c\u0010!\u001a\u00020\u00138\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\u0015R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010$\u001a\u0004\b%\u0010\u0004¨\u0006)"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "Lcom/discord/widgets/channels/permissions/PermissionOwner;", "component1", "()Lcom/discord/widgets/channels/permissions/PermissionOwner;", "", "component2", "()Z", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;", "component3", "()Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;", "permissionOwner", "checked", "addStatus", "copy", "(Lcom/discord/widgets/channels/permissions/PermissionOwner;ZLcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;)Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getChecked", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;", "getAddStatus", "type", "I", "getType", "Lcom/discord/widgets/channels/permissions/PermissionOwner;", "getPermissionOwner", HookHelper.constructorName, "(Lcom/discord/widgets/channels/permissions/PermissionOwner;ZLcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PermissionOwnerItem extends Item {
            public static final Companion Companion = new Companion(null);
            private final Companion.AddStatus addStatus;
            private final boolean checked;
            private final String key;
            private final PermissionOwner permissionOwner;
            private final int type = 1;

            /* compiled from: AddMemberAdapter.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion;", "", HookHelper.constructorName, "()V", "AddStatus", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Companion {

                /* compiled from: AddMemberAdapter.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;", "", HookHelper.constructorName, "()V", "CanAdd", "CannotAdd", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CanAdd;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                /* loaded from: classes2.dex */
                public static abstract class AddStatus {

                    /* compiled from: AddMemberAdapter.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CanAdd;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                    /* loaded from: classes2.dex */
                    public static final class CanAdd extends AddStatus {
                        public static final CanAdd INSTANCE = new CanAdd();

                        private CanAdd() {
                            super(null);
                        }
                    }

                    /* compiled from: AddMemberAdapter.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0017B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus;", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd$Reason;", "component1", "()Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd$Reason;", ModelAuditLogEntry.CHANGE_KEY_REASON, "copy", "(Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd$Reason;)Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd$Reason;", "getReason", HookHelper.constructorName, "(Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd$Reason;)V", "Reason", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                    /* loaded from: classes2.dex */
                    public static final class CannotAdd extends AddStatus {
                        private final Reason reason;

                        /* compiled from: AddMemberAdapter.kt */
                        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item$PermissionOwnerItem$Companion$AddStatus$CannotAdd$Reason;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "HAS_GUILD_PERMISSIONS", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                        /* loaded from: classes2.dex */
                        public enum Reason {
                            HAS_GUILD_PERMISSIONS
                        }

                        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                        public CannotAdd(Reason reason) {
                            super(null);
                            m.checkNotNullParameter(reason, ModelAuditLogEntry.CHANGE_KEY_REASON);
                            this.reason = reason;
                        }

                        public static /* synthetic */ CannotAdd copy$default(CannotAdd cannotAdd, Reason reason, int i, Object obj) {
                            if ((i & 1) != 0) {
                                reason = cannotAdd.reason;
                            }
                            return cannotAdd.copy(reason);
                        }

                        public final Reason component1() {
                            return this.reason;
                        }

                        public final CannotAdd copy(Reason reason) {
                            m.checkNotNullParameter(reason, ModelAuditLogEntry.CHANGE_KEY_REASON);
                            return new CannotAdd(reason);
                        }

                        public boolean equals(Object obj) {
                            if (this != obj) {
                                return (obj instanceof CannotAdd) && m.areEqual(this.reason, ((CannotAdd) obj).reason);
                            }
                            return true;
                        }

                        public final Reason getReason() {
                            return this.reason;
                        }

                        public int hashCode() {
                            Reason reason = this.reason;
                            if (reason != null) {
                                return reason.hashCode();
                            }
                            return 0;
                        }

                        public String toString() {
                            StringBuilder R = a.R("CannotAdd(reason=");
                            R.append(this.reason);
                            R.append(")");
                            return R.toString();
                        }
                    }

                    private AddStatus() {
                    }

                    public /* synthetic */ AddStatus(DefaultConstructorMarker defaultConstructorMarker) {
                        this();
                    }
                }

                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public PermissionOwnerItem(PermissionOwner permissionOwner, boolean z2, Companion.AddStatus addStatus) {
                super(null);
                String str;
                m.checkNotNullParameter(permissionOwner, "permissionOwner");
                m.checkNotNullParameter(addStatus, "addStatus");
                this.permissionOwner = permissionOwner;
                this.checked = z2;
                this.addStatus = addStatus;
                if (permissionOwner instanceof PermissionOwner.Member) {
                    str = String.valueOf(((PermissionOwner.Member) permissionOwner).getUser().getId());
                } else if (permissionOwner instanceof PermissionOwner.Role) {
                    str = String.valueOf(((PermissionOwner.Role) permissionOwner).getRole().getId());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                this.key = str;
            }

            public static /* synthetic */ PermissionOwnerItem copy$default(PermissionOwnerItem permissionOwnerItem, PermissionOwner permissionOwner, boolean z2, Companion.AddStatus addStatus, int i, Object obj) {
                if ((i & 1) != 0) {
                    permissionOwner = permissionOwnerItem.permissionOwner;
                }
                if ((i & 2) != 0) {
                    z2 = permissionOwnerItem.checked;
                }
                if ((i & 4) != 0) {
                    addStatus = permissionOwnerItem.addStatus;
                }
                return permissionOwnerItem.copy(permissionOwner, z2, addStatus);
            }

            public final PermissionOwner component1() {
                return this.permissionOwner;
            }

            public final boolean component2() {
                return this.checked;
            }

            public final Companion.AddStatus component3() {
                return this.addStatus;
            }

            public final PermissionOwnerItem copy(PermissionOwner permissionOwner, boolean z2, Companion.AddStatus addStatus) {
                m.checkNotNullParameter(permissionOwner, "permissionOwner");
                m.checkNotNullParameter(addStatus, "addStatus");
                return new PermissionOwnerItem(permissionOwner, z2, addStatus);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof PermissionOwnerItem)) {
                    return false;
                }
                PermissionOwnerItem permissionOwnerItem = (PermissionOwnerItem) obj;
                return m.areEqual(this.permissionOwner, permissionOwnerItem.permissionOwner) && this.checked == permissionOwnerItem.checked && m.areEqual(this.addStatus, permissionOwnerItem.addStatus);
            }

            public final Companion.AddStatus getAddStatus() {
                return this.addStatus;
            }

            public final boolean getChecked() {
                return this.checked;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final PermissionOwner getPermissionOwner() {
                return this.permissionOwner;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                PermissionOwner permissionOwner = this.permissionOwner;
                int i = 0;
                int hashCode = (permissionOwner != null ? permissionOwner.hashCode() : 0) * 31;
                boolean z2 = this.checked;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode + i2) * 31;
                Companion.AddStatus addStatus = this.addStatus;
                if (addStatus != null) {
                    i = addStatus.hashCode();
                }
                return i4 + i;
            }

            public String toString() {
                StringBuilder R = a.R("PermissionOwnerItem(permissionOwner=");
                R.append(this.permissionOwner);
                R.append(", checked=");
                R.append(this.checked);
                R.append(", addStatus=");
                R.append(this.addStatus);
                R.append(")");
                return R.toString();
            }
        }

        private Item() {
        }

        public /* synthetic */ Item(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddMemberAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recyclerView");
    }

    public final void setOnClickListener(Function2<? super Long, ? super PermissionOverwrite.Type, Unit> function2) {
        this.onClickListener = function2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<AddMemberAdapter, Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new AddMemberAdapterCategoryItem(this);
        }
        if (i == 1) {
            return new AddMemberAdapterItemItem(this);
        }
        throw invalidViewTypeException(i);
    }
}
