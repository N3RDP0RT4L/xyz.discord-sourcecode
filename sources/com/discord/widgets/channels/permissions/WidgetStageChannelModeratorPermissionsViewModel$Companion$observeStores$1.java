package com.discord.widgets.channels.permissions;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.models.guild.Guild;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.utilities.stage.StageChannelUtils;
import com.discord.widgets.channels.permissions.WidgetStageChannelModeratorPermissionsViewModel;
import d0.t.h0;
import d0.t.m;
import d0.t.n;
import d0.t.u;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", "invoke", "()Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStageChannelModeratorPermissionsViewModel$Companion$observeStores$1 extends o implements Function0<WidgetStageChannelModeratorPermissionsViewModel.StoreState> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreChannels $channelStore;
    public final /* synthetic */ StoreGuilds $guildStore;
    public final /* synthetic */ StorePermissions $permissionStore;
    public final /* synthetic */ StoreUser $userStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStageChannelModeratorPermissionsViewModel$Companion$observeStores$1(StoreChannels storeChannels, long j, StoreGuilds storeGuilds, StoreUser storeUser, StorePermissions storePermissions) {
        super(0);
        this.$channelStore = storeChannels;
        this.$channelId = j;
        this.$guildStore = storeGuilds;
        this.$userStore = storeUser;
        this.$permissionStore = storePermissions;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetStageChannelModeratorPermissionsViewModel.StoreState invoke() {
        Channel channel = this.$channelStore.getChannel(this.$channelId);
        if (channel == null || channel.A() != 13) {
            return WidgetStageChannelModeratorPermissionsViewModel.StoreState.Invalid.INSTANCE;
        }
        Guild guild = (Guild) a.u0(channel, this.$guildStore.getGuilds());
        if (guild == null) {
            return WidgetStageChannelModeratorPermissionsViewModel.StoreState.Invalid.INSTANCE;
        }
        List<PermissionOverwrite> s2 = channel.s();
        if (s2 == null) {
            s2 = n.emptyList();
        }
        ArrayList<PermissionOverwrite> arrayList = new ArrayList();
        Iterator<T> it = s2.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((PermissionOverwrite) next).f() != PermissionOverwrite.Type.MEMBER) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        for (PermissionOverwrite permissionOverwrite : arrayList) {
            arrayList2.add(Long.valueOf(permissionOverwrite.e()));
        }
        Set set = u.toSet(arrayList2);
        Map map = (Map) a.u0(channel, this.$guildStore.getRoles());
        if (map == null) {
            map = h0.emptyMap();
        }
        Map map2 = map;
        Map<Long, User> users = this.$userStore.getUsers(set, true);
        Map map3 = (Map) a.u0(channel, this.$guildStore.getMembers());
        if (map3 == null) {
            map3 = h0.emptyMap();
        }
        return new WidgetStageChannelModeratorPermissionsViewModel.StoreState.Valid(guild, s2, map2, users, map3, this.$userStore.getUsers(m.listOf(Long.valueOf(guild.getOwnerId())), true).get(Long.valueOf(guild.getOwnerId())), StageChannelUtils.computeCanEditStageModerators(this.$permissionStore, this.$guildStore, this.$userStore.getMe().getId(), guild.getId(), channel));
    }
}
