package com.discord.widgets.channels.permissions;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel;
import com.discord.widgets.chat.list.NewThreadsPermissionsFeatureFlag;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import d0.t.n;
import d0.t.r;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func8;
/* compiled from: WidgetChannelSettingsEditPermissionsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u001a2\u00020\u0001:\u0003\u001a\u001b\u001cB1\b\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u0011\u001a\u00020\f¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u001c\u0010\r\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u001c\u0010\u0014\u001a\u00020\u00138\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\u0082\u0001\u0002\u001d\u001e¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;", "", "", "type", "I", "getType", "()I", "", "targetId", "J", "getTargetId", "()J", "", "useNewThreadsPermissions", "Z", "getUseNewThreadsPermissions", "()Z", "hasEventFeature", "getHasEventFeature", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;JIZZ)V", "Companion", "ModelForRole", "ModelForUser", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel$ModelForRole;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel$ModelForUser;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class WidgetChannelSettingsEditPermissionsModel {
    public static final Companion Companion = new Companion(null);
    private final Channel channel;
    private final boolean hasEventFeature;
    private final long targetId;
    private final int type;
    private final boolean useNewThreadsPermissions;

    /* compiled from: WidgetChannelSettingsEditPermissionsModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b'\u0010(Jc\u0010\u0013\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u00112\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\n\u0010\b\u001a\u00060\u0002j\u0002`\u00072\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J;\u0010\u0017\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u00112\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\n\u0010\u0016\u001a\u00060\u0002j\u0002`\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J+\u0010 \u001a\u00020\u001f2\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u001b2\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u001dH\u0002¢\u0006\u0004\b \u0010!J5\u0010%\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u00112\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b%\u0010&¨\u0006)"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/UserId;", "targetUserId", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lrx/Observable;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;", "getForUser", "(JJJLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", "Lcom/discord/primitives/RoleId;", "targetRoleId", "getForRole", "(JJJ)Lrx/Observable;", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/user/MeUser;", "meUser", "Lcom/discord/api/permission/PermissionBit;", "myPermissionsForChannel", "", "isAbleToManagePerms", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;J)Z", "targetId", "", "type", "get", "(JJJI)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<WidgetChannelSettingsEditPermissionsModel> getForRole(long j, long j2, final long j3) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable d = Observable.d(companion.getChannels().observeChannel(j2), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getGuilds().observeGuild(j), companion.getPermissions().observePermissionsForChannel(j2), companion.getGuilds().observeRoles(j), companion.getGuilds().observeComputed(j), NewThreadsPermissionsFeatureFlag.Companion.getINSTANCE().observeEnabled(j), StageEventsGuildsFeatureFlag.Companion.getINSTANCE().observeCanGuildAccessStageEvents(j), new Func8<Channel, MeUser, Guild, Long, Map<Long, ? extends GuildRole>, Map<Long, ? extends GuildMember>, Boolean, Boolean, WidgetChannelSettingsEditPermissionsModel>() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel$Companion$getForRole$1

                /* compiled from: WidgetChannelSettingsEditPermissionsModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/permission/PermissionOverwrite;", "kotlin.jvm.PlatformType", "<name for destructuring parameter 0>", "", "invoke", "(Lcom/discord/api/permission/PermissionOverwrite;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel$Companion$getForRole$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function1<PermissionOverwrite, Boolean> {
                    public AnonymousClass1() {
                        super(1);
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Boolean invoke(PermissionOverwrite permissionOverwrite) {
                        return Boolean.valueOf(invoke2(permissionOverwrite));
                    }

                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final boolean invoke2(PermissionOverwrite permissionOverwrite) {
                        return permissionOverwrite.a() == j3;
                    }
                }

                /* compiled from: WidgetChannelSettingsEditPermissionsModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/permission/PermissionOverwrite;", "kotlin.jvm.PlatformType", "<name for destructuring parameter 0>", "", "invoke", "(Lcom/discord/api/permission/PermissionOverwrite;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel$Companion$getForRole$1$2  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass2 extends o implements Function1<PermissionOverwrite, Boolean> {
                    public AnonymousClass2() {
                        super(1);
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Boolean invoke(PermissionOverwrite permissionOverwrite) {
                        return Boolean.valueOf(invoke2(permissionOverwrite));
                    }

                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final boolean invoke2(PermissionOverwrite permissionOverwrite) {
                        return permissionOverwrite.a() == j3;
                    }
                }

                @Override // rx.functions.Func8
                public /* bridge */ /* synthetic */ WidgetChannelSettingsEditPermissionsModel call(Channel channel, MeUser meUser, Guild guild, Long l, Map<Long, ? extends GuildRole> map, Map<Long, ? extends GuildMember> map2, Boolean bool, Boolean bool2) {
                    return call2(channel, meUser, guild, l, (Map<Long, GuildRole>) map, (Map<Long, GuildMember>) map2, bool, bool2);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetChannelSettingsEditPermissionsModel call2(Channel channel, MeUser meUser, Guild guild, Long l, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Boolean bool, Boolean bool2) {
                    boolean isAbleToManagePerms;
                    GuildMember guildMember;
                    List<Long> roles;
                    GuildRole guildRole = map.get(Long.valueOf(j3));
                    if (!(channel == null || guild == null || guildRole == null || l == null)) {
                        WidgetChannelSettingsEditPermissionsModel.Companion companion2 = WidgetChannelSettingsEditPermissionsModel.Companion;
                        m.checkNotNullExpressionValue(meUser, "meUser");
                        isAbleToManagePerms = companion2.isAbleToManagePerms(guild, meUser, l.longValue());
                        if (isAbleToManagePerms) {
                            ArrayList arrayList = new ArrayList(channel.s() != null ? channel.s() : n.emptyList());
                            long j4 = j3;
                            PermissionOverwrite.Type type = PermissionOverwrite.Type.ROLE;
                            PermissionOverwrite permissionOverwrite = new PermissionOverwrite(j4, type, 0L, Permission.ALL);
                            r.removeAll((List) arrayList, (Function1) new AnonymousClass1());
                            arrayList.add(permissionOverwrite);
                            PermissionOverwrite permissionOverwrite2 = new PermissionOverwrite(j3, type, 0L, 0L);
                            r.removeAll((List) arrayList, (Function1) new AnonymousClass2());
                            arrayList.add(permissionOverwrite2);
                            boolean z2 = false;
                            boolean z3 = j3 == guild.getId();
                            long longValue = l.longValue();
                            long j5 = j3;
                            long computeNonThreadPermissions = PermissionUtils.computeNonThreadPermissions(meUser.getId(), guild.getId(), guild.getOwnerId(), map2.get(Long.valueOf(meUser.getId())), map, arrayList);
                            long computeNonThreadPermissions2 = PermissionUtils.computeNonThreadPermissions(meUser.getId(), guild.getId(), guild.getOwnerId(), map2.get(Long.valueOf(meUser.getId())), map, arrayList);
                            if (z3 || !((guildMember = map2.get(Long.valueOf(meUser.getId()))) == null || (roles = guildMember.getRoles()) == null || !roles.contains(Long.valueOf(j3)))) {
                                z2 = true;
                            }
                            m.checkNotNullExpressionValue(bool, "useNewThreadsPermissions");
                            boolean booleanValue = bool.booleanValue();
                            m.checkNotNullExpressionValue(bool2, "hasEventFeature");
                            return new WidgetChannelSettingsEditPermissionsModel.ModelForRole(channel, longValue, guildRole, j5, computeNonThreadPermissions, computeNonThreadPermissions2, z2, z3, booleanValue, bool2.booleanValue());
                        }
                    }
                    return null;
                }
            });
            m.checkNotNullExpressionValue(d, "Observable\n            .…          }\n            }");
            Observable<WidgetChannelSettingsEditPermissionsModel> q = ObservableExtensionsKt.computationLatest(d).q();
            m.checkNotNullExpressionValue(q, "Observable\n            .…  .distinctUntilChanged()");
            return q;
        }

        private final Observable<WidgetChannelSettingsEditPermissionsModel> getForUser(long j, long j2, long j3, StoreChannels storeChannels, StoreUser storeUser, StoreGuilds storeGuilds, StorePermissions storePermissions) {
            Observable<WidgetChannelSettingsEditPermissionsModel> q = ObservableExtensionsKt.computationLatest(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{storeChannels, storeUser, storeGuilds, storePermissions}, false, null, null, new WidgetChannelSettingsEditPermissionsModel$Companion$getForUser$1(storeChannels, j2, storeUser, j3, storeGuilds, j, storePermissions), 14, null)).q();
            m.checkNotNullExpressionValue(q, "ObservationDeckProvider.…  .distinctUntilChanged()");
            return q;
        }

        public static /* synthetic */ Observable getForUser$default(Companion companion, long j, long j2, long j3, StoreChannels storeChannels, StoreUser storeUser, StoreGuilds storeGuilds, StorePermissions storePermissions, int i, Object obj) {
            return companion.getForUser(j, j2, j3, (i & 8) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 16) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 32) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 64) != 0 ? StoreStream.Companion.getPermissions() : storePermissions);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final boolean isAbleToManagePerms(Guild guild, MeUser meUser, long j) {
            return ((guild.getOwnerId() > meUser.getId() ? 1 : (guild.getOwnerId() == meUser.getId() ? 0 : -1)) == 0) || PermissionUtils.canAndIsElevated(Permission.MANAGE_ROLES, Long.valueOf(j), meUser.getMfaEnabled(), guild.getMfaLevel()) || PermissionUtils.canAndIsElevated(8L, Long.valueOf(j), meUser.getMfaEnabled(), guild.getMfaLevel());
        }

        public final Observable<WidgetChannelSettingsEditPermissionsModel> get(long j, long j2, long j3, int i) {
            if (i == 0) {
                return getForUser$default(this, j, j2, j3, null, null, null, null, 120, null);
            }
            if (i == 1) {
                return getForRole(j, j2, j3);
            }
            k kVar = new k(null);
            m.checkNotNullExpressionValue(kVar, "Observable\n              .just(null)");
            return kVar;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelSettingsEditPermissionsModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B[\u0012\u0006\u0010\u0019\u001a\u00020\b\u0012\n\u0010\u001a\u001a\u00060\u0002j\u0002`\u000b\u0012\u0006\u0010\u001b\u001a\u00020\u000e\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\u0006\u0010\u001e\u001a\u00020\u0002\u0012\u0006\u0010\u001f\u001a\u00020\u0004\u0012\u0006\u0010 \u001a\u00020\u0004\u0012\u0006\u0010!\u001a\u00020\u0004\u0012\u0006\u0010\"\u001a\u00020\u0004¢\u0006\u0004\b<\u0010=J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\u0002j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0011\u0010\rJ\u0010\u0010\u0012\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0012\u0010\rJ\u0010\u0010\u0013\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0013\u0010\rJ\u0010\u0010\u0014\u001a\u00020\u0004HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0004HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0004HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0015J\u0010\u0010\u0018\u001a\u00020\u0004HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0015Jx\u0010#\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\b2\f\b\u0002\u0010\u001a\u001a\u00060\u0002j\u0002`\u000b2\b\b\u0002\u0010\u001b\u001a\u00020\u000e2\b\b\u0002\u0010\u001c\u001a\u00020\u00022\b\b\u0002\u0010\u001d\u001a\u00020\u00022\b\b\u0002\u0010\u001e\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020\u00042\b\b\u0002\u0010 \u001a\u00020\u00042\b\b\u0002\u0010!\u001a\u00020\u00042\b\b\u0002\u0010\"\u001a\u00020\u0004HÆ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010)\u001a\u00020(HÖ\u0001¢\u0006\u0004\b)\u0010*J\u001a\u0010-\u001a\u00020\u00042\b\u0010,\u001a\u0004\u0018\u00010+HÖ\u0003¢\u0006\u0004\b-\u0010.R\u001c\u0010\u0019\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010/\u001a\u0004\b0\u0010\nR\u001c\u0010\u001c\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u00101\u001a\u0004\b2\u0010\rR\u001c\u0010!\u001a\u00020\u00048\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u00103\u001a\u0004\b4\u0010\u0015R\u001d\u0010\u001a\u001a\u00060\u0002j\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00101\u001a\u0004\b5\u0010\rR\u001c\u0010\"\u001a\u00020\u00048\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u00103\u001a\u0004\b6\u0010\u0015R\u0019\u0010\u001d\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b7\u0010\rR\u0019\u0010\u001b\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00108\u001a\u0004\b9\u0010\u0010R\u0019\u0010 \u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b \u00103\u001a\u0004\b \u0010\u0015R\u0019\u0010\u001f\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00103\u001a\u0004\b:\u0010\u0015R\u0019\u0010\u001e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b;\u0010\r¨\u0006>"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel$ModelForRole;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;", "", "permission", "", "canNeutralizeRolePermission", "(J)Z", "canDenyRolePermission", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/api/permission/PermissionBit;", "component2", "()J", "Lcom/discord/api/role/GuildRole;", "component3", "()Lcom/discord/api/role/GuildRole;", "component4", "component5", "component6", "component7", "()Z", "component8", "component9", "component10", "channel", "myPermissionsForChannel", "guildRole", "targetId", "myPermissionsWithRoleNeutral", "myPermissionsWithRoleDenied", "meHasRole", "isEveryoneRole", "useNewThreadsPermissions", "hasEventFeature", "copy", "(Lcom/discord/api/channel/Channel;JLcom/discord/api/role/GuildRole;JJJZZZZ)Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel$ModelForRole;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "J", "getTargetId", "Z", "getUseNewThreadsPermissions", "getMyPermissionsForChannel", "getHasEventFeature", "getMyPermissionsWithRoleNeutral", "Lcom/discord/api/role/GuildRole;", "getGuildRole", "getMeHasRole", "getMyPermissionsWithRoleDenied", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;JLcom/discord/api/role/GuildRole;JJJZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ModelForRole extends WidgetChannelSettingsEditPermissionsModel {
        private final Channel channel;
        private final GuildRole guildRole;
        private final boolean hasEventFeature;
        private final boolean isEveryoneRole;
        private final boolean meHasRole;
        private final long myPermissionsForChannel;
        private final long myPermissionsWithRoleDenied;
        private final long myPermissionsWithRoleNeutral;
        private final long targetId;
        private final boolean useNewThreadsPermissions;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ModelForRole(Channel channel, long j, GuildRole guildRole, long j2, long j3, long j4, boolean z2, boolean z3, boolean z4, boolean z5) {
            super(channel, j2, 1, z4, z5, null);
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(guildRole, "guildRole");
            this.channel = channel;
            this.myPermissionsForChannel = j;
            this.guildRole = guildRole;
            this.targetId = j2;
            this.myPermissionsWithRoleNeutral = j3;
            this.myPermissionsWithRoleDenied = j4;
            this.meHasRole = z2;
            this.isEveryoneRole = z3;
            this.useNewThreadsPermissions = z4;
            this.hasEventFeature = z5;
        }

        public final boolean canDenyRolePermission(long j) {
            return (this.myPermissionsWithRoleDenied & j) == (j & this.myPermissionsForChannel);
        }

        public final boolean canNeutralizeRolePermission(long j) {
            return (this.myPermissionsWithRoleNeutral & j) == (j & this.myPermissionsForChannel);
        }

        public final Channel component1() {
            return getChannel();
        }

        public final boolean component10() {
            return getHasEventFeature();
        }

        public final long component2() {
            return this.myPermissionsForChannel;
        }

        public final GuildRole component3() {
            return this.guildRole;
        }

        public final long component4() {
            return getTargetId();
        }

        public final long component5() {
            return this.myPermissionsWithRoleNeutral;
        }

        public final long component6() {
            return this.myPermissionsWithRoleDenied;
        }

        public final boolean component7() {
            return this.meHasRole;
        }

        public final boolean component8() {
            return this.isEveryoneRole;
        }

        public final boolean component9() {
            return getUseNewThreadsPermissions();
        }

        public final ModelForRole copy(Channel channel, long j, GuildRole guildRole, long j2, long j3, long j4, boolean z2, boolean z3, boolean z4, boolean z5) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(guildRole, "guildRole");
            return new ModelForRole(channel, j, guildRole, j2, j3, j4, z2, z3, z4, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ModelForRole)) {
                return false;
            }
            ModelForRole modelForRole = (ModelForRole) obj;
            return m.areEqual(getChannel(), modelForRole.getChannel()) && this.myPermissionsForChannel == modelForRole.myPermissionsForChannel && m.areEqual(this.guildRole, modelForRole.guildRole) && getTargetId() == modelForRole.getTargetId() && this.myPermissionsWithRoleNeutral == modelForRole.myPermissionsWithRoleNeutral && this.myPermissionsWithRoleDenied == modelForRole.myPermissionsWithRoleDenied && this.meHasRole == modelForRole.meHasRole && this.isEveryoneRole == modelForRole.isEveryoneRole && getUseNewThreadsPermissions() == modelForRole.getUseNewThreadsPermissions() && getHasEventFeature() == modelForRole.getHasEventFeature();
        }

        @Override // com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel
        public Channel getChannel() {
            return this.channel;
        }

        public final GuildRole getGuildRole() {
            return this.guildRole;
        }

        @Override // com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel
        public boolean getHasEventFeature() {
            return this.hasEventFeature;
        }

        public final boolean getMeHasRole() {
            return this.meHasRole;
        }

        public final long getMyPermissionsForChannel() {
            return this.myPermissionsForChannel;
        }

        public final long getMyPermissionsWithRoleDenied() {
            return this.myPermissionsWithRoleDenied;
        }

        public final long getMyPermissionsWithRoleNeutral() {
            return this.myPermissionsWithRoleNeutral;
        }

        @Override // com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel
        public long getTargetId() {
            return this.targetId;
        }

        @Override // com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel
        public boolean getUseNewThreadsPermissions() {
            return this.useNewThreadsPermissions;
        }

        public int hashCode() {
            Channel channel = getChannel();
            int i = 0;
            int a = (b.a(this.myPermissionsForChannel) + ((channel != null ? channel.hashCode() : 0) * 31)) * 31;
            GuildRole guildRole = this.guildRole;
            if (guildRole != null) {
                i = guildRole.hashCode();
            }
            int a2 = b.a(getTargetId());
            int a3 = (b.a(this.myPermissionsWithRoleDenied) + ((b.a(this.myPermissionsWithRoleNeutral) + ((a2 + ((a + i) * 31)) * 31)) * 31)) * 31;
            boolean z2 = this.meHasRole;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (a3 + i3) * 31;
            boolean z3 = this.isEveryoneRole;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            boolean useNewThreadsPermissions = getUseNewThreadsPermissions();
            if (useNewThreadsPermissions) {
                useNewThreadsPermissions = true;
            }
            int i9 = useNewThreadsPermissions ? 1 : 0;
            int i10 = useNewThreadsPermissions ? 1 : 0;
            int i11 = (i8 + i9) * 31;
            boolean hasEventFeature = getHasEventFeature();
            if (!hasEventFeature) {
                i2 = hasEventFeature;
            }
            return i11 + i2;
        }

        public final boolean isEveryoneRole() {
            return this.isEveryoneRole;
        }

        public String toString() {
            StringBuilder R = a.R("ModelForRole(channel=");
            R.append(getChannel());
            R.append(", myPermissionsForChannel=");
            R.append(this.myPermissionsForChannel);
            R.append(", guildRole=");
            R.append(this.guildRole);
            R.append(", targetId=");
            R.append(getTargetId());
            R.append(", myPermissionsWithRoleNeutral=");
            R.append(this.myPermissionsWithRoleNeutral);
            R.append(", myPermissionsWithRoleDenied=");
            R.append(this.myPermissionsWithRoleDenied);
            R.append(", meHasRole=");
            R.append(this.meHasRole);
            R.append(", isEveryoneRole=");
            R.append(this.isEveryoneRole);
            R.append(", useNewThreadsPermissions=");
            R.append(getUseNewThreadsPermissions());
            R.append(", hasEventFeature=");
            R.append(getHasEventFeature());
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetChannelSettingsEditPermissionsModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\n\u0010\u0015\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010\u0016\u001a\u00020\t\u0012\u0006\u0010\u0017\u001a\u00020\f\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\u0019\u001a\u00020\t\u0012\u0006\u0010\u001a\u001a\u00020\t¢\u0006\u0004\b2\u00103J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000bJ\\\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\f\b\u0002\u0010\u0015\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u0016\u001a\u00020\t2\b\b\u0002\u0010\u0017\u001a\u00020\f2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010\u0019\u001a\u00020\t2\b\b\u0002\u0010\u001a\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010%\u001a\u00020\t2\b\u0010$\u001a\u0004\u0018\u00010#HÖ\u0003¢\u0006\u0004\b%\u0010&R\u001c\u0010\u0019\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010'\u001a\u0004\b(\u0010\u000bR\u001c\u0010\u0014\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b*\u0010\u0004R\u001c\u0010\u001a\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u0010'\u001a\u0004\b+\u0010\u000bR\u001d\u0010\u0015\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010,\u001a\u0004\b-\u0010\bR\u0019\u0010\u0016\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010'\u001a\u0004\b\u0016\u0010\u000bR\u0019\u0010\u0017\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010.\u001a\u0004\b/\u0010\u000eR\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00100\u001a\u0004\b1\u0010\u0011¨\u00064"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel$ModelForUser;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/api/permission/PermissionBit;", "component2", "()J", "", "component3", "()Z", "Lcom/discord/models/user/User;", "component4", "()Lcom/discord/models/user/User;", "Lcom/discord/models/member/GuildMember;", "component5", "()Lcom/discord/models/member/GuildMember;", "component6", "component7", "channel", "myPermissionsForChannel", "isMe", "user", "guildMember", "useNewThreadsPermissions", "hasEventFeature", "copy", "(Lcom/discord/api/channel/Channel;JZLcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;ZZ)Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel$ModelForUser;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getUseNewThreadsPermissions", "Lcom/discord/api/channel/Channel;", "getChannel", "getHasEventFeature", "J", "getMyPermissionsForChannel", "Lcom/discord/models/user/User;", "getUser", "Lcom/discord/models/member/GuildMember;", "getGuildMember", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;JZLcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ModelForUser extends WidgetChannelSettingsEditPermissionsModel {
        private final Channel channel;
        private final GuildMember guildMember;
        private final boolean hasEventFeature;
        private final boolean isMe;
        private final long myPermissionsForChannel;
        private final boolean useNewThreadsPermissions;
        private final User user;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ModelForUser(Channel channel, long j, boolean z2, User user, GuildMember guildMember, boolean z3, boolean z4) {
            super(channel, user.getId(), 0, z3, z4, null);
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(user, "user");
            this.channel = channel;
            this.myPermissionsForChannel = j;
            this.isMe = z2;
            this.user = user;
            this.guildMember = guildMember;
            this.useNewThreadsPermissions = z3;
            this.hasEventFeature = z4;
        }

        public final Channel component1() {
            return getChannel();
        }

        public final long component2() {
            return this.myPermissionsForChannel;
        }

        public final boolean component3() {
            return this.isMe;
        }

        public final User component4() {
            return this.user;
        }

        public final GuildMember component5() {
            return this.guildMember;
        }

        public final boolean component6() {
            return getUseNewThreadsPermissions();
        }

        public final boolean component7() {
            return getHasEventFeature();
        }

        public final ModelForUser copy(Channel channel, long j, boolean z2, User user, GuildMember guildMember, boolean z3, boolean z4) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(user, "user");
            return new ModelForUser(channel, j, z2, user, guildMember, z3, z4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ModelForUser)) {
                return false;
            }
            ModelForUser modelForUser = (ModelForUser) obj;
            return m.areEqual(getChannel(), modelForUser.getChannel()) && this.myPermissionsForChannel == modelForUser.myPermissionsForChannel && this.isMe == modelForUser.isMe && m.areEqual(this.user, modelForUser.user) && m.areEqual(this.guildMember, modelForUser.guildMember) && getUseNewThreadsPermissions() == modelForUser.getUseNewThreadsPermissions() && getHasEventFeature() == modelForUser.getHasEventFeature();
        }

        @Override // com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel
        public Channel getChannel() {
            return this.channel;
        }

        public final GuildMember getGuildMember() {
            return this.guildMember;
        }

        @Override // com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel
        public boolean getHasEventFeature() {
            return this.hasEventFeature;
        }

        public final long getMyPermissionsForChannel() {
            return this.myPermissionsForChannel;
        }

        @Override // com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel
        public boolean getUseNewThreadsPermissions() {
            return this.useNewThreadsPermissions;
        }

        public final User getUser() {
            return this.user;
        }

        public int hashCode() {
            Channel channel = getChannel();
            int i = 0;
            int a = (b.a(this.myPermissionsForChannel) + ((channel != null ? channel.hashCode() : 0) * 31)) * 31;
            boolean z2 = this.isMe;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (a + i3) * 31;
            User user = this.user;
            int hashCode = (i5 + (user != null ? user.hashCode() : 0)) * 31;
            GuildMember guildMember = this.guildMember;
            if (guildMember != null) {
                i = guildMember.hashCode();
            }
            int i6 = (hashCode + i) * 31;
            boolean useNewThreadsPermissions = getUseNewThreadsPermissions();
            if (useNewThreadsPermissions) {
                useNewThreadsPermissions = true;
            }
            int i7 = useNewThreadsPermissions ? 1 : 0;
            int i8 = useNewThreadsPermissions ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean hasEventFeature = getHasEventFeature();
            if (!hasEventFeature) {
                i2 = hasEventFeature;
            }
            return i9 + i2;
        }

        public final boolean isMe() {
            return this.isMe;
        }

        public String toString() {
            StringBuilder R = a.R("ModelForUser(channel=");
            R.append(getChannel());
            R.append(", myPermissionsForChannel=");
            R.append(this.myPermissionsForChannel);
            R.append(", isMe=");
            R.append(this.isMe);
            R.append(", user=");
            R.append(this.user);
            R.append(", guildMember=");
            R.append(this.guildMember);
            R.append(", useNewThreadsPermissions=");
            R.append(getUseNewThreadsPermissions());
            R.append(", hasEventFeature=");
            R.append(getHasEventFeature());
            R.append(")");
            return R.toString();
        }
    }

    private WidgetChannelSettingsEditPermissionsModel(Channel channel, long j, int i, boolean z2, boolean z3) {
        this.channel = channel;
        this.targetId = j;
        this.type = i;
        this.useNewThreadsPermissions = z2;
        this.hasEventFeature = z3;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public boolean getHasEventFeature() {
        return this.hasEventFeature;
    }

    public long getTargetId() {
        return this.targetId;
    }

    public final int getType() {
        return this.type;
    }

    public boolean getUseNewThreadsPermissions() {
        return this.useNewThreadsPermissions;
    }

    public /* synthetic */ WidgetChannelSettingsEditPermissionsModel(Channel channel, long j, int i, boolean z2, boolean z3, DefaultConstructorMarker defaultConstructorMarker) {
        this(channel, j, i, z2, z3);
    }
}
