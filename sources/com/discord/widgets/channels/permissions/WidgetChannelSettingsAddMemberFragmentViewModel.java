package com.discord.widgets.channels.permissions;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGatewayConnection;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.widgets.channels.permissions.AddMemberAdapter;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel;
import d0.d0.f;
import d0.g0.w;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func4;
/* compiled from: WidgetChannelSettingsAddMemberFragmentViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 >2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003>?@BS\u0012\n\u00102\u001a\u00060\u0014j\u0002`1\u0012\u0006\u00107\u001a\u00020\u0017\u0012\b\b\u0002\u0010!\u001a\u00020 \u0012\b\b\u0002\u0010+\u001a\u00020*\u0012\b\b\u0002\u0010(\u001a\u00020'\u0012\b\b\u0002\u00105\u001a\u000204\u0012\u000e\b\u0002\u0010;\u001a\b\u0012\u0004\u0012\u00020\b0:¢\u0006\u0004\b<\u0010=J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\f\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J%\u0010\u0018\u001a\u00020\u00172\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u0015H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001a\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u001a\u0010\u0007J\u001d\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\"\u0010\u0004\u001a\u00020\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0004\u0010#\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\u0007R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R2\u0010/\u001a\u001e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u001c0-j\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u001c`.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u001a\u00102\u001a\u00060\u0014j\u0002`18\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R\u0016\u00107\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108R\u0018\u0010\t\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\t\u00109¨\u0006A"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$ViewState;", "", "query", "", "requestMembers", "(Ljava/lang/String;)V", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;)V", "generateViewState", "(Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;)Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$ViewState;", "", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "makeAdapterItems", "(Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;)Ljava/util/List;", "Lcom/discord/api/permission/PermissionOverwrite;", "overwrite", "", "Lcom/discord/api/permission/PermissionBit;", "permission", "", "isPermissionOverrideAlreadyPresent", "(Lcom/discord/api/permission/PermissionOverwrite;J)Z", "updateQuery", ModelAuditLogEntry.CHANGE_KEY_ID, "Lcom/discord/api/permission/PermissionOverwrite$Type;", "type", "toggleItem", "(JLcom/discord/api/permission/PermissionOverwrite$Type;)V", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Ljava/lang/String;", "getQuery", "()Ljava/lang/String;", "setQuery", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuilds;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "selected", "Ljava/util/HashMap;", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "Lcom/discord/stores/StoreGatewayConnection;", "gatewaySocket", "Lcom/discord/stores/StoreGatewayConnection;", "showRolesWithGuildPermission", "Z", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(JZLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGatewayConnection;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsAddMemberFragmentViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final long channelId;
    private final StoreChannels channelsStore;
    private final StoreGatewayConnection gatewaySocket;
    private final StoreGuilds guildsStore;
    private String query;
    private final HashMap<Long, PermissionOverwrite.Type> selected;
    private final boolean showRolesWithGuildPermission;
    private StoreState storeState;
    private final StoreUser userStore;

    /* compiled from: WidgetChannelSettingsAddMemberFragmentViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetChannelSettingsAddMemberFragmentViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetChannelSettingsAddMemberFragmentViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J9\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreUser;", "userStore", "Lrx/Observable;", "Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, StoreChannels storeChannels, final StoreGuilds storeGuilds, final StoreUser storeUser) {
            Observable<R> F = storeChannels.observeChannel(j).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
            m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
            Observable<StoreState> Y = F.q().Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel$Companion$observeStoreState$1
                public final Observable<? extends WidgetChannelSettingsAddMemberFragmentViewModel.StoreState> call(final Channel channel) {
                    return Observable.h(StoreGuilds.this.observeGuild(channel.f()), StoreGuilds.this.observeRoles(channel.f()), StoreGuilds.this.observeComputed(channel.f()), storeUser.observeAllUsers(), new Func4<Guild, Map<Long, ? extends GuildRole>, Map<Long, ? extends GuildMember>, Map<Long, ? extends User>, WidgetChannelSettingsAddMemberFragmentViewModel.StoreState>() { // from class: com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel$Companion$observeStoreState$1.1
                        @Override // rx.functions.Func4
                        public /* bridge */ /* synthetic */ WidgetChannelSettingsAddMemberFragmentViewModel.StoreState call(Guild guild, Map<Long, ? extends GuildRole> map, Map<Long, ? extends GuildMember> map2, Map<Long, ? extends User> map3) {
                            return call2(guild, (Map<Long, GuildRole>) map, (Map<Long, GuildMember>) map2, map3);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final WidgetChannelSettingsAddMemberFragmentViewModel.StoreState call2(Guild guild, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Map<Long, ? extends User> map3) {
                            Map map4;
                            Channel channel2 = Channel.this;
                            m.checkNotNullExpressionValue(channel2, "channel");
                            List<PermissionOverwrite> s2 = Channel.this.s();
                            if (s2 != null) {
                                map4 = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(s2, 10)), 16));
                                for (T t : s2) {
                                    map4.put(Long.valueOf(((PermissionOverwrite) t).e()), t);
                                }
                            } else {
                                map4 = h0.emptyMap();
                            }
                            m.checkNotNullExpressionValue(map, "roles");
                            m.checkNotNullExpressionValue(map2, "members");
                            m.checkNotNullExpressionValue(map3, "users");
                            return new WidgetChannelSettingsAddMemberFragmentViewModel.StoreState(guild, channel2, map4, map, map2, map3);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "channelsStore.observeCha…          }\n            }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelSettingsAddMemberFragmentViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001Bu\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b\u0012\u0016\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\b\u0012\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\b\u0012\u0016\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00130\b¢\u0006\u0004\b0\u00101J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001c\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\fJ \u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\fJ \u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00130\bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\fJ\u008a\u0001\u0010\u001b\u001a\u00020\u00002\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00052\u0014\b\u0002\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b2\u0018\b\u0002\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\b2\u0018\b\u0002\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\b2\u0018\b\u0002\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00130\bHÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010%\u001a\u00020$2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b%\u0010&R)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010'\u001a\u0004\b(\u0010\fR)\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010'\u001a\u0004\b)\u0010\fR)\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0010\u0012\u0004\u0012\u00020\u00130\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010'\u001a\u0004\b*\u0010\fR\u0019\u0010\u0016\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010+\u001a\u0004\b,\u0010\u0007R%\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010'\u001a\u0004\b-\u0010\fR\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010.\u001a\u0004\b/\u0010\u0004¨\u00062"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/api/permission/PermissionOverwrite;", "component3", "()Ljava/util/Map;", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component4", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "component5", "Lcom/discord/models/user/User;", "component6", "guild", "channel", "channelPermissionOverwritesMap", "roles", "members", "users", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getMembers", "getRoles", "getUsers", "Lcom/discord/api/channel/Channel;", "getChannel", "getChannelPermissionOverwritesMap", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Channel channel;
        private final Map<Long, PermissionOverwrite> channelPermissionOverwritesMap;
        private final Guild guild;
        private final Map<Long, GuildMember> members;
        private final Map<Long, GuildRole> roles;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(Guild guild, Channel channel, Map<Long, PermissionOverwrite> map, Map<Long, GuildRole> map2, Map<Long, GuildMember> map3, Map<Long, ? extends User> map4) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "channelPermissionOverwritesMap");
            m.checkNotNullParameter(map2, "roles");
            m.checkNotNullParameter(map3, "members");
            m.checkNotNullParameter(map4, "users");
            this.guild = guild;
            this.channel = channel;
            this.channelPermissionOverwritesMap = map;
            this.roles = map2;
            this.members = map3;
            this.users = map4;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Guild guild, Channel channel, Map map, Map map2, Map map3, Map map4, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = storeState.guild;
            }
            if ((i & 2) != 0) {
                channel = storeState.channel;
            }
            Channel channel2 = channel;
            Map<Long, PermissionOverwrite> map5 = map;
            if ((i & 4) != 0) {
                map5 = storeState.channelPermissionOverwritesMap;
            }
            Map map6 = map5;
            Map<Long, GuildRole> map7 = map2;
            if ((i & 8) != 0) {
                map7 = storeState.roles;
            }
            Map map8 = map7;
            Map<Long, GuildMember> map9 = map3;
            if ((i & 16) != 0) {
                map9 = storeState.members;
            }
            Map map10 = map9;
            Map<Long, User> map11 = map4;
            if ((i & 32) != 0) {
                map11 = storeState.users;
            }
            return storeState.copy(guild, channel2, map6, map8, map10, map11);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final Map<Long, PermissionOverwrite> component3() {
            return this.channelPermissionOverwritesMap;
        }

        public final Map<Long, GuildRole> component4() {
            return this.roles;
        }

        public final Map<Long, GuildMember> component5() {
            return this.members;
        }

        public final Map<Long, User> component6() {
            return this.users;
        }

        public final StoreState copy(Guild guild, Channel channel, Map<Long, PermissionOverwrite> map, Map<Long, GuildRole> map2, Map<Long, GuildMember> map3, Map<Long, ? extends User> map4) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "channelPermissionOverwritesMap");
            m.checkNotNullParameter(map2, "roles");
            m.checkNotNullParameter(map3, "members");
            m.checkNotNullParameter(map4, "users");
            return new StoreState(guild, channel, map, map2, map3, map4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guild, storeState.guild) && m.areEqual(this.channel, storeState.channel) && m.areEqual(this.channelPermissionOverwritesMap, storeState.channelPermissionOverwritesMap) && m.areEqual(this.roles, storeState.roles) && m.areEqual(this.members, storeState.members) && m.areEqual(this.users, storeState.users);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Map<Long, PermissionOverwrite> getChannelPermissionOverwritesMap() {
            return this.channelPermissionOverwritesMap;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Map<Long, GuildMember> getMembers() {
            return this.members;
        }

        public final Map<Long, GuildRole> getRoles() {
            return this.roles;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            Map<Long, PermissionOverwrite> map = this.channelPermissionOverwritesMap;
            int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map2 = this.roles;
            int hashCode4 = (hashCode3 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, GuildMember> map3 = this.members;
            int hashCode5 = (hashCode4 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, User> map4 = this.users;
            if (map4 != null) {
                i = map4.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guild=");
            R.append(this.guild);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", channelPermissionOverwritesMap=");
            R.append(this.channelPermissionOverwritesMap);
            R.append(", roles=");
            R.append(this.roles);
            R.append(", members=");
            R.append(this.members);
            R.append(", users=");
            return a.L(R, this.users, ")");
        }
    }

    /* compiled from: WidgetChannelSettingsAddMemberFragmentViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000f¢\u0006\u0004\b-\u0010.J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001c\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013JV\u0010\u0019\u001a\u00020\u00002\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\b2\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0014\b\u0002\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fHÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001b\u0010\nJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010!\u001a\u00020 2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b$\u0010\u0004R%\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010%\u001a\u0004\b&\u0010\u0013R\u0019\u0010\u0016\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010'\u001a\u0004\b(\u0010\nR\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010)\u001a\u0004\b*\u0010\u0007R\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010+\u001a\u0004\b,\u0010\u000e¨\u0006/"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$ViewState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "component3", "()Ljava/lang/String;", "", "Lcom/discord/widgets/channels/permissions/AddMemberAdapter$Item;", "component4", "()Ljava/util/List;", "", "", "Lcom/discord/api/permission/PermissionOverwrite$Type;", "component5", "()Ljava/util/Map;", "guild", "channel", "query", "items", "selected", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsAddMemberFragmentViewModel$ViewState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/util/Map;", "getSelected", "Ljava/lang/String;", "getQuery", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Channel channel;
        private final Guild guild;
        private final List<AddMemberAdapter.Item> items;
        private final String query;
        private final Map<Long, PermissionOverwrite.Type> selected;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(Guild guild, Channel channel, String str, List<? extends AddMemberAdapter.Item> list, Map<Long, ? extends PermissionOverwrite.Type> map) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(str, "query");
            m.checkNotNullParameter(list, "items");
            m.checkNotNullParameter(map, "selected");
            this.guild = guild;
            this.channel = channel;
            this.query = str;
            this.items = list;
            this.selected = map;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, Guild guild, Channel channel, String str, List list, Map map, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = viewState.guild;
            }
            if ((i & 2) != 0) {
                channel = viewState.channel;
            }
            Channel channel2 = channel;
            if ((i & 4) != 0) {
                str = viewState.query;
            }
            String str2 = str;
            List<AddMemberAdapter.Item> list2 = list;
            if ((i & 8) != 0) {
                list2 = viewState.items;
            }
            List list3 = list2;
            Map<Long, PermissionOverwrite.Type> map2 = map;
            if ((i & 16) != 0) {
                map2 = viewState.selected;
            }
            return viewState.copy(guild, channel2, str2, list3, map2);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final String component3() {
            return this.query;
        }

        public final List<AddMemberAdapter.Item> component4() {
            return this.items;
        }

        public final Map<Long, PermissionOverwrite.Type> component5() {
            return this.selected;
        }

        public final ViewState copy(Guild guild, Channel channel, String str, List<? extends AddMemberAdapter.Item> list, Map<Long, ? extends PermissionOverwrite.Type> map) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(str, "query");
            m.checkNotNullParameter(list, "items");
            m.checkNotNullParameter(map, "selected");
            return new ViewState(guild, channel, str, list, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.guild, viewState.guild) && m.areEqual(this.channel, viewState.channel) && m.areEqual(this.query, viewState.query) && m.areEqual(this.items, viewState.items) && m.areEqual(this.selected, viewState.selected);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final List<AddMemberAdapter.Item> getItems() {
            return this.items;
        }

        public final String getQuery() {
            return this.query;
        }

        public final Map<Long, PermissionOverwrite.Type> getSelected() {
            return this.selected;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            String str = this.query;
            int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
            List<AddMemberAdapter.Item> list = this.items;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            Map<Long, PermissionOverwrite.Type> map = this.selected;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(guild=");
            R.append(this.guild);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", query=");
            R.append(this.query);
            R.append(", items=");
            R.append(this.items);
            R.append(", selected=");
            return a.L(R, this.selected, ")");
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetChannelSettingsAddMemberFragmentViewModel(long r11, boolean r13, com.discord.stores.StoreChannels r14, com.discord.stores.StoreGuilds r15, com.discord.stores.StoreUser r16, com.discord.stores.StoreGatewayConnection r17, rx.Observable r18, int r19, kotlin.jvm.internal.DefaultConstructorMarker r20) {
        /*
            r10 = this;
            r0 = r19 & 4
            if (r0 == 0) goto Lc
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r0 = r0.getChannels()
            r5 = r0
            goto Ld
        Lc:
            r5 = r14
        Ld:
            r0 = r19 & 8
            if (r0 == 0) goto L19
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r0 = r0.getGuilds()
            r6 = r0
            goto L1a
        L19:
            r6 = r15
        L1a:
            r0 = r19 & 16
            if (r0 == 0) goto L26
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r0 = r0.getUsers()
            r7 = r0
            goto L28
        L26:
            r7 = r16
        L28:
            r0 = r19 & 32
            if (r0 == 0) goto L34
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGatewayConnection r0 = r0.getGatewaySocket()
            r8 = r0
            goto L36
        L34:
            r8 = r17
        L36:
            r0 = r19 & 64
            if (r0 == 0) goto L4a
            com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel$Companion r0 = com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel.Companion
            r14 = r0
            r15 = r11
            r17 = r5
            r18 = r6
            r19 = r7
            rx.Observable r0 = com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel.Companion.access$observeStoreState(r14, r15, r17, r18, r19)
            r9 = r0
            goto L4c
        L4a:
            r9 = r18
        L4c:
            r1 = r10
            r2 = r11
            r4 = r13
            r1.<init>(r2, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel.<init>(long, boolean, com.discord.stores.StoreChannels, com.discord.stores.StoreGuilds, com.discord.stores.StoreUser, com.discord.stores.StoreGatewayConnection, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final ViewState generateViewState(StoreState storeState) {
        return new ViewState(storeState.getGuild(), storeState.getChannel(), this.query, makeAdapterItems(storeState), this.selected);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        this.storeState = storeState;
        updateViewState(generateViewState(storeState));
    }

    private final boolean isPermissionOverrideAlreadyPresent(PermissionOverwrite permissionOverwrite, long j) {
        return permissionOverwrite != null && (permissionOverwrite.c() & j) == j;
    }

    /* JADX WARN: Removed duplicated region for block: B:80:0x0093 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:83:0x002a A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final java.util.List<com.discord.widgets.channels.permissions.AddMemberAdapter.Item> makeAdapterItems(com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel.StoreState r18) {
        /*
            Method dump skipped, instructions count: 537
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel.makeAdapterItems(com.discord.widgets.channels.permissions.WidgetChannelSettingsAddMemberFragmentViewModel$StoreState):java.util.List");
    }

    private final void requestMembers(String str) {
        Guild guild;
        StoreState storeState = this.storeState;
        if (storeState != null && (guild = storeState.getGuild()) != null) {
            long id2 = guild.getId();
            StoreGatewayConnection storeGatewayConnection = this.gatewaySocket;
            Objects.requireNonNull(str, "null cannot be cast to non-null type kotlin.CharSequence");
            storeGatewayConnection.requestGuildMembers(id2, w.trim(str).toString(), null, 20);
        }
    }

    public final String getQuery() {
        return this.query;
    }

    public final void setQuery(String str) {
        m.checkNotNullParameter(str, "<set-?>");
        this.query = str;
    }

    public final void toggleItem(long j, PermissionOverwrite.Type type) {
        m.checkNotNullParameter(type, "type");
        if (this.selected.containsKey(Long.valueOf(j))) {
            this.selected.remove(Long.valueOf(j));
        } else {
            this.selected.put(Long.valueOf(j), type);
        }
        StoreState storeState = this.storeState;
        if (storeState != null) {
            updateViewState(generateViewState(storeState));
        }
    }

    public final void updateQuery(String str) {
        m.checkNotNullParameter(str, "query");
        this.query = str;
        StoreState storeState = this.storeState;
        if (storeState != null) {
            updateViewState(generateViewState(storeState));
        }
        requestMembers(str);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsAddMemberFragmentViewModel(long j, boolean z2, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreUser storeUser, StoreGatewayConnection storeGatewayConnection, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeGatewayConnection, "gatewaySocket");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.channelId = j;
        this.showRolesWithGuildPermission = z2;
        this.channelsStore = storeChannels;
        this.guildsStore = storeGuilds;
        this.userStore = storeUser;
        this.gatewaySocket = storeGatewayConnection;
        this.query = "";
        this.selected = new HashMap<>();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetChannelSettingsAddMemberFragmentViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
