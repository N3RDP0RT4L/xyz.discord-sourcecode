package com.discord.widgets.channels.permissions;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.d.n;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.app.AppFragment;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.channels.SimpleRolesAdapter;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAddRole;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.q;
import j0.l.e.k;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func4;
import xyz.discord.R;
/* loaded from: classes2.dex */
public class WidgetChannelSettingsPermissionsAddRole extends AppFragment {
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private SimpleRolesAdapter rolesAdapter;
    private RecyclerView rolesRecycler;

    /* loaded from: classes2.dex */
    public static class Model {
        private final boolean canManage;
        private final Channel channel;
        private final List<SimpleRolesAdapter.RoleItem> roleItems;

        private Model(MeUser meUser, Guild guild, Channel channel, Long l, List<SimpleRolesAdapter.RoleItem> list) {
            this.channel = channel;
            this.roleItems = list;
            boolean mfaEnabled = meUser.getMfaEnabled();
            this.canManage = guild.getOwnerId() == meUser.getId() || PermissionUtils.canAndIsElevated(Permission.MANAGE_ROLES, l, mfaEnabled, guild.getMfaLevel()) || PermissionUtils.canAndIsElevated(8L, l, mfaEnabled, guild.getMfaLevel());
        }

        public static Observable<Model> get(final long j) {
            return StoreStream.getChannels().observeChannel(j).Y(new b() { // from class: b.a.z.a.a.q
                @Override // j0.k.b
                public final Object call(Object obj) {
                    long j2 = j;
                    final Channel channel = (Channel) obj;
                    if (channel == null) {
                        return new k(null);
                    }
                    return Observable.h(StoreStream.getGuilds().observeGuild(channel.f()), StoreStream.getUsers().observeMe(), StoreStream.getPermissions().observePermissionsForChannel(j2), StoreStream.getGuilds().observeSortedRoles(channel.f()).Y(new b() { // from class: b.a.z.a.a.s
                        @Override // j0.k.b
                        public final Object call(Object obj2) {
                            final Channel channel2 = Channel.this;
                            return Observable.h0(new q((List) obj2)).x(new b() { // from class: b.a.z.a.a.t
                                @Override // j0.k.b
                                public final Object call(Object obj3) {
                                    Channel channel3 = Channel.this;
                                    GuildRole guildRole = (GuildRole) obj3;
                                    if (channel3.s() == null) {
                                        return Boolean.TRUE;
                                    }
                                    for (int i = 0; i < channel3.s().size(); i++) {
                                        if (channel3.s().get(i).e() == guildRole.getId()) {
                                            return Boolean.FALSE;
                                        }
                                    }
                                    return Boolean.TRUE;
                                }
                            }).F(v.j).f0();
                        }
                    }), new Func4() { // from class: b.a.z.a.a.r
                        @Override // rx.functions.Func4
                        public final Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                            return WidgetChannelSettingsPermissionsAddRole.Model.lambda$null$2(Channel.this, (Guild) obj2, (MeUser) obj3, (Long) obj4, (List) obj5);
                        }
                    });
                }
            }).k(n.j);
        }

        private static boolean isValid(Guild guild, Channel channel) {
            return (channel == null || guild == null) ? false : true;
        }

        public static /* synthetic */ Model lambda$null$2(Channel channel, Guild guild, MeUser meUser, Long l, List list) {
            if (isValid(guild, channel)) {
                return new Model(meUser, guild, channel, l, list);
            }
            return null;
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Model;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            if (!model.canEqual(this) || this.canManage != model.canManage) {
                return false;
            }
            Channel channel = this.channel;
            Channel channel2 = model.channel;
            if (channel != null ? !channel.equals(channel2) : channel2 != null) {
                return false;
            }
            List<SimpleRolesAdapter.RoleItem> list = this.roleItems;
            List<SimpleRolesAdapter.RoleItem> list2 = model.roleItems;
            return list != null ? list.equals(list2) : list2 == null;
        }

        public int hashCode() {
            int i = this.canManage ? 79 : 97;
            Channel channel = this.channel;
            int i2 = 43;
            int hashCode = ((i + 59) * 59) + (channel == null ? 43 : channel.hashCode());
            List<SimpleRolesAdapter.RoleItem> list = this.roleItems;
            int i3 = hashCode * 59;
            if (list != null) {
                i2 = list.hashCode();
            }
            return i3 + i2;
        }

        public String toString() {
            StringBuilder R = a.R("WidgetChannelSettingsPermissionsAddRole.Model(channel=");
            R.append(this.channel);
            R.append(", roleItems=");
            R.append(this.roleItems);
            R.append(", canManage=");
            return a.M(R, this.canManage, ")");
        }
    }

    public WidgetChannelSettingsPermissionsAddRole() {
        super(R.layout.widget_channel_settings_permissions_add_role);
    }

    private void configureToolbar(Channel channel) {
        setActionBarTitle(R.string.add_a_role);
        setActionBarSubtitle(ChannelUtils.d(channel, requireContext(), true));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void configureUI(final Model model) {
        if (model != null && model.canManage && !model.roleItems.isEmpty()) {
            configureToolbar(model.channel);
            this.rolesAdapter.setData(model.roleItems, new Function1() { // from class: b.a.z.a.a.u
                @Override // kotlin.jvm.functions.Function1
                public final Object invoke(Object obj) {
                    WidgetChannelSettingsPermissionsAddRole widgetChannelSettingsPermissionsAddRole = WidgetChannelSettingsPermissionsAddRole.this;
                    WidgetChannelSettingsPermissionsAddRole.Model model2 = model;
                    WidgetChannelSettingsEditPermissions.createForRole(widgetChannelSettingsPermissionsAddRole.getContext(), model2.channel.f(), model2.channel.h(), ((GuildRole) obj).getId());
                    return Unit.a;
                }
            });
        } else if (e() != null) {
            e().onBackPressed();
        }
    }

    public static void create(Context context, long j) {
        Intent intent = new Intent();
        intent.putExtra(INTENT_EXTRA_CHANNEL_ID, j);
        j.d(context, WidgetChannelSettingsPermissionsAddRole.class, intent);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(@NonNull View view) {
        super.onViewBound(view);
        setActionBarDisplayHomeAsUpEnabled();
        this.rolesRecycler = (RecyclerView) view.findViewById(R.id.channel_settings_permissions_add_role_recycler);
        this.rolesAdapter = (SimpleRolesAdapter) MGRecyclerAdapter.configure(new SimpleRolesAdapter(this.rolesRecycler));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<Model> observable = Model.get(getMostRecentIntent().getLongExtra(INTENT_EXTRA_CHANNEL_ID, -1L));
        m.checkNotNullParameter(this, "appComponent");
        m.checkNotNullExpressionValue(observable, "it");
        ObservableExtensionsKt.ui(observable, this, null).k(o.e(new Action1() { // from class: b.a.z.a.a.p
            @Override // rx.functions.Action1
            public final void call(Object obj) {
                WidgetChannelSettingsPermissionsAddRole.this.configureUI((WidgetChannelSettingsPermissionsAddRole.Model) obj);
            }
        }, getClass()));
    }
}
