package com.discord.widgets.channels.permissions;

import com.discord.restapi.RestAPIParams;
import com.discord.views.TernaryCheckBox;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelSettingsEditPermissions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", "invoke", "()Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", "getRequestBody"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsEditPermissions$updatePermissionOverwrites$1 extends o implements Function0<RestAPIParams.ChannelPermissionOverwrites> {
    public final /* synthetic */ long $targetId;
    public final /* synthetic */ int $type;
    public final /* synthetic */ WidgetChannelSettingsEditPermissions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsEditPermissions$updatePermissionOverwrites$1(WidgetChannelSettingsEditPermissions widgetChannelSettingsEditPermissions, int i, long j) {
        super(0);
        this.this$0 = widgetChannelSettingsEditPermissions;
        this.$type = i;
        this.$targetId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final RestAPIParams.ChannelPermissionOverwrites invoke() {
        List permissionCheckboxes;
        permissionCheckboxes = this.this$0.getPermissionCheckboxes();
        Iterator it = permissionCheckboxes.iterator();
        long j = 0;
        long j2 = 0;
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            TernaryCheckBox ternaryCheckBox = (TernaryCheckBox) it.next();
            if (ternaryCheckBox.b()) {
                j |= WidgetChannelSettingsEditPermissions.Companion.getPermission(ternaryCheckBox.getId());
            } else {
                if (ternaryCheckBox.o != -1) {
                    z2 = false;
                }
                if (z2) {
                    j2 |= WidgetChannelSettingsEditPermissions.Companion.getPermission(ternaryCheckBox.getId());
                }
            }
        }
        if (this.$type == 1) {
            return RestAPIParams.ChannelPermissionOverwrites.Companion.createForRole(this.$targetId, Long.valueOf(j), Long.valueOf(j2));
        }
        return RestAPIParams.ChannelPermissionOverwrites.Companion.createForMember(this.$targetId, Long.valueOf(j), Long.valueOf(j2));
    }
}
