package com.discord.widgets.channels.permissions;

import com.discord.databinding.WidgetChannelSettingsEditPermissionsBinding;
import com.discord.views.TernaryCheckBox;
import d0.t.n;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelSettingsEditPermissions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/views/TernaryCheckBox;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsEditPermissions$permissionCheckboxes$2 extends o implements Function0<List<? extends TernaryCheckBox>> {
    public final /* synthetic */ WidgetChannelSettingsEditPermissions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsEditPermissions$permissionCheckboxes$2(WidgetChannelSettingsEditPermissions widgetChannelSettingsEditPermissions) {
        super(0);
        this.this$0 = widgetChannelSettingsEditPermissions;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends TernaryCheckBox> invoke() {
        WidgetChannelSettingsEditPermissionsBinding binding;
        WidgetChannelSettingsEditPermissionsBinding binding2;
        WidgetChannelSettingsEditPermissionsBinding binding3;
        WidgetChannelSettingsEditPermissionsBinding binding4;
        WidgetChannelSettingsEditPermissionsBinding binding5;
        WidgetChannelSettingsEditPermissionsBinding binding6;
        WidgetChannelSettingsEditPermissionsBinding binding7;
        WidgetChannelSettingsEditPermissionsBinding binding8;
        WidgetChannelSettingsEditPermissionsBinding binding9;
        WidgetChannelSettingsEditPermissionsBinding binding10;
        WidgetChannelSettingsEditPermissionsBinding binding11;
        WidgetChannelSettingsEditPermissionsBinding binding12;
        WidgetChannelSettingsEditPermissionsBinding binding13;
        WidgetChannelSettingsEditPermissionsBinding binding14;
        WidgetChannelSettingsEditPermissionsBinding binding15;
        WidgetChannelSettingsEditPermissionsBinding binding16;
        WidgetChannelSettingsEditPermissionsBinding binding17;
        WidgetChannelSettingsEditPermissionsBinding binding18;
        WidgetChannelSettingsEditPermissionsBinding binding19;
        WidgetChannelSettingsEditPermissionsBinding binding20;
        WidgetChannelSettingsEditPermissionsBinding binding21;
        WidgetChannelSettingsEditPermissionsBinding binding22;
        WidgetChannelSettingsEditPermissionsBinding binding23;
        WidgetChannelSettingsEditPermissionsBinding binding24;
        WidgetChannelSettingsEditPermissionsBinding binding25;
        WidgetChannelSettingsEditPermissionsBinding binding26;
        WidgetChannelSettingsEditPermissionsBinding binding27;
        WidgetChannelSettingsEditPermissionsBinding binding28;
        WidgetChannelSettingsEditPermissionsBinding binding29;
        WidgetChannelSettingsEditPermissionsBinding binding30;
        binding = this.this$0.getBinding();
        binding2 = this.this$0.getBinding();
        binding3 = this.this$0.getBinding();
        binding4 = this.this$0.getBinding();
        binding5 = this.this$0.getBinding();
        binding6 = this.this$0.getBinding();
        binding7 = this.this$0.getBinding();
        binding8 = this.this$0.getBinding();
        binding9 = this.this$0.getBinding();
        binding10 = this.this$0.getBinding();
        binding11 = this.this$0.getBinding();
        binding12 = this.this$0.getBinding();
        binding13 = this.this$0.getBinding();
        binding14 = this.this$0.getBinding();
        binding15 = this.this$0.getBinding();
        binding16 = this.this$0.getBinding();
        binding17 = this.this$0.getBinding();
        binding18 = this.this$0.getBinding();
        binding19 = this.this$0.getBinding();
        binding20 = this.this$0.getBinding();
        binding21 = this.this$0.getBinding();
        binding22 = this.this$0.getBinding();
        binding23 = this.this$0.getBinding();
        binding24 = this.this$0.getBinding();
        binding25 = this.this$0.getBinding();
        binding26 = this.this$0.getBinding();
        binding27 = this.this$0.getBinding();
        binding28 = this.this$0.getBinding();
        binding29 = this.this$0.getBinding();
        binding30 = this.this$0.getBinding();
        return n.listOf((Object[]) new TernaryCheckBox[]{binding.c, binding2.d, binding3.f, binding4.e, binding5.g, binding6.j, binding7.m, binding8.n, binding9.o, binding10.p, binding11.q, binding12.r, binding13.f2257s, binding14.l, binding15.k, binding16.t, binding17.w, binding18.u, binding19.v, binding20.i, binding21.f2258x, binding22.f2259y, binding23.f2260z, binding24.A, binding25.C, binding26.E, binding27.D, binding28.B, binding29.h, binding30.f2256b});
    }
}
