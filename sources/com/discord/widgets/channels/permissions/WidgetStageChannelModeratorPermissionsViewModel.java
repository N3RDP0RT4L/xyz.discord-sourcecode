package com.discord.widgets.channels.permissions;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.channels.permissions.PermissionOwnerListView;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u00172\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0017\u0018\u0019B\u0013\u0012\n\u0010\u0014\u001a\u00060\u0012j\u0002`\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;)V", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Valid;", "Lcom/discord/api/role/GuildRole;", "role", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "getRoleRemoveStatus", "(Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Valid;Lcom/discord/api/role/GuildRole;)Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "", "isOwner", "getMemberRemoveStatus", "(Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Valid;Z)Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "", "Lcom/discord/primitives/ChannelId;", "channelId", HookHelper.constructorName, "(J)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStageChannelModeratorPermissionsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.permissions.WidgetStageChannelModeratorPermissionsViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            WidgetStageChannelModeratorPermissionsViewModel widgetStageChannelModeratorPermissionsViewModel = WidgetStageChannelModeratorPermissionsViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            widgetStageChannelModeratorPermissionsViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012JI\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StorePermissions;", "permissionStore", "Lrx/Observable;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", "observeStores", "(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStores(long j, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreUser storeUser, StorePermissions storePermissions) {
            return ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{storeGuilds, storeChannels, storeUser, storePermissions}, false, null, null, new WidgetStageChannelModeratorPermissionsViewModel$Companion$observeStores$1(storeChannels, j, storeGuilds, storeUser, storePermissions), 14, null);
        }

        public static /* synthetic */ Observable observeStores$default(Companion companion, long j, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreUser storeUser, StorePermissions storePermissions, int i, Object obj) {
            if ((i & 2) != 0) {
                storeGuilds = StoreStream.Companion.getGuilds();
            }
            StoreGuilds storeGuilds2 = storeGuilds;
            if ((i & 4) != 0) {
                storeChannels = StoreStream.Companion.getChannels();
            }
            StoreChannels storeChannels2 = storeChannels;
            if ((i & 8) != 0) {
                storeUser = StoreStream.Companion.getUsers();
            }
            StoreUser storeUser2 = storeUser;
            if ((i & 16) != 0) {
                storePermissions = StoreStream.Companion.getPermissions();
            }
            return companion.observeStores(j, storeGuilds2, storeChannels2, storeUser2, storePermissions);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Valid;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Invalid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Invalid;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends StoreState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001Bw\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0016\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f0\t\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00100\t\u0012\u0016\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00120\t\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0010\u0012\u0006\u0010\u001f\u001a\u00020\u0016¢\u0006\u0004\b8\u00109J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ \u0010\r\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f0\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ \u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00100\tHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000eJ \u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00120\tHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000eJ\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u008e\u0001\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0018\b\u0002\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f0\t2\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00100\t2\u0018\b\u0002\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00120\t2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00102\b\b\u0002\u0010\u001f\u001a\u00020\u0016HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u001a\u0010*\u001a\u00020\u00162\b\u0010)\u001a\u0004\u0018\u00010(HÖ\u0003¢\u0006\u0004\b*\u0010+R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010,\u001a\u0004\b-\u0010\u0004R)\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f0\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010.\u001a\u0004\b/\u0010\u000eR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00100\u001a\u0004\b1\u0010\u0015R)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00100\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010.\u001a\u0004\b2\u0010\u000eR\u001f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00103\u001a\u0004\b4\u0010\bR)\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000f\u0012\u0004\u0012\u00020\u00120\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010.\u001a\u0004\b5\u0010\u000eR\u0019\u0010\u001f\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00106\u001a\u0004\b7\u0010\u0018¨\u0006:"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Valid;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "Lcom/discord/api/permission/PermissionOverwrite;", "component2", "()Ljava/util/List;", "", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component3", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component4", "Lcom/discord/models/member/GuildMember;", "component5", "component6", "()Lcom/discord/models/user/User;", "", "component7", "()Z", "guild", "channelPermissionOverwrites", "guildRoles", "usersWithOverwrites", "guildMembers", "guildOwnerUser", "canEditModerators", "copy", "(Lcom/discord/models/guild/Guild;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/user/User;Z)Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$StoreState$Valid;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/util/Map;", "getGuildRoles", "Lcom/discord/models/user/User;", "getGuildOwnerUser", "getUsersWithOverwrites", "Ljava/util/List;", "getChannelPermissionOverwrites", "getGuildMembers", "Z", "getCanEditModerators", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/user/User;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends StoreState {
            private final boolean canEditModerators;
            private final List<PermissionOverwrite> channelPermissionOverwrites;
            private final Guild guild;
            private final Map<Long, GuildMember> guildMembers;
            private final User guildOwnerUser;
            private final Map<Long, GuildRole> guildRoles;
            private final Map<Long, User> usersWithOverwrites;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Valid(Guild guild, List<PermissionOverwrite> list, Map<Long, GuildRole> map, Map<Long, ? extends User> map2, Map<Long, GuildMember> map3, User user, boolean z2) {
                super(null);
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(list, "channelPermissionOverwrites");
                m.checkNotNullParameter(map, "guildRoles");
                m.checkNotNullParameter(map2, "usersWithOverwrites");
                m.checkNotNullParameter(map3, "guildMembers");
                this.guild = guild;
                this.channelPermissionOverwrites = list;
                this.guildRoles = map;
                this.usersWithOverwrites = map2;
                this.guildMembers = map3;
                this.guildOwnerUser = user;
                this.canEditModerators = z2;
            }

            public static /* synthetic */ Valid copy$default(Valid valid, Guild guild, List list, Map map, Map map2, Map map3, User user, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    guild = valid.guild;
                }
                List<PermissionOverwrite> list2 = list;
                if ((i & 2) != 0) {
                    list2 = valid.channelPermissionOverwrites;
                }
                List list3 = list2;
                Map<Long, GuildRole> map4 = map;
                if ((i & 4) != 0) {
                    map4 = valid.guildRoles;
                }
                Map map5 = map4;
                Map<Long, User> map6 = map2;
                if ((i & 8) != 0) {
                    map6 = valid.usersWithOverwrites;
                }
                Map map7 = map6;
                Map<Long, GuildMember> map8 = map3;
                if ((i & 16) != 0) {
                    map8 = valid.guildMembers;
                }
                Map map9 = map8;
                if ((i & 32) != 0) {
                    user = valid.guildOwnerUser;
                }
                User user2 = user;
                if ((i & 64) != 0) {
                    z2 = valid.canEditModerators;
                }
                return valid.copy(guild, list3, map5, map7, map9, user2, z2);
            }

            public final Guild component1() {
                return this.guild;
            }

            public final List<PermissionOverwrite> component2() {
                return this.channelPermissionOverwrites;
            }

            public final Map<Long, GuildRole> component3() {
                return this.guildRoles;
            }

            public final Map<Long, User> component4() {
                return this.usersWithOverwrites;
            }

            public final Map<Long, GuildMember> component5() {
                return this.guildMembers;
            }

            public final User component6() {
                return this.guildOwnerUser;
            }

            public final boolean component7() {
                return this.canEditModerators;
            }

            public final Valid copy(Guild guild, List<PermissionOverwrite> list, Map<Long, GuildRole> map, Map<Long, ? extends User> map2, Map<Long, GuildMember> map3, User user, boolean z2) {
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(list, "channelPermissionOverwrites");
                m.checkNotNullParameter(map, "guildRoles");
                m.checkNotNullParameter(map2, "usersWithOverwrites");
                m.checkNotNullParameter(map3, "guildMembers");
                return new Valid(guild, list, map, map2, map3, user, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.guild, valid.guild) && m.areEqual(this.channelPermissionOverwrites, valid.channelPermissionOverwrites) && m.areEqual(this.guildRoles, valid.guildRoles) && m.areEqual(this.usersWithOverwrites, valid.usersWithOverwrites) && m.areEqual(this.guildMembers, valid.guildMembers) && m.areEqual(this.guildOwnerUser, valid.guildOwnerUser) && this.canEditModerators == valid.canEditModerators;
            }

            public final boolean getCanEditModerators() {
                return this.canEditModerators;
            }

            public final List<PermissionOverwrite> getChannelPermissionOverwrites() {
                return this.channelPermissionOverwrites;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final Map<Long, GuildMember> getGuildMembers() {
                return this.guildMembers;
            }

            public final User getGuildOwnerUser() {
                return this.guildOwnerUser;
            }

            public final Map<Long, GuildRole> getGuildRoles() {
                return this.guildRoles;
            }

            public final Map<Long, User> getUsersWithOverwrites() {
                return this.usersWithOverwrites;
            }

            public int hashCode() {
                Guild guild = this.guild;
                int i = 0;
                int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
                List<PermissionOverwrite> list = this.channelPermissionOverwrites;
                int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
                Map<Long, GuildRole> map = this.guildRoles;
                int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
                Map<Long, User> map2 = this.usersWithOverwrites;
                int hashCode4 = (hashCode3 + (map2 != null ? map2.hashCode() : 0)) * 31;
                Map<Long, GuildMember> map3 = this.guildMembers;
                int hashCode5 = (hashCode4 + (map3 != null ? map3.hashCode() : 0)) * 31;
                User user = this.guildOwnerUser;
                if (user != null) {
                    i = user.hashCode();
                }
                int i2 = (hashCode5 + i) * 31;
                boolean z2 = this.canEditModerators;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(guild=");
                R.append(this.guild);
                R.append(", channelPermissionOverwrites=");
                R.append(this.channelPermissionOverwrites);
                R.append(", guildRoles=");
                R.append(this.guildRoles);
                R.append(", usersWithOverwrites=");
                R.append(this.usersWithOverwrites);
                R.append(", guildMembers=");
                R.append(this.guildMembers);
                R.append(", guildOwnerUser=");
                R.append(this.guildOwnerUser);
                R.append(", canEditModerators=");
                return a.M(R, this.canEditModerators, ")");
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState$Valid;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState$Invalid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetStageChannelModeratorPermissionsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ:\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\f\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00072\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001b\u0010\u0005R\u0019\u0010\f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\t¨\u0006 "}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState$Valid;", "Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState;", "", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Item;", "component1", "()Ljava/util/List;", "component2", "", "component3", "()Z", "roleItems", "memberItems", "canEditModerators", "copy", "(Ljava/util/List;Ljava/util/List;Z)Lcom/discord/widgets/channels/permissions/WidgetStageChannelModeratorPermissionsViewModel$ViewState$Valid;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getMemberItems", "getRoleItems", "Z", "getCanEditModerators", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends ViewState {
            private final boolean canEditModerators;
            private final List<PermissionOwnerListView.Item> memberItems;
            private final List<PermissionOwnerListView.Item> roleItems;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Valid(List<PermissionOwnerListView.Item> list, List<PermissionOwnerListView.Item> list2, boolean z2) {
                super(null);
                m.checkNotNullParameter(list, "roleItems");
                m.checkNotNullParameter(list2, "memberItems");
                this.roleItems = list;
                this.memberItems = list2;
                this.canEditModerators = z2;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Valid copy$default(Valid valid, List list, List list2, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = valid.roleItems;
                }
                if ((i & 2) != 0) {
                    list2 = valid.memberItems;
                }
                if ((i & 4) != 0) {
                    z2 = valid.canEditModerators;
                }
                return valid.copy(list, list2, z2);
            }

            public final List<PermissionOwnerListView.Item> component1() {
                return this.roleItems;
            }

            public final List<PermissionOwnerListView.Item> component2() {
                return this.memberItems;
            }

            public final boolean component3() {
                return this.canEditModerators;
            }

            public final Valid copy(List<PermissionOwnerListView.Item> list, List<PermissionOwnerListView.Item> list2, boolean z2) {
                m.checkNotNullParameter(list, "roleItems");
                m.checkNotNullParameter(list2, "memberItems");
                return new Valid(list, list2, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.roleItems, valid.roleItems) && m.areEqual(this.memberItems, valid.memberItems) && this.canEditModerators == valid.canEditModerators;
            }

            public final boolean getCanEditModerators() {
                return this.canEditModerators;
            }

            public final List<PermissionOwnerListView.Item> getMemberItems() {
                return this.memberItems;
            }

            public final List<PermissionOwnerListView.Item> getRoleItems() {
                return this.roleItems;
            }

            public int hashCode() {
                List<PermissionOwnerListView.Item> list = this.roleItems;
                int i = 0;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                List<PermissionOwnerListView.Item> list2 = this.memberItems;
                if (list2 != null) {
                    i = list2.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.canEditModerators;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(roleItems=");
                R.append(this.roleItems);
                R.append(", memberItems=");
                R.append(this.memberItems);
                R.append(", canEditModerators=");
                return a.M(R, this.canEditModerators, ")");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetStageChannelModeratorPermissionsViewModel(long j) {
        super(null, 1, null);
        Observable q = Companion.observeStores$default(Companion, j, null, null, null, null, 30, null).X(j0.p.a.a()).q();
        m.checkNotNullExpressionValue(q, "observeStores(channelId)…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), WidgetStageChannelModeratorPermissionsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }

    private final PermissionOwnerListView.RemoveStatus getMemberRemoveStatus(StoreState.Valid valid, boolean z2) {
        if (!valid.getCanEditModerators()) {
            return new PermissionOwnerListView.RemoveStatus.CannotRemove(PermissionOwnerListView.RemoveStatus.CannotRemove.Reason.HAS_NO_PERMISSION);
        }
        if (z2) {
            return new PermissionOwnerListView.RemoveStatus.CannotRemove(PermissionOwnerListView.RemoveStatus.CannotRemove.Reason.IS_GUILD_OWNER);
        }
        return PermissionOwnerListView.RemoveStatus.CanRemove.INSTANCE;
    }

    private final PermissionOwnerListView.RemoveStatus getRoleRemoveStatus(StoreState.Valid valid, GuildRole guildRole) {
        if (!valid.getCanEditModerators()) {
            return new PermissionOwnerListView.RemoveStatus.CannotRemove(PermissionOwnerListView.RemoveStatus.CannotRemove.Reason.HAS_NO_PERMISSION);
        }
        if (PermissionUtils.INSTANCE.canRole(20971536L, guildRole, null)) {
            return new PermissionOwnerListView.RemoveStatus.CannotRemove(PermissionOwnerListView.RemoveStatus.CannotRemove.Reason.IS_NOT_OVERRIDE);
        }
        if ((guildRole.h() & 8) == 8) {
            return new PermissionOwnerListView.RemoveStatus.CannotRemove(PermissionOwnerListView.RemoveStatus.CannotRemove.Reason.IS_ADMINISTRATOR);
        }
        return PermissionOwnerListView.RemoveStatus.CanRemove.INSTANCE;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x0184, code lost:
        if (r15 != null) goto L52;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v0, types: [com.discord.widgets.channels.permissions.PermissionOwnerListView$Item] */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.channels.permissions.WidgetStageChannelModeratorPermissionsViewModel.StoreState r15) {
        /*
            Method dump skipped, instructions count: 412
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.permissions.WidgetStageChannelModeratorPermissionsViewModel.handleStoreState(com.discord.widgets.channels.permissions.WidgetStageChannelModeratorPermissionsViewModel$StoreState):void");
    }
}
