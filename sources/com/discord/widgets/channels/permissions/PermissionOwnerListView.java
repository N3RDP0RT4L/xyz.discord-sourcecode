package com.discord.widgets.channels.permissions;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.RemovablePermissionOwnerViewBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.channels.permissions.PermissionOwnerListView;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: PermissionOwnerListView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0004\u0018\u0019\u001a\u001bB\u0011\b\u0016\u0012\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0010\u0010\u0011B\u001d\b\u0016\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b\u0010\u0010\u0014B'\b\u0016\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0010\u0010\u0017J/\u0010\t\u001a\u00020\u00072\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView;", "Landroidx/recyclerview/widget/RecyclerView;", "", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Item;", "permissionOwners", "Lkotlin/Function1;", "Lcom/discord/widgets/channels/permissions/PermissionOwner;", "", "onRemoveClicked", "setData", "(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Adapter;", "adapter", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Adapter;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "Adapter", "Item", "PermissionOwnerViewHolder", "RemoveStatus", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PermissionOwnerListView extends RecyclerView {
    private final Adapter adapter;

    /* compiled from: PermissionOwnerListView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u001b\u0010\u0007\u001a\u00020\u00062\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u000f\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\tH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\tH\u0016¢\u0006\u0004\b\u0013\u0010\u0014R.\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00060\u00158\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001c\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010\u001d¨\u0006 "}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$PermissionOwnerViewHolder;", "", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Item;", "data", "", "setData", "(Ljava/util/List;)V", "", "getItemCount", "()I", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$PermissionOwnerViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onBindViewHolder", "(Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$PermissionOwnerViewHolder;I)V", "Lkotlin/Function1;", "Lcom/discord/widgets/channels/permissions/PermissionOwner;", "onRemoveClicked", "Lkotlin/jvm/functions/Function1;", "getOnRemoveClicked", "()Lkotlin/jvm/functions/Function1;", "setOnRemoveClicked", "(Lkotlin/jvm/functions/Function1;)V", "Ljava/util/List;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Adapter extends RecyclerView.Adapter<PermissionOwnerViewHolder> {
        private Function1<? super PermissionOwner, Unit> onRemoveClicked = PermissionOwnerListView$Adapter$onRemoveClicked$1.INSTANCE;
        private List<Item> data = n.emptyList();

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.data.size();
        }

        public final Function1<PermissionOwner, Unit> getOnRemoveClicked() {
            return this.onRemoveClicked;
        }

        public final void setData(List<Item> list) {
            m.checkNotNullParameter(list, "data");
            this.data = list;
            notifyDataSetChanged();
        }

        public final void setOnRemoveClicked(Function1<? super PermissionOwner, Unit> function1) {
            m.checkNotNullParameter(function1, "<set-?>");
            this.onRemoveClicked = function1;
        }

        public void onBindViewHolder(PermissionOwnerViewHolder permissionOwnerViewHolder, int i) {
            m.checkNotNullParameter(permissionOwnerViewHolder, "holder");
            permissionOwnerViewHolder.configure(this.data.get(i), new PermissionOwnerListView$Adapter$onBindViewHolder$1(this, this.data.get(i)));
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public PermissionOwnerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            RemovablePermissionOwnerViewBinding a = RemovablePermissionOwnerViewBinding.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
            m.checkNotNullExpressionValue(a, "RemovablePermissionOwner…,\n          false\n      )");
            return new PermissionOwnerViewHolder(a);
        }
    }

    /* compiled from: PermissionOwnerListView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Item;", "", "Lcom/discord/widgets/channels/permissions/PermissionOwner;", "component1", "()Lcom/discord/widgets/channels/permissions/PermissionOwner;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "component2", "()Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "permissionOwner", "removeStatus", "copy", "(Lcom/discord/widgets/channels/permissions/PermissionOwner;Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;)Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Item;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "getRemoveStatus", "Lcom/discord/widgets/channels/permissions/PermissionOwner;", "getPermissionOwner", HookHelper.constructorName, "(Lcom/discord/widgets/channels/permissions/PermissionOwner;Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Item {
        private final PermissionOwner permissionOwner;
        private final RemoveStatus removeStatus;

        public Item(PermissionOwner permissionOwner, RemoveStatus removeStatus) {
            m.checkNotNullParameter(permissionOwner, "permissionOwner");
            m.checkNotNullParameter(removeStatus, "removeStatus");
            this.permissionOwner = permissionOwner;
            this.removeStatus = removeStatus;
        }

        public static /* synthetic */ Item copy$default(Item item, PermissionOwner permissionOwner, RemoveStatus removeStatus, int i, Object obj) {
            if ((i & 1) != 0) {
                permissionOwner = item.permissionOwner;
            }
            if ((i & 2) != 0) {
                removeStatus = item.removeStatus;
            }
            return item.copy(permissionOwner, removeStatus);
        }

        public final PermissionOwner component1() {
            return this.permissionOwner;
        }

        public final RemoveStatus component2() {
            return this.removeStatus;
        }

        public final Item copy(PermissionOwner permissionOwner, RemoveStatus removeStatus) {
            m.checkNotNullParameter(permissionOwner, "permissionOwner");
            m.checkNotNullParameter(removeStatus, "removeStatus");
            return new Item(permissionOwner, removeStatus);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Item)) {
                return false;
            }
            Item item = (Item) obj;
            return m.areEqual(this.permissionOwner, item.permissionOwner) && m.areEqual(this.removeStatus, item.removeStatus);
        }

        public final PermissionOwner getPermissionOwner() {
            return this.permissionOwner;
        }

        public final RemoveStatus getRemoveStatus() {
            return this.removeStatus;
        }

        public int hashCode() {
            PermissionOwner permissionOwner = this.permissionOwner;
            int i = 0;
            int hashCode = (permissionOwner != null ? permissionOwner.hashCode() : 0) * 31;
            RemoveStatus removeStatus = this.removeStatus;
            if (removeStatus != null) {
                i = removeStatus.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("Item(permissionOwner=");
            R.append(this.permissionOwner);
            R.append(", removeStatus=");
            R.append(this.removeStatus);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: PermissionOwnerListView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ#\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$PermissionOwnerViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Item;", "item", "Lkotlin/Function0;", "", "onRemoveClicked", "configure", "(Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$Item;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/databinding/RemovablePermissionOwnerViewBinding;", "binding", "Lcom/discord/databinding/RemovablePermissionOwnerViewBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/RemovablePermissionOwnerViewBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PermissionOwnerViewHolder extends RecyclerView.ViewHolder {
        private final RemovablePermissionOwnerViewBinding binding;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                RemoveStatus.CannotRemove.Reason.values();
                int[] iArr = new int[4];
                $EnumSwitchMapping$0 = iArr;
                iArr[RemoveStatus.CannotRemove.Reason.IS_NOT_OVERRIDE.ordinal()] = 1;
                iArr[RemoveStatus.CannotRemove.Reason.HAS_NO_PERMISSION.ordinal()] = 2;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PermissionOwnerViewHolder(RemovablePermissionOwnerViewBinding removablePermissionOwnerViewBinding) {
            super(removablePermissionOwnerViewBinding.a);
            m.checkNotNullParameter(removablePermissionOwnerViewBinding, "binding");
            this.binding = removablePermissionOwnerViewBinding;
        }

        public final void configure(final Item item, final Function0<Unit> function0) {
            m.checkNotNullParameter(item, "item");
            m.checkNotNullParameter(function0, "onRemoveClicked");
            this.binding.f2121b.a(item.getPermissionOwner());
            ImageView imageView = this.binding.c;
            m.checkNotNullExpressionValue(imageView, "binding.remove");
            ViewExtensions.setEnabledAlpha$default(imageView, item.getRemoveStatus() instanceof RemoveStatus.CanRemove, 0.0f, 2, null);
            this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.permissions.PermissionOwnerListView$PermissionOwnerViewHolder$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    RemovablePermissionOwnerViewBinding removablePermissionOwnerViewBinding;
                    RemovablePermissionOwnerViewBinding removablePermissionOwnerViewBinding2;
                    PermissionOwnerListView.RemoveStatus removeStatus = item.getRemoveStatus();
                    if (m.areEqual(removeStatus, PermissionOwnerListView.RemoveStatus.CanRemove.INSTANCE)) {
                        function0.invoke();
                    } else if (!m.areEqual(removeStatus, PermissionOwnerListView.RemoveStatus.IsRemoving.INSTANCE) && (removeStatus instanceof PermissionOwnerListView.RemoveStatus.CannotRemove)) {
                        int ordinal = ((PermissionOwnerListView.RemoveStatus.CannotRemove) item.getRemoveStatus()).getReason().ordinal();
                        if (ordinal == 2) {
                            removablePermissionOwnerViewBinding = PermissionOwnerListView.PermissionOwnerViewHolder.this.binding;
                            ConstraintLayout constraintLayout = removablePermissionOwnerViewBinding.a;
                            m.checkNotNullExpressionValue(constraintLayout, "binding.root");
                            Toast.makeText(constraintLayout.getContext(), (int) R.string.channel_permissions_remove_not_overwrite, 0).show();
                        } else if (ordinal == 3) {
                            removablePermissionOwnerViewBinding2 = PermissionOwnerListView.PermissionOwnerViewHolder.this.binding;
                            ConstraintLayout constraintLayout2 = removablePermissionOwnerViewBinding2.a;
                            m.checkNotNullExpressionValue(constraintLayout2, "binding.root");
                            Toast.makeText(constraintLayout2.getContext(), (int) R.string.channel_permissions_cannot_edit_moderators, 0).show();
                        }
                    }
                }
            });
        }
    }

    /* compiled from: PermissionOwnerListView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "", HookHelper.constructorName, "()V", "CanRemove", "CannotRemove", "IsRemoving", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CanRemove;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$IsRemoving;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class RemoveStatus {

        /* compiled from: PermissionOwnerListView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CanRemove;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CanRemove extends RemoveStatus {
            public static final CanRemove INSTANCE = new CanRemove();

            private CanRemove() {
                super(null);
            }
        }

        /* compiled from: PermissionOwnerListView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0017B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove$Reason;", "component1", "()Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove$Reason;", ModelAuditLogEntry.CHANGE_KEY_REASON, "copy", "(Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove$Reason;)Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove$Reason;", "getReason", HookHelper.constructorName, "(Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove$Reason;)V", "Reason", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CannotRemove extends RemoveStatus {
            private final Reason reason;

            /* compiled from: PermissionOwnerListView.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$CannotRemove$Reason;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "IS_GUILD_OWNER", "IS_ADMINISTRATOR", "IS_NOT_OVERRIDE", "HAS_NO_PERMISSION", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public enum Reason {
                IS_GUILD_OWNER,
                IS_ADMINISTRATOR,
                IS_NOT_OVERRIDE,
                HAS_NO_PERMISSION
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public CannotRemove(Reason reason) {
                super(null);
                m.checkNotNullParameter(reason, ModelAuditLogEntry.CHANGE_KEY_REASON);
                this.reason = reason;
            }

            public static /* synthetic */ CannotRemove copy$default(CannotRemove cannotRemove, Reason reason, int i, Object obj) {
                if ((i & 1) != 0) {
                    reason = cannotRemove.reason;
                }
                return cannotRemove.copy(reason);
            }

            public final Reason component1() {
                return this.reason;
            }

            public final CannotRemove copy(Reason reason) {
                m.checkNotNullParameter(reason, ModelAuditLogEntry.CHANGE_KEY_REASON);
                return new CannotRemove(reason);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof CannotRemove) && m.areEqual(this.reason, ((CannotRemove) obj).reason);
                }
                return true;
            }

            public final Reason getReason() {
                return this.reason;
            }

            public int hashCode() {
                Reason reason = this.reason;
                if (reason != null) {
                    return reason.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("CannotRemove(reason=");
                R.append(this.reason);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: PermissionOwnerListView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus$IsRemoving;", "Lcom/discord/widgets/channels/permissions/PermissionOwnerListView$RemoveStatus;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class IsRemoving extends RemoveStatus {
            public static final IsRemoving INSTANCE = new IsRemoving();

            private IsRemoving() {
                super(null);
            }
        }

        private RemoveStatus() {
        }

        public /* synthetic */ RemoveStatus(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PermissionOwnerListView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        Adapter adapter = new Adapter();
        this.adapter = adapter;
        setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        setAdapter(adapter);
    }

    public final void setData(List<Item> list, Function1<? super PermissionOwner, Unit> function1) {
        m.checkNotNullParameter(list, "permissionOwners");
        m.checkNotNullParameter(function1, "onRemoveClicked");
        this.adapter.setOnRemoveClicked(function1);
        this.adapter.setData(list);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PermissionOwnerListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        Adapter adapter = new Adapter();
        this.adapter = adapter;
        setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        setAdapter(adapter);
    }

    public /* synthetic */ PermissionOwnerListView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PermissionOwnerListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        Adapter adapter = new Adapter();
        this.adapter = adapter;
        setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        setAdapter(adapter);
    }

    public /* synthetic */ PermissionOwnerListView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
