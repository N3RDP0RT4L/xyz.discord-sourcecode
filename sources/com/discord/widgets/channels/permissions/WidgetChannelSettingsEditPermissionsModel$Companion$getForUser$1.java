package com.discord.widgets.channels.permissions;

import com.discord.api.channel.Channel;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissionsModel;
import com.discord.widgets.chat.list.NewThreadsPermissionsFeatureFlag;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelSettingsEditPermissionsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;", "invoke", "()Lcom/discord/widgets/channels/permissions/WidgetChannelSettingsEditPermissionsModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelSettingsEditPermissionsModel$Companion$getForUser$1 extends o implements Function0<WidgetChannelSettingsEditPermissionsModel> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreChannels $storeChannels;
    public final /* synthetic */ StoreGuilds $storeGuilds;
    public final /* synthetic */ StorePermissions $storePermissions;
    public final /* synthetic */ StoreUser $storeUser;
    public final /* synthetic */ long $targetUserId;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelSettingsEditPermissionsModel$Companion$getForUser$1(StoreChannels storeChannels, long j, StoreUser storeUser, long j2, StoreGuilds storeGuilds, long j3, StorePermissions storePermissions) {
        super(0);
        this.$storeChannels = storeChannels;
        this.$channelId = j;
        this.$storeUser = storeUser;
        this.$targetUserId = j2;
        this.$storeGuilds = storeGuilds;
        this.$guildId = j3;
        this.$storePermissions = storePermissions;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetChannelSettingsEditPermissionsModel invoke() {
        boolean isAbleToManagePerms;
        Channel channel = this.$storeChannels.getChannel(this.$channelId);
        MeUser me2 = this.$storeUser.getMe();
        User user = this.$storeUser.getUsers().get(Long.valueOf(this.$targetUserId));
        Guild guild = this.$storeGuilds.getGuild(this.$guildId);
        Long l = this.$storePermissions.getPermissionsByChannel().get(Long.valueOf(this.$channelId));
        boolean isEnabled = NewThreadsPermissionsFeatureFlag.Companion.getINSTANCE().isEnabled(this.$guildId);
        boolean canGuildAccessStageEvents = StageEventsGuildsFeatureFlag.Companion.getINSTANCE().canGuildAccessStageEvents(this.$guildId);
        if (channel == null || guild == null || user == null || l == null) {
            return null;
        }
        isAbleToManagePerms = WidgetChannelSettingsEditPermissionsModel.Companion.isAbleToManagePerms(guild, me2, l.longValue());
        if (!isAbleToManagePerms) {
            return null;
        }
        return new WidgetChannelSettingsEditPermissionsModel.ModelForUser(channel, l.longValue(), user.getId() == me2.getId(), user, this.$storeGuilds.getMember(this.$guildId, user.getId()), isEnabled, canGuildAccessStageEvents);
    }
}
