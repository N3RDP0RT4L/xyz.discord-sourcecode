package com.discord.widgets.channels.permissions;

import android.content.Context;
import android.view.MenuItem;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.app.AppFragment;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetCreateChannelAddMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052&\u0010\u0004\u001a\"\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002 \u0003*\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/api/permission/PermissionOverwrite$Type;", "kotlin.jvm.PlatformType", "selected", "", "invoke", "(Ljava/util/Map;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCreateChannelAddMember$onViewBound$2 extends o implements Function1<Map<Long, ? extends PermissionOverwrite.Type>, Unit> {
    public final /* synthetic */ WidgetCreateChannelAddMember this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCreateChannelAddMember$onViewBound$2(WidgetCreateChannelAddMember widgetCreateChannelAddMember) {
        super(1);
        this.this$0 = widgetCreateChannelAddMember;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Map<Long, ? extends PermissionOverwrite.Type> map) {
        invoke2(map);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(final Map<Long, ? extends PermissionOverwrite.Type> map) {
        AppFragment.setActionBarOptionsMenu$default(this.this$0, R.menu.menu_add_member_continue, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.channels.permissions.WidgetCreateChannelAddMember$onViewBound$2.1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_continue) {
                    WidgetCreateChannelAddMember widgetCreateChannelAddMember = WidgetCreateChannelAddMember$onViewBound$2.this.this$0;
                    Map map2 = map;
                    m.checkNotNullExpressionValue(map2, "selected");
                    widgetCreateChannelAddMember.addPermissionOverwrites(map2);
                }
            }
        }, null, 4, null);
    }
}
