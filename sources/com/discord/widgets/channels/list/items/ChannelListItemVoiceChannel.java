package com.discord.widgets.channels.list.items;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.widgets.channels.list.WidgetChannelListUnreads;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelListItemVoiceChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0081\u0001\u0012\u0006\u0010\u001f\u001a\u00020\u0004\u0012\u0006\u0010 \u001a\u00020\u0007\u0012\u0006\u0010!\u001a\u00020\u0007\u0012\u000e\u0010\"\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f\u0012\u0006\u0010#\u001a\u00020\u000f\u0012\u0006\u0010$\u001a\u00020\u0007\u0012\u0006\u0010%\u001a\u00020\u000f\u0012\u0006\u0010&\u001a\u00020\u0007\u0012\u0006\u0010'\u001a\u00020\u0007\u0012\u0006\u0010(\u001a\u00020\u0007\u0012\u0006\u0010)\u001a\u00020\u0017\u0012\u0006\u0010*\u001a\u00020\u0007\u0012\u0006\u0010+\u001a\u00020\u0007\u0012\b\u0010,\u001a\u0004\u0018\u00010\u001c¢\u0006\u0004\bJ\u0010KJ\u0010\u0010\u0005\u001a\u00020\u0004HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\n\u0010\tJ\u0018\u0010\r\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u0012\u0010\tJ\u0010\u0010\u0013\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0011J\u0010\u0010\u0014\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u0014\u0010\tJ\u0010\u0010\u0015\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u0015\u0010\tJ\u0010\u0010\u0016\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u0016\u0010\tJ\u0010\u0010\u0018\u001a\u00020\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u001a\u0010\tJ\u0010\u0010\u001b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u001b\u0010\tJ\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ¦\u0001\u0010-\u001a\u00020\u00002\b\b\u0002\u0010\u001f\u001a\u00020\u00042\b\b\u0002\u0010 \u001a\u00020\u00072\b\b\u0002\u0010!\u001a\u00020\u00072\u0010\b\u0002\u0010\"\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f2\b\b\u0002\u0010#\u001a\u00020\u000f2\b\b\u0002\u0010$\u001a\u00020\u00072\b\b\u0002\u0010%\u001a\u00020\u000f2\b\b\u0002\u0010&\u001a\u00020\u00072\b\b\u0002\u0010'\u001a\u00020\u00072\b\b\u0002\u0010(\u001a\u00020\u00072\b\b\u0002\u0010)\u001a\u00020\u00172\b\b\u0002\u0010*\u001a\u00020\u00072\b\b\u0002\u0010+\u001a\u00020\u00072\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u001cHÆ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u00100\u001a\u00020/HÖ\u0001¢\u0006\u0004\b0\u00101J\u0010\u00102\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b2\u0010\u0011J\u001a\u00105\u001a\u00020\u00072\b\u00104\u001a\u0004\u0018\u000103HÖ\u0003¢\u0006\u0004\b5\u00106R\u001c\u0010$\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u00107\u001a\u0004\b$\u0010\tR!\u0010\"\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00108\u001a\u0004\b9\u0010\u000eR\u001c\u0010%\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u0010:\u001a\u0004\b;\u0010\u0011R\u0019\u0010&\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b&\u00107\u001a\u0004\b&\u0010\tR\u001c\u0010#\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b#\u0010:\u001a\u0004\b<\u0010\u0011R\u001c\u0010=\u001a\u00020/8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u00101R\u001c\u0010\u001f\u001a\u00020\u00048\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001f\u0010@\u001a\u0004\bA\u0010\u0006R\u0019\u0010 \u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b \u00107\u001a\u0004\bB\u0010\tR\u0019\u0010!\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b!\u00107\u001a\u0004\bC\u0010\tR\u0019\u0010(\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b(\u00107\u001a\u0004\b(\u0010\tR\u001c\u0010D\u001a\u00020\u000f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bD\u0010:\u001a\u0004\bE\u0010\u0011R\u0019\u0010*\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b*\u00107\u001a\u0004\b*\u0010\tR\u0019\u0010)\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010F\u001a\u0004\bG\u0010\u0019R\u0019\u0010'\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b'\u00107\u001a\u0004\b'\u0010\tR\u0019\u0010+\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b+\u00107\u001a\u0004\b+\u0010\tR\u001b\u0010,\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010H\u001a\u0004\bI\u0010\u001e¨\u0006L"}, d2 = {"Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceChannel;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "Lcom/discord/widgets/channels/list/items/ChannelListVocalItem;", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "component3", "", "Lcom/discord/api/permission/PermissionBit;", "component4", "()Ljava/lang/Long;", "", "component5", "()I", "component6", "component7", "component8", "component9", "component10", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "component11", "()Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "component12", "component13", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component14", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "channel", "textSelected", "voiceSelected", "permission", "mentionCount", "isUnread", "numUsersConnected", "isLocked", "isNsfw", "isAnyoneUsingVideo", "guildMaxVideoChannelUsers", "isGuildRoleSubscriptionLockedChannel", "isGuildRoleSubscriptionChannel", "guildScheduledEvent", "copy", "(Lcom/discord/api/channel/Channel;ZZLjava/lang/Long;IZIZZZLcom/discord/api/guild/GuildMaxVideoChannelUsers;ZZLcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceChannel;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/Long;", "getPermission", "I", "getNumUsersConnected", "getMentionCount", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/api/channel/Channel;", "getChannel", "getTextSelected", "getVoiceSelected", "type", "getType", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "getGuildMaxVideoChannelUsers", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZZLjava/lang/Long;IZIZZZLcom/discord/api/guild/GuildMaxVideoChannelUsers;ZZLcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelListItemVoiceChannel implements ChannelListItem, ChannelListVocalItem, WidgetChannelListUnreads.UnreadItem {
    private final Channel channel;
    private final GuildMaxVideoChannelUsers guildMaxVideoChannelUsers;
    private final GuildScheduledEvent guildScheduledEvent;
    private final boolean isAnyoneUsingVideo;
    private final boolean isGuildRoleSubscriptionChannel;
    private final boolean isGuildRoleSubscriptionLockedChannel;
    private final boolean isLocked;
    private final boolean isNsfw;
    private final boolean isUnread;
    private final String key;
    private final int mentionCount;
    private final int numUsersConnected;
    private final Long permission;
    private final boolean textSelected;
    private final int type = 1;
    private final boolean voiceSelected;

    public ChannelListItemVoiceChannel(Channel channel, boolean z2, boolean z3, Long l, int i, boolean z4, int i2, boolean z5, boolean z6, boolean z7, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, boolean z8, boolean z9, GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(guildMaxVideoChannelUsers, "guildMaxVideoChannelUsers");
        this.channel = channel;
        this.textSelected = z2;
        this.voiceSelected = z3;
        this.permission = l;
        this.mentionCount = i;
        this.isUnread = z4;
        this.numUsersConnected = i2;
        this.isLocked = z5;
        this.isNsfw = z6;
        this.isAnyoneUsingVideo = z7;
        this.guildMaxVideoChannelUsers = guildMaxVideoChannelUsers;
        this.isGuildRoleSubscriptionLockedChannel = z8;
        this.isGuildRoleSubscriptionChannel = z9;
        this.guildScheduledEvent = guildScheduledEvent;
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(getChannel().h());
        this.key = sb.toString();
    }

    public final Channel component1() {
        return getChannel();
    }

    public final boolean component10() {
        return this.isAnyoneUsingVideo;
    }

    public final GuildMaxVideoChannelUsers component11() {
        return this.guildMaxVideoChannelUsers;
    }

    public final boolean component12() {
        return this.isGuildRoleSubscriptionLockedChannel;
    }

    public final boolean component13() {
        return this.isGuildRoleSubscriptionChannel;
    }

    public final GuildScheduledEvent component14() {
        return this.guildScheduledEvent;
    }

    public final boolean component2() {
        return this.textSelected;
    }

    public final boolean component3() {
        return this.voiceSelected;
    }

    public final Long component4() {
        return this.permission;
    }

    public final int component5() {
        return getMentionCount();
    }

    public final boolean component6() {
        return isUnread();
    }

    public final int component7() {
        return getNumUsersConnected();
    }

    public final boolean component8() {
        return this.isLocked;
    }

    public final boolean component9() {
        return this.isNsfw;
    }

    public final ChannelListItemVoiceChannel copy(Channel channel, boolean z2, boolean z3, Long l, int i, boolean z4, int i2, boolean z5, boolean z6, boolean z7, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, boolean z8, boolean z9, GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(guildMaxVideoChannelUsers, "guildMaxVideoChannelUsers");
        return new ChannelListItemVoiceChannel(channel, z2, z3, l, i, z4, i2, z5, z6, z7, guildMaxVideoChannelUsers, z8, z9, guildScheduledEvent);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChannelListItemVoiceChannel)) {
            return false;
        }
        ChannelListItemVoiceChannel channelListItemVoiceChannel = (ChannelListItemVoiceChannel) obj;
        return m.areEqual(getChannel(), channelListItemVoiceChannel.getChannel()) && this.textSelected == channelListItemVoiceChannel.textSelected && this.voiceSelected == channelListItemVoiceChannel.voiceSelected && m.areEqual(this.permission, channelListItemVoiceChannel.permission) && getMentionCount() == channelListItemVoiceChannel.getMentionCount() && isUnread() == channelListItemVoiceChannel.isUnread() && getNumUsersConnected() == channelListItemVoiceChannel.getNumUsersConnected() && this.isLocked == channelListItemVoiceChannel.isLocked && this.isNsfw == channelListItemVoiceChannel.isNsfw && this.isAnyoneUsingVideo == channelListItemVoiceChannel.isAnyoneUsingVideo && m.areEqual(this.guildMaxVideoChannelUsers, channelListItemVoiceChannel.guildMaxVideoChannelUsers) && this.isGuildRoleSubscriptionLockedChannel == channelListItemVoiceChannel.isGuildRoleSubscriptionLockedChannel && this.isGuildRoleSubscriptionChannel == channelListItemVoiceChannel.isGuildRoleSubscriptionChannel && m.areEqual(this.guildScheduledEvent, channelListItemVoiceChannel.guildScheduledEvent);
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListVocalItem
    public Channel getChannel() {
        return this.channel;
    }

    public final GuildMaxVideoChannelUsers getGuildMaxVideoChannelUsers() {
        return this.guildMaxVideoChannelUsers;
    }

    public final GuildScheduledEvent getGuildScheduledEvent() {
        return this.guildScheduledEvent;
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListItem, com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    @Override // com.discord.widgets.channels.list.WidgetChannelListUnreads.UnreadItem
    public int getMentionCount() {
        return this.mentionCount;
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListVocalItem
    public int getNumUsersConnected() {
        return this.numUsersConnected;
    }

    public final Long getPermission() {
        return this.permission;
    }

    public final boolean getTextSelected() {
        return this.textSelected;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public final boolean getVoiceSelected() {
        return this.voiceSelected;
    }

    public int hashCode() {
        Channel channel = getChannel();
        int i = 0;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        boolean z2 = this.textSelected;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        boolean z3 = this.voiceSelected;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        Long l = this.permission;
        int mentionCount = (getMentionCount() + ((i8 + (l != null ? l.hashCode() : 0)) * 31)) * 31;
        boolean isUnread = isUnread();
        if (isUnread) {
            isUnread = true;
        }
        int i9 = isUnread ? 1 : 0;
        int i10 = isUnread ? 1 : 0;
        int numUsersConnected = (getNumUsersConnected() + ((mentionCount + i9) * 31)) * 31;
        boolean z4 = this.isLocked;
        if (z4) {
            z4 = true;
        }
        int i11 = z4 ? 1 : 0;
        int i12 = z4 ? 1 : 0;
        int i13 = (numUsersConnected + i11) * 31;
        boolean z5 = this.isNsfw;
        if (z5) {
            z5 = true;
        }
        int i14 = z5 ? 1 : 0;
        int i15 = z5 ? 1 : 0;
        int i16 = (i13 + i14) * 31;
        boolean z6 = this.isAnyoneUsingVideo;
        if (z6) {
            z6 = true;
        }
        int i17 = z6 ? 1 : 0;
        int i18 = z6 ? 1 : 0;
        int i19 = (i16 + i17) * 31;
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers = this.guildMaxVideoChannelUsers;
        int hashCode2 = (i19 + (guildMaxVideoChannelUsers != null ? guildMaxVideoChannelUsers.hashCode() : 0)) * 31;
        boolean z7 = this.isGuildRoleSubscriptionLockedChannel;
        if (z7) {
            z7 = true;
        }
        int i20 = z7 ? 1 : 0;
        int i21 = z7 ? 1 : 0;
        int i22 = (hashCode2 + i20) * 31;
        boolean z8 = this.isGuildRoleSubscriptionChannel;
        if (!z8) {
            i2 = z8 ? 1 : 0;
        }
        int i23 = (i22 + i2) * 31;
        GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
        if (guildScheduledEvent != null) {
            i = guildScheduledEvent.hashCode();
        }
        return i23 + i;
    }

    public final boolean isAnyoneUsingVideo() {
        return this.isAnyoneUsingVideo;
    }

    public final boolean isGuildRoleSubscriptionChannel() {
        return this.isGuildRoleSubscriptionChannel;
    }

    public final boolean isGuildRoleSubscriptionLockedChannel() {
        return this.isGuildRoleSubscriptionLockedChannel;
    }

    public final boolean isLocked() {
        return this.isLocked;
    }

    public final boolean isNsfw() {
        return this.isNsfw;
    }

    @Override // com.discord.widgets.channels.list.WidgetChannelListUnreads.UnreadItem
    public boolean isUnread() {
        return this.isUnread;
    }

    public String toString() {
        StringBuilder R = a.R("ChannelListItemVoiceChannel(channel=");
        R.append(getChannel());
        R.append(", textSelected=");
        R.append(this.textSelected);
        R.append(", voiceSelected=");
        R.append(this.voiceSelected);
        R.append(", permission=");
        R.append(this.permission);
        R.append(", mentionCount=");
        R.append(getMentionCount());
        R.append(", isUnread=");
        R.append(isUnread());
        R.append(", numUsersConnected=");
        R.append(getNumUsersConnected());
        R.append(", isLocked=");
        R.append(this.isLocked);
        R.append(", isNsfw=");
        R.append(this.isNsfw);
        R.append(", isAnyoneUsingVideo=");
        R.append(this.isAnyoneUsingVideo);
        R.append(", guildMaxVideoChannelUsers=");
        R.append(this.guildMaxVideoChannelUsers);
        R.append(", isGuildRoleSubscriptionLockedChannel=");
        R.append(this.isGuildRoleSubscriptionLockedChannel);
        R.append(", isGuildRoleSubscriptionChannel=");
        R.append(this.isGuildRoleSubscriptionChannel);
        R.append(", guildScheduledEvent=");
        R.append(this.guildScheduledEvent);
        R.append(")");
        return R.toString();
    }
}
