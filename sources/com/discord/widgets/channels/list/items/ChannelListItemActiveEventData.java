package com.discord.widgets.channels.list.items;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.UserGuildMember;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventLocationInfo;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ChannelListItemActiveEvent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u001f\b\u0086\b\u0018\u00002\u00020\u0001BO\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0005\u0012\u0006\u0010\u0019\u001a\u00020\b\u0012\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0006\u0010\u001b\u001a\u00020\u000f\u0012\u0006\u0010\u001c\u001a\u00020\u000f\u0012\u000e\u0010\u001d\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u0014¢\u0006\u0004\b1\u00102J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\u0018\u0010\u0015\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016Jf\u0010\u001e\u001a\u00020\u00002\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00052\b\b\u0002\u0010\u0019\u001a\u00020\b2\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\b\u0002\u0010\u001b\u001a\u00020\u000f2\b\b\u0002\u0010\u001c\u001a\u00020\u000f2\u0010\b\u0002\u0010\u001d\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u0014HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b \u0010\u0004J\u0010\u0010!\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b!\u0010\nJ\u001a\u0010#\u001a\u00020\u000f2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u001f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010%\u001a\u0004\b&\u0010\u000eR\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010'\u001a\u0004\b(\u0010\u0004R!\u0010\u001d\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010)\u001a\u0004\b*\u0010\u0016R\u0019\u0010\u001b\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010+\u001a\u0004\b,\u0010\u0011R\u0019\u0010\u0018\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010-\u001a\u0004\b.\u0010\u0007R\u0019\u0010\u001c\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010+\u001a\u0004\b\u001c\u0010\u0011R\u0019\u0010\u0019\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010/\u001a\u0004\b0\u0010\n¨\u00063"}, d2 = {"Lcom/discord/widgets/channels/list/items/ChannelListItemActiveEventData;", "", "", "component1", "()Ljava/lang/String;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "component2", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "", "component3", "()I", "", "Lcom/discord/models/guild/UserGuildMember;", "component4", "()Ljava/util/List;", "", "component5", "()Z", "component6", "", "Lcom/discord/primitives/GuildScheduledEventId;", "component7", "()Ljava/lang/Long;", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "locationInfo", "audienceSize", "speakers", "connected", "isSpeaker", "eventId", "copy", "(Ljava/lang/String;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;ILjava/util/List;ZZLjava/lang/Long;)Lcom/discord/widgets/channels/list/items/ChannelListItemActiveEventData;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getSpeakers", "Ljava/lang/String;", "getTopic", "Ljava/lang/Long;", "getEventId", "Z", "getConnected", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "getLocationInfo", "I", "getAudienceSize", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;ILjava/util/List;ZZLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelListItemActiveEventData {
    private final int audienceSize;
    private final boolean connected;
    private final Long eventId;
    private final boolean isSpeaker;
    private final GuildScheduledEventLocationInfo locationInfo;
    private final List<UserGuildMember> speakers;
    private final String topic;

    public ChannelListItemActiveEventData(String str, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, int i, List<UserGuildMember> list, boolean z2, boolean z3, Long l) {
        m.checkNotNullParameter(guildScheduledEventLocationInfo, "locationInfo");
        m.checkNotNullParameter(list, "speakers");
        this.topic = str;
        this.locationInfo = guildScheduledEventLocationInfo;
        this.audienceSize = i;
        this.speakers = list;
        this.connected = z2;
        this.isSpeaker = z3;
        this.eventId = l;
    }

    public static /* synthetic */ ChannelListItemActiveEventData copy$default(ChannelListItemActiveEventData channelListItemActiveEventData, String str, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, int i, List list, boolean z2, boolean z3, Long l, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = channelListItemActiveEventData.topic;
        }
        if ((i2 & 2) != 0) {
            guildScheduledEventLocationInfo = channelListItemActiveEventData.locationInfo;
        }
        GuildScheduledEventLocationInfo guildScheduledEventLocationInfo2 = guildScheduledEventLocationInfo;
        if ((i2 & 4) != 0) {
            i = channelListItemActiveEventData.audienceSize;
        }
        int i3 = i;
        List<UserGuildMember> list2 = list;
        if ((i2 & 8) != 0) {
            list2 = channelListItemActiveEventData.speakers;
        }
        List list3 = list2;
        if ((i2 & 16) != 0) {
            z2 = channelListItemActiveEventData.connected;
        }
        boolean z4 = z2;
        if ((i2 & 32) != 0) {
            z3 = channelListItemActiveEventData.isSpeaker;
        }
        boolean z5 = z3;
        if ((i2 & 64) != 0) {
            l = channelListItemActiveEventData.eventId;
        }
        return channelListItemActiveEventData.copy(str, guildScheduledEventLocationInfo2, i3, list3, z4, z5, l);
    }

    public final String component1() {
        return this.topic;
    }

    public final GuildScheduledEventLocationInfo component2() {
        return this.locationInfo;
    }

    public final int component3() {
        return this.audienceSize;
    }

    public final List<UserGuildMember> component4() {
        return this.speakers;
    }

    public final boolean component5() {
        return this.connected;
    }

    public final boolean component6() {
        return this.isSpeaker;
    }

    public final Long component7() {
        return this.eventId;
    }

    public final ChannelListItemActiveEventData copy(String str, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, int i, List<UserGuildMember> list, boolean z2, boolean z3, Long l) {
        m.checkNotNullParameter(guildScheduledEventLocationInfo, "locationInfo");
        m.checkNotNullParameter(list, "speakers");
        return new ChannelListItemActiveEventData(str, guildScheduledEventLocationInfo, i, list, z2, z3, l);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChannelListItemActiveEventData)) {
            return false;
        }
        ChannelListItemActiveEventData channelListItemActiveEventData = (ChannelListItemActiveEventData) obj;
        return m.areEqual(this.topic, channelListItemActiveEventData.topic) && m.areEqual(this.locationInfo, channelListItemActiveEventData.locationInfo) && this.audienceSize == channelListItemActiveEventData.audienceSize && m.areEqual(this.speakers, channelListItemActiveEventData.speakers) && this.connected == channelListItemActiveEventData.connected && this.isSpeaker == channelListItemActiveEventData.isSpeaker && m.areEqual(this.eventId, channelListItemActiveEventData.eventId);
    }

    public final int getAudienceSize() {
        return this.audienceSize;
    }

    public final boolean getConnected() {
        return this.connected;
    }

    public final Long getEventId() {
        return this.eventId;
    }

    public final GuildScheduledEventLocationInfo getLocationInfo() {
        return this.locationInfo;
    }

    public final List<UserGuildMember> getSpeakers() {
        return this.speakers;
    }

    public final String getTopic() {
        return this.topic;
    }

    public int hashCode() {
        String str = this.topic;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        GuildScheduledEventLocationInfo guildScheduledEventLocationInfo = this.locationInfo;
        int hashCode2 = (((hashCode + (guildScheduledEventLocationInfo != null ? guildScheduledEventLocationInfo.hashCode() : 0)) * 31) + this.audienceSize) * 31;
        List<UserGuildMember> list = this.speakers;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        boolean z2 = this.connected;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        boolean z3 = this.isSpeaker;
        if (!z3) {
            i2 = z3 ? 1 : 0;
        }
        int i6 = (i5 + i2) * 31;
        Long l = this.eventId;
        if (l != null) {
            i = l.hashCode();
        }
        return i6 + i;
    }

    public final boolean isSpeaker() {
        return this.isSpeaker;
    }

    public String toString() {
        StringBuilder R = a.R("ChannelListItemActiveEventData(topic=");
        R.append(this.topic);
        R.append(", locationInfo=");
        R.append(this.locationInfo);
        R.append(", audienceSize=");
        R.append(this.audienceSize);
        R.append(", speakers=");
        R.append(this.speakers);
        R.append(", connected=");
        R.append(this.connected);
        R.append(", isSpeaker=");
        R.append(this.isSpeaker);
        R.append(", eventId=");
        return a.F(R, this.eventId, ")");
    }
}
