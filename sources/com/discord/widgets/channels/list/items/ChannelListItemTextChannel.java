package com.discord.widgets.channels.list.items;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.widgets.channels.list.WidgetChannelListUnreads;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelListItemTextChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002BO\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0006\u0012\u0006\u0010\u0014\u001a\u00020\t\u0012\u0006\u0010\u0015\u001a\u00020\u0006\u0012\u0006\u0010\u0016\u001a\u00020\u0006\u0012\u0006\u0010\u0017\u001a\u00020\u0006\u0012\u0006\u0010\u0018\u001a\u00020\u0006\u0012\u0006\u0010\u0019\u001a\u00020\u0006\u0012\u0006\u0010\u001a\u001a\u00020\u0006¢\u0006\u0004\b2\u00103J\u0010\u0010\u0004\u001a\u00020\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0010\u0010\r\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000e\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u000e\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u000f\u0010\bJ\u0010\u0010\u0010\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0010\u0010\bJ\u0010\u0010\u0011\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0011\u0010\bJj\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00032\b\b\u0002\u0010\u0013\u001a\u00020\u00062\b\b\u0002\u0010\u0014\u001a\u00020\t2\b\b\u0002\u0010\u0015\u001a\u00020\u00062\b\b\u0002\u0010\u0016\u001a\u00020\u00062\b\b\u0002\u0010\u0017\u001a\u00020\u00062\b\b\u0002\u0010\u0018\u001a\u00020\u00062\b\b\u0002\u0010\u0019\u001a\u00020\u00062\b\b\u0002\u0010\u001a\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\tHÖ\u0001¢\u0006\u0004\b \u0010\u000bJ\u001a\u0010#\u001a\u00020\u00062\b\u0010\"\u001a\u0004\u0018\u00010!HÖ\u0003¢\u0006\u0004\b#\u0010$R\u001c\u0010%\u001a\u00020\t8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010\u000bR\u0019\u0010\u0012\u001a\u00020\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010(\u001a\u0004\b)\u0010\u0005R\u0019\u0010\u0013\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010*\u001a\u0004\b+\u0010\bR\u001c\u0010\u0014\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b,\u0010\u000bR\u0019\u0010\u0017\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010*\u001a\u0004\b\u0017\u0010\bR\u0019\u0010\u0016\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010*\u001a\u0004\b-\u0010\bR\u0019\u0010\u0018\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b.\u0010\bR\u001c\u0010/\u001a\u00020\u001d8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u001fR\u0019\u0010\u0019\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\u001a\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010*\u001a\u0004\b\u001a\u0010\bR\u001c\u0010\u0015\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0015\u0010*\u001a\u0004\b\u0015\u0010\b¨\u00064"}, d2 = {"Lcom/discord/widgets/channels/list/items/ChannelListItemTextChannel;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "", "component3", "()I", "component4", "component5", "component6", "component7", "component8", "component9", "channel", "selected", "mentionCount", "isUnread", "muted", "isLocked", "hasActiveThreads", "isGuildRoleSubscriptionLockedChannel", "isGuildRoleSubscriptionChannel", "copy", "(Lcom/discord/api/channel/Channel;ZIZZZZZZ)Lcom/discord/widgets/channels/list/items/ChannelListItemTextChannel;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "Lcom/discord/api/channel/Channel;", "getChannel", "Z", "getSelected", "getMentionCount", "getMuted", "getHasActiveThreads", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZIZZZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelListItemTextChannel implements ChannelListItem, WidgetChannelListUnreads.UnreadItem {
    private final Channel channel;
    private final boolean hasActiveThreads;
    private final boolean isGuildRoleSubscriptionChannel;
    private final boolean isGuildRoleSubscriptionLockedChannel;
    private final boolean isLocked;
    private final boolean isUnread;
    private final String key;
    private final int mentionCount;
    private final boolean muted;
    private final boolean selected;
    private final int type;

    public ChannelListItemTextChannel(Channel channel, boolean z2, int i, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8) {
        m.checkNotNullParameter(channel, "channel");
        this.channel = channel;
        this.selected = z2;
        this.mentionCount = i;
        this.isUnread = z3;
        this.muted = z4;
        this.isLocked = z5;
        this.hasActiveThreads = z6;
        this.isGuildRoleSubscriptionLockedChannel = z7;
        this.isGuildRoleSubscriptionChannel = z8;
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(channel.h());
        this.key = sb.toString();
    }

    public final Channel component1() {
        return this.channel;
    }

    public final boolean component2() {
        return this.selected;
    }

    public final int component3() {
        return getMentionCount();
    }

    public final boolean component4() {
        return isUnread();
    }

    public final boolean component5() {
        return this.muted;
    }

    public final boolean component6() {
        return this.isLocked;
    }

    public final boolean component7() {
        return this.hasActiveThreads;
    }

    public final boolean component8() {
        return this.isGuildRoleSubscriptionLockedChannel;
    }

    public final boolean component9() {
        return this.isGuildRoleSubscriptionChannel;
    }

    public final ChannelListItemTextChannel copy(Channel channel, boolean z2, int i, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8) {
        m.checkNotNullParameter(channel, "channel");
        return new ChannelListItemTextChannel(channel, z2, i, z3, z4, z5, z6, z7, z8);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChannelListItemTextChannel)) {
            return false;
        }
        ChannelListItemTextChannel channelListItemTextChannel = (ChannelListItemTextChannel) obj;
        return m.areEqual(this.channel, channelListItemTextChannel.channel) && this.selected == channelListItemTextChannel.selected && getMentionCount() == channelListItemTextChannel.getMentionCount() && isUnread() == channelListItemTextChannel.isUnread() && this.muted == channelListItemTextChannel.muted && this.isLocked == channelListItemTextChannel.isLocked && this.hasActiveThreads == channelListItemTextChannel.hasActiveThreads && this.isGuildRoleSubscriptionLockedChannel == channelListItemTextChannel.isGuildRoleSubscriptionLockedChannel && this.isGuildRoleSubscriptionChannel == channelListItemTextChannel.isGuildRoleSubscriptionChannel;
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final boolean getHasActiveThreads() {
        return this.hasActiveThreads;
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListItem, com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    @Override // com.discord.widgets.channels.list.WidgetChannelListUnreads.UnreadItem
    public int getMentionCount() {
        return this.mentionCount;
    }

    public final boolean getMuted() {
        return this.muted;
    }

    public final boolean getSelected() {
        return this.selected;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        Channel channel = this.channel;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        boolean z2 = this.selected;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int mentionCount = (getMentionCount() + ((hashCode + i2) * 31)) * 31;
        boolean isUnread = isUnread();
        if (isUnread) {
            isUnread = true;
        }
        int i4 = isUnread ? 1 : 0;
        int i5 = isUnread ? 1 : 0;
        int i6 = (mentionCount + i4) * 31;
        boolean z3 = this.muted;
        if (z3) {
            z3 = true;
        }
        int i7 = z3 ? 1 : 0;
        int i8 = z3 ? 1 : 0;
        int i9 = (i6 + i7) * 31;
        boolean z4 = this.isLocked;
        if (z4) {
            z4 = true;
        }
        int i10 = z4 ? 1 : 0;
        int i11 = z4 ? 1 : 0;
        int i12 = (i9 + i10) * 31;
        boolean z5 = this.hasActiveThreads;
        if (z5) {
            z5 = true;
        }
        int i13 = z5 ? 1 : 0;
        int i14 = z5 ? 1 : 0;
        int i15 = (i12 + i13) * 31;
        boolean z6 = this.isGuildRoleSubscriptionLockedChannel;
        if (z6) {
            z6 = true;
        }
        int i16 = z6 ? 1 : 0;
        int i17 = z6 ? 1 : 0;
        int i18 = (i15 + i16) * 31;
        boolean z7 = this.isGuildRoleSubscriptionChannel;
        if (!z7) {
            i = z7 ? 1 : 0;
        }
        return i18 + i;
    }

    public final boolean isGuildRoleSubscriptionChannel() {
        return this.isGuildRoleSubscriptionChannel;
    }

    public final boolean isGuildRoleSubscriptionLockedChannel() {
        return this.isGuildRoleSubscriptionLockedChannel;
    }

    public final boolean isLocked() {
        return this.isLocked;
    }

    @Override // com.discord.widgets.channels.list.WidgetChannelListUnreads.UnreadItem
    public boolean isUnread() {
        return this.isUnread;
    }

    public String toString() {
        StringBuilder R = a.R("ChannelListItemTextChannel(channel=");
        R.append(this.channel);
        R.append(", selected=");
        R.append(this.selected);
        R.append(", mentionCount=");
        R.append(getMentionCount());
        R.append(", isUnread=");
        R.append(isUnread());
        R.append(", muted=");
        R.append(this.muted);
        R.append(", isLocked=");
        R.append(this.isLocked);
        R.append(", hasActiveThreads=");
        R.append(this.hasActiveThreads);
        R.append(", isGuildRoleSubscriptionLockedChannel=");
        R.append(this.isGuildRoleSubscriptionLockedChannel);
        R.append(", isGuildRoleSubscriptionChannel=");
        return a.M(R, this.isGuildRoleSubscriptionChannel, ")");
    }
}
