package com.discord.widgets.channels.list.items;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelListItemCategory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\t\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J8\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00052\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u000b\u0010\u0007R\u001c\u0010\u001b\u001a\u00020\u00138\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0015R\u001c\u0010\u001e\u001a\u00020\u00108\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010\u0012R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\f\u0010\u0007R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010!\u001a\u0004\b\"\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b#\u0010\u0007¨\u0006&"}, d2 = {"Lcom/discord/widgets/channels/list/items/ChannelListItemCategory;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "component3", "component4", "channel", "isCollapsed", "isMuted", "canManageChannels", "copy", "(Lcom/discord/api/channel/Channel;ZZZ)Lcom/discord/widgets/channels/list/items/ChannelListItemCategory;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/api/channel/Channel;", "getChannel", "getCanManageChannels", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelListItemCategory implements ChannelListItem {
    private final boolean canManageChannels;
    private final Channel channel;
    private final boolean isCollapsed;
    private final boolean isMuted;
    private final String key;
    private final int type = 8;

    public ChannelListItemCategory(Channel channel, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(channel, "channel");
        this.channel = channel;
        this.isCollapsed = z2;
        this.isMuted = z3;
        this.canManageChannels = z4;
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(channel.h());
        this.key = sb.toString();
    }

    public static /* synthetic */ ChannelListItemCategory copy$default(ChannelListItemCategory channelListItemCategory, Channel channel, boolean z2, boolean z3, boolean z4, int i, Object obj) {
        if ((i & 1) != 0) {
            channel = channelListItemCategory.channel;
        }
        if ((i & 2) != 0) {
            z2 = channelListItemCategory.isCollapsed;
        }
        if ((i & 4) != 0) {
            z3 = channelListItemCategory.isMuted;
        }
        if ((i & 8) != 0) {
            z4 = channelListItemCategory.canManageChannels;
        }
        return channelListItemCategory.copy(channel, z2, z3, z4);
    }

    public final Channel component1() {
        return this.channel;
    }

    public final boolean component2() {
        return this.isCollapsed;
    }

    public final boolean component3() {
        return this.isMuted;
    }

    public final boolean component4() {
        return this.canManageChannels;
    }

    public final ChannelListItemCategory copy(Channel channel, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(channel, "channel");
        return new ChannelListItemCategory(channel, z2, z3, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChannelListItemCategory)) {
            return false;
        }
        ChannelListItemCategory channelListItemCategory = (ChannelListItemCategory) obj;
        return m.areEqual(this.channel, channelListItemCategory.channel) && this.isCollapsed == channelListItemCategory.isCollapsed && this.isMuted == channelListItemCategory.isMuted && this.canManageChannels == channelListItemCategory.canManageChannels;
    }

    public final boolean getCanManageChannels() {
        return this.canManageChannels;
    }

    public final Channel getChannel() {
        return this.channel;
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListItem, com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        Channel channel = this.channel;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        boolean z2 = this.isCollapsed;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode + i2) * 31;
        boolean z3 = this.isMuted;
        if (z3) {
            z3 = true;
        }
        int i5 = z3 ? 1 : 0;
        int i6 = z3 ? 1 : 0;
        int i7 = (i4 + i5) * 31;
        boolean z4 = this.canManageChannels;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        return i7 + i;
    }

    public final boolean isCollapsed() {
        return this.isCollapsed;
    }

    public final boolean isMuted() {
        return this.isMuted;
    }

    public String toString() {
        StringBuilder R = a.R("ChannelListItemCategory(channel=");
        R.append(this.channel);
        R.append(", isCollapsed=");
        R.append(this.isCollapsed);
        R.append(", isMuted=");
        R.append(this.isMuted);
        R.append(", canManageChannels=");
        return a.M(R, this.canManageChannels, ")");
    }
}
