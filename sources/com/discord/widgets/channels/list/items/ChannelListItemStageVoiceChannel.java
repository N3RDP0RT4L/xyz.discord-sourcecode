package com.discord.widgets.channels.list.items;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.stageinstance.StageInstance;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelListItemStageVoiceChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002BY\u0012\u0006\u0010\u0017\u001a\u00020\u0003\u0012\u0006\u0010\u0018\u001a\u00020\u0006\u0012\u000e\u0010\u0019\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n\u0012\u0006\u0010\u001a\u001a\u00020\u0006\u0012\u0006\u0010\u001b\u001a\u00020\u000e\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\u001d\u001a\u00020\u0006\u0012\u0006\u0010\u001e\u001a\u00020\u0006\u0012\u0006\u0010\u001f\u001a\u00020\u0006¢\u0006\u0004\b<\u0010=J\u0010\u0010\u0004\u001a\u00020\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0018\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0014\u0010\bJ\u0010\u0010\u0015\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0015\u0010\bJ\u0010\u0010\u0016\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0016\u0010\bJt\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0017\u001a\u00020\u00032\b\b\u0002\u0010\u0018\u001a\u00020\u00062\u0010\b\u0002\u0010\u0019\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\u001b\u001a\u00020\u000e2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010\u001d\u001a\u00020\u00062\b\b\u0002\u0010\u001e\u001a\u00020\u00062\b\b\u0002\u0010\u001f\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b%\u0010\u0010J\u001a\u0010(\u001a\u00020\u00062\b\u0010'\u001a\u0004\u0018\u00010&HÖ\u0003¢\u0006\u0004\b(\u0010)R\u001c\u0010*\u001a\u00020\"8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010$R!\u0010\u0019\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010-\u001a\u0004\b.\u0010\fR\u001c\u0010\u001b\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u0010/\u001a\u0004\b0\u0010\u0010R\u0019\u0010\u001a\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00101\u001a\u0004\b\u001a\u0010\bR\u0019\u0010\u001e\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b\u001e\u0010\bR\u001c\u0010\u0017\u001a\u00020\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0017\u00102\u001a\u0004\b3\u0010\u0005R\u001c\u00104\u001a\u00020\u000e8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b4\u0010/\u001a\u0004\b5\u0010\u0010R\u0019\u0010\u0018\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00101\u001a\u0004\b6\u0010\bR\u0019\u0010\u001d\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b7\u0010\bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00108\u001a\u0004\b9\u0010\u0013R\u0019\u0010\u001f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b\u001f\u0010\bR\u0013\u0010;\u001a\u00020\u00068F@\u0006¢\u0006\u0006\u001a\u0004\b:\u0010\b¨\u0006>"}, d2 = {"Lcom/discord/widgets/channels/list/items/ChannelListItemStageVoiceChannel;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "Lcom/discord/widgets/channels/list/items/ChannelListVocalItem;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "", "Lcom/discord/api/permission/PermissionBit;", "component3", "()Ljava/lang/Long;", "component4", "", "component5", "()I", "Lcom/discord/api/stageinstance/StageInstance;", "component6", "()Lcom/discord/api/stageinstance/StageInstance;", "component7", "component8", "component9", "channel", "selected", "permission", "isLocked", "numUsersConnected", "stageInstance", "hasSpeakers", "isGuildRoleSubscriptionLockedChannel", "isGuildRoleSubscriptionChannel", "copy", "(Lcom/discord/api/channel/Channel;ZLjava/lang/Long;ZILcom/discord/api/stageinstance/StageInstance;ZZZ)Lcom/discord/widgets/channels/list/items/ChannelListItemStageVoiceChannel;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Ljava/lang/Long;", "getPermission", "I", "getNumUsersConnected", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", "type", "getType", "getSelected", "getHasSpeakers", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "getHasActiveEvent", "hasActiveEvent", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZLjava/lang/Long;ZILcom/discord/api/stageinstance/StageInstance;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelListItemStageVoiceChannel implements ChannelListItem, ChannelListVocalItem {
    private final Channel channel;
    private final boolean hasSpeakers;
    private final boolean isGuildRoleSubscriptionChannel;
    private final boolean isGuildRoleSubscriptionLockedChannel;
    private final boolean isLocked;
    private final String key;
    private final int numUsersConnected;
    private final Long permission;
    private final boolean selected;
    private final StageInstance stageInstance;
    private final int type = 13;

    public ChannelListItemStageVoiceChannel(Channel channel, boolean z2, Long l, boolean z3, int i, StageInstance stageInstance, boolean z4, boolean z5, boolean z6) {
        m.checkNotNullParameter(channel, "channel");
        this.channel = channel;
        this.selected = z2;
        this.permission = l;
        this.isLocked = z3;
        this.numUsersConnected = i;
        this.stageInstance = stageInstance;
        this.hasSpeakers = z4;
        this.isGuildRoleSubscriptionLockedChannel = z5;
        this.isGuildRoleSubscriptionChannel = z6;
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(getChannel().h());
        this.key = sb.toString();
    }

    public final Channel component1() {
        return getChannel();
    }

    public final boolean component2() {
        return this.selected;
    }

    public final Long component3() {
        return this.permission;
    }

    public final boolean component4() {
        return this.isLocked;
    }

    public final int component5() {
        return getNumUsersConnected();
    }

    public final StageInstance component6() {
        return this.stageInstance;
    }

    public final boolean component7() {
        return this.hasSpeakers;
    }

    public final boolean component8() {
        return this.isGuildRoleSubscriptionLockedChannel;
    }

    public final boolean component9() {
        return this.isGuildRoleSubscriptionChannel;
    }

    public final ChannelListItemStageVoiceChannel copy(Channel channel, boolean z2, Long l, boolean z3, int i, StageInstance stageInstance, boolean z4, boolean z5, boolean z6) {
        m.checkNotNullParameter(channel, "channel");
        return new ChannelListItemStageVoiceChannel(channel, z2, l, z3, i, stageInstance, z4, z5, z6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChannelListItemStageVoiceChannel)) {
            return false;
        }
        ChannelListItemStageVoiceChannel channelListItemStageVoiceChannel = (ChannelListItemStageVoiceChannel) obj;
        return m.areEqual(getChannel(), channelListItemStageVoiceChannel.getChannel()) && this.selected == channelListItemStageVoiceChannel.selected && m.areEqual(this.permission, channelListItemStageVoiceChannel.permission) && this.isLocked == channelListItemStageVoiceChannel.isLocked && getNumUsersConnected() == channelListItemStageVoiceChannel.getNumUsersConnected() && m.areEqual(this.stageInstance, channelListItemStageVoiceChannel.stageInstance) && this.hasSpeakers == channelListItemStageVoiceChannel.hasSpeakers && this.isGuildRoleSubscriptionLockedChannel == channelListItemStageVoiceChannel.isGuildRoleSubscriptionLockedChannel && this.isGuildRoleSubscriptionChannel == channelListItemStageVoiceChannel.isGuildRoleSubscriptionChannel;
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListVocalItem
    public Channel getChannel() {
        return this.channel;
    }

    public final boolean getHasActiveEvent() {
        return this.stageInstance != null;
    }

    public final boolean getHasSpeakers() {
        return this.hasSpeakers;
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListItem, com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    @Override // com.discord.widgets.channels.list.items.ChannelListVocalItem
    public int getNumUsersConnected() {
        return this.numUsersConnected;
    }

    public final Long getPermission() {
        return this.permission;
    }

    public final boolean getSelected() {
        return this.selected;
    }

    public final StageInstance getStageInstance() {
        return this.stageInstance;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        Channel channel = getChannel();
        int i = 0;
        int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
        boolean z2 = this.selected;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        Long l = this.permission;
        int hashCode2 = (i5 + (l != null ? l.hashCode() : 0)) * 31;
        boolean z3 = this.isLocked;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int numUsersConnected = (getNumUsersConnected() + ((hashCode2 + i6) * 31)) * 31;
        StageInstance stageInstance = this.stageInstance;
        if (stageInstance != null) {
            i = stageInstance.hashCode();
        }
        int i8 = (numUsersConnected + i) * 31;
        boolean z4 = this.hasSpeakers;
        if (z4) {
            z4 = true;
        }
        int i9 = z4 ? 1 : 0;
        int i10 = z4 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        boolean z5 = this.isGuildRoleSubscriptionLockedChannel;
        if (z5) {
            z5 = true;
        }
        int i12 = z5 ? 1 : 0;
        int i13 = z5 ? 1 : 0;
        int i14 = (i11 + i12) * 31;
        boolean z6 = this.isGuildRoleSubscriptionChannel;
        if (!z6) {
            i2 = z6 ? 1 : 0;
        }
        return i14 + i2;
    }

    public final boolean isGuildRoleSubscriptionChannel() {
        return this.isGuildRoleSubscriptionChannel;
    }

    public final boolean isGuildRoleSubscriptionLockedChannel() {
        return this.isGuildRoleSubscriptionLockedChannel;
    }

    public final boolean isLocked() {
        return this.isLocked;
    }

    public String toString() {
        StringBuilder R = a.R("ChannelListItemStageVoiceChannel(channel=");
        R.append(getChannel());
        R.append(", selected=");
        R.append(this.selected);
        R.append(", permission=");
        R.append(this.permission);
        R.append(", isLocked=");
        R.append(this.isLocked);
        R.append(", numUsersConnected=");
        R.append(getNumUsersConnected());
        R.append(", stageInstance=");
        R.append(this.stageInstance);
        R.append(", hasSpeakers=");
        R.append(this.hasSpeakers);
        R.append(", isGuildRoleSubscriptionLockedChannel=");
        R.append(this.isGuildRoleSubscriptionLockedChannel);
        R.append(", isGuildRoleSubscriptionChannel=");
        return a.M(R, this.isGuildRoleSubscriptionChannel, ")");
    }
}
