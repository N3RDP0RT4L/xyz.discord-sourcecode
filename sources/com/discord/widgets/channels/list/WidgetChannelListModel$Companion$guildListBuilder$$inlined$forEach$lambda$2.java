package com.discord.widgets.channels.list;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.widgets.channels.list.WidgetChannelListModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelListModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "vocalChannel", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$VocalChannelData;", "invoke", "(Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$VocalChannelData;", "com/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$6$2", "getVocalChannelData"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2 extends o implements Function1<Channel, WidgetChannelListModel.Companion.VocalChannelData> {
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$4 $areAllChildThreadsRead$4$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$5 $areAnyChildThreadsSelected$5$inlined;
    public final /* synthetic */ boolean $canSeeGuildRoleSubscriptions$inlined;
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Set $channelsWithActiveThreads$inlined;
    public final /* synthetic */ Set $collapsedCategories$inlined;
    public final /* synthetic */ Map $directories$inlined;
    public final /* synthetic */ Map $directoryEvents$inlined;
    public final /* synthetic */ HashSet $forceViewCategories$inlined;
    public final /* synthetic */ GuildChannelsInfo $guild$inlined;
    public final /* synthetic */ List $guildScheduledEvents$inlined;
    public final /* synthetic */ HashSet $hiddenChannelsIds$inlined;
    public final /* synthetic */ boolean $isGuildHub$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$3 $isThreadUnread$3$inlined;
    public final /* synthetic */ ArrayList $items$inlined;
    public final /* synthetic */ Map $joinedThreads$inlined;
    public final /* synthetic */ Map $mentionCounts$inlined;
    public final /* synthetic */ Map $messageAcks$inlined;
    public final /* synthetic */ Long $permissions;
    public final /* synthetic */ Channel $selectedChannel$inlined;
    public final /* synthetic */ long $selectedGuildId$inlined;
    public final /* synthetic */ long $selectedVoiceChannelId$inlined;
    public final /* synthetic */ Map $stageChannels$inlined;
    public final /* synthetic */ Map $stageInstances$inlined;
    public final /* synthetic */ Map $threadParentMap$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$2 $tryRemoveEmptyCategory$2$inlined;
    public final /* synthetic */ Set $unreadChannelIds$inlined;
    public final /* synthetic */ Map $voiceStates$inlined;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2(long j, Channel channel, Long l, GuildChannelsInfo guildChannelsInfo, HashSet hashSet, boolean z2, Channel channel2, Map map, Set set, long j2, WidgetChannelListModel$Companion$guildListBuilder$5 widgetChannelListModel$Companion$guildListBuilder$5, Set set2, WidgetChannelListModel$Companion$guildListBuilder$3 widgetChannelListModel$Companion$guildListBuilder$3, HashSet hashSet2, WidgetChannelListModel$Companion$guildListBuilder$4 widgetChannelListModel$Companion$guildListBuilder$4, long j3, Map map2, boolean z3, Set set3, List list, Map map3, Map map4, Map map5, Map map6, ArrayList arrayList, WidgetChannelListModel$Companion$guildListBuilder$2 widgetChannelListModel$Companion$guildListBuilder$2, Map map7, Map map8, Map map9) {
        super(1);
        this.$channelId = j;
        this.$channel = channel;
        this.$permissions = l;
        this.$guild$inlined = guildChannelsInfo;
        this.$forceViewCategories$inlined = hashSet;
        this.$isGuildHub$inlined = z2;
        this.$selectedChannel$inlined = channel2;
        this.$mentionCounts$inlined = map;
        this.$unreadChannelIds$inlined = set;
        this.$selectedGuildId$inlined = j2;
        this.$areAnyChildThreadsSelected$5$inlined = widgetChannelListModel$Companion$guildListBuilder$5;
        this.$collapsedCategories$inlined = set2;
        this.$isThreadUnread$3$inlined = widgetChannelListModel$Companion$guildListBuilder$3;
        this.$hiddenChannelsIds$inlined = hashSet2;
        this.$areAllChildThreadsRead$4$inlined = widgetChannelListModel$Companion$guildListBuilder$4;
        this.$selectedVoiceChannelId$inlined = j3;
        this.$voiceStates$inlined = map2;
        this.$canSeeGuildRoleSubscriptions$inlined = z3;
        this.$channelsWithActiveThreads$inlined = set3;
        this.$guildScheduledEvents$inlined = list;
        this.$stageInstances$inlined = map3;
        this.$stageChannels$inlined = map4;
        this.$threadParentMap$inlined = map5;
        this.$joinedThreads$inlined = map6;
        this.$items$inlined = arrayList;
        this.$tryRemoveEmptyCategory$2$inlined = widgetChannelListModel$Companion$guildListBuilder$2;
        this.$messageAcks$inlined = map7;
        this.$directories$inlined = map8;
        this.$directoryEvents$inlined = map9;
    }

    public final WidgetChannelListModel.Companion.VocalChannelData invoke(Channel channel) {
        PermissionOverwrite permissionOverwrite;
        Object obj;
        boolean z2;
        m.checkNotNullParameter(channel, "vocalChannel");
        long j = this.$channelId;
        boolean z3 = j == this.$selectedVoiceChannelId$inlined;
        Channel channel2 = this.$selectedChannel$inlined;
        boolean z4 = channel2 != null && j == channel2.h();
        Collection collection = (Collection) a.c(this.$channel, this.$voiceStates$inlined);
        int size = collection != null ? collection.size() : 0;
        boolean can = PermissionUtils.can(Permission.VIEW_CHANNEL, this.$permissions);
        boolean z5 = this.$canSeeGuildRoleSubscriptions$inlined && ChannelUtils.q(channel, this.$guild$inlined.getGuild(), this.$guild$inlined.getEveryoneRole(), this.$guild$inlined.getGuildRoles());
        PermissionUtils permissionUtils = PermissionUtils.INSTANCE;
        GuildRole everyoneRole = this.$guild$inlined.getEveryoneRole();
        List<PermissionOverwrite> s2 = channel.s();
        if (s2 != null) {
            Iterator<T> it = s2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                Object next = it.next();
                if (((PermissionOverwrite) next).e() == this.$selectedGuildId$inlined) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    obj = next;
                    break;
                }
            }
            permissionOverwrite = (PermissionOverwrite) obj;
        } else {
            permissionOverwrite = null;
        }
        boolean z6 = !permissionUtils.canRole(Permission.CONNECT, everyoneRole, permissionOverwrite) || (!can && z5);
        boolean o = this.$channel.o();
        Integer num = (Integer) this.$mentionCounts$inlined.get(Long.valueOf(this.$channelId));
        int intValue = num != null ? num.intValue() : 0;
        boolean contains = this.$unreadChannelIds$inlined.contains(Long.valueOf(this.$channelId));
        if (!can && !z3 && !z5) {
            return null;
        }
        if (this.$collapsedCategories$inlined.contains(Long.valueOf(channel.r())) && size == 0 && !z3) {
            return null;
        }
        return new WidgetChannelListModel.Companion.VocalChannelData(z3, z4, intValue, contains, z6, o, this.$permissions, size, (!can || !PermissionUtils.can(Permission.CONNECT, this.$permissions)) && z5, z5);
    }
}
