package com.discord.widgets.channels.list;

import com.discord.api.channel.Channel;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.widgets.channels.list.items.ChannelListItemVoiceChannel;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelListModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceChannel;", "invoke", "()Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceChannel;", "com/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$6$5", "toChannelListItemVoiceChannel"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$5 extends o implements Function0<ChannelListItemVoiceChannel> {
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$4 $areAllChildThreadsRead$4$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$5 $areAnyChildThreadsSelected$5$inlined;
    public final /* synthetic */ boolean $canSeeGuildRoleSubscriptions$inlined;
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ Set $channelsWithActiveThreads$inlined;
    public final /* synthetic */ Set $collapsedCategories$inlined;
    public final /* synthetic */ Map $directories$inlined;
    public final /* synthetic */ Map $directoryEvents$inlined;
    public final /* synthetic */ HashSet $forceViewCategories$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2 $getVocalChannelData$2;
    public final /* synthetic */ GuildChannelsInfo $guild$inlined;
    public final /* synthetic */ List $guildScheduledEvents$inlined;
    public final /* synthetic */ HashSet $hiddenChannelsIds$inlined;
    public final /* synthetic */ boolean $isGuildHub$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$3 $isThreadUnread$3$inlined;
    public final /* synthetic */ ArrayList $items$inlined;
    public final /* synthetic */ Map $joinedThreads$inlined;
    public final /* synthetic */ Map $mentionCounts$inlined;
    public final /* synthetic */ Map $messageAcks$inlined;
    public final /* synthetic */ Long $permissions;
    public final /* synthetic */ Channel $selectedChannel$inlined;
    public final /* synthetic */ long $selectedGuildId$inlined;
    public final /* synthetic */ long $selectedVoiceChannelId$inlined;
    public final /* synthetic */ Map $stageChannels$inlined;
    public final /* synthetic */ Map $stageInstances$inlined;
    public final /* synthetic */ Map $threadParentMap$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$2 $tryRemoveEmptyCategory$2$inlined;
    public final /* synthetic */ Set $unreadChannelIds$inlined;
    public final /* synthetic */ Map $voiceStates$inlined;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$5(WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2 widgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2, Channel channel, Long l, GuildChannelsInfo guildChannelsInfo, HashSet hashSet, boolean z2, Channel channel2, Map map, Set set, long j, WidgetChannelListModel$Companion$guildListBuilder$5 widgetChannelListModel$Companion$guildListBuilder$5, Set set2, WidgetChannelListModel$Companion$guildListBuilder$3 widgetChannelListModel$Companion$guildListBuilder$3, HashSet hashSet2, WidgetChannelListModel$Companion$guildListBuilder$4 widgetChannelListModel$Companion$guildListBuilder$4, long j2, Map map2, boolean z3, Set set3, List list, Map map3, Map map4, Map map5, Map map6, ArrayList arrayList, WidgetChannelListModel$Companion$guildListBuilder$2 widgetChannelListModel$Companion$guildListBuilder$2, Map map7, Map map8, Map map9) {
        super(0);
        this.$getVocalChannelData$2 = widgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2;
        this.$channel = channel;
        this.$permissions = l;
        this.$guild$inlined = guildChannelsInfo;
        this.$forceViewCategories$inlined = hashSet;
        this.$isGuildHub$inlined = z2;
        this.$selectedChannel$inlined = channel2;
        this.$mentionCounts$inlined = map;
        this.$unreadChannelIds$inlined = set;
        this.$selectedGuildId$inlined = j;
        this.$areAnyChildThreadsSelected$5$inlined = widgetChannelListModel$Companion$guildListBuilder$5;
        this.$collapsedCategories$inlined = set2;
        this.$isThreadUnread$3$inlined = widgetChannelListModel$Companion$guildListBuilder$3;
        this.$hiddenChannelsIds$inlined = hashSet2;
        this.$areAllChildThreadsRead$4$inlined = widgetChannelListModel$Companion$guildListBuilder$4;
        this.$selectedVoiceChannelId$inlined = j2;
        this.$voiceStates$inlined = map2;
        this.$canSeeGuildRoleSubscriptions$inlined = z3;
        this.$channelsWithActiveThreads$inlined = set3;
        this.$guildScheduledEvents$inlined = list;
        this.$stageInstances$inlined = map3;
        this.$stageChannels$inlined = map4;
        this.$threadParentMap$inlined = map5;
        this.$joinedThreads$inlined = map6;
        this.$items$inlined = arrayList;
        this.$tryRemoveEmptyCategory$2$inlined = widgetChannelListModel$Companion$guildListBuilder$2;
        this.$messageAcks$inlined = map7;
        this.$directories$inlined = map8;
        this.$directoryEvents$inlined = map9;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0043 A[SYNTHETIC] */
    @Override // kotlin.jvm.functions.Function0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.channels.list.items.ChannelListItemVoiceChannel invoke() {
        /*
            r21 = this;
            r0 = r21
            com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2 r1 = r0.$getVocalChannelData$2
            com.discord.api.channel.Channel r2 = r0.$channel
            com.discord.widgets.channels.list.WidgetChannelListModel$Companion$VocalChannelData r1 = r1.invoke(r2)
            r2 = 0
            if (r1 == 0) goto Lc2
            java.util.List r3 = r0.$guildScheduledEvents$inlined
            java.util.Iterator r3 = r3.iterator()
        L13:
            boolean r4 = r3.hasNext()
            r5 = 1
            if (r4 == 0) goto L44
            java.lang.Object r4 = r3.next()
            r6 = r4
            com.discord.api.guildscheduledevent.GuildScheduledEvent r6 = (com.discord.api.guildscheduledevent.GuildScheduledEvent) r6
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r7 = r6.m()
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r8 = com.discord.api.guildscheduledevent.GuildScheduledEventStatus.ACTIVE
            if (r7 != r8) goto L40
            java.lang.Long r6 = r6.b()
            com.discord.api.channel.Channel r7 = r0.$channel
            long r7 = r7.h()
            if (r6 != 0) goto L36
            goto L40
        L36:
            long r9 = r6.longValue()
            int r6 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r6 != 0) goto L40
            r6 = 1
            goto L41
        L40:
            r6 = 0
        L41:
            if (r6 == 0) goto L13
            r2 = r4
        L44:
            r20 = r2
            com.discord.api.guildscheduledevent.GuildScheduledEvent r20 = (com.discord.api.guildscheduledevent.GuildScheduledEvent) r20
            java.util.Map r2 = r0.$voiceStates$inlined
            com.discord.api.channel.Channel r3 = r0.$channel
            java.lang.Object r2 = b.d.b.a.a.c(r3, r2)
            java.util.Collection r2 = (java.util.Collection) r2
            if (r2 == 0) goto L7e
            boolean r3 = r2.isEmpty()
            if (r3 == 0) goto L5b
            goto L77
        L5b:
            java.util.Iterator r2 = r2.iterator()
        L5f:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L77
            java.lang.Object r3 = r2.next()
            com.discord.widgets.channels.list.items.ChannelListItemVoiceUser r3 = (com.discord.widgets.channels.list.items.ChannelListItemVoiceUser) r3
            com.discord.api.voice.state.VoiceState r3 = r3.getVoiceState()
            boolean r3 = r3.j()
            if (r3 == 0) goto L5f
            r2 = 1
            goto L78
        L77:
            r2 = 0
        L78:
            if (r2 != r5) goto L7e
            r2 = 1
            r16 = 1
            goto L81
        L7e:
            r2 = 0
            r16 = 0
        L81:
            com.discord.widgets.channels.list.items.ChannelListItemVoiceChannel r2 = new com.discord.widgets.channels.list.items.ChannelListItemVoiceChannel
            com.discord.api.channel.Channel r7 = r0.$channel
            boolean r8 = r1.getTextChannelSelected()
            boolean r9 = r1.getVoiceChannelSelected()
            java.lang.Long r10 = r0.$permissions
            int r11 = r1.getMentionCount()
            boolean r12 = r1.getUnread()
            int r13 = r1.getNumUsersConnected()
            boolean r14 = r1.getLocked()
            boolean r15 = r1.getNsfw()
            com.discord.utilities.channel.GuildChannelsInfo r3 = r0.$guild$inlined
            com.discord.models.guild.Guild r3 = r3.getGuild()
            if (r3 == 0) goto Lb2
            com.discord.api.guild.GuildMaxVideoChannelUsers r3 = r3.getMaxVideoChannelUsers()
            if (r3 == 0) goto Lb2
            goto Lb4
        Lb2:
            com.discord.api.guild.GuildMaxVideoChannelUsers$Unlimited r3 = com.discord.api.guild.GuildMaxVideoChannelUsers.Unlimited.INSTANCE
        Lb4:
            r17 = r3
            boolean r18 = r1.isGuildRoleSubscriptionLockedChannel()
            boolean r19 = r1.isGuildRoleSubscriptionChannel()
            r6 = r2
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
        Lc2:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$5.invoke():com.discord.widgets.channels.list.items.ChannelListItemVoiceChannel");
    }
}
