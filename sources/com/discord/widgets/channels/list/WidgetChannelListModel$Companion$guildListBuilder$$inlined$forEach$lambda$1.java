package com.discord.widgets.channels.list;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreStream;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.widgets.channels.list.WidgetChannelListModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function3;
/* compiled from: WidgetChannelListModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\t\u001a\u00020\u00052\u0006\u0010\u0001\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0002H\n¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/api/channel/Channel;", "textChannel", "", "isMuted", "isParentMuted", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;", "invoke", "(Lcom/discord/api/channel/Channel;ZLjava/lang/Boolean;)Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;", "com/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$6$1", "getTextLikeChannelData"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1 extends o implements Function3<Channel, Boolean, Boolean, WidgetChannelListModel.Companion.TextLikeChannelData> {
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$4 $areAllChildThreadsRead$4$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$5 $areAnyChildThreadsSelected$5$inlined;
    public final /* synthetic */ boolean $canSeeGuildRoleSubscriptions$inlined;
    public final /* synthetic */ Set $channelsWithActiveThreads$inlined;
    public final /* synthetic */ Set $collapsedCategories$inlined;
    public final /* synthetic */ Map $directories$inlined;
    public final /* synthetic */ Map $directoryEvents$inlined;
    public final /* synthetic */ HashSet $forceViewCategories$inlined;
    public final /* synthetic */ GuildChannelsInfo $guild$inlined;
    public final /* synthetic */ List $guildScheduledEvents$inlined;
    public final /* synthetic */ HashSet $hiddenChannelsIds$inlined;
    public final /* synthetic */ boolean $isCategoryMuted;
    public final /* synthetic */ boolean $isGuildHub$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$3 $isThreadUnread$3$inlined;
    public final /* synthetic */ ArrayList $items$inlined;
    public final /* synthetic */ Map $joinedThreads$inlined;
    public final /* synthetic */ Map $mentionCounts$inlined;
    public final /* synthetic */ Map $messageAcks$inlined;
    public final /* synthetic */ Channel $selectedChannel$inlined;
    public final /* synthetic */ long $selectedGuildId$inlined;
    public final /* synthetic */ long $selectedVoiceChannelId$inlined;
    public final /* synthetic */ Map $stageChannels$inlined;
    public final /* synthetic */ Map $stageInstances$inlined;
    public final /* synthetic */ Map $threadParentMap$inlined;
    public final /* synthetic */ WidgetChannelListModel$Companion$guildListBuilder$2 $tryRemoveEmptyCategory$2$inlined;
    public final /* synthetic */ Set $unreadChannelIds$inlined;
    public final /* synthetic */ Map $voiceStates$inlined;

    /* compiled from: WidgetChannelListModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()Z", "com/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$6$1$1", "hideThread"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Boolean> {
        public final /* synthetic */ Boolean $isParentMuted;
        public final /* synthetic */ Channel $textChannel;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Channel channel, Boolean bool) {
            super(0);
            this.$textChannel = channel;
            this.$isParentMuted = bool;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final Boolean invoke2() {
            Channel channel = StoreStream.Companion.getChannels().getChannel(this.$textChannel.r());
            if (channel == null) {
                return 1;
            }
            long h = channel.h();
            Channel channel2 = WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$selectedChannel$inlined;
            boolean z2 = (channel2 != null && (h > channel2.h() ? 1 : (h == channel2.h() ? 0 : -1)) == 0) || WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$areAnyChildThreadsSelected$5$inlined.invoke(channel.h());
            boolean contains = WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$collapsedCategories$inlined.contains(Long.valueOf(channel.r()));
            return (WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$hiddenChannelsIds$inlined.contains(Long.valueOf(this.$textChannel.r())) || (!z2 && !WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$isThreadUnread$3$inlined.invoke2(this.$textChannel) && (contains || m.areEqual(this.$isParentMuted, Boolean.TRUE)))) ? 1 : null;
        }
    }

    /* compiled from: WidgetChannelListModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()Z", "com/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$6$1$2", "hideChannel"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function0<Boolean> {
        public final /* synthetic */ boolean $channelSelected;
        public final /* synthetic */ boolean $isMuted;
        public final /* synthetic */ int $mentionCount;
        public final /* synthetic */ Channel $textChannel;
        public final /* synthetic */ long $textChannelId;
        public final /* synthetic */ boolean $unread;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(int i, Channel channel, boolean z2, boolean z3, boolean z4, long j) {
            super(0);
            this.$mentionCount = i;
            this.$textChannel = channel;
            this.$channelSelected = z2;
            this.$isMuted = z3;
            this.$unread = z4;
            this.$textChannelId = j;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r1v0 */
        /* JADX WARN: Type inference failed for: r1v2 */
        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final Boolean invoke2() {
            Boolean bool = 1;
            boolean z2 = this.$mentionCount <= 0;
            boolean contains = WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$collapsedCategories$inlined.contains(Long.valueOf(this.$textChannel.r()));
            boolean z3 = this.$channelSelected || WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$areAnyChildThreadsSelected$5$inlined.invoke(this.$textChannel.h());
            boolean invoke = WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$areAllChildThreadsRead$4$inlined.invoke(this.$textChannel.h());
            boolean z4 = (contains && z2 && (WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$isCategoryMuted || this.$isMuted || !this.$unread)) || (this.$isMuted && WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$guild$inlined.getHideMutedChannels());
            if (z3 || !invoke || !z4) {
                bool = 0;
            }
            if (bool != 0) {
                WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.this.$hiddenChannelsIds$inlined.add(Long.valueOf(this.$textChannelId));
            }
            return bool;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1(boolean z2, GuildChannelsInfo guildChannelsInfo, HashSet hashSet, boolean z3, Channel channel, Map map, Set set, long j, WidgetChannelListModel$Companion$guildListBuilder$5 widgetChannelListModel$Companion$guildListBuilder$5, Set set2, WidgetChannelListModel$Companion$guildListBuilder$3 widgetChannelListModel$Companion$guildListBuilder$3, HashSet hashSet2, WidgetChannelListModel$Companion$guildListBuilder$4 widgetChannelListModel$Companion$guildListBuilder$4, long j2, Map map2, boolean z4, Set set3, List list, Map map3, Map map4, Map map5, Map map6, ArrayList arrayList, WidgetChannelListModel$Companion$guildListBuilder$2 widgetChannelListModel$Companion$guildListBuilder$2, Map map7, Map map8, Map map9) {
        super(3);
        this.$isCategoryMuted = z2;
        this.$guild$inlined = guildChannelsInfo;
        this.$forceViewCategories$inlined = hashSet;
        this.$isGuildHub$inlined = z3;
        this.$selectedChannel$inlined = channel;
        this.$mentionCounts$inlined = map;
        this.$unreadChannelIds$inlined = set;
        this.$selectedGuildId$inlined = j;
        this.$areAnyChildThreadsSelected$5$inlined = widgetChannelListModel$Companion$guildListBuilder$5;
        this.$collapsedCategories$inlined = set2;
        this.$isThreadUnread$3$inlined = widgetChannelListModel$Companion$guildListBuilder$3;
        this.$hiddenChannelsIds$inlined = hashSet2;
        this.$areAllChildThreadsRead$4$inlined = widgetChannelListModel$Companion$guildListBuilder$4;
        this.$selectedVoiceChannelId$inlined = j2;
        this.$voiceStates$inlined = map2;
        this.$canSeeGuildRoleSubscriptions$inlined = z4;
        this.$channelsWithActiveThreads$inlined = set3;
        this.$guildScheduledEvents$inlined = list;
        this.$stageInstances$inlined = map3;
        this.$stageChannels$inlined = map4;
        this.$threadParentMap$inlined = map5;
        this.$joinedThreads$inlined = map6;
        this.$items$inlined = arrayList;
        this.$tryRemoveEmptyCategory$2$inlined = widgetChannelListModel$Companion$guildListBuilder$2;
        this.$messageAcks$inlined = map7;
        this.$directories$inlined = map8;
        this.$directoryEvents$inlined = map9;
    }

    public static /* synthetic */ WidgetChannelListModel.Companion.TextLikeChannelData invoke$default(WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1 widgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1, Channel channel, boolean z2, Boolean bool, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        if ((i & 4) != 0) {
            bool = null;
        }
        return widgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.invoke(channel, z2, bool);
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ WidgetChannelListModel.Companion.TextLikeChannelData invoke(Channel channel, Boolean bool, Boolean bool2) {
        return invoke(channel, bool.booleanValue(), bool2);
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0096  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.channels.list.WidgetChannelListModel.Companion.TextLikeChannelData invoke(com.discord.api.channel.Channel r18, boolean r19, java.lang.Boolean r20) {
        /*
            r17 = this;
            r9 = r17
            r10 = r18
            java.lang.String r0 = "textChannel"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            long r7 = r18.h()
            com.discord.api.channel.Channel r0 = r9.$selectedChannel$inlined
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L1d
            long r3 = r0.h()
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 != 0) goto L1d
            r11 = 1
            goto L1e
        L1d:
            r11 = 0
        L1e:
            java.util.Map r0 = r9.$mentionCounts$inlined
            java.lang.Long r3 = java.lang.Long.valueOf(r7)
            java.lang.Object r0 = r0.get(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L32
            int r0 = r0.intValue()
            r12 = r0
            goto L33
        L32:
            r12 = 0
        L33:
            java.util.Set r0 = r9.$unreadChannelIds$inlined
            java.lang.Long r3 = java.lang.Long.valueOf(r7)
            boolean r13 = r0.contains(r3)
            java.util.List r0 = r18.s()
            if (r0 == 0) goto L73
            java.util.Iterator r0 = r0.iterator()
        L47:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L64
            java.lang.Object r3 = r0.next()
            r4 = r3
            com.discord.api.permission.PermissionOverwrite r4 = (com.discord.api.permission.PermissionOverwrite) r4
            long r4 = r4.e()
            long r14 = r9.$selectedGuildId$inlined
            int r6 = (r4 > r14 ? 1 : (r4 == r14 ? 0 : -1))
            if (r6 != 0) goto L60
            r4 = 1
            goto L61
        L60:
            r4 = 0
        L61:
            if (r4 == 0) goto L47
            goto L65
        L64:
            r3 = 0
        L65:
            com.discord.api.permission.PermissionOverwrite r3 = (com.discord.api.permission.PermissionOverwrite) r3
            if (r3 == 0) goto L73
            r4 = 1024(0x400, double:5.06E-321)
            boolean r0 = com.discord.utilities.PermissionOverwriteUtilsKt.denies(r3, r4)
            if (r0 != r1) goto L73
            r14 = 1
            goto L74
        L73:
            r14 = 0
        L74:
            com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1$1 r15 = new com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1$1
            r0 = r20
            r15.<init>(r10, r0)
            com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1$2 r16 = new com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1$2
            r0 = r16
            r1 = r17
            r2 = r12
            r3 = r18
            r4 = r11
            r5 = r19
            r6 = r13
            r0.<init>(r2, r3, r4, r5, r6, r7)
            boolean r0 = com.discord.api.channel.ChannelUtils.C(r18)
            if (r0 == 0) goto L96
            boolean r0 = r15.invoke2()
            goto L9a
        L96:
            boolean r0 = r16.invoke2()
        L9a:
            r15 = r0
            com.discord.widgets.channels.list.WidgetChannelListModel$Companion$TextLikeChannelData r0 = new com.discord.widgets.channels.list.WidgetChannelListModel$Companion$TextLikeChannelData
            r10 = r0
            r10.<init>(r11, r12, r13, r14, r15)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.list.WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1.invoke(com.discord.api.channel.Channel, boolean, java.lang.Boolean):com.discord.widgets.channels.list.WidgetChannelListModel$Companion$TextLikeChannelData");
    }
}
