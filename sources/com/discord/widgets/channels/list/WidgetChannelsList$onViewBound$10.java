package com.discord.widgets.channels.list;

import android.content.Context;
import android.content.Intent;
import androidx.activity.result.ActivityResultLauncher;
import b.a.d.j;
import com.discord.widgets.hubs.WidgetHubAddServer;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsList$onViewBound$10 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetChannelsList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsList$onViewBound$10(WidgetChannelsList widgetChannelsList) {
        super(0);
        this.this$0 = widgetChannelsList;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ActivityResultLauncher<Intent> activityResultLauncher;
        j jVar = j.g;
        Context requireContext = this.this$0.requireContext();
        activityResultLauncher = this.this$0.activityResult;
        jVar.f(requireContext, activityResultLauncher, WidgetHubAddServer.class, null);
    }
}
