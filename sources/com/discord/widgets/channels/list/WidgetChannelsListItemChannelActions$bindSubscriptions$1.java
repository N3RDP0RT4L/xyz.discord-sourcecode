package com.discord.widgets.channels.list;

import com.discord.widgets.channels.list.WidgetChannelsListItemChannelActions;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelsListItemChannelActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListItemChannelActions$Model;", "it", "", "invoke", "(Lcom/discord/widgets/channels/list/WidgetChannelsListItemChannelActions$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsListItemChannelActions$bindSubscriptions$1 extends o implements Function1<WidgetChannelsListItemChannelActions.Model, Unit> {
    public final /* synthetic */ WidgetChannelsListItemChannelActions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsListItemChannelActions$bindSubscriptions$1(WidgetChannelsListItemChannelActions widgetChannelsListItemChannelActions) {
        super(1);
        this.this$0 = widgetChannelsListItemChannelActions;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetChannelsListItemChannelActions.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetChannelsListItemChannelActions.Model model) {
        this.this$0.configureUI(model);
    }
}
