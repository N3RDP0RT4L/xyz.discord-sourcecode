package com.discord.widgets.channels.list;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.VoiceState;
import com.discord.databinding.WidgetChannelsListItemActiveEventBinding;
import com.discord.databinding.WidgetChannelsListItemAudienceCountBinding;
import com.discord.databinding.WidgetChannelsListItemCategoryBinding;
import com.discord.databinding.WidgetChannelsListItemChannelBinding;
import com.discord.databinding.WidgetChannelsListItemChannelPrivateBinding;
import com.discord.databinding.WidgetChannelsListItemChannelStageVoiceBinding;
import com.discord.databinding.WidgetChannelsListItemChannelVoiceBinding;
import com.discord.databinding.WidgetChannelsListItemDirectoryBinding;
import com.discord.databinding.WidgetChannelsListItemGuildJoinRequestBinding;
import com.discord.databinding.WidgetChannelsListItemGuildRoleSubsBinding;
import com.discord.databinding.WidgetChannelsListItemGuildScheduledEventsBinding;
import com.discord.databinding.WidgetChannelsListItemHeaderBinding;
import com.discord.databinding.WidgetChannelsListItemMfaBinding;
import com.discord.databinding.WidgetChannelsListItemThreadBinding;
import com.discord.databinding.WidgetChannelsListItemVoiceUserBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.guilds.MemberVerificationUtils;
import com.discord.utilities.guilds.MemberVerificationUtils$maybeShowVerificationGate$1;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.views.StatusView;
import com.discord.views.VoiceUserLimitView;
import com.discord.views.user.UserSummaryView;
import com.discord.widgets.channels.WidgetCreateChannel;
import com.discord.widgets.channels.list.WidgetChannelListModel;
import com.discord.widgets.channels.list.WidgetChannelsListAdapter;
import com.discord.widgets.channels.list.items.ChannelListBottomNavSpaceItem;
import com.discord.widgets.channels.list.items.ChannelListItem;
import com.discord.widgets.channels.list.items.ChannelListItemActiveEvent;
import com.discord.widgets.channels.list.items.ChannelListItemAddServer;
import com.discord.widgets.channels.list.items.ChannelListItemCategory;
import com.discord.widgets.channels.list.items.ChannelListItemDirectory;
import com.discord.widgets.channels.list.items.ChannelListItemGuildJoinRequest;
import com.discord.widgets.channels.list.items.ChannelListItemGuildScheduledEvents;
import com.discord.widgets.channels.list.items.ChannelListItemHeader;
import com.discord.widgets.channels.list.items.ChannelListItemInvite;
import com.discord.widgets.channels.list.items.ChannelListItemPrivate;
import com.discord.widgets.channels.list.items.ChannelListItemStageAudienceCount;
import com.discord.widgets.channels.list.items.ChannelListItemStageVoiceChannel;
import com.discord.widgets.channels.list.items.ChannelListItemTextChannel;
import com.discord.widgets.channels.list.items.ChannelListItemThread;
import com.discord.widgets.channels.list.items.ChannelListItemVoiceChannel;
import com.discord.widgets.channels.list.items.ChannelListItemVoiceUser;
import com.discord.widgets.channels.list.items.ChannelListVocalItem;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShare;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventLocationInfo;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetChannelsListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0017\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0013NOPQRSTUVWXYZ[\\]^_`B\u0017\u0012\u0006\u0010K\u001a\u00020J\u0012\u0006\u0010@\u001a\u00020?¢\u0006\u0004\bL\u0010MJ)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u0005¢\u0006\u0004\b\f\u0010\rR4\u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u000b0\u000e8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R.\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u000b0\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR(\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000b0 8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R.\u0010(\u001a\u000e\u0012\u0004\u0012\u00020'\u0012\u0004\u0012\u00020\u000b0\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b(\u0010\u001b\u001a\u0004\b)\u0010\u001d\"\u0004\b*\u0010\u001fR&\u0010-\u001a\u00060+j\u0002`,8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b-\u0010.\u001a\u0004\b/\u00100\"\u0004\b1\u00102R0\u00103\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\u000b0\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b3\u0010\u001b\u001a\u0004\b4\u0010\u001d\"\u0004\b5\u0010\u001fR.\u00106\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u000b0\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b6\u0010\u001b\u001a\u0004\b7\u0010\u001d\"\u0004\b8\u0010\u001fR0\u00109\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\u000b0\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b9\u0010\u001b\u001a\u0004\b:\u0010\u001d\"\u0004\b;\u0010\u001fR(\u0010<\u001a\b\u0012\u0004\u0012\u00020\u000b0 8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b<\u0010\"\u001a\u0004\b=\u0010$\"\u0004\b>\u0010&R\u0016\u0010@\u001a\u00020?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010AR.\u0010C\u001a\u000e\u0012\u0004\u0012\u00020B\u0012\u0004\u0012\u00020\u000b0\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bC\u0010\u001b\u001a\u0004\bD\u0010\u001d\"\u0004\bE\u0010\u001fR4\u0010G\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020F\u0012\u0004\u0012\u00020\u000b0\u000e8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bG\u0010\u0012\u001a\u0004\bH\u0010\u0014\"\u0004\bI\u0010\u0016¨\u0006a"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "height", "", "handleBottomNavHeight", "(I)V", "Lkotlin/Function2;", "Lcom/discord/models/user/User;", "Lcom/discord/api/channel/Channel;", "onSelectUserOptions", "Lkotlin/jvm/functions/Function2;", "getOnSelectUserOptions", "()Lkotlin/jvm/functions/Function2;", "setOnSelectUserOptions", "(Lkotlin/jvm/functions/Function2;)V", "bottomNavHeight", "I", "Lkotlin/Function1;", "onSelectChannelOptions", "Lkotlin/jvm/functions/Function1;", "getOnSelectChannelOptions", "()Lkotlin/jvm/functions/Function1;", "setOnSelectChannelOptions", "(Lkotlin/jvm/functions/Function1;)V", "Lkotlin/Function0;", "onViewGuildRoleSubscriptions", "Lkotlin/jvm/functions/Function0;", "getOnViewGuildRoleSubscriptions", "()Lkotlin/jvm/functions/Function0;", "setOnViewGuildRoleSubscriptions", "(Lkotlin/jvm/functions/Function0;)V", "Landroid/view/View;", "onSelectInvite", "getOnSelectInvite", "setOnSelectInvite", "", "Lcom/discord/primitives/GuildId;", "selectedGuildId", "J", "getSelectedGuildId", "()J", "setSelectedGuildId", "(J)V", "onSelectGuildRoleSubscriptionLockedChannel", "getOnSelectGuildRoleSubscriptionLockedChannel", "setOnSelectGuildRoleSubscriptionLockedChannel", "onCallChannel", "getOnCallChannel", "setOnCallChannel", "onSelectChannel", "getOnSelectChannel", "setOnSelectChannel", "onAddServer", "getOnAddServer", "setOnAddServer", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Lcom/discord/models/guild/Guild;", "onViewGuildScheduledEvents", "getOnViewGuildScheduledEvents", "setOnViewGuildScheduledEvents", "", "onCollapseCategory", "getOnCollapseCategory", "setOnCollapseCategory", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Landroidx/fragment/app/FragmentManager;)V", "Item", "ItemChannelAddServer", "ItemChannelCategory", "ItemChannelDirectory", "ItemChannelPrivate", "ItemChannelStageVoice", "ItemChannelText", "ItemChannelThread", "ItemChannelVoice", "ItemGuildJoinRequest", "ItemGuildRoleSubscriptionsOverview", "ItemGuildScheduledEvents", "ItemHeader", "ItemInvite", "ItemMFA", "ItemSpace", "ItemStageActiveEvent", "ItemStageChannelAudienceCount", "ItemVoiceUser", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsListAdapter extends MGRecyclerAdapterSimple<ChannelListItem> {
    private int bottomNavHeight;
    private final FragmentManager fragmentManager;
    private long selectedGuildId;
    private Function1<? super Channel, Unit> onSelectChannel = WidgetChannelsListAdapter$onSelectChannel$1.INSTANCE;
    private Function1<? super Channel, Unit> onSelectChannelOptions = WidgetChannelsListAdapter$onSelectChannelOptions$1.INSTANCE;
    private Function2<? super User, ? super Channel, Unit> onSelectUserOptions = WidgetChannelsListAdapter$onSelectUserOptions$1.INSTANCE;
    private Function1<? super View, Unit> onSelectInvite = WidgetChannelsListAdapter$onSelectInvite$1.INSTANCE;
    private Function1<? super Channel, Unit> onSelectGuildRoleSubscriptionLockedChannel = WidgetChannelsListAdapter$onSelectGuildRoleSubscriptionLockedChannel$1.INSTANCE;
    private Function1<? super Channel, Unit> onCallChannel = WidgetChannelsListAdapter$onCallChannel$1.INSTANCE;
    private Function1<? super Guild, Unit> onViewGuildScheduledEvents = WidgetChannelsListAdapter$onViewGuildScheduledEvents$1.INSTANCE;
    private Function0<Unit> onViewGuildRoleSubscriptions = WidgetChannelsListAdapter$onViewGuildRoleSubscriptions$1.INSTANCE;
    private Function2<? super Channel, ? super Boolean, Unit> onCollapseCategory = WidgetChannelsListAdapter$onCollapseCategory$1.INSTANCE;
    private Function0<Unit> onAddServer = WidgetChannelsListAdapter$onAddServer$1.INSTANCE;

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b&\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J#\u0010\n\u001a\u00020\t*\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0004¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "Landroid/view/View;", "", "selected", "Lcom/discord/api/channel/Channel;", "channel", "", "setBackground", "(Landroid/view/View;ZLcom/discord/api/channel/Channel;)V", "", "layoutRes", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item extends MGRecyclerViewHolder<WidgetChannelsListAdapter, ChannelListItem> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Item(@LayoutRes int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
        }

        public final void setBackground(View view, boolean z2, Channel channel) {
            int i;
            m.checkNotNullParameter(view, "$this$setBackground");
            m.checkNotNullParameter(channel, "channel");
            if (!z2 || ChannelUtils.t(channel)) {
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "context");
                i = DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.drawable_overlay_channels, 0, 2, (Object) null);
            } else {
                Context context2 = view.getContext();
                m.checkNotNullExpressionValue(context2, "context");
                i = DrawableCompat.getThemedDrawableRes$default(context2, (int) R.attr.drawable_overlay_channels_selected, 0, 2, (Object) null);
            }
            ViewExtensions.setBackgroundAndKeepPadding(view, ContextCompat.getDrawable(view.getContext(), i));
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelAddServer;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemDirectoryBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemDirectoryBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelAddServer extends Item {
        private final WidgetChannelsListItemDirectoryBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelAddServer(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            WidgetChannelsListItemDirectoryBinding a = WidgetChannelsListItemDirectoryBinding.a(this.itemView);
            m.checkNotNullExpressionValue(a, "WidgetChannelsListItemDi…oryBinding.bind(itemView)");
            this.binding = a;
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelAddServer itemChannelAddServer) {
            return (WidgetChannelsListAdapter) itemChannelAddServer.adapter;
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            if (channelListItem instanceof ChannelListItemAddServer) {
                this.binding.c.setText(R.string.hub_sidebar_add_servers);
                this.binding.f2274b.setImageResource(R.drawable.ic_add_24dp);
                this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelAddServer$onConfigure$$inlined$apply$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetChannelsListAdapter.ItemChannelAddServer.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelAddServer.this).getOnAddServer().invoke();
                    }
                });
            }
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0017\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelCategory;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemCategoryBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemCategoryBinding;", "", "isCollapsed", "Z", "layouResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelCategory extends Item {
        private static final long ANIMATION_DURATION = 300;
        private static final RotateAnimation ARROW_ANIM_COLLAPSE;
        private static final RotateAnimation ARROW_ANIM_EXPAND;
        public static final Companion Companion;
        private final WidgetChannelsListItemCategoryBinding binding;
        private boolean isCollapsed;

        /* compiled from: WidgetChannelsListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u001b\u0010\u0006\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\b\u001a\u00020\u0005*\u00020\u0002H\u0003¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0013¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelCategory$Companion;", "", "Lcom/discord/widgets/channels/list/items/ChannelListItemCategory;", "Landroid/content/Context;", "context", "", "getChannelColor", "(Lcom/discord/widgets/channels/list/items/ChannelListItemCategory;Landroid/content/Context;)I", "getArrowDrawable", "(Lcom/discord/widgets/channels/list/items/ChannelListItemCategory;)I", "", "expand", "Landroid/view/animation/RotateAnimation;", "getAnimation", "(Z)Landroid/view/animation/RotateAnimation;", "", "ANIMATION_DURATION", "J", "ARROW_ANIM_COLLAPSE", "Landroid/view/animation/RotateAnimation;", "ARROW_ANIM_EXPAND", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final RotateAnimation getAnimation(boolean z2) {
                RotateAnimation rotateAnimation = new RotateAnimation(z2 ? -90.0f : 90.0f, 0.0f, 1, 0.5f, 1, 0.5f);
                rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                rotateAnimation.setDuration(300L);
                return rotateAnimation;
            }

            /* JADX INFO: Access modifiers changed from: private */
            @DrawableRes
            public final int getArrowDrawable(ChannelListItemCategory channelListItemCategory) {
                if (channelListItemCategory.isCollapsed()) {
                    return R.drawable.ic_chevron_right_grey_12dp;
                }
                if (!channelListItemCategory.isCollapsed()) {
                    return R.drawable.ic_chevron_down_grey_12dp;
                }
                return 0;
            }

            /* JADX INFO: Access modifiers changed from: private */
            @ColorInt
            public final int getChannelColor(ChannelListItemCategory channelListItemCategory, Context context) {
                return channelListItemCategory.isMuted() ? ColorCompat.getThemedColor(context, (int) R.attr.colorInteractiveMuted) : ColorCompat.getThemedColor(context, (int) R.attr.colorChannelDefault);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        static {
            Companion companion = new Companion(null);
            Companion = companion;
            ARROW_ANIM_EXPAND = companion.getAnimation(true);
            ARROW_ANIM_COLLAPSE = companion.getAnimation(false);
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelCategory(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_item_category_add;
            ImageView imageView = (ImageView) view.findViewById(R.id.channels_item_category_add);
            if (imageView != null) {
                i2 = R.id.channels_item_category_arrow;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.channels_item_category_arrow);
                if (imageView2 != null) {
                    i2 = R.id.channels_item_category_name;
                    TextView textView = (TextView) view.findViewById(R.id.channels_item_category_name);
                    if (textView != null) {
                        WidgetChannelsListItemCategoryBinding widgetChannelsListItemCategoryBinding = new WidgetChannelsListItemCategoryBinding((LinearLayout) view, imageView, imageView2, textView);
                        m.checkNotNullExpressionValue(widgetChannelsListItemCategoryBinding, "WidgetChannelsListItemCa…oryBinding.bind(itemView)");
                        this.binding = widgetChannelsListItemCategoryBinding;
                        return;
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelCategory itemChannelCategory) {
            return (WidgetChannelsListAdapter) itemChannelCategory.adapter;
        }

        public void onConfigure(int i, final ChannelListItem channelListItem) {
            CharSequence d;
            CharSequence d2;
            RotateAnimation rotateAnimation;
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            final ChannelListItemCategory channelListItemCategory = (ChannelListItemCategory) channelListItem;
            LinearLayout linearLayout = this.binding.a;
            m.checkNotNullExpressionValue(linearLayout, "binding.root");
            ViewExtensions.setOnLongClickListenerConsumeClick(linearLayout, new WidgetChannelsListAdapter$ItemChannelCategory$onConfigure$1(this, channelListItem));
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelCategory$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChannelsListAdapter.ItemChannelCategory.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelCategory.this).getOnCollapseCategory().invoke(((ChannelListItemCategory) channelListItem).getChannel(), Boolean.valueOf(((ChannelListItemCategory) channelListItem).isCollapsed()));
                }
            });
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.channelsItemCategoryName");
            textView.setText(ChannelUtils.c(channelListItemCategory.getChannel()));
            TextView textView2 = this.binding.d;
            m.checkNotNullExpressionValue(textView2, "binding.channelsItemCategoryName");
            TextView textView3 = this.binding.d;
            m.checkNotNullExpressionValue(textView3, "binding.channelsItemCategoryName");
            Object[] objArr = new Object[2];
            int i2 = 0;
            objArr[0] = ChannelUtils.c(channelListItemCategory.getChannel());
            TextView textView4 = this.binding.d;
            m.checkNotNullExpressionValue(textView4, "binding.channelsItemCategoryName");
            d = b.d(textView4, channelListItemCategory.isCollapsed() ? R.string.collapsed : R.string.expanded, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            objArr[1] = d;
            d2 = b.d(textView3, R.string.category_a11y_label_with_expanded_state, objArr, (r4 & 4) != 0 ? b.c.j : null);
            textView2.setContentDescription(d2);
            Companion companion = Companion;
            TextView textView5 = this.binding.d;
            m.checkNotNullExpressionValue(textView5, "binding.channelsItemCategoryName");
            Context context = textView5.getContext();
            m.checkNotNullExpressionValue(context, "binding.channelsItemCategoryName.context");
            int channelColor = companion.getChannelColor(channelListItemCategory, context);
            this.binding.d.setTextColor(channelColor);
            ViewCompat.setAccessibilityDelegate(this.binding.d, new AccessibilityDelegateCompat() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelCategory$onConfigure$3
                @Override // androidx.core.view.AccessibilityDelegateCompat
                public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
                    m.checkNotNullParameter(view, "host");
                    m.checkNotNullParameter(accessibilityNodeInfoCompat, "info");
                    super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
                    accessibilityNodeInfoCompat.setHeading(true);
                }
            });
            this.binding.c.setImageResource(companion.getArrowDrawable(channelListItemCategory));
            ImageView imageView = this.binding.c;
            m.checkNotNullExpressionValue(imageView, "binding.channelsItemCategoryArrow");
            imageView.setImageTintList(ColorStateList.valueOf(channelColor));
            if (this.isCollapsed != channelListItemCategory.isCollapsed()) {
                this.isCollapsed = channelListItemCategory.isCollapsed();
                ImageView imageView2 = this.binding.c;
                if (channelListItemCategory.isCollapsed()) {
                    rotateAnimation = ARROW_ANIM_COLLAPSE;
                } else {
                    rotateAnimation = ARROW_ANIM_EXPAND;
                }
                imageView2.startAnimation(rotateAnimation);
            }
            ImageView imageView3 = this.binding.f2269b;
            m.checkNotNullExpressionValue(imageView3, "binding.channelsItemCategoryAdd");
            if (!channelListItemCategory.getCanManageChannels()) {
                i2 = 8;
            }
            imageView3.setVisibility(i2);
            ImageView imageView4 = this.binding.f2269b;
            m.checkNotNullExpressionValue(imageView4, "binding.channelsItemCategoryAdd");
            imageView4.setImageTintList(ColorStateList.valueOf(channelColor));
            this.binding.f2269b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelCategory$onConfigure$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetCreateChannel.Companion.show(a.x(WidgetChannelsListAdapter.ItemChannelCategory.this.itemView, "itemView", "itemView.context"), channelListItemCategory.getChannel().f(), 0, Long.valueOf(channelListItemCategory.getChannel().h()));
                }
            });
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelDirectory;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemDirectoryBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemDirectoryBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelDirectory extends Item {
        private final WidgetChannelsListItemDirectoryBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelDirectory(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            WidgetChannelsListItemDirectoryBinding a = WidgetChannelsListItemDirectoryBinding.a(this.itemView);
            m.checkNotNullExpressionValue(a, "WidgetChannelsListItemDi…oryBinding.bind(itemView)");
            this.binding = a;
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelDirectory itemChannelDirectory) {
            return (WidgetChannelsListAdapter) itemChannelDirectory.adapter;
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            final ChannelListItemDirectory channelListItemDirectory = (ChannelListItemDirectory) channelListItem;
            boolean u = ChannelUtils.u(channelListItemDirectory.getChannel());
            if (u) {
                TextView textView = this.binding.c;
                m.checkNotNullExpressionValue(textView, "binding.directoryChannelName");
                String m = channelListItemDirectory.getChannel().m();
                if (m == null) {
                    m = "";
                }
                textView.setText(m);
            } else {
                this.binding.c.setText(R.string.hub_sidebar_join_servers);
            }
            this.binding.f2274b.setImageResource(u ? R.drawable.ic_hub_24dp : R.drawable.ic_compass);
            TextView textView2 = this.binding.d;
            m.checkNotNullExpressionValue(textView2, "binding.directoryChannelUnreadCount");
            b.a(textView2, channelListItemDirectory.getUnreadCount() > 0 ? String.valueOf(channelListItemDirectory.getUnreadCount()) : null);
            LinearLayout linearLayout = this.binding.a;
            linearLayout.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelDirectory$onConfigure$$inlined$apply$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChannelsListAdapter.ItemChannelDirectory.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelDirectory.this).getOnSelectChannel().invoke(channelListItemDirectory.getChannel());
                }
            });
            linearLayout.setSelected(channelListItemDirectory.getSelected());
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u001e\u001a\u00020\b\u0012\u0006\u0010 \u001a\u00020\u001f¢\u0006\u0004\b!\u0010\"J!\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\u000f\u001a\u00020\u000e*\u00020\u000b2\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u001f\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u0016H\u0014¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001d¨\u0006#"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelPrivate;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "Lcom/discord/widgets/channels/list/items/ChannelListItemPrivate;", "channelListItemPrivate", "", "channelName", "getContentDescription", "(Lcom/discord/widgets/channels/list/items/ChannelListItemPrivate;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;", "", "getTextColor", "(Lcom/discord/widgets/channels/list/items/ChannelListItemPrivate;)I", "Lcom/discord/api/channel/Channel;", "Landroid/content/Context;", "context", "", "getMemberCount", "(Lcom/discord/api/channel/Channel;Landroid/content/Context;)Ljava/lang/String;", "", "selected", "getPresenceBg", "(Z)I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemChannelPrivateBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemChannelPrivateBinding;", "layouResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelPrivate extends Item {
        private final WidgetChannelsListItemChannelPrivateBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelPrivate(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channel_list_item_private_status;
            StatusView statusView = (StatusView) view.findViewById(R.id.channel_list_item_private_status);
            if (statusView != null) {
                i2 = R.id.channels_list_item_private_avatar;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.channels_list_item_private_avatar);
                if (simpleDraweeView != null) {
                    i2 = R.id.channels_list_item_private_desc;
                    SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) view.findViewById(R.id.channels_list_item_private_desc);
                    if (simpleDraweeSpanTextView != null) {
                        i2 = R.id.channels_list_item_private_mentions;
                        TextView textView = (TextView) view.findViewById(R.id.channels_list_item_private_mentions);
                        if (textView != null) {
                            i2 = R.id.channels_list_item_private_name;
                            TextView textView2 = (TextView) view.findViewById(R.id.channels_list_item_private_name);
                            if (textView2 != null) {
                                i2 = R.id.channels_list_item_private_tag;
                                TextView textView3 = (TextView) view.findViewById(R.id.channels_list_item_private_tag);
                                if (textView3 != null) {
                                    WidgetChannelsListItemChannelPrivateBinding widgetChannelsListItemChannelPrivateBinding = new WidgetChannelsListItemChannelPrivateBinding((RelativeLayout) view, statusView, simpleDraweeView, simpleDraweeSpanTextView, textView, textView2, textView3);
                                    m.checkNotNullExpressionValue(widgetChannelsListItemChannelPrivateBinding, "WidgetChannelsListItemCh…ateBinding.bind(itemView)");
                                    this.binding = widgetChannelsListItemChannelPrivateBinding;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelPrivate itemChannelPrivate) {
            return (WidgetChannelsListAdapter) itemChannelPrivate.adapter;
        }

        private final CharSequence getContentDescription(ChannelListItemPrivate channelListItemPrivate, CharSequence charSequence) {
            CharSequence d;
            CharSequence d2;
            CharSequence d3;
            CharSequence d4;
            if (ChannelUtils.p(channelListItemPrivate.getChannel()) && channelListItemPrivate.getMentionCount() > 0) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                d4 = b.d(view, R.string.group_message_a11y_label_with_unreads, new Object[]{charSequence}, (r4 & 4) != 0 ? b.c.j : null);
                return d4;
            } else if (ChannelUtils.p(channelListItemPrivate.getChannel())) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                d3 = b.d(view2, R.string.group_message_a11y_label, new Object[]{charSequence}, (r4 & 4) != 0 ? b.c.j : null);
                return d3;
            } else if (channelListItemPrivate.getMentionCount() > 0) {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                d2 = b.d(view3, R.string.direct_message_a11y_label_with_unreads, new Object[]{charSequence}, (r4 & 4) != 0 ? b.c.j : null);
                return d2;
            } else {
                View view4 = this.itemView;
                m.checkNotNullExpressionValue(view4, "itemView");
                d = b.d(view4, R.string.direct_message_a11y_label, new Object[]{charSequence}, (r4 & 4) != 0 ? b.c.j : null);
                return d;
            }
        }

        @SuppressLint({"SetTextI18n"})
        private final String getMemberCount(Channel channel, Context context) {
            CharSequence b2;
            List<com.discord.api.user.User> w = channel.w();
            int size = w != null ? w.size() : 0;
            b2 = b.b(context, R.string.members, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            StringBuilder sb = new StringBuilder();
            sb.append(size + 1);
            sb.append(' ');
            sb.append(b2);
            return sb.toString();
        }

        private final int getPresenceBg(boolean z2) {
            if (z2) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                return ColorCompat.getThemedColor(view, (int) R.attr.color_bg_private_channel_presence_selected);
            }
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            return ColorCompat.getThemedColor(view2, (int) R.attr.colorBackgroundSecondary);
        }

        @ColorInt
        private final int getTextColor(ChannelListItemPrivate channelListItemPrivate) {
            if (channelListItemPrivate.getSelected()) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                return ColorCompat.getThemedColor(view, (int) R.attr.colorInteractiveActive);
            } else if (channelListItemPrivate.getMuted()) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                return ColorCompat.getThemedColor(view2, (int) R.attr.colorInteractiveMuted);
            } else {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                return ColorCompat.getThemedColor(view3, (int) R.attr.colorChannelDefault);
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:24:0x015f  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x0161  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x0164  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x0166  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void onConfigure(int r19, final com.discord.widgets.channels.list.items.ChannelListItem r20) {
            /*
                Method dump skipped, instructions count: 384
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.list.WidgetChannelsListAdapter.ItemChannelPrivate.onConfigure(int, com.discord.widgets.channels.list.items.ChannelListItem):void");
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0017\u0012\u0006\u0010\u0011\u001a\u00020\u0004\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\nH\u0015¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelStageVoice;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "Lcom/discord/widgets/channels/list/items/ChannelListVocalItem;", "data", "", "userLimit", "", "getContentDescription", "(Lcom/discord/widgets/channels/list/items/ChannelListVocalItem;I)Ljava/lang/CharSequence;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemChannelStageVoiceBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemChannelStageVoiceBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelStageVoice extends Item {
        public static final Companion Companion = new Companion(null);
        private final WidgetChannelsListItemChannelStageVoiceBinding binding;

        /* compiled from: WidgetChannelsListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelStageVoice$Companion;", "", "", "isSelected", "Landroid/content/Context;", "context", "", "getVoiceChannelColor", "(ZLandroid/content/Context;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            @ColorInt
            public final int getVoiceChannelColor(boolean z2, Context context) {
                if (z2) {
                    return ColorCompat.getThemedColor(context, (int) R.attr.colorInteractiveActive);
                }
                return ColorCompat.getThemedColor(context, (int) R.attr.colorChannelDefault);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelStageVoice(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_item_stage_voice_channel_guild_role_subscription_icon;
            ImageView imageView = (ImageView) view.findViewById(R.id.channels_item_stage_voice_channel_guild_role_subscription_icon);
            if (imageView != null) {
                i2 = R.id.stage_channel_item_stage_channel_icon;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.stage_channel_item_stage_channel_icon);
                if (imageView2 != null) {
                    i2 = R.id.stage_channel_item_voice_channel_name;
                    TextView textView = (TextView) view.findViewById(R.id.stage_channel_item_voice_channel_name);
                    if (textView != null) {
                        i2 = R.id.stage_channel_item_voice_channel_topic;
                        TextView textView2 = (TextView) view.findViewById(R.id.stage_channel_item_voice_channel_topic);
                        if (textView2 != null) {
                            WidgetChannelsListItemChannelStageVoiceBinding widgetChannelsListItemChannelStageVoiceBinding = new WidgetChannelsListItemChannelStageVoiceBinding((RelativeLayout) view, imageView, imageView2, textView, textView2);
                            m.checkNotNullExpressionValue(widgetChannelsListItemChannelStageVoiceBinding, "WidgetChannelsListItemCh…iceBinding.bind(itemView)");
                            this.binding = widgetChannelsListItemChannelStageVoiceBinding;
                            return;
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelStageVoice itemChannelStageVoice) {
            return (WidgetChannelsListAdapter) itemChannelStageVoice.adapter;
        }

        private final CharSequence getContentDescription(ChannelListVocalItem channelListVocalItem, int i) {
            CharSequence d;
            CharSequence d2;
            CharSequence d3;
            if (i > 0) {
                RelativeLayout relativeLayout = this.binding.a;
                m.checkNotNullExpressionValue(relativeLayout, "binding.root");
                d3 = b.d(relativeLayout, R.string.guild_sidebar_voice_channel_a11y_label_with_limit, new Object[]{ChannelUtils.c(channelListVocalItem.getChannel()), Integer.valueOf(channelListVocalItem.getNumUsersConnected()), Integer.valueOf(i)}, (r4 & 4) != 0 ? b.c.j : null);
                return d3;
            } else if (channelListVocalItem.getNumUsersConnected() > 0) {
                RelativeLayout relativeLayout2 = this.binding.a;
                m.checkNotNullExpressionValue(relativeLayout2, "binding.root");
                RelativeLayout relativeLayout3 = this.binding.a;
                m.checkNotNullExpressionValue(relativeLayout3, "binding.root");
                Context context = relativeLayout3.getContext();
                m.checkNotNullExpressionValue(context, "binding.root.context");
                d2 = b.d(relativeLayout2, R.string.guild_sidebar_voice_channel_a11y_label_with_users, new Object[]{ChannelUtils.c(channelListVocalItem.getChannel()), StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_sidebar_voice_channel_a11y_label_with_users_userCount, channelListVocalItem.getNumUsersConnected(), Integer.valueOf(channelListVocalItem.getNumUsersConnected()))}, (r4 & 4) != 0 ? b.c.j : null);
                return d2;
            } else {
                RelativeLayout relativeLayout4 = this.binding.a;
                m.checkNotNullExpressionValue(relativeLayout4, "binding.root");
                d = b.d(relativeLayout4, R.string.guild_sidebar_voice_channel_a11y_label, new Object[]{ChannelUtils.c(channelListVocalItem.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d;
            }
        }

        @SuppressLint({"SetTextI18n"})
        public void onConfigure(int i, final ChannelListItem channelListItem) {
            ColorStateList colorStateList;
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            ChannelListItemStageVoiceChannel channelListItemStageVoiceChannel = (ChannelListItemStageVoiceChannel) channelListItem;
            Channel component1 = channelListItemStageVoiceChannel.component1();
            boolean component2 = channelListItemStageVoiceChannel.component2();
            Long component3 = channelListItemStageVoiceChannel.component3();
            boolean component4 = channelListItemStageVoiceChannel.component4();
            StageInstance component6 = channelListItemStageVoiceChannel.component6();
            final boolean component8 = channelListItemStageVoiceChannel.component8();
            boolean component9 = channelListItemStageVoiceChannel.component9();
            int i2 = 0;
            boolean z2 = component6 != null;
            boolean can = PermissionUtils.can(Permission.CONNECT, component3);
            Companion companion = Companion;
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            Context context = view.getContext();
            m.checkNotNullExpressionValue(context, "itemView.context");
            int voiceChannelColor = companion.getVoiceChannelColor(component2, context);
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelStageVoice$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    if (component8) {
                        WidgetChannelsListAdapter.ItemChannelStageVoice.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelStageVoice.this).getOnSelectGuildRoleSubscriptionLockedChannel().invoke(((ChannelListItemStageVoiceChannel) channelListItem).getChannel());
                    } else {
                        WidgetChannelsListAdapter.ItemChannelStageVoice.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelStageVoice.this).getOnCallChannel().invoke(((ChannelListItemStageVoiceChannel) channelListItem).getChannel());
                    }
                }
            });
            RelativeLayout relativeLayout = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout, "binding.root");
            relativeLayout.setSelected(component2);
            RelativeLayout relativeLayout2 = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout2, "binding.root");
            setBackground(relativeLayout2, component2, component1);
            TextView textView = this.binding.d;
            textView.setText(ChannelUtils.c(component1));
            textView.setTextColor(voiceChannelColor);
            TextView textView2 = this.binding.e;
            m.checkNotNullExpressionValue(textView2, "binding.stageChannelItemVoiceChannelTopic");
            ViewExtensions.setTextAndVisibilityBy(textView2, component6 != null ? component6.f() : null);
            ImageView imageView = this.binding.f2272b;
            m.checkNotNullExpressionValue(imageView, "binding.channelsItemStag…GuildRoleSubscriptionIcon");
            if (!component9) {
                i2 = 8;
            }
            imageView.setVisibility(i2);
            if (component8) {
                this.binding.f2272b.setImageResource(R.drawable.ic_premium_channel_locked);
            } else if (component9) {
                this.binding.f2272b.setImageResource(R.drawable.ic_premium_channel_unlocked);
            }
            int i3 = !can ? R.drawable.ic_channel_lock_16dp : component4 ? R.drawable.ic_channel_stage_locked : R.drawable.ic_channel_stage_24dp;
            ImageView imageView2 = this.binding.c;
            imageView2.setImageResource(i3);
            if (!z2 || !can) {
                colorStateList = ColorStateList.valueOf(voiceChannelColor);
            } else {
                RelativeLayout relativeLayout3 = this.binding.a;
                m.checkNotNullExpressionValue(relativeLayout3, "binding.root");
                colorStateList = ColorStateList.valueOf(ColorCompat.getColor(relativeLayout3.getContext(), (int) R.color.status_green_600));
            }
            imageView2.setImageTintList(colorStateList);
            RelativeLayout relativeLayout4 = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout4, "binding.root");
            relativeLayout4.setContentDescription(getContentDescription((ChannelListVocalItem) channelListItem, component1.B()));
            RelativeLayout relativeLayout5 = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout5, "binding.root");
            ViewExtensions.setOnLongClickListenerConsumeClick(relativeLayout5, new WidgetChannelsListAdapter$ItemChannelStageVoice$onConfigure$4(this, component1));
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0019\u001a\u00020\t\u0012\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u001b\u0010\u0006\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\n\u001a\u00020\t*\u00020\u00022\u0006\u0010\b\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0013\u0010\f\u001a\u00020\t*\u00020\u0002H\u0003¢\u0006\u0004\b\f\u0010\rJ\u0013\u0010\u000e\u001a\u00020\t*\u00020\u0002H\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0013\u0010\u000f\u001a\u00020\t*\u00020\u0002H\u0003¢\u0006\u0004\b\u000f\u0010\rJ\u001f\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u0011H\u0014¢\u0006\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelText;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "Lcom/discord/widgets/channels/list/items/ChannelListItemTextChannel;", "", "announcementChannel", "", "getContentDescription", "(Lcom/discord/widgets/channels/list/items/ChannelListItemTextChannel;Z)Ljava/lang/CharSequence;", "selected", "", "getTextColor", "(Lcom/discord/widgets/channels/list/items/ChannelListItemTextChannel;Z)I", "getHashColor", "(Lcom/discord/widgets/channels/list/items/ChannelListItemTextChannel;)I", "getHashIcon", "getAnnouncementsIcon", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemChannelBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemChannelBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelText extends Item {
        private final WidgetChannelsListItemChannelBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelText(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_item_channel_guild_role_subscription_icon;
            ImageView imageView = (ImageView) view.findViewById(R.id.channels_item_channel_guild_role_subscription_icon);
            if (imageView != null) {
                i2 = R.id.channels_item_channel_hash;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.channels_item_channel_hash);
                if (imageView2 != null) {
                    i2 = R.id.channels_item_channel_mentions;
                    TextView textView = (TextView) view.findViewById(R.id.channels_item_channel_mentions);
                    if (textView != null) {
                        i2 = R.id.channels_item_channel_name;
                        TextView textView2 = (TextView) view.findViewById(R.id.channels_item_channel_name);
                        if (textView2 != null) {
                            i2 = R.id.channels_item_channel_unread;
                            ImageView imageView3 = (ImageView) view.findViewById(R.id.channels_item_channel_unread);
                            if (imageView3 != null) {
                                WidgetChannelsListItemChannelBinding widgetChannelsListItemChannelBinding = new WidgetChannelsListItemChannelBinding((RelativeLayout) view, imageView, imageView2, textView, textView2, imageView3);
                                m.checkNotNullExpressionValue(widgetChannelsListItemChannelBinding, "WidgetChannelsListItemCh…nelBinding.bind(itemView)");
                                this.binding = widgetChannelsListItemChannelBinding;
                                return;
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelText itemChannelText) {
            return (WidgetChannelsListAdapter) itemChannelText.adapter;
        }

        @DrawableRes
        private final int getAnnouncementsIcon(ChannelListItemTextChannel channelListItemTextChannel) {
            return channelListItemTextChannel.getChannel().o() ? channelListItemTextChannel.getHasActiveThreads() ? R.drawable.ic_channel_announcements_thread_nsfw : R.drawable.ic_channel_announcements_nsfw : channelListItemTextChannel.isLocked() ? channelListItemTextChannel.getHasActiveThreads() ? R.drawable.ic_channel_announcements_thread_locked : R.drawable.ic_channel_announcements_locked : channelListItemTextChannel.getHasActiveThreads() ? R.drawable.ic_channel_announcements_thread : R.drawable.ic_channel_announcements;
        }

        private final CharSequence getContentDescription(ChannelListItemTextChannel channelListItemTextChannel, boolean z2) {
            CharSequence d;
            CharSequence d2;
            CharSequence d3;
            CharSequence d4;
            CharSequence d5;
            CharSequence d6;
            if (z2 && channelListItemTextChannel.getMentionCount() > 0) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                d6 = b.d(view, R.string.guild_sidebar_announcement_channel_a11y_label_with_mentions, new Object[]{StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.guild_sidebar_announcement_channel_a11y_label_with_mentions_mentionCount, channelListItemTextChannel.getMentionCount(), Integer.valueOf(channelListItemTextChannel.getMentionCount())), ChannelUtils.c(channelListItemTextChannel.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d6;
            } else if (z2 && channelListItemTextChannel.isUnread()) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                d5 = b.d(view2, R.string.guild_sidebar_announcement_channel_a11y_label_with_unreads, new Object[]{ChannelUtils.c(channelListItemTextChannel.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d5;
            } else if (z2) {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                d4 = b.d(view3, R.string.guild_sidebar_announcement_channel_a11y_label, new Object[]{ChannelUtils.c(channelListItemTextChannel.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d4;
            } else if (channelListItemTextChannel.getMentionCount() > 0) {
                View view4 = this.itemView;
                m.checkNotNullExpressionValue(view4, "itemView");
                d3 = b.d(view4, R.string.guild_sidebar_default_channel_a11y_label_with_mentions, new Object[]{StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.guild_sidebar_default_channel_a11y_label_with_mentions_mentionCount, channelListItemTextChannel.getMentionCount(), Integer.valueOf(channelListItemTextChannel.getMentionCount())), ChannelUtils.c(channelListItemTextChannel.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d3;
            } else if (channelListItemTextChannel.isUnread()) {
                View view5 = this.itemView;
                m.checkNotNullExpressionValue(view5, "itemView");
                d2 = b.d(view5, R.string.guild_sidebar_default_channel_a11y_label_with_unreads, new Object[]{ChannelUtils.c(channelListItemTextChannel.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d2;
            } else {
                View view6 = this.itemView;
                m.checkNotNullExpressionValue(view6, "itemView");
                d = b.d(view6, R.string.guild_sidebar_default_channel_a11y_label, new Object[]{ChannelUtils.c(channelListItemTextChannel.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d;
            }
        }

        @ColorInt
        private final int getHashColor(ChannelListItemTextChannel channelListItemTextChannel) {
            if (channelListItemTextChannel.getMuted()) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                return ColorCompat.getThemedColor(view, (int) R.attr.colorInteractiveMuted);
            } else if (channelListItemTextChannel.isUnread()) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                return ColorCompat.getThemedColor(view2, (int) R.attr.colorInteractiveActive);
            } else {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                return ColorCompat.getThemedColor(view3, (int) R.attr.colorChannelDefault);
            }
        }

        @DrawableRes
        private final int getHashIcon(ChannelListItemTextChannel channelListItemTextChannel) {
            return channelListItemTextChannel.getChannel().o() ? channelListItemTextChannel.getHasActiveThreads() ? R.drawable.ic_thread_nsfw : R.drawable.ic_channel_text_nsfw : channelListItemTextChannel.isLocked() ? channelListItemTextChannel.getHasActiveThreads() ? R.drawable.ic_thread_locked : R.drawable.ic_channel_text_locked : channelListItemTextChannel.getHasActiveThreads() ? R.drawable.ic_thread : R.drawable.ic_channel_text;
        }

        @ColorInt
        private final int getTextColor(ChannelListItemTextChannel channelListItemTextChannel, boolean z2) {
            if (z2) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                return ColorCompat.getThemedColor(view, (int) R.attr.colorInteractiveActive);
            } else if (channelListItemTextChannel.getMuted()) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                return ColorCompat.getThemedColor(view2, (int) R.attr.colorInteractiveMuted);
            } else if (channelListItemTextChannel.isUnread()) {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                return ColorCompat.getThemedColor(view3, (int) R.attr.colorInteractiveActive);
            } else {
                View view4 = this.itemView;
                m.checkNotNullExpressionValue(view4, "itemView");
                return ColorCompat.getThemedColor(view4, (int) R.attr.colorChannelDefault);
            }
        }

        public void onConfigure(int i, final ChannelListItem channelListItem) {
            int i2;
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            final ChannelListItemTextChannel channelListItemTextChannel = (ChannelListItemTextChannel) channelListItem;
            boolean z2 = true;
            int i3 = 0;
            boolean z3 = channelListItemTextChannel.getChannel().A() == 5;
            RelativeLayout relativeLayout = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout, "binding.root");
            ViewExtensions.setOnLongClickListenerConsumeClick(relativeLayout, new WidgetChannelsListAdapter$ItemChannelText$onConfigure$1(this, channelListItem));
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelText$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    if (channelListItemTextChannel.isGuildRoleSubscriptionLockedChannel()) {
                        WidgetChannelsListAdapter.ItemChannelText.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelText.this).getOnSelectGuildRoleSubscriptionLockedChannel().invoke(((ChannelListItemTextChannel) channelListItem).getChannel());
                    } else {
                        WidgetChannelsListAdapter.ItemChannelText.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelText.this).getOnSelectChannel().invoke(((ChannelListItemTextChannel) channelListItem).getChannel());
                    }
                }
            });
            RelativeLayout relativeLayout2 = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout2, "binding.root");
            setBackground(relativeLayout2, channelListItemTextChannel.getSelected(), channelListItemTextChannel.getChannel());
            RelativeLayout relativeLayout3 = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout3, "binding.root");
            relativeLayout3.setContentDescription(getContentDescription(channelListItemTextChannel, z3));
            TextView textView = this.binding.e;
            m.checkNotNullExpressionValue(textView, "binding.channelsItemChannelName");
            textView.setText(ChannelUtils.c(channelListItemTextChannel.getChannel()));
            this.binding.e.setTextColor(getTextColor(channelListItemTextChannel, channelListItemTextChannel.getSelected()));
            ImageView imageView = this.binding.c;
            if (z3) {
                i2 = getAnnouncementsIcon(channelListItemTextChannel);
            } else {
                i2 = getHashIcon(channelListItemTextChannel);
            }
            imageView.setImageResource(i2);
            ImageView imageView2 = this.binding.c;
            m.checkNotNullExpressionValue(imageView2, "binding.channelsItemChannelHash");
            imageView2.setImageTintList(ColorStateList.valueOf(getHashColor(channelListItemTextChannel)));
            TextView textView2 = this.binding.d;
            m.checkNotNullExpressionValue(textView2, "binding.channelsItemChannelMentions");
            textView2.setVisibility(channelListItemTextChannel.getMentionCount() > 0 ? 0 : 8);
            TextView textView3 = this.binding.d;
            m.checkNotNullExpressionValue(textView3, "binding.channelsItemChannelMentions");
            textView3.setText(String.valueOf(Math.min(99, channelListItemTextChannel.getMentionCount())));
            ImageView imageView3 = this.binding.f;
            m.checkNotNullExpressionValue(imageView3, "binding.channelsItemChannelUnread");
            if (!channelListItemTextChannel.isUnread() || channelListItemTextChannel.getSelected() || channelListItemTextChannel.getMuted()) {
                z2 = false;
            }
            imageView3.setVisibility(z2 ? 0 : 8);
            TextView textView4 = this.binding.e;
            m.checkNotNullExpressionValue(textView4, "binding.channelsItemChannelName");
            FontUtils fontUtils = FontUtils.INSTANCE;
            RelativeLayout relativeLayout4 = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout4, "binding.root");
            Context context = relativeLayout4.getContext();
            m.checkNotNullExpressionValue(context, "binding.root.context");
            textView4.setTypeface(fontUtils.getThemedFont(context, (channelListItemTextChannel.isUnread() || channelListItemTextChannel.getSelected()) ? R.attr.font_primary_semibold : R.attr.font_primary_normal));
            ImageView imageView4 = this.binding.f2270b;
            m.checkNotNullExpressionValue(imageView4, "binding.channelsItemChan…GuildRoleSubscriptionIcon");
            if (!channelListItemTextChannel.isGuildRoleSubscriptionChannel()) {
                i3 = 8;
            }
            imageView4.setVisibility(i3);
            if (channelListItemTextChannel.isGuildRoleSubscriptionLockedChannel()) {
                this.binding.f2270b.setImageResource(R.drawable.ic_premium_channel_locked);
            } else if (channelListItemTextChannel.isGuildRoleSubscriptionChannel()) {
                this.binding.f2270b.setImageResource(R.drawable.ic_premium_channel_unlocked);
            }
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0004\u0012\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0013\u0010\t\u001a\u00020\b*\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\r\u001a\u00020\u0004*\u00020\u00072\u0006\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0010H\u0014¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelThread;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType;", "spineType", "", "getSpineResourceId", "(Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType;)I", "Lcom/discord/widgets/channels/list/items/ChannelListItemThread;", "", "getContentDescription", "(Lcom/discord/widgets/channels/list/items/ChannelListItemThread;)Ljava/lang/CharSequence;", "", "selected", "getTextColor", "(Lcom/discord/widgets/channels/list/items/ChannelListItemThread;Z)I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemThreadBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemThreadBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelThread extends Item {
        private final WidgetChannelsListItemThreadBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelThread(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_item_thread_content;
            ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.channels_item_thread_content);
            if (constraintLayout != null) {
                i2 = R.id.channels_item_thread_mentions;
                TextView textView = (TextView) view.findViewById(R.id.channels_item_thread_mentions);
                if (textView != null) {
                    i2 = R.id.channels_item_thread_name;
                    TextView textView2 = (TextView) view.findViewById(R.id.channels_item_thread_name);
                    if (textView2 != null) {
                        i2 = R.id.channels_item_thread_spine;
                        ImageView imageView = (ImageView) view.findViewById(R.id.channels_item_thread_spine);
                        if (imageView != null) {
                            i2 = R.id.channels_item_thread_unread;
                            ImageView imageView2 = (ImageView) view.findViewById(R.id.channels_item_thread_unread);
                            if (imageView2 != null) {
                                WidgetChannelsListItemThreadBinding widgetChannelsListItemThreadBinding = new WidgetChannelsListItemThreadBinding((ConstraintLayout) view, constraintLayout, textView, textView2, imageView, imageView2);
                                m.checkNotNullExpressionValue(widgetChannelsListItemThreadBinding, "WidgetChannelsListItemThreadBinding.bind(itemView)");
                                this.binding = widgetChannelsListItemThreadBinding;
                                return;
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelThread itemChannelThread) {
            return (WidgetChannelsListAdapter) itemChannelThread.adapter;
        }

        private final CharSequence getContentDescription(ChannelListItemThread channelListItemThread) {
            CharSequence d;
            CharSequence d2;
            CharSequence d3;
            if (channelListItemThread.getMentionCount() > 0) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                d3 = b.d(view, R.string.guild_sidebar_default_channel_a11y_label_with_mentions, new Object[]{StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.guild_sidebar_default_channel_a11y_label_with_mentions_mentionCount, channelListItemThread.getMentionCount(), Integer.valueOf(channelListItemThread.getMentionCount())), channelListItemThread.getChannel().m()}, (r4 & 4) != 0 ? b.c.j : null);
                return d3;
            } else if (channelListItemThread.isUnread()) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                d2 = b.d(view2, R.string.guild_sidebar_default_channel_a11y_label_with_unreads, new Object[]{channelListItemThread.getChannel().m()}, (r4 & 4) != 0 ? b.c.j : null);
                return d2;
            } else {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                d = b.d(view3, R.string.guild_sidebar_default_channel_a11y_label, new Object[]{channelListItemThread.getChannel().m()}, (r4 & 4) != 0 ? b.c.j : null);
                return d;
            }
        }

        private final int getSpineResourceId(WidgetChannelListModel.ThreadSpineType threadSpineType) {
            return m.areEqual(threadSpineType, WidgetChannelListModel.ThreadSpineType.Single.INSTANCE) ? R.drawable.ic_spine_short_cap : m.areEqual(threadSpineType, WidgetChannelListModel.ThreadSpineType.Start.INSTANCE) ? R.drawable.ic_spine_long_cap : m.areEqual(threadSpineType, WidgetChannelListModel.ThreadSpineType.End.INSTANCE) ? R.drawable.ic_spine_short_no_cap : R.drawable.ic_spine_long_no_cap;
        }

        @ColorInt
        private final int getTextColor(ChannelListItemThread channelListItemThread, boolean z2) {
            if (z2) {
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                return ColorCompat.getThemedColor(view, (int) R.attr.colorInteractiveActive);
            } else if (channelListItemThread.getMuted()) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                return ColorCompat.getThemedColor(view2, (int) R.attr.colorInteractiveMuted);
            } else if (channelListItemThread.isUnread()) {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                return ColorCompat.getThemedColor(view3, (int) R.attr.colorInteractiveActive);
            } else {
                View view4 = this.itemView;
                m.checkNotNullExpressionValue(view4, "itemView");
                return ColorCompat.getThemedColor(view4, (int) R.attr.colorChannelDefault);
            }
        }

        public void onConfigure(int i, final ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            ChannelListItemThread channelListItemThread = (ChannelListItemThread) channelListItem;
            ConstraintLayout constraintLayout = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout, "binding.root");
            ViewExtensions.setOnLongClickListenerConsumeClick(constraintLayout, new WidgetChannelsListAdapter$ItemChannelThread$onConfigure$1(this, channelListItem));
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelThread$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChannelsListAdapter.ItemChannelThread.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelThread.this).getOnSelectChannel().invoke(((ChannelListItemThread) channelListItem).getChannel());
                }
            });
            ConstraintLayout constraintLayout2 = this.binding.f2281b;
            m.checkNotNullExpressionValue(constraintLayout2, "binding.channelsItemThreadContent");
            setBackground(constraintLayout2, channelListItemThread.getSelected(), channelListItemThread.getChannel());
            ConstraintLayout constraintLayout3 = this.binding.f2281b;
            m.checkNotNullExpressionValue(constraintLayout3, "binding.channelsItemThreadContent");
            constraintLayout3.setContentDescription(getContentDescription(channelListItemThread));
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.channelsItemThreadName");
            textView.setText(channelListItemThread.getChannel().m());
            this.binding.d.setTextColor(getTextColor(channelListItemThread, channelListItemThread.getSelected()));
            TextView textView2 = this.binding.c;
            m.checkNotNullExpressionValue(textView2, "binding.channelsItemThreadMentions");
            boolean z2 = true;
            int i2 = 0;
            textView2.setVisibility(channelListItemThread.getMentionCount() > 0 ? 0 : 8);
            TextView textView3 = this.binding.c;
            m.checkNotNullExpressionValue(textView3, "binding.channelsItemThreadMentions");
            textView3.setText(String.valueOf(Math.min(99, channelListItemThread.getMentionCount())));
            ImageView imageView = this.binding.f;
            m.checkNotNullExpressionValue(imageView, "binding.channelsItemThreadUnread");
            if (!channelListItemThread.isUnread() || channelListItemThread.getSelected() || channelListItemThread.getMuted()) {
                z2 = false;
            }
            if (!z2) {
                i2 = 8;
            }
            imageView.setVisibility(i2);
            TextView textView4 = this.binding.d;
            m.checkNotNullExpressionValue(textView4, "binding.channelsItemThreadName");
            FontUtils fontUtils = FontUtils.INSTANCE;
            ConstraintLayout constraintLayout4 = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout4, "binding.root");
            Context context = constraintLayout4.getContext();
            m.checkNotNullExpressionValue(context, "binding.root.context");
            textView4.setTypeface(fontUtils.getThemedFont(context, (channelListItemThread.isUnread() || channelListItemThread.getSelected()) ? R.attr.font_primary_semibold : R.attr.font_primary_normal));
            this.binding.e.setImageResource(getSpineResourceId(channelListItemThread.getSpineType()));
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0017\u0012\u0006\u0010\u0011\u001a\u00020\u0004\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\nH\u0015¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelVoice;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceChannel;", "data", "", "userLimit", "", "getContentDescription", "(Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceChannel;I)Ljava/lang/CharSequence;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemChannelVoiceBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemChannelVoiceBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemChannelVoice extends Item {
        public static final Companion Companion = new Companion(null);
        private final WidgetChannelsListItemChannelVoiceBinding binding;

        /* compiled from: WidgetChannelsListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelVoice$Companion;", "", "", "isSelected", "Landroid/content/Context;", "context", "", "getVoiceChannelColor", "(ZLandroid/content/Context;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            @ColorInt
            public final int getVoiceChannelColor(boolean z2, Context context) {
                if (z2) {
                    return ColorCompat.getThemedColor(context, (int) R.attr.colorInteractiveActive);
                }
                return ColorCompat.getThemedColor(context, (int) R.attr.colorChannelDefault);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemChannelVoice(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_item_channel_mentions;
            TextView textView = (TextView) view.findViewById(R.id.channels_item_channel_mentions);
            if (textView != null) {
                i2 = R.id.channels_item_channel_unread;
                ImageView imageView = (ImageView) view.findViewById(R.id.channels_item_channel_unread);
                if (imageView != null) {
                    i2 = R.id.channels_item_voice_channel_event_topic;
                    TextView textView2 = (TextView) view.findViewById(R.id.channels_item_voice_channel_event_topic);
                    if (textView2 != null) {
                        i2 = R.id.channels_item_voice_channel_guild_role_subscription_icon;
                        ImageView imageView2 = (ImageView) view.findViewById(R.id.channels_item_voice_channel_guild_role_subscription_icon);
                        if (imageView2 != null) {
                            i2 = R.id.channels_item_voice_channel_name;
                            TextView textView3 = (TextView) view.findViewById(R.id.channels_item_voice_channel_name);
                            if (textView3 != null) {
                                i2 = R.id.channels_item_voice_channel_speaker;
                                ImageView imageView3 = (ImageView) view.findViewById(R.id.channels_item_voice_channel_speaker);
                                if (imageView3 != null) {
                                    i2 = R.id.channels_item_voice_channel_user_limit;
                                    VoiceUserLimitView voiceUserLimitView = (VoiceUserLimitView) view.findViewById(R.id.channels_item_voice_channel_user_limit);
                                    if (voiceUserLimitView != null) {
                                        i2 = R.id.channels_item_voice_channel_wrapper;
                                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.channels_item_voice_channel_wrapper);
                                        if (linearLayout != null) {
                                            WidgetChannelsListItemChannelVoiceBinding widgetChannelsListItemChannelVoiceBinding = new WidgetChannelsListItemChannelVoiceBinding((ConstraintLayout) view, textView, imageView, textView2, imageView2, textView3, imageView3, voiceUserLimitView, linearLayout);
                                            m.checkNotNullExpressionValue(widgetChannelsListItemChannelVoiceBinding, "WidgetChannelsListItemCh…iceBinding.bind(itemView)");
                                            this.binding = widgetChannelsListItemChannelVoiceBinding;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemChannelVoice itemChannelVoice) {
            return (WidgetChannelsListAdapter) itemChannelVoice.adapter;
        }

        private final CharSequence getContentDescription(ChannelListItemVoiceChannel channelListItemVoiceChannel, int i) {
            CharSequence d;
            CharSequence d2;
            CharSequence d3;
            if (i > 0) {
                ConstraintLayout constraintLayout = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout, "binding.root");
                d3 = b.d(constraintLayout, R.string.guild_sidebar_voice_channel_a11y_label_with_limit, new Object[]{ChannelUtils.c(channelListItemVoiceChannel.getChannel()), Integer.valueOf(channelListItemVoiceChannel.getNumUsersConnected()), Integer.valueOf(i)}, (r4 & 4) != 0 ? b.c.j : null);
                return d3;
            } else if (channelListItemVoiceChannel.getNumUsersConnected() > 0) {
                ConstraintLayout constraintLayout2 = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout2, "binding.root");
                ConstraintLayout constraintLayout3 = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout3, "binding.root");
                Context context = constraintLayout3.getContext();
                m.checkNotNullExpressionValue(context, "binding.root.context");
                d2 = b.d(constraintLayout2, R.string.guild_sidebar_voice_channel_a11y_label_with_users, new Object[]{ChannelUtils.c(channelListItemVoiceChannel.getChannel()), StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_sidebar_voice_channel_a11y_label_with_users_userCount, channelListItemVoiceChannel.getNumUsersConnected(), Integer.valueOf(channelListItemVoiceChannel.getNumUsersConnected()))}, (r4 & 4) != 0 ? b.c.j : null);
                return d2;
            } else {
                ConstraintLayout constraintLayout4 = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout4, "binding.root");
                d = b.d(constraintLayout4, R.string.guild_sidebar_voice_channel_a11y_label, new Object[]{ChannelUtils.c(channelListItemVoiceChannel.getChannel())}, (r4 & 4) != 0 ? b.c.j : null);
                return d;
            }
        }

        @SuppressLint({"SetTextI18n"})
        public void onConfigure(int i, final ChannelListItem channelListItem) {
            int i2;
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            ChannelListItemVoiceChannel channelListItemVoiceChannel = (ChannelListItemVoiceChannel) channelListItem;
            Channel component1 = channelListItemVoiceChannel.component1();
            boolean component2 = channelListItemVoiceChannel.component2();
            boolean component3 = channelListItemVoiceChannel.component3();
            Long component4 = channelListItemVoiceChannel.component4();
            int component5 = channelListItemVoiceChannel.component5();
            boolean component6 = channelListItemVoiceChannel.component6();
            int component7 = channelListItemVoiceChannel.component7();
            boolean component8 = channelListItemVoiceChannel.component8();
            boolean component9 = channelListItemVoiceChannel.component9();
            boolean component10 = channelListItemVoiceChannel.component10();
            GuildMaxVideoChannelUsers component11 = channelListItemVoiceChannel.component11();
            final boolean component12 = channelListItemVoiceChannel.component12();
            boolean component13 = channelListItemVoiceChannel.component13();
            GuildScheduledEvent component14 = channelListItemVoiceChannel.component14();
            boolean can = PermissionUtils.can(Permission.CONNECT, component4);
            Companion companion = Companion;
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            Context context = view.getContext();
            m.checkNotNullExpressionValue(context, "itemView.context");
            int voiceChannelColor = companion.getVoiceChannelColor(component3, context);
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemChannelVoice$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    if (component12) {
                        WidgetChannelsListAdapter.ItemChannelVoice.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelVoice.this).getOnSelectGuildRoleSubscriptionLockedChannel().invoke(((ChannelListItemVoiceChannel) channelListItem).getChannel());
                    } else {
                        WidgetChannelsListAdapter.ItemChannelVoice.access$getAdapter$p(WidgetChannelsListAdapter.ItemChannelVoice.this).getOnCallChannel().invoke(((ChannelListItemVoiceChannel) channelListItem).getChannel());
                    }
                }
            });
            ConstraintLayout constraintLayout = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout, "binding.root");
            constraintLayout.setSelected(component3);
            ConstraintLayout constraintLayout2 = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout2, "binding.root");
            setBackground(constraintLayout2, component3, component1);
            TextView textView = this.binding.f;
            m.checkNotNullExpressionValue(textView, "binding.channelsItemVoiceChannelName");
            textView.setText(ChannelUtils.c(component1));
            this.binding.f.setTextColor(voiceChannelColor);
            TextView textView2 = this.binding.d;
            m.checkNotNullExpressionValue(textView2, "binding.channelsItemVoiceChannelEventTopic");
            ViewExtensions.setTextAndVisibilityBy(textView2, component14 != null ? component14.j() : null);
            TextView textView3 = this.binding.f2273b;
            m.checkNotNullExpressionValue(textView3, "binding.channelsItemChannelMentions");
            textView3.setVisibility(component5 > 0 ? 0 : 8);
            TextView textView4 = this.binding.f2273b;
            m.checkNotNullExpressionValue(textView4, "binding.channelsItemChannelMentions");
            textView4.setText(String.valueOf(Math.min(99, component5)));
            ImageView imageView = this.binding.c;
            m.checkNotNullExpressionValue(imageView, "binding.channelsItemChannelUnread");
            boolean z2 = true;
            imageView.setVisibility(component6 && !component2 && component3 ? 0 : 8);
            ImageView imageView2 = this.binding.e;
            m.checkNotNullExpressionValue(imageView2, "binding.channelsItemVoic…GuildRoleSubscriptionIcon");
            imageView2.setVisibility(component13 ? 0 : 8);
            if (component12) {
                this.binding.e.setImageResource(R.drawable.ic_premium_channel_locked);
            } else if (component13) {
                this.binding.e.setImageResource(R.drawable.ic_premium_channel_unlocked);
            }
            int i3 = !can ? R.drawable.ic_channel_lock_16dp : component9 ? R.drawable.ic_voice_nsfw : component8 ? R.drawable.ic_channel_voice_locked : R.drawable.ic_channel_voice;
            if (component14 != null) {
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                i2 = ColorCompat.getColor(view2.getContext(), (int) R.color.status_green_600);
            } else {
                i2 = voiceChannelColor;
            }
            this.binding.g.setImageResource(i3);
            ImageView imageView3 = this.binding.g;
            m.checkNotNullExpressionValue(imageView3, "binding.channelsItemVoiceChannelSpeaker");
            imageView3.setImageTintList(ColorStateList.valueOf(i2));
            GuildMaxVideoChannelUsers.Limited limited = (GuildMaxVideoChannelUsers.Limited) (!(component11 instanceof GuildMaxVideoChannelUsers.Limited) ? null : component11);
            int a = limited != null ? limited.a() : 0;
            int min = Math.min(component1.B(), a);
            if (min > 0) {
                boolean z3 = component10 && min == a;
                if (!z3) {
                    a = component1.B();
                }
                VoiceUserLimitView voiceUserLimitView = this.binding.h;
                m.checkNotNullExpressionValue(voiceUserLimitView, "binding.channelsItemVoiceChannelUserLimit");
                if (a <= 0 || !can) {
                    z2 = false;
                }
                voiceUserLimitView.setVisibility(z2 ? 0 : 8);
                this.binding.h.a(component7, a, z3);
                ConstraintLayout constraintLayout3 = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout3, "binding.root");
                constraintLayout3.setContentDescription(getContentDescription(channelListItemVoiceChannel, a));
            } else {
                VoiceUserLimitView voiceUserLimitView2 = this.binding.h;
                m.checkNotNullExpressionValue(voiceUserLimitView2, "binding.channelsItemVoiceChannelUserLimit");
                voiceUserLimitView2.setVisibility(8);
                ConstraintLayout constraintLayout4 = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout4, "binding.root");
                constraintLayout4.setContentDescription(getContentDescription(channelListItemVoiceChannel, min));
            }
            if (PermissionUtils.can(16L, component4)) {
                ConstraintLayout constraintLayout5 = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout5, "binding.root");
                ViewExtensions.setOnLongClickListenerConsumeClick(constraintLayout5, new WidgetChannelsListAdapter$ItemChannelVoice$onConfigure$2(this, component1));
                return;
            }
            ConstraintLayout constraintLayout6 = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout6, "binding.root");
            ViewExtensions.setOnLongClickListenerConsumeClick(constraintLayout6, WidgetChannelsListAdapter$ItemChannelVoice$onConfigure$3.INSTANCE);
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemGuildJoinRequest;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemGuildJoinRequestBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemGuildJoinRequestBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemGuildJoinRequest extends Item {
        private final WidgetChannelsListItemGuildJoinRequestBinding binding;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                ApplicationStatus.values();
                int[] iArr = new int[5];
                $EnumSwitchMapping$0 = iArr;
                iArr[ApplicationStatus.STARTED.ordinal()] = 1;
                iArr[ApplicationStatus.PENDING.ordinal()] = 2;
                iArr[ApplicationStatus.REJECTED.ordinal()] = 3;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemGuildJoinRequest(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            int i2 = R.id.channels_list_item_guild_join_request_button;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.channels_list_item_guild_join_request_button);
            if (materialButton != null) {
                i2 = R.id.channels_list_item_guild_join_request_label;
                TextView textView = (TextView) view.findViewById(R.id.channels_list_item_guild_join_request_label);
                if (textView != null) {
                    WidgetChannelsListItemGuildJoinRequestBinding widgetChannelsListItemGuildJoinRequestBinding = new WidgetChannelsListItemGuildJoinRequestBinding((ConstraintLayout) view, constraintLayout, materialButton, textView);
                    m.checkNotNullExpressionValue(widgetChannelsListItemGuildJoinRequestBinding, "WidgetChannelsListItemGu…estBinding.bind(itemView)");
                    this.binding = widgetChannelsListItemGuildJoinRequestBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemGuildJoinRequest itemGuildJoinRequest) {
            return (WidgetChannelsListAdapter) itemGuildJoinRequest.adapter;
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            if (channelListItem instanceof ChannelListItemGuildJoinRequest) {
                ConstraintLayout constraintLayout = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout, "binding.root");
                final Context context = constraintLayout.getContext();
                ChannelListItemGuildJoinRequest channelListItemGuildJoinRequest = (ChannelListItemGuildJoinRequest) channelListItem;
                ApplicationStatus a = channelListItemGuildJoinRequest.getGuildJoinRequest().a();
                GuildMember member = channelListItemGuildJoinRequest.getMember();
                this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemGuildJoinRequest$onConfigure$1

                    /* compiled from: WidgetChannelsListAdapter.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemGuildJoinRequest$onConfigure$1$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1 extends o implements Function0<Unit> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        public AnonymousClass1() {
                            super(0);
                        }

                        @Override // kotlin.jvm.functions.Function0
                        /* renamed from: invoke  reason: avoid collision after fix types in other method */
                        public final void invoke2() {
                        }
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        MemberVerificationUtils memberVerificationUtils = MemberVerificationUtils.INSTANCE;
                        Context context2 = context;
                        m.checkNotNullExpressionValue(context2, "context");
                        memberVerificationUtils.maybeShowVerificationGate(context2, WidgetChannelsListAdapter.ItemGuildJoinRequest.access$getAdapter$p(WidgetChannelsListAdapter.ItemGuildJoinRequest.this).fragmentManager, WidgetChannelsListAdapter.ItemGuildJoinRequest.access$getAdapter$p(WidgetChannelsListAdapter.ItemGuildJoinRequest.this).getSelectedGuildId(), "Channel Notice", (r19 & 16) != 0 ? null : null, (r19 & 32) != 0 ? MemberVerificationUtils$maybeShowVerificationGate$1.INSTANCE : null, AnonymousClass1.INSTANCE);
                    }
                });
                if (!member.getPending()) {
                    ConstraintLayout constraintLayout2 = this.binding.f2275b;
                    m.checkNotNullExpressionValue(constraintLayout2, "binding.channelsListItemGuildJoinRequest");
                    constraintLayout2.setVisibility(8);
                }
                int ordinal = a.ordinal();
                if (ordinal == 0) {
                    this.binding.d.setText(R.string.member_verification_notice_text);
                    this.binding.c.setText(R.string.member_verification_notice_cta);
                    this.binding.c.setBackgroundColor(ColorCompat.getThemedColor(context, (int) R.attr.color_brand));
                    ConstraintLayout constraintLayout3 = this.binding.f2275b;
                    m.checkNotNullExpressionValue(constraintLayout3, "binding.channelsListItemGuildJoinRequest");
                    constraintLayout3.setVisibility(0);
                } else if (ordinal == 1) {
                    this.binding.d.setText(R.string.member_verification_pending_application_notice_title);
                    this.binding.c.setText(R.string.member_verification_pending_application_modal_cancel);
                    this.binding.c.setBackgroundColor(ColorCompat.getThemedColor(context, (int) R.attr.colorBackgroundAccent));
                    ConstraintLayout constraintLayout4 = this.binding.f2275b;
                    m.checkNotNullExpressionValue(constraintLayout4, "binding.channelsListItemGuildJoinRequest");
                    constraintLayout4.setVisibility(0);
                } else if (ordinal != 2) {
                    ConstraintLayout constraintLayout5 = this.binding.f2275b;
                    m.checkNotNullExpressionValue(constraintLayout5, "binding.channelsListItemGuildJoinRequest");
                    constraintLayout5.setVisibility(8);
                } else {
                    this.binding.d.setText(R.string.member_verification_application_rejected_notice_title);
                    this.binding.c.setText(R.string.member_verification_learn_more);
                    this.binding.c.setBackgroundColor(ColorCompat.getThemedColor(context, (int) R.attr.colorBackgroundAccent));
                    ConstraintLayout constraintLayout6 = this.binding.f2275b;
                    m.checkNotNullExpressionValue(constraintLayout6, "binding.channelsListItemGuildJoinRequest");
                    constraintLayout6.setVisibility(0);
                }
            }
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemGuildRoleSubscriptionsOverview;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemGuildRoleSubsBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemGuildRoleSubsBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemGuildRoleSubscriptionsOverview extends Item {
        private final WidgetChannelsListItemGuildRoleSubsBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemGuildRoleSubscriptionsOverview(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.directory_channel_icon;
            ImageView imageView = (ImageView) view.findViewById(R.id.directory_channel_icon);
            if (imageView != null) {
                i2 = R.id.directory_channel_name;
                TextView textView = (TextView) view.findViewById(R.id.directory_channel_name);
                if (textView != null) {
                    WidgetChannelsListItemGuildRoleSubsBinding widgetChannelsListItemGuildRoleSubsBinding = new WidgetChannelsListItemGuildRoleSubsBinding((LinearLayout) view, imageView, textView);
                    m.checkNotNullExpressionValue(widgetChannelsListItemGuildRoleSubsBinding, "WidgetChannelsListItemGu…ubsBinding.bind(itemView)");
                    this.binding = widgetChannelsListItemGuildRoleSubsBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemGuildRoleSubscriptionsOverview itemGuildRoleSubscriptionsOverview) {
            return (WidgetChannelsListAdapter) itemGuildRoleSubscriptionsOverview.adapter;
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            LinearLayout linearLayout = this.binding.a;
            m.checkNotNullExpressionValue(linearLayout, "binding.root");
            Context context = linearLayout.getContext();
            m.checkNotNullExpressionValue(context, "context");
            int themedDrawableRes$default = DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.drawable_overlay_channels, 0, 2, (Object) null);
            LinearLayout linearLayout2 = this.binding.a;
            m.checkNotNullExpressionValue(linearLayout2, "binding.root");
            ViewExtensions.setBackgroundAndKeepPadding(linearLayout2, ContextCompat.getDrawable(context, themedDrawableRes$default));
            this.binding.f2276b.setText(R.string.guild_role_subscriptions_channel_label);
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemGuildRoleSubscriptionsOverview$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChannelsListAdapter.ItemGuildRoleSubscriptionsOverview.access$getAdapter$p(WidgetChannelsListAdapter.ItemGuildRoleSubscriptionsOverview.this).getOnViewGuildRoleSubscriptions().invoke();
                }
            });
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemGuildScheduledEvents;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemGuildScheduledEventsBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemGuildScheduledEventsBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemGuildScheduledEvents extends Item {
        private final WidgetChannelsListItemGuildScheduledEventsBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemGuildScheduledEvents(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_list_item_guild_scheduled_event_count;
            TextView textView = (TextView) view.findViewById(R.id.channels_list_item_guild_scheduled_event_count);
            if (textView != null) {
                i2 = R.id.channels_list_item_guild_scheduled_event_icon;
                ImageView imageView = (ImageView) view.findViewById(R.id.channels_list_item_guild_scheduled_event_icon);
                if (imageView != null) {
                    i2 = R.id.channels_list_item_guild_scheduled_event_label;
                    TextView textView2 = (TextView) view.findViewById(R.id.channels_list_item_guild_scheduled_event_label);
                    if (textView2 != null) {
                        WidgetChannelsListItemGuildScheduledEventsBinding widgetChannelsListItemGuildScheduledEventsBinding = new WidgetChannelsListItemGuildScheduledEventsBinding((ConstraintLayout) view, textView, imageView, textView2);
                        m.checkNotNullExpressionValue(widgetChannelsListItemGuildScheduledEventsBinding, "WidgetChannelsListItemGu…ntsBinding.bind(itemView)");
                        this.binding = widgetChannelsListItemGuildScheduledEventsBinding;
                        return;
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemGuildScheduledEvents itemGuildScheduledEvents) {
            return (WidgetChannelsListAdapter) itemGuildScheduledEvents.adapter;
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            final ChannelListItemGuildScheduledEvents channelListItemGuildScheduledEvents = (ChannelListItemGuildScheduledEvents) channelListItem;
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            Context context = view.getContext();
            TextView textView = this.binding.f2277b;
            m.checkNotNullExpressionValue(textView, "binding.channelsListItemGuildScheduledEventCount");
            textView.setVisibility(channelListItemGuildScheduledEvents.getData().isEmpty() ^ true ? 0 : 8);
            TextView textView2 = this.binding.f2277b;
            m.checkNotNullExpressionValue(textView2, "binding.channelsListItemGuildScheduledEventCount");
            textView2.setText(String.valueOf(channelListItemGuildScheduledEvents.getData().size()));
            ConstraintLayout constraintLayout = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout, "binding.root");
            m.checkNotNullExpressionValue(context, "context");
            ViewExtensions.setBackgroundAndKeepPadding(constraintLayout, ContextCompat.getDrawable(context, DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.drawable_overlay_channels, 0, 2, (Object) null)));
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemGuildScheduledEvents$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WidgetChannelsListAdapter.ItemGuildScheduledEvents.access$getAdapter$p(WidgetChannelsListAdapter.ItemGuildScheduledEvents.this).getOnViewGuildScheduledEvents().invoke(channelListItemGuildScheduledEvents.getGuild());
                }
            });
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemHeader;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemHeaderBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemHeaderBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemHeader extends Item {
        private final WidgetChannelsListItemHeaderBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemHeader(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_list_item_header;
            TextView textView = (TextView) view.findViewById(R.id.channels_list_item_header);
            if (textView != null) {
                i2 = R.id.channels_list_new;
                ImageView imageView = (ImageView) view.findViewById(R.id.channels_list_new);
                if (imageView != null) {
                    WidgetChannelsListItemHeaderBinding widgetChannelsListItemHeaderBinding = new WidgetChannelsListItemHeaderBinding((RelativeLayout) view, textView, imageView);
                    m.checkNotNullExpressionValue(widgetChannelsListItemHeaderBinding, "WidgetChannelsListItemHeaderBinding.bind(itemView)");
                    this.binding = widgetChannelsListItemHeaderBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            ChannelListItemHeader channelListItemHeader = (ChannelListItemHeader) channelListItem;
            int component2 = channelListItemHeader.component2();
            boolean component3 = channelListItemHeader.component3();
            final long component4 = channelListItemHeader.component4();
            if (!component3) {
                ImageView imageView = this.binding.c;
                m.checkNotNullExpressionValue(imageView, "binding.channelsListNew");
                imageView.setVisibility(4);
                this.binding.c.setOnClickListener(null);
            } else if (component2 == R.string.others_online) {
                ImageView imageView2 = this.binding.c;
                m.checkNotNullExpressionValue(imageView2, "binding.channelsListNew");
                imageView2.setVisibility(4);
                this.binding.c.setOnClickListener(null);
            } else if (component2 == R.string.text_channels) {
                ImageView imageView3 = this.binding.c;
                m.checkNotNullExpressionValue(imageView3, "binding.channelsListNew");
                imageView3.setVisibility(0);
                this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemHeader$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetCreateChannel.Companion.show$default(WidgetCreateChannel.Companion, a.x(view, "v", "v.context"), component4, 0, null, 8, null);
                    }
                });
            } else if (component2 == R.string.voice_channels) {
                ImageView imageView4 = this.binding.c;
                m.checkNotNullExpressionValue(imageView4, "binding.channelsListNew");
                imageView4.setVisibility(0);
                this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemHeader$onConfigure$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetCreateChannel.Companion.show$default(WidgetCreateChannel.Companion, a.x(view, "v", "v.context"), component4, 2, null, 8, null);
                    }
                });
            }
            this.binding.f2278b.setText(component2);
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "layouResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemInvite extends Item {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemInvite(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemInvite itemInvite) {
            return (WidgetChannelsListAdapter) itemInvite.adapter;
        }

        public void onConfigure(int i, final ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            if (channelListItem instanceof ChannelListItemInvite) {
                this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemInvite$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetGuildInviteShare.Companion.launch(a.x(view, "it", "it.context"), WidgetChannelsListAdapter.ItemInvite.access$getAdapter$p(WidgetChannelsListAdapter.ItemInvite.this).fragmentManager, ((ChannelListItemInvite) channelListItem).getGuildId(), (r22 & 8) != 0 ? null : null, (r22 & 16) != 0 ? false : false, (r22 & 32) != 0 ? null : null, (r22 & 64) != 0 ? null : null, "Directory");
                    }
                });
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                ViewExtensions.setOnLongClickListenerConsumeClick(view, new WidgetChannelsListAdapter$ItemInvite$onConfigure$2(this));
                if (((ChannelListItemInvite) channelListItem).isHub()) {
                    WidgetChannelsListItemDirectoryBinding a = WidgetChannelsListItemDirectoryBinding.a(this.itemView);
                    m.checkNotNullExpressionValue(a, "WidgetChannelsListItemDi…oryBinding.bind(itemView)");
                    a.c.setText(R.string.invite_a_friend);
                    a.f2274b.setImageResource(R.drawable.ic_guild_invite_24dp);
                }
            }
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemMFA;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemMfaBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemMfaBinding;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemMFA extends MGRecyclerViewHolder<WidgetChannelsListAdapter, ChannelListItem> {
        private final WidgetChannelsListItemMfaBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemMFA(@LayoutRes int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.channels_list_item_mfa_text);
            if (linkifiedTextView != null) {
                WidgetChannelsListItemMfaBinding widgetChannelsListItemMfaBinding = new WidgetChannelsListItemMfaBinding((LinearLayout) view, linkifiedTextView);
                m.checkNotNullExpressionValue(widgetChannelsListItemMfaBinding, "WidgetChannelsListItemMfaBinding.bind(itemView)");
                this.binding = widgetChannelsListItemMfaBinding;
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.channels_list_item_mfa_text)));
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            LinkifiedTextView linkifiedTextView = this.binding.f2279b;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.channelsListItemMfaText");
            b.m(linkifiedTextView, R.string.two_fa_guild_mfa_warning, new Object[0], WidgetChannelsListAdapter$ItemMFA$onConfigure$1.INSTANCE);
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\n\u001a\u00020\u0004\u0012\u0006\u0010\u000b\u001a\u00020\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\t¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemSpace;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemSpace extends MGRecyclerViewHolder<WidgetChannelsListAdapter, ChannelListItem> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemSpace(@LayoutRes int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.height = ((WidgetChannelsListAdapter) this.adapter).bottomNavHeight;
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            view2.setLayoutParams(layoutParams);
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemStageActiveEvent;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemActiveEventBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemActiveEventBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemStageActiveEvent extends Item {
        private final WidgetChannelsListItemActiveEventBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemStageActiveEvent(int i, final WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channel_separator;
            ImageView imageView = (ImageView) view.findViewById(R.id.channel_separator);
            if (imageView != null) {
                i2 = R.id.event_channel;
                TextView textView = (TextView) view.findViewById(R.id.event_channel);
                if (textView != null) {
                    i2 = R.id.event_connect_button;
                    MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.event_connect_button);
                    if (materialButton != null) {
                        i2 = R.id.event_topic;
                        TextView textView2 = (TextView) view.findViewById(R.id.event_topic);
                        if (textView2 != null) {
                            i2 = R.id.live_now_dot;
                            ImageView imageView2 = (ImageView) view.findViewById(R.id.live_now_dot);
                            if (imageView2 != null) {
                                i2 = R.id.live_now_label;
                                TextView textView3 = (TextView) view.findViewById(R.id.live_now_label);
                                if (textView3 != null) {
                                    i2 = R.id.stage_event_listeners;
                                    TextView textView4 = (TextView) view.findViewById(R.id.stage_event_listeners);
                                    if (textView4 != null) {
                                        i2 = R.id.user_summary;
                                        UserSummaryView userSummaryView = (UserSummaryView) view.findViewById(R.id.user_summary);
                                        if (userSummaryView != null) {
                                            i2 = R.id.user_summary_label;
                                            MaterialTextView materialTextView = (MaterialTextView) view.findViewById(R.id.user_summary_label);
                                            if (materialTextView != null) {
                                                WidgetChannelsListItemActiveEventBinding widgetChannelsListItemActiveEventBinding = new WidgetChannelsListItemActiveEventBinding((ConstraintLayout) view, imageView, textView, materialButton, textView2, imageView2, textView3, textView4, userSummaryView, materialTextView);
                                                m.checkNotNullExpressionValue(widgetChannelsListItemActiveEventBinding, "WidgetChannelsListItemAc…entBinding.bind(itemView)");
                                                this.binding = widgetChannelsListItemActiveEventBinding;
                                                materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter.ItemStageActiveEvent.1
                                                    @Override // android.view.View.OnClickListener
                                                    public final void onClick(View view2) {
                                                        GuildScheduledEventLocationInfo.ChannelLocation channelLocation;
                                                        Channel channel;
                                                        if (ItemStageActiveEvent.this.getAdapterPosition() != -1) {
                                                            ChannelListItem item = widgetChannelsListAdapter.getItem(ItemStageActiveEvent.this.getAdapterPosition());
                                                            if (!(item instanceof ChannelListItemActiveEvent)) {
                                                                item = null;
                                                            }
                                                            ChannelListItemActiveEvent channelListItemActiveEvent = (ChannelListItemActiveEvent) item;
                                                            if (channelListItemActiveEvent != null) {
                                                                GuildScheduledEventLocationInfo locationInfo = channelListItemActiveEvent.getData().getLocationInfo();
                                                                if ((locationInfo instanceof GuildScheduledEventLocationInfo.ChannelLocation) && (channel = (channelLocation = (GuildScheduledEventLocationInfo.ChannelLocation) locationInfo).getChannel()) != null && ChannelUtils.t(channel)) {
                                                                    widgetChannelsListAdapter.getOnCallChannel().invoke(channelLocation.getChannel());
                                                                } else if ((locationInfo instanceof GuildScheduledEventLocationInfo.ExternalLocation) && channelListItemActiveEvent.getData().getEventId() != null) {
                                                                    WidgetGuildScheduledEventDetailsBottomSheet.Companion.showForGuild(widgetChannelsListAdapter.fragmentManager, channelListItemActiveEvent.getData().getEventId().longValue());
                                                                }
                                                            }
                                                        }
                                                    }
                                                });
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        /* JADX WARN: Code restructure failed: missing block: B:30:0x00a1, code lost:
            if (r14 != null) goto L32;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void onConfigure(int r13, com.discord.widgets.channels.list.items.ChannelListItem r14) {
            /*
                Method dump skipped, instructions count: 446
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.list.WidgetChannelsListAdapter.ItemStageActiveEvent.onConfigure(int, com.discord.widgets.channels.list.items.ChannelListItem):void");
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemStageChannelAudienceCount;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemAudienceCountBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemAudienceCountBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemStageChannelAudienceCount extends Item {
        private final WidgetChannelsListItemAudienceCountBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemStageChannelAudienceCount(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.stage_channel_audience_icon;
            ImageView imageView = (ImageView) view.findViewById(R.id.stage_channel_audience_icon);
            if (imageView != null) {
                i2 = R.id.stage_channels_audience_count;
                TextView textView = (TextView) view.findViewById(R.id.stage_channels_audience_count);
                if (textView != null) {
                    WidgetChannelsListItemAudienceCountBinding widgetChannelsListItemAudienceCountBinding = new WidgetChannelsListItemAudienceCountBinding((RelativeLayout) view, imageView, textView);
                    m.checkNotNullExpressionValue(widgetChannelsListItemAudienceCountBinding, "WidgetChannelsListItemAu…untBinding.bind(itemView)");
                    this.binding = widgetChannelsListItemAudienceCountBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public void onConfigure(int i, ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            ChannelListItemStageAudienceCount channelListItemStageAudienceCount = (ChannelListItemStageAudienceCount) channelListItem;
            TextView textView = this.binding.f2268b;
            if (channelListItemStageAudienceCount.getAudienceSize() != 0) {
                b.m(textView, R.string.stage_channel_audience_count, new Object[]{Integer.valueOf(channelListItemStageAudienceCount.getAudienceSize())}, (r4 & 4) != 0 ? b.g.j : null);
            } else {
                textView.setText(R.string.stage_channel_no_audience);
            }
        }
    }

    /* compiled from: WidgetChannelsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemVoiceUser;", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V", "Lcom/discord/databinding/WidgetChannelsListItemVoiceUserBinding;", "binding", "Lcom/discord/databinding/WidgetChannelsListItemVoiceUserBinding;", "layoutResId", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemVoiceUser extends Item {
        private final WidgetChannelsListItemVoiceUserBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemVoiceUser(int i, WidgetChannelsListAdapter widgetChannelsListAdapter) {
            super(i, widgetChannelsListAdapter);
            m.checkNotNullParameter(widgetChannelsListAdapter, "adapter");
            View view = this.itemView;
            int i2 = R.id.channels_item_voice_user_avatar;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.channels_item_voice_user_avatar);
            if (simpleDraweeView != null) {
                i2 = R.id.channels_item_voice_user_camera;
                ImageView imageView = (ImageView) view.findViewById(R.id.channels_item_voice_user_camera);
                if (imageView != null) {
                    i2 = R.id.channels_item_voice_user_headphones;
                    ImageView imageView2 = (ImageView) view.findViewById(R.id.channels_item_voice_user_headphones);
                    if (imageView2 != null) {
                        i2 = R.id.channels_item_voice_user_live;
                        TextView textView = (TextView) view.findViewById(R.id.channels_item_voice_user_live);
                        if (textView != null) {
                            i2 = R.id.channels_item_voice_user_microphone;
                            ImageView imageView3 = (ImageView) view.findViewById(R.id.channels_item_voice_user_microphone);
                            if (imageView3 != null) {
                                i2 = R.id.channels_item_voice_user_name;
                                TextView textView2 = (TextView) view.findViewById(R.id.channels_item_voice_user_name);
                                if (textView2 != null) {
                                    i2 = R.id.indicators_container;
                                    LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.indicators_container);
                                    if (linearLayout != null) {
                                        WidgetChannelsListItemVoiceUserBinding widgetChannelsListItemVoiceUserBinding = new WidgetChannelsListItemVoiceUserBinding((RelativeLayout) view, simpleDraweeView, imageView, imageView2, textView, imageView3, textView2, linearLayout);
                                        m.checkNotNullExpressionValue(widgetChannelsListItemVoiceUserBinding, "WidgetChannelsListItemVo…serBinding.bind(itemView)");
                                        this.binding = widgetChannelsListItemVoiceUserBinding;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }

        public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(ItemVoiceUser itemVoiceUser) {
            return (WidgetChannelsListAdapter) itemVoiceUser.adapter;
        }

        public void onConfigure(int i, final ChannelListItem channelListItem) {
            m.checkNotNullParameter(channelListItem, "data");
            super.onConfigure(i, (int) channelListItem);
            ChannelListItemVoiceUser channelListItemVoiceUser = (ChannelListItemVoiceUser) channelListItem;
            Channel component1 = channelListItemVoiceUser.component1();
            VoiceState component2 = channelListItemVoiceUser.component2();
            User component3 = channelListItemVoiceUser.component3();
            GuildMember component4 = channelListItemVoiceUser.component4();
            boolean component5 = channelListItemVoiceUser.component5();
            boolean component6 = channelListItemVoiceUser.component6();
            SimpleDraweeView simpleDraweeView = this.binding.f2282b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.channelsItemVoiceUserAvatar");
            IconUtils.setIcon$default(simpleDraweeView, component3, R.dimen.avatar_size_small, null, null, channelListItemVoiceUser.getComputed(), 24, null);
            RelativeLayout relativeLayout = this.binding.a;
            m.checkNotNullExpressionValue(relativeLayout, "binding.root");
            ViewExtensions.setOnLongClickListenerConsumeClick(relativeLayout, new WidgetChannelsListAdapter$ItemVoiceUser$onConfigure$1(this, channelListItem));
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListAdapter$ItemVoiceUser$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChannelsListAdapter.ItemVoiceUser.access$getAdapter$p(WidgetChannelsListAdapter.ItemVoiceUser.this).getOnCallChannel().invoke(((ChannelListItemVoiceUser) channelListItem).getChannel());
                }
            });
            TextView textView = this.binding.g;
            m.checkNotNullExpressionValue(textView, "binding.channelsItemVoiceUserName");
            textView.setText(GuildMember.Companion.getNickOrUsername$default(GuildMember.Companion, component3, component4, component1, null, 8, null));
            ImageView imageView = this.binding.c;
            m.checkNotNullExpressionValue(imageView, "binding.channelsItemVoiceUserCamera");
            int i2 = 8;
            imageView.setVisibility(component2.j() ? 0 : 8);
            ImageView imageView2 = this.binding.f;
            m.checkNotNullExpressionValue(imageView2, "binding.channelsItemVoiceUserMicrophone");
            imageView2.setVisibility(d.V0(component2) ? 0 : 8);
            ImageView imageView3 = this.binding.d;
            m.checkNotNullExpressionValue(imageView3, "binding.channelsItemVoiceUserHeadphones");
            boolean z2 = true;
            imageView3.setVisibility(component2.g() || component2.b() ? 0 : 8);
            TextView textView2 = this.binding.e;
            m.checkNotNullExpressionValue(textView2, "binding.channelsItemVoiceUserLive");
            if (!component5 || !component6) {
                z2 = false;
            }
            if (z2) {
                i2 = 0;
            }
            textView2.setVisibility(i2);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsListAdapter(RecyclerView recyclerView, FragmentManager fragmentManager) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        this.fragmentManager = fragmentManager;
    }

    public final Function0<Unit> getOnAddServer() {
        return this.onAddServer;
    }

    public final Function1<Channel, Unit> getOnCallChannel() {
        return this.onCallChannel;
    }

    public final Function2<Channel, Boolean, Unit> getOnCollapseCategory() {
        return this.onCollapseCategory;
    }

    public final Function1<Channel, Unit> getOnSelectChannel() {
        return this.onSelectChannel;
    }

    public final Function1<Channel, Unit> getOnSelectChannelOptions() {
        return this.onSelectChannelOptions;
    }

    public final Function1<Channel, Unit> getOnSelectGuildRoleSubscriptionLockedChannel() {
        return this.onSelectGuildRoleSubscriptionLockedChannel;
    }

    public final Function1<View, Unit> getOnSelectInvite() {
        return this.onSelectInvite;
    }

    public final Function2<User, Channel, Unit> getOnSelectUserOptions() {
        return this.onSelectUserOptions;
    }

    public final Function0<Unit> getOnViewGuildRoleSubscriptions() {
        return this.onViewGuildRoleSubscriptions;
    }

    public final Function1<Guild, Unit> getOnViewGuildScheduledEvents() {
        return this.onViewGuildScheduledEvents;
    }

    public final long getSelectedGuildId() {
        return this.selectedGuildId;
    }

    public final void handleBottomNavHeight(int i) {
        this.bottomNavHeight = i;
        notifyItemChanged(getInternalData().lastIndexOf(new ChannelListBottomNavSpaceItem(this.selectedGuildId)));
    }

    public final void setOnAddServer(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onAddServer = function0;
    }

    public final void setOnCallChannel(Function1<? super Channel, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onCallChannel = function1;
    }

    public final void setOnCollapseCategory(Function2<? super Channel, ? super Boolean, Unit> function2) {
        m.checkNotNullParameter(function2, "<set-?>");
        this.onCollapseCategory = function2;
    }

    public final void setOnSelectChannel(Function1<? super Channel, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onSelectChannel = function1;
    }

    public final void setOnSelectChannelOptions(Function1<? super Channel, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onSelectChannelOptions = function1;
    }

    public final void setOnSelectGuildRoleSubscriptionLockedChannel(Function1<? super Channel, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onSelectGuildRoleSubscriptionLockedChannel = function1;
    }

    public final void setOnSelectInvite(Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onSelectInvite = function1;
    }

    public final void setOnSelectUserOptions(Function2<? super User, ? super Channel, Unit> function2) {
        m.checkNotNullParameter(function2, "<set-?>");
        this.onSelectUserOptions = function2;
    }

    public final void setOnViewGuildRoleSubscriptions(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onViewGuildRoleSubscriptions = function0;
    }

    public final void setOnViewGuildScheduledEvents(Function1<? super Guild, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onViewGuildScheduledEvents = function1;
    }

    public final void setSelectedGuildId(long j) {
        this.selectedGuildId = j;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, ChannelListItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
        MGRecyclerViewHolder<?, ChannelListItem> mGRecyclerViewHolder;
        m.checkNotNullParameter(viewGroup, "parent");
        switch (i) {
            case 0:
                return new ItemChannelText(R.layout.widget_channels_list_item_channel, this);
            case 1:
                return new ItemChannelVoice(R.layout.widget_channels_list_item_channel_voice, this);
            case 2:
                return new ItemVoiceUser(R.layout.widget_channels_list_item_voice_user, this);
            case 3:
                return new ItemChannelPrivate(R.layout.widget_channels_list_item_channel_private, this);
            case 4:
            case 5:
            case 10:
            default:
                throw invalidViewTypeException(i);
            case 6:
                return new ItemHeader(R.layout.widget_channels_list_item_header, this);
            case 7:
                return new ItemMFA(R.layout.widget_channels_list_item_mfa, this);
            case 8:
                return new ItemChannelCategory(R.layout.widget_channels_list_item_category, this);
            case 9:
                return new ItemInvite(R.layout.widget_channels_list_item_invite, this);
            case 11:
                return new ItemSpace(R.layout.recycler_item_bottom_nav_space, this);
            case 12:
                return new ItemChannelThread(R.layout.widget_channels_list_item_thread, this);
            case 13:
                return new ItemChannelStageVoice(R.layout.widget_channels_list_item_channel_stage_voice, this);
            case 14:
                return new MGRecyclerViewHolder<>((int) R.layout.widget_channels_list_item_stage_events_separator, this);
            case 15:
                return new ItemStageActiveEvent(R.layout.widget_channels_list_item_active_event, this);
            case 16:
                return new ItemStageChannelAudienceCount(R.layout.widget_channels_list_item_audience_count, this);
            case 17:
                mGRecyclerViewHolder = new ItemChannelDirectory(R.layout.widget_channels_list_item_directory, this);
                break;
            case 18:
                mGRecyclerViewHolder = new ItemChannelAddServer(R.layout.widget_channels_list_item_directory, this);
                break;
            case 19:
                return new ItemGuildScheduledEvents(R.layout.widget_channels_list_item_guild_scheduled_events, this);
            case 20:
                mGRecyclerViewHolder = new ItemInvite(R.layout.widget_channels_list_item_directory, this);
                break;
            case 21:
                return new ItemGuildRoleSubscriptionsOverview(R.layout.widget_channels_list_item_guild_role_subs, this);
            case 22:
                return new ItemGuildJoinRequest(R.layout.widget_channels_list_item_guild_join_request, this);
        }
        return mGRecyclerViewHolder;
    }
}
