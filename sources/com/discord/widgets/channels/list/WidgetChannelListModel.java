package com.discord.widgets.channels.list;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.channel.ChannelUtils$getSortByMostRecent$1;
import com.discord.api.directory.DirectoryEntryEvent;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.api.guildjoinrequest.GuildJoinRequest;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreMentions;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.StoreMessagesMostRecent;
import com.discord.stores.StoreNux;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadsActiveJoined;
import com.discord.stores.StoreThreadsJoined;
import com.discord.stores.StoreUserConnections;
import com.discord.stores.StoreUserGuildSettings;
import com.discord.stores.StoreUserPresence;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.stores.utilities.RestCallState;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.channels.list.WidgetChannelListModel;
import com.discord.widgets.channels.list.items.ChannelListBottomNavSpaceItem;
import com.discord.widgets.channels.list.items.ChannelListItem;
import com.discord.widgets.channels.list.items.ChannelListItemCategory;
import com.discord.widgets.channels.list.items.ChannelListItemEventsSeparator;
import com.discord.widgets.channels.list.items.ChannelListItemGuildJoinRequest;
import com.discord.widgets.channels.list.items.ChannelListItemInvite;
import com.discord.widgets.channels.list.items.ChannelListItemMfaNotice;
import com.discord.widgets.channels.list.items.ChannelListItemPrivate;
import com.discord.widgets.channels.list.items.ChannelListItemVoiceUser;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionsFeatureFlag;
import com.discord.widgets.stage.model.StageChannel;
import d0.t.n;
import d0.t.o;
import d0.t.q;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func6;
/* compiled from: WidgetChannelListModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0015\b\u0086\b\u0018\u0000 -2\u00020\u0001:\u0003-./BM\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\u0012\u001a\u00020\t\u0012\b\b\u0002\u0010\u0013\u001a\u00020\t\u0012\b\b\u0002\u0010\u0014\u001a\u00020\t\u0012\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005¢\u0006\u0004\b+\u0010,J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\r\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005HÆ\u0003¢\u0006\u0004\b\u000f\u0010\bJZ\u0010\u0016\u001a\u00020\u00002\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0012\u001a\u00020\t2\b\b\u0002\u0010\u0013\u001a\u00020\t2\b\b\u0002\u0010\u0014\u001a\u00020\t2\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005HÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010\u001f\u001a\u00020\t2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b\u0012\u0010\u000bR\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\"\u001a\u0004\b#\u0010\bR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b$\u0010\bR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010%\u001a\u0004\b&\u0010\u0004R\u0013\u0010(\u001a\u00020\t8F@\u0006¢\u0006\u0006\u001a\u0004\b'\u0010\u000bR\u0019\u0010\u0013\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010!\u001a\u0004\b)\u0010\u000bR\u0019\u0010\u0014\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b*\u0010\u000b¨\u00060"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "component2", "()Ljava/util/List;", "", "component3", "()Z", "component4", "component5", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component6", "selectedGuild", "items", "isGuildSelected", "showPremiumGuildHint", "showEmptyState", "guildScheduledEvents", "copy", "(Lcom/discord/models/guild/Guild;Ljava/util/List;ZZZLjava/util/List;)Lcom/discord/widgets/channels/list/WidgetChannelListModel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/List;", "getGuildScheduledEvents", "getItems", "Lcom/discord/models/guild/Guild;", "getSelectedGuild", "getStartsWithPaddedElement", "startsWithPaddedElement", "getShowPremiumGuildHint", "getShowEmptyState", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/util/List;ZZZLjava/util/List;)V", "Companion", "ThreadSpineType", "VoiceStates", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelListModel {
    public static final Companion Companion = new Companion(null);
    private final List<GuildScheduledEvent> guildScheduledEvents;
    private final boolean isGuildSelected;
    private final List<ChannelListItem> items;
    private final Guild selectedGuild;
    private final boolean showEmptyState;
    private final boolean showPremiumGuildHint;

    /* compiled from: WidgetChannelListModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000¬\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0002?@B\t\b\u0002¢\u0006\u0004\b=\u0010>J\u0081\u0001\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0003\u001a\u00020\u00022\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u001c\u0010\u000e\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\u00042\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\u00042\n\u0010\u0011\u001a\u00060\u0005j\u0002`\u0006H\u0002¢\u0006\u0004\b\u0013\u0010\u0014JÁ\u0003\u00104\u001a\b\u0012\u0004\u0012\u0002030\t2\n\u0010\u0016\u001a\u00060\u0005j\u0002`\u00152\u0006\u0010\u0017\u001a\u00020\u00022\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u000f0\u00042&\u0010\u0019\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00180\u00040\u00042\u0016\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u001a0\u00042\u0010\u0010\u001d\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u001c2\b\u0010\u001e\u001a\u0004\u0018\u00010\u000f2\n\u0010\u0011\u001a\u00060\u0005j\u0002`\u00062\u001c\u0010\u000e\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\u00042\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u001f0\u00042\u0010\u0010!\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u001c2\u0010\u0010\"\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u001c2\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00042\u0016\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020#0\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010&\u001a\u00020%2\u0006\u0010'\u001a\u00020%2\"\u0010*\u001a\u001e\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020)0\t0(0\u00042\u0016\u0010,\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020+0\u00042\"\u0010.\u001a\u001e\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020-0\t0(0\u00042\b\u00100\u001a\u0004\u0018\u00010/2\b\u00102\u001a\u0004\u0018\u000101H\u0002¢\u0006\u0004\b4\u00105J\u0015\u00108\u001a\b\u0012\u0004\u0012\u00020706H\u0002¢\u0006\u0004\b8\u00109J!\u0010:\u001a\b\u0012\u0004\u0012\u000207062\n\u0010\u0016\u001a\u00060\u0005j\u0002`\u0015H\u0002¢\u0006\u0004\b:\u0010;J\u0013\u0010<\u001a\b\u0012\u0004\u0012\u00020706¢\u0006\u0004\b<\u00109¨\u0006A"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;", "", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "guildChannelsInfo", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/widgets/stage/model/StageChannel;", "stageChannels", "", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvents", "", "Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceUser;", "voiceStates", "Lcom/discord/api/channel/Channel;", "guildChannels", "selectedVoiceChannelId", "Lcom/discord/widgets/channels/list/items/ChannelListItemActiveEventData;", "getChannelEventNoticeData", "(Lcom/discord/utilities/channel/GuildChannelsInfo;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;J)Lcom/discord/widgets/channels/list/items/ChannelListItemActiveEventData;", "Lcom/discord/primitives/GuildId;", "selectedGuildId", "guild", "Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "activeJoinedGuildThreads", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "joinedThreads", "", "channelsWithActiveThreads", "selectedChannel", "", "mentionCounts", "unreadChannelIds", "collapsedCategories", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstances", "", "canCreateAnyEvent", "canSeeGuildRoleSubscriptions", "Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/directory/DirectoryEntryGuild;", "directories", "Lcom/discord/stores/StoreMessageAck$Ack;", "messageAcks", "Lcom/discord/api/directory/DirectoryEntryEvent;", "directoryEvents", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "guildJoinRequest", "Lcom/discord/models/member/GuildMember;", "member", "Lcom/discord/widgets/channels/list/items/ChannelListItem;", "guildListBuilder", "(JLcom/discord/utilities/channel/GuildChannelsInfo;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/discord/api/channel/Channel;JLjava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZLjava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/api/guildjoinrequest/GuildJoinRequest;Lcom/discord/models/member/GuildMember;)Ljava/util/List;", "Lrx/Observable;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel;", "getPrivateChannelList", "()Lrx/Observable;", "getSelectedGuildChannelList", "(J)Lrx/Observable;", "get", HookHelper.constructorName, "()V", "TextLikeChannelData", "VocalChannelData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetChannelListModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004JB\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0007J\u001a\u0010\u0017\u001a\u00020\u00022\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001f\u0010\u0004¨\u0006\""}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;", "", "", "component1", "()Z", "", "component2", "()I", "component3", "component4", "component5", "selected", "mentionCount", "unread", ModelAuditLogEntry.CHANGE_KEY_LOCKED, "hide", "copy", "(ZIZZZ)Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getHide", "getUnread", "I", "getMentionCount", "getSelected", "getLocked", HookHelper.constructorName, "(ZIZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class TextLikeChannelData {
            private final boolean hide;
            private final boolean locked;
            private final int mentionCount;
            private final boolean selected;
            private final boolean unread;

            public TextLikeChannelData(boolean z2, int i, boolean z3, boolean z4, boolean z5) {
                this.selected = z2;
                this.mentionCount = i;
                this.unread = z3;
                this.locked = z4;
                this.hide = z5;
            }

            public static /* synthetic */ TextLikeChannelData copy$default(TextLikeChannelData textLikeChannelData, boolean z2, int i, boolean z3, boolean z4, boolean z5, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    z2 = textLikeChannelData.selected;
                }
                if ((i2 & 2) != 0) {
                    i = textLikeChannelData.mentionCount;
                }
                int i3 = i;
                if ((i2 & 4) != 0) {
                    z3 = textLikeChannelData.unread;
                }
                boolean z6 = z3;
                if ((i2 & 8) != 0) {
                    z4 = textLikeChannelData.locked;
                }
                boolean z7 = z4;
                if ((i2 & 16) != 0) {
                    z5 = textLikeChannelData.hide;
                }
                return textLikeChannelData.copy(z2, i3, z6, z7, z5);
            }

            public final boolean component1() {
                return this.selected;
            }

            public final int component2() {
                return this.mentionCount;
            }

            public final boolean component3() {
                return this.unread;
            }

            public final boolean component4() {
                return this.locked;
            }

            public final boolean component5() {
                return this.hide;
            }

            public final TextLikeChannelData copy(boolean z2, int i, boolean z3, boolean z4, boolean z5) {
                return new TextLikeChannelData(z2, i, z3, z4, z5);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof TextLikeChannelData)) {
                    return false;
                }
                TextLikeChannelData textLikeChannelData = (TextLikeChannelData) obj;
                return this.selected == textLikeChannelData.selected && this.mentionCount == textLikeChannelData.mentionCount && this.unread == textLikeChannelData.unread && this.locked == textLikeChannelData.locked && this.hide == textLikeChannelData.hide;
            }

            public final boolean getHide() {
                return this.hide;
            }

            public final boolean getLocked() {
                return this.locked;
            }

            public final int getMentionCount() {
                return this.mentionCount;
            }

            public final boolean getSelected() {
                return this.selected;
            }

            public final boolean getUnread() {
                return this.unread;
            }

            public int hashCode() {
                boolean z2 = this.selected;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = ((i2 * 31) + this.mentionCount) * 31;
                boolean z3 = this.unread;
                if (z3) {
                    z3 = true;
                }
                int i5 = z3 ? 1 : 0;
                int i6 = z3 ? 1 : 0;
                int i7 = (i4 + i5) * 31;
                boolean z4 = this.locked;
                if (z4) {
                    z4 = true;
                }
                int i8 = z4 ? 1 : 0;
                int i9 = z4 ? 1 : 0;
                int i10 = (i7 + i8) * 31;
                boolean z5 = this.hide;
                if (!z5) {
                    i = z5 ? 1 : 0;
                }
                return i10 + i;
            }

            public String toString() {
                StringBuilder R = a.R("TextLikeChannelData(selected=");
                R.append(this.selected);
                R.append(", mentionCount=");
                R.append(this.mentionCount);
                R.append(", unread=");
                R.append(this.unread);
                R.append(", locked=");
                R.append(this.locked);
                R.append(", hide=");
                return a.M(R, this.hide, ")");
            }
        }

        /* compiled from: WidgetChannelListModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0006\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u000e\u0010\u0019\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r\u0012\u0006\u0010\u001a\u001a\u00020\u0006\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\u0006\u0010\u001c\u001a\u00020\u0002¢\u0006\u0004\b1\u00102J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0018\u0010\u000e\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0010\u0010\bJ\u0010\u0010\u0011\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0004J|\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00062\b\b\u0002\u0010\u0016\u001a\u00020\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00022\u0010\b\u0002\u0010\u0019\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\u001b\u001a\u00020\u00022\b\b\u0002\u0010\u001c\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010\"\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\"\u0010\bJ\u001a\u0010$\u001a\u00020\u00022\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010&\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b'\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010&\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010&\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010+\u001a\u0004\b,\u0010\bR!\u0010\u0019\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010-\u001a\u0004\b.\u0010\u000fR\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b/\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010+\u001a\u0004\b0\u0010\b¨\u00063"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$VocalChannelData;", "", "", "component1", "()Z", "component2", "", "component3", "()I", "component4", "component5", "component6", "", "Lcom/discord/api/permission/PermissionBit;", "component7", "()Ljava/lang/Long;", "component8", "component9", "component10", "voiceChannelSelected", "textChannelSelected", "mentionCount", "unread", ModelAuditLogEntry.CHANGE_KEY_LOCKED, ModelAuditLogEntry.CHANGE_KEY_NSFW, "permission", "numUsersConnected", "isGuildRoleSubscriptionLockedChannel", "isGuildRoleSubscriptionChannel", "copy", "(ZZIZZZLjava/lang/Long;IZZ)Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$VocalChannelData;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getVoiceChannelSelected", "getLocked", "getNsfw", "getTextChannelSelected", "I", "getNumUsersConnected", "Ljava/lang/Long;", "getPermission", "getUnread", "getMentionCount", HookHelper.constructorName, "(ZZIZZZLjava/lang/Long;IZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class VocalChannelData {
            private final boolean isGuildRoleSubscriptionChannel;
            private final boolean isGuildRoleSubscriptionLockedChannel;
            private final boolean locked;
            private final int mentionCount;
            private final boolean nsfw;
            private final int numUsersConnected;
            private final Long permission;
            private final boolean textChannelSelected;
            private final boolean unread;
            private final boolean voiceChannelSelected;

            public VocalChannelData(boolean z2, boolean z3, int i, boolean z4, boolean z5, boolean z6, Long l, int i2, boolean z7, boolean z8) {
                this.voiceChannelSelected = z2;
                this.textChannelSelected = z3;
                this.mentionCount = i;
                this.unread = z4;
                this.locked = z5;
                this.nsfw = z6;
                this.permission = l;
                this.numUsersConnected = i2;
                this.isGuildRoleSubscriptionLockedChannel = z7;
                this.isGuildRoleSubscriptionChannel = z8;
            }

            public final boolean component1() {
                return this.voiceChannelSelected;
            }

            public final boolean component10() {
                return this.isGuildRoleSubscriptionChannel;
            }

            public final boolean component2() {
                return this.textChannelSelected;
            }

            public final int component3() {
                return this.mentionCount;
            }

            public final boolean component4() {
                return this.unread;
            }

            public final boolean component5() {
                return this.locked;
            }

            public final boolean component6() {
                return this.nsfw;
            }

            public final Long component7() {
                return this.permission;
            }

            public final int component8() {
                return this.numUsersConnected;
            }

            public final boolean component9() {
                return this.isGuildRoleSubscriptionLockedChannel;
            }

            public final VocalChannelData copy(boolean z2, boolean z3, int i, boolean z4, boolean z5, boolean z6, Long l, int i2, boolean z7, boolean z8) {
                return new VocalChannelData(z2, z3, i, z4, z5, z6, l, i2, z7, z8);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof VocalChannelData)) {
                    return false;
                }
                VocalChannelData vocalChannelData = (VocalChannelData) obj;
                return this.voiceChannelSelected == vocalChannelData.voiceChannelSelected && this.textChannelSelected == vocalChannelData.textChannelSelected && this.mentionCount == vocalChannelData.mentionCount && this.unread == vocalChannelData.unread && this.locked == vocalChannelData.locked && this.nsfw == vocalChannelData.nsfw && m.areEqual(this.permission, vocalChannelData.permission) && this.numUsersConnected == vocalChannelData.numUsersConnected && this.isGuildRoleSubscriptionLockedChannel == vocalChannelData.isGuildRoleSubscriptionLockedChannel && this.isGuildRoleSubscriptionChannel == vocalChannelData.isGuildRoleSubscriptionChannel;
            }

            public final boolean getLocked() {
                return this.locked;
            }

            public final int getMentionCount() {
                return this.mentionCount;
            }

            public final boolean getNsfw() {
                return this.nsfw;
            }

            public final int getNumUsersConnected() {
                return this.numUsersConnected;
            }

            public final Long getPermission() {
                return this.permission;
            }

            public final boolean getTextChannelSelected() {
                return this.textChannelSelected;
            }

            public final boolean getUnread() {
                return this.unread;
            }

            public final boolean getVoiceChannelSelected() {
                return this.voiceChannelSelected;
            }

            public int hashCode() {
                boolean z2 = this.voiceChannelSelected;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                boolean z3 = this.textChannelSelected;
                if (z3) {
                    z3 = true;
                }
                int i5 = z3 ? 1 : 0;
                int i6 = z3 ? 1 : 0;
                int i7 = (((i4 + i5) * 31) + this.mentionCount) * 31;
                boolean z4 = this.unread;
                if (z4) {
                    z4 = true;
                }
                int i8 = z4 ? 1 : 0;
                int i9 = z4 ? 1 : 0;
                int i10 = (i7 + i8) * 31;
                boolean z5 = this.locked;
                if (z5) {
                    z5 = true;
                }
                int i11 = z5 ? 1 : 0;
                int i12 = z5 ? 1 : 0;
                int i13 = (i10 + i11) * 31;
                boolean z6 = this.nsfw;
                if (z6) {
                    z6 = true;
                }
                int i14 = z6 ? 1 : 0;
                int i15 = z6 ? 1 : 0;
                int i16 = (i13 + i14) * 31;
                Long l = this.permission;
                int hashCode = (((i16 + (l != null ? l.hashCode() : 0)) * 31) + this.numUsersConnected) * 31;
                boolean z7 = this.isGuildRoleSubscriptionLockedChannel;
                if (z7) {
                    z7 = true;
                }
                int i17 = z7 ? 1 : 0;
                int i18 = z7 ? 1 : 0;
                int i19 = (hashCode + i17) * 31;
                boolean z8 = this.isGuildRoleSubscriptionChannel;
                if (!z8) {
                    i = z8 ? 1 : 0;
                }
                return i19 + i;
            }

            public final boolean isGuildRoleSubscriptionChannel() {
                return this.isGuildRoleSubscriptionChannel;
            }

            public final boolean isGuildRoleSubscriptionLockedChannel() {
                return this.isGuildRoleSubscriptionLockedChannel;
            }

            public String toString() {
                StringBuilder R = a.R("VocalChannelData(voiceChannelSelected=");
                R.append(this.voiceChannelSelected);
                R.append(", textChannelSelected=");
                R.append(this.textChannelSelected);
                R.append(", mentionCount=");
                R.append(this.mentionCount);
                R.append(", unread=");
                R.append(this.unread);
                R.append(", locked=");
                R.append(this.locked);
                R.append(", nsfw=");
                R.append(this.nsfw);
                R.append(", permission=");
                R.append(this.permission);
                R.append(", numUsersConnected=");
                R.append(this.numUsersConnected);
                R.append(", isGuildRoleSubscriptionLockedChannel=");
                R.append(this.isGuildRoleSubscriptionLockedChannel);
                R.append(", isGuildRoleSubscriptionChannel=");
                return a.M(R, this.isGuildRoleSubscriptionChannel, ")");
            }
        }

        private Companion() {
        }

        /* JADX WARN: Removed duplicated region for block: B:103:0x0195 A[EDGE_INSN: B:103:0x0195->B:83:0x0195 ?: BREAK  , SYNTHETIC] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        private final com.discord.widgets.channels.list.items.ChannelListItemActiveEventData getChannelEventNoticeData(com.discord.utilities.channel.GuildChannelsInfo r17, java.util.Map<java.lang.Long, com.discord.widgets.stage.model.StageChannel> r18, java.util.List<com.discord.api.guildscheduledevent.GuildScheduledEvent> r19, java.util.Map<java.lang.Long, ? extends java.util.Collection<com.discord.widgets.channels.list.items.ChannelListItemVoiceUser>> r20, java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r21, long r22) {
            /*
                Method dump skipped, instructions count: 455
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.list.WidgetChannelListModel.Companion.getChannelEventNoticeData(com.discord.utilities.channel.GuildChannelsInfo, java.util.Map, java.util.List, java.util.Map, java.util.Map, long):com.discord.widgets.channels.list.items.ChannelListItemActiveEventData");
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<WidgetChannelListModel> getPrivateChannelList() {
            StoreStream.Companion companion = StoreStream.Companion;
            final StoreChannels channels = companion.getChannels();
            final StoreChannelsSelected channelsSelected = companion.getChannelsSelected();
            final StoreUserPresence presences = companion.getPresences();
            final StoreMessagesMostRecent messagesMostRecent = companion.getMessagesMostRecent();
            final StoreMentions mentions = companion.getMentions();
            final StoreApplicationStreaming applicationStreaming = companion.getApplicationStreaming();
            final StoreUserGuildSettings userGuildSettings = companion.getUserGuildSettings();
            final StoreExperiments experiments = companion.getExperiments();
            final StoreUserConnections userConnections = companion.getUserConnections();
            Observable<WidgetChannelListModel> F = ObservableExtensionsKt.leadingEdgeThrottle(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{channels, channelsSelected, presences, messagesMostRecent, mentions, applicationStreaming, userGuildSettings, experiments, userConnections}, false, null, null, 14, null), 250L, TimeUnit.MILLISECONDS).F(new b<Unit, WidgetChannelListModel>() { // from class: com.discord.widgets.channels.list.WidgetChannelListModel$Companion$getPrivateChannelList$1
                public final WidgetChannelListModel call(Unit unit) {
                    Map<Long, Channel> channelsForGuild = StoreChannels.this.getChannelsForGuild(0L);
                    Channel.Companion companion2 = Channel.Companion;
                    Map<Long, Long> mostRecentIds = messagesMostRecent.getMostRecentIds();
                    m.checkNotNullParameter(companion2, "$this$getSortByMostRecent");
                    m.checkNotNullParameter(mostRecentIds, "mostRecentMessageIds");
                    ChannelUtils$getSortByMostRecent$1 channelUtils$getSortByMostRecent$1 = new ChannelUtils$getSortByMostRecent$1(mostRecentIds);
                    long id2 = channelsSelected.getId();
                    Map<Long, Presence> presences2 = presences.m14getPresences();
                    Map<Long, Integer> mentionCounts = mentions.getMentionCounts();
                    Map<Long, ModelApplicationStream> streamsByUser = applicationStreaming.getStreamsByUser();
                    ModelNotificationSettings modelNotificationSettings = userGuildSettings.getGuildSettings().get(0L);
                    if (modelNotificationSettings == null) {
                        modelNotificationSettings = new ModelNotificationSettings();
                    }
                    experiments.getUserExperiment("2021-04_contact_sync_android_main", true);
                    userConnections.getConnectedAccounts();
                    List sortedWith = u.sortedWith(channelsForGuild.values(), channelUtils$getSortByMostRecent$1);
                    ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(sortedWith, 10));
                    Iterator<T> it = sortedWith.iterator();
                    while (true) {
                        Long l = null;
                        if (it.hasNext()) {
                            Channel channel = (Channel) it.next();
                            User a = ChannelUtils.a(channel);
                            Presence presence = presences2.get(a != null ? Long.valueOf(a.getId()) : null);
                            boolean z2 = channel.h() == id2;
                            Integer num = (Integer) a.c(channel, mentionCounts);
                            int intValue = num != null ? num.intValue() : 0;
                            User a2 = ChannelUtils.a(channel);
                            if (a2 != null) {
                                l = Long.valueOf(a2.getId());
                            }
                            boolean containsKey = streamsByUser.containsKey(l);
                            ModelNotificationSettings.ChannelOverride channelOverride = modelNotificationSettings.getChannelOverride(channel.h());
                            arrayList.add(new ChannelListItemPrivate(channel, presence, z2, intValue, containsKey, channelOverride != null ? channelOverride.isMuted() : false));
                        } else {
                            return new WidgetChannelListModel(null, u.plus((Collection<? extends ChannelListBottomNavSpaceItem>) arrayList, new ChannelListBottomNavSpaceItem(0L, 1, null)), false, false, arrayList.isEmpty(), null, 44, null);
                        }
                    }
                }
            });
            m.checkNotNullExpressionValue(F, "ObservationDeckProvider\n…            )\n          }");
            return F;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<WidgetChannelListModel> getSelectedGuildChannelList(long j) {
            StoreStream.Companion companion = StoreStream.Companion;
            long id2 = companion.getUsers().getMe().getId();
            Observable<GuildChannelsInfo> observable = GuildChannelsInfo.Companion.get(j);
            Observable observeChannelsForGuild$default = StoreChannels.observeChannelsForGuild$default(companion.getChannels(), j, null, 2, null);
            Observable<Map<Long, StoreThreadsJoined.JoinedThread>> observeJoinedThreads = companion.getThreadsJoined().observeJoinedThreads();
            Observable<Map<Long, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>>> observeActiveJoinedThreadsForGuild = companion.getThreadsActiveJoined().observeActiveJoinedThreadsForGuild(j);
            Observable<Set<Long>> observeChannelsWithActiveThreadsByGuild = companion.getThreadsActive().observeChannelsWithActiveThreadsByGuild(j);
            Observable<Channel> observeSelectedChannel = companion.getChannelsSelected().observeSelectedChannel();
            Observable<Long> observeSelectedVoiceChannelId = companion.getVoiceChannelSelected().observeSelectedVoiceChannelId();
            Observable observable2 = VoiceStates.get$default(VoiceStates.INSTANCE, j, null, 2, null);
            Observable<Map<Long, Integer>> observeMentionCounts = companion.getMentions().observeMentionCounts();
            Observable<Set<Long>> unreadChannelIds = companion.getReadStates().getUnreadChannelIds();
            Observable<Set<Long>> observeCollapsedCategories = companion.getStoreChannelCategories().observeCollapsedCategories(j);
            Observable<StoreNux.NuxState> nuxState = companion.getNux().getNuxState();
            Observable<Map<Long, StageChannel>> observeGuildStageChannels = companion.getStageChannels().observeGuildStageChannels(j);
            Observable<Map<Long, StageInstance>> observeStageInstancesForGuild = companion.getStageInstances().observeStageInstancesForGuild(j);
            Observable observeGuildScheduledEvents$default = StoreGuildScheduledEvents.observeGuildScheduledEvents$default(companion.getGuildScheduledEvents(), j, false, 2, null);
            Observable observeCanCreateAnyEvent$default = GuildScheduledEventUtilities.Companion.observeCanCreateAnyEvent$default(GuildScheduledEventUtilities.Companion, j, null, null, null, 14, null);
            Observable<Boolean> observeCanGuildSeeGuildRoleSubscriptions = GuildRoleSubscriptionsFeatureFlag.Companion.getINSTANCE().observeCanGuildSeeGuildRoleSubscriptions(j);
            Observable<Map<Long, RestCallState<List<DirectoryEntryGuild>>>> observeDirectories = companion.getDirectories().observeDirectories();
            Observable<Map<Long, StoreMessageAck.Ack>> observeAll = companion.getMessageAck().observeAll();
            Observable<Map<Long, RestCallState<List<DirectoryEntryEvent>>>> observeDirectoryGuildScheduledEvents = companion.getDirectories().observeDirectoryGuildScheduledEvents();
            Observable<GuildJoinRequest> observeGuildJoinRequest = companion.getGuildJoinRequests().observeGuildJoinRequest(j);
            Observable<GuildMember> observeGuildMember = companion.getGuilds().observeGuildMember(j, id2);
            m.checkNotNullExpressionValue(observeGuildMember, "StoreStream\n            …er(selectedGuildId, meId)");
            return ObservableCombineLatestOverloadsKt.combineLatest(observable, observeChannelsForGuild$default, observeJoinedThreads, observeActiveJoinedThreadsForGuild, observeChannelsWithActiveThreadsByGuild, observeSelectedChannel, observeSelectedVoiceChannelId, observable2, observeMentionCounts, unreadChannelIds, observeCollapsedCategories, nuxState, observeGuildStageChannels, observeStageInstancesForGuild, observeGuildScheduledEvents$default, observeCanCreateAnyEvent$default, observeCanGuildSeeGuildRoleSubscriptions, observeDirectories, observeAll, observeDirectoryGuildScheduledEvents, observeGuildJoinRequest, observeGuildMember, new WidgetChannelListModel$Companion$getSelectedGuildChannelList$1(j));
        }

        /*  JADX ERROR: IndexOutOfBoundsException in pass: SSATransform
            java.lang.IndexOutOfBoundsException: bitIndex < 0: -61
            	at java.base/java.util.BitSet.get(BitSet.java:626)
            	at jadx.core.dex.visitors.ssa.LiveVarAnalysis.fillBasicBlockInfo(LiveVarAnalysis.java:65)
            	at jadx.core.dex.visitors.ssa.LiveVarAnalysis.runAnalysis(LiveVarAnalysis.java:36)
            	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:55)
            	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
            */
        /* JADX INFO: Access modifiers changed from: private */
        public final java.util.List<com.discord.widgets.channels.list.items.ChannelListItem> guildListBuilder(long r178, com.discord.utilities.channel.GuildChannelsInfo r180, java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r181, java.util.Map<java.lang.Long, ? extends java.util.Map<java.lang.Long, com.discord.stores.StoreThreadsActiveJoined.ActiveJoinedThread>> r182, java.util.Map<java.lang.Long, com.discord.stores.StoreThreadsJoined.JoinedThread> r183, java.util.Set<java.lang.Long> r184, com.discord.api.channel.Channel r185, long r186, java.util.Map<java.lang.Long, ? extends java.util.Collection<com.discord.widgets.channels.list.items.ChannelListItemVoiceUser>> r188, java.util.Map<java.lang.Long, java.lang.Integer> r189, java.util.Set<java.lang.Long> r190, java.util.Set<java.lang.Long> r191, java.util.Map<java.lang.Long, com.discord.widgets.stage.model.StageChannel> r192, java.util.Map<java.lang.Long, com.discord.api.stageinstance.StageInstance> r193, java.util.List<com.discord.api.guildscheduledevent.GuildScheduledEvent> r194, boolean r195, boolean r196, java.util.Map<java.lang.Long, ? extends com.discord.stores.utilities.RestCallState<? extends java.util.List<com.discord.api.directory.DirectoryEntryGuild>>> r197, java.util.Map<java.lang.Long, com.discord.stores.StoreMessageAck.Ack> r198, java.util.Map<java.lang.Long, ? extends com.discord.stores.utilities.RestCallState<? extends java.util.List<com.discord.api.directory.DirectoryEntryEvent>>> r199, com.discord.api.guildjoinrequest.GuildJoinRequest r200, com.discord.models.member.GuildMember r201) {
            /*
                Method dump skipped, instructions count: 1756
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.list.WidgetChannelListModel.Companion.guildListBuilder(long, com.discord.utilities.channel.GuildChannelsInfo, java.util.Map, java.util.Map, java.util.Map, java.util.Set, com.discord.api.channel.Channel, long, java.util.Map, java.util.Map, java.util.Set, java.util.Set, java.util.Map, java.util.Map, java.util.List, boolean, boolean, java.util.Map, java.util.Map, java.util.Map, com.discord.api.guildjoinrequest.GuildJoinRequest, com.discord.models.member.GuildMember):java.util.List");
        }

        public final Observable<WidgetChannelListModel> get() {
            Observable<WidgetChannelListModel> q = StoreStream.Companion.getGuildSelected().observeSelectedGuildId().Y(WidgetChannelListModel$Companion$get$1.INSTANCE).q();
            m.checkNotNullExpressionValue(q, "StoreStream\n          .g…  .distinctUntilChanged()");
            return q;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelListModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType;", "", HookHelper.constructorName, "()V", "End", "Middle", "Single", "Start", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$Start;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$Middle;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$End;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$Single;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ThreadSpineType {

        /* compiled from: WidgetChannelListModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$End;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class End extends ThreadSpineType {
            public static final End INSTANCE = new End();

            private End() {
                super(null);
            }
        }

        /* compiled from: WidgetChannelListModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$Middle;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Middle extends ThreadSpineType {
            public static final Middle INSTANCE = new Middle();

            private Middle() {
                super(null);
            }
        }

        /* compiled from: WidgetChannelListModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$Single;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Single extends ThreadSpineType {
            public static final Single INSTANCE = new Single();

            private Single() {
                super(null);
            }
        }

        /* compiled from: WidgetChannelListModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType$Start;", "Lcom/discord/widgets/channels/list/WidgetChannelListModel$ThreadSpineType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Start extends ThreadSpineType {
            public static final Start INSTANCE = new Start();

            private Start() {
                super(null);
            }
        }

        private ThreadSpineType() {
        }

        public /* synthetic */ ThreadSpineType(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelListModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b \u0010!J\u001f\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u0004H\u0002¢\u0006\u0004\b\u0005\u0010\u0006JÑ\u0001\u0010\u0019\u001a\u0018\u0012\b\u0012\u00060\bj\u0002`\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00180\u00072\u0016\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\n0\u00072\u0016\u0010\r\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\f0\u00072\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u000e0\u00072\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u00072\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00130\u00072\u001a\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\u0010\u0012\b\u0012\u00060\bj\u0002`\u00150\u00072\u0016\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u0004H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJO\u0010\u001e\u001a\u001e\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\bj\u0002`\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00180\u00070\u001d2\n\u0010\u001c\u001a\u00060\bj\u0002`\u001b2\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u0004¢\u0006\u0004\b\u001e\u0010\u001f¨\u0006\""}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListModel$VoiceStates;", "", "Ljava/util/Comparator;", "Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceUser;", "Lkotlin/Comparator;", "createVoiceUserComparator", "()Ljava/util/Comparator;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/voice/state/VoiceState;", "voiceStates", "Lcom/discord/models/user/User;", "users", "Lcom/discord/models/member/GuildMember;", "computed", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channels", "Lcom/discord/models/domain/ModelApplicationStream;", "guildStreams", "Lcom/discord/api/permission/PermissionBit;", "guildPermissions", "voiceUserComparator", "", "createVoiceStates", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Comparator;)Ljava/util/Map;", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "get", "(JLjava/util/Comparator;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class VoiceStates {
        public static final VoiceStates INSTANCE = new VoiceStates();

        private VoiceStates() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Map<Long, List<ChannelListItemVoiceUser>> createVoiceStates(Map<Long, VoiceState> map, Map<Long, ? extends User> map2, Map<Long, GuildMember> map3, Map<Long, Channel> map4, Map<Long, ? extends ModelApplicationStream> map5, Map<Long, Long> map6, Comparator<ChannelListItemVoiceUser> comparator) {
            Long a;
            Channel channel;
            HashMap hashMap = new HashMap();
            for (VoiceState voiceState : map.values()) {
                long m = voiceState.m();
                User user = map2.get(Long.valueOf(m));
                if (user != null && (a = voiceState.a()) != null) {
                    long longValue = a.longValue();
                    boolean can = PermissionUtils.can(Permission.CONNECT, map6.get(Long.valueOf(longValue)));
                    Long valueOf = Long.valueOf(longValue);
                    Object obj = hashMap.get(valueOf);
                    if (obj == null) {
                        obj = new ArrayList();
                        hashMap.put(valueOf, obj);
                    }
                    List list = (List) obj;
                    GuildMember guildMember = map3.get(Long.valueOf(m));
                    if (!(guildMember == null || (channel = map4.get(Long.valueOf(longValue))) == null)) {
                        list.add(new ChannelListItemVoiceUser(channel, voiceState, user, guildMember, map5.containsKey(Long.valueOf(m)), can));
                    }
                }
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                q.sortWith((List) entry.getValue(), comparator);
            }
            return hashMap;
        }

        private final Comparator<ChannelListItemVoiceUser> createVoiceUserComparator() {
            return WidgetChannelListModel$VoiceStates$createVoiceUserComparator$1.INSTANCE;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Observable get$default(VoiceStates voiceStates, long j, Comparator comparator, int i, Object obj) {
            if ((i & 2) != 0) {
                comparator = voiceStates.createVoiceUserComparator();
            }
            return voiceStates.get(j, comparator);
        }

        public final Observable<Map<Long, List<ChannelListItemVoiceUser>>> get(long j, final Comparator<ChannelListItemVoiceUser> comparator) {
            m.checkNotNullParameter(comparator, "voiceUserComparator");
            StoreStream.Companion companion = StoreStream.Companion;
            Observable f = Observable.f(companion.getApplicationStreaming().observeStreamsForGuild(j), ObservableExtensionsKt.leadingEdgeThrottle(companion.getVoiceStates().observe(j), 200L, TimeUnit.MILLISECONDS), companion.getUsers().observeAllUsers(), companion.getGuilds().observeComputed(j), StoreChannels.observeChannelsForGuild$default(companion.getChannels(), j, null, 2, null), companion.getPermissions().observeChannelPermissionsForGuild(j), new Func6<Map<Long, ? extends ModelApplicationStream>, Map<Long, ? extends VoiceState>, Map<Long, ? extends User>, Map<Long, ? extends GuildMember>, Map<Long, ? extends Channel>, Map<Long, ? extends Long>, Map<Long, ? extends List<? extends ChannelListItemVoiceUser>>>() { // from class: com.discord.widgets.channels.list.WidgetChannelListModel$VoiceStates$get$1
                @Override // rx.functions.Func6
                public /* bridge */ /* synthetic */ Map<Long, ? extends List<? extends ChannelListItemVoiceUser>> call(Map<Long, ? extends ModelApplicationStream> map, Map<Long, ? extends VoiceState> map2, Map<Long, ? extends User> map3, Map<Long, ? extends GuildMember> map4, Map<Long, ? extends Channel> map5, Map<Long, ? extends Long> map6) {
                    return call2(map, (Map<Long, VoiceState>) map2, map3, (Map<Long, GuildMember>) map4, (Map<Long, Channel>) map5, (Map<Long, Long>) map6);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Map<Long, List<ChannelListItemVoiceUser>> call2(Map<Long, ? extends ModelApplicationStream> map, Map<Long, VoiceState> map2, Map<Long, ? extends User> map3, Map<Long, GuildMember> map4, Map<Long, Channel> map5, Map<Long, Long> map6) {
                    Map<Long, List<ChannelListItemVoiceUser>> createVoiceStates;
                    WidgetChannelListModel.VoiceStates voiceStates = WidgetChannelListModel.VoiceStates.INSTANCE;
                    m.checkNotNullExpressionValue(map2, "voiceStates");
                    m.checkNotNullExpressionValue(map3, "users");
                    m.checkNotNullExpressionValue(map4, "guildMembers");
                    m.checkNotNullExpressionValue(map5, "guildChannels");
                    m.checkNotNullExpressionValue(map, "guildStreams");
                    m.checkNotNullExpressionValue(map6, "guildPermissions");
                    createVoiceStates = voiceStates.createVoiceStates(map2, map3, map4, map5, map, map6, comparator);
                    return createVoiceStates;
                }
            });
            m.checkNotNullExpressionValue(f, "Observable\n          .co…            )\n          }");
            Observable<Map<Long, List<ChannelListItemVoiceUser>>> q = ObservableExtensionsKt.computationLatest(f).q();
            m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
            return q;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public WidgetChannelListModel(Guild guild, List<? extends ChannelListItem> list, boolean z2, boolean z3, boolean z4, List<GuildScheduledEvent> list2) {
        m.checkNotNullParameter(list, "items");
        m.checkNotNullParameter(list2, "guildScheduledEvents");
        this.selectedGuild = guild;
        this.items = list;
        this.isGuildSelected = z2;
        this.showPremiumGuildHint = z3;
        this.showEmptyState = z4;
        this.guildScheduledEvents = list2;
    }

    public static /* synthetic */ WidgetChannelListModel copy$default(WidgetChannelListModel widgetChannelListModel, Guild guild, List list, boolean z2, boolean z3, boolean z4, List list2, int i, Object obj) {
        if ((i & 1) != 0) {
            guild = widgetChannelListModel.selectedGuild;
        }
        List<ChannelListItem> list3 = list;
        if ((i & 2) != 0) {
            list3 = widgetChannelListModel.items;
        }
        List list4 = list3;
        if ((i & 4) != 0) {
            z2 = widgetChannelListModel.isGuildSelected;
        }
        boolean z5 = z2;
        if ((i & 8) != 0) {
            z3 = widgetChannelListModel.showPremiumGuildHint;
        }
        boolean z6 = z3;
        if ((i & 16) != 0) {
            z4 = widgetChannelListModel.showEmptyState;
        }
        boolean z7 = z4;
        List<GuildScheduledEvent> list5 = list2;
        if ((i & 32) != 0) {
            list5 = widgetChannelListModel.guildScheduledEvents;
        }
        return widgetChannelListModel.copy(guild, list4, z5, z6, z7, list5);
    }

    public final Guild component1() {
        return this.selectedGuild;
    }

    public final List<ChannelListItem> component2() {
        return this.items;
    }

    public final boolean component3() {
        return this.isGuildSelected;
    }

    public final boolean component4() {
        return this.showPremiumGuildHint;
    }

    public final boolean component5() {
        return this.showEmptyState;
    }

    public final List<GuildScheduledEvent> component6() {
        return this.guildScheduledEvents;
    }

    public final WidgetChannelListModel copy(Guild guild, List<? extends ChannelListItem> list, boolean z2, boolean z3, boolean z4, List<GuildScheduledEvent> list2) {
        m.checkNotNullParameter(list, "items");
        m.checkNotNullParameter(list2, "guildScheduledEvents");
        return new WidgetChannelListModel(guild, list, z2, z3, z4, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetChannelListModel)) {
            return false;
        }
        WidgetChannelListModel widgetChannelListModel = (WidgetChannelListModel) obj;
        return m.areEqual(this.selectedGuild, widgetChannelListModel.selectedGuild) && m.areEqual(this.items, widgetChannelListModel.items) && this.isGuildSelected == widgetChannelListModel.isGuildSelected && this.showPremiumGuildHint == widgetChannelListModel.showPremiumGuildHint && this.showEmptyState == widgetChannelListModel.showEmptyState && m.areEqual(this.guildScheduledEvents, widgetChannelListModel.guildScheduledEvents);
    }

    public final List<GuildScheduledEvent> getGuildScheduledEvents() {
        return this.guildScheduledEvents;
    }

    public final List<ChannelListItem> getItems() {
        return this.items;
    }

    public final Guild getSelectedGuild() {
        return this.selectedGuild;
    }

    public final boolean getShowEmptyState() {
        return this.showEmptyState;
    }

    public final boolean getShowPremiumGuildHint() {
        return this.showPremiumGuildHint;
    }

    public final boolean getStartsWithPaddedElement() {
        ChannelListItem channelListItem = (ChannelListItem) u.firstOrNull((List<? extends Object>) this.items);
        if (channelListItem != null) {
            return (channelListItem instanceof ChannelListItemInvite) || (channelListItem instanceof ChannelListItemMfaNotice) || (channelListItem instanceof ChannelListItemCategory) || (channelListItem instanceof ChannelListItemEventsSeparator) || (channelListItem instanceof ChannelListItemGuildJoinRequest);
        }
        return false;
    }

    public int hashCode() {
        Guild guild = this.selectedGuild;
        int i = 0;
        int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
        List<ChannelListItem> list = this.items;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        boolean z2 = this.isGuildSelected;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode2 + i3) * 31;
        boolean z3 = this.showPremiumGuildHint;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        boolean z4 = this.showEmptyState;
        if (!z4) {
            i2 = z4 ? 1 : 0;
        }
        int i9 = (i8 + i2) * 31;
        List<GuildScheduledEvent> list2 = this.guildScheduledEvents;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return i9 + i;
    }

    public final boolean isGuildSelected() {
        return this.isGuildSelected;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetChannelListModel(selectedGuild=");
        R.append(this.selectedGuild);
        R.append(", items=");
        R.append(this.items);
        R.append(", isGuildSelected=");
        R.append(this.isGuildSelected);
        R.append(", showPremiumGuildHint=");
        R.append(this.showPremiumGuildHint);
        R.append(", showEmptyState=");
        R.append(this.showEmptyState);
        R.append(", guildScheduledEvents=");
        return a.K(R, this.guildScheduledEvents, ")");
    }

    public /* synthetic */ WidgetChannelListModel(Guild guild, List list, boolean z2, boolean z3, boolean z4, List list2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(guild, list, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? false : z3, (i & 16) != 0 ? false : z4, (i & 32) != 0 ? n.emptyList() : list2);
    }
}
