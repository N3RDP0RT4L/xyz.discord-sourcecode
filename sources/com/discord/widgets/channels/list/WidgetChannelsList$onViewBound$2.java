package com.discord.widgets.channels.list;

import androidx.fragment.app.FragmentManager;
import com.discord.api.channel.Channel;
import com.discord.widgets.channels.list.WidgetChannelsListItemChannelActions;
import com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions;
import com.discord.widgets.voice.settings.WidgetVoiceChannelSettings;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsList$onViewBound$2 extends o implements Function1<Channel, Unit> {
    public final /* synthetic */ WidgetChannelsList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsList$onViewBound$2(WidgetChannelsList widgetChannelsList) {
        super(1);
        this.this$0 = widgetChannelsList;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
        invoke2(channel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        int A = channel.A();
        if (!(A == 0 || A == 1)) {
            if (A == 2) {
                WidgetVoiceChannelSettings.Companion.launch(channel.h(), this.this$0.requireContext());
                return;
            } else if (!(A == 3 || A == 4 || A == 5 || A == 15)) {
                switch (A) {
                    case 10:
                    case 11:
                    case 12:
                        WidgetChannelsListItemThreadActions.Companion companion = WidgetChannelsListItemThreadActions.Companion;
                        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                        companion.show(parentFragmentManager, channel.h());
                        return;
                    case 13:
                        break;
                    default:
                        return;
                }
            }
        }
        WidgetChannelsListItemChannelActions.Companion companion2 = WidgetChannelsListItemChannelActions.Companion;
        FragmentManager parentFragmentManager2 = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
        companion2.show(parentFragmentManager2, channel.h());
    }
}
