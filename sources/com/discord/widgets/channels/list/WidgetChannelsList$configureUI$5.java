package com.discord.widgets.channels.list;

import com.discord.databinding.WidgetChannelsListBinding;
import com.discord.models.guild.Guild;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "percentCollapsed", "", "invoke", "(F)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsList$configureUI$5 extends o implements Function1<Float, Unit> {
    public final /* synthetic */ Guild $guild;
    public final /* synthetic */ boolean $hasBanner;
    public final /* synthetic */ WidgetChannelListModel $list;
    public final /* synthetic */ WidgetChannelsList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsList$configureUI$5(WidgetChannelsList widgetChannelsList, Guild guild, WidgetChannelListModel widgetChannelListModel, boolean z2) {
        super(1);
        this.this$0 = widgetChannelsList;
        this.$guild = guild;
        this.$list = widgetChannelListModel;
        this.$hasBanner = z2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Float f) {
        invoke(f.floatValue());
        return Unit.a;
    }

    public final void invoke(float f) {
        WidgetChannelsListBinding binding;
        WidgetChannelsListBinding binding2;
        boolean z2;
        boolean z3;
        binding = this.this$0.getBinding();
        SimpleDraweeView simpleDraweeView = binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.channelsListBanner");
        simpleDraweeView.setAlpha(1.0f - f);
        binding2 = this.this$0.getBinding();
        boolean a = binding2.f2265b.a();
        z2 = this.this$0.isCollapsed;
        if (z2 != a) {
            this.this$0.isCollapsed = a;
            WidgetChannelsList widgetChannelsList = this.this$0;
            Guild guild = this.$guild;
            z3 = widgetChannelsList.isCollapsed;
            widgetChannelsList.configureHeaderIcons(guild, z3);
            this.this$0.configureHeaderColors(this.$list.getSelectedGuild(), this.$hasBanner);
        }
    }
}
