package com.discord.widgets.channels.list;

import android.view.ViewGroup;
import android.view.ViewStub;
import com.discord.databinding.WidgetChannelsListBinding;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "bottomNavHeight", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsList$configureBottomNavSpace$1 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ WidgetChannelsList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsList$configureBottomNavSpace$1(WidgetChannelsList widgetChannelsList) {
        super(1);
        this.this$0 = widgetChannelsList;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.a;
    }

    public final void invoke(int i) {
        WidgetChannelsListBinding binding;
        WidgetChannelsListBinding binding2;
        WidgetChannelsList.access$getAdapter$p(this.this$0).handleBottomNavHeight(i);
        binding = this.this$0.getBinding();
        ViewStub viewStub = binding.m;
        m.checkNotNullExpressionValue(viewStub, "binding.channelsListUnreadsStub");
        ViewGroup.LayoutParams layoutParams = viewStub.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, i);
        binding2 = this.this$0.getBinding();
        ViewStub viewStub2 = binding2.m;
        m.checkNotNullExpressionValue(viewStub2, "binding.channelsListUnreadsStub");
        viewStub2.setLayoutParams(marginLayoutParams);
    }
}
