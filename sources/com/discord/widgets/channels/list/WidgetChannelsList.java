package com.discord.widgets.channels.list;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelsListBinding;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.hubs.HubUtilsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.WidgetGroupInviteFriends;
import com.discord.widgets.channels.invite.GroupInviteFriendsSheet;
import com.discord.widgets.channels.invite.GroupInviteFriendsSheetFeatureFlag;
import com.discord.widgets.friends.EmptyFriendsStateView;
import com.discord.widgets.guilds.profile.WidgetGuildProfileSheet;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorState;
import com.discord.widgets.tabs.BottomNavViewObserver;
import com.discord.widgets.user.search.WidgetGlobalSearchDialog;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetChannelsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 E2\u00020\u0001:\u0001EB\u0007¢\u0006\u0004\bD\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\t\u0010\bJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ!\u0010\u0012\u001a\u00020\u00042\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J!\u0010\u0015\u001a\u00020\u00042\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0014\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0015\u0010\u0013J\u000f\u0010\u0016\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0016\u0010\bJ)\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0018\u001a\u00020\u00172\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0014\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001c\u0010\bJ\u0017\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u0004H\u0016¢\u0006\u0004\b!\u0010\bR$\u0010%\u001a\u0010\u0012\f\u0012\n $*\u0004\u0018\u00010#0#0\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u001e\u0010/\u001a\n\u0018\u00010-j\u0004\u0018\u0001`.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R\u0018\u00102\u001a\u0004\u0018\u0001018\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u0010\u0014\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b6\u00107R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u001d\u0010C\u001a\u00020>8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010B¨\u0006F"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsList;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;", "state", "", "handleGlobalStatusIndicatorState", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V", "roundPanelCorners", "()V", "unroundPanelCorners", "Lcom/discord/widgets/channels/list/WidgetChannelListModel;", "list", "configureUI", "(Lcom/discord/widgets/channels/list/WidgetChannelListModel;)V", "Lcom/discord/models/guild/Guild;", "guild", "", "hasBanner", "configureHeaderColors", "(Lcom/discord/models/guild/Guild;Z)V", "isCollapsed", "configureHeaderIcons", "ackPremiumGuildHint", "Landroid/content/Context;", "context", "", "getTintColor", "(Landroid/content/Context;Lcom/discord/models/guild/Guild;Z)I", "configureBottomNavSpace", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "activityResult", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/widgets/tabs/BottomNavViewObserver;", "bottomNavViewObserver", "Lcom/discord/widgets/tabs/BottomNavViewObserver;", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "bannerChangeDetector", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "", "Lcom/discord/primitives/GuildId;", "selectedGuildId", "Ljava/lang/Long;", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;", "channelListUnreads", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;", "Z", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "adapter", "Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "globalStatusIndicatorStateObserver", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreNavigation;", "Lcom/discord/databinding/WidgetChannelsListBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelsListBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsList extends AppFragment {
    public static final String ANALYTICS_SOURCE = "Channels List";
    public static final float BANNER_TEXT_SHADOW_DX = 0.0f;
    public static final float BANNER_TEXT_SHADOW_DY = 4.0f;
    public static final float BANNER_TEXT_SHADOW_RADIUS = 1.0f;
    private WidgetChannelsListAdapter adapter;
    private WidgetChannelListUnreads channelListUnreads;
    private boolean isCollapsed;
    private Long selectedGuildId;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelsList.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelsListBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelsList$binding$2.INSTANCE, null, 2, null);
    private final BottomNavViewObserver bottomNavViewObserver = BottomNavViewObserver.Companion.getINSTANCE();
    private final StoreNavigation storeNavigation = StoreStream.Companion.getNavigation();
    private final WidgetGlobalStatusIndicatorState globalStatusIndicatorStateObserver = WidgetGlobalStatusIndicatorState.Provider.get();
    private final ActivityResultLauncher<Intent> activityResult = HubUtilsKt.getAddServerActivityResultHandler(this);
    private final MGImages.DistinctChangeDetector bannerChangeDetector = new MGImages.DistinctChangeDetector();

    /* compiled from: WidgetChannelsList.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u0016\u0010\t\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0007¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsList$Companion;", "", "", "ANALYTICS_SOURCE", "Ljava/lang/String;", "", "BANNER_TEXT_SHADOW_DX", "F", "BANNER_TEXT_SHADOW_DY", "BANNER_TEXT_SHADOW_RADIUS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChannelsList() {
        super(R.layout.widget_channels_list);
    }

    public static final /* synthetic */ WidgetChannelsListAdapter access$getAdapter$p(WidgetChannelsList widgetChannelsList) {
        WidgetChannelsListAdapter widgetChannelsListAdapter = widgetChannelsList.adapter;
        if (widgetChannelsListAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        return widgetChannelsListAdapter;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void ackPremiumGuildHint() {
        StoreStream.Companion.getNux().setPremiumGuildHintGuildId(null);
    }

    private final void configureBottomNavSpace() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(this.bottomNavViewObserver.observeHeight(), this, null, 2, null), WidgetChannelsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelsList$configureBottomNavSpace$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureHeaderColors(Guild guild, boolean z2) {
        getBinding().g.setTextColor(getTintColor(requireContext(), guild, this.isCollapsed));
        getBinding().g.setShadowLayer(1.0f, 0.0f, 4.0f, (!z2 || this.isCollapsed) ? ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundSecondary) : ColorCompat.getColor(this, (int) R.color.black_alpha_80));
        FrameLayout frameLayout = getBinding().e;
        m.checkNotNullExpressionValue(frameLayout, "binding.channelsListBannerForeground");
        int i = 0;
        if (!(z2 && !this.isCollapsed)) {
            i = 8;
        }
        frameLayout.setVisibility(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00c3  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00c5  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00e2  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00ee  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureHeaderIcons(com.discord.models.guild.Guild r9, boolean r10) {
        /*
            r8 = this;
            r0 = 2
            r1 = 0
            if (r9 != 0) goto L7
        L4:
            r2 = 0
            goto Lba
        L7:
            java.util.Set r2 = r9.getFeatures()
            com.discord.api.guild.GuildFeature r3 = com.discord.api.guild.GuildFeature.VERIFIED
            boolean r2 = r2.contains(r3)
            if (r2 == 0) goto L29
            com.discord.databinding.WidgetChannelsListBinding r2 = r8.getBinding()
            com.discord.views.CustomAppBarLayout r2 = r2.f2265b
            boolean r2 = r2.a()
            if (r2 == 0) goto L24
            r2 = 2131232177(0x7f0805b1, float:1.8080456E38)
            goto Lba
        L24:
            r2 = 2131232178(0x7f0805b2, float:1.8080458E38)
            goto Lba
        L29:
            java.util.Set r2 = r9.getFeatures()
            com.discord.api.guild.GuildFeature r3 = com.discord.api.guild.GuildFeature.PARTNERED
            boolean r2 = r2.contains(r3)
            if (r2 == 0) goto L4b
            com.discord.databinding.WidgetChannelsListBinding r2 = r8.getBinding()
            com.discord.views.CustomAppBarLayout r2 = r2.f2265b
            boolean r2 = r2.a()
            if (r2 == 0) goto L46
            r2 = 2131231935(0x7f0804bf, float:1.8079965E38)
            goto Lba
        L46:
            r2 = 2131231936(0x7f0804c0, float:1.8079967E38)
            goto Lba
        L4b:
            int r2 = r9.getPremiumTier()
            if (r2 != 0) goto L6b
            int r2 = r9.getPremiumSubscriptionCount()
            if (r2 <= 0) goto L6b
            com.discord.databinding.WidgetChannelsListBinding r2 = r8.getBinding()
            com.discord.views.CustomAppBarLayout r2 = r2.f2265b
            boolean r2 = r2.a()
            if (r2 == 0) goto L67
            r2 = 2131231781(0x7f080425, float:1.8079653E38)
            goto Lba
        L67:
            r2 = 2131231782(0x7f080426, float:1.8079655E38)
            goto Lba
        L6b:
            int r2 = r9.getPremiumTier()
            r3 = 1
            if (r2 != r3) goto L86
            com.discord.databinding.WidgetChannelsListBinding r2 = r8.getBinding()
            com.discord.views.CustomAppBarLayout r2 = r2.f2265b
            boolean r2 = r2.a()
            if (r2 == 0) goto L82
            r2 = 2131231770(0x7f08041a, float:1.807963E38)
            goto Lba
        L82:
            r2 = 2131231771(0x7f08041b, float:1.8079632E38)
            goto Lba
        L86:
            int r2 = r9.getPremiumTier()
            if (r2 != r0) goto La0
            com.discord.databinding.WidgetChannelsListBinding r2 = r8.getBinding()
            com.discord.views.CustomAppBarLayout r2 = r2.f2265b
            boolean r2 = r2.a()
            if (r2 == 0) goto L9c
            r2 = 2131231772(0x7f08041c, float:1.8079634E38)
            goto Lba
        L9c:
            r2 = 2131231773(0x7f08041d, float:1.8079636E38)
            goto Lba
        La0:
            int r2 = r9.getPremiumTier()
            r3 = 3
            if (r2 != r3) goto L4
            com.discord.databinding.WidgetChannelsListBinding r2 = r8.getBinding()
            com.discord.views.CustomAppBarLayout r2 = r2.f2265b
            boolean r2 = r2.a()
            if (r2 == 0) goto Lb7
            r2 = 2131231774(0x7f08041e, float:1.8079639E38)
            goto Lba
        Lb7:
            r2 = 2131231775(0x7f08041f, float:1.807964E38)
        Lba:
            com.discord.databinding.WidgetChannelsListBinding r3 = r8.getBinding()
            android.widget.TextView r3 = r3.g
            r4 = 0
            if (r2 != 0) goto Lc5
            r2 = r4
            goto Lcd
        Lc5:
            android.content.Context r5 = r8.requireContext()
            android.graphics.drawable.Drawable r2 = androidx.core.content.ContextCompat.getDrawable(r5, r2)
        Lcd:
            android.content.Context r5 = r8.requireContext()
            android.content.Context r6 = r8.requireContext()
            r7 = 2130969456(0x7f040370, float:1.7547594E38)
            int r0 = com.discord.utilities.drawable.DrawableCompat.getThemedDrawableRes$default(r6, r7, r1, r0, r4)
            android.graphics.drawable.Drawable r0 = androidx.core.content.ContextCompat.getDrawable(r5, r0)
            if (r0 == 0) goto Lee
            android.content.Context r5 = r8.requireContext()
            int r9 = r8.getTintColor(r5, r9, r10)
            com.discord.utilities.color.ColorCompatKt.setTint(r0, r9, r1)
            goto Lef
        Lee:
            r0 = r4
        Lef:
            r3.setCompoundDrawablesWithIntrinsicBounds(r2, r4, r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.list.WidgetChannelsList.configureHeaderIcons(com.discord.models.guild.Guild, boolean):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetChannelListModel widgetChannelListModel) {
        final Guild selectedGuild = widgetChannelListModel.getSelectedGuild();
        String str = null;
        boolean z2 = true;
        int i = 0;
        boolean z3 = (selectedGuild != null ? selectedGuild.getBanner() : null) != null;
        EmptyFriendsStateView emptyFriendsStateView = getBinding().p;
        m.checkNotNullExpressionValue(emptyFriendsStateView, "binding.widgetChannelsListEmptyFriendsStateView");
        emptyFriendsStateView.setVisibility(widgetChannelListModel.getShowEmptyState() ? 0 : 8);
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.channelsList");
        recyclerView.setVisibility(widgetChannelListModel.getShowEmptyState() ? 4 : 0);
        if (!m.areEqual(this.selectedGuildId, selectedGuild != null ? Long.valueOf(selectedGuild.getId()) : null)) {
            if (z3) {
                getBinding().c.scrollToPosition(0);
                getBinding().f2265b.setExpanded(true);
                configureHeaderColors(widgetChannelListModel.getSelectedGuild(), true);
            }
            Long valueOf = selectedGuild != null ? Long.valueOf(selectedGuild.getId()) : null;
            this.selectedGuildId = valueOf;
            if (valueOf != null) {
                long longValue = valueOf.longValue();
                WidgetChannelsListAdapter widgetChannelsListAdapter = this.adapter;
                if (widgetChannelsListAdapter == null) {
                    m.throwUninitializedPropertyAccessException("adapter");
                }
                widgetChannelsListAdapter.setSelectedGuildId(longValue);
            }
        }
        WidgetChannelsListAdapter widgetChannelsListAdapter2 = this.adapter;
        if (widgetChannelsListAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter2.setData(widgetChannelListModel.getItems());
        CollapsingToolbarLayout collapsingToolbarLayout = getBinding().n;
        m.checkNotNullExpressionValue(collapsingToolbarLayout, "binding.collapsingToolbar");
        collapsingToolbarLayout.setVisibility(widgetChannelListModel.isGuildSelected() ? 0 : 8);
        CollapsingToolbarLayout collapsingToolbarLayout2 = getBinding().n;
        m.checkNotNullExpressionValue(collapsingToolbarLayout2, "binding.collapsingToolbar");
        ViewGroup.LayoutParams layoutParams = collapsingToolbarLayout2.getLayoutParams();
        if (!(layoutParams instanceof AppBarLayout.LayoutParams)) {
            layoutParams = null;
        }
        AppBarLayout.LayoutParams layoutParams2 = (AppBarLayout.LayoutParams) layoutParams;
        if (layoutParams2 != null) {
            layoutParams2.setScrollFlags((!widgetChannelListModel.isGuildSelected() || !z3) ? 0 : 5);
        }
        Toolbar toolbar = getBinding().j;
        m.checkNotNullExpressionValue(toolbar, "binding.channelsListPrivateChannelsHeader");
        toolbar.setVisibility(!widgetChannelListModel.isGuildSelected() && !widgetChannelListModel.getShowEmptyState() ? 0 : 8);
        TextView textView = getBinding().k;
        m.checkNotNullExpressionValue(textView, "binding.channelsListSearch");
        textView.setVisibility(widgetChannelListModel.isGuildSelected() ^ true ? 0 : 8);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsList$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelsList.this.ackPremiumGuildHint();
                WidgetGuildProfileSheet.Companion companion = WidgetGuildProfileSheet.Companion;
                FragmentManager parentFragmentManager = WidgetChannelsList.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                Guild guild = selectedGuild;
                companion.show(parentFragmentManager, true, guild != null ? guild.getId() : 0L, (r18 & 8) != 0 ? 0L : 0L, (r18 & 16) != 0 ? false : false);
            }
        });
        TextView textView2 = getBinding().g;
        m.checkNotNullExpressionValue(textView2, "binding.channelsListHeader");
        if (selectedGuild != null) {
            str = selectedGuild.getName();
        }
        textView2.setText(str);
        configureHeaderIcons(selectedGuild, false);
        SimpleDraweeView simpleDraweeView = getBinding().d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.channelsListBanner");
        simpleDraweeView.setVisibility(z3 ? 0 : 8);
        FrameLayout frameLayout = getBinding().e;
        m.checkNotNullExpressionValue(frameLayout, "binding.channelsListBannerForeground");
        if (!z3 || this.isCollapsed) {
            z2 = false;
        }
        frameLayout.setVisibility(z2 ? 0 : 8);
        if (z3) {
            SimpleDraweeView simpleDraweeView2 = getBinding().d;
            ViewGroup.LayoutParams layoutParams3 = simpleDraweeView2.getLayoutParams();
            Objects.requireNonNull(layoutParams3, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams3;
            marginLayoutParams.bottomMargin = !widgetChannelListModel.getStartsWithPaddedElement() ? DimenUtils.dpToPixels(8) : 0;
            simpleDraweeView2.setLayoutParams(marginLayoutParams);
            boolean canHaveAnimatedBanner = selectedGuild != null ? selectedGuild.canHaveAnimatedBanner() : false;
            m.checkNotNullExpressionValue(simpleDraweeView2, "this");
            MGImages.setImage$default(simpleDraweeView2, IconUtils.INSTANCE.getBannerForGuild(selectedGuild, Integer.valueOf(simpleDraweeView2.getResources().getDimensionPixelSize(R.dimen.nav_panel_width)), canHaveAnimatedBanner), 0, 0, false, null, this.bannerChangeDetector, 60, null);
        }
        getBinding().f2265b.setOnPercentCollapsedCallback(new WidgetChannelsList$configureUI$5(this, selectedGuild, widgetChannelListModel, z3));
        ConstraintLayout constraintLayout = getBinding().h;
        m.checkNotNullExpressionValue(constraintLayout, "binding.channelsListPremiumGuildHint");
        if (!widgetChannelListModel.getShowPremiumGuildHint()) {
            i = 8;
        }
        constraintLayout.setVisibility(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChannelsListBinding getBinding() {
        return (WidgetChannelsListBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final int getTintColor(Context context, Guild guild, boolean z2) {
        return ((guild != null ? guild.getBanner() : null) == null || z2) ? ColorCompat.getThemedColor(context, (int) R.attr.colorInteractiveActive) : ColorCompat.getColor(context, (int) R.color.white);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleGlobalStatusIndicatorState(WidgetGlobalStatusIndicatorState.State state) {
        if (state.isCustomBackground()) {
            unroundPanelCorners();
        } else {
            roundPanelCorners();
        }
    }

    private final void roundPanelCorners() {
        float dpToPixels = DimenUtils.dpToPixels(8);
        getBinding().o.updateTopLeftRadius(dpToPixels);
        getBinding().o.updateTopRightRadius(dpToPixels);
    }

    private final void unroundPanelCorners() {
        getBinding().o.updateTopLeftRadius(0.0f);
        getBinding().o.updateTopRightRadius(0.0f);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        getBinding().p.updateView(ANALYTICS_SOURCE);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.channelsList");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        WidgetChannelsListAdapter widgetChannelsListAdapter = (WidgetChannelsListAdapter) companion.configure(new WidgetChannelsListAdapter(recyclerView, parentFragmentManager));
        this.adapter = widgetChannelsListAdapter;
        if (widgetChannelsListAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter.setOnSelectChannel(new WidgetChannelsList$onViewBound$1(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter2 = this.adapter;
        if (widgetChannelsListAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter2.setOnSelectChannelOptions(new WidgetChannelsList$onViewBound$2(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter3 = this.adapter;
        if (widgetChannelsListAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter3.setOnCallChannel(new WidgetChannelsList$onViewBound$3(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter4 = this.adapter;
        if (widgetChannelsListAdapter4 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter4.setOnSelectGuildRoleSubscriptionLockedChannel(new WidgetChannelsList$onViewBound$4(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter5 = this.adapter;
        if (widgetChannelsListAdapter5 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter5.setOnCollapseCategory(new WidgetChannelsList$onViewBound$5(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter6 = this.adapter;
        if (widgetChannelsListAdapter6 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter6.setOnSelectInvite(new WidgetChannelsList$onViewBound$6(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter7 = this.adapter;
        if (widgetChannelsListAdapter7 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter7.setOnSelectUserOptions(new WidgetChannelsList$onViewBound$7(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter8 = this.adapter;
        if (widgetChannelsListAdapter8 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter8.setOnViewGuildScheduledEvents(new WidgetChannelsList$onViewBound$8(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter9 = this.adapter;
        if (widgetChannelsListAdapter9 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter9.setOnViewGuildRoleSubscriptions(new WidgetChannelsList$onViewBound$9(this));
        WidgetChannelsListAdapter widgetChannelsListAdapter10 = this.adapter;
        if (widgetChannelsListAdapter10 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter10.setOnAddServer(new WidgetChannelsList$onViewBound$10(this));
        getBinding().k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsList$onViewBound$11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetGlobalSearchDialog.Companion companion2 = WidgetGlobalSearchDialog.Companion;
                FragmentManager parentFragmentManager2 = WidgetChannelsList.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
                companion2.show(parentFragmentManager2, "@");
            }
        });
        getBinding().c.setHasFixedSize(false);
        RecyclerView recyclerView2 = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView2, "binding.channelsList");
        recyclerView2.setItemAnimator(null);
        ViewStub viewStub = getBinding().m;
        m.checkNotNullExpressionValue(viewStub, "binding.channelsListUnreadsStub");
        RecyclerView recyclerView3 = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView3, "binding.channelsList");
        this.channelListUnreads = new WidgetChannelListUnreads(viewStub, recyclerView3, getBinding().f2265b, new WidgetChannelsList$onViewBound$12(this), 0, 0, false, 112, null);
        WidgetChannelsListAdapter widgetChannelsListAdapter11 = this.adapter;
        if (widgetChannelsListAdapter11 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChannelsListAdapter11.setOnUpdated(new WidgetChannelsList$onViewBound$13(this));
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsList$onViewBound$14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChannelsList.this.ackPremiumGuildHint();
            }
        });
        getBinding().l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsList$onViewBound$15
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                if (GroupInviteFriendsSheetFeatureFlag.Companion.getINSTANCE().isEnabled()) {
                    GroupInviteFriendsSheet.Companion companion2 = GroupInviteFriendsSheet.Companion;
                    FragmentManager parentFragmentManager2 = WidgetChannelsList.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
                    GroupInviteFriendsSheet.Companion.show$default(companion2, parentFragmentManager2, 0L, WidgetChannelsList.ANALYTICS_SOURCE, 2, null);
                    return;
                }
                WidgetGroupInviteFriends.Companion.launch(WidgetChannelsList.this.requireContext(), WidgetChannelsList.ANALYTICS_SOURCE);
            }
        });
        ViewCompat.setAccessibilityHeading(getBinding().g, true);
        ViewCompat.setAccessibilityHeading(getBinding().f, true);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable computationLatest = ObservableExtensionsKt.computationLatest(WidgetChannelListModel.Companion.get());
        WidgetChannelsListAdapter widgetChannelsListAdapter = this.adapter;
        if (widgetChannelsListAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(computationLatest, this, widgetChannelsListAdapter), WidgetChannelsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelsList$onViewBoundOrOnResume$1(this));
        if (getMostRecentIntent().getBooleanExtra("com.discord.intent.extra.EXTRA_OPEN_PANEL", false)) {
            StoreNavigation.setNavigationPanelAction$default(this.storeNavigation, StoreNavigation.PanelAction.OPEN, null, 2, null);
            getMostRecentIntent().removeExtra("com.discord.intent.extra.EXTRA_OPEN_PANEL");
        }
        configureBottomNavSpace();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(this.globalStatusIndicatorStateObserver.observeState(), this, null, 2, null), WidgetChannelsList.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelsList$onViewBoundOrOnResume$2(this));
    }
}
