package com.discord.widgets.channels.list;

import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetChannelsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "collapsed", "", "invoke", "(Lcom/discord/api/channel/Channel;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsList$onViewBound$5 extends o implements Function2<Channel, Boolean, Unit> {
    public final /* synthetic */ WidgetChannelsList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsList$onViewBound$5(WidgetChannelsList widgetChannelsList) {
        super(2);
        this.this$0 = widgetChannelsList;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel, Boolean bool) {
        invoke(channel, bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(Channel channel, boolean z2) {
        m.checkNotNullParameter(channel, "channel");
        Object systemService = WidgetChannelsList.access$getAdapter$p(this.this$0).getContext().getSystemService("accessibility");
        if (systemService instanceof AccessibilityManager) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            m.checkNotNullExpressionValue(obtain, "event");
            obtain.setEventType(16384);
            Object[] objArr = new Object[2];
            objArr[0] = this.this$0.getString(z2 ? R.string.expanded : R.string.collapsed);
            objArr[1] = ChannelUtils.c(channel);
            obtain.getText().add(a.N(objArr, 2, "%s %s", "java.lang.String.format(format, *args)"));
            try {
                ((AccessibilityManager) systemService).sendAccessibilityEvent(obtain);
            } catch (IllegalStateException unused) {
            }
        }
        StoreStream.Companion.getStoreChannelCategories().setCollapsedCategory(channel.f(), channel.h(), !z2);
    }
}
