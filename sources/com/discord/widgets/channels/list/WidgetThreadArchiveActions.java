package com.discord.widgets.channels.list;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildFeature;
import com.discord.api.thread.ThreadMetadata;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetThreadArchiveActionsSheetBinding;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.radiobutton.MaterialRadioButton;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetThreadArchiveActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \"2\u00020\u0001:\u0002\"#B\u0007¢\u0006\u0004\b!\u0010\u0018J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u000f\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\rH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions$Model;)V", "Lcom/discord/api/channel/Channel;", "channel", "", "lockThread", "archiveThread", "(Lcom/discord/api/channel/Channel;Z)V", "", "minutes", "setAutoArchiveDuration", "(Lcom/discord/api/channel/Channel;I)V", "getContentViewResId", "()I", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "onPause", "()V", "Lcom/discord/databinding/WidgetThreadArchiveActionsSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetThreadArchiveActionsSheetBinding;", "binding", "hasFiredAnalytics", "Z", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetThreadArchiveActions extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetThreadArchiveActions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetThreadArchiveActionsSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetThreadArchiveActions$binding$2.INSTANCE, null, 2, null);
    private boolean hasFiredAnalytics;

    /* compiled from: WidgetThreadArchiveActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "", "show", "(Landroidx/fragment/app/FragmentManager;JJ)V", "", WidgetThreadArchiveActions.INTENT_EXTRA_CHANNEL_ID, "Ljava/lang/String;", "INTENT_EXTRA_GUILD_ID", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, long j, long j2) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetThreadArchiveActions widgetThreadArchiveActions = new WidgetThreadArchiveActions();
            Bundle I = a.I(WidgetThreadArchiveActions.INTENT_EXTRA_CHANNEL_ID, j);
            I.putLong("INTENT_EXTRA_GUILD_ID", j2);
            widgetThreadArchiveActions.setArguments(I);
            widgetThreadArchiveActions.show(fragmentManager, WidgetThreadArchiveActions.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetThreadArchiveActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0082\b\u0018\u0000  2\u00020\u0001:\u0001 B!\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0017\u001a\u00020\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\r\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004¨\u0006!"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions$Model;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "", "component3", "()Z", "channel", "guild", "isModerator", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Z)Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Z)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Channel channel;
        private final Guild guild;
        private final boolean isModerator;

        /* compiled from: WidgetThreadArchiveActions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ-\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions$Model$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/channels/list/WidgetThreadArchiveActions$Model;", "get", "(JJ)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j, long j2) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable i = Observable.i(companion.getChannels().observeChannel(j), companion.getGuilds().observeGuild(j2), companion.getPermissions().observePermissionsForChannel(j), WidgetThreadArchiveActions$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(i, "Observable.combineLatest…            }\n          }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(i).q();
                m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Channel channel, Guild guild, boolean z2) {
            m.checkNotNullParameter(channel, "channel");
            this.channel = channel;
            this.guild = guild;
            this.isModerator = z2;
        }

        public static /* synthetic */ Model copy$default(Model model, Channel channel, Guild guild, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                channel = model.channel;
            }
            if ((i & 2) != 0) {
                guild = model.guild;
            }
            if ((i & 4) != 0) {
                z2 = model.isModerator;
            }
            return model.copy(channel, guild, z2);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final boolean component3() {
            return this.isModerator;
        }

        public final Model copy(Channel channel, Guild guild, boolean z2) {
            m.checkNotNullParameter(channel, "channel");
            return new Model(channel, guild, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.channel, model.channel) && m.areEqual(this.guild, model.guild) && this.isModerator == model.isModerator;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            Guild guild = this.guild;
            if (guild != null) {
                i = guild.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.isModerator;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isModerator() {
            return this.isModerator;
        }

        public String toString() {
            StringBuilder R = a.R("Model(channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", isModerator=");
            return a.M(R, this.isModerator, ")");
        }
    }

    public WidgetThreadArchiveActions() {
        super(false, 1, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void archiveThread(Channel channel, boolean z2) {
        RestAPI api = RestAPI.Companion.getApi();
        long h = channel.h();
        Boolean bool = Boolean.TRUE;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.editThread(h, new RestAPIParams.ThreadSettings(bool, z2 ? bool : null, null, 4, null)), false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : getContext(), "REST: archiveThread", (r18 & 4) != 0 ? null : null, new WidgetThreadArchiveActions$archiveThread$1(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence b2;
        if (model == null) {
            dismiss();
            return;
        }
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        final Channel channel = model.getChannel();
        Guild guild = model.getGuild();
        Traits.Location location = new Traits.Location(Traits.Location.Page.GUILD_CHANNEL, Traits.Location.Section.THREAD_ARCHIVAL_DURATION_SHEET, Traits.Location.Obj.LIST_ITEM, null, null, 24, null);
        if (!this.hasFiredAnalytics) {
            AnalyticsTracker.GuildBoostUpsellType threadTypeForGuild = AnalyticsTracker.GuildBoostUpsellType.Companion.getThreadTypeForGuild(guild);
            if (!(guild == null || threadTypeForGuild == null)) {
                AnalyticsTracker.INSTANCE.guildBoostUpsellViewed(threadTypeForGuild, guild.getId(), Long.valueOf(channel.h()), new Traits.Location(null, Traits.Location.Section.THREAD_ARCHIVAL_DURATION_SHEET, null, null, null, 29, null));
                this.hasFiredAnalytics = true;
            }
        }
        LinearLayout linearLayout = getBinding().c;
        m.checkNotNullExpressionValue(linearLayout, "binding.autoArchiveHeader");
        linearLayout.setVisibility(0);
        long computeThreadAutoArchiveTimeMs = ThreadUtils.INSTANCE.computeThreadAutoArchiveTimeMs(channel);
        TextView textView = getBinding().r;
        m.checkNotNullExpressionValue(textView, "binding.subtitle");
        Object[] objArr = {TimeUtils.toReadableTimeString$default(requireContext, computeThreadAutoArchiveTimeMs, null, 4, null)};
        Integer num = null;
        b2 = b.b(requireContext, R.string.auto_archive_thread_at_long, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
        textView.setText(b2);
        ThreadMetadata y2 = channel.y();
        if (y2 != null) {
            num = Integer.valueOf(y2.c());
        }
        TextView textView2 = getBinding().f2640b;
        m.checkNotNullExpressionValue(textView2, "binding.archiveNow");
        textView2.setVisibility(0);
        getBinding().f2640b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetThreadArchiveActions$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadArchiveActions.this.archiveThread(channel, model.isModerator());
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetThreadArchiveActions$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadArchiveActions.this.setAutoArchiveDuration(channel, 60);
            }
        });
        MaterialRadioButton materialRadioButton = getBinding().e;
        m.checkNotNullExpressionValue(materialRadioButton, "binding.optionOneHourRadio");
        materialRadioButton.setChecked(num != null && num.intValue() == 60);
        getBinding().p.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetThreadArchiveActions$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadArchiveActions.this.setAutoArchiveDuration(channel, 1440);
            }
        });
        MaterialRadioButton materialRadioButton2 = getBinding().q;
        m.checkNotNullExpressionValue(materialRadioButton2, "binding.optionTwentyFourHoursRadio");
        materialRadioButton2.setChecked(num != null && num.intValue() == 1440);
        PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
        Long valueOf = Long.valueOf(channel.h());
        GuildFeature guildFeature = GuildFeature.THREE_DAY_THREAD_ARCHIVE;
        Context requireContext2 = requireContext();
        m.checkNotNullExpressionValue(requireContext2, "requireContext()");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        PremiumUtils.BoostFeatureBadgeData boostFeatureBadgeDataForGuildFeature = premiumUtils.getBoostFeatureBadgeDataForGuildFeature(guild, valueOf, guildFeature, requireContext2, parentFragmentManager, new WidgetThreadArchiveActions$configureUI$threeDayArchiveOption$1(this, channel), location);
        ConstraintLayout constraintLayout = getBinding().k;
        final Function1<View, Unit> onClickListener = boostFeatureBadgeDataForGuildFeature.getOnClickListener();
        Object obj = onClickListener;
        if (onClickListener != null) {
            obj = new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetThreadArchiveActions$sam$android_view_View_OnClickListener$0
                @Override // android.view.View.OnClickListener
                public final /* synthetic */ void onClick(View view) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(view), "invoke(...)");
                }
            };
        }
        constraintLayout.setOnClickListener((View.OnClickListener) obj);
        TextView textView3 = getBinding().n;
        m.checkNotNullExpressionValue(textView3, "binding.optionThreeDaysLabel");
        textView3.setText(boostFeatureBadgeDataForGuildFeature.getText());
        getBinding().l.setTextColor(boostFeatureBadgeDataForGuildFeature.getTextColor());
        ImageView imageView = getBinding().m;
        m.checkNotNullExpressionValue(imageView, "binding.optionThreeDaysIcon");
        ColorCompatKt.tintWithColor(imageView, boostFeatureBadgeDataForGuildFeature.getIconColor());
        MaterialRadioButton materialRadioButton3 = getBinding().o;
        m.checkNotNullExpressionValue(materialRadioButton3, "binding.optionThreeDaysRadio");
        materialRadioButton3.setChecked(num != null && num.intValue() == 4320);
        Long valueOf2 = Long.valueOf(channel.h());
        GuildFeature guildFeature2 = GuildFeature.SEVEN_DAY_THREAD_ARCHIVE;
        Context requireContext3 = requireContext();
        m.checkNotNullExpressionValue(requireContext3, "requireContext()");
        FragmentManager parentFragmentManager2 = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
        PremiumUtils.BoostFeatureBadgeData boostFeatureBadgeDataForGuildFeature2 = premiumUtils.getBoostFeatureBadgeDataForGuildFeature(guild, valueOf2, guildFeature2, requireContext3, parentFragmentManager2, new WidgetThreadArchiveActions$configureUI$sevenDayArchiveOption$1(this, channel), location);
        ConstraintLayout constraintLayout2 = getBinding().f;
        final Function1<View, Unit> onClickListener2 = boostFeatureBadgeDataForGuildFeature2.getOnClickListener();
        Object obj2 = onClickListener2;
        if (onClickListener2 != null) {
            obj2 = new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetThreadArchiveActions$sam$android_view_View_OnClickListener$0
                @Override // android.view.View.OnClickListener
                public final /* synthetic */ void onClick(View view) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(view), "invoke(...)");
                }
            };
        }
        constraintLayout2.setOnClickListener((View.OnClickListener) obj2);
        TextView textView4 = getBinding().i;
        m.checkNotNullExpressionValue(textView4, "binding.optionSevenDaysLabel");
        textView4.setText(boostFeatureBadgeDataForGuildFeature2.getText());
        getBinding().g.setTextColor(boostFeatureBadgeDataForGuildFeature2.getTextColor());
        ImageView imageView2 = getBinding().h;
        m.checkNotNullExpressionValue(imageView2, "binding.optionSevenDaysIcon");
        ColorCompatKt.tintWithColor(imageView2, boostFeatureBadgeDataForGuildFeature2.getIconColor());
        MaterialRadioButton materialRadioButton4 = getBinding().j;
        m.checkNotNullExpressionValue(materialRadioButton4, "binding.optionSevenDaysRadio");
        materialRadioButton4.setChecked(num != null && num.intValue() == 10080);
    }

    private final WidgetThreadArchiveActionsSheetBinding getBinding() {
        return (WidgetThreadArchiveActionsSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setAutoArchiveDuration(Channel channel, int i) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().editThread(channel.h(), new RestAPIParams.ThreadSettings(null, null, Integer.valueOf(i), 3, null)), false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : getContext(), "REST: setAutoArchiveDuration", (r18 & 4) != 0 ? null : null, new WidgetThreadArchiveActions$setAutoArchiveDuration$1(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    public static final void show(FragmentManager fragmentManager, long j, long j2) {
        Companion.show(fragmentManager, j, j2);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        super.bindSubscriptions(compositeSubscription);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getArgumentsOrDefault().getLong(INTENT_EXTRA_CHANNEL_ID, -1L), getArgumentsOrDefault().getLong("INTENT_EXTRA_GUILD_ID", -1L)), this, null, 2, null), WidgetThreadArchiveActions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetThreadArchiveActions$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_thread_archive_actions_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
