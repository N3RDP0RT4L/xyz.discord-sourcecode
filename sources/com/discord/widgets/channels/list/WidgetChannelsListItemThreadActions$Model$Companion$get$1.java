package com.discord.widgets.channels.list;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.NotificationTextUtils;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadsJoined;
import com.discord.stores.StoreUser;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func7;
/* compiled from: WidgetChannelsListItemThreadActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003 \u0004*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Model;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsListItemThreadActions$Model$Companion$get$1<T, R> implements b<Channel, Observable<? extends WidgetChannelsListItemThreadActions.Model>> {
    public static final WidgetChannelsListItemThreadActions$Model$Companion$get$1 INSTANCE = new WidgetChannelsListItemThreadActions$Model$Companion$get$1();

    public final Observable<? extends WidgetChannelsListItemThreadActions.Model> call(final Channel channel) {
        if (channel == null) {
            return new k(null);
        }
        StoreStream.Companion companion = StoreStream.Companion;
        return Observable.e(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getChannels().observeChannel(channel.r()), companion.getGuilds().observeGuild(channel.f()), companion.getPermissions().observePermissionsForChannel(channel.r()), companion.getUserGuildSettings().observeGuildSettings(), companion.getThreadsJoined().observeJoinedThread(channel.h()), companion.getThreadsActive().observeActiveThreadsForGuild(channel.f()).F(new b<Map<Long, ? extends Channel>, Boolean>() { // from class: com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions$Model$Companion$get$1.1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Boolean call(Map<Long, ? extends Channel> map) {
                return call2((Map<Long, Channel>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Boolean call2(Map<Long, Channel> map) {
                return Boolean.valueOf(a.c(Channel.this, map) != null);
            }
        }).q(), new Func7<MeUser, Channel, Guild, Long, Map<Long, ? extends ModelNotificationSettings>, StoreThreadsJoined.JoinedThread, Boolean, WidgetChannelsListItemThreadActions.Model>() { // from class: com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions$Model$Companion$get$1.2
            public final WidgetChannelsListItemThreadActions.Model call(MeUser meUser, Channel channel2, Guild guild, Long l, Map<Long, ? extends ModelNotificationSettings> map, StoreThreadsJoined.JoinedThread joinedThread, Boolean bool) {
                NotificationTextUtils notificationTextUtils = NotificationTextUtils.INSTANCE;
                boolean isGuildOrCategoryOrChannelMuted = notificationTextUtils.isGuildOrCategoryOrChannelMuted((ModelNotificationSettings) a.u0(Channel.this, map), channel2);
                Integer channelMessageNotificationLevel = notificationTextUtils.channelMessageNotificationLevel((ModelNotificationSettings) a.u0(Channel.this, map), Channel.this, guild);
                ThreadUtils threadUtils = ThreadUtils.INSTANCE;
                int computeThreadNotificationSetting = threadUtils.computeThreadNotificationSetting(joinedThread, isGuildOrCategoryOrChannelMuted, channelMessageNotificationLevel);
                m.checkNotNullExpressionValue(meUser, "meUser");
                long j = 0;
                boolean canManageThread = threadUtils.canManageThread(meUser, Channel.this, Long.valueOf(l != null ? l.longValue() : 0L));
                boolean canUnarchiveThread = threadUtils.canUnarchiveThread(Channel.this, Long.valueOf(l != null ? l.longValue() : 0L));
                if (l != null) {
                    j = l.longValue();
                }
                boolean isThreadModerator = threadUtils.isThreadModerator(Long.valueOf(j));
                boolean z2 = joinedThread != null && joinedThread.getMuted();
                Channel channel3 = Channel.this;
                m.checkNotNullExpressionValue(bool, "isActiveThread");
                return new WidgetChannelsListItemThreadActions.Model(meUser, channel3, guild, channel2, computeThreadNotificationSetting, joinedThread, bool.booleanValue(), isThreadModerator, canManageThread, canUnarchiveThread, z2);
            }
        });
    }
}
