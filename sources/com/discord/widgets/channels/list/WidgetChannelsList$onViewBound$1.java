package com.discord.widgets.channels.list;

import com.discord.api.channel.Channel;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreNavigation;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowser;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsList$onViewBound$1 extends o implements Function1<Channel, Unit> {
    public final /* synthetic */ WidgetChannelsList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelsList$onViewBound$1(WidgetChannelsList widgetChannelsList) {
        super(1);
        this.this$0 = widgetChannelsList;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
        invoke2(channel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Channel channel) {
        StoreNavigation storeNavigation;
        StoreNavigation storeNavigation2;
        if (channel == null) {
            ChannelSelector.Companion.getInstance().selectChannel(0L, 0L, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null);
            return;
        }
        ChannelSelector.selectChannel$default(ChannelSelector.Companion.getInstance(), channel, null, SelectedChannelAnalyticsLocation.CHANNEL_LIST, 2, null);
        if (channel.A() == 15) {
            WidgetThreadBrowser.Companion.show(this.this$0.getContext(), channel.f(), channel.h(), "Forum");
        }
        storeNavigation = this.this$0.storeNavigation;
        StoreNavigation.setNavigationPanelAction$default(storeNavigation, StoreNavigation.PanelAction.UNLOCK_LEFT, null, 2, null);
        storeNavigation2 = this.this$0.storeNavigation;
        StoreNavigation.setNavigationPanelAction$default(storeNavigation2, StoreNavigation.PanelAction.CLOSE, null, 2, null);
    }
}
