package com.discord.widgets.channels.list;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.thread.ThreadMetadata;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetChannelsListItemThreadActionsBinding;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadsJoined;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.settings.WidgetChannelNotificationSettings;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetChannelsListItemThreadActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 &2\u00020\u0001:\u0002&'B\u0007¢\u0006\u0004\b%\u0010\u001eJ\u0015\u0010\u0004\u001a\u00020\u0003*\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J'\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u0019H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u001d\u0010\u001eR\u001d\u0010$\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#¨\u0006("}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Model;", "", "configureUI", "(Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Model;)V", "Lcom/discord/api/channel/Channel;", "channel", "", "isMuted", "Landroid/content/Context;", "context", "", "getMuteThreadText", "(Lcom/discord/api/channel/Channel;ZLandroid/content/Context;)Ljava/lang/CharSequence;", "", "getMuteIconResId", "(ZLandroid/content/Context;)I", "leaveThread", "(Lcom/discord/api/channel/Channel;)V", "unlockThread", "unarchiveThread", "(Lcom/discord/api/channel/Channel;Z)V", "getContentViewResId", "()I", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "onPause", "()V", "Lcom/discord/databinding/WidgetChannelsListItemThreadActionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelsListItemThreadActionsBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelsListItemThreadActions extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelsListItemThreadActions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelsListItemThreadActionsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelsListItemThreadActions$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetChannelsListItemThreadActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "show", "(Landroidx/fragment/app/FragmentManager;J)V", "", WidgetChannelsListItemThreadActions.INTENT_EXTRA_CHANNEL_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, long j) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetChannelsListItemThreadActions widgetChannelsListItemThreadActions = new WidgetChannelsListItemThreadActions();
            Bundle bundle = new Bundle();
            bundle.putLong(WidgetChannelsListItemThreadActions.INTENT_EXTRA_CHANNEL_ID, j);
            widgetChannelsListItemThreadActions.setArguments(bundle);
            widgetChannelsListItemThreadActions.show(fragmentManager, WidgetChannelsListItemThreadActions.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelsListItemThreadActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0013\n\u0002\u0010\u000e\n\u0002\b\u0019\b\u0082\b\u0018\u0000 >2\u00020\u0001:\u0001>Be\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0005\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u001d\u001a\u00020\f\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\u001f\u001a\u00020\u0012\u0012\u0006\u0010 \u001a\u00020\u0012\u0012\u0006\u0010!\u001a\u00020\u0012\u0012\u0006\u0010\"\u001a\u00020\u0012\u0012\u0006\u0010#\u001a\u00020\u0012¢\u0006\u0004\b<\u0010=J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0014J\u0010\u0010\u0017\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0014J\u0010\u0010\u0018\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0014J\u0084\u0001\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u001d\u001a\u00020\f2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010\u001f\u001a\u00020\u00122\b\b\u0002\u0010 \u001a\u00020\u00122\b\b\u0002\u0010!\u001a\u00020\u00122\b\b\u0002\u0010\"\u001a\u00020\u00122\b\b\u0002\u0010#\u001a\u00020\u0012HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010'\u001a\u00020&HÖ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010)\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b)\u0010\u000eJ\u001a\u0010+\u001a\u00020\u00122\b\u0010*\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b+\u0010,R\u0019\u0010-\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b-\u0010\u0014R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010/\u001a\u0004\b0\u0010\u0011R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00101\u001a\u0004\b2\u0010\nR\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00103\u001a\u0004\b4\u0010\u0004R\u0019\u0010\u001f\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010.\u001a\u0004\b\u001f\u0010\u0014R\u0019\u0010!\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010.\u001a\u0004\b5\u0010\u0014R\u0019\u0010#\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010.\u001a\u0004\b#\u0010\u0014R\u0019\u0010 \u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b \u0010.\u001a\u0004\b \u0010\u0014R\u0019\u0010\u001a\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00106\u001a\u0004\b7\u0010\u0007R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00106\u001a\u0004\b8\u0010\u0007R\u0019\u0010\"\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010.\u001a\u0004\b9\u0010\u0014R\u0019\u0010\u001d\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010:\u001a\u0004\b;\u0010\u000e¨\u0006?"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Model;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "component4", "", "component5", "()I", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "component6", "()Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "", "component7", "()Z", "component8", "component9", "component10", "component11", "meUser", "channel", "guild", "parentChannel", "notificationSetting", "joinedThread", "isActiveThread", "isModerator", "canManageThread", "canUnarchiveThread", "isMuted", "copy", "(Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;ILcom/discord/stores/StoreThreadsJoined$JoinedThread;ZZZZZ)Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "isDeveloper", "Z", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "getJoinedThread", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/user/MeUser;", "getMeUser", "getCanManageThread", "Lcom/discord/api/channel/Channel;", "getChannel", "getParentChannel", "getCanUnarchiveThread", "I", "getNotificationSetting", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;ILcom/discord/stores/StoreThreadsJoined$JoinedThread;ZZZZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canManageThread;
        private final boolean canUnarchiveThread;
        private final Channel channel;
        private final Guild guild;
        private final boolean isActiveThread;
        private final boolean isDeveloper = StoreStream.Companion.getUserSettings().getIsDeveloperMode();
        private final boolean isModerator;
        private final boolean isMuted;
        private final StoreThreadsJoined.JoinedThread joinedThread;
        private final MeUser meUser;
        private final int notificationSetting;
        private final Channel parentChannel;

        /* compiled from: WidgetChannelsListItemThreadActions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Model$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/widgets/channels/list/WidgetChannelsListItemThreadActions$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j) {
                Observable<R> Y = StoreStream.Companion.getChannels().observeChannel(j).Y(WidgetChannelsListItemThreadActions$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …        }\n              }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(Y).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(MeUser meUser, Channel channel, Guild guild, Channel channel2, int i, StoreThreadsJoined.JoinedThread joinedThread, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(channel, "channel");
            this.meUser = meUser;
            this.channel = channel;
            this.guild = guild;
            this.parentChannel = channel2;
            this.notificationSetting = i;
            this.joinedThread = joinedThread;
            this.isActiveThread = z2;
            this.isModerator = z3;
            this.canManageThread = z4;
            this.canUnarchiveThread = z5;
            this.isMuted = z6;
        }

        public final MeUser component1() {
            return this.meUser;
        }

        public final boolean component10() {
            return this.canUnarchiveThread;
        }

        public final boolean component11() {
            return this.isMuted;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final Guild component3() {
            return this.guild;
        }

        public final Channel component4() {
            return this.parentChannel;
        }

        public final int component5() {
            return this.notificationSetting;
        }

        public final StoreThreadsJoined.JoinedThread component6() {
            return this.joinedThread;
        }

        public final boolean component7() {
            return this.isActiveThread;
        }

        public final boolean component8() {
            return this.isModerator;
        }

        public final boolean component9() {
            return this.canManageThread;
        }

        public final Model copy(MeUser meUser, Channel channel, Guild guild, Channel channel2, int i, StoreThreadsJoined.JoinedThread joinedThread, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(channel, "channel");
            return new Model(meUser, channel, guild, channel2, i, joinedThread, z2, z3, z4, z5, z6);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.meUser, model.meUser) && m.areEqual(this.channel, model.channel) && m.areEqual(this.guild, model.guild) && m.areEqual(this.parentChannel, model.parentChannel) && this.notificationSetting == model.notificationSetting && m.areEqual(this.joinedThread, model.joinedThread) && this.isActiveThread == model.isActiveThread && this.isModerator == model.isModerator && this.canManageThread == model.canManageThread && this.canUnarchiveThread == model.canUnarchiveThread && this.isMuted == model.isMuted;
        }

        public final boolean getCanManageThread() {
            return this.canManageThread;
        }

        public final boolean getCanUnarchiveThread() {
            return this.canUnarchiveThread;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final StoreThreadsJoined.JoinedThread getJoinedThread() {
            return this.joinedThread;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final int getNotificationSetting() {
            return this.notificationSetting;
        }

        public final Channel getParentChannel() {
            return this.parentChannel;
        }

        public int hashCode() {
            MeUser meUser = this.meUser;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            int hashCode3 = (hashCode2 + (guild != null ? guild.hashCode() : 0)) * 31;
            Channel channel2 = this.parentChannel;
            int hashCode4 = (((hashCode3 + (channel2 != null ? channel2.hashCode() : 0)) * 31) + this.notificationSetting) * 31;
            StoreThreadsJoined.JoinedThread joinedThread = this.joinedThread;
            if (joinedThread != null) {
                i = joinedThread.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            boolean z2 = this.isActiveThread;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.isModerator;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.canManageThread;
            if (z4) {
                z4 = true;
            }
            int i10 = z4 ? 1 : 0;
            int i11 = z4 ? 1 : 0;
            int i12 = (i9 + i10) * 31;
            boolean z5 = this.canUnarchiveThread;
            if (z5) {
                z5 = true;
            }
            int i13 = z5 ? 1 : 0;
            int i14 = z5 ? 1 : 0;
            int i15 = (i12 + i13) * 31;
            boolean z6 = this.isMuted;
            if (!z6) {
                i3 = z6 ? 1 : 0;
            }
            return i15 + i3;
        }

        public final boolean isActiveThread() {
            return this.isActiveThread;
        }

        public final boolean isDeveloper() {
            return this.isDeveloper;
        }

        public final boolean isModerator() {
            return this.isModerator;
        }

        public final boolean isMuted() {
            return this.isMuted;
        }

        public String toString() {
            StringBuilder R = a.R("Model(meUser=");
            R.append(this.meUser);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", parentChannel=");
            R.append(this.parentChannel);
            R.append(", notificationSetting=");
            R.append(this.notificationSetting);
            R.append(", joinedThread=");
            R.append(this.joinedThread);
            R.append(", isActiveThread=");
            R.append(this.isActiveThread);
            R.append(", isModerator=");
            R.append(this.isModerator);
            R.append(", canManageThread=");
            R.append(this.canManageThread);
            R.append(", canUnarchiveThread=");
            R.append(this.canUnarchiveThread);
            R.append(", isMuted=");
            return a.M(R, this.isMuted, ")");
        }
    }

    public WidgetChannelsListItemThreadActions() {
        super(false, 1, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        ThreadMetadata y2;
        CharSequence e;
        ThreadMetadata y3;
        if (model == null) {
            dismiss();
            return;
        }
        if (model.getGuild() != null) {
            SimpleDraweeView simpleDraweeView = getBinding().i;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.channelsListItemThreadActionsIcon");
            IconUtils.setIcon$default((ImageView) simpleDraweeView, model.getGuild(), 0, (MGImages.ChangeDetector) null, false, 28, (Object) null);
        } else {
            SimpleDraweeView simpleDraweeView2 = getBinding().i;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.channelsListItemThreadActionsIcon");
            IconUtils.setIcon$default(simpleDraweeView2, model.getChannel(), 0, (MGImages.ChangeDetector) null, 12, (Object) null);
        }
        TextView textView = getBinding().m;
        m.checkNotNullExpressionValue(textView, "binding.channelsListItemThreadActionsTitle");
        Channel channel = model.getChannel();
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        int i = 0;
        textView.setText(ChannelUtils.e(channel, requireContext, false, 2));
        TextView textView2 = getBinding().k;
        m.checkNotNullExpressionValue(textView2, "binding.channelsListItemThreadActionsMarkAsRead");
        setOnClickAndDismissListener(textView2, new WidgetChannelsListItemThreadActions$configureUI$1(model));
        TextView textView3 = getBinding().l;
        m.checkNotNullExpressionValue(textView3, "binding.channelsListItemThreadActionsMute");
        Channel channel2 = model.getChannel();
        boolean isMuted = model.isMuted();
        Context requireContext2 = requireContext();
        m.checkNotNullExpressionValue(requireContext2, "requireContext()");
        textView3.setText(getMuteThreadText(channel2, isMuted, requireContext2));
        TextView textView4 = getBinding().l;
        boolean isMuted2 = model.isMuted();
        Context requireContext3 = requireContext();
        m.checkNotNullExpressionValue(requireContext3, "requireContext()");
        textView4.setCompoundDrawablesWithIntrinsicBounds(getMuteIconResId(isMuted2, requireContext3), 0, 0, 0);
        TextView textView5 = getBinding().l;
        m.checkNotNullExpressionValue(textView5, "binding.channelsListItemThreadActionsMute");
        setOnClickAndDismissListener(textView5, new WidgetChannelsListItemThreadActions$configureUI$2(this, model));
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelNotificationSettings.Companion companion = WidgetChannelNotificationSettings.Companion;
                Context requireContext4 = WidgetChannelsListItemThreadActions.this.requireContext();
                m.checkNotNullExpressionValue(requireContext4, "requireContext()");
                WidgetChannelNotificationSettings.Companion.launch$default(companion, requireContext4, model.getChannel().h(), false, 4, null);
            }
        });
        View view = getBinding().g;
        m.checkNotNullExpressionValue(view, "binding.channelsListItemThreadActionsDivider");
        view.setVisibility(model.isActiveThread() ? 0 : 8);
        TextView textView6 = getBinding().h;
        m.checkNotNullExpressionValue(textView6, "binding.channelsListItemThreadActionsEdit");
        textView6.setVisibility(model.getCanManageThread() ? 0 : 8);
        TextView textView7 = getBinding().h;
        m.checkNotNullExpressionValue(textView7, "binding.channelsListItemThreadActionsEdit");
        setOnClickAndDismissListener(textView7, new WidgetChannelsListItemThreadActions$configureUI$4(model));
        boolean z2 = true;
        boolean z3 = model.getCanManageThread() && ((y3 = model.getChannel().y()) == null || !y3.b());
        LinearLayout linearLayout = getBinding().f2280b;
        m.checkNotNullExpressionValue(linearLayout, "binding.channelsListItem…eadActionsArchiveSettings");
        linearLayout.setVisibility(z3 ? 0 : 8);
        if (z3) {
            ThreadUtils threadUtils = ThreadUtils.INSTANCE;
            Context requireContext4 = requireContext();
            m.checkNotNullExpressionValue(requireContext4, "requireContext()");
            ThreadMetadata y4 = model.getChannel().y();
            m.checkNotNull(y4);
            String autoArchiveDurationName = threadUtils.autoArchiveDurationName(requireContext4, y4.c());
            TextView textView8 = getBinding().c;
            m.checkNotNullExpressionValue(textView8, "binding.channelsListItem…sArchiveSettingsSubheader");
            Locale locale = Locale.getDefault();
            m.checkNotNullExpressionValue(locale, "Locale.getDefault()");
            Objects.requireNonNull(autoArchiveDurationName, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = autoArchiveDurationName.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            e = b.e(this, R.string.auto_archive_thread_after, new Object[]{lowerCase}, (r4 & 4) != 0 ? b.a.j : null);
            textView8.setText(e);
        }
        LinearLayout linearLayout2 = getBinding().f2280b;
        m.checkNotNullExpressionValue(linearLayout2, "binding.channelsListItem…eadActionsArchiveSettings");
        setOnClickAndDismissListener(linearLayout2, new WidgetChannelsListItemThreadActions$configureUI$5(this, model));
        TextView textView9 = getBinding().n;
        m.checkNotNullExpressionValue(textView9, "binding.channelsListItemThreadActionsUnarchive");
        textView9.setVisibility(model.getCanUnarchiveThread() && (y2 = model.getChannel().y()) != null && y2.b() ? 0 : 8);
        getBinding().n.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions$configureUI$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChannelsListItemThreadActions.this.unarchiveThread(model.getChannel(), model.isModerator());
            }
        });
        TextView textView10 = getBinding().j;
        m.checkNotNullExpressionValue(textView10, "binding.channelsListItemThreadActionsLeave");
        if (model.getJoinedThread() == null) {
            z2 = false;
        }
        textView10.setVisibility(z2 ? 0 : 8);
        getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions$configureUI$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChannelsListItemThreadActions.this.leaveThread(model.getChannel());
            }
        });
        View view2 = getBinding().f;
        m.checkNotNullExpressionValue(view2, "binding.channelsListItem…adActionsDeveloperDivider");
        view2.setVisibility(model.isDeveloper() ? 0 : 8);
        TextView textView11 = getBinding().e;
        m.checkNotNullExpressionValue(textView11, "binding.channelsListItemThreadActionsCopyId");
        if (!model.isDeveloper()) {
            i = 8;
        }
        textView11.setVisibility(i);
        TextView textView12 = getBinding().e;
        m.checkNotNullExpressionValue(textView12, "binding.channelsListItemThreadActionsCopyId");
        setOnClickAndDismissListener(textView12, new WidgetChannelsListItemThreadActions$configureUI$8(model));
    }

    private final WidgetChannelsListItemThreadActionsBinding getBinding() {
        return (WidgetChannelsListItemThreadActionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final int getMuteIconResId(boolean z2, Context context) {
        return DrawableCompat.getThemedDrawableRes$default(context, z2 ? R.attr.ic_channel_muted : R.attr.ic_channel_mute, 0, 2, (Object) null);
    }

    private final CharSequence getMuteThreadText(Channel channel, boolean z2, Context context) {
        CharSequence b2;
        CharSequence b3;
        if (z2) {
            b3 = b.b(context, R.string.unmute, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        }
        b2 = b.b(context, R.string.mute_channel, new Object[]{ChannelUtils.e(channel, context, false, 2)}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void leaveThread(Channel channel) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().leaveThread(channel.h(), "Context Menu"), false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : getContext(), "REST: leaveThread", (r18 & 4) != 0 ? null : null, new WidgetChannelsListItemThreadActions$leaveThread$1(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    public static final void show(FragmentManager fragmentManager, long j) {
        Companion.show(fragmentManager, j);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void unarchiveThread(Channel channel, boolean z2) {
        RestAPI api = RestAPI.Companion.getApi();
        long h = channel.h();
        Boolean bool = Boolean.FALSE;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.editThread(h, new RestAPIParams.ThreadSettings(bool, z2 ? bool : null, null, 4, null)), false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : getContext(), "REST: unarchiveThread", (r18 & 4) != 0 ? null : null, new WidgetChannelsListItemThreadActions$unarchiveThread$2(this), (r18 & 16) != 0 ? null : new WidgetChannelsListItemThreadActions$unarchiveThread$1(this), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        super.bindSubscriptions(compositeSubscription);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getArgumentsOrDefault().getLong(INTENT_EXTRA_CHANNEL_ID, -1L)), this, null, 2, null), WidgetChannelsListItemThreadActions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelsListItemThreadActions$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_channels_list_item_thread_actions;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
