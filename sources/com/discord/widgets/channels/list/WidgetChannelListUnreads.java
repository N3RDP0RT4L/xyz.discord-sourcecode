package com.discord.widgets.channels.list;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.WidgetChannelsListUnreadsBinding;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.layoutparams.LayoutParamsExtensionsKt;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.google.android.material.appbar.AppBarLayout;
import d0.d0.f;
import d0.g;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntProgression;
import kotlin.ranges.IntRange;
import kotlinx.coroutines.CoroutineScope;
import xyz.discord.R;
/* compiled from: WidgetChannelListUnreads.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0016\u0018\u0000 92\u00020\u0001:\u00039:;BO\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010$\u001a\u00020#\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0016\u0012\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\f0\u001f\u0012\b\b\u0002\u0010.\u001a\u00020\f\u0012\b\b\u0002\u00104\u001a\u00020\f\u0012\b\b\u0002\u0010)\u001a\u00020\u0004¢\u0006\u0004\b7\u00108J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\u000b\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\bJ\u0013\u0010\r\u001a\u00020\f*\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0013\u0010\u000f\u001a\u00020\f*\u00020\u0002H\u0002¢\u0006\u0004\b\u000f\u0010\u000eJ\u001b\u0010\u0012\u001a\u00020\u00062\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00010\u0010¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001c\u0010 \u001a\b\u0012\u0004\u0012\u00020\f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u001c\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\"\u0010)\u001a\u00020\u00048\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b)\u0010\u0015\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\"\u0010.\u001a\u00020\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b.\u0010/\u001a\u0004\b0\u00101\"\u0004\b2\u00103R\"\u00104\u001a\u00020\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b4\u0010/\u001a\u0004\b5\u00101\"\u0004\b6\u00103¨\u0006<"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;", "", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "indicator", "", "indicatorUpwards", "", "onConfigureView", "(Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;Z)V", "handleVisibleRangeUpdate", "()V", "handleClick", "", "getIcon", "(Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;)I", "getText", "", "data", "onDatasetChanged", "(Ljava/util/List;)V", "unreadsInitialized", "Z", "Lcom/google/android/material/appbar/AppBarLayout;", "appBarLayout", "Lcom/google/android/material/appbar/AppBarLayout;", "Lcom/discord/databinding/WidgetChannelsListUnreadsBinding;", "binding$delegate", "Lkotlin/Lazy;", "getBinding", "()Lcom/discord/databinding/WidgetChannelsListUnreadsBinding;", "binding", "Lkotlin/Function0;", "getItemCount", "Lkotlin/jvm/functions/Function0;", "Ljava/util/List;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", "Landroidx/recyclerview/widget/RecyclerView;", "Landroid/view/ViewStub;", "unreadsStub", "Landroid/view/ViewStub;", "unreadsEnabled", "getUnreadsEnabled", "()Z", "setUnreadsEnabled", "(Z)V", "mentionResId", "I", "getMentionResId", "()I", "setMentionResId", "(I)V", "unreadsResId", "getUnreadsResId", "setUnreadsResId", HookHelper.constructorName, "(Landroid/view/ViewStub;Landroidx/recyclerview/widget/RecyclerView;Lcom/google/android/material/appbar/AppBarLayout;Lkotlin/jvm/functions/Function0;IIZ)V", "Companion", ExifInterface.TAG_MODEL, "UnreadItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelListUnreads {
    private static final Companion Companion = new Companion(null);
    @Deprecated
    private static final long FADE_DURATION_MS = 150;
    @Deprecated
    private static final int ITEM_OVER_SCROLL_COUNT = 3;
    private final AppBarLayout appBarLayout;
    private final Lazy binding$delegate;
    private List<? extends Object> data;
    private final Function0<Integer> getItemCount;
    private int mentionResId;
    private final RecyclerView recycler;
    private boolean unreadsEnabled;
    private boolean unreadsInitialized;
    private int unreadsResId;
    private final ViewStub unreadsStub;

    /* compiled from: WidgetChannelListUnreads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Companion;", "", "", "FADE_DURATION_MS", "J", "", "ITEM_OVER_SCROLL_COUNT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelListUnreads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u0000 \u00192\u00020\u0001:\u0002\u0019\u001aB\u001f\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J(\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0006\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u001b\u0010\u0007\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;", "", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "component1", "()Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "component2", "topIndicator", "bottomIndicator", "copy", "(Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "getTopIndicator", "getBottomIndicator", HookHelper.constructorName, "(Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;)V", "Companion", "Indicator", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        public static final int INVALID_INDEX = -1;
        public static final int TYPE_MENTION = 0;
        public static final int TYPE_UNREAD = 1;
        private final Indicator bottomIndicator;
        private final Indicator topIndicator;

        /* compiled from: WidgetChannelListUnreads.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u0006\u001a\u00020\u0005*\b\u0012\u0004\u0012\u00020\u00010\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J3\u0010\u000b\u001a\u0004\u0018\u00010\n*\b\u0012\u0004\u0012\u00020\u00010\u00022\u0006\u0010\u0004\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000b\u0010\fJ-\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\u00032\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00010\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u00128\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00128\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0014¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;", "", "", "Lkotlin/ranges/IntRange;", "range", "", "hasUnread", "(Ljava/util/List;Lkotlin/ranges/IntRange;)Z", "Lkotlin/ranges/IntProgression;", "unreadsEnabled", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "findIndicator", "(Ljava/util/List;Lkotlin/ranges/IntProgression;ZZ)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "visibleRange", "items", "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;", "get", "(Lkotlin/ranges/IntRange;Ljava/util/List;Z)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;", "", "INVALID_INDEX", "I", "TYPE_MENTION", "TYPE_UNREAD", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            private final Indicator findIndicator(List<? extends Object> list, IntProgression intProgression, boolean z2, boolean z3) {
                int i;
                int first = intProgression.getFirst();
                int last = intProgression.getLast();
                int step = intProgression.getStep();
                if (step < 0 ? first >= last : first <= last) {
                    i = -1;
                    while (true) {
                        Object obj = list.get(first);
                        if (!(obj instanceof UnreadItem)) {
                            obj = null;
                        }
                        UnreadItem unreadItem = (UnreadItem) obj;
                        if (unreadItem != null) {
                            if (unreadItem.getMentionCount() > 0) {
                                return new Indicator(0, first);
                            }
                            if (z3 && !z2 && i == -1 && unreadItem.isUnread()) {
                                i = first;
                            }
                        }
                        if (first == last) {
                            break;
                        }
                        first += step;
                    }
                } else {
                    i = -1;
                }
                if (i != -1) {
                    return new Indicator(1, i);
                }
                return null;
            }

            public static /* synthetic */ Model get$default(Companion companion, IntRange intRange, List list, boolean z2, int i, Object obj) {
                if ((i & 4) != 0) {
                    z2 = true;
                }
                return companion.get(intRange, list, z2);
            }

            private final boolean hasUnread(List<? extends Object> list, IntRange intRange) {
                boolean z2;
                List slice = u.slice(list, intRange);
                if (!(slice instanceof Collection) || !slice.isEmpty()) {
                    for (Object obj : slice) {
                        if (!(obj instanceof UnreadItem)) {
                            obj = null;
                        }
                        UnreadItem unreadItem = (UnreadItem) obj;
                        if (unreadItem == null || !unreadItem.isUnread()) {
                            z2 = false;
                            continue;
                        } else {
                            z2 = true;
                            continue;
                        }
                        if (z2) {
                            return true;
                        }
                    }
                }
                return false;
            }

            public final Model get(IntRange intRange, List<? extends Object> list, boolean z2) {
                boolean z3;
                m.checkNotNullParameter(intRange, "visibleRange");
                m.checkNotNullParameter(list, "items");
                if (z2) {
                    try {
                        if (hasUnread(list, intRange)) {
                            z3 = true;
                            return new Model(findIndicator(list, f.until(0, intRange.getFirst()), z3, z2), findIndicator(list, f.downTo(n.getLastIndex(list), intRange.getLast() + 1), z3, z2));
                        }
                    } catch (IndexOutOfBoundsException unused) {
                        return new Model(null, null, 3, null);
                    }
                }
                z3 = false;
                return new Model(findIndicator(list, f.until(0, intRange.getFirst()), z3, z2), findIndicator(list, f.downTo(n.getLastIndex(list), intRange.getLast() + 1), z3, z2));
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetChannelListUnreads.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "", "", "component1", "()I", "component2", "type", "index", "copy", "(II)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getType", "getIndex", HookHelper.constructorName, "(II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Indicator {
            private final int index;
            private final int type;

            public Indicator(int i, int i2) {
                this.type = i;
                this.index = i2;
            }

            public static /* synthetic */ Indicator copy$default(Indicator indicator, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = indicator.type;
                }
                if ((i3 & 2) != 0) {
                    i2 = indicator.index;
                }
                return indicator.copy(i, i2);
            }

            public final int component1() {
                return this.type;
            }

            public final int component2() {
                return this.index;
            }

            public final Indicator copy(int i, int i2) {
                return new Indicator(i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Indicator)) {
                    return false;
                }
                Indicator indicator = (Indicator) obj;
                return this.type == indicator.type && this.index == indicator.index;
            }

            public final int getIndex() {
                return this.index;
            }

            public final int getType() {
                return this.type;
            }

            public int hashCode() {
                return (this.type * 31) + this.index;
            }

            public String toString() {
                StringBuilder R = a.R("Indicator(type=");
                R.append(this.type);
                R.append(", index=");
                return a.A(R, this.index, ")");
            }
        }

        public Model() {
            this(null, null, 3, null);
        }

        public Model(Indicator indicator, Indicator indicator2) {
            this.topIndicator = indicator;
            this.bottomIndicator = indicator2;
        }

        public static /* synthetic */ Model copy$default(Model model, Indicator indicator, Indicator indicator2, int i, Object obj) {
            if ((i & 1) != 0) {
                indicator = model.topIndicator;
            }
            if ((i & 2) != 0) {
                indicator2 = model.bottomIndicator;
            }
            return model.copy(indicator, indicator2);
        }

        public final Indicator component1() {
            return this.topIndicator;
        }

        public final Indicator component2() {
            return this.bottomIndicator;
        }

        public final Model copy(Indicator indicator, Indicator indicator2) {
            return new Model(indicator, indicator2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.topIndicator, model.topIndicator) && m.areEqual(this.bottomIndicator, model.bottomIndicator);
        }

        public final Indicator getBottomIndicator() {
            return this.bottomIndicator;
        }

        public final Indicator getTopIndicator() {
            return this.topIndicator;
        }

        public int hashCode() {
            Indicator indicator = this.topIndicator;
            int i = 0;
            int hashCode = (indicator != null ? indicator.hashCode() : 0) * 31;
            Indicator indicator2 = this.bottomIndicator;
            if (indicator2 != null) {
                i = indicator2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(topIndicator=");
            R.append(this.topIndicator);
            R.append(", bottomIndicator=");
            R.append(this.bottomIndicator);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ Model(Indicator indicator, Indicator indicator2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : indicator, (i & 2) != 0 ? null : indicator2);
        }
    }

    /* compiled from: WidgetChannelListUnreads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0003\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00058&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;", "", "", "isUnread", "()Z", "", "getMentionCount", "()I", "mentionCount", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface UnreadItem {
        int getMentionCount();

        boolean isUnread();
    }

    public WidgetChannelListUnreads(ViewStub viewStub, RecyclerView recyclerView, AppBarLayout appBarLayout, Function0<Integer> function0, int i, int i2, boolean z2) {
        m.checkNotNullParameter(viewStub, "unreadsStub");
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(function0, "getItemCount");
        this.unreadsStub = viewStub;
        this.recycler = recyclerView;
        this.appBarLayout = appBarLayout;
        this.getItemCount = function0;
        this.mentionResId = i;
        this.unreadsResId = i2;
        this.unreadsEnabled = z2;
        this.binding$delegate = g.lazy(new WidgetChannelListUnreads$binding$2(this));
        this.data = n.emptyList();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.channels.list.WidgetChannelListUnreads.1
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrollStateChanged(RecyclerView recyclerView2, int i3) {
                m.checkNotNullParameter(recyclerView2, "recyclerView");
                super.onScrollStateChanged(recyclerView2, i3);
                if (i3 == 0 || i3 == 2) {
                    WidgetChannelListUnreads.this.handleVisibleRangeUpdate();
                }
            }

            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrolled(RecyclerView recyclerView2, int i3, int i4) {
                m.checkNotNullParameter(recyclerView2, "recyclerView");
                super.onScrolled(recyclerView2, i3, i4);
                if (AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
                    WidgetChannelListUnreads.this.handleVisibleRangeUpdate();
                }
            }
        });
    }

    private final WidgetChannelsListUnreadsBinding getBinding() {
        return (WidgetChannelsListUnreadsBinding) this.binding$delegate.getValue();
    }

    private final int getIcon(Model.Indicator indicator) {
        return indicator.getType() != 0 ? R.drawable.drawable_button_grey : R.drawable.drawable_button_red;
    }

    private final int getText(Model.Indicator indicator) {
        if (indicator.getType() != 0) {
            return this.unreadsResId;
        }
        return this.mentionResId;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleClick(Model.Indicator indicator, boolean z2) {
        int i;
        AppBarLayout appBarLayout;
        if (z2) {
            i = Math.max(indicator.getIndex() - 3, 0);
        } else {
            i = Math.min(indicator.getIndex() + 3, this.getItemCount.invoke().intValue() - 1);
        }
        if (!z2 && (appBarLayout = this.appBarLayout) != null) {
            appBarLayout.setExpanded(false);
        }
        if (AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
            this.recycler.scrollToPosition(i);
        } else {
            this.recycler.smoothScrollToPosition(i);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleVisibleRangeUpdate() {
        RecyclerView.LayoutManager layoutManager = this.recycler.getLayoutManager();
        if (!(layoutManager instanceof LinearLayoutManager)) {
            layoutManager = null;
        }
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
        if (linearLayoutManager != null) {
            Model model = Model.Companion.get(new IntRange(linearLayoutManager.findFirstVisibleItemPosition(), linearLayoutManager.findLastVisibleItemPosition()), this.data, this.unreadsEnabled);
            final Model.Indicator topIndicator = model.getTopIndicator();
            if (topIndicator == null) {
                topIndicator = model.getBottomIndicator();
            }
            final boolean z2 = model.getTopIndicator() != null;
            if (topIndicator == null && this.unreadsInitialized) {
                ViewExtensions.fadeOut$default(getBinding().a, 150L, null, null, 6, null);
            } else if (topIndicator != null) {
                ViewExtensions.fadeIn$default(getBinding().a, 150L, null, null, null, 14, null);
                getBinding().a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.list.WidgetChannelListUnreads$handleVisibleRangeUpdate$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetChannelListUnreads.this.handleClick(topIndicator, z2);
                    }
                });
                this.unreadsInitialized = true;
                onConfigureView(topIndicator, z2);
            }
        }
    }

    private final void onConfigureView(Model.Indicator indicator, boolean z2) {
        getBinding().c.setBackgroundResource(getIcon(indicator));
        getBinding().f2283b.setText(getText(indicator));
        TextView textView = getBinding().f2283b;
        m.checkNotNullExpressionValue(textView, "binding.channelsListUnreads");
        DrawableCompat.setCompoundDrawablesCompat$default(textView, 0, 0, z2 ? R.drawable.ic_arrow_upward_white_16dp : R.drawable.ic_arrow_downward_white_16dp, 0, 11, (Object) null);
        FrameLayout frameLayout = getBinding().a;
        m.checkNotNullExpressionValue(frameLayout, "it");
        ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
        if (!(layoutParams instanceof ViewGroup.MarginLayoutParams)) {
            layoutParams = null;
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        if (marginLayoutParams instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) marginLayoutParams;
            int i = 8;
            LayoutParamsExtensionsKt.removeRuleCompat(layoutParams2, z2 ? 8 : 6);
            if (z2) {
                i = 6;
            }
            layoutParams2.addRule(i, this.recycler.getId());
        } else if (marginLayoutParams instanceof CoordinatorLayout.LayoutParams) {
            CoordinatorLayout.LayoutParams layoutParams3 = (CoordinatorLayout.LayoutParams) marginLayoutParams;
            int i2 = 48;
            layoutParams3.anchorGravity = z2 ? 48 : 80;
            if (z2) {
                i2 = 80;
            }
            layoutParams3.gravity = i2;
        } else {
            throw new IllegalStateException("Unread Layout Params unrecognized");
        }
        FrameLayout frameLayout2 = getBinding().a;
        m.checkNotNullExpressionValue(frameLayout2, "binding.root");
        frameLayout2.setLayoutParams(marginLayoutParams);
        FrameLayout frameLayout3 = getBinding().a;
        m.checkNotNullExpressionValue(frameLayout3, "binding.root");
        frameLayout3.setImportantForAccessibility(4);
    }

    public final int getMentionResId() {
        return this.mentionResId;
    }

    public final boolean getUnreadsEnabled() {
        return this.unreadsEnabled;
    }

    public final int getUnreadsResId() {
        return this.unreadsResId;
    }

    public final void onDatasetChanged(List<? extends Object> list) {
        m.checkNotNullParameter(list, "data");
        this.data = list;
        CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(this.recycler);
        if (coroutineScope != null) {
            b.i.a.f.e.o.f.H0(coroutineScope, null, null, new WidgetChannelListUnreads$onDatasetChanged$1(this, null), 3, null);
        }
    }

    public final void setMentionResId(int i) {
        this.mentionResId = i;
    }

    public final void setUnreadsEnabled(boolean z2) {
        this.unreadsEnabled = z2;
    }

    public final void setUnreadsResId(int i) {
        this.unreadsResId = i;
    }

    public /* synthetic */ WidgetChannelListUnreads(ViewStub viewStub, RecyclerView recyclerView, AppBarLayout appBarLayout, Function0 function0, int i, int i2, boolean z2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(viewStub, recyclerView, (i3 & 4) != 0 ? null : appBarLayout, function0, (i3 & 16) != 0 ? R.string.new_mentions : i, (i3 & 32) != 0 ? R.string.new_unreads : i2, (i3 & 64) != 0 ? true : z2);
    }
}
