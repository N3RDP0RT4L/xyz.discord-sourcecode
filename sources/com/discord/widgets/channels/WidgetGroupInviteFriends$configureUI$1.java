package com.discord.widgets.channels;

import com.discord.models.user.User;
import com.discord.widgets.channels.WidgetGroupInviteFriends;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetGroupInviteFriends.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/models/user/User;", "user", "", "selected", "", "invoke", "(Lcom/discord/models/user/User;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGroupInviteFriends$configureUI$1 extends o implements Function2<User, Boolean, Unit> {
    public final /* synthetic */ WidgetGroupInviteFriends.Model $data;
    public final /* synthetic */ WidgetGroupInviteFriends this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGroupInviteFriends$configureUI$1(WidgetGroupInviteFriends widgetGroupInviteFriends, WidgetGroupInviteFriends.Model model) {
        super(2);
        this.this$0 = widgetGroupInviteFriends;
        this.$data = model;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(User user, Boolean bool) {
        invoke(user, bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(User user, boolean z2) {
        m.checkNotNullParameter(user, "user");
        if (!z2) {
            this.this$0.unselectUser(user);
        } else if (this.$data.getTotalNumRecipients() >= this.$data.getMaxGroupMemberCount()) {
            b.a.d.m.i(this.this$0, R.string.group_dm_invite_full_sub, 0, 4);
        } else {
            this.this$0.selectUser(user);
        }
    }
}
