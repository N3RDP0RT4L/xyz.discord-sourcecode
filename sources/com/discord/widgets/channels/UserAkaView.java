package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.SpannableString;
import android.text.style.LeadingMarginSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.OneShotPreDrawListener;
import com.discord.databinding.UserAkaViewBinding;
import com.discord.models.member.GuildMember;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.views.PileView;
import d0.g0.t;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: UserAkaView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u001a\u001a\u00020\u0019\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u001b\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 J\u001b\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007R(\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0004\u0010\b\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\u0007R\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u001d\u0010\u0011\u001a\u00060\u000fj\u0002`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R(\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010\b\u001a\u0004\b\u0017\u0010\n\"\u0004\b\u0018\u0010\u0007¨\u0006!"}, d2 = {"Lcom/discord/widgets/channels/UserAkaView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "Lcom/discord/models/member/GuildMember;", "guildMembers", "", "configure", "(Ljava/util/List;)V", "Ljava/util/List;", "getGuildMembers", "()Ljava/util/List;", "setGuildMembers", "Lcom/discord/databinding/UserAkaViewBinding;", "binding", "Lcom/discord/databinding/UserAkaViewBinding;", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "nicknameStringBuilder", "Ljava/lang/StringBuilder;", "getNicknameStringBuilder", "()Ljava/lang/StringBuilder;", "Lcom/discord/views/PileView$c;", "pileItems", "getPileItems", "setPileItems", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserAkaView extends ConstraintLayout {
    private final UserAkaViewBinding binding;
    private List<GuildMember> guildMembers;
    private final StringBuilder nicknameStringBuilder;
    private List<PileView.c> pileItems;

    public UserAkaView(Context context) {
        this(context, null, 0, 6, null);
    }

    public UserAkaView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ UserAkaView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    public final void configure(final List<GuildMember> list) {
        m.checkNotNullParameter(list, "guildMembers");
        this.guildMembers = list;
        float dimension = getResources().getDimension(R.dimen.aka_avatar_width);
        ArrayList<GuildMember> arrayList = new ArrayList();
        for (Object obj : list) {
            if (((GuildMember) obj).hasAvatar()) {
                arrayList.add(obj);
            }
        }
        ArrayList<String> arrayList2 = new ArrayList();
        for (GuildMember guildMember : arrayList) {
            String forGuildMember$default = IconUtils.getForGuildMember$default(IconUtils.INSTANCE, guildMember, Integer.valueOf((int) dimension), false, 4, null);
            if (forGuildMember$default != null) {
                arrayList2.add(forGuildMember$default);
            }
        }
        ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(arrayList2, 10));
        for (String str : arrayList2) {
            arrayList3.add(new PileView.c(new UserAkaView$configure$1$1(str), null));
        }
        this.pileItems = arrayList3;
        this.binding.c.setItems(arrayList3);
        m.checkNotNullExpressionValue(OneShotPreDrawListener.add(this, new Runnable() { // from class: com.discord.widgets.channels.UserAkaView$configure$$inlined$doOnPreDraw$1
            @Override // java.lang.Runnable
            public final void run() {
                Number number;
                UserAkaViewBinding userAkaViewBinding;
                UserAkaViewBinding userAkaViewBinding2;
                UserAkaViewBinding userAkaViewBinding3;
                UserAkaViewBinding userAkaViewBinding4;
                UserAkaViewBinding userAkaViewBinding5;
                List<GuildMember> list2 = list;
                ArrayList arrayList4 = new ArrayList();
                for (GuildMember guildMember2 : list2) {
                    String nick = guildMember2.getNick();
                    if (nick != null) {
                        arrayList4.add(nick);
                    }
                }
                ArrayList arrayList5 = new ArrayList();
                for (Object obj2 : arrayList4) {
                    if (!t.isBlank((String) obj2)) {
                        arrayList5.add(obj2);
                    }
                }
                int i = 8;
                if (this.getPileItems().isEmpty()) {
                    userAkaViewBinding5 = this.binding;
                    TextView textView = userAkaViewBinding5.e;
                    m.checkNotNullExpressionValue(textView, "binding.spaceForMeasuring");
                    number = Float.valueOf(textView.getMeasuredWidth());
                } else {
                    number = Integer.valueOf(DimenUtils.dpToPixels(8));
                }
                userAkaViewBinding = this.binding;
                TextView textView2 = userAkaViewBinding.f2145b;
                m.checkNotNullExpressionValue(textView2, "binding.akaText");
                userAkaViewBinding2 = this.binding;
                PileView pileView = userAkaViewBinding2.c;
                m.checkNotNullExpressionValue(pileView, "binding.avatarsPileView");
                float floatValue = number.floatValue() + textView2.getMeasuredWidth() + pileView.getMeasuredWidth();
                SpannableString spannableString = new SpannableString(u.joinToString$default(arrayList5, ", ", null, null, 0, null, null, 62, null));
                spannableString.setSpan(new LeadingMarginSpan.Standard((int) floatValue, 0), 0, spannableString.length(), 33);
                userAkaViewBinding3 = this.binding;
                TextView textView3 = userAkaViewBinding3.d;
                m.checkNotNullExpressionValue(textView3, "binding.nicknames");
                textView3.setText(spannableString);
                userAkaViewBinding4 = this.binding;
                TextView textView4 = userAkaViewBinding4.d;
                m.checkNotNullExpressionValue(textView4, "binding.nicknames");
                if (!arrayList5.isEmpty()) {
                    i = 0;
                }
                textView4.setVisibility(i);
            }
        }), "View.doOnPreDraw(\n    crossinline action: (view: View) -> Unit\n): OneShotPreDrawListener = OneShotPreDrawListener.add(this) { action(this) }");
    }

    public final List<GuildMember> getGuildMembers() {
        return this.guildMembers;
    }

    public final StringBuilder getNicknameStringBuilder() {
        return this.nicknameStringBuilder;
    }

    public final List<PileView.c> getPileItems() {
        return this.pileItems;
    }

    public final void setGuildMembers(List<GuildMember> list) {
        m.checkNotNullParameter(list, "<set-?>");
        this.guildMembers = list;
    }

    public final void setPileItems(List<PileView.c> list) {
        m.checkNotNullParameter(list, "<set-?>");
        this.pileItems = list;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserAkaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        this.guildMembers = n.emptyList();
        this.pileItems = n.emptyList();
        this.nicknameStringBuilder = new StringBuilder();
        LayoutInflater.from(context).inflate(R.layout.user_aka_view, this);
        int i2 = R.id.aka_text;
        TextView textView = (TextView) findViewById(R.id.aka_text);
        if (textView != null) {
            i2 = R.id.avatars_pile_view;
            PileView pileView = (PileView) findViewById(R.id.avatars_pile_view);
            if (pileView != null) {
                i2 = R.id.nicknames;
                TextView textView2 = (TextView) findViewById(R.id.nicknames);
                if (textView2 != null) {
                    i2 = R.id.space_for_measuring;
                    TextView textView3 = (TextView) findViewById(R.id.space_for_measuring);
                    if (textView3 != null) {
                        UserAkaViewBinding userAkaViewBinding = new UserAkaViewBinding(this, textView, pileView, textView2, textView3);
                        m.checkNotNullExpressionValue(userAkaViewBinding, "UserAkaViewBinding.infla…ater.from(context), this)");
                        this.binding = userAkaViewBinding;
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i2)));
    }
}
