package com.discord.widgets.channels;

import androidx.core.app.NotificationCompat;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.channels.WidgetGroupInviteFriends;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: WidgetGroupInviteFriends.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion$AddedUsersInput;", "kotlin.jvm.PlatformType", "usersFilter", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion$AddedUsersInput;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGroupInviteFriends$Model$Companion$getForCreate$2<T, R> implements b<WidgetGroupInviteFriends.Model.Companion.AddedUsersInput, Observable<? extends WidgetGroupInviteFriends.Model>> {
    public static final WidgetGroupInviteFriends$Model$Companion$getForCreate$2 INSTANCE = new WidgetGroupInviteFriends$Model$Companion$getForCreate$2();

    public final Observable<? extends WidgetGroupInviteFriends.Model> call(final WidgetGroupInviteFriends.Model.Companion.AddedUsersInput addedUsersInput) {
        Observable filteredFriends;
        filteredFriends = WidgetGroupInviteFriends.Model.Companion.getFilteredFriends(null, addedUsersInput.getFilter());
        return (Observable<R>) filteredFriends.F(new b<WidgetGroupInviteFriends.Model.ModelAppUserRelationship, List<? extends WidgetGroupInviteFriends.Model.FriendItem>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getForCreate$2.1
            public final List<WidgetGroupInviteFriends.Model.FriendItem> call(WidgetGroupInviteFriends.Model.ModelAppUserRelationship modelAppUserRelationship) {
                WidgetGroupInviteFriends.Model.FriendItem.Companion companion = WidgetGroupInviteFriends.Model.FriendItem.Companion;
                m.checkNotNullExpressionValue(modelAppUserRelationship, "friends");
                return companion.createData(modelAppUserRelationship, WidgetGroupInviteFriends.Model.Companion.AddedUsersInput.this.getAddedUsers());
            }
        }).Y(new b<List<? extends WidgetGroupInviteFriends.Model.FriendItem>, Observable<? extends WidgetGroupInviteFriends.Model>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getForCreate$2.2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends WidgetGroupInviteFriends.Model> call(List<? extends WidgetGroupInviteFriends.Model.FriendItem> list) {
                return call2((List<WidgetGroupInviteFriends.Model.FriendItem>) list);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends WidgetGroupInviteFriends.Model> call2(List<WidgetGroupInviteFriends.Model.FriendItem> list) {
                String filter = WidgetGroupInviteFriends.Model.Companion.AddedUsersInput.this.getFilter();
                Collection<User> addedUsers = WidgetGroupInviteFriends.Model.Companion.AddedUsersInput.this.getAddedUsers();
                m.checkNotNullExpressionValue(list, "friendItems");
                return new k(new WidgetGroupInviteFriends.Model(null, filter, addedUsers, list, 0, UserUtils.INSTANCE.isStaff(StoreStream.Companion.getUsers().getMe()) ? 25 : 10));
            }
        });
    }
}
