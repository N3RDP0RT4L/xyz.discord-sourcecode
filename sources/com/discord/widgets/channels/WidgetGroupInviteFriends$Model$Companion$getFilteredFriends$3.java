package com.discord.widgets.channels;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.widgets.channels.WidgetGroupInviteFriends;
import d0.z.d.m;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func3;
/* compiled from: WidgetGroupInviteFriends.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\f\u001a*\u0012\u000e\b\u0001\u0012\n \u0003*\u0004\u0018\u00010\t0\t \u0003*\u0014\u0012\u000e\b\u0001\u0012\n \u0003*\u0004\u0018\u00010\t0\t\u0018\u00010\b0\b2n\u0010\u0007\u001aj\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00050\u0004j\u0002`\u0005 \u0003*4\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00050\u0004j\u0002`\u0005\u0018\u00010\u00060\u0000H\n¢\u0006\u0004\b\n\u0010\u000b"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "kotlin.jvm.PlatformType", "", "Lcom/discord/primitives/RelationshipType;", "", "filteredFriends", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$ModelAppUserRelationship;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$3<T, R> implements b<Map<Long, Integer>, Observable<? extends WidgetGroupInviteFriends.Model.ModelAppUserRelationship>> {
    public static final WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$3 INSTANCE = new WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$3();

    public final Observable<? extends WidgetGroupInviteFriends.Model.ModelAppUserRelationship> call(final Map<Long, Integer> map) {
        StoreStream.Companion companion = StoreStream.Companion;
        return Observable.i(companion.getPresences().observePresencesForUsers(map.keySet()), companion.getUsers().observeUsers(map.keySet()), companion.getApplicationStreaming().observeStreamsByUser(), new Func3<Map<Long, ? extends Presence>, Map<Long, ? extends User>, Map<Long, ? extends ModelApplicationStream>, WidgetGroupInviteFriends.Model.ModelAppUserRelationship>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$3.1
            @Override // rx.functions.Func3
            public /* bridge */ /* synthetic */ WidgetGroupInviteFriends.Model.ModelAppUserRelationship call(Map<Long, ? extends Presence> map2, Map<Long, ? extends User> map3, Map<Long, ? extends ModelApplicationStream> map4) {
                return call2((Map<Long, Presence>) map2, map3, map4);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final WidgetGroupInviteFriends.Model.ModelAppUserRelationship call2(Map<Long, Presence> map2, Map<Long, ? extends User> map3, Map<Long, ? extends ModelApplicationStream> map4) {
                Map map5 = map;
                m.checkNotNullExpressionValue(map5, "filteredFriends");
                m.checkNotNullExpressionValue(map2, "presences");
                m.checkNotNullExpressionValue(map3, "users");
                m.checkNotNullExpressionValue(map4, "applicationStreams");
                return new WidgetGroupInviteFriends.Model.ModelAppUserRelationship(map5, map2, map3, map4);
            }
        });
    }
}
