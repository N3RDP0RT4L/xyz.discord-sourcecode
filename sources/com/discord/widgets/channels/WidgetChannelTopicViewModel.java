package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.Parser;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreTabsNavigation;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.channel.GuildChannelIconType;
import com.discord.utilities.channel.GuildChannelIconUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.textprocessing.MessageParseState;
import com.discord.utilities.textprocessing.MessagePreprocessor;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.widgets.channels.WidgetChannelTopicViewModel;
import d0.t.g0;
import d0.t.n0;
import d0.t.o0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func5;
import rx.subjects.PublishSubject;
/* compiled from: WidgetChannelTopicViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 82\b\u0012\u0004\u0012\u00020\u00020\u0001:\u000489:;B=\u0012\u000e\b\u0002\u00105\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0014\u0012$\b\u0002\u0010/\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020-0,j\u0002`.¢\u0006\u0004\b6\u00107J'\u0010\t\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005j\u0002`\b2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0013\u0010\u000fJ\u0013\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014¢\u0006\u0004\b\u0016\u0010\u0017J\u001b\u0010\u001a\u001a\u00020\r2\n\u0010\u0019\u001a\u0006\u0012\u0002\b\u00030\u0018H\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001e\u001a\u0004\u0018\u00010\r2\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fR\u001e\u0010\"\u001a\n\u0018\u00010 j\u0004\u0018\u0001`!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u001c\u0010)\u001a\b\u0012\u0004\u0012\u00020(0'8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010+\u001a\u00020$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010&R2\u0010/\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020-0,j\u0002`.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R:\u00103\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00150\u0015 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00150\u0015\u0018\u000101018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u0006<"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;", "", "rawTopicString", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/channels/AST;", "generateAST", "(Ljava/lang/CharSequence;)Ljava/util/List;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;)V", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;", "handleGuildStoreState", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;)V", "handlePrivateStoreState", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;", "listenForEvents", "()Lrx/Observable;", "Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "spoilerNode", "handleOnIndexClicked", "(Lcom/discord/utilities/textprocessing/node/SpoilerNode;)V", "Landroid/content/Context;", "context", "handleClosePrivateChannel", "(Landroid/content/Context;)Lkotlin/Unit;", "", "Lcom/discord/primitives/ChannelId;", "previousChannelId", "Ljava/lang/Long;", "", "wasRightPanelOpened", "Z", "", "", "revealedIndices", "Ljava/util/Set;", "wasOnHomeTab", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/utilities/textprocessing/MessageParseState;", "Lcom/discord/widgets/channels/TopicParser;", "topicParser", "Lcom/discord/simpleast/core/parser/Parser;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "storeStateObservable", HookHelper.constructorName, "(Lrx/Observable;Lcom/discord/simpleast/core/parser/Parser;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelTopicViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private Long previousChannelId;
    private Set<Integer> revealedIndices;
    private Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> topicParser;
    private boolean wasOnHomeTab;
    private boolean wasRightPanelOpened;

    /* compiled from: WidgetChannelTopicViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;", "kotlin.jvm.PlatformType", "navState", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.WidgetChannelTopicViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T, R> implements b<Companion.NavState, Observable<? extends StoreState>> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Observable<? extends StoreState> call(Companion.NavState navState) {
            if (navState.isRightPanelOpened()) {
                Companion companion = WidgetChannelTopicViewModel.Companion;
                m.checkNotNullExpressionValue(navState, "navState");
                return Companion.observeStoreState$default(companion, navState, null, null, null, null, null, 62, null);
            }
            Companion companion2 = WidgetChannelTopicViewModel.Companion;
            m.checkNotNullExpressionValue(navState, "navState");
            return Companion.observeStoreState$default(companion2, navState, null, null, null, null, null, 62, null).Z(1);
        }
    }

    /* compiled from: WidgetChannelTopicViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.WidgetChannelTopicViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetChannelTopicViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetChannelTopicViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001!B\t\b\u0002¢\u0006\u0004\b\u001f\u0010 J)\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJO\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00062\u0006\u0010\n\u001a\u00020\u00072\b\b\u0002\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u0010\u001a\u00020\u000f2\b\b\u0002\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J5\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00150\u00062\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJE\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00062\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u001d\u0010\u001e¨\u0006\""}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion;", "", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreTabsNavigation;", "storeTabsNavigation", "Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;", "observeNavState", "(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;)Lrx/Observable;", "navState", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "observeStoreState", "(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;)Lrx/Observable;", "Lcom/discord/api/channel/Channel;", "channel", "mapChannelToPrivateStoreState", "(Lcom/discord/api/channel/Channel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;)Lrx/Observable;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;", "mapChannelToGuildStoreState", "(Lcom/discord/api/channel/Channel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;)Lrx/Observable;", HookHelper.constructorName, "()V", "NavState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0011\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0007\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0006\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;", "", "", "component1", "()Z", "component2", "isRightPanelOpened", "isOnHomeTab", "copy", "(ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Companion$NavState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NavState {
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            public NavState(boolean z2, boolean z3) {
                this.isRightPanelOpened = z2;
                this.isOnHomeTab = z3;
            }

            public static /* synthetic */ NavState copy$default(NavState navState, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = navState.isRightPanelOpened;
                }
                if ((i & 2) != 0) {
                    z3 = navState.isOnHomeTab;
                }
                return navState.copy(z2, z3);
            }

            public final boolean component1() {
                return this.isRightPanelOpened;
            }

            public final boolean component2() {
                return this.isOnHomeTab;
            }

            public final NavState copy(boolean z2, boolean z3) {
                return new NavState(z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof NavState)) {
                    return false;
                }
                NavState navState = (NavState) obj;
                return this.isRightPanelOpened == navState.isRightPanelOpened && this.isOnHomeTab == navState.isOnHomeTab;
            }

            public int hashCode() {
                boolean z2 = this.isRightPanelOpened;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                boolean z3 = this.isOnHomeTab;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                return i4 + i;
            }

            public final boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            public final boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            public String toString() {
                StringBuilder R = a.R("NavState(isRightPanelOpened=");
                R.append(this.isRightPanelOpened);
                R.append(", isOnHomeTab=");
                return a.M(R, this.isOnHomeTab, ")");
            }
        }

        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState.Guild> mapChannelToGuildStoreState(final Channel channel, final NavState navState, StoreChannels storeChannels, StoreUser storeUser, StoreGuilds storeGuilds, StoreUserSettings storeUserSettings) {
            long f = channel.f();
            Observable<StoreState.Guild> g = Observable.g(StoreChannels.observeChannelsForGuild$default(storeChannels, f, null, 2, null), storeUser.observeAllUsers(), storeGuilds.observeComputed(f), storeGuilds.observeRoles(f), StoreUserSettings.observeIsAnimatedEmojisEnabled$default(storeUserSettings, false, 1, null), new Func5<Map<Long, ? extends Channel>, Map<Long, ? extends User>, Map<Long, ? extends GuildMember>, Map<Long, ? extends GuildRole>, Boolean, StoreState.Guild>() { // from class: com.discord.widgets.channels.WidgetChannelTopicViewModel$Companion$mapChannelToGuildStoreState$1
                @Override // rx.functions.Func5
                public /* bridge */ /* synthetic */ WidgetChannelTopicViewModel.StoreState.Guild call(Map<Long, ? extends Channel> map, Map<Long, ? extends User> map2, Map<Long, ? extends GuildMember> map3, Map<Long, ? extends GuildRole> map4, Boolean bool) {
                    return call2((Map<Long, Channel>) map, map2, (Map<Long, GuildMember>) map3, (Map<Long, GuildRole>) map4, bool);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetChannelTopicViewModel.StoreState.Guild call2(Map<Long, Channel> map, Map<Long, ? extends User> map2, Map<Long, GuildMember> map3, Map<Long, GuildRole> map4, Boolean bool) {
                    Channel channel2 = Channel.this;
                    m.checkNotNullExpressionValue(map, "channels");
                    m.checkNotNullExpressionValue(map2, "users");
                    m.checkNotNullExpressionValue(map3, "members");
                    m.checkNotNullExpressionValue(map4, "roles");
                    m.checkNotNullExpressionValue(bool, "allowAnimatedEmojis");
                    return new WidgetChannelTopicViewModel.StoreState.Guild.Topic(channel2, map, map2, map3, map4, bool.booleanValue(), navState.isRightPanelOpened(), navState.isOnHomeTab());
                }
            });
            m.checkNotNullExpressionValue(g, "Observable.combineLatest…HomeTab\n        )\n      }");
            return g;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> mapChannelToPrivateStoreState(final Channel channel, final NavState navState, StoreGuilds storeGuilds, final StoreUserSettings storeUserSettings) {
            if (ChannelUtils.p(channel)) {
                k kVar = new k(new StoreState.GDM(channel, storeUserSettings.getIsDeveloperMode(), navState.isRightPanelOpened(), navState.isOnHomeTab()));
                m.checkNotNullExpressionValue(kVar, "Observable.just(\n       …            )\n          )");
                return kVar;
            }
            Observable F = storeGuilds.observeComputed().F(new b<Map<Long, ? extends Map<Long, ? extends GuildMember>>, StoreState>() { // from class: com.discord.widgets.channels.WidgetChannelTopicViewModel$Companion$mapChannelToPrivateStoreState$1
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ WidgetChannelTopicViewModel.StoreState call(Map<Long, ? extends Map<Long, ? extends GuildMember>> map) {
                    return call2((Map<Long, ? extends Map<Long, GuildMember>>) map);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetChannelTopicViewModel.StoreState call2(Map<Long, ? extends Map<Long, GuildMember>> map) {
                    return new WidgetChannelTopicViewModel.StoreState.DM(Channel.this, map.values(), storeUserSettings.getIsDeveloperMode(), navState.isRightPanelOpened(), navState.isOnHomeTab());
                }
            });
            m.checkNotNullExpressionValue(F, "storeGuilds.observeCompu…            )\n          }");
            return F;
        }

        private final Observable<NavState> observeNavState(StoreNavigation storeNavigation, StoreTabsNavigation storeTabsNavigation) {
            Observable<NavState> j = Observable.j(storeNavigation.observeRightPanelState().F(WidgetChannelTopicViewModel$Companion$observeNavState$1.INSTANCE), storeTabsNavigation.observeSelectedTab().F(WidgetChannelTopicViewModel$Companion$observeNavState$2.INSTANCE), WidgetChannelTopicViewModel$Companion$observeNavState$3.INSTANCE);
            m.checkNotNullExpressionValue(j, "Observable.combineLatest…sOpen, isOnHomeTab)\n    }");
            return j;
        }

        public static /* synthetic */ Observable observeNavState$default(Companion companion, StoreNavigation storeNavigation, StoreTabsNavigation storeTabsNavigation, int i, Object obj) {
            if ((i & 1) != 0) {
                storeNavigation = StoreStream.Companion.getNavigation();
            }
            if ((i & 2) != 0) {
                storeTabsNavigation = StoreStream.Companion.getTabsNavigation();
            }
            return companion.observeNavState(storeNavigation, storeTabsNavigation);
        }

        private final Observable<StoreState> observeStoreState(final NavState navState, StoreChannelsSelected storeChannelsSelected, final StoreChannels storeChannels, final StoreUser storeUser, final StoreGuilds storeGuilds, final StoreUserSettings storeUserSettings) {
            Observable Y = storeChannelsSelected.observeSelectedChannel().Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.channels.WidgetChannelTopicViewModel$Companion$observeStoreState$1
                public final Observable<? extends WidgetChannelTopicViewModel.StoreState> call(Channel channel) {
                    Observable<? extends WidgetChannelTopicViewModel.StoreState> mapChannelToGuildStoreState;
                    Observable<? extends WidgetChannelTopicViewModel.StoreState> mapChannelToPrivateStoreState;
                    if (channel == null) {
                        return new k(new WidgetChannelTopicViewModel.StoreState.NoChannel(WidgetChannelTopicViewModel.Companion.NavState.this.isRightPanelOpened(), WidgetChannelTopicViewModel.Companion.NavState.this.isOnHomeTab()));
                    }
                    if (ChannelUtils.x(channel)) {
                        mapChannelToPrivateStoreState = WidgetChannelTopicViewModel.Companion.mapChannelToPrivateStoreState(channel, WidgetChannelTopicViewModel.Companion.NavState.this, storeGuilds, storeUserSettings);
                        return mapChannelToPrivateStoreState;
                    }
                    if (channel.z() != null) {
                        String z2 = channel.z();
                        if (!(z2 == null || z2.length() == 0)) {
                            mapChannelToGuildStoreState = WidgetChannelTopicViewModel.Companion.mapChannelToGuildStoreState(channel, WidgetChannelTopicViewModel.Companion.NavState.this, storeChannels, storeUser, storeGuilds, storeUserSettings);
                            return mapChannelToGuildStoreState;
                        }
                    }
                    return new k(new WidgetChannelTopicViewModel.StoreState.Guild.DefaultTopic(channel, WidgetChannelTopicViewModel.Companion.NavState.this.isRightPanelOpened(), WidgetChannelTopicViewModel.Companion.NavState.this.isOnHomeTab()));
                }
            });
            m.checkNotNullExpressionValue(Y, "storeChannelsSelected\n  …            }\n          }");
            return Y;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, NavState navState, StoreChannelsSelected storeChannelsSelected, StoreChannels storeChannels, StoreUser storeUser, StoreGuilds storeGuilds, StoreUserSettings storeUserSettings, int i, Object obj) {
            if ((i & 2) != 0) {
                storeChannelsSelected = StoreStream.Companion.getChannelsSelected();
            }
            StoreChannelsSelected storeChannelsSelected2 = storeChannelsSelected;
            if ((i & 4) != 0) {
                storeChannels = StoreStream.Companion.getChannels();
            }
            StoreChannels storeChannels2 = storeChannels;
            if ((i & 8) != 0) {
                storeUser = StoreStream.Companion.getUsers();
            }
            StoreUser storeUser2 = storeUser;
            if ((i & 16) != 0) {
                storeGuilds = StoreStream.Companion.getGuilds();
            }
            StoreGuilds storeGuilds2 = storeGuilds;
            if ((i & 32) != 0) {
                storeUserSettings = StoreStream.Companion.getUserSettings();
            }
            return companion.observeStoreState(navState, storeChannelsSelected2, storeChannels2, storeUser2, storeGuilds2, storeUserSettings);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelTopicViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;", "", HookHelper.constructorName, "()V", "FocusFirstElement", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event$FocusFirstElement;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event$FocusFirstElement;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FocusFirstElement extends Event {
            public static final FocusFirstElement INSTANCE = new FocusFirstElement();

            private FocusFirstElement() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelTopicViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\t\n\u000b\fB\u0019\b\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0003\u0010\u0005R\u001c\u0010\u0006\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0004\u001a\u0004\b\u0006\u0010\u0005\u0082\u0001\u0004\r\u000e\u000f\u0010¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "", "", "isOnHomeTab", "Z", "()Z", "isRightPanelOpened", HookHelper.constructorName, "(ZZ)V", "DM", "GDM", "Guild", "NoChannel", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$NoChannel;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {
        private final boolean isOnHomeTab;
        private final boolean isRightPanelOpened;

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0018\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00060\u0005\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u000b\u0012\u0006\u0010\u0013\u001a\u00020\u000b\u0012\u0006\u0010\u0014\u001a\u00020\u000b¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\"\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00060\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJT\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0010\u001a\u00020\u00022\u001a\b\u0002\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00060\u00052\b\b\u0002\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\u0013\u001a\u00020\u000b2\b\b\u0002\u0010\u0014\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001f\u001a\u00020\u000b2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÖ\u0003¢\u0006\u0004\b\u001f\u0010 R+\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\nR\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\rR\u001c\u0010\u0014\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b\u0014\u0010\rR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010%\u001a\u0004\b&\u0010\u0004R\u001c\u0010\u0013\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b\u0013\u0010\r¨\u0006)"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "", "", "Lcom/discord/models/member/GuildMember;", "component2", "()Ljava/util/Collection;", "", "component3", "()Z", "component4", "component5", "channel", "guildMembers", "developerModeEnabled", "isRightPanelOpened", "isOnHomeTab", "copy", "(Lcom/discord/api/channel/Channel;Ljava/util/Collection;ZZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Collection;", "getGuildMembers", "Z", "getDeveloperModeEnabled", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/util/Collection;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DM extends StoreState {
            private final Channel channel;
            private final boolean developerModeEnabled;
            private final Collection<Map<Long, GuildMember>> guildMembers;
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            public /* synthetic */ DM(Channel channel, Collection collection, boolean z2, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(channel, collection, (i & 4) != 0 ? false : z2, z3, z4);
            }

            public static /* synthetic */ DM copy$default(DM dm, Channel channel, Collection collection, boolean z2, boolean z3, boolean z4, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = dm.channel;
                }
                Collection<Map<Long, GuildMember>> collection2 = collection;
                if ((i & 2) != 0) {
                    collection2 = dm.guildMembers;
                }
                Collection collection3 = collection2;
                if ((i & 4) != 0) {
                    z2 = dm.developerModeEnabled;
                }
                boolean z5 = z2;
                if ((i & 8) != 0) {
                    z3 = dm.isRightPanelOpened();
                }
                boolean z6 = z3;
                if ((i & 16) != 0) {
                    z4 = dm.isOnHomeTab();
                }
                return dm.copy(channel, collection3, z5, z6, z4);
            }

            public final Channel component1() {
                return this.channel;
            }

            public final Collection<Map<Long, GuildMember>> component2() {
                return this.guildMembers;
            }

            public final boolean component3() {
                return this.developerModeEnabled;
            }

            public final boolean component4() {
                return isRightPanelOpened();
            }

            public final boolean component5() {
                return isOnHomeTab();
            }

            public final DM copy(Channel channel, Collection<? extends Map<Long, GuildMember>> collection, boolean z2, boolean z3, boolean z4) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(collection, "guildMembers");
                return new DM(channel, collection, z2, z3, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof DM)) {
                    return false;
                }
                DM dm = (DM) obj;
                return m.areEqual(this.channel, dm.channel) && m.areEqual(this.guildMembers, dm.guildMembers) && this.developerModeEnabled == dm.developerModeEnabled && isRightPanelOpened() == dm.isRightPanelOpened() && isOnHomeTab() == dm.isOnHomeTab();
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final boolean getDeveloperModeEnabled() {
                return this.developerModeEnabled;
            }

            public final Collection<Map<Long, GuildMember>> getGuildMembers() {
                return this.guildMembers;
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v1 */
            /* JADX WARN: Type inference failed for: r2v2 */
            /* JADX WARN: Type inference failed for: r2v3 */
            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Collection<Map<Long, GuildMember>> collection = this.guildMembers;
                if (collection != null) {
                    i = collection.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.developerModeEnabled;
                ?? r2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (i2 + i3) * 31;
                boolean isRightPanelOpened = isRightPanelOpened();
                if (isRightPanelOpened) {
                    isRightPanelOpened = true;
                }
                int i6 = isRightPanelOpened ? 1 : 0;
                int i7 = isRightPanelOpened ? 1 : 0;
                int i8 = (i5 + i6) * 31;
                boolean isOnHomeTab = isOnHomeTab();
                if (!isOnHomeTab) {
                    r2 = isOnHomeTab;
                }
                return i8 + (r2 == true ? 1 : 0);
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            public String toString() {
                StringBuilder R = a.R("DM(channel=");
                R.append(this.channel);
                R.append(", guildMembers=");
                R.append(this.guildMembers);
                R.append(", developerModeEnabled=");
                R.append(this.developerModeEnabled);
                R.append(", isRightPanelOpened=");
                R.append(isRightPanelOpened());
                R.append(", isOnHomeTab=");
                R.append(isOnHomeTab());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public DM(Channel channel, Collection<? extends Map<Long, GuildMember>> collection, boolean z2, boolean z3, boolean z4) {
                super(z3, z4, null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(collection, "guildMembers");
                this.channel = channel;
                this.guildMembers = collection;
                this.developerModeEnabled = z2;
                this.isRightPanelOpened = z3;
                this.isOnHomeTab = z4;
            }
        }

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\t\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J8\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00052\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001c\u0010\r\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\r\u0010\u0007R\u001c\u0010\f\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\f\u0010\u0007R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001d\u0010\u0007¨\u0006 "}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "component3", "component4", "channel", "developerModeEnabled", "isRightPanelOpened", "isOnHomeTab", "copy", "(Lcom/discord/api/channel/Channel;ZZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", "getDeveloperModeEnabled", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class GDM extends StoreState {
            private final Channel channel;
            private final boolean developerModeEnabled;
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            public /* synthetic */ GDM(Channel channel, boolean z2, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(channel, (i & 2) != 0 ? false : z2, z3, z4);
            }

            public static /* synthetic */ GDM copy$default(GDM gdm, Channel channel, boolean z2, boolean z3, boolean z4, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = gdm.channel;
                }
                if ((i & 2) != 0) {
                    z2 = gdm.developerModeEnabled;
                }
                if ((i & 4) != 0) {
                    z3 = gdm.isRightPanelOpened();
                }
                if ((i & 8) != 0) {
                    z4 = gdm.isOnHomeTab();
                }
                return gdm.copy(channel, z2, z3, z4);
            }

            public final Channel component1() {
                return this.channel;
            }

            public final boolean component2() {
                return this.developerModeEnabled;
            }

            public final boolean component3() {
                return isRightPanelOpened();
            }

            public final boolean component4() {
                return isOnHomeTab();
            }

            public final GDM copy(Channel channel, boolean z2, boolean z3, boolean z4) {
                m.checkNotNullParameter(channel, "channel");
                return new GDM(channel, z2, z3, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof GDM)) {
                    return false;
                }
                GDM gdm = (GDM) obj;
                return m.areEqual(this.channel, gdm.channel) && this.developerModeEnabled == gdm.developerModeEnabled && isRightPanelOpened() == gdm.isRightPanelOpened() && isOnHomeTab() == gdm.isOnHomeTab();
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final boolean getDeveloperModeEnabled() {
                return this.developerModeEnabled;
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v0 */
            /* JADX WARN: Type inference failed for: r2v1 */
            /* JADX WARN: Type inference failed for: r2v2 */
            public int hashCode() {
                Channel channel = this.channel;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                boolean z2 = this.developerModeEnabled;
                ?? r2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                int i3 = (hashCode + i) * 31;
                boolean isRightPanelOpened = isRightPanelOpened();
                if (isRightPanelOpened) {
                    isRightPanelOpened = true;
                }
                int i4 = isRightPanelOpened ? 1 : 0;
                int i5 = isRightPanelOpened ? 1 : 0;
                int i6 = (i3 + i4) * 31;
                boolean isOnHomeTab = isOnHomeTab();
                if (!isOnHomeTab) {
                    r2 = isOnHomeTab;
                }
                return i6 + (r2 == true ? 1 : 0);
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            public String toString() {
                StringBuilder R = a.R("GDM(channel=");
                R.append(this.channel);
                R.append(", developerModeEnabled=");
                R.append(this.developerModeEnabled);
                R.append(", isRightPanelOpened=");
                R.append(isRightPanelOpened());
                R.append(", isOnHomeTab=");
                R.append(isOnHomeTab());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public GDM(Channel channel, boolean z2, boolean z3, boolean z4) {
                super(z3, z4, null);
                m.checkNotNullParameter(channel, "channel");
                this.channel = channel;
                this.developerModeEnabled = z2;
                this.isRightPanelOpened = z3;
                this.isOnHomeTab = z4;
            }
        }

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\t\nB\u0019\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0003\u0010\u0005R\u001c\u0010\u0006\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0004\u001a\u0004\b\u0006\u0010\u0005\u0082\u0001\u0002\u000b\f¨\u0006\r"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "", "isRightPanelOpened", "Z", "()Z", "isOnHomeTab", HookHelper.constructorName, "(ZZ)V", "DefaultTopic", "Topic", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static abstract class Guild extends StoreState {
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            /* compiled from: WidgetChannelTopicViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00052\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001c\u0010\u000b\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u000b\u0010\u0007R\u001c\u0010\n\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\n\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "component3", "channel", "isRightPanelOpened", "isOnHomeTab", "copy", "(Lcom/discord/api/channel/Channel;ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class DefaultTopic extends Guild {
                private final Channel channel;
                private final boolean isOnHomeTab;
                private final boolean isRightPanelOpened;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public DefaultTopic(Channel channel, boolean z2, boolean z3) {
                    super(z2, z3, null);
                    m.checkNotNullParameter(channel, "channel");
                    this.channel = channel;
                    this.isRightPanelOpened = z2;
                    this.isOnHomeTab = z3;
                }

                public static /* synthetic */ DefaultTopic copy$default(DefaultTopic defaultTopic, Channel channel, boolean z2, boolean z3, int i, Object obj) {
                    if ((i & 1) != 0) {
                        channel = defaultTopic.channel;
                    }
                    if ((i & 2) != 0) {
                        z2 = defaultTopic.isRightPanelOpened();
                    }
                    if ((i & 4) != 0) {
                        z3 = defaultTopic.isOnHomeTab();
                    }
                    return defaultTopic.copy(channel, z2, z3);
                }

                public final Channel component1() {
                    return this.channel;
                }

                public final boolean component2() {
                    return isRightPanelOpened();
                }

                public final boolean component3() {
                    return isOnHomeTab();
                }

                public final DefaultTopic copy(Channel channel, boolean z2, boolean z3) {
                    m.checkNotNullParameter(channel, "channel");
                    return new DefaultTopic(channel, z2, z3);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof DefaultTopic)) {
                        return false;
                    }
                    DefaultTopic defaultTopic = (DefaultTopic) obj;
                    return m.areEqual(this.channel, defaultTopic.channel) && isRightPanelOpened() == defaultTopic.isRightPanelOpened() && isOnHomeTab() == defaultTopic.isOnHomeTab();
                }

                public final Channel getChannel() {
                    return this.channel;
                }

                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r2v0 */
                /* JADX WARN: Type inference failed for: r2v1 */
                /* JADX WARN: Type inference failed for: r2v2 */
                public int hashCode() {
                    Channel channel = this.channel;
                    int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                    boolean isRightPanelOpened = isRightPanelOpened();
                    ?? r2 = 1;
                    if (isRightPanelOpened) {
                        isRightPanelOpened = true;
                    }
                    int i = isRightPanelOpened ? 1 : 0;
                    int i2 = isRightPanelOpened ? 1 : 0;
                    int i3 = (hashCode + i) * 31;
                    boolean isOnHomeTab = isOnHomeTab();
                    if (!isOnHomeTab) {
                        r2 = isOnHomeTab;
                    }
                    return i3 + (r2 == true ? 1 : 0);
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
                public boolean isOnHomeTab() {
                    return this.isOnHomeTab;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
                public boolean isRightPanelOpened() {
                    return this.isRightPanelOpened;
                }

                public String toString() {
                    StringBuilder R = a.R("DefaultTopic(channel=");
                    R.append(this.channel);
                    R.append(", isRightPanelOpened=");
                    R.append(isRightPanelOpened());
                    R.append(", isOnHomeTab=");
                    R.append(isOnHomeTab());
                    R.append(")");
                    return R.toString();
                }
            }

            /* compiled from: WidgetChannelTopicViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B\u007f\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u0005\u0012\u0016\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u0005\u0012\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u0005\u0012\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f0\u0005\u0012\u0006\u0010\u001b\u001a\u00020\u0011\u0012\u0006\u0010\u001c\u001a\u00020\u0011\u0012\u0006\u0010\u001d\u001a\u00020\u0011¢\u0006\u0004\b3\u00104J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ \u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\tJ\u001c\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\tJ\u001c\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f0\u0005HÆ\u0003¢\u0006\u0004\b\u0010\u0010\tJ\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0013J\u0098\u0001\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0016\u001a\u00020\u00022\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u00052\u0018\b\u0002\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u00052\u0014\b\u0002\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u00052\u0014\b\u0002\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f0\u00052\b\b\u0002\u0010\u001b\u001a\u00020\u00112\b\b\u0002\u0010\u001c\u001a\u00020\u00112\b\b\u0002\u0010\u001d\u001a\u00020\u0011HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010(\u001a\u00020\u00112\b\u0010'\u001a\u0004\u0018\u00010&HÖ\u0003¢\u0006\u0004\b(\u0010)R\u001c\u0010\u001d\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010*\u001a\u0004\b\u001d\u0010\u0013R)\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010+\u001a\u0004\b,\u0010\tR\u001c\u0010\u001c\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010*\u001a\u0004\b\u001c\u0010\u0013R\u0019\u0010\u001b\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010*\u001a\u0004\b-\u0010\u0013R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010.\u001a\u0004\b/\u0010\u0004R)\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010+\u001a\u0004\b0\u0010\tR%\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010+\u001a\u0004\b1\u0010\tR%\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010+\u001a\u0004\b2\u0010\t¨\u00065"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/ChannelId;", "component2", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component3", "Lcom/discord/models/member/GuildMember;", "component4", "Lcom/discord/api/role/GuildRole;", "component5", "", "component6", "()Z", "component7", "component8", "channel", "channels", "users", "members", "roles", "allowAnimatedEmojis", "isRightPanelOpened", "isOnHomeTab", "copy", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/Map;", "getUsers", "getAllowAnimatedEmojis", "Lcom/discord/api/channel/Channel;", "getChannel", "getChannels", "getMembers", "getRoles", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Topic extends Guild {
                private final boolean allowAnimatedEmojis;
                private final Channel channel;
                private final Map<Long, Channel> channels;
                private final boolean isOnHomeTab;
                private final boolean isRightPanelOpened;
                private final Map<Long, GuildMember> members;
                private final Map<Long, GuildRole> roles;
                private final Map<Long, User> users;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                /* JADX WARN: Multi-variable type inference failed */
                public Topic(Channel channel, Map<Long, Channel> map, Map<Long, ? extends User> map2, Map<Long, GuildMember> map3, Map<Long, GuildRole> map4, boolean z2, boolean z3, boolean z4) {
                    super(z3, z4, null);
                    m.checkNotNullParameter(channel, "channel");
                    m.checkNotNullParameter(map, "channels");
                    m.checkNotNullParameter(map2, "users");
                    m.checkNotNullParameter(map3, "members");
                    m.checkNotNullParameter(map4, "roles");
                    this.channel = channel;
                    this.channels = map;
                    this.users = map2;
                    this.members = map3;
                    this.roles = map4;
                    this.allowAnimatedEmojis = z2;
                    this.isRightPanelOpened = z3;
                    this.isOnHomeTab = z4;
                }

                public final Channel component1() {
                    return this.channel;
                }

                public final Map<Long, Channel> component2() {
                    return this.channels;
                }

                public final Map<Long, User> component3() {
                    return this.users;
                }

                public final Map<Long, GuildMember> component4() {
                    return this.members;
                }

                public final Map<Long, GuildRole> component5() {
                    return this.roles;
                }

                public final boolean component6() {
                    return this.allowAnimatedEmojis;
                }

                public final boolean component7() {
                    return isRightPanelOpened();
                }

                public final boolean component8() {
                    return isOnHomeTab();
                }

                public final Topic copy(Channel channel, Map<Long, Channel> map, Map<Long, ? extends User> map2, Map<Long, GuildMember> map3, Map<Long, GuildRole> map4, boolean z2, boolean z3, boolean z4) {
                    m.checkNotNullParameter(channel, "channel");
                    m.checkNotNullParameter(map, "channels");
                    m.checkNotNullParameter(map2, "users");
                    m.checkNotNullParameter(map3, "members");
                    m.checkNotNullParameter(map4, "roles");
                    return new Topic(channel, map, map2, map3, map4, z2, z3, z4);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof Topic)) {
                        return false;
                    }
                    Topic topic = (Topic) obj;
                    return m.areEqual(this.channel, topic.channel) && m.areEqual(this.channels, topic.channels) && m.areEqual(this.users, topic.users) && m.areEqual(this.members, topic.members) && m.areEqual(this.roles, topic.roles) && this.allowAnimatedEmojis == topic.allowAnimatedEmojis && isRightPanelOpened() == topic.isRightPanelOpened() && isOnHomeTab() == topic.isOnHomeTab();
                }

                public final boolean getAllowAnimatedEmojis() {
                    return this.allowAnimatedEmojis;
                }

                public final Channel getChannel() {
                    return this.channel;
                }

                public final Map<Long, Channel> getChannels() {
                    return this.channels;
                }

                public final Map<Long, GuildMember> getMembers() {
                    return this.members;
                }

                public final Map<Long, GuildRole> getRoles() {
                    return this.roles;
                }

                public final Map<Long, User> getUsers() {
                    return this.users;
                }

                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r2v7 */
                /* JADX WARN: Type inference failed for: r2v8 */
                /* JADX WARN: Type inference failed for: r2v9 */
                public int hashCode() {
                    Channel channel = this.channel;
                    int i = 0;
                    int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                    Map<Long, Channel> map = this.channels;
                    int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
                    Map<Long, User> map2 = this.users;
                    int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
                    Map<Long, GuildMember> map3 = this.members;
                    int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
                    Map<Long, GuildRole> map4 = this.roles;
                    if (map4 != null) {
                        i = map4.hashCode();
                    }
                    int i2 = (hashCode4 + i) * 31;
                    boolean z2 = this.allowAnimatedEmojis;
                    ?? r2 = 1;
                    if (z2) {
                        z2 = true;
                    }
                    int i3 = z2 ? 1 : 0;
                    int i4 = z2 ? 1 : 0;
                    int i5 = (i2 + i3) * 31;
                    boolean isRightPanelOpened = isRightPanelOpened();
                    if (isRightPanelOpened) {
                        isRightPanelOpened = true;
                    }
                    int i6 = isRightPanelOpened ? 1 : 0;
                    int i7 = isRightPanelOpened ? 1 : 0;
                    int i8 = (i5 + i6) * 31;
                    boolean isOnHomeTab = isOnHomeTab();
                    if (!isOnHomeTab) {
                        r2 = isOnHomeTab;
                    }
                    return i8 + (r2 == true ? 1 : 0);
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
                public boolean isOnHomeTab() {
                    return this.isOnHomeTab;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
                public boolean isRightPanelOpened() {
                    return this.isRightPanelOpened;
                }

                public String toString() {
                    StringBuilder R = a.R("Topic(channel=");
                    R.append(this.channel);
                    R.append(", channels=");
                    R.append(this.channels);
                    R.append(", users=");
                    R.append(this.users);
                    R.append(", members=");
                    R.append(this.members);
                    R.append(", roles=");
                    R.append(this.roles);
                    R.append(", allowAnimatedEmojis=");
                    R.append(this.allowAnimatedEmojis);
                    R.append(", isRightPanelOpened=");
                    R.append(isRightPanelOpened());
                    R.append(", isOnHomeTab=");
                    R.append(isOnHomeTab());
                    R.append(")");
                    return R.toString();
                }
            }

            public /* synthetic */ Guild(boolean z2, boolean z3, DefaultConstructorMarker defaultConstructorMarker) {
                this(z2, z3);
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            private Guild(boolean z2, boolean z3) {
                super(z2, z3, null);
                this.isRightPanelOpened = z2;
                this.isOnHomeTab = z3;
            }
        }

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0006\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0006\u0010\u0004R\u001c\u0010\u0007\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0007\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$NoChannel;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;", "", "component1", "()Z", "component2", "isRightPanelOpened", "isOnHomeTab", "copy", "(ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$NoChannel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NoChannel extends StoreState {
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            public NoChannel(boolean z2, boolean z3) {
                super(z2, z3, null);
                this.isRightPanelOpened = z2;
                this.isOnHomeTab = z3;
            }

            public static /* synthetic */ NoChannel copy$default(NoChannel noChannel, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = noChannel.isRightPanelOpened();
                }
                if ((i & 2) != 0) {
                    z3 = noChannel.isOnHomeTab();
                }
                return noChannel.copy(z2, z3);
            }

            public final boolean component1() {
                return isRightPanelOpened();
            }

            public final boolean component2() {
                return isOnHomeTab();
            }

            public final NoChannel copy(boolean z2, boolean z3) {
                return new NoChannel(z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof NoChannel)) {
                    return false;
                }
                NoChannel noChannel = (NoChannel) obj;
                return isRightPanelOpened() == noChannel.isRightPanelOpened() && isOnHomeTab() == noChannel.isOnHomeTab();
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r1v0 */
            /* JADX WARN: Type inference failed for: r1v1 */
            /* JADX WARN: Type inference failed for: r1v2 */
            public int hashCode() {
                boolean isRightPanelOpened = isRightPanelOpened();
                ?? r1 = 1;
                if (isRightPanelOpened) {
                    isRightPanelOpened = true;
                }
                int i = isRightPanelOpened ? 1 : 0;
                int i2 = isRightPanelOpened ? 1 : 0;
                int i3 = i * 31;
                boolean isOnHomeTab = isOnHomeTab();
                if (!isOnHomeTab) {
                    r1 = isOnHomeTab;
                }
                return i3 + (r1 == true ? 1 : 0);
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.StoreState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            public String toString() {
                StringBuilder R = a.R("NoChannel(isRightPanelOpened=");
                R.append(isRightPanelOpened());
                R.append(", isOnHomeTab=");
                R.append(isOnHomeTab());
                R.append(")");
                return R.toString();
            }
        }

        private StoreState(boolean z2, boolean z3) {
            this.isRightPanelOpened = z2;
            this.isOnHomeTab = z3;
        }

        public boolean isOnHomeTab() {
            return this.isOnHomeTab;
        }

        public boolean isRightPanelOpened() {
            return this.isRightPanelOpened;
        }

        public /* synthetic */ StoreState(boolean z2, boolean z3, DefaultConstructorMarker defaultConstructorMarker) {
            this(z2, z3);
        }
    }

    /* compiled from: WidgetChannelTopicViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\t\n\u000b\fB\u0019\b\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0003\u0010\u0005R\u001c\u0010\u0006\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0004\u001a\u0004\b\u0006\u0010\u0005\u0082\u0001\u0004\r\u000e\u000f\u0010¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;", "", "", "isOnHomeTab", "Z", "()Z", "isRightPanelOpened", HookHelper.constructorName, "(ZZ)V", "DM", "GDM", "Guild", "NoChannel", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {
        private final boolean isOnHomeTab;
        private final boolean isRightPanelOpened;

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001Bc\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\u0019\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00020\t\u0012\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\r0\f\u0012\n\u0010\u001c\u001a\u00060\u0005j\u0002`\u0010\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u0013\u0012\u0006\u0010\u001e\u001a\u00020\u0013\u0012\u0006\u0010\u001f\u001a\u00020\u0013¢\u0006\u0004\b6\u00107J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0014\u0010\u0011\u001a\u00060\u0005j\u0002`\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0015Jz\u0010 \u001a\u00020\u00002\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u0019\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00020\t2\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\f\b\u0002\u0010\u001c\u001a\u00060\u0005j\u0002`\u00102\b\b\u0002\u0010\u001d\u001a\u00020\u00132\b\b\u0002\u0010\u001e\u001a\u00020\u00132\b\b\u0002\u0010\u001f\u001a\u00020\u0013HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010\"\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\"\u0010\u0004J\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010(\u001a\u00020\u00132\b\u0010'\u001a\u0004\u0018\u00010&HÖ\u0003¢\u0006\u0004\b(\u0010)R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b+\u0010\u0004R\u001f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b-\u0010\u000bR\u001c\u0010\u001e\u001a\u00020\u00138\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001e\u0010.\u001a\u0004\b\u001e\u0010\u0015R\u001c\u0010\u001f\u001a\u00020\u00138\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001f\u0010.\u001a\u0004\b\u001f\u0010\u0015R\u001d\u0010\u001c\u001a\u00060\u0005j\u0002`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010/\u001a\u0004\b0\u0010\u0012R!\u0010\u0019\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00101\u001a\u0004\b2\u0010\bR\u0019\u0010\u001d\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010.\u001a\u0004\b3\u0010\u0015R\u001f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00104\u001a\u0004\b5\u0010\u000f¨\u00068"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/UserId;", "component2", "()Ljava/lang/Long;", "", "component3", "()Ljava/util/Set;", "", "Lcom/discord/models/member/GuildMember;", "component4", "()Ljava/util/List;", "Lcom/discord/primitives/ChannelId;", "component5", "()J", "", "component6", "()Z", "component7", "component8", "recipientName", "recipientUserId", "recipientNicknames", "guildMembers", "channelId", "developerModeEnabled", "isRightPanelOpened", "isOnHomeTab", "copy", "(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Set;Ljava/util/List;JZZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getRecipientName", "Ljava/util/Set;", "getRecipientNicknames", "Z", "J", "getChannelId", "Ljava/lang/Long;", "getRecipientUserId", "getDeveloperModeEnabled", "Ljava/util/List;", "getGuildMembers", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Set;Ljava/util/List;JZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DM extends ViewState {
            private final long channelId;
            private final boolean developerModeEnabled;
            private final List<GuildMember> guildMembers;
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;
            private final String recipientName;
            private final Set<String> recipientNicknames;
            private final Long recipientUserId;

            public /* synthetic */ DM(String str, Long l, Set set, List list, long j, boolean z2, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(str, l, set, list, j, (i & 32) != 0 ? false : z2, z3, z4);
            }

            public final String component1() {
                return this.recipientName;
            }

            public final Long component2() {
                return this.recipientUserId;
            }

            public final Set<String> component3() {
                return this.recipientNicknames;
            }

            public final List<GuildMember> component4() {
                return this.guildMembers;
            }

            public final long component5() {
                return this.channelId;
            }

            public final boolean component6() {
                return this.developerModeEnabled;
            }

            public final boolean component7() {
                return isRightPanelOpened();
            }

            public final boolean component8() {
                return isOnHomeTab();
            }

            public final DM copy(String str, Long l, Set<String> set, List<GuildMember> list, long j, boolean z2, boolean z3, boolean z4) {
                m.checkNotNullParameter(set, "recipientNicknames");
                m.checkNotNullParameter(list, "guildMembers");
                return new DM(str, l, set, list, j, z2, z3, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof DM)) {
                    return false;
                }
                DM dm = (DM) obj;
                return m.areEqual(this.recipientName, dm.recipientName) && m.areEqual(this.recipientUserId, dm.recipientUserId) && m.areEqual(this.recipientNicknames, dm.recipientNicknames) && m.areEqual(this.guildMembers, dm.guildMembers) && this.channelId == dm.channelId && this.developerModeEnabled == dm.developerModeEnabled && isRightPanelOpened() == dm.isRightPanelOpened() && isOnHomeTab() == dm.isOnHomeTab();
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final boolean getDeveloperModeEnabled() {
                return this.developerModeEnabled;
            }

            public final List<GuildMember> getGuildMembers() {
                return this.guildMembers;
            }

            public final String getRecipientName() {
                return this.recipientName;
            }

            public final Set<String> getRecipientNicknames() {
                return this.recipientNicknames;
            }

            public final Long getRecipientUserId() {
                return this.recipientUserId;
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v5 */
            /* JADX WARN: Type inference failed for: r2v6 */
            /* JADX WARN: Type inference failed for: r2v7 */
            public int hashCode() {
                String str = this.recipientName;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                Long l = this.recipientUserId;
                int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
                Set<String> set = this.recipientNicknames;
                int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
                List<GuildMember> list = this.guildMembers;
                if (list != null) {
                    i = list.hashCode();
                }
                int a = (a0.a.a.b.a(this.channelId) + ((hashCode3 + i) * 31)) * 31;
                boolean z2 = this.developerModeEnabled;
                ?? r2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (a + i2) * 31;
                boolean isRightPanelOpened = isRightPanelOpened();
                if (isRightPanelOpened) {
                    isRightPanelOpened = true;
                }
                int i5 = isRightPanelOpened ? 1 : 0;
                int i6 = isRightPanelOpened ? 1 : 0;
                int i7 = (i4 + i5) * 31;
                boolean isOnHomeTab = isOnHomeTab();
                if (!isOnHomeTab) {
                    r2 = isOnHomeTab;
                }
                return i7 + (r2 == true ? 1 : 0);
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            public String toString() {
                StringBuilder R = a.R("DM(recipientName=");
                R.append(this.recipientName);
                R.append(", recipientUserId=");
                R.append(this.recipientUserId);
                R.append(", recipientNicknames=");
                R.append(this.recipientNicknames);
                R.append(", guildMembers=");
                R.append(this.guildMembers);
                R.append(", channelId=");
                R.append(this.channelId);
                R.append(", developerModeEnabled=");
                R.append(this.developerModeEnabled);
                R.append(", isRightPanelOpened=");
                R.append(isRightPanelOpened());
                R.append(", isOnHomeTab=");
                R.append(isOnHomeTab());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public DM(String str, Long l, Set<String> set, List<GuildMember> list, long j, boolean z2, boolean z3, boolean z4) {
                super(z3, z4, null);
                m.checkNotNullParameter(set, "recipientNicknames");
                m.checkNotNullParameter(list, "guildMembers");
                this.recipientName = str;
                this.recipientUserId = l;
                this.recipientNicknames = set;
                this.guildMembers = list;
                this.channelId = j;
                this.developerModeEnabled = z2;
                this.isRightPanelOpened = z3;
                this.isOnHomeTab = z4;
            }
        }

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\n\u0010\u000f\u001a\u00060\u0005j\u0002`\u0006\u0012\b\b\u0002\u0010\u0010\u001a\u00020\t\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u0006\u0010\u0012\u001a\u00020\t¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\r\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJF\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\f\b\u0002\u0010\u000f\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u0010\u001a\u00020\t2\b\b\u0002\u0010\u0011\u001a\u00020\t2\b\b\u0002\u0010\u0012\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001d\u001a\u00020\t2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u001d\u0010\u000f\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\bR\u001c\u0010\u0011\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\u0011\u0010\u000bR\u001c\u0010\u0012\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b\u0012\u0010\u000bR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\u000bR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010#\u001a\u0004\b$\u0010\u0004¨\u0006'"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/primitives/ChannelId;", "component2", "()J", "", "component3", "()Z", "component4", "component5", "channel", "channelId", "developerModeEnabled", "isRightPanelOpened", "isOnHomeTab", "copy", "(Lcom/discord/api/channel/Channel;JZZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Z", "getDeveloperModeEnabled", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;JZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class GDM extends ViewState {
            private final Channel channel;
            private final long channelId;
            private final boolean developerModeEnabled;
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            public /* synthetic */ GDM(Channel channel, long j, boolean z2, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(channel, j, (i & 4) != 0 ? false : z2, z3, z4);
            }

            public static /* synthetic */ GDM copy$default(GDM gdm, Channel channel, long j, boolean z2, boolean z3, boolean z4, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = gdm.channel;
                }
                if ((i & 2) != 0) {
                    j = gdm.channelId;
                }
                long j2 = j;
                if ((i & 4) != 0) {
                    z2 = gdm.developerModeEnabled;
                }
                boolean z5 = z2;
                if ((i & 8) != 0) {
                    z3 = gdm.isRightPanelOpened();
                }
                boolean z6 = z3;
                if ((i & 16) != 0) {
                    z4 = gdm.isOnHomeTab();
                }
                return gdm.copy(channel, j2, z5, z6, z4);
            }

            public final Channel component1() {
                return this.channel;
            }

            public final long component2() {
                return this.channelId;
            }

            public final boolean component3() {
                return this.developerModeEnabled;
            }

            public final boolean component4() {
                return isRightPanelOpened();
            }

            public final boolean component5() {
                return isOnHomeTab();
            }

            public final GDM copy(Channel channel, long j, boolean z2, boolean z3, boolean z4) {
                m.checkNotNullParameter(channel, "channel");
                return new GDM(channel, j, z2, z3, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof GDM)) {
                    return false;
                }
                GDM gdm = (GDM) obj;
                return m.areEqual(this.channel, gdm.channel) && this.channelId == gdm.channelId && this.developerModeEnabled == gdm.developerModeEnabled && isRightPanelOpened() == gdm.isRightPanelOpened() && isOnHomeTab() == gdm.isOnHomeTab();
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final boolean getDeveloperModeEnabled() {
                return this.developerModeEnabled;
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v0 */
            /* JADX WARN: Type inference failed for: r2v1 */
            /* JADX WARN: Type inference failed for: r2v2 */
            public int hashCode() {
                Channel channel = this.channel;
                int a = (a0.a.a.b.a(this.channelId) + ((channel != null ? channel.hashCode() : 0) * 31)) * 31;
                boolean z2 = this.developerModeEnabled;
                ?? r2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                int i3 = (a + i) * 31;
                boolean isRightPanelOpened = isRightPanelOpened();
                if (isRightPanelOpened) {
                    isRightPanelOpened = true;
                }
                int i4 = isRightPanelOpened ? 1 : 0;
                int i5 = isRightPanelOpened ? 1 : 0;
                int i6 = (i3 + i4) * 31;
                boolean isOnHomeTab = isOnHomeTab();
                if (!isOnHomeTab) {
                    r2 = isOnHomeTab;
                }
                return i6 + (r2 == true ? 1 : 0);
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            public String toString() {
                StringBuilder R = a.R("GDM(channel=");
                R.append(this.channel);
                R.append(", channelId=");
                R.append(this.channelId);
                R.append(", developerModeEnabled=");
                R.append(this.developerModeEnabled);
                R.append(", isRightPanelOpened=");
                R.append(isRightPanelOpened());
                R.append(", isOnHomeTab=");
                R.append(isOnHomeTab());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public GDM(Channel channel, long j, boolean z2, boolean z3, boolean z4) {
                super(z3, z4, null);
                m.checkNotNullParameter(channel, "channel");
                this.channel = channel;
                this.channelId = j;
                this.developerModeEnabled = z2;
                this.isRightPanelOpened = z3;
                this.isOnHomeTab = z4;
            }
        }

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u000e\u000fB!\b\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0003\u0010\u0005R\u001c\u0010\u0007\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\nR\u001c\u0010\u000b\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000b\u0010\u0004\u001a\u0004\b\u000b\u0010\u0005\u0082\u0001\u0002\u0010\u0011¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;", "", "isOnHomeTab", "Z", "()Z", "Lcom/discord/utilities/channel/GuildChannelIconType;", "channelIconType", "Lcom/discord/utilities/channel/GuildChannelIconType;", "getChannelIconType", "()Lcom/discord/utilities/channel/GuildChannelIconType;", "isRightPanelOpened", HookHelper.constructorName, "(Lcom/discord/utilities/channel/GuildChannelIconType;ZZ)V", "DefaultTopic", "Topic", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static abstract class Guild extends ViewState {
            private final GuildChannelIconType channelIconType;
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            /* compiled from: WidgetChannelTopicViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u001c\u0010\u000f\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u000f\u0010\nR\u001c\u0010\u000e\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u000e\u0010\nR\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R\u001c\u0010\f\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\f\u0010\u001f\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;", "Lcom/discord/utilities/channel/GuildChannelIconType;", "component1", "()Lcom/discord/utilities/channel/GuildChannelIconType;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "component3", "()Z", "component4", "channelIconType", "channel", "isRightPanelOpened", "isOnHomeTab", "copy", "(Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/api/channel/Channel;ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/utilities/channel/GuildChannelIconType;", "getChannelIconType", HookHelper.constructorName, "(Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/api/channel/Channel;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class DefaultTopic extends Guild {
                private final Channel channel;
                private final GuildChannelIconType channelIconType;
                private final boolean isOnHomeTab;
                private final boolean isRightPanelOpened;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public DefaultTopic(GuildChannelIconType guildChannelIconType, Channel channel, boolean z2, boolean z3) {
                    super(guildChannelIconType, z2, z3, null);
                    m.checkNotNullParameter(guildChannelIconType, "channelIconType");
                    m.checkNotNullParameter(channel, "channel");
                    this.channelIconType = guildChannelIconType;
                    this.channel = channel;
                    this.isRightPanelOpened = z2;
                    this.isOnHomeTab = z3;
                }

                public static /* synthetic */ DefaultTopic copy$default(DefaultTopic defaultTopic, GuildChannelIconType guildChannelIconType, Channel channel, boolean z2, boolean z3, int i, Object obj) {
                    if ((i & 1) != 0) {
                        guildChannelIconType = defaultTopic.getChannelIconType();
                    }
                    if ((i & 2) != 0) {
                        channel = defaultTopic.channel;
                    }
                    if ((i & 4) != 0) {
                        z2 = defaultTopic.isRightPanelOpened();
                    }
                    if ((i & 8) != 0) {
                        z3 = defaultTopic.isOnHomeTab();
                    }
                    return defaultTopic.copy(guildChannelIconType, channel, z2, z3);
                }

                public final GuildChannelIconType component1() {
                    return getChannelIconType();
                }

                public final Channel component2() {
                    return this.channel;
                }

                public final boolean component3() {
                    return isRightPanelOpened();
                }

                public final boolean component4() {
                    return isOnHomeTab();
                }

                public final DefaultTopic copy(GuildChannelIconType guildChannelIconType, Channel channel, boolean z2, boolean z3) {
                    m.checkNotNullParameter(guildChannelIconType, "channelIconType");
                    m.checkNotNullParameter(channel, "channel");
                    return new DefaultTopic(guildChannelIconType, channel, z2, z3);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof DefaultTopic)) {
                        return false;
                    }
                    DefaultTopic defaultTopic = (DefaultTopic) obj;
                    return m.areEqual(getChannelIconType(), defaultTopic.getChannelIconType()) && m.areEqual(this.channel, defaultTopic.channel) && isRightPanelOpened() == defaultTopic.isRightPanelOpened() && isOnHomeTab() == defaultTopic.isOnHomeTab();
                }

                public final Channel getChannel() {
                    return this.channel;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild
                public GuildChannelIconType getChannelIconType() {
                    return this.channelIconType;
                }

                public int hashCode() {
                    GuildChannelIconType channelIconType = getChannelIconType();
                    int i = 0;
                    int hashCode = (channelIconType != null ? channelIconType.hashCode() : 0) * 31;
                    Channel channel = this.channel;
                    if (channel != null) {
                        i = channel.hashCode();
                    }
                    int i2 = (hashCode + i) * 31;
                    boolean isRightPanelOpened = isRightPanelOpened();
                    int i3 = 1;
                    if (isRightPanelOpened) {
                        isRightPanelOpened = true;
                    }
                    int i4 = isRightPanelOpened ? 1 : 0;
                    int i5 = isRightPanelOpened ? 1 : 0;
                    int i6 = (i2 + i4) * 31;
                    boolean isOnHomeTab = isOnHomeTab();
                    if (!isOnHomeTab) {
                        i3 = isOnHomeTab;
                    }
                    return i6 + i3;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
                public boolean isOnHomeTab() {
                    return this.isOnHomeTab;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
                public boolean isRightPanelOpened() {
                    return this.isRightPanelOpened;
                }

                public String toString() {
                    StringBuilder R = a.R("DefaultTopic(channelIconType=");
                    R.append(getChannelIconType());
                    R.append(", channel=");
                    R.append(this.channel);
                    R.append(", isRightPanelOpened=");
                    R.append(isRightPanelOpened());
                    R.append(", isOnHomeTab=");
                    R.append(isOnHomeTab());
                    R.append(")");
                    return R.toString();
                }
            }

            /* compiled from: WidgetChannelTopicViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001B\u009b\u0001\u0012\u0006\u0010 \u001a\u00020\u0002\u0012\u0016\u0010!\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u0005\u0012\u0016\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u0005\u0012\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\f0\u0005\u0012\u0006\u0010$\u001a\u00020\u000e\u0012\u0016\u0010%\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011j\u0002`\u0014\u0012\u0006\u0010&\u001a\u00020\u000e\u0012\u0006\u0010'\u001a\u00020\u0018\u0012\u0006\u0010(\u001a\u00020\u001b\u0012\u0006\u0010)\u001a\u00020\u000e\u0012\u0006\u0010*\u001a\u00020\u000e¢\u0006\u0004\bC\u0010DJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ \u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u001c\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\f0\u0005HÆ\u0003¢\u0006\u0004\b\r\u0010\tJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J \u0010\u0015\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011j\u0002`\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0010J\u0010\u0010\u0019\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u0010J\u0010\u0010\u001f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u001f\u0010\u0010Jº\u0001\u0010+\u001a\u00020\u00002\b\b\u0002\u0010 \u001a\u00020\u00022\u0018\b\u0002\u0010!\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u00052\u0018\b\u0002\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00052\u0014\b\u0002\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\f0\u00052\b\b\u0002\u0010$\u001a\u00020\u000e2\u0018\b\u0002\u0010%\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011j\u0002`\u00142\b\b\u0002\u0010&\u001a\u00020\u000e2\b\b\u0002\u0010'\u001a\u00020\u00182\b\b\u0002\u0010(\u001a\u00020\u001b2\b\b\u0002\u0010)\u001a\u00020\u000e2\b\b\u0002\u0010*\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010-\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b-\u0010\u0004J\u0010\u0010/\u001a\u00020.HÖ\u0001¢\u0006\u0004\b/\u00100J\u001a\u00103\u001a\u00020\u000e2\b\u00102\u001a\u0004\u0018\u000101HÖ\u0003¢\u0006\u0004\b3\u00104R\u001c\u0010'\u001a\u00020\u00188\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u00105\u001a\u0004\b6\u0010\u001aR)\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00107\u001a\u0004\b8\u0010\tR\u0019\u0010 \u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u00109\u001a\u0004\b:\u0010\u0004R\u0019\u0010$\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010;\u001a\u0004\b<\u0010\u0010R\u0019\u0010(\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010=\u001a\u0004\b>\u0010\u001dR)\u0010%\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011j\u0002`\u00148\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010?\u001a\u0004\b@\u0010\u0016R\u001c\u0010)\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b)\u0010;\u001a\u0004\b)\u0010\u0010R\u0019\u0010&\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010;\u001a\u0004\b&\u0010\u0010R%\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\f0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b#\u00107\u001a\u0004\bA\u0010\tR)\u0010!\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b!\u00107\u001a\u0004\bB\u0010\tR\u001c\u0010*\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010;\u001a\u0004\b*\u0010\u0010¨\u0006E"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;", "", "component1", "()Ljava/lang/String;", "", "", "Lcom/discord/primitives/ChannelId;", "component2", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "component3", "Lcom/discord/api/role/GuildRole;", "component4", "", "component5", "()Z", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/channels/AST;", "component6", "()Ljava/util/List;", "component7", "Lcom/discord/utilities/channel/GuildChannelIconType;", "component8", "()Lcom/discord/utilities/channel/GuildChannelIconType;", "Lcom/discord/api/channel/Channel;", "component9", "()Lcom/discord/api/channel/Channel;", "component10", "component11", "rawTopicString", "channelNames", "userNames", "roles", "allowAnimatedEmojis", "ast", "isLinkifyConflicting", "channelIconType", "channel", "isRightPanelOpened", "isOnHomeTab", "copy", "(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/api/channel/Channel;ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/channel/GuildChannelIconType;", "getChannelIconType", "Ljava/util/Map;", "getUserNames", "Ljava/lang/String;", "getRawTopicString", "Z", "getAllowAnimatedEmojis", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/List;", "getAst", "getRoles", "getChannelNames", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/api/channel/Channel;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Topic extends Guild {
                private final boolean allowAnimatedEmojis;
                private final List<Node<MessageRenderContext>> ast;
                private final Channel channel;
                private final GuildChannelIconType channelIconType;
                private final Map<Long, String> channelNames;
                private final boolean isLinkifyConflicting;
                private final boolean isOnHomeTab;
                private final boolean isRightPanelOpened;
                private final String rawTopicString;
                private final Map<Long, GuildRole> roles;
                private final Map<Long, String> userNames;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public Topic(String str, Map<Long, String> map, Map<Long, String> map2, Map<Long, GuildRole> map3, boolean z2, List<Node<MessageRenderContext>> list, boolean z3, GuildChannelIconType guildChannelIconType, Channel channel, boolean z4, boolean z5) {
                    super(guildChannelIconType, z4, z5, null);
                    m.checkNotNullParameter(str, "rawTopicString");
                    m.checkNotNullParameter(map, "channelNames");
                    m.checkNotNullParameter(map2, "userNames");
                    m.checkNotNullParameter(map3, "roles");
                    m.checkNotNullParameter(list, "ast");
                    m.checkNotNullParameter(guildChannelIconType, "channelIconType");
                    m.checkNotNullParameter(channel, "channel");
                    this.rawTopicString = str;
                    this.channelNames = map;
                    this.userNames = map2;
                    this.roles = map3;
                    this.allowAnimatedEmojis = z2;
                    this.ast = list;
                    this.isLinkifyConflicting = z3;
                    this.channelIconType = guildChannelIconType;
                    this.channel = channel;
                    this.isRightPanelOpened = z4;
                    this.isOnHomeTab = z5;
                }

                /* JADX WARN: Multi-variable type inference failed */
                public static /* synthetic */ Topic copy$default(Topic topic, String str, Map map, Map map2, Map map3, boolean z2, List list, boolean z3, GuildChannelIconType guildChannelIconType, Channel channel, boolean z4, boolean z5, int i, Object obj) {
                    return topic.copy((i & 1) != 0 ? topic.rawTopicString : str, (i & 2) != 0 ? topic.channelNames : map, (i & 4) != 0 ? topic.userNames : map2, (i & 8) != 0 ? topic.roles : map3, (i & 16) != 0 ? topic.allowAnimatedEmojis : z2, (i & 32) != 0 ? topic.ast : list, (i & 64) != 0 ? topic.isLinkifyConflicting : z3, (i & 128) != 0 ? topic.getChannelIconType() : guildChannelIconType, (i & 256) != 0 ? topic.channel : channel, (i & 512) != 0 ? topic.isRightPanelOpened() : z4, (i & 1024) != 0 ? topic.isOnHomeTab() : z5);
                }

                public final String component1() {
                    return this.rawTopicString;
                }

                public final boolean component10() {
                    return isRightPanelOpened();
                }

                public final boolean component11() {
                    return isOnHomeTab();
                }

                public final Map<Long, String> component2() {
                    return this.channelNames;
                }

                public final Map<Long, String> component3() {
                    return this.userNames;
                }

                public final Map<Long, GuildRole> component4() {
                    return this.roles;
                }

                public final boolean component5() {
                    return this.allowAnimatedEmojis;
                }

                public final List<Node<MessageRenderContext>> component6() {
                    return this.ast;
                }

                public final boolean component7() {
                    return this.isLinkifyConflicting;
                }

                public final GuildChannelIconType component8() {
                    return getChannelIconType();
                }

                public final Channel component9() {
                    return this.channel;
                }

                public final Topic copy(String str, Map<Long, String> map, Map<Long, String> map2, Map<Long, GuildRole> map3, boolean z2, List<Node<MessageRenderContext>> list, boolean z3, GuildChannelIconType guildChannelIconType, Channel channel, boolean z4, boolean z5) {
                    m.checkNotNullParameter(str, "rawTopicString");
                    m.checkNotNullParameter(map, "channelNames");
                    m.checkNotNullParameter(map2, "userNames");
                    m.checkNotNullParameter(map3, "roles");
                    m.checkNotNullParameter(list, "ast");
                    m.checkNotNullParameter(guildChannelIconType, "channelIconType");
                    m.checkNotNullParameter(channel, "channel");
                    return new Topic(str, map, map2, map3, z2, list, z3, guildChannelIconType, channel, z4, z5);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof Topic)) {
                        return false;
                    }
                    Topic topic = (Topic) obj;
                    return m.areEqual(this.rawTopicString, topic.rawTopicString) && m.areEqual(this.channelNames, topic.channelNames) && m.areEqual(this.userNames, topic.userNames) && m.areEqual(this.roles, topic.roles) && this.allowAnimatedEmojis == topic.allowAnimatedEmojis && m.areEqual(this.ast, topic.ast) && this.isLinkifyConflicting == topic.isLinkifyConflicting && m.areEqual(getChannelIconType(), topic.getChannelIconType()) && m.areEqual(this.channel, topic.channel) && isRightPanelOpened() == topic.isRightPanelOpened() && isOnHomeTab() == topic.isOnHomeTab();
                }

                public final boolean getAllowAnimatedEmojis() {
                    return this.allowAnimatedEmojis;
                }

                public final List<Node<MessageRenderContext>> getAst() {
                    return this.ast;
                }

                public final Channel getChannel() {
                    return this.channel;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild
                public GuildChannelIconType getChannelIconType() {
                    return this.channelIconType;
                }

                public final Map<Long, String> getChannelNames() {
                    return this.channelNames;
                }

                public final String getRawTopicString() {
                    return this.rawTopicString;
                }

                public final Map<Long, GuildRole> getRoles() {
                    return this.roles;
                }

                public final Map<Long, String> getUserNames() {
                    return this.userNames;
                }

                public int hashCode() {
                    String str = this.rawTopicString;
                    int i = 0;
                    int hashCode = (str != null ? str.hashCode() : 0) * 31;
                    Map<Long, String> map = this.channelNames;
                    int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
                    Map<Long, String> map2 = this.userNames;
                    int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
                    Map<Long, GuildRole> map3 = this.roles;
                    int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
                    boolean z2 = this.allowAnimatedEmojis;
                    int i2 = 1;
                    if (z2) {
                        z2 = true;
                    }
                    int i3 = z2 ? 1 : 0;
                    int i4 = z2 ? 1 : 0;
                    int i5 = (hashCode4 + i3) * 31;
                    List<Node<MessageRenderContext>> list = this.ast;
                    int hashCode5 = (i5 + (list != null ? list.hashCode() : 0)) * 31;
                    boolean z3 = this.isLinkifyConflicting;
                    if (z3) {
                        z3 = true;
                    }
                    int i6 = z3 ? 1 : 0;
                    int i7 = z3 ? 1 : 0;
                    int i8 = (hashCode5 + i6) * 31;
                    GuildChannelIconType channelIconType = getChannelIconType();
                    int hashCode6 = (i8 + (channelIconType != null ? channelIconType.hashCode() : 0)) * 31;
                    Channel channel = this.channel;
                    if (channel != null) {
                        i = channel.hashCode();
                    }
                    int i9 = (hashCode6 + i) * 31;
                    boolean isRightPanelOpened = isRightPanelOpened();
                    if (isRightPanelOpened) {
                        isRightPanelOpened = true;
                    }
                    int i10 = isRightPanelOpened ? 1 : 0;
                    int i11 = isRightPanelOpened ? 1 : 0;
                    int i12 = (i9 + i10) * 31;
                    boolean isOnHomeTab = isOnHomeTab();
                    if (!isOnHomeTab) {
                        i2 = isOnHomeTab;
                    }
                    return i12 + i2;
                }

                public final boolean isLinkifyConflicting() {
                    return this.isLinkifyConflicting;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
                public boolean isOnHomeTab() {
                    return this.isOnHomeTab;
                }

                @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild, com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
                public boolean isRightPanelOpened() {
                    return this.isRightPanelOpened;
                }

                public String toString() {
                    StringBuilder R = a.R("Topic(rawTopicString=");
                    R.append(this.rawTopicString);
                    R.append(", channelNames=");
                    R.append(this.channelNames);
                    R.append(", userNames=");
                    R.append(this.userNames);
                    R.append(", roles=");
                    R.append(this.roles);
                    R.append(", allowAnimatedEmojis=");
                    R.append(this.allowAnimatedEmojis);
                    R.append(", ast=");
                    R.append(this.ast);
                    R.append(", isLinkifyConflicting=");
                    R.append(this.isLinkifyConflicting);
                    R.append(", channelIconType=");
                    R.append(getChannelIconType());
                    R.append(", channel=");
                    R.append(this.channel);
                    R.append(", isRightPanelOpened=");
                    R.append(isRightPanelOpened());
                    R.append(", isOnHomeTab=");
                    R.append(isOnHomeTab());
                    R.append(")");
                    return R.toString();
                }
            }

            public /* synthetic */ Guild(GuildChannelIconType guildChannelIconType, boolean z2, boolean z3, DefaultConstructorMarker defaultConstructorMarker) {
                this(guildChannelIconType, z2, z3);
            }

            public GuildChannelIconType getChannelIconType() {
                return this.channelIconType;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            private Guild(GuildChannelIconType guildChannelIconType, boolean z2, boolean z3) {
                super(z2, z3, null);
                this.channelIconType = guildChannelIconType;
                this.isRightPanelOpened = z2;
                this.isOnHomeTab = z3;
            }
        }

        /* compiled from: WidgetChannelTopicViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0006\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0006\u0010\u0004R\u001c\u0010\u0007\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0007\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;", "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;", "", "component1", "()Z", "component2", "isRightPanelOpened", "isOnHomeTab", "copy", "(ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NoChannel extends ViewState {
            private final boolean isOnHomeTab;
            private final boolean isRightPanelOpened;

            public NoChannel(boolean z2, boolean z3) {
                super(z2, z3, null);
                this.isRightPanelOpened = z2;
                this.isOnHomeTab = z3;
            }

            public static /* synthetic */ NoChannel copy$default(NoChannel noChannel, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = noChannel.isRightPanelOpened();
                }
                if ((i & 2) != 0) {
                    z3 = noChannel.isOnHomeTab();
                }
                return noChannel.copy(z2, z3);
            }

            public final boolean component1() {
                return isRightPanelOpened();
            }

            public final boolean component2() {
                return isOnHomeTab();
            }

            public final NoChannel copy(boolean z2, boolean z3) {
                return new NoChannel(z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof NoChannel)) {
                    return false;
                }
                NoChannel noChannel = (NoChannel) obj;
                return isRightPanelOpened() == noChannel.isRightPanelOpened() && isOnHomeTab() == noChannel.isOnHomeTab();
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r1v0 */
            /* JADX WARN: Type inference failed for: r1v1 */
            /* JADX WARN: Type inference failed for: r1v2 */
            public int hashCode() {
                boolean isRightPanelOpened = isRightPanelOpened();
                ?? r1 = 1;
                if (isRightPanelOpened) {
                    isRightPanelOpened = true;
                }
                int i = isRightPanelOpened ? 1 : 0;
                int i2 = isRightPanelOpened ? 1 : 0;
                int i3 = i * 31;
                boolean isOnHomeTab = isOnHomeTab();
                if (!isOnHomeTab) {
                    r1 = isOnHomeTab;
                }
                return i3 + (r1 == true ? 1 : 0);
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isOnHomeTab() {
                return this.isOnHomeTab;
            }

            @Override // com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState
            public boolean isRightPanelOpened() {
                return this.isRightPanelOpened;
            }

            public String toString() {
                StringBuilder R = a.R("NoChannel(isRightPanelOpened=");
                R.append(isRightPanelOpened());
                R.append(", isOnHomeTab=");
                R.append(isOnHomeTab());
                R.append(")");
                return R.toString();
            }
        }

        private ViewState(boolean z2, boolean z3) {
            this.isRightPanelOpened = z2;
            this.isOnHomeTab = z3;
        }

        public boolean isOnHomeTab() {
            return this.isOnHomeTab;
        }

        public boolean isRightPanelOpened() {
            return this.isRightPanelOpened;
        }

        public /* synthetic */ ViewState(boolean z2, boolean z3, DefaultConstructorMarker defaultConstructorMarker) {
            this(z2, z3);
        }
    }

    public WidgetChannelTopicViewModel() {
        this(null, null, 3, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetChannelTopicViewModel(rx.Observable r2, com.discord.simpleast.core.parser.Parser r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
        /*
            r1 = this;
            r5 = r4 & 1
            r0 = 0
            if (r5 == 0) goto L17
            com.discord.widgets.channels.WidgetChannelTopicViewModel$Companion r2 = com.discord.widgets.channels.WidgetChannelTopicViewModel.Companion
            r5 = 3
            rx.Observable r2 = com.discord.widgets.channels.WidgetChannelTopicViewModel.Companion.observeNavState$default(r2, r0, r0, r5, r0)
            com.discord.widgets.channels.WidgetChannelTopicViewModel$1 r5 = com.discord.widgets.channels.WidgetChannelTopicViewModel.AnonymousClass1.INSTANCE
            rx.Observable r2 = r2.Y(r5)
            java.lang.String r5 = "observeNavState().switch…avState\n    ).take(1)\n  }"
            d0.z.d.m.checkNotNullExpressionValue(r2, r5)
        L17:
            r4 = r4 & 2
            if (r4 == 0) goto L22
            r3 = 4
            r4 = 0
            r5 = 1
            com.discord.simpleast.core.parser.Parser r3 = com.discord.utilities.textprocessing.DiscordParser.createParser$default(r4, r5, r4, r3, r0)
        L22:
            r1.<init>(r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.WidgetChannelTopicViewModel.<init>(rx.Observable, com.discord.simpleast.core.parser.Parser, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final List<Node<MessageRenderContext>> generateAST(CharSequence charSequence) {
        return Parser.parse$default(this.topicParser, charSequence, MessageParseState.Companion.getInitialState(), null, 4, null);
    }

    private final void handleGuildStoreState(StoreState.Guild guild) {
        String str;
        if (guild instanceof StoreState.Guild.DefaultTopic) {
            StoreState.Guild.DefaultTopic defaultTopic = (StoreState.Guild.DefaultTopic) guild;
            updateViewState(new ViewState.Guild.DefaultTopic(GuildChannelIconUtilsKt.getChannelType(defaultTopic.getChannel()), defaultTopic.getChannel(), guild.isRightPanelOpened(), guild.isOnHomeTab()));
        } else if (guild instanceof StoreState.Guild.Topic) {
            StoreState.Guild.Topic topic = (StoreState.Guild.Topic) guild;
            String z2 = topic.getChannel().z();
            List<Node<MessageRenderContext>> generateAST = generateAST(z2 != null ? z2 : "");
            MessagePreprocessor messagePreprocessor = new MessagePreprocessor(-1L, this.revealedIndices, null, false, null, 28, null);
            messagePreprocessor.process(generateAST);
            Map<Long, User> users = topic.getUsers();
            LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(users.size()));
            Iterator<T> it = users.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                long longValue = ((Number) entry.getKey()).longValue();
                User user = (User) entry.getValue();
                GuildMember guildMember = topic.getMembers().get(Long.valueOf(longValue));
                if (guildMember == null || (str = guildMember.getNick()) == null) {
                    str = user.getUsername();
                }
                linkedHashMap.put(key, str);
            }
            String str2 = z2 != null ? z2 : "";
            Map<Long, Channel> channels = topic.getChannels();
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(channels.size()));
            Iterator<T> it2 = channels.entrySet().iterator();
            while (it2.hasNext()) {
                Map.Entry entry2 = (Map.Entry) it2.next();
                linkedHashMap2.put(entry2.getKey(), ChannelUtils.c((Channel) entry2.getValue()));
            }
            LinkedHashMap linkedHashMap3 = new LinkedHashMap();
            for (Map.Entry entry3 : linkedHashMap2.entrySet()) {
                if (((String) entry3.getValue()).length() > 0) {
                    linkedHashMap3.put(entry3.getKey(), entry3.getValue());
                }
            }
            updateViewState(new ViewState.Guild.Topic(str2, linkedHashMap3, linkedHashMap, topic.getRoles(), topic.getAllowAnimatedEmojis(), generateAST, messagePreprocessor.isLinkifyConflicting(), GuildChannelIconUtilsKt.getChannelType(topic.getChannel()), topic.getChannel(), guild.isRightPanelOpened(), guild.isOnHomeTab()));
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void handlePrivateStoreState(StoreState storeState) {
        if (storeState instanceof StoreState.DM) {
            StoreState.DM dm = (StoreState.DM) storeState;
            User a = ChannelUtils.a(dm.getChannel());
            String nickOrUsername$default = a != null ? GuildMember.Companion.getNickOrUsername$default(GuildMember.Companion, a, null, dm.getChannel(), null, 8, null) : null;
            User a2 = ChannelUtils.a(dm.getChannel());
            Long valueOf = a2 != null ? Long.valueOf(a2.getId()) : null;
            long h = dm.getChannel().h();
            Collection<Map<Long, GuildMember>> guildMembers = dm.getGuildMembers();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = guildMembers.iterator();
            while (it.hasNext()) {
                Map map = (Map) it.next();
                User a3 = ChannelUtils.a(dm.getChannel());
                GuildMember guildMember = (GuildMember) map.get(a3 != null ? Long.valueOf(a3.getId()) : null);
                String nick = guildMember != null ? guildMember.getNick() : null;
                if (nick != null) {
                    arrayList.add(nick);
                }
            }
            Set set = u.toSet(arrayList);
            Collection<Map<Long, GuildMember>> guildMembers2 = dm.getGuildMembers();
            ArrayList arrayList2 = new ArrayList();
            Iterator<T> it2 = guildMembers2.iterator();
            while (it2.hasNext()) {
                Map map2 = (Map) it2.next();
                User a4 = ChannelUtils.a(dm.getChannel());
                GuildMember guildMember2 = (GuildMember) map2.get(a4 != null ? Long.valueOf(a4.getId()) : null);
                if (guildMember2 != null) {
                    arrayList2.add(guildMember2);
                }
            }
            updateViewState(new ViewState.DM(nickOrUsername$default, valueOf, set, u.toList(arrayList2), h, dm.getDeveloperModeEnabled(), storeState.isRightPanelOpened(), storeState.isOnHomeTab()));
        } else if (storeState instanceof StoreState.GDM) {
            StoreState.GDM gdm = (StoreState.GDM) storeState;
            updateViewState(new ViewState.GDM(gdm.getChannel(), gdm.getChannel().h(), gdm.getDeveloperModeEnabled(), storeState.isRightPanelOpened(), storeState.isOnHomeTab()));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        Long l;
        if (storeState instanceof StoreState.Guild.Topic) {
            long h = ((StoreState.Guild.Topic) storeState).getChannel().h();
            Long l2 = this.previousChannelId;
            this.revealedIndices = (l2 != null && h == l2.longValue()) ? this.revealedIndices : n0.emptySet();
            l = Long.valueOf(h);
        } else if (storeState instanceof StoreState.Guild.DefaultTopic) {
            l = Long.valueOf(((StoreState.Guild.DefaultTopic) storeState).getChannel().h());
        } else if (storeState instanceof StoreState.DM) {
            l = Long.valueOf(((StoreState.DM) storeState).getChannel().h());
        } else {
            l = storeState instanceof StoreState.GDM ? Long.valueOf(((StoreState.GDM) storeState).getChannel().h()) : null;
        }
        this.previousChannelId = l;
        if (!(storeState.isRightPanelOpened() == this.wasRightPanelOpened && storeState.isOnHomeTab() == this.wasOnHomeTab)) {
            if (storeState.isRightPanelOpened() && storeState.isOnHomeTab()) {
                this.eventSubject.k.onNext(Event.FocusFirstElement.INSTANCE);
            }
            this.wasRightPanelOpened = storeState.isRightPanelOpened();
            this.wasOnHomeTab = storeState.isOnHomeTab();
        }
        if (storeState instanceof StoreState.NoChannel) {
            updateViewState(new ViewState.NoChannel(storeState.isRightPanelOpened(), storeState.isOnHomeTab()));
        } else if (storeState instanceof StoreState.Guild) {
            handleGuildStoreState((StoreState.Guild) storeState);
        } else {
            handlePrivateStoreState(storeState);
        }
    }

    public final Unit handleClosePrivateChannel(Context context) {
        m.checkNotNullParameter(context, "context");
        Long l = this.previousChannelId;
        if (l == null) {
            return null;
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteChannel(l.longValue()), false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : context, "javaClass", (r18 & 4) != 0 ? null : null, new WidgetChannelTopicViewModel$handleClosePrivateChannel$$inlined$let$lambda$1(this, context), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
        return Unit.a;
    }

    @MainThread
    public final void handleOnIndexClicked(SpoilerNode<?> spoilerNode) {
        m.checkNotNullParameter(spoilerNode, "spoilerNode");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Guild.Topic)) {
            viewState = null;
        }
        ViewState.Guild.Topic topic = (ViewState.Guild.Topic) viewState;
        if (topic != null) {
            List<Node<MessageRenderContext>> generateAST = generateAST(topic.getRawTopicString());
            this.revealedIndices = o0.plus(this.revealedIndices, Integer.valueOf(spoilerNode.getId()));
            MessagePreprocessor messagePreprocessor = new MessagePreprocessor(-1L, this.revealedIndices, null, false, null, 28, null);
            messagePreprocessor.process(generateAST);
            updateViewState(ViewState.Guild.Topic.copy$default(topic, null, null, null, null, false, generateAST, messagePreprocessor.isLinkifyConflicting(), null, null, false, false, 1951, null));
        }
    }

    public final Observable<Event> listenForEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelTopicViewModel(Observable<StoreState> observable, Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser) {
        super(new ViewState.NoChannel(false, false));
        m.checkNotNullParameter(observable, "storeStateObservable");
        m.checkNotNullParameter(parser, "topicParser");
        this.topicParser = parser;
        this.revealedIndices = n0.emptySet();
        this.previousChannelId = 0L;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetChannelTopicViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
