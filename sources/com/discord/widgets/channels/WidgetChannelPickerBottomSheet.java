package com.discord.widgets.channels;

import andhook.lib.HookHelper;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import androidx.core.os.BundleKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetChannelPickerSheetBinding;
import com.discord.utilities.channel.GuildChannelIconUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.SearchInputView;
import com.discord.widgets.channels.ChannelPickerAdapterItem;
import com.discord.widgets.channels.WidgetChannelPickerBottomSheet;
import com.discord.widgets.channels.WidgetChannelPickerBottomSheetViewModel;
import com.discord.widgets.channels.WidgetchannelPickerAdapter;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import d0.g;
import d0.o;
import d0.t.k;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetChannelPickerBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000s\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t*\u0001\u001b\u0018\u0000 =2\u00020\u0001:\u0002=>B\u0007¢\u0006\u0004\b;\u0010<J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ!\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u001f\u0010\u0019\u001a\u00020\u00042\u0010\b\u0002\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0018¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010$R\u001d\u0010*\u001a\u00020%8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u001d\u0010/\u001a\u00020+8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b,\u0010'\u001a\u0004\b-\u0010.R\u001d\u00102\u001a\u00020+8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b0\u0010'\u001a\u0004\b1\u0010.R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b4\u00105R\u001d\u0010:\u001a\u0002068B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b7\u0010'\u001a\u0004\b8\u00109¨\u0006?"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheetViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheetViewModel$ViewState;)V", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Landroid/content/DialogInterface;", "dialog", "onCancel", "(Landroid/content/DialogInterface;)V", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Lkotlin/Function0;", "setOnCancel", "(Lkotlin/jvm/functions/Function0;)V", "com/discord/widgets/channels/WidgetChannelPickerBottomSheet$itemClickListener$1", "itemClickListener", "Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheet$itemClickListener$1;", "Lcom/discord/databinding/WidgetChannelPickerSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelPickerSheetBinding;", "binding", "Lkotlin/jvm/functions/Function0;", "", "hideAnnouncementChannels$delegate", "Lkotlin/Lazy;", "getHideAnnouncementChannels", "()Z", "hideAnnouncementChannels", "", "selectedChannelId$delegate", "getSelectedChannelId", "()J", "selectedChannelId", "guildId$delegate", "getGuildId", "guildId", "Lcom/discord/widgets/channels/WidgetchannelPickerAdapter;", "adapter", "Lcom/discord/widgets/channels/WidgetchannelPickerAdapter;", "Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheetViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheetViewModel;", "viewModel", HookHelper.constructorName, "()V", "Companion", "SelectionType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelPickerBottomSheet extends AppBottomSheet {
    private static final String ARG_CHANNEL_PICKER_GUILD_ID = "ARG_CHANNEL_PICKER_GUILD_ID";
    private static final String ARG_HIDE_ANNOUNCEMENT_CHANNELS = "ARG_HIDE_ANNOUNCEMENT_CHANNELS";
    private static final String ARG_REQUEST_KEY = "INTENT_EXTRA_REQUEST_CODE";
    private static final String ARG_SELECTED_CHANNEL_ID = "ARG_SELECTED_CHANNEL_ID";
    private static final int CHANNEL_PICKER_VIEW_FLIPPER_LOADING_STATE = 0;
    private static final int CHANNEL_PICKER_VIEW_FLIPPER_RESULT = 1;
    private static final String RESULT_EXTRA_CHANNEL_ICON_RES_ID = "RESULT_EXTRA_CHANNEL_ICON_RES_ID";
    private static final String RESULT_EXTRA_CHANNEL_ID = "RESULT_EXTRA_CHANNEL_ID";
    private static final String RESULT_EXTRA_CHANNEL_NAME = "RESULT_EXTRA_CHANNEL_NAME";
    private static final String RESULT_EXTRA_SELECTION_TYPE = "RESULT_EXTRA_SELECTION_TYPE";
    private WidgetchannelPickerAdapter adapter;
    private Function0<Unit> onCancel;
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelPickerBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelPickerSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelPickerBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy selectedChannelId$delegate = g.lazy(new WidgetChannelPickerBottomSheet$selectedChannelId$2(this));
    private final Lazy guildId$delegate = g.lazy(new WidgetChannelPickerBottomSheet$guildId$2(this));
    private final Lazy hideAnnouncementChannels$delegate = g.lazy(new WidgetChannelPickerBottomSheet$hideAnnouncementChannels$2(this));
    private final WidgetChannelPickerBottomSheet$itemClickListener$1 itemClickListener = new WidgetchannelPickerAdapter.OnItemClickListener() { // from class: com.discord.widgets.channels.WidgetChannelPickerBottomSheet$itemClickListener$1
        @Override // com.discord.widgets.channels.WidgetchannelPickerAdapter.OnItemClickListener
        public void onChannelItemClick(ChannelPickerAdapterItem.ChannelItem channelItem) {
            Bundle argumentsOrDefault;
            m.checkNotNullParameter(channelItem, "channelItem");
            argumentsOrDefault = WidgetChannelPickerBottomSheet.this.getArgumentsOrDefault();
            String string = argumentsOrDefault.getString("INTENT_EXTRA_REQUEST_CODE", "");
            WidgetChannelPickerBottomSheet widgetChannelPickerBottomSheet = WidgetChannelPickerBottomSheet.this;
            m.checkNotNullExpressionValue(string, "requestCode");
            FragmentKt.setFragmentResult(widgetChannelPickerBottomSheet, string, BundleKt.bundleOf(o.to("RESULT_EXTRA_CHANNEL_ID", Long.valueOf(channelItem.getChannel().h())), o.to("RESULT_EXTRA_CHANNEL_NAME", ChannelUtils.c(channelItem.getChannel())), o.to("RESULT_EXTRA_CHANNEL_ICON_RES_ID", Integer.valueOf(GuildChannelIconUtilsKt.guildChannelIcon(channelItem.getChannel()))), o.to("RESULT_EXTRA_SELECTION_TYPE", Integer.valueOf(WidgetChannelPickerBottomSheet.SelectionType.CHANNEL.ordinal()))));
            WidgetChannelPickerBottomSheet.this.dismiss();
        }

        @Override // com.discord.widgets.channels.WidgetchannelPickerAdapter.OnItemClickListener
        public void onCreateChannelClick() {
            Bundle argumentsOrDefault;
            argumentsOrDefault = WidgetChannelPickerBottomSheet.this.getArgumentsOrDefault();
            String string = argumentsOrDefault.getString("INTENT_EXTRA_REQUEST_CODE", "");
            WidgetChannelPickerBottomSheet widgetChannelPickerBottomSheet = WidgetChannelPickerBottomSheet.this;
            m.checkNotNullExpressionValue(string, "requestCode");
            Bundle bundle = new Bundle();
            bundle.putInt("RESULT_EXTRA_SELECTION_TYPE", WidgetChannelPickerBottomSheet.SelectionType.CREATE_CHANNEL.ordinal());
            FragmentKt.setFragmentResult(widgetChannelPickerBottomSheet, string, bundle);
            WidgetChannelPickerBottomSheet.this.dismiss();
        }
    };

    /* compiled from: WidgetChannelPickerBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b$\u0010%JO\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\"\u0010\u000b\u001a\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00062\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\f¢\u0006\u0004\b\u000e\u0010\u000fJW\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\u0011\u001a\u00060\u0007j\u0002`\u00102\u0010\b\u0002\u0010\u0012\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\f2\b\b\u0002\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001a\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0019R\u0016\u0010\u001c\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u0019R\u0016\u0010\u001d\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u001eR\u0016\u0010 \u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b \u0010\u0019R\u0016\u0010!\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\u0019R\u0016\u0010\"\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010\u0019R\u0016\u0010#\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010\u0019¨\u0006&"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheet$Companion;", "", "Landroidx/fragment/app/Fragment;", "fragment", "", "requestKey", "Lkotlin/Function3;", "", "Lcom/discord/primitives/ChannelId;", "", "", "onChannelSelected", "Lkotlin/Function0;", "onCreateChannelSelected", "registerForResult", "(Landroidx/fragment/app/Fragment;Ljava/lang/String;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/primitives/GuildId;", "guildId", "selectedChannelId", "onCancel", "", "hideAnnouncementChannels", "launch", "(Landroidx/fragment/app/Fragment;Ljava/lang/String;JLjava/lang/Long;Lkotlin/jvm/functions/Function0;Z)V", WidgetChannelPickerBottomSheet.ARG_CHANNEL_PICKER_GUILD_ID, "Ljava/lang/String;", WidgetChannelPickerBottomSheet.ARG_HIDE_ANNOUNCEMENT_CHANNELS, "ARG_REQUEST_KEY", WidgetChannelPickerBottomSheet.ARG_SELECTED_CHANNEL_ID, "CHANNEL_PICKER_VIEW_FLIPPER_LOADING_STATE", "I", "CHANNEL_PICKER_VIEW_FLIPPER_RESULT", WidgetChannelPickerBottomSheet.RESULT_EXTRA_CHANNEL_ICON_RES_ID, WidgetChannelPickerBottomSheet.RESULT_EXTRA_CHANNEL_ID, WidgetChannelPickerBottomSheet.RESULT_EXTRA_CHANNEL_NAME, WidgetChannelPickerBottomSheet.RESULT_EXTRA_SELECTION_TYPE, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                SelectionType.values();
                int[] iArr = new int[3];
                $EnumSwitchMapping$0 = iArr;
                iArr[SelectionType.CREATE_CHANNEL.ordinal()] = 1;
                iArr[SelectionType.CHANNEL.ordinal()] = 2;
                iArr[SelectionType.UNKNOWN.ordinal()] = 3;
            }
        }

        private Companion() {
        }

        public final void launch(Fragment fragment, String str, long j, Long l, Function0<Unit> function0, boolean z2) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            WidgetChannelPickerBottomSheet widgetChannelPickerBottomSheet = new WidgetChannelPickerBottomSheet();
            Bundle bundle = new Bundle();
            bundle.putString(WidgetChannelPickerBottomSheet.ARG_REQUEST_KEY, str);
            bundle.putLong(WidgetChannelPickerBottomSheet.ARG_CHANNEL_PICKER_GUILD_ID, j);
            bundle.putBoolean(WidgetChannelPickerBottomSheet.ARG_HIDE_ANNOUNCEMENT_CHANNELS, z2);
            if (l != null) {
                bundle.putLong(WidgetChannelPickerBottomSheet.ARG_SELECTED_CHANNEL_ID, l.longValue());
            }
            widgetChannelPickerBottomSheet.setArguments(bundle);
            widgetChannelPickerBottomSheet.setOnCancel(function0);
            FragmentManager parentFragmentManager = fragment.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "fragment.parentFragmentManager");
            widgetChannelPickerBottomSheet.show(parentFragmentManager, WidgetChannelPickerBottomSheet.class.getName());
        }

        public final void registerForResult(Fragment fragment, String str, Function3<? super Long, ? super String, ? super Integer, Unit> function3, Function0<Unit> function0) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(str, "requestKey");
            m.checkNotNullParameter(function3, "onChannelSelected");
            m.checkNotNullParameter(function0, "onCreateChannelSelected");
            FragmentKt.setFragmentResultListener(fragment, str, new WidgetChannelPickerBottomSheet$Companion$registerForResult$1(str, function0, function3));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelPickerBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheet$SelectionType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "UNKNOWN", "CREATE_CHANNEL", AutocompleteSelectionTypes.CHANNEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum SelectionType {
        UNKNOWN,
        CREATE_CHANNEL,
        CHANNEL;
        
        public static final Companion Companion = new Companion(null);

        /* compiled from: WidgetChannelPickerBottomSheet.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheet$SelectionType$Companion;", "", "", "value", "Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheet$SelectionType;", "fromInt", "(I)Lcom/discord/widgets/channels/WidgetChannelPickerBottomSheet$SelectionType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final SelectionType fromInt(int i) {
                SelectionType selectionType = (SelectionType) k.getOrNull(SelectionType.values(), i);
                return selectionType != null ? selectionType : SelectionType.UNKNOWN;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    /* JADX WARN: Type inference failed for: r0v13, types: [com.discord.widgets.channels.WidgetChannelPickerBottomSheet$itemClickListener$1] */
    public WidgetChannelPickerBottomSheet() {
        super(false, 1, null);
        WidgetChannelPickerBottomSheet$viewModel$2 widgetChannelPickerBottomSheet$viewModel$2 = new WidgetChannelPickerBottomSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetChannelPickerBottomSheetViewModel.class), new WidgetChannelPickerBottomSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetChannelPickerBottomSheet$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetChannelPickerBottomSheetViewModel.ViewState viewState) {
        if (viewState instanceof WidgetChannelPickerBottomSheetViewModel.ViewState.Loading) {
            AppViewFlipper appViewFlipper = getBinding().f2247b;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.channelPickerAppFlipper");
            appViewFlipper.setDisplayedChild(0);
            SearchInputView searchInputView = getBinding().d;
            m.checkNotNullExpressionValue(searchInputView, "binding.channelPickerSearchInput");
            searchInputView.setVisibility(8);
        } else if (viewState instanceof WidgetChannelPickerBottomSheetViewModel.ViewState.Loaded) {
            SearchInputView searchInputView2 = getBinding().d;
            m.checkNotNullExpressionValue(searchInputView2, "binding.channelPickerSearchInput");
            searchInputView2.setVisibility(0);
            AppViewFlipper appViewFlipper2 = getBinding().f2247b;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.channelPickerAppFlipper");
            appViewFlipper2.setDisplayedChild(1);
            WidgetchannelPickerAdapter widgetchannelPickerAdapter = this.adapter;
            if (widgetchannelPickerAdapter == null) {
                m.throwUninitializedPropertyAccessException("adapter");
            }
            widgetchannelPickerAdapter.setItems(((WidgetChannelPickerBottomSheetViewModel.ViewState.Loaded) viewState).getAdapterItems());
        }
    }

    private final WidgetChannelPickerSheetBinding getBinding() {
        return (WidgetChannelPickerSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean getHideAnnouncementChannels() {
        return ((Boolean) this.hideAnnouncementChannels$delegate.getValue()).booleanValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getSelectedChannelId() {
        return ((Number) this.selectedChannelId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChannelPickerBottomSheetViewModel getViewModel() {
        return (WidgetChannelPickerBottomSheetViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void setOnCancel$default(WidgetChannelPickerBottomSheet widgetChannelPickerBottomSheet, Function0 function0, int i, Object obj) {
        if ((i & 1) != 0) {
            function0 = null;
        }
        widgetChannelPickerBottomSheet.setOnCancel(function0);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        super.bindSubscriptions(compositeSubscription);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetChannelPickerBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelPickerBottomSheet$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_channel_picker_sheet;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onCancel(dialogInterface);
        AppBottomSheet.hideKeyboard$default(this, null, 1, null);
        Function0<Unit> function0 = this.onCancel;
        if (function0 != null) {
            function0.invoke();
        }
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        this.adapter = new WidgetchannelPickerAdapter(this.itemClickListener, this, null, 4, null);
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.channelPickerRecycler");
        WidgetchannelPickerAdapter widgetchannelPickerAdapter = this.adapter;
        if (widgetchannelPickerAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        recyclerView.setAdapter(widgetchannelPickerAdapter);
        getBinding().d.a(this, new WidgetChannelPickerBottomSheet$onViewCreated$1(this));
    }

    public final void setOnCancel(Function0<Unit> function0) {
        this.onCancel = function0;
    }
}
