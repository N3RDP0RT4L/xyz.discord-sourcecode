package com.discord.widgets.channels.memberlist.adapter;

import andhook.lib.HookHelper;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChannelMembersListItemLoadingBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ChannelMembersListViewHolderLoading.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderLoading;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "bind", "(I)V", "Lcom/discord/databinding/WidgetChannelMembersListItemLoadingBinding;", "binding", "Lcom/discord/databinding/WidgetChannelMembersListItemLoadingBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetChannelMembersListItemLoadingBinding;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelMembersListViewHolderLoading extends RecyclerView.ViewHolder {
    public static final Companion Companion = new Companion(null);
    private static final Map<Integer, Float> POSITION_PERCENT_MAP;
    private final WidgetChannelMembersListItemLoadingBinding binding;

    /* compiled from: ChannelMembersListViewHolderLoading.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0010\u0007\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\"\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderLoading$Companion;", "", "", "", "", "POSITION_PERCENT_MAP", "Ljava/util/Map;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    static {
        Float valueOf = Float.valueOf(0.7f);
        Float valueOf2 = Float.valueOf(0.3f);
        Float valueOf3 = Float.valueOf(0.6f);
        Float valueOf4 = Float.valueOf(0.4f);
        POSITION_PERCENT_MAP = h0.mapOf(o.to(0, valueOf), o.to(1, valueOf2), o.to(2, valueOf3), o.to(3, valueOf4), o.to(4, valueOf3), o.to(5, Float.valueOf(0.8f)), o.to(6, valueOf2), o.to(7, Float.valueOf(0.5f)), o.to(8, valueOf), o.to(9, valueOf4));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelMembersListViewHolderLoading(WidgetChannelMembersListItemLoadingBinding widgetChannelMembersListItemLoadingBinding) {
        super(widgetChannelMembersListItemLoadingBinding.a);
        m.checkNotNullParameter(widgetChannelMembersListItemLoadingBinding, "binding");
        this.binding = widgetChannelMembersListItemLoadingBinding;
    }

    public final void bind(int i) {
        Guideline guideline = this.binding.f2243b;
        Float f = POSITION_PERCENT_MAP.get(Integer.valueOf(i % 10));
        guideline.setGuidelinePercent(f != null ? f.floatValue() : 0.0f);
    }
}
