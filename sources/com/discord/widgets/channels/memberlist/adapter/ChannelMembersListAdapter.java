package com.discord.widgets.channels.memberlist.adapter;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.ColorInt;
import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetChannelMembersListItemAddOrLeaveBinding;
import com.discord.databinding.WidgetChannelMembersListItemHeaderBinding;
import com.discord.databinding.WidgetChannelMembersListItemLoadingBinding;
import com.discord.databinding.WidgetChannelMembersListItemPlaceholderHeaderBinding;
import com.discord.databinding.WidgetChannelMembersListItemUserBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.presence.Presence;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.utilities.views.StickyHeaderItemDecoration;
import com.discord.views.StatusView;
import com.discord.views.UsernameView;
import com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel;
import com.discord.widgets.roles.RoleIconView;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func2;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: ChannelMembersListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0006JKLMNOB\u0007¢\u0006\u0004\bH\u0010IJ\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J%\u0010 \u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b\"\u0010#J'\u0010'\u001a\u00020\u000b2\u0018\u0010&\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`%\u0012\u0004\u0012\u00020\u000b\u0018\u00010$¢\u0006\u0004\b'\u0010(J\u001b\u0010+\u001a\u00020\u000b2\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u000b0)¢\u0006\u0004\b+\u0010,J\u001b\u0010.\u001a\u00020\u000b2\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u000b0)¢\u0006\u0004\b.\u0010,J\u0019\u00100\u001a\u0004\u0018\u00010\u00042\u0006\u0010/\u001a\u00020\u0004H\u0016¢\u0006\u0004\b0\u00101J\u0019\u00103\u001a\u0004\u0018\u0001022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b3\u00104J\u0017\u00105\u001a\u00020\u001e2\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b5\u00106R:\u0010:\u001a&\u0012\f\u0012\n 9*\u0004\u0018\u00010808 9*\u0012\u0012\f\u0012\n 9*\u0004\u0018\u00010808\u0018\u000107078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R\u001e\u0010<\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010)8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u0010=R\u0018\u0010?\u001a\u0004\u0018\u00010>8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u0010@R\u0016\u0010B\u001a\u00020A8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bB\u0010CR\u001e\u0010D\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010)8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010=R\u0016\u0010E\u001a\u00020\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bE\u0010FR(\u0010&\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`%\u0012\u0004\u0012\u00020\u000b\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010G¨\u0006P"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "getItemId", "(I)J", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "onAttachedToRecyclerView", "(Landroidx/recyclerview/widget/RecyclerView;)V", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "getItemCount", "()I", "getItemViewType", "(I)I", "holder", "onBindViewHolder", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V", "", "listId", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "rows", "", "forceOverwrite", "setData", "(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Z)V", "dispose", "()Lkotlin/Unit;", "Lkotlin/Function1;", "Lcom/discord/primitives/UserId;", "onUserClicked", "setOnUserClicked", "(Lkotlin/jvm/functions/Function1;)V", "Lkotlin/Function0;", "addMember", "setOnAddMemberClicked", "(Lkotlin/jvm/functions/Function0;)V", "joinLeaveThread", "setOnJoinLeaveThreadClicked", "itemPosition", "getHeaderPositionForItem", "(I)Ljava/lang/Integer;", "Landroid/view/View;", "getAndBindHeaderView", "(I)Landroid/view/View;", "isHeader", "(I)Z", "Lrx/subjects/PublishSubject;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "kotlin.jvm.PlatformType", "updatesSubject", "Lrx/subjects/PublishSubject;", "onJoinLeaveThreadClicked", "Lkotlin/jvm/functions/Function0;", "Lrx/Subscription;", "updatesSubscription", "Lrx/Subscription;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;", "stickyHeadersManager", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;", "onAddMemberClicked", "memberList", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "()V", "DiffUtilCallback", "Item", "ListUpdateOperation", "ListUpdateRequest", "StickyHeadersManager", "ViewType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelMembersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyHeaderItemDecoration.StickyHeaderAdapter {
    private WidgetChannelMembersListViewModel.MemberList memberList;
    private Function0<Unit> onAddMemberClicked;
    private Function0<Unit> onJoinLeaveThreadClicked;
    private Function1<? super Long, Unit> onUserClicked;
    private StickyHeadersManager stickyHeadersManager;
    private final PublishSubject<ListUpdateRequest> updatesSubject;
    private Subscription updatesSubscription;

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "prevRequest", "nextRequest", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "invoke", "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "computeListUpdateOperation"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function2<ListUpdateRequest, ListUpdateRequest, ListUpdateOperation> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(2);
        }

        public final ListUpdateOperation invoke(ListUpdateRequest listUpdateRequest, ListUpdateRequest listUpdateRequest2) {
            m.checkNotNullParameter(listUpdateRequest, "prevRequest");
            m.checkNotNullParameter(listUpdateRequest2, "nextRequest");
            if (listUpdateRequest2.getForceOverwrite()) {
                return new ListUpdateOperation.OverwriteUpdate(listUpdateRequest2);
            }
            String component1 = listUpdateRequest.component1();
            WidgetChannelMembersListViewModel.MemberList component2 = listUpdateRequest.component2();
            String component12 = listUpdateRequest2.component1();
            WidgetChannelMembersListViewModel.MemberList component22 = listUpdateRequest2.component2();
            if (!m.areEqual(component1, component12) || Math.abs(component2.getSize() - component22.getSize()) >= 255) {
                return new ListUpdateOperation.OverwriteUpdate(listUpdateRequest2);
            }
            DiffUtil.DiffResult calculateDiff = DiffUtil.calculateDiff(new DiffUtilCallback(component2, component22));
            m.checkNotNullExpressionValue(calculateDiff, "DiffUtil.calculateDiff(diffUtilCallback)");
            return new ListUpdateOperation.DiffUpdate(calculateDiff, listUpdateRequest2);
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "kotlin.jvm.PlatformType", "prevUpdate", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "nextUpdate", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2<T1, T2, R> implements Func2<ListUpdateOperation, ListUpdateRequest, ListUpdateOperation> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public final ListUpdateOperation call(ListUpdateOperation listUpdateOperation, ListUpdateRequest listUpdateRequest) {
            AnonymousClass1 r0 = AnonymousClass1.INSTANCE;
            ListUpdateRequest request = listUpdateOperation.getRequest();
            m.checkNotNullExpressionValue(listUpdateRequest, "nextUpdate");
            return r0.invoke(request, listUpdateRequest);
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "kotlin.jvm.PlatformType", "listUpdateResult", "", "invoke", "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends o implements Function1<ListUpdateOperation, Unit> {
        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ListUpdateOperation listUpdateOperation) {
            invoke2(listUpdateOperation);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ListUpdateOperation listUpdateOperation) {
            ChannelMembersListAdapter.this.memberList = listUpdateOperation.getRequest().getRows();
            if (listUpdateOperation instanceof ListUpdateOperation.DiffUpdate) {
                ((ListUpdateOperation.DiffUpdate) listUpdateOperation).getDiffResult().dispatchUpdatesTo(ChannelMembersListAdapter.this);
            } else if (listUpdateOperation instanceof ListUpdateOperation.OverwriteUpdate) {
                ChannelMembersListAdapter.this.notifyDataSetChanged();
            }
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lrx/Subscription;", "it", "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass4 extends o implements Function1<Subscription, Unit> {
        public AnonymousClass4() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
            invoke2(subscription);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Subscription subscription) {
            m.checkNotNullParameter(subscription, "it");
            ChannelMembersListAdapter.this.updatesSubscription = subscription;
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u000f\u001a\u00020\f¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u000b\u0010\nR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$DiffUtilCallback;", "Landroidx/recyclerview/widget/DiffUtil$Callback;", "", "getOldListSize", "()I", "getNewListSize", "oldItemPosition", "newItemPosition", "", "areItemsTheSame", "(II)Z", "areContentsTheSame", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "prevMemberList", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "nextMemberList", HookHelper.constructorName, "(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DiffUtilCallback extends DiffUtil.Callback {
        private final WidgetChannelMembersListViewModel.MemberList nextMemberList;
        private final WidgetChannelMembersListViewModel.MemberList prevMemberList;

        public DiffUtilCallback(WidgetChannelMembersListViewModel.MemberList memberList, WidgetChannelMembersListViewModel.MemberList memberList2) {
            m.checkNotNullParameter(memberList, "prevMemberList");
            m.checkNotNullParameter(memberList2, "nextMemberList");
            this.prevMemberList = memberList;
            this.nextMemberList = memberList2;
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public boolean areContentsTheSame(int i, int i2) {
            return m.areEqual(this.prevMemberList.get(i), this.nextMemberList.get(i2));
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public boolean areItemsTheSame(int i, int i2) {
            return m.areEqual(this.prevMemberList.get(i).getRowId(), this.nextMemberList.get(i2).getRowId());
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public int getNewListSize() {
            return this.nextMemberList.getSize();
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public int getOldListSize() {
            return this.prevMemberList.getSize();
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0007\r\u000e\u000f\u0010\u0011\u0012\u0013B\u0019\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0001¢\u0006\u0004\b\u000b\u0010\fR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\u00020\u00018\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\n\u0082\u0001\u0007\u0014\u0015\u0016\u0017\u0018\u0019\u001a¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;", "type", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;", "getType", "()Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;", "rowId", "Ljava/lang/Object;", "getRowId", "()Ljava/lang/Object;", HookHelper.constructorName, "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;Ljava/lang/Object;)V", "AddMember", Traits.Location.Section.HEADER, "JoinLeaveThread", "Member", "PlaceholderHeader", "PlaceholderMember", "RoleHeader", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$JoinLeaveThread;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item {
        private final Object rowId;
        private final ViewType type;

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\b\u0001\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0003\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\r\u0010\u0007J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0013\u001a\u0004\b\u0014\u0010\u0007R\u001c\u0010\b\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", "rowId", "title", "copy", "(Ljava/lang/String;I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getTitle", "Ljava/lang/String;", "getRowId", HookHelper.constructorName, "(Ljava/lang/String;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AddMember extends Item {
            private final String rowId;
            private final int title;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AddMember(String str, @StringRes int i) {
                super(ViewType.ADD_MEMBER, str, null);
                m.checkNotNullParameter(str, "rowId");
                this.rowId = str;
                this.title = i;
            }

            public static /* synthetic */ AddMember copy$default(AddMember addMember, String str, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    str = addMember.getRowId();
                }
                if ((i2 & 2) != 0) {
                    i = addMember.title;
                }
                return addMember.copy(str, i);
            }

            public final String component1() {
                return getRowId();
            }

            public final int component2() {
                return this.title;
            }

            public final AddMember copy(String str, @StringRes int i) {
                m.checkNotNullParameter(str, "rowId");
                return new AddMember(str, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof AddMember)) {
                    return false;
                }
                AddMember addMember = (AddMember) obj;
                return m.areEqual(getRowId(), addMember.getRowId()) && this.title == addMember.title;
            }

            public final int getTitle() {
                return this.title;
            }

            public int hashCode() {
                String rowId = getRowId();
                return ((rowId != null ? rowId.hashCode() : 0) * 31) + this.title;
            }

            public String toString() {
                StringBuilder R = a.R("AddMember(rowId=");
                R.append(getRowId());
                R.append(", title=");
                return a.A(R, this.title, ")");
            }

            @Override // com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item
            public String getRowId() {
                return this.rowId;
            }
        }

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001fB\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0011\u0010\nJ\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001c\u0010\u000b\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\n¨\u0006 "}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "component1", "()Ljava/lang/String;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;", "component2", "()Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;", "", "component3", "()I", "rowId", "headerType", "memberCount", "copy", "(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getRowId", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;", "getHeaderType", "I", "getMemberCount", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;I)V", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Header extends Item {
            private final Type headerType;
            private final int memberCount;
            private final String rowId;

            /* compiled from: ChannelMembersListAdapter.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ONLINE", "OFFLINE", "GROUP_DM", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public enum Type {
                ONLINE,
                OFFLINE,
                GROUP_DM
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Header(String str, Type type, int i) {
                super(ViewType.HEADER, str, null);
                m.checkNotNullParameter(str, "rowId");
                m.checkNotNullParameter(type, "headerType");
                this.rowId = str;
                this.headerType = type;
                this.memberCount = i;
            }

            public static /* synthetic */ Header copy$default(Header header, String str, Type type, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    str = header.getRowId();
                }
                if ((i2 & 2) != 0) {
                    type = header.headerType;
                }
                if ((i2 & 4) != 0) {
                    i = header.memberCount;
                }
                return header.copy(str, type, i);
            }

            public final String component1() {
                return getRowId();
            }

            public final Type component2() {
                return this.headerType;
            }

            public final int component3() {
                return this.memberCount;
            }

            public final Header copy(String str, Type type, int i) {
                m.checkNotNullParameter(str, "rowId");
                m.checkNotNullParameter(type, "headerType");
                return new Header(str, type, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Header)) {
                    return false;
                }
                Header header = (Header) obj;
                return m.areEqual(getRowId(), header.getRowId()) && m.areEqual(this.headerType, header.headerType) && this.memberCount == header.memberCount;
            }

            public final Type getHeaderType() {
                return this.headerType;
            }

            public final int getMemberCount() {
                return this.memberCount;
            }

            public int hashCode() {
                String rowId = getRowId();
                int i = 0;
                int hashCode = (rowId != null ? rowId.hashCode() : 0) * 31;
                Type type = this.headerType;
                if (type != null) {
                    i = type.hashCode();
                }
                return ((hashCode + i) * 31) + this.memberCount;
            }

            public String toString() {
                StringBuilder R = a.R("Header(rowId=");
                R.append(getRowId());
                R.append(", headerType=");
                R.append(this.headerType);
                R.append(", memberCount=");
                return a.A(R, this.memberCount, ")");
            }

            @Override // com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item
            public String getRowId() {
                return this.rowId;
            }
        }

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\t\u0010\u0007R\u001c\u0010\b\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$JoinLeaveThread;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "component1", "()Ljava/lang/String;", "", "component2", "()Z", "rowId", "isThreadJoined", "copy", "(Ljava/lang/String;Z)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$JoinLeaveThread;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/String;", "getRowId", HookHelper.constructorName, "(Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class JoinLeaveThread extends Item {
            private final boolean isThreadJoined;
            private final String rowId;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public JoinLeaveThread(String str, boolean z2) {
                super(ViewType.JOIN_LEAVE_THREAD, str, null);
                m.checkNotNullParameter(str, "rowId");
                this.rowId = str;
                this.isThreadJoined = z2;
            }

            public static /* synthetic */ JoinLeaveThread copy$default(JoinLeaveThread joinLeaveThread, String str, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = joinLeaveThread.getRowId();
                }
                if ((i & 2) != 0) {
                    z2 = joinLeaveThread.isThreadJoined;
                }
                return joinLeaveThread.copy(str, z2);
            }

            public final String component1() {
                return getRowId();
            }

            public final boolean component2() {
                return this.isThreadJoined;
            }

            public final JoinLeaveThread copy(String str, boolean z2) {
                m.checkNotNullParameter(str, "rowId");
                return new JoinLeaveThread(str, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof JoinLeaveThread)) {
                    return false;
                }
                JoinLeaveThread joinLeaveThread = (JoinLeaveThread) obj;
                return m.areEqual(getRowId(), joinLeaveThread.getRowId()) && this.isThreadJoined == joinLeaveThread.isThreadJoined;
            }

            public int hashCode() {
                String rowId = getRowId();
                int hashCode = (rowId != null ? rowId.hashCode() : 0) * 31;
                boolean z2 = this.isThreadJoined;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public final boolean isThreadJoined() {
                return this.isThreadJoined;
            }

            public String toString() {
                StringBuilder R = a.R("JoinLeaveThread(rowId=");
                R.append(getRowId());
                R.append(", isThreadJoined=");
                return a.M(R, this.isThreadJoined, ")");
            }

            @Override // com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item
            public String getRowId() {
                return this.rowId;
            }
        }

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001c\n\u0002\u0010\u0000\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u0001B\u008f\u0001\u0012\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\u001f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006\u0012\u0006\u0010 \u001a\u00020\t\u0012\u0006\u0010!\u001a\u00020\f\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010#\u001a\u00020\f\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0013\u0012\n\b\u0001\u0010%\u001a\u0004\u0018\u00010\u000f\u0012\b\u0010&\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010'\u001a\u00020\f\u0012\b\u0010(\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010)\u001a\u00020\f\u0012\u0006\u0010*\u001a\u00020\f\u0012\u0006\u0010+\u001a\u00020\u000f¢\u0006\u0004\bG\u0010HJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000eJ\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0011J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u000bJ\u0010\u0010\u0018\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0018\u0010\u000eJ\u0012\u0010\u0019\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\u0019\u0010\u000bJ\u0010\u0010\u001a\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u001a\u0010\u000eJ\u0010\u0010\u001b\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u000eJ\u0010\u0010\u001c\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ²\u0001\u0010,\u001a\u00020\u00002\f\b\u0002\u0010\u001e\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00062\b\b\u0002\u0010 \u001a\u00020\t2\b\b\u0002\u0010!\u001a\u00020\f2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010#\u001a\u00020\f2\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00132\n\b\u0003\u0010%\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010'\u001a\u00020\f2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010)\u001a\u00020\f2\b\b\u0002\u0010*\u001a\u00020\f2\b\b\u0002\u0010+\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b,\u0010-J\u0010\u0010.\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b.\u0010\u000bJ\u0010\u0010/\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b/\u0010\u001dJ\u001a\u00102\u001a\u00020\f2\b\u00101\u001a\u0004\u0018\u000100HÖ\u0003¢\u0006\u0004\b2\u00103R\u0019\u0010!\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\b!\u0010\u000eR\u0019\u0010)\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b)\u00104\u001a\u0004\b)\u0010\u000eR\u001d\u0010\u001e\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00105\u001a\u0004\b6\u0010\u0005R\u001b\u0010$\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b$\u00107\u001a\u0004\b8\u0010\u0015R!\u0010\u001f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00109\u001a\u0004\b:\u0010\bR\u001b\u0010\"\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010;\u001a\u0004\b<\u0010\u0011R\u001b\u0010%\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010;\u001a\u0004\b=\u0010\u0011R\u001b\u0010(\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010>\u001a\u0004\b?\u0010\u000bR\u0019\u0010 \u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010>\u001a\u0004\b@\u0010\u000bR\u0019\u0010*\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b*\u00104\u001a\u0004\bA\u0010\u000eR\u0019\u0010#\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\bB\u0010\u000eR\u0019\u0010'\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b'\u00104\u001a\u0004\bC\u0010\u000eR\u0019\u0010+\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010D\u001a\u0004\bE\u0010\u001dR\u001b\u0010&\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010>\u001a\u0004\bF\u0010\u000b¨\u0006I"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "", "component3", "()Ljava/lang/String;", "", "component4", "()Z", "", "component5", "()Ljava/lang/Integer;", "component6", "Lcom/discord/models/presence/Presence;", "component7", "()Lcom/discord/models/presence/Presence;", "component8", "component9", "component10", "component11", "component12", "component13", "component14", "()I", "userId", "guildId", ModelAuditLogEntry.CHANGE_KEY_NAME, "isBot", "tagText", "tagVerified", "presence", ModelAuditLogEntry.CHANGE_KEY_COLOR, "avatarUrl", "showOwnerIndicator", "premiumSince", "isApplicationStreaming", "canDisplayStatusEmoji", "userFlags", "copy", "(JLjava/lang/Long;Ljava/lang/String;ZLjava/lang/Integer;ZLcom/discord/models/presence/Presence;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;ZZI)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "J", "getUserId", "Lcom/discord/models/presence/Presence;", "getPresence", "Ljava/lang/Long;", "getGuildId", "Ljava/lang/Integer;", "getTagText", "getColor", "Ljava/lang/String;", "getPremiumSince", "getName", "getCanDisplayStatusEmoji", "getTagVerified", "getShowOwnerIndicator", "I", "getUserFlags", "getAvatarUrl", HookHelper.constructorName, "(JLjava/lang/Long;Ljava/lang/String;ZLjava/lang/Integer;ZLcom/discord/models/presence/Presence;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;ZZI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Member extends Item {
            private final String avatarUrl;
            private final boolean canDisplayStatusEmoji;
            private final Integer color;
            private final Long guildId;
            private final boolean isApplicationStreaming;
            private final boolean isBot;
            private final String name;
            private final String premiumSince;
            private final Presence presence;
            private final boolean showOwnerIndicator;
            private final Integer tagText;
            private final boolean tagVerified;
            private final int userFlags;
            private final long userId;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Member(long j, Long l, String str, boolean z2, Integer num, boolean z3, Presence presence, @ColorInt Integer num2, String str2, boolean z4, String str3, boolean z5, boolean z6, int i) {
                super(ViewType.MEMBER, Long.valueOf(j), null);
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                this.userId = j;
                this.guildId = l;
                this.name = str;
                this.isBot = z2;
                this.tagText = num;
                this.tagVerified = z3;
                this.presence = presence;
                this.color = num2;
                this.avatarUrl = str2;
                this.showOwnerIndicator = z4;
                this.premiumSince = str3;
                this.isApplicationStreaming = z5;
                this.canDisplayStatusEmoji = z6;
                this.userFlags = i;
            }

            public final long component1() {
                return this.userId;
            }

            public final boolean component10() {
                return this.showOwnerIndicator;
            }

            public final String component11() {
                return this.premiumSince;
            }

            public final boolean component12() {
                return this.isApplicationStreaming;
            }

            public final boolean component13() {
                return this.canDisplayStatusEmoji;
            }

            public final int component14() {
                return this.userFlags;
            }

            public final Long component2() {
                return this.guildId;
            }

            public final String component3() {
                return this.name;
            }

            public final boolean component4() {
                return this.isBot;
            }

            public final Integer component5() {
                return this.tagText;
            }

            public final boolean component6() {
                return this.tagVerified;
            }

            public final Presence component7() {
                return this.presence;
            }

            public final Integer component8() {
                return this.color;
            }

            public final String component9() {
                return this.avatarUrl;
            }

            public final Member copy(long j, Long l, String str, boolean z2, Integer num, boolean z3, Presence presence, @ColorInt Integer num2, String str2, boolean z4, String str3, boolean z5, boolean z6, int i) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                return new Member(j, l, str, z2, num, z3, presence, num2, str2, z4, str3, z5, z6, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Member)) {
                    return false;
                }
                Member member = (Member) obj;
                return this.userId == member.userId && m.areEqual(this.guildId, member.guildId) && m.areEqual(this.name, member.name) && this.isBot == member.isBot && m.areEqual(this.tagText, member.tagText) && this.tagVerified == member.tagVerified && m.areEqual(this.presence, member.presence) && m.areEqual(this.color, member.color) && m.areEqual(this.avatarUrl, member.avatarUrl) && this.showOwnerIndicator == member.showOwnerIndicator && m.areEqual(this.premiumSince, member.premiumSince) && this.isApplicationStreaming == member.isApplicationStreaming && this.canDisplayStatusEmoji == member.canDisplayStatusEmoji && this.userFlags == member.userFlags;
            }

            public final String getAvatarUrl() {
                return this.avatarUrl;
            }

            public final boolean getCanDisplayStatusEmoji() {
                return this.canDisplayStatusEmoji;
            }

            public final Integer getColor() {
                return this.color;
            }

            public final Long getGuildId() {
                return this.guildId;
            }

            public final String getName() {
                return this.name;
            }

            public final String getPremiumSince() {
                return this.premiumSince;
            }

            public final Presence getPresence() {
                return this.presence;
            }

            public final boolean getShowOwnerIndicator() {
                return this.showOwnerIndicator;
            }

            public final Integer getTagText() {
                return this.tagText;
            }

            public final boolean getTagVerified() {
                return this.tagVerified;
            }

            public final int getUserFlags() {
                return this.userFlags;
            }

            public final long getUserId() {
                return this.userId;
            }

            public int hashCode() {
                int a = b.a(this.userId) * 31;
                Long l = this.guildId;
                int i = 0;
                int hashCode = (a + (l != null ? l.hashCode() : 0)) * 31;
                String str = this.name;
                int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
                boolean z2 = this.isBot;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode2 + i3) * 31;
                Integer num = this.tagText;
                int hashCode3 = (i5 + (num != null ? num.hashCode() : 0)) * 31;
                boolean z3 = this.tagVerified;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (hashCode3 + i6) * 31;
                Presence presence = this.presence;
                int hashCode4 = (i8 + (presence != null ? presence.hashCode() : 0)) * 31;
                Integer num2 = this.color;
                int hashCode5 = (hashCode4 + (num2 != null ? num2.hashCode() : 0)) * 31;
                String str2 = this.avatarUrl;
                int hashCode6 = (hashCode5 + (str2 != null ? str2.hashCode() : 0)) * 31;
                boolean z4 = this.showOwnerIndicator;
                if (z4) {
                    z4 = true;
                }
                int i9 = z4 ? 1 : 0;
                int i10 = z4 ? 1 : 0;
                int i11 = (hashCode6 + i9) * 31;
                String str3 = this.premiumSince;
                if (str3 != null) {
                    i = str3.hashCode();
                }
                int i12 = (i11 + i) * 31;
                boolean z5 = this.isApplicationStreaming;
                if (z5) {
                    z5 = true;
                }
                int i13 = z5 ? 1 : 0;
                int i14 = z5 ? 1 : 0;
                int i15 = (i12 + i13) * 31;
                boolean z6 = this.canDisplayStatusEmoji;
                if (!z6) {
                    i2 = z6 ? 1 : 0;
                }
                return ((i15 + i2) * 31) + this.userFlags;
            }

            public final boolean isApplicationStreaming() {
                return this.isApplicationStreaming;
            }

            public final boolean isBot() {
                return this.isBot;
            }

            public String toString() {
                StringBuilder R = a.R("Member(userId=");
                R.append(this.userId);
                R.append(", guildId=");
                R.append(this.guildId);
                R.append(", name=");
                R.append(this.name);
                R.append(", isBot=");
                R.append(this.isBot);
                R.append(", tagText=");
                R.append(this.tagText);
                R.append(", tagVerified=");
                R.append(this.tagVerified);
                R.append(", presence=");
                R.append(this.presence);
                R.append(", color=");
                R.append(this.color);
                R.append(", avatarUrl=");
                R.append(this.avatarUrl);
                R.append(", showOwnerIndicator=");
                R.append(this.showOwnerIndicator);
                R.append(", premiumSince=");
                R.append(this.premiumSince);
                R.append(", isApplicationStreaming=");
                R.append(this.isApplicationStreaming);
                R.append(", canDisplayStatusEmoji=");
                R.append(this.canDisplayStatusEmoji);
                R.append(", userFlags=");
                return a.A(R, this.userFlags, ")");
            }
        }

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\t\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0013\u0010\u0005¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "Lcom/discord/primitives/MemberListId;", "component1", "()Ljava/lang/String;", "listId", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getListId", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PlaceholderHeader extends Item {
            private final String listId;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public PlaceholderHeader(String str) {
                super(ViewType.PLACEHOLDER_HEADER, a.v("placeholder:", str), null);
                m.checkNotNullParameter(str, "listId");
                this.listId = str;
            }

            public static /* synthetic */ PlaceholderHeader copy$default(PlaceholderHeader placeholderHeader, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = placeholderHeader.listId;
                }
                return placeholderHeader.copy(str);
            }

            public final String component1() {
                return this.listId;
            }

            public final PlaceholderHeader copy(String str) {
                m.checkNotNullParameter(str, "listId");
                return new PlaceholderHeader(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof PlaceholderHeader) && m.areEqual(this.listId, ((PlaceholderHeader) obj).listId);
                }
                return true;
            }

            public final String getListId() {
                return this.listId;
            }

            public int hashCode() {
                String str = this.listId;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("PlaceholderHeader(listId="), this.listId, ")");
            }
        }

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "component1", "()F", "placeholderSize", "copy", "(F)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "F", HookHelper.constructorName, "(F)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PlaceholderMember extends Item {
            private final float placeholderSize;

            public PlaceholderMember(float f) {
                super(ViewType.LOADING, "", null);
                this.placeholderSize = f;
            }

            private final float component1() {
                return this.placeholderSize;
            }

            public static /* synthetic */ PlaceholderMember copy$default(PlaceholderMember placeholderMember, float f, int i, Object obj) {
                if ((i & 1) != 0) {
                    f = placeholderMember.placeholderSize;
                }
                return placeholderMember.copy(f);
            }

            public final PlaceholderMember copy(float f) {
                return new PlaceholderMember(f);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof PlaceholderMember) && Float.compare(this.placeholderSize, ((PlaceholderMember) obj).placeholderSize) == 0;
                }
                return true;
            }

            public int hashCode() {
                return Float.floatToIntBits(this.placeholderSize);
            }

            public String toString() {
                StringBuilder R = a.R("PlaceholderMember(placeholderSize=");
                R.append(this.placeholderSize);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0010\u001a\u00020\u0006\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u000e\u0010\u0012\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\f¢\u0006\u0004\b$\u0010%J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0018\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJD\u0010\u0013\u001a\u00020\u00002\f\b\u0002\u0010\u000f\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0010\u001a\u00020\u00062\b\b\u0002\u0010\u0011\u001a\u00020\t2\u0010\b\u0002\u0010\u0012\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0015\u0010\bJ\u0010\u0010\u0016\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0016\u0010\u000bJ\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR!\u0010\u0012\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001c\u001a\u0004\b\u001d\u0010\u000eR\u001d\u0010\u000f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005R\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\u000bR\u0019\u0010\u0010\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\b¨\u0006&"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "", "Lcom/discord/primitives/RoleId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "", "component3", "()I", "Lcom/discord/primitives/GuildId;", "component4", "()Ljava/lang/Long;", "roleId", "roleName", "memberCount", "guildId", "copy", "(JLjava/lang/String;ILjava/lang/Long;)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getGuildId", "J", "getRoleId", "I", "getMemberCount", "Ljava/lang/String;", "getRoleName", HookHelper.constructorName, "(JLjava/lang/String;ILjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RoleHeader extends Item {
            private final Long guildId;
            private final int memberCount;
            private final long roleId;
            private final String roleName;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public RoleHeader(long j, String str, int i, Long l) {
                super(ViewType.ROLE_HEADER, Long.valueOf(j), null);
                m.checkNotNullParameter(str, "roleName");
                this.roleId = j;
                this.roleName = str;
                this.memberCount = i;
                this.guildId = l;
            }

            public static /* synthetic */ RoleHeader copy$default(RoleHeader roleHeader, long j, String str, int i, Long l, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    j = roleHeader.roleId;
                }
                long j2 = j;
                if ((i2 & 2) != 0) {
                    str = roleHeader.roleName;
                }
                String str2 = str;
                if ((i2 & 4) != 0) {
                    i = roleHeader.memberCount;
                }
                int i3 = i;
                if ((i2 & 8) != 0) {
                    l = roleHeader.guildId;
                }
                return roleHeader.copy(j2, str2, i3, l);
            }

            public final long component1() {
                return this.roleId;
            }

            public final String component2() {
                return this.roleName;
            }

            public final int component3() {
                return this.memberCount;
            }

            public final Long component4() {
                return this.guildId;
            }

            public final RoleHeader copy(long j, String str, int i, Long l) {
                m.checkNotNullParameter(str, "roleName");
                return new RoleHeader(j, str, i, l);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof RoleHeader)) {
                    return false;
                }
                RoleHeader roleHeader = (RoleHeader) obj;
                return this.roleId == roleHeader.roleId && m.areEqual(this.roleName, roleHeader.roleName) && this.memberCount == roleHeader.memberCount && m.areEqual(this.guildId, roleHeader.guildId);
            }

            public final Long getGuildId() {
                return this.guildId;
            }

            public final int getMemberCount() {
                return this.memberCount;
            }

            public final long getRoleId() {
                return this.roleId;
            }

            public final String getRoleName() {
                return this.roleName;
            }

            public int hashCode() {
                int a = b.a(this.roleId) * 31;
                String str = this.roleName;
                int i = 0;
                int hashCode = (((a + (str != null ? str.hashCode() : 0)) * 31) + this.memberCount) * 31;
                Long l = this.guildId;
                if (l != null) {
                    i = l.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("RoleHeader(roleId=");
                R.append(this.roleId);
                R.append(", roleName=");
                R.append(this.roleName);
                R.append(", memberCount=");
                R.append(this.memberCount);
                R.append(", guildId=");
                return a.F(R, this.guildId, ")");
            }
        }

        private Item(ViewType viewType, Object obj) {
            this.type = viewType;
            this.rowId = obj;
        }

        public Object getRowId() {
            return this.rowId;
        }

        public final ViewType getType() {
            return this.type;
        }

        public /* synthetic */ Item(ViewType viewType, Object obj, DefaultConstructorMarker defaultConstructorMarker) {
            this(viewType, obj);
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0002\t\nB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0002\u000b\f¨\u0006\r"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "request", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "getRequest", "()Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", HookHelper.constructorName, "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;)V", "DiffUpdate", "OverwriteUpdate", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation$OverwriteUpdate;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation$DiffUpdate;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ListUpdateOperation {
        private final ListUpdateRequest request;

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation$DiffUpdate;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "diffResult", "Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "getDiffResult", "()Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "listUpdateRequest", HookHelper.constructorName, "(Landroidx/recyclerview/widget/DiffUtil$DiffResult;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DiffUpdate extends ListUpdateOperation {
            private final DiffUtil.DiffResult diffResult;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public DiffUpdate(DiffUtil.DiffResult diffResult, ListUpdateRequest listUpdateRequest) {
                super(listUpdateRequest, null);
                m.checkNotNullParameter(diffResult, "diffResult");
                m.checkNotNullParameter(listUpdateRequest, "listUpdateRequest");
                this.diffResult = diffResult;
            }

            public final DiffUtil.DiffResult getDiffResult() {
                return this.diffResult;
            }
        }

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation$OverwriteUpdate;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "listUpdateRequest", HookHelper.constructorName, "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class OverwriteUpdate extends ListUpdateOperation {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public OverwriteUpdate(ListUpdateRequest listUpdateRequest) {
                super(listUpdateRequest, null);
                m.checkNotNullParameter(listUpdateRequest, "listUpdateRequest");
            }
        }

        private ListUpdateOperation(ListUpdateRequest listUpdateRequest) {
            this.request = listUpdateRequest;
        }

        public final ListUpdateRequest getRequest() {
            return this.request;
        }

        public /* synthetic */ ListUpdateOperation(ListUpdateRequest listUpdateRequest, DefaultConstructorMarker defaultConstructorMarker) {
            this(listUpdateRequest);
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u000e\b\u0082\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0015\u001a\u00020\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0017\u001a\u0004\b\u0018\u0010\nR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "", "", "component1", "()Ljava/lang/String;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "component2", "()Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "", "component3", "()Z", "listId", "rows", "forceOverwrite", "copy", "(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Z)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getForceOverwrite", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "getRows", "Ljava/lang/String;", "getListId", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ListUpdateRequest {
        private final boolean forceOverwrite;
        private final String listId;
        private final WidgetChannelMembersListViewModel.MemberList rows;

        public ListUpdateRequest(String str, WidgetChannelMembersListViewModel.MemberList memberList, boolean z2) {
            m.checkNotNullParameter(str, "listId");
            m.checkNotNullParameter(memberList, "rows");
            this.listId = str;
            this.rows = memberList;
            this.forceOverwrite = z2;
        }

        public static /* synthetic */ ListUpdateRequest copy$default(ListUpdateRequest listUpdateRequest, String str, WidgetChannelMembersListViewModel.MemberList memberList, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = listUpdateRequest.listId;
            }
            if ((i & 2) != 0) {
                memberList = listUpdateRequest.rows;
            }
            if ((i & 4) != 0) {
                z2 = listUpdateRequest.forceOverwrite;
            }
            return listUpdateRequest.copy(str, memberList, z2);
        }

        public final String component1() {
            return this.listId;
        }

        public final WidgetChannelMembersListViewModel.MemberList component2() {
            return this.rows;
        }

        public final boolean component3() {
            return this.forceOverwrite;
        }

        public final ListUpdateRequest copy(String str, WidgetChannelMembersListViewModel.MemberList memberList, boolean z2) {
            m.checkNotNullParameter(str, "listId");
            m.checkNotNullParameter(memberList, "rows");
            return new ListUpdateRequest(str, memberList, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ListUpdateRequest)) {
                return false;
            }
            ListUpdateRequest listUpdateRequest = (ListUpdateRequest) obj;
            return m.areEqual(this.listId, listUpdateRequest.listId) && m.areEqual(this.rows, listUpdateRequest.rows) && this.forceOverwrite == listUpdateRequest.forceOverwrite;
        }

        public final boolean getForceOverwrite() {
            return this.forceOverwrite;
        }

        public final String getListId() {
            return this.listId;
        }

        public final WidgetChannelMembersListViewModel.MemberList getRows() {
            return this.rows;
        }

        public int hashCode() {
            String str = this.listId;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            WidgetChannelMembersListViewModel.MemberList memberList = this.rows;
            if (memberList != null) {
                i = memberList.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.forceOverwrite;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("ListUpdateRequest(listId=");
            R.append(this.listId);
            R.append(", rows=");
            R.append(this.rows);
            R.append(", forceOverwrite=");
            return a.M(R, this.forceOverwrite, ")");
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0015\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rR\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;", "", "Landroid/view/View;", "getCurrentStickyHeaderView", "()Landroid/view/View;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "layoutViews", "(Landroidx/recyclerview/widget/RecyclerView;)V", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "row", "bindStickyHeaderView", "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;)V", "currentStickyHeaderView", "Landroid/view/View;", "Lcom/discord/databinding/WidgetChannelMembersListItemHeaderBinding;", "headerViewBinding", "Lcom/discord/databinding/WidgetChannelMembersListItemHeaderBinding;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;", "onlineOfflineStickyHeader", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;", "Lcom/discord/databinding/WidgetChannelMembersListItemPlaceholderHeaderBinding;", "placeholderHeaderViewBinding", "Lcom/discord/databinding/WidgetChannelMembersListItemPlaceholderHeaderBinding;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;", "roleStickyHeader", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetChannelMembersListItemHeaderBinding;Lcom/discord/databinding/WidgetChannelMembersListItemPlaceholderHeaderBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StickyHeadersManager {
        private View currentStickyHeaderView;
        private final WidgetChannelMembersListItemHeaderBinding headerViewBinding;
        private final ChannelMembersListViewHolderHeader onlineOfflineStickyHeader;
        private final WidgetChannelMembersListItemPlaceholderHeaderBinding placeholderHeaderViewBinding;
        private final ChannelMembersListViewHolderRoleHeader roleStickyHeader;

        public StickyHeadersManager(WidgetChannelMembersListItemHeaderBinding widgetChannelMembersListItemHeaderBinding, WidgetChannelMembersListItemPlaceholderHeaderBinding widgetChannelMembersListItemPlaceholderHeaderBinding) {
            m.checkNotNullParameter(widgetChannelMembersListItemHeaderBinding, "headerViewBinding");
            m.checkNotNullParameter(widgetChannelMembersListItemPlaceholderHeaderBinding, "placeholderHeaderViewBinding");
            this.headerViewBinding = widgetChannelMembersListItemHeaderBinding;
            this.placeholderHeaderViewBinding = widgetChannelMembersListItemPlaceholderHeaderBinding;
            this.roleStickyHeader = new ChannelMembersListViewHolderRoleHeader(widgetChannelMembersListItemHeaderBinding);
            this.onlineOfflineStickyHeader = new ChannelMembersListViewHolderHeader(widgetChannelMembersListItemHeaderBinding);
            this.currentStickyHeaderView = widgetChannelMembersListItemHeaderBinding.a;
        }

        public final void bindStickyHeaderView(Item item) {
            m.checkNotNullParameter(item, "row");
            if (item instanceof Item.RoleHeader) {
                this.roleStickyHeader.bind((Item.RoleHeader) item);
                this.currentStickyHeaderView = this.headerViewBinding.a;
            } else if (item instanceof Item.Header) {
                this.onlineOfflineStickyHeader.bind((Item.Header) item);
                WidgetChannelMembersListItemHeaderBinding widgetChannelMembersListItemHeaderBinding = this.headerViewBinding;
                this.currentStickyHeaderView = widgetChannelMembersListItemHeaderBinding.a;
                RoleIconView roleIconView = widgetChannelMembersListItemHeaderBinding.f2242b;
                m.checkNotNullExpressionValue(roleIconView, "headerViewBinding.channe…ersListItemHeaderRoleIcon");
                roleIconView.setVisibility(8);
            } else if (item instanceof Item.PlaceholderHeader) {
                this.currentStickyHeaderView = this.placeholderHeaderViewBinding.a;
                RoleIconView roleIconView2 = this.headerViewBinding.f2242b;
                m.checkNotNullExpressionValue(roleIconView2, "headerViewBinding.channe…ersListItemHeaderRoleIcon");
                roleIconView2.setVisibility(8);
            } else if (item instanceof Item.AddMember) {
                this.currentStickyHeaderView = null;
                RoleIconView roleIconView3 = this.headerViewBinding.f2242b;
                m.checkNotNullExpressionValue(roleIconView3, "headerViewBinding.channe…ersListItemHeaderRoleIcon");
                roleIconView3.setVisibility(8);
            }
        }

        public final View getCurrentStickyHeaderView() {
            return this.currentStickyHeaderView;
        }

        public final void layoutViews(RecyclerView recyclerView) {
            m.checkNotNullParameter(recyclerView, "recyclerView");
            LinearLayout linearLayout = this.headerViewBinding.a;
            m.checkNotNullExpressionValue(linearLayout, "headerViewBinding.root");
            StickyHeaderItemDecoration.LayoutManager.layoutHeaderView(recyclerView, linearLayout);
            FrameLayout frameLayout = this.placeholderHeaderViewBinding.a;
            m.checkNotNullExpressionValue(frameLayout, "placeholderHeaderViewBinding.root");
            StickyHeaderItemDecoration.LayoutManager.layoutHeaderView(recyclerView, frameLayout);
        }
    }

    /* compiled from: ChannelMembersListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u000b\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "ROLE_HEADER", "PLACEHOLDER_HEADER", "HEADER", "MEMBER", "ADD_MEMBER", "LOADING", "JOIN_LEAVE_THREAD", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ViewType {
        ROLE_HEADER,
        PLACEHOLDER_HEADER,
        HEADER,
        MEMBER,
        ADD_MEMBER,
        LOADING,
        JOIN_LEAVE_THREAD;
        
        public static final Companion Companion = new Companion(null);
        private static final ViewType[] cachedValues = values();

        /* compiled from: ChannelMembersListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType$Companion;", "", "", "ordinal", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;", "fromOrdinal", "(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;", "", "cachedValues", "[Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final ViewType fromOrdinal(int i) {
                return ViewType.cachedValues[i];
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            Item.Header.Type.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[Item.Header.Type.ONLINE.ordinal()] = 1;
            iArr[Item.Header.Type.OFFLINE.ordinal()] = 2;
            iArr[Item.Header.Type.GROUP_DM.ordinal()] = 3;
            ViewType.values();
            int[] iArr2 = new int[7];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[ViewType.HEADER.ordinal()] = 1;
            iArr2[ViewType.ROLE_HEADER.ordinal()] = 2;
            iArr2[ViewType.MEMBER.ordinal()] = 3;
            iArr2[ViewType.LOADING.ordinal()] = 4;
            iArr2[ViewType.PLACEHOLDER_HEADER.ordinal()] = 5;
            iArr2[ViewType.ADD_MEMBER.ordinal()] = 6;
            iArr2[ViewType.JOIN_LEAVE_THREAD.ordinal()] = 7;
        }
    }

    public ChannelMembersListAdapter() {
        PublishSubject<ListUpdateRequest> k0 = PublishSubject.k0();
        this.updatesSubject = k0;
        WidgetChannelMembersListViewModel.MemberList empty = WidgetChannelMembersListViewModel.MemberList.Companion.getEMPTY();
        this.memberList = empty;
        ListUpdateOperation.OverwriteUpdate overwriteUpdate = new ListUpdateOperation.OverwriteUpdate(new ListUpdateRequest("", empty, false));
        AnonymousClass1 r1 = AnonymousClass1.INSTANCE;
        m.checkNotNullExpressionValue(k0, "updatesSubject");
        Observable S = ObservableExtensionsKt.computationLatest(k0).P(overwriteUpdate, AnonymousClass2.INSTANCE).S(1);
        m.checkNotNullExpressionValue(S, "updatesSubject\n        .…      })\n        .skip(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(S), ChannelMembersListAdapter.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new AnonymousClass4(), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3());
    }

    public static final /* synthetic */ StickyHeadersManager access$getStickyHeadersManager$p(ChannelMembersListAdapter channelMembersListAdapter) {
        StickyHeadersManager stickyHeadersManager = channelMembersListAdapter.stickyHeadersManager;
        if (stickyHeadersManager == null) {
            m.throwUninitializedPropertyAccessException("stickyHeadersManager");
        }
        return stickyHeadersManager;
    }

    public final Unit dispose() {
        Subscription subscription = this.updatesSubscription;
        if (subscription == null) {
            return null;
        }
        subscription.unsubscribe();
        return Unit.a;
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public View getAndBindHeaderView(int i) {
        Item item = this.memberList.get(i);
        try {
            try {
                StickyHeadersManager stickyHeadersManager = this.stickyHeadersManager;
                if (stickyHeadersManager == null) {
                    m.throwUninitializedPropertyAccessException("stickyHeadersManager");
                }
                stickyHeadersManager.bindStickyHeaderView(item);
                StickyHeadersManager stickyHeadersManager2 = this.stickyHeadersManager;
                if (stickyHeadersManager2 == null) {
                    m.throwUninitializedPropertyAccessException("stickyHeadersManager");
                }
                return stickyHeadersManager2.getCurrentStickyHeaderView();
            } catch (ClassCastException unused) {
                Logger.e$default(AppLog.g, "Failed to cast header", null, null, 6, null);
                StickyHeadersManager stickyHeadersManager3 = this.stickyHeadersManager;
                if (stickyHeadersManager3 == null) {
                    m.throwUninitializedPropertyAccessException("stickyHeadersManager");
                }
                return stickyHeadersManager3.getCurrentStickyHeaderView();
            }
        } catch (Throwable unused2) {
            StickyHeadersManager stickyHeadersManager4 = this.stickyHeadersManager;
            if (stickyHeadersManager4 == null) {
                m.throwUninitializedPropertyAccessException("stickyHeadersManager");
            }
            return stickyHeadersManager4.getCurrentStickyHeaderView();
        }
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public Integer getHeaderPositionForItem(int i) {
        return this.memberList.getHeaderPositionForItem(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.memberList.getSize();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        Item item = this.memberList.get(i);
        if (item instanceof Item.Member) {
            return ((Item.Member) item).getUserId();
        }
        if (item instanceof Item.RoleHeader) {
            return ((Item.RoleHeader) item).getRoleId();
        }
        if ((item instanceof Item.AddMember) || (item instanceof Item.JoinLeaveThread)) {
            return -1L;
        }
        if (item instanceof Item.Header) {
            int ordinal = ((Item.Header) item).getHeaderType().ordinal();
            if (ordinal == 0) {
                return -2L;
            }
            if (ordinal == 1) {
                return -3L;
            }
            if (ordinal == 2) {
                return -4L;
            }
            throw new NoWhenBranchMatchedException();
        } else if (item instanceof Item.PlaceholderHeader) {
            return -5L;
        } else {
            if (item instanceof Item.PlaceholderMember) {
                return (-1) * (i + 6);
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.memberList.get(i).getType().ordinal();
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public boolean isHeader(int i) {
        Item item = this.memberList.get(i);
        return (item instanceof Item.Header) || (item instanceof Item.RoleHeader) || (item instanceof Item.PlaceholderHeader);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        super.onAttachedToRecyclerView(recyclerView);
        WidgetChannelMembersListItemHeaderBinding a = WidgetChannelMembersListItemHeaderBinding.a(LayoutInflater.from(recyclerView.getContext()), recyclerView, false);
        m.checkNotNullExpressionValue(a, "WidgetChannelMembersList…View,\n        false\n    )");
        WidgetChannelMembersListItemPlaceholderHeaderBinding a2 = WidgetChannelMembersListItemPlaceholderHeaderBinding.a(LayoutInflater.from(recyclerView.getContext()), recyclerView, false);
        m.checkNotNullExpressionValue(a2, "WidgetChannelMembersList…View,\n        false\n    )");
        this.stickyHeadersManager = new StickyHeadersManager(a, a2);
        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter$onAttachedToRecyclerView$1
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                ChannelMembersListAdapter.access$getStickyHeadersManager$p(ChannelMembersListAdapter.this).layoutViews(recyclerView);
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        m.checkNotNullParameter(viewHolder, "holder");
        if (viewHolder instanceof ChannelMembersListViewHolderHeader) {
            Item item = this.memberList.get(i);
            Objects.requireNonNull(item, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.Header");
            ((ChannelMembersListViewHolderHeader) viewHolder).bind((Item.Header) item);
        } else if (viewHolder instanceof ChannelMembersListViewHolderMember) {
            Item item2 = this.memberList.get(i);
            Objects.requireNonNull(item2, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.Member");
            Item.Member member = (Item.Member) item2;
            ((ChannelMembersListViewHolderMember) viewHolder).bind(member, new ChannelMembersListAdapter$onBindViewHolder$1(this, member));
        } else if (viewHolder instanceof ChannelMembersListViewHolderLoading) {
            ((ChannelMembersListViewHolderLoading) viewHolder).bind(i);
        } else if (viewHolder instanceof ChannelMembersListViewHolderRoleHeader) {
            Item item3 = this.memberList.get(i);
            Objects.requireNonNull(item3, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.RoleHeader");
            ((ChannelMembersListViewHolderRoleHeader) viewHolder).bind((Item.RoleHeader) item3);
        } else if (viewHolder instanceof ChannelMembersListViewHolderAdd) {
            Item item4 = this.memberList.get(i);
            Objects.requireNonNull(item4, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.AddMember");
            ((ChannelMembersListViewHolderAdd) viewHolder).bind(new ChannelMembersListAdapter$onBindViewHolder$2(this), ((Item.AddMember) item4).getTitle());
        } else if (viewHolder instanceof ChannelMembersListViewHolderJoinLeaveThread) {
            Item item5 = this.memberList.get(i);
            Objects.requireNonNull(item5, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.JoinLeaveThread");
            ((ChannelMembersListViewHolderJoinLeaveThread) viewHolder).bind(new ChannelMembersListAdapter$onBindViewHolder$3(this), ((Item.JoinLeaveThread) item5).isThreadJoined());
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        switch (ViewType.Companion.fromOrdinal(i).ordinal()) {
            case 0:
                WidgetChannelMembersListItemHeaderBinding a = WidgetChannelMembersListItemHeaderBinding.a(from, viewGroup, false);
                m.checkNotNullExpressionValue(a, "WidgetChannelMembersList…(inflater, parent, false)");
                return new ChannelMembersListViewHolderRoleHeader(a);
            case 1:
                WidgetChannelMembersListItemPlaceholderHeaderBinding a2 = WidgetChannelMembersListItemPlaceholderHeaderBinding.a(from, viewGroup, false);
                m.checkNotNullExpressionValue(a2, "WidgetChannelMembersList…(inflater, parent, false)");
                return new ChannelMembersListViewHolderPlaceholderHeader(a2);
            case 2:
                WidgetChannelMembersListItemHeaderBinding a3 = WidgetChannelMembersListItemHeaderBinding.a(from, viewGroup, false);
                m.checkNotNullExpressionValue(a3, "WidgetChannelMembersList…(inflater, parent, false)");
                return new ChannelMembersListViewHolderHeader(a3);
            case 3:
                View inflate = from.inflate(R.layout.widget_channel_members_list_item_user, viewGroup, false);
                int i2 = R.id.channel_members_list_item_avatar;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.channel_members_list_item_avatar);
                if (simpleDraweeView != null) {
                    i2 = R.id.channel_members_list_item_boosted_indicator;
                    ImageView imageView = (ImageView) inflate.findViewById(R.id.channel_members_list_item_boosted_indicator);
                    if (imageView != null) {
                        i2 = R.id.channel_members_list_item_game;
                        SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) inflate.findViewById(R.id.channel_members_list_item_game);
                        if (simpleDraweeSpanTextView != null) {
                            i2 = R.id.channel_members_list_item_group_owner_indicator;
                            ImageView imageView2 = (ImageView) inflate.findViewById(R.id.channel_members_list_item_group_owner_indicator);
                            if (imageView2 != null) {
                                i2 = R.id.channel_members_list_item_name;
                                UsernameView usernameView = (UsernameView) inflate.findViewById(R.id.channel_members_list_item_name);
                                if (usernameView != null) {
                                    i2 = R.id.channel_members_list_item_presence;
                                    StatusView statusView = (StatusView) inflate.findViewById(R.id.channel_members_list_item_presence);
                                    if (statusView != null) {
                                        i2 = R.id.channel_members_list_item_rich_presence_iv;
                                        ImageView imageView3 = (ImageView) inflate.findViewById(R.id.channel_members_list_item_rich_presence_iv);
                                        if (imageView3 != null) {
                                            WidgetChannelMembersListItemUserBinding widgetChannelMembersListItemUserBinding = new WidgetChannelMembersListItemUserBinding((ConstraintLayout) inflate, simpleDraweeView, imageView, simpleDraweeSpanTextView, imageView2, usernameView, statusView, imageView3);
                                            m.checkNotNullExpressionValue(widgetChannelMembersListItemUserBinding, "WidgetChannelMembersList…(inflater, parent, false)");
                                            return new ChannelMembersListViewHolderMember(widgetChannelMembersListItemUserBinding);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
            case 4:
                WidgetChannelMembersListItemAddOrLeaveBinding a4 = WidgetChannelMembersListItemAddOrLeaveBinding.a(from, viewGroup, false);
                m.checkNotNullExpressionValue(a4, "WidgetChannelMembersList…(inflater, parent, false)");
                return new ChannelMembersListViewHolderAdd(a4);
            case 5:
                View inflate2 = from.inflate(R.layout.widget_channel_members_list_item_loading, viewGroup, false);
                int i3 = R.id.blank_item_avatar_placeholder;
                View findViewById = inflate2.findViewById(R.id.blank_item_avatar_placeholder);
                if (findViewById != null) {
                    i3 = R.id.blank_item_username_placeholder;
                    View findViewById2 = inflate2.findViewById(R.id.blank_item_username_placeholder);
                    if (findViewById2 != null) {
                        i3 = R.id.username_placeholder_end_guideline;
                        Guideline guideline = (Guideline) inflate2.findViewById(R.id.username_placeholder_end_guideline);
                        if (guideline != null) {
                            WidgetChannelMembersListItemLoadingBinding widgetChannelMembersListItemLoadingBinding = new WidgetChannelMembersListItemLoadingBinding((ConstraintLayout) inflate2, findViewById, findViewById2, guideline);
                            m.checkNotNullExpressionValue(widgetChannelMembersListItemLoadingBinding, "WidgetChannelMembersList…(inflater, parent, false)");
                            return new ChannelMembersListViewHolderLoading(widgetChannelMembersListItemLoadingBinding);
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i3)));
            case 6:
                WidgetChannelMembersListItemAddOrLeaveBinding a5 = WidgetChannelMembersListItemAddOrLeaveBinding.a(from, viewGroup, false);
                m.checkNotNullExpressionValue(a5, "WidgetChannelMembersList…(inflater, parent, false)");
                return new ChannelMembersListViewHolderJoinLeaveThread(a5);
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public final void setData(String str, WidgetChannelMembersListViewModel.MemberList memberList, boolean z2) {
        m.checkNotNullParameter(str, "listId");
        m.checkNotNullParameter(memberList, "rows");
        PublishSubject<ListUpdateRequest> publishSubject = this.updatesSubject;
        publishSubject.k.onNext(new ListUpdateRequest(str, memberList, z2));
    }

    public final void setOnAddMemberClicked(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "addMember");
        this.onAddMemberClicked = function0;
    }

    public final void setOnJoinLeaveThreadClicked(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "joinLeaveThread");
        this.onJoinLeaveThreadClicked = function0;
    }

    public final void setOnUserClicked(Function1<? super Long, Unit> function1) {
        this.onUserClicked = function1;
    }
}
