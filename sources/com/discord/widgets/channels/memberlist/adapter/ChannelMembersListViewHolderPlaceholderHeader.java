package com.discord.widgets.channels.memberlist.adapter;

import andhook.lib.HookHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChannelMembersListItemPlaceholderHeaderBinding;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelMembersListViewHolderPlaceholderHeader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderPlaceholderHeader;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/databinding/WidgetChannelMembersListItemPlaceholderHeaderBinding;", "binding", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetChannelMembersListItemPlaceholderHeaderBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelMembersListViewHolderPlaceholderHeader extends RecyclerView.ViewHolder {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelMembersListViewHolderPlaceholderHeader(WidgetChannelMembersListItemPlaceholderHeaderBinding widgetChannelMembersListItemPlaceholderHeaderBinding) {
        super(widgetChannelMembersListItemPlaceholderHeaderBinding.a);
        m.checkNotNullParameter(widgetChannelMembersListItemPlaceholderHeaderBinding, "binding");
    }
}
