package com.discord.widgets.channels.memberlist.adapter;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.TextView;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChannelMembersListItemHeaderBinding;
import com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelMembersListViewHolderRoleHeader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;", "data", "", "bind", "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;)V", "Lcom/discord/databinding/WidgetChannelMembersListItemHeaderBinding;", "binding", "Lcom/discord/databinding/WidgetChannelMembersListItemHeaderBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetChannelMembersListItemHeaderBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelMembersListViewHolderRoleHeader extends RecyclerView.ViewHolder {
    private final WidgetChannelMembersListItemHeaderBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelMembersListViewHolderRoleHeader(WidgetChannelMembersListItemHeaderBinding widgetChannelMembersListItemHeaderBinding) {
        super(widgetChannelMembersListItemHeaderBinding.a);
        m.checkNotNullParameter(widgetChannelMembersListItemHeaderBinding, "binding");
        this.binding = widgetChannelMembersListItemHeaderBinding;
    }

    public final void bind(ChannelMembersListAdapter.Item.RoleHeader roleHeader) {
        m.checkNotNullParameter(roleHeader, "data");
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.channelMembersListItemHeaderText");
        textView.setText(roleHeader.getRoleName() + " — " + roleHeader.getMemberCount());
        this.binding.f2242b.setRole(Long.valueOf(roleHeader.getRoleId()), roleHeader.getGuildId());
        ViewCompat.setAccessibilityDelegate(this.binding.c, new AccessibilityDelegateCompat() { // from class: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListViewHolderRoleHeader$bind$1
            @Override // androidx.core.view.AccessibilityDelegateCompat
            public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
                m.checkNotNullParameter(view, "host");
                m.checkNotNullParameter(accessibilityNodeInfoCompat, "info");
                super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
                accessibilityNodeInfoCompat.setHeading(true);
            }
        });
    }
}
