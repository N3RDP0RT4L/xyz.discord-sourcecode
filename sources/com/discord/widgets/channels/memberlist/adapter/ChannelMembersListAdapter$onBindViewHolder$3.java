package com.discord.widgets.channels.memberlist.adapter;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: ChannelMembersListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelMembersListAdapter$onBindViewHolder$3 extends o implements Function0<Unit> {
    public final /* synthetic */ ChannelMembersListAdapter this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelMembersListAdapter$onBindViewHolder$3(ChannelMembersListAdapter channelMembersListAdapter) {
        super(0);
        this.this$0 = channelMembersListAdapter;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Function0 function0;
        function0 = this.this$0.onJoinLeaveThreadClicked;
        if (function0 != null) {
            Unit unit = (Unit) function0.invoke();
        }
    }
}
