package com.discord.widgets.channels.memberlist.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.databinding.WidgetChannelMembersListItemUserBinding;
import com.discord.models.presence.Presence;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.views.StatusView;
import com.discord.views.UsernameView;
import com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: ChannelMembersListViewHolderMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ#\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderMember;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;", "data", "Lkotlin/Function0;", "", "onClicked", "bind", "(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/databinding/WidgetChannelMembersListItemUserBinding;", "binding", "Lcom/discord/databinding/WidgetChannelMembersListItemUserBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetChannelMembersListItemUserBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelMembersListViewHolderMember extends RecyclerView.ViewHolder {
    private final WidgetChannelMembersListItemUserBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelMembersListViewHolderMember(WidgetChannelMembersListItemUserBinding widgetChannelMembersListItemUserBinding) {
        super(widgetChannelMembersListItemUserBinding.a);
        m.checkNotNullParameter(widgetChannelMembersListItemUserBinding, "binding");
        this.binding = widgetChannelMembersListItemUserBinding;
    }

    public final void bind(final ChannelMembersListAdapter.Item.Member member, final Function0<Unit> function0) {
        int i;
        m.checkNotNullParameter(member, "data");
        m.checkNotNullParameter(function0, "onClicked");
        this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListViewHolderMember$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        UsernameView usernameView = this.binding.f;
        UsernameView.c(usernameView, member.getName(), null, false, null, null, 30);
        Integer color = member.getColor();
        if (color != null) {
            i = color.intValue();
        } else {
            m.checkNotNullExpressionValue(usernameView, "this");
            i = ColorCompat.getThemedColor(usernameView, (int) R.attr.primary_000);
        }
        usernameView.setUsernameColor(i);
        int i2 = 0;
        final boolean z2 = member.getPremiumSince() != null;
        UsernameView usernameView2 = this.binding.f;
        boolean isBot = member.isBot();
        Integer tagText = member.getTagText();
        usernameView2.a(isBot, tagText != null ? tagText.intValue() : R.string.bot_tag_bot, member.getTagVerified());
        ImageView imageView = this.binding.e;
        m.checkNotNullExpressionValue(imageView, "binding.channelMembersListItemGroupOwnerIndicator");
        imageView.setVisibility(member.getShowOwnerIndicator() ? 0 : 8);
        ImageView imageView2 = this.binding.c;
        m.checkNotNullExpressionValue(imageView2, "binding.channelMembersListItemBoostedIndicator");
        imageView2.setVisibility(z2 ? 0 : 8);
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.channels.memberlist.adapter.ChannelMembersListViewHolderMember$bind$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CharSequence b2;
                if (z2) {
                    String readableTimeString = TimeUtils.getReadableTimeString(a.x(view, "it", "it.context"), member.getPremiumSince());
                    Context context = view.getContext();
                    Context context2 = view.getContext();
                    m.checkNotNullExpressionValue(context2, "it.context");
                    b2 = b.b(context2, R.string.premium_guild_subscription_tooltip, new Object[]{readableTimeString}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    b.a.d.m.h(context, b2, 0, null, 12);
                }
            }
        });
        this.binding.g.setPresence(member.getPresence());
        StatusView statusView = this.binding.g;
        m.checkNotNullExpressionValue(statusView, "binding.channelMembersListItemPresence");
        statusView.setVisibility(UserUtils.INSTANCE.isStatusVisible(member.getUserFlags(), member.getPresence(), true) ? 0 : 8);
        ImageView imageView3 = this.binding.h;
        m.checkNotNullExpressionValue(imageView3, "binding.channelMembersListItemRichPresenceIv");
        if (!PresenceUtils.INSTANCE.shouldShowRichPresenceIcon(member.getPresence())) {
            i2 = 8;
        }
        imageView3.setVisibility(i2);
        Presence presence = member.getPresence();
        boolean isApplicationStreaming = member.isApplicationStreaming();
        SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.channelMembersListItemGame");
        PresenceUtils.setPresenceText$default(presence, isApplicationStreaming, simpleDraweeSpanTextView, false, !member.getCanDisplayStatusEmoji(), 8, null);
        SimpleDraweeView simpleDraweeView = this.binding.f2244b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.channelMembersListItemAvatar");
        IconUtils.setIcon$default(simpleDraweeView, member.getAvatarUrl(), (int) R.dimen.avatar_size_standard, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
    }
}
