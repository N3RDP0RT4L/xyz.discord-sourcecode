package com.discord.widgets.channels.memberlist;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.utilities.collections.SparseMutableList;
import com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel;
import com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter;
import d0.z.d.m;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import kotlin.Metadata;
/* compiled from: GuildMemberListItemGenerator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0082\b\u0018\u00002\u00020\u0001B=\u0012\n\u0010\u0016\u001a\u00060\bj\u0002`\u0011\u0012\u0006\u0010\u0017\u001a\u00020\u0007\u0012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0004\b)\u0010*J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÂ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001c\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006HÂ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0018\u0010\f\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0007H\u0096\u0002¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000f\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u000e\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u0014\u0010\u0012\u001a\u00060\bj\u0002`\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015JN\u0010\u001a\u001a\u00020\u00002\f\b\u0002\u0010\u0016\u001a\u00060\bj\u0002`\u00112\b\b\u0002\u0010\u0017\u001a\u00020\u00072\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0014\b\u0002\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u0013J\u0010\u0010\u001d\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u001d\u0010\u0015J\u001a\u0010!\u001a\u00020 2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÖ\u0003¢\u0006\u0004\b!\u0010\"R\u001c\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010#R \u0010\u0016\u001a\u00060\bj\u0002`\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010$\u001a\u0004\b%\u0010\u0013R\"\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010&R\u001c\u0010\u0017\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0017\u0010'\u001a\u0004\b(\u0010\u0015¨\u0006+"}, d2 = {"Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "Lcom/discord/utilities/collections/SparseMutableList;", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "component3", "()Lcom/discord/utilities/collections/SparseMutableList;", "Ljava/util/SortedMap;", "", "", "component4", "()Ljava/util/SortedMap;", "index", "get", "(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "itemPosition", "getHeaderPositionForItem", "(I)Ljava/lang/Integer;", "Lcom/discord/primitives/MemberListId;", "component1", "()Ljava/lang/String;", "component2", "()I", "listId", "size", "listItems", "groupIndices", "copy", "(Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;)Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/collections/SparseMutableList;", "Ljava/lang/String;", "getListId", "Ljava/util/SortedMap;", "I", "getSize", HookHelper.constructorName, "(Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildMemberListItems implements WidgetChannelMembersListViewModel.MemberList {
    private final SortedMap<Integer, String> groupIndices;
    private final String listId;
    private final SparseMutableList<ChannelMembersListAdapter.Item> listItems;
    private final int size;

    public GuildMemberListItems(String str, int i, SparseMutableList<ChannelMembersListAdapter.Item> sparseMutableList, SortedMap<Integer, String> sortedMap) {
        m.checkNotNullParameter(str, "listId");
        m.checkNotNullParameter(sparseMutableList, "listItems");
        m.checkNotNullParameter(sortedMap, "groupIndices");
        this.listId = str;
        this.size = i;
        this.listItems = sparseMutableList;
        this.groupIndices = sortedMap;
    }

    private final SparseMutableList<ChannelMembersListAdapter.Item> component3() {
        return this.listItems;
    }

    private final SortedMap<Integer, String> component4() {
        return this.groupIndices;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ GuildMemberListItems copy$default(GuildMemberListItems guildMemberListItems, String str, int i, SparseMutableList sparseMutableList, SortedMap sortedMap, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = guildMemberListItems.getListId();
        }
        if ((i2 & 2) != 0) {
            i = guildMemberListItems.getSize();
        }
        if ((i2 & 4) != 0) {
            sparseMutableList = guildMemberListItems.listItems;
        }
        if ((i2 & 8) != 0) {
            sortedMap = guildMemberListItems.groupIndices;
        }
        return guildMemberListItems.copy(str, i, sparseMutableList, sortedMap);
    }

    public final String component1() {
        return getListId();
    }

    public final int component2() {
        return getSize();
    }

    public final GuildMemberListItems copy(String str, int i, SparseMutableList<ChannelMembersListAdapter.Item> sparseMutableList, SortedMap<Integer, String> sortedMap) {
        m.checkNotNullParameter(str, "listId");
        m.checkNotNullParameter(sparseMutableList, "listItems");
        m.checkNotNullParameter(sortedMap, "groupIndices");
        return new GuildMemberListItems(str, i, sparseMutableList, sortedMap);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildMemberListItems)) {
            return false;
        }
        GuildMemberListItems guildMemberListItems = (GuildMemberListItems) obj;
        return m.areEqual(getListId(), guildMemberListItems.getListId()) && getSize() == guildMemberListItems.getSize() && m.areEqual(this.listItems, guildMemberListItems.listItems) && m.areEqual(this.groupIndices, guildMemberListItems.groupIndices);
    }

    @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
    public ChannelMembersListAdapter.Item get(int i) {
        List list;
        List list2;
        ChannelMembersListAdapter.Item item = this.listItems.get(i);
        if (item != null) {
            return item;
        }
        list = GuildMemberListItemGeneratorKt.PLACEHOLDER_INSTANCES;
        list2 = GuildMemberListItemGeneratorKt.PLACEHOLDER_INSTANCES;
        return (ChannelMembersListAdapter.Item) list.get(i % list2.size());
    }

    @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
    public Integer getHeaderPositionForItem(int i) {
        Set<Integer> keySet = this.groupIndices.keySet();
        m.checkNotNullExpressionValue(keySet, "groupIndices.keys");
        Object obj = null;
        for (Object obj2 : keySet) {
            boolean z2 = true;
            if (((Integer) obj2).intValue() >= i + 1) {
                z2 = false;
            }
            if (z2) {
                obj = obj2;
            }
        }
        return (Integer) obj;
    }

    @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
    public String getListId() {
        return this.listId;
    }

    @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
    public int getSize() {
        return this.size;
    }

    public int hashCode() {
        String listId = getListId();
        int i = 0;
        int size = (getSize() + ((listId != null ? listId.hashCode() : 0) * 31)) * 31;
        SparseMutableList<ChannelMembersListAdapter.Item> sparseMutableList = this.listItems;
        int hashCode = (size + (sparseMutableList != null ? sparseMutableList.hashCode() : 0)) * 31;
        SortedMap<Integer, String> sortedMap = this.groupIndices;
        if (sortedMap != null) {
            i = sortedMap.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("GuildMemberListItems(listId=");
        R.append(getListId());
        R.append(", size=");
        R.append(getSize());
        R.append(", listItems=");
        R.append(this.listItems);
        R.append(", groupIndices=");
        R.append(this.groupIndices);
        R.append(")");
        return R.toString();
    }
}
