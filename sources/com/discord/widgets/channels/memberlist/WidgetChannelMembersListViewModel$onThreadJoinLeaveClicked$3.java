package com.discord.widgets.channels.memberlist;

import com.discord.utilities.error.Error;
import com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: WidgetChannelMembersListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelMembersListViewModel$onThreadJoinLeaveClicked$3 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ WidgetChannelMembersListViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelMembersListViewModel$onThreadJoinLeaveClicked$3(WidgetChannelMembersListViewModel widgetChannelMembersListViewModel) {
        super(1);
        this.this$0 = widgetChannelMembersListViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        PublishSubject publishSubject;
        m.checkNotNullParameter(error, "error");
        publishSubject = this.this$0.eventSubject;
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        publishSubject.k.onNext(new WidgetChannelMembersListViewModel.Event.Error(response.getCode()));
    }
}
