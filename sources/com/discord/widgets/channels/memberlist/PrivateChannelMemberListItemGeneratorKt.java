package com.discord.widgets.channels.memberlist;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelRecipientNick;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel;
import com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: PrivateChannelMemberListItemGenerator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001ae\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0001\u001a\u00020\u00002\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00070\u00022\u0016\u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\t0\u00022\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000f\u001a;\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00020\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0012\u001a\u00020\u000b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0015\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0017\u0010\u0018\u001a\u0017\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0004\b\u001c\u0010\u001d¨\u0006\u001e"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "users", "Lcom/discord/models/presence/Presence;", "presences", "Lcom/discord/models/domain/ModelApplicationStream;", "applicationStreams", "", "canInvite", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "generateGroupDmMemberListItems", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "user", "presence", "isOwner", "", ModelAuditLogEntry.CHANGE_KEY_NICK, "isApplicationStreaming", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;", "createMemberListItem", "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;ZLjava/lang/String;Z)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;", "", "memberCount", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;", "createGroupDmHeader", "(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PrivateChannelMemberListItemGeneratorKt {
    private static final ChannelMembersListAdapter.Item.Header createGroupDmHeader(int i) {
        return new ChannelMembersListAdapter.Item.Header("%group_header_key", ChannelMembersListAdapter.Item.Header.Type.GROUP_DM, i);
    }

    private static final ChannelMembersListAdapter.Item.Member createMemberListItem(User user, Presence presence, boolean z2, String str, boolean z3) {
        return new ChannelMembersListAdapter.Item.Member(user.getId(), null, str != null ? str : user.getUsername(), user.isBot(), Integer.valueOf(user.isSystemUser() ? R.string.system_dm_tag_system : R.string.bot_tag_bot), UserUtils.INSTANCE.isVerifiedBot(user), presence, null, IconUtils.getForUser$default(user, false, null, 6, null), z2, null, z3, true, user.getFlags() | user.getPublicFlags());
    }

    public static final WidgetChannelMembersListViewModel.MemberList generateGroupDmMemberListItems(Channel channel, Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3, boolean z2) {
        Object obj;
        boolean z3;
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(map, "users");
        m.checkNotNullParameter(map2, "presences");
        m.checkNotNullParameter(map3, "applicationStreams");
        final PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1 privateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1 = PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1.INSTANCE;
        Object obj2 = privateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;
        if (privateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1 != null) {
            obj2 = new Comparator() { // from class: com.discord.widgets.channels.memberlist.PrivateChannelMemberListItemGeneratorKt$sam$java_util_Comparator$0
                @Override // java.util.Comparator
                public final /* synthetic */ int compare(Object obj3, Object obj4) {
                    Object invoke = Function2.this.invoke(obj3, obj4);
                    m.checkNotNullExpressionValue(invoke, "invoke(...)");
                    return ((Number) invoke).intValue();
                }
            };
        }
        TreeMap treeMap = new TreeMap((Comparator) obj2);
        Iterator<T> it = map.values().iterator();
        while (true) {
            boolean z4 = false;
            if (!it.hasNext()) {
                break;
            }
            User user = (User) it.next();
            StringBuilder sb = new StringBuilder();
            String username = user.getUsername();
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            Objects.requireNonNull(username, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = username.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            sb.append(lowerCase);
            sb.append(UserUtils.INSTANCE.getDiscriminatorWithPadding(user));
            String sb2 = sb.toString();
            List<ChannelRecipientNick> n = channel.n();
            String str = null;
            if (n != null) {
                Iterator<T> it2 = n.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it2.next();
                    if (((ChannelRecipientNick) obj).b() == user.getId()) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        break;
                    }
                }
                ChannelRecipientNick channelRecipientNick = (ChannelRecipientNick) obj;
                if (channelRecipientNick != null) {
                    str = channelRecipientNick.c();
                }
            }
            if (channel.q() == user.getId()) {
                z4 = true;
            }
            treeMap.put(sb2, createMemberListItem(user, (Presence) a.e(user, map2), z4, str, map3.containsKey(Long.valueOf(user.getId()))));
        }
        int size = map.size();
        ArrayList arrayList = new ArrayList(size + 1);
        if (z2) {
            arrayList.add(0, new ChannelMembersListAdapter.Item.AddMember(String.valueOf(channel.h()), ChannelUtils.p(channel) ? R.string.group_dm_add_friends : R.string.create_group_dm));
        }
        arrayList.add(createGroupDmHeader(size));
        arrayList.addAll(treeMap.values());
        return new PrivateChannelMemberListItems(String.valueOf(channel.h()), arrayList);
    }
}
