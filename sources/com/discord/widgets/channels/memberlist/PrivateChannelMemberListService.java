package com.discord.widgets.channels.memberlist;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserPresence;
import com.discord.widgets.channels.memberlist.PrivateChannelMemberListService;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
/* compiled from: PrivateChannelMemberListService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u0013B%\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b\u0012\b\b\u0002\u0010\t\u001a\u00020\b¢\u0006\u0004\b\u0011\u0010\u0012J\u001b\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;", "observeStateForGroupDm", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "Lcom/discord/stores/StoreApplicationStreaming;", "storeApplicationStreaming", "Lcom/discord/stores/StoreApplicationStreaming;", "Lcom/discord/stores/StoreUserPresence;", "storePresences", "Lcom/discord/stores/StoreUserPresence;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreApplicationStreaming;)V", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PrivateChannelMemberListService {
    private final StoreApplicationStreaming storeApplicationStreaming;
    private final StoreUserPresence storePresences;
    private final StoreUser storeUser;

    /* compiled from: PrivateChannelMemberListService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001BW\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u0005\u0012\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\r0\u0005¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\nJ \u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\r0\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJh\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u0018\b\u0002\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\u0018\b\u0002\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u00052\u0018\b\u0002\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\r0\u0005HÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001d\u001a\u00020\u001c2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0004R)\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\nR)\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u000b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b#\u0010\nR)\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\r0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b$\u0010\n¨\u0006'"}, d2 = {"Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component2", "()Ljava/util/Map;", "Lcom/discord/models/presence/Presence;", "component3", "Lcom/discord/models/domain/ModelApplicationStream;", "component4", "channel", "users", "presences", "applicationStreams", "copy", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/Map;", "getUsers", "getPresences", "getApplicationStreams", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class State {
        private final Map<Long, ModelApplicationStream> applicationStreams;
        private final Channel channel;
        private final Map<Long, Presence> presences;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public State(Channel channel, Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "presences");
            m.checkNotNullParameter(map3, "applicationStreams");
            this.channel = channel;
            this.users = map;
            this.presences = map2;
            this.applicationStreams = map3;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ State copy$default(State state, Channel channel, Map map, Map map2, Map map3, int i, Object obj) {
            if ((i & 1) != 0) {
                channel = state.channel;
            }
            if ((i & 2) != 0) {
                map = state.users;
            }
            if ((i & 4) != 0) {
                map2 = state.presences;
            }
            if ((i & 8) != 0) {
                map3 = state.applicationStreams;
            }
            return state.copy(channel, map, map2, map3);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final Map<Long, User> component2() {
            return this.users;
        }

        public final Map<Long, Presence> component3() {
            return this.presences;
        }

        public final Map<Long, ModelApplicationStream> component4() {
            return this.applicationStreams;
        }

        public final State copy(Channel channel, Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "presences");
            m.checkNotNullParameter(map3, "applicationStreams");
            return new State(channel, map, map2, map3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return m.areEqual(this.channel, state.channel) && m.areEqual(this.users, state.users) && m.areEqual(this.presences, state.presences) && m.areEqual(this.applicationStreams, state.applicationStreams);
        }

        public final Map<Long, ModelApplicationStream> getApplicationStreams() {
            return this.applicationStreams;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Map<Long, Presence> getPresences() {
            return this.presences;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            Map<Long, User> map = this.users;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Presence> map2 = this.presences;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, ModelApplicationStream> map3 = this.applicationStreams;
            if (map3 != null) {
                i = map3.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("State(channel=");
            R.append(this.channel);
            R.append(", users=");
            R.append(this.users);
            R.append(", presences=");
            R.append(this.presences);
            R.append(", applicationStreams=");
            return a.L(R, this.applicationStreams, ")");
        }
    }

    public PrivateChannelMemberListService() {
        this(null, null, null, 7, null);
    }

    public PrivateChannelMemberListService(StoreUser storeUser, StoreUserPresence storeUserPresence, StoreApplicationStreaming storeApplicationStreaming) {
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeUserPresence, "storePresences");
        m.checkNotNullParameter(storeApplicationStreaming, "storeApplicationStreaming");
        this.storeUser = storeUser;
        this.storePresences = storeUserPresence;
        this.storeApplicationStreaming = storeApplicationStreaming;
    }

    public final Observable<State> observeStateForGroupDm(final Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        Observable<State> Y = Observable.H(this.storeUser.observeMeId().Z(1).c0(5000L, TimeUnit.MILLISECONDS), Observable.A(channel.w()).F(PrivateChannelMemberListService$observeStateForGroupDm$1.INSTANCE)).f0().Y(new b<List<Long>, Observable<? extends State>>() { // from class: com.discord.widgets.channels.memberlist.PrivateChannelMemberListService$observeStateForGroupDm$2
            public final Observable<? extends PrivateChannelMemberListService.State> call(List<Long> list) {
                StoreUser storeUser;
                StoreUserPresence storeUserPresence;
                StoreApplicationStreaming storeApplicationStreaming;
                storeUser = PrivateChannelMemberListService.this.storeUser;
                m.checkNotNullExpressionValue(list, "ids");
                Observable<Map<Long, User>> observeUsers = storeUser.observeUsers(list);
                storeUserPresence = PrivateChannelMemberListService.this.storePresences;
                Observable<Map<Long, Presence>> observePresencesForUsers = storeUserPresence.observePresencesForUsers(list);
                storeApplicationStreaming = PrivateChannelMemberListService.this.storeApplicationStreaming;
                return Observable.i(observeUsers, observePresencesForUsers, storeApplicationStreaming.observeStreamsByUser(), new Func3<Map<Long, ? extends User>, Map<Long, ? extends Presence>, Map<Long, ? extends ModelApplicationStream>, PrivateChannelMemberListService.State>() { // from class: com.discord.widgets.channels.memberlist.PrivateChannelMemberListService$observeStateForGroupDm$2.1
                    @Override // rx.functions.Func3
                    public /* bridge */ /* synthetic */ PrivateChannelMemberListService.State call(Map<Long, ? extends User> map, Map<Long, ? extends Presence> map2, Map<Long, ? extends ModelApplicationStream> map3) {
                        return call2(map, (Map<Long, Presence>) map2, map3);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final PrivateChannelMemberListService.State call2(Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3) {
                        Channel channel2 = channel;
                        m.checkNotNullExpressionValue(map, "users");
                        m.checkNotNullExpressionValue(map2, "presences");
                        m.checkNotNullExpressionValue(map3, "applicationStreams");
                        return new PrivateChannelMemberListService.State(channel2, map, map2, map3);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "Observable.merge(\n      …            }\n          }");
        return Y;
    }

    public /* synthetic */ PrivateChannelMemberListService(StoreUser storeUser, StoreUserPresence storeUserPresence, StoreApplicationStreaming storeApplicationStreaming, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 2) != 0 ? StoreStream.Companion.getPresences() : storeUserPresence, (i & 4) != 0 ? StoreStream.Companion.getApplicationStreaming() : storeApplicationStreaming);
    }
}
