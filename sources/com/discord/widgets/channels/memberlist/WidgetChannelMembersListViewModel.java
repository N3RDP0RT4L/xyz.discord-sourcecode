package com.discord.widgets.channels.memberlist;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreChannelMembers;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreEmojiCustom;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadMembers;
import com.discord.stores.StoreThreadsJoined;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserPresence;
import com.discord.stores.StoreUserRelationships;
import com.discord.utilities.lazy.memberlist.ChannelMemberList;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.channels.memberlist.PrivateChannelMemberListService;
import com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel;
import com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func5;
import rx.functions.Func9;
import rx.subjects.PublishSubject;
/* compiled from: WidgetChannelMembersListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 02\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0006012345B5\u0012\b\b\u0002\u0010!\u001a\u00020 \u0012\b\b\u0002\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010$\u001a\u00020#\u0012\u000e\b\u0002\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00030\b¢\u0006\u0004\b.\u0010/J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\u000f\u001a\u00020\u00052\b\u0010\f\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0011H\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u001e\u001a\u00020\u00052\n\u0010\u001b\u001a\u00060\u0019j\u0002`\u001a2\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R:\u0010+\u001a&\u0012\f\u0012\n **\u0004\u0018\u00010\t0\t **\u0012\u0012\f\u0012\n **\u0004\u0018\u00010\t0\t\u0018\u00010)0)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,¨\u00066"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;)V", "Lrx/Observable;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;", "observeEvents", "()Lrx/Observable;", "prevViewState", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;", "newViewState", "updateSubscriptions", "(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;)V", "Lkotlin/ranges/IntRange;", "range", "updateSubscriptionsForChannel", "(Lkotlin/ranges/IntRange;)V", "Lcom/discord/api/channel/Channel;", "channel", "updateSubscriptionsForThread", "(Lcom/discord/api/channel/Channel;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "isThreadJoined", "onThreadJoinLeaveClicked", "(JZ)V", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelSubscriber;", "guildChannelSubscriber", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelSubscriber;", "Lcom/discord/stores/StoreEmojiCustom;", "storeCustomEmojis", "Lcom/discord/stores/StoreEmojiCustom;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelSubscriber;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreEmojiCustom;Lrx/Observable;)V", "Companion", "Event", "GuildChannelSubscriber", "MemberList", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelMembersListViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final GuildChannelSubscriber guildChannelSubscriber;
    private final StoreEmojiCustom storeCustomEmojis;
    private final StoreGuilds storeGuilds;

    /* compiled from: WidgetChannelMembersListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetChannelMembersListViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetChannelMembersListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b \u0010!J\u0097\u0001\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c2\b\b\u0002\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\r\u001a\u00020\f2\b\b\u0002\u0010\u000f\u001a\u00020\u000e2\b\b\u0002\u0010\u0011\u001a\u00020\u00102\b\b\u0002\u0010\u0013\u001a\u00020\u00122\b\b\u0002\u0010\u0015\u001a\u00020\u00142\b\b\u0002\u0010\u0017\u001a\u00020\u00162\b\b\u0002\u0010\u0019\u001a\u00020\u00182\b\b\u0002\u0010\u001b\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001e\u0010\u001f¨\u0006\""}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Companion;", "", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreChannelMembers;", "storeChannelMembers", "Lcom/discord/stores/StoreThreadMembers;", "storeThreadMembers", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;", "privateChannelMemberListService", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUserPresence;", "storePresence", "Lcom/discord/stores/StoreApplicationStreaming;", "storeApplicationStreaming", "Lcom/discord/stores/StoreThreadsJoined;", "storeThreadsJoined", "Lrx/Observable;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannelMembers;Lcom/discord/stores/StoreThreadMembers;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreThreadsJoined;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStoreState(final StoreGuilds storeGuilds, final StoreChannelMembers storeChannelMembers, final StoreThreadMembers storeThreadMembers, StoreChannelsSelected storeChannelsSelected, StoreNavigation storeNavigation, final PrivateChannelMemberListService privateChannelMemberListService, final StorePermissions storePermissions, final StoreUserRelationships storeUserRelationships, final StoreChannels storeChannels, final StoreUser storeUser, final StoreUserPresence storeUserPresence, final StoreApplicationStreaming storeApplicationStreaming, final StoreThreadsJoined storeThreadsJoined) {
            final Observable<R> F = storeNavigation.observeRightPanelState().F(WidgetChannelMembersListViewModel$Companion$observeStoreState$isPanelOpenObservable$1.INSTANCE);
            Observable Y = storeChannelsSelected.observeSelectedChannel().Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$Companion$observeStoreState$1

                /* compiled from: WidgetChannelMembersListViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "isPanelOpen", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$Companion$observeStoreState$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1<T, R> implements b<Boolean, WidgetChannelMembersListViewModel.StoreState.None> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    public final WidgetChannelMembersListViewModel.StoreState.None call(Boolean bool) {
                        m.checkNotNullExpressionValue(bool, "isPanelOpen");
                        return new WidgetChannelMembersListViewModel.StoreState.None(bool.booleanValue());
                    }
                }

                public final Observable<? extends WidgetChannelMembersListViewModel.StoreState> call(final Channel channel) {
                    if (channel == null) {
                        return Observable.this.F(AnonymousClass1.INSTANCE);
                    }
                    if (ChannelUtils.x(channel)) {
                        return Observable.j(privateChannelMemberListService.observeStateForGroupDm(channel), Observable.this, new Func2<PrivateChannelMemberListService.State, Boolean, WidgetChannelMembersListViewModel.StoreState.Private>() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$Companion$observeStoreState$1.2
                            public final WidgetChannelMembersListViewModel.StoreState.Private call(PrivateChannelMemberListService.State state, Boolean bool) {
                                m.checkNotNullExpressionValue(bool, "isPanelOpen");
                                return new WidgetChannelMembersListViewModel.StoreState.Private(bool.booleanValue(), state.getChannel(), state.getUsers(), state.getPresences(), state.getApplicationStreams(), storeUserRelationships.getRelationships());
                            }
                        });
                    }
                    if (ChannelUtils.C(channel)) {
                        return (Observable<R>) storeChannels.observeChannel(channel.r()).Y(new b<Channel, Observable<? extends WidgetChannelMembersListViewModel.StoreState>>() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$Companion$observeStoreState$1.3

                            /* compiled from: WidgetChannelMembersListViewModel.kt */
                            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "isPanelOpen", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                            /* renamed from: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$Companion$observeStoreState$1$3$1  reason: invalid class name */
                            /* loaded from: classes2.dex */
                            public static final class AnonymousClass1<T, R> implements b<Boolean, WidgetChannelMembersListViewModel.StoreState.None> {
                                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                                public final WidgetChannelMembersListViewModel.StoreState.None call(Boolean bool) {
                                    m.checkNotNullExpressionValue(bool, "isPanelOpen");
                                    return new WidgetChannelMembersListViewModel.StoreState.None(bool.booleanValue());
                                }
                            }

                            public final Observable<? extends WidgetChannelMembersListViewModel.StoreState> call(final Channel channel2) {
                                if (channel2 == null) {
                                    return Observable.this.F(AnonymousClass1.INSTANCE);
                                }
                                if (ChannelUtils.i(channel2)) {
                                    Observable<Long> observePermissionsForChannel = storePermissions.observePermissionsForChannel(channel2.h());
                                    Observable<ChannelMemberList> observeChannelMemberList = storeChannelMembers.observeChannelMemberList(channel2.f(), channel2.h());
                                    WidgetChannelMembersListViewModel$Companion$observeStoreState$1 widgetChannelMembersListViewModel$Companion$observeStoreState$1 = WidgetChannelMembersListViewModel$Companion$observeStoreState$1.this;
                                    return Observable.g(observePermissionsForChannel, observeChannelMemberList, Observable.this, storeGuilds.observeRoles(channel.f()), storeGuilds.observeGuild(channel.f()), new Func5<Long, ChannelMemberList, Boolean, Map<Long, ? extends GuildRole>, Guild, WidgetChannelMembersListViewModel.StoreState.Guild>() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.Companion.observeStoreState.1.3.2
                                        @Override // rx.functions.Func5
                                        public /* bridge */ /* synthetic */ WidgetChannelMembersListViewModel.StoreState.Guild call(Long l, ChannelMemberList channelMemberList, Boolean bool, Map<Long, ? extends GuildRole> map, Guild guild) {
                                            return call2(l, channelMemberList, bool, (Map<Long, GuildRole>) map, guild);
                                        }

                                        /* renamed from: call  reason: avoid collision after fix types in other method */
                                        public final WidgetChannelMembersListViewModel.StoreState.Guild call2(Long l, ChannelMemberList channelMemberList, Boolean bool, Map<Long, GuildRole> map, Guild guild) {
                                            m.checkNotNullExpressionValue(bool, "isPanelOpen");
                                            boolean booleanValue = bool.booleanValue();
                                            Channel channel3 = Channel.this;
                                            m.checkNotNullExpressionValue(channelMemberList, "channelMemberList");
                                            m.checkNotNullExpressionValue(map, "roles");
                                            return new WidgetChannelMembersListViewModel.StoreState.Guild(booleanValue, channel3, guild, channelMemberList, l, map);
                                        }
                                    });
                                }
                                Observable<Set<Long>> observeThreadMembers = storeThreadMembers.observeThreadMembers(channel.h());
                                WidgetChannelMembersListViewModel$Companion$observeStoreState$1 widgetChannelMembersListViewModel$Companion$observeStoreState$12 = WidgetChannelMembersListViewModel$Companion$observeStoreState$1.this;
                                return Observable.c(observeThreadMembers, Observable.this, storeGuilds.observeRoles(channel.f()), storeGuilds.observeGuildMembers(channel.f()), storeGuilds.observeGuild(channel.f()), storeUser.observeAllUsers(), storeUserPresence.observeAllPresences(), storeApplicationStreaming.observeStreamsByUser(), storeThreadsJoined.observeJoinedThread(channel.h()), new Func9<Set<? extends Long>, Boolean, Map<Long, ? extends GuildRole>, Map<Long, ? extends GuildMember>, Guild, Map<Long, ? extends User>, Map<Long, ? extends Presence>, Map<Long, ? extends ModelApplicationStream>, StoreThreadsJoined.JoinedThread, WidgetChannelMembersListViewModel.StoreState.Thread>() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.Companion.observeStoreState.1.3.3
                                    @Override // rx.functions.Func9
                                    public /* bridge */ /* synthetic */ WidgetChannelMembersListViewModel.StoreState.Thread call(Set<? extends Long> set, Boolean bool, Map<Long, ? extends GuildRole> map, Map<Long, ? extends GuildMember> map2, Guild guild, Map<Long, ? extends User> map3, Map<Long, ? extends Presence> map4, Map<Long, ? extends ModelApplicationStream> map5, StoreThreadsJoined.JoinedThread joinedThread) {
                                        return call2((Set<Long>) set, bool, (Map<Long, GuildRole>) map, (Map<Long, GuildMember>) map2, guild, map3, (Map<Long, Presence>) map4, map5, joinedThread);
                                    }

                                    /* renamed from: call  reason: avoid collision after fix types in other method */
                                    public final WidgetChannelMembersListViewModel.StoreState.Thread call2(Set<Long> set, Boolean bool, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Guild guild, Map<Long, ? extends User> map3, Map<Long, Presence> map4, Map<Long, ? extends ModelApplicationStream> map5, StoreThreadsJoined.JoinedThread joinedThread) {
                                        m.checkNotNullExpressionValue(bool, "isPanelOpen");
                                        boolean booleanValue = bool.booleanValue();
                                        Channel channel3 = channel;
                                        m.checkNotNullExpressionValue(map, "roles");
                                        m.checkNotNullExpressionValue(map2, "guildMembers");
                                        m.checkNotNullExpressionValue(map3, "users");
                                        m.checkNotNullExpressionValue(map4, "presences");
                                        m.checkNotNullExpressionValue(map5, "streams");
                                        m.checkNotNullExpressionValue(set, "threadMembers");
                                        return new WidgetChannelMembersListViewModel.StoreState.Thread(booleanValue, channel3, guild, map, map2, map3, map4, map5, set, joinedThread);
                                    }
                                });
                            }
                        });
                    }
                    return Observable.g(storeGuilds.observeGuild(channel.f()), storeGuilds.observeRoles(channel.f()), storePermissions.observePermissionsForChannel(channel.h()), storeChannelMembers.observeChannelMemberList(channel.f(), channel.h()), Observable.this, new Func5<Guild, Map<Long, ? extends GuildRole>, Long, ChannelMemberList, Boolean, WidgetChannelMembersListViewModel.StoreState.Guild>() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$Companion$observeStoreState$1.4
                        @Override // rx.functions.Func5
                        public /* bridge */ /* synthetic */ WidgetChannelMembersListViewModel.StoreState.Guild call(Guild guild, Map<Long, ? extends GuildRole> map, Long l, ChannelMemberList channelMemberList, Boolean bool) {
                            return call2(guild, (Map<Long, GuildRole>) map, l, channelMemberList, bool);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final WidgetChannelMembersListViewModel.StoreState.Guild call2(Guild guild, Map<Long, GuildRole> map, Long l, ChannelMemberList channelMemberList, Boolean bool) {
                            m.checkNotNullExpressionValue(bool, "isPanelOpen");
                            boolean booleanValue = bool.booleanValue();
                            Channel channel2 = Channel.this;
                            m.checkNotNullExpressionValue(channelMemberList, "channelMemberList");
                            m.checkNotNullExpressionValue(map, "guildRoles");
                            return new WidgetChannelMembersListViewModel.StoreState.Guild(booleanValue, channel2, guild, channelMemberList, l, map);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "storeChannelsSelected\n  …            }\n          }");
            return Y;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, StoreGuilds storeGuilds, StoreChannelMembers storeChannelMembers, StoreThreadMembers storeThreadMembers, StoreChannelsSelected storeChannelsSelected, StoreNavigation storeNavigation, PrivateChannelMemberListService privateChannelMemberListService, StorePermissions storePermissions, StoreUserRelationships storeUserRelationships, StoreChannels storeChannels, StoreUser storeUser, StoreUserPresence storeUserPresence, StoreApplicationStreaming storeApplicationStreaming, StoreThreadsJoined storeThreadsJoined, int i, Object obj) {
            return companion.observeStoreState((i & 1) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 2) != 0 ? StoreStream.Companion.getChannelMembers() : storeChannelMembers, (i & 4) != 0 ? StoreStream.Companion.getThreadMembers() : storeThreadMembers, (i & 8) != 0 ? StoreStream.Companion.getChannelsSelected() : storeChannelsSelected, (i & 16) != 0 ? StoreStream.Companion.getNavigation() : storeNavigation, (i & 32) != 0 ? new PrivateChannelMemberListService(null, null, null, 7, null) : privateChannelMemberListService, (i & 64) != 0 ? StoreStream.Companion.getPermissions() : storePermissions, (i & 128) != 0 ? StoreStream.Companion.getUserRelationships() : storeUserRelationships, (i & 256) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 512) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 1024) != 0 ? StoreStream.Companion.getPresences() : storeUserPresence, (i & 2048) != 0 ? StoreStream.Companion.getApplicationStreaming() : storeApplicationStreaming, (i & 4096) != 0 ? StoreStream.Companion.getThreadsJoined() : storeThreadsJoined);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelMembersListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;", "", HookHelper.constructorName, "()V", "Error", "ScrollToTop", "UpdateRanges", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$ScrollToTop;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$UpdateRanges;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$Error;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;", "", "component1", "()I", ModelAuditLogEntry.CHANGE_KEY_CODE, "copy", "(I)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$Error;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getCode", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends Event {
            private final int code;

            public Error(int i) {
                super(null);
                this.code = i;
            }

            public static /* synthetic */ Error copy$default(Error error, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = error.code;
                }
                return error.copy(i);
            }

            public final int component1() {
                return this.code;
            }

            public final Error copy(int i) {
                return new Error(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Error) && this.code == ((Error) obj).code;
                }
                return true;
            }

            public final int getCode() {
                return this.code;
            }

            public int hashCode() {
                return this.code;
            }

            public String toString() {
                return a.A(a.R("Error(code="), this.code, ")");
            }
        }

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$ScrollToTop;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ScrollToTop extends Event {
            public static final ScrollToTop INSTANCE = new ScrollToTop();

            private ScrollToTop() {
                super(null);
            }
        }

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$UpdateRanges;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UpdateRanges extends Event {
            public static final UpdateRanges INSTANCE = new UpdateRanges();

            private UpdateRanges() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelMembersListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H&¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelSubscriber;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lkotlin/ranges/IntRange;", "range", "", "subscribeToChannelRange", "(Lcom/discord/api/channel/Channel;Lkotlin/ranges/IntRange;)V", "subscribeToThread", "(Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface GuildChannelSubscriber {
        void subscribeToChannelRange(Channel channel, IntRange intRange);

        void subscribeToThread(Channel channel);
    }

    /* compiled from: WidgetChannelMembersListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\b\bf\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012J\u0018\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H¦\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\b\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0007\u001a\u00020\u0002H&¢\u0006\u0004\b\b\u0010\tR\u001a\u0010\u000e\u001a\u00060\nj\u0002`\u000b8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0016\u0010\u0011\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "", "", "index", "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "get", "(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;", "itemPosition", "getHeaderPositionForItem", "(I)Ljava/lang/Integer;", "", "Lcom/discord/primitives/MemberListId;", "getListId", "()Ljava/lang/String;", "listId", "getSize", "()I", "size", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface MemberList {
        public static final Companion Companion = Companion.$$INSTANCE;

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList$Companion;", "", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "EMPTY", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "getEMPTY", "()Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            public static final /* synthetic */ Companion $$INSTANCE = new Companion();
            private static final MemberList EMPTY = new MemberList() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel$MemberList$Companion$EMPTY$1
                private final String listId = "";
                private final int size;

                @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
                public ChannelMembersListAdapter.Item get(int i) {
                    throw new IllegalStateException("");
                }

                @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
                public Integer getHeaderPositionForItem(int i) {
                    return 0;
                }

                @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
                public String getListId() {
                    return this.listId;
                }

                @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.MemberList
                public int getSize() {
                    return this.size;
                }
            };

            private Companion() {
            }

            public final MemberList getEMPTY() {
                return EMPTY;
            }
        }

        ChannelMembersListAdapter.Item get(int i);

        Integer getHeaderPositionForItem(int i);

        String getListId();

        int getSize();
    }

    /* compiled from: WidgetChannelMembersListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\r\u000e\u000f\u0010B\u001b\b\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u000b\u0010\fR\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\b\u0010\n\u0082\u0001\u0004\u0011\u0012\u0013\u0014¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", "", "isPanelOpen", "Z", "()Z", HookHelper.constructorName, "(ZLcom/discord/api/channel/Channel;)V", "Guild", "None", "Private", "Thread", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Thread;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {
        private final Channel channel;
        private final boolean isPanelOpen;

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001BQ\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0005\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u001a\u001a\u00020\u000b\u0012\u000e\u0010\u001b\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u0012¢\u0006\u0004\b4\u00105J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0018\u0010\u0010\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u0012HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016Jf\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00052\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u001a\u001a\u00020\u000b2\u0010\b\u0002\u0010\u001b\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f2\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u0012HÆ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010'\u001a\u00020\u00022\b\u0010&\u001a\u0004\u0018\u00010%HÖ\u0003¢\u0006\u0004\b'\u0010(R)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010)\u001a\u0004\b*\u0010\u0016R\u0019\u0010\u001a\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010+\u001a\u0004\b,\u0010\rR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010-\u001a\u0004\b.\u0010\nR\u001c\u0010\u0017\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0017\u0010/\u001a\u0004\b\u0017\u0010\u0004R!\u0010\u001b\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00100\u001a\u0004\b1\u0010\u0011R\u001c\u0010\u0018\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u00102\u001a\u0004\b3\u0010\u0007¨\u00066"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "", "component1", "()Z", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "component4", "()Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "", "Lcom/discord/api/permission/PermissionBit;", "component5", "()Ljava/lang/Long;", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component6", "()Ljava/util/Map;", "isPanelOpen", "channel", "guild", "channelMembers", "channelPermissions", "guildRoles", "copy", "(ZLcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;Ljava/lang/Long;Ljava/util/Map;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getGuildRoles", "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "getChannelMembers", "Lcom/discord/models/guild/Guild;", "getGuild", "Z", "Ljava/lang/Long;", "getChannelPermissions", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(ZLcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;Ljava/lang/Long;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Guild extends StoreState {
            private final Channel channel;
            private final ChannelMemberList channelMembers;
            private final Long channelPermissions;
            private final com.discord.models.guild.Guild guild;
            private final Map<Long, GuildRole> guildRoles;
            private final boolean isPanelOpen;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Guild(boolean z2, Channel channel, com.discord.models.guild.Guild guild, ChannelMemberList channelMemberList, Long l, Map<Long, GuildRole> map) {
                super(z2, channel, null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(channelMemberList, "channelMembers");
                m.checkNotNullParameter(map, "guildRoles");
                this.isPanelOpen = z2;
                this.channel = channel;
                this.guild = guild;
                this.channelMembers = channelMemberList;
                this.channelPermissions = l;
                this.guildRoles = map;
            }

            public static /* synthetic */ Guild copy$default(Guild guild, boolean z2, Channel channel, com.discord.models.guild.Guild guild2, ChannelMemberList channelMemberList, Long l, Map map, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = guild.isPanelOpen();
                }
                if ((i & 2) != 0) {
                    channel = guild.getChannel();
                }
                Channel channel2 = channel;
                if ((i & 4) != 0) {
                    guild2 = guild.guild;
                }
                com.discord.models.guild.Guild guild3 = guild2;
                if ((i & 8) != 0) {
                    channelMemberList = guild.channelMembers;
                }
                ChannelMemberList channelMemberList2 = channelMemberList;
                if ((i & 16) != 0) {
                    l = guild.channelPermissions;
                }
                Long l2 = l;
                Map<Long, GuildRole> map2 = map;
                if ((i & 32) != 0) {
                    map2 = guild.guildRoles;
                }
                return guild.copy(z2, channel2, guild3, channelMemberList2, l2, map2);
            }

            public final boolean component1() {
                return isPanelOpen();
            }

            public final Channel component2() {
                return getChannel();
            }

            public final com.discord.models.guild.Guild component3() {
                return this.guild;
            }

            public final ChannelMemberList component4() {
                return this.channelMembers;
            }

            public final Long component5() {
                return this.channelPermissions;
            }

            public final Map<Long, GuildRole> component6() {
                return this.guildRoles;
            }

            public final Guild copy(boolean z2, Channel channel, com.discord.models.guild.Guild guild, ChannelMemberList channelMemberList, Long l, Map<Long, GuildRole> map) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(channelMemberList, "channelMembers");
                m.checkNotNullParameter(map, "guildRoles");
                return new Guild(z2, channel, guild, channelMemberList, l, map);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Guild)) {
                    return false;
                }
                Guild guild = (Guild) obj;
                return isPanelOpen() == guild.isPanelOpen() && m.areEqual(getChannel(), guild.getChannel()) && m.areEqual(this.guild, guild.guild) && m.areEqual(this.channelMembers, guild.channelMembers) && m.areEqual(this.channelPermissions, guild.channelPermissions) && m.areEqual(this.guildRoles, guild.guildRoles);
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.StoreState
            public Channel getChannel() {
                return this.channel;
            }

            public final ChannelMemberList getChannelMembers() {
                return this.channelMembers;
            }

            public final Long getChannelPermissions() {
                return this.channelPermissions;
            }

            public final com.discord.models.guild.Guild getGuild() {
                return this.guild;
            }

            public final Map<Long, GuildRole> getGuildRoles() {
                return this.guildRoles;
            }

            public int hashCode() {
                boolean isPanelOpen = isPanelOpen();
                if (isPanelOpen) {
                    isPanelOpen = true;
                }
                int i = isPanelOpen ? 1 : 0;
                int i2 = isPanelOpen ? 1 : 0;
                int i3 = i * 31;
                Channel channel = getChannel();
                int i4 = 0;
                int hashCode = (i3 + (channel != null ? channel.hashCode() : 0)) * 31;
                com.discord.models.guild.Guild guild = this.guild;
                int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
                ChannelMemberList channelMemberList = this.channelMembers;
                int hashCode3 = (hashCode2 + (channelMemberList != null ? channelMemberList.hashCode() : 0)) * 31;
                Long l = this.channelPermissions;
                int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
                Map<Long, GuildRole> map = this.guildRoles;
                if (map != null) {
                    i4 = map.hashCode();
                }
                return hashCode4 + i4;
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.StoreState
            public boolean isPanelOpen() {
                return this.isPanelOpen;
            }

            public String toString() {
                StringBuilder R = a.R("Guild(isPanelOpen=");
                R.append(isPanelOpen());
                R.append(", channel=");
                R.append(getChannel());
                R.append(", guild=");
                R.append(this.guild);
                R.append(", channelMembers=");
                R.append(this.channelMembers);
                R.append(", channelPermissions=");
                R.append(this.channelPermissions);
                R.append(", guildRoles=");
                return a.L(R, this.guildRoles, ")");
            }
        }

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0005\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0005\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "", "component1", "()Z", "isPanelOpen", "copy", "(Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class None extends StoreState {
            private final boolean isPanelOpen;

            public None(boolean z2) {
                super(z2, null, null);
                this.isPanelOpen = z2;
            }

            public static /* synthetic */ None copy$default(None none, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = none.isPanelOpen();
                }
                return none.copy(z2);
            }

            public final boolean component1() {
                return isPanelOpen();
            }

            public final None copy(boolean z2) {
                return new None(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof None) && isPanelOpen() == ((None) obj).isPanelOpen();
                }
                return true;
            }

            public int hashCode() {
                boolean isPanelOpen = isPanelOpen();
                if (isPanelOpen) {
                    return 1;
                }
                return isPanelOpen ? 1 : 0;
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.StoreState
            public boolean isPanelOpen() {
                return this.isPanelOpen;
            }

            public String toString() {
                StringBuilder R = a.R("None(isPanelOpen=");
                R.append(isPanelOpen());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B{\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b\u0012\u0016\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000e0\b\u0012\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00100\b\u0012\u001a\u0010\u001a\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\u0012j\u0002`\u00130\b¢\u0006\u0004\b.\u0010/J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J \u0010\f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000e0\bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJ \u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00100\bHÆ\u0003¢\u0006\u0004\b\u0011\u0010\rJ$\u0010\u0014\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\u0012j\u0002`\u00130\bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\rJ\u0090\u0001\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00052\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b2\u0018\b\u0002\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000e0\b2\u0018\b\u0002\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00100\b2\u001c\b\u0002\u0010\u001a\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\u0012j\u0002`\u00130\bHÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010$\u001a\u00020\u00022\b\u0010#\u001a\u0004\u0018\u00010\"HÖ\u0003¢\u0006\u0004\b$\u0010%R)\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\rR)\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000e0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010&\u001a\u0004\b(\u0010\rR-\u0010\u001a\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\u0012j\u0002`\u00130\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010&\u001a\u0004\b)\u0010\rR)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00100\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010&\u001a\u0004\b*\u0010\rR\u001c\u0010\u0016\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010+\u001a\u0004\b,\u0010\u0007R\u001c\u0010\u0015\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0015\u0010-\u001a\u0004\b\u0015\u0010\u0004¨\u00060"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "", "component1", "()Z", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component3", "()Ljava/util/Map;", "Lcom/discord/models/presence/Presence;", "component4", "Lcom/discord/models/domain/ModelApplicationStream;", "component5", "", "Lcom/discord/primitives/RelationshipType;", "component6", "isPanelOpen", "channel", "users", "presences", "applicationStreams", "relationships", "copy", "(ZLcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getUsers", "getPresences", "getRelationships", "getApplicationStreams", "Lcom/discord/api/channel/Channel;", "getChannel", "Z", HookHelper.constructorName, "(ZLcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Private extends StoreState {
            private final Map<Long, ModelApplicationStream> applicationStreams;
            private final Channel channel;
            private final boolean isPanelOpen;
            private final Map<Long, Presence> presences;
            private final Map<Long, Integer> relationships;
            private final Map<Long, User> users;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Private(boolean z2, Channel channel, Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3, Map<Long, Integer> map4) {
                super(z2, channel, null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "users");
                m.checkNotNullParameter(map2, "presences");
                m.checkNotNullParameter(map3, "applicationStreams");
                m.checkNotNullParameter(map4, "relationships");
                this.isPanelOpen = z2;
                this.channel = channel;
                this.users = map;
                this.presences = map2;
                this.applicationStreams = map3;
                this.relationships = map4;
            }

            public static /* synthetic */ Private copy$default(Private r4, boolean z2, Channel channel, Map map, Map map2, Map map3, Map map4, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = r4.isPanelOpen();
                }
                if ((i & 2) != 0) {
                    channel = r4.getChannel();
                }
                Channel channel2 = channel;
                Map<Long, User> map5 = map;
                if ((i & 4) != 0) {
                    map5 = r4.users;
                }
                Map map6 = map5;
                Map<Long, Presence> map7 = map2;
                if ((i & 8) != 0) {
                    map7 = r4.presences;
                }
                Map map8 = map7;
                Map<Long, ModelApplicationStream> map9 = map3;
                if ((i & 16) != 0) {
                    map9 = r4.applicationStreams;
                }
                Map map10 = map9;
                Map<Long, Integer> map11 = map4;
                if ((i & 32) != 0) {
                    map11 = r4.relationships;
                }
                return r4.copy(z2, channel2, map6, map8, map10, map11);
            }

            public final boolean component1() {
                return isPanelOpen();
            }

            public final Channel component2() {
                return getChannel();
            }

            public final Map<Long, User> component3() {
                return this.users;
            }

            public final Map<Long, Presence> component4() {
                return this.presences;
            }

            public final Map<Long, ModelApplicationStream> component5() {
                return this.applicationStreams;
            }

            public final Map<Long, Integer> component6() {
                return this.relationships;
            }

            public final Private copy(boolean z2, Channel channel, Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3, Map<Long, Integer> map4) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "users");
                m.checkNotNullParameter(map2, "presences");
                m.checkNotNullParameter(map3, "applicationStreams");
                m.checkNotNullParameter(map4, "relationships");
                return new Private(z2, channel, map, map2, map3, map4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Private)) {
                    return false;
                }
                Private r3 = (Private) obj;
                return isPanelOpen() == r3.isPanelOpen() && m.areEqual(getChannel(), r3.getChannel()) && m.areEqual(this.users, r3.users) && m.areEqual(this.presences, r3.presences) && m.areEqual(this.applicationStreams, r3.applicationStreams) && m.areEqual(this.relationships, r3.relationships);
            }

            public final Map<Long, ModelApplicationStream> getApplicationStreams() {
                return this.applicationStreams;
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.StoreState
            public Channel getChannel() {
                return this.channel;
            }

            public final Map<Long, Presence> getPresences() {
                return this.presences;
            }

            public final Map<Long, Integer> getRelationships() {
                return this.relationships;
            }

            public final Map<Long, User> getUsers() {
                return this.users;
            }

            public int hashCode() {
                boolean isPanelOpen = isPanelOpen();
                if (isPanelOpen) {
                    isPanelOpen = true;
                }
                int i = isPanelOpen ? 1 : 0;
                int i2 = isPanelOpen ? 1 : 0;
                int i3 = i * 31;
                Channel channel = getChannel();
                int i4 = 0;
                int hashCode = (i3 + (channel != null ? channel.hashCode() : 0)) * 31;
                Map<Long, User> map = this.users;
                int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
                Map<Long, Presence> map2 = this.presences;
                int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
                Map<Long, ModelApplicationStream> map3 = this.applicationStreams;
                int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
                Map<Long, Integer> map4 = this.relationships;
                if (map4 != null) {
                    i4 = map4.hashCode();
                }
                return hashCode4 + i4;
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.StoreState
            public boolean isPanelOpen() {
                return this.isPanelOpen;
            }

            public String toString() {
                StringBuilder R = a.R("Private(isPanelOpen=");
                R.append(isPanelOpen());
                R.append(", channel=");
                R.append(getChannel());
                R.append(", users=");
                R.append(this.users);
                R.append(", presences=");
                R.append(this.presences);
                R.append(", applicationStreams=");
                R.append(this.applicationStreams);
                R.append(", relationships=");
                return a.L(R, this.relationships, ")");
            }
        }

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001Bµ\u0001\u0012\u0006\u0010 \u001a\u00020\u0002\u0012\u0006\u0010!\u001a\u00020\u0005\u0012\b\u0010\"\u001a\u0004\u0018\u00010\b\u0012\u0016\u0010#\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000b\u0012\u0016\u0010$\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000b\u0012\u0016\u0010%\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00140\u000b\u0012\u0016\u0010&\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00160\u000b\u0012\u0016\u0010'\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00180\u000b\u0012\u0010\u0010(\u001a\f\u0012\b\u0012\u00060\fj\u0002`\u00110\u001a\u0012\b\u0010)\u001a\u0004\u0018\u00010\u001d¢\u0006\u0004\bE\u0010FJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J \u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000bHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0010J \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00140\u000bHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0010J \u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00160\u000bHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0010J \u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00180\u000bHÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0010J\u001a\u0010\u001b\u001a\f\u0012\b\u0012\u00060\fj\u0002`\u00110\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJÒ\u0001\u0010*\u001a\u00020\u00002\b\b\u0002\u0010 \u001a\u00020\u00022\b\b\u0002\u0010!\u001a\u00020\u00052\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\b2\u0018\b\u0002\u0010#\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000b2\u0018\b\u0002\u0010$\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000b2\u0018\b\u0002\u0010%\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00140\u000b2\u0018\b\u0002\u0010&\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00160\u000b2\u0018\b\u0002\u0010'\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00180\u000b2\u0012\b\u0002\u0010(\u001a\f\u0012\b\u0012\u00060\fj\u0002`\u00110\u001a2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u001dHÆ\u0001¢\u0006\u0004\b*\u0010+J\u0010\u0010-\u001a\u00020,HÖ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u00100\u001a\u00020/HÖ\u0001¢\u0006\u0004\b0\u00101J\u001a\u00104\u001a\u00020\u00022\b\u00103\u001a\u0004\u0018\u000102HÖ\u0003¢\u0006\u0004\b4\u00105R\u001b\u0010)\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b)\u00106\u001a\u0004\b7\u0010\u001fR)\u0010#\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00108\u001a\u0004\b9\u0010\u0010R)\u0010'\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00180\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u00108\u001a\u0004\b:\u0010\u0010R)\u0010$\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00120\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00108\u001a\u0004\b;\u0010\u0010R\u001c\u0010!\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u0010<\u001a\u0004\b=\u0010\u0007R#\u0010(\u001a\f\u0012\b\u0012\u00060\fj\u0002`\u00110\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010>\u001a\u0004\b?\u0010\u001cR)\u0010&\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00160\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00108\u001a\u0004\b@\u0010\u0010R\u001b\u0010\"\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010A\u001a\u0004\bB\u0010\nR)\u0010%\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\u0011\u0012\u0004\u0012\u00020\u00140\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u00108\u001a\u0004\bC\u0010\u0010R\u001c\u0010 \u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u0010D\u001a\u0004\b \u0010\u0004¨\u0006G"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Thread;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;", "", "component1", "()Z", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component4", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "component5", "Lcom/discord/models/user/User;", "component6", "Lcom/discord/models/presence/Presence;", "component7", "Lcom/discord/models/domain/ModelApplicationStream;", "component8", "", "component9", "()Ljava/util/Set;", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "component10", "()Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "isPanelOpen", "channel", "guild", "roles", "guildMembers", "users", "presences", "streams", "threadMembers", "joinedThread", "copy", "(ZLcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/discord/stores/StoreThreadsJoined$JoinedThread;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Thread;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "getJoinedThread", "Ljava/util/Map;", "getRoles", "getStreams", "getGuildMembers", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/Set;", "getThreadMembers", "getPresences", "Lcom/discord/models/guild/Guild;", "getGuild", "getUsers", "Z", HookHelper.constructorName, "(ZLcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/discord/stores/StoreThreadsJoined$JoinedThread;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Thread extends StoreState {
            private final Channel channel;
            private final com.discord.models.guild.Guild guild;
            private final Map<Long, GuildMember> guildMembers;
            private final boolean isPanelOpen;
            private final StoreThreadsJoined.JoinedThread joinedThread;
            private final Map<Long, Presence> presences;
            private final Map<Long, GuildRole> roles;
            private final Map<Long, ModelApplicationStream> streams;
            private final Set<Long> threadMembers;
            private final Map<Long, User> users;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Thread(boolean z2, Channel channel, com.discord.models.guild.Guild guild, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Map<Long, ? extends User> map3, Map<Long, Presence> map4, Map<Long, ? extends ModelApplicationStream> map5, Set<Long> set, StoreThreadsJoined.JoinedThread joinedThread) {
                super(z2, channel, null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "roles");
                m.checkNotNullParameter(map2, "guildMembers");
                m.checkNotNullParameter(map3, "users");
                m.checkNotNullParameter(map4, "presences");
                m.checkNotNullParameter(map5, "streams");
                m.checkNotNullParameter(set, "threadMembers");
                this.isPanelOpen = z2;
                this.channel = channel;
                this.guild = guild;
                this.roles = map;
                this.guildMembers = map2;
                this.users = map3;
                this.presences = map4;
                this.streams = map5;
                this.threadMembers = set;
                this.joinedThread = joinedThread;
            }

            public final boolean component1() {
                return isPanelOpen();
            }

            public final StoreThreadsJoined.JoinedThread component10() {
                return this.joinedThread;
            }

            public final Channel component2() {
                return getChannel();
            }

            public final com.discord.models.guild.Guild component3() {
                return this.guild;
            }

            public final Map<Long, GuildRole> component4() {
                return this.roles;
            }

            public final Map<Long, GuildMember> component5() {
                return this.guildMembers;
            }

            public final Map<Long, User> component6() {
                return this.users;
            }

            public final Map<Long, Presence> component7() {
                return this.presences;
            }

            public final Map<Long, ModelApplicationStream> component8() {
                return this.streams;
            }

            public final Set<Long> component9() {
                return this.threadMembers;
            }

            public final Thread copy(boolean z2, Channel channel, com.discord.models.guild.Guild guild, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Map<Long, ? extends User> map3, Map<Long, Presence> map4, Map<Long, ? extends ModelApplicationStream> map5, Set<Long> set, StoreThreadsJoined.JoinedThread joinedThread) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(map, "roles");
                m.checkNotNullParameter(map2, "guildMembers");
                m.checkNotNullParameter(map3, "users");
                m.checkNotNullParameter(map4, "presences");
                m.checkNotNullParameter(map5, "streams");
                m.checkNotNullParameter(set, "threadMembers");
                return new Thread(z2, channel, guild, map, map2, map3, map4, map5, set, joinedThread);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Thread)) {
                    return false;
                }
                Thread thread = (Thread) obj;
                return isPanelOpen() == thread.isPanelOpen() && m.areEqual(getChannel(), thread.getChannel()) && m.areEqual(this.guild, thread.guild) && m.areEqual(this.roles, thread.roles) && m.areEqual(this.guildMembers, thread.guildMembers) && m.areEqual(this.users, thread.users) && m.areEqual(this.presences, thread.presences) && m.areEqual(this.streams, thread.streams) && m.areEqual(this.threadMembers, thread.threadMembers) && m.areEqual(this.joinedThread, thread.joinedThread);
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.StoreState
            public Channel getChannel() {
                return this.channel;
            }

            public final com.discord.models.guild.Guild getGuild() {
                return this.guild;
            }

            public final Map<Long, GuildMember> getGuildMembers() {
                return this.guildMembers;
            }

            public final StoreThreadsJoined.JoinedThread getJoinedThread() {
                return this.joinedThread;
            }

            public final Map<Long, Presence> getPresences() {
                return this.presences;
            }

            public final Map<Long, GuildRole> getRoles() {
                return this.roles;
            }

            public final Map<Long, ModelApplicationStream> getStreams() {
                return this.streams;
            }

            public final Set<Long> getThreadMembers() {
                return this.threadMembers;
            }

            public final Map<Long, User> getUsers() {
                return this.users;
            }

            public int hashCode() {
                boolean isPanelOpen = isPanelOpen();
                if (isPanelOpen) {
                    isPanelOpen = true;
                }
                int i = isPanelOpen ? 1 : 0;
                int i2 = isPanelOpen ? 1 : 0;
                int i3 = i * 31;
                Channel channel = getChannel();
                int i4 = 0;
                int hashCode = (i3 + (channel != null ? channel.hashCode() : 0)) * 31;
                com.discord.models.guild.Guild guild = this.guild;
                int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
                Map<Long, GuildRole> map = this.roles;
                int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
                Map<Long, GuildMember> map2 = this.guildMembers;
                int hashCode4 = (hashCode3 + (map2 != null ? map2.hashCode() : 0)) * 31;
                Map<Long, User> map3 = this.users;
                int hashCode5 = (hashCode4 + (map3 != null ? map3.hashCode() : 0)) * 31;
                Map<Long, Presence> map4 = this.presences;
                int hashCode6 = (hashCode5 + (map4 != null ? map4.hashCode() : 0)) * 31;
                Map<Long, ModelApplicationStream> map5 = this.streams;
                int hashCode7 = (hashCode6 + (map5 != null ? map5.hashCode() : 0)) * 31;
                Set<Long> set = this.threadMembers;
                int hashCode8 = (hashCode7 + (set != null ? set.hashCode() : 0)) * 31;
                StoreThreadsJoined.JoinedThread joinedThread = this.joinedThread;
                if (joinedThread != null) {
                    i4 = joinedThread.hashCode();
                }
                return hashCode8 + i4;
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.StoreState
            public boolean isPanelOpen() {
                return this.isPanelOpen;
            }

            public String toString() {
                StringBuilder R = a.R("Thread(isPanelOpen=");
                R.append(isPanelOpen());
                R.append(", channel=");
                R.append(getChannel());
                R.append(", guild=");
                R.append(this.guild);
                R.append(", roles=");
                R.append(this.roles);
                R.append(", guildMembers=");
                R.append(this.guildMembers);
                R.append(", users=");
                R.append(this.users);
                R.append(", presences=");
                R.append(this.presences);
                R.append(", streams=");
                R.append(this.streams);
                R.append(", threadMembers=");
                R.append(this.threadMembers);
                R.append(", joinedThread=");
                R.append(this.joinedThread);
                R.append(")");
                return R.toString();
            }
        }

        private StoreState(boolean z2, Channel channel) {
            this.isPanelOpen = z2;
            this.channel = channel;
        }

        public Channel getChannel() {
            return this.channel;
        }

        public boolean isPanelOpen() {
            return this.isPanelOpen;
        }

        public /* synthetic */ StoreState(boolean z2, Channel channel, DefaultConstructorMarker defaultConstructorMarker) {
            this(z2, channel);
        }
    }

    /* compiled from: WidgetChannelMembersListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0015\b\u0002\u0012\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\b\u0010\tR\u001d\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007\u0082\u0001\u0002\f\r¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;", "", "", "Lcom/discord/primitives/MemberListId;", "listId", "Ljava/lang/String;", "getListId", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "Empty", "Loaded", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {
        private final String listId;

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Empty extends ViewState {
            public static final Empty INSTANCE = new Empty();

            private Empty() {
                super("empty", null);
            }
        }

        /* compiled from: WidgetChannelMembersListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u000f\u001a\u00020\u0005¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J:\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u000f\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00052\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\r\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u000f\u0010\u0007R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\nR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001f\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "component1", "()Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "", "component2", "()Z", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "component4", "listItems", "isOpen", "channel", "isThreadJoined", "copy", "(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;ZLcom/discord/api/channel/Channel;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;", "getListItems", HookHelper.constructorName, "(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;ZLcom/discord/api/channel/Channel;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final Channel channel;
            private final boolean isOpen;
            private final boolean isThreadJoined;
            private final MemberList listItems;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(MemberList memberList, boolean z2, Channel channel, boolean z3) {
                super(memberList.getListId(), null);
                m.checkNotNullParameter(memberList, "listItems");
                this.listItems = memberList;
                this.isOpen = z2;
                this.channel = channel;
                this.isThreadJoined = z3;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, MemberList memberList, boolean z2, Channel channel, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    memberList = loaded.listItems;
                }
                if ((i & 2) != 0) {
                    z2 = loaded.isOpen;
                }
                if ((i & 4) != 0) {
                    channel = loaded.channel;
                }
                if ((i & 8) != 0) {
                    z3 = loaded.isThreadJoined;
                }
                return loaded.copy(memberList, z2, channel, z3);
            }

            public final MemberList component1() {
                return this.listItems;
            }

            public final boolean component2() {
                return this.isOpen;
            }

            public final Channel component3() {
                return this.channel;
            }

            public final boolean component4() {
                return this.isThreadJoined;
            }

            public final Loaded copy(MemberList memberList, boolean z2, Channel channel, boolean z3) {
                m.checkNotNullParameter(memberList, "listItems");
                return new Loaded(memberList, z2, channel, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.listItems, loaded.listItems) && this.isOpen == loaded.isOpen && m.areEqual(this.channel, loaded.channel) && this.isThreadJoined == loaded.isThreadJoined;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final MemberList getListItems() {
                return this.listItems;
            }

            public int hashCode() {
                MemberList memberList = this.listItems;
                int i = 0;
                int hashCode = (memberList != null ? memberList.hashCode() : 0) * 31;
                boolean z2 = this.isOpen;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode + i3) * 31;
                Channel channel = this.channel;
                if (channel != null) {
                    i = channel.hashCode();
                }
                int i6 = (i5 + i) * 31;
                boolean z3 = this.isThreadJoined;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                return i6 + i2;
            }

            public final boolean isOpen() {
                return this.isOpen;
            }

            public final boolean isThreadJoined() {
                return this.isThreadJoined;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(listItems=");
                R.append(this.listItems);
                R.append(", isOpen=");
                R.append(this.isOpen);
                R.append(", channel=");
                R.append(this.channel);
                R.append(", isThreadJoined=");
                return a.M(R, this.isThreadJoined, ")");
            }
        }

        private ViewState(String str) {
            this.listId = str;
        }

        public final String getListId() {
            return this.listId;
        }

        public /* synthetic */ ViewState(String str, DefaultConstructorMarker defaultConstructorMarker) {
            this(str);
        }
    }

    public WidgetChannelMembersListViewModel() {
        this(null, null, null, null, 15, null);
    }

    public /* synthetic */ WidgetChannelMembersListViewModel(GuildChannelSubscriber guildChannelSubscriber, StoreGuilds storeGuilds, StoreEmojiCustom storeEmojiCustom, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        StoreEmojiCustom storeEmojiCustom2;
        Observable observable2;
        WidgetChannelMembersListViewModel widgetChannelMembersListViewModel;
        GuildChannelSubscriber guildChannelSubscriber2 = (i & 1) != 0 ? new GuildChannelSubscriber() { // from class: com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.1
            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.GuildChannelSubscriber
            public void subscribeToChannelRange(Channel channel, IntRange intRange) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(intRange, "range");
                StoreStream.Companion.getGuildSubscriptions().subscribeChannelRange(channel.f(), channel.h(), intRange);
            }

            @Override // com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel.GuildChannelSubscriber
            public void subscribeToThread(Channel channel) {
                m.checkNotNullParameter(channel, "channel");
                StoreStream.Companion.getGuildSubscriptions().subscribeThread(channel.f(), channel.h());
            }
        } : guildChannelSubscriber;
        StoreGuilds guilds = (i & 2) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds;
        StoreEmojiCustom customEmojis = (i & 4) != 0 ? StoreStream.Companion.getCustomEmojis() : storeEmojiCustom;
        if ((i & 8) != 0) {
            storeEmojiCustom2 = customEmojis;
            observable2 = Companion.observeStoreState$default(Companion, guilds, null, null, null, null, null, null, null, null, null, null, null, null, 8190, null);
            widgetChannelMembersListViewModel = this;
        } else {
            widgetChannelMembersListViewModel = this;
            observable2 = observable;
            storeEmojiCustom2 = customEmojis;
        }
        new WidgetChannelMembersListViewModel(guildChannelSubscriber2, guilds, storeEmojiCustom2, observable2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        MemberList memberList;
        if (storeState instanceof StoreState.None) {
            updateViewState(ViewState.Empty.INSTANCE);
            return;
        }
        boolean z2 = false;
        if (storeState instanceof StoreState.Guild) {
            StoreState.Guild guild = (StoreState.Guild) storeState;
            memberList = GuildMemberListItemGeneratorKt.generateGuildMemberListItems(guild.getChannelMembers(), guild.getGuild(), guild.getChannel(), guild.getGuildRoles(), PermissionUtils.can(1L, guild.getChannelPermissions()), false, this.storeGuilds, this.storeCustomEmojis);
        } else if (storeState instanceof StoreState.Private) {
            StoreState.Private r0 = (StoreState.Private) storeState;
            Map<Long, Integer> relationships = r0.getRelationships();
            User a = ChannelUtils.a(r0.getChannel());
            memberList = PrivateChannelMemberListItemGeneratorKt.generateGroupDmMemberListItems(r0.getChannel(), r0.getUsers(), r0.getPresences(), r0.getApplicationStreams(), ChannelUtils.p(r0.getChannel()) || ModelUserRelationship.isType(relationships.get(a != null ? Long.valueOf(a.getId()) : null), 1));
        } else if (storeState instanceof StoreState.Thread) {
            StoreState.Thread thread = (StoreState.Thread) storeState;
            memberList = ThreadMemberListItemGeneratorKt.generateThreadMemberListItems(thread.getChannel(), thread.getRoles(), thread.getGuild(), thread.getGuildMembers(), thread.getUsers(), thread.getPresences(), thread.getStreams(), thread.getThreadMembers(), thread.getJoinedThread(), this.storeGuilds, this.storeCustomEmojis);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        ViewState viewState = getViewState();
        boolean isPanelOpen = storeState.isPanelOpen();
        Channel channel = storeState.getChannel();
        if ((storeState instanceof StoreState.Thread) && ((StoreState.Thread) storeState).getJoinedThread() != null) {
            z2 = true;
        }
        ViewState.Loaded loaded = new ViewState.Loaded(memberList, isPanelOpen, channel, z2);
        updateViewState(loaded);
        updateSubscriptions(viewState, loaded, storeState);
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void onThreadJoinLeaveClicked(long j, boolean z2) {
        if (z2) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().leaveThread(j, "Thread Member List"), false, 1, null), this, null, 2, null), WidgetChannelMembersListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetChannelMembersListViewModel$onThreadJoinLeaveClicked$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetChannelMembersListViewModel$onThreadJoinLeaveClicked$2.INSTANCE);
            StoreNavigation.setNavigationPanelAction$default(StoreStream.Companion.getNavigation(), StoreNavigation.PanelAction.CLOSE, null, 2, null);
            return;
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().joinThread(j, "Thread Member List", new RestAPIParams.EmptyBody()), false, 1, null), WidgetChannelMembersListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetChannelMembersListViewModel$onThreadJoinLeaveClicked$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetChannelMembersListViewModel$onThreadJoinLeaveClicked$4.INSTANCE);
        StoreNavigation.setNavigationPanelAction$default(StoreStream.Companion.getNavigation(), StoreNavigation.PanelAction.CLOSE, null, 2, null);
    }

    public final void updateSubscriptions(ViewState viewState, ViewState.Loaded loaded, StoreState storeState) {
        m.checkNotNullParameter(loaded, "newViewState");
        m.checkNotNullParameter(storeState, "storeState");
        boolean z2 = (m.areEqual(viewState != null ? viewState.getListId() : null, loaded.getListId()) ^ true) && storeState.isPanelOpen();
        Channel channel = loaded.getChannel();
        if (channel == null || !ChannelUtils.C(channel)) {
            if (z2) {
                this.eventSubject.k.onNext(Event.ScrollToTop.INSTANCE);
                updateSubscriptionsForChannel(new IntRange(0, 99));
            } else if ((viewState instanceof ViewState.Loaded) && !((ViewState.Loaded) viewState).isOpen() && loaded.isOpen()) {
                this.eventSubject.k.onNext(Event.UpdateRanges.INSTANCE);
            }
        } else if (z2) {
            this.eventSubject.k.onNext(Event.ScrollToTop.INSTANCE);
            updateSubscriptionsForThread(loaded.getChannel());
        } else if ((viewState instanceof ViewState.Loaded) && !((ViewState.Loaded) viewState).isOpen() && loaded.isOpen()) {
            updateSubscriptionsForThread(loaded.getChannel());
        }
    }

    @MainThread
    public final void updateSubscriptionsForChannel(IntRange intRange) {
        m.checkNotNullParameter(intRange, "range");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && loaded.getChannel() != null && loaded.isOpen()) {
            this.guildChannelSubscriber.subscribeToChannelRange(loaded.getChannel(), intRange);
        }
    }

    public final void updateSubscriptionsForThread(Channel channel) {
        if (channel != null && !ChannelUtils.j(channel)) {
            m.checkNotNullParameter(channel, "$this$isAnnouncementThread");
            if (!(channel.A() == 10)) {
                this.guildChannelSubscriber.subscribeToThread(channel);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelMembersListViewModel(GuildChannelSubscriber guildChannelSubscriber, StoreGuilds storeGuilds, StoreEmojiCustom storeEmojiCustom, Observable<StoreState> observable) {
        super(ViewState.Empty.INSTANCE);
        m.checkNotNullParameter(guildChannelSubscriber, "guildChannelSubscriber");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeEmojiCustom, "storeCustomEmojis");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.guildChannelSubscriber = guildChannelSubscriber;
        this.storeGuilds = storeGuilds;
        this.storeCustomEmojis = storeEmojiCustom;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetChannelMembersListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
