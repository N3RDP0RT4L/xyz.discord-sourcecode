package com.discord.widgets.channels.memberlist;

import androidx.fragment.app.FragmentManager;
import com.discord.widgets.channels.memberlist.WidgetChannelMembersListViewModel;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelMembersList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/UserId;", "userId", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelMembersList$configureLoadedUI$1 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ WidgetChannelMembersListViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ WidgetChannelMembersList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelMembersList$configureLoadedUI$1(WidgetChannelMembersList widgetChannelMembersList, WidgetChannelMembersListViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = widgetChannelMembersList;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        WidgetUserSheet.Companion companion = WidgetUserSheet.Companion;
        Long valueOf = Long.valueOf(this.$viewState.getChannel().h());
        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        WidgetUserSheet.Companion.show$default(companion, j, valueOf, parentFragmentManager, Long.valueOf(this.$viewState.getChannel().f()), null, null, null, 112, null);
    }
}
