package com.discord.widgets.channels;

import androidx.core.app.NotificationCompat;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import j0.k.b;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: WidgetGroupInviteFriends.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0010!\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\b\u0003\u0010\r\u001aê\u0001\u0012n\b\u0001\u0012j\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\bj\u0004\u0018\u0001`\t0\bj\u0002`\t \u0003*4\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\bj\u0004\u0018\u0001`\t0\bj\u0002`\t\u0018\u00010\n0\u0007 \u0003*t\u0012n\b\u0001\u0012j\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\bj\u0004\u0018\u0001`\t0\bj\u0002`\t \u0003*4\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\bj\u0004\u0018\u0001`\t0\bj\u0002`\t\u0018\u00010\n0\u0007\u0018\u00010\u00060\u00062>\u0010\u0005\u001a:\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002 \u0003*\u001c\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0018\u00010\u00040\u0000H\n¢\u0006\u0004\b\u000b\u0010\f"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "kotlin.jvm.PlatformType", "", "excludeUserIds", "Lrx/Observable;", "", "", "Lcom/discord/primitives/RelationshipType;", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2<T, R> implements b<List<Long>, Observable<? extends Map<Long, Integer>>> {
    public final /* synthetic */ String $nameFilter;

    /* compiled from: WidgetGroupInviteFriends.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\b\u0003\u0010\u000b\u001aê\u0001\u0012n\b\u0001\u0012j\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004 \u0005*4\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004\u0018\u00010\u00000\b \u0005*t\u0012n\b\u0001\u0012j\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004 \u0005*4\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\u0016\u0012\u0014 \u0005*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004\u0018\u00010\u00000\b\u0018\u00010\u00070\u000726\u0010\u0006\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004 \u0005*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\t\u0010\n"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "kotlin.jvm.PlatformType", "relationships", "Lrx/Observable;", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T, R> implements b<Map<Long, ? extends Integer>, Observable<? extends Map<Long, Integer>>> {
        public final /* synthetic */ List $excludeUserIds;

        public AnonymousClass1(List list) {
            this.$excludeUserIds = list;
        }

        @Override // j0.k.b
        public /* bridge */ /* synthetic */ Observable<? extends Map<Long, Integer>> call(Map<Long, ? extends Integer> map) {
            return call2((Map<Long, Integer>) map);
        }

        /* renamed from: call  reason: avoid collision after fix types in other method */
        public final Observable<? extends Map<Long, Integer>> call2(final Map<Long, Integer> map) {
            return (Observable<R>) StoreStream.Companion.getUsers().observeUsers(map.keySet()).Y(new b<Map<Long, ? extends User>, Observable<? extends Map<Long, Integer>>>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends.Model.Companion.getFilteredFriends.2.1.1

                /* compiled from: WidgetGroupInviteFriends.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010&\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\t\u001a\u0014 \u0005*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u000226\u0010\u0006\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004 \u0005*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map$Entry;)Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2$1$1$2  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass2<T, R> implements b<Map.Entry<? extends Long, ? extends Integer>, Long> {
                    public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Long call(Map.Entry<? extends Long, ? extends Integer> entry) {
                        return call2((Map.Entry<Long, Integer>) entry);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Long call2(Map.Entry<Long, Integer> entry) {
                        return entry.getKey();
                    }
                }

                /* compiled from: WidgetGroupInviteFriends.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010&\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\t\u001a\u0014 \u0005*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u000426\u0010\u0006\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004 \u0005*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map$Entry;)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2$1$1$3  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass3<T, R> implements b<Map.Entry<? extends Long, ? extends Integer>, Integer> {
                    public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Integer call(Map.Entry<? extends Long, ? extends Integer> entry) {
                        return call2((Map.Entry<Long, Integer>) entry);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Integer call2(Map.Entry<Long, Integer> entry) {
                        return entry.getValue();
                    }
                }

                public final Observable<? extends Map<Long, Integer>> call(final Map<Long, ? extends User> map2) {
                    return Observable.A(map.entrySet()).x(new b<Map.Entry<? extends Long, ? extends Integer>, Boolean>() { // from class: com.discord.widgets.channels.WidgetGroupInviteFriends.Model.Companion.getFilteredFriends.2.1.1.1
                        @Override // j0.k.b
                        public /* bridge */ /* synthetic */ Boolean call(Map.Entry<? extends Long, ? extends Integer> entry) {
                            return call2((Map.Entry<Long, Integer>) entry);
                        }

                        /* JADX WARN: Code restructure failed: missing block: B:15:0x0047, code lost:
                            if (r0 != null) goto L17;
                         */
                        /* JADX WARN: Code restructure failed: missing block: B:19:0x0052, code lost:
                            if (r5 != false) goto L21;
                         */
                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        /*
                            Code decompiled incorrectly, please refer to instructions dump.
                            To view partially-correct add '--show-bad-code' argument
                        */
                        public final java.lang.Boolean call2(java.util.Map.Entry<java.lang.Long, java.lang.Integer> r5) {
                            /*
                                r4 = this;
                                com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2$1$1 r0 = com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2.AnonymousClass1.C02341.this
                                com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2$1 r0 = com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2.AnonymousClass1.this
                                java.util.List r0 = r0.$excludeUserIds
                                java.lang.Object r1 = r5.getKey()
                                boolean r0 = r0.contains(r1)
                                r1 = 1
                                r2 = 0
                                if (r0 != 0) goto L55
                                java.util.Map r0 = r2
                                java.lang.Object r3 = r5.getKey()
                                boolean r0 = r0.containsKey(r3)
                                if (r0 == 0) goto L55
                                java.util.Map r0 = r2
                                if (r0 == 0) goto L51
                                java.lang.Object r5 = r5.getKey()
                                java.lang.Object r5 = r0.get(r5)
                                com.discord.models.user.User r5 = (com.discord.models.user.User) r5
                                if (r5 == 0) goto L51
                                java.lang.String r5 = r5.getUsername()
                                if (r5 == 0) goto L51
                                com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2$1$1 r0 = com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2.AnonymousClass1.C02341.this
                                com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2$1 r0 = com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2.AnonymousClass1.this
                                com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2 r0 = com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2.this
                                java.lang.String r0 = r0.$nameFilter
                                if (r0 == 0) goto L4a
                                java.lang.String r0 = r0.toLowerCase()
                                java.lang.String r3 = "(this as java.lang.String).toLowerCase()"
                                d0.z.d.m.checkNotNullExpressionValue(r0, r3)
                                if (r0 == 0) goto L4a
                                goto L4c
                            L4a:
                                java.lang.String r0 = ""
                            L4c:
                                boolean r5 = d0.g0.w.contains(r5, r0, r1)
                                goto L52
                            L51:
                                r5 = 0
                            L52:
                                if (r5 == 0) goto L55
                                goto L56
                            L55:
                                r1 = 0
                            L56:
                                java.lang.Boolean r5 = java.lang.Boolean.valueOf(r1)
                                return r5
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.channels.WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2.AnonymousClass1.C02341.C02351.call2(java.util.Map$Entry):java.lang.Boolean");
                        }
                    }).g0(AnonymousClass2.INSTANCE, AnonymousClass3.INSTANCE);
                }
            });
        }
    }

    public WidgetGroupInviteFriends$Model$Companion$getFilteredFriends$2(String str) {
        this.$nameFilter = str;
    }

    public final Observable<? extends Map<Long, Integer>> call(List<Long> list) {
        return (Observable<R>) StoreStream.Companion.getUserRelationships().observeForType(1).Y(new AnonymousClass1(list));
    }
}
