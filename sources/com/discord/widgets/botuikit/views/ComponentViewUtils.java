package com.discord.widgets.botuikit.views;

import andhook.lib.HookHelper;
import com.discord.api.botuikit.ComponentEmoji;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.stores.StoreStream;
import com.discord.utilities.images.MGImages;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.s;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ComponentViewUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ#\u0010\b\u001a\u00020\u0007*\u00020\u00022\b\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/botuikit/views/ComponentViewUtils;", "", "Lcom/facebook/drawee/view/SimpleDraweeView;", "Lcom/discord/api/botuikit/ComponentEmoji;", "componentEmoji", "", "emojiAnimationsEnabled", "", "setEmojiOrHide", "(Lcom/facebook/drawee/view/SimpleDraweeView;Lcom/discord/api/botuikit/ComponentEmoji;Z)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ComponentViewUtils {
    public static final ComponentViewUtils INSTANCE = new ComponentViewUtils();

    private ComponentViewUtils() {
    }

    public final void setEmojiOrHide(SimpleDraweeView simpleDraweeView, ComponentEmoji componentEmoji, boolean z2) {
        String str;
        m.checkNotNullParameter(simpleDraweeView, "$this$setEmojiOrHide");
        if (componentEmoji == null) {
            simpleDraweeView.setVisibility(8);
            return;
        }
        boolean z3 = false;
        simpleDraweeView.setVisibility(0);
        String b2 = componentEmoji.b();
        String str2 = null;
        Long longOrNull = b2 != null ? s.toLongOrNull(b2) : null;
        if (longOrNull != null) {
            long longValue = longOrNull.longValue();
            if (z2 && m.areEqual(componentEmoji.a(), Boolean.TRUE)) {
                z3 = true;
            }
            str = ModelEmojiCustom.getImageUri(longValue, z3, 64);
        } else {
            ModelEmojiUnicode modelEmojiUnicode = StoreStream.Companion.getEmojis().getUnicodeEmojiSurrogateMap().get(componentEmoji.c());
            if (modelEmojiUnicode != null) {
                str2 = modelEmojiUnicode.getCodePoints();
            }
            str = ModelEmojiUnicode.getImageUri(str2, simpleDraweeView.getContext());
        }
        if (!m.areEqual(simpleDraweeView.getTag(R.string.tag_emoji_url), str)) {
            simpleDraweeView.setTag(R.string.tag_emoji_url, str);
            MGImages.setImage$default(simpleDraweeView, str, R.dimen.emoji_size, R.dimen.emoji_size, true, null, null, 96, null);
        }
    }
}
