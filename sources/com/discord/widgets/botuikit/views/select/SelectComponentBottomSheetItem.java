package com.discord.widgets.botuikit.views.select;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.botuikit.SelectItem;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: SelectComponentBottomSheetAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetItem;", "", "Lcom/discord/api/botuikit/SelectItem;", "component1", "()Lcom/discord/api/botuikit/SelectItem;", "", "component2", "()Z", "selectItem", "selected", "copy", "(Lcom/discord/api/botuikit/SelectItem;Z)Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/botuikit/SelectItem;", "getSelectItem", "Z", "getSelected", HookHelper.constructorName, "(Lcom/discord/api/botuikit/SelectItem;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectComponentBottomSheetItem {
    private final SelectItem selectItem;
    private final boolean selected;

    public SelectComponentBottomSheetItem(SelectItem selectItem, boolean z2) {
        m.checkNotNullParameter(selectItem, "selectItem");
        this.selectItem = selectItem;
        this.selected = z2;
    }

    public static /* synthetic */ SelectComponentBottomSheetItem copy$default(SelectComponentBottomSheetItem selectComponentBottomSheetItem, SelectItem selectItem, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            selectItem = selectComponentBottomSheetItem.selectItem;
        }
        if ((i & 2) != 0) {
            z2 = selectComponentBottomSheetItem.selected;
        }
        return selectComponentBottomSheetItem.copy(selectItem, z2);
    }

    public final SelectItem component1() {
        return this.selectItem;
    }

    public final boolean component2() {
        return this.selected;
    }

    public final SelectComponentBottomSheetItem copy(SelectItem selectItem, boolean z2) {
        m.checkNotNullParameter(selectItem, "selectItem");
        return new SelectComponentBottomSheetItem(selectItem, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SelectComponentBottomSheetItem)) {
            return false;
        }
        SelectComponentBottomSheetItem selectComponentBottomSheetItem = (SelectComponentBottomSheetItem) obj;
        return m.areEqual(this.selectItem, selectComponentBottomSheetItem.selectItem) && this.selected == selectComponentBottomSheetItem.selected;
    }

    public final SelectItem getSelectItem() {
        return this.selectItem;
    }

    public final boolean getSelected() {
        return this.selected;
    }

    public int hashCode() {
        SelectItem selectItem = this.selectItem;
        int hashCode = (selectItem != null ? selectItem.hashCode() : 0) * 31;
        boolean z2 = this.selected;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("SelectComponentBottomSheetItem(selectItem=");
        R.append(this.selectItem);
        R.append(", selected=");
        return a.M(R, this.selected, ")");
    }
}
