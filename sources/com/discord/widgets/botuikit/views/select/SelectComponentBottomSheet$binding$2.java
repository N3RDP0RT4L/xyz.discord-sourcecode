package com.discord.widgets.botuikit.views.select;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.discord.databinding.WidgetSelectComponentBottomSheetBinding;
import com.discord.utilities.view.recycler.MaxHeightRecyclerView;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: SelectComponentBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetSelectComponentBottomSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetSelectComponentBottomSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class SelectComponentBottomSheet$binding$2 extends k implements Function1<View, WidgetSelectComponentBottomSheetBinding> {
    public static final SelectComponentBottomSheet$binding$2 INSTANCE = new SelectComponentBottomSheet$binding$2();

    public SelectComponentBottomSheet$binding$2() {
        super(1, WidgetSelectComponentBottomSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetSelectComponentBottomSheetBinding;", 0);
    }

    public final WidgetSelectComponentBottomSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.widget_sheet_component_bottom_sheet_header;
        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.widget_sheet_component_bottom_sheet_header);
        if (constraintLayout != null) {
            i = R.id.widget_sheet_component_bottom_sheet_placeholder;
            TextView textView = (TextView) view.findViewById(R.id.widget_sheet_component_bottom_sheet_placeholder);
            if (textView != null) {
                i = R.id.widget_sheet_component_bottom_sheet_recycler;
                MaxHeightRecyclerView maxHeightRecyclerView = (MaxHeightRecyclerView) view.findViewById(R.id.widget_sheet_component_bottom_sheet_recycler);
                if (maxHeightRecyclerView != null) {
                    i = R.id.widget_sheet_component_bottom_sheet_select;
                    TextView textView2 = (TextView) view.findViewById(R.id.widget_sheet_component_bottom_sheet_select);
                    if (textView2 != null) {
                        i = R.id.widget_sheet_component_bottom_sheet_subtitle;
                        TextView textView3 = (TextView) view.findViewById(R.id.widget_sheet_component_bottom_sheet_subtitle);
                        if (textView3 != null) {
                            return new WidgetSelectComponentBottomSheetBinding((ConstraintLayout) view, constraintLayout, textView, maxHeightRecyclerView, textView2, textView3);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
