package com.discord.widgets.botuikit.views.select;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import com.discord.api.botuikit.SelectItem;
import com.discord.app.AppViewModel;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.stores.StoreStream;
import d0.t.m0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: SelectComponentBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010#\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002BCBa\u0012\u0006\u0010:\u001a\u000209\u0012\n\u0010\u001b\u001a\u00060\u0019j\u0002`\u001a\u0012\u0006\u0010\"\u001a\u00020!\u0012\b\u0010>\u001a\u0004\u0018\u00010!\u0012\f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00040(\u0012\u0006\u0010\u001f\u001a\u00020\u0019\u0012\u0006\u0010&\u001a\u00020\u0019\u0012\f\u00106\u001a\b\u0012\u0004\u0012\u00020\u00040(\u0012\u0006\u0010\u0016\u001a\u00020\t¢\u0006\u0004\b@\u0010AJ\u001d\u0010\u0007\u001a\u00020\u00062\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0013\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\tH\u0007¢\u0006\u0004\b\u0010\u0010\u0012J\r\u0010\u0013\u001a\u00020\u0006¢\u0006\u0004\b\u0013\u0010\u0014J\r\u0010\u0015\u001a\u00020\u0006¢\u0006\u0004\b\u0015\u0010\u0014R\u0019\u0010\u0016\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u000bR\u001d\u0010\u001b\u001a\u00060\u0019j\u0002`\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0019\u0010\u001f\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u001c\u001a\u0004\b \u0010\u001eR\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u0019\u0010&\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u001c\u001a\u0004\b'\u0010\u001eR\u001f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00040(8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R:\u0010/\u001a&\u0012\f\u0012\n .*\u0004\u0018\u00010\r0\r .*\u0012\u0012\f\u0012\n .*\u0004\u0018\u00010\r0\r\u0018\u00010-0-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u001f\u00102\u001a\b\u0012\u0004\u0012\u00020\u0004018\u0006@\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105R\u001f\u00106\u001a\b\u0012\u0004\u0012\u00020\u00040(8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010*\u001a\u0004\b7\u0010,R\u0016\u00108\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u0010\u0017R\u0019\u0010:\u001a\u0002098\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u0010=R\u001b\u0010>\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010#\u001a\u0004\b?\u0010%¨\u0006D"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$ViewState;", "", "Lcom/discord/api/botuikit/SelectItem;", "selection", "", "sendSelectInteraction", "(Ljava/util/Set;)V", "", "isValidSelection", "()Z", "Lrx/Observable;", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$Event;", "observeEvents", "()Lrx/Observable;", "selectItem", "selected", "(Lcom/discord/api/botuikit/SelectItem;Z)V", "onClickSelect", "()V", "updateViewState", "emojiAnimationsEnabled", "Z", "getEmojiAnimationsEnabled", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "componentIndex", "I", "getComponentIndex", "()I", "min", "getMin", "", "customId", "Ljava/lang/String;", "getCustomId", "()Ljava/lang/String;", "max", "getMax", "", "items", "Ljava/util/List;", "getItems", "()Ljava/util/List;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "", "selectedItems", "Ljava/util/Set;", "getSelectedItems", "()Ljava/util/Set;", "selectedOptions", "getSelectedOptions", "isMultiSelect", "Lcom/discord/widgets/botuikit/views/select/ComponentContext;", "componentContext", "Lcom/discord/widgets/botuikit/views/select/ComponentContext;", "getComponentContext", "()Lcom/discord/widgets/botuikit/views/select/ComponentContext;", "placeholder", "getPlaceholder", HookHelper.constructorName, "(Lcom/discord/widgets/botuikit/views/select/ComponentContext;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;IILjava/util/List;Z)V", "Event", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectComponentBottomSheetViewModel extends AppViewModel<ViewState> {
    private final ComponentContext componentContext;
    private final int componentIndex;
    private final String customId;
    private final boolean emojiAnimationsEnabled;
    private final PublishSubject<Event> eventSubject;
    private final boolean isMultiSelect;
    private final List<SelectItem> items;
    private final int max;
    private final int min;
    private final String placeholder;
    private final Set<SelectItem> selectedItems;
    private final List<SelectItem> selectedOptions;

    /* compiled from: SelectComponentBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "CloseSheet", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$Event$CloseSheet;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: SelectComponentBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$Event$CloseSheet;", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CloseSheet extends Event {
            public static final CloseSheet INSTANCE = new CloseSheet();

            private CloseSheet() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SelectComponentBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001BO\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\r\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0006\u0010\u000b\u001a\u00020\u0007\u0012\u0006\u0010\u001a\u001a\u00020\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\u001b\u001a\u00020\u0007¢\u0006\u0004\b\u001d\u0010\u001eR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\b\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\t\u001a\u0004\b\f\u0010\nR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0004\u001a\u0004\b\u0019\u0010\u0006R\u0019\u0010\u001a\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\t\u001a\u0004\b\u001a\u0010\nR\u0019\u0010\u001b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\t\u001a\u0004\b\u001c\u0010\n¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$ViewState;", "", "", "minSelections", "I", "getMinSelections", "()I", "", "isValidSelection", "Z", "()Z", "showSelectButton", "getShowSelectButton", "", "title", "Ljava/lang/String;", "getTitle", "()Ljava/lang/String;", "", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetItem;", "items", "Ljava/util/List;", "getItems", "()Ljava/util/List;", "maxSelections", "getMaxSelections", "isMultiSelect", "emojiAnimationsEnabled", "getEmojiAnimationsEnabled", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;ZZIIZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean emojiAnimationsEnabled;
        private final boolean isMultiSelect;
        private final boolean isValidSelection;
        private final List<SelectComponentBottomSheetItem> items;
        private final int maxSelections;
        private final int minSelections;
        private final boolean showSelectButton;
        private final String title;

        public ViewState(String str, List<SelectComponentBottomSheetItem> list, boolean z2, boolean z3, int i, int i2, boolean z4, boolean z5) {
            m.checkNotNullParameter(list, "items");
            this.title = str;
            this.items = list;
            this.showSelectButton = z2;
            this.isMultiSelect = z3;
            this.minSelections = i;
            this.maxSelections = i2;
            this.isValidSelection = z4;
            this.emojiAnimationsEnabled = z5;
        }

        public final boolean getEmojiAnimationsEnabled() {
            return this.emojiAnimationsEnabled;
        }

        public final List<SelectComponentBottomSheetItem> getItems() {
            return this.items;
        }

        public final int getMaxSelections() {
            return this.maxSelections;
        }

        public final int getMinSelections() {
            return this.minSelections;
        }

        public final boolean getShowSelectButton() {
            return this.showSelectButton;
        }

        public final String getTitle() {
            return this.title;
        }

        public final boolean isMultiSelect() {
            return this.isMultiSelect;
        }

        public final boolean isValidSelection() {
            return this.isValidSelection;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectComponentBottomSheetViewModel(ComponentContext componentContext, int i, String str, String str2, List<SelectItem> list, int i2, int i3, List<SelectItem> list2, boolean z2) {
        super(null, 1, null);
        m.checkNotNullParameter(componentContext, "componentContext");
        m.checkNotNullParameter(str, "customId");
        m.checkNotNullParameter(list, "items");
        m.checkNotNullParameter(list2, "selectedOptions");
        boolean z3 = true;
        this.componentContext = componentContext;
        this.componentIndex = i;
        this.customId = str;
        this.placeholder = str2;
        this.items = list;
        this.min = i2;
        this.max = i3;
        this.selectedOptions = list2;
        this.emojiAnimationsEnabled = z2;
        this.isMultiSelect = i3 <= 1 ? false : z3;
        this.selectedItems = u.toMutableSet(list2);
        updateViewState();
        this.eventSubject = PublishSubject.k0();
    }

    private final boolean isValidSelection() {
        int i = this.min;
        int i2 = this.max;
        int size = this.selectedItems.size();
        return i <= size && i2 >= size;
    }

    private final void sendSelectInteraction(Set<SelectItem> set) {
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getLocalActionComponentState().setSelectComponentSelection(this.componentContext.getMessageId(), this.componentIndex, u.toList(set));
        StoreApplicationInteractions interactions = companion.getInteractions();
        long applicationId = this.componentContext.getApplicationId();
        Long guildId = this.componentContext.getGuildId();
        long channelId = this.componentContext.getChannelId();
        long messageId = this.componentContext.getMessageId();
        Long messageFlags = this.componentContext.getMessageFlags();
        int i = this.componentIndex;
        String str = this.customId;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(set, 10));
        for (SelectItem selectItem : set) {
            arrayList.add(selectItem.e());
        }
        interactions.sendComponentInteraction(applicationId, guildId, channelId, messageId, i, new RestAPIParams.ComponentInteractionData.SelectComponentInteractionData(null, str, u.toList(arrayList), 1, null), messageFlags);
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.CloseSheet.INSTANCE);
    }

    public final ComponentContext getComponentContext() {
        return this.componentContext;
    }

    public final int getComponentIndex() {
        return this.componentIndex;
    }

    public final String getCustomId() {
        return this.customId;
    }

    public final boolean getEmojiAnimationsEnabled() {
        return this.emojiAnimationsEnabled;
    }

    public final List<SelectItem> getItems() {
        return this.items;
    }

    public final int getMax() {
        return this.max;
    }

    public final int getMin() {
        return this.min;
    }

    public final String getPlaceholder() {
        return this.placeholder;
    }

    public final Set<SelectItem> getSelectedItems() {
        return this.selectedItems;
    }

    public final List<SelectItem> getSelectedOptions() {
        return this.selectedOptions;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void onClickSelect() {
        sendSelectInteraction(u.toSet(this.selectedItems));
    }

    @MainThread
    public final void selectItem(SelectItem selectItem, boolean z2) {
        m.checkNotNullParameter(selectItem, "selectItem");
        if (!this.isMultiSelect) {
            sendSelectInteraction(m0.setOf(selectItem));
            return;
        }
        if (z2) {
            this.selectedItems.add(selectItem);
        } else {
            this.selectedItems.remove(selectItem);
        }
        updateViewState();
    }

    public final void updateViewState() {
        String str = this.placeholder;
        List<SelectItem> list = this.items;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (SelectItem selectItem : list) {
            arrayList.add(new SelectComponentBottomSheetItem(selectItem, this.selectedItems.contains(selectItem)));
        }
        updateViewState(new ViewState(str, arrayList, this.isMultiSelect, this.isMultiSelect, this.min, this.max, isValidSelection(), this.emojiAnimationsEnabled));
    }
}
