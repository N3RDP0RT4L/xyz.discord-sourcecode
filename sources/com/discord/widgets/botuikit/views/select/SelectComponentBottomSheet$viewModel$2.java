package com.discord.widgets.botuikit.views.select;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.botuikit.views.select.SelectComponentBottomSheetViewModel;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: SelectComponentBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectComponentBottomSheet$viewModel$2 extends o implements Function0<AppViewModel<SelectComponentBottomSheetViewModel.ViewState>> {
    public final /* synthetic */ SelectComponentBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectComponentBottomSheet$viewModel$2(SelectComponentBottomSheet selectComponentBottomSheet) {
        super(0);
        this.this$0 = selectComponentBottomSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<SelectComponentBottomSheetViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        Bundle argumentsOrDefault3;
        Bundle argumentsOrDefault4;
        Bundle argumentsOrDefault5;
        Bundle argumentsOrDefault6;
        Bundle argumentsOrDefault7;
        Bundle argumentsOrDefault8;
        Bundle argumentsOrDefault9;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        Serializable serializable = argumentsOrDefault.getSerializable(SelectComponentBottomSheet.EXTRA_COMPONENT_CONTEXT);
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.widgets.botuikit.views.select.ComponentContext");
        ComponentContext componentContext = (ComponentContext) serializable;
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        int i = argumentsOrDefault2.getInt(SelectComponentBottomSheet.EXTRA_COMPONENT_INDEX);
        argumentsOrDefault3 = this.this$0.getArgumentsOrDefault();
        String string = argumentsOrDefault3.getString(SelectComponentBottomSheet.EXTRA_CUSTOM_ID, "");
        argumentsOrDefault4 = this.this$0.getArgumentsOrDefault();
        String string2 = argumentsOrDefault4.getString(SelectComponentBottomSheet.EXTRA_PLACEHOLDER);
        argumentsOrDefault5 = this.this$0.getArgumentsOrDefault();
        int i2 = argumentsOrDefault5.getInt(SelectComponentBottomSheet.EXTRA_MIN);
        argumentsOrDefault6 = this.this$0.getArgumentsOrDefault();
        int i3 = argumentsOrDefault6.getInt(SelectComponentBottomSheet.EXTRA_MAX);
        argumentsOrDefault7 = this.this$0.getArgumentsOrDefault();
        Serializable serializable2 = argumentsOrDefault7.getSerializable(SelectComponentBottomSheet.EXTRA_OPTIONS);
        List list = null;
        if (!(serializable2 instanceof List)) {
            serializable2 = null;
        }
        List list2 = (List) serializable2;
        if (list2 == null) {
            list2 = n.emptyList();
        }
        List list3 = list2;
        argumentsOrDefault8 = this.this$0.getArgumentsOrDefault();
        Serializable serializable3 = argumentsOrDefault8.getSerializable(SelectComponentBottomSheet.EXTRA_SELECTED);
        if (serializable3 instanceof List) {
            list = serializable3;
        }
        List list4 = list;
        List emptyList = list4 != null ? list4 : n.emptyList();
        argumentsOrDefault9 = this.this$0.getArgumentsOrDefault();
        boolean z2 = argumentsOrDefault9.getBoolean(SelectComponentBottomSheet.EXTRA_EMOJI_ANIMATIONS_ENABLED);
        m.checkNotNullExpressionValue(string, "customId");
        return new SelectComponentBottomSheetViewModel(componentContext, i, string, string2, list3, i2, i3, emptyList, z2);
    }
}
