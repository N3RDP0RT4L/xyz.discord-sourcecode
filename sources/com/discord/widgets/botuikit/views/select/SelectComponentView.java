package com.discord.widgets.botuikit.views.select;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.w4;
import b.a.i.x4;
import com.discord.api.botuikit.ComponentType;
import com.discord.api.botuikit.SelectItem;
import com.discord.models.botuikit.ActionInteractionComponentState;
import com.discord.models.botuikit.SelectMessageComponent;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.typing.TypingDots;
import com.discord.widgets.botuikit.ComponentProvider;
import com.discord.widgets.botuikit.views.ComponentActionListener;
import com.discord.widgets.botuikit.views.ComponentView;
import com.discord.widgets.botuikit.views.ComponentViewUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.textview.MaterialTextView;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: SelectComponentView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \u00182\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002:\u0001\u0018B!\b\u0016\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016B\u0019\b\u0016\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0015\u0010\u0017J'\u0010\n\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\r\u001a\u00020\fH\u0016¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lcom/discord/widgets/botuikit/views/ComponentView;", "Lcom/discord/models/botuikit/SelectMessageComponent;", "component", "Lcom/discord/widgets/botuikit/ComponentProvider;", "componentProvider", "Lcom/discord/widgets/botuikit/views/ComponentActionListener;", "componentActionListener", "", "configure", "(Lcom/discord/models/botuikit/SelectMessageComponent;Lcom/discord/widgets/botuikit/ComponentProvider;Lcom/discord/widgets/botuikit/views/ComponentActionListener;)V", "Lcom/discord/api/botuikit/ComponentType;", "type", "()Lcom/discord/api/botuikit/ComponentType;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyle", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectComponentView extends ConstraintLayout implements ComponentView<SelectMessageComponent> {
    public static final Companion Companion = new Companion(null);

    /* compiled from: SelectComponentView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentView$Companion;", "", "Landroid/content/Context;", "context", "Landroid/view/ViewGroup;", "root", "Lcom/discord/widgets/botuikit/views/select/SelectComponentView;", "inflateComponent", "(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/discord/widgets/botuikit/views/select/SelectComponentView;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final SelectComponentView inflateComponent(Context context, ViewGroup viewGroup) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(viewGroup, "root");
            w4 a = w4.a(LayoutInflater.from(context).inflate(R.layout.widget_chat_list_bot_ui_select_component, viewGroup, false));
            m.checkNotNullExpressionValue(a, "WidgetChatListBotUiSelec…om(context), root, false)");
            SelectComponentView selectComponentView = a.a;
            m.checkNotNullExpressionValue(selectComponentView, "WidgetChatListBotUiSelec…ntext), root, false).root");
            return selectComponentView;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectComponentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
    }

    @Override // com.discord.widgets.botuikit.views.ComponentView
    public ComponentType type() {
        return ComponentType.SELECT;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public SelectComponentView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
    }

    public void configure(final SelectMessageComponent selectMessageComponent, ComponentProvider componentProvider, final ComponentActionListener componentActionListener) {
        m.checkNotNullParameter(selectMessageComponent, "component");
        m.checkNotNullParameter(componentProvider, "componentProvider");
        m.checkNotNullParameter(componentActionListener, "componentActionListener");
        w4 a = w4.a(this);
        m.checkNotNullExpressionValue(a, "WidgetChatListBotUiSelec…mponentBinding.bind(this)");
        final String placeholder = selectMessageComponent.getPlaceholder();
        if (placeholder == null) {
            placeholder = getResources().getString(R.string.message_select_component_default_placeholder);
            m.checkNotNullExpressionValue(placeholder, "resources.getString(R.st…nent_default_placeholder)");
        }
        boolean z2 = true;
        if (!(!selectMessageComponent.getSelectedOptions().isEmpty())) {
            MaterialTextView materialTextView = a.e;
            m.checkNotNullExpressionValue(materialTextView, "binding.selectComponentSelectionText");
            materialTextView.setVisibility(0);
            FlexboxLayout flexboxLayout = a.f;
            m.checkNotNullExpressionValue(flexboxLayout, "binding.selectComponentSelectionsRoot");
            flexboxLayout.setVisibility(8);
            a.e.setTextColor(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorInteractiveNormal));
            MaterialTextView materialTextView2 = a.e;
            m.checkNotNullExpressionValue(materialTextView2, "binding.selectComponentSelectionText");
            materialTextView2.setText(placeholder);
        } else if (selectMessageComponent.getMaxValues() == 1) {
            MaterialTextView materialTextView3 = a.e;
            m.checkNotNullExpressionValue(materialTextView3, "binding.selectComponentSelectionText");
            materialTextView3.setVisibility(0);
            FlexboxLayout flexboxLayout2 = a.f;
            m.checkNotNullExpressionValue(flexboxLayout2, "binding.selectComponentSelectionsRoot");
            flexboxLayout2.setVisibility(8);
            a.e.setTextColor(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorTextNormal));
            SelectItem selectItem = (SelectItem) u.firstOrNull((List<? extends Object>) selectMessageComponent.getSelectedOptions());
            if (selectItem != null) {
                MaterialTextView materialTextView4 = a.e;
                m.checkNotNullExpressionValue(materialTextView4, "binding.selectComponentSelectionText");
                materialTextView4.setText(selectItem.d());
                ComponentViewUtils componentViewUtils = ComponentViewUtils.INSTANCE;
                SimpleDraweeView simpleDraweeView = a.d;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.selectComponentSelectionIcon");
                componentViewUtils.setEmojiOrHide(simpleDraweeView, selectItem.c(), selectMessageComponent.getEmojiAnimationsEnabled());
            }
        } else {
            FlexboxLayout flexboxLayout3 = a.f;
            m.checkNotNullExpressionValue(flexboxLayout3, "binding.selectComponentSelectionsRoot");
            MaterialTextView materialTextView5 = a.e;
            m.checkNotNullExpressionValue(materialTextView5, "binding.selectComponentSelectionText");
            materialTextView5.setVisibility(8);
            flexboxLayout3.setVisibility(0);
            flexboxLayout3.removeAllViews();
            LayoutInflater from = LayoutInflater.from(getContext());
            for (SelectItem selectItem2 : selectMessageComponent.getSelectedOptions()) {
                View inflate = from.inflate(R.layout.widget_chat_list_bot_ui_select_component_pill, (ViewGroup) null, false);
                Objects.requireNonNull(inflate, "rootView");
                MaterialTextView materialTextView6 = (MaterialTextView) inflate;
                m.checkNotNullExpressionValue(new x4(materialTextView6), "WidgetChatListBotUiSelec…Binding.inflate(inflater)");
                m.checkNotNullExpressionValue(materialTextView6, "WidgetChatListBotUiSelec…ng.inflate(inflater).root");
                materialTextView6.setText(selectItem2.d());
                flexboxLayout3.addView(materialTextView6);
            }
        }
        boolean z3 = !(selectMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Disabled);
        boolean z4 = selectMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Loading;
        TypingDots typingDots = a.c;
        if (z4) {
            typingDots.a(false);
        } else {
            typingDots.b();
        }
        TypingDots typingDots2 = a.c;
        m.checkNotNullExpressionValue(typingDots2, "binding.selectComponentLoading");
        int i = 4;
        typingDots2.setVisibility(z4 ^ true ? 4 : 0);
        ImageView imageView = a.f220b;
        m.checkNotNullExpressionValue(imageView, "binding.selectComponentChevron");
        if (!z4) {
            i = 0;
        }
        imageView.setVisibility(i);
        SelectComponentView selectComponentView = a.a;
        m.checkNotNullExpressionValue(selectComponentView, "binding.root");
        ViewExtensions.setEnabledAlpha(selectComponentView, z3, 0.3f);
        a.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.botuikit.views.select.SelectComponentView$configure$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ComponentActionListener.this.onSelectComponentClick(selectMessageComponent.getIndex(), selectMessageComponent.getCustomId(), placeholder, selectMessageComponent.getOptions(), selectMessageComponent.getSelectedOptions(), selectMessageComponent.getMinValues(), selectMessageComponent.getMaxValues(), selectMessageComponent.getEmojiAnimationsEnabled());
            }
        });
        SelectComponentView selectComponentView2 = a.a;
        m.checkNotNullExpressionValue(selectComponentView2, "binding.root");
        if (z4 || !z3) {
            z2 = false;
        }
        selectComponentView2.setClickable(z2);
    }
}
