package com.discord.widgets.botuikit.views.select;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.botuikit.SelectItem;
import com.discord.databinding.WidgetSelectComponentBottomSheetItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textview.MaterialTextView;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: SelectComponentBottomSheetAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0018\u0010#\u001a\u0014\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\n0!¢\u0006\u0004\b&\u0010'J+\u0010\u000b\u001a\u00020\n2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u001f\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0013\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010\u001cR\u0016\u0010 \u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010\u001cR\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\t\u0010\u001cR(\u0010#\u001a\u0014\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\n0!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010\u001c¨\u0006("}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetItem;", "items", "", "maxSelections", "", "emojiAnimationsEnabled", "", "setItems", "(Ljava/util/List;IZ)V", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onBindViewHolder", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V", "getItemCount", "()I", "", "getItemId", "(I)J", "isMultiSelect", "Z", "data", "Ljava/util/List;", "hasIcons", "isMaxSelected", "Lkotlin/Function2;", "Lcom/discord/api/botuikit/SelectItem;", "onItemSelected", "Lkotlin/jvm/functions/Function2;", "hasDescriptions", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function2;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectComponentBottomSheetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<SelectComponentBottomSheetItem> data = n.emptyList();
    private boolean emojiAnimationsEnabled = true;
    private boolean hasDescriptions;
    private boolean hasIcons;
    private boolean isMaxSelected;
    private boolean isMultiSelect;
    private final Function2<SelectItem, Boolean, Unit> onItemSelected;

    /* JADX WARN: Multi-variable type inference failed */
    public SelectComponentBottomSheetAdapter(Function2<? super SelectItem, ? super Boolean, Unit> function2) {
        m.checkNotNullParameter(function2, "onItemSelected");
        this.onItemSelected = function2;
        setHasStableIds(true);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        m.checkNotNullParameter(viewHolder, "holder");
        ((SelectComponentBottomSheetItemViewHolder) viewHolder).bind(this.data.get(i), this.isMultiSelect, this.hasIcons, this.hasDescriptions, i == this.data.size() - 1, this.isMaxSelected, this.emojiAnimationsEnabled);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.widget_select_component_bottom_sheet_item, viewGroup, false);
        int i2 = R.id.select_component_sheet_item_description;
        MaterialTextView materialTextView = (MaterialTextView) inflate.findViewById(R.id.select_component_sheet_item_description);
        if (materialTextView != null) {
            i2 = R.id.select_component_sheet_item_divider;
            View findViewById = inflate.findViewById(R.id.select_component_sheet_item_divider);
            if (findViewById != null) {
                i2 = R.id.select_component_sheet_item_divider_icon;
                View findViewById2 = inflate.findViewById(R.id.select_component_sheet_item_divider_icon);
                if (findViewById2 != null) {
                    i2 = R.id.select_component_sheet_item_icon;
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.select_component_sheet_item_icon);
                    if (simpleDraweeView != null) {
                        i2 = R.id.select_component_sheet_item_selected;
                        MaterialCheckBox materialCheckBox = (MaterialCheckBox) inflate.findViewById(R.id.select_component_sheet_item_selected);
                        if (materialCheckBox != null) {
                            i2 = R.id.select_component_sheet_item_title;
                            MaterialTextView materialTextView2 = (MaterialTextView) inflate.findViewById(R.id.select_component_sheet_item_title);
                            if (materialTextView2 != null) {
                                WidgetSelectComponentBottomSheetItemBinding widgetSelectComponentBottomSheetItemBinding = new WidgetSelectComponentBottomSheetItemBinding((ConstraintLayout) inflate, materialTextView, findViewById, findViewById2, simpleDraweeView, materialCheckBox, materialTextView2);
                                m.checkNotNullExpressionValue(widgetSelectComponentBottomSheetItemBinding, "WidgetSelectComponentBot…          false\n        )");
                                return new SelectComponentBottomSheetItemViewHolder(widgetSelectComponentBottomSheetItemBinding, this.onItemSelected);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }

    public final void setItems(List<SelectComponentBottomSheetItem> list, int i, boolean z2) {
        boolean z3;
        boolean z4;
        int i2;
        boolean z5;
        boolean z6;
        m.checkNotNullParameter(list, "items");
        this.data = list;
        boolean z7 = list instanceof Collection;
        boolean z8 = true;
        if (!z7 || !list.isEmpty()) {
            for (SelectComponentBottomSheetItem selectComponentBottomSheetItem : list) {
                if (selectComponentBottomSheetItem.getSelectItem().c() != null) {
                    z6 = true;
                    continue;
                } else {
                    z6 = false;
                    continue;
                }
                if (z6) {
                    z3 = true;
                    break;
                }
            }
        }
        z3 = false;
        this.hasIcons = z3;
        if (!z7 || !list.isEmpty()) {
            for (SelectComponentBottomSheetItem selectComponentBottomSheetItem2 : list) {
                if (selectComponentBottomSheetItem2.getSelectItem().b() != null) {
                    z5 = true;
                    continue;
                } else {
                    z5 = false;
                    continue;
                }
                if (z5) {
                    z4 = true;
                    break;
                }
            }
        }
        z4 = false;
        this.hasDescriptions = z4;
        this.isMultiSelect = i > 1;
        if (!z7 || !list.isEmpty()) {
            i2 = 0;
            for (SelectComponentBottomSheetItem selectComponentBottomSheetItem3 : list) {
                if (selectComponentBottomSheetItem3.getSelected() && (i2 = i2 + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
        } else {
            i2 = 0;
        }
        if (i2 != i) {
            z8 = false;
        }
        this.isMaxSelected = z8;
        this.emojiAnimationsEnabled = z2;
        notifyDataSetChanged();
    }
}
