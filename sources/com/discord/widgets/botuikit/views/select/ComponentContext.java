package com.discord.widgets.botuikit.views.select;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
/* compiled from: SelectComponentBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001BE\u0012\u000e\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u0006\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\t\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002\u0012\n\u0010\u0012\u001a\u00060\u0002j\u0002`\f¢\u0006\u0004\b'\u0010(J\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0002j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0005J\u0014\u0010\r\u001a\u00060\u0002j\u0002`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\bJX\u0010\u0013\u001a\u00020\u00002\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\f\b\u0002\u0010\u000f\u001a\u00060\u0002j\u0002`\u00062\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\t2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00022\f\b\u0002\u0010\u0012\u001a\u00060\u0002j\u0002`\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001e\u001a\u00020\u001d2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u001d\u0010\u0012\u001a\u00060\u0002j\u0002`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b!\u0010\bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\u0005R!\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\"\u001a\u0004\b$\u0010\u0005R\u001d\u0010\u000f\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b%\u0010\bR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b&\u0010\b¨\u0006)"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/ComponentContext;", "Ljava/io/Serializable;", "", "Lcom/discord/primitives/GuildId;", "component1", "()Ljava/lang/Long;", "Lcom/discord/primitives/MessageId;", "component2", "()J", "Lcom/discord/primitives/ChannelId;", "component3", "component4", "Lcom/discord/primitives/ApplicationId;", "component5", "guildId", "messageId", "channelId", "messageFlags", "applicationId", "copy", "(Ljava/lang/Long;JJLjava/lang/Long;J)Lcom/discord/widgets/botuikit/views/select/ComponentContext;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getApplicationId", "Ljava/lang/Long;", "getMessageFlags", "getGuildId", "getMessageId", "getChannelId", HookHelper.constructorName, "(Ljava/lang/Long;JJLjava/lang/Long;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ComponentContext implements Serializable {
    private final long applicationId;
    private final long channelId;
    private final Long guildId;
    private final Long messageFlags;
    private final long messageId;

    public ComponentContext(Long l, long j, long j2, Long l2, long j3) {
        this.guildId = l;
        this.messageId = j;
        this.channelId = j2;
        this.messageFlags = l2;
        this.applicationId = j3;
    }

    public final Long component1() {
        return this.guildId;
    }

    public final long component2() {
        return this.messageId;
    }

    public final long component3() {
        return this.channelId;
    }

    public final Long component4() {
        return this.messageFlags;
    }

    public final long component5() {
        return this.applicationId;
    }

    public final ComponentContext copy(Long l, long j, long j2, Long l2, long j3) {
        return new ComponentContext(l, j, j2, l2, j3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ComponentContext)) {
            return false;
        }
        ComponentContext componentContext = (ComponentContext) obj;
        return m.areEqual(this.guildId, componentContext.guildId) && this.messageId == componentContext.messageId && this.channelId == componentContext.channelId && m.areEqual(this.messageFlags, componentContext.messageFlags) && this.applicationId == componentContext.applicationId;
    }

    public final long getApplicationId() {
        return this.applicationId;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final Long getMessageFlags() {
        return this.messageFlags;
    }

    public final long getMessageId() {
        return this.messageId;
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = l != null ? l.hashCode() : 0;
        int a = (b.a(this.channelId) + ((b.a(this.messageId) + (hashCode * 31)) * 31)) * 31;
        Long l2 = this.messageFlags;
        if (l2 != null) {
            i = l2.hashCode();
        }
        return b.a(this.applicationId) + ((a + i) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("ComponentContext(guildId=");
        R.append(this.guildId);
        R.append(", messageId=");
        R.append(this.messageId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", messageFlags=");
        R.append(this.messageFlags);
        R.append(", applicationId=");
        return a.B(R, this.applicationId, ")");
    }
}
