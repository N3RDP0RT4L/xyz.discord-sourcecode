package com.discord.widgets.botuikit.views.select;

import com.discord.api.botuikit.SelectItem;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: SelectComponentBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/botuikit/SelectItem;", "p1", "", "p2", "", "invoke", "(Lcom/discord/api/botuikit/SelectItem;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class SelectComponentBottomSheet$adapter$1 extends k implements Function2<SelectItem, Boolean, Unit> {
    public SelectComponentBottomSheet$adapter$1(SelectComponentBottomSheet selectComponentBottomSheet) {
        super(2, selectComponentBottomSheet, SelectComponentBottomSheet.class, "onItemSelected", "onItemSelected(Lcom/discord/api/botuikit/SelectItem;Z)V", 0);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(SelectItem selectItem, Boolean bool) {
        invoke(selectItem, bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(SelectItem selectItem, boolean z2) {
        m.checkNotNullParameter(selectItem, "p1");
        ((SelectComponentBottomSheet) this.receiver).onItemSelected(selectItem, z2);
    }
}
