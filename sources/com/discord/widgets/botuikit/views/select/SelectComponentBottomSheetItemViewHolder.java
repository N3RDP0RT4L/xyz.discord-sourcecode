package com.discord.widgets.botuikit.views.select;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.botuikit.ComponentEmoji;
import com.discord.api.botuikit.SelectItem;
import com.discord.databinding.WidgetSelectComponentBottomSheetItemBinding;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.botuikit.views.ComponentViewUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textview.MaterialTextView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: SelectComponentBottomSheetAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\u0018\u0010\u0013\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000b0\u0011¢\u0006\u0004\b\u0015\u0010\u0016JE\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R(\u0010\u0013\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000b0\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetItemViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetItem;", "data", "", "isMultiSelect", "hasIcons", "hasDescriptions", "lastItem", "maxSelected", "emojiAnimationsEnabled", "", "bind", "(Lcom/discord/widgets/botuikit/views/select/SelectComponentBottomSheetItem;ZZZZZZ)V", "Lcom/discord/databinding/WidgetSelectComponentBottomSheetItemBinding;", "binding", "Lcom/discord/databinding/WidgetSelectComponentBottomSheetItemBinding;", "Lkotlin/Function2;", "Lcom/discord/api/botuikit/SelectItem;", "onItemSelected", "Lkotlin/jvm/functions/Function2;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetSelectComponentBottomSheetItemBinding;Lkotlin/jvm/functions/Function2;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectComponentBottomSheetItemViewHolder extends RecyclerView.ViewHolder {
    private final WidgetSelectComponentBottomSheetItemBinding binding;
    private final Function2<SelectItem, Boolean, Unit> onItemSelected;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public SelectComponentBottomSheetItemViewHolder(WidgetSelectComponentBottomSheetItemBinding widgetSelectComponentBottomSheetItemBinding, Function2<? super SelectItem, ? super Boolean, Unit> function2) {
        super(widgetSelectComponentBottomSheetItemBinding.a);
        m.checkNotNullParameter(widgetSelectComponentBottomSheetItemBinding, "binding");
        m.checkNotNullParameter(function2, "onItemSelected");
        this.binding = widgetSelectComponentBottomSheetItemBinding;
        this.onItemSelected = function2;
    }

    public final void bind(final SelectComponentBottomSheetItem selectComponentBottomSheetItem, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        m.checkNotNullParameter(selectComponentBottomSheetItem, "data");
        boolean z8 = true;
        int i = 0;
        boolean z9 = z6 && !selectComponentBottomSheetItem.getSelected() && z2;
        ConstraintLayout constraintLayout = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        constraintLayout.setEnabled(!z9);
        this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.botuikit.views.select.SelectComponentBottomSheetItemViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function2 function2;
                function2 = SelectComponentBottomSheetItemViewHolder.this.onItemSelected;
                function2.invoke(selectComponentBottomSheetItem.getSelectItem(), Boolean.valueOf(!selectComponentBottomSheetItem.getSelected()));
            }
        });
        ConstraintLayout constraintLayout2 = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout2, "binding.root");
        ViewExtensions.setEnabledAlpha(constraintLayout2, !z9, 0.3f);
        MaterialTextView materialTextView = this.binding.g;
        m.checkNotNullExpressionValue(materialTextView, "binding.selectComponentSheetItemTitle");
        materialTextView.setText(selectComponentBottomSheetItem.getSelectItem().d());
        String b2 = selectComponentBottomSheetItem.getSelectItem().b();
        MaterialTextView materialTextView2 = this.binding.f2509b;
        m.checkNotNullExpressionValue(materialTextView2, "binding.selectComponentSheetItemDescription");
        ViewExtensions.setTextAndVisibilityBy(materialTextView2, b2);
        MaterialCheckBox materialCheckBox = this.binding.f;
        m.checkNotNullExpressionValue(materialCheckBox, "binding.selectComponentSheetItemSelected");
        materialCheckBox.setChecked(selectComponentBottomSheetItem.getSelected());
        MaterialCheckBox materialCheckBox2 = this.binding.f;
        m.checkNotNullExpressionValue(materialCheckBox2, "binding.selectComponentSheetItemSelected");
        materialCheckBox2.setVisibility(z2 ^ true ? 4 : 0);
        MaterialCheckBox materialCheckBox3 = this.binding.f;
        m.checkNotNullExpressionValue(materialCheckBox3, "binding.selectComponentSheetItemSelected");
        materialCheckBox3.setEnabled(!z9);
        ComponentEmoji c = selectComponentBottomSheetItem.getSelectItem().c();
        ComponentViewUtils componentViewUtils = ComponentViewUtils.INSTANCE;
        SimpleDraweeView simpleDraweeView = this.binding.e;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.selectComponentSheetItemIcon");
        componentViewUtils.setEmojiOrHide(simpleDraweeView, c, z7);
        int dpToPixels = DimenUtils.dpToPixels(12);
        int dpToPixels2 = selectComponentBottomSheetItem.getSelectItem().b() == null ? DimenUtils.dpToPixels(12) : 0;
        int dpToPixels3 = (!z3 || selectComponentBottomSheetItem.getSelectItem().c() != null) ? 0 : DimenUtils.dpToPixels(40);
        this.binding.g.setPadding(dpToPixels3, dpToPixels, 0, dpToPixels2);
        this.binding.f2509b.setPadding(dpToPixels3, 0, 0, DimenUtils.dpToPixels(12));
        View view = this.binding.d;
        m.checkNotNullExpressionValue(view, "binding.selectComponentSheetItemDividerIcon");
        view.setVisibility(z3 && !z5 ? 0 : 8);
        View view2 = this.binding.c;
        m.checkNotNullExpressionValue(view2, "binding.selectComponentSheetItemDivider");
        if (z3 || z5) {
            z8 = false;
        }
        if (!z8) {
            i = 8;
        }
        view2.setVisibility(i);
        int dpToPixels4 = DimenUtils.dpToPixels(z4 ? 62 : 46);
        ConstraintLayout constraintLayout3 = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout3, "binding.root");
        constraintLayout3.setMinHeight(dpToPixels4);
    }
}
