package com.discord.widgets.botuikit.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import com.discord.api.botuikit.ButtonStyle;
import com.discord.api.botuikit.ComponentEmoji;
import com.discord.api.botuikit.ComponentType;
import com.discord.databinding.WidgetChatListBotUiButtonComponentBinding;
import com.discord.models.botuikit.ActionInteractionComponentState;
import com.discord.models.botuikit.ButtonMessageComponent;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.typing.TypingDots;
import com.discord.widgets.botuikit.ComponentProvider;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: ButtonComponentView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 12\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002:\u00011B!\b\u0016\u0012\u0006\u0010)\u001a\u00020(\u0012\u0006\u0010+\u001a\u00020*\u0012\u0006\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010/B\u0019\b\u0016\u0012\u0006\u0010)\u001a\u00020(\u0012\u0006\u0010+\u001a\u00020*¢\u0006\u0004\b.\u00100J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\b\u0010\tJ1\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0016\u0010\u0017J'\u0010\u001d\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u001bH\u0016¢\u0006\u0004\b\u001d\u0010\u001eJ\u001d\u0010#\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b#\u0010$J\u000f\u0010&\u001a\u00020%H\u0016¢\u0006\u0004\b&\u0010'¨\u00062"}, d2 = {"Lcom/discord/widgets/botuikit/views/ButtonComponentView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lcom/discord/widgets/botuikit/views/ComponentView;", "Lcom/discord/models/botuikit/ButtonMessageComponent;", "Landroid/widget/TextView;", "label", "messageComponent", "", "configureLabelPadding", "(Landroid/widget/TextView;Lcom/discord/models/botuikit/ButtonMessageComponent;)V", "Lcom/facebook/drawee/view/SimpleDraweeView;", "emojiView", "Lcom/discord/api/botuikit/ComponentEmoji;", "emoji", "", "isLoading", "emojiAnimationsEnabled", "configureEmoji", "(Lcom/facebook/drawee/view/SimpleDraweeView;Lcom/discord/api/botuikit/ComponentEmoji;ZZ)V", "Landroid/widget/ImageView;", "icon", "showIcon", "configureLinkIcon", "(Landroid/widget/ImageView;Z)V", "component", "Lcom/discord/widgets/botuikit/ComponentProvider;", "componentProvider", "Lcom/discord/widgets/botuikit/views/ComponentActionListener;", "componentActionListener", "configure", "(Lcom/discord/models/botuikit/ButtonMessageComponent;Lcom/discord/widgets/botuikit/ComponentProvider;Lcom/discord/widgets/botuikit/views/ComponentActionListener;)V", "Landroid/widget/Button;", "button", "Lcom/discord/api/botuikit/ButtonStyle;", "style", "configureStyle", "(Landroid/widget/Button;Lcom/discord/api/botuikit/ButtonStyle;)V", "Lcom/discord/api/botuikit/ComponentType;", "type", "()Lcom/discord/api/botuikit/ComponentType;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyle", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ButtonComponentView extends ConstraintLayout implements ComponentView<ButtonMessageComponent> {
    public static final Companion Companion = new Companion(null);

    /* compiled from: ButtonComponentView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/botuikit/views/ButtonComponentView$Companion;", "", "Landroid/content/Context;", "context", "Landroid/view/ViewGroup;", "root", "Lcom/discord/widgets/botuikit/views/ButtonComponentView;", "inflateComponent", "(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/discord/widgets/botuikit/views/ButtonComponentView;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ButtonComponentView inflateComponent(Context context, ViewGroup viewGroup) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(viewGroup, "root");
            WidgetChatListBotUiButtonComponentBinding a = WidgetChatListBotUiButtonComponentBinding.a(LayoutInflater.from(context).inflate(R.layout.widget_chat_list_bot_ui_button_component, viewGroup, false));
            m.checkNotNullExpressionValue(a, "WidgetChatListBotUiButto…om(context), root, false)");
            ButtonComponentView buttonComponentView = a.a;
            m.checkNotNullExpressionValue(buttonComponentView, "WidgetChatListBotUiButto…ntext), root, false).root");
            return buttonComponentView;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ButtonStyle.values();
            int[] iArr = new int[6];
            $EnumSwitchMapping$0 = iArr;
            iArr[ButtonStyle.UNKNOWN.ordinal()] = 1;
            iArr[ButtonStyle.PRIMARY.ordinal()] = 2;
            iArr[ButtonStyle.SECONDARY.ordinal()] = 3;
            iArr[ButtonStyle.LINK.ordinal()] = 4;
            iArr[ButtonStyle.DANGER.ordinal()] = 5;
            iArr[ButtonStyle.SUCCESS.ordinal()] = 6;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ButtonComponentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
    }

    private final void configureEmoji(SimpleDraweeView simpleDraweeView, ComponentEmoji componentEmoji, boolean z2, boolean z3) {
        if (componentEmoji == null || !z2) {
            ComponentViewUtils.INSTANCE.setEmojiOrHide(simpleDraweeView, componentEmoji, z3);
        } else {
            simpleDraweeView.setVisibility(4);
        }
    }

    private final void configureLabelPadding(TextView textView, ButtonMessageComponent buttonMessageComponent) {
        int i;
        if (buttonMessageComponent.getLabel() == null) {
            if (!ButtonComponentViewKt.hasEmoji(buttonMessageComponent) || !ButtonComponentViewKt.hasIcon(buttonMessageComponent)) {
                i = DimenUtils.dpToPixels(16);
            } else {
                i = DimenUtils.dpToPixels(8);
            }
            textView.setPadding(0, 0, i, 0);
            return;
        }
        textView.setPadding(ButtonComponentViewKt.hasEmoji(buttonMessageComponent) ? DimenUtils.dpToPixels(8) : DimenUtils.dpToPixels(16), 0, ButtonComponentViewKt.hasIcon(buttonMessageComponent) ? DimenUtils.dpToPixels(8) : DimenUtils.dpToPixels(16), 0);
    }

    private final void configureLinkIcon(ImageView imageView, boolean z2) {
        imageView.setVisibility(z2 ? 0 : 8);
    }

    public final void configureStyle(Button button, ButtonStyle buttonStyle) {
        m.checkNotNullParameter(button, "button");
        m.checkNotNullParameter(buttonStyle, "style");
        int ordinal = buttonStyle.ordinal();
        int i = R.color.uikit_btn_bg_color_selector_secondary;
        if (ordinal != 0) {
            if (ordinal == 1) {
                i = R.color.uikit_btn_bg_color_selector_brand;
            } else if (ordinal != 2) {
                if (ordinal == 3) {
                    i = R.color.uikit_btn_bg_color_selector_green;
                } else if (ordinal == 4) {
                    i = R.color.uikit_btn_bg_color_selector_red;
                } else if (ordinal != 5) {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        Resources resources = getResources();
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        ViewCompat.setBackgroundTintList(button, ResourcesCompat.getColorStateList(resources, i, context.getTheme()));
    }

    @Override // com.discord.widgets.botuikit.views.ComponentView
    public ComponentType type() {
        return ComponentType.BUTTON;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public ButtonComponentView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
    }

    public void configure(final ButtonMessageComponent buttonMessageComponent, ComponentProvider componentProvider, final ComponentActionListener componentActionListener) {
        m.checkNotNullParameter(buttonMessageComponent, "component");
        m.checkNotNullParameter(componentProvider, "componentProvider");
        m.checkNotNullParameter(componentActionListener, "componentActionListener");
        final WidgetChatListBotUiButtonComponentBinding a = WidgetChatListBotUiButtonComponentBinding.a(this);
        m.checkNotNullExpressionValue(a, "WidgetChatListBotUiButto…mponentBinding.bind(this)");
        MaterialButton materialButton = a.f2325b;
        m.checkNotNullExpressionValue(materialButton, "binding.button");
        configureStyle(materialButton, buttonMessageComponent.getStyle());
        SimpleDraweeView simpleDraweeView = a.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.emoji");
        configureEmoji(simpleDraweeView, buttonMessageComponent.getEmoji(), buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Loading, buttonMessageComponent.getEmojiAnimationsEnabled());
        AppCompatImageView appCompatImageView = a.e;
        m.checkNotNullExpressionValue(appCompatImageView, "binding.linkIcon");
        configureLinkIcon(appCompatImageView, buttonMessageComponent.getStyle() == ButtonStyle.LINK);
        MaterialTextView materialTextView = a.d;
        m.checkNotNullExpressionValue(materialTextView, "binding.label");
        configureLabelPadding(materialTextView, buttonMessageComponent);
        MaterialTextView materialTextView2 = a.d;
        m.checkNotNullExpressionValue(materialTextView2, "binding.label");
        ViewExtensions.setEnabledAlpha(materialTextView2, !(buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Disabled), 0.5f);
        SimpleDraweeView simpleDraweeView2 = a.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.emoji");
        ViewExtensions.setEnabledAlpha$default(simpleDraweeView2, !(buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Disabled), 0.0f, 2, null);
        AppCompatImageView appCompatImageView2 = a.e;
        m.checkNotNullExpressionValue(appCompatImageView2, "binding.linkIcon");
        ViewExtensions.setEnabledAlpha$default(appCompatImageView2, !(buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Disabled), 0.0f, 2, null);
        MaterialButton materialButton2 = a.f2325b;
        m.checkNotNullExpressionValue(materialButton2, "binding.button");
        materialButton2.setEnabled(!(buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Disabled));
        MaterialTextView materialTextView3 = a.d;
        m.checkNotNullExpressionValue(materialTextView3, "binding.label");
        materialTextView3.setText(buttonMessageComponent.getLabel());
        MaterialTextView materialTextView4 = a.d;
        m.checkNotNullExpressionValue(materialTextView4, "binding.label");
        materialTextView4.setVisibility(buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Loading ? 4 : 0);
        a.f2325b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.botuikit.views.ButtonComponentView$configure$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (ButtonMessageComponent.this.getUrl() != null) {
                    MaterialButton materialButton3 = a.f2325b;
                    m.checkNotNullExpressionValue(materialButton3, "binding.button");
                    Context context = materialButton3.getContext();
                    m.checkNotNullExpressionValue(context, "binding.button.context");
                    UriHandler.handleOrUntrusted$default(context, ButtonMessageComponent.this.getUrl(), null, 4, null);
                } else if (ButtonMessageComponent.this.getCustomId() != null) {
                    componentActionListener.onButtonComponentClick(ButtonMessageComponent.this.getIndex(), ButtonMessageComponent.this.getCustomId());
                }
            }
        });
        TypingDots typingDots = a.f;
        m.checkNotNullExpressionValue(typingDots, "binding.loadingDots");
        typingDots.setVisibility(buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Loading ? 0 : 8);
        if (buttonMessageComponent.getStateInteraction() instanceof ActionInteractionComponentState.Loading) {
            a.f.a(false);
            MaterialButton materialButton3 = a.f2325b;
            m.checkNotNullExpressionValue(materialButton3, "binding.button");
            materialButton3.setClickable(false);
            return;
        }
        a.f.b();
        MaterialButton materialButton4 = a.f2325b;
        m.checkNotNullExpressionValue(materialButton4, "binding.button");
        materialButton4.setClickable(true);
    }
}
