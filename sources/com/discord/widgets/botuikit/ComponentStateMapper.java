package com.discord.widgets.botuikit;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.botuikit.ActionRowComponent;
import com.discord.api.botuikit.Component;
import com.discord.api.botuikit.ComponentUtils;
import com.discord.api.botuikit.LayoutComponent;
import com.discord.models.botuikit.ActionRowMessageComponentKt;
import com.discord.models.botuikit.MessageComponent;
import com.discord.widgets.botuikit.ComponentChatListState;
import d0.t.k;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
/* compiled from: ComponentStateMapper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u001dB\t\b\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ;\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ3\u0010\u0015\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000f\u001a\u00020\u000e2\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u00112\u0006\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0007¢\u0006\u0004\b\u0015\u0010\u0016J9\u0010\u0019\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000f\u001a\u00020\u00172\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u00112\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00022\u0006\u0010\u0014\u001a\u00020\u0007¢\u0006\u0004\b\u0019\u0010\u001a¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/botuikit/ComponentStateMapper;", "", "", "Lcom/discord/api/botuikit/Component;", "apiComponents", "Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", "componentStoreState", "Lcom/discord/widgets/botuikit/ComponentExperiments;", "componentExperiments", "", "animateEmojis", "Lcom/discord/models/botuikit/MessageComponent;", "processComponentsToMessageComponents", "(Ljava/util/List;Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;Lcom/discord/widgets/botuikit/ComponentExperiments;Z)Ljava/util/List;", "Lcom/discord/api/botuikit/ActionComponent;", "component", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "index", "storeState", "experimentController", "createActionMessageComponent", "(Lcom/discord/api/botuikit/ActionComponent;ILcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;Lcom/discord/widgets/botuikit/ComponentExperiments;)Lcom/discord/models/botuikit/MessageComponent;", "Lcom/discord/api/botuikit/LayoutComponent;", "children", "toMessageLayoutComponent", "(Lcom/discord/api/botuikit/LayoutComponent;ILjava/util/List;Lcom/discord/widgets/botuikit/ComponentExperiments;)Lcom/discord/models/botuikit/MessageComponent;", HookHelper.constructorName, "()V", "ChildComponents", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ComponentStateMapper {
    public static final ComponentStateMapper INSTANCE = new ComponentStateMapper();

    private ComponentStateMapper() {
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0054  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x005f  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0064  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0080  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x008a  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0097  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.models.botuikit.MessageComponent createActionMessageComponent(com.discord.api.botuikit.ActionComponent r4, int r5, com.discord.widgets.botuikit.ComponentChatListState.ComponentStoreState r6, com.discord.widgets.botuikit.ComponentExperiments r7) {
        /*
            r3 = this;
            java.lang.String r0 = "component"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            java.lang.String r0 = "storeState"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            java.lang.String r0 = "experimentController"
            d0.z.d.m.checkNotNullParameter(r7, r0)
            com.discord.api.botuikit.ComponentType r0 = r4.getType()
            boolean r7 = r7.isEnabled(r0)
            r0 = 0
            if (r7 != 0) goto L1b
            return r0
        L1b:
            java.util.Map r7 = r6.getInteractionState()
            if (r7 == 0) goto L4d
            java.util.Set r7 = r7.entrySet()
            if (r7 == 0) goto L4d
            java.util.Iterator r7 = r7.iterator()
        L2b:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L41
            java.lang.Object r1 = r7.next()
            r2 = r1
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r2 = r2.getValue()
            boolean r2 = r2 instanceof com.discord.stores.StoreApplicationInteractions.InteractionSendState.Loading
            if (r2 == 0) goto L2b
            goto L42
        L41:
            r1 = r0
        L42:
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            if (r1 == 0) goto L4d
            java.lang.Object r7 = r1.getKey()
            java.lang.Integer r7 = (java.lang.Integer) r7
            goto L4e
        L4d:
            r7 = r0
        L4e:
            java.util.Map r1 = r6.getInteractionState()
            if (r1 == 0) goto L5f
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)
            java.lang.Object r1 = r1.get(r2)
            com.discord.stores.StoreApplicationInteractions$InteractionSendState r1 = (com.discord.stores.StoreApplicationInteractions.InteractionSendState) r1
            goto L60
        L5f:
            r1 = r0
        L60:
            boolean r2 = r1 instanceof com.discord.stores.StoreApplicationInteractions.InteractionSendState.Failed
            if (r2 != 0) goto L65
            r1 = r0
        L65:
            com.discord.stores.StoreApplicationInteractions$InteractionSendState$Failed r1 = (com.discord.stores.StoreApplicationInteractions.InteractionSendState.Failed) r1
            if (r7 != 0) goto L6a
            goto L73
        L6a:
            int r2 = r7.intValue()
            if (r5 != r2) goto L73
            com.discord.models.botuikit.ActionInteractionComponentState$Loading r7 = com.discord.models.botuikit.ActionInteractionComponentState.Loading.INSTANCE
            goto L8c
        L73:
            if (r7 == 0) goto L7e
            int r7 = r7.intValue()
            if (r7 == r5) goto L7e
            com.discord.models.botuikit.ActionInteractionComponentState$Disabled r7 = com.discord.models.botuikit.ActionInteractionComponentState.Disabled.INSTANCE
            goto L8c
        L7e:
            if (r1 == 0) goto L8a
            com.discord.models.botuikit.ActionInteractionComponentState$Failed r7 = new com.discord.models.botuikit.ActionInteractionComponentState$Failed
            java.lang.String r1 = r1.getErrorMessage()
            r7.<init>(r1)
            goto L8c
        L8a:
            com.discord.models.botuikit.ActionInteractionComponentState$Enabled r7 = com.discord.models.botuikit.ActionInteractionComponentState.Enabled.INSTANCE
        L8c:
            boolean r1 = r4 instanceof com.discord.api.botuikit.ButtonComponent
            if (r1 == 0) goto L97
            com.discord.api.botuikit.ButtonComponent r4 = (com.discord.api.botuikit.ButtonComponent) r4
            com.discord.models.botuikit.ButtonMessageComponent r0 = com.discord.models.botuikit.ButtonMessageComponentKt.mergeToMessageComponent(r4, r5, r7, r6)
            goto La1
        L97:
            boolean r1 = r4 instanceof com.discord.api.botuikit.SelectComponent
            if (r1 == 0) goto La1
            com.discord.api.botuikit.SelectComponent r4 = (com.discord.api.botuikit.SelectComponent) r4
            com.discord.models.botuikit.SelectMessageComponent r0 = com.discord.models.botuikit.SelectMessageComponentKt.mergeToMessageComponent(r4, r5, r7, r6)
        La1:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.botuikit.ComponentStateMapper.createActionMessageComponent(com.discord.api.botuikit.ActionComponent, int, com.discord.widgets.botuikit.ComponentChatListState$ComponentStoreState, com.discord.widgets.botuikit.ComponentExperiments):com.discord.models.botuikit.MessageComponent");
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [T, com.discord.widgets.botuikit.ComponentStateMapper$ChildComponents] */
    public final List<MessageComponent> processComponentsToMessageComponents(List<? extends Component> list, ComponentChatListState.ComponentStoreState componentStoreState, ComponentExperiments componentExperiments, boolean z2) {
        m.checkNotNullParameter(list, "apiComponents");
        m.checkNotNullParameter(componentExperiments, "componentExperiments");
        if (componentStoreState == null) {
            componentStoreState = new ComponentChatListState.ComponentStoreState(null, null, z2, 3, null);
        }
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = new ChildComponents(-1, null, 0, list.size());
        Stack stack = new Stack();
        ComponentUtils componentUtils = ComponentUtils.INSTANCE;
        ComponentStateMapper$processComponentsToMessageComponents$1 componentStateMapper$processComponentsToMessageComponents$1 = new ComponentStateMapper$processComponentsToMessageComponents$1(componentStoreState, componentExperiments, ref$ObjectRef, stack);
        Objects.requireNonNull(componentUtils);
        m.checkNotNullParameter(list, "$this$forEachComponentIndexed");
        m.checkNotNullParameter(componentStateMapper$processComponentsToMessageComponents$1, "action");
        componentUtils.a(list, 0, componentStateMapper$processComponentsToMessageComponents$1);
        return k.filterNotNull(((ChildComponents) ref$ObjectRef.element).getComponents());
    }

    public final MessageComponent toMessageLayoutComponent(LayoutComponent layoutComponent, int i, List<? extends MessageComponent> list, ComponentExperiments componentExperiments) {
        m.checkNotNullParameter(layoutComponent, "component");
        m.checkNotNullParameter(list, "children");
        m.checkNotNullParameter(componentExperiments, "experimentController");
        ActionRowComponent actionRowComponent = (ActionRowComponent) layoutComponent;
        if (!componentExperiments.isEnabled(actionRowComponent.getType())) {
            return null;
        }
        return ActionRowMessageComponentKt.mergeToMessageComponent(actionRowComponent, i, list);
    }

    /* compiled from: ComponentStateMapper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\n\u0010\u000b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0006\u0012\f\b\u0002\u0010\r\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b'\u0010(J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\t\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\t\u0010\u0005J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0005JB\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\u000b\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00062\f\b\u0002\u0010\r\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0005J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R!\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001a0\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001d\u0010\u000b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001f\u001a\u0004\b \u0010\u0005R&\u0010\r\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\r\u0010\u001f\u001a\u0004\b!\u0010\u0005\"\u0004\b\"\u0010#R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010$\u001a\u0004\b%\u0010\bR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b&\u0010\u0005¨\u0006)"}, d2 = {"Lcom/discord/widgets/botuikit/ComponentStateMapper$ChildComponents;", "", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "component1", "()I", "Lcom/discord/api/botuikit/LayoutComponent;", "component2", "()Lcom/discord/api/botuikit/LayoutComponent;", "component3", "component4", "layoutComponentIndex", "parentLayoutComponent", "currentComponentInsertionIndex", "total", "copy", "(ILcom/discord/api/botuikit/LayoutComponent;II)Lcom/discord/widgets/botuikit/ComponentStateMapper$ChildComponents;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/models/botuikit/MessageComponent;", "components", "[Lcom/discord/models/botuikit/MessageComponent;", "getComponents", "()[Lcom/discord/models/botuikit/MessageComponent;", "I", "getLayoutComponentIndex", "getCurrentComponentInsertionIndex", "setCurrentComponentInsertionIndex", "(I)V", "Lcom/discord/api/botuikit/LayoutComponent;", "getParentLayoutComponent", "getTotal", HookHelper.constructorName, "(ILcom/discord/api/botuikit/LayoutComponent;II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChildComponents {
        private final MessageComponent[] components;
        private int currentComponentInsertionIndex;
        private final int layoutComponentIndex;
        private final LayoutComponent parentLayoutComponent;
        private final int total;

        public ChildComponents(int i, LayoutComponent layoutComponent, int i2, int i3) {
            this.layoutComponentIndex = i;
            this.parentLayoutComponent = layoutComponent;
            this.currentComponentInsertionIndex = i2;
            this.total = i3;
            MessageComponent[] messageComponentArr = new MessageComponent[i3];
            for (int i4 = 0; i4 < i3; i4++) {
                messageComponentArr[i4] = null;
            }
            this.components = messageComponentArr;
        }

        public static /* synthetic */ ChildComponents copy$default(ChildComponents childComponents, int i, LayoutComponent layoutComponent, int i2, int i3, int i4, Object obj) {
            if ((i4 & 1) != 0) {
                i = childComponents.layoutComponentIndex;
            }
            if ((i4 & 2) != 0) {
                layoutComponent = childComponents.parentLayoutComponent;
            }
            if ((i4 & 4) != 0) {
                i2 = childComponents.currentComponentInsertionIndex;
            }
            if ((i4 & 8) != 0) {
                i3 = childComponents.total;
            }
            return childComponents.copy(i, layoutComponent, i2, i3);
        }

        public final int component1() {
            return this.layoutComponentIndex;
        }

        public final LayoutComponent component2() {
            return this.parentLayoutComponent;
        }

        public final int component3() {
            return this.currentComponentInsertionIndex;
        }

        public final int component4() {
            return this.total;
        }

        public final ChildComponents copy(int i, LayoutComponent layoutComponent, int i2, int i3) {
            return new ChildComponents(i, layoutComponent, i2, i3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChildComponents)) {
                return false;
            }
            ChildComponents childComponents = (ChildComponents) obj;
            return this.layoutComponentIndex == childComponents.layoutComponentIndex && m.areEqual(this.parentLayoutComponent, childComponents.parentLayoutComponent) && this.currentComponentInsertionIndex == childComponents.currentComponentInsertionIndex && this.total == childComponents.total;
        }

        public final MessageComponent[] getComponents() {
            return this.components;
        }

        public final int getCurrentComponentInsertionIndex() {
            return this.currentComponentInsertionIndex;
        }

        public final int getLayoutComponentIndex() {
            return this.layoutComponentIndex;
        }

        public final LayoutComponent getParentLayoutComponent() {
            return this.parentLayoutComponent;
        }

        public final int getTotal() {
            return this.total;
        }

        public int hashCode() {
            int i = this.layoutComponentIndex * 31;
            LayoutComponent layoutComponent = this.parentLayoutComponent;
            return ((((i + (layoutComponent != null ? layoutComponent.hashCode() : 0)) * 31) + this.currentComponentInsertionIndex) * 31) + this.total;
        }

        public final void setCurrentComponentInsertionIndex(int i) {
            this.currentComponentInsertionIndex = i;
        }

        public String toString() {
            StringBuilder R = a.R("ChildComponents(layoutComponentIndex=");
            R.append(this.layoutComponentIndex);
            R.append(", parentLayoutComponent=");
            R.append(this.parentLayoutComponent);
            R.append(", currentComponentInsertionIndex=");
            R.append(this.currentComponentInsertionIndex);
            R.append(", total=");
            return a.A(R, this.total, ")");
        }

        public /* synthetic */ ChildComponents(int i, LayoutComponent layoutComponent, int i2, int i3, int i4, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, (i4 & 2) != 0 ? null : layoutComponent, (i4 & 4) != 0 ? 0 : i2, i3);
        }
    }
}
