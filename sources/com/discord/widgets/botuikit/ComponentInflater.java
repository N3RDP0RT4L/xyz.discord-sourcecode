package com.discord.widgets.botuikit;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.ViewGroup;
import com.discord.api.botuikit.ComponentType;
import com.discord.models.botuikit.MessageComponent;
import com.discord.widgets.botuikit.views.ActionRowComponentView;
import com.discord.widgets.botuikit.views.ButtonComponentView;
import com.discord.widgets.botuikit.views.ComponentView;
import com.discord.widgets.botuikit.views.select.SelectComponentView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ComponentInflater.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\b\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0007\u0018\u00010\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u000b\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/botuikit/ComponentInflater;", "", "Lcom/discord/api/botuikit/ComponentType;", "component", "Landroid/view/ViewGroup;", "root", "Lcom/discord/widgets/botuikit/views/ComponentView;", "Lcom/discord/models/botuikit/MessageComponent;", "inflateComponent", "(Lcom/discord/api/botuikit/ComponentType;Landroid/view/ViewGroup;)Lcom/discord/widgets/botuikit/views/ComponentView;", "Landroid/content/Context;", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", HookHelper.constructorName, "(Landroid/content/Context;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ComponentInflater {
    public static final Companion Companion = new Companion(null);
    private final Context context;

    /* compiled from: ComponentInflater.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/botuikit/ComponentInflater$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/widgets/botuikit/ComponentInflater;", "from", "(Landroid/content/Context;)Lcom/discord/widgets/botuikit/ComponentInflater;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ComponentInflater from(Context context) {
            m.checkNotNullParameter(context, "context");
            return new ComponentInflater(context);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ComponentType.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[ComponentType.ACTION_ROW.ordinal()] = 1;
            iArr[ComponentType.BUTTON.ordinal()] = 2;
            iArr[ComponentType.SELECT.ordinal()] = 3;
            iArr[ComponentType.UNKNOWN.ordinal()] = 4;
        }
    }

    public ComponentInflater(Context context) {
        m.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final Context getContext() {
        return this.context;
    }

    public final ComponentView<? extends MessageComponent> inflateComponent(ComponentType componentType, ViewGroup viewGroup) {
        m.checkNotNullParameter(componentType, "component");
        m.checkNotNullParameter(viewGroup, "root");
        int ordinal = componentType.ordinal();
        if (ordinal == 0) {
            return null;
        }
        if (ordinal == 1) {
            return ActionRowComponentView.Companion.inflateComponent(this.context, viewGroup);
        }
        if (ordinal == 2) {
            return ButtonComponentView.Companion.inflateComponent(this.context, viewGroup);
        }
        if (ordinal == 3) {
            return SelectComponentView.Companion.inflateComponent(this.context, viewGroup);
        }
        throw new NoWhenBranchMatchedException();
    }
}
