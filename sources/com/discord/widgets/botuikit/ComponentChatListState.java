package com.discord.widgets.botuikit;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.botuikit.SelectItem;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import d0.t.h0;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: ComponentChatListState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u00030\u0002¢\u0006\u0004\b\u0007\u0010\b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/botuikit/ComponentChatListState;", "", "Lrx/Observable;", "", "", "Lcom/discord/primitives/MessageId;", "Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", "observeChatListComponentState", "()Lrx/Observable;", HookHelper.constructorName, "()V", "ComponentStoreState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ComponentChatListState {
    public static final ComponentChatListState INSTANCE = new ComponentChatListState();

    /* compiled from: ComponentChatListState.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001BO\u0012\u001a\b\u0002\u0010\u000e\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0002\u0012 \b\u0002\u0010\u000f\u001a\u001a\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u00010\u0002\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000b¢\u0006\u0004\b \u0010!J\"\u0010\u0006\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J(\u0010\n\u001a\u001a\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0007J\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJX\u0010\u0011\u001a\u00020\u00002\u001a\b\u0002\u0010\u000e\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00022 \b\u0002\u0010\u000f\u001a\u001a\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u00010\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0019\u001a\u00020\u000b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR+\u0010\u000e\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\rR1\u0010\u000f\u001a\u001a\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001b\u001a\u0004\b\u001f\u0010\u0007¨\u0006\""}, d2 = {"Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", "", "", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState;", "component1", "()Ljava/util/Map;", "", "Lcom/discord/api/botuikit/SelectItem;", "component2", "", "component3", "()Z", "interactionState", "selections", "animateEmojis", "copy", "(Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getInteractionState", "Z", "getAnimateEmojis", "getSelections", HookHelper.constructorName, "(Ljava/util/Map;Ljava/util/Map;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ComponentStoreState {
        private final boolean animateEmojis;
        private final Map<Integer, StoreApplicationInteractions.InteractionSendState> interactionState;
        private final Map<Integer, List<SelectItem>> selections;

        public ComponentStoreState() {
            this(null, null, false, 7, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ComponentStoreState(Map<Integer, ? extends StoreApplicationInteractions.InteractionSendState> map, Map<Integer, ? extends List<SelectItem>> map2, boolean z2) {
            this.interactionState = map;
            this.selections = map2;
            this.animateEmojis = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ComponentStoreState copy$default(ComponentStoreState componentStoreState, Map map, Map map2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                map = componentStoreState.interactionState;
            }
            if ((i & 2) != 0) {
                map2 = componentStoreState.selections;
            }
            if ((i & 4) != 0) {
                z2 = componentStoreState.animateEmojis;
            }
            return componentStoreState.copy(map, map2, z2);
        }

        public final Map<Integer, StoreApplicationInteractions.InteractionSendState> component1() {
            return this.interactionState;
        }

        public final Map<Integer, List<SelectItem>> component2() {
            return this.selections;
        }

        public final boolean component3() {
            return this.animateEmojis;
        }

        public final ComponentStoreState copy(Map<Integer, ? extends StoreApplicationInteractions.InteractionSendState> map, Map<Integer, ? extends List<SelectItem>> map2, boolean z2) {
            return new ComponentStoreState(map, map2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ComponentStoreState)) {
                return false;
            }
            ComponentStoreState componentStoreState = (ComponentStoreState) obj;
            return m.areEqual(this.interactionState, componentStoreState.interactionState) && m.areEqual(this.selections, componentStoreState.selections) && this.animateEmojis == componentStoreState.animateEmojis;
        }

        public final boolean getAnimateEmojis() {
            return this.animateEmojis;
        }

        public final Map<Integer, StoreApplicationInteractions.InteractionSendState> getInteractionState() {
            return this.interactionState;
        }

        public final Map<Integer, List<SelectItem>> getSelections() {
            return this.selections;
        }

        public int hashCode() {
            Map<Integer, StoreApplicationInteractions.InteractionSendState> map = this.interactionState;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            Map<Integer, List<SelectItem>> map2 = this.selections;
            if (map2 != null) {
                i = map2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.animateEmojis;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("ComponentStoreState(interactionState=");
            R.append(this.interactionState);
            R.append(", selections=");
            R.append(this.selections);
            R.append(", animateEmojis=");
            return a.M(R, this.animateEmojis, ")");
        }

        public /* synthetic */ ComponentStoreState(Map map, Map map2, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? h0.emptyMap() : map, (i & 2) != 0 ? h0.emptyMap() : map2, (i & 4) != 0 ? true : z2);
        }
    }

    private ComponentChatListState() {
    }

    public final Observable<Map<Long, ComponentStoreState>> observeChatListComponentState() {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Map<Long, ComponentStoreState>> i = Observable.i(companion.getInteractions().observeComponentInteractionState(), companion.getLocalActionComponentState().observeSelectComponentSelections(), StoreUserSettings.observeIsAnimatedEmojisEnabled$default(companion.getUserSettings(), false, 1, null), ComponentChatListState$observeChatListComponentState$1.INSTANCE);
        m.checkNotNullExpressionValue(i, "Observable.combineLatest…s\n        )\n      }\n    }");
        return i;
    }
}
