package com.discord.widgets.botuikit;

import com.discord.api.botuikit.ActionComponent;
import com.discord.api.botuikit.Component;
import com.discord.api.botuikit.LayoutComponent;
import com.discord.models.botuikit.MessageComponent;
import com.discord.widgets.botuikit.ComponentChatListState;
import com.discord.widgets.botuikit.ComponentStateMapper;
import d0.t.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Stack;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Ref$ObjectRef;
/* compiled from: ComponentStateMapper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "i", "Lcom/discord/api/botuikit/Component;", "component", "", "invoke", "(ILcom/discord/api/botuikit/Component;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ComponentStateMapper$processComponentsToMessageComponents$1 extends o implements Function2<Integer, Component, Unit> {
    public final /* synthetic */ Stack $childComponentsLevelStack;
    public final /* synthetic */ Ref$ObjectRef $childrenComponents;
    public final /* synthetic */ ComponentExperiments $componentExperiments;
    public final /* synthetic */ ComponentChatListState.ComponentStoreState $componentState;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ComponentStateMapper$processComponentsToMessageComponents$1(ComponentChatListState.ComponentStoreState componentStoreState, ComponentExperiments componentExperiments, Ref$ObjectRef ref$ObjectRef, Stack stack) {
        super(2);
        this.$componentState = componentStoreState;
        this.$componentExperiments = componentExperiments;
        this.$childrenComponents = ref$ObjectRef;
        this.$childComponentsLevelStack = stack;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Integer num, Component component) {
        invoke(num.intValue(), component);
        return Unit.a;
    }

    /* JADX WARN: Type inference failed for: r0v8, types: [T, com.discord.widgets.botuikit.ComponentStateMapper$ChildComponents] */
    /* JADX WARN: Type inference failed for: r8v0, types: [T, com.discord.widgets.botuikit.ComponentStateMapper$ChildComponents] */
    public final void invoke(int i, Component component) {
        LayoutComponent parentLayoutComponent;
        m.checkNotNullParameter(component, "component");
        if (component instanceof ActionComponent) {
            MessageComponent createActionMessageComponent = ComponentStateMapper.INSTANCE.createActionMessageComponent((ActionComponent) component, i, this.$componentState, this.$componentExperiments);
            if (createActionMessageComponent != null) {
                ((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getComponents()[((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getCurrentComponentInsertionIndex()] = createActionMessageComponent;
            }
            ComponentStateMapper.ChildComponents childComponents = (ComponentStateMapper.ChildComponents) this.$childrenComponents.element;
            childComponents.setCurrentComponentInsertionIndex(childComponents.getCurrentComponentInsertionIndex() + 1);
        } else if (component instanceof LayoutComponent) {
            this.$childComponentsLevelStack.push((ComponentStateMapper.ChildComponents) this.$childrenComponents.element);
            LayoutComponent layoutComponent = (LayoutComponent) component;
            this.$childrenComponents.element = new ComponentStateMapper.ChildComponents(i, layoutComponent, 0, layoutComponent.a().size(), 4, null);
        }
        while (((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getCurrentComponentInsertionIndex() == ((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getTotal() && (parentLayoutComponent = ((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getParentLayoutComponent()) != null) {
            MessageComponent messageLayoutComponent = ComponentStateMapper.INSTANCE.toMessageLayoutComponent(parentLayoutComponent, ((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getLayoutComponentIndex(), k.filterNotNull(((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getComponents()), this.$componentExperiments);
            Ref$ObjectRef ref$ObjectRef = this.$childrenComponents;
            Object pop = this.$childComponentsLevelStack.pop();
            m.checkNotNullExpressionValue(pop, "childComponentsLevelStack.pop()");
            ref$ObjectRef.element = (ComponentStateMapper.ChildComponents) pop;
            ((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getComponents()[((ComponentStateMapper.ChildComponents) this.$childrenComponents.element).getCurrentComponentInsertionIndex()] = messageLayoutComponent;
            ComponentStateMapper.ChildComponents childComponents2 = (ComponentStateMapper.ChildComponents) this.$childrenComponents.element;
            childComponents2.setCurrentComponentInsertionIndex(childComponents2.getCurrentComponentInsertionIndex() + 1);
        }
    }
}
