package com.discord.widgets.botuikit;

import androidx.core.app.NotificationCompat;
import com.discord.api.botuikit.SelectItem;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.widgets.botuikit.ComponentChatListState;
import d0.d0.f;
import d0.t.g0;
import d0.t.o;
import d0.t.o0;
import d0.t.u;
import d0.z.d.m;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.functions.Func3;
/* compiled from: ComponentChatListState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0010\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\r \u0006*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\r\u0018\u00010\u00000\u00002N\u0010\u0007\u001aJ\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0000 \u0006*$\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0000\u0018\u00010\u00000\u00002Z\u0010\n\u001aV\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0000 \u0006**\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0000\u0018\u00010\u00000\u00002\u000e\u0010\f\u001a\n \u0006*\u0004\u0018\u00010\u000b0\u000bH\n¢\u0006\u0004\b\u000e\u0010\u000f"}, d2 = {"", "", "Lcom/discord/primitives/MessageId;", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState;", "kotlin.jvm.PlatformType", "interactions", "", "Lcom/discord/api/botuikit/SelectItem;", "selections", "", "animateEmojis", "Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Boolean;)Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ComponentChatListState$observeChatListComponentState$1<T1, T2, T3, R> implements Func3<Map<Long, ? extends Map<Integer, ? extends StoreApplicationInteractions.InteractionSendState>>, Map<Long, ? extends Map<Integer, ? extends List<? extends SelectItem>>>, Boolean, Map<Long, ? extends ComponentChatListState.ComponentStoreState>> {
    public static final ComponentChatListState$observeChatListComponentState$1 INSTANCE = new ComponentChatListState$observeChatListComponentState$1();

    @Override // rx.functions.Func3
    public /* bridge */ /* synthetic */ Map<Long, ? extends ComponentChatListState.ComponentStoreState> call(Map<Long, ? extends Map<Integer, ? extends StoreApplicationInteractions.InteractionSendState>> map, Map<Long, ? extends Map<Integer, ? extends List<? extends SelectItem>>> map2, Boolean bool) {
        return call2(map, (Map<Long, ? extends Map<Integer, ? extends List<SelectItem>>>) map2, bool);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Map<Long, ComponentChatListState.ComponentStoreState> call2(Map<Long, ? extends Map<Integer, ? extends StoreApplicationInteractions.InteractionSendState>> map, Map<Long, ? extends Map<Integer, ? extends List<SelectItem>>> map2, Boolean bool) {
        List distinct = u.distinct(o0.plus((Set) map.keySet(), (Iterable) map2.keySet()));
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(distinct, 10)), 16));
        for (Object obj : distinct) {
            long longValue = ((Number) obj).longValue();
            m.checkNotNullExpressionValue(bool, "animateEmojis");
            linkedHashMap.put(obj, new ComponentChatListState.ComponentStoreState(map.get(Long.valueOf(longValue)), map2.get(Long.valueOf(longValue)), bool.booleanValue()));
        }
        return linkedHashMap;
    }
}
