package com.discord.widgets.debugging;

import androidx.core.app.NotificationCompat;
import com.discord.app.AppLog;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: WidgetDebugging.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0004\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u00002*\u0010\u0005\u001a&\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001 \u0002*\u0012\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00000\u0004H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/app/AppLog$LoggedItem;", "kotlin.jvm.PlatformType", "existingLogs", "", "newLogs", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Ljava/util/List;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDebugging$onViewBoundOrOnResume$2<T1, T2, R> implements Func2<List<? extends AppLog.LoggedItem>, List<AppLog.LoggedItem>, List<? extends AppLog.LoggedItem>> {
    public static final WidgetDebugging$onViewBoundOrOnResume$2 INSTANCE = new WidgetDebugging$onViewBoundOrOnResume$2();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ List<? extends AppLog.LoggedItem> call(List<? extends AppLog.LoggedItem> list, List<AppLog.LoggedItem> list2) {
        return call2((List<AppLog.LoggedItem>) list, list2);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<AppLog.LoggedItem> call2(List<AppLog.LoggedItem> list, List<AppLog.LoggedItem> list2) {
        m.checkNotNullExpressionValue(list, "existingLogs");
        m.checkNotNullExpressionValue(list2, "newLogs");
        return u.plus((Collection) list, (Iterable) list2);
    }
}
