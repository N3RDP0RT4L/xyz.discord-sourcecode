package com.discord.widgets.debugging;

import androidx.core.app.NotificationCompat;
import com.discord.app.AppLog;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
/* compiled from: WidgetDebugging.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\b\u001a\n \u0002*\u0004\u0018\u00010\u00050\u00052*\u0010\u0004\u001a&\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001 \u0002*\u0012\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00030\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/app/AppLog$LoggedItem;", "kotlin.jvm.PlatformType", "", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDebugging$onViewBoundOrOnResume$1<T, R> implements b<List<AppLog.LoggedItem>, Boolean> {
    public static final WidgetDebugging$onViewBoundOrOnResume$1 INSTANCE = new WidgetDebugging$onViewBoundOrOnResume$1();

    public final Boolean call(List<AppLog.LoggedItem> list) {
        m.checkNotNullExpressionValue(list, "it");
        return Boolean.valueOf(!list.isEmpty());
    }
}
