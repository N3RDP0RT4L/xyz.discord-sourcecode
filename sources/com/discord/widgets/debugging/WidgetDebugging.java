package com.discord.widgets.debugging;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetDebuggingAdapterItemBinding;
import com.discord.databinding.WidgetDebuggingBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.debugging.WidgetDebugging;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetDebugging.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u001c2\u00020\u0001:\u0003\u001d\u001c\u001eB\u0007¢\u0006\u0004\b\u001b\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR:\u0010\u0010\u001a&\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e \u000f*\u0012\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\r0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001a\u001a\u00020\u00158B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/debugging/WidgetDebugging;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/debugging/WidgetDebugging$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/debugging/WidgetDebugging$Model;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lrx/subjects/BehaviorSubject;", "", "kotlin.jvm.PlatformType", "filterSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/widgets/debugging/WidgetDebugging$Adapter;", "logsAdapter", "Lcom/discord/widgets/debugging/WidgetDebugging$Adapter;", "Lcom/discord/databinding/WidgetDebuggingBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetDebuggingBinding;", "binding", HookHelper.constructorName, "Companion", "Adapter", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDebugging extends AppFragment {
    private static final int COLLAPSED_MAX_LINES = 2;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetDebugging$binding$2.INSTANCE, null, 2, null);
    private final BehaviorSubject<Boolean> filterSubject = BehaviorSubject.l0(Boolean.TRUE);
    private Adapter logsAdapter;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetDebugging.class, "binding", "getBinding()Lcom/discord/databinding/WidgetDebuggingBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetDebugging.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J)\u0010\u000b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\n2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/debugging/WidgetDebugging$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/app/AppLog$LoggedItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "getItem", "(I)Lcom/discord/app/AppLog$LoggedItem;", "Landroid/view/ViewGroup;", "parent", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "Item", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Adapter extends MGRecyclerAdapterSimple<AppLog.LoggedItem> {

        /* compiled from: WidgetDebugging.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u00002\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/app/AppLog$LoggedItem;", "<anonymous parameter 0>", "<anonymous parameter 1>", "", "invoke", "(Ljava/util/List;Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.debugging.WidgetDebugging$Adapter$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends o implements Function2<List<? extends AppLog.LoggedItem>, List<? extends AppLog.LoggedItem>, Unit> {
            public final /* synthetic */ RecyclerView $recycler;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(RecyclerView recyclerView) {
                super(2);
                this.$recycler = recyclerView;
            }

            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(List<? extends AppLog.LoggedItem> list, List<? extends AppLog.LoggedItem> list2) {
                invoke2((List<AppLog.LoggedItem>) list, (List<AppLog.LoggedItem>) list2);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(List<AppLog.LoggedItem> list, List<AppLog.LoggedItem> list2) {
                m.checkNotNullParameter(list, "<anonymous parameter 0>");
                m.checkNotNullParameter(list2, "<anonymous parameter 1>");
                RecyclerView.LayoutManager layoutManager = this.$recycler.getLayoutManager();
                Objects.requireNonNull(layoutManager, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                if (((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition() == 0) {
                    this.$recycler.scrollToPosition(0);
                }
            }
        }

        /* compiled from: WidgetDebugging.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0019\u0012\b\b\u0001\u0010\u0012\u001a\u00020\u0006\u0012\u0006\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\b\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\r\u001a\u00020\f2\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0003H\u0015¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/debugging/WidgetDebugging$Adapter;", "Lcom/discord/app/AppLog$LoggedItem;", "Landroid/content/Context;", "context", "", "priority", "getColor", "(Landroid/content/Context;I)I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/app/AppLog$LoggedItem;)V", "Lcom/discord/databinding/WidgetDebuggingAdapterItemBinding;", "binding", "Lcom/discord/databinding/WidgetDebuggingAdapterItemBinding;", "layout", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/debugging/WidgetDebugging$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Item extends MGRecyclerViewHolder<Adapter, AppLog.LoggedItem> {
            private final WidgetDebuggingAdapterItemBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Item(@LayoutRes int i, Adapter adapter) {
                super(i, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                View view = this.itemView;
                TextView textView = (TextView) view.findViewById(R.id.log_message);
                if (textView != null) {
                    WidgetDebuggingAdapterItemBinding widgetDebuggingAdapterItemBinding = new WidgetDebuggingAdapterItemBinding((LinearLayout) view, textView);
                    m.checkNotNullExpressionValue(widgetDebuggingAdapterItemBinding, "WidgetDebuggingAdapterItemBinding.bind(itemView)");
                    this.binding = widgetDebuggingAdapterItemBinding;
                    return;
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.log_message)));
            }

            @ColorInt
            private final int getColor(Context context, int i) {
                if (i == 2) {
                    return ColorCompat.getThemedColor(context, (int) R.attr.primary_300);
                }
                if (i == 3) {
                    return ColorCompat.getColor(context, (int) R.color.status_green_600);
                }
                if (i == 5) {
                    return ColorCompat.getColor(context, (int) R.color.status_yellow_500);
                }
                if (i != 6) {
                    return ColorCompat.getThemedColor(context, (int) R.attr.primary_100);
                }
                return ColorCompat.getColor(context, (int) R.color.status_red_500);
            }

            /* JADX WARN: Code restructure failed: missing block: B:5:0x0049, code lost:
                if (r6 != null) goto L7;
             */
            @android.annotation.SuppressLint({"SetTextI18n"})
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public void onConfigure(int r5, com.discord.app.AppLog.LoggedItem r6) {
                /*
                    r4 = this;
                    java.lang.String r0 = "data"
                    d0.z.d.m.checkNotNullParameter(r6, r0)
                    super.onConfigure(r5, r6)
                    com.discord.databinding.WidgetDebuggingAdapterItemBinding r5 = r4.binding
                    android.widget.TextView r5 = r5.f2341b
                    java.lang.String r0 = "binding.logMessage"
                    d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                    android.content.Context r1 = r5.getContext()
                    java.lang.String r2 = "binding.logMessage.context"
                    d0.z.d.m.checkNotNullExpressionValue(r1, r2)
                    int r2 = r6.k
                    int r1 = r4.getColor(r1, r2)
                    r5.setTextColor(r1)
                    com.discord.databinding.WidgetDebuggingAdapterItemBinding r5 = r4.binding
                    android.widget.TextView r5 = r5.f2341b
                    d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    r1.<init>()
                    java.lang.String r2 = r6.l
                    r1.append(r2)
                    java.lang.Throwable r6 = r6.m
                    if (r6 == 0) goto L4c
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    r3 = 10
                    r2.append(r3)
                    r2.append(r6)
                    java.lang.String r6 = r2.toString()
                    if (r6 == 0) goto L4c
                    goto L4e
                L4c:
                    java.lang.String r6 = ""
                L4e:
                    r1.append(r6)
                    java.lang.String r6 = r1.toString()
                    r5.setText(r6)
                    com.discord.databinding.WidgetDebuggingAdapterItemBinding r5 = r4.binding
                    android.widget.TextView r5 = r5.f2341b
                    d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                    r6 = 2
                    r5.setMaxLines(r6)
                    com.discord.databinding.WidgetDebuggingAdapterItemBinding r5 = r4.binding
                    android.widget.LinearLayout r5 = r5.a
                    java.lang.String r6 = "binding.root"
                    d0.z.d.m.checkNotNullExpressionValue(r5, r6)
                    com.discord.widgets.debugging.WidgetDebugging$Adapter$Item$onConfigure$2 r6 = new com.discord.widgets.debugging.WidgetDebugging$Adapter$Item$onConfigure$2
                    r6.<init>(r4)
                    com.discord.utilities.view.extensions.ViewExtensions.setOnLongClickListenerConsumeClick(r5, r6)
                    android.view.View r5 = r4.itemView
                    com.discord.widgets.debugging.WidgetDebugging$Adapter$Item$onConfigure$3 r6 = new com.discord.widgets.debugging.WidgetDebugging$Adapter$Item$onConfigure$3
                    r6.<init>()
                    r5.setOnClickListener(r6)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.debugging.WidgetDebugging.Adapter.Item.onConfigure(int, com.discord.app.AppLog$LoggedItem):void");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Adapter(RecyclerView recyclerView) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recycler");
            setOnUpdated(new AnonymousClass1(recyclerView));
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public MGRecyclerViewHolder<?, AppLog.LoggedItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            if (i == 0) {
                return new Item(R.layout.widget_debugging_adapter_item, this);
            }
            throw invalidViewTypeException(i);
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple, com.discord.utilities.mg_recycler.MGRecyclerAdapter
        public AppLog.LoggedItem getItem(int i) {
            return getInternalData().get((getInternalData().size() - 1) - i);
        }
    }

    /* compiled from: WidgetDebugging.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/debugging/WidgetDebugging$Companion;", "", "Landroid/content/Context;", "context", "", "launch", "(Landroid/content/Context;)V", "", "COLLAPSED_MAX_LINES", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetDebugging.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetDebugging.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0082\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0014\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\n\u0010\b¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/debugging/WidgetDebugging$Model;", "", "", "Lcom/discord/app/AppLog$LoggedItem;", "component1", "()Ljava/util/List;", "", "component2", "()Z", "logs", "isFiltered", "copy", "(Ljava/util/List;Z)Lcom/discord/widgets/debugging/WidgetDebugging$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getLogs", "Z", HookHelper.constructorName, "(Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        private final boolean isFiltered;
        private final List<AppLog.LoggedItem> logs;

        public Model(List<AppLog.LoggedItem> list, boolean z2) {
            m.checkNotNullParameter(list, "logs");
            this.logs = list;
            this.isFiltered = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Model copy$default(Model model, List list, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = model.logs;
            }
            if ((i & 2) != 0) {
                z2 = model.isFiltered;
            }
            return model.copy(list, z2);
        }

        public final List<AppLog.LoggedItem> component1() {
            return this.logs;
        }

        public final boolean component2() {
            return this.isFiltered;
        }

        public final Model copy(List<AppLog.LoggedItem> list, boolean z2) {
            m.checkNotNullParameter(list, "logs");
            return new Model(list, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.logs, model.logs) && this.isFiltered == model.isFiltered;
        }

        public final List<AppLog.LoggedItem> getLogs() {
            return this.logs;
        }

        public int hashCode() {
            List<AppLog.LoggedItem> list = this.logs;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            boolean z2 = this.isFiltered;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public final boolean isFiltered() {
            return this.isFiltered;
        }

        public String toString() {
            StringBuilder R = a.R("Model(logs=");
            R.append(this.logs);
            R.append(", isFiltered=");
            return a.M(R, this.isFiltered, ")");
        }
    }

    public WidgetDebugging() {
        super(R.layout.widget_debugging);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        Adapter adapter = this.logsAdapter;
        if (adapter != null) {
            adapter.setData(model.getLogs());
        }
        setActionBarOptionsMenu(R.menu.menu_debugging, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.debugging.WidgetDebugging$configureUI$1
            public final void call(MenuItem menuItem, Context context) {
                BehaviorSubject behaviorSubject;
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_debugging_filter) {
                    boolean z2 = !menuItem.isChecked();
                    menuItem.setChecked(z2);
                    behaviorSubject = WidgetDebugging.this.filterSubject;
                    behaviorSubject.onNext(Boolean.valueOf(z2));
                }
            }
        }, new Action1<Menu>() { // from class: com.discord.widgets.debugging.WidgetDebugging$configureUI$2
            public final void call(Menu menu) {
                MenuItem findItem = menu.findItem(R.id.menu_debugging_filter);
                m.checkNotNullExpressionValue(findItem, "menu.findItem(R.id.menu_debugging_filter)");
                findItem.setChecked(WidgetDebugging.Model.this.isFiltered());
            }
        });
    }

    private final WidgetDebuggingBinding getBinding() {
        return (WidgetDebuggingBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2342b;
        m.checkNotNullExpressionValue(recyclerView, "binding.debuggingLogs");
        this.logsAdapter = (Adapter) companion.configure(new Adapter(recyclerView));
        RecyclerView recyclerView2 = getBinding().f2342b;
        m.checkNotNullExpressionValue(recyclerView2, "binding.debuggingLogs");
        RecyclerView.LayoutManager layoutManager = recyclerView2.getLayoutManager();
        Objects.requireNonNull(layoutManager, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setSmoothScrollbarEnabled(true);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.debug);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Objects.requireNonNull(AppLog.g);
        Observable q = ObservableExtensionsKt.computationBuffered(AppLog.d).q();
        m.checkNotNullExpressionValue(q, "logsSubject\n          .c…  .distinctUntilChanged()");
        Observable j = Observable.j(q.a(200L, TimeUnit.MILLISECONDS).x(WidgetDebugging$onViewBoundOrOnResume$1.INSTANCE).P(new ArrayList(), WidgetDebugging$onViewBoundOrOnResume$2.INSTANCE), this.filterSubject.q(), WidgetDebugging$onViewBoundOrOnResume$3.INSTANCE);
        m.checkNotNullExpressionValue(j, "Observable\n        .comb…gs, isFiltered)\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationBuffered(j), this, null, 2, null), WidgetDebugging.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetDebugging$onViewBoundOrOnResume$4(this));
    }
}
