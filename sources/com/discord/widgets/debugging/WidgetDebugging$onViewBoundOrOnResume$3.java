package com.discord.widgets.debugging;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.widgets.debugging.WidgetDebugging;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: WidgetDebugging.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\n \u0002*\u0004\u0018\u00010\u00060\u00062\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u00002\u000e\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00040\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "Lcom/discord/app/AppLog$LoggedItem;", "kotlin.jvm.PlatformType", "logs", "", "isFiltered", "Lcom/discord/widgets/debugging/WidgetDebugging$Model;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Ljava/lang/Boolean;)Lcom/discord/widgets/debugging/WidgetDebugging$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetDebugging$onViewBoundOrOnResume$3<T1, T2, R> implements Func2<List<? extends AppLog.LoggedItem>, Boolean, WidgetDebugging.Model> {
    public static final WidgetDebugging$onViewBoundOrOnResume$3 INSTANCE = new WidgetDebugging$onViewBoundOrOnResume$3();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ WidgetDebugging.Model call(List<? extends AppLog.LoggedItem> list, Boolean bool) {
        return call2((List<AppLog.LoggedItem>) list, bool);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final WidgetDebugging.Model call2(List<AppLog.LoggedItem> list, Boolean bool) {
        m.checkNotNullExpressionValue(bool, "isFiltered");
        if (bool.booleanValue()) {
            ArrayList Y = a.Y(list, "logs");
            for (Object obj : list) {
                if (((AppLog.LoggedItem) obj).k > 2) {
                    Y.add(obj);
                }
            }
            list = Y;
        }
        m.checkNotNullExpressionValue(list, "filteredLogs");
        return new WidgetDebugging.Model(list, bool.booleanValue());
    }
}
