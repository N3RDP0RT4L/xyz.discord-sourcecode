package com.discord.widgets.guildscheduledevent;

import androidx.core.app.NotificationCompat;
import com.discord.api.role.GuildRole;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventDescriptionParser;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import rx.functions.Func3;
/* compiled from: GuildScheduledEventDescriptionParser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0010\u001a\n \u0004*\u0004\u0018\u00010\r0\r2.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u00002±\u0001\u0010\t\u001a¬\u0001\u0012\u0016\u0012\u0014 \u0004*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00070\u0001j\u0002`\u0007\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*V\u0012\u0016\u0012\u0014 \u0004*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00070\u0001j\u0002`\u0007\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u0006j*\u0012\u0016\u0012\u0014 \u0004*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00070\u0001j\u0002`\u0007\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u0001`\b0\u0006j(\u0012\u0016\u0012\u0014 \u0004*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00070\u0001j\u0002`\u0007\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003`\b2.\u0010\f\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\n\u0012\u0004\u0012\u00020\u000b \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u000e\u0010\u000f"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "", "kotlin.jvm.PlatformType", "channelNames", "Ljava/util/HashMap;", "Lcom/discord/primitives/UserId;", "Lkotlin/collections/HashMap;", "userNames", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "roles", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser$MessageRenderContextModel;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;Ljava/util/HashMap;Ljava/util/Map;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser$MessageRenderContextModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventDescriptionParser$getRenderContext$2<T1, T2, T3, R> implements Func3<Map<Long, ? extends String>, HashMap<Long, String>, Map<Long, ? extends GuildRole>, GuildScheduledEventDescriptionParser.MessageRenderContextModel> {
    public static final GuildScheduledEventDescriptionParser$getRenderContext$2 INSTANCE = new GuildScheduledEventDescriptionParser$getRenderContext$2();

    @Override // rx.functions.Func3
    public /* bridge */ /* synthetic */ GuildScheduledEventDescriptionParser.MessageRenderContextModel call(Map<Long, ? extends String> map, HashMap<Long, String> hashMap, Map<Long, ? extends GuildRole> map2) {
        return call2((Map<Long, String>) map, hashMap, (Map<Long, GuildRole>) map2);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final GuildScheduledEventDescriptionParser.MessageRenderContextModel call2(Map<Long, String> map, HashMap<Long, String> hashMap, Map<Long, GuildRole> map2) {
        return new GuildScheduledEventDescriptionParser.MessageRenderContextModel(map, hashMap, map2);
    }
}
