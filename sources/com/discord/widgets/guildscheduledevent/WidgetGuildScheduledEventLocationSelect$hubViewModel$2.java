package com.discord.widgets.guildscheduledevent;

import com.discord.app.AppViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildScheduledEventLocationSelect.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventLocationSelect$hubViewModel$2 extends o implements Function0<AppViewModel<GuildScheduledEventDirectoryAssociationState>> {
    public final /* synthetic */ WidgetGuildScheduledEventLocationSelect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildScheduledEventLocationSelect$hubViewModel$2(WidgetGuildScheduledEventLocationSelect widgetGuildScheduledEventLocationSelect) {
        super(0);
        this.this$0 = widgetGuildScheduledEventLocationSelect;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<GuildScheduledEventDirectoryAssociationState> invoke() {
        long guildId;
        Long existingGuildScheduledEventId;
        guildId = this.this$0.getGuildId();
        existingGuildScheduledEventId = this.this$0.getExistingGuildScheduledEventId();
        return new GuildScheduledEventDirectoryAssociationViewModel(guildId, existingGuildScheduledEventId, null, null, 12, null);
    }
}
