package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.permission.Permission;
import com.discord.app.AppViewModel;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventListItem;
import d0.t.o;
import d0.z.d.k;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: GuildScheduledEventListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 02\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003012Bo\u0012\n\u0010\u001e\u001a\u00060\u001cj\u0002`\u001d\u0012\u000e\u0010$\u001a\n\u0018\u00010\u001cj\u0004\u0018\u0001`#\u0012\b\b\u0002\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u0010!\u001a\u00020 \u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0013\u0012\b\b\u0002\u0010*\u001a\u00020)\u0012\u000e\b\u0002\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00030,¢\u0006\u0004\b.\u0010/J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bJ1\u0010\u0011\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\f¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u001a\u0010\u001e\u001a\u00060\u001cj\u0002`\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u001e\u0010$\u001a\n\u0018\u00010\u001cj\u0004\u0018\u0001`#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+¨\u00063"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;)V", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "toggleRsvp", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "Ljava/lang/ref/WeakReference;", "Landroid/content/Context;", "weakContext", "Landroidx/fragment/app/Fragment;", "weakFragment", "onShareClicked", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuilds;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/primitives/ChannelId;", "channelId", "Ljava/lang/Long;", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(JLjava/lang/Long;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUser;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventListViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final Long channelId;
    private final StoreChannels channelsStore;
    private final long guildId;
    private final StoreGuildScheduledEvents guildScheduledEventsStore;
    private final StoreGuilds guildsStore;
    private final StorePermissions permissionsStore;
    private final StoreUser userStore;
    private final StoreVoiceChannelSelected voiceChannelSelectedStore;

    /* compiled from: GuildScheduledEventListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guildscheduledevent.GuildScheduledEventListViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(GuildScheduledEventListViewModel guildScheduledEventListViewModel) {
            super(1, guildScheduledEventListViewModel, GuildScheduledEventListViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((GuildScheduledEventListViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: GuildScheduledEventListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018Ja\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreUser;", "userStore", "Lrx/Observable;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;", "observeStores", "(JLjava/lang/Long;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUser;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(long j, Long l, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreChannels storeChannels, StorePermissions storePermissions, StoreGuilds storeGuilds, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreUser storeUser) {
            return ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{storeGuildScheduledEvents, storeChannels, storePermissions, storeGuilds, storeVoiceChannelSelected, storeUser}, false, null, null, new GuildScheduledEventListViewModel$Companion$observeStores$1(storeChannels, j, storeGuildScheduledEvents, l, storeUser, storeGuilds, storePermissions, storeVoiceChannelSelected), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildScheduledEventListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001B\u008b\u0001\u0012\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0010\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u0006\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000b\u0012\u0016\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000b\u0012\u0006\u0010\u001e\u001a\u00020\u0013\u0012\u000e\u0010\u001f\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\f\u0012\u001a\u0010 \u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\b\u0012\u00060\u0007j\u0002`\u00180\u000b¢\u0006\u0004\b8\u00109J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\t\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ \u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000fJ\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0018\u0010\u0016\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J$\u0010\u0019\u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\b\u0012\u00060\u0007j\u0002`\u00180\u000bHÆ\u0003¢\u0006\u0004\b\u0019\u0010\u000fJ¢\u0001\u0010!\u001a\u00020\u00002\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00062\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000b2\u0018\b\u0002\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000b2\b\b\u0002\u0010\u001e\u001a\u00020\u00132\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\f2\u001c\b\u0002\u0010 \u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\b\u0012\u00060\u0007j\u0002`\u00180\u000bHÆ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010'\u001a\u00020&HÖ\u0001¢\u0006\u0004\b'\u0010(J\u001a\u0010*\u001a\u00020\u00132\b\u0010)\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b*\u0010+R-\u0010 \u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\b\u0012\u00060\u0007j\u0002`\u00180\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010,\u001a\u0004\b-\u0010\u000fR!\u0010\u001f\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010.\u001a\u0004\b/\u0010\u0017R)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010,\u001a\u0004\b0\u0010\u000fR)\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010,\u001a\u0004\b1\u0010\u000fR#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00102\u001a\u0004\b3\u0010\nR\u0019\u0010\u001e\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00104\u001a\u0004\b5\u0010\u0015R\u001f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00106\u001a\u0004\b7\u0010\u0005¨\u0006:"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;", "", "", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Ljava/util/List;", "", "", "Lcom/discord/primitives/GuildScheduledEventId;", "component2", "()Ljava/util/Set;", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component3", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/guild/UserGuildMember;", "component4", "", "component5", "()Z", "component6", "()Ljava/lang/Long;", "Lcom/discord/api/permission/PermissionBit;", "component7", "guildScheduledEvents", "userGuildScheduledEventIds", "guildChannels", "creators", "canCreateEvents", "selectedVoiceChannelId", "channelPermissions", "copy", "(Ljava/util/List;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/Long;Ljava/util/Map;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getChannelPermissions", "Ljava/lang/Long;", "getSelectedVoiceChannelId", "getGuildChannels", "getCreators", "Ljava/util/Set;", "getUserGuildScheduledEventIds", "Z", "getCanCreateEvents", "Ljava/util/List;", "getGuildScheduledEvents", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/Long;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final boolean canCreateEvents;
        private final Map<Long, Long> channelPermissions;
        private final Map<Long, UserGuildMember> creators;
        private final Map<Long, Channel> guildChannels;
        private final List<GuildScheduledEvent> guildScheduledEvents;
        private final Long selectedVoiceChannelId;
        private final Set<Long> userGuildScheduledEventIds;

        public StoreState(List<GuildScheduledEvent> list, Set<Long> set, Map<Long, Channel> map, Map<Long, UserGuildMember> map2, boolean z2, Long l, Map<Long, Long> map3) {
            m.checkNotNullParameter(list, "guildScheduledEvents");
            m.checkNotNullParameter(set, "userGuildScheduledEventIds");
            m.checkNotNullParameter(map, "guildChannels");
            m.checkNotNullParameter(map2, "creators");
            m.checkNotNullParameter(map3, "channelPermissions");
            this.guildScheduledEvents = list;
            this.userGuildScheduledEventIds = set;
            this.guildChannels = map;
            this.creators = map2;
            this.canCreateEvents = z2;
            this.selectedVoiceChannelId = l;
            this.channelPermissions = map3;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, List list, Set set, Map map, Map map2, boolean z2, Long l, Map map3, int i, Object obj) {
            List<GuildScheduledEvent> list2 = list;
            if ((i & 1) != 0) {
                list2 = storeState.guildScheduledEvents;
            }
            Set<Long> set2 = set;
            if ((i & 2) != 0) {
                set2 = storeState.userGuildScheduledEventIds;
            }
            Set set3 = set2;
            Map<Long, Channel> map4 = map;
            if ((i & 4) != 0) {
                map4 = storeState.guildChannels;
            }
            Map map5 = map4;
            Map<Long, UserGuildMember> map6 = map2;
            if ((i & 8) != 0) {
                map6 = storeState.creators;
            }
            Map map7 = map6;
            if ((i & 16) != 0) {
                z2 = storeState.canCreateEvents;
            }
            boolean z3 = z2;
            if ((i & 32) != 0) {
                l = storeState.selectedVoiceChannelId;
            }
            Long l2 = l;
            Map<Long, Long> map8 = map3;
            if ((i & 64) != 0) {
                map8 = storeState.channelPermissions;
            }
            return storeState.copy(list2, set3, map5, map7, z3, l2, map8);
        }

        public final List<GuildScheduledEvent> component1() {
            return this.guildScheduledEvents;
        }

        public final Set<Long> component2() {
            return this.userGuildScheduledEventIds;
        }

        public final Map<Long, Channel> component3() {
            return this.guildChannels;
        }

        public final Map<Long, UserGuildMember> component4() {
            return this.creators;
        }

        public final boolean component5() {
            return this.canCreateEvents;
        }

        public final Long component6() {
            return this.selectedVoiceChannelId;
        }

        public final Map<Long, Long> component7() {
            return this.channelPermissions;
        }

        public final StoreState copy(List<GuildScheduledEvent> list, Set<Long> set, Map<Long, Channel> map, Map<Long, UserGuildMember> map2, boolean z2, Long l, Map<Long, Long> map3) {
            m.checkNotNullParameter(list, "guildScheduledEvents");
            m.checkNotNullParameter(set, "userGuildScheduledEventIds");
            m.checkNotNullParameter(map, "guildChannels");
            m.checkNotNullParameter(map2, "creators");
            m.checkNotNullParameter(map3, "channelPermissions");
            return new StoreState(list, set, map, map2, z2, l, map3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guildScheduledEvents, storeState.guildScheduledEvents) && m.areEqual(this.userGuildScheduledEventIds, storeState.userGuildScheduledEventIds) && m.areEqual(this.guildChannels, storeState.guildChannels) && m.areEqual(this.creators, storeState.creators) && this.canCreateEvents == storeState.canCreateEvents && m.areEqual(this.selectedVoiceChannelId, storeState.selectedVoiceChannelId) && m.areEqual(this.channelPermissions, storeState.channelPermissions);
        }

        public final boolean getCanCreateEvents() {
            return this.canCreateEvents;
        }

        public final Map<Long, Long> getChannelPermissions() {
            return this.channelPermissions;
        }

        public final Map<Long, UserGuildMember> getCreators() {
            return this.creators;
        }

        public final Map<Long, Channel> getGuildChannels() {
            return this.guildChannels;
        }

        public final List<GuildScheduledEvent> getGuildScheduledEvents() {
            return this.guildScheduledEvents;
        }

        public final Long getSelectedVoiceChannelId() {
            return this.selectedVoiceChannelId;
        }

        public final Set<Long> getUserGuildScheduledEventIds() {
            return this.userGuildScheduledEventIds;
        }

        public int hashCode() {
            List<GuildScheduledEvent> list = this.guildScheduledEvents;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            Set<Long> set = this.userGuildScheduledEventIds;
            int hashCode2 = (hashCode + (set != null ? set.hashCode() : 0)) * 31;
            Map<Long, Channel> map = this.guildChannels;
            int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, UserGuildMember> map2 = this.creators;
            int hashCode4 = (hashCode3 + (map2 != null ? map2.hashCode() : 0)) * 31;
            boolean z2 = this.canCreateEvents;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode4 + i2) * 31;
            Long l = this.selectedVoiceChannelId;
            int hashCode5 = (i4 + (l != null ? l.hashCode() : 0)) * 31;
            Map<Long, Long> map3 = this.channelPermissions;
            if (map3 != null) {
                i = map3.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guildScheduledEvents=");
            R.append(this.guildScheduledEvents);
            R.append(", userGuildScheduledEventIds=");
            R.append(this.userGuildScheduledEventIds);
            R.append(", guildChannels=");
            R.append(this.guildChannels);
            R.append(", creators=");
            R.append(this.creators);
            R.append(", canCreateEvents=");
            R.append(this.canCreateEvents);
            R.append(", selectedVoiceChannelId=");
            R.append(this.selectedVoiceChannelId);
            R.append(", channelPermissions=");
            return a.L(R, this.channelPermissions, ")");
        }
    }

    /* compiled from: GuildScheduledEventListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Initial", "Loaded", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState$Initial;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: GuildScheduledEventListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState$Initial;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Initial extends ViewState {
            public static final Initial INSTANCE = new Initial();

            private Initial() {
                super(null);
            }
        }

        /* compiled from: GuildScheduledEventListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00062\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\b¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState$Loaded;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState;", "", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem$Event;", "component1", "()Ljava/util/List;", "", "component2", "()Z", "guildScheduledEvents", "canCreateEvents", "copy", "(Ljava/util/List;Z)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getGuildScheduledEvents", "Z", "getCanCreateEvents", HookHelper.constructorName, "(Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean canCreateEvents;
            private final List<GuildScheduledEventListItem.Event> guildScheduledEvents;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(List<GuildScheduledEventListItem.Event> list, boolean z2) {
                super(null);
                m.checkNotNullParameter(list, "guildScheduledEvents");
                this.guildScheduledEvents = list;
                this.canCreateEvents = z2;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.guildScheduledEvents;
                }
                if ((i & 2) != 0) {
                    z2 = loaded.canCreateEvents;
                }
                return loaded.copy(list, z2);
            }

            public final List<GuildScheduledEventListItem.Event> component1() {
                return this.guildScheduledEvents;
            }

            public final boolean component2() {
                return this.canCreateEvents;
            }

            public final Loaded copy(List<GuildScheduledEventListItem.Event> list, boolean z2) {
                m.checkNotNullParameter(list, "guildScheduledEvents");
                return new Loaded(list, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.guildScheduledEvents, loaded.guildScheduledEvents) && this.canCreateEvents == loaded.canCreateEvents;
            }

            public final boolean getCanCreateEvents() {
                return this.canCreateEvents;
            }

            public final List<GuildScheduledEventListItem.Event> getGuildScheduledEvents() {
                return this.guildScheduledEvents;
            }

            public int hashCode() {
                List<GuildScheduledEventListItem.Event> list = this.guildScheduledEvents;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                boolean z2 = this.canCreateEvents;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(guildScheduledEvents=");
                R.append(this.guildScheduledEvents);
                R.append(", canCreateEvents=");
                return a.M(R, this.canCreateEvents, ")");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildScheduledEventListViewModel(long r18, java.lang.Long r20, com.discord.stores.StoreGuildScheduledEvents r21, com.discord.stores.StoreChannels r22, com.discord.stores.StorePermissions r23, com.discord.stores.StoreGuilds r24, com.discord.stores.StoreVoiceChannelSelected r25, com.discord.stores.StoreUser r26, rx.Observable r27, int r28, kotlin.jvm.internal.DefaultConstructorMarker r29) {
        /*
            r17 = this;
            r0 = r28
            r1 = r0 & 4
            if (r1 == 0) goto Ld
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildScheduledEvents r1 = r1.getGuildScheduledEvents()
            goto Lf
        Ld:
            r1 = r21
        Lf:
            r2 = r0 & 8
            if (r2 == 0) goto L1b
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r2 = r2.getChannels()
            r12 = r2
            goto L1d
        L1b:
            r12 = r22
        L1d:
            r2 = r0 & 16
            if (r2 == 0) goto L29
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePermissions r2 = r2.getPermissions()
            r13 = r2
            goto L2b
        L29:
            r13 = r23
        L2b:
            r2 = r0 & 32
            if (r2 == 0) goto L37
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r2 = r2.getGuilds()
            r14 = r2
            goto L39
        L37:
            r14 = r24
        L39:
            r2 = r0 & 64
            if (r2 == 0) goto L45
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreVoiceChannelSelected r2 = r2.getVoiceChannelSelected()
            r15 = r2
            goto L47
        L45:
            r15 = r25
        L47:
            r2 = r0 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L54
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r2 = r2.getUsers()
            r16 = r2
            goto L56
        L54:
            r16 = r26
        L56:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L6c
            com.discord.widgets.guildscheduledevent.GuildScheduledEventListViewModel$Companion r2 = com.discord.widgets.guildscheduledevent.GuildScheduledEventListViewModel.Companion
            r3 = r18
            r5 = r20
            r6 = r1
            r7 = r12
            r8 = r13
            r9 = r14
            r10 = r15
            r11 = r16
            rx.Observable r0 = com.discord.widgets.guildscheduledevent.GuildScheduledEventListViewModel.Companion.access$observeStores(r2, r3, r5, r6, r7, r8, r9, r10, r11)
            goto L6e
        L6c:
            r0 = r27
        L6e:
            r2 = r17
            r3 = r18
            r5 = r20
            r6 = r1
            r7 = r12
            r8 = r13
            r9 = r14
            r10 = r15
            r11 = r16
            r12 = r0
            r2.<init>(r3, r5, r6, r7, r8, r9, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guildscheduledevent.GuildScheduledEventListViewModel.<init>(long, java.lang.Long, com.discord.stores.StoreGuildScheduledEvents, com.discord.stores.StoreChannels, com.discord.stores.StorePermissions, com.discord.stores.StoreGuilds, com.discord.stores.StoreVoiceChannelSelected, com.discord.stores.StoreUser, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        boolean canShareEvent;
        boolean z2;
        if (!(getViewState() instanceof ViewState.Loaded)) {
            AnalyticsTracker.INSTANCE.openGuildScheduledEventSheet(this.guildId, storeState.getGuildScheduledEvents().size());
        }
        List<GuildScheduledEvent> guildScheduledEvents = storeState.getGuildScheduledEvents();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(guildScheduledEvents, 10));
        for (GuildScheduledEvent guildScheduledEvent : guildScheduledEvents) {
            Long b2 = guildScheduledEvent.b();
            Long l = null;
            Channel channel = b2 != null ? storeState.getGuildChannels().get(Long.valueOf(b2.longValue())) : null;
            Long c = guildScheduledEvent.c();
            UserGuildMember userGuildMember = c != null ? storeState.getCreators().get(Long.valueOf(c.longValue())) : null;
            boolean contains = storeState.getUserGuildScheduledEventIds().contains(Long.valueOf(guildScheduledEvent.i()));
            GuildScheduledEventUtilities.Companion companion = GuildScheduledEventUtilities.Companion;
            boolean canStartEvent$default = GuildScheduledEventUtilities.Companion.canStartEvent$default(companion, this.guildId, channel != null ? Long.valueOf(channel.h()) : null, null, null, 12, null);
            if (channel != null) {
                l = Long.valueOf(channel.h());
            }
            canShareEvent = companion.canShareEvent(l, this.guildId, (r17 & 4) != 0 ? StoreStream.Companion.getChannels() : null, (r17 & 8) != 0 ? StoreStream.Companion.getGuilds() : null, (r17 & 16) != 0 ? StoreStream.Companion.getUsers() : null, (r17 & 32) != 0 ? StoreStream.Companion.getPermissions() : null);
            Long selectedVoiceChannelId = storeState.getSelectedVoiceChannelId();
            if (selectedVoiceChannelId != null) {
                long longValue = selectedVoiceChannelId.longValue();
                Long b3 = guildScheduledEvent.b();
                z2 = b3 != null && longValue == b3.longValue();
            } else {
                z2 = false;
            }
            Long b4 = guildScheduledEvent.b();
            arrayList.add(new GuildScheduledEventListItem.Event(guildScheduledEvent, channel, userGuildMember, contains, canStartEvent$default, canShareEvent, z2, b4 != null ? PermissionUtils.can(Permission.CONNECT, storeState.getChannelPermissions().get(Long.valueOf(b4.longValue()))) : false));
        }
        updateViewState(new ViewState.Loaded(arrayList, storeState.getCanCreateEvents()));
    }

    public final void onShareClicked(GuildScheduledEvent guildScheduledEvent, WeakReference<Context> weakReference, WeakReference<Fragment> weakReference2) {
        boolean canShareEvent;
        Channel channel;
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        m.checkNotNullParameter(weakReference, "weakContext");
        m.checkNotNullParameter(weakReference2, "weakFragment");
        long h = guildScheduledEvent.h();
        long i = guildScheduledEvent.i();
        Long b2 = guildScheduledEvent.b();
        canShareEvent = GuildScheduledEventUtilities.Companion.canShareEvent(b2, h, (r17 & 4) != 0 ? StoreStream.Companion.getChannels() : null, (r17 & 8) != 0 ? StoreStream.Companion.getGuilds() : null, (r17 & 16) != 0 ? StoreStream.Companion.getUsers() : null, (r17 & 32) != 0 ? StoreStream.Companion.getPermissions() : null);
        if (b2 != null) {
            channel = this.channelsStore.getChannel(b2.longValue());
        } else {
            channel = null;
        }
        if (canShareEvent) {
            Observable<Channel> y2 = StoreStream.Companion.getChannels().observeDefaultChannel(h).y();
            m.checkNotNullExpressionValue(y2, "StoreStream.getChannels(…ildId)\n          .first()");
            ObservableExtensionsKt.appSubscribe(y2, GuildScheduledEventListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildScheduledEventListViewModel$onShareClicked$1(weakReference2, channel, i));
            return;
        }
        Context context = weakReference.get();
        if (context != null) {
            CharSequence eventDetailsUrl = GuildScheduledEventUrlUtils.INSTANCE.getEventDetailsUrl(h, i);
            m.checkNotNullExpressionValue(context, "context");
            b.a.d.m.c(context, eventDetailsUrl, 0, 4);
        }
    }

    public final void toggleRsvp(GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        this.guildScheduledEventsStore.toggleMeRsvpForEvent(guildScheduledEvent);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventListViewModel(long j, Long l, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreChannels storeChannels, StorePermissions storePermissions, StoreGuilds storeGuilds, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreUser storeUser, Observable<StoreState> observable) {
        super(ViewState.Initial.INSTANCE);
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventsStore");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.guildId = j;
        this.channelId = l;
        this.guildScheduledEventsStore = storeGuildScheduledEvents;
        this.channelsStore = storeChannels;
        this.permissionsStore = storePermissions;
        this.guildsStore = storeGuilds;
        this.voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.userStore = storeUser;
        storeGuildScheduledEvents.fetchGuildScheduledEventUserCounts(j);
        storeGuildScheduledEvents.fetchMeGuildScheduledEvents(j);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GuildScheduledEventListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
