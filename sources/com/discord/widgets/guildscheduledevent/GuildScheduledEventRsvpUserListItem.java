package com.discord.widgets.guildscheduledevent;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildScheduledEventRsvpUserListItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", "Companion", "RsvpUser", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildScheduledEventRsvpUserListItem implements MGRecyclerDataPayload {
    public static final Companion Companion = new Companion(null);
    public static final int TYPE_EVENT_RSVP_USER = 0;

    /* compiled from: GuildScheduledEventRsvpUserListItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$Companion;", "", "", "TYPE_EVENT_RSVP_USER", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildScheduledEventRsvpUserListItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0016\b\u0086\b\u0018\u0000 32\u00020\u0001:\u00013BM\u0012\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0012\u001a\u00020\u0006\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0014\u001a\u00020\n\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0006\u0012\n\u0010\u0017\u001a\u00060\u0002j\u0002`\u000f¢\u0006\u0004\b1\u00102J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\r\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000e\u0010\bJ\u0014\u0010\u0010\u001a\u00060\u0002j\u0002`\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0005Jd\u0010\u0018\u001a\u00020\u00002\f\b\u0002\u0010\u0011\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0012\u001a\u00020\u00062\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0014\u001a\u00020\n2\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00062\f\b\u0002\u0010\u0017\u001a\u00060\u0002j\u0002`\u000fHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u001a\u0010\bJ\u0010\u0010\u001b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u001b\u0010\fJ\u001a\u0010\u001f\u001a\u00020\u001e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u001d\u0010\u0017\u001a\u00060\u0002j\u0002`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010!\u001a\u0004\b\"\u0010\u0005R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b$\u0010\bR\u001c\u0010%\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u0010#\u001a\u0004\b&\u0010\bR\u0019\u0010'\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010#\u001a\u0004\b(\u0010\bR\u0019\u0010\u0014\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b*\u0010\fR\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010#\u001a\u0004\b+\u0010\bR\u001c\u0010,\u001a\u00020\n8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b,\u0010)\u001a\u0004\b-\u0010\fR\u001d\u0010\u0011\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b.\u0010\u0005R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b/\u0010\bR\u0019\u0010\u0012\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b0\u0010\b¨\u00064"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "", "component4", "()I", "component5", "component6", "Lcom/discord/primitives/GuildId;", "component7", "userId", "username", "userAvatar", "discriminator", "nickname", "guildAvatar", "guildId", "copy", "(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;J)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "Ljava/lang/String;", "getGuildAvatar", "key", "getKey", "displayName", "getDisplayName", "I", "getDiscriminator", "getNickname", "type", "getType", "getUserId", "getUserAvatar", "getUsername", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;J)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RsvpUser extends GuildScheduledEventRsvpUserListItem {
        public static final Companion Companion = new Companion(null);
        private final int discriminator;
        private final String displayName;
        private final String guildAvatar;
        private final long guildId;
        private final String key;
        private final String nickname;
        private final int type;
        private final String userAvatar;
        private final long userId;
        private final String username;

        /* compiled from: GuildScheduledEventRsvpUserListItem.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser$Companion;", "", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventUser;", "guildScheduledEventUser", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser;", "from", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventUser;J)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final RsvpUser from(GuildScheduledEventUser guildScheduledEventUser, long j) {
                m.checkNotNullParameter(guildScheduledEventUser, "guildScheduledEventUser");
                return new RsvpUser(guildScheduledEventUser.getUser().getId(), guildScheduledEventUser.getUser().getUsername(), guildScheduledEventUser.getUser().getAvatar(), guildScheduledEventUser.getUser().getDiscriminator(), guildScheduledEventUser.getGuildMember().getNick(), guildScheduledEventUser.getGuildMember().getAvatarHash(), j);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public RsvpUser(long j, String str, String str2, int i, String str3, String str4, long j2) {
            super(null);
            m.checkNotNullParameter(str, "username");
            this.userId = j;
            this.username = str;
            this.userAvatar = str2;
            this.discriminator = i;
            this.nickname = str3;
            this.guildAvatar = str4;
            this.guildId = j2;
            this.key = String.valueOf(j);
            this.displayName = str3 != null ? str3 : str;
        }

        public final long component1() {
            return this.userId;
        }

        public final String component2() {
            return this.username;
        }

        public final String component3() {
            return this.userAvatar;
        }

        public final int component4() {
            return this.discriminator;
        }

        public final String component5() {
            return this.nickname;
        }

        public final String component6() {
            return this.guildAvatar;
        }

        public final long component7() {
            return this.guildId;
        }

        public final RsvpUser copy(long j, String str, String str2, int i, String str3, String str4, long j2) {
            m.checkNotNullParameter(str, "username");
            return new RsvpUser(j, str, str2, i, str3, str4, j2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RsvpUser)) {
                return false;
            }
            RsvpUser rsvpUser = (RsvpUser) obj;
            return this.userId == rsvpUser.userId && m.areEqual(this.username, rsvpUser.username) && m.areEqual(this.userAvatar, rsvpUser.userAvatar) && this.discriminator == rsvpUser.discriminator && m.areEqual(this.nickname, rsvpUser.nickname) && m.areEqual(this.guildAvatar, rsvpUser.guildAvatar) && this.guildId == rsvpUser.guildId;
        }

        public final int getDiscriminator() {
            return this.discriminator;
        }

        public final String getDisplayName() {
            return this.displayName;
        }

        public final String getGuildAvatar() {
            return this.guildAvatar;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final String getNickname() {
            return this.nickname;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public final String getUserAvatar() {
            return this.userAvatar;
        }

        public final long getUserId() {
            return this.userId;
        }

        public final String getUsername() {
            return this.username;
        }

        public int hashCode() {
            int a = b.a(this.userId) * 31;
            String str = this.username;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.userAvatar;
            int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.discriminator) * 31;
            String str3 = this.nickname;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.guildAvatar;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return b.a(this.guildId) + ((hashCode3 + i) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("RsvpUser(userId=");
            R.append(this.userId);
            R.append(", username=");
            R.append(this.username);
            R.append(", userAvatar=");
            R.append(this.userAvatar);
            R.append(", discriminator=");
            R.append(this.discriminator);
            R.append(", nickname=");
            R.append(this.nickname);
            R.append(", guildAvatar=");
            R.append(this.guildAvatar);
            R.append(", guildId=");
            return a.B(R, this.guildId, ")");
        }
    }

    private GuildScheduledEventRsvpUserListItem() {
    }

    public /* synthetic */ GuildScheduledEventRsvpUserListItem(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
