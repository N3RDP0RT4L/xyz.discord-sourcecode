package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildScheduledEventBroadcast;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildScheduledEventDirectoryAssociationViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0005¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationState;", "", "Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventBroadcast;", "component1", "()Lcom/discord/stores/utilities/RestCallState;", "enabledAsync", "copy", "(Lcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/utilities/RestCallState;", "getEnabledAsync", HookHelper.constructorName, "(Lcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventDirectoryAssociationState {
    private final RestCallState<GuildScheduledEventBroadcast> enabledAsync;

    public GuildScheduledEventDirectoryAssociationState() {
        this(null, 1, null);
    }

    public GuildScheduledEventDirectoryAssociationState(RestCallState<GuildScheduledEventBroadcast> restCallState) {
        m.checkNotNullParameter(restCallState, "enabledAsync");
        this.enabledAsync = restCallState;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ GuildScheduledEventDirectoryAssociationState copy$default(GuildScheduledEventDirectoryAssociationState guildScheduledEventDirectoryAssociationState, RestCallState restCallState, int i, Object obj) {
        if ((i & 1) != 0) {
            restCallState = guildScheduledEventDirectoryAssociationState.enabledAsync;
        }
        return guildScheduledEventDirectoryAssociationState.copy(restCallState);
    }

    public final RestCallState<GuildScheduledEventBroadcast> component1() {
        return this.enabledAsync;
    }

    public final GuildScheduledEventDirectoryAssociationState copy(RestCallState<GuildScheduledEventBroadcast> restCallState) {
        m.checkNotNullParameter(restCallState, "enabledAsync");
        return new GuildScheduledEventDirectoryAssociationState(restCallState);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof GuildScheduledEventDirectoryAssociationState) && m.areEqual(this.enabledAsync, ((GuildScheduledEventDirectoryAssociationState) obj).enabledAsync);
        }
        return true;
    }

    public final RestCallState<GuildScheduledEventBroadcast> getEnabledAsync() {
        return this.enabledAsync;
    }

    public int hashCode() {
        RestCallState<GuildScheduledEventBroadcast> restCallState = this.enabledAsync;
        if (restCallState != null) {
            return restCallState.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEventDirectoryAssociationState(enabledAsync=");
        R.append(this.enabledAsync);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ GuildScheduledEventDirectoryAssociationState(RestCallState restCallState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? Default.INSTANCE : restCallState);
    }
}
