package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreDirectories;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserSettings;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventRsvpUserListItem;
import d0.t.n;
import d0.z.d.k;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: GuildScheduledEventDetailsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 @2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003@ABB\u0097\u0001\u0012\u0006\u0010,\u001a\u00020+\u0012\b\b\u0002\u0010/\u001a\u00020.\u0012\b\b\u0002\u0010)\u001a\u00020(\u0012\b\b\u0002\u00101\u001a\u000200\u0012\b\b\u0002\u0010\"\u001a\u00020!\u0012\b\b\u0002\u00103\u001a\u000202\u0012\b\b\u0002\u00105\u001a\u000204\u0012\b\b\u0002\u00107\u001a\u000206\u0012\b\b\u0002\u00109\u001a\u000208\u0012\b\b\u0002\u0010;\u001a\u00020:\u0012\u000e\b\u0002\u0010=\u001a\b\u0012\u0004\u0012\u00020\u00030<\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u001e\u0012\b\b\u0002\u0010$\u001a\u00020\u0005\u0012\b\b\u0002\u0010&\u001a\u00020\u001a¢\u0006\u0004\b>\u0010?J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\r\u0010\u000b\u001a\u00020\b¢\u0006\u0004\b\u000b\u0010\fJ\u001b\u0010\u0010\u001a\u00020\b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\u0004\b\u0010\u0010\u0011J#\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00122\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\b0\u0014¢\u0006\u0004\b\u0016\u0010\u0017J#\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00122\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\b0\u0014¢\u0006\u0004\b\u0018\u0010\u0017J\r\u0010\u0019\u001a\u00020\b¢\u0006\u0004\b\u0019\u0010\fJ\u0015\u0010\u001c\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010$\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010&\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-¨\u0006C"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;", "storeState", "Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;", "getRsvpUsersFetchState", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;)Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;", "", "handleStoreState", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;)V", "onRsvpButtonClicked", "()V", "Ljava/lang/ref/WeakReference;", "Landroidx/fragment/app/Fragment;", "weakFragment", "onShareButtonClicked", "(Ljava/lang/ref/WeakReference;)V", "Landroid/content/Context;", "context", "Lkotlin/Function0;", "onSuccess", "onDeleteButtonClicked", "(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V", "endEventClicked", "onGuildNameClicked", "", "index", "setSegmentedControlIndex", "(I)V", "Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;", "section", "Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "rsvpUsersFetchState", "Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;", "segmentControlIndex", "I", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreUserSettings;", "userSettingsStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "selectedVoiceChannelStore", "Lcom/discord/stores/StoreDirectories;", "directoriesStore", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreDirectories;Lrx/Observable;Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;I)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventDetailsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final GuildScheduledEventDetailsArgs args;
    private final StoreGuildScheduledEvents guildScheduledEventsStore;
    private EventDetailsRsvpUsersFetchState rsvpUsersFetchState;
    private EventDetailsSection section;
    private int segmentControlIndex;
    private final StoreUser userStore;

    /* compiled from: GuildScheduledEventDetailsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(GuildScheduledEventDetailsViewModel guildScheduledEventDetailsViewModel) {
            super(1, guildScheduledEventDetailsViewModel, GuildScheduledEventDetailsViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((GuildScheduledEventDetailsViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: GuildScheduledEventDetailsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001a\u0010\u001bJe\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$Companion;", "", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreUserSettings;", "userSettingsStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "selectedVoiceChannelStore", "Lcom/discord/stores/StoreDirectories;", "directoriesStore", "Lrx/Observable;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;", "observeStores", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreDirectories;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;
            public static final /* synthetic */ int[] $EnumSwitchMapping$1;

            static {
                GuildScheduledEventDetailsSource.values();
                int[] iArr = new int[2];
                $EnumSwitchMapping$0 = iArr;
                GuildScheduledEventDetailsSource guildScheduledEventDetailsSource = GuildScheduledEventDetailsSource.Directory;
                iArr[guildScheduledEventDetailsSource.ordinal()] = 1;
                GuildScheduledEventDetailsSource guildScheduledEventDetailsSource2 = GuildScheduledEventDetailsSource.Guild;
                iArr[guildScheduledEventDetailsSource2.ordinal()] = 2;
                GuildScheduledEventDetailsSource.values();
                int[] iArr2 = new int[2];
                $EnumSwitchMapping$1 = iArr2;
                iArr2[guildScheduledEventDetailsSource.ordinal()] = 1;
                iArr2[guildScheduledEventDetailsSource2.ordinal()] = 2;
            }
        }

        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(GuildScheduledEventDetailsArgs guildScheduledEventDetailsArgs, ObservationDeck observationDeck, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreGuilds storeGuilds, StoreUser storeUser, StoreChannels storeChannels, StorePermissions storePermissions, StoreUserSettings storeUserSettings, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreDirectories storeDirectories) {
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeGuildScheduledEvents, storeGuilds, storeUser, storeChannels, storePermissions, storeVoiceChannelSelected, storeDirectories}, false, null, null, new GuildScheduledEventDetailsViewModel$Companion$observeStores$1(guildScheduledEventDetailsArgs, storeDirectories, storeGuildScheduledEvents, storeGuilds, storeChannels, storeVoiceChannelSelected, storePermissions, storeUserSettings), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildScheduledEventDetailsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u0001B©\u0001\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u000b\u0012\b\b\u0002\u0010$\u001a\u00020\u000e\u0012\b\b\u0002\u0010%\u001a\u00020\u000e\u0012\b\b\u0002\u0010&\u001a\u00020\u000e\u0012\b\b\u0002\u0010'\u001a\u00020\u000e\u0012\b\b\u0002\u0010(\u001a\u00020\u000e\u0012\u0010\b\u0002\u0010)\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u0016\u0012\b\b\u0002\u0010*\u001a\u00020\u000e\u0012\u000e\b\u0002\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a\u0012\b\b\u0002\u0010,\u001a\u00020\u000e\u0012\b\b\u0002\u0010-\u001a\u00020\u000e¢\u0006\u0004\bI\u0010JJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0010J\u0010\u0010\u0014\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0010J\u0018\u0010\u0017\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0010J\u0016\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u0010J\u0010\u0010\u001f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u001f\u0010\u0010J²\u0001\u0010.\u001a\u00020\u00002\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010$\u001a\u00020\u000e2\b\b\u0002\u0010%\u001a\u00020\u000e2\b\b\u0002\u0010&\u001a\u00020\u000e2\b\b\u0002\u0010'\u001a\u00020\u000e2\b\b\u0002\u0010(\u001a\u00020\u000e2\u0010\b\u0002\u0010)\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u00162\b\b\u0002\u0010*\u001a\u00020\u000e2\u000e\b\u0002\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a2\b\b\u0002\u0010,\u001a\u00020\u000e2\b\b\u0002\u0010-\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b.\u0010/J\u0010\u00101\u001a\u000200HÖ\u0001¢\u0006\u0004\b1\u00102J\u0010\u00104\u001a\u000203HÖ\u0001¢\u0006\u0004\b4\u00105J\u001a\u00107\u001a\u00020\u000e2\b\u00106\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b7\u00108R\u0019\u0010(\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b(\u00109\u001a\u0004\b(\u0010\u0010R\u001b\u0010!\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010:\u001a\u0004\b;\u0010\u0007R\u0019\u0010-\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u00109\u001a\u0004\b-\u0010\u0010R\u0019\u0010$\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00109\u001a\u0004\b$\u0010\u0010R\u0019\u0010'\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u00109\u001a\u0004\b<\u0010\u0010R\u0019\u0010%\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u00109\u001a\u0004\b%\u0010\u0010R\u0019\u0010,\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u00109\u001a\u0004\b,\u0010\u0010R\u001b\u0010#\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010=\u001a\u0004\b>\u0010\rR\u001b\u0010\"\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010?\u001a\u0004\b@\u0010\nR\u0019\u0010&\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00109\u001a\u0004\bA\u0010\u0010R\u0019\u0010*\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u00109\u001a\u0004\bB\u0010\u0010R!\u0010)\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u00168\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010C\u001a\u0004\bD\u0010\u0018R\u001f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010E\u001a\u0004\bF\u0010\u001dR\u001b\u0010 \u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u0010G\u001a\u0004\bH\u0010\u0004¨\u0006K"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;", "", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/guild/UserGuildMember;", "component4", "()Lcom/discord/models/guild/UserGuildMember;", "", "component5", "()Z", "component6", "component7", "component8", "component9", "", "Lcom/discord/primitives/ChannelId;", "component10", "()Ljava/lang/Long;", "component11", "", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser;", "component12", "()Ljava/util/List;", "component13", "component14", "guildScheduledEvent", "channel", "guild", "creator", "isInGuild", "isRsvped", "canShare", "canStartEvent", "isDeveloperMode", "selectedVoiceChannelId", "canConnect", "rsvpUsers", "isRsvpUsersFetching", "isRsvpUsersError", "copy", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/UserGuildMember;ZZZZZLjava/lang/Long;ZLjava/util/List;ZZ)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/api/channel/Channel;", "getChannel", "getCanStartEvent", "Lcom/discord/models/guild/UserGuildMember;", "getCreator", "Lcom/discord/models/guild/Guild;", "getGuild", "getCanShare", "getCanConnect", "Ljava/lang/Long;", "getSelectedVoiceChannelId", "Ljava/util/List;", "getRsvpUsers", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", HookHelper.constructorName, "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/UserGuildMember;ZZZZZLjava/lang/Long;ZLjava/util/List;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final boolean canConnect;
        private final boolean canShare;
        private final boolean canStartEvent;
        private final Channel channel;
        private final UserGuildMember creator;
        private final Guild guild;
        private final GuildScheduledEvent guildScheduledEvent;
        private final boolean isDeveloperMode;
        private final boolean isInGuild;
        private final boolean isRsvpUsersError;
        private final boolean isRsvpUsersFetching;
        private final boolean isRsvped;
        private final List<GuildScheduledEventRsvpUserListItem.RsvpUser> rsvpUsers;
        private final Long selectedVoiceChannelId;

        public StoreState() {
            this(null, null, null, null, false, false, false, false, false, null, false, null, false, false, 16383, null);
        }

        public StoreState(GuildScheduledEvent guildScheduledEvent, Channel channel, Guild guild, UserGuildMember userGuildMember, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Long l, boolean z7, List<GuildScheduledEventRsvpUserListItem.RsvpUser> list, boolean z8, boolean z9) {
            m.checkNotNullParameter(list, "rsvpUsers");
            this.guildScheduledEvent = guildScheduledEvent;
            this.channel = channel;
            this.guild = guild;
            this.creator = userGuildMember;
            this.isInGuild = z2;
            this.isRsvped = z3;
            this.canShare = z4;
            this.canStartEvent = z5;
            this.isDeveloperMode = z6;
            this.selectedVoiceChannelId = l;
            this.canConnect = z7;
            this.rsvpUsers = list;
            this.isRsvpUsersFetching = z8;
            this.isRsvpUsersError = z9;
        }

        public final GuildScheduledEvent component1() {
            return this.guildScheduledEvent;
        }

        public final Long component10() {
            return this.selectedVoiceChannelId;
        }

        public final boolean component11() {
            return this.canConnect;
        }

        public final List<GuildScheduledEventRsvpUserListItem.RsvpUser> component12() {
            return this.rsvpUsers;
        }

        public final boolean component13() {
            return this.isRsvpUsersFetching;
        }

        public final boolean component14() {
            return this.isRsvpUsersError;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final Guild component3() {
            return this.guild;
        }

        public final UserGuildMember component4() {
            return this.creator;
        }

        public final boolean component5() {
            return this.isInGuild;
        }

        public final boolean component6() {
            return this.isRsvped;
        }

        public final boolean component7() {
            return this.canShare;
        }

        public final boolean component8() {
            return this.canStartEvent;
        }

        public final boolean component9() {
            return this.isDeveloperMode;
        }

        public final StoreState copy(GuildScheduledEvent guildScheduledEvent, Channel channel, Guild guild, UserGuildMember userGuildMember, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Long l, boolean z7, List<GuildScheduledEventRsvpUserListItem.RsvpUser> list, boolean z8, boolean z9) {
            m.checkNotNullParameter(list, "rsvpUsers");
            return new StoreState(guildScheduledEvent, channel, guild, userGuildMember, z2, z3, z4, z5, z6, l, z7, list, z8, z9);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guildScheduledEvent, storeState.guildScheduledEvent) && m.areEqual(this.channel, storeState.channel) && m.areEqual(this.guild, storeState.guild) && m.areEqual(this.creator, storeState.creator) && this.isInGuild == storeState.isInGuild && this.isRsvped == storeState.isRsvped && this.canShare == storeState.canShare && this.canStartEvent == storeState.canStartEvent && this.isDeveloperMode == storeState.isDeveloperMode && m.areEqual(this.selectedVoiceChannelId, storeState.selectedVoiceChannelId) && this.canConnect == storeState.canConnect && m.areEqual(this.rsvpUsers, storeState.rsvpUsers) && this.isRsvpUsersFetching == storeState.isRsvpUsersFetching && this.isRsvpUsersError == storeState.isRsvpUsersError;
        }

        public final boolean getCanConnect() {
            return this.canConnect;
        }

        public final boolean getCanShare() {
            return this.canShare;
        }

        public final boolean getCanStartEvent() {
            return this.canStartEvent;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final UserGuildMember getCreator() {
            return this.creator;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final GuildScheduledEvent getGuildScheduledEvent() {
            return this.guildScheduledEvent;
        }

        public final List<GuildScheduledEventRsvpUserListItem.RsvpUser> getRsvpUsers() {
            return this.rsvpUsers;
        }

        public final Long getSelectedVoiceChannelId() {
            return this.selectedVoiceChannelId;
        }

        public int hashCode() {
            GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
            int i = 0;
            int hashCode = (guildScheduledEvent != null ? guildScheduledEvent.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            int hashCode3 = (hashCode2 + (guild != null ? guild.hashCode() : 0)) * 31;
            UserGuildMember userGuildMember = this.creator;
            int hashCode4 = (hashCode3 + (userGuildMember != null ? userGuildMember.hashCode() : 0)) * 31;
            boolean z2 = this.isInGuild;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode4 + i3) * 31;
            boolean z3 = this.isRsvped;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            boolean z4 = this.canShare;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (i8 + i9) * 31;
            boolean z5 = this.canStartEvent;
            if (z5) {
                z5 = true;
            }
            int i12 = z5 ? 1 : 0;
            int i13 = z5 ? 1 : 0;
            int i14 = (i11 + i12) * 31;
            boolean z6 = this.isDeveloperMode;
            if (z6) {
                z6 = true;
            }
            int i15 = z6 ? 1 : 0;
            int i16 = z6 ? 1 : 0;
            int i17 = (i14 + i15) * 31;
            Long l = this.selectedVoiceChannelId;
            int hashCode5 = (i17 + (l != null ? l.hashCode() : 0)) * 31;
            boolean z7 = this.canConnect;
            if (z7) {
                z7 = true;
            }
            int i18 = z7 ? 1 : 0;
            int i19 = z7 ? 1 : 0;
            int i20 = (hashCode5 + i18) * 31;
            List<GuildScheduledEventRsvpUserListItem.RsvpUser> list = this.rsvpUsers;
            if (list != null) {
                i = list.hashCode();
            }
            int i21 = (i20 + i) * 31;
            boolean z8 = this.isRsvpUsersFetching;
            if (z8) {
                z8 = true;
            }
            int i22 = z8 ? 1 : 0;
            int i23 = z8 ? 1 : 0;
            int i24 = (i21 + i22) * 31;
            boolean z9 = this.isRsvpUsersError;
            if (!z9) {
                i2 = z9 ? 1 : 0;
            }
            return i24 + i2;
        }

        public final boolean isDeveloperMode() {
            return this.isDeveloperMode;
        }

        public final boolean isInGuild() {
            return this.isInGuild;
        }

        public final boolean isRsvpUsersError() {
            return this.isRsvpUsersError;
        }

        public final boolean isRsvpUsersFetching() {
            return this.isRsvpUsersFetching;
        }

        public final boolean isRsvped() {
            return this.isRsvped;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guildScheduledEvent=");
            R.append(this.guildScheduledEvent);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", creator=");
            R.append(this.creator);
            R.append(", isInGuild=");
            R.append(this.isInGuild);
            R.append(", isRsvped=");
            R.append(this.isRsvped);
            R.append(", canShare=");
            R.append(this.canShare);
            R.append(", canStartEvent=");
            R.append(this.canStartEvent);
            R.append(", isDeveloperMode=");
            R.append(this.isDeveloperMode);
            R.append(", selectedVoiceChannelId=");
            R.append(this.selectedVoiceChannelId);
            R.append(", canConnect=");
            R.append(this.canConnect);
            R.append(", rsvpUsers=");
            R.append(this.rsvpUsers);
            R.append(", isRsvpUsersFetching=");
            R.append(this.isRsvpUsersFetching);
            R.append(", isRsvpUsersError=");
            return a.M(R, this.isRsvpUsersError, ")");
        }

        public /* synthetic */ StoreState(GuildScheduledEvent guildScheduledEvent, Channel channel, Guild guild, UserGuildMember userGuildMember, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Long l, boolean z7, List list, boolean z8, boolean z9, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : guildScheduledEvent, (i & 2) != 0 ? null : channel, (i & 4) != 0 ? null : guild, (i & 8) != 0 ? null : userGuildMember, (i & 16) != 0 ? true : z2, (i & 32) != 0 ? false : z3, (i & 64) != 0 ? false : z4, (i & 128) != 0 ? false : z5, (i & 256) != 0 ? false : z6, (i & 512) == 0 ? l : null, (i & 1024) != 0 ? false : z7, (i & 2048) != 0 ? n.emptyList() : list, (i & 4096) != 0 ? false : z8, (i & 8192) == 0 ? z9 : false);
        }
    }

    /* compiled from: GuildScheduledEventDetailsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Initialized", "Invalid", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState$Initialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: GuildScheduledEventDetailsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u001c\b\u0086\b\u0018\u00002\u00020\u0001B\u0093\u0001\u0012\u0006\u0010'\u001a\u00020\u0002\u0012\b\u0010(\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010)\u001a\u00020\b\u0012\b\u0010*\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010+\u001a\u00020\u000e\u0012\u0006\u0010,\u001a\u00020\u000e\u0012\u0006\u0010-\u001a\u00020\u000e\u0012\u0006\u0010.\u001a\u00020\u000e\u0012\u0006\u0010/\u001a\u00020\u000e\u0012\u0006\u00100\u001a\u00020\u000e\u0012\u0006\u00101\u001a\u00020\u000e\u0012\f\u00102\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017\u0012\u0006\u00103\u001a\u00020\u001b\u0012\u0006\u00104\u001a\u00020\u001e\u0012\u0006\u00105\u001a\u00020!\u0012\b\u00106\u001a\u0004\u0018\u00010$¢\u0006\u0004\bW\u0010XJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0010J\u0010\u0010\u0014\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0010J\u0010\u0010\u0015\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0010J\u0010\u0010\u0016\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0010J\u0016\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\"\u001a\u00020!HÆ\u0003¢\u0006\u0004\b\"\u0010#J\u0012\u0010%\u001a\u0004\u0018\u00010$HÆ\u0003¢\u0006\u0004\b%\u0010&J¼\u0001\u00107\u001a\u00020\u00002\b\b\u0002\u0010'\u001a\u00020\u00022\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010)\u001a\u00020\b2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010+\u001a\u00020\u000e2\b\b\u0002\u0010,\u001a\u00020\u000e2\b\b\u0002\u0010-\u001a\u00020\u000e2\b\b\u0002\u0010.\u001a\u00020\u000e2\b\b\u0002\u0010/\u001a\u00020\u000e2\b\b\u0002\u00100\u001a\u00020\u000e2\b\b\u0002\u00101\u001a\u00020\u000e2\u000e\b\u0002\u00102\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\b\b\u0002\u00103\u001a\u00020\u001b2\b\b\u0002\u00104\u001a\u00020\u001e2\b\b\u0002\u00105\u001a\u00020!2\n\b\u0002\u00106\u001a\u0004\u0018\u00010$HÆ\u0001¢\u0006\u0004\b7\u00108J\u0010\u0010:\u001a\u000209HÖ\u0001¢\u0006\u0004\b:\u0010;J\u0010\u0010<\u001a\u00020!HÖ\u0001¢\u0006\u0004\b<\u0010#J\u001a\u0010?\u001a\u00020\u000e2\b\u0010>\u001a\u0004\u0018\u00010=HÖ\u0003¢\u0006\u0004\b?\u0010@R\u0019\u00103\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010A\u001a\u0004\bB\u0010\u001dR\u0019\u0010/\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010C\u001a\u0004\bD\u0010\u0010R\u0019\u00100\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010C\u001a\u0004\b0\u0010\u0010R\u0019\u0010'\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010E\u001a\u0004\bF\u0010\u0004R\u0019\u0010,\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010C\u001a\u0004\b,\u0010\u0010R\u0019\u00101\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010C\u001a\u0004\bG\u0010\u0010R\u0019\u00105\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010H\u001a\u0004\bI\u0010#R\u0019\u0010.\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010C\u001a\u0004\b.\u0010\u0010R\u001b\u00106\u001a\u0004\u0018\u00010$8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010J\u001a\u0004\bK\u0010&R\u0019\u0010)\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010L\u001a\u0004\bM\u0010\nR\u001b\u0010(\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010N\u001a\u0004\bO\u0010\u0007R\u001f\u00102\u001a\b\u0012\u0004\u0012\u00020\u00180\u00178\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010P\u001a\u0004\bQ\u0010\u001aR\u0019\u0010+\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010C\u001a\u0004\b+\u0010\u0010R\u001b\u0010*\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010R\u001a\u0004\bS\u0010\rR\u0019\u0010-\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010C\u001a\u0004\bT\u0010\u0010R\u0019\u00104\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010U\u001a\u0004\bV\u0010 ¨\u0006Y"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState$Initialized;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "component2", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/channel/Channel;", "component4", "()Lcom/discord/api/channel/Channel;", "", "component5", "()Z", "component6", "component7", "component8", "component9", "component10", "component11", "", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListItem$RsvpUser;", "component12", "()Ljava/util/List;", "Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;", "component13", "()Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;", "Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;", "component14", "()Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;", "", "component15", "()I", "Lcom/discord/models/guild/UserGuildMember;", "component16", "()Lcom/discord/models/guild/UserGuildMember;", "guildScheduledEvent", "locationInfo", "guild", "channel", "isInGuild", "isRsvped", "canShare", "isConnected", "canStartEvent", "isDeveloperMode", "canConnect", "rsvpUsers", "section", "rsvpUsersFetchState", "segmentedControlIndex", "creator", "copy", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;ZZZZZZZLjava/util/List;Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;ILcom/discord/models/guild/UserGuildMember;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState$Initialized;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;", "getSection", "Z", "getCanStartEvent", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", "getCanConnect", "I", "getSegmentedControlIndex", "Lcom/discord/models/guild/UserGuildMember;", "getCreator", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "getLocationInfo", "Ljava/util/List;", "getRsvpUsers", "Lcom/discord/api/channel/Channel;", "getChannel", "getCanShare", "Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;", "getRsvpUsersFetchState", HookHelper.constructorName, "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;ZZZZZZZLjava/util/List;Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;ILcom/discord/models/guild/UserGuildMember;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Initialized extends ViewState {
            private final boolean canConnect;
            private final boolean canShare;
            private final boolean canStartEvent;
            private final Channel channel;
            private final UserGuildMember creator;
            private final Guild guild;
            private final GuildScheduledEvent guildScheduledEvent;
            private final boolean isConnected;
            private final boolean isDeveloperMode;
            private final boolean isInGuild;
            private final boolean isRsvped;
            private final GuildScheduledEventLocationInfo locationInfo;
            private final List<GuildScheduledEventRsvpUserListItem.RsvpUser> rsvpUsers;
            private final EventDetailsRsvpUsersFetchState rsvpUsersFetchState;
            private final EventDetailsSection section;
            private final int segmentedControlIndex;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Initialized(GuildScheduledEvent guildScheduledEvent, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, Guild guild, Channel channel, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, List<GuildScheduledEventRsvpUserListItem.RsvpUser> list, EventDetailsSection eventDetailsSection, EventDetailsRsvpUsersFetchState eventDetailsRsvpUsersFetchState, int i, UserGuildMember userGuildMember) {
                super(null);
                m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(list, "rsvpUsers");
                m.checkNotNullParameter(eventDetailsSection, "section");
                m.checkNotNullParameter(eventDetailsRsvpUsersFetchState, "rsvpUsersFetchState");
                this.guildScheduledEvent = guildScheduledEvent;
                this.locationInfo = guildScheduledEventLocationInfo;
                this.guild = guild;
                this.channel = channel;
                this.isInGuild = z2;
                this.isRsvped = z3;
                this.canShare = z4;
                this.isConnected = z5;
                this.canStartEvent = z6;
                this.isDeveloperMode = z7;
                this.canConnect = z8;
                this.rsvpUsers = list;
                this.section = eventDetailsSection;
                this.rsvpUsersFetchState = eventDetailsRsvpUsersFetchState;
                this.segmentedControlIndex = i;
                this.creator = userGuildMember;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Initialized copy$default(Initialized initialized, GuildScheduledEvent guildScheduledEvent, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, Guild guild, Channel channel, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, List list, EventDetailsSection eventDetailsSection, EventDetailsRsvpUsersFetchState eventDetailsRsvpUsersFetchState, int i, UserGuildMember userGuildMember, int i2, Object obj) {
                return initialized.copy((i2 & 1) != 0 ? initialized.guildScheduledEvent : guildScheduledEvent, (i2 & 2) != 0 ? initialized.locationInfo : guildScheduledEventLocationInfo, (i2 & 4) != 0 ? initialized.guild : guild, (i2 & 8) != 0 ? initialized.channel : channel, (i2 & 16) != 0 ? initialized.isInGuild : z2, (i2 & 32) != 0 ? initialized.isRsvped : z3, (i2 & 64) != 0 ? initialized.canShare : z4, (i2 & 128) != 0 ? initialized.isConnected : z5, (i2 & 256) != 0 ? initialized.canStartEvent : z6, (i2 & 512) != 0 ? initialized.isDeveloperMode : z7, (i2 & 1024) != 0 ? initialized.canConnect : z8, (i2 & 2048) != 0 ? initialized.rsvpUsers : list, (i2 & 4096) != 0 ? initialized.section : eventDetailsSection, (i2 & 8192) != 0 ? initialized.rsvpUsersFetchState : eventDetailsRsvpUsersFetchState, (i2 & 16384) != 0 ? initialized.segmentedControlIndex : i, (i2 & 32768) != 0 ? initialized.creator : userGuildMember);
            }

            public final GuildScheduledEvent component1() {
                return this.guildScheduledEvent;
            }

            public final boolean component10() {
                return this.isDeveloperMode;
            }

            public final boolean component11() {
                return this.canConnect;
            }

            public final List<GuildScheduledEventRsvpUserListItem.RsvpUser> component12() {
                return this.rsvpUsers;
            }

            public final EventDetailsSection component13() {
                return this.section;
            }

            public final EventDetailsRsvpUsersFetchState component14() {
                return this.rsvpUsersFetchState;
            }

            public final int component15() {
                return this.segmentedControlIndex;
            }

            public final UserGuildMember component16() {
                return this.creator;
            }

            public final GuildScheduledEventLocationInfo component2() {
                return this.locationInfo;
            }

            public final Guild component3() {
                return this.guild;
            }

            public final Channel component4() {
                return this.channel;
            }

            public final boolean component5() {
                return this.isInGuild;
            }

            public final boolean component6() {
                return this.isRsvped;
            }

            public final boolean component7() {
                return this.canShare;
            }

            public final boolean component8() {
                return this.isConnected;
            }

            public final boolean component9() {
                return this.canStartEvent;
            }

            public final Initialized copy(GuildScheduledEvent guildScheduledEvent, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, Guild guild, Channel channel, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, List<GuildScheduledEventRsvpUserListItem.RsvpUser> list, EventDetailsSection eventDetailsSection, EventDetailsRsvpUsersFetchState eventDetailsRsvpUsersFetchState, int i, UserGuildMember userGuildMember) {
                m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(list, "rsvpUsers");
                m.checkNotNullParameter(eventDetailsSection, "section");
                m.checkNotNullParameter(eventDetailsRsvpUsersFetchState, "rsvpUsersFetchState");
                return new Initialized(guildScheduledEvent, guildScheduledEventLocationInfo, guild, channel, z2, z3, z4, z5, z6, z7, z8, list, eventDetailsSection, eventDetailsRsvpUsersFetchState, i, userGuildMember);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Initialized)) {
                    return false;
                }
                Initialized initialized = (Initialized) obj;
                return m.areEqual(this.guildScheduledEvent, initialized.guildScheduledEvent) && m.areEqual(this.locationInfo, initialized.locationInfo) && m.areEqual(this.guild, initialized.guild) && m.areEqual(this.channel, initialized.channel) && this.isInGuild == initialized.isInGuild && this.isRsvped == initialized.isRsvped && this.canShare == initialized.canShare && this.isConnected == initialized.isConnected && this.canStartEvent == initialized.canStartEvent && this.isDeveloperMode == initialized.isDeveloperMode && this.canConnect == initialized.canConnect && m.areEqual(this.rsvpUsers, initialized.rsvpUsers) && m.areEqual(this.section, initialized.section) && m.areEqual(this.rsvpUsersFetchState, initialized.rsvpUsersFetchState) && this.segmentedControlIndex == initialized.segmentedControlIndex && m.areEqual(this.creator, initialized.creator);
            }

            public final boolean getCanConnect() {
                return this.canConnect;
            }

            public final boolean getCanShare() {
                return this.canShare;
            }

            public final boolean getCanStartEvent() {
                return this.canStartEvent;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final UserGuildMember getCreator() {
                return this.creator;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final GuildScheduledEvent getGuildScheduledEvent() {
                return this.guildScheduledEvent;
            }

            public final GuildScheduledEventLocationInfo getLocationInfo() {
                return this.locationInfo;
            }

            public final List<GuildScheduledEventRsvpUserListItem.RsvpUser> getRsvpUsers() {
                return this.rsvpUsers;
            }

            public final EventDetailsRsvpUsersFetchState getRsvpUsersFetchState() {
                return this.rsvpUsersFetchState;
            }

            public final EventDetailsSection getSection() {
                return this.section;
            }

            public final int getSegmentedControlIndex() {
                return this.segmentedControlIndex;
            }

            public int hashCode() {
                GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
                int i = 0;
                int hashCode = (guildScheduledEvent != null ? guildScheduledEvent.hashCode() : 0) * 31;
                GuildScheduledEventLocationInfo guildScheduledEventLocationInfo = this.locationInfo;
                int hashCode2 = (hashCode + (guildScheduledEventLocationInfo != null ? guildScheduledEventLocationInfo.hashCode() : 0)) * 31;
                Guild guild = this.guild;
                int hashCode3 = (hashCode2 + (guild != null ? guild.hashCode() : 0)) * 31;
                Channel channel = this.channel;
                int hashCode4 = (hashCode3 + (channel != null ? channel.hashCode() : 0)) * 31;
                boolean z2 = this.isInGuild;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode4 + i3) * 31;
                boolean z3 = this.isRsvped;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (i5 + i6) * 31;
                boolean z4 = this.canShare;
                if (z4) {
                    z4 = true;
                }
                int i9 = z4 ? 1 : 0;
                int i10 = z4 ? 1 : 0;
                int i11 = (i8 + i9) * 31;
                boolean z5 = this.isConnected;
                if (z5) {
                    z5 = true;
                }
                int i12 = z5 ? 1 : 0;
                int i13 = z5 ? 1 : 0;
                int i14 = (i11 + i12) * 31;
                boolean z6 = this.canStartEvent;
                if (z6) {
                    z6 = true;
                }
                int i15 = z6 ? 1 : 0;
                int i16 = z6 ? 1 : 0;
                int i17 = (i14 + i15) * 31;
                boolean z7 = this.isDeveloperMode;
                if (z7) {
                    z7 = true;
                }
                int i18 = z7 ? 1 : 0;
                int i19 = z7 ? 1 : 0;
                int i20 = (i17 + i18) * 31;
                boolean z8 = this.canConnect;
                if (!z8) {
                    i2 = z8 ? 1 : 0;
                }
                int i21 = (i20 + i2) * 31;
                List<GuildScheduledEventRsvpUserListItem.RsvpUser> list = this.rsvpUsers;
                int hashCode5 = (i21 + (list != null ? list.hashCode() : 0)) * 31;
                EventDetailsSection eventDetailsSection = this.section;
                int hashCode6 = (hashCode5 + (eventDetailsSection != null ? eventDetailsSection.hashCode() : 0)) * 31;
                EventDetailsRsvpUsersFetchState eventDetailsRsvpUsersFetchState = this.rsvpUsersFetchState;
                int hashCode7 = (((hashCode6 + (eventDetailsRsvpUsersFetchState != null ? eventDetailsRsvpUsersFetchState.hashCode() : 0)) * 31) + this.segmentedControlIndex) * 31;
                UserGuildMember userGuildMember = this.creator;
                if (userGuildMember != null) {
                    i = userGuildMember.hashCode();
                }
                return hashCode7 + i;
            }

            public final boolean isConnected() {
                return this.isConnected;
            }

            public final boolean isDeveloperMode() {
                return this.isDeveloperMode;
            }

            public final boolean isInGuild() {
                return this.isInGuild;
            }

            public final boolean isRsvped() {
                return this.isRsvped;
            }

            public String toString() {
                StringBuilder R = a.R("Initialized(guildScheduledEvent=");
                R.append(this.guildScheduledEvent);
                R.append(", locationInfo=");
                R.append(this.locationInfo);
                R.append(", guild=");
                R.append(this.guild);
                R.append(", channel=");
                R.append(this.channel);
                R.append(", isInGuild=");
                R.append(this.isInGuild);
                R.append(", isRsvped=");
                R.append(this.isRsvped);
                R.append(", canShare=");
                R.append(this.canShare);
                R.append(", isConnected=");
                R.append(this.isConnected);
                R.append(", canStartEvent=");
                R.append(this.canStartEvent);
                R.append(", isDeveloperMode=");
                R.append(this.isDeveloperMode);
                R.append(", canConnect=");
                R.append(this.canConnect);
                R.append(", rsvpUsers=");
                R.append(this.rsvpUsers);
                R.append(", section=");
                R.append(this.section);
                R.append(", rsvpUsersFetchState=");
                R.append(this.rsvpUsersFetchState);
                R.append(", segmentedControlIndex=");
                R.append(this.segmentedControlIndex);
                R.append(", creator=");
                R.append(this.creator);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: GuildScheduledEventDetailsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildScheduledEventDetailsViewModel(com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsArgs r22, com.discord.stores.updates.ObservationDeck r23, com.discord.stores.StoreGuildScheduledEvents r24, com.discord.stores.StoreGuilds r25, com.discord.stores.StoreUser r26, com.discord.stores.StoreChannels r27, com.discord.stores.StorePermissions r28, com.discord.stores.StoreUserSettings r29, com.discord.stores.StoreVoiceChannelSelected r30, com.discord.stores.StoreDirectories r31, rx.Observable r32, com.discord.widgets.guildscheduledevent.EventDetailsSection r33, com.discord.widgets.guildscheduledevent.EventDetailsRsvpUsersFetchState r34, int r35, int r36, kotlin.jvm.internal.DefaultConstructorMarker r37) {
        /*
            Method dump skipped, instructions count: 219
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel.<init>(com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsArgs, com.discord.stores.updates.ObservationDeck, com.discord.stores.StoreGuildScheduledEvents, com.discord.stores.StoreGuilds, com.discord.stores.StoreUser, com.discord.stores.StoreChannels, com.discord.stores.StorePermissions, com.discord.stores.StoreUserSettings, com.discord.stores.StoreVoiceChannelSelected, com.discord.stores.StoreDirectories, rx.Observable, com.discord.widgets.guildscheduledevent.EventDetailsSection, com.discord.widgets.guildscheduledevent.EventDetailsRsvpUsersFetchState, int, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final EventDetailsRsvpUsersFetchState getRsvpUsersFetchState(StoreState storeState) {
        return (!storeState.isRsvpUsersFetching() || !storeState.getRsvpUsers().isEmpty()) ? storeState.isRsvpUsersError() ? EventDetailsRsvpUsersFetchState.ERROR : storeState.getRsvpUsers().isEmpty() ? EventDetailsRsvpUsersFetchState.EMPTY : EventDetailsRsvpUsersFetchState.SUCCESS : EventDetailsRsvpUsersFetchState.LOADING;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Object obj;
        boolean z2;
        if (storeState.getGuildScheduledEvent() == null || storeState.getGuild() == null) {
            obj = ViewState.Invalid.INSTANCE;
        } else {
            if (getViewState() == null) {
                this.guildScheduledEventsStore.fetchGuildScheduledEventUserCounts(storeState.getGuild().getId());
            }
            GuildScheduledEventLocationInfo buildLocationInfo = (this.args.getSource() == GuildScheduledEventDetailsSource.Guild || storeState.getGuildScheduledEvent().f() == GuildScheduledEventEntityType.EXTERNAL || storeState.isInGuild()) ? GuildScheduledEventLocationInfo.Companion.buildLocationInfo(storeState.getGuildScheduledEvent(), storeState.getChannel()) : null;
            this.rsvpUsersFetchState = getRsvpUsersFetchState(storeState);
            GuildScheduledEvent guildScheduledEvent = storeState.getGuildScheduledEvent();
            Guild guild = storeState.getGuild();
            Channel channel = storeState.getChannel();
            boolean isInGuild = storeState.isInGuild();
            boolean isRsvped = storeState.isRsvped();
            Long b2 = storeState.getGuildScheduledEvent().b();
            boolean z3 = false;
            if (b2 != null) {
                long longValue = b2.longValue();
                Long selectedVoiceChannelId = storeState.getSelectedVoiceChannelId();
                if (selectedVoiceChannelId != null && longValue == selectedVoiceChannelId.longValue()) {
                    z3 = true;
                }
                z2 = z3;
            } else {
                z2 = false;
            }
            obj = new ViewState.Initialized(guildScheduledEvent, buildLocationInfo, guild, channel, isInGuild, isRsvped, storeState.getCanShare(), z2, storeState.getCanStartEvent(), storeState.isDeveloperMode(), storeState.getCanConnect(), storeState.getRsvpUsers(), this.section, this.rsvpUsersFetchState, this.segmentControlIndex, storeState.getCreator());
        }
        updateViewState(obj);
    }

    public final void endEventClicked(Context context, Function0<Unit> function0) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(function0, "onSuccess");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Initialized)) {
            viewState = null;
        }
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (initialized != null) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(GuildScheduledEventAPI.INSTANCE.endEvent(initialized.getGuildScheduledEvent().h(), initialized.getGuildScheduledEvent().i()), false, 1, null), GuildScheduledEventDetailsViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildScheduledEventDetailsViewModel$endEventClicked$$inlined$let$lambda$1(this, context, function0));
        }
    }

    public final void onDeleteButtonClicked(Context context, Function0<Unit> function0) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(function0, "onSuccess");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Initialized)) {
            viewState = null;
        }
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (initialized != null) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteGuildScheduledEvent(initialized.getGuildScheduledEvent().h(), initialized.getGuildScheduledEvent().i()), false, 1, null), GuildScheduledEventDetailsViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildScheduledEventDetailsViewModel$onDeleteButtonClicked$$inlined$let$lambda$1(this, context, function0));
        }
    }

    public final void onGuildNameClicked() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Initialized)) {
            viewState = null;
        }
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (initialized != null) {
            StoreStream.Companion.getGuildSelected().set(initialized.getGuild().getId());
        }
    }

    public final void onRsvpButtonClicked() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Initialized)) {
            viewState = null;
        }
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (initialized != null) {
            this.guildScheduledEventsStore.toggleMeRsvpForEvent(initialized.getGuildScheduledEvent());
        }
    }

    public final void onShareButtonClicked(WeakReference<Fragment> weakReference) {
        m.checkNotNullParameter(weakReference, "weakFragment");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Initialized)) {
            viewState = null;
        }
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (initialized != null) {
            Long b2 = initialized.getGuildScheduledEvent().b();
            long h = initialized.getGuildScheduledEvent().h();
            long i = initialized.getGuildScheduledEvent().i();
            GuildScheduledEventUtilities.Companion companion = GuildScheduledEventUtilities.Companion;
            boolean canShareEvent$default = GuildScheduledEventUtilities.Companion.canShareEvent$default(companion, b2, h, null, null, null, null, 60, null);
            Fragment fragment = weakReference.get();
            if (fragment != null) {
                m.checkNotNullExpressionValue(fragment, "weakFragment.get() ?: return");
                companion.launchInvite(canShareEvent$default, fragment, h, initialized.getChannel(), i);
            }
        }
    }

    public final void setSegmentedControlIndex(int i) {
        if (this.segmentControlIndex != i) {
            ViewState viewState = getViewState();
            if (!(viewState instanceof ViewState.Initialized)) {
                viewState = null;
            }
            ViewState.Initialized initialized = (ViewState.Initialized) viewState;
            if (initialized != null) {
                this.segmentControlIndex = i;
                EventDetailsSection eventDetailsSection = i == 0 ? EventDetailsSection.EVENT_INFO : EventDetailsSection.RSVP_LIST;
                this.section = eventDetailsSection;
                if (eventDetailsSection == EventDetailsSection.RSVP_LIST) {
                    this.guildScheduledEventsStore.fetchGuildScheduledEventUsers(initialized.getGuildScheduledEvent().h(), initialized.getGuildScheduledEvent().i());
                    if (initialized.getRsvpUsers().isEmpty()) {
                        this.rsvpUsersFetchState = EventDetailsRsvpUsersFetchState.LOADING;
                    }
                }
                updateViewState(ViewState.Initialized.copy$default(initialized, null, null, null, null, false, false, false, false, false, false, false, null, this.section, this.rsvpUsersFetchState, this.segmentControlIndex, null, 36863, null));
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventDetailsViewModel(GuildScheduledEventDetailsArgs guildScheduledEventDetailsArgs, ObservationDeck observationDeck, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreGuilds storeGuilds, StoreUser storeUser, StoreChannels storeChannels, StorePermissions storePermissions, StoreUserSettings storeUserSettings, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreDirectories storeDirectories, Observable<StoreState> observable, EventDetailsSection eventDetailsSection, EventDetailsRsvpUsersFetchState eventDetailsRsvpUsersFetchState, int i) {
        super(null, 1, null);
        m.checkNotNullParameter(guildScheduledEventDetailsArgs, "args");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventsStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeUserSettings, "userSettingsStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "selectedVoiceChannelStore");
        m.checkNotNullParameter(storeDirectories, "directoriesStore");
        m.checkNotNullParameter(observable, "storeStateObservable");
        m.checkNotNullParameter(eventDetailsSection, "section");
        m.checkNotNullParameter(eventDetailsRsvpUsersFetchState, "rsvpUsersFetchState");
        this.args = guildScheduledEventDetailsArgs;
        this.guildScheduledEventsStore = storeGuildScheduledEvents;
        this.userStore = storeUser;
        this.section = eventDetailsSection;
        this.rsvpUsersFetchState = eventDetailsRsvpUsersFetchState;
        this.segmentControlIndex = i;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GuildScheduledEventDetailsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
