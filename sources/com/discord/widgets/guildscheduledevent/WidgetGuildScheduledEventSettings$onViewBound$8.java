package com.discord.widgets.guildscheduledevent;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import com.discord.utilities.time.ClockFactory;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventSettingsViewModel;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetGuildScheduledEventSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/view/View;", "kotlin.jvm.PlatformType", "it", "", "onClick", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventSettings$onViewBound$8 implements View.OnClickListener {
    public final /* synthetic */ WidgetGuildScheduledEventSettings this$0;

    public WidgetGuildScheduledEventSettings$onViewBound$8(WidgetGuildScheduledEventSettings widgetGuildScheduledEventSettings) {
        this.this$0 = widgetGuildScheduledEventSettings;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized;
        GuildScheduledEventModel eventModel;
        initialized = this.this$0.currentViewState;
        if (initialized != null && (eventModel = initialized.getEventModel()) != null) {
            GuildScheduledEventPickerDate first = GuildScheduledEventPickerDateTime.INSTANCE.generateDefaultEndDateTime(eventModel.getStartDate(), eventModel.getStartTime()).getFirst();
            Context requireContext = this.this$0.requireContext();
            DatePickerDialog.OnDateSetListener widgetGuildScheduledEventSettings$onViewBound$8$$special$$inlined$let$lambda$1 = new DatePickerDialog.OnDateSetListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$8$$special$$inlined$let$lambda$1
                @Override // android.app.DatePickerDialog.OnDateSetListener
                public final void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
                    GuildScheduledEventSettingsViewModel viewModel;
                    viewModel = WidgetGuildScheduledEventSettings$onViewBound$8.this.this$0.getViewModel();
                    GuildScheduledEventSettingsViewModel.DateError endDate = viewModel.setEndDate(i, i2, i3);
                    if (endDate != null) {
                        WidgetGuildScheduledEventSettings$onViewBound$8.this.this$0.showDateErrorToast(endDate);
                    }
                }
            };
            GuildScheduledEventPickerDate endDate = eventModel.getEndDate();
            int year = endDate != null ? endDate.getYear() : first.getYear();
            GuildScheduledEventPickerDate endDate2 = eventModel.getEndDate();
            int month = endDate2 != null ? endDate2.getMonth() : first.getMonth();
            GuildScheduledEventPickerDate endDate3 = eventModel.getEndDate();
            DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext, widgetGuildScheduledEventSettings$onViewBound$8$$special$$inlined$let$lambda$1, year, month, endDate3 != null ? endDate3.getDayOfMonth() : first.getDayOfMonth());
            DatePicker datePicker = datePickerDialog.getDatePicker();
            m.checkNotNullExpressionValue(datePicker, "datePicker");
            datePicker.setMinDate(Math.max(eventModel.getStartDate().toMillis(), ClockFactory.get().currentTimeMillis()));
            datePickerDialog.show();
        }
    }
}
