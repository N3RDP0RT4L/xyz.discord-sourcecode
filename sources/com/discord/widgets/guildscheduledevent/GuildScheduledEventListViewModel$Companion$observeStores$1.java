package com.discord.widgets.guildscheduledevent;

import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.models.guild.UserGuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventsComparator;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventListViewModel;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
/* compiled from: GuildScheduledEventListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;", "invoke", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventListViewModel$Companion$observeStores$1 extends o implements Function0<GuildScheduledEventListViewModel.StoreState> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ StoreChannels $channelStore;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildScheduledEvents $guildScheduledEventsStore;
    public final /* synthetic */ StoreGuilds $guildStore;
    public final /* synthetic */ StorePermissions $permissionsStore;
    public final /* synthetic */ StoreUser $userStore;
    public final /* synthetic */ StoreVoiceChannelSelected $voiceChannelSelectedStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventListViewModel$Companion$observeStores$1(StoreChannels storeChannels, long j, StoreGuildScheduledEvents storeGuildScheduledEvents, Long l, StoreUser storeUser, StoreGuilds storeGuilds, StorePermissions storePermissions, StoreVoiceChannelSelected storeVoiceChannelSelected) {
        super(0);
        this.$channelStore = storeChannels;
        this.$guildId = j;
        this.$guildScheduledEventsStore = storeGuildScheduledEvents;
        this.$channelId = l;
        this.$userStore = storeUser;
        this.$guildStore = storeGuilds;
        this.$permissionsStore = storePermissions;
        this.$voiceChannelSelectedStore = storeVoiceChannelSelected;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final GuildScheduledEventListViewModel.StoreState invoke() {
        Map<Long, Channel> channelsForGuild = this.$channelStore.getChannelsForGuild(this.$guildId);
        List guildScheduledEvents$default = StoreGuildScheduledEvents.getGuildScheduledEvents$default(this.$guildScheduledEventsStore, this.$guildId, false, 2, null);
        if (this.$channelId != null) {
            ArrayList arrayList = new ArrayList();
            for (Object obj : guildScheduledEvents$default) {
                if (m.areEqual(((GuildScheduledEvent) obj).b(), this.$channelId)) {
                    arrayList.add(obj);
                }
            }
            guildScheduledEvents$default = arrayList;
        }
        List<GuildScheduledEvent> sortedWith = u.sortedWith(guildScheduledEvents$default, GuildScheduledEventsComparator.INSTANCE);
        ArrayList arrayList2 = new ArrayList();
        for (GuildScheduledEvent guildScheduledEvent : sortedWith) {
            Long c = guildScheduledEvent.c();
            if (c != null) {
                arrayList2.add(c);
            }
        }
        Set set = u.toSet(arrayList2);
        boolean z2 = true;
        Map<Long, User> users = this.$userStore.getUsers(set, true);
        ArrayList arrayList3 = new ArrayList();
        Iterator it = set.iterator();
        while (true) {
            Pair pair = null;
            if (!it.hasNext()) {
                break;
            }
            long longValue = ((Number) it.next()).longValue();
            User user = users.get(Long.valueOf(longValue));
            if (user != null) {
                pair = d0.o.to(Long.valueOf(longValue), new UserGuildMember(user, this.$guildStore.getMember(this.$guildId, longValue)));
            }
            if (pair != null) {
                arrayList3.add(pair);
            }
        }
        Map map = h0.toMap(arrayList3);
        Set<Long> meGuildScheduledEventIds = this.$guildScheduledEventsStore.getMeGuildScheduledEventIds(this.$guildId);
        boolean canCreateAnyEvent = GuildScheduledEventUtilities.Companion.canCreateAnyEvent(this.$guildId, this.$channelStore, this.$permissionsStore);
        Map<Long, Long> permissionsByChannel = this.$permissionsStore.getPermissionsByChannel();
        Long valueOf = Long.valueOf(this.$voiceChannelSelectedStore.getSelectedVoiceChannelId());
        if (valueOf.longValue() == 0) {
            z2 = false;
        }
        return new GuildScheduledEventListViewModel.StoreState(sortedWith, meGuildScheduledEventIds, channelsForGuild, map, canCreateAnyEvent, z2 ? valueOf : null, permissionsByChannel);
    }
}
