package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.e0;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventBroadcast;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGuildScheduledEventSettingsBinding;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventTiming;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventSettingsViewModel;
import com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGuildScheduledEventSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 ?2\u00020\u0001:\u0001?B\u0007¢\u0006\u0004\b>\u0010\u001dJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\tJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\u0012\u001a\u00020\u00042\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00100\u000fH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u0018H\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001c\u0010\u001dR\u0018\u0010\u001e\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001f\u0010%\u001a\u0004\u0018\u00010 8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u001d\u0010)\u001a\u00020 8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b&\u0010\"\u001a\u0004\b'\u0010(R\u001d\u0010.\u001a\u00020*8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b+\u0010\"\u001a\u0004\b,\u0010-R\u001d\u00103\u001a\u00020/8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b0\u0010\"\u001a\u0004\b1\u00102R\u001d\u00109\u001a\u0002048B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u00108R\u001c\u0010<\u001a\b\u0012\u0004\u0012\u00020;0:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=¨\u0006@"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;", "viewState", "", "configureUi", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;", "configureStartDateTime", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;)V", "configureEndDateTime", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$DateError;", "error", "showDateErrorToast", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$DateError;)V", "Lkotlin/Pair;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationState;", "states", "configureVisibilitySettings", "(Lkotlin/Pair;)V", "", "show", "toggleVisibilityOptions", "(Z)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "()V", "currentViewState", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;", "", "existingGuildScheduledEventId$delegate", "Lkotlin/Lazy;", "getExistingGuildScheduledEventId", "()Ljava/lang/Long;", "existingGuildScheduledEventId", "guildId$delegate", "getGuildId", "()J", "guildId", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationViewModel;", "hubViewModel$delegate", "getHubViewModel", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationViewModel;", "hubViewModel", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel;", "viewModel", "Lcom/discord/databinding/WidgetGuildScheduledEventSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildScheduledEventSettingsBinding;", "binding", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "previewLauncher", "Landroidx/activity/result/ActivityResultLauncher;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildScheduledEventSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildScheduledEventSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_ENTITY_TYPE = "EXTRA_ENTITY_TYPE";
    private static final String EXTRA_EXTERNAL_LOCATION = "EXTRA_EXTERNAL_LOCATION";
    private static final int RESULT_QUIT = 2;
    private GuildScheduledEventSettingsViewModel.ViewState.Initialized currentViewState;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildScheduledEventSettings$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetGuildScheduledEventSettings$guildId$2(this));
    private final Lazy existingGuildScheduledEventId$delegate = g.lazy(new WidgetGuildScheduledEventSettings$existingGuildScheduledEventId$2(this));
    private final Lazy hubViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildScheduledEventDirectoryAssociationViewModel.class), new WidgetGuildScheduledEventSettings$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(new WidgetGuildScheduledEventSettings$hubViewModel$2(this)));
    private final ActivityResultLauncher<Intent> previewLauncher = WidgetPreviewGuildScheduledEvent.Companion.registerForResult(this, new WidgetGuildScheduledEventSettings$previewLauncher$1(this), new WidgetGuildScheduledEventSettings$previewLauncher$2(this));

    /* compiled from: WidgetGuildScheduledEventSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b#\u0010$Ja\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0006\u0010\u000b\u001a\u00020\n2\u000e\u0010\r\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u000e\u0010\u0011\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\u0010¢\u0006\u0004\b\u0013\u0010\u0014J?\u0010\u001b\u001a\u0010\u0012\f\u0012\n \u001a*\u0004\u0018\u00010\u00050\u00050\u00042\u0006\u0010\u0016\u001a\u00020\u00152\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00120\u00172\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00120\u0017¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u001eR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006%"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventSettings$Companion;", "", "Landroid/content/Context;", "context", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "launcher", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "entityType", "Lcom/discord/primitives/ChannelId;", "channelId", "", "externalLocation", "Lcom/discord/primitives/GuildScheduledEventId;", "existingGuildScheduledEventId", "", "launch", "(Landroid/content/Context;Landroidx/activity/result/ActivityResultLauncher;JLcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;)V", "Landroidx/fragment/app/Fragment;", "fragment", "Lkotlin/Function0;", "onFinished", "onQuit", "kotlin.jvm.PlatformType", "registerForResult", "(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Landroidx/activity/result/ActivityResultLauncher;", WidgetGuildScheduledEventSettings.EXTRA_ENTITY_TYPE, "Ljava/lang/String;", WidgetGuildScheduledEventSettings.EXTRA_EXTERNAL_LOCATION, "", "RESULT_QUIT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, ActivityResultLauncher<Intent> activityResultLauncher, long j, GuildScheduledEventEntityType guildScheduledEventEntityType, Long l, String str, Long l2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(activityResultLauncher, "launcher");
            m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
            Intent intent = new Intent();
            intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            intent.putExtra(WidgetGuildScheduledEventSettings.EXTRA_ENTITY_TYPE, guildScheduledEventEntityType);
            if (l != null) {
                intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", l.longValue());
            }
            if (str != null) {
                intent.putExtra(WidgetGuildScheduledEventSettings.EXTRA_EXTERNAL_LOCATION, str);
            }
            if (l2 != null) {
                intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_SCHEDULED_EVENT_ID", l2.longValue());
            }
            j.g.f(context, activityResultLauncher, WidgetGuildScheduledEventSettings.class, intent);
        }

        public final ActivityResultLauncher<Intent> registerForResult(Fragment fragment, final Function0<Unit> function0, final Function0<Unit> function02) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(function0, "onFinished");
            m.checkNotNullParameter(function02, "onQuit");
            ActivityResultLauncher<Intent> registerForActivityResult = fragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$Companion$registerForResult$1
                public final void onActivityResult(ActivityResult activityResult) {
                    m.checkNotNullExpressionValue(activityResult, "result");
                    int resultCode = activityResult.getResultCode();
                    if (resultCode == -1) {
                        Function0.this.invoke();
                    } else if (resultCode == 2) {
                        function02.invoke();
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…  }\n          }\n        }");
            return registerForActivityResult;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            GuildScheduledEventSettingsViewModel.DateError.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[GuildScheduledEventSettingsViewModel.DateError.INVALID_VIEW_STATE.ordinal()] = 1;
            iArr[GuildScheduledEventSettingsViewModel.DateError.START_DATE_IN_PAST.ordinal()] = 2;
            iArr[GuildScheduledEventSettingsViewModel.DateError.END_DATE_IN_PAST.ordinal()] = 3;
            iArr[GuildScheduledEventSettingsViewModel.DateError.END_DATE_BEFORE_START_DATE.ordinal()] = 4;
        }
    }

    public WidgetGuildScheduledEventSettings() {
        super(R.layout.widget_guild_scheduled_event_settings);
        WidgetGuildScheduledEventSettings$viewModel$2 widgetGuildScheduledEventSettings$viewModel$2 = new WidgetGuildScheduledEventSettings$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildScheduledEventSettingsViewModel.class), new WidgetGuildScheduledEventSettings$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildScheduledEventSettings$viewModel$2));
    }

    private final void configureEndDateTime(GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized) {
        String str;
        if (initialized.getEventModel().getEntityType() != GuildScheduledEventEntityType.EXTERNAL) {
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.endDateHeader");
            textView.setVisibility(8);
            TextView textView2 = getBinding().g;
            m.checkNotNullExpressionValue(textView2, "binding.endTimeHeader");
            textView2.setVisibility(8);
            TextInputLayout textInputLayout = getBinding().f;
            m.checkNotNullExpressionValue(textInputLayout, "binding.endDateLayout");
            textInputLayout.setVisibility(8);
            TextInputLayout textInputLayout2 = getBinding().i;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.endTimeLayout");
            textInputLayout2.setVisibility(8);
            return;
        }
        TextView textView3 = getBinding().d;
        m.checkNotNullExpressionValue(textView3, "binding.endDateHeader");
        textView3.setVisibility(0);
        TextView textView4 = getBinding().g;
        m.checkNotNullExpressionValue(textView4, "binding.endTimeHeader");
        textView4.setVisibility(0);
        TextInputLayout textInputLayout3 = getBinding().f;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.endDateLayout");
        textInputLayout3.setVisibility(0);
        TextInputLayout textInputLayout4 = getBinding().i;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.endTimeLayout");
        textInputLayout4.setVisibility(0);
        GuildScheduledEventPickerDate endDate = initialized.getEventModel().getEndDate();
        GuildScheduledEventPickerTime endTime = initialized.getEventModel().getEndTime();
        String str2 = "";
        if (endDate == null || (str = DateUtils.formatDateTime(getContext(), endDate.toMillis(), 524292)) == null) {
            str = str2;
        }
        if (endTime != null) {
            GuildScheduledEventPickerDateTime guildScheduledEventPickerDateTime = GuildScheduledEventPickerDateTime.INSTANCE;
            if (endDate == null) {
                endDate = GuildScheduledEventPickerDate.Companion.now();
            }
            String formatDateTime = DateUtils.formatDateTime(getContext(), guildScheduledEventPickerDateTime.toMillis(endDate, endTime), 257);
            if (formatDateTime != null) {
                str2 = formatDateTime;
            }
        }
        getBinding().e.setText(str);
        getBinding().h.setText(str2);
    }

    private final void configureStartDateTime(GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized) {
        long millis = GuildScheduledEventPickerDateTime.INSTANCE.toMillis(initialized.getEventModel().getStartDate(), initialized.getEventModel().getStartTime());
        GuildScheduledEvent existingEvent = initialized.getExistingEvent();
        boolean z2 = (existingEvent != null ? GuildScheduledEventUtilitiesKt.getEventTiming(existingEvent) : null) == GuildScheduledEventTiming.LIVE;
        TextInputEditText textInputEditText = getBinding().l;
        ViewExtensions.setEnabledAndAlpha$default(textInputEditText, !z2, 0.0f, 2, null);
        textInputEditText.setText(DateUtils.formatDateTime(textInputEditText.getContext(), millis, 524292));
        TextInputEditText textInputEditText2 = getBinding().m;
        ViewExtensions.setEnabledAndAlpha$default(textInputEditText2, !z2, 0.0f, 2, null);
        textInputEditText2.setText(DateUtils.formatDateTime(textInputEditText2.getContext(), millis, 1));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(GuildScheduledEventSettingsViewModel.ViewState viewState) {
        if (viewState instanceof GuildScheduledEventSettingsViewModel.ViewState.Invalid) {
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.guildscheduledevent.GuildScheduledEventSettingsViewModel.ViewState.Initialized");
        GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized = (GuildScheduledEventSettingsViewModel.ViewState.Initialized) viewState;
        this.currentViewState = initialized;
        GuildScheduledEventModel eventModel = initialized.getEventModel();
        TextInputEditText textInputEditText = getBinding().o;
        Editable text = textInputEditText.getText();
        String str = null;
        if (!m.areEqual(text != null ? text.toString() : null, eventModel.getName())) {
            textInputEditText.setText(eventModel.getName());
        }
        TextInputEditText textInputEditText2 = getBinding().j;
        Editable text2 = textInputEditText2.getText();
        if (text2 != null) {
            str = text2.toString();
        }
        if (!m.areEqual(str, eventModel.getDescription())) {
            textInputEditText2.setText(eventModel.getDescription());
        }
        configureStartDateTime(initialized);
        configureEndDateTime(initialized);
        MaterialButton materialButton = getBinding().k;
        m.checkNotNullExpressionValue(materialButton, "binding.guildScheduledEventSettingsNextButton");
        materialButton.setEnabled(getViewModel().isNextButtonEnabled());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureVisibilitySettings(Pair<? extends GuildScheduledEventSettingsViewModel.ViewState, GuildScheduledEventDirectoryAssociationState> pair) {
        GuildScheduledEventSettingsViewModel.ViewState component1 = pair.component1();
        GuildScheduledEventBroadcast invoke = pair.component2().getEnabledAsync().invoke();
        if (invoke != null && (component1 instanceof GuildScheduledEventSettingsViewModel.ViewState.Initialized)) {
            toggleVisibilityOptions(invoke.a());
            GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized = (GuildScheduledEventSettingsViewModel.ViewState.Initialized) component1;
            boolean isEventViewableByEveryone = GuildScheduledEventUtilities.Companion.isEventViewableByEveryone(initialized.getEventModel().getChannelId());
            LinearLayout linearLayout = getBinding().q;
            m.checkNotNullExpressionValue(linearLayout, "binding.guildScheduledEv…ttingsVisibilityContainer");
            linearLayout.setClickable(isEventViewableByEveryone);
            View view = getBinding().r;
            m.checkNotNullExpressionValue(view, "binding.guildScheduledEv…sibilityContainerDisabled");
            boolean z2 = false;
            view.setVisibility(isEventViewableByEveryone ^ true ? 0 : 8);
            MaterialCheckBox materialCheckBox = getBinding().p;
            m.checkNotNullExpressionValue(materialCheckBox, "binding.guildScheduledEv…ettingsVisibilityCheckbox");
            materialCheckBox.setClickable(isEventViewableByEveryone);
            MaterialCheckBox materialCheckBox2 = getBinding().p;
            m.checkNotNullExpressionValue(materialCheckBox2, "binding.guildScheduledEv…ettingsVisibilityCheckbox");
            materialCheckBox2.setChecked(m.areEqual(initialized.getEventModel().getBroadcastToDirectoryChannels(), Boolean.TRUE));
            if (invoke.a() && initialized.getEventModel().getBroadcastToDirectoryChannels() == null) {
                if (isEventViewableByEveryone) {
                    Boolean b2 = invoke.b();
                    z2 = b2 != null ? b2.booleanValue() : true;
                }
                getViewModel().toggleBroadcastToDirectoryChannel(z2);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGuildScheduledEventSettingsBinding getBinding() {
        return (WidgetGuildScheduledEventSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Long getExistingGuildScheduledEventId() {
        return (Long) this.existingGuildScheduledEventId$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    private final GuildScheduledEventDirectoryAssociationViewModel getHubViewModel() {
        return (GuildScheduledEventDirectoryAssociationViewModel) this.hubViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildScheduledEventSettingsViewModel getViewModel() {
        return (GuildScheduledEventSettingsViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showDateErrorToast(GuildScheduledEventSettingsViewModel.DateError dateError) {
        int ordinal = dateError.ordinal();
        if (ordinal == 1) {
            b.a.d.m.g(requireContext(), R.string.guild_event_past_start_date, 0, null, 12);
        } else if (ordinal == 2) {
            b.a.d.m.g(requireContext(), R.string.guild_event_past_end_date_1, 0, null, 12);
        } else if (ordinal == 3) {
            b.a.d.m.g(requireContext(), R.string.guild_event_end_date_before_start_date, 0, null, 12);
        }
    }

    private final void toggleVisibilityOptions(boolean z2) {
        TextView textView = getBinding().t;
        m.checkNotNullExpressionValue(textView, "binding.guildScheduledEv…tSettingsVisibilityHeader");
        int i = 0;
        textView.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout = getBinding().q;
        m.checkNotNullExpressionValue(linearLayout, "binding.guildScheduledEv…ttingsVisibilityContainer");
        linearLayout.setVisibility(z2 ? 0 : 8);
        TextView textView2 = getBinding().f2426s;
        m.checkNotNullExpressionValue(textView2, "binding.guildScheduledEv…ingsVisibilityDescription");
        if (!z2) {
            i = 8;
        }
        textView2.setVisibility(i);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildScheduledEventSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildScheduledEventSettings$onResume$1(this));
        Observable j = Observable.j(getViewModel().observeViewState(), getHubViewModel().observeViewState(), WidgetGuildScheduledEventSettings$onResume$2.INSTANCE);
        m.checkNotNullExpressionValue(j, "Observable.combineLatest…ttingsState to hubState }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(ObservableExtensionsKt.ui(j), this, null, 2, null), WidgetGuildScheduledEventSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildScheduledEventSettings$onResume$3(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        TextView textView = getBinding().n;
        m.checkNotNullExpressionValue(textView, "binding.guildScheduledEventSettingsStepText");
        b.m(textView, R.string.guild_event_step_label, new Object[]{2, 3}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().f2425b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                FragmentActivity activity = WidgetGuildScheduledEventSettings.this.e();
                if (activity != null) {
                    activity.finish();
                }
            }
        });
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                FragmentActivity activity = WidgetGuildScheduledEventSettings.this.e();
                if (activity != null) {
                    activity.setResult(2);
                    activity.finish();
                }
            }
        });
        TextInputEditText textInputEditText = getBinding().o;
        m.checkNotNullExpressionValue(textInputEditText, "binding.guildScheduledEventSettingsTopicInput");
        textInputEditText.addTextChangedListener(new TextWatcher() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$$inlined$addTextChangedListener$1
            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                String obj;
                GuildScheduledEventSettingsViewModel viewModel;
                if (editable != null && (obj = editable.toString()) != null) {
                    viewModel = WidgetGuildScheduledEventSettings.this.getViewModel();
                    viewModel.setTopic(obj);
                }
            }

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        TextInputEditText textInputEditText2 = getBinding().j;
        m.checkNotNullExpressionValue(textInputEditText2, "binding.guildScheduledEv…tSettingsDescriptionInput");
        textInputEditText2.addTextChangedListener(new TextWatcher() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$$inlined$addTextChangedListener$2
            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                String obj;
                GuildScheduledEventSettingsViewModel viewModel;
                if (editable != null && (obj = editable.toString()) != null) {
                    viewModel = WidgetGuildScheduledEventSettings.this.getViewModel();
                    viewModel.setDescription(obj);
                }
            }

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        getBinding().m.setOnClickListener(new WidgetGuildScheduledEventSettings$onViewBound$5(this));
        getBinding().l.setOnClickListener(new WidgetGuildScheduledEventSettings$onViewBound$6(this));
        getBinding().h.setOnClickListener(new WidgetGuildScheduledEventSettings$onViewBound$7(this));
        getBinding().e.setOnClickListener(new WidgetGuildScheduledEventSettings$onViewBound$8(this));
        getBinding().k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized;
                GuildScheduledEventSettingsViewModel viewModel;
                GuildScheduledEventSettingsViewModel viewModel2;
                GuildScheduledEventSettingsViewModel viewModel3;
                Long existingGuildScheduledEventId;
                ActivityResultLauncher<Intent> activityResultLauncher;
                GuildScheduledEventSettingsViewModel viewModel4;
                GuildScheduledEventSettingsViewModel viewModel5;
                initialized = WidgetGuildScheduledEventSettings.this.currentViewState;
                if (initialized != null) {
                    viewModel = WidgetGuildScheduledEventSettings.this.getViewModel();
                    if (viewModel.hasStartTimeChanged(initialized)) {
                        viewModel5 = WidgetGuildScheduledEventSettings.this.getViewModel();
                        if (!viewModel5.isDateInFuture(initialized.getEventModel().getStartDate(), initialized.getEventModel().getStartTime())) {
                            WidgetGuildScheduledEventSettings.this.showDateErrorToast(GuildScheduledEventSettingsViewModel.DateError.START_DATE_IN_PAST);
                            return;
                        }
                    }
                    viewModel2 = WidgetGuildScheduledEventSettings.this.getViewModel();
                    if (viewModel2.hasEndTimeChanged(initialized)) {
                        viewModel4 = WidgetGuildScheduledEventSettings.this.getViewModel();
                        if (!viewModel4.isDateInFuture(initialized.getEventModel().getEndDate(), initialized.getEventModel().getEndTime())) {
                            WidgetGuildScheduledEventSettings.this.showDateErrorToast(GuildScheduledEventSettingsViewModel.DateError.END_DATE_IN_PAST);
                            return;
                        }
                    }
                    viewModel3 = WidgetGuildScheduledEventSettings.this.getViewModel();
                    if (!viewModel3.isStartDateBeforeEndDate(initialized)) {
                        WidgetGuildScheduledEventSettings.this.showDateErrorToast(GuildScheduledEventSettingsViewModel.DateError.END_DATE_BEFORE_START_DATE);
                        return;
                    }
                    WidgetPreviewGuildScheduledEvent.Companion companion = WidgetPreviewGuildScheduledEvent.Companion;
                    Context requireContext = WidgetGuildScheduledEventSettings.this.requireContext();
                    GuildScheduledEventModel eventModel = initialized.getEventModel();
                    existingGuildScheduledEventId = WidgetGuildScheduledEventSettings.this.getExistingGuildScheduledEventId();
                    WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData = existingGuildScheduledEventId != null ? new WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData(existingGuildScheduledEventId.longValue(), WidgetPreviewGuildScheduledEvent.Companion.Action.EDIT_EVENT) : null;
                    activityResultLauncher = WidgetGuildScheduledEventSettings.this.previewLauncher;
                    companion.launch(requireContext, eventModel, existingEventData, activityResultLauncher, true);
                }
            }
        });
        getBinding().q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GuildScheduledEventSettingsViewModel viewModel;
                WidgetGuildScheduledEventSettingsBinding binding;
                viewModel = WidgetGuildScheduledEventSettings.this.getViewModel();
                binding = WidgetGuildScheduledEventSettings.this.getBinding();
                MaterialCheckBox materialCheckBox = binding.p;
                m.checkNotNullExpressionValue(materialCheckBox, "binding.guildScheduledEv…ettingsVisibilityCheckbox");
                viewModel.toggleBroadcastToDirectoryChannel(!materialCheckBox.isChecked());
            }
        });
        getBinding().p.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GuildScheduledEventSettingsViewModel viewModel;
                WidgetGuildScheduledEventSettingsBinding binding;
                viewModel = WidgetGuildScheduledEventSettings.this.getViewModel();
                binding = WidgetGuildScheduledEventSettings.this.getBinding();
                MaterialCheckBox materialCheckBox = binding.p;
                m.checkNotNullExpressionValue(materialCheckBox, "binding.guildScheduledEv…ettingsVisibilityCheckbox");
                viewModel.toggleBroadcastToDirectoryChannel(materialCheckBox.isChecked());
            }
        });
    }
}
