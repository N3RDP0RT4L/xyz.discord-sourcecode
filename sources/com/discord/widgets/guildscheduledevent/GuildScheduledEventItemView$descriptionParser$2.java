package com.discord.widgets.guildscheduledevent;

import com.discord.databinding.GuildScheduledEventItemViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: GuildScheduledEventItemView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser;", "invoke", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventItemView$descriptionParser$2 extends o implements Function0<GuildScheduledEventDescriptionParser> {
    public final /* synthetic */ GuildScheduledEventItemView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventItemView$descriptionParser$2(GuildScheduledEventItemView guildScheduledEventItemView) {
        super(0);
        this.this$0 = guildScheduledEventItemView;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final GuildScheduledEventDescriptionParser invoke() {
        GuildScheduledEventItemViewBinding guildScheduledEventItemViewBinding;
        guildScheduledEventItemViewBinding = this.this$0.binding;
        LinkifiedTextView linkifiedTextView = guildScheduledEventItemViewBinding.f;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.guildScheduledEventListItemDescText");
        return new GuildScheduledEventDescriptionParser(linkifiedTextView);
    }
}
