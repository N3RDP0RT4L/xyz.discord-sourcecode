package com.discord.widgets.guildscheduledevent;

import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.stageinstance.StageInstance;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreInstantInvites;
import com.discord.widgets.guilds.invite.InviteGenerator;
import com.discord.widgets.guilds.invite.WidgetInviteModel;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventCreateSuccessViewModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function10;
/* compiled from: WidgetGuildScheduledEventCreateSuccessViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0001\u001a\u00020\u00002\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000f0\u00022\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0014\u001a\u00020\u00132\b\u0010\u0015\u001a\u0004\u0018\u00010\u0005H\n¢\u0006\u0004\b\u0017\u0010\u0018"}, d2 = {"Lcom/discord/models/domain/ModelInvite$Settings;", "settings", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "invitableChannels", "Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;", "inviteGenerationState", "Lcom/discord/models/user/MeUser;", "me", "", "dms", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/stageinstance/StageInstance;", "guildStageInstances", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "Lcom/discord/stores/StoreInstantInvites$InviteState;", "storeInvite", "defaultChannel", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventCreateSuccessViewModel$ViewState$Loaded;", "invoke", "(Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/Map;Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;Lcom/discord/models/user/MeUser;Ljava/util/List;Lcom/discord/models/guild/Guild;Ljava/util/Map;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/stores/StoreInstantInvites$InviteState;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventCreateSuccessViewModel$ViewState$Loaded;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventCreateSuccessViewModel$observeInvite$1 extends o implements Function10<ModelInvite.Settings, Map<Long, ? extends Channel>, InviteGenerator.InviteGenerationState, MeUser, List<? extends Channel>, Guild, Map<Long, ? extends StageInstance>, GuildScheduledEvent, StoreInstantInvites.InviteState, Channel, WidgetGuildScheduledEventCreateSuccessViewModel.ViewState.Loaded> {
    public static final WidgetGuildScheduledEventCreateSuccessViewModel$observeInvite$1 INSTANCE = new WidgetGuildScheduledEventCreateSuccessViewModel$observeInvite$1();

    public WidgetGuildScheduledEventCreateSuccessViewModel$observeInvite$1() {
        super(10);
    }

    @Override // kotlin.jvm.functions.Function10
    public /* bridge */ /* synthetic */ WidgetGuildScheduledEventCreateSuccessViewModel.ViewState.Loaded invoke(ModelInvite.Settings settings, Map<Long, ? extends Channel> map, InviteGenerator.InviteGenerationState inviteGenerationState, MeUser meUser, List<? extends Channel> list, Guild guild, Map<Long, ? extends StageInstance> map2, GuildScheduledEvent guildScheduledEvent, StoreInstantInvites.InviteState inviteState, Channel channel) {
        return invoke2(settings, (Map<Long, Channel>) map, inviteGenerationState, meUser, (List<Channel>) list, guild, (Map<Long, StageInstance>) map2, guildScheduledEvent, inviteState, channel);
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final WidgetGuildScheduledEventCreateSuccessViewModel.ViewState.Loaded invoke2(ModelInvite.Settings settings, Map<Long, Channel> map, InviteGenerator.InviteGenerationState inviteGenerationState, MeUser meUser, List<Channel> list, Guild guild, Map<Long, StageInstance> map2, GuildScheduledEvent guildScheduledEvent, StoreInstantInvites.InviteState inviteState, Channel channel) {
        Long l;
        StoreInstantInvites.InviteState inviteState2 = inviteState;
        m.checkNotNullParameter(settings, "settings");
        m.checkNotNullParameter(map, "invitableChannels");
        m.checkNotNullParameter(inviteGenerationState, "inviteGenerationState");
        m.checkNotNullParameter(meUser, "me");
        m.checkNotNullParameter(list, "dms");
        m.checkNotNullParameter(map2, "guildStageInstances");
        m.checkNotNullParameter(inviteState2, "storeInvite");
        WidgetInviteModel.Companion companion = WidgetInviteModel.Companion;
        if (guildScheduledEvent == null || (l = guildScheduledEvent.b()) == null) {
            l = channel != null ? Long.valueOf(channel.h()) : null;
        }
        if (!(inviteState2 instanceof StoreInstantInvites.InviteState.Resolved)) {
            inviteState2 = null;
        }
        StoreInstantInvites.InviteState.Resolved resolved = (StoreInstantInvites.InviteState.Resolved) inviteState2;
        WidgetInviteModel create = companion.create(settings, map, inviteGenerationState, l, meUser, list, guild, map2, guildScheduledEvent, resolved != null ? resolved.getInvite() : null);
        boolean z2 = true;
        if (create.getInvite() != null && (create.getInvite().isStatic() || create.isInviteFromStore())) {
            z2 = false;
        }
        return new WidgetGuildScheduledEventCreateSuccessViewModel.ViewState.Loaded(create, z2);
    }
}
