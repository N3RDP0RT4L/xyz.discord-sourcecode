package com.discord.widgets.guildscheduledevent;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.TimePicker;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventSettingsViewModel;
import kotlin.Metadata;
/* compiled from: WidgetGuildScheduledEventSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/view/View;", "kotlin.jvm.PlatformType", "it", "", "onClick", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventSettings$onViewBound$7 implements View.OnClickListener {
    public final /* synthetic */ WidgetGuildScheduledEventSettings this$0;

    public WidgetGuildScheduledEventSettings$onViewBound$7(WidgetGuildScheduledEventSettings widgetGuildScheduledEventSettings) {
        this.this$0 = widgetGuildScheduledEventSettings;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized;
        GuildScheduledEventModel eventModel;
        initialized = this.this$0.currentViewState;
        if (initialized != null && (eventModel = initialized.getEventModel()) != null) {
            GuildScheduledEventPickerTime second = GuildScheduledEventPickerDateTime.INSTANCE.generateDefaultEndDateTime(eventModel.getStartDate(), eventModel.getStartTime()).getSecond();
            Context requireContext = this.this$0.requireContext();
            TimePickerDialog.OnTimeSetListener widgetGuildScheduledEventSettings$onViewBound$7$$special$$inlined$let$lambda$1 = new TimePickerDialog.OnTimeSetListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$7$$special$$inlined$let$lambda$1
                @Override // android.app.TimePickerDialog.OnTimeSetListener
                public final void onTimeSet(TimePicker timePicker, int i, int i2) {
                    GuildScheduledEventSettingsViewModel viewModel;
                    viewModel = WidgetGuildScheduledEventSettings$onViewBound$7.this.this$0.getViewModel();
                    GuildScheduledEventSettingsViewModel.DateError endTime = viewModel.setEndTime(i, i2);
                    if (endTime != null) {
                        WidgetGuildScheduledEventSettings$onViewBound$7.this.this$0.showDateErrorToast(endTime);
                    }
                }
            };
            GuildScheduledEventPickerTime endTime = eventModel.getEndTime();
            int hourOfDay = endTime != null ? endTime.getHourOfDay() : second.getHourOfDay();
            GuildScheduledEventPickerTime endTime2 = eventModel.getEndTime();
            new TimePickerDialog(requireContext, widgetGuildScheduledEventSettings$onViewBound$7$$special$$inlined$let$lambda$1, hourOfDay, endTime2 != null ? endTime2.getMinute() : second.getMinute(), false).show();
        }
    }
}
