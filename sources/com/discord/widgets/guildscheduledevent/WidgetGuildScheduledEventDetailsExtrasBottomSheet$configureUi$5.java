package com.discord.widgets.guildscheduledevent;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.t.g0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildScheduledEventDetailsExtrasBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/view/View;", "kotlin.jvm.PlatformType", "it", "", "onClick", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5 implements View.OnClickListener {
    public final /* synthetic */ GuildScheduledEventDetailsViewModel.ViewState $viewState;
    public final /* synthetic */ WidgetGuildScheduledEventDetailsExtrasBottomSheet this$0;

    /* compiled from: WidgetGuildScheduledEventDetailsExtrasBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<View, Unit> {

        /* compiled from: WidgetGuildScheduledEventDetailsExtrasBottomSheet.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02511 extends o implements Function0<Unit> {
            public C02511() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5.this.this$0.dismiss();
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(View view) {
            invoke2(view);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(View view) {
            GuildScheduledEventDetailsViewModel viewModel;
            m.checkNotNullParameter(view, "it");
            viewModel = WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5.this.this$0.getViewModel();
            Context requireContext = WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5.this.this$0.requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            viewModel.onDeleteButtonClicked(requireContext, new C02511());
        }
    }

    public WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5(WidgetGuildScheduledEventDetailsExtrasBottomSheet widgetGuildScheduledEventDetailsExtrasBottomSheet, GuildScheduledEventDetailsViewModel.ViewState viewState) {
        this.this$0 = widgetGuildScheduledEventDetailsExtrasBottomSheet;
        this.$viewState = viewState;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        CharSequence b2;
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        FragmentManager childFragmentManager = this.this$0.getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        String string = this.this$0.requireContext().getString(R.string.delete_event);
        Context requireContext = this.this$0.requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        b2 = b.b(requireContext, R.string.delete_stage_event_confirmation_description, new Object[]{((GuildScheduledEventDetailsViewModel.ViewState.Initialized) this.$viewState).getGuildScheduledEvent().j()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        WidgetNoticeDialog.Companion.show$default(companion, childFragmentManager, string, b2, this.this$0.requireContext().getString(R.string.delete_event), this.this$0.requireContext().getString(R.string.back), g0.mapOf(d0.o.to(Integer.valueOf((int) R.id.OK_BUTTON), new AnonymousClass1())), null, null, null, null, null, null, 0, null, 16320, null);
    }
}
