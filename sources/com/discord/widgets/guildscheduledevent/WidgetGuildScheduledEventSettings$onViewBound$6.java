package com.discord.widgets.guildscheduledevent;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import com.discord.utilities.time.ClockFactory;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventSettingsViewModel;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetGuildScheduledEventSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/view/View;", "kotlin.jvm.PlatformType", "it", "", "onClick", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventSettings$onViewBound$6 implements View.OnClickListener {
    public final /* synthetic */ WidgetGuildScheduledEventSettings this$0;

    public WidgetGuildScheduledEventSettings$onViewBound$6(WidgetGuildScheduledEventSettings widgetGuildScheduledEventSettings) {
        this.this$0 = widgetGuildScheduledEventSettings;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GuildScheduledEventSettingsViewModel.ViewState.Initialized initialized;
        GuildScheduledEventModel eventModel;
        GuildScheduledEventPickerDate startDate;
        initialized = this.this$0.currentViewState;
        if (initialized != null && (eventModel = initialized.getEventModel()) != null && (startDate = eventModel.getStartDate()) != null) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.this$0.requireContext(), new DatePickerDialog.OnDateSetListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings$onViewBound$6$$special$$inlined$let$lambda$1
                @Override // android.app.DatePickerDialog.OnDateSetListener
                public final void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
                    GuildScheduledEventSettingsViewModel viewModel;
                    viewModel = WidgetGuildScheduledEventSettings$onViewBound$6.this.this$0.getViewModel();
                    GuildScheduledEventSettingsViewModel.DateError startDate2 = viewModel.setStartDate(i, i2, i3);
                    if (startDate2 != null) {
                        WidgetGuildScheduledEventSettings$onViewBound$6.this.this$0.showDateErrorToast(startDate2);
                    }
                }
            }, startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth());
            DatePicker datePicker = datePickerDialog.getDatePicker();
            m.checkNotNullExpressionValue(datePicker, "datePicker");
            datePicker.setMinDate(ClockFactory.get().currentTimeMillis());
            datePickerDialog.show();
        }
    }
}
