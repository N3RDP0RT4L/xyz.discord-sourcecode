package com.discord.widgets.guildscheduledevent;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityMetadata;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.nullserializable.NullSerializable;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.time.TimeUtils;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildScheduledEventModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0014\n\u0002\u0010\u0000\n\u0002\b\u001c\b\u0086\b\u0018\u00002\u00020\u0001B\u0093\u0001\u0012\n\u0010+\u001a\u00060\nj\u0002`\u000b\u0012\b\u0010,\u001a\u0004\u0018\u00010\u000e\u0012\u000e\u0010-\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u0011\u0012\u000e\u0010.\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u0014\u0012\u0006\u0010/\u001a\u00020\u0016\u0012\u0006\u00100\u001a\u00020\u0019\u0012\b\u00101\u001a\u0004\u0018\u00010\u0016\u0012\b\u00102\u001a\u0004\u0018\u00010\u0019\u0012\b\u00103\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u00104\u001a\u00020\u001f\u0012\b\u00105\u001a\u0004\u0018\u00010\"\u0012\b\u00106\u001a\u0004\u0018\u00010%\u0012\n\b\u0002\u00107\u001a\u0004\u0018\u00010(¢\u0006\u0004\bW\u0010XJ\u000f\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tJ\u0014\u0010\f\u001a\u00060\nj\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0018\u0010\u0012\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0018\u0010\u0015\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0013J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u0016HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0018J\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u0019HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001bJ\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u0010J\u0010\u0010 \u001a\u00020\u001fHÆ\u0003¢\u0006\u0004\b \u0010!J\u0012\u0010#\u001a\u0004\u0018\u00010\"HÆ\u0003¢\u0006\u0004\b#\u0010$J\u0012\u0010&\u001a\u0004\u0018\u00010%HÆ\u0003¢\u0006\u0004\b&\u0010'J\u0012\u0010)\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b)\u0010*J´\u0001\u00108\u001a\u00020\u00002\f\b\u0002\u0010+\u001a\u00060\nj\u0002`\u000b2\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u000e2\u0010\b\u0002\u0010-\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u00112\u0010\b\u0002\u0010.\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u00142\b\b\u0002\u0010/\u001a\u00020\u00162\b\b\u0002\u00100\u001a\u00020\u00192\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u00192\n\b\u0002\u00103\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u00104\u001a\u00020\u001f2\n\b\u0002\u00105\u001a\u0004\u0018\u00010\"2\n\b\u0002\u00106\u001a\u0004\u0018\u00010%2\n\b\u0002\u00107\u001a\u0004\u0018\u00010(HÆ\u0001¢\u0006\u0004\b8\u00109J\u0010\u0010:\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b:\u0010\u0010J\u0010\u0010;\u001a\u00020%HÖ\u0001¢\u0006\u0004\b;\u0010<J\u001a\u0010?\u001a\u00020(2\b\u0010>\u001a\u0004\u0018\u00010=HÖ\u0003¢\u0006\u0004\b?\u0010@R\u001b\u00103\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010A\u001a\u0004\bB\u0010\u0010R\u0019\u0010/\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010C\u001a\u0004\bD\u0010\u0018R\u001b\u00105\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010E\u001a\u0004\bF\u0010$R\u001d\u0010+\u001a\u00060\nj\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010G\u001a\u0004\bH\u0010\rR\u0019\u00104\u001a\u00020\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010I\u001a\u0004\bJ\u0010!R\u0019\u00100\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010K\u001a\u0004\bL\u0010\u001bR\u001b\u00106\u001a\u0004\u0018\u00010%8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010M\u001a\u0004\bN\u0010'R\u001b\u00107\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010O\u001a\u0004\bP\u0010*R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010A\u001a\u0004\bQ\u0010\u0010R!\u0010-\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010R\u001a\u0004\bS\u0010\u0013R\u001b\u00102\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010K\u001a\u0004\bT\u0010\u001bR!\u0010.\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u00148\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010R\u001a\u0004\bU\u0010\u0013R\u001b\u00101\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010C\u001a\u0004\bV\u0010\u0018¨\u0006Y"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "Ljava/io/Serializable;", "Lcom/discord/restapi/RestAPIParams$CreateGuildScheduledEventBody;", "toCreateRequestBody", "()Lcom/discord/restapi/RestAPIParams$CreateGuildScheduledEventBody;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "currentEvent", "Lcom/discord/restapi/RestAPIParams$UpdateGuildScheduledEventBody;", "toUpdateRequestBody", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/restapi/RestAPIParams$UpdateGuildScheduledEventBody;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "Lcom/discord/primitives/ChannelId;", "component3", "()Ljava/lang/Long;", "Lcom/discord/primitives/UserId;", "component4", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;", "component5", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;", "component6", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;", "component7", "component8", "component9", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "component10", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "component11", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "", "component12", "()Ljava/lang/Integer;", "", "component13", "()Ljava/lang/Boolean;", "guildId", ModelAuditLogEntry.CHANGE_KEY_NAME, "channelId", "creatorId", "startDate", "startTime", "endDate", "endTime", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "entityType", "entityMetadata", "userCount", "broadcastToDirectoryChannels", "copy", "(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;Ljava/lang/String;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;Ljava/lang/Integer;Ljava/lang/Boolean;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "toString", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getDescription", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;", "getStartDate", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "getEntityMetadata", "J", "getGuildId", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "getEntityType", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;", "getStartTime", "Ljava/lang/Integer;", "getUserCount", "Ljava/lang/Boolean;", "getBroadcastToDirectoryChannels", "getName", "Ljava/lang/Long;", "getChannelId", "getEndTime", "getCreatorId", "getEndDate", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;Ljava/lang/String;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;Ljava/lang/Integer;Ljava/lang/Boolean;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventModel implements Serializable {
    private final Boolean broadcastToDirectoryChannels;
    private final Long channelId;
    private final Long creatorId;
    private final String description;
    private final GuildScheduledEventPickerDate endDate;
    private final GuildScheduledEventPickerTime endTime;
    private final GuildScheduledEventEntityMetadata entityMetadata;
    private final GuildScheduledEventEntityType entityType;
    private final long guildId;
    private final String name;
    private final GuildScheduledEventPickerDate startDate;
    private final GuildScheduledEventPickerTime startTime;
    private final Integer userCount;

    public GuildScheduledEventModel(long j, String str, Long l, Long l2, GuildScheduledEventPickerDate guildScheduledEventPickerDate, GuildScheduledEventPickerTime guildScheduledEventPickerTime, GuildScheduledEventPickerDate guildScheduledEventPickerDate2, GuildScheduledEventPickerTime guildScheduledEventPickerTime2, String str2, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata, Integer num, Boolean bool) {
        m.checkNotNullParameter(guildScheduledEventPickerDate, "startDate");
        m.checkNotNullParameter(guildScheduledEventPickerTime, "startTime");
        m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
        this.guildId = j;
        this.name = str;
        this.channelId = l;
        this.creatorId = l2;
        this.startDate = guildScheduledEventPickerDate;
        this.startTime = guildScheduledEventPickerTime;
        this.endDate = guildScheduledEventPickerDate2;
        this.endTime = guildScheduledEventPickerTime2;
        this.description = str2;
        this.entityType = guildScheduledEventEntityType;
        this.entityMetadata = guildScheduledEventEntityMetadata;
        this.userCount = num;
        this.broadcastToDirectoryChannels = bool;
    }

    public final long component1() {
        return this.guildId;
    }

    public final GuildScheduledEventEntityType component10() {
        return this.entityType;
    }

    public final GuildScheduledEventEntityMetadata component11() {
        return this.entityMetadata;
    }

    public final Integer component12() {
        return this.userCount;
    }

    public final Boolean component13() {
        return this.broadcastToDirectoryChannels;
    }

    public final String component2() {
        return this.name;
    }

    public final Long component3() {
        return this.channelId;
    }

    public final Long component4() {
        return this.creatorId;
    }

    public final GuildScheduledEventPickerDate component5() {
        return this.startDate;
    }

    public final GuildScheduledEventPickerTime component6() {
        return this.startTime;
    }

    public final GuildScheduledEventPickerDate component7() {
        return this.endDate;
    }

    public final GuildScheduledEventPickerTime component8() {
        return this.endTime;
    }

    public final String component9() {
        return this.description;
    }

    public final GuildScheduledEventModel copy(long j, String str, Long l, Long l2, GuildScheduledEventPickerDate guildScheduledEventPickerDate, GuildScheduledEventPickerTime guildScheduledEventPickerTime, GuildScheduledEventPickerDate guildScheduledEventPickerDate2, GuildScheduledEventPickerTime guildScheduledEventPickerTime2, String str2, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata, Integer num, Boolean bool) {
        m.checkNotNullParameter(guildScheduledEventPickerDate, "startDate");
        m.checkNotNullParameter(guildScheduledEventPickerTime, "startTime");
        m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
        return new GuildScheduledEventModel(j, str, l, l2, guildScheduledEventPickerDate, guildScheduledEventPickerTime, guildScheduledEventPickerDate2, guildScheduledEventPickerTime2, str2, guildScheduledEventEntityType, guildScheduledEventEntityMetadata, num, bool);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildScheduledEventModel)) {
            return false;
        }
        GuildScheduledEventModel guildScheduledEventModel = (GuildScheduledEventModel) obj;
        return this.guildId == guildScheduledEventModel.guildId && m.areEqual(this.name, guildScheduledEventModel.name) && m.areEqual(this.channelId, guildScheduledEventModel.channelId) && m.areEqual(this.creatorId, guildScheduledEventModel.creatorId) && m.areEqual(this.startDate, guildScheduledEventModel.startDate) && m.areEqual(this.startTime, guildScheduledEventModel.startTime) && m.areEqual(this.endDate, guildScheduledEventModel.endDate) && m.areEqual(this.endTime, guildScheduledEventModel.endTime) && m.areEqual(this.description, guildScheduledEventModel.description) && m.areEqual(this.entityType, guildScheduledEventModel.entityType) && m.areEqual(this.entityMetadata, guildScheduledEventModel.entityMetadata) && m.areEqual(this.userCount, guildScheduledEventModel.userCount) && m.areEqual(this.broadcastToDirectoryChannels, guildScheduledEventModel.broadcastToDirectoryChannels);
    }

    public final Boolean getBroadcastToDirectoryChannels() {
        return this.broadcastToDirectoryChannels;
    }

    public final Long getChannelId() {
        return this.channelId;
    }

    public final Long getCreatorId() {
        return this.creatorId;
    }

    public final String getDescription() {
        return this.description;
    }

    public final GuildScheduledEventPickerDate getEndDate() {
        return this.endDate;
    }

    public final GuildScheduledEventPickerTime getEndTime() {
        return this.endTime;
    }

    public final GuildScheduledEventEntityMetadata getEntityMetadata() {
        return this.entityMetadata;
    }

    public final GuildScheduledEventEntityType getEntityType() {
        return this.entityType;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final String getName() {
        return this.name;
    }

    public final GuildScheduledEventPickerDate getStartDate() {
        return this.startDate;
    }

    public final GuildScheduledEventPickerTime getStartTime() {
        return this.startTime;
    }

    public final Integer getUserCount() {
        return this.userCount;
    }

    public int hashCode() {
        int a = b.a(this.guildId) * 31;
        String str = this.name;
        int i = 0;
        int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
        Long l = this.channelId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.creatorId;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        GuildScheduledEventPickerDate guildScheduledEventPickerDate = this.startDate;
        int hashCode4 = (hashCode3 + (guildScheduledEventPickerDate != null ? guildScheduledEventPickerDate.hashCode() : 0)) * 31;
        GuildScheduledEventPickerTime guildScheduledEventPickerTime = this.startTime;
        int hashCode5 = (hashCode4 + (guildScheduledEventPickerTime != null ? guildScheduledEventPickerTime.hashCode() : 0)) * 31;
        GuildScheduledEventPickerDate guildScheduledEventPickerDate2 = this.endDate;
        int hashCode6 = (hashCode5 + (guildScheduledEventPickerDate2 != null ? guildScheduledEventPickerDate2.hashCode() : 0)) * 31;
        GuildScheduledEventPickerTime guildScheduledEventPickerTime2 = this.endTime;
        int hashCode7 = (hashCode6 + (guildScheduledEventPickerTime2 != null ? guildScheduledEventPickerTime2.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode8 = (hashCode7 + (str2 != null ? str2.hashCode() : 0)) * 31;
        GuildScheduledEventEntityType guildScheduledEventEntityType = this.entityType;
        int hashCode9 = (hashCode8 + (guildScheduledEventEntityType != null ? guildScheduledEventEntityType.hashCode() : 0)) * 31;
        GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata = this.entityMetadata;
        int hashCode10 = (hashCode9 + (guildScheduledEventEntityMetadata != null ? guildScheduledEventEntityMetadata.hashCode() : 0)) * 31;
        Integer num = this.userCount;
        int hashCode11 = (hashCode10 + (num != null ? num.hashCode() : 0)) * 31;
        Boolean bool = this.broadcastToDirectoryChannels;
        if (bool != null) {
            i = bool.hashCode();
        }
        return hashCode11 + i;
    }

    public final RestAPIParams.CreateGuildScheduledEventBody toCreateRequestBody() {
        GuildScheduledEventPickerTime guildScheduledEventPickerTime;
        String str = this.name;
        String str2 = null;
        if (str == null) {
            return null;
        }
        GuildScheduledEventPickerDateTime guildScheduledEventPickerDateTime = GuildScheduledEventPickerDateTime.INSTANCE;
        String utcDateString = guildScheduledEventPickerDateTime.toUtcDateString(this.startDate, this.startTime);
        GuildScheduledEventPickerDate guildScheduledEventPickerDate = this.endDate;
        if (!(guildScheduledEventPickerDate == null || (guildScheduledEventPickerTime = this.endTime) == null)) {
            str2 = guildScheduledEventPickerDateTime.toUtcDateString(guildScheduledEventPickerDate, guildScheduledEventPickerTime);
        }
        return new RestAPIParams.CreateGuildScheduledEventBody(str, this.description, StageInstancePrivacyLevel.GUILD_ONLY, utcDateString, str2, this.channelId, this.entityType, this.entityMetadata, this.broadcastToDirectoryChannels);
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEventModel(guildId=");
        R.append(this.guildId);
        R.append(", name=");
        R.append(this.name);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", creatorId=");
        R.append(this.creatorId);
        R.append(", startDate=");
        R.append(this.startDate);
        R.append(", startTime=");
        R.append(this.startTime);
        R.append(", endDate=");
        R.append(this.endDate);
        R.append(", endTime=");
        R.append(this.endTime);
        R.append(", description=");
        R.append(this.description);
        R.append(", entityType=");
        R.append(this.entityType);
        R.append(", entityMetadata=");
        R.append(this.entityMetadata);
        R.append(", userCount=");
        R.append(this.userCount);
        R.append(", broadcastToDirectoryChannels=");
        return a.C(R, this.broadcastToDirectoryChannels, ")");
    }

    public final RestAPIParams.UpdateGuildScheduledEventBody toUpdateRequestBody(GuildScheduledEvent guildScheduledEvent) {
        NullSerializable nullSerializable;
        GuildScheduledEventPickerTime guildScheduledEventPickerTime;
        m.checkNotNullParameter(guildScheduledEvent, "currentEvent");
        String str = this.name;
        String str2 = null;
        if (str == null) {
            return null;
        }
        GuildScheduledEventPickerDateTime guildScheduledEventPickerDateTime = GuildScheduledEventPickerDateTime.INSTANCE;
        long millis = guildScheduledEventPickerDateTime.toMillis(this.startDate, this.startTime);
        GuildScheduledEventPickerDate guildScheduledEventPickerDate = this.endDate;
        Long valueOf = (guildScheduledEventPickerDate == null || (guildScheduledEventPickerTime = this.endTime) == null) ? null : Long.valueOf(guildScheduledEventPickerDateTime.toMillis(guildScheduledEventPickerDate, guildScheduledEventPickerTime));
        Long l = this.channelId;
        NullSerializable bVar = l != null ? new NullSerializable.b(l) : new NullSerializable.a(null, 1);
        GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata = this.entityMetadata;
        if (guildScheduledEventEntityMetadata == null || guildScheduledEventEntityMetadata.b()) {
            nullSerializable = new NullSerializable.a(null, 1);
        } else {
            nullSerializable = new NullSerializable.b(this.entityMetadata);
        }
        NullSerializable nullSerializable2 = nullSerializable;
        String str3 = this.description;
        StageInstancePrivacyLevel stageInstancePrivacyLevel = StageInstancePrivacyLevel.GUILD_ONLY;
        String uTCDateTime$default = guildScheduledEvent.l().g() == millis ? null : TimeUtils.toUTCDateTime$default(Long.valueOf(millis), null, 2, null);
        UtcDateTime k = guildScheduledEvent.k();
        if (!m.areEqual(k != null ? Long.valueOf(k.g()) : null, valueOf)) {
            str2 = TimeUtils.toUTCDateTime$default(valueOf, null, 2, null);
        }
        return new RestAPIParams.UpdateGuildScheduledEventBody(str, str3, stageInstancePrivacyLevel, uTCDateTime$default, str2, bVar, this.entityType, nullSerializable2, null, this.broadcastToDirectoryChannels, 256, null);
    }

    public /* synthetic */ GuildScheduledEventModel(long j, String str, Long l, Long l2, GuildScheduledEventPickerDate guildScheduledEventPickerDate, GuildScheduledEventPickerTime guildScheduledEventPickerTime, GuildScheduledEventPickerDate guildScheduledEventPickerDate2, GuildScheduledEventPickerTime guildScheduledEventPickerTime2, String str2, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata, Integer num, Boolean bool, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, str, l, l2, guildScheduledEventPickerDate, guildScheduledEventPickerTime, guildScheduledEventPickerDate2, guildScheduledEventPickerTime2, str2, guildScheduledEventEntityType, guildScheduledEventEntityMetadata, num, (i & 4096) != 0 ? null : bool);
    }
}
