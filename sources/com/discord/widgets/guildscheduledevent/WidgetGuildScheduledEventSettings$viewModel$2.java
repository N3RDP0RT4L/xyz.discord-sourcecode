package com.discord.widgets.guildscheduledevent;

import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.app.AppViewModel;
import com.discord.utilities.intent.IntentUtilsKt;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventSettingsViewModel;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildScheduledEventSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventSettings$viewModel$2 extends o implements Function0<AppViewModel<GuildScheduledEventSettingsViewModel.ViewState>> {
    public final /* synthetic */ WidgetGuildScheduledEventSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildScheduledEventSettings$viewModel$2(WidgetGuildScheduledEventSettings widgetGuildScheduledEventSettings) {
        super(0);
        this.this$0 = widgetGuildScheduledEventSettings;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<GuildScheduledEventSettingsViewModel.ViewState> invoke() {
        long guildId;
        Long existingGuildScheduledEventId;
        guildId = this.this$0.getGuildId();
        existingGuildScheduledEventId = this.this$0.getExistingGuildScheduledEventId();
        Serializable serializableExtra = this.this$0.getMostRecentIntent().getSerializableExtra("EXTRA_ENTITY_TYPE");
        Objects.requireNonNull(serializableExtra, "null cannot be cast to non-null type com.discord.api.guildscheduledevent.GuildScheduledEventEntityType");
        return new GuildScheduledEventSettingsViewModel(guildId, existingGuildScheduledEventId, (GuildScheduledEventEntityType) serializableExtra, IntentUtilsKt.getLongExtraOrNull(this.this$0.getMostRecentIntent(), "com.discord.intent.extra.EXTRA_CHANNEL_ID"), this.this$0.getMostRecentIntent().getStringExtra("EXTRA_EXTERNAL_LOCATION"), null, 32, null);
    }
}
