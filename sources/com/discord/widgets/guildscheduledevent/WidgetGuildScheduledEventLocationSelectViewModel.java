package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetGuildScheduledEventLocationSelectViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 12\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003123Bk\u0012\n\u0010&\u001a\u00060\u0010j\u0002`%\u0012\u000e\u0010$\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u0011\u0012\u000e\u0010\"\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`!\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019\u0012\b\b\u0002\u0010(\u001a\u00020'\u0012\b\b\u0002\u0010*\u001a\u00020)\u0012\b\b\u0002\u0010,\u001a\u00020+\u0012\u000e\b\u0002\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00030-¢\u0006\u0004\b/\u00100J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0019\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\n¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0013\u001a\u00020\u00052\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u0011¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR$\u0010\u001c\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010\u0007R\u001e\u0010\"\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u001e\u0010$\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010#¨\u00064"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState;", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;)V", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "getEntityTypeForChannel", "(Lcom/discord/api/channel/Channel;)Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "locationOption", "selectLocationOption", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "selectChannel", "(J)V", "", "newExternalLocation", "setExternalLocation", "(Ljava/lang/String;)V", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "currentStoreState", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;", "getCurrentStoreState", "()Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;", "setCurrentStoreState", "Lcom/discord/primitives/GuildScheduledEventId;", "existingGuildScheduledEventId", "Ljava/lang/Long;", "initialChannelId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(JLjava/lang/Long;Ljava/lang/Long;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuildScheduledEvents;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventLocationSelectViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final StoreChannels channelsStore;
    private StoreState currentStoreState;
    private final Long existingGuildScheduledEventId;
    private final Long initialChannelId;

    /* compiled from: WidgetGuildScheduledEventLocationSelectViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            WidgetGuildScheduledEventLocationSelectViewModel widgetGuildScheduledEventLocationSelectViewModel = WidgetGuildScheduledEventLocationSelectViewModel.this;
            m.checkNotNullExpressionValue(storeState, "it");
            widgetGuildScheduledEventLocationSelectViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetGuildScheduledEventLocationSelectViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014JQ\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/GuildScheduledEventId;", "existingGuildScheduledEventId", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lrx/Observable;", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;", "observeStores", "(JLjava/lang/Long;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuildScheduledEvents;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(long j, Long l, StoreGuilds storeGuilds, StoreChannels storeChannels, StorePermissions storePermissions, StoreGuildScheduledEvents storeGuildScheduledEvents) {
            return ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{storeChannels, storePermissions}, false, null, null, new WidgetGuildScheduledEventLocationSelectViewModel$Companion$observeStores$1(storeGuilds, j, storeChannels, storePermissions, l, storeGuildScheduledEvents), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildScheduledEventLocationSelectViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\u0016\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u001a\u0010\u0014\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\u0006j\u0002`\u000b0\u0005\u0012\u000e\u0010\u0015\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u000b\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b,\u0010-J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ$\u0010\f\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\u0006j\u0002`\u000b0\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0018\u0010\r\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011Jr\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00022\u0018\b\u0002\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00052\u001c\b\u0002\u0010\u0014\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\u0006j\u0002`\u000b0\u00052\u0010\b\u0002\u0010\u0015\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u000b2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u000fHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010!\u001a\u00020 2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b$\u0010\u0011R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010%\u001a\u0004\b&\u0010\u0004R)\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\nR-\u0010\u0014\u001a\u0016\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\u0006j\u0002`\u000b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010'\u001a\u0004\b)\u0010\nR!\u0010\u0015\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010*\u001a\u0004\b+\u0010\u000e¨\u0006."}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component2", "()Ljava/util/Map;", "Lcom/discord/api/permission/PermissionBit;", "component3", "component4", "()Ljava/lang/Long;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component5", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guild", "channels", "channelPermissions", "guildPermissions", "existingGuildScheduledEvent", "copy", "(Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getExistingGuildScheduledEvent", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/util/Map;", "getChannels", "getChannelPermissions", "Ljava/lang/Long;", "getGuildPermissions", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, Long> channelPermissions;
        private final Map<Long, Channel> channels;
        private final GuildScheduledEvent existingGuildScheduledEvent;
        private final Guild guild;
        private final Long guildPermissions;

        public StoreState(Guild guild, Map<Long, Channel> map, Map<Long, Long> map2, Long l, GuildScheduledEvent guildScheduledEvent) {
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(map2, "channelPermissions");
            this.guild = guild;
            this.channels = map;
            this.channelPermissions = map2;
            this.guildPermissions = l;
            this.existingGuildScheduledEvent = guildScheduledEvent;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Guild guild, Map map, Map map2, Long l, GuildScheduledEvent guildScheduledEvent, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = storeState.guild;
            }
            Map<Long, Channel> map3 = map;
            if ((i & 2) != 0) {
                map3 = storeState.channels;
            }
            Map map4 = map3;
            Map<Long, Long> map5 = map2;
            if ((i & 4) != 0) {
                map5 = storeState.channelPermissions;
            }
            Map map6 = map5;
            if ((i & 8) != 0) {
                l = storeState.guildPermissions;
            }
            Long l2 = l;
            if ((i & 16) != 0) {
                guildScheduledEvent = storeState.existingGuildScheduledEvent;
            }
            return storeState.copy(guild, map4, map6, l2, guildScheduledEvent);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final Map<Long, Channel> component2() {
            return this.channels;
        }

        public final Map<Long, Long> component3() {
            return this.channelPermissions;
        }

        public final Long component4() {
            return this.guildPermissions;
        }

        public final GuildScheduledEvent component5() {
            return this.existingGuildScheduledEvent;
        }

        public final StoreState copy(Guild guild, Map<Long, Channel> map, Map<Long, Long> map2, Long l, GuildScheduledEvent guildScheduledEvent) {
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(map2, "channelPermissions");
            return new StoreState(guild, map, map2, l, guildScheduledEvent);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guild, storeState.guild) && m.areEqual(this.channels, storeState.channels) && m.areEqual(this.channelPermissions, storeState.channelPermissions) && m.areEqual(this.guildPermissions, storeState.guildPermissions) && m.areEqual(this.existingGuildScheduledEvent, storeState.existingGuildScheduledEvent);
        }

        public final Map<Long, Long> getChannelPermissions() {
            return this.channelPermissions;
        }

        public final Map<Long, Channel> getChannels() {
            return this.channels;
        }

        public final GuildScheduledEvent getExistingGuildScheduledEvent() {
            return this.existingGuildScheduledEvent;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Long getGuildPermissions() {
            return this.guildPermissions;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Map<Long, Channel> map = this.channels;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Long> map2 = this.channelPermissions;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Long l = this.guildPermissions;
            int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
            GuildScheduledEvent guildScheduledEvent = this.existingGuildScheduledEvent;
            if (guildScheduledEvent != null) {
                i = guildScheduledEvent.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guild=");
            R.append(this.guild);
            R.append(", channels=");
            R.append(this.channels);
            R.append(", channelPermissions=");
            R.append(this.channelPermissions);
            R.append(", guildPermissions=");
            R.append(this.guildPermissions);
            R.append(", existingGuildScheduledEvent=");
            R.append(this.existingGuildScheduledEvent);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetGuildScheduledEventLocationSelectViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState$Valid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetGuildScheduledEventLocationSelectViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetGuildScheduledEventLocationSelectViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001Bu\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u001b\u001a\u00020\b\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u00050\u000b\u0012\u0010\u0010\u001d\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u0010\u0012\u0010\u0010\u001e\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u0010\u0012\u0006\u0010\u001f\u001a\u00020\u0014\u0012\u0006\u0010 \u001a\u00020\u0014\u0012\u0006\u0010!\u001a\u00020\u0014¢\u0006\u0004\b;\u0010<J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u00050\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0011\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0013\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u0010HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0012J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0016J\u0090\u0001\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u001b\u001a\u00020\b2\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u00050\u000b2\u0012\b\u0002\u0010\u001d\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u00102\u0012\b\u0002\u0010\u001e\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u00102\b\b\u0002\u0010\u001f\u001a\u00020\u00142\b\b\u0002\u0010 \u001a\u00020\u00142\b\b\u0002\u0010!\u001a\u00020\u0014HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010$\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b$\u0010\nJ\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u001a\u0010*\u001a\u00020\u00142\b\u0010)\u001a\u0004\u0018\u00010(HÖ\u0003¢\u0006\u0004\b*\u0010+R\u0019\u0010!\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010,\u001a\u0004\b-\u0010\u0016R#\u0010\u001e\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010.\u001a\u0004\b/\u0010\u0012R\u0019\u0010\u001f\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010,\u001a\u0004\b0\u0010\u0016R)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u00050\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00101\u001a\u0004\b2\u0010\u000fR\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00103\u001a\u0004\b4\u0010\u0004R\u0019\u0010 \u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b \u0010,\u001a\u0004\b5\u0010\u0016R#\u0010\u001d\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010.\u001a\u0004\b6\u0010\u0012R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00107\u001a\u0004\b8\u0010\u0007R\u0019\u0010\u001b\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00109\u001a\u0004\b:\u0010\n¨\u0006="}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState$Valid;", "Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "component1", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "component3", "()Ljava/lang/String;", "", "", "Lcom/discord/primitives/ChannelId;", "component4", "()Ljava/util/Map;", "", "component5", "()Ljava/util/Set;", "component6", "", "component7", "()Z", "component8", "component9", "selectedLocationOption", "selectedChannel", "externalLocation", "availableChannels", "availableVoiceChannelIds", "availableStageChannelIds", "canCreateExternalEvent", "showStageOptionIfUnavailable", "canChangeChannel", "copy", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;ZZZ)Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventLocationSelectViewModel$ViewState$Valid;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanChangeChannel", "Ljava/util/Set;", "getAvailableStageChannelIds", "getCanCreateExternalEvent", "Ljava/util/Map;", "getAvailableChannels", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "getSelectedLocationOption", "getShowStageOptionIfUnavailable", "getAvailableVoiceChannelIds", "Lcom/discord/api/channel/Channel;", "getSelectedChannel", "Ljava/lang/String;", "getExternalLocation", HookHelper.constructorName, "(Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends ViewState {
            private final Map<Long, Channel> availableChannels;
            private final Set<Long> availableStageChannelIds;
            private final Set<Long> availableVoiceChannelIds;
            private final boolean canChangeChannel;
            private final boolean canCreateExternalEvent;
            private final String externalLocation;
            private final Channel selectedChannel;
            private final GuildScheduledEventEntityType selectedLocationOption;
            private final boolean showStageOptionIfUnavailable;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Valid(GuildScheduledEventEntityType guildScheduledEventEntityType, Channel channel, String str, Map<Long, Channel> map, Set<Long> set, Set<Long> set2, boolean z2, boolean z3, boolean z4) {
                super(null);
                m.checkNotNullParameter(guildScheduledEventEntityType, "selectedLocationOption");
                m.checkNotNullParameter(str, "externalLocation");
                m.checkNotNullParameter(map, "availableChannels");
                m.checkNotNullParameter(set, "availableVoiceChannelIds");
                m.checkNotNullParameter(set2, "availableStageChannelIds");
                this.selectedLocationOption = guildScheduledEventEntityType;
                this.selectedChannel = channel;
                this.externalLocation = str;
                this.availableChannels = map;
                this.availableVoiceChannelIds = set;
                this.availableStageChannelIds = set2;
                this.canCreateExternalEvent = z2;
                this.showStageOptionIfUnavailable = z3;
                this.canChangeChannel = z4;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Valid copy$default(Valid valid, GuildScheduledEventEntityType guildScheduledEventEntityType, Channel channel, String str, Map map, Set set, Set set2, boolean z2, boolean z3, boolean z4, int i, Object obj) {
                return valid.copy((i & 1) != 0 ? valid.selectedLocationOption : guildScheduledEventEntityType, (i & 2) != 0 ? valid.selectedChannel : channel, (i & 4) != 0 ? valid.externalLocation : str, (i & 8) != 0 ? valid.availableChannels : map, (i & 16) != 0 ? valid.availableVoiceChannelIds : set, (i & 32) != 0 ? valid.availableStageChannelIds : set2, (i & 64) != 0 ? valid.canCreateExternalEvent : z2, (i & 128) != 0 ? valid.showStageOptionIfUnavailable : z3, (i & 256) != 0 ? valid.canChangeChannel : z4);
            }

            public final GuildScheduledEventEntityType component1() {
                return this.selectedLocationOption;
            }

            public final Channel component2() {
                return this.selectedChannel;
            }

            public final String component3() {
                return this.externalLocation;
            }

            public final Map<Long, Channel> component4() {
                return this.availableChannels;
            }

            public final Set<Long> component5() {
                return this.availableVoiceChannelIds;
            }

            public final Set<Long> component6() {
                return this.availableStageChannelIds;
            }

            public final boolean component7() {
                return this.canCreateExternalEvent;
            }

            public final boolean component8() {
                return this.showStageOptionIfUnavailable;
            }

            public final boolean component9() {
                return this.canChangeChannel;
            }

            public final Valid copy(GuildScheduledEventEntityType guildScheduledEventEntityType, Channel channel, String str, Map<Long, Channel> map, Set<Long> set, Set<Long> set2, boolean z2, boolean z3, boolean z4) {
                m.checkNotNullParameter(guildScheduledEventEntityType, "selectedLocationOption");
                m.checkNotNullParameter(str, "externalLocation");
                m.checkNotNullParameter(map, "availableChannels");
                m.checkNotNullParameter(set, "availableVoiceChannelIds");
                m.checkNotNullParameter(set2, "availableStageChannelIds");
                return new Valid(guildScheduledEventEntityType, channel, str, map, set, set2, z2, z3, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.selectedLocationOption, valid.selectedLocationOption) && m.areEqual(this.selectedChannel, valid.selectedChannel) && m.areEqual(this.externalLocation, valid.externalLocation) && m.areEqual(this.availableChannels, valid.availableChannels) && m.areEqual(this.availableVoiceChannelIds, valid.availableVoiceChannelIds) && m.areEqual(this.availableStageChannelIds, valid.availableStageChannelIds) && this.canCreateExternalEvent == valid.canCreateExternalEvent && this.showStageOptionIfUnavailable == valid.showStageOptionIfUnavailable && this.canChangeChannel == valid.canChangeChannel;
            }

            public final Map<Long, Channel> getAvailableChannels() {
                return this.availableChannels;
            }

            public final Set<Long> getAvailableStageChannelIds() {
                return this.availableStageChannelIds;
            }

            public final Set<Long> getAvailableVoiceChannelIds() {
                return this.availableVoiceChannelIds;
            }

            public final boolean getCanChangeChannel() {
                return this.canChangeChannel;
            }

            public final boolean getCanCreateExternalEvent() {
                return this.canCreateExternalEvent;
            }

            public final String getExternalLocation() {
                return this.externalLocation;
            }

            public final Channel getSelectedChannel() {
                return this.selectedChannel;
            }

            public final GuildScheduledEventEntityType getSelectedLocationOption() {
                return this.selectedLocationOption;
            }

            public final boolean getShowStageOptionIfUnavailable() {
                return this.showStageOptionIfUnavailable;
            }

            public int hashCode() {
                GuildScheduledEventEntityType guildScheduledEventEntityType = this.selectedLocationOption;
                int i = 0;
                int hashCode = (guildScheduledEventEntityType != null ? guildScheduledEventEntityType.hashCode() : 0) * 31;
                Channel channel = this.selectedChannel;
                int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
                String str = this.externalLocation;
                int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
                Map<Long, Channel> map = this.availableChannels;
                int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
                Set<Long> set = this.availableVoiceChannelIds;
                int hashCode5 = (hashCode4 + (set != null ? set.hashCode() : 0)) * 31;
                Set<Long> set2 = this.availableStageChannelIds;
                if (set2 != null) {
                    i = set2.hashCode();
                }
                int i2 = (hashCode5 + i) * 31;
                boolean z2 = this.canCreateExternalEvent;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.showStageOptionIfUnavailable;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.canChangeChannel;
                if (!z4) {
                    i3 = z4 ? 1 : 0;
                }
                return i9 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(selectedLocationOption=");
                R.append(this.selectedLocationOption);
                R.append(", selectedChannel=");
                R.append(this.selectedChannel);
                R.append(", externalLocation=");
                R.append(this.externalLocation);
                R.append(", availableChannels=");
                R.append(this.availableChannels);
                R.append(", availableVoiceChannelIds=");
                R.append(this.availableVoiceChannelIds);
                R.append(", availableStageChannelIds=");
                R.append(this.availableStageChannelIds);
                R.append(", canCreateExternalEvent=");
                R.append(this.canCreateExternalEvent);
                R.append(", showStageOptionIfUnavailable=");
                R.append(this.showStageOptionIfUnavailable);
                R.append(", canChangeChannel=");
                return a.M(R, this.canChangeChannel, ")");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetGuildScheduledEventLocationSelectViewModel(long r14, java.lang.Long r16, java.lang.Long r17, com.discord.stores.StoreChannels r18, com.discord.stores.StoreGuilds r19, com.discord.stores.StorePermissions r20, com.discord.stores.StoreGuildScheduledEvents r21, rx.Observable r22, int r23, kotlin.jvm.internal.DefaultConstructorMarker r24) {
        /*
            r13 = this;
            r0 = r23
            r1 = r0 & 8
            if (r1 == 0) goto Ld
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r1 = r1.getChannels()
            goto Lf
        Ld:
            r1 = r18
        Lf:
            r2 = r0 & 16
            if (r2 == 0) goto L1b
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r2 = r2.getGuilds()
            r10 = r2
            goto L1d
        L1b:
            r10 = r19
        L1d:
            r2 = r0 & 32
            if (r2 == 0) goto L29
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePermissions r2 = r2.getPermissions()
            r11 = r2
            goto L2b
        L29:
            r11 = r20
        L2b:
            r2 = r0 & 64
            if (r2 == 0) goto L37
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildScheduledEvents r2 = r2.getGuildScheduledEvents()
            r12 = r2
            goto L39
        L37:
            r12 = r21
        L39:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L4b
            com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel$Companion r2 = com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.Companion
            r3 = r14
            r5 = r17
            r6 = r10
            r7 = r1
            r8 = r11
            r9 = r12
            rx.Observable r0 = com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.Companion.access$observeStores(r2, r3, r5, r6, r7, r8, r9)
            goto L4d
        L4b:
            r0 = r22
        L4d:
            r2 = r13
            r3 = r14
            r5 = r16
            r6 = r17
            r7 = r1
            r8 = r10
            r9 = r11
            r10 = r12
            r11 = r0
            r2.<init>(r3, r5, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.<init>(long, java.lang.Long, java.lang.Long, com.discord.stores.StoreChannels, com.discord.stores.StoreGuilds, com.discord.stores.StorePermissions, com.discord.stores.StoreGuildScheduledEvents, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final GuildScheduledEventEntityType getEntityTypeForChannel(Channel channel) {
        if (channel == null) {
            return GuildScheduledEventEntityType.NONE;
        }
        return ChannelUtils.E(channel) ? GuildScheduledEventEntityType.VOICE : ChannelUtils.z(channel) ? GuildScheduledEventEntityType.STAGE_INSTANCE : GuildScheduledEventEntityType.NONE;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:81:0x0156  */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.StoreState r15) {
        /*
            Method dump skipped, instructions count: 353
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.handleStoreState(com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel$StoreState):void");
    }

    public final StoreState getCurrentStoreState() {
        return this.currentStoreState;
    }

    public final void selectChannel(long j) {
        StoreState storeState;
        Map<Long, Channel> channels;
        Channel channel;
        ViewState viewState = getViewState();
        if ((viewState instanceof ViewState.Valid) && (storeState = this.currentStoreState) != null && (channels = storeState.getChannels()) != null && (channel = channels.get(Long.valueOf(j))) != null) {
            updateViewState(ViewState.Valid.copy$default((ViewState.Valid) viewState, null, channel, null, null, null, null, false, false, false, 509, null));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0048  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0070  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void selectLocationOption(com.discord.api.guildscheduledevent.GuildScheduledEventEntityType r15) {
        /*
            r14 = this;
            java.lang.String r0 = "locationOption"
            d0.z.d.m.checkNotNullParameter(r15, r0)
            java.lang.Object r0 = r14.getViewState()
            com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel$ViewState r0 = (com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.ViewState) r0
            boolean r1 = r0 instanceof com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.ViewState.Valid
            if (r1 != 0) goto L10
            return
        L10:
            r2 = r0
            com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel$ViewState$Valid r2 = (com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.ViewState.Valid) r2
            com.discord.api.guildscheduledevent.GuildScheduledEventEntityType r0 = r2.getSelectedLocationOption()
            if (r15 != r0) goto L1a
            return
        L1a:
            java.lang.Long r0 = r14.initialChannelId
            r1 = 0
            if (r0 == 0) goto L35
            r0.longValue()
            com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel$StoreState r0 = r14.currentStoreState
            if (r0 == 0) goto L35
            java.util.Map r0 = r0.getChannels()
            if (r0 == 0) goto L35
            java.lang.Long r3 = r14.initialChannelId
            java.lang.Object r0 = r0.get(r3)
            com.discord.api.channel.Channel r0 = (com.discord.api.channel.Channel) r0
            goto L36
        L35:
            r0 = r1
        L36:
            com.discord.api.guildscheduledevent.GuildScheduledEventEntityType r3 = com.discord.api.guildscheduledevent.GuildScheduledEventEntityType.VOICE
            r4 = 2
            if (r15 != r3) goto L44
            if (r0 == 0) goto L44
            int r5 = r0.A()
            if (r5 != r4) goto L44
            goto L7e
        L44:
            r5 = 1
            r6 = 0
            if (r15 != r3) goto L70
            java.util.Map r0 = r2.getAvailableChannels()
            java.util.Collection r0 = r0.values()
            java.util.Iterator r0 = r0.iterator()
        L54:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L6d
            java.lang.Object r3 = r0.next()
            r7 = r3
            com.discord.api.channel.Channel r7 = (com.discord.api.channel.Channel) r7
            int r7 = r7.A()
            if (r7 != r4) goto L69
            r7 = 1
            goto L6a
        L69:
            r7 = 0
        L6a:
            if (r7 == 0) goto L54
            r1 = r3
        L6d:
            com.discord.api.channel.Channel r1 = (com.discord.api.channel.Channel) r1
            goto La9
        L70:
            com.discord.api.guildscheduledevent.GuildScheduledEventEntityType r3 = com.discord.api.guildscheduledevent.GuildScheduledEventEntityType.STAGE_INSTANCE
            r4 = 13
            if (r15 != r3) goto L80
            if (r0 == 0) goto L80
            int r7 = r0.A()
            if (r7 != r4) goto L80
        L7e:
            r4 = r0
            goto Laa
        L80:
            if (r15 != r3) goto La9
            java.util.Map r0 = r2.getAvailableChannels()
            java.util.Collection r0 = r0.values()
            java.util.Iterator r0 = r0.iterator()
        L8e:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto La7
            java.lang.Object r3 = r0.next()
            r7 = r3
            com.discord.api.channel.Channel r7 = (com.discord.api.channel.Channel) r7
            int r7 = r7.A()
            if (r7 != r4) goto La3
            r7 = 1
            goto La4
        La3:
            r7 = 0
        La4:
            if (r7 == 0) goto L8e
            r1 = r3
        La7:
            com.discord.api.channel.Channel r1 = (com.discord.api.channel.Channel) r1
        La9:
            r4 = r1
        Laa:
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 504(0x1f8, float:7.06E-43)
            r13 = 0
            java.lang.String r5 = ""
            r3 = r15
            com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel$ViewState$Valid r15 = com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.ViewState.Valid.copy$default(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            r14.updateViewState(r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelectViewModel.selectLocationOption(com.discord.api.guildscheduledevent.GuildScheduledEventEntityType):void");
    }

    public final void setCurrentStoreState(StoreState storeState) {
        this.currentStoreState = storeState;
    }

    public final void setExternalLocation(String str) {
        m.checkNotNullParameter(str, "newExternalLocation");
        ViewState viewState = getViewState();
        if (viewState instanceof ViewState.Valid) {
            updateViewState(ViewState.Valid.copy$default((ViewState.Valid) viewState, null, null, str, null, null, null, false, false, false, 507, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildScheduledEventLocationSelectViewModel(long j, Long l, Long l2, StoreChannels storeChannels, StoreGuilds storeGuilds, StorePermissions storePermissions, StoreGuildScheduledEvents storeGuildScheduledEvents, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventsStore");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.initialChannelId = l;
        this.existingGuildScheduledEventId = l2;
        this.channelsStore = storeChannels;
        Observable<StoreState> X = observable.X(j0.p.a.a());
        m.checkNotNullExpressionValue(X, "storeStateObservable\n   …Schedulers.computation())");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(X, this, null, 2, null), WidgetGuildScheduledEventLocationSelectViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
