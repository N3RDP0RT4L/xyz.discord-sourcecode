package com.discord.widgets.guildscheduledevent.buttonconfiguration;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventTiming;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildButtonConfiguration.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b)\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u0091\u0001\u0012\u0006\u0010\u001f\u001a\u00020\u000b\u0012\u0006\u0010 \u001a\u00020\u000e\u0012\u0006\u0010!\u001a\u00020\u000e\u0012\u0006\u0010\"\u001a\u00020\u000e\u0012\u0006\u0010#\u001a\u00020\u000e\u0012\u0006\u0010$\u001a\u00020\u000e\u0012\u0006\u0010%\u001a\u00020\u000e\u0012\b\b\u0002\u0010&\u001a\u00020\u000e\u0012\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\bb\u0010cJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\n\u0010\u0006J\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0010J\u0010\u0010\u0014\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0010J\u0010\u0010\u0015\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0010J\u0010\u0010\u0016\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0010J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0019J\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0019J\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0019J\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0019J\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u001e\u0010\u0019J¨\u0001\u0010-\u001a\u00020\u00002\b\b\u0002\u0010\u001f\u001a\u00020\u000b2\b\b\u0002\u0010 \u001a\u00020\u000e2\b\b\u0002\u0010!\u001a\u00020\u000e2\b\b\u0002\u0010\"\u001a\u00020\u000e2\b\b\u0002\u0010#\u001a\u00020\u000e2\b\b\u0002\u0010$\u001a\u00020\u000e2\b\b\u0002\u0010%\u001a\u00020\u000e2\b\b\u0002\u0010&\u001a\u00020\u000e2\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u0017HÆ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u00100\u001a\u00020/HÖ\u0001¢\u0006\u0004\b0\u00101J\u0010\u00102\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b2\u00103J\u001a\u00106\u001a\u00020\u000e2\b\u00105\u001a\u0004\u0018\u000104HÖ\u0003¢\u0006\u0004\b6\u00107R\u001b\u0010)\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b)\u00108\u001a\u0004\b9\u0010\u0019R\u001c\u0010:\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u00103R\u001e\u0010=\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b=\u00108\u001a\u0004\b>\u0010\u0019R\u001e\u0010?\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b?\u00108\u001a\u0004\b@\u0010\u0019R\u001c\u0010A\u001a\u00020\u000e8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bA\u0010B\u001a\u0004\bA\u0010\u0010R\u001b\u0010+\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b+\u00108\u001a\u0004\bC\u0010\u0019R\u001c\u0010D\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bD\u0010;\u001a\u0004\bE\u00103R\u0019\u0010 \u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010B\u001a\u0004\bF\u0010\u0010R\u0019\u0010%\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010B\u001a\u0004\b%\u0010\u0010R\u001b\u0010,\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b,\u00108\u001a\u0004\bG\u0010\u0019R\u0016\u0010H\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bH\u0010BR\u0019\u0010\"\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010B\u001a\u0004\b\"\u0010\u0010R\u001c\u0010I\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bI\u0010B\u001a\u0004\bJ\u0010\u0010R\u001b\u0010*\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b*\u00108\u001a\u0004\bK\u0010\u0019R\u001e\u0010L\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bL\u00108\u001a\u0004\bM\u0010\u0019R\u001c\u0010N\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bN\u0010B\u001a\u0004\bO\u0010\u0010R\u001c\u0010P\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bP\u0010B\u001a\u0004\bP\u0010\u0010R\u001e\u0010Q\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bQ\u00108\u001a\u0004\bR\u0010\u0019R\u0016\u0010S\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010BR\u0019\u0010&\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010B\u001a\u0004\b&\u0010\u0010R\u001c\u0010T\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bT\u0010B\u001a\u0004\bU\u0010\u0010R\u0019\u0010\u001f\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010V\u001a\u0004\bW\u0010\rR\u0016\u0010X\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bX\u0010BR\u0019\u0010$\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010B\u001a\u0004\b$\u0010\u0010R\u001c\u0010Y\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bY\u0010B\u001a\u0004\bY\u0010\u0010R\u0016\u0010Z\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010BR\u0019\u0010!\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010B\u001a\u0004\b[\u0010\u0010R\u001b\u0010'\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b'\u00108\u001a\u0004\b\\\u0010\u0019R\u001b\u0010(\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b(\u00108\u001a\u0004\b]\u0010\u0019R\u0016\u0010_\u001a\u00020^8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b_\u0010`R\u0019\u0010#\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010B\u001a\u0004\ba\u0010\u0010¨\u0006d"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/buttonconfiguration/GuildButtonConfiguration;", "Lcom/discord/widgets/guildscheduledevent/buttonconfiguration/ButtonConfiguration;", "Landroid/content/Context;", "context", "", "primaryButtonText", "(Landroid/content/Context;)Ljava/lang/CharSequence;", "", "secondaryButtonTextColor", "(Landroid/content/Context;)I", "secondaryButtonText", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "", "component2", "()Z", "component3", "component4", "component5", "component6", "component7", "component8", "Landroid/view/View$OnClickListener;", "component9", "()Landroid/view/View$OnClickListener;", "component10", "component11", "component12", "component13", "component14", "guildScheduledEvent", "canRsvp", "canStartEvent", "isConnected", "canConnect", "isInGuild", "isRsvped", "isDetailView", "onRsvpButtonClicked", "onJoinButtonClicked", "onEndEventButtonClicked", "onStartEventButtonClicked", "onShareButtonClicked", "onExtraButtonClicked", "copy", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;ZZZZZZZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)Lcom/discord/widgets/guildscheduledevent/buttonconfiguration/GuildButtonConfiguration;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Landroid/view/View$OnClickListener;", "getOnEndEventButtonClicked", "secondaryButtonTextDrawableRes", "I", "getSecondaryButtonTextDrawableRes", "primaryButtonOnClickListener", "getPrimaryButtonOnClickListener", "secondaryButtonOnClickListener", "getSecondaryButtonOnClickListener", "isExtrasVisible", "Z", "getOnShareButtonClicked", "secondaryButtonBackground", "getSecondaryButtonBackground", "getCanRsvp", "getOnExtraButtonClicked", "isEventComplete", "primaryButtonVisible", "getPrimaryButtonVisible", "getOnStartEventButtonClicked", "shareButtonOnClickListener", "getShareButtonOnClickListener", "secondaryButtonVisible", "getSecondaryButtonVisible", "isAnyButtonVisible", "extrasButtonOnClickListener", "getExtrasButtonOnClickListener", "isEndEventVisible", "secondaryButtonEnabled", "getSecondaryButtonEnabled", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", "canConnectToChannel", "isShareVisible", "isEventActive", "getCanStartEvent", "getOnRsvpButtonClicked", "getOnJoinButtonClicked", "Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;", "timing", "Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;", "getCanConnect", HookHelper.constructorName, "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;ZZZZZZZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildButtonConfiguration implements ButtonConfiguration {
    private final boolean canConnect;
    private final boolean canConnectToChannel;
    private final boolean canRsvp;
    private final boolean canStartEvent;
    private final View.OnClickListener extrasButtonOnClickListener;
    private final GuildScheduledEvent guildScheduledEvent;
    private final boolean isAnyButtonVisible;
    private final boolean isConnected;
    private final boolean isDetailView;
    private final boolean isEndEventVisible;
    private final boolean isEventActive;
    private final boolean isEventComplete;
    private final boolean isExtrasVisible;
    private final boolean isInGuild;
    private final boolean isRsvped;
    private final boolean isShareVisible;
    private final View.OnClickListener onEndEventButtonClicked;
    private final View.OnClickListener onExtraButtonClicked;
    private final View.OnClickListener onJoinButtonClicked;
    private final View.OnClickListener onRsvpButtonClicked;
    private final View.OnClickListener onShareButtonClicked;
    private final View.OnClickListener onStartEventButtonClicked;
    private final View.OnClickListener primaryButtonOnClickListener;
    private final boolean primaryButtonVisible;
    private final int secondaryButtonBackground;
    private final boolean secondaryButtonEnabled;
    private final View.OnClickListener secondaryButtonOnClickListener;
    private final int secondaryButtonTextDrawableRes;
    private final boolean secondaryButtonVisible;
    private final View.OnClickListener shareButtonOnClickListener;
    private final GuildScheduledEventTiming timing;

    public GuildButtonConfiguration(GuildScheduledEvent guildScheduledEvent, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, View.OnClickListener onClickListener, View.OnClickListener onClickListener2, View.OnClickListener onClickListener3, View.OnClickListener onClickListener4, View.OnClickListener onClickListener5, View.OnClickListener onClickListener6) {
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        this.guildScheduledEvent = guildScheduledEvent;
        this.canRsvp = z2;
        this.canStartEvent = z3;
        this.isConnected = z4;
        this.canConnect = z5;
        this.isInGuild = z6;
        this.isRsvped = z7;
        this.isDetailView = z8;
        this.onRsvpButtonClicked = onClickListener;
        View.OnClickListener onClickListener7 = onClickListener2;
        this.onJoinButtonClicked = onClickListener7;
        this.onEndEventButtonClicked = onClickListener3;
        this.onStartEventButtonClicked = onClickListener4;
        this.onShareButtonClicked = onClickListener5;
        this.onExtraButtonClicked = onClickListener6;
        GuildScheduledEventTiming eventTiming = GuildScheduledEventUtilitiesKt.getEventTiming(guildScheduledEvent);
        this.timing = eventTiming;
        boolean z9 = true;
        boolean z10 = guildScheduledEvent.m() == GuildScheduledEventStatus.COMPLETED;
        this.isEventComplete = z10;
        boolean z11 = eventTiming == GuildScheduledEventTiming.LIVE;
        this.isEventActive = z11;
        this.canConnectToChannel = z5;
        boolean z12 = z8 && z11 && z4 && z3;
        this.isEndEventVisible = z12;
        this.primaryButtonVisible = (!z3 || z10) ? false : eventTiming.isStartable();
        this.primaryButtonOnClickListener = onClickListener4;
        this.secondaryButtonVisible = z2;
        if (z12) {
            onClickListener7 = onClickListener3;
        } else if (!z11) {
            onClickListener7 = onClickListener;
        }
        this.secondaryButtonOnClickListener = onClickListener7;
        this.secondaryButtonEnabled = !z10 && (!z11 || (guildScheduledEvent.f() != GuildScheduledEventEntityType.EXTERNAL && z5));
        this.secondaryButtonTextDrawableRes = (z11 || !getSecondaryButtonEnabled()) ? 0 : z7 ? R.drawable.ic_check_active_16dp : R.drawable.ic_bell_16dp;
        boolean secondaryButtonEnabled = getSecondaryButtonEnabled();
        int i = R.drawable.bg_guild_scheduled_event_list_item_interested_button_default;
        if (!secondaryButtonEnabled) {
            i = R.drawable.bg_guild_scheduled_event_list_item_interested_button_disabled;
        } else if (!z12) {
            if (z11) {
                i = R.drawable.bg_guild_scheduled_event_list_item_interested_button_active;
            } else if (z7) {
                i = R.drawable.bg_guild_scheduled_event_list_item_interested_button_interested;
            }
        }
        this.secondaryButtonBackground = i;
        this.isShareVisible = !z10;
        this.shareButtonOnClickListener = onClickListener5;
        this.isExtrasVisible = true;
        this.extrasButtonOnClickListener = onClickListener6;
        if (!getSecondaryButtonVisible() && !getPrimaryButtonVisible() && !z12 && !isShareVisible()) {
            z9 = false;
        }
        this.isAnyButtonVisible = z9;
    }

    public final GuildScheduledEvent component1() {
        return this.guildScheduledEvent;
    }

    public final View.OnClickListener component10() {
        return this.onJoinButtonClicked;
    }

    public final View.OnClickListener component11() {
        return this.onEndEventButtonClicked;
    }

    public final View.OnClickListener component12() {
        return this.onStartEventButtonClicked;
    }

    public final View.OnClickListener component13() {
        return this.onShareButtonClicked;
    }

    public final View.OnClickListener component14() {
        return this.onExtraButtonClicked;
    }

    public final boolean component2() {
        return this.canRsvp;
    }

    public final boolean component3() {
        return this.canStartEvent;
    }

    public final boolean component4() {
        return this.isConnected;
    }

    public final boolean component5() {
        return this.canConnect;
    }

    public final boolean component6() {
        return this.isInGuild;
    }

    public final boolean component7() {
        return this.isRsvped;
    }

    public final boolean component8() {
        return this.isDetailView;
    }

    public final View.OnClickListener component9() {
        return this.onRsvpButtonClicked;
    }

    public final GuildButtonConfiguration copy(GuildScheduledEvent guildScheduledEvent, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, View.OnClickListener onClickListener, View.OnClickListener onClickListener2, View.OnClickListener onClickListener3, View.OnClickListener onClickListener4, View.OnClickListener onClickListener5, View.OnClickListener onClickListener6) {
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        return new GuildButtonConfiguration(guildScheduledEvent, z2, z3, z4, z5, z6, z7, z8, onClickListener, onClickListener2, onClickListener3, onClickListener4, onClickListener5, onClickListener6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildButtonConfiguration)) {
            return false;
        }
        GuildButtonConfiguration guildButtonConfiguration = (GuildButtonConfiguration) obj;
        return m.areEqual(this.guildScheduledEvent, guildButtonConfiguration.guildScheduledEvent) && this.canRsvp == guildButtonConfiguration.canRsvp && this.canStartEvent == guildButtonConfiguration.canStartEvent && this.isConnected == guildButtonConfiguration.isConnected && this.canConnect == guildButtonConfiguration.canConnect && this.isInGuild == guildButtonConfiguration.isInGuild && this.isRsvped == guildButtonConfiguration.isRsvped && this.isDetailView == guildButtonConfiguration.isDetailView && m.areEqual(this.onRsvpButtonClicked, guildButtonConfiguration.onRsvpButtonClicked) && m.areEqual(this.onJoinButtonClicked, guildButtonConfiguration.onJoinButtonClicked) && m.areEqual(this.onEndEventButtonClicked, guildButtonConfiguration.onEndEventButtonClicked) && m.areEqual(this.onStartEventButtonClicked, guildButtonConfiguration.onStartEventButtonClicked) && m.areEqual(this.onShareButtonClicked, guildButtonConfiguration.onShareButtonClicked) && m.areEqual(this.onExtraButtonClicked, guildButtonConfiguration.onExtraButtonClicked);
    }

    public final boolean getCanConnect() {
        return this.canConnect;
    }

    public final boolean getCanRsvp() {
        return this.canRsvp;
    }

    public final boolean getCanStartEvent() {
        return this.canStartEvent;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public View.OnClickListener getExtrasButtonOnClickListener() {
        return this.extrasButtonOnClickListener;
    }

    public final GuildScheduledEvent getGuildScheduledEvent() {
        return this.guildScheduledEvent;
    }

    public final View.OnClickListener getOnEndEventButtonClicked() {
        return this.onEndEventButtonClicked;
    }

    public final View.OnClickListener getOnExtraButtonClicked() {
        return this.onExtraButtonClicked;
    }

    public final View.OnClickListener getOnJoinButtonClicked() {
        return this.onJoinButtonClicked;
    }

    public final View.OnClickListener getOnRsvpButtonClicked() {
        return this.onRsvpButtonClicked;
    }

    public final View.OnClickListener getOnShareButtonClicked() {
        return this.onShareButtonClicked;
    }

    public final View.OnClickListener getOnStartEventButtonClicked() {
        return this.onStartEventButtonClicked;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public View.OnClickListener getPrimaryButtonOnClickListener() {
        return this.primaryButtonOnClickListener;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public boolean getPrimaryButtonVisible() {
        return this.primaryButtonVisible;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public int getSecondaryButtonBackground() {
        return this.secondaryButtonBackground;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public boolean getSecondaryButtonEnabled() {
        return this.secondaryButtonEnabled;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public View.OnClickListener getSecondaryButtonOnClickListener() {
        return this.secondaryButtonOnClickListener;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public int getSecondaryButtonTextDrawableRes() {
        return this.secondaryButtonTextDrawableRes;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public boolean getSecondaryButtonVisible() {
        return this.secondaryButtonVisible;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public View.OnClickListener getShareButtonOnClickListener() {
        return this.shareButtonOnClickListener;
    }

    public int hashCode() {
        GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
        int i = 0;
        int hashCode = (guildScheduledEvent != null ? guildScheduledEvent.hashCode() : 0) * 31;
        boolean z2 = this.canRsvp;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        boolean z3 = this.canStartEvent;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        boolean z4 = this.isConnected;
        if (z4) {
            z4 = true;
        }
        int i9 = z4 ? 1 : 0;
        int i10 = z4 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        boolean z5 = this.canConnect;
        if (z5) {
            z5 = true;
        }
        int i12 = z5 ? 1 : 0;
        int i13 = z5 ? 1 : 0;
        int i14 = (i11 + i12) * 31;
        boolean z6 = this.isInGuild;
        if (z6) {
            z6 = true;
        }
        int i15 = z6 ? 1 : 0;
        int i16 = z6 ? 1 : 0;
        int i17 = (i14 + i15) * 31;
        boolean z7 = this.isRsvped;
        if (z7) {
            z7 = true;
        }
        int i18 = z7 ? 1 : 0;
        int i19 = z7 ? 1 : 0;
        int i20 = (i17 + i18) * 31;
        boolean z8 = this.isDetailView;
        if (!z8) {
            i2 = z8 ? 1 : 0;
        }
        int i21 = (i20 + i2) * 31;
        View.OnClickListener onClickListener = this.onRsvpButtonClicked;
        int hashCode2 = (i21 + (onClickListener != null ? onClickListener.hashCode() : 0)) * 31;
        View.OnClickListener onClickListener2 = this.onJoinButtonClicked;
        int hashCode3 = (hashCode2 + (onClickListener2 != null ? onClickListener2.hashCode() : 0)) * 31;
        View.OnClickListener onClickListener3 = this.onEndEventButtonClicked;
        int hashCode4 = (hashCode3 + (onClickListener3 != null ? onClickListener3.hashCode() : 0)) * 31;
        View.OnClickListener onClickListener4 = this.onStartEventButtonClicked;
        int hashCode5 = (hashCode4 + (onClickListener4 != null ? onClickListener4.hashCode() : 0)) * 31;
        View.OnClickListener onClickListener5 = this.onShareButtonClicked;
        int hashCode6 = (hashCode5 + (onClickListener5 != null ? onClickListener5.hashCode() : 0)) * 31;
        View.OnClickListener onClickListener6 = this.onExtraButtonClicked;
        if (onClickListener6 != null) {
            i = onClickListener6.hashCode();
        }
        return hashCode6 + i;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public boolean isAnyButtonVisible() {
        return this.isAnyButtonVisible;
    }

    public final boolean isConnected() {
        return this.isConnected;
    }

    public final boolean isDetailView() {
        return this.isDetailView;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public boolean isExtrasVisible() {
        return this.isExtrasVisible;
    }

    public final boolean isInGuild() {
        return this.isInGuild;
    }

    public final boolean isRsvped() {
        return this.isRsvped;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public boolean isShareVisible() {
        return this.isShareVisible;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public CharSequence primaryButtonText(Context context) {
        CharSequence b2;
        m.checkNotNullParameter(context, "context");
        b2 = b.b(context, R.string.start_event, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public CharSequence secondaryButtonText(Context context) {
        int i;
        CharSequence b2;
        m.checkNotNullParameter(context, "context");
        if (!this.isEventActive || this.guildScheduledEvent.f() != GuildScheduledEventEntityType.EXTERNAL) {
            boolean z2 = this.isEventActive;
            if (z2 && !this.canConnectToChannel) {
                i = R.string.channel_locked_short;
            } else if (this.isEndEventVisible) {
                i = R.string.end_event;
            } else if (z2 && this.isConnected) {
                i = R.string.go_to_channel;
            } else if (z2 && this.guildScheduledEvent.f() == GuildScheduledEventEntityType.VOICE) {
                i = R.string.guild_event_join;
            } else if (this.isEventActive) {
                i = R.string.stage_channel_join_button;
            } else {
                i = this.isEventComplete ? R.string.guild_event_invite_completed : R.string.indicate_rsvp;
            }
        } else {
            i = R.string.guild_event_started;
        }
        b2 = b.b(context, i, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    @Override // com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration
    public int secondaryButtonTextColor(Context context) {
        m.checkNotNullParameter(context, "context");
        return !getSecondaryButtonEnabled() ? ColorCompat.getColor(context, (int) R.color.white_alpha_40) : (!this.isRsvped || this.isEventActive || this.isEventComplete) ? ColorCompat.getColor(context, (int) R.color.white) : ColorCompat.getThemedColor(context, (int) R.attr.colorBackgroundAccent);
    }

    public String toString() {
        StringBuilder R = a.R("GuildButtonConfiguration(guildScheduledEvent=");
        R.append(this.guildScheduledEvent);
        R.append(", canRsvp=");
        R.append(this.canRsvp);
        R.append(", canStartEvent=");
        R.append(this.canStartEvent);
        R.append(", isConnected=");
        R.append(this.isConnected);
        R.append(", canConnect=");
        R.append(this.canConnect);
        R.append(", isInGuild=");
        R.append(this.isInGuild);
        R.append(", isRsvped=");
        R.append(this.isRsvped);
        R.append(", isDetailView=");
        R.append(this.isDetailView);
        R.append(", onRsvpButtonClicked=");
        R.append(this.onRsvpButtonClicked);
        R.append(", onJoinButtonClicked=");
        R.append(this.onJoinButtonClicked);
        R.append(", onEndEventButtonClicked=");
        R.append(this.onEndEventButtonClicked);
        R.append(", onStartEventButtonClicked=");
        R.append(this.onStartEventButtonClicked);
        R.append(", onShareButtonClicked=");
        R.append(this.onShareButtonClicked);
        R.append(", onExtraButtonClicked=");
        R.append(this.onExtraButtonClicked);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ GuildButtonConfiguration(GuildScheduledEvent guildScheduledEvent, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, View.OnClickListener onClickListener, View.OnClickListener onClickListener2, View.OnClickListener onClickListener3, View.OnClickListener onClickListener4, View.OnClickListener onClickListener5, View.OnClickListener onClickListener6, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(guildScheduledEvent, z2, z3, z4, z5, z6, z7, (i & 128) != 0 ? false : z8, (i & 256) != 0 ? null : onClickListener, (i & 512) != 0 ? null : onClickListener2, (i & 1024) != 0 ? null : onClickListener3, (i & 2048) != 0 ? null : onClickListener4, (i & 4096) != 0 ? null : onClickListener5, (i & 8192) != 0 ? null : onClickListener6);
    }
}
