package com.discord.widgets.guildscheduledevent;

import com.discord.stores.StoreChannels;
import com.discord.stores.StoreDirectories;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUserSettings;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: GuildScheduledEventDetailsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;", "invoke", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventDetailsViewModel$Companion$observeStores$1 extends o implements Function0<GuildScheduledEventDetailsViewModel.StoreState> {
    public final /* synthetic */ GuildScheduledEventDetailsArgs $args;
    public final /* synthetic */ StoreChannels $channelsStore;
    public final /* synthetic */ StoreDirectories $directoriesStore;
    public final /* synthetic */ StoreGuildScheduledEvents $guildScheduledEventsStore;
    public final /* synthetic */ StoreGuilds $guildsStore;
    public final /* synthetic */ StorePermissions $permissionsStore;
    public final /* synthetic */ StoreVoiceChannelSelected $selectedVoiceChannelStore;
    public final /* synthetic */ StoreUserSettings $userSettingsStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventDetailsViewModel$Companion$observeStores$1(GuildScheduledEventDetailsArgs guildScheduledEventDetailsArgs, StoreDirectories storeDirectories, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreVoiceChannelSelected storeVoiceChannelSelected, StorePermissions storePermissions, StoreUserSettings storeUserSettings) {
        super(0);
        this.$args = guildScheduledEventDetailsArgs;
        this.$directoriesStore = storeDirectories;
        this.$guildScheduledEventsStore = storeGuildScheduledEvents;
        this.$guildsStore = storeGuilds;
        this.$channelsStore = storeChannels;
        this.$selectedVoiceChannelStore = storeVoiceChannelSelected;
        this.$permissionsStore = storePermissions;
        this.$userSettingsStore = storeUserSettings;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    /* JADX WARN: Removed duplicated region for block: B:30:0x007e  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x0182  */
    @Override // kotlin.jvm.functions.Function0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel.StoreState invoke() {
        /*
            Method dump skipped, instructions count: 426
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel$Companion$observeStores$1.invoke():com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel$StoreState");
    }
}
