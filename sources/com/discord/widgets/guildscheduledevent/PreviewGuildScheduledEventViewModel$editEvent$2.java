package com.discord.widgets.guildscheduledevent;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: PreviewGuildScheduledEventViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "it", "invoke", "(Lkotlin/Unit;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PreviewGuildScheduledEventViewModel$editEvent$2 extends o implements Function1<Unit, Unit> {
    public final /* synthetic */ long $eventId;
    public final /* synthetic */ Function1 $onRequestSuccess;
    public final /* synthetic */ PreviewGuildScheduledEventViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PreviewGuildScheduledEventViewModel$editEvent$2(PreviewGuildScheduledEventViewModel previewGuildScheduledEventViewModel, Function1 function1, long j) {
        super(1);
        this.this$0 = previewGuildScheduledEventViewModel;
        this.$onRequestSuccess = function1;
        this.$eventId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Unit unit) {
        invoke2(unit);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Unit unit) {
        m.checkNotNullParameter(unit, "it");
        this.this$0.setRequestFinished();
        this.$onRequestSuccess.invoke(Long.valueOf(this.$eventId));
    }
}
