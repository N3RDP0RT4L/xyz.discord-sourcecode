package com.discord.widgets.guildscheduledevent;

import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.utcdatetime.UtcDateTime;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: GuildScheduledEventModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0011\u0010\u0002\u001a\u00020\u0001*\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "toModel", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventModelKt {
    public static final GuildScheduledEventModel toModel(GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(guildScheduledEvent, "$this$toModel");
        GuildScheduledEventPickerDateTime guildScheduledEventPickerDateTime = GuildScheduledEventPickerDateTime.INSTANCE;
        Pair<GuildScheduledEventPickerDate, GuildScheduledEventPickerTime> fromUtcDateTime = guildScheduledEventPickerDateTime.fromUtcDateTime(guildScheduledEvent.l());
        UtcDateTime k = guildScheduledEvent.k();
        GuildScheduledEventPickerTime guildScheduledEventPickerTime = null;
        Pair<GuildScheduledEventPickerDate, GuildScheduledEventPickerTime> fromUtcDateTime2 = k != null ? guildScheduledEventPickerDateTime.fromUtcDateTime(k) : null;
        long h = guildScheduledEvent.h();
        String j = guildScheduledEvent.j();
        Long b2 = guildScheduledEvent.b();
        Long c = guildScheduledEvent.c();
        GuildScheduledEventPickerDate first = fromUtcDateTime.getFirst();
        GuildScheduledEventPickerTime second = fromUtcDateTime.getSecond();
        GuildScheduledEventPickerDate first2 = fromUtcDateTime2 != null ? fromUtcDateTime2.getFirst() : null;
        if (fromUtcDateTime2 != null) {
            guildScheduledEventPickerTime = fromUtcDateTime2.getSecond();
        }
        return new GuildScheduledEventModel(h, j, b2, c, first, second, first2, guildScheduledEventPickerTime, guildScheduledEvent.d(), guildScheduledEvent.f(), guildScheduledEvent.e(), guildScheduledEvent.n(), null, 4096, null);
    }
}
