package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.app.AppViewModel;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent;
import com.discord.widgets.stage.StageChannelAPI;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PreviewGuildScheduledEventViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u00012B7\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\b\u0010.\u001a\u0004\u0018\u00010-\u0012\b\b\u0002\u0010+\u001a\u00020*\u0012\b\b\u0002\u0010(\u001a\u00020'\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b0\u00101J/\u0010\n\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00032\u0016\u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005H\u0002¢\u0006\u0004\b\n\u0010\u000bJ;\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\f2\u001a\u0010\t\u001a\u0016\u0012\f\u0012\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\u0004\u0012\u00020\b0\u0005H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ3\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00032\u001a\u0010\t\u001a\u0016\u0012\f\u0012\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\u0004\u0012\u00020\b0\u0005H\u0002¢\u0006\u0004\b\u0010\u0010\u000bJ3\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00032\u001a\u0010\t\u001a\u0016\u0012\f\u0012\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\u0004\u0012\u00020\b0\u0005H\u0002¢\u0006\u0004\b\u0011\u0010\u000bJ\u000f\u0010\u0012\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0019\u0010\u0016\u001a\u00020\f2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J;\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\f2\u001a\u0010\t\u001a\u0016\u0012\f\u0012\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\u0004\u0012\u00020\b0\u0005¢\u0006\u0004\b\u0018\u0010\u000fJ\r\u0010\u0019\u001a\u00020\f¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\"\u0010!\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u0018\u0010.\u001a\u0004\u0018\u00010-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/¨\u00063"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", "Landroid/content/Context;", "context", "Lkotlin/Function1;", "", "Lcom/discord/primitives/GuildScheduledEventId;", "", "onRequestSuccess", "startEvent", "(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V", "", "sendNotification", "startStageEvent", "(Landroid/content/Context;ZLkotlin/jvm/functions/Function1;)V", "createEvent", "editEvent", "setRequestFinished", "()V", "Lcom/discord/api/channel/Channel;", "channel", "canNotifyEveryone", "(Lcom/discord/api/channel/Channel;)Z", "onBottomButtonClicked", "isCreate", "()Z", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "eventModel", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "currentViewState", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", "getCurrentViewState", "()Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", "setCurrentViewState", "(Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;)V", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$ExistingEventData;", "existingEventData", "Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$ExistingEventData;", HookHelper.constructorName, "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$ExistingEventData;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuildScheduledEvents;)V", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PreviewGuildScheduledEventViewModel extends AppViewModel<ViewState> {
    private final StoreChannels channelsStore;
    private ViewState currentViewState;
    private final GuildScheduledEventModel eventModel;
    private final WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData;
    private final StoreGuildScheduledEvents guildScheduledEventsStore;
    private final StorePermissions permissionsStore;

    /* compiled from: PreviewGuildScheduledEventViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Initial", "Initialized", "Invalid", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Initial;", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Initialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: PreviewGuildScheduledEventViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Initial;", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Initial extends ViewState {
            public static final Initial INSTANCE = new Initial();

            private Initial() {
                super(null);
            }
        }

        /* compiled from: PreviewGuildScheduledEventViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\t\u0012\u0006\u0010\u0013\u001a\u00020\f¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJD\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\t2\b\b\u0002\u0010\u0013\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001e\u001a\u00020\u00022\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b%\u0010\u000eR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010'\u001a\u0004\b(\u0010\u000bR\u0019\u0010)\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\"\u001a\u0004\b)\u0010\u0004¨\u0006,"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Initialized;", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", "", "component1", "()Z", "Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;", "component2", "()Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;", "component3", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "component4", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "component5", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "requestProcessing", "existingEventAction", "canNotifyEveryone", "locationInfo", "eventModel", "copy", "(ZLcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;ZLcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;)Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Initialized;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;", "getExistingEventAction", "Z", "getCanNotifyEveryone", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "getEventModel", "getRequestProcessing", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "getLocationInfo", "isStartingEvent", HookHelper.constructorName, "(ZLcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;ZLcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Initialized extends ViewState {
            private final boolean canNotifyEveryone;
            private final GuildScheduledEventModel eventModel;
            private final WidgetPreviewGuildScheduledEvent.Companion.Action existingEventAction;
            private final boolean isStartingEvent;
            private final GuildScheduledEventLocationInfo locationInfo;
            private final boolean requestProcessing;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Initialized(boolean z2, WidgetPreviewGuildScheduledEvent.Companion.Action action, boolean z3, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, GuildScheduledEventModel guildScheduledEventModel) {
                super(null);
                m.checkNotNullParameter(guildScheduledEventLocationInfo, "locationInfo");
                m.checkNotNullParameter(guildScheduledEventModel, "eventModel");
                this.requestProcessing = z2;
                this.existingEventAction = action;
                this.canNotifyEveryone = z3;
                this.locationInfo = guildScheduledEventLocationInfo;
                this.eventModel = guildScheduledEventModel;
                this.isStartingEvent = action == WidgetPreviewGuildScheduledEvent.Companion.Action.START_EVENT;
            }

            public static /* synthetic */ Initialized copy$default(Initialized initialized, boolean z2, WidgetPreviewGuildScheduledEvent.Companion.Action action, boolean z3, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, GuildScheduledEventModel guildScheduledEventModel, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = initialized.requestProcessing;
                }
                if ((i & 2) != 0) {
                    action = initialized.existingEventAction;
                }
                WidgetPreviewGuildScheduledEvent.Companion.Action action2 = action;
                if ((i & 4) != 0) {
                    z3 = initialized.canNotifyEveryone;
                }
                boolean z4 = z3;
                if ((i & 8) != 0) {
                    guildScheduledEventLocationInfo = initialized.locationInfo;
                }
                GuildScheduledEventLocationInfo guildScheduledEventLocationInfo2 = guildScheduledEventLocationInfo;
                if ((i & 16) != 0) {
                    guildScheduledEventModel = initialized.eventModel;
                }
                return initialized.copy(z2, action2, z4, guildScheduledEventLocationInfo2, guildScheduledEventModel);
            }

            public final boolean component1() {
                return this.requestProcessing;
            }

            public final WidgetPreviewGuildScheduledEvent.Companion.Action component2() {
                return this.existingEventAction;
            }

            public final boolean component3() {
                return this.canNotifyEveryone;
            }

            public final GuildScheduledEventLocationInfo component4() {
                return this.locationInfo;
            }

            public final GuildScheduledEventModel component5() {
                return this.eventModel;
            }

            public final Initialized copy(boolean z2, WidgetPreviewGuildScheduledEvent.Companion.Action action, boolean z3, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, GuildScheduledEventModel guildScheduledEventModel) {
                m.checkNotNullParameter(guildScheduledEventLocationInfo, "locationInfo");
                m.checkNotNullParameter(guildScheduledEventModel, "eventModel");
                return new Initialized(z2, action, z3, guildScheduledEventLocationInfo, guildScheduledEventModel);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Initialized)) {
                    return false;
                }
                Initialized initialized = (Initialized) obj;
                return this.requestProcessing == initialized.requestProcessing && m.areEqual(this.existingEventAction, initialized.existingEventAction) && this.canNotifyEveryone == initialized.canNotifyEveryone && m.areEqual(this.locationInfo, initialized.locationInfo) && m.areEqual(this.eventModel, initialized.eventModel);
            }

            public final boolean getCanNotifyEveryone() {
                return this.canNotifyEveryone;
            }

            public final GuildScheduledEventModel getEventModel() {
                return this.eventModel;
            }

            public final WidgetPreviewGuildScheduledEvent.Companion.Action getExistingEventAction() {
                return this.existingEventAction;
            }

            public final GuildScheduledEventLocationInfo getLocationInfo() {
                return this.locationInfo;
            }

            public final boolean getRequestProcessing() {
                return this.requestProcessing;
            }

            public int hashCode() {
                boolean z2 = this.requestProcessing;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                WidgetPreviewGuildScheduledEvent.Companion.Action action = this.existingEventAction;
                int i5 = 0;
                int hashCode = (i4 + (action != null ? action.hashCode() : 0)) * 31;
                boolean z3 = this.canNotifyEveryone;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                int i6 = (hashCode + i) * 31;
                GuildScheduledEventLocationInfo guildScheduledEventLocationInfo = this.locationInfo;
                int hashCode2 = (i6 + (guildScheduledEventLocationInfo != null ? guildScheduledEventLocationInfo.hashCode() : 0)) * 31;
                GuildScheduledEventModel guildScheduledEventModel = this.eventModel;
                if (guildScheduledEventModel != null) {
                    i5 = guildScheduledEventModel.hashCode();
                }
                return hashCode2 + i5;
            }

            public final boolean isStartingEvent() {
                return this.isStartingEvent;
            }

            public String toString() {
                StringBuilder R = a.R("Initialized(requestProcessing=");
                R.append(this.requestProcessing);
                R.append(", existingEventAction=");
                R.append(this.existingEventAction);
                R.append(", canNotifyEveryone=");
                R.append(this.canNotifyEveryone);
                R.append(", locationInfo=");
                R.append(this.locationInfo);
                R.append(", eventModel=");
                R.append(this.eventModel);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: PreviewGuildScheduledEventViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            GuildScheduledEventEntityType.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[GuildScheduledEventEntityType.STAGE_INSTANCE.ordinal()] = 1;
        }
    }

    public /* synthetic */ PreviewGuildScheduledEventViewModel(GuildScheduledEventModel guildScheduledEventModel, WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData, StoreChannels storeChannels, StorePermissions storePermissions, StoreGuildScheduledEvents storeGuildScheduledEvents, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(guildScheduledEventModel, existingEventData, (i & 4) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 8) != 0 ? StoreStream.Companion.getPermissions() : storePermissions, (i & 16) != 0 ? StoreStream.Companion.getGuildScheduledEvents() : storeGuildScheduledEvents);
    }

    private final boolean canNotifyEveryone(Channel channel) {
        Long l;
        if (channel == null || !ChannelUtils.z(channel) || (l = (Long) a.c(channel, this.permissionsStore.getPermissionsByChannel())) == null) {
            return false;
        }
        return PermissionUtils.can(Permission.MENTION_EVERYONE, Long.valueOf(l.longValue()));
    }

    private final void createEvent(Context context, Function1<? super Long, Unit> function1) {
        RestAPIParams.CreateGuildScheduledEventBody createRequestBody = this.eventModel.toCreateRequestBody();
        if (createRequestBody != null) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().createGuildScheduledEvent(this.eventModel.getGuildId(), createRequestBody), false, 1, null), this, null, 2, null), PreviewGuildScheduledEventViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new PreviewGuildScheduledEventViewModel$createEvent$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new PreviewGuildScheduledEventViewModel$createEvent$2(this, function1));
        }
    }

    private final void editEvent(Context context, Function1<? super Long, Unit> function1) {
        long guildScheduledEventId;
        GuildScheduledEvent findEventFromStore;
        RestAPIParams.UpdateGuildScheduledEventBody updateRequestBody;
        WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData = this.existingEventData;
        if (existingEventData != null && (findEventFromStore = this.guildScheduledEventsStore.findEventFromStore((guildScheduledEventId = existingEventData.getGuildScheduledEventId()), Long.valueOf(this.eventModel.getGuildId()))) != null && (updateRequestBody = this.eventModel.toUpdateRequestBody(findEventFromStore)) != null) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateGuildScheduledEvent(findEventFromStore.h(), guildScheduledEventId, updateRequestBody), false, 1, null), this, null, 2, null), PreviewGuildScheduledEventViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new PreviewGuildScheduledEventViewModel$editEvent$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new PreviewGuildScheduledEventViewModel$editEvent$2(this, function1, guildScheduledEventId));
        }
    }

    public static /* synthetic */ void onBottomButtonClicked$default(PreviewGuildScheduledEventViewModel previewGuildScheduledEventViewModel, Context context, boolean z2, Function1 function1, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        previewGuildScheduledEventViewModel.onBottomButtonClicked(context, z2, function1);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setRequestFinished() {
        ViewState requireViewState = requireViewState();
        Objects.requireNonNull(requireViewState, "null cannot be cast to non-null type com.discord.widgets.guildscheduledevent.PreviewGuildScheduledEventViewModel.ViewState.Initialized");
        updateViewState(ViewState.Initialized.copy$default((ViewState.Initialized) requireViewState, false, null, false, null, null, 30, null));
    }

    private final void startEvent(Context context, Function1<? super Long, Unit> function1) {
        WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData = this.existingEventData;
        if (existingEventData != null) {
            long guildScheduledEventId = existingEventData.getGuildScheduledEventId();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(GuildScheduledEventAPI.INSTANCE.startEvent(this.eventModel.getGuildId(), guildScheduledEventId), false, 1, null), this, null, 2, null), PreviewGuildScheduledEventViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new PreviewGuildScheduledEventViewModel$startEvent$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new PreviewGuildScheduledEventViewModel$startEvent$2(this, function1, guildScheduledEventId));
        }
    }

    private final void startStageEvent(Context context, boolean z2, Function1<? super Long, Unit> function1) {
        if (this.eventModel.getName() != null && this.eventModel.getChannelId() != null) {
            WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData = this.existingEventData;
            Long valueOf = existingEventData != null ? Long.valueOf(existingEventData.getGuildScheduledEventId()) : null;
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(StageChannelAPI.INSTANCE.startStageInstance(this.eventModel.getChannelId().longValue(), this.eventModel.getName(), StageInstancePrivacyLevel.GUILD_ONLY, z2, String.valueOf(valueOf)), false, 1, null), this, null, 2, null), PreviewGuildScheduledEventViewModel.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new PreviewGuildScheduledEventViewModel$startStageEvent$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new PreviewGuildScheduledEventViewModel$startStageEvent$2(this, function1, valueOf));
        }
    }

    public final ViewState getCurrentViewState() {
        return this.currentViewState;
    }

    public final boolean isCreate() {
        return this.existingEventData == null;
    }

    public final void onBottomButtonClicked(Context context, boolean z2, Function1<? super Long, Unit> function1) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(function1, "onRequestSuccess");
        ViewState viewState = this.currentViewState;
        Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.guildscheduledevent.PreviewGuildScheduledEventViewModel.ViewState.Initialized");
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (!initialized.getRequestProcessing()) {
            updateViewState(ViewState.Initialized.copy$default(initialized, true, null, false, null, null, 30, null));
            WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData = this.existingEventData;
            if (existingEventData == null) {
                createEvent(context, function1);
            } else if (existingEventData.getAction() == WidgetPreviewGuildScheduledEvent.Companion.Action.EDIT_EVENT) {
                editEvent(context, function1);
            } else if (this.eventModel.getEntityType().ordinal() != 1) {
                startEvent(context, function1);
            } else {
                startStageEvent(context, z2, function1);
            }
        }
    }

    public final void setCurrentViewState(ViewState viewState) {
        m.checkNotNullParameter(viewState, "<set-?>");
        this.currentViewState = viewState;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PreviewGuildScheduledEventViewModel(GuildScheduledEventModel guildScheduledEventModel, WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData, StoreChannels storeChannels, StorePermissions storePermissions, StoreGuildScheduledEvents storeGuildScheduledEvents) {
        super(ViewState.Initial.INSTANCE);
        m.checkNotNullParameter(guildScheduledEventModel, "eventModel");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventsStore");
        this.eventModel = guildScheduledEventModel;
        this.existingEventData = existingEventData;
        this.channelsStore = storeChannels;
        this.permissionsStore = storePermissions;
        this.guildScheduledEventsStore = storeGuildScheduledEvents;
        WidgetPreviewGuildScheduledEvent.Companion.Action action = null;
        boolean z2 = (existingEventData != null ? existingEventData.getAction() : null) == WidgetPreviewGuildScheduledEvent.Companion.Action.START_EVENT;
        Long channelId = guildScheduledEventModel.getChannelId();
        Channel channel = channelId != null ? storeChannels.getChannel(channelId.longValue()) : null;
        ViewState.Initialized initialized = new ViewState.Initialized(false, existingEventData != null ? existingEventData.getAction() : action, z2 && canNotifyEveryone(channel), GuildScheduledEventLocationInfo.Companion.buildLocationInfo(guildScheduledEventModel, channel), guildScheduledEventModel);
        this.currentViewState = initialized;
        updateViewState(initialized);
    }
}
