package com.discord.widgets.guildscheduledevent;

import androidx.core.app.NotificationCompat;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventSettingsViewModel;
import d0.o;
import kotlin.Metadata;
import kotlin.Pair;
import rx.functions.Func2;
/* compiled from: WidgetGuildScheduledEventSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001aB\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003 \u0001* \u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00050\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;", "kotlin.jvm.PlatformType", "settingsState", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationState;", "hubState", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDirectoryAssociationState;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventSettings$onResume$2<T1, T2, R> implements Func2<GuildScheduledEventSettingsViewModel.ViewState, GuildScheduledEventDirectoryAssociationState, Pair<? extends GuildScheduledEventSettingsViewModel.ViewState, ? extends GuildScheduledEventDirectoryAssociationState>> {
    public static final WidgetGuildScheduledEventSettings$onResume$2 INSTANCE = new WidgetGuildScheduledEventSettings$onResume$2();

    public final Pair<GuildScheduledEventSettingsViewModel.ViewState, GuildScheduledEventDirectoryAssociationState> call(GuildScheduledEventSettingsViewModel.ViewState viewState, GuildScheduledEventDirectoryAssociationState guildScheduledEventDirectoryAssociationState) {
        return o.to(viewState, guildScheduledEventDirectoryAssociationState);
    }
}
