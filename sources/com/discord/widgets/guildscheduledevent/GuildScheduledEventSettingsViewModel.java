package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityMetadata;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreStream;
import com.discord.utilities.time.ClockFactory;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildScheduledEventSettingsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 @2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003@ABBO\u0012\n\u00104\u001a\u000602j\u0002`3\u0012\u000e\u00106\u001a\n\u0018\u000102j\u0004\u0018\u0001`5\u0012\u0006\u00108\u001a\u000207\u0012\u000e\u0010:\u001a\n\u0018\u000102j\u0004\u0018\u0001`9\u0012\b\u0010;\u001a\u0004\u0018\u00010\b\u0012\b\b\u0002\u0010=\u001a\u00020<¢\u0006\u0004\b>\u0010?J%\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u0002¢\u0006\u0004\b\u0005\u0010\u0007J\u0017\u0010\n\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\f¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0015\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0014\u001a\u00020\f¢\u0006\u0004\b\u0015\u0010\u0016J'\u0010\u0017\u001a\u0004\u0018\u00010\u00102\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\f¢\u0006\u0004\b\u0017\u0010\u0012J\u001f\u0010\u0018\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0014\u001a\u00020\f¢\u0006\u0004\b\u0018\u0010\u0016J\u0017\u0010\u001a\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0019\u001a\u00020\b¢\u0006\u0004\b\u001a\u0010\u000bJ\u0017\u0010\u001d\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ!\u0010#\u001a\u00020\u001b2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\b\u0010\"\u001a\u0004\u0018\u00010!¢\u0006\u0004\b#\u0010$J1\u0010)\u001a\u00020\u001b2\u0006\u0010%\u001a\u00020\u001f2\u0006\u0010&\u001a\u00020!2\b\u0010'\u001a\u0004\u0018\u00010\u001f2\b\u0010(\u001a\u0004\u0018\u00010!¢\u0006\u0004\b)\u0010*J\u0015\u0010-\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020+¢\u0006\u0004\b-\u0010.J\u0015\u0010/\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020+¢\u0006\u0004\b/\u0010.J\u0015\u0010)\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020+¢\u0006\u0004\b)\u0010.J\r\u00100\u001a\u00020\u001b¢\u0006\u0004\b0\u00101¨\u0006C"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;", "Lkotlin/Function1;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "updateEventModel", "", "(Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;", "", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "setTopic", "(Ljava/lang/String;)Lkotlin/Unit;", "", "year", "month", "dayOfMonth", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$DateError;", "setStartDate", "(III)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$DateError;", "hourOfDay", "minute", "setStartTime", "(II)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$DateError;", "setEndDate", "setEndTime", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "setDescription", "", "associateToHubs", "toggleBroadcastToDirectoryChannel", "(Z)Lkotlin/Unit;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;", "date", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;", "time", "isDateInFuture", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;)Z", "startDate", "startTime", "endDate", "endTime", "isStartDateBeforeEndDate", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerDate;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventPickerTime;)Z", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;", "viewState", "hasStartTimeChanged", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;)Z", "hasEndTimeChanged", "isNextButtonEnabled", "()Z", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/GuildScheduledEventId;", "existingGuildScheduledEventId", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "entityType", "Lcom/discord/primitives/ChannelId;", "channelId", "externalLocation", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", HookHelper.constructorName, "(JLjava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/stores/StoreGuildScheduledEvents;)V", "Companion", "DateError", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventSettingsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    public static final int SAMPLE_USER_COUNT = 1;

    /* compiled from: GuildScheduledEventSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$Companion;", "", "", "SAMPLE_USER_COUNT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildScheduledEventSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$DateError;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "INVALID_VIEW_STATE", "START_DATE_IN_PAST", "END_DATE_IN_PAST", "END_DATE_BEFORE_START_DATE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum DateError {
        INVALID_VIEW_STATE,
        START_DATE_IN_PAST,
        END_DATE_IN_PAST,
        END_DATE_BEFORE_START_DATE
    }

    /* compiled from: GuildScheduledEventSettingsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Initialized", "Invalid", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: GuildScheduledEventSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "component1", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component2", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "eventModel", "existingEvent", "copy", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Initialized;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getExistingEvent", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "getEventModel", HookHelper.constructorName, "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Initialized extends ViewState {
            private final GuildScheduledEventModel eventModel;
            private final GuildScheduledEvent existingEvent;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Initialized(GuildScheduledEventModel guildScheduledEventModel, GuildScheduledEvent guildScheduledEvent) {
                super(null);
                m.checkNotNullParameter(guildScheduledEventModel, "eventModel");
                this.eventModel = guildScheduledEventModel;
                this.existingEvent = guildScheduledEvent;
            }

            public static /* synthetic */ Initialized copy$default(Initialized initialized, GuildScheduledEventModel guildScheduledEventModel, GuildScheduledEvent guildScheduledEvent, int i, Object obj) {
                if ((i & 1) != 0) {
                    guildScheduledEventModel = initialized.eventModel;
                }
                if ((i & 2) != 0) {
                    guildScheduledEvent = initialized.existingEvent;
                }
                return initialized.copy(guildScheduledEventModel, guildScheduledEvent);
            }

            public final GuildScheduledEventModel component1() {
                return this.eventModel;
            }

            public final GuildScheduledEvent component2() {
                return this.existingEvent;
            }

            public final Initialized copy(GuildScheduledEventModel guildScheduledEventModel, GuildScheduledEvent guildScheduledEvent) {
                m.checkNotNullParameter(guildScheduledEventModel, "eventModel");
                return new Initialized(guildScheduledEventModel, guildScheduledEvent);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Initialized)) {
                    return false;
                }
                Initialized initialized = (Initialized) obj;
                return m.areEqual(this.eventModel, initialized.eventModel) && m.areEqual(this.existingEvent, initialized.existingEvent);
            }

            public final GuildScheduledEventModel getEventModel() {
                return this.eventModel;
            }

            public final GuildScheduledEvent getExistingEvent() {
                return this.existingEvent;
            }

            public int hashCode() {
                GuildScheduledEventModel guildScheduledEventModel = this.eventModel;
                int i = 0;
                int hashCode = (guildScheduledEventModel != null ? guildScheduledEventModel.hashCode() : 0) * 31;
                GuildScheduledEvent guildScheduledEvent = this.existingEvent;
                if (guildScheduledEvent != null) {
                    i = guildScheduledEvent.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Initialized(eventModel=");
                R.append(this.eventModel);
                R.append(", existingEvent=");
                R.append(this.existingEvent);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: GuildScheduledEventSettingsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState$Invalid;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventSettingsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ GuildScheduledEventSettingsViewModel(long j, Long l, GuildScheduledEventEntityType guildScheduledEventEntityType, Long l2, String str, StoreGuildScheduledEvents storeGuildScheduledEvents, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, l, guildScheduledEventEntityType, l2, str, (i & 32) != 0 ? StoreStream.Companion.getGuildScheduledEvents() : storeGuildScheduledEvents);
    }

    private final Unit updateEventModel(Function1<? super GuildScheduledEventModel, GuildScheduledEventModel> function1) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Initialized)) {
            viewState = null;
        }
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (initialized == null) {
            return null;
        }
        updateViewState(ViewState.Initialized.copy$default(initialized, function1.invoke(initialized.getEventModel()), null, 2, null));
        return Unit.a;
    }

    public final boolean hasEndTimeChanged(ViewState.Initialized initialized) {
        m.checkNotNullParameter(initialized, "viewState");
        if (initialized.getExistingEvent() == null) {
            return true;
        }
        if (initialized.getEventModel().getEndDate() == null || initialized.getEventModel().getEndTime() == null) {
            return initialized.getExistingEvent().k() == null;
        }
        long millis = GuildScheduledEventPickerDateTime.INSTANCE.toMillis(initialized.getEventModel().getEndDate(), initialized.getEventModel().getEndTime());
        UtcDateTime k = initialized.getExistingEvent().k();
        Long valueOf = k != null ? Long.valueOf(k.g()) : null;
        return valueOf == null || millis != valueOf.longValue();
    }

    public final boolean hasStartTimeChanged(ViewState.Initialized initialized) {
        m.checkNotNullParameter(initialized, "viewState");
        return initialized.getExistingEvent() == null || GuildScheduledEventPickerDateTime.INSTANCE.toMillis(initialized.getEventModel().getStartDate(), initialized.getEventModel().getStartTime()) != initialized.getExistingEvent().l().g();
    }

    public final boolean isDateInFuture(GuildScheduledEventPickerDate guildScheduledEventPickerDate, GuildScheduledEventPickerTime guildScheduledEventPickerTime) {
        return guildScheduledEventPickerDate == null || guildScheduledEventPickerTime == null || GuildScheduledEventPickerDateTime.INSTANCE.toMillis(guildScheduledEventPickerDate, guildScheduledEventPickerTime) >= ClockFactory.get().currentTimeMillis();
    }

    public final boolean isNextButtonEnabled() {
        GuildScheduledEventModel eventModel;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Initialized)) {
            viewState = null;
        }
        ViewState.Initialized initialized = (ViewState.Initialized) viewState;
        if (initialized == null || (eventModel = initialized.getEventModel()) == null) {
            return false;
        }
        String name = eventModel.getName();
        if (name == null || name.length() == 0) {
            return false;
        }
        return (eventModel.getEntityType() == GuildScheduledEventEntityType.EXTERNAL && (eventModel.getEndDate() == null || eventModel.getEndTime() == null)) ? false : true;
    }

    public final boolean isStartDateBeforeEndDate(GuildScheduledEventPickerDate guildScheduledEventPickerDate, GuildScheduledEventPickerTime guildScheduledEventPickerTime, GuildScheduledEventPickerDate guildScheduledEventPickerDate2, GuildScheduledEventPickerTime guildScheduledEventPickerTime2) {
        m.checkNotNullParameter(guildScheduledEventPickerDate, "startDate");
        m.checkNotNullParameter(guildScheduledEventPickerTime, "startTime");
        if (guildScheduledEventPickerDate2 == null || guildScheduledEventPickerTime2 == null) {
            return guildScheduledEventPickerDate2 == null || guildScheduledEventPickerDate2.toMillis() >= guildScheduledEventPickerDate.toMillis();
        }
        GuildScheduledEventPickerDateTime guildScheduledEventPickerDateTime = GuildScheduledEventPickerDateTime.INSTANCE;
        return guildScheduledEventPickerDateTime.toMillis(guildScheduledEventPickerDate2, guildScheduledEventPickerTime2) > guildScheduledEventPickerDateTime.toMillis(guildScheduledEventPickerDate, guildScheduledEventPickerTime);
    }

    public final Unit setDescription(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        return updateEventModel(new GuildScheduledEventSettingsViewModel$setDescription$1(str));
    }

    public final DateError setEndDate(int i, int i2, int i3) {
        if (updateEventModel(new GuildScheduledEventSettingsViewModel$setEndDate$1(new GuildScheduledEventPickerDate(i, i2, i3))) != null) {
            return null;
        }
        return DateError.INVALID_VIEW_STATE;
    }

    public final DateError setEndTime(int i, int i2) {
        if (updateEventModel(new GuildScheduledEventSettingsViewModel$setEndTime$1(new GuildScheduledEventPickerTime(i, i2, 0))) != null) {
            return null;
        }
        return DateError.INVALID_VIEW_STATE;
    }

    public final DateError setStartDate(int i, int i2, int i3) {
        if (updateEventModel(new GuildScheduledEventSettingsViewModel$setStartDate$1(new GuildScheduledEventPickerDate(i, i2, i3))) != null) {
            return null;
        }
        return DateError.INVALID_VIEW_STATE;
    }

    public final DateError setStartTime(int i, int i2) {
        if (updateEventModel(new GuildScheduledEventSettingsViewModel$setStartTime$1(new GuildScheduledEventPickerTime(i, i2, 0))) != null) {
            return null;
        }
        return DateError.INVALID_VIEW_STATE;
    }

    public final Unit setTopic(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_TOPIC);
        return updateEventModel(new GuildScheduledEventSettingsViewModel$setTopic$1(str));
    }

    public final Unit toggleBroadcastToDirectoryChannel(boolean z2) {
        return updateEventModel(new GuildScheduledEventSettingsViewModel$toggleBroadcastToDirectoryChannel$1(z2));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventSettingsViewModel(long j, Long l, GuildScheduledEventEntityType guildScheduledEventEntityType, Long l2, String str, StoreGuildScheduledEvents storeGuildScheduledEvents) {
        super(null, 1, null);
        GuildScheduledEvent guildScheduledEvent;
        int i;
        GuildScheduledEventEntityType guildScheduledEventEntityType2;
        ViewState.Initialized initialized;
        ViewState.Initialized initialized2;
        GuildScheduledEventModel model;
        m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventsStore");
        GuildScheduledEventModel guildScheduledEventModel = null;
        GuildScheduledEvent findEventFromStore = l != null ? storeGuildScheduledEvents.findEventFromStore(l.longValue(), Long.valueOf(j)) : null;
        if (findEventFromStore == null || (model = GuildScheduledEventModelKt.toModel(findEventFromStore)) == null) {
            guildScheduledEvent = findEventFromStore;
            guildScheduledEventEntityType2 = guildScheduledEventEntityType;
            i = 1;
        } else {
            i = 1;
            guildScheduledEvent = findEventFromStore;
            guildScheduledEventEntityType2 = guildScheduledEventEntityType;
            guildScheduledEventModel = model.copy((r30 & 1) != 0 ? model.guildId : 0L, (r30 & 2) != 0 ? model.name : null, (r30 & 4) != 0 ? model.channelId : l2, (r30 & 8) != 0 ? model.creatorId : null, (r30 & 16) != 0 ? model.startDate : null, (r30 & 32) != 0 ? model.startTime : null, (r30 & 64) != 0 ? model.endDate : null, (r30 & 128) != 0 ? model.endTime : null, (r30 & 256) != 0 ? model.description : null, (r30 & 512) != 0 ? model.entityType : guildScheduledEventEntityType, (r30 & 1024) != 0 ? model.entityMetadata : GuildScheduledEventEntityMetadata.Companion.a(guildScheduledEventEntityType, str), (r30 & 2048) != 0 ? model.userCount : null, (r30 & 4096) != 0 ? model.broadcastToDirectoryChannels : null);
        }
        Pair<GuildScheduledEventPickerDate, GuildScheduledEventPickerTime> generateAppropriateStartDateTime = GuildScheduledEventPickerDateTime.INSTANCE.generateAppropriateStartDateTime();
        if (guildScheduledEventModel != null) {
            initialized2 = initialized;
        } else {
            initialized2 = initialized;
            guildScheduledEventModel = new GuildScheduledEventModel(j, null, l2, null, generateAppropriateStartDateTime.getFirst(), generateAppropriateStartDateTime.getSecond(), null, null, null, guildScheduledEventEntityType, GuildScheduledEventEntityMetadata.Companion.a(guildScheduledEventEntityType2, str), Integer.valueOf(i), null, 4096, null);
        }
        updateViewState(new ViewState.Initialized(guildScheduledEventModel, guildScheduledEvent));
    }

    public final boolean isStartDateBeforeEndDate(ViewState.Initialized initialized) {
        m.checkNotNullParameter(initialized, "viewState");
        return isStartDateBeforeEndDate(initialized.getEventModel().getStartDate(), initialized.getEventModel().getStartTime(), initialized.getEventModel().getEndDate(), initialized.getEventModel().getEndTime());
    }
}
