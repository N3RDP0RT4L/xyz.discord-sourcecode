package com.discord.widgets.guildscheduledevent;

import com.discord.api.stageinstance.StageInstance;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: PreviewGuildScheduledEventViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/stageinstance/StageInstance;", "it", "", "invoke", "(Lcom/discord/api/stageinstance/StageInstance;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PreviewGuildScheduledEventViewModel$startStageEvent$2 extends o implements Function1<StageInstance, Unit> {
    public final /* synthetic */ Long $eventId;
    public final /* synthetic */ Function1 $onRequestSuccess;
    public final /* synthetic */ PreviewGuildScheduledEventViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PreviewGuildScheduledEventViewModel$startStageEvent$2(PreviewGuildScheduledEventViewModel previewGuildScheduledEventViewModel, Function1 function1, Long l) {
        super(1);
        this.this$0 = previewGuildScheduledEventViewModel;
        this.$onRequestSuccess = function1;
        this.$eventId = l;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StageInstance stageInstance) {
        invoke2(stageInstance);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StageInstance stageInstance) {
        m.checkNotNullParameter(stageInstance, "it");
        this.this$0.setRequestFinished();
        this.$onRequestSuccess.invoke(this.$eventId);
    }
}
