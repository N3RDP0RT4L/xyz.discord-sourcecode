package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetGuildScheduledEventDetailsBottomSheetBinding;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.directories.DirectoryUtils;
import com.discord.utilities.extensions.SimpleDraweeViewExtensionsKt;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.segmentedcontrol.CardSegment;
import com.discord.views.segmentedcontrol.SegmentedControlContainer;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet;
import com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent;
import com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration;
import com.discord.widgets.guildscheduledevent.buttonconfiguration.DirectoryButtonConfiguration;
import com.discord.widgets.guildscheduledevent.buttonconfiguration.GuildButtonConfiguration;
import com.discord.widgets.home.WidgetHome;
import com.discord.widgets.voice.VoiceUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g;
import d0.t.m;
import d0.z.d.a0;
import d0.z.d.o;
import java.lang.ref.WeakReference;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetGuildScheduledEventDetailsBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 G2\u00020\u0001:\u0001GB\u0007¢\u0006\u0004\bF\u0010\"J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0013\u0010\u0017\u001a\u00020\u0016*\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ!\u0010\u001f\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u0006H\u0016¢\u0006\u0004\b!\u0010\"R\u001d\u0010(\u001a\u00020#8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b*\u0010+R\u001d\u00101\u001a\u00020,8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R$\u00105\u001a\u0010\u0012\f\u0012\n 4*\u0004\u0018\u00010303028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R\u001d\u0010;\u001a\u0002078F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b8\u0010.\u001a\u0004\b9\u0010:R\u001d\u0010@\u001a\u00020<8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b=\u0010.\u001a\u0004\b>\u0010?R\u001d\u0010E\u001a\u00020A8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bB\u0010.\u001a\u0004\bC\u0010D¨\u0006H"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventDetailsBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "", "segmentIndex", "", "hide", "", "configureSegmentControl", "(IZ)V", "Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;", "rsvpUsersFetchState", "configureForRsvpUsersFetchState", "(Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;)V", "Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;", "section", "configureForSection", "(Lcom/discord/widgets/guildscheduledevent/EventDetailsSection;Lcom/discord/widgets/guildscheduledevent/EventDetailsRsvpUsersFetchState;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;", "viewState", "configureUi", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState$Initialized;", "Lcom/discord/widgets/guildscheduledevent/buttonconfiguration/ButtonConfiguration;", "getButtonConfiguration", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState$Initialized;)Lcom/discord/widgets/guildscheduledevent/buttonconfiguration/ButtonConfiguration;", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "()V", "Lcom/discord/databinding/WidgetGuildScheduledEventDetailsBottomSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildScheduledEventDetailsBottomSheetBinding;", "binding", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListAdapter;", "adapter", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventRsvpUserListAdapter;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel;", "viewModel", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "previewLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser;", "descriptionParser$delegate", "getDescriptionParser", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser;", "descriptionParser", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args$delegate", "getArgs", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventExternalLocationParser;", "locationParser$delegate", "getLocationParser", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventExternalLocationParser;", "locationParser", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventDetailsBottomSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildScheduledEventDetailsBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildScheduledEventDetailsBottomSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private GuildScheduledEventRsvpUserListAdapter adapter;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildScheduledEventDetailsBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy descriptionParser$delegate = g.lazy(new WidgetGuildScheduledEventDetailsBottomSheet$descriptionParser$2(this));
    private final Lazy locationParser$delegate = g.lazy(WidgetGuildScheduledEventDetailsBottomSheet$locationParser$2.INSTANCE);
    private final Lazy args$delegate = g.lazy(new WidgetGuildScheduledEventDetailsBottomSheet$$special$$inlined$args$1(this, "intent_args_key"));
    private final ActivityResultLauncher<Intent> previewLauncher = WidgetPreviewGuildScheduledEvent.Companion.createJoinOnStartActivityRegistration(this, new WidgetGuildScheduledEventDetailsBottomSheet$previewLauncher$1(this));

    /* compiled from: WidgetGuildScheduledEventDetailsBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0019\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\n\u0010\u000bJ9\u0010\u0010\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\r\u001a\u00060\u0002j\u0002`\f2\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u000e¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0014\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventDetailsBottomSheet$Companion;", "", "", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "", "enqueue", "(J)V", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "showForGuild", "(Landroidx/fragment/app/FragmentManager;J)V", "Lcom/discord/primitives/GuildId;", "hubGuildId", "Lcom/discord/primitives/ChannelId;", "directoryChannelId", "showForDirectory", "(Landroidx/fragment/app/FragmentManager;JJJ)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;)V", "Landroid/content/Context;", "context", "handleInvalidEvent", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void enqueue(long j) {
            StoreNotices notices = StoreStream.Companion.getNotices();
            String s2 = a.s("EVENTDETAILS-", j);
            notices.requestToShow(new StoreNotices.Notice(s2, null, 0L, 0, false, m.listOf(a0.getOrCreateKotlinClass(WidgetHome.class)), 0L, false, 0L, new WidgetGuildScheduledEventDetailsBottomSheet$Companion$enqueue$guildScheduledEventDetailsNotice$1(j, notices, s2), 150, null));
        }

        public final void handleInvalidEvent(Context context) {
            d0.z.d.m.checkNotNullParameter(context, "context");
            b.a.d.m.g(context, R.string.inaccessible_channel_link_title, 0, null, 12);
        }

        public final void show(FragmentManager fragmentManager, GuildScheduledEventDetailsArgs guildScheduledEventDetailsArgs) {
            d0.z.d.m.checkNotNullParameter(fragmentManager, "fragmentManager");
            d0.z.d.m.checkNotNullParameter(guildScheduledEventDetailsArgs, "args");
            WidgetGuildScheduledEventDetailsBottomSheet widgetGuildScheduledEventDetailsBottomSheet = new WidgetGuildScheduledEventDetailsBottomSheet();
            widgetGuildScheduledEventDetailsBottomSheet.setArguments(d.e2(guildScheduledEventDetailsArgs));
            widgetGuildScheduledEventDetailsBottomSheet.show(fragmentManager, WidgetGuildScheduledEventDetailsBottomSheet.class.getName());
        }

        public final void showForDirectory(FragmentManager fragmentManager, long j, long j2, long j3) {
            d0.z.d.m.checkNotNullParameter(fragmentManager, "fragmentManager");
            show(fragmentManager, new GuildScheduledEventDetailsArgs(j, Long.valueOf(j2), Long.valueOf(j3), GuildScheduledEventDetailsSource.Directory));
        }

        public final void showForGuild(FragmentManager fragmentManager, long j) {
            d0.z.d.m.checkNotNullParameter(fragmentManager, "fragmentManager");
            show(fragmentManager, new GuildScheduledEventDetailsArgs(j, null, null, null, 14, null));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            EventDetailsRsvpUsersFetchState.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[EventDetailsRsvpUsersFetchState.LOADING.ordinal()] = 1;
            iArr[EventDetailsRsvpUsersFetchState.EMPTY.ordinal()] = 2;
            iArr[EventDetailsRsvpUsersFetchState.ERROR.ordinal()] = 3;
            iArr[EventDetailsRsvpUsersFetchState.SUCCESS.ordinal()] = 4;
        }
    }

    public WidgetGuildScheduledEventDetailsBottomSheet() {
        super(false, 1, null);
        WidgetGuildScheduledEventDetailsBottomSheet$viewModel$2 widgetGuildScheduledEventDetailsBottomSheet$viewModel$2 = new WidgetGuildScheduledEventDetailsBottomSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildScheduledEventDetailsViewModel.class), new WidgetGuildScheduledEventDetailsBottomSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildScheduledEventDetailsBottomSheet$viewModel$2));
    }

    private final void configureForRsvpUsersFetchState(EventDetailsRsvpUsersFetchState eventDetailsRsvpUsersFetchState) {
        ProgressBar progressBar = getBinding().f2421s;
        d0.z.d.m.checkNotNullExpressionValue(progressBar, "binding.loadingIndicator");
        progressBar.setVisibility(8);
        ConstraintLayout constraintLayout = getBinding().n;
        d0.z.d.m.checkNotNullExpressionValue(constraintLayout, "binding.guildScheduledEventEmptyOrErrorContainer");
        constraintLayout.setVisibility(8);
        RecyclerView recyclerView = getBinding().r;
        d0.z.d.m.checkNotNullExpressionValue(recyclerView, "binding.guildScheduledEventRsvpListRecyclerView");
        recyclerView.setVisibility(8);
        int ordinal = eventDetailsRsvpUsersFetchState.ordinal();
        if (ordinal == 0) {
            ProgressBar progressBar2 = getBinding().f2421s;
            d0.z.d.m.checkNotNullExpressionValue(progressBar2, "binding.loadingIndicator");
            progressBar2.setVisibility(0);
        } else if (ordinal == 1) {
            ConstraintLayout constraintLayout2 = getBinding().n;
            d0.z.d.m.checkNotNullExpressionValue(constraintLayout2, "binding.guildScheduledEventEmptyOrErrorContainer");
            constraintLayout2.setVisibility(0);
            TextView textView = getBinding().o;
            d0.z.d.m.checkNotNullExpressionValue(textView, "binding.guildScheduledEventEmptyOrErrorTitle");
            b.m(textView, R.string.guild_event_details_user_list_fetch_error, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        } else if (ordinal == 2) {
            ConstraintLayout constraintLayout3 = getBinding().n;
            d0.z.d.m.checkNotNullExpressionValue(constraintLayout3, "binding.guildScheduledEventEmptyOrErrorContainer");
            constraintLayout3.setVisibility(0);
            TextView textView2 = getBinding().o;
            d0.z.d.m.checkNotNullExpressionValue(textView2, "binding.guildScheduledEventEmptyOrErrorTitle");
            b.m(textView2, R.string.guild_event_details_user_list_empty_title, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        } else if (ordinal == 3) {
            RecyclerView recyclerView2 = getBinding().r;
            d0.z.d.m.checkNotNullExpressionValue(recyclerView2, "binding.guildScheduledEventRsvpListRecyclerView");
            recyclerView2.setVisibility(0);
        }
    }

    private final void configureForSection(EventDetailsSection eventDetailsSection, EventDetailsRsvpUsersFetchState eventDetailsRsvpUsersFetchState) {
        EventDetailsSection eventDetailsSection2 = EventDetailsSection.RSVP_LIST;
        if (eventDetailsSection == eventDetailsSection2) {
            configureForRsvpUsersFetchState(eventDetailsRsvpUsersFetchState);
        }
        ConstraintLayout constraintLayout = getBinding().p;
        d0.z.d.m.checkNotNullExpressionValue(constraintLayout, "binding.guildScheduledEventInfoContainer");
        boolean z2 = true;
        int i = 0;
        constraintLayout.setVisibility(eventDetailsSection == EventDetailsSection.EVENT_INFO ? 0 : 8);
        ConstraintLayout constraintLayout2 = getBinding().q;
        d0.z.d.m.checkNotNullExpressionValue(constraintLayout2, "binding.guildScheduledEventRsvpListContainer");
        if (eventDetailsSection != eventDetailsSection2) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        constraintLayout2.setVisibility(i);
    }

    private final void configureSegmentControl(int i, boolean z2) {
        CharSequence b2;
        CharSequence b3;
        SegmentedControlContainer segmentedControlContainer = getBinding().t;
        d0.z.d.m.checkNotNullExpressionValue(segmentedControlContainer, "binding.segmentedControl");
        segmentedControlContainer.setVisibility(z2 ^ true ? 0 : 8);
        if (!z2) {
            SegmentedControlContainer.b(getBinding().t, 0, 1);
            getBinding().t.setOnSegmentSelectedChangeListener(new WidgetGuildScheduledEventDetailsBottomSheet$configureSegmentControl$1(this));
            getBinding().t.setSelectedIndex(i);
            CardSegment cardSegment = getBinding().u;
            Context requireContext = requireContext();
            d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
            b2 = b.b(requireContext, R.string.guild_event_details_info_tab_title, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            cardSegment.setText(b2);
            CardSegment cardSegment2 = getBinding().v;
            Context requireContext2 = requireContext();
            d0.z.d.m.checkNotNullExpressionValue(requireContext2, "requireContext()");
            b3 = b.b(requireContext2, R.string.indicate_rsvp, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            cardSegment2.setText(b3);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(GuildScheduledEventDetailsViewModel.ViewState viewState) {
        CharSequence charSequence;
        if (!(viewState instanceof GuildScheduledEventDetailsViewModel.ViewState.Initialized)) {
            dismiss();
            return;
        }
        GuildScheduledEventDetailsViewModel.ViewState.Initialized initialized = (GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState;
        configureSegmentControl(initialized.getSegmentedControlIndex(), !initialized.isInGuild());
        configureForSection(initialized.getSection(), initialized.getRsvpUsersFetchState());
        getBinding().g.configure(initialized.getGuildScheduledEvent(), false);
        TextView textView = getBinding().m;
        d0.z.d.m.checkNotNullExpressionValue(textView, "binding.guildScheduledEventDetailsTitleText");
        textView.setText(initialized.getGuildScheduledEvent().j());
        SimpleDraweeView simpleDraweeView = getBinding().i;
        d0.z.d.m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildScheduledEventDetailsGuildIcon");
        IconUtils.setIcon$default((ImageView) simpleDraweeView, initialized.getGuild(), 0, (MGImages.ChangeDetector) null, false, 28, (Object) null);
        TextView textView2 = getBinding().j;
        d0.z.d.m.checkNotNullExpressionValue(textView2, "binding.guildScheduledEventDetailsGuildName");
        textView2.setText(initialized.getGuild().getName());
        GuildScheduledEventLocationInfo locationInfo = initialized.getLocationInfo();
        if (locationInfo != null) {
            getBinding().c.setImageResource(locationInfo.getLocationIcon());
        }
        ImageView imageView = getBinding().c;
        d0.z.d.m.checkNotNullExpressionValue(imageView, "binding.guildScheduledEventDetailsChannelIcon");
        imageView.setVisibility(initialized.getLocationInfo() != null ? 0 : 8);
        LinkifiedTextView linkifiedTextView = getBinding().d;
        d0.z.d.m.checkNotNullExpressionValue(linkifiedTextView, "binding.guildScheduledEventDetailsChannelName");
        GuildScheduledEventLocationInfo locationInfo2 = initialized.getLocationInfo();
        CharSequence charSequence2 = null;
        if (locationInfo2 != null) {
            GuildScheduledEventExternalLocationParser locationParser = getLocationParser();
            Context requireContext = requireContext();
            d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
            charSequence = locationParser.getTextFromLocation(requireContext, locationInfo2);
        } else {
            charSequence = null;
        }
        ViewExtensions.setTextAndVisibilityBy(linkifiedTextView, charSequence);
        ImageView imageView2 = getBinding().l;
        d0.z.d.m.checkNotNullExpressionValue(imageView2, "binding.guildScheduledEventDetailsRsvpIcon");
        imageView2.setVisibility(initialized.getGuildScheduledEvent().n() != null ? 0 : 8);
        TextView textView3 = getBinding().k;
        d0.z.d.m.checkNotNullExpressionValue(textView3, "binding.guildScheduledEventDetailsRsvpCount");
        Integer n = initialized.getGuildScheduledEvent().n();
        if (n != null) {
            int intValue = n.intValue();
            Context requireContext2 = requireContext();
            d0.z.d.m.checkNotNullExpressionValue(requireContext2, "requireContext()");
            Context requireContext3 = requireContext();
            d0.z.d.m.checkNotNullExpressionValue(requireContext3, "requireContext()");
            charSequence2 = b.b(requireContext2, R.string.guild_event_interested_count, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext3, R.plurals.guild_event_interested_count_count, intValue, Integer.valueOf(intValue))}, (r4 & 4) != 0 ? b.C0034b.j : null);
        }
        ViewExtensions.setTextAndVisibilityBy(textView3, charSequence2);
        UserGuildMember creator = initialized.getCreator();
        if (creator != null) {
            TextView textView4 = getBinding().e;
            d0.z.d.m.checkNotNullExpressionValue(textView4, "binding.guildScheduledEventDetailsCreatedBy");
            textView4.setVisibility(0);
            SimpleDraweeView simpleDraweeView2 = getBinding().f;
            d0.z.d.m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildScheduledEventDetailsCreatorAvatar");
            simpleDraweeView2.setVisibility(0);
            TextView textView5 = getBinding().e;
            d0.z.d.m.checkNotNullExpressionValue(textView5, "binding.guildScheduledEventDetailsCreatedBy");
            b.m(textView5, R.string.guild_event_created_by_hook, new Object[]{creator.getNickOrUserName()}, new WidgetGuildScheduledEventDetailsBottomSheet$configureUi$4(this, creator));
            SimpleDraweeView simpleDraweeView3 = getBinding().f;
            d0.z.d.m.checkNotNullExpressionValue(simpleDraweeView3, "binding.guildScheduledEventDetailsCreatorAvatar");
            SimpleDraweeViewExtensionsKt.configureCreator(simpleDraweeView3, creator);
        } else {
            TextView textView6 = getBinding().e;
            d0.z.d.m.checkNotNullExpressionValue(textView6, "binding.guildScheduledEventDetailsCreatedBy");
            textView6.setVisibility(8);
            SimpleDraweeView simpleDraweeView4 = getBinding().f;
            d0.z.d.m.checkNotNullExpressionValue(simpleDraweeView4, "binding.guildScheduledEventDetailsCreatorAvatar");
            simpleDraweeView4.setVisibility(8);
        }
        getDescriptionParser().configureDescription(initialized.getGuildScheduledEvent().d(), initialized.getGuildScheduledEvent().h());
        GuildScheduledEventBottomButtonView guildScheduledEventBottomButtonView = getBinding().f2420b;
        d0.z.d.m.checkNotNullExpressionValue(guildScheduledEventBottomButtonView, "binding.guildScheduledEventDetailsButtonView");
        guildScheduledEventBottomButtonView.setVisibility(0);
        getBinding().f2420b.configureForDetails(getButtonConfiguration(initialized));
        getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$configureUi$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildScheduledEventDetailsViewModel viewModel;
                viewModel = WidgetGuildScheduledEventDetailsBottomSheet.this.getViewModel();
                viewModel.onGuildNameClicked();
                WidgetGuildScheduledEventDetailsBottomSheet.this.dismiss();
            }
        });
        GuildScheduledEventRsvpUserListAdapter guildScheduledEventRsvpUserListAdapter = this.adapter;
        if (guildScheduledEventRsvpUserListAdapter == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("adapter");
        }
        guildScheduledEventRsvpUserListAdapter.setData(initialized.getRsvpUsers());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildScheduledEventDetailsArgs getArgs() {
        return (GuildScheduledEventDetailsArgs) this.args$delegate.getValue();
    }

    private final ButtonConfiguration getButtonConfiguration(final GuildScheduledEventDetailsViewModel.ViewState.Initialized initialized) {
        if (getArgs().getSource() == GuildScheduledEventDetailsSource.Directory) {
            return new DirectoryButtonConfiguration(initialized.getGuildScheduledEvent(), initialized.isInGuild(), initialized.isRsvped(), true, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$1

                /* compiled from: WidgetGuildScheduledEventDetailsBottomSheet.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function0<Unit> {
                    public AnonymousClass1() {
                        super(0);
                    }

                    @Override // kotlin.jvm.functions.Function0
                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2() {
                        GuildScheduledEventDetailsViewModel viewModel;
                        viewModel = WidgetGuildScheduledEventDetailsBottomSheet.this.getViewModel();
                        viewModel.onRsvpButtonClicked();
                    }
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GuildScheduledEventDetailsArgs args;
                    GuildScheduledEventDetailsArgs args2;
                    DirectoryUtils directoryUtils = DirectoryUtils.INSTANCE;
                    WidgetGuildScheduledEventDetailsBottomSheet widgetGuildScheduledEventDetailsBottomSheet = WidgetGuildScheduledEventDetailsBottomSheet.this;
                    GuildScheduledEvent guildScheduledEvent = initialized.getGuildScheduledEvent();
                    boolean isInGuild = initialized.isInGuild();
                    args = WidgetGuildScheduledEventDetailsBottomSheet.this.getArgs();
                    Long guildId = args.getGuildId();
                    long j = 0;
                    long longValue = guildId != null ? guildId.longValue() : 0L;
                    args2 = WidgetGuildScheduledEventDetailsBottomSheet.this.getArgs();
                    Long channelId = args2.getChannelId();
                    if (channelId != null) {
                        j = channelId.longValue();
                    }
                    directoryUtils.maybeJoinAndGoToGuild(widgetGuildScheduledEventDetailsBottomSheet, guildScheduledEvent, isInGuild, longValue, j, !initialized.isRsvped() && GuildScheduledEventUtilitiesKt.canRsvp(initialized.getGuildScheduledEvent()), new AnonymousClass1());
                }
            }, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GuildScheduledEventDetailsViewModel viewModel;
                    viewModel = WidgetGuildScheduledEventDetailsBottomSheet.this.getViewModel();
                    viewModel.onRsvpButtonClicked();
                }
            }, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GuildScheduledEventDetailsViewModel viewModel;
                    viewModel = WidgetGuildScheduledEventDetailsBottomSheet.this.getViewModel();
                    viewModel.onShareButtonClicked(new WeakReference<>(WidgetGuildScheduledEventDetailsBottomSheet.this));
                }
            }, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GuildScheduledEventDetailsArgs args;
                    WidgetGuildScheduledEventDetailsBottomSheet.this.dismiss();
                    WidgetGuildScheduledEventDetailsExtrasBottomSheet.Companion companion = WidgetGuildScheduledEventDetailsExtrasBottomSheet.Companion;
                    FragmentManager parentFragmentManager = WidgetGuildScheduledEventDetailsBottomSheet.this.getParentFragmentManager();
                    d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    args = WidgetGuildScheduledEventDetailsBottomSheet.this.getArgs();
                    companion.show(parentFragmentManager, args);
                }
            });
        }
        GuildScheduledEvent guildScheduledEvent = initialized.getGuildScheduledEvent();
        boolean canStartEvent = initialized.getCanStartEvent();
        boolean canConnect = initialized.getCanConnect();
        boolean isConnected = initialized.isConnected();
        boolean isInGuild = initialized.isInGuild();
        boolean isRsvped = initialized.isRsvped();
        View.OnClickListener widgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$5 = new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ActivityResultLauncher activityResultLauncher;
                WidgetPreviewGuildScheduledEvent.Companion companion = WidgetPreviewGuildScheduledEvent.Companion;
                Context requireContext = WidgetGuildScheduledEventDetailsBottomSheet.this.requireContext();
                d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
                GuildScheduledEventModel model = GuildScheduledEventModelKt.toModel(initialized.getGuildScheduledEvent());
                WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData = new WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData(initialized.getGuildScheduledEvent().i(), WidgetPreviewGuildScheduledEvent.Companion.Action.START_EVENT);
                activityResultLauncher = WidgetGuildScheduledEventDetailsBottomSheet.this.previewLauncher;
                companion.launch(requireContext, model, (r13 & 4) != 0 ? null : existingEventData, (r13 & 8) != 0 ? null : activityResultLauncher, (r13 & 16) != 0 ? false : false);
            }
        };
        return new GuildButtonConfiguration(guildScheduledEvent, true, canStartEvent, isConnected, canConnect, isInGuild, isRsvped, true, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildScheduledEventDetailsViewModel viewModel;
                viewModel = WidgetGuildScheduledEventDetailsBottomSheet.this.getViewModel();
                viewModel.onRsvpButtonClicked();
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildScheduledEventDetailsBottomSheet.this.dismiss();
                Channel channel = initialized.getChannel();
                if (channel != null) {
                    VoiceUtils.handleCallChannel(channel, WidgetGuildScheduledEventDetailsBottomSheet.this);
                }
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$10

            /* compiled from: WidgetGuildScheduledEventDetailsBottomSheet.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$10$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function0<Unit> {
                public AnonymousClass1() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetGuildScheduledEventDetailsBottomSheet.this.dismiss();
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildScheduledEventDetailsViewModel viewModel;
                viewModel = WidgetGuildScheduledEventDetailsBottomSheet.this.getViewModel();
                Context requireContext = WidgetGuildScheduledEventDetailsBottomSheet.this.requireContext();
                d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
                viewModel.endEventClicked(requireContext, new AnonymousClass1());
            }
        }, widgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$5, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildScheduledEventDetailsViewModel viewModel;
                viewModel = WidgetGuildScheduledEventDetailsBottomSheet.this.getViewModel();
                viewModel.onShareButtonClicked(new WeakReference<>(WidgetGuildScheduledEventDetailsBottomSheet.this));
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet$getButtonConfiguration$8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildScheduledEventDetailsArgs args;
                WidgetGuildScheduledEventDetailsBottomSheet.this.dismiss();
                WidgetGuildScheduledEventDetailsExtrasBottomSheet.Companion companion = WidgetGuildScheduledEventDetailsExtrasBottomSheet.Companion;
                FragmentManager parentFragmentManager = WidgetGuildScheduledEventDetailsBottomSheet.this.getParentFragmentManager();
                d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                args = WidgetGuildScheduledEventDetailsBottomSheet.this.getArgs();
                companion.show(parentFragmentManager, args);
            }
        });
    }

    private final GuildScheduledEventExternalLocationParser getLocationParser() {
        return (GuildScheduledEventExternalLocationParser) this.locationParser$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildScheduledEventDetailsViewModel getViewModel() {
        return (GuildScheduledEventDetailsViewModel) this.viewModel$delegate.getValue();
    }

    public final WidgetGuildScheduledEventDetailsBottomSheetBinding getBinding() {
        return (WidgetGuildScheduledEventDetailsBottomSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_guild_scheduled_event_details_bottom_sheet;
    }

    public final GuildScheduledEventDescriptionParser getDescriptionParser() {
        return (GuildScheduledEventDescriptionParser) this.descriptionParser$delegate.getValue();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildScheduledEventDetailsBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildScheduledEventDetailsBottomSheet$onResume$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        d0.z.d.m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        RecyclerView recyclerView = getBinding().r;
        d0.z.d.m.checkNotNullExpressionValue(recyclerView, "binding.guildScheduledEventRsvpListRecyclerView");
        this.adapter = new GuildScheduledEventRsvpUserListAdapter(recyclerView, new WidgetGuildScheduledEventDetailsBottomSheet$onViewCreated$1(this));
        RecyclerView recyclerView2 = getBinding().r;
        GuildScheduledEventRsvpUserListAdapter guildScheduledEventRsvpUserListAdapter = this.adapter;
        if (guildScheduledEventRsvpUserListAdapter == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("adapter");
        }
        recyclerView2.setAdapter(guildScheduledEventRsvpUserListAdapter);
        recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext()));
    }
}
