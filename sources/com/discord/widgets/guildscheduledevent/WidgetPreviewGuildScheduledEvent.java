package com.discord.widgets.guildscheduledevent;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppFragment;
import com.discord.app.AppTransitionActivity;
import com.discord.databinding.WidgetPreviewGuildScheduledEventBinding;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.intent.IntentUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventLocationInfo;
import com.discord.widgets.guildscheduledevent.PreviewGuildScheduledEventViewModel;
import com.discord.widgets.voice.VoiceUtils;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetPreviewGuildScheduledEvent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 #2\u00020\u0001:\u0001#B\u0007¢\u0006\u0004\b\"\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\tJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0016\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010!\u001a\u00020\u001d8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u0013\u001a\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;", "viewState", "", "configureUi", "(Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState;)V", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Initialized;", "configureTextForCreation", "(Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel$ViewState$Initialized;)V", "configureTextForStart", "onResume", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "eventModel$delegate", "Lkotlin/Lazy;", "getEventModel", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "eventModel", "Lcom/discord/databinding/WidgetPreviewGuildScheduledEventBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetPreviewGuildScheduledEventBinding;", "binding", "Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/guildscheduledevent/PreviewGuildScheduledEventViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPreviewGuildScheduledEvent extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetPreviewGuildScheduledEvent.class, "binding", "getBinding()Lcom/discord/databinding/WidgetPreviewGuildScheduledEventBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final String EXTRA_EVENT_CREATED = "INTENT_EXTRA_EVENT_CREATED";
    private static final String EXTRA_EVENT_MODEL = "INTENT_EXTRA_EVENT_MODEL";
    private static final String EXTRA_EXISTING_EVENT_DATA = "INTENT_EXTRA_EXISTING_EVENT_DATA";
    private static final int RESULT_QUIT = 2;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetPreviewGuildScheduledEvent$binding$2.INSTANCE, null, 2, null);
    private final Lazy eventModel$delegate = g.lazy(new WidgetPreviewGuildScheduledEvent$eventModel$2(this));
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetPreviewGuildScheduledEvent.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0002)*B\t\b\u0002¢\u0006\u0004\b'\u0010(JE\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b2\b\b\u0002\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000fJ1\u0010\u0015\u001a\u0010\u0012\f\u0012\n \u0014*\u0004\u0018\u00010\t0\t0\b2\u0006\u0010\u0011\u001a\u00020\u00102\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\r0\u0012¢\u0006\u0004\b\u0015\u0010\u0016JY\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0011\u001a\u00020\u00102*\u0010\u001b\u001a&\u0012\f\u0012\n\u0018\u00010\u0018j\u0004\u0018\u0001`\u0019\u0012\b\u0012\u00060\u0018j\u0002`\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\r0\u00172\u0010\b\u0002\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0012¢\u0006\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010\"\u001a\u00020\u001f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010!R\u0016\u0010#\u001a\u00020\u001f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010!R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010&¨\u0006+"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "eventModel", "Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$ExistingEventData;", "existingEventData", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "launcher", "", "fromSettings", "", "launch", "(Landroid/content/Context;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$ExistingEventData;Landroidx/activity/result/ActivityResultLauncher;Z)V", "Landroidx/fragment/app/Fragment;", "fragment", "Lkotlin/Function0;", "onEventStarted", "kotlin.jvm.PlatformType", "createJoinOnStartActivityRegistration", "(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function0;)Landroidx/activity/result/ActivityResultLauncher;", "Lkotlin/Function3;", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/primitives/GuildScheduledEventId;", "onEventCreated", "onQuit", "registerForResult", "(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)Landroidx/activity/result/ActivityResultLauncher;", "", "EXTRA_EVENT_CREATED", "Ljava/lang/String;", "EXTRA_EVENT_MODEL", "EXTRA_EXISTING_EVENT_DATA", "", "RESULT_QUIT", "I", HookHelper.constructorName, "()V", "Action", "ExistingEventData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetPreviewGuildScheduledEvent.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "EDIT_EVENT", "START_EVENT", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum Action {
            EDIT_EVENT,
            START_EVENT
        }

        /* compiled from: WidgetPreviewGuildScheduledEvent.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001c\u0010\u001dJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001b\u0010\b¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$ExistingEventData;", "Ljava/io/Serializable;", "", "Lcom/discord/primitives/GuildScheduledEventId;", "component1", "()J", "Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;", "component2", "()Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;", "guildScheduledEventId", "action", "copy", "(JLcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;)Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$ExistingEventData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildScheduledEventId", "Lcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;", "getAction", HookHelper.constructorName, "(JLcom/discord/widgets/guildscheduledevent/WidgetPreviewGuildScheduledEvent$Companion$Action;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ExistingEventData implements Serializable {
            private final Action action;
            private final long guildScheduledEventId;

            public ExistingEventData(long j, Action action) {
                m.checkNotNullParameter(action, "action");
                this.guildScheduledEventId = j;
                this.action = action;
            }

            public static /* synthetic */ ExistingEventData copy$default(ExistingEventData existingEventData, long j, Action action, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = existingEventData.guildScheduledEventId;
                }
                if ((i & 2) != 0) {
                    action = existingEventData.action;
                }
                return existingEventData.copy(j, action);
            }

            public final long component1() {
                return this.guildScheduledEventId;
            }

            public final Action component2() {
                return this.action;
            }

            public final ExistingEventData copy(long j, Action action) {
                m.checkNotNullParameter(action, "action");
                return new ExistingEventData(j, action);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ExistingEventData)) {
                    return false;
                }
                ExistingEventData existingEventData = (ExistingEventData) obj;
                return this.guildScheduledEventId == existingEventData.guildScheduledEventId && m.areEqual(this.action, existingEventData.action);
            }

            public final Action getAction() {
                return this.action;
            }

            public final long getGuildScheduledEventId() {
                return this.guildScheduledEventId;
            }

            public int hashCode() {
                int a = b.a(this.guildScheduledEventId) * 31;
                Action action = this.action;
                return a + (action != null ? action.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("ExistingEventData(guildScheduledEventId=");
                R.append(this.guildScheduledEventId);
                R.append(", action=");
                R.append(this.action);
                R.append(")");
                return R.toString();
            }
        }

        private Companion() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ActivityResultLauncher registerForResult$default(Companion companion, Fragment fragment, Function3 function3, Function0 function0, int i, Object obj) {
            if ((i & 4) != 0) {
                function0 = null;
            }
            return companion.registerForResult(fragment, function3, function0);
        }

        public final ActivityResultLauncher<Intent> createJoinOnStartActivityRegistration(final Fragment fragment, final Function0<Unit> function0) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(function0, "onEventStarted");
            ActivityResultLauncher<Intent> registerForActivityResult = fragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent$Companion$createJoinOnStartActivityRegistration$1
                public final void onActivityResult(ActivityResult activityResult) {
                    Intent data;
                    Long longExtraOrNull;
                    m.checkNotNullExpressionValue(activityResult, "activityResult");
                    if (activityResult.getResultCode() == -1 && (data = activityResult.getData()) != null && (longExtraOrNull = IntentUtilsKt.getLongExtraOrNull(data, "com.discord.intent.extra.EXTRA_CHANNEL_ID")) != null) {
                        Channel channel = StoreStream.Companion.getChannels().getChannel(longExtraOrNull.longValue());
                        if (channel != null) {
                            Fragment fragment2 = Fragment.this;
                            if (fragment2 instanceof AppFragment) {
                                VoiceUtils.handleConnectToEventChannel(channel, (AppFragment) fragment2, function0);
                            } else if (fragment2 instanceof AppBottomSheet) {
                                VoiceUtils.handleConnectToEventChannel(channel, (AppBottomSheet) fragment2, function0);
                            }
                        }
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…entStarted)\n      }\n    }");
            return registerForActivityResult;
        }

        public final void launch(Context context, GuildScheduledEventModel guildScheduledEventModel, ExistingEventData existingEventData, ActivityResultLauncher<Intent> activityResultLauncher, boolean z2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(guildScheduledEventModel, "eventModel");
            Intent intent = new Intent();
            intent.putExtra(WidgetPreviewGuildScheduledEvent.EXTRA_EVENT_MODEL, guildScheduledEventModel);
            if (existingEventData != null) {
                intent.putExtra(WidgetPreviewGuildScheduledEvent.EXTRA_EXISTING_EVENT_DATA, existingEventData);
            }
            if (z2) {
                intent.putExtra("transition", AppTransitionActivity.Transition.TYPE_SLIDE_HORIZONTAL);
            }
            if (activityResultLauncher != null) {
                j.g.f(context, activityResultLauncher, WidgetPreviewGuildScheduledEvent.class, intent);
            } else {
                j.d(context, WidgetPreviewGuildScheduledEvent.class, intent);
            }
        }

        public final ActivityResultLauncher<Intent> registerForResult(Fragment fragment, final Function3<? super Long, ? super Long, ? super Boolean, Unit> function3, final Function0<Unit> function0) {
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(function3, "onEventCreated");
            ActivityResultLauncher<Intent> registerForActivityResult = fragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent$Companion$registerForResult$1
                public final void onActivityResult(ActivityResult activityResult) {
                    Function0 function02;
                    m.checkNotNullExpressionValue(activityResult, "activityResult");
                    int resultCode = activityResult.getResultCode();
                    if (resultCode == -1) {
                        Intent data = activityResult.getData();
                        if (data != null) {
                            m.checkNotNullExpressionValue(data, "it");
                            Function3.this.invoke(IntentUtilsKt.getLongExtraOrNull(data, "com.discord.intent.extra.EXTRA_CHANNEL_ID"), Long.valueOf(data.getLongExtra("com.discord.intent.extra.EXTRA_GUILD_SCHEDULED_EVENT_ID", 0L)), Boolean.valueOf(data.getBooleanExtra(WidgetPreviewGuildScheduledEvent.EXTRA_EVENT_CREATED, false)));
                        }
                    } else if (resultCode == 2 && (function02 = function0) != null) {
                        Unit unit = (Unit) function02.invoke();
                    }
                }
            });
            m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…  }\n          }\n        }");
            return registerForActivityResult;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            Companion.Action.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[Companion.Action.START_EVENT.ordinal()] = 1;
            iArr[Companion.Action.EDIT_EVENT.ordinal()] = 2;
        }
    }

    public WidgetPreviewGuildScheduledEvent() {
        super(R.layout.widget_preview_guild_scheduled_event);
        WidgetPreviewGuildScheduledEvent$viewModel$2 widgetPreviewGuildScheduledEvent$viewModel$2 = new WidgetPreviewGuildScheduledEvent$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(PreviewGuildScheduledEventViewModel.class), new WidgetPreviewGuildScheduledEvent$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetPreviewGuildScheduledEvent$viewModel$2));
    }

    private final void configureTextForCreation(PreviewGuildScheduledEventViewModel.ViewState.Initialized initialized) {
        CharSequence d;
        TextView textView = getBinding().f;
        m.checkNotNullExpressionValue(textView, "binding.confirmGuildScheduledEventSubtitle");
        textView.setVisibility(8);
        TextView textView2 = getBinding().g;
        ViewGroup.LayoutParams layoutParams = textView2.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.setMargins(DimenUtils.dpToPixels(16), DimenUtils.dpToPixels(24), DimenUtils.dpToPixels(16), 0);
        textView2.setLayoutParams(marginLayoutParams);
        d = b.a.k.b.d(textView2, R.string.guild_event_preview_title, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        textView2.setText(d);
        TextView textView3 = getBinding().h;
        m.checkNotNullExpressionValue(textView3, "binding.createEventSubtitle");
        textView3.setVisibility(0);
        GuildScheduledEventLocationInfo locationInfo = initialized.getLocationInfo();
        if (locationInfo instanceof GuildScheduledEventLocationInfo.ChannelLocation) {
            String locationName = initialized.getLocationInfo().getLocationName();
            TextView textView4 = getBinding().h;
            m.checkNotNullExpressionValue(textView4, "binding.createEventSubtitle");
            b.a.k.b.m(textView4, R.string.guild_event_preview_body, new Object[0], new WidgetPreviewGuildScheduledEvent$configureTextForCreation$2(this, locationName, initialized));
        } else if (locationInfo instanceof GuildScheduledEventLocationInfo.ExternalLocation) {
            TextView textView5 = getBinding().h;
            m.checkNotNullExpressionValue(textView5, "binding.createEventSubtitle");
            textView5.setText(getString(R.string.guild_event_preview_external_event_body));
        }
    }

    private final void configureTextForStart(PreviewGuildScheduledEventViewModel.ViewState.Initialized initialized) {
        TextView textView = getBinding().f;
        textView.setVisibility(0);
        textView.setText(getString(R.string.start_event_confirmation_no_hook));
        getBinding().g.setText(initialized.getEventModel().getName());
        TextView textView2 = getBinding().h;
        m.checkNotNullExpressionValue(textView2, "binding.createEventSubtitle");
        textView2.setVisibility(8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(PreviewGuildScheduledEventViewModel.ViewState viewState) {
        int i;
        CharSequence b2;
        if (viewState instanceof PreviewGuildScheduledEventViewModel.ViewState.Invalid) {
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
            }
        } else if (viewState instanceof PreviewGuildScheduledEventViewModel.ViewState.Initialized) {
            PreviewGuildScheduledEventViewModel.ViewState.Initialized initialized = (PreviewGuildScheduledEventViewModel.ViewState.Initialized) viewState;
            GuildScheduledEventLocationInfo locationInfo = initialized.getLocationInfo();
            if (!(locationInfo instanceof GuildScheduledEventLocationInfo.ChannelLocation)) {
                locationInfo = null;
            }
            GuildScheduledEventLocationInfo.ChannelLocation channelLocation = (GuildScheduledEventLocationInfo.ChannelLocation) locationInfo;
            Channel channel = channelLocation != null ? channelLocation.getChannel() : null;
            UserGuildMember creatorUserGuildMember$default = GuildScheduledEventUtilitiesKt.getCreatorUserGuildMember$default(initialized.getEventModel(), (StoreGuilds) null, (StoreUser) null, 3, (Object) null);
            GuildScheduledEventItemView guildScheduledEventItemView = getBinding().e;
            GuildScheduledEventModel eventModel = initialized.getEventModel();
            if (creatorUserGuildMember$default == null) {
                creatorUserGuildMember$default = new UserGuildMember(StoreStream.Companion.getUsers().getMe(), null);
            }
            guildScheduledEventItemView.configureAsPreview(eventModel, channel, creatorUserGuildMember$default);
            MaterialButton materialButton = getBinding().i;
            materialButton.setEnabled(!initialized.getRequestProcessing());
            materialButton.setBackgroundTintList(ContextCompat.getColorStateList(materialButton.getContext(), initialized.isStartingEvent() ? R.color.uikit_btn_bg_color_selector_green : R.color.uikit_btn_bg_color_selector_brand));
            Context context = materialButton.getContext();
            m.checkNotNullExpressionValue(context, "context");
            Companion.Action existingEventAction = initialized.getExistingEventAction();
            if (existingEventAction == null) {
                i = R.string.schedule_event;
            } else {
                int ordinal = existingEventAction.ordinal();
                if (ordinal == 0) {
                    i = R.string.edit_event;
                } else if (ordinal == 1) {
                    i = R.string.start_event;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
            int i2 = 0;
            b2 = b.a.k.b.b(context, i, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            materialButton.setText(b2);
            ImageView imageView = getBinding().f2489b;
            m.checkNotNullExpressionValue(imageView, "binding.backButton");
            imageView.setVisibility(initialized.isStartingEvent() ^ true ? 0 : 8);
            TextView textView = getBinding().j;
            m.checkNotNullExpressionValue(textView, "binding.stepText");
            textView.setVisibility(initialized.isStartingEvent() ^ true ? 0 : 8);
            if (initialized.isStartingEvent()) {
                configureTextForStart(initialized);
            } else {
                configureTextForCreation(initialized);
            }
            CheckedSetting checkedSetting = getBinding().d;
            m.checkNotNullExpressionValue(checkedSetting, "binding.confirmGuildScheduledEventNotify");
            if (!initialized.getCanNotifyEveryone()) {
                i2 = 8;
            }
            checkedSetting.setVisibility(i2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetPreviewGuildScheduledEventBinding getBinding() {
        return (WidgetPreviewGuildScheduledEventBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildScheduledEventModel getEventModel() {
        return (GuildScheduledEventModel) this.eventModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final PreviewGuildScheduledEventViewModel getViewModel() {
        return (PreviewGuildScheduledEventViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetPreviewGuildScheduledEvent.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetPreviewGuildScheduledEvent$onResume$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        TextView textView = getBinding().j;
        m.checkNotNullExpressionValue(textView, "binding.stepText");
        b.a.k.b.m(textView, R.string.guild_event_step_label, new Object[]{3, 3}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().f2489b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                FragmentActivity activity = WidgetPreviewGuildScheduledEvent.this.e();
                if (activity != null) {
                    activity.finish();
                }
            }
        });
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                FragmentActivity activity = WidgetPreviewGuildScheduledEvent.this.e();
                if (activity != null) {
                    activity.setResult(2);
                    activity.finish();
                }
            }
        });
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent$onViewBound$3

            /* compiled from: WidgetPreviewGuildScheduledEvent.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/GuildScheduledEventId;", "it", "", "invoke", "(Ljava/lang/Long;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent$onViewBound$3$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends o implements Function1<Long, Unit> {
                public AnonymousClass2() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Long l) {
                    invoke2(l);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Long l) {
                    PreviewGuildScheduledEventViewModel viewModel;
                    GuildScheduledEventModel eventModel;
                    GuildScheduledEventModel eventModel2;
                    FragmentActivity activity = WidgetPreviewGuildScheduledEvent.this.e();
                    if (activity != null) {
                        viewModel = WidgetPreviewGuildScheduledEvent.this.getViewModel();
                        if (viewModel.isCreate()) {
                            Intent intent = new Intent();
                            eventModel2 = WidgetPreviewGuildScheduledEvent.this.getEventModel();
                            intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", eventModel2.getChannelId());
                            intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_SCHEDULED_EVENT_ID", l);
                            intent.putExtra(WidgetPreviewGuildScheduledEvent.EXTRA_EVENT_CREATED, true);
                            activity.setResult(-1, intent);
                        } else {
                            Intent intent2 = new Intent();
                            eventModel = WidgetPreviewGuildScheduledEvent.this.getEventModel();
                            intent2.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", eventModel.getChannelId());
                            intent2.putExtra("com.discord.intent.extra.EXTRA_GUILD_SCHEDULED_EVENT_ID", l);
                            activity.setResult(-1, intent2);
                        }
                        activity.finish();
                    }
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PreviewGuildScheduledEventViewModel viewModel;
                WidgetPreviewGuildScheduledEventBinding binding;
                viewModel = WidgetPreviewGuildScheduledEvent.this.getViewModel();
                binding = WidgetPreviewGuildScheduledEvent.this.getBinding();
                CheckedSetting checkedSetting = binding.d;
                m.checkNotNullExpressionValue(checkedSetting, "checkedSetting");
                boolean z2 = true;
                if (!(checkedSetting.getVisibility() == 0) || !checkedSetting.isChecked()) {
                    z2 = false;
                }
                viewModel.onBottomButtonClicked(WidgetPreviewGuildScheduledEvent.this.requireContext(), z2, new AnonymousClass2());
            }
        });
    }
}
