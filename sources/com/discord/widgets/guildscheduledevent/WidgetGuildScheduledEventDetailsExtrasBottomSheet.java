package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetGuildScheduledEventDetailsExtrasBottomSheetBinding;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventTiming;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventDetailsViewModel;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventLocationSelect;
import com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent;
import com.discord.widgets.mobile_reports.WidgetMobileReports;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetGuildScheduledEventDetailsExtrasBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u0000 +2\u00020\u0001:\u0001+B\u0007¢\u0006\u0004\b*\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\f\u0010\bJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0016\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001c\u001a\u00020\u00178F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010!\u001a\u00020\u001d8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u0013\u001a\u0004\b\u001f\u0010 R$\u0010%\u001a\u0010\u0012\f\u0012\n $*\u0004\u0018\u00010#0#0\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)¨\u0006,"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventDetailsExtrasBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;", "viewState", "", "configureUi", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel$ViewState;)V", "dismissWithActionTaken", "()V", "", "getContentViewResId", "()I", "onResume", "Landroid/content/DialogInterface;", "dialog", "onDismiss", "(Landroid/content/DialogInterface;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsViewModel;", "viewModel", "Lcom/discord/databinding/WidgetGuildScheduledEventDetailsExtrasBottomSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildScheduledEventDetailsExtrasBottomSheetBinding;", "binding", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args$delegate", "getArgs", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "previewLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "", "actionTaken", "Z", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildScheduledEventDetailsExtrasBottomSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildScheduledEventDetailsExtrasBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildScheduledEventDetailsExtrasBottomSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private boolean actionTaken;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildScheduledEventDetailsExtrasBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy args$delegate = g.lazy(new WidgetGuildScheduledEventDetailsExtrasBottomSheet$$special$$inlined$args$1(this, "intent_args_key"));
    private final ActivityResultLauncher<Intent> previewLauncher = WidgetPreviewGuildScheduledEvent.Companion.createJoinOnStartActivityRegistration(this, new WidgetGuildScheduledEventDetailsExtrasBottomSheet$previewLauncher$1(this));

    /* compiled from: WidgetGuildScheduledEventDetailsExtrasBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/WidgetGuildScheduledEventDetailsExtrasBottomSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "args", "", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, GuildScheduledEventDetailsArgs guildScheduledEventDetailsArgs) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(guildScheduledEventDetailsArgs, "args");
            WidgetGuildScheduledEventDetailsExtrasBottomSheet widgetGuildScheduledEventDetailsExtrasBottomSheet = new WidgetGuildScheduledEventDetailsExtrasBottomSheet();
            widgetGuildScheduledEventDetailsExtrasBottomSheet.setArguments(d.e2(guildScheduledEventDetailsArgs));
            widgetGuildScheduledEventDetailsExtrasBottomSheet.show(fragmentManager, WidgetGuildScheduledEventDetailsExtrasBottomSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildScheduledEventDetailsExtrasBottomSheet() {
        super(false, 1, null);
        WidgetGuildScheduledEventDetailsExtrasBottomSheet$viewModel$2 widgetGuildScheduledEventDetailsExtrasBottomSheet$viewModel$2 = new WidgetGuildScheduledEventDetailsExtrasBottomSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildScheduledEventDetailsViewModel.class), new WidgetGuildScheduledEventDetailsExtrasBottomSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildScheduledEventDetailsExtrasBottomSheet$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(final GuildScheduledEventDetailsViewModel.ViewState viewState) {
        String str;
        if (!(viewState instanceof GuildScheduledEventDetailsViewModel.ViewState.Initialized)) {
            dismiss();
            return;
        }
        GuildScheduledEventDetailsViewModel.ViewState.Initialized initialized = (GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState;
        final boolean canStartEvent = initialized.getCanStartEvent();
        final GuildScheduledEventTiming eventTiming = GuildScheduledEventUtilitiesKt.getEventTiming(initialized.getGuildScheduledEvent());
        TextView textView = getBinding().f;
        boolean z2 = true;
        int i = 0;
        textView.setVisibility(canStartEvent && eventTiming.isStartable() ? 0 : 8);
        boolean isRsvped = initialized.isRsvped();
        if (isRsvped) {
            str = getString(R.string.event_mark_not_interested);
        } else if (!isRsvped) {
            str = getString(R.string.event_mark_interested);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        textView.setText(str);
        textView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildScheduledEventDetailsViewModel viewModel;
                viewModel = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.getViewModel();
                viewModel.onRsvpButtonClicked();
            }
        });
        TextView textView2 = getBinding().h;
        m.checkNotNullExpressionValue(textView2, "binding.startEvent");
        textView2.setVisibility(canStartEvent && eventTiming.isLongStartable() ? 0 : 8);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ActivityResultLauncher activityResultLauncher;
                WidgetPreviewGuildScheduledEvent.Companion companion = WidgetPreviewGuildScheduledEvent.Companion;
                Context requireContext = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                GuildScheduledEventModel model = GuildScheduledEventModelKt.toModel(((GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState).getGuildScheduledEvent());
                WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData existingEventData = new WidgetPreviewGuildScheduledEvent.Companion.ExistingEventData(((GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState).getGuildScheduledEvent().i(), WidgetPreviewGuildScheduledEvent.Companion.Action.START_EVENT);
                activityResultLauncher = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.previewLauncher;
                companion.launch(requireContext, model, (r13 & 4) != 0 ? null : existingEventData, (r13 & 8) != 0 ? null : activityResultLauncher, (r13 & 16) != 0 ? false : false);
            }
        });
        TextView textView3 = getBinding().d;
        m.checkNotNullExpressionValue(textView3, "binding.editEvent");
        textView3.setVisibility(canStartEvent ? 0 : 8);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.dismissWithActionTaken();
                WidgetGuildScheduledEventLocationSelect.Companion companion = WidgetGuildScheduledEventLocationSelect.Companion;
                Context requireContext = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                companion.launchForEdit(requireContext, ((GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState).getGuildScheduledEvent().h(), ((GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState).getGuildScheduledEvent().i());
            }
        });
        boolean z3 = eventTiming == GuildScheduledEventTiming.LIVE;
        TextView textView4 = getBinding().e;
        m.checkNotNullExpressionValue(textView4, "binding.endEvent");
        textView4.setVisibility(canStartEvent && z3 ? 0 : 8);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$4

            /* compiled from: WidgetGuildScheduledEventDetailsExtrasBottomSheet.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$4$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function0<Unit> {
                public AnonymousClass1() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.dismissWithActionTaken();
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildScheduledEventDetailsViewModel viewModel;
                viewModel = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.getViewModel();
                Context requireContext = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                viewModel.endEventClicked(requireContext, new AnonymousClass1());
            }
        });
        TextView textView5 = getBinding().f2422b;
        m.checkNotNullExpressionValue(textView5, "binding.cancelEvent");
        if (!initialized.getCanStartEvent() || z3) {
            z2 = false;
        }
        textView5.setVisibility(z2 ? 0 : 8);
        getBinding().f2422b.setOnClickListener(new WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$5(this, viewState));
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.dismissWithActionTaken();
                WidgetMobileReports.Companion companion = WidgetMobileReports.Companion;
                Context requireContext = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                companion.launchGuildScheduledEventReport(requireContext, ((GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState).getGuildScheduledEvent().h(), ((GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState).getGuildScheduledEvent().i());
            }
        });
        TextView textView6 = getBinding().c;
        m.checkNotNullExpressionValue(textView6, "binding.copyId");
        if (!initialized.isDeveloperMode()) {
            i = 8;
        }
        textView6.setVisibility(i);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsExtrasBottomSheet$configureUi$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.dismissWithActionTaken();
                Context requireContext = WidgetGuildScheduledEventDetailsExtrasBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                b.a.d.m.c(requireContext, String.valueOf(((GuildScheduledEventDetailsViewModel.ViewState.Initialized) viewState).getGuildScheduledEvent().i()), 0, 4);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void dismissWithActionTaken() {
        this.actionTaken = true;
        dismiss();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildScheduledEventDetailsViewModel getViewModel() {
        return (GuildScheduledEventDetailsViewModel) this.viewModel$delegate.getValue();
    }

    public final GuildScheduledEventDetailsArgs getArgs() {
        return (GuildScheduledEventDetailsArgs) this.args$delegate.getValue();
    }

    public final WidgetGuildScheduledEventDetailsExtrasBottomSheetBinding getBinding() {
        return (WidgetGuildScheduledEventDetailsExtrasBottomSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_guild_scheduled_event_details_extras_bottom_sheet;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        if (!this.actionTaken) {
            WidgetGuildScheduledEventDetailsBottomSheet.Companion companion = WidgetGuildScheduledEventDetailsBottomSheet.Companion;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(parentFragmentManager, getArgs());
        }
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetGuildScheduledEventDetailsExtrasBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildScheduledEventDetailsExtrasBottomSheet$onResume$1(this));
    }
}
