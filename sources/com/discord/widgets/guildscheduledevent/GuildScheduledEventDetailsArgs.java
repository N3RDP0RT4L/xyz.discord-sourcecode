package com.discord.widgets.guildscheduledevent;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetGuildScheduledEventDetailsBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\f\b\u0087\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0005\u0012\u0010\b\u0002\u0010\u000f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\b\u0012\b\b\u0002\u0010\u0010\u001a\u00020\n¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\t\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\bHÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJH\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\u0010\b\u0002\u0010\u000f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\b2\b\b\u0002\u0010\u0010\u001a\u00020\nHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0018J \u0010#\u001a\u00020\"2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b#\u0010$R!\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010%\u001a\u0004\b&\u0010\u0007R!\u0010\u000f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010%\u001a\u0004\b'\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010(\u001a\u0004\b)\u0010\fR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010*\u001a\u0004\b+\u0010\u0004¨\u0006."}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "Landroid/os/Parcelable;", "", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "Lcom/discord/primitives/ChannelId;", "component3", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsSource;", "component4", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsSource;", "eventId", "guildId", "channelId", "source", "copy", "(JLjava/lang/Long;Ljava/lang/Long;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsSource;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsArgs;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Ljava/lang/Long;", "getGuildId", "getChannelId", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsSource;", "getSource", "J", "getEventId", HookHelper.constructorName, "(JLjava/lang/Long;Ljava/lang/Long;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDetailsSource;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventDetailsArgs implements Parcelable {
    public static final Parcelable.Creator<GuildScheduledEventDetailsArgs> CREATOR = new Creator();
    private final Long channelId;
    private final long eventId;
    private final Long guildId;
    private final GuildScheduledEventDetailsSource source;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<GuildScheduledEventDetailsArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final GuildScheduledEventDetailsArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            return new GuildScheduledEventDetailsArgs(parcel.readLong(), parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null, parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null, (GuildScheduledEventDetailsSource) Enum.valueOf(GuildScheduledEventDetailsSource.class, parcel.readString()));
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final GuildScheduledEventDetailsArgs[] newArray(int i) {
            return new GuildScheduledEventDetailsArgs[i];
        }
    }

    public GuildScheduledEventDetailsArgs(long j, Long l, Long l2, GuildScheduledEventDetailsSource guildScheduledEventDetailsSource) {
        m.checkNotNullParameter(guildScheduledEventDetailsSource, "source");
        this.eventId = j;
        this.guildId = l;
        this.channelId = l2;
        this.source = guildScheduledEventDetailsSource;
    }

    public static /* synthetic */ GuildScheduledEventDetailsArgs copy$default(GuildScheduledEventDetailsArgs guildScheduledEventDetailsArgs, long j, Long l, Long l2, GuildScheduledEventDetailsSource guildScheduledEventDetailsSource, int i, Object obj) {
        if ((i & 1) != 0) {
            j = guildScheduledEventDetailsArgs.eventId;
        }
        long j2 = j;
        if ((i & 2) != 0) {
            l = guildScheduledEventDetailsArgs.guildId;
        }
        Long l3 = l;
        if ((i & 4) != 0) {
            l2 = guildScheduledEventDetailsArgs.channelId;
        }
        Long l4 = l2;
        if ((i & 8) != 0) {
            guildScheduledEventDetailsSource = guildScheduledEventDetailsArgs.source;
        }
        return guildScheduledEventDetailsArgs.copy(j2, l3, l4, guildScheduledEventDetailsSource);
    }

    public final long component1() {
        return this.eventId;
    }

    public final Long component2() {
        return this.guildId;
    }

    public final Long component3() {
        return this.channelId;
    }

    public final GuildScheduledEventDetailsSource component4() {
        return this.source;
    }

    public final GuildScheduledEventDetailsArgs copy(long j, Long l, Long l2, GuildScheduledEventDetailsSource guildScheduledEventDetailsSource) {
        m.checkNotNullParameter(guildScheduledEventDetailsSource, "source");
        return new GuildScheduledEventDetailsArgs(j, l, l2, guildScheduledEventDetailsSource);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildScheduledEventDetailsArgs)) {
            return false;
        }
        GuildScheduledEventDetailsArgs guildScheduledEventDetailsArgs = (GuildScheduledEventDetailsArgs) obj;
        return this.eventId == guildScheduledEventDetailsArgs.eventId && m.areEqual(this.guildId, guildScheduledEventDetailsArgs.guildId) && m.areEqual(this.channelId, guildScheduledEventDetailsArgs.channelId) && m.areEqual(this.source, guildScheduledEventDetailsArgs.source);
    }

    public final Long getChannelId() {
        return this.channelId;
    }

    public final long getEventId() {
        return this.eventId;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final GuildScheduledEventDetailsSource getSource() {
        return this.source;
    }

    public int hashCode() {
        int a = b.a(this.eventId) * 31;
        Long l = this.guildId;
        int i = 0;
        int hashCode = (a + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        GuildScheduledEventDetailsSource guildScheduledEventDetailsSource = this.source;
        if (guildScheduledEventDetailsSource != null) {
            i = guildScheduledEventDetailsSource.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEventDetailsArgs(eventId=");
        R.append(this.eventId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", source=");
        R.append(this.source);
        R.append(")");
        return R.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeLong(this.eventId);
        Long l = this.guildId;
        if (l != null) {
            parcel.writeInt(1);
            parcel.writeLong(l.longValue());
        } else {
            parcel.writeInt(0);
        }
        Long l2 = this.channelId;
        if (l2 != null) {
            parcel.writeInt(1);
            parcel.writeLong(l2.longValue());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.source.name());
    }

    public /* synthetic */ GuildScheduledEventDetailsArgs(long j, Long l, Long l2, GuildScheduledEventDetailsSource guildScheduledEventDetailsSource, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : l2, (i & 8) != 0 ? GuildScheduledEventDetailsSource.Guild : guildScheduledEventDetailsSource);
    }
}
