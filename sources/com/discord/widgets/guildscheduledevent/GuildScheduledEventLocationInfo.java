package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import androidx.annotation.DrawableRes;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityMetadata;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildScheduledEventLocationInfo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00102\u00020\u0001:\u0003\u0011\u0010\u0012B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\u0007\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0003¢\u0006\u0004\b\u0007\u0010\u0006J\r\u0010\t\u001a\u00020\b¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\r\u0010\f\u0082\u0001\u0002\u0013\u0014¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "", "Lcom/discord/api/channel/Channel;", "channel", "", "getChannelIcon", "(Lcom/discord/api/channel/Channel;)I", "getChannelIconSmall", "", "getLocationName", "()Ljava/lang/String;", "getLocationIcon", "()I", "getLocationIconSmall", HookHelper.constructorName, "()V", "Companion", "ChannelLocation", "ExternalLocation", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo$ChannelLocation;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo$ExternalLocation;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildScheduledEventLocationInfo {
    public static final Companion Companion = new Companion(null);

    /* compiled from: GuildScheduledEventLocationInfo.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo$ChannelLocation;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "channel", "copy", "(Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo$ChannelLocation;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelLocation extends GuildScheduledEventLocationInfo {
        private final Channel channel;

        public ChannelLocation(Channel channel) {
            super(null);
            this.channel = channel;
        }

        public static /* synthetic */ ChannelLocation copy$default(ChannelLocation channelLocation, Channel channel, int i, Object obj) {
            if ((i & 1) != 0) {
                channel = channelLocation.channel;
            }
            return channelLocation.copy(channel);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final ChannelLocation copy(Channel channel) {
            return new ChannelLocation(channel);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ChannelLocation) && m.areEqual(this.channel, ((ChannelLocation) obj).channel);
            }
            return true;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public int hashCode() {
            Channel channel = this.channel;
            if (channel != null) {
                return channel.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelLocation(channel=");
            R.append(this.channel);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildScheduledEventLocationInfo.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\t2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0007\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo$Companion;", "", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "buildLocationInfo", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "guildScheduledEventModel", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final GuildScheduledEventLocationInfo buildLocationInfo(GuildScheduledEvent guildScheduledEvent, Channel channel) {
            String str;
            m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
            if (guildScheduledEvent.f() != GuildScheduledEventEntityType.EXTERNAL) {
                return new ChannelLocation(channel);
            }
            GuildScheduledEventEntityMetadata e = guildScheduledEvent.e();
            if (e == null || (str = e.a()) == null) {
                str = "";
            }
            return new ExternalLocation(str);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final GuildScheduledEventLocationInfo buildLocationInfo(GuildScheduledEventModel guildScheduledEventModel, Channel channel) {
            String str;
            m.checkNotNullParameter(guildScheduledEventModel, "guildScheduledEventModel");
            if (guildScheduledEventModel.getEntityType() != GuildScheduledEventEntityType.EXTERNAL) {
                return new ChannelLocation(channel);
            }
            GuildScheduledEventEntityMetadata entityMetadata = guildScheduledEventModel.getEntityMetadata();
            if (entityMetadata == null || (str = entityMetadata.a()) == null) {
                str = "";
            }
            return new ExternalLocation(str);
        }
    }

    /* compiled from: GuildScheduledEventLocationInfo.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo$ExternalLocation;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "", "component1", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "copy", "(Ljava/lang/String;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo$ExternalLocation;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getLocation", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ExternalLocation extends GuildScheduledEventLocationInfo {
        private final String location;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ExternalLocation(String str) {
            super(null);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            this.location = str;
        }

        public static /* synthetic */ ExternalLocation copy$default(ExternalLocation externalLocation, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = externalLocation.location;
            }
            return externalLocation.copy(str);
        }

        public final String component1() {
            return this.location;
        }

        public final ExternalLocation copy(String str) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            return new ExternalLocation(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ExternalLocation) && m.areEqual(this.location, ((ExternalLocation) obj).location);
            }
            return true;
        }

        public final String getLocation() {
            return this.location;
        }

        public int hashCode() {
            String str = this.location;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.H(a.R("ExternalLocation(location="), this.location, ")");
        }
    }

    private GuildScheduledEventLocationInfo() {
    }

    @DrawableRes
    private final int getChannelIcon(Channel channel) {
        Integer valueOf = channel != null ? Integer.valueOf(channel.A()) : null;
        return ((valueOf != null && valueOf.intValue() == 2) || valueOf == null || valueOf.intValue() != 13) ? R.drawable.ic_channel_voice : R.drawable.ic_stage_20dp;
    }

    @DrawableRes
    private final int getChannelIconSmall(Channel channel) {
        Integer valueOf = channel != null ? Integer.valueOf(channel.A()) : null;
        return ((valueOf != null && valueOf.intValue() == 2) || valueOf == null || valueOf.intValue() != 13) ? R.drawable.ic_channel_voice_16dp : R.drawable.ic_channel_stage_16dp;
    }

    @DrawableRes
    public final int getLocationIcon() {
        if (this instanceof ChannelLocation) {
            return getChannelIcon(((ChannelLocation) this).getChannel());
        }
        if (this instanceof ExternalLocation) {
            return R.drawable.ic_location_24dp;
        }
        throw new NoWhenBranchMatchedException();
    }

    @DrawableRes
    public final int getLocationIconSmall() {
        if (this instanceof ChannelLocation) {
            return getChannelIconSmall(((ChannelLocation) this).getChannel());
        }
        if (this instanceof ExternalLocation) {
            return R.drawable.ic_location_16dp;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final String getLocationName() {
        String m;
        if (this instanceof ChannelLocation) {
            Channel channel = ((ChannelLocation) this).getChannel();
            return (channel == null || (m = channel.m()) == null) ? "" : m;
        } else if (this instanceof ExternalLocation) {
            return ((ExternalLocation) this).getLocation();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public /* synthetic */ GuildScheduledEventLocationInfo(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
