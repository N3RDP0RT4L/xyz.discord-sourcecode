package com.discord.widgets.guildscheduledevent;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildScheduledEventSettingsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000*\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "invoke", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventSettingsViewModel$toggleBroadcastToDirectoryChannel$1 extends o implements Function1<GuildScheduledEventModel, GuildScheduledEventModel> {
    public final /* synthetic */ boolean $associateToHubs;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventSettingsViewModel$toggleBroadcastToDirectoryChannel$1(boolean z2) {
        super(1);
        this.$associateToHubs = z2;
    }

    public final GuildScheduledEventModel invoke(GuildScheduledEventModel guildScheduledEventModel) {
        GuildScheduledEventModel copy;
        m.checkNotNullParameter(guildScheduledEventModel, "$receiver");
        copy = guildScheduledEventModel.copy((r30 & 1) != 0 ? guildScheduledEventModel.guildId : 0L, (r30 & 2) != 0 ? guildScheduledEventModel.name : null, (r30 & 4) != 0 ? guildScheduledEventModel.channelId : null, (r30 & 8) != 0 ? guildScheduledEventModel.creatorId : null, (r30 & 16) != 0 ? guildScheduledEventModel.startDate : null, (r30 & 32) != 0 ? guildScheduledEventModel.startTime : null, (r30 & 64) != 0 ? guildScheduledEventModel.endDate : null, (r30 & 128) != 0 ? guildScheduledEventModel.endTime : null, (r30 & 256) != 0 ? guildScheduledEventModel.description : null, (r30 & 512) != 0 ? guildScheduledEventModel.entityType : null, (r30 & 1024) != 0 ? guildScheduledEventModel.entityMetadata : null, (r30 & 2048) != 0 ? guildScheduledEventModel.userCount : null, (r30 & 4096) != 0 ? guildScheduledEventModel.broadcastToDirectoryChannels : Boolean.valueOf(this.$associateToHubs));
        return copy;
    }
}
