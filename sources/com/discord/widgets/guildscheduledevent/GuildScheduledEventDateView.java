package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.k.b;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.databinding.GuildScheduledEventDateViewBinding;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventTiming;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import d0.z.d.m;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildScheduledEventDateView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b%\u0010&B\u0019\b\u0016\u0012\u0006\u0010$\u001a\u00020#\u0012\u0006\u0010(\u001a\u00020'¢\u0006\u0004\b%\u0010)B!\b\u0016\u0012\u0006\u0010$\u001a\u00020#\u0012\u0006\u0010(\u001a\u00020'\u0012\u0006\u0010*\u001a\u00020\u0012¢\u0006\u0004\b%\u0010+J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J'\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ1\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\r\u001a\u0004\u0018\u00010\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J/\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0015\u0010\u0016J7\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001f\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00192\b\b\u0002\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ9\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\r\u001a\u0004\u0018\u00010\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001d\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006,"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDateView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "configureTruncatedDateTime", "()V", "", "startTimeMillis", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "entityType", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "status", "configureFinishedEvent", "(JLcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;)V", "endTimeMillis", "configureViaTiming", "(JLjava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;)V", "Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;", "timing", "", "getTextColorViaTiming", "(Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;)I", "configureStartingTimeString", "(Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;JLcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;)V", "configureCompleteTimeString", "(Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;JJLcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;)V", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "event", "", "truncateDateTime", "configure", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Z)V", "(JLjava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;Z)V", "Lcom/discord/databinding/GuildScheduledEventDateViewBinding;", "binding", "Lcom/discord/databinding/GuildScheduledEventDateViewBinding;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventDateView extends ConstraintLayout {
    private final GuildScheduledEventDateViewBinding binding;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            GuildScheduledEventTiming.values();
            int[] iArr = new int[7];
            $EnumSwitchMapping$0 = iArr;
            GuildScheduledEventTiming guildScheduledEventTiming = GuildScheduledEventTiming.LIVE;
            iArr[guildScheduledEventTiming.ordinal()] = 1;
            GuildScheduledEventTiming guildScheduledEventTiming2 = GuildScheduledEventTiming.SOON;
            iArr[guildScheduledEventTiming2.ordinal()] = 2;
            GuildScheduledEventTiming guildScheduledEventTiming3 = GuildScheduledEventTiming.NOW;
            iArr[guildScheduledEventTiming3.ordinal()] = 3;
            GuildScheduledEventTiming guildScheduledEventTiming4 = GuildScheduledEventTiming.EXPIRED;
            iArr[guildScheduledEventTiming4.ordinal()] = 4;
            GuildScheduledEventTiming.values();
            int[] iArr2 = new int[7];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[guildScheduledEventTiming.ordinal()] = 1;
            iArr2[guildScheduledEventTiming2.ordinal()] = 2;
            iArr2[guildScheduledEventTiming3.ordinal()] = 3;
            iArr2[guildScheduledEventTiming4.ordinal()] = 4;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventDateView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        GuildScheduledEventDateViewBinding a = GuildScheduledEventDateViewBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "GuildScheduledEventDateV…rom(context), this, true)");
        this.binding = a;
    }

    public static /* synthetic */ void configure$default(GuildScheduledEventDateView guildScheduledEventDateView, GuildScheduledEvent guildScheduledEvent, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        guildScheduledEventDateView.configure(guildScheduledEvent, z2);
    }

    private final void configureCompleteTimeString(GuildScheduledEventTiming guildScheduledEventTiming, long j, long j2, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventStatus guildScheduledEventStatus) {
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        CharSequence eventEndingTimeString = GuildScheduledEventUtilitiesKt.getEventEndingTimeString(context, j, j2, guildScheduledEventStatus);
        if (eventEndingTimeString == null || eventEndingTimeString.length() == 0) {
            configureStartingTimeString(guildScheduledEventTiming, j, guildScheduledEventEntityType, guildScheduledEventStatus);
            return;
        }
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        CharSequence eventStartingTimeString = GuildScheduledEventUtilitiesKt.getEventStartingTimeString(context2, j, guildScheduledEventEntityType, guildScheduledEventStatus);
        TextView textView = this.binding.c;
        textView.setTextColor(ColorCompat.getThemedColor(textView.getContext(), (int) R.attr.colorHeaderSecondary));
        Context context3 = textView.getContext();
        m.checkNotNullExpressionValue(context3, "context");
        textView.setText(b.b(context3, R.string.start_date_to_end_date_with_color, new Object[]{eventStartingTimeString, eventEndingTimeString}, new GuildScheduledEventDateView$configureCompleteTimeString$$inlined$apply$lambda$1(this, eventStartingTimeString, eventEndingTimeString, guildScheduledEventTiming)));
        m.checkNotNullExpressionValue(textView, "binding.guildScheduledEv…      }\n        }\n      }");
    }

    private final void configureFinishedEvent(long j, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventStatus guildScheduledEventStatus) {
        ImageView imageView = this.binding.f2103b;
        imageView.setImageResource(R.drawable.ic_event_20dp);
        ColorCompatKt.tintWithColor(imageView, ColorCompat.getThemedColor(imageView.getContext(), (int) R.attr.colorControlBrandForegroundNew));
        TextView textView = this.binding.c;
        textView.setTextColor(ColorCompat.getThemedColor(textView.getContext(), (int) R.attr.colorHeaderSecondary));
        Context context = textView.getContext();
        m.checkNotNullExpressionValue(context, "context");
        textView.setText(GuildScheduledEventUtilitiesKt.getEventStartingTimeString(context, j, guildScheduledEventEntityType, guildScheduledEventStatus));
    }

    private final void configureStartingTimeString(GuildScheduledEventTiming guildScheduledEventTiming, long j, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventStatus guildScheduledEventStatus) {
        TextView textView = this.binding.c;
        Context context = textView.getContext();
        m.checkNotNullExpressionValue(context, "context");
        textView.setText(GuildScheduledEventUtilitiesKt.getEventStartingTimeString(context, j, guildScheduledEventEntityType, guildScheduledEventStatus));
        textView.setTextColor(getTextColorViaTiming(guildScheduledEventTiming));
    }

    private final void configureTruncatedDateTime() {
        TextView textView = this.binding.c;
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setMaxLines(1);
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x0052  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x005a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureViaTiming(long r9, java.lang.Long r11, com.discord.api.guildscheduledevent.GuildScheduledEventEntityType r12, com.discord.api.guildscheduledevent.GuildScheduledEventStatus r13) {
        /*
            r8 = this;
            com.discord.utilities.guildscheduledevent.GuildScheduledEventTiming r1 = com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt.getEventTiming(r9, r13)
            com.discord.databinding.GuildScheduledEventDateViewBinding r0 = r8.binding
            android.widget.ImageView r0 = r0.f2103b
            int r2 = r1.ordinal()
            r3 = 3
            r4 = 2130968921(0x7f040159, float:1.754651E38)
            if (r2 == r3) goto L3f
            r3 = 4
            if (r2 == r3) goto L3f
            r3 = 5
            r5 = 2131231693(0x7f0803cd, float:1.8079474E38)
            if (r2 == r3) goto L2d
            r3 = 6
            if (r2 == r3) goto L3f
            r0.setImageResource(r5)
            android.content.Context r2 = r0.getContext()
            int r2 = com.discord.utilities.color.ColorCompat.getThemedColor(r2, r4)
            com.discord.utilities.color.ColorCompatKt.tintWithColor(r0, r2)
            goto L50
        L2d:
            r0.setImageResource(r5)
            android.content.Context r2 = r0.getContext()
            r3 = 2131100277(0x7f060275, float:1.781293E38)
            int r2 = com.discord.utilities.color.ColorCompat.getColor(r2, r3)
            com.discord.utilities.color.ColorCompatKt.tintWithColor(r0, r2)
            goto L50
        L3f:
            r2 = 2131231619(0x7f080383, float:1.8079324E38)
            r0.setImageResource(r2)
            android.content.Context r2 = r0.getContext()
            int r2 = com.discord.utilities.color.ColorCompat.getThemedColor(r2, r4)
            com.discord.utilities.color.ColorCompatKt.tintWithColor(r0, r2)
        L50:
            if (r11 != 0) goto L5a
            r0 = r8
            r2 = r9
            r4 = r12
            r5 = r13
            r0.configureStartingTimeString(r1, r2, r4, r5)
            goto L65
        L5a:
            long r4 = r11.longValue()
            r0 = r8
            r2 = r9
            r6 = r12
            r7 = r13
            r0.configureCompleteTimeString(r1, r2, r4, r6, r7)
        L65:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.guildscheduledevent.GuildScheduledEventDateView.configureViaTiming(long, java.lang.Long, com.discord.api.guildscheduledevent.GuildScheduledEventEntityType, com.discord.api.guildscheduledevent.GuildScheduledEventStatus):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final int getTextColorViaTiming(GuildScheduledEventTiming guildScheduledEventTiming) {
        int ordinal = guildScheduledEventTiming.ordinal();
        if (!(ordinal == 3 || ordinal == 4)) {
            if (ordinal == 5) {
                return ColorCompat.getColor(getContext(), (int) R.color.status_green_600);
            }
            if (ordinal != 6) {
                return ColorCompat.getThemedColor(getContext(), (int) R.attr.colorHeaderSecondary);
            }
        }
        return ColorCompat.getThemedColor(getContext(), (int) R.attr.colorControlBrandForegroundNew);
    }

    public final void configure(GuildScheduledEvent guildScheduledEvent, boolean z2) {
        m.checkNotNullParameter(guildScheduledEvent, "event");
        long g = guildScheduledEvent.l().g();
        UtcDateTime k = guildScheduledEvent.k();
        configure(g, k != null ? Long.valueOf(k.g()) : null, guildScheduledEvent.f(), guildScheduledEvent.m(), z2);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventDateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        GuildScheduledEventDateViewBinding a = GuildScheduledEventDateViewBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "GuildScheduledEventDateV…rom(context), this, true)");
        this.binding = a;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventDateView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        GuildScheduledEventDateViewBinding a = GuildScheduledEventDateViewBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "GuildScheduledEventDateV…rom(context), this, true)");
        this.binding = a;
    }

    public final void configure(long j, Long l, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventStatus guildScheduledEventStatus, boolean z2) {
        Set set;
        m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
        m.checkNotNullParameter(guildScheduledEventStatus, "status");
        Objects.requireNonNull(GuildScheduledEventStatus.Companion);
        set = GuildScheduledEventStatus.DONE;
        if (set.contains(guildScheduledEventStatus)) {
            configureFinishedEvent(j, guildScheduledEventEntityType, guildScheduledEventStatus);
        } else {
            configureViaTiming(j, l, guildScheduledEventEntityType, guildScheduledEventStatus);
        }
        if (z2) {
            configureTruncatedDateTime();
        }
    }
}
