package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import b.a.k.b;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.databinding.GuildScheduledEventBottomButtonViewBinding;
import com.discord.databinding.GuildScheduledEventItemViewBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.guild.UserGuildMember;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.extensions.SimpleDraweeViewExtensionsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.utilities.textview.TextViewFadeHelper;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventListItem;
import com.discord.widgets.guildscheduledevent.buttonconfiguration.ButtonConfiguration;
import com.discord.widgets.guildscheduledevent.buttonconfiguration.DirectoryButtonConfiguration;
import com.discord.widgets.guildscheduledevent.buttonconfiguration.GuildButtonConfiguration;
import com.discord.widgets.hubs.events.HubGuildScheduledEventData;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildScheduledEventItemView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010M\u001a\u00020L\u0012\n\b\u0002\u0010O\u001a\u0004\u0018\u00010N\u0012\b\b\u0002\u0010Q\u001a\u00020P¢\u0006\u0004\bR\u0010SJG\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00062\b\u0010\t\u001a\u0004\u0018\u00010\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J%\u0010\u0011\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J%\u0010\u0018\u001a\u00020\u000e2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\u0010\u0017\u001a\u00060\u0015j\u0002`\u0016H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J5\u0010 \u001a\u00020\u000e2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\f2\u0006\u0010\u001d\u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\f2\u0006\u0010\u001f\u001a\u00020\f¢\u0006\u0004\b \u0010!J=\u0010(\u001a\u00020\u000e2\u0006\u0010#\u001a\u00020\"2\u0006\u0010\u001c\u001a\u00020\f2\u0006\u0010$\u001a\u00020\f2\u0006\u0010%\u001a\u00020\f2\u0006\u0010&\u001a\u00020\f2\u0006\u0010'\u001a\u00020\f¢\u0006\u0004\b(\u0010)JO\u00100\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010+\u001a\u00020*2\u0006\u0010,\u001a\u00020*2\u0006\u0010-\u001a\u00020*2\u0006\u0010.\u001a\u00020*2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010/\u001a\u00020\f¢\u0006\u0004\b0\u00101Jy\u00108\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u00103\u001a\u0004\u0018\u0001022\b\u0010\t\u001a\u0004\u0018\u00010\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u00104\u001a\u00020*2\u0006\u00105\u001a\u00020*2\u0006\u0010-\u001a\u00020*2\u0006\u00106\u001a\u00020*2\b\u0010\u001c\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010$\u001a\u00020\f2\b\b\u0002\u00107\u001a\u00020\f2\u0006\u0010&\u001a\u00020\f¢\u0006\u0004\b8\u00109J)\u0010<\u001a\u00020\u000e2\u0006\u0010;\u001a\u00020:2\b\u00103\u001a\u0004\u0018\u0001022\b\u0010\u000b\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b<\u0010=R\u001d\u0010C\u001a\u00020>8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010BR\u0016\u0010E\u001a\u00020D8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u0010FR\u001d\u0010K\u001a\u00020G8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bH\u0010@\u001a\u0004\bI\u0010J¨\u0006T"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventItemView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;", "locationInfo", "Lcom/discord/widgets/guildscheduledevent/buttonconfiguration/ButtonConfiguration;", "buttonConfiguration", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/guild/UserGuildMember;", "creator", "Landroid/view/View$OnClickListener;", "onCardClicked", "", "configureInternal", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;Lcom/discord/widgets/guildscheduledevent/buttonconfiguration/ButtonConfiguration;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/UserGuildMember;Landroid/view/View$OnClickListener;)V", "configureLocation", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventLocationInfo;Lcom/discord/models/guild/Guild;)V", "", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "", "Lcom/discord/primitives/GuildId;", "guildId", "configureDescription", "(Ljava/lang/String;J)V", "Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;", "eventData", "cardClickListener", "secondaryButtonOnClickListener", "primaryButtonOnClickListener", "shareButtonOnClickListener", "configureInDirectoryEventList", "(Lcom/discord/widgets/hubs/events/HubGuildScheduledEventData;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem$Event;", "item", "rsvpButtonClickListener", "eventStartButtonClickListener", "shareButtonClickListener", "joinButtonClickListener", "configureInEventList", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem$Event;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V", "", "canStartEvent", "isCollapsed", "isConnected", "isCallPreview", "onStartEventButtonClicked", "configureInVoiceChannel", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/models/guild/UserGuildMember;ZZZZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V", "Lcom/discord/api/channel/Channel;", "channel", "isInGuild", "isRsvped", "canConnect", "joinServerButtonClickListener", "configureInChatList", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/UserGuildMember;ZZZZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "eventModel", "configureAsPreview", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/UserGuildMember;)V", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventExternalLocationParser;", "locationParser$delegate", "Lkotlin/Lazy;", "getLocationParser", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventExternalLocationParser;", "locationParser", "Lcom/discord/databinding/GuildScheduledEventItemViewBinding;", "binding", "Lcom/discord/databinding/GuildScheduledEventItemViewBinding;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser;", "descriptionParser$delegate", "getDescriptionParser", "()Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventDescriptionParser;", "descriptionParser", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventItemView extends ConstraintLayout {
    private final GuildScheduledEventItemViewBinding binding;
    private final Lazy descriptionParser$delegate;
    private final Lazy locationParser$delegate;

    public GuildScheduledEventItemView(Context context) {
        this(context, null, 0, 6, null);
    }

    public GuildScheduledEventItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ GuildScheduledEventItemView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    private final void configureDescription(String str, long j) {
        getDescriptionParser().configureDescription(str, j);
    }

    private final void configureInternal(GuildScheduledEvent guildScheduledEvent, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, ButtonConfiguration buttonConfiguration, Guild guild, UserGuildMember userGuildMember, View.OnClickListener onClickListener) {
        String str;
        GuildScheduledEventBottomButtonView guildScheduledEventBottomButtonView = this.binding.f2104b;
        m.checkNotNullExpressionValue(guildScheduledEventBottomButtonView, "binding.guildScheduledEventListItemButtomView");
        guildScheduledEventBottomButtonView.setVisibility(buttonConfiguration.isAnyButtonVisible() ? 0 : 8);
        this.binding.f2104b.configure(buttonConfiguration);
        TextView textView = this.binding.k;
        m.checkNotNullExpressionValue(textView, "binding.guildScheduledEventListItemRsvpText");
        Integer n = guildScheduledEvent.n();
        if (n != null) {
            int intValue = n.intValue();
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            str = StringUtilsKt.format(intValue, context);
        } else {
            str = null;
        }
        ViewExtensions.setTextAndVisibilityBy(textView, str);
        SimpleDraweeView simpleDraweeView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildScheduledEventListItemCreatorAvatar");
        SimpleDraweeViewExtensionsKt.configureCreator(simpleDraweeView, userGuildMember);
        GuildScheduledEventDateView.configure$default(this.binding.e, guildScheduledEvent, false, 2, null);
        TextView textView2 = this.binding.l;
        m.checkNotNullExpressionValue(textView2, "binding.guildScheduledEventListItemTitleText");
        textView2.setText(guildScheduledEvent.j());
        configureDescription(guildScheduledEvent.d(), guildScheduledEvent.h());
        configureLocation(guildScheduledEventLocationInfo, guild);
        this.binding.a.setOnClickListener(onClickListener);
    }

    private final void configureLocation(GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, Guild guild) {
        CharSequence charSequence;
        int i;
        int i2;
        if (guildScheduledEventLocationInfo == null && guild == null) {
            ConstraintLayout constraintLayout = this.binding.c;
            m.checkNotNullExpressionValue(constraintLayout, "binding.guildScheduledEv…ItemChannelGuildContainer");
            constraintLayout.setVisibility(8);
        } else if (guildScheduledEventLocationInfo != null && guild == null) {
            ConstraintLayout constraintLayout2 = this.binding.c;
            m.checkNotNullExpressionValue(constraintLayout2, "binding.guildScheduledEv…ItemChannelGuildContainer");
            constraintLayout2.setVisibility(0);
            SimpleDraweeView simpleDraweeView = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildScheduledEventListItemGuildIcon");
            simpleDraweeView.setVisibility(8);
            TextView textView = this.binding.i;
            m.checkNotNullExpressionValue(textView, "binding.guildScheduledEv…mGuildIconPlaceholderText");
            textView.setVisibility(8);
            TextView textView2 = this.binding.j;
            m.checkNotNullExpressionValue(textView2, "binding.guildScheduledEventListItemGuildName");
            textView2.setVisibility(8);
            TextView textView3 = this.binding.g;
            m.checkNotNullExpressionValue(textView3, "binding.guildScheduledEv…tListItemGuildChannelName");
            textView3.setVisibility(8);
            ImageView imageView = this.binding.m;
            imageView.setVisibility(0);
            imageView.setImageResource(guildScheduledEventLocationInfo.getLocationIcon());
            LinkifiedTextView linkifiedTextView = this.binding.n;
            linkifiedTextView.setVisibility(0);
            GuildScheduledEventExternalLocationParser locationParser = getLocationParser();
            Context context = linkifiedTextView.getContext();
            m.checkNotNullExpressionValue(context, "context");
            linkifiedTextView.setText(locationParser.getTextFromLocation(context, guildScheduledEventLocationInfo));
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.standaloneChanne…xt, locationInfo)\n      }");
        } else if (guild != null) {
            ConstraintLayout constraintLayout3 = this.binding.c;
            m.checkNotNullExpressionValue(constraintLayout3, "binding.guildScheduledEv…ItemChannelGuildContainer");
            constraintLayout3.setVisibility(0);
            ImageView imageView2 = this.binding.m;
            m.checkNotNullExpressionValue(imageView2, "binding.standaloneChannelIcon");
            imageView2.setVisibility(8);
            LinkifiedTextView linkifiedTextView2 = this.binding.n;
            m.checkNotNullExpressionValue(linkifiedTextView2, "binding.standaloneChannelName");
            linkifiedTextView2.setVisibility(8);
            SimpleDraweeView simpleDraweeView2 = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.guildScheduledEventListItemGuildIcon");
            simpleDraweeView2.setVisibility(0);
            TextView textView4 = this.binding.i;
            m.checkNotNullExpressionValue(textView4, "binding.guildScheduledEv…mGuildIconPlaceholderText");
            textView4.setVisibility(0);
            TextView textView5 = this.binding.i;
            textView5.setText(guild.getShortName());
            textView5.setVisibility(guild.hasIcon() ^ true ? 0 : 8);
            SimpleDraweeView simpleDraweeView3 = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.guildScheduledEventListItemGuildIcon");
            SimpleDraweeViewExtensionsKt.setGuildIcon(simpleDraweeView3, true, (r23 & 2) != 0 ? null : guild, getResources().getDimensionPixelSize(R.dimen.guild_scheduled_event_icon_corner_radius), (r23 & 8) != 0 ? null : Integer.valueOf(IconUtils.getMediaProxySize(getResources().getDimensionPixelSize(R.dimen.avatar_size_standard))), (r23 & 16) != 0 ? null : null, (r23 & 32) != 0 ? null : null, (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? false : false, (r23 & 256) != 0 ? null : null);
            TextView textView6 = this.binding.j;
            m.checkNotNullExpressionValue(textView6, "binding.guildScheduledEventListItemGuildName");
            textView6.setText(guild.getName());
            TextView textView7 = this.binding.g;
            m.checkNotNullExpressionValue(textView7, "binding.guildScheduledEv…tListItemGuildChannelName");
            textView7.setVisibility(guildScheduledEventLocationInfo != null ? 0 : 8);
            TextView textView8 = this.binding.g;
            m.checkNotNullExpressionValue(textView8, "binding.guildScheduledEv…tListItemGuildChannelName");
            if (guildScheduledEventLocationInfo != null) {
                GuildScheduledEventExternalLocationParser locationParser2 = getLocationParser();
                Context context2 = getContext();
                m.checkNotNullExpressionValue(context2, "context");
                charSequence = locationParser2.getTextFromLocation(context2, guildScheduledEventLocationInfo);
            } else {
                charSequence = null;
            }
            ViewExtensions.setTextAndVisibilityBy(textView8, charSequence);
            TextView textView9 = this.binding.g;
            if (guildScheduledEventLocationInfo != null) {
                i = guildScheduledEventLocationInfo.getLocationIconSmall();
                i2 = 0;
            } else {
                i2 = 0;
                i = 0;
            }
            textView9.setCompoundDrawablesWithIntrinsicBounds(i, i2, i2, i2);
        }
    }

    public static /* synthetic */ void configureLocation$default(GuildScheduledEventItemView guildScheduledEventItemView, GuildScheduledEventLocationInfo guildScheduledEventLocationInfo, Guild guild, int i, Object obj) {
        if ((i & 2) != 0) {
            guild = null;
        }
        guildScheduledEventItemView.configureLocation(guildScheduledEventLocationInfo, guild);
    }

    private final GuildScheduledEventDescriptionParser getDescriptionParser() {
        return (GuildScheduledEventDescriptionParser) this.descriptionParser$delegate.getValue();
    }

    private final GuildScheduledEventExternalLocationParser getLocationParser() {
        return (GuildScheduledEventExternalLocationParser) this.locationParser$delegate.getValue();
    }

    public final void configureAsPreview(GuildScheduledEventModel guildScheduledEventModel, Channel channel, UserGuildMember userGuildMember) {
        Long l;
        m.checkNotNullParameter(guildScheduledEventModel, "eventModel");
        GuildScheduledEventPickerDateTime guildScheduledEventPickerDateTime = GuildScheduledEventPickerDateTime.INSTANCE;
        long millis = guildScheduledEventPickerDateTime.toMillis(guildScheduledEventModel.getStartDate(), guildScheduledEventModel.getStartTime());
        GuildScheduledEventPickerDate endDate = guildScheduledEventModel.getEndDate();
        if (endDate != null) {
            GuildScheduledEventPickerTime endTime = guildScheduledEventModel.getEndTime();
            l = endTime != null ? Long.valueOf(guildScheduledEventPickerDateTime.toMillis(endDate, endTime)) : null;
        } else {
            l = null;
        }
        ConstraintLayout constraintLayout = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        constraintLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_floating));
        TextView textView = this.binding.k;
        m.checkNotNullExpressionValue(textView, "binding.guildScheduledEventListItemRsvpText");
        textView.setBackgroundTintList(ColorCompat.INSTANCE.createDefaultColorStateList(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundTertiary)));
        SimpleDraweeView simpleDraweeView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildScheduledEventListItemCreatorAvatar");
        SimpleDraweeViewExtensionsKt.configureCreator(simpleDraweeView, userGuildMember);
        TextView textView2 = this.binding.k;
        m.checkNotNullExpressionValue(textView2, "binding.guildScheduledEventListItemRsvpText");
        Integer userCount = guildScheduledEventModel.getUserCount();
        ViewExtensions.setTextAndVisibilityBy(textView2, userCount != null ? String.valueOf(userCount.intValue()) : null);
        GuildScheduledEventBottomButtonView guildScheduledEventBottomButtonView = this.binding.f2104b;
        m.checkNotNullExpressionValue(guildScheduledEventBottomButtonView, "binding.guildScheduledEventListItemButtomView");
        guildScheduledEventBottomButtonView.setVisibility(8);
        TextView textView3 = this.binding.l;
        m.checkNotNullExpressionValue(textView3, "binding.guildScheduledEventListItemTitleText");
        textView3.setText(guildScheduledEventModel.getName());
        configureDescription(guildScheduledEventModel.getDescription(), guildScheduledEventModel.getGuildId());
        this.binding.e.configure(millis, l, guildScheduledEventModel.getEntityType(), GuildScheduledEventStatus.SCHEDULED, (r14 & 16) != 0);
        configureLocation$default(this, GuildScheduledEventLocationInfo.Companion.buildLocationInfo(guildScheduledEventModel, channel), null, 2, null);
    }

    public final void configureInChatList(GuildScheduledEvent guildScheduledEvent, Channel channel, Guild guild, UserGuildMember userGuildMember, boolean z2, boolean z3, boolean z4, boolean z5, View.OnClickListener onClickListener, View.OnClickListener onClickListener2, View.OnClickListener onClickListener3, View.OnClickListener onClickListener4) {
        Drawable drawable;
        CharSequence b2;
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        m.checkNotNullParameter(onClickListener2, "rsvpButtonClickListener");
        m.checkNotNullParameter(onClickListener3, "joinServerButtonClickListener");
        m.checkNotNullParameter(onClickListener4, "shareButtonClickListener");
        configureInternal(guildScheduledEvent, GuildScheduledEventLocationInfo.Companion.buildLocationInfo(guildScheduledEvent, channel), new GuildButtonConfiguration(guildScheduledEvent, true, false, z4, z5, true, z3, false, onClickListener2, onClickListener3, null, onClickListener3, onClickListener4, null, 9344, null), guild, userGuildMember, onClickListener);
        ConstraintLayout constraintLayout = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        if (!z4 || guildScheduledEvent.m() != GuildScheduledEventStatus.ACTIVE) {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_secondary);
        } else {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_connected_secondary);
        }
        constraintLayout.setBackground(drawable);
        TextView textView = this.binding.k;
        m.checkNotNullExpressionValue(textView, "binding.guildScheduledEventListItemRsvpText");
        textView.setBackgroundTintList(ColorCompat.INSTANCE.createDefaultColorStateList(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundTertiary)));
        TextView textView2 = this.binding.l;
        ViewGroup.LayoutParams layoutParams = textView2.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        int i = 8;
        marginLayoutParams.topMargin = DimenUtils.dpToPixels(8);
        textView2.setLayoutParams(marginLayoutParams);
        GuildScheduledEventBottomButtonViewBinding binding = this.binding.f2104b.getBinding();
        TextView textView3 = binding.d;
        m.checkNotNullExpressionValue(textView3, "buttonBinding.primaryButtonText");
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        b2 = b.b(context, R.string.guild_event_invite_embed_join_server_button_label, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        textView3.setText(b2);
        TextView textView4 = binding.d;
        m.checkNotNullExpressionValue(textView4, "buttonBinding.primaryButtonText");
        textView4.setVisibility(z2 ^ true ? 0 : 8);
        ImageView imageView = binding.c;
        m.checkNotNullExpressionValue(imageView, "buttonBinding.primaryButton");
        imageView.setVisibility(z2 ^ true ? 0 : 8);
        ImageView imageView2 = binding.e;
        m.checkNotNullExpressionValue(imageView2, "buttonBinding.secondaryButton");
        imageView2.setVisibility(z2 ? 0 : 8);
        TextView textView5 = binding.f;
        m.checkNotNullExpressionValue(textView5, "buttonBinding.secondaryButtonText");
        if (z2) {
            i = 0;
        }
        textView5.setVisibility(i);
    }

    public final void configureInDirectoryEventList(HubGuildScheduledEventData hubGuildScheduledEventData, View.OnClickListener onClickListener, View.OnClickListener onClickListener2, View.OnClickListener onClickListener3, View.OnClickListener onClickListener4) {
        m.checkNotNullParameter(hubGuildScheduledEventData, "eventData");
        m.checkNotNullParameter(onClickListener, "cardClickListener");
        m.checkNotNullParameter(onClickListener2, "secondaryButtonOnClickListener");
        m.checkNotNullParameter(onClickListener3, "primaryButtonOnClickListener");
        m.checkNotNullParameter(onClickListener4, "shareButtonOnClickListener");
        GuildScheduledEvent event = hubGuildScheduledEventData.getEvent();
        com.discord.api.guild.Guild g = hubGuildScheduledEventData.getEvent().g();
        configureInternal(event, null, new DirectoryButtonConfiguration(hubGuildScheduledEventData.getEvent(), hubGuildScheduledEventData.isInGuild(), hubGuildScheduledEventData.isRsvped(), false, onClickListener3, onClickListener2, onClickListener4, GuildScheduledEventItemView$configureInDirectoryEventList$2.INSTANCE), g != null ? new Guild(g) : null, null, onClickListener);
    }

    public final void configureInEventList(GuildScheduledEventListItem.Event event, View.OnClickListener onClickListener, View.OnClickListener onClickListener2, View.OnClickListener onClickListener3, View.OnClickListener onClickListener4, View.OnClickListener onClickListener5) {
        Drawable drawable;
        m.checkNotNullParameter(event, "item");
        m.checkNotNullParameter(onClickListener, "cardClickListener");
        m.checkNotNullParameter(onClickListener2, "rsvpButtonClickListener");
        m.checkNotNullParameter(onClickListener3, "eventStartButtonClickListener");
        m.checkNotNullParameter(onClickListener4, "shareButtonClickListener");
        m.checkNotNullParameter(onClickListener5, "joinButtonClickListener");
        configureInternal(event.getEvent(), GuildScheduledEventLocationInfo.Companion.buildLocationInfo(event.getEvent(), event.getChannel()), new GuildButtonConfiguration(event.getEvent(), true, event.getCanStartEvent(), event.isConnected(), event.getCanConnect(), true, event.isRsvped(), false, onClickListener2, onClickListener5, null, onClickListener3, onClickListener4, null, 9344, null), null, event.getCreator(), onClickListener);
        ConstraintLayout constraintLayout = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        if (!event.isConnected() || event.getEvent().m() != GuildScheduledEventStatus.ACTIVE) {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_primary);
        } else {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_connected_primary);
        }
        constraintLayout.setBackground(drawable);
    }

    public final void configureInVoiceChannel(GuildScheduledEvent guildScheduledEvent, UserGuildMember userGuildMember, boolean z2, boolean z3, boolean z4, boolean z5, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        Drawable drawable;
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        m.checkNotNullParameter(onClickListener, "onCardClicked");
        m.checkNotNullParameter(onClickListener2, "onStartEventButtonClicked");
        configureInternal(guildScheduledEvent, null, new GuildButtonConfiguration(guildScheduledEvent, false, z2, z4, false, true, false, false, null, null, null, onClickListener2, null, null, 14208, null), null, userGuildMember, onClickListener);
        ConstraintLayout constraintLayout = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        if (!z4 || guildScheduledEvent.m() != GuildScheduledEventStatus.ACTIVE) {
            if (z5) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_dark);
            } else {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_secondary);
            }
        } else if (z5) {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_connected_dark);
        } else {
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_guild_scheduled_event_list_item_connected_secondary);
        }
        constraintLayout.setBackground(drawable);
        TextView textView = this.binding.k;
        m.checkNotNullExpressionValue(textView, "binding.guildScheduledEventListItemRsvpText");
        int i = 8;
        textView.setVisibility(8);
        if (z3) {
            LinkifiedTextView linkifiedTextView = this.binding.f;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.guildScheduledEventListItemDescText");
            linkifiedTextView.setVisibility(8);
        }
        GuildScheduledEventBottomButtonView guildScheduledEventBottomButtonView = this.binding.f2104b;
        m.checkNotNullExpressionValue(guildScheduledEventBottomButtonView, "binding.guildScheduledEventListItemButtomView");
        if (z2 && !z3) {
            i = 0;
        }
        guildScheduledEventBottomButtonView.setVisibility(i);
        TextView textView2 = this.binding.l;
        if (z3) {
            textView2.setMaxLines(Integer.MAX_VALUE);
            textView2.setEllipsize(null);
            return;
        }
        textView2.setMaxLines(1);
        textView2.setEllipsize(TextUtils.TruncateAt.END);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildScheduledEventItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.guild_scheduled_event_item_view, (ViewGroup) this, false);
        addView(inflate);
        int i2 = R.id.guild_scheduled_event_list_item_buttom_view;
        GuildScheduledEventBottomButtonView guildScheduledEventBottomButtonView = (GuildScheduledEventBottomButtonView) inflate.findViewById(R.id.guild_scheduled_event_list_item_buttom_view);
        if (guildScheduledEventBottomButtonView != null) {
            i2 = R.id.guild_scheduled_event_list_item_channel_guild_container;
            ConstraintLayout constraintLayout = (ConstraintLayout) inflate.findViewById(R.id.guild_scheduled_event_list_item_channel_guild_container);
            if (constraintLayout != null) {
                i2 = R.id.guild_scheduled_event_list_item_creator_avatar;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.guild_scheduled_event_list_item_creator_avatar);
                if (simpleDraweeView != null) {
                    i2 = R.id.guild_scheduled_event_list_item_date_view;
                    GuildScheduledEventDateView guildScheduledEventDateView = (GuildScheduledEventDateView) inflate.findViewById(R.id.guild_scheduled_event_list_item_date_view);
                    if (guildScheduledEventDateView != null) {
                        i2 = R.id.guild_scheduled_event_list_item_desc_text;
                        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) inflate.findViewById(R.id.guild_scheduled_event_list_item_desc_text);
                        if (linkifiedTextView != null) {
                            i2 = R.id.guild_scheduled_event_list_item_guild_channel_name;
                            TextView textView = (TextView) inflate.findViewById(R.id.guild_scheduled_event_list_item_guild_channel_name);
                            if (textView != null) {
                                i2 = R.id.guild_scheduled_event_list_item_guild_icon;
                                SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) inflate.findViewById(R.id.guild_scheduled_event_list_item_guild_icon);
                                if (simpleDraweeView2 != null) {
                                    i2 = R.id.guild_scheduled_event_list_item_guild_icon_placeholder_text;
                                    TextView textView2 = (TextView) inflate.findViewById(R.id.guild_scheduled_event_list_item_guild_icon_placeholder_text);
                                    if (textView2 != null) {
                                        i2 = R.id.guild_scheduled_event_list_item_guild_name;
                                        TextView textView3 = (TextView) inflate.findViewById(R.id.guild_scheduled_event_list_item_guild_name);
                                        if (textView3 != null) {
                                            i2 = R.id.guild_scheduled_event_list_item_rsvp_text;
                                            TextView textView4 = (TextView) inflate.findViewById(R.id.guild_scheduled_event_list_item_rsvp_text);
                                            if (textView4 != null) {
                                                i2 = R.id.guild_scheduled_event_list_item_title_text;
                                                TextView textView5 = (TextView) inflate.findViewById(R.id.guild_scheduled_event_list_item_title_text);
                                                if (textView5 != null) {
                                                    i2 = R.id.header_barrier;
                                                    Barrier barrier = (Barrier) inflate.findViewById(R.id.header_barrier);
                                                    if (barrier != null) {
                                                        i2 = R.id.standalone_channel_icon;
                                                        ImageView imageView = (ImageView) inflate.findViewById(R.id.standalone_channel_icon);
                                                        if (imageView != null) {
                                                            i2 = R.id.standalone_channel_name;
                                                            LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) inflate.findViewById(R.id.standalone_channel_name);
                                                            if (linkifiedTextView2 != null) {
                                                                GuildScheduledEventItemViewBinding guildScheduledEventItemViewBinding = new GuildScheduledEventItemViewBinding((ConstraintLayout) inflate, guildScheduledEventBottomButtonView, constraintLayout, simpleDraweeView, guildScheduledEventDateView, linkifiedTextView, textView, simpleDraweeView2, textView2, textView3, textView4, textView5, barrier, imageView, linkifiedTextView2);
                                                                m.checkNotNullExpressionValue(guildScheduledEventItemViewBinding, "GuildScheduledEventItemV…rom(context), this, true)");
                                                                this.binding = guildScheduledEventItemViewBinding;
                                                                this.descriptionParser$delegate = g.lazy(new GuildScheduledEventItemView$descriptionParser$2(this));
                                                                this.locationParser$delegate = g.lazy(GuildScheduledEventItemView$locationParser$2.INSTANCE);
                                                                m.checkNotNullExpressionValue(linkifiedTextView, "binding.guildScheduledEventListItemDescText");
                                                                new TextViewFadeHelper(linkifiedTextView).registerFadeHelper();
                                                                return;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }
}
