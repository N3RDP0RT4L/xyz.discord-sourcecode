package com.discord.widgets.guildscheduledevent;

import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import androidx.core.content.ContextCompat;
import b.d.b.a.a;
import com.discord.i18n.Hook;
import com.discord.i18n.RenderContext;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.widgets.guildscheduledevent.PreviewGuildScheduledEventViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetPreviewGuildScheduledEvent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetPreviewGuildScheduledEvent$configureTextForCreation$2 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ String $channelName;
    public final /* synthetic */ PreviewGuildScheduledEventViewModel.ViewState.Initialized $viewState;
    public final /* synthetic */ WidgetPreviewGuildScheduledEvent this$0;

    /* compiled from: WidgetPreviewGuildScheduledEvent.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/Hook;", "", "invoke", "(Lcom/discord/i18n/Hook;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.guildscheduledevent.WidgetPreviewGuildScheduledEvent$configureTextForCreation$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Hook, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Hook hook) {
            invoke2(hook);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Hook hook) {
            m.checkNotNullParameter(hook, "$receiver");
            StringBuilder R = a.R(". ");
            R.append(WidgetPreviewGuildScheduledEvent$configureTextForCreation$2.this.$channelName);
            SpannableString spannableString = new SpannableString(R.toString());
            int dpToPixels = DimenUtils.dpToPixels(20);
            Drawable drawable = ContextCompat.getDrawable(WidgetPreviewGuildScheduledEvent$configureTextForCreation$2.this.this$0.requireContext(), WidgetPreviewGuildScheduledEvent$configureTextForCreation$2.this.$viewState.getLocationInfo().getLocationIcon());
            m.checkNotNull(drawable);
            drawable.setBounds(0, 0, dpToPixels, dpToPixels);
            m.checkNotNullExpressionValue(drawable, "ContextCompat.getDrawabl…ze)\n                    }");
            spannableString.setSpan(new ImageSpan(drawable), 0, 1, 17);
            hook.f2681b = spannableString;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetPreviewGuildScheduledEvent$configureTextForCreation$2(WidgetPreviewGuildScheduledEvent widgetPreviewGuildScheduledEvent, String str, PreviewGuildScheduledEventViewModel.ViewState.Initialized initialized) {
        super(1);
        this.this$0 = widgetPreviewGuildScheduledEvent;
        this.$channelName = str;
        this.$viewState = initialized;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        renderContext.a.put("channelName", this.$channelName);
        renderContext.a("channelHook", new AnonymousClass1());
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }
}
