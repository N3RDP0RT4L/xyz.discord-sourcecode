package com.discord.widgets.guildscheduledevent;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.models.guild.UserGuildMember;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildScheduledEventListItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", "Companion", "Event", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem$Event;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildScheduledEventListItem implements MGRecyclerDataPayload {
    public static final Companion Companion = new Companion(null);
    public static final int TYPE_EVENT = 0;

    /* compiled from: GuildScheduledEventListItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem$Companion;", "", "", "TYPE_EVENT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildScheduledEventListItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001BK\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u0015\u001a\u00020\u000b\u0012\u0006\u0010\u0016\u001a\u00020\u000b\u0012\u0006\u0010\u0017\u001a\u00020\u000b\u0012\u0006\u0010\u0018\u001a\u00020\u000b\u0012\u0006\u0010\u0019\u001a\u00020\u000b¢\u0006\u0004\b6\u00107J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u0010\u0010\rJ\u0010\u0010\u0011\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u0011\u0010\rJd\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u0015\u001a\u00020\u000b2\b\b\u0002\u0010\u0016\u001a\u00020\u000b2\b\b\u0002\u0010\u0017\u001a\u00020\u000b2\b\b\u0002\u0010\u0018\u001a\u00020\u000b2\b\b\u0002\u0010\u0019\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010$\u001a\u00020\u000b2\b\u0010#\u001a\u0004\u0018\u00010\"HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0017\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\rR\u001c\u0010(\u001a\u00020\u001c8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010\u001eR\u0019\u0010\u0019\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010&\u001a\u0004\b+\u0010\rR\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010,\u001a\u0004\b-\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b.\u0010\rR\u001c\u0010/\u001a\u00020\u001f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010!R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u00102\u001a\u0004\b3\u0010\u0007R\u0019\u0010\u0018\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010&\u001a\u0004\b\u0018\u0010\rR\u0019\u0010\u0015\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b\u0015\u0010\rR\u001b\u0010\u0014\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u00104\u001a\u0004\b5\u0010\n¨\u00068"}, d2 = {"Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem$Event;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/UserGuildMember;", "component3", "()Lcom/discord/models/guild/UserGuildMember;", "", "component4", "()Z", "component5", "component6", "component7", "component8", "event", "channel", "creator", "isRsvped", "canStartEvent", "canShare", "isConnected", "canConnect", "copy", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/UserGuildMember;ZZZZZ)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventListItem$Event;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanShare", "key", "Ljava/lang/String;", "getKey", "getCanConnect", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getEvent", "getCanStartEvent", "type", "I", "getType", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/guild/UserGuildMember;", "getCreator", HookHelper.constructorName, "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/UserGuildMember;ZZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Event extends GuildScheduledEventListItem {
        private final boolean canConnect;
        private final boolean canShare;
        private final boolean canStartEvent;
        private final Channel channel;
        private final UserGuildMember creator;
        private final GuildScheduledEvent event;
        private final boolean isConnected;
        private final boolean isRsvped;
        private final String key;
        private final int type;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Event(GuildScheduledEvent guildScheduledEvent, Channel channel, UserGuildMember userGuildMember, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
            super(null);
            m.checkNotNullParameter(guildScheduledEvent, "event");
            this.event = guildScheduledEvent;
            this.channel = channel;
            this.creator = userGuildMember;
            this.isRsvped = z2;
            this.canStartEvent = z3;
            this.canShare = z4;
            this.isConnected = z5;
            this.canConnect = z6;
            this.key = String.valueOf(guildScheduledEvent.i());
        }

        public final GuildScheduledEvent component1() {
            return this.event;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final UserGuildMember component3() {
            return this.creator;
        }

        public final boolean component4() {
            return this.isRsvped;
        }

        public final boolean component5() {
            return this.canStartEvent;
        }

        public final boolean component6() {
            return this.canShare;
        }

        public final boolean component7() {
            return this.isConnected;
        }

        public final boolean component8() {
            return this.canConnect;
        }

        public final Event copy(GuildScheduledEvent guildScheduledEvent, Channel channel, UserGuildMember userGuildMember, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
            m.checkNotNullParameter(guildScheduledEvent, "event");
            return new Event(guildScheduledEvent, channel, userGuildMember, z2, z3, z4, z5, z6);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Event)) {
                return false;
            }
            Event event = (Event) obj;
            return m.areEqual(this.event, event.event) && m.areEqual(this.channel, event.channel) && m.areEqual(this.creator, event.creator) && this.isRsvped == event.isRsvped && this.canStartEvent == event.canStartEvent && this.canShare == event.canShare && this.isConnected == event.isConnected && this.canConnect == event.canConnect;
        }

        public final boolean getCanConnect() {
            return this.canConnect;
        }

        public final boolean getCanShare() {
            return this.canShare;
        }

        public final boolean getCanStartEvent() {
            return this.canStartEvent;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final UserGuildMember getCreator() {
            return this.creator;
        }

        public final GuildScheduledEvent getEvent() {
            return this.event;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            GuildScheduledEvent guildScheduledEvent = this.event;
            int i = 0;
            int hashCode = (guildScheduledEvent != null ? guildScheduledEvent.hashCode() : 0) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            UserGuildMember userGuildMember = this.creator;
            if (userGuildMember != null) {
                i = userGuildMember.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isRsvped;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.canStartEvent;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.canShare;
            if (z4) {
                z4 = true;
            }
            int i10 = z4 ? 1 : 0;
            int i11 = z4 ? 1 : 0;
            int i12 = (i9 + i10) * 31;
            boolean z5 = this.isConnected;
            if (z5) {
                z5 = true;
            }
            int i13 = z5 ? 1 : 0;
            int i14 = z5 ? 1 : 0;
            int i15 = (i12 + i13) * 31;
            boolean z6 = this.canConnect;
            if (!z6) {
                i3 = z6 ? 1 : 0;
            }
            return i15 + i3;
        }

        public final boolean isConnected() {
            return this.isConnected;
        }

        public final boolean isRsvped() {
            return this.isRsvped;
        }

        public String toString() {
            StringBuilder R = a.R("Event(event=");
            R.append(this.event);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", creator=");
            R.append(this.creator);
            R.append(", isRsvped=");
            R.append(this.isRsvped);
            R.append(", canStartEvent=");
            R.append(this.canStartEvent);
            R.append(", canShare=");
            R.append(this.canShare);
            R.append(", isConnected=");
            R.append(this.isConnected);
            R.append(", canConnect=");
            return a.M(R, this.canConnect, ")");
        }
    }

    private GuildScheduledEventListItem() {
    }

    public /* synthetic */ GuildScheduledEventListItem(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
