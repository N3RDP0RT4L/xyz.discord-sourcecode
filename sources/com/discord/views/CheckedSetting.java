package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.method.LinkMovementMethod;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.StringRes;
import androidx.constraintlayout.motion.widget.Key;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import b.a.i.l3;
import b.a.i.m3;
import b.a.i.n3;
import b.a.i.o3;
import b.a.k.b;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.functions.Action0;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: CheckedSetting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002:\u0002H(J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\t\u001a\u00020\bH\u0014¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\bH\u0014¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0010\u001a\u00020\u00052\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u000eH\u0014¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0012\u001a\u00020\u00052\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u000eH\u0014¢\u0006\u0004\b\u0012\u0010\u0011J\u000f\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\u001d\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\u0013¢\u0006\u0004\b\u001c\u0010\u001dJ\u001d\u0010 \u001a\u00020\u00052\u000e\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u001e¢\u0006\u0004\b \u0010!J\u0015\u0010$\u001a\u00020\u00052\u0006\u0010#\u001a\u00020\"¢\u0006\u0004\b$\u0010%J\u0017\u0010(\u001a\u00020\u00052\b\b\u0001\u0010'\u001a\u00020&¢\u0006\u0004\b(\u0010)J\u001b\u0010,\u001a\u00020\u00052\n\b\u0002\u0010+\u001a\u0004\u0018\u00010*H\u0007¢\u0006\u0004\b,\u0010-J\u0017\u0010/\u001a\u00020\u00052\b\u0010.\u001a\u0004\u0018\u00010*¢\u0006\u0004\b/\u0010-J\u0015\u00102\u001a\u00020\u00052\u0006\u00101\u001a\u000200¢\u0006\u0004\b2\u00103J\u0015\u00105\u001a\u00020\u00052\u0006\u00104\u001a\u00020\u0013¢\u0006\u0004\b5\u0010\u001aJ\u0017\u00107\u001a\u00020\u00052\b\b\u0001\u00106\u001a\u00020&¢\u0006\u0004\b7\u0010)J\u0017\u00109\u001a\u00020\u00052\b\b\u0001\u00108\u001a\u00020&¢\u0006\u0004\b9\u0010)J\u0015\u0010:\u001a\u00020\u00052\u0006\u00104\u001a\u00020\u0013¢\u0006\u0004\b:\u0010\u001aJ#\u0010<\u001a\u00020\u00052\b\u0010.\u001a\u0004\u0018\u00010*2\b\b\u0002\u0010;\u001a\u00020\u0013H\u0007¢\u0006\u0004\b<\u0010=J\u0017\u0010>\u001a\u00020\u00052\b\b\u0001\u00106\u001a\u00020&¢\u0006\u0004\b>\u0010)J\u0015\u0010@\u001a\u00020\u00052\u0006\u0010#\u001a\u00020?¢\u0006\u0004\b@\u0010AR\u001e\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010CR\u0016\u0010G\u001a\u00020D8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bE\u0010F¨\u0006I"}, d2 = {"Lcom/discord/views/CheckedSetting;", "Landroid/widget/RelativeLayout;", "Landroid/widget/Checkable;", "Lcom/discord/views/CheckedSetting$ViewType;", "viewType", "", "f", "(Lcom/discord/views/CheckedSetting$ViewType;)V", "Landroid/os/Parcelable;", "onSaveInstanceState", "()Landroid/os/Parcelable;", "state", "onRestoreInstanceState", "(Landroid/os/Parcelable;)V", "Landroid/util/SparseArray;", "container", "dispatchSaveInstanceState", "(Landroid/util/SparseArray;)V", "dispatchRestoreInstanceState", "", "isChecked", "()Z", "toggle", "()V", "checked", "setChecked", "(Z)V", "reportChange", "g", "(ZZ)V", "Lrx/functions/Action1;", "checkedListener", "setOnCheckedListener", "(Lrx/functions/Action1;)V", "Landroid/view/View$OnClickListener;", "listener", "e", "(Landroid/view/View$OnClickListener;)V", "", "disabledToastRes", "b", "(I)V", "", "disabledToastText", "c", "(Ljava/lang/CharSequence;)V", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setText", "", Key.ALPHA, "setButtonAlpha", "(F)V", "isVisible", "setButtonVisibility", ModelAuditLogEntry.CHANGE_KEY_COLOR, "setTextColor", "resId", "setLabelTagText", "setLabelTagVisibility", "containsClickables", "h", "(Ljava/lang/CharSequence;Z)V", "setSubtextColor", "Lrx/functions/Action0;", "setSubtextOnClickListener", "(Lrx/functions/Action0;)V", "k", "Lrx/functions/Action1;", "Lcom/discord/views/CheckedSetting$b;", "l", "Lcom/discord/views/CheckedSetting$b;", "binding", "ViewType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CheckedSetting extends RelativeLayout implements Checkable {
    public static final /* synthetic */ int j = 0;
    public Action1<Boolean> k;
    public b l;

    /* compiled from: CheckedSetting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/views/CheckedSetting$ViewType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "a", "CHECK", "RADIO", "SWITCH", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ViewType {
        CHECK,
        RADIO,
        SWITCH;
        
        public static final a Companion = new a(null);
        private static final ViewType[] VALUES = values();

        /* compiled from: CheckedSetting.kt */
        /* loaded from: classes2.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }
    }

    /* compiled from: java-style lambda group */
    /* loaded from: classes.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                CheckedSetting.a((CheckedSetting) this.k).c().setChecked(true);
            } else if (i == 1) {
                CheckedSetting.a((CheckedSetting) this.k).c().setChecked(true ^ CheckedSetting.a((CheckedSetting) this.k).c().isChecked());
            } else {
                throw null;
            }
        }
    }

    /* compiled from: CheckedSetting.kt */
    /* loaded from: classes2.dex */
    public interface b {

        /* compiled from: CheckedSetting.kt */
        /* loaded from: classes2.dex */
        public static final class a extends c {
            public final ConstraintLayout e;
            public final MaterialCheckBox f;

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public a(b.a.i.l3 r5) {
                /*
                    r4 = this;
                    java.lang.String r0 = "binding"
                    d0.z.d.m.checkNotNullParameter(r5, r0)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r5.c
                    java.lang.String r1 = "binding.settingContainer"
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r2 = 0
                    r3 = 2
                    r4.<init>(r0, r2, r3)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r5.c
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r4.e = r0
                    com.google.android.material.checkbox.MaterialCheckBox r5 = r5.f151b
                    java.lang.String r0 = "binding.settingButton"
                    d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                    r4.f = r5
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.views.CheckedSetting.b.a.<init>(b.a.i.l3):void");
            }

            @Override // com.discord.views.CheckedSetting.b
            public View b() {
                return this.e;
            }

            @Override // com.discord.views.CheckedSetting.b
            public CompoundButton c() {
                return this.f;
            }
        }

        /* compiled from: CheckedSetting.kt */
        /* renamed from: com.discord.views.CheckedSetting$b$b  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0228b extends c {
            public final ConstraintLayout e;
            public final MaterialRadioButton f;

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public C0228b(b.a.i.m3 r5) {
                /*
                    r4 = this;
                    java.lang.String r0 = "binding"
                    d0.z.d.m.checkNotNullParameter(r5, r0)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r5.c
                    java.lang.String r1 = "binding.settingContainer"
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r2 = 0
                    r3 = 2
                    r4.<init>(r0, r2, r3)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r5.c
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r4.e = r0
                    com.google.android.material.radiobutton.MaterialRadioButton r5 = r5.f158b
                    java.lang.String r0 = "binding.settingButton"
                    d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                    r4.f = r5
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.views.CheckedSetting.b.C0228b.<init>(b.a.i.m3):void");
            }

            @Override // com.discord.views.CheckedSetting.b
            public View b() {
                return this.e;
            }

            @Override // com.discord.views.CheckedSetting.b
            public CompoundButton c() {
                return this.f;
            }
        }

        /* compiled from: CheckedSetting.kt */
        /* loaded from: classes2.dex */
        public static abstract class c implements b {
            public final TextView a;

            /* renamed from: b  reason: collision with root package name */
            public final TextView f2796b;
            public final LinkifiedTextView c;
            public final ImageView d;

            public c(ViewGroup viewGroup, n3 n3Var, int i) {
                n3 n3Var2;
                if ((i & 2) != 0) {
                    int i2 = R.id.setting_drawable_left;
                    ImageView imageView = (ImageView) viewGroup.findViewById(R.id.setting_drawable_left);
                    if (imageView != null) {
                        i2 = R.id.setting_label;
                        TextView textView = (TextView) viewGroup.findViewById(R.id.setting_label);
                        if (textView != null) {
                            i2 = R.id.setting_subtext;
                            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) viewGroup.findViewById(R.id.setting_subtext);
                            if (linkifiedTextView != null) {
                                i2 = R.id.setting_tag;
                                TextView textView2 = (TextView) viewGroup.findViewById(R.id.setting_tag);
                                if (textView2 != null) {
                                    n3Var2 = new n3(viewGroup, imageView, textView, linkifiedTextView, textView2);
                                    m.checkNotNullExpressionValue(n3Var2, "ViewSettingSharedBinding.bind(container)");
                                }
                            }
                        }
                    }
                    throw new NullPointerException("Missing required view with ID: ".concat(viewGroup.getResources().getResourceName(i2)));
                }
                n3Var2 = null;
                m.checkNotNullParameter(viewGroup, "container");
                m.checkNotNullParameter(n3Var2, "binding");
                TextView textView3 = n3Var2.c;
                m.checkNotNullExpressionValue(textView3, "binding.settingLabel");
                this.a = textView3;
                TextView textView4 = n3Var2.e;
                m.checkNotNullExpressionValue(textView4, "binding.settingTag");
                this.f2796b = textView4;
                LinkifiedTextView linkifiedTextView2 = n3Var2.d;
                m.checkNotNullExpressionValue(linkifiedTextView2, "binding.settingSubtext");
                this.c = linkifiedTextView2;
                ImageView imageView2 = n3Var2.f163b;
                m.checkNotNullExpressionValue(imageView2, "binding.settingDrawableLeft");
                this.d = imageView2;
            }

            @Override // com.discord.views.CheckedSetting.b
            public TextView a() {
                return this.a;
            }

            @Override // com.discord.views.CheckedSetting.b
            public ImageView d() {
                return this.d;
            }

            @Override // com.discord.views.CheckedSetting.b
            public TextView e() {
                return this.f2796b;
            }

            @Override // com.discord.views.CheckedSetting.b
            public TextView f() {
                return this.c;
            }
        }

        /* compiled from: CheckedSetting.kt */
        /* loaded from: classes2.dex */
        public static final class d extends c {
            public final ConstraintLayout e;
            public final SwitchMaterial f;

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public d(b.a.i.o3 r5) {
                /*
                    r4 = this;
                    java.lang.String r0 = "binding"
                    d0.z.d.m.checkNotNullParameter(r5, r0)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r5.c
                    java.lang.String r1 = "binding.settingContainer"
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r2 = 0
                    r3 = 2
                    r4.<init>(r0, r2, r3)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r5.c
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r4.e = r0
                    com.google.android.material.switchmaterial.SwitchMaterial r5 = r5.f168b
                    java.lang.String r0 = "binding.settingButton"
                    d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                    r4.f = r5
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.views.CheckedSetting.b.d.<init>(b.a.i.o3):void");
            }

            @Override // com.discord.views.CheckedSetting.b
            public View b() {
                return this.e;
            }

            @Override // com.discord.views.CheckedSetting.b
            public CompoundButton c() {
                return this.f;
            }
        }

        TextView a();

        View b();

        CompoundButton c();

        ImageView d();

        TextView e();

        TextView f();
    }

    /* compiled from: CheckedSetting.kt */
    /* loaded from: classes2.dex */
    public static final class c implements View.OnClickListener {
        public final /* synthetic */ CharSequence k;

        public c(CharSequence charSequence) {
            this.k = charSequence;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            if (this.k != null) {
                b.a.d.m.h(CheckedSetting.this.getContext(), this.k, 0, null, 12);
            }
        }
    }

    /* compiled from: CheckedSetting.kt */
    /* loaded from: classes2.dex */
    public static final class d extends AccessibilityDelegateCompat {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ViewType f2797b;

        public d(ViewType viewType) {
            this.f2797b = viewType;
        }

        @Override // androidx.core.view.AccessibilityDelegateCompat
        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            m.checkNotNullParameter(view, "host");
            m.checkNotNullParameter(accessibilityEvent, "event");
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setChecked(CheckedSetting.this.isChecked());
        }

        @Override // androidx.core.view.AccessibilityDelegateCompat
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            int i;
            m.checkNotNullParameter(view, "host");
            m.checkNotNullParameter(accessibilityNodeInfoCompat, "info");
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
            accessibilityNodeInfoCompat.setCheckable(true);
            accessibilityNodeInfoCompat.setChecked(CheckedSetting.this.isChecked());
            Resources resources = view.getResources();
            int ordinal = this.f2797b.ordinal();
            if (ordinal == 0) {
                i = R.string.a11y_role_checkbox;
            } else if (ordinal == 1) {
                i = R.string.a11y_role_radio_button;
            } else if (ordinal == 2) {
                i = R.string.a11y_role_switch;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            accessibilityNodeInfoCompat.setRoleDescription(resources.getString(i));
        }
    }

    /* compiled from: CheckedSetting.kt */
    /* loaded from: classes2.dex */
    public static final class e implements CompoundButton.OnCheckedChangeListener {
        public final /* synthetic */ Action1 a;

        public e(Action1 action1) {
            this.a = action1;
        }

        @Override // android.widget.CompoundButton.OnCheckedChangeListener
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
            Action1 action1 = this.a;
            if (action1 != null) {
                action1.call(Boolean.valueOf(z2));
            }
        }
    }

    /* compiled from: CheckedSetting.kt */
    /* loaded from: classes2.dex */
    public static final class f implements View.OnClickListener {
        public final /* synthetic */ Action0 j;

        public f(Action0 action0) {
            this.j = action0;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            this.j.call();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Removed duplicated region for block: B:65:0x012f A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /* JADX WARN: Removed duplicated region for block: B:69:0x013e A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0164 A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x017a A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /* JADX WARN: Removed duplicated region for block: B:90:0x018e A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /* JADX WARN: Removed duplicated region for block: B:91:0x0192 A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /* JADX WARN: Removed duplicated region for block: B:94:0x019e A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /* JADX WARN: Removed duplicated region for block: B:97:0x01a7 A[Catch: all -> 0x01b9, TryCatch #0 {all -> 0x01b9, blocks: (B:6:0x0025, B:9:0x0043, B:10:0x0046, B:12:0x0052, B:13:0x0055, B:15:0x006f, B:16:0x0072, B:18:0x0086, B:20:0x008a, B:21:0x008d, B:22:0x0094, B:24:0x009c, B:26:0x00a0, B:27:0x00a3, B:28:0x00aa, B:30:0x00ae, B:31:0x00b1, B:33:0x00be, B:35:0x00c6, B:37:0x00cd, B:38:0x00d0, B:46:0x00e7, B:48:0x00f3, B:49:0x00f6, B:51:0x00ff, B:53:0x0103, B:54:0x0106, B:55:0x0115, B:57:0x011b, B:59:0x0123, B:65:0x012f, B:67:0x0133, B:68:0x0136, B:69:0x013e, B:71:0x0142, B:72:0x0145, B:74:0x0150, B:75:0x0153, B:76:0x015a, B:78:0x0164, B:80:0x0168, B:81:0x016b, B:82:0x0172, B:84:0x017a, B:86:0x017e, B:87:0x0181, B:88:0x0188, B:90:0x018e, B:91:0x0192, B:92:0x0198, B:94:0x019e, B:95:0x01a1, B:97:0x01a7, B:99:0x01ab, B:100:0x01ae), top: B:106:0x0025 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public CheckedSetting(android.content.Context r12, android.util.AttributeSet r13) {
        /*
            Method dump skipped, instructions count: 446
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.CheckedSetting.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public static final /* synthetic */ b a(CheckedSetting checkedSetting) {
        b bVar = checkedSetting.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        return bVar;
    }

    public static /* synthetic */ void d(CheckedSetting checkedSetting, CharSequence charSequence, int i) {
        int i2 = i & 1;
        checkedSetting.c(null);
    }

    public static /* synthetic */ void i(CheckedSetting checkedSetting, CharSequence charSequence, boolean z2, int i) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        checkedSetting.h(charSequence, z2);
    }

    public final void b(@StringRes int i) {
        CharSequence b2;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        b2 = b.a.k.b.b(context, i, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        c(b2);
    }

    public final void c(CharSequence charSequence) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.b().setOnClickListener(new c(charSequence));
        setAlpha(0.3f);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        m.checkNotNullParameter(sparseArray, "container");
        dispatchThawSelfOnly(sparseArray);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        m.checkNotNullParameter(sparseArray, "container");
        dispatchFreezeSelfOnly(sparseArray);
    }

    public final void e(View.OnClickListener onClickListener) {
        m.checkNotNullParameter(onClickListener, "listener");
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.b().setOnClickListener(onClickListener);
        setAlpha(1.0f);
    }

    public final void f(ViewType viewType) {
        b bVar;
        LayoutInflater from = LayoutInflater.from(getContext());
        int ordinal = viewType.ordinal();
        int i = R.id.setting_button;
        if (ordinal == 0) {
            from.inflate(R.layout.view_setting_check, this);
            MaterialCheckBox materialCheckBox = (MaterialCheckBox) findViewById(R.id.setting_button);
            if (materialCheckBox != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(R.id.setting_container);
                if (constraintLayout != null) {
                    l3 l3Var = new l3(this, materialCheckBox, constraintLayout);
                    m.checkNotNullExpressionValue(l3Var, "ViewSettingCheckBinding.…ate(layoutInflater, this)");
                    m.checkNotNullParameter(l3Var, "binding");
                    bVar = new b.a(l3Var);
                } else {
                    i = R.id.setting_container;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
        } else if (ordinal == 1) {
            from.inflate(R.layout.view_setting_radio, this);
            MaterialRadioButton materialRadioButton = (MaterialRadioButton) findViewById(R.id.setting_button);
            if (materialRadioButton != null) {
                ConstraintLayout constraintLayout2 = (ConstraintLayout) findViewById(R.id.setting_container);
                if (constraintLayout2 != null) {
                    m3 m3Var = new m3(this, materialRadioButton, constraintLayout2);
                    m.checkNotNullExpressionValue(m3Var, "ViewSettingRadioBinding.…ate(layoutInflater, this)");
                    m.checkNotNullParameter(m3Var, "binding");
                    bVar = new b.C0228b(m3Var);
                } else {
                    i = R.id.setting_container;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
        } else if (ordinal == 2) {
            from.inflate(R.layout.view_setting_switch, this);
            SwitchMaterial switchMaterial = (SwitchMaterial) findViewById(R.id.setting_button);
            if (switchMaterial != null) {
                ConstraintLayout constraintLayout3 = (ConstraintLayout) findViewById(R.id.setting_container);
                if (constraintLayout3 != null) {
                    o3 o3Var = new o3(this, switchMaterial, constraintLayout3);
                    m.checkNotNullExpressionValue(o3Var, "ViewSettingSwitchBinding…ate(layoutInflater, this)");
                    m.checkNotNullParameter(o3Var, "binding");
                    bVar = new b.d(o3Var);
                } else {
                    i = R.id.setting_container;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
        } else {
            throw new NoWhenBranchMatchedException();
        }
        this.l = bVar;
        if (viewType == ViewType.RADIO) {
            bVar.b().setOnClickListener(new a(0, this));
        } else {
            bVar.b().setOnClickListener(new a(1, this));
        }
        b bVar2 = this.l;
        if (bVar2 == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        ViewCompat.setAccessibilityDelegate(bVar2.b(), new d(viewType));
    }

    public final void g(boolean z2, boolean z3) {
        if (!z3) {
            b bVar = this.l;
            if (bVar == null) {
                m.throwUninitializedPropertyAccessException("binding");
            }
            bVar.c().setOnCheckedChangeListener(null);
            b bVar2 = this.l;
            if (bVar2 == null) {
                m.throwUninitializedPropertyAccessException("binding");
            }
            bVar2.c().setChecked(z2);
            setOnCheckedListener(this.k);
            return;
        }
        b bVar3 = this.l;
        if (bVar3 == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar3.c().setChecked(z2);
    }

    public final void h(CharSequence charSequence, boolean z2) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.f().setText(charSequence);
        b bVar2 = this.l;
        if (bVar2 == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        TextView f2 = bVar2.f();
        int i = 0;
        if (charSequence == null || charSequence.length() == 0) {
            i = 8;
        }
        f2.setVisibility(i);
        if (z2) {
            b bVar3 = this.l;
            if (bVar3 == null) {
                m.throwUninitializedPropertyAccessException("binding");
            }
            bVar3.f().setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    @Override // android.widget.Checkable
    public boolean isChecked() {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        return bVar.c().isChecked();
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        m.checkNotNullParameter(parcelable, "state");
        if (parcelable instanceof Bundle) {
            b bVar = this.l;
            if (bVar == null) {
                m.throwUninitializedPropertyAccessException("binding");
            }
            Bundle bundle = (Bundle) parcelable;
            bVar.c().setChecked(bundle.getBoolean("STATE_CHECKED"));
            parcelable2 = bundle.getParcelable("STATE_SUPER");
        } else {
            parcelable2 = null;
        }
        if (parcelable2 != null) {
            parcelable = parcelable2;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("STATE_SUPER", super.onSaveInstanceState());
        bundle.putBoolean("STATE_CHECKED", isChecked());
        return bundle;
    }

    public final void setButtonAlpha(float f2) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.c().setAlpha(f2);
    }

    public final void setButtonVisibility(boolean z2) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.c().setVisibility(z2 ? 0 : 8);
    }

    @Override // android.widget.Checkable
    public void setChecked(boolean z2) {
        g(z2, false);
    }

    public final void setLabelTagText(@StringRes int i) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.e().setText(i);
    }

    public final void setLabelTagVisibility(boolean z2) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.e().setVisibility(z2 ? 0 : 8);
    }

    public final void setOnCheckedListener(Action1<Boolean> action1) {
        this.k = action1;
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.c().setOnCheckedChangeListener(new e(action1));
    }

    public final void setSubtext(CharSequence charSequence) {
        h(charSequence, false);
    }

    public final void setSubtextColor(@ColorInt int i) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.f().setTextColor(i);
    }

    public final void setSubtextOnClickListener(Action0 action0) {
        m.checkNotNullParameter(action0, "listener");
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.f().setOnClickListener(new f(action0));
    }

    public final void setText(CharSequence charSequence) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.a().setText(charSequence);
    }

    public final void setTextColor(@ColorInt int i) {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        bVar.a().setTextColor(i);
    }

    @Override // android.widget.Checkable
    public void toggle() {
        b bVar = this.l;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("binding");
        }
        g(!bVar.c().isChecked(), false);
    }
}
