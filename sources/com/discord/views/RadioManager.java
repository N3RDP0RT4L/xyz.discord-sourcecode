package com.discord.views;

import andhook.lib.HookHelper;
import android.widget.Checkable;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: RadioManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00020\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u001c\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\bR\u0013\u0010\r\u001a\u00020\n8F@\u0006¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/views/RadioManager;", "", "Landroid/widget/Checkable;", "targetButton", "", "a", "(Landroid/widget/Checkable;)V", "", "Ljava/util/List;", "buttons", "", "b", "()I", "checkedButtonIndex", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RadioManager {
    public final List<Checkable> a;

    /* JADX WARN: Multi-variable type inference failed */
    public RadioManager(List<? extends Checkable> list) {
        m.checkNotNullParameter(list, "buttons");
        this.a = list;
        boolean z2 = false;
        for (Checkable checkable : list) {
            if (!z2 && checkable.isChecked()) {
                z2 = true;
            }
            checkable.setChecked(false);
        }
        if (!z2) {
            this.a.get(0).setChecked(true);
        }
    }

    public final void a(Checkable checkable) {
        m.checkNotNullParameter(checkable, "targetButton");
        for (Checkable checkable2 : this.a) {
            checkable2.setChecked(m.areEqual(checkable2, checkable));
        }
    }

    public final int b() {
        int i = 0;
        for (Checkable checkable : this.a) {
            if (checkable.isChecked()) {
                return i;
            }
            i++;
        }
        return -1;
    }
}
