package com.discord.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;
import b.a.i.g2;
import b.f.g.f.c;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J#\u0010\n\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0004¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u0011\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0013\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0010R\u0016\u0010\u0017\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u0018"}, d2 = {"Lcom/discord/views/GuildView;", "Landroid/widget/FrameLayout;", "Lb/f/g/f/c;", "roundingParams", "", "setRoundingParams", "(Lb/f/g/f/c;)V", "", "iconUrl", "shortGuildName", "a", "(Ljava/lang/String;Ljava/lang/String;)V", "b", "()V", "", "m", "I", "textSize", "l", "targetImageSize", "Lb/a/i/g2;", "k", "Lb/a/i/g2;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildView extends FrameLayout {
    public static final /* synthetic */ int j = 0;
    public final g2 k;
    public int l;
    public int m;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.view_guild, this);
        int i = R.id.guild_avatar;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.guild_avatar);
        if (simpleDraweeView != null) {
            i = R.id.guild_text;
            TextView textView = (TextView) findViewById(R.id.guild_text);
            if (textView != null) {
                g2 g2Var = new g2(this, simpleDraweeView, textView);
                m.checkNotNullExpressionValue(g2Var, "ViewGuildBinding.inflate…ater.from(context), this)");
                this.k = g2Var;
                this.l = -1;
                this.m = -1;
                setClipToOutline(true);
                setBackgroundResource(R.drawable.drawable_squircle_transparent);
                if (attributeSet != null) {
                    TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.GuildView, 0, 0);
                    m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…tyleable.GuildView, 0, 0)");
                    try {
                        this.l = obtainStyledAttributes.getDimensionPixelSize(0, -1);
                        this.m = obtainStyledAttributes.getDimensionPixelSize(1, -1);
                    } finally {
                        obtainStyledAttributes.recycle();
                    }
                }
                int i2 = this.m;
                if (i2 != -1) {
                    textView.setTextSize(0, i2);
                    return;
                }
                return;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    private final void setRoundingParams(c cVar) {
        SimpleDraweeView simpleDraweeView = this.k.f115b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildAvatar");
        GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
        m.checkNotNullExpressionValue(hierarchy, "binding.guildAvatar.hierarchy");
        hierarchy.s(cVar);
    }

    public final void a(String str, String str2) {
        int i = 0;
        boolean z2 = str != null;
        int i2 = this.l;
        int mediaProxySize = i2 != -1 ? IconUtils.getMediaProxySize(i2) : 0;
        SimpleDraweeView simpleDraweeView = this.k.f115b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildAvatar");
        if (!z2) {
            str = IconUtils.DEFAULT_ICON_BLURPLE;
        }
        MGImages.setImage$default(simpleDraweeView, str, mediaProxySize, mediaProxySize, false, null, null, 112, null);
        TextView textView = this.k.c;
        m.checkNotNullExpressionValue(textView, "binding.guildText");
        if (!(true ^ z2)) {
            i = 8;
        }
        textView.setVisibility(i);
        TextView textView2 = this.k.c;
        m.checkNotNullExpressionValue(textView2, "binding.guildText");
        textView2.setText(str2);
    }

    public final void b() {
        c a = c.a(getResources().getDimensionPixelSize(R.dimen.guild_icon_radius));
        m.checkNotNullExpressionValue(a, "RoundingParams.fromCorne…d_icon_radius).toFloat())");
        setRoundingParams(a);
    }
}
