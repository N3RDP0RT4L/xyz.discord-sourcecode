package com.discord.views;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import b.a.i.b2;
import com.discord.api.message.LocalAttachment;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.file.FileUtilsKt;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.SendUtilsKt;
import com.lytefast.flexinput.model.Attachment;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: FailedUploadList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002\u0016\u0017B\u001f\b\u0016\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u001b\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001c\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u00028B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0018"}, d2 = {"Lcom/discord/views/FailedUploadList;", "Landroid/widget/LinearLayout;", "", "Lcom/discord/api/message/LocalAttachment;", "localAttachments", "", "setUp", "(Ljava/util/List;)V", "Lcom/discord/views/FailedUploadView;", "getFailedUploadViews", "()Ljava/util/List;", "failedUploadViews", "Lb/a/i/b2;", "j", "Lb/a/i/b2;", "binding", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "a", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FailedUploadList extends LinearLayout {
    public final b2 j;

    /* compiled from: FailedUploadList.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final long f2800b;
        public final String c;

        public a(String str, long j, String str2) {
            m.checkNotNullParameter(str, "displayName");
            m.checkNotNullParameter(str2, "mimeType");
            this.a = str;
            this.f2800b = j;
            this.c = str2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return m.areEqual(this.a, aVar.a) && this.f2800b == aVar.f2800b && m.areEqual(this.c, aVar.c);
        }

        public int hashCode() {
            String str = this.a;
            int i = 0;
            int a = (a0.a.a.b.a(this.f2800b) + ((str != null ? str.hashCode() : 0) * 31)) * 31;
            String str2 = this.c;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return a + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("SingleFailedUpload(displayName=");
            R.append(this.a);
            R.append(", sizeBytes=");
            R.append(this.f2800b);
            R.append(", mimeType=");
            return b.d.b.a.a.H(R, this.c, ")");
        }
    }

    /* compiled from: FailedUploadList.kt */
    /* loaded from: classes2.dex */
    public static abstract class b {

        /* compiled from: FailedUploadList.kt */
        /* loaded from: classes2.dex */
        public static final class a extends b {
            public final List<a> a;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(List<a> list) {
                super(null);
                m.checkNotNullParameter(list, "uploads");
                this.a = list;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof a) && m.areEqual(this.a, ((a) obj).a);
                }
                return true;
            }

            public int hashCode() {
                List<a> list = this.a;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return b.d.b.a.a.K(b.d.b.a.a.R("FewFailedUploads(uploads="), this.a, ")");
            }
        }

        /* compiled from: FailedUploadList.kt */
        /* renamed from: com.discord.views.FailedUploadList$b$b  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0231b extends b {
            public final int a;

            /* renamed from: b  reason: collision with root package name */
            public final long f2801b;

            public C0231b(int i, long j) {
                super(null);
                this.a = i;
                this.f2801b = j;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof C0231b)) {
                    return false;
                }
                C0231b bVar = (C0231b) obj;
                return this.a == bVar.a && this.f2801b == bVar.f2801b;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.f2801b) + (this.a * 31);
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("ManyFailedUploads(uploadCount=");
                R.append(this.a);
                R.append(", sizeBytes=");
                return b.d.b.a.a.B(R, this.f2801b, ")");
            }
        }

        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public FailedUploadList(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_chat_upload_list, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.chat_upload_1;
        FailedUploadView failedUploadView = (FailedUploadView) inflate.findViewById(R.id.chat_upload_1);
        if (failedUploadView != null) {
            i = R.id.chat_upload_2;
            FailedUploadView failedUploadView2 = (FailedUploadView) inflate.findViewById(R.id.chat_upload_2);
            if (failedUploadView2 != null) {
                i = R.id.chat_upload_3;
                FailedUploadView failedUploadView3 = (FailedUploadView) inflate.findViewById(R.id.chat_upload_3);
                if (failedUploadView3 != null) {
                    b2 b2Var = new b2((LinearLayout) inflate, failedUploadView, failedUploadView2, failedUploadView3);
                    m.checkNotNullExpressionValue(b2Var, "ViewChatUploadListBindin…rom(context), this, true)");
                    this.j = b2Var;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    private final List<FailedUploadView> getFailedUploadViews() {
        FailedUploadView failedUploadView = this.j.f83b;
        m.checkNotNullExpressionValue(failedUploadView, "binding.chatUpload1");
        FailedUploadView failedUploadView2 = this.j.c;
        m.checkNotNullExpressionValue(failedUploadView2, "binding.chatUpload2");
        FailedUploadView failedUploadView3 = this.j.d;
        m.checkNotNullExpressionValue(failedUploadView3, "binding.chatUpload3");
        return n.listOf((Object[]) new FailedUploadView[]{failedUploadView, failedUploadView2, failedUploadView3});
    }

    public final void setUp(List<LocalAttachment> list) {
        Object obj;
        m.checkNotNullParameter(list, "localAttachments");
        for (FailedUploadView failedUploadView : getFailedUploadViews()) {
            failedUploadView.setVisibility(8);
        }
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (LocalAttachment localAttachment : list) {
                arrayList.add(AttachmentUtilsKt.toAttachment(localAttachment));
            }
            if (arrayList.size() <= 3) {
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    Attachment attachment = (Attachment) it.next();
                    String displayName = attachment.getDisplayName();
                    Uri uri = attachment.getUri();
                    Context context = getContext();
                    m.checkNotNullExpressionValue(context, "context");
                    ContentResolver contentResolver = context.getContentResolver();
                    m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
                    long computeFileSizeBytes = SendUtilsKt.computeFileSizeBytes(uri, contentResolver);
                    Context context2 = getContext();
                    m.checkNotNullExpressionValue(context2, "context");
                    arrayList2.add(new a(displayName, computeFileSizeBytes, AttachmentUtilsKt.getMimeType(attachment, context2.getContentResolver())));
                }
                obj = new b.a(arrayList2);
            } else {
                long j = 0;
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    Uri uri2 = ((Attachment) it2.next()).getUri();
                    Context context3 = getContext();
                    m.checkNotNullExpressionValue(context3, "context");
                    ContentResolver contentResolver2 = context3.getContentResolver();
                    m.checkNotNullExpressionValue(contentResolver2, "context.contentResolver");
                    j += SendUtilsKt.computeFileSizeBytes(uri2, contentResolver2);
                }
                obj = new b.C0231b(arrayList.size(), j);
            }
            if (obj instanceof b.a) {
                List<a> list2 = ((b.a) obj).a;
                int size = list2.size();
                for (int i = 0; i < size; i++) {
                    a aVar = list2.get(i);
                    FailedUploadView failedUploadView2 = getFailedUploadViews().get(i);
                    failedUploadView2.setVisibility(0);
                    String str = aVar.a;
                    Context context4 = failedUploadView2.getContext();
                    m.checkNotNullExpressionValue(context4, "context");
                    failedUploadView2.a(str, FileUtilsKt.getIconForFiletype(context4, aVar.c), FileUtilsKt.getSizeSubtitle(aVar.f2800b));
                }
            } else if (obj instanceof b.C0231b) {
                b.C0231b bVar = (b.C0231b) obj;
                int i2 = bVar.a;
                long j2 = bVar.f2801b;
                ((View) u.first((List<? extends Object>) getFailedUploadViews())).setVisibility(0);
                FailedUploadView failedUploadView3 = (FailedUploadView) u.first((List<? extends Object>) getFailedUploadViews());
                Resources resources = failedUploadView3.getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                Context context5 = failedUploadView3.getContext();
                m.checkNotNullExpressionValue(context5, "context");
                CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources, context5, (int) R.plurals.uploading_files_failed_count, i2, Integer.valueOf(i2));
                Context context6 = failedUploadView3.getContext();
                m.checkNotNullExpressionValue(context6, "context");
                failedUploadView3.a(quantityString, DrawableCompat.getThemedDrawableRes$default(context6, (int) R.attr.ic_uploads_generic, 0, 2, (Object) null), FileUtilsKt.getSizeSubtitle(j2));
            }
        }
    }
}
