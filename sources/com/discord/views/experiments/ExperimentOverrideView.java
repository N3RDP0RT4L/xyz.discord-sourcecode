package com.discord.views.experiments;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import b.a.i.f2;
import com.google.android.material.card.MaterialCardView;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ExperimentOverrideView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002\u0006\u0007R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/views/experiments/ExperimentOverrideView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lb/a/i/f2;", "j", "Lb/a/i/f2;", "binding", "a", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ExperimentOverrideView extends ConstraintLayout {
    public final f2 j;

    /* compiled from: ExperimentOverrideView.kt */
    /* loaded from: classes2.dex */
    public static final class a extends ArrayAdapter<b> {
        public final List<b> j;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Context context, List<b> list) {
            super(context, (int) R.layout.view_simple_spinner_dropdown_item, list);
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(list, "items");
            this.j = list;
        }

        /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
            r5 = r5;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final android.view.View a(int r4, android.view.View r5) {
            /*
                r3 = this;
                if (r5 == 0) goto L3
                goto L2a
            L3:
                android.content.Context r5 = r3.getContext()
                android.view.LayoutInflater r5 = android.view.LayoutInflater.from(r5)
                r0 = 0
                r1 = 2131558863(0x7f0d01cf, float:1.8743054E38)
                r2 = 0
                android.view.View r5 = r5.inflate(r1, r0, r2)
                java.lang.String r0 = "rootView"
                java.util.Objects.requireNonNull(r5, r0)
                b.a.i.q3 r0 = new b.a.i.q3
                android.widget.TextView r5 = (android.widget.TextView) r5
                r0.<init>(r5)
                java.lang.String r1 = "ViewSimpleSpinnerDropdow…om(context), null, false)"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                java.lang.String r0 = "ViewSimpleSpinnerDropdow…ntext), null, false).root"
                d0.z.d.m.checkNotNullExpressionValue(r5, r0)
            L2a:
                r0 = r5
                android.widget.TextView r0 = (android.widget.TextView) r0
                java.util.List<com.discord.views.experiments.ExperimentOverrideView$b> r1 = r3.j
                java.lang.Object r4 = r1.get(r4)
                com.discord.views.experiments.ExperimentOverrideView$b r4 = (com.discord.views.experiments.ExperimentOverrideView.b) r4
                java.lang.String r4 = r4.f2814b
                r0.setText(r4)
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.views.experiments.ExperimentOverrideView.a.a(int, android.view.View):android.view.View");
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public int getCount() {
            return this.j.size();
        }

        @Override // android.widget.ArrayAdapter, android.widget.BaseAdapter, android.widget.SpinnerAdapter
        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            m.checkNotNullParameter(viewGroup, "parent");
            return a(i, view);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public Object getItem(int i) {
            return this.j.get(i);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public long getItemId(int i) {
            Integer num = this.j.get(i).a;
            if (num != null) {
                return num.intValue();
            }
            return -1L;
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            m.checkNotNullParameter(viewGroup, "parent");
            return a(i, view);
        }
    }

    /* compiled from: ExperimentOverrideView.kt */
    /* loaded from: classes2.dex */
    public static final class b {
        public final Integer a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2814b;

        public b(Integer num, String str) {
            m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            this.a = num;
            this.f2814b = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return m.areEqual(this.a, bVar.a) && m.areEqual(this.f2814b, bVar.f2814b);
        }

        public int hashCode() {
            Integer num = this.a;
            int i = 0;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            String str = this.f2814b;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("SpinnerItem(bucket=");
            R.append(this.a);
            R.append(", text=");
            return b.d.b.a.a.H(R, this.f2814b, ")");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ExperimentOverrideView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.view_experiment_override, this);
        int i = R.id.experiment_override_bucket_descriptions;
        TextView textView = (TextView) findViewById(R.id.experiment_override_bucket_descriptions);
        if (textView != null) {
            i = R.id.experiment_override_buckets_spinner;
            Spinner spinner = (Spinner) findViewById(R.id.experiment_override_buckets_spinner);
            if (spinner != null) {
                i = R.id.experiment_override_buckets_spinner_container;
                MaterialCardView materialCardView = (MaterialCardView) findViewById(R.id.experiment_override_buckets_spinner_container);
                if (materialCardView != null) {
                    i = R.id.experiment_override_clear;
                    TextView textView2 = (TextView) findViewById(R.id.experiment_override_clear);
                    if (textView2 != null) {
                        i = R.id.experiment_override_experiment_api_name;
                        TextView textView3 = (TextView) findViewById(R.id.experiment_override_experiment_api_name);
                        if (textView3 != null) {
                            i = R.id.experiment_override_experiment_name;
                            TextView textView4 = (TextView) findViewById(R.id.experiment_override_experiment_name);
                            if (textView4 != null) {
                                f2 f2Var = new f2(this, textView, spinner, materialCardView, textView2, textView3, textView4);
                                m.checkNotNullExpressionValue(f2Var, "ViewExperimentOverrideBi…ater.from(context), this)");
                                this.j = f2Var;
                                return;
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }
}
