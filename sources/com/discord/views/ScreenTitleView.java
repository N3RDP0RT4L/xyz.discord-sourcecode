package com.discord.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.i.a3;
import com.discord.utilities.view.extensions.ViewExtensions;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ScreenTitleView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\b\u0010\u0006R\u0016\u0010\f\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\r"}, d2 = {"Lcom/discord/views/ScreenTitleView;", "Landroid/widget/LinearLayout;", "", "title", "", "setTitle", "(Ljava/lang/CharSequence;)V", "subtitle", "setSubtitle", "Lb/a/i/a3;", "j", "Lb/a/i/a3;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ScreenTitleView extends LinearLayout {
    public final a3 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ScreenTitleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_screen_title, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.screen_title_subtitle;
        TextView textView = (TextView) inflate.findViewById(R.id.screen_title_subtitle);
        if (textView != null) {
            i = R.id.screen_title_title;
            TextView textView2 = (TextView) inflate.findViewById(R.id.screen_title_title);
            if (textView2 != null) {
                a3 a3Var = new a3((LinearLayout) inflate, textView, textView2);
                m.checkNotNullExpressionValue(a3Var, "ViewScreenTitleBinding.i…rom(context), this, true)");
                this.j = a3Var;
                if (attributeSet != null) {
                    TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, com.discord.R.a.ScreenTitleView, 0, 0);
                    m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…le.ScreenTitleView, 0, 0)");
                    CharSequence text = obtainStyledAttributes.getText(1);
                    CharSequence text2 = obtainStyledAttributes.getText(0);
                    obtainStyledAttributes.recycle();
                    if (text != null) {
                        setTitle(text);
                    }
                    setSubtitle(text2);
                    return;
                }
                return;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void setSubtitle(CharSequence charSequence) {
        TextView textView = this.j.f76b;
        m.checkNotNullExpressionValue(textView, "binding.screenTitleSubtitle");
        ViewExtensions.setTextAndVisibilityBy(textView, charSequence);
    }

    public final void setTitle(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "title");
        TextView textView = this.j.c;
        m.checkNotNullExpressionValue(textView, "binding.screenTitleTitle");
        textView.setText(charSequence);
    }
}
