package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import b.a.i.a1;
import b.a.i.r2;
import com.discord.R;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.views.CutoutView;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: PileView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001.B\u001d\b\u0007\u0012\u0006\u0010)\u001a\u00020(\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010*¢\u0006\u0004\b,\u0010-J\u001b\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u000b\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\r\u001a\u00020\b8\u0002@\u0002X\u0083\u000e¢\u0006\u0006\n\u0004\b\f\u0010\nR\u0016\u0010\u000f\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u0010\nR\u0016\u0010\u0011\u001a\u00020\b8\u0002@\u0002X\u0083\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\nR\u0016\u0010\u0013\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\nR\u0016\u0010\u0015\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\nR\u0016\u0010\u0019\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001b\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u0018R\u0016\u0010\u001f\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010!\u001a\u00020\b8\u0002@\u0002X\u0083\u000e¢\u0006\u0006\n\u0004\b \u0010\nR\u0016\u0010#\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010\nR\u0016\u0010%\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010\nR\u0016\u0010'\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010\u0018¨\u0006/"}, d2 = {"Lcom/discord/views/PileView;", "Landroid/widget/FrameLayout;", "", "Lcom/discord/views/PileView$c;", "items", "", "setItems", "(Ljava/util/Collection;)V", "", "m", "I", "itemSizePx", "v", "overflowBackgroundColor", "n", "overlapPx", "o", "itemBackgroundColor", "t", "overflowTextSizePx", "j", "maxItems", "", "k", "Z", "doRoundItem", "l", "doRoundBg", "Lcom/discord/views/CutoutView$a;", "s", "Lcom/discord/views/CutoutView$a;", "cutoutStyle", "q", "itemTextColor", "r", "cutoutThicknessPx", "p", "itemPaddingPx", "u", "showOverFlowItem", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "c", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PileView extends FrameLayout {
    public int j;
    public boolean k;
    public boolean l;
    public int m;
    public int n;
    @ColorInt
    public int o;
    public int p;
    @ColorInt
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public final CutoutView.a f2806s;
    public int t;
    public boolean u;
    @ColorInt
    public int v;

    /* compiled from: kotlin-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a extends o implements Function1<Integer, String> {
        public static final a j = new a(0);
        public static final a k = new a(1);
        public static final a l = new a(2);
        public final /* synthetic */ int m;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(int i) {
            super(1);
            this.m = i;
        }

        @Override // kotlin.jvm.functions.Function1
        public final String invoke(Integer num) {
            int i = this.m;
            if (i == 0) {
                num.intValue();
                return "asset://asset/images/default_avatar_0.jpg";
            } else if (i == 1) {
                num.intValue();
                return "asset://asset/images/default_avatar_1.jpg";
            } else if (i == 2) {
                num.intValue();
                return "asset://asset/images/default_avatar_2.jpg";
            } else {
                throw null;
            }
        }
    }

    /* compiled from: kotlin-style lambda group */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function0<String> {
        public static final b j = new b(0);
        public static final b k = new b(1);
        public static final b l = new b(2);
        public final /* synthetic */ int m;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(int i) {
            super(0);
            this.m = i;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String invoke() {
            int i = this.m;
            if (i == 0 || i == 1 || i == 2) {
                return "";
            }
            throw null;
        }
    }

    /* compiled from: PileView.kt */
    /* loaded from: classes2.dex */
    public static final class c {
        public final Function1<Integer, String> a;

        /* renamed from: b  reason: collision with root package name */
        public final Function0<String> f2807b;

        /* JADX WARN: Multi-variable type inference failed */
        public c(Function1<? super Integer, String> function1, Function0<String> function0) {
            m.checkNotNullParameter(function1, "getImageURI");
            this.a = function1;
            this.f2807b = function0;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PileView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        this.j = 3;
        this.k = true;
        this.l = true;
        this.m = DimenUtils.dpToPixels(16);
        this.r = DimenUtils.dpToPixels(2);
        this.t = DimenUtils.dpToPixels(14);
        int[] iArr = R.a.PileView;
        m.checkNotNullExpressionValue(iArr, "R.styleable.PileView");
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        this.j = obtainStyledAttributes.getInt(6, this.j);
        this.m = obtainStyledAttributes.getDimensionPixelSize(4, this.m);
        this.o = obtainStyledAttributes.getColor(0, this.o);
        this.p = obtainStyledAttributes.getDimensionPixelSize(3, this.p);
        this.q = obtainStyledAttributes.getColor(5, this.q);
        this.n = obtainStyledAttributes.getDimensionPixelSize(2, this.n);
        this.r = obtainStyledAttributes.getDimensionPixelSize(1, this.r);
        this.k = obtainStyledAttributes.getBoolean(10, this.k);
        this.l = obtainStyledAttributes.getBoolean(9, this.l);
        this.t = obtainStyledAttributes.getDimensionPixelSize(7, this.t);
        this.u = obtainStyledAttributes.getBoolean(11, this.u);
        this.v = obtainStyledAttributes.getColor(8, this.v);
        obtainStyledAttributes.recycle();
        int i = this.n;
        int i2 = this.r;
        this.f2806s = new CutoutView.a.b((this.m / 2) + i2, i + i2);
        if (isInEditMode()) {
            setItems(n.listOf((Object[]) new c[]{new c(a.j, b.j), new c(a.k, b.k), new c(a.l, b.l)}));
        }
    }

    public final void setItems(Collection<c> collection) {
        int i;
        m.checkNotNullParameter(collection, "items");
        removeAllViews();
        boolean z2 = false;
        int size = collection.size() > this.j ? (collection.size() - this.j) + 1 : 0;
        if (size > 0) {
            i = this.j - 1;
        } else {
            i = collection.size();
        }
        int i2 = i - 1;
        int i3 = 0;
        for (Object obj : u.take(collection, i)) {
            i3++;
            if (i3 < 0) {
                n.throwIndexOverflow();
            }
            c cVar = (c) obj;
            int i4 = this.m * i3;
            int i5 = this.n * i3;
            View inflate = LayoutInflater.from(getContext()).inflate(xyz.discord.R.layout.view_pile_item, this, z2);
            int i6 = xyz.discord.R.id.pileItemBg;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(xyz.discord.R.id.pileItemBg);
            if (simpleDraweeView != null) {
                i6 = xyz.discord.R.id.pileItemImage;
                SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) inflate.findViewById(xyz.discord.R.id.pileItemImage);
                if (simpleDraweeView2 != null) {
                    TextView textView = (TextView) inflate.findViewById(xyz.discord.R.id.pileItemInitials);
                    if (textView != null) {
                        CutoutView cutoutView = (CutoutView) inflate;
                        m.checkNotNullExpressionValue(new r2(cutoutView, simpleDraweeView, simpleDraweeView2, textView), "ViewPileItemBinding.infl…om(context), this, false)");
                        cutoutView.setId(FrameLayout.generateViewId());
                        int i7 = this.m;
                        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i7, i7);
                        layoutParams.setMarginStart(i4 - i5);
                        cutoutView.setLayoutParams(layoutParams);
                        m.checkNotNullExpressionValue(cutoutView, "itemBinding.root");
                        addView(cutoutView, cutoutView.getLayoutParams());
                        m.checkNotNullExpressionValue(simpleDraweeView2, "itemBinding.pileItemImage");
                        int i8 = this.p;
                        simpleDraweeView2.setPadding(i8, i8, i8, i8);
                        if (!this.k) {
                            m.checkNotNullExpressionValue(simpleDraweeView2, "itemBinding.pileItemImage");
                            GenericDraweeHierarchy hierarchy = simpleDraweeView2.getHierarchy();
                            m.checkNotNullExpressionValue(hierarchy, "itemBinding.pileItemImage.hierarchy");
                            hierarchy.s(null);
                        }
                        if (!this.l) {
                            m.checkNotNullExpressionValue(simpleDraweeView, "itemBinding.pileItemBg");
                            GenericDraweeHierarchy hierarchy2 = simpleDraweeView.getHierarchy();
                            m.checkNotNullExpressionValue(hierarchy2, "itemBinding.pileItemBg.hierarchy");
                            hierarchy2.s(null);
                        }
                        if (this.o != 0) {
                            m.checkNotNullExpressionValue(simpleDraweeView, "itemBinding.pileItemBg");
                            simpleDraweeView.getHierarchy().o(1, new ColorDrawable(this.o));
                            m.checkNotNullExpressionValue(simpleDraweeView, "itemBinding.pileItemBg");
                            simpleDraweeView.setVisibility(0);
                        }
                        if (i3 != i2 || size > 0) {
                            cutoutView.setStyle(this.f2806s);
                        }
                        String invoke = cVar.a.invoke(Integer.valueOf(IconUtils.getMediaProxySize(this.m)));
                        Function0<String> function0 = cVar.f2807b;
                        String invoke2 = function0 != null ? function0.invoke() : null;
                        m.checkNotNullExpressionValue(textView, "itemBinding.pileItemInitials");
                        int i9 = 8;
                        textView.setVisibility(invoke == null ? 0 : 8);
                        m.checkNotNullExpressionValue(textView, "itemBinding.pileItemInitials");
                        textView.setText(invoke2);
                        int i10 = this.q;
                        if (i10 != 0) {
                            textView.setTextColor(i10);
                        }
                        m.checkNotNullExpressionValue(simpleDraweeView2, "itemBinding.pileItemImage");
                        if (invoke != null) {
                            i9 = 0;
                        }
                        simpleDraweeView2.setVisibility(i9);
                        if (invoke != null) {
                            m.checkNotNullExpressionValue(simpleDraweeView2, "itemBinding.pileItemImage");
                            MGImages.setImage$default(simpleDraweeView2, invoke, 0, 0, true, null, null, 108, null);
                        }
                        z2 = false;
                    } else {
                        i6 = xyz.discord.R.id.pileItemInitials;
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i6)));
        }
        if (size > 0) {
            int i11 = this.j - 1;
            int i12 = this.m * i11;
            int i13 = i11 * this.n;
            View inflate2 = LayoutInflater.from(getContext()).inflate(xyz.discord.R.layout.pile_item_overflow_view, (ViewGroup) this, false);
            TextView textView2 = (TextView) inflate2.findViewById(xyz.discord.R.id.pile_item_text);
            if (textView2 != null) {
                CutoutView cutoutView2 = (CutoutView) inflate2;
                m.checkNotNullExpressionValue(new a1(cutoutView2, textView2), "PileItemOverflowViewBind…om(context), this, false)");
                cutoutView2.setId(FrameLayout.generateViewId());
                FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, this.m);
                layoutParams2.setMarginStart(i12 - i13);
                cutoutView2.setLayoutParams(layoutParams2);
                m.checkNotNullExpressionValue(textView2, "overflowItemBinding.pileItemText");
                StringBuilder sb = new StringBuilder();
                sb.append('+');
                sb.append(size);
                textView2.setText(sb.toString());
                textView2.setTextSize(0, this.t);
                m.checkNotNullExpressionValue(cutoutView2, "overflowItemBinding.root");
                addView(cutoutView2, cutoutView2.getLayoutParams());
                m.checkNotNullExpressionValue(textView2, "overflowItemBinding.pileItemText");
                MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(new ShapeAppearanceModel.Builder().setAllCornerSizes(ShapeAppearanceModel.PILL).build());
                int i14 = this.v;
                if (i14 == 0) {
                    i14 = this.o;
                }
                materialShapeDrawable.setFillColor(ColorStateList.valueOf(i14));
                textView2.setBackground(materialShapeDrawable);
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(xyz.discord.R.id.pile_item_text)));
        }
    }
}
