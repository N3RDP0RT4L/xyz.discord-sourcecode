package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.annotation.Px;
import androidx.appcompat.widget.AppCompatImageView;
import com.discord.R;
import com.discord.api.presence.ClientStatus;
import com.discord.api.presence.ClientStatuses;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.presence.Presence;
import com.discord.utilities.presence.PresenceUtils;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StatusView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010\"\u001a\u00020!\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010#¢\u0006\u0004\b%\u0010&J\u0017\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\b\b\u0001\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\b\b\u0001\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u0010\u0010\nJ\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0014¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001e\u001a\u00020\u001b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010 \u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010\u0017¨\u0006'"}, d2 = {"Lcom/discord/views/StatusView;", "Landroidx/appcompat/widget/AppCompatImageView;", "Lcom/discord/models/presence/Presence;", "presence", "", "setPresence", "(Lcom/discord/models/presence/Presence;)V", "", "borderWidthPx", "setBorderWidth", "(I)V", "", "cornerRadius", "setCornerRadius", "(F)V", ModelAuditLogEntry.CHANGE_KEY_COLOR, "setBackgroundColor", "Landroid/graphics/Canvas;", "canvas", "onDraw", "(Landroid/graphics/Canvas;)V", "", "l", "Z", "showMobile", "j", "F", "Landroid/graphics/Paint;", "k", "Landroid/graphics/Paint;", "backgroundPaint", "m", "isCircle", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StatusView extends AppCompatImageView {
    public float j;
    public Paint k;
    public boolean l;
    public boolean m = true;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StatusView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        this.j = 2.0f;
        this.l = true;
        setAdjustViewBounds(true);
        setScaleType(ImageView.ScaleType.FIT_XY);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.a.StatusView, 0, 0);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…yleable.StatusView, 0, 0)");
        try {
            setBorderWidth((int) obtainStyledAttributes.getDimension(1, 0.0f));
            int color = obtainStyledAttributes.getColor(0, 0);
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(color);
            paint.setAntiAlias(true);
            this.k = paint;
            this.l = obtainStyledAttributes.getBoolean(3, true);
            this.j = obtainStyledAttributes.getDimension(2, 0.0f);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        m.checkNotNullParameter(canvas, "canvas");
        if (this.m) {
            float width = getWidth() / 2.0f;
            float height = getHeight() / 2.0f;
            float width2 = getWidth() / 2.0f;
            Paint paint = this.k;
            if (paint == null) {
                m.throwUninitializedPropertyAccessException("backgroundPaint");
            }
            canvas.drawCircle(width, height, width2, paint);
        } else {
            float width3 = getWidth();
            float height2 = getHeight();
            float f = this.j;
            Paint paint2 = this.k;
            if (paint2 == null) {
                m.throwUninitializedPropertyAccessException("backgroundPaint");
            }
            canvas.drawRoundRect(0.0f, 0.0f, width3, height2, f, f, paint2);
        }
        super.onDraw(canvas);
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(i);
        paint.setAntiAlias(true);
        this.k = paint;
    }

    public final void setBorderWidth(@Px int i) {
        setPadding(i, i, i, i);
    }

    public final void setCornerRadius(@Px float f) {
        this.j = f;
        invalidate();
    }

    public final void setPresence(Presence presence) {
        int i;
        ClientStatuses clientStatuses;
        ClientStatuses clientStatuses2;
        if (!this.l || presence == null || (clientStatuses2 = presence.getClientStatuses()) == null || !PresenceUtils.INSTANCE.isMobile(clientStatuses2)) {
            ClientStatus clientStatus = null;
            if ((presence != null ? PresenceUtils.INSTANCE.getStreamingActivity(presence) : null) != null) {
                i = xyz.discord.R.drawable.ic_status_streaming_16dp;
            } else {
                if (presence != null) {
                    clientStatus = presence.getStatus();
                }
                if (clientStatus != null) {
                    int ordinal = clientStatus.ordinal();
                    if (ordinal == 0) {
                        i = xyz.discord.R.drawable.ic_status_online_16dp;
                    } else if (ordinal == 1) {
                        i = xyz.discord.R.drawable.ic_status_idle_16dp;
                    } else if (ordinal == 2) {
                        i = xyz.discord.R.drawable.ic_status_dnd_16dp;
                    }
                }
                i = xyz.discord.R.drawable.ic_status_invisible_16dp;
            }
        } else {
            i = xyz.discord.R.drawable.ic_mobile;
        }
        setImageResource(i);
        this.m = !(this.l && presence != null && (clientStatuses = presence.getClientStatuses()) != null && PresenceUtils.INSTANCE.isMobile(clientStatuses));
    }
}
