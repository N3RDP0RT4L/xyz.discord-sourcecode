package com.discord.views.directories;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.c3;
import b.a.k.b;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.views.GuildView;
import com.discord.views.guilds.ServerMemberCount;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ServerDiscoveryItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\b\u0010\u0006J\u0015\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u000e\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\t¢\u0006\u0004\b\u000e\u0010\fJ\u0017\u0010\u0011\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0013\u0010\u0012J\u0015\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u001b\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001a¨\u0006\u001c"}, d2 = {"Lcom/discord/views/directories/ServerDiscoveryItem;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "title", "", "setTitle", "(Ljava/lang/CharSequence;)V", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "setDescription", "", "active", "setOnline", "(I)V", "members", "setMembers", "Landroid/view/View$OnClickListener;", "listener", "setJoinButtonOnClickListener", "(Landroid/view/View$OnClickListener;)V", "setJoinedButtonOnClickListener", "", "joinedGuild", "setJoinedGuild", "(Z)V", "Lb/a/i/c3;", "j", "Lb/a/i/c3;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerDiscoveryItem extends ConstraintLayout {
    public final c3 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerDiscoveryItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.view_server_discovery_item, this);
        int i = R.id.button_barrier;
        Barrier barrier = (Barrier) findViewById(R.id.button_barrier);
        if (barrier != null) {
            i = R.id.server_discovery_item_barrier;
            Barrier barrier2 = (Barrier) findViewById(R.id.server_discovery_item_barrier);
            if (barrier2 != null) {
                i = R.id.server_discovery_item_button;
                MaterialButton materialButton = (MaterialButton) findViewById(R.id.server_discovery_item_button);
                if (materialButton != null) {
                    i = R.id.server_discovery_item_button_joined;
                    MaterialButton materialButton2 = (MaterialButton) findViewById(R.id.server_discovery_item_button_joined);
                    if (materialButton2 != null) {
                        i = R.id.server_discovery_item_count_container;
                        ServerMemberCount serverMemberCount = (ServerMemberCount) findViewById(R.id.server_discovery_item_count_container);
                        if (serverMemberCount != null) {
                            i = R.id.server_discovery_item_description;
                            TextView textView = (TextView) findViewById(R.id.server_discovery_item_description);
                            if (textView != null) {
                                i = R.id.server_discovery_item_image;
                                GuildView guildView = (GuildView) findViewById(R.id.server_discovery_item_image);
                                if (guildView != null) {
                                    i = R.id.server_discovery_item_title;
                                    TextView textView2 = (TextView) findViewById(R.id.server_discovery_item_title);
                                    if (textView2 != null) {
                                        c3 c3Var = new c3(this, barrier, barrier2, materialButton, materialButton2, serverMemberCount, textView, guildView, textView2);
                                        m.checkNotNullExpressionValue(c3Var, "ViewServerDiscoveryItemB…ater.from(context), this)");
                                        this.j = c3Var;
                                        guildView.b();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    public final void setDescription(CharSequence charSequence) {
        TextView textView = this.j.e;
        m.checkNotNullExpressionValue(textView, "binding.serverDiscoveryItemDescription");
        b.a(textView, charSequence);
    }

    public final void setJoinButtonOnClickListener(View.OnClickListener onClickListener) {
        this.j.f91b.setOnClickListener(onClickListener);
    }

    public final void setJoinedButtonOnClickListener(View.OnClickListener onClickListener) {
        this.j.c.setOnClickListener(onClickListener);
    }

    public final void setJoinedGuild(boolean z2) {
        MaterialButton materialButton = this.j.f91b;
        m.checkNotNullExpressionValue(materialButton, "binding.serverDiscoveryItemButton");
        int i = 0;
        materialButton.setVisibility(z2 ^ true ? 0 : 8);
        MaterialButton materialButton2 = this.j.c;
        m.checkNotNullExpressionValue(materialButton2, "binding.serverDiscoveryItemButtonJoined");
        if (!z2) {
            i = 8;
        }
        materialButton2.setVisibility(i);
    }

    public final void setMembers(int i) {
        this.j.d.setMembers(Integer.valueOf(i));
    }

    public final void setOnline(int i) {
        this.j.d.setOnline(Integer.valueOf(i));
    }

    public final void setTitle(CharSequence charSequence) {
        TextView textView = this.j.g;
        m.checkNotNullExpressionValue(textView, "binding.serverDiscoveryItemTitle");
        textView.setText(charSequence);
    }
}
