package com.discord.views.premium;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.v1;
import b.a.k.b;
import com.discord.models.domain.ModelSubscription;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.time.TimeUtils;
import d0.z.d.m;
import java.text.DateFormat;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: AccountCreditView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J'\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u000f"}, d2 = {"Lcom/discord/views/premium/AccountCreditView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "planId", "", "monthsCredit", "Lcom/discord/models/domain/ModelSubscription;", "currentSubscription", "", "a", "(JILcom/discord/models/domain/ModelSubscription;)V", "Lb/a/i/v1;", "j", "Lb/a/i/v1;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AccountCreditView extends ConstraintLayout {
    public final v1 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AccountCreditView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_account_credit, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.credit_header;
        TextView textView = (TextView) inflate.findViewById(R.id.credit_header);
        if (textView != null) {
            i = R.id.credit_icon;
            ImageView imageView = (ImageView) inflate.findViewById(R.id.credit_icon);
            if (imageView != null) {
                i = R.id.credit_info;
                TextView textView2 = (TextView) inflate.findViewById(R.id.credit_info);
                if (textView2 != null) {
                    i = R.id.credit_info_container;
                    LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.credit_info_container);
                    if (linearLayout != null) {
                        i = R.id.credit_time;
                        TextView textView3 = (TextView) inflate.findViewById(R.id.credit_time);
                        if (textView3 != null) {
                            v1 v1Var = new v1((ConstraintLayout) inflate, textView, imageView, textView2, linearLayout, textView3);
                            m.checkNotNullExpressionValue(v1Var, "ViewAccountCreditBinding…rom(context), this, true)");
                            this.j = v1Var;
                            return;
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void a(long j, int i, ModelSubscription modelSubscription) {
        Object obj;
        setVisibility(i > 0 ? 0 : 8);
        if (i > 0) {
            if (j == SubscriptionPlanType.PREMIUM_MONTH_TIER_1.getPlanId()) {
                obj = b.d(this, R.string.premium_tier_1, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                this.j.c.setImageResource(R.drawable.drawable_ic_nitro_classic);
                TextView textView = this.j.f210b;
                m.checkNotNullExpressionValue(textView, "binding.creditHeader");
                b.m(textView, R.string.premium_subscription_credit, new Object[]{obj}, (r4 & 4) != 0 ? b.g.j : null);
            } else if (j == SubscriptionPlanType.PREMIUM_MONTH_TIER_2.getPlanId()) {
                obj = b.d(this, R.string.premium_tier_2, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                this.j.c.setImageResource(R.drawable.drawable_ic_nitro);
                TextView textView2 = this.j.f210b;
                m.checkNotNullExpressionValue(textView2, "binding.creditHeader");
                b.m(textView2, R.string.premium_subscription_credit, new Object[]{obj}, (r4 & 4) != 0 ? b.g.j : null);
            } else {
                obj = "";
            }
            if ((modelSubscription != null && modelSubscription.isGoogleSubscription()) || modelSubscription == null || !modelSubscription.hasPlan(j)) {
                TextView textView3 = this.j.d;
                m.checkNotNullExpressionValue(textView3, "binding.creditInfo");
                b.m(textView3, R.string.premium_subscription_credit_applied_mismatched_plan_android, new Object[]{obj}, (r4 & 4) != 0 ? b.g.j : null);
            } else {
                TimeUtils timeUtils = TimeUtils.INSTANCE;
                String currentPeriodEnd = modelSubscription.getCurrentPeriodEnd();
                Context context = getContext();
                m.checkNotNullExpressionValue(context, "context");
                String renderUtcDate$default = TimeUtils.renderUtcDate$default(timeUtils, currentPeriodEnd, context, (String) null, (DateFormat) null, 0, 28, (Object) null);
                TextView textView4 = this.j.d;
                m.checkNotNullExpressionValue(textView4, "binding.creditInfo");
                b.m(textView4, R.string.premium_subscription_credit_applied_on, new Object[]{renderUtcDate$default}, (r4 & 4) != 0 ? b.g.j : null);
            }
            TextView textView5 = this.j.e;
            m.checkNotNullExpressionValue(textView5, "binding.creditTime");
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            Context context2 = getContext();
            m.checkNotNullExpressionValue(context2, "context");
            textView5.setText(StringResourceUtilsKt.getQuantityString(resources, context2, (int) R.plurals.premium_subscription_credit_count_months_count, i, Integer.valueOf(i)));
        }
    }
}
