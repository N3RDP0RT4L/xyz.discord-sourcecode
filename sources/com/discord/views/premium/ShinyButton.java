package com.discord.views.premium;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.app.NotificationCompat;
import b.a.i.l1;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ShinyButton.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0006J\u0015\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0010\u0010\u0006J7\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001c\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001d"}, d2 = {"Lcom/discord/views/premium/ShinyButton;", "Landroid/widget/RelativeLayout;", "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "", "setTextColor", "(I)V", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setText", "(Ljava/lang/String;)V", "Landroid/graphics/drawable/Drawable;", "iconStart", "setDrawable", "(Landroid/graphics/drawable/Drawable;)V", "setDrawableColor", "setBackgroundTint", "", "changed", "l", "t", "r", "b", "onLayout", "(ZIIII)V", "Lb/a/i/l1;", "j", "Lb/a/i/l1;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ShinyButton extends RelativeLayout {
    public final l1 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShinyButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.shiny_button, this);
        int i = R.id.button_container;
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.button_container);
        if (linearLayout != null) {
            i = R.id.icon;
            ImageView imageView = (ImageView) findViewById(R.id.icon);
            if (imageView != null) {
                i = R.id.text;
                TextView textView = (TextView) findViewById(R.id.text);
                if (textView != null) {
                    l1 l1Var = new l1(this, linearLayout, imageView, textView);
                    m.checkNotNullExpressionValue(l1Var, "ShinyButtonBinding.infla…ater.from(context), this)");
                    this.j = l1Var;
                    if (attributeSet != null) {
                        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, com.discord.R.a.ShinyButton, 0, 0);
                        try {
                            String string = obtainStyledAttributes.getString(3);
                            if (string != null) {
                                m.checkNotNullExpressionValue(string, "it");
                                setText(string);
                            }
                            Drawable drawable = obtainStyledAttributes.getDrawable(2);
                            if (drawable != null) {
                                m.checkNotNullExpressionValue(drawable, "it");
                                setDrawable(drawable);
                            }
                            setBackgroundTint(obtainStyledAttributes.getColor(0, ColorCompat.getColor(context, (int) R.color.uikit_btn_bg_color_selector_green)));
                            int color = obtainStyledAttributes.getColor(1, ColorCompat.getColor(context, (int) R.color.white));
                            setTextColor(color);
                            setDrawableColor(color);
                            return;
                        } finally {
                            obtainStyledAttributes.recycle();
                        }
                    } else {
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    @Override // android.widget.RelativeLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        if (z2) {
            LinearLayout linearLayout = this.j.f149b;
            m.checkNotNullExpressionValue(linearLayout, "binding.buttonContainer");
            ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
            layoutParams.width = getLayoutParams().width;
            layoutParams.height = getLayoutParams().height;
            LinearLayout linearLayout2 = this.j.f149b;
            m.checkNotNullExpressionValue(linearLayout2, "binding.buttonContainer");
            linearLayout2.setLayoutParams(layoutParams);
        }
    }

    public final void setBackgroundTint(int i) {
        LinearLayout linearLayout = this.j.f149b;
        m.checkNotNullExpressionValue(linearLayout, "binding.buttonContainer");
        linearLayout.getBackground().setTint(i);
    }

    public final void setDrawable(Drawable drawable) {
        m.checkNotNullParameter(drawable, "iconStart");
        this.j.c.setImageDrawable(drawable);
    }

    public final void setDrawableColor(int i) {
        ImageView imageView = this.j.c;
        m.checkNotNullExpressionValue(imageView, "binding.icon");
        ColorCompatKt.tintWithColor(imageView, i);
    }

    public final void setText(String str) {
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        TextView textView = this.j.d;
        m.checkNotNullExpressionValue(textView, "binding.text");
        textView.setText(str);
    }

    public final void setTextColor(int i) {
        this.j.d.setTextColor(i);
    }
}
