package com.discord.views.premium;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.discord.databinding.WidgetChoosePlanAdapterPlanItemBinding;
import com.discord.utilities.billing.GooglePlayInAppSku;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: GiftSelectView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0007\u0010\bR$\u0010\u000e\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u000f"}, d2 = {"Lcom/discord/views/premium/GiftSelectView;", "Landroid/widget/FrameLayout;", "Lcom/discord/databinding/WidgetChoosePlanAdapterPlanItemBinding;", "j", "Lcom/discord/databinding/WidgetChoosePlanAdapterPlanItemBinding;", "binding", "Lcom/discord/utilities/billing/GooglePlayInAppSku;", "l", "Lcom/discord/utilities/billing/GooglePlayInAppSku;", "inAppSku", "Lkotlin/Function1;", "", "k", "Lkotlin/jvm/functions/Function1;", "onClickPlan", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GiftSelectView extends FrameLayout {
    public final WidgetChoosePlanAdapterPlanItemBinding j;
    public Function1<? super GooglePlayInAppSku, Unit> k;
    public GooglePlayInAppSku l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GiftSelectView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.widget_choose_plan_adapter_plan_item, (ViewGroup) this, false);
        addView(inflate);
        WidgetChoosePlanAdapterPlanItemBinding a = WidgetChoosePlanAdapterPlanItemBinding.a(inflate);
        m.checkNotNullExpressionValue(a, "WidgetChoosePlanAdapterP…rom(context), this, true)");
        this.j = a;
    }
}
