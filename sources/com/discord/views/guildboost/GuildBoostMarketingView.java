package com.discord.views.guildboost;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.i.u2;
import b.a.k.b;
import com.discord.api.premium.PremiumTier;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import java.text.NumberFormat;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: GuildBoostMarketingView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J#\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\f\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\r"}, d2 = {"Lcom/discord/views/guildboost/GuildBoostMarketingView;", "Landroid/widget/FrameLayout;", "Lcom/discord/api/premium/PremiumTier;", "userPremiumTier", "Lkotlin/Function0;", "", "onlearnMoreClickCallback", "a", "(Lcom/discord/api/premium/PremiumTier;Lkotlin/jvm/functions/Function0;)V", "Lb/a/i/u2;", "j", "Lb/a/i/u2;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostMarketingView extends FrameLayout {
    public final u2 j;

    /* compiled from: GuildBoostMarketingView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ Function0 j;

        public a(Function0 function0) {
            this.j = function0;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            this.j.invoke();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostMarketingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_premium_marketing, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.view_premium_marketing_container_tier1;
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.view_premium_marketing_container_tier1);
        if (linearLayout != null) {
            i = R.id.view_premium_marketing_learn_more;
            MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.view_premium_marketing_learn_more);
            if (materialButton != null) {
                LinearLayout linearLayout2 = (LinearLayout) inflate;
                i = R.id.view_premium_marketing_marketing_subtitle;
                TextView textView = (TextView) inflate.findViewById(R.id.view_premium_marketing_marketing_subtitle);
                if (textView != null) {
                    i = R.id.view_premium_marketing_marketing_title;
                    TextView textView2 = (TextView) inflate.findViewById(R.id.view_premium_marketing_marketing_title);
                    if (textView2 != null) {
                        i = R.id.view_premium_marketing_nitro_boost_count;
                        TextView textView3 = (TextView) inflate.findViewById(R.id.view_premium_marketing_nitro_boost_count);
                        if (textView3 != null) {
                            i = R.id.view_premium_marketing_nitro_boost_discount;
                            TextView textView4 = (TextView) inflate.findViewById(R.id.view_premium_marketing_nitro_boost_discount);
                            if (textView4 != null) {
                                i = R.id.view_premium_marketing_nitro_classic_boost_count;
                                TextView textView5 = (TextView) inflate.findViewById(R.id.view_premium_marketing_nitro_classic_boost_count);
                                if (textView5 != null) {
                                    i = R.id.view_premium_marketing_nitro_classic_boost_discount;
                                    TextView textView6 = (TextView) inflate.findViewById(R.id.view_premium_marketing_nitro_classic_boost_discount);
                                    if (textView6 != null) {
                                        u2 u2Var = new u2(linearLayout2, linearLayout, materialButton, linearLayout2, textView, textView2, textView3, textView4, textView5, textView6);
                                        m.checkNotNullExpressionValue(u2Var, "ViewPremiumMarketingBind…rom(context), this, true)");
                                        this.j = u2Var;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void a(PremiumTier premiumTier, Function0<Unit> function0) {
        Unit unit;
        CharSequence d;
        CharSequence d2;
        CharSequence d3;
        m.checkNotNullParameter(premiumTier, "userPremiumTier");
        m.checkNotNullParameter(function0, "onlearnMoreClickCallback");
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        CharSequence i18nPluralString = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_settings_premium_upsell_body_perk_no_free_guild_subscriptions_numFreeGuildSubscriptions, 2, 2);
        TextView textView = this.j.h;
        m.checkNotNullExpressionValue(textView, "binding.viewPremiumMarketingNitroBoostDiscount");
        NumberFormat percentInstance = NumberFormat.getPercentInstance();
        Float valueOf = Float.valueOf(0.3f);
        b.m(textView, R.string.guild_settings_premium_upsell_body_perk_guild_subscription_discount, new Object[]{percentInstance.format(valueOf)}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView2 = this.j.g;
        m.checkNotNullExpressionValue(textView2, "binding.viewPremiumMarketingNitroBoostCount");
        b.m(textView2, R.string.guild_settings_premium_upsell_body_perk_num_guild_subscriptions, new Object[]{i18nPluralString}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView3 = this.j.j;
        m.checkNotNullExpressionValue(textView3, "binding.viewPremiumMarke…NitroClassicBoostDiscount");
        b.m(textView3, R.string.guild_settings_premium_upsell_body_perk_guild_subscription_discount, new Object[]{NumberFormat.getPercentInstance().format(valueOf)}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView4 = this.j.i;
        m.checkNotNullExpressionValue(textView4, "binding.viewPremiumMarketingNitroClassicBoostCount");
        b.m(textView4, R.string.guild_settings_premium_upsell_body_perk_no_free_guild_subscriptions, new Object[]{i18nPluralString}, (r4 & 4) != 0 ? b.g.j : null);
        this.j.c.setOnClickListener(new a(function0));
        int ordinal = premiumTier.ordinal();
        if (ordinal == 0) {
            TextView textView5 = this.j.e;
            m.checkNotNullExpressionValue(textView5, "binding.viewPremiumMarketingMarketingSubtitle");
            textView5.setVisibility(8);
            unit = Unit.a;
        } else if (ordinal == 1) {
            LinearLayout linearLayout = this.j.d;
            m.checkNotNullExpressionValue(linearLayout, "binding.viewPremiumMarketingMarketingContainer");
            linearLayout.setVisibility(0);
            LinearLayout linearLayout2 = this.j.f205b;
            m.checkNotNullExpressionValue(linearLayout2, "binding.viewPremiumMarketingContainerTier1");
            linearLayout2.setVisibility(0);
            TextView textView6 = this.j.f;
            m.checkNotNullExpressionValue(textView6, "binding.viewPremiumMarketingMarketingTitle");
            d = b.d(this, R.string.guild_settings_premium_upsell_heading_secondary, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            ViewExtensions.setTextAndVisibilityBy(textView6, d);
            TextView textView7 = this.j.e;
            m.checkNotNullExpressionValue(textView7, "binding.viewPremiumMarketingMarketingSubtitle");
            textView7.setVisibility(8);
            unit = Unit.a;
        } else if (ordinal == 2) {
            LinearLayout linearLayout3 = this.j.d;
            m.checkNotNullExpressionValue(linearLayout3, "binding.viewPremiumMarketingMarketingContainer");
            linearLayout3.setVisibility(0);
            LinearLayout linearLayout4 = this.j.f205b;
            m.checkNotNullExpressionValue(linearLayout4, "binding.viewPremiumMarketingContainerTier1");
            linearLayout4.setVisibility(8);
            TextView textView8 = this.j.f;
            m.checkNotNullExpressionValue(textView8, "binding.viewPremiumMarketingMarketingTitle");
            d2 = b.d(this, R.string.guild_settings_premium_upsell_heading_secondary_premium_user, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            ViewExtensions.setTextAndVisibilityBy(textView8, d2);
            TextView textView9 = this.j.e;
            m.checkNotNullExpressionValue(textView9, "binding.viewPremiumMarketingMarketingSubtitle");
            d3 = b.d(this, R.string.guild_settings_premium_upsell_heading_tertiary_premium_user, new Object[]{String.valueOf(2)}, (r4 & 4) != 0 ? b.c.j : null);
            ViewExtensions.setTextAndVisibilityBy(textView9, d3);
            unit = Unit.a;
        } else if (ordinal == 3) {
            LinearLayout linearLayout5 = this.j.d;
            m.checkNotNullExpressionValue(linearLayout5, "binding.viewPremiumMarketingMarketingContainer");
            linearLayout5.setVisibility(8);
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }
}
