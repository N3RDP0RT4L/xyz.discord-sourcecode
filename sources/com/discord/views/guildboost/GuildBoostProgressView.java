package com.discord.views.guildboost;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import b.a.i.t2;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.premium.GuildBoostUtils;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildBoostProgressView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010\r\u001a\u00020\f\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u000b\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\u0012"}, d2 = {"Lcom/discord/views/guildboost/GuildBoostProgressView;", "Landroid/widget/FrameLayout;", "", "premiumTier", "premiumSubscriptionCount", "", "a", "(II)V", "Lb/a/i/t2;", "j", "Lb/a/i/t2;", "binding", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostProgressView extends FrameLayout {
    public final t2 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_premium_guild_progress, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.progress_gem_icon;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.progress_gem_icon);
        if (imageView != null) {
            i = R.id.progress_level_text;
            TextView textView = (TextView) inflate.findViewById(R.id.progress_level_text);
            if (textView != null) {
                i = R.id.progress_progress;
                ProgressBar progressBar = (ProgressBar) inflate.findViewById(R.id.progress_progress);
                if (progressBar != null) {
                    t2 t2Var = new t2((FrameLayout) inflate, imageView, textView, progressBar);
                    m.checkNotNullExpressionValue(t2Var, "ViewPremiumGuildProgress…rom(context), this, true)");
                    this.j = t2Var;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void a(int i, int i2) {
        String str;
        ImageView imageView = this.j.f200b;
        int i3 = 0;
        if (i == 0) {
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            i3 = DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.boosted_guild_tier_0, 0, 2, (Object) null);
        } else if (i == 1) {
            i3 = R.drawable.ic_boosted_guild_tier_1;
        } else if (i == 2) {
            i3 = R.drawable.ic_boosted_guild_tier_2;
        } else if (i == 3) {
            i3 = R.drawable.ic_boosted_guild_tier_3;
        }
        imageView.setImageResource(i3);
        TextView textView = this.j.c;
        m.checkNotNullExpressionValue(textView, "binding.progressLevelText");
        if (i == 0) {
            str = getContext().getString(R.string.premium_guild_header_badge_no_tier);
        } else if (i == 1) {
            str = getContext().getString(R.string.premium_guild_tier_1);
        } else if (i != 2) {
            str = i != 3 ? "" : getContext().getString(R.string.premium_guild_tier_3);
        } else {
            str = getContext().getString(R.string.premium_guild_tier_2);
        }
        textView.setText(str);
        ProgressBar progressBar = this.j.d;
        m.checkNotNullExpressionValue(progressBar, "binding.progressProgress");
        progressBar.setProgress(GuildBoostUtils.INSTANCE.calculatePercentToNextTier(i, i2));
    }
}
