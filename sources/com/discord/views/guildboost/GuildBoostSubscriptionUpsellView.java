package com.discord.views.guildboost;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.i.y2;
import b.a.k.b;
import com.discord.api.premium.PremiumTier;
import com.discord.i18n.RenderContext;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.utilities.billing.PremiumUtilsKt;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.view.extensions.ViewExtensions;
import d0.z.d.m;
import d0.z.d.o;
import java.text.NumberFormat;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: GuildBoostSubscriptionUpsellView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\f\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\r"}, d2 = {"Lcom/discord/views/guildboost/GuildBoostSubscriptionUpsellView;", "Landroid/widget/FrameLayout;", "Lcom/discord/api/premium/PremiumTier;", "userPremiumTier", "", "showBlurb", "", "a", "(Lcom/discord/api/premium/PremiumTier;Z)V", "Lb/a/i/y2;", "j", "Lb/a/i/y2;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostSubscriptionUpsellView extends FrameLayout {
    public final y2 j;

    /* compiled from: GuildBoostSubscriptionUpsellView.kt */
    /* loaded from: classes2.dex */
    public static final class a extends o implements Function1<RenderContext, Unit> {
        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            RenderContext renderContext2 = renderContext;
            m.checkNotNullParameter(renderContext2, "$receiver");
            renderContext2.e = Integer.valueOf(ColorCompat.getThemedColor(GuildBoostSubscriptionUpsellView.this.getContext(), (int) R.attr.colorTextMuted));
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostSubscriptionUpsellView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_premium_upsell_info, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.view_premium_upsell_info_subheading;
        TextView textView = (TextView) inflate.findViewById(R.id.view_premium_upsell_info_subheading);
        if (textView != null) {
            i = R.id.view_premium_upsell_info_subheading_blurb;
            TextView textView2 = (TextView) inflate.findViewById(R.id.view_premium_upsell_info_subheading_blurb);
            if (textView2 != null) {
                i = R.id.view_premium_upsell_info_subheading_price;
                TextView textView3 = (TextView) inflate.findViewById(R.id.view_premium_upsell_info_subheading_price);
                if (textView3 != null) {
                    y2 y2Var = new y2((LinearLayout) inflate, textView, textView2, textView3);
                    m.checkNotNullExpressionValue(y2Var, "ViewPremiumUpsellInfoBin…rom(context), this, true)");
                    this.j = y2Var;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void a(PremiumTier premiumTier, boolean z2) {
        CharSequence d;
        CharSequence d2;
        CharSequence d3;
        m.checkNotNullParameter(premiumTier, "userPremiumTier");
        TextView textView = this.j.c;
        m.checkNotNullExpressionValue(textView, "binding.viewPremiumUpsellInfoSubheadingBlurb");
        textView.setVisibility(z2 ? 0 : 8);
        SubscriptionPlanType subscriptionPlanType = SubscriptionPlanType.PREMIUM_GUILD_MONTH;
        int price = subscriptionPlanType.getPrice();
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        d = b.d(this, R.string.billing_price_per_month, new Object[]{PremiumUtilsKt.getFormattedPriceUsd(price, context)}, (r4 & 4) != 0 ? b.c.j : null);
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        d2 = b.d(this, R.string.billing_price_per_month, new Object[]{PremiumUtilsKt.getFormattedPriceUsd((int) (subscriptionPlanType.getPrice() * 0.7f), context2)}, (r4 & 4) != 0 ? b.c.j : null);
        d3 = b.d(this, R.string.guild_settings_premium_upsell_body_perk_guild_subscription_discount, new Object[]{NumberFormat.getPercentInstance().format(Float.valueOf(0.3f))}, (r4 & 4) != 0 ? b.c.j : null);
        Context context3 = getContext();
        m.checkNotNullExpressionValue(context3, "context");
        CharSequence b2 = b.b(context3, R.string.guild_settings_premium_upsell_subheading_extra_android, new Object[]{d2, d}, new a());
        int ordinal = premiumTier.ordinal();
        if (ordinal == 0 || ordinal == 1) {
            TextView textView2 = this.j.f229b;
            m.checkNotNullExpressionValue(textView2, "binding.viewPremiumUpsellInfoSubheading");
            b.m(textView2, R.string.guild_settings_premium_upsell_subheading, new Object[]{d}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView3 = this.j.d;
            m.checkNotNullExpressionValue(textView3, "binding.viewPremiumUpsellInfoSubheadingPrice");
            ViewExtensions.setTextAndVisibilityBy(textView3, null);
        } else if (ordinal == 2) {
            TextView textView4 = this.j.f229b;
            m.checkNotNullExpressionValue(textView4, "binding.viewPremiumUpsellInfoSubheading");
            b.m(textView4, R.string.guild_settings_premium_upsell_subheading_tier_1_mobile, new Object[]{d3}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView5 = this.j.d;
            m.checkNotNullExpressionValue(textView5, "binding.viewPremiumUpsellInfoSubheadingPrice");
            ViewExtensions.setTextAndVisibilityBy(textView5, b2);
        } else if (ordinal == 3) {
            TextView textView6 = this.j.f229b;
            m.checkNotNullExpressionValue(textView6, "binding.viewPremiumUpsellInfoSubheading");
            b.m(textView6, R.string.guild_settings_premium_upsell_subheading_tier_2_mobile, new Object[]{String.valueOf(2), d3}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView7 = this.j.d;
            m.checkNotNullExpressionValue(textView7, "binding.viewPremiumUpsellInfoSubheadingPrice");
            ViewExtensions.setTextAndVisibilityBy(textView7, b2);
        }
    }
}
