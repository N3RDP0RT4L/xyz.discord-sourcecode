package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.IntRange;
import androidx.annotation.Px;
import com.discord.R;
import com.discord.utilities.dimen.DimenUtils;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: CutoutView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0016\u0018\u00002\u00020\u0001:\u0001-B\u001d\b\u0007\u0012\u0006\u0010(\u001a\u00020'\u0012\n\b\u0002\u0010*\u001a\u0004\u0018\u00010)¢\u0006\u0004\b+\u0010,J\u000f\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH\u0014¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u0010\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R*\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00158\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u000fR*\u0010&\u001a\u00020\u001f2\u0006\u0010\u0016\u001a\u00020\u001f8\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%¨\u0006."}, d2 = {"Lcom/discord/views/CutoutView;", "Landroid/widget/FrameLayout;", "", "onFinishInflate", "()V", "Landroid/graphics/Canvas;", "canvas", "draw", "(Landroid/graphics/Canvas;)V", "", "widthMeasureSpec", "heightMeasureSpec", "onMeasure", "(II)V", "o", "I", "lastWidth", "Landroid/graphics/Path;", "n", "Landroid/graphics/Path;", "drawBounds", "", "value", "m", "Z", "isCutoutEnabled", "()Z", "setCutoutEnabled", "(Z)V", "p", "lastHeight", "Lcom/discord/views/CutoutView$a;", "l", "Lcom/discord/views/CutoutView$a;", "getStyle", "()Lcom/discord/views/CutoutView$a;", "setStyle", "(Lcom/discord/views/CutoutView$a;)V", "style", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class CutoutView extends FrameLayout {
    public static final int j = DimenUtils.dpToPixels(14);
    public static final int k = DimenUtils.dpToPixels(8);
    public a l = a.d.a;
    public boolean m = true;
    public Path n;
    public int o;
    public int p;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CutoutView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        setWillNotDraw(false);
        int[] iArr = R.a.CutoutView;
        m.checkNotNullExpressionValue(iArr, "R.styleable.CutoutView");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        if (obtainStyledAttributes.getInt(2, 0) == 1) {
            setStyle(new a.C0230a(obtainStyledAttributes.getDimensionPixelSize(1, j), obtainStyledAttributes.getDimensionPixelSize(0, k)));
        }
        obtainStyledAttributes.recycle();
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        Path path;
        m.checkNotNullParameter(canvas, "canvas");
        int save = canvas.save();
        try {
            if (this.m && (path = this.n) != null) {
                canvas.clipPath(path);
            }
            super.draw(canvas);
        } finally {
            canvas.restoreToCount(save);
        }
    }

    public final a getStyle() {
        return this.l;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode()) {
            if (getChildCount() == 0) {
                View view = new View(getContext());
                view.setBackgroundColor((int) 4278255360L);
                addView(view);
                setStyle(new a.C0230a(0, 0, 3));
            }
        }
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (measuredWidth != this.o || measuredHeight != this.p || this.n == null) {
            this.o = measuredWidth;
            this.p = measuredHeight;
            a aVar = this.l;
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            this.n = aVar.a(context, measuredWidth, measuredHeight);
        }
    }

    public final void setCutoutEnabled(boolean z2) {
        if (this.m != z2) {
            this.m = z2;
            invalidate();
        }
    }

    public final void setStyle(a aVar) {
        m.checkNotNullParameter(aVar, "value");
        this.l = aVar;
        if (this.o > 0 && this.p > 0) {
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            this.n = aVar.a(context, this.o, this.p);
            invalidate();
        }
    }

    /* compiled from: CutoutView.kt */
    /* loaded from: classes2.dex */
    public interface a {

        /* compiled from: CutoutView.kt */
        /* loaded from: classes2.dex */
        public static final class b implements a {
            public final int a;

            /* renamed from: b  reason: collision with root package name */
            public final int f2799b;

            public b(@IntRange(from = 0) int i, @IntRange(from = 0) int i2) {
                this.a = i;
                this.f2799b = i2;
            }

            @Override // com.discord.views.CutoutView.a
            public Path a(Context context, int i, int i2) {
                m.checkNotNullParameter(context, "context");
                float f = i;
                int i3 = this.a;
                float f2 = i3 - (f / 2.0f);
                float f3 = i3 * 2.0f;
                float f4 = -f2;
                float f5 = i2 + f2;
                if (b.c.a.a0.d.U0(context)) {
                    float f6 = f - this.f2799b;
                    RectF rectF = new RectF(f6, f4, f3 + f6, f5);
                    Path path = new Path();
                    path.moveTo(f4, f4);
                    path.arcTo(rectF, 270.0f, -180.0f);
                    path.lineTo(f4, f5);
                    path.close();
                    return path;
                }
                float f7 = this.f2799b;
                RectF rectF2 = new RectF(f7 - f3, f4, f7, f5);
                Path path2 = new Path();
                float f8 = f + f2;
                path2.moveTo(f8, f4);
                path2.arcTo(rectF2, 270.0f, 180.0f);
                path2.lineTo(f8, f5);
                path2.close();
                return path2;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof b)) {
                    return false;
                }
                b bVar = (b) obj;
                return this.a == bVar.a && this.f2799b == bVar.f2799b;
            }

            public int hashCode() {
                return (this.a * 31) + this.f2799b;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("End(cutCurveRadiusPx=");
                R.append(this.a);
                R.append(", cutDistanceInwardFromEdgePx=");
                return b.d.b.a.a.A(R, this.f2799b, ")");
            }
        }

        /* compiled from: CutoutView.kt */
        /* loaded from: classes2.dex */
        public static final class c implements a {
            public final int a;

            public c(int i) {
                this.a = i;
            }

            @Override // com.discord.views.CutoutView.a
            public Path a(Context context, int i, int i2) {
                m.checkNotNullParameter(context, "context");
                Resources resources = context.getResources();
                m.checkNotNullExpressionValue(resources, "context.resources");
                float f = this.a * resources.getDisplayMetrics().density;
                float f2 = i2;
                float f3 = f2 / 2.0f;
                if (b.c.a.a0.d.U0(context)) {
                    Path path = new Path();
                    float f4 = i + f;
                    path.moveTo(0.0f, 0.0f);
                    path.lineTo(f4, 0.0f);
                    path.arcTo(new RectF(f4 - f3, 0.0f, f4 + f3, f2), 270.0f, -180.0f);
                    path.lineTo(0.0f, f2);
                    path.lineTo(0.0f, 0.0f);
                    return path;
                }
                Path path2 = new Path();
                float f5 = -f;
                float f6 = i;
                path2.moveTo(f6, 0.0f);
                path2.lineTo(f5, 0.0f);
                path2.arcTo(new RectF(f5 - f3, 0.0f, f5 + f3, f2), 270.0f, 180.0f);
                path2.lineTo(f6, f2);
                path2.lineTo(f6, 0.0f);
                return path2;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof c) && this.a == ((c) obj).a;
                }
                return true;
            }

            public int hashCode() {
                return this.a;
            }

            public String toString() {
                return b.d.b.a.a.A(b.d.b.a.a.R("EndOverlap(offsetDp="), this.a, ")");
            }
        }

        /* compiled from: CutoutView.kt */
        /* loaded from: classes2.dex */
        public static final class d implements a {
            public static final d a = new d();

            @Override // com.discord.views.CutoutView.a
            public Path a(Context context, int i, int i2) {
                m.checkNotNullParameter(context, "context");
                return null;
            }
        }

        Path a(Context context, int i, int i2);

        /* compiled from: CutoutView.kt */
        /* renamed from: com.discord.views.CutoutView$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0230a implements a {
            public final int a;

            /* renamed from: b  reason: collision with root package name */
            public final int f2798b;

            public C0230a() {
                int i = CutoutView.j;
                int i2 = CutoutView.k;
                this.a = i;
                this.f2798b = i2;
            }

            @Override // com.discord.views.CutoutView.a
            public Path a(Context context, int i, int i2) {
                float f;
                m.checkNotNullParameter(context, "context");
                Path path = new Path();
                path.addRect(0.0f, 0.0f, i, i2, Path.Direction.CW);
                Path path2 = new Path();
                if (b.c.a.a0.d.U0(context)) {
                    f = i - this.f2798b;
                } else {
                    f = this.f2798b;
                }
                path2.addCircle(f, this.f2798b, this.a, Path.Direction.CW);
                Path path3 = new Path(path);
                path3.op(path2, Path.Op.DIFFERENCE);
                return path3;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof C0230a)) {
                    return false;
                }
                C0230a aVar = (C0230a) obj;
                return this.a == aVar.a && this.f2798b == aVar.f2798b;
            }

            public int hashCode() {
                return (this.a * 31) + this.f2798b;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("CircularBadge(badgeRadius=");
                R.append(this.a);
                R.append(", inset=");
                return b.d.b.a.a.A(R, this.f2798b, ")");
            }

            public C0230a(int i, int i2, int i3) {
                i = (i3 & 1) != 0 ? CutoutView.j : i;
                i2 = (i3 & 2) != 0 ? CutoutView.k : i2;
                this.a = i;
                this.f2798b = i2;
            }

            public C0230a(@Px int i, @Px int i2) {
                this.a = i;
                this.f2798b = i2;
            }
        }
    }
}
