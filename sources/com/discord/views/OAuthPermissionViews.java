package com.discord.views;

import andhook.lib.HookHelper;
import android.widget.TextView;
import b.a.i.v0;
import com.discord.api.auth.OAuthScope;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import xyz.discord.R;
/* compiled from: OAuthPermissionViews.kt */
/* loaded from: classes2.dex */
public final class OAuthPermissionViews {

    /* compiled from: OAuthPermissionViews.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00060\u0001j\u0002`\u0002B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\u0004\u001a\u00020\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/views/OAuthPermissionViews$InvalidScopeException;", "Ljava/lang/IllegalArgumentException;", "Lkotlin/IllegalArgumentException;", "", "scope", "Ljava/lang/String;", "a", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class InvalidScopeException extends IllegalArgumentException {
        private final String scope;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public InvalidScopeException(String str) {
            super("invalid scope: " + str);
            m.checkNotNullParameter(str, "scope");
            this.scope = str;
        }

        public final String a() {
            return this.scope;
        }
    }

    /* compiled from: OAuthPermissionViews.kt */
    /* loaded from: classes2.dex */
    public static final class a extends SimpleRecyclerAdapter.ViewHolder<OAuthScope> {
        public final v0 a;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(b.a.i.v0 r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.TextView r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r0)
                r2.a = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.views.OAuthPermissionViews.a.<init>(b.a.i.v0):void");
        }

        @Override // com.discord.utilities.views.SimpleRecyclerAdapter.ViewHolder
        public void bind(OAuthScope oAuthScope) {
            OAuthScope oAuthScope2 = oAuthScope;
            m.checkNotNullParameter(oAuthScope2, "data");
            TextView textView = this.a.a;
            m.checkNotNullExpressionValue(textView, "binding.root");
            OAuthPermissionViews.a(textView, oAuthScope2);
        }
    }

    public static final void a(TextView textView, OAuthScope oAuthScope) throws InvalidScopeException {
        int i;
        m.checkNotNullParameter(textView, "$this$setScopePermissionText");
        m.checkNotNullParameter(oAuthScope, "scope");
        if (m.areEqual(oAuthScope, OAuthScope.Identify.INSTANCE)) {
            i = R.string.scope_identify;
        } else if (m.areEqual(oAuthScope, OAuthScope.Email.INSTANCE)) {
            i = R.string.scope_email;
        } else if (m.areEqual(oAuthScope, OAuthScope.Connections.INSTANCE)) {
            i = R.string.scope_connections;
        } else if (m.areEqual(oAuthScope, OAuthScope.Guilds.INSTANCE)) {
            i = R.string.scope_guilds;
        } else if (m.areEqual(oAuthScope, OAuthScope.GuildsJoin.INSTANCE)) {
            i = R.string.scope_guilds_join;
        } else if (m.areEqual(oAuthScope, OAuthScope.GuildsMembersRead.INSTANCE)) {
            i = R.string.scope_guilds_members_read;
        } else if (m.areEqual(oAuthScope, OAuthScope.GdmJoin.INSTANCE)) {
            i = R.string.scope_gdm_join;
        } else if (m.areEqual(oAuthScope, OAuthScope.Bot.INSTANCE)) {
            i = R.string.scope_bot;
        } else if (m.areEqual(oAuthScope, OAuthScope.WebhookIncoming.INSTANCE)) {
            i = R.string.scope_webhook_incoming;
        } else if (m.areEqual(oAuthScope, OAuthScope.Rpc.INSTANCE)) {
            i = R.string.scope_rpc;
        } else if (m.areEqual(oAuthScope, OAuthScope.RpcNotificationsRead.INSTANCE)) {
            i = R.string.scope_rpc_notifications_read;
        } else if (m.areEqual(oAuthScope, OAuthScope.RpcVoiceRead.INSTANCE)) {
            i = R.string.scope_rpc_voice_read;
        } else if (m.areEqual(oAuthScope, OAuthScope.RpcVoiceWrite.INSTANCE)) {
            i = R.string.scope_rpc_voice_write;
        } else if (m.areEqual(oAuthScope, OAuthScope.RpcActivitiesWrite.INSTANCE)) {
            i = R.string.scope_rpc_activities_write;
        } else if (m.areEqual(oAuthScope, OAuthScope.MessagesRead.INSTANCE)) {
            i = R.string.scope_messages_read;
        } else if (m.areEqual(oAuthScope, OAuthScope.ApplicationsBuildsUpload.INSTANCE)) {
            i = R.string.scope_applications_builds_upload;
        } else if (m.areEqual(oAuthScope, OAuthScope.ApplicationsBuildsRead.INSTANCE)) {
            i = R.string.scope_applications_builds_read;
        } else if (m.areEqual(oAuthScope, OAuthScope.ApplicationsCommands.INSTANCE)) {
            i = R.string.scope_applications_commands;
        } else if (m.areEqual(oAuthScope, OAuthScope.ApplicationsCommandsUpdate.INSTANCE)) {
            i = R.string.scope_applications_commands_update;
        } else if (m.areEqual(oAuthScope, OAuthScope.ApplicationsStoreUpdate.INSTANCE)) {
            i = R.string.scope_applications_store_update;
        } else if (m.areEqual(oAuthScope, OAuthScope.ApplicationsEntitlements.INSTANCE)) {
            i = R.string.scope_applications_entitlements;
        } else if (m.areEqual(oAuthScope, OAuthScope.ActivitiesRead.INSTANCE)) {
            i = R.string.scope_activities_read;
        } else if (m.areEqual(oAuthScope, OAuthScope.ActivitiesWrite.INSTANCE)) {
            i = R.string.scope_activities_write;
        } else if (m.areEqual(oAuthScope, OAuthScope.RelationshipsRead.INSTANCE)) {
            i = R.string.scope_relationships_read;
        } else if (oAuthScope instanceof OAuthScope.Invalid) {
            throw new InvalidScopeException(((OAuthScope.Invalid) oAuthScope).b());
        } else {
            throw new NoWhenBranchMatchedException();
        }
        textView.setText(i);
    }
}
