package com.discord.views.calls;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.PathInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import d0.g;
import d0.i;
import d0.l;
import d0.w.i.a.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Job;
import org.objectweb.asm.Opcodes;
import s.a.u;
import xyz.discord.R;
/* compiled from: SpeakerPulseView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0007\u0012\u0006\u00101\u001a\u000200\u0012\n\b\u0002\u00103\u001a\u0004\u0018\u000102¢\u0006\u0004\b4\u00105J\u000f\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0006\u001a\u00020\u0002*\u00020\u0005H\u0082@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007J'\u0010\f\u001a\u00020\u0002*\u00020\b2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH\u0082@ø\u0001\u0000¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000e\u0010\u0004R\u0016\u0010\u0012\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0016\u001a\u00020\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0018\u0010 \u001a\u0004\u0018\u00010\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001d\u0010#\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\u0019\u001a\u0004\b\"\u0010\u001bR\u0016\u0010'\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R*\u0010*\u001a\u00020\u00132\u0006\u0010(\u001a\u00020\u00138\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\b)\u0010\u0015\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010\u0011\u0082\u0002\u0004\n\u0002\b\u0019¨\u00066"}, d2 = {"Lcom/discord/views/calls/SpeakerPulseView;", "Landroid/widget/FrameLayout;", "", "onAttachedToWindow", "()V", "Lkotlinx/coroutines/CoroutineScope;", "e", "(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Landroid/view/View;", "", "rampStartDelay", "rampedDuration", "d", "(Landroid/view/View;JJLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "f", "Landroid/view/animation/PathInterpolator;", "j", "Landroid/view/animation/PathInterpolator;", "rampUpInterpolator", "", "l", "Z", "hasEverAnimated", "Landroid/widget/ImageView;", "o", "Lkotlin/Lazy;", "getInnerView", "()Landroid/widget/ImageView;", "innerView", "Lkotlinx/coroutines/Job;", "n", "Lkotlinx/coroutines/Job;", "animationJob", "p", "getOuterView", "outerView", "Ls/a/u;", "m", "Ls/a/u;", "animationSupervisor", "value", "q", "isPulsing", "()Z", "setPulsing", "(Z)V", "k", "fadeOutInterpolator", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpeakerPulseView extends FrameLayout {
    public final PathInterpolator j;
    public final PathInterpolator k;
    public boolean l;
    public final u m = b.i.a.f.e.o.f.d(null, 1);
    public Job n;
    public final Lazy o;
    public final Lazy p;
    public boolean q;

    /* compiled from: kotlin-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a extends o implements Function0<ImageView> {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(int i, Object obj) {
            super(0);
            this.j = i;
            this.k = obj;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ImageView invoke() {
            int i = this.j;
            if (i == 0) {
                return SpeakerPulseView.a((SpeakerPulseView) this.k, 0);
            }
            if (i == 1) {
                return SpeakerPulseView.a((SpeakerPulseView) this.k, 1);
            }
            throw null;
        }
    }

    /* compiled from: SpeakerPulseView.kt */
    @d0.w.i.a.e(c = "com.discord.views.calls.SpeakerPulseView", f = "SpeakerPulseView.kt", l = {Opcodes.LOR, Opcodes.I2D}, m = "animatePulse")
    /* loaded from: classes2.dex */
    public static final class b extends d0.w.i.a.d {
        public long J$0;
        public Object L$0;
        public Object L$1;
        public int label;
        public /* synthetic */ Object result;

        public b(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return SpeakerPulseView.this.d(null, 0L, 0L, this);
        }
    }

    /* compiled from: SpeakerPulseView.kt */
    @d0.w.i.a.e(c = "com.discord.views.calls.SpeakerPulseView", f = "SpeakerPulseView.kt", l = {117}, m = "performPulseAnimation")
    /* loaded from: classes2.dex */
    public static final class c extends d0.w.i.a.d {
        public Object L$0;
        public Object L$1;
        public int label;
        public /* synthetic */ Object result;

        public c(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return SpeakerPulseView.this.e(null, this);
        }
    }

    /* compiled from: SpeakerPulseView.kt */
    @d0.w.i.a.e(c = "com.discord.views.calls.SpeakerPulseView$performPulseAnimation$inner$1", f = "SpeakerPulseView.kt", l = {106}, m = "invokeSuspend")
    /* loaded from: classes2.dex */
    public static final class d extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public int label;

        public d(Continuation continuation) {
            super(2, continuation);
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            m.checkNotNullParameter(continuation, "completion");
            return new d(continuation);
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            Continuation<? super Unit> continuation2 = continuation;
            m.checkNotNullParameter(continuation2, "completion");
            return new d(continuation2).invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            Object coroutine_suspended = d0.w.h.c.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                l.throwOnFailure(obj);
                SpeakerPulseView speakerPulseView = SpeakerPulseView.this;
                ImageView innerView = speakerPulseView.getInnerView();
                this.label = 1;
                if (speakerPulseView.d(innerView, 200L, 500L, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                l.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Unit.a;
        }
    }

    /* compiled from: SpeakerPulseView.kt */
    @d0.w.i.a.e(c = "com.discord.views.calls.SpeakerPulseView$performPulseAnimation$outer$1", f = "SpeakerPulseView.kt", l = {113}, m = "invokeSuspend")
    /* loaded from: classes2.dex */
    public static final class e extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public int label;

        public e(Continuation continuation) {
            super(2, continuation);
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            m.checkNotNullParameter(continuation, "completion");
            return new e(continuation);
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            Continuation<? super Unit> continuation2 = continuation;
            m.checkNotNullParameter(continuation2, "completion");
            return new e(continuation2).invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            Object coroutine_suspended = d0.w.h.c.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                l.throwOnFailure(obj);
                SpeakerPulseView speakerPulseView = SpeakerPulseView.this;
                ImageView outerView = speakerPulseView.getOuterView();
                this.label = 1;
                if (speakerPulseView.d(outerView, 700L, 0L, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                l.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Unit.a;
        }
    }

    /* compiled from: SpeakerPulseView.kt */
    @d0.w.i.a.e(c = "com.discord.views.calls.SpeakerPulseView$startAnimating$1", f = "SpeakerPulseView.kt", l = {87}, m = "invokeSuspend")
    /* loaded from: classes2.dex */
    public static final class f extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public int label;

        /* compiled from: SpeakerPulseView.kt */
        @d0.w.i.a.e(c = "com.discord.views.calls.SpeakerPulseView$startAnimating$1$1", f = "SpeakerPulseView.kt", l = {88}, m = "invokeSuspend")
        /* loaded from: classes2.dex */
        public static final class a extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
            private /* synthetic */ Object L$0;
            public int label;

            public a(Continuation continuation) {
                super(2, continuation);
            }

            @Override // d0.w.i.a.a
            public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
                m.checkNotNullParameter(continuation, "completion");
                a aVar = new a(continuation);
                aVar.L$0 = obj;
                return aVar;
            }

            @Override // kotlin.jvm.functions.Function2
            public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
                Continuation<? super Unit> continuation2 = continuation;
                m.checkNotNullParameter(continuation2, "completion");
                a aVar = new a(continuation2);
                aVar.L$0 = coroutineScope;
                return aVar.invokeSuspend(Unit.a);
            }

            @Override // d0.w.i.a.a
            public final Object invokeSuspend(Object obj) {
                Object coroutine_suspended = d0.w.h.c.getCOROUTINE_SUSPENDED();
                int i = this.label;
                if (i == 0) {
                    l.throwOnFailure(obj);
                    SpeakerPulseView speakerPulseView = SpeakerPulseView.this;
                    this.label = 1;
                    if (speakerPulseView.e((CoroutineScope) this.L$0, this) == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                } else if (i == 1) {
                    l.throwOnFailure(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Unit.a;
            }
        }

        public f(Continuation continuation) {
            super(2, continuation);
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            m.checkNotNullParameter(continuation, "completion");
            return new f(continuation);
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            Continuation<? super Unit> continuation2 = continuation;
            m.checkNotNullParameter(continuation2, "completion");
            return new f(continuation2).invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            Object coroutine_suspended = d0.w.h.c.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                l.throwOnFailure(obj);
                u uVar = SpeakerPulseView.this.m;
                a aVar = new a(null);
                this.label = 1;
                if (b.i.a.f.e.o.f.C1(uVar, aVar, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                l.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SpeakerPulseView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        PathInterpolator pathInterpolator = new PathInterpolator(0.4f, 0.0f, 1.0f, 1.0f);
        this.j = pathInterpolator;
        this.k = pathInterpolator;
        i iVar = i.NONE;
        this.o = g.lazy(iVar, new a(0, this));
        this.p = g.lazy(iVar, new a(1, this));
        setClipChildren(false);
        setClipToOutline(false);
        setClipToPadding(false);
    }

    public static final ImageView a(SpeakerPulseView speakerPulseView, int i) {
        Objects.requireNonNull(speakerPulseView);
        ImageView imageView = new ImageView(speakerPulseView.getContext());
        int i2 = -1;
        View childAt = speakerPulseView.getChildAt(speakerPulseView.getChildCount() - 1);
        float paddingLeft = speakerPulseView.getPaddingLeft() / 2.0f;
        int i3 = i + 1;
        m.checkNotNullExpressionValue(childAt, "actualChild");
        ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
        Integer valueOf = layoutParams != null ? Integer.valueOf(layoutParams.width + ((int) (2 * paddingLeft * i3))) : null;
        imageView.setAlpha(0.0f);
        imageView.setImageResource(R.drawable.drawable_circle_white);
        int coerceAtLeast = d0.d0.f.coerceAtLeast(speakerPulseView.getChildCount() - 1, 0);
        int intValue = valueOf != null ? valueOf.intValue() : -1;
        if (valueOf != null) {
            i2 = valueOf.intValue();
        }
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(intValue, i2);
        layoutParams2.gravity = 17;
        speakerPulseView.addView(imageView, coerceAtLeast, layoutParams2);
        return imageView;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ImageView getInnerView() {
        return (ImageView) this.o.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ImageView getOuterView() {
        return (ImageView) this.p.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00a0 A[RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final /* synthetic */ java.lang.Object d(android.view.View r8, long r9, long r11, kotlin.coroutines.Continuation<? super kotlin.Unit> r13) {
        /*
            r7 = this;
            boolean r0 = r13 instanceof com.discord.views.calls.SpeakerPulseView.b
            if (r0 == 0) goto L13
            r0 = r13
            com.discord.views.calls.SpeakerPulseView$b r0 = (com.discord.views.calls.SpeakerPulseView.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            com.discord.views.calls.SpeakerPulseView$b r0 = new com.discord.views.calls.SpeakerPulseView$b
            r0.<init>(r13)
        L18:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L42
            if (r2 == r4) goto L34
            if (r2 != r3) goto L2c
            d0.l.throwOnFailure(r13)
            goto La1
        L2c:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L34:
            long r11 = r0.J$0
            java.lang.Object r8 = r0.L$1
            android.view.View r8 = (android.view.View) r8
            java.lang.Object r9 = r0.L$0
            com.discord.views.calls.SpeakerPulseView r9 = (com.discord.views.calls.SpeakerPulseView) r9
            d0.l.throwOnFailure(r13)
            goto L75
        L42:
            d0.l.throwOnFailure(r13)
            android.view.ViewPropertyAnimator r13 = r8.animate()
            android.view.animation.PathInterpolator r2 = r7.j
            android.view.ViewPropertyAnimator r13 = r13.setInterpolator(r2)
            android.view.ViewPropertyAnimator r9 = r13.setStartDelay(r9)
            r5 = 500(0x1f4, double:2.47E-321)
            android.view.ViewPropertyAnimator r9 = r9.setDuration(r5)
            r10 = 1042536202(0x3e23d70a, float:0.16)
            android.view.ViewPropertyAnimator r9 = r9.alpha(r10)
            java.lang.String r10 = "animate()\n        .setIn…    .alpha(PULSE_OPACITY)"
            d0.z.d.m.checkNotNullExpressionValue(r9, r10)
            r0.L$0 = r7
            r0.L$1 = r8
            r0.J$0 = r11
            r0.label = r4
            java.lang.Object r9 = com.discord.utilities.animations.AnimationCoroutineUtilsKt.await(r9, r0)
            if (r9 != r1) goto L74
            return r1
        L74:
            r9 = r7
        L75:
            android.view.ViewPropertyAnimator r8 = r8.animate()
            android.view.animation.PathInterpolator r9 = r9.k
            android.view.ViewPropertyAnimator r8 = r8.setInterpolator(r9)
            android.view.ViewPropertyAnimator r8 = r8.setStartDelay(r11)
            r9 = 1000(0x3e8, double:4.94E-321)
            android.view.ViewPropertyAnimator r8 = r8.setDuration(r9)
            r9 = 0
            android.view.ViewPropertyAnimator r8 = r8.alpha(r9)
            java.lang.String r9 = "animate()\n        .setIn…ON_MS)\n        .alpha(0f)"
            d0.z.d.m.checkNotNullExpressionValue(r8, r9)
            r9 = 0
            r0.L$0 = r9
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r8 = com.discord.utilities.animations.AnimationCoroutineUtilsKt.await(r8, r0)
            if (r8 != r1) goto La1
            return r1
        La1:
            kotlin.Unit r8 = kotlin.Unit.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.calls.SpeakerPulseView.d(android.view.View, long, long, kotlin.coroutines.Continuation):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x004c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final /* synthetic */ java.lang.Object e(kotlinx.coroutines.CoroutineScope r19, kotlin.coroutines.Continuation<? super kotlin.Unit> r20) {
        /*
            r18 = this;
            r0 = r20
            boolean r1 = r0 instanceof com.discord.views.calls.SpeakerPulseView.c
            if (r1 == 0) goto L17
            r1 = r0
            com.discord.views.calls.SpeakerPulseView$c r1 = (com.discord.views.calls.SpeakerPulseView.c) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L17
            int r2 = r2 - r3
            r1.label = r2
            r2 = r18
            goto L1e
        L17:
            com.discord.views.calls.SpeakerPulseView$c r1 = new com.discord.views.calls.SpeakerPulseView$c
            r2 = r18
            r1.<init>(r0)
        L1e:
            java.lang.Object r0 = r1.result
            java.lang.Object r3 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r4 = r1.label
            r5 = 1
            if (r4 == 0) goto L40
            if (r4 != r5) goto L38
            java.lang.Object r4 = r1.L$1
            kotlinx.coroutines.CoroutineScope r4 = (kotlinx.coroutines.CoroutineScope) r4
            java.lang.Object r6 = r1.L$0
            com.discord.views.calls.SpeakerPulseView r6 = (com.discord.views.calls.SpeakerPulseView) r6
            d0.l.throwOnFailure(r0)
            r0 = r6
            goto L46
        L38:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L40:
            d0.l.throwOnFailure(r0)
            r4 = r19
            r0 = r2
        L46:
            boolean r6 = b.i.a.f.e.o.f.y0(r4)
            if (r6 == 0) goto L83
            r12 = 0
            r13 = 0
            com.discord.views.calls.SpeakerPulseView$d r9 = new com.discord.views.calls.SpeakerPulseView$d
            r14 = 0
            r9.<init>(r14)
            r15 = 3
            r16 = 0
            r7 = 0
            r8 = 0
            r10 = 3
            r11 = 0
            r6 = r4
            s.a.f0 r17 = b.i.a.f.e.o.f.i(r6, r7, r8, r9, r10, r11)
            com.discord.views.calls.SpeakerPulseView$e r9 = new com.discord.views.calls.SpeakerPulseView$e
            r9.<init>(r14)
            r7 = r12
            r8 = r13
            r10 = r15
            r11 = r16
            s.a.f0 r6 = b.i.a.f.e.o.f.i(r6, r7, r8, r9, r10, r11)
            r7 = 2
            s.a.f0[] r7 = new s.a.f0[r7]
            r8 = 0
            r7[r8] = r17
            r7[r5] = r6
            r1.L$0 = r0
            r1.L$1 = r4
            r1.label = r5
            java.lang.Object r6 = b.i.a.f.e.o.f.l(r7, r1)
            if (r6 != r3) goto L46
            return r3
        L83:
            kotlin.Unit r0 = kotlin.Unit.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.calls.SpeakerPulseView.e(kotlinx.coroutines.CoroutineScope, kotlin.coroutines.Continuation):java.lang.Object");
    }

    public final void f() {
        CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(this);
        if (coroutineScope != null && !AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
            this.l = true;
            Job job = this.n;
            if (job != null) {
                b.i.a.f.e.o.f.t(job, null, 1, null);
            }
            this.n = b.i.a.f.e.o.f.H0(coroutineScope, null, null, new f(null), 3, null);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.q) {
            f();
        }
    }

    public final void setPulsing(boolean z2) {
        if (this.q != z2) {
            this.q = z2;
            if (z2) {
                f();
                return;
            }
            for (Job job : this.m.e()) {
                job.b(null);
            }
            if (this.l) {
                getInnerView().animate().setDuration(500L).alpha(0.0f);
                getOuterView().animate().setDuration(500L).alpha(0.0f);
            }
        }
    }
}
