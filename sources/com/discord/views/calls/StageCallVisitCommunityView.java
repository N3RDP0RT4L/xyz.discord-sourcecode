package com.discord.views.calls;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.t3;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: StageCallVisitCommunityView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/discord/views/calls/StageCallVisitCommunityView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lb/a/i/t3;", "j", "Lb/a/i/t3;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageCallVisitCommunityView extends ConstraintLayout {
    public final t3 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StageCallVisitCommunityView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_stage_channel_visit_community, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.visit_community_icon;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.visit_community_icon);
        if (simpleDraweeView != null) {
            i = R.id.visit_community_icon_text;
            TextView textView = (TextView) inflate.findViewById(R.id.visit_community_icon_text);
            if (textView != null) {
                i = R.id.visit_community_text;
                TextView textView2 = (TextView) inflate.findViewById(R.id.visit_community_text);
                if (textView2 != null) {
                    t3 t3Var = new t3((ConstraintLayout) inflate, simpleDraweeView, textView, textView2);
                    m.checkNotNullExpressionValue(t3Var, "ViewStageChannelVisitCom…rom(context), this, true)");
                    this.j = t3Var;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }
}
