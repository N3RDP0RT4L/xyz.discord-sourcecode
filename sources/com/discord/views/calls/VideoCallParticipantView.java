package com.discord.views.calls;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.DisplayCutoutCompat;
import b.a.i.d4;
import b.a.k.b;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.grid.FrameGridLayout;
import com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter;
import d0.g0.w;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.webrtc.RendererCommon;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: VideoCallParticipantView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0017\u0018\u00002\u00020\u00012\u00020\u0002:\u0003PQRJ\u0011\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\bH\u0014¢\u0006\u0004\b\u000b\u0010\fJC\u0010\u0015\u001a\u00020\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010\u0011\u001a\u00020\u00102\b\b\u0002\u0010\u0013\u001a\u00020\u00122\b\b\u0002\u0010\u0014\u001a\u00020\u0010H\u0007¢\u0006\u0004\b\u0015\u0010\u0016J%\u0010\u001a\u001a\u00020\b2\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0018\u0012\u0004\u0012\u00020\b0\u0017¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001d\u001a\u00020\b2\u0006\u0010\u001c\u001a\u00020\rH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010 \u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u0010H\u0002¢\u0006\u0004\b \u0010!R\u0018\u0010%\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u0018\u0010+\u001a\u0004\u0018\u00010(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u00101\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u00103R$\u0010\u0007\u001a\u0004\u0018\u00010\r8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b4\u00105\u001a\u0004\b6\u00107\"\u0004\b8\u0010\u001eR.\u0010?\u001a\u001a\u0012\u0004\u0012\u00020:\u0012\u0006\u0012\u0004\u0018\u00010;\u0012\u0004\u0012\u00020\b09j\u0002`<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b=\u0010>R\u0016\u0010A\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u00103R&\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0018\u0012\u0004\u0012\u00020\b0\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010CR\u0018\u0010E\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010$R\u0018\u0010G\u001a\u0004\u0018\u00010(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bF\u0010*R\u0018\u0010J\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bH\u0010IR.\u0010L\u001a\u001a\u0012\u0004\u0012\u00020:\u0012\u0006\u0012\u0004\u0018\u00010;\u0012\u0004\u0012\u00020\b09j\u0002`<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bK\u0010>R\u0016\u0010N\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bM\u00103R\u0016\u0010\u0014\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bO\u00103¨\u0006S"}, d2 = {"Lcom/discord/views/calls/VideoCallParticipantView;", "Lcom/discord/utilities/view/grid/FrameGridLayout$DataView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "getDataId", "()Ljava/lang/String;", "Lcom/discord/utilities/view/grid/FrameGridLayout$Data;", "data", "", "onBind", "(Lcom/discord/utilities/view/grid/FrameGridLayout$Data;)V", "onDetachedFromWindow", "()V", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;", "Landroidx/core/view/DisplayCutoutCompat;", "displayCutout", "", "canOverlapTopCutout", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;", "callUiInsets", "controlsVisible", "c", "(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Landroidx/core/view/DisplayCutoutCompat;ZLcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;Z)V", "Lkotlin/Function1;", "Lcom/discord/primitives/StreamKey;", "onWatchStreamClicked", "setOnWatchStreamClicked", "(Lkotlin/jvm/functions/Function1;)V", "participantData", "a", "(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V", "visible", "b", "(Z)V", "Landroid/graphics/drawable/Drawable;", "w", "Landroid/graphics/drawable/Drawable;", "liveSplitPillBg", "r", "Landroidx/core/view/DisplayCutoutCompat;", "Lrx/Subscription;", "o", "Lrx/Subscription;", "representativeColorSubscription", "t", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;", "Lb/a/i/d4;", "j", "Lb/a/i/d4;", "binding", "s", "Z", "l", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;", "getData", "()Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;", "setData", "Lkotlin/Function2;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamFps;", "Lcom/discord/views/calls/StreamQualityCallback;", "y", "Lkotlin/jvm/functions/Function2;", "onStreamQualityIndicatorShown", "k", "matchVideoOrientation", "n", "Lkotlin/jvm/functions/Function1;", "v", "livePillBg", "q", "frameRenderedSubscription", "m", "Ljava/lang/String;", "userAvatarUrl", "x", "onStreamQualityIndicatorClicked", "p", "animateLabelFades", "u", "ParticipantData", "StreamFps", "StreamResolution", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VideoCallParticipantView extends ConstraintLayout implements FrameGridLayout.DataView {
    public final d4 j;
    public final boolean k;
    public ParticipantData l;
    public String m;
    public Function1<? super String, Unit> n;
    public Subscription o;
    public boolean p;
    public Subscription q;
    public DisplayCutoutCompat r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f2809s;
    public VideoCallGridAdapter.CallUiInsets t;
    public boolean u;
    public Drawable v;
    public Drawable w;

    /* renamed from: x  reason: collision with root package name */
    public Function2<? super StreamResolution, ? super StreamFps, Unit> f2810x;

    /* renamed from: y  reason: collision with root package name */
    public Function2<? super StreamResolution, ? super StreamFps, Unit> f2811y;

    /* compiled from: VideoCallParticipantView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0010\u001a\u00020\b¢\u0006\u0004\b\u0011\u0010\u0012J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\f\u001a\u00020\u00022\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u000e\u001a\u0004\b\u000f\u0010\n¨\u0006\u0013"}, d2 = {"Lcom/discord/views/calls/VideoCallParticipantView$StreamFps;", "", "", "a", "()Z", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getFps", "fps", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StreamFps {
        public final int a;

        public StreamFps(int i) {
            this.a = i;
        }

        public final boolean a() {
            return this.a == 60;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof StreamFps) && this.a == ((StreamFps) obj).a;
            }
            return true;
        }

        public int hashCode() {
            return this.a;
        }

        public String toString() {
            return b.d.b.a.a.A(b.d.b.a.a.R("StreamFps(fps="), this.a, ")");
        }
    }

    /* compiled from: VideoCallParticipantView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0005\bB\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\b\u0010\t\u0082\u0001\u0002\f\r¨\u0006\u000e"}, d2 = {"Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution;", "", "Landroid/content/Context;", "context", "", "a", "(Landroid/content/Context;)Ljava/lang/CharSequence;", "", "b", "()Z", HookHelper.constructorName, "()V", "Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution$b;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution$a;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StreamResolution {

        /* compiled from: VideoCallParticipantView.kt */
        /* loaded from: classes2.dex */
        public static final class a extends StreamResolution {
            public final int a;

            public a(int i) {
                super(null);
                this.a = i;
            }

            @Override // com.discord.views.calls.VideoCallParticipantView.StreamResolution
            public CharSequence a(Context context) {
                CharSequence b2;
                m.checkNotNullParameter(context, "context");
                b2 = b.a.k.b.b(context, R.string.screenshare_resolution_abbreviated, new Object[]{Integer.valueOf(this.a)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b2;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof a) && this.a == ((a) obj).a;
                }
                return true;
            }

            public int hashCode() {
                return this.a;
            }

            public String toString() {
                return b.d.b.a.a.A(b.d.b.a.a.R("Fixed(heightPx="), this.a, ")");
            }
        }

        /* compiled from: VideoCallParticipantView.kt */
        /* loaded from: classes2.dex */
        public static final class b extends StreamResolution {
            public static final b a = new b();

            public b() {
                super(null);
            }

            @Override // com.discord.views.calls.VideoCallParticipantView.StreamResolution
            public CharSequence a(Context context) {
                CharSequence b2;
                m.checkNotNullParameter(context, "context");
                b2 = b.a.k.b.b(context, R.string.screenshare_source, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b2;
            }
        }

        public StreamResolution() {
        }

        public abstract CharSequence a(Context context);

        public final boolean b() {
            return (this instanceof b) || ((this instanceof a) && ((a) this).a == 1080);
        }

        public StreamResolution(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: VideoCallParticipantView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ ParticipantData.a k;

        public a(ParticipantData.a aVar) {
            this.k = aVar;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Function2<? super StreamResolution, ? super StreamFps, Unit> function2 = VideoCallParticipantView.this.f2810x;
            ParticipantData.a aVar = this.k;
            function2.invoke(aVar.f2813b, aVar.c);
        }
    }

    public VideoCallParticipantView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public VideoCallParticipantView(android.content.Context r29, android.util.AttributeSet r30, int r31, int r32) {
        /*
            Method dump skipped, instructions count: 459
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.calls.VideoCallParticipantView.<init>(android.content.Context, android.util.AttributeSet, int, int):void");
    }

    public static /* synthetic */ void d(VideoCallParticipantView videoCallParticipantView, ParticipantData participantData, DisplayCutoutCompat displayCutoutCompat, boolean z2, VideoCallGridAdapter.CallUiInsets callUiInsets, boolean z3, int i) {
        VideoCallGridAdapter.CallUiInsets callUiInsets2 = null;
        DisplayCutoutCompat displayCutoutCompat2 = (i & 2) != 0 ? videoCallParticipantView.r : null;
        if ((i & 4) != 0) {
            z2 = videoCallParticipantView.f2809s;
        }
        boolean z4 = z2;
        if ((i & 8) != 0) {
            callUiInsets2 = videoCallParticipantView.t;
        }
        VideoCallGridAdapter.CallUiInsets callUiInsets3 = callUiInsets2;
        if ((i & 16) != 0) {
            z3 = videoCallParticipantView.u;
        }
        videoCallParticipantView.c(participantData, displayCutoutCompat2, z4, callUiInsets3, z3);
    }

    public final void a(ParticipantData participantData) {
        CharSequence charSequence;
        ParticipantData.a aVar = participantData.j;
        int i = 0;
        if (!participantData.i || !this.u || aVar == null) {
            b(false);
            return;
        }
        ConstraintLayout constraintLayout = this.j.e;
        constraintLayout.setTranslationX(-this.t.getRight());
        constraintLayout.setTranslationY(this.t.getTop());
        constraintLayout.setOnClickListener(new a(aVar));
        ImageView imageView = this.j.f;
        m.checkNotNullExpressionValue(imageView, "binding.participantFullscreenStreamqualIcon");
        imageView.setVisibility(aVar.a ? 0 : 8);
        StreamResolution streamResolution = aVar.f2813b;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        CharSequence a2 = streamResolution.a(context);
        StreamFps streamFps = aVar.c;
        if (streamFps != null) {
            Context context2 = getContext();
            m.checkNotNullExpressionValue(context2, "context");
            m.checkNotNullParameter(context2, "context");
            charSequence = b.b(context2, R.string.screenshare_fps_abbreviated, new Object[]{Integer.valueOf(streamFps.a)}, (r4 & 4) != 0 ? b.C0034b.j : null);
        } else {
            charSequence = "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(a2);
        sb.append(' ');
        sb.append(charSequence);
        String sb2 = sb.toString();
        Objects.requireNonNull(sb2, "null cannot be cast to non-null type kotlin.CharSequence");
        String obj = w.trim(sb2).toString();
        Drawable drawable = (!(obj.length() == 0) || aVar.a) ? this.w : this.v;
        TextView textView = this.j.h;
        m.checkNotNullExpressionValue(textView, "binding.participantFulls…enStreamqualLiveIndicator");
        textView.setBackground(drawable);
        int i2 = aVar.d ? R.color.primary_300 : R.color.white;
        TextView textView2 = this.j.g;
        textView2.setText(obj);
        CharSequence text = textView2.getText();
        m.checkNotNullExpressionValue(text, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        if (!(text.length() > 0)) {
            i = 8;
        }
        textView2.setVisibility(i);
        textView2.setTextColor(ContextCompat.getColor(textView2.getContext(), i2));
        textView2.requestLayout();
        this.f2811y.invoke(aVar.f2813b, aVar.c);
        b(true);
    }

    public final void b(boolean z2) {
        ViewExtensions.fadeBy(this.j.e, z2, 200L);
    }

    /* JADX WARN: Removed duplicated region for block: B:141:0x04c3  */
    /* JADX WARN: Removed duplicated region for block: B:142:0x04c6  */
    /* JADX WARN: Removed duplicated region for block: B:144:0x04c9  */
    /* JADX WARN: Removed duplicated region for block: B:145:0x04cc  */
    /* JADX WARN: Removed duplicated region for block: B:147:0x04cf  */
    /* JADX WARN: Removed duplicated region for block: B:148:0x04d4  */
    /* JADX WARN: Removed duplicated region for block: B:151:0x04de  */
    /* JADX WARN: Removed duplicated region for block: B:153:0x04e3  */
    /* JADX WARN: Removed duplicated region for block: B:156:0x0525  */
    /* JADX WARN: Removed duplicated region for block: B:157:0x0528  */
    /* JADX WARN: Removed duplicated region for block: B:175:0x05c2  */
    /* JADX WARN: Removed duplicated region for block: B:191:? A[RETURN, SYNTHETIC] */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void c(com.discord.views.calls.VideoCallParticipantView.ParticipantData r31, androidx.core.view.DisplayCutoutCompat r32, boolean r33, com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter.CallUiInsets r34, boolean r35) {
        /*
            Method dump skipped, instructions count: 1590
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.calls.VideoCallParticipantView.c(com.discord.views.calls.VideoCallParticipantView$ParticipantData, androidx.core.view.DisplayCutoutCompat, boolean, com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter$CallUiInsets, boolean):void");
    }

    public final ParticipantData getData() {
        return this.l;
    }

    @Override // com.discord.utilities.view.grid.FrameGridLayout.DataView
    public String getDataId() {
        ParticipantData participantData = this.l;
        if (participantData != null) {
            return participantData.a;
        }
        return null;
    }

    @Override // com.discord.utilities.view.grid.FrameGridLayout.DataView
    public void onBind(FrameGridLayout.Data data) {
        m.checkNotNullParameter(data, "data");
        d(this, (ParticipantData) data, null, false, null, false, 30);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Subscription subscription = this.o;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.p = false;
        this.j.q.c(null, null, null, false);
    }

    @Override // com.discord.utilities.view.grid.FrameGridLayout.DataView
    public void onRemove() {
        FrameGridLayout.DataView.DefaultImpls.onRemove(this);
    }

    public final void setData(ParticipantData participantData) {
        this.l = participantData;
    }

    public final void setOnWatchStreamClicked(Function1<? super String, Unit> function1) {
        m.checkNotNullParameter(function1, "onWatchStreamClicked");
        this.n = function1;
    }

    /* compiled from: VideoCallParticipantView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001:\u0003E?FBg\u0012\u0006\u0010%\u001a\u00020!\u0012\b\b\u0002\u0010/\u001a\u00020\u0012\u0012\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u001b\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u001b\u0012\n\b\u0002\u00105\u001a\u0004\u0018\u000100\u0012\b\b\u0002\u0010;\u001a\u000206\u0012\b\b\u0002\u0010>\u001a\u00020\u0012\u0012\b\b\u0002\u0010(\u001a\u00020\u0012\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\bC\u0010DJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010 \u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0019\u0010%\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\"\u001a\u0004\b#\u0010$R\u0019\u0010(\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u001b\u0010,\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u001d\u001a\u0004\b+\u0010\u001fR\u0019\u0010/\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010'\u001a\u0004\b.\u0010)R\u001b\u00105\u001a\u0004\u0018\u0001008\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0019\u0010;\u001a\u0002068\u0006@\u0006¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b9\u0010:R\u0019\u0010>\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010'\u001a\u0004\b=\u0010)R\u001c\u0010B\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010\r¨\u0006G"}, d2 = {"Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;", "Lcom/discord/utilities/view/grid/FrameGridLayout$Data;", "Landroid/content/Context;", "context", "Landroid/view/View;", "createView", "(Landroid/content/Context;)Landroid/view/View;", "", "Lcom/discord/primitives/StreamId;", "b", "()Ljava/lang/Integer;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$a;", "j", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$a;", "getStreamQualityIndicatorData", "()Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$a;", "streamQualityIndicatorData", "Lorg/webrtc/RendererCommon$ScalingType;", "e", "Lorg/webrtc/RendererCommon$ScalingType;", "getScalingTypeMismatchOrientation", "()Lorg/webrtc/RendererCommon$ScalingType;", "scalingTypeMismatchOrientation", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "getVoiceParticipant", "()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "voiceParticipant", "i", "Z", "isFocused", "()Z", "d", "getScalingType", "scalingType", "c", "getMirrorVideo", "mirrorVideo", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;", "f", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;", "getApplicationStreamState", "()Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;", "applicationStreamState", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;", "g", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;", "getType", "()Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;", "type", "h", "getShowLabel", "showLabel", "a", "Ljava/lang/String;", "getId", ModelAuditLogEntry.CHANGE_KEY_ID, HookHelper.constructorName, "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZLorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;ZZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData$a;)V", "ApplicationStreamState", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ParticipantData implements FrameGridLayout.Data {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final StoreVoiceParticipants.VoiceUser f2812b;
        public final boolean c;
        public final RendererCommon.ScalingType d;
        public final RendererCommon.ScalingType e;
        public final ApplicationStreamState f;
        public final Type g;
        public final boolean h;
        public final boolean i;
        public final a j;

        /* compiled from: VideoCallParticipantView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "CONNECTING", "ACTIVE", "INACTIVE", "PAUSED", "ENDED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum ApplicationStreamState {
            CONNECTING,
            ACTIVE,
            INACTIVE,
            PAUSED,
            ENDED
        }

        /* compiled from: VideoCallParticipantView.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "DEFAULT", "APPLICATION_STREAMING", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum Type {
            DEFAULT,
            APPLICATION_STREAMING
        }

        /* compiled from: VideoCallParticipantView.kt */
        /* loaded from: classes2.dex */
        public static final class a {
            public final boolean a;

            /* renamed from: b  reason: collision with root package name */
            public final StreamResolution f2813b;
            public final StreamFps c;
            public final boolean d;

            public a(boolean z2, StreamResolution streamResolution, StreamFps streamFps, boolean z3) {
                m.checkNotNullParameter(streamResolution, "resolution");
                this.a = z2;
                this.f2813b = streamResolution;
                this.c = streamFps;
                this.d = z3;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof a)) {
                    return false;
                }
                a aVar = (a) obj;
                return this.a == aVar.a && m.areEqual(this.f2813b, aVar.f2813b) && m.areEqual(this.c, aVar.c) && this.d == aVar.d;
            }

            public int hashCode() {
                boolean z2 = this.a;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                StreamResolution streamResolution = this.f2813b;
                int i5 = 0;
                int hashCode = (i4 + (streamResolution != null ? streamResolution.hashCode() : 0)) * 31;
                StreamFps streamFps = this.c;
                if (streamFps != null) {
                    i5 = streamFps.hashCode();
                }
                int i6 = (hashCode + i5) * 31;
                boolean z3 = this.d;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                return i6 + i;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("StreamQualityIndicatorData(showPremiumIcon=");
                R.append(this.a);
                R.append(", resolution=");
                R.append(this.f2813b);
                R.append(", fps=");
                R.append(this.c);
                R.append(", isBadQuality=");
                return b.d.b.a.a.M(R, this.d, ")");
            }
        }

        public ParticipantData(StoreVoiceParticipants.VoiceUser voiceUser, boolean z2, RendererCommon.ScalingType scalingType, RendererCommon.ScalingType scalingType2, ApplicationStreamState applicationStreamState, Type type, boolean z3, boolean z4, a aVar) {
            m.checkNotNullParameter(voiceUser, "voiceParticipant");
            m.checkNotNullParameter(type, "type");
            this.f2812b = voiceUser;
            this.c = z2;
            this.d = scalingType;
            this.e = scalingType2;
            this.f = applicationStreamState;
            this.g = type;
            this.h = z3;
            this.i = z4;
            this.j = aVar;
            StringBuilder sb = new StringBuilder();
            sb.append(voiceUser.getUser().getId());
            sb.append(type);
            this.a = sb.toString();
        }

        public static ParticipantData a(ParticipantData participantData, StoreVoiceParticipants.VoiceUser voiceUser, boolean z2, RendererCommon.ScalingType scalingType, RendererCommon.ScalingType scalingType2, ApplicationStreamState applicationStreamState, Type type, boolean z3, boolean z4, a aVar, int i) {
            Type type2 = null;
            StoreVoiceParticipants.VoiceUser voiceUser2 = (i & 1) != 0 ? participantData.f2812b : null;
            boolean z5 = (i & 2) != 0 ? participantData.c : z2;
            RendererCommon.ScalingType scalingType3 = (i & 4) != 0 ? participantData.d : scalingType;
            RendererCommon.ScalingType scalingType4 = (i & 8) != 0 ? participantData.e : scalingType2;
            ApplicationStreamState applicationStreamState2 = (i & 16) != 0 ? participantData.f : null;
            if ((i & 32) != 0) {
                type2 = participantData.g;
            }
            boolean z6 = (i & 64) != 0 ? participantData.h : z3;
            boolean z7 = (i & 128) != 0 ? participantData.i : z4;
            a aVar2 = (i & 256) != 0 ? participantData.j : aVar;
            Objects.requireNonNull(participantData);
            m.checkNotNullParameter(voiceUser2, "voiceParticipant");
            m.checkNotNullParameter(type2, "type");
            return new ParticipantData(voiceUser2, z5, scalingType3, scalingType4, applicationStreamState2, type2, z6, z7, aVar2);
        }

        public final Integer b() {
            VoiceState voiceState;
            int ordinal = this.g.ordinal();
            if (ordinal == 0) {
                VoiceState voiceState2 = this.f2812b.getVoiceState();
                if (voiceState2 == null || !voiceState2.j()) {
                    return null;
                }
                return this.f2812b.getCallStreamId();
            } else if (ordinal == 1) {
                ApplicationStreamState applicationStreamState = this.f;
                if ((applicationStreamState == ApplicationStreamState.CONNECTING || applicationStreamState == ApplicationStreamState.ACTIVE || applicationStreamState == ApplicationStreamState.PAUSED) && (voiceState = this.f2812b.getVoiceState()) != null && voiceState.i()) {
                    return this.f2812b.getApplicationStreamId();
                }
                return null;
            } else {
                throw new NoWhenBranchMatchedException();
            }
        }

        @Override // com.discord.utilities.view.grid.FrameGridLayout.Data
        public View createView(Context context) {
            m.checkNotNullParameter(context, "context");
            return new VideoCallParticipantView(context, null, 0, 6);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ParticipantData)) {
                return false;
            }
            ParticipantData participantData = (ParticipantData) obj;
            return m.areEqual(this.f2812b, participantData.f2812b) && this.c == participantData.c && m.areEqual(this.d, participantData.d) && m.areEqual(this.e, participantData.e) && m.areEqual(this.f, participantData.f) && m.areEqual(this.g, participantData.g) && this.h == participantData.h && this.i == participantData.i && m.areEqual(this.j, participantData.j);
        }

        @Override // com.discord.utilities.view.grid.FrameGridLayout.Data
        public String getId() {
            return this.a;
        }

        public int hashCode() {
            StoreVoiceParticipants.VoiceUser voiceUser = this.f2812b;
            int i = 0;
            int hashCode = (voiceUser != null ? voiceUser.hashCode() : 0) * 31;
            boolean z2 = this.c;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            RendererCommon.ScalingType scalingType = this.d;
            int hashCode2 = (i5 + (scalingType != null ? scalingType.hashCode() : 0)) * 31;
            RendererCommon.ScalingType scalingType2 = this.e;
            int hashCode3 = (hashCode2 + (scalingType2 != null ? scalingType2.hashCode() : 0)) * 31;
            ApplicationStreamState applicationStreamState = this.f;
            int hashCode4 = (hashCode3 + (applicationStreamState != null ? applicationStreamState.hashCode() : 0)) * 31;
            Type type = this.g;
            int hashCode5 = (hashCode4 + (type != null ? type.hashCode() : 0)) * 31;
            boolean z3 = this.h;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (hashCode5 + i6) * 31;
            boolean z4 = this.i;
            if (!z4) {
                i2 = z4 ? 1 : 0;
            }
            int i9 = (i8 + i2) * 31;
            a aVar = this.j;
            if (aVar != null) {
                i = aVar.hashCode();
            }
            return i9 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ParticipantData(voiceParticipant=");
            R.append(this.f2812b);
            R.append(", mirrorVideo=");
            R.append(this.c);
            R.append(", scalingType=");
            R.append(this.d);
            R.append(", scalingTypeMismatchOrientation=");
            R.append(this.e);
            R.append(", applicationStreamState=");
            R.append(this.f);
            R.append(", type=");
            R.append(this.g);
            R.append(", showLabel=");
            R.append(this.h);
            R.append(", isFocused=");
            R.append(this.i);
            R.append(", streamQualityIndicatorData=");
            R.append(this.j);
            R.append(")");
            return R.toString();
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public /* synthetic */ ParticipantData(com.discord.stores.StoreVoiceParticipants.VoiceUser r14, boolean r15, org.webrtc.RendererCommon.ScalingType r16, org.webrtc.RendererCommon.ScalingType r17, com.discord.views.calls.VideoCallParticipantView.ParticipantData.ApplicationStreamState r18, com.discord.views.calls.VideoCallParticipantView.ParticipantData.Type r19, boolean r20, boolean r21, com.discord.views.calls.VideoCallParticipantView.ParticipantData.a r22, int r23) {
            /*
                r13 = this;
                r0 = r23
                r1 = r0 & 2
                r2 = 0
                if (r1 == 0) goto L9
                r5 = 0
                goto La
            L9:
                r5 = r15
            La:
                r1 = r0 & 4
                if (r1 == 0) goto L12
                org.webrtc.RendererCommon$ScalingType r1 = org.webrtc.RendererCommon.ScalingType.SCALE_ASPECT_BALANCED
                r6 = r1
                goto L14
            L12:
                r6 = r16
            L14:
                r1 = r0 & 8
                if (r1 == 0) goto L1c
                org.webrtc.RendererCommon$ScalingType r1 = org.webrtc.RendererCommon.ScalingType.SCALE_ASPECT_FIT
                r7 = r1
                goto L1e
            L1c:
                r7 = r17
            L1e:
                r1 = r0 & 16
                r3 = 0
                if (r1 == 0) goto L25
                r8 = r3
                goto L27
            L25:
                r8 = r18
            L27:
                r1 = r0 & 32
                if (r1 == 0) goto L2f
                com.discord.views.calls.VideoCallParticipantView$ParticipantData$Type r1 = com.discord.views.calls.VideoCallParticipantView.ParticipantData.Type.DEFAULT
                r9 = r1
                goto L31
            L2f:
                r9 = r19
            L31:
                r1 = r0 & 64
                if (r1 == 0) goto L37
                r10 = 0
                goto L39
            L37:
                r10 = r20
            L39:
                r1 = r0 & 128(0x80, float:1.794E-43)
                if (r1 == 0) goto L3f
                r11 = 0
                goto L41
            L3f:
                r11 = r21
            L41:
                r0 = r0 & 256(0x100, float:3.59E-43)
                r12 = 0
                r3 = r13
                r4 = r14
                r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.views.calls.VideoCallParticipantView.ParticipantData.<init>(com.discord.stores.StoreVoiceParticipants$VoiceUser, boolean, org.webrtc.RendererCommon$ScalingType, org.webrtc.RendererCommon$ScalingType, com.discord.views.calls.VideoCallParticipantView$ParticipantData$ApplicationStreamState, com.discord.views.calls.VideoCallParticipantView$ParticipantData$Type, boolean, boolean, com.discord.views.calls.VideoCallParticipantView$ParticipantData$a, int):void");
        }
    }
}
