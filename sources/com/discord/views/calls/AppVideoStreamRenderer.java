package com.discord.views.calls;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import androidx.annotation.MainThread;
import b.a.y.k0.a;
import b.a.y.k0.b;
import b.a.y.k0.c;
import b.a.y.k0.d;
import b.a.y.k0.e;
import co.discord.media_engine.VideoStreamRenderer;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import d0.z.d.e0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.webrtc.RendererCommon;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
/* compiled from: AppVideoStreamRenderer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0019\b\u0016\u0012\u0006\u00109\u001a\u000208\u0012\u0006\u0010;\u001a\u00020:¢\u0006\u0004\b<\u0010=J/\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\b\u0010\tJ;\u0010\u0011\u001a\u00020\u00072\u000e\u0010\u000b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\n2\b\u0010\r\u001a\u0004\u0018\u00010\f2\b\u0010\u000e\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u000f¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00020\u000f¢\u0006\u0004\b\u0017\u0010\u0015J\u000f\u0010\u0018\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001e\u0010\u001c\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001f\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR:\u0010$\u001a&\u0012\f\u0012\n !*\u0004\u0018\u00010\u00070\u0007 !*\u0012\u0012\f\u0012\n !*\u0004\u0018\u00010\u00070\u0007\u0018\u00010 0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R0\u0010-\u001a\u0010\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020\u0007\u0018\u00010%8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u0016\u0010\u0016\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/R\u0016\u00101\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u0010\u001eR\u0018\u00105\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R:\u00107\u001a&\u0012\f\u0012\n !*\u0004\u0018\u00010&0& !*\u0012\u0012\f\u0012\n !*\u0004\u0018\u00010&0&\u0018\u00010 0 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u0010#¨\u0006>"}, d2 = {"Lcom/discord/views/calls/AppVideoStreamRenderer;", "Lco/discord/media_engine/VideoStreamRenderer;", "", "w", "h", "oldw", "oldh", "", "onSizeChanged", "(IIII)V", "Lcom/discord/primitives/StreamId;", "newStreamId", "Lorg/webrtc/RendererCommon$ScalingType;", "scalingType", "scalingTypeMismatchOrientation", "", "isVideoMirrored", "c", "(Ljava/lang/Integer;Lorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Z)V", "matchVideoOrientation", "setMatchVideoOrientation", "(Z)V", "isOverlay", "setIsOverlay", "b", "()V", "m", "Ljava/lang/Integer;", "streamId", "q", "Lorg/webrtc/RendererCommon$ScalingType;", "prevScalingTypeMismatchOrientation", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "k", "Lrx/subjects/BehaviorSubject;", "onSizeChangedSubject", "Lkotlin/Function1;", "Landroid/graphics/Point;", "o", "Lkotlin/jvm/functions/Function1;", "getOnFrameRenderedListener", "()Lkotlin/jvm/functions/Function1;", "setOnFrameRenderedListener", "(Lkotlin/jvm/functions/Function1;)V", "onFrameRenderedListener", "j", "Z", "p", "prevScalingType", "Lrx/Subscription;", "l", "Lrx/Subscription;", "updateRendererSizeSubscription", "n", "currentFrameResolutionSubject", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AppVideoStreamRenderer extends VideoStreamRenderer {
    public boolean j;
    public Subscription l;
    public Integer m;
    public Function1<? super Point, Unit> o;
    public RendererCommon.ScalingType p;
    public RendererCommon.ScalingType q;
    public final BehaviorSubject<Unit> k = BehaviorSubject.l0(Unit.a);
    public BehaviorSubject<Point> n = BehaviorSubject.l0(null);

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AppVideoStreamRenderer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        RendererCommon.ScalingType scalingType = RendererCommon.ScalingType.SCALE_ASPECT_BALANCED;
        this.p = scalingType;
        this.q = scalingType;
    }

    @MainThread
    public final void b() {
        Subscription subscription = this.l;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        HashMap<Integer, VideoStreamRenderer> hashMap = d.a;
        Integer num = this.m;
        Objects.requireNonNull(hashMap, "null cannot be cast to non-null type kotlin.collections.MutableMap<K, V>");
        e0.asMutableMap(hashMap).remove(num);
        this.n.onNext(null);
        hashMap.isEmpty();
        VideoStreamRenderer.attachToStream$default(this, StoreStream.Companion.getMediaEngine().getVoiceEngineNative(), null, null, 4, null);
    }

    @MainThread
    public final void c(Integer num, RendererCommon.ScalingType scalingType, RendererCommon.ScalingType scalingType2, boolean z2) {
        if (num != null) {
            boolean z3 = !m.areEqual(num, this.m);
            if (z3) {
                b();
            }
            if (z3) {
                int intValue = num.intValue();
                HashMap<Integer, VideoStreamRenderer> hashMap = d.a;
                VideoStreamRenderer videoStreamRenderer = hashMap.get(Integer.valueOf(intValue));
                if (!(videoStreamRenderer == null || videoStreamRenderer == this)) {
                    VideoStreamRenderer.attachToStream$default(videoStreamRenderer, StoreStream.Companion.getMediaEngine().getVoiceEngineNative(), null, null, 4, null);
                    hashMap.remove(Integer.valueOf(intValue));
                    hashMap.isEmpty();
                }
                hashMap.put(Integer.valueOf(intValue), this);
                e eVar = new e();
                Subscription subscription = this.l;
                if (subscription != null) {
                    subscription.unsubscribe();
                }
                Observable<R> z4 = this.k.z(new a(eVar));
                m.checkNotNullExpressionValue(z4, "onSizeChangedSubject\n   …rameResolutionSampled() }");
                Observable F = z4.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
                m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
                Observable ui = ObservableExtensionsKt.ui(F);
                b bVar = new b(this);
                String simpleName = AppVideoStreamRenderer.class.getSimpleName();
                m.checkNotNullExpressionValue(simpleName, "javaClass.simpleName");
                ObservableExtensionsKt.appSubscribe(ui, (r18 & 1) != 0 ? null : null, simpleName, (r18 & 4) != 0 ? null : new c(this), bVar, (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
                StringBuilder R = b.d.b.a.a.R("binding native renderer ");
                R.append(hashCode());
                R.append(" to stream id: ");
                R.append(intValue);
                Log.d("AppVideoStreamRenderer", R.toString());
                attachToStream(StoreStream.Companion.getMediaEngine().getVoiceEngineNative(), String.valueOf(intValue), eVar);
                this.m = num;
            }
            RendererCommon.ScalingType scalingType3 = scalingType != null ? scalingType : this.p;
            RendererCommon.ScalingType scalingType4 = scalingType2 != null ? scalingType2 : this.q;
            setMirror(z2);
            setZOrderMediaOverlay(this.j);
            if (scalingType3 != this.p || scalingType4 != this.q) {
                setScalingType(scalingType3, scalingType4);
                this.p = scalingType3;
                this.q = scalingType4;
                return;
            }
            return;
        }
        b();
        this.m = null;
    }

    public final Function1<Point, Unit> getOnFrameRenderedListener() {
        return this.o;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.k.onNext(Unit.a);
        super.clearImage();
    }

    public final void setIsOverlay(boolean z2) {
        this.j = z2;
    }

    public final void setMatchVideoOrientation(boolean z2) {
    }

    public final void setOnFrameRenderedListener(Function1<? super Point, Unit> function1) {
        this.o = function1;
    }
}
