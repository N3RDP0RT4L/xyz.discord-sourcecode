package com.discord.views.sticker;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import b.a.i.p1;
import b.a.k.b;
import b.a.y.r0.a;
import b.a.y.r0.d;
import b.a.y.r0.e;
import b.a.y.r0.f;
import b.a.y.r0.h;
import b.f.g.e.v;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerFormatType;
import com.discord.api.sticker.StickerPartial;
import com.discord.app.AppLog;
import com.discord.rlottie.RLottieImageView;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.apng.ApngUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import java.io.File;
import java.util.Objects;
import kotlin.Metadata;
import kotlinx.coroutines.Job;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: StickerView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010$\u001a\u00020#\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010%¢\u0006\u0004\b'\u0010(J\u000f\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0003\u0010\u0004J!\u0010\t\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\t\u0010\nJ\r\u0010\u000b\u001a\u00020\u0002¢\u0006\u0004\b\u000b\u0010\u0004J\u000f\u0010\r\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u0011\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0018\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0018\u0010\u001c\u001a\u0004\u0018\u00010\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010 \u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006)"}, d2 = {"Lcom/discord/views/sticker/StickerView;", "Landroid/widget/FrameLayout;", "", "onDetachedFromWindow", "()V", "Lcom/discord/api/sticker/BaseSticker;", "sticker", "", "stickerAnimationSettings", "d", "(Lcom/discord/api/sticker/BaseSticker;Ljava/lang/Integer;)V", "b", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "Landroid/view/View$OnClickListener;", "onClickListener", "setOnClickListener", "(Landroid/view/View$OnClickListener;)V", "", "c", "(Lcom/discord/api/sticker/BaseSticker;)Ljava/lang/CharSequence;", "l", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "Lkotlinx/coroutines/Job;", "m", "Lkotlinx/coroutines/Job;", "apngLoadingJob", "Lb/a/i/p1;", "j", "Lb/a/i/p1;", "binding", "k", "Lcom/discord/api/sticker/BaseSticker;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerView extends FrameLayout {
    public final p1 j;
    public BaseSticker k;
    public Subscription l;
    public Job m;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StickerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(getContext()).inflate(R.layout.sticker_view, this);
        int i = R.id.sticker_view_imageview;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.sticker_view_imageview);
        if (simpleDraweeView != null) {
            i = R.id.sticker_view_lottie;
            RLottieImageView rLottieImageView = (RLottieImageView) findViewById(R.id.sticker_view_lottie);
            if (rLottieImageView != null) {
                i = R.id.sticker_view_placeholder;
                ImageView imageView = (ImageView) findViewById(R.id.sticker_view_placeholder);
                if (imageView != null) {
                    p1 p1Var = new p1(this, simpleDraweeView, rLottieImageView, imageView);
                    m.checkNotNullExpressionValue(p1Var, "StickerViewBinding.infla…ater.from(context), this)");
                    this.j = p1Var;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    public static final Job a(StickerView stickerView, File file, boolean z2) {
        Objects.requireNonNull(stickerView);
        ApngUtils apngUtils = ApngUtils.INSTANCE;
        SimpleDraweeView simpleDraweeView = stickerView.j.f173b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.stickerViewImageview");
        StickerUtils stickerUtils = StickerUtils.INSTANCE;
        return apngUtils.renderApngFromFile(file, simpleDraweeView, Integer.valueOf(stickerUtils.getDEFAULT_STICKER_SIZE_PX()), Integer.valueOf(stickerUtils.getDEFAULT_STICKER_SIZE_PX()), z2);
    }

    public static /* synthetic */ void e(StickerView stickerView, BaseSticker baseSticker, Integer num, int i) {
        int i2 = i & 2;
        stickerView.d(baseSticker, null);
    }

    public final void b() {
        SimpleDraweeView simpleDraweeView = this.j.f173b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.stickerViewImageview");
        simpleDraweeView.setBackground(null);
        RLottieImageView rLottieImageView = this.j.c;
        m.checkNotNullExpressionValue(rLottieImageView, "binding.stickerViewLottie");
        rLottieImageView.setBackground(null);
    }

    public final CharSequence c(BaseSticker baseSticker) {
        CharSequence b2;
        CharSequence b3;
        if (baseSticker instanceof Sticker) {
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            StringBuilder sb = new StringBuilder();
            Sticker sticker = (Sticker) baseSticker;
            sb.append(sticker.h());
            sb.append(", ");
            sb.append(sticker.f());
            b3 = b.b(context, R.string.sticker_a11y_label, new Object[]{sb.toString()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else if (!(baseSticker instanceof StickerPartial)) {
            return null;
        } else {
            Context context2 = getContext();
            m.checkNotNullExpressionValue(context2, "context");
            b2 = b.b(context2, R.string.sticker_a11y_label, new Object[]{((StickerPartial) baseSticker).e()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
    }

    public final void d(BaseSticker baseSticker, Integer num) {
        m.checkNotNullParameter(baseSticker, "sticker");
        BaseSticker baseSticker2 = this.k;
        if (baseSticker2 != null && baseSticker2.d() == baseSticker.d()) {
            if (this.l != null) {
                return;
            }
        }
        BaseSticker baseSticker3 = this.k;
        if (baseSticker3 != null && (baseSticker3 == null || baseSticker3.d() != baseSticker.d())) {
            Subscription subscription = this.l;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            this.l = null;
        }
        this.k = baseSticker;
        int ordinal = baseSticker.a().ordinal();
        if (ordinal == 1) {
            SimpleDraweeView simpleDraweeView = this.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.stickerViewImageview");
            simpleDraweeView.setVisibility(0);
            ImageView imageView = this.j.d;
            m.checkNotNullExpressionValue(imageView, "binding.stickerViewPlaceholder");
            imageView.setVisibility(8);
            RLottieImageView rLottieImageView = this.j.c;
            m.checkNotNullExpressionValue(rLottieImageView, "binding.stickerViewLottie");
            rLottieImageView.setVisibility(8);
            SimpleDraweeView simpleDraweeView2 = this.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.stickerViewImageview");
            simpleDraweeView2.setContentDescription(c(baseSticker));
            SimpleDraweeView simpleDraweeView3 = this.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.stickerViewImageview");
            MGImages.setImage$default(simpleDraweeView3, StickerUtils.getCDNAssetUrl$default(StickerUtils.INSTANCE, baseSticker, null, false, 6, null), 0, 0, false, null, null, 124, null);
            SimpleDraweeView simpleDraweeView4 = this.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView4, "binding.stickerViewImageview");
            GenericDraweeHierarchy hierarchy = simpleDraweeView4.getHierarchy();
            m.checkNotNullExpressionValue(hierarchy, "binding.stickerViewImageview.hierarchy");
            ScalingUtils$ScaleType scalingUtils$ScaleType = ScalingUtils$ScaleType.a;
            hierarchy.n(v.l);
        } else if (ordinal == 2) {
            SimpleDraweeView simpleDraweeView5 = this.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView5, "binding.stickerViewImageview");
            simpleDraweeView5.setVisibility(0);
            ImageView imageView2 = this.j.d;
            m.checkNotNullExpressionValue(imageView2, "binding.stickerViewPlaceholder");
            imageView2.setVisibility(0);
            RLottieImageView rLottieImageView2 = this.j.c;
            m.checkNotNullExpressionValue(rLottieImageView2, "binding.stickerViewLottie");
            rLottieImageView2.setVisibility(8);
            this.j.f173b.setImageDrawable(null);
            SimpleDraweeView simpleDraweeView6 = this.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView6, "binding.stickerViewImageview");
            GenericDraweeHierarchy hierarchy2 = simpleDraweeView6.getHierarchy();
            m.checkNotNullExpressionValue(hierarchy2, "binding.stickerViewImageview.hierarchy");
            ScalingUtils$ScaleType scalingUtils$ScaleType2 = ScalingUtils$ScaleType.a;
            hierarchy2.n(v.l);
            StickerUtils stickerUtils = StickerUtils.INSTANCE;
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            Observable j = Observable.j(ObservableExtensionsKt.restSubscribeOn$default(stickerUtils.fetchSticker(context, baseSticker), false, 1, null), StoreUserSettings.observeStickerAnimationSettings$default(StoreStream.Companion.getUserSettings(), false, 1, null), a.j);
            m.checkNotNullExpressionValue(j, "Observable.combineLatest…lobalAnimationSettings) }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(j), StickerView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new b.a.y.r0.b(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d(this, num, baseSticker));
        } else if (ordinal != 3) {
            AppLog appLog = AppLog.g;
            Logger.e$default(appLog, "Invalid Sticker Format passed to " + StickerView.class + ", type: " + baseSticker.a(), null, null, 6, null);
        } else {
            SimpleDraweeView simpleDraweeView7 = this.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView7, "binding.stickerViewImageview");
            simpleDraweeView7.setVisibility(8);
            ImageView imageView3 = this.j.d;
            m.checkNotNullExpressionValue(imageView3, "binding.stickerViewPlaceholder");
            imageView3.setVisibility(0);
            RLottieImageView rLottieImageView3 = this.j.c;
            m.checkNotNullExpressionValue(rLottieImageView3, "binding.stickerViewLottie");
            rLottieImageView3.setVisibility(0);
            this.j.c.setImageDrawable(null);
            this.j.c.clearAnimation();
            StickerUtils stickerUtils2 = StickerUtils.INSTANCE;
            Context context2 = getContext();
            m.checkNotNullExpressionValue(context2, "context");
            Observable j2 = Observable.j(ObservableExtensionsKt.restSubscribeOn$default(stickerUtils2.fetchSticker(context2, baseSticker), false, 1, null), StoreUserSettings.observeStickerAnimationSettings$default(StoreStream.Companion.getUserSettings(), false, 1, null), e.j);
            m.checkNotNullExpressionValue(j2, "Observable.combineLatest…lobalAnimationSettings) }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(j2), StickerView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new f(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new h(this, num, baseSticker));
        }
    }

    public final Subscription getSubscription() {
        return this.l;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Subscription subscription = this.l;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.l = null;
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        BaseSticker baseSticker = this.k;
        StickerFormatType a = baseSticker != null ? baseSticker.a() : null;
        if (a != null) {
            int ordinal = a.ordinal();
            if (ordinal == 1 || ordinal == 2) {
                this.j.f173b.setOnClickListener(onClickListener);
            } else if (ordinal == 3) {
                this.j.c.setOnClickListener(onClickListener);
            }
        }
    }
}
