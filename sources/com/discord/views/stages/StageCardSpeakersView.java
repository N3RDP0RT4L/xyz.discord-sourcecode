package com.discord.views.stages;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.r3;
import b.a.y.p0.c;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: StageCardSpeakersView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0007\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\r\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0014"}, d2 = {"Lcom/discord/views/stages/StageCardSpeakersView;", "Landroid/widget/LinearLayout;", "Lb/a/i/r3;", "k", "Lb/a/i/r3;", "binding", "Lb/a/y/p0/c;", "l", "Lb/a/y/p0/c;", "speakersAdapter", "Landroidx/recyclerview/widget/GridLayoutManager;", "m", "Landroidx/recyclerview/widget/GridLayoutManager;", "speakersLayoutManager", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageCardSpeakersView extends LinearLayout {
    public static final RecyclerView.ItemDecoration j = new a();
    public final r3 k;
    public final c l;
    public final GridLayoutManager m;

    /* compiled from: StageCardSpeakersView.kt */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerView.ItemDecoration {
        @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
            m.checkNotNullParameter(rect, "outRect");
            m.checkNotNullParameter(view, "view");
            m.checkNotNullParameter(recyclerView, "parent");
            m.checkNotNullParameter(state, "state");
            super.getItemOffsets(rect, view, recyclerView, state);
            int itemCount = state.getItemCount();
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            int i = 0;
            boolean z2 = childAdapterPosition >= itemCount + (-2);
            rect.left = DimenUtils.dpToPixels(4);
            rect.right = DimenUtils.dpToPixels(4);
            if (!z2) {
                i = DimenUtils.dpToPixels(8);
            }
            rect.bottom = i;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StageCardSpeakersView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.view_stage_card_speakers, this);
        SpeakersRecyclerView speakersRecyclerView = (SpeakersRecyclerView) findViewById(R.id.discovery_speakers_recycler);
        if (speakersRecyclerView != null) {
            r3 r3Var = new r3(this, speakersRecyclerView);
            m.checkNotNullExpressionValue(r3Var, "ViewStageCardSpeakersBin…ater.from(context), this)");
            this.k = r3Var;
            MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
            m.checkNotNullExpressionValue(speakersRecyclerView, "binding.discoverySpeakersRecycler");
            c cVar = (c) companion.configure(new c(speakersRecyclerView));
            this.l = cVar;
            m.checkNotNullExpressionValue(speakersRecyclerView, "binding.discoverySpeakersRecycler");
            GridLayoutManager gridLayoutManager = new GridLayoutManager(speakersRecyclerView.getContext(), 2);
            gridLayoutManager.setSpanSizeLookup(cVar.a);
            this.m = gridLayoutManager;
            RecyclerView recycler = cVar.getRecycler();
            recycler.setLayoutManager(gridLayoutManager);
            recycler.addItemDecoration(j);
            recycler.setHasFixedSize(false);
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(R.id.discovery_speakers_recycler)));
    }
}
