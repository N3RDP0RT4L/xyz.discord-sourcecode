package com.discord.views.steps;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import b.a.i.u3;
import com.discord.app.AppFragment;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: StepsView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0003\r\u001e1B\u001d\b\u0016\u0012\u0006\u0010,\u001a\u00020+\u0012\n\b\u0002\u0010.\u001a\u0004\u0018\u00010-¢\u0006\u0004\b/\u00100J9\u0010\n\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0014\b\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00050\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\r\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\b¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0013\u0010\u0012J\u0015\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u000f¢\u0006\u0004\b\u0015\u0010\u0012J\u0015\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u000f¢\u0006\u0004\b\u0017\u0010\u0012J\u0015\u0010\u001a\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u000f¢\u0006\u0004\b\u001c\u0010\u0012J%\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\b2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002¢\u0006\u0004\b\u001e\u0010\u001fR\u0018\u0010\"\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!R\u0018\u0010&\u001a\u0004\u0018\u00010#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010*\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)¨\u00062"}, d2 = {"Lcom/discord/views/steps/StepsView;", "Landroid/widget/RelativeLayout;", "Lcom/discord/views/steps/StepsView$d;", "adapter", "Lkotlin/Function0;", "", "onClose", "Lkotlin/Function1;", "", "onPageSelected", "a", "(Lcom/discord/views/steps/StepsView$d;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "index", "b", "(I)V", "", "isEnabled", "setIsNextButtonEnabled", "(Z)V", "setIsDoneButtonEnabled", "isLoading", "setIsLoading", "isVisible", "setIsTitleVisible", "", "title", "setTitle", "(Ljava/lang/String;)V", "setStepProgressIndicatorVisible", ModelAuditLogEntry.CHANGE_KEY_POSITION, "c", "(ILkotlin/jvm/functions/Function0;)V", "l", "Lcom/discord/views/steps/StepsView$d;", "pagerAdapter", "Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;", "m", "Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;", "pagerAdapterChangeCallback", "Lb/a/i/u3;", "k", "Lb/a/i/u3;", "binding", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "d", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StepsView extends RelativeLayout {
    public static final /* synthetic */ int j = 0;
    public final u3 k;
    public d l;
    public ViewPager2.OnPageChangeCallback m;

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ int k;
        public final /* synthetic */ Object l;
        public final /* synthetic */ Object m;

        public a(int i, int i2, Object obj, Object obj2) {
            this.j = i;
            this.k = i2;
            this.l = obj;
            this.m = obj2;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                Function1<View, Unit> function1 = ((b) ((List) this.m).get(this.k)).d;
                if (function1 != null) {
                    m.checkNotNullExpressionValue(view, "it");
                    function1.invoke(view);
                    return;
                }
                ViewPager2 viewPager2 = ((StepsView) this.l).k.i;
                m.checkNotNullExpressionValue(viewPager2, "binding.stepsViewpager");
                int currentItem = viewPager2.getCurrentItem();
                d dVar = ((StepsView) this.l).l;
                if (dVar == null || currentItem != dVar.getItemCount() - 1) {
                    ViewPager2 viewPager22 = ((StepsView) this.l).k.i;
                    m.checkNotNullExpressionValue(viewPager22, "binding.stepsViewpager");
                    ViewPager2 viewPager23 = ((StepsView) this.l).k.i;
                    m.checkNotNullExpressionValue(viewPager23, "binding.stepsViewpager");
                    viewPager22.setCurrentItem(viewPager23.getCurrentItem() + 1);
                }
            } else if (i == 1) {
                Function1<View, Unit> function12 = ((b) ((List) this.l).get(this.k)).f;
                if (function12 != null) {
                    m.checkNotNullExpressionValue(view, "it");
                    function12.invoke(view);
                    return;
                }
                ((Function0) this.m).invoke();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: StepsView.kt */
    /* loaded from: classes2.dex */
    public static abstract class b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f2815b;
        public final int c;
        public final Function1<View, Unit> d;
        public final Function1<View, Unit> e;
        public final Function1<View, Unit> f;
        public final boolean g;
        public final boolean h;
        public final boolean i;

        /* compiled from: StepsView.kt */
        /* loaded from: classes2.dex */
        public static final class a extends b {
            public final Class<? extends Fragment> j;
            public final int k;
            public final int l;
            public final int m;
            public final Bundle n;
            public final Function1<View, Unit> o;
            public final Function1<View, Unit> p;
            public final Function1<View, Unit> q;
            public final boolean r;

            /* renamed from: s  reason: collision with root package name */
            public final boolean f2816s;
            public final boolean t;

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public a(java.lang.Class r14, int r15, int r16, int r17, android.os.Bundle r18, kotlin.jvm.functions.Function1 r19, kotlin.jvm.functions.Function1 r20, kotlin.jvm.functions.Function1 r21, boolean r22, boolean r23, boolean r24, int r25) {
                /*
                    r13 = this;
                    r0 = r13
                    r1 = r14
                    r2 = r25
                    r3 = r2 & 2
                    if (r3 == 0) goto Lc
                    r3 = 2131892158(0x7f1217be, float:1.9419056E38)
                    goto Ld
                Lc:
                    r3 = r15
                Ld:
                    r4 = r2 & 4
                    if (r4 == 0) goto L15
                    r4 = 2131886885(0x7f120325, float:1.9408361E38)
                    goto L17
                L15:
                    r4 = r16
                L17:
                    r5 = r2 & 8
                    if (r5 == 0) goto L1f
                    r5 = 2131888335(0x7f1208cf, float:1.9411302E38)
                    goto L21
                L1f:
                    r5 = r17
                L21:
                    r6 = r2 & 16
                    r7 = 0
                    if (r6 == 0) goto L28
                    r6 = r7
                    goto L2a
                L28:
                    r6 = r18
                L2a:
                    r8 = r2 & 32
                    if (r8 == 0) goto L30
                    r8 = r7
                    goto L32
                L30:
                    r8 = r19
                L32:
                    r9 = r2 & 64
                    r9 = 0
                    r10 = r2 & 128(0x80, float:1.794E-43)
                    if (r10 == 0) goto L3a
                    goto L3c
                L3a:
                    r7 = r21
                L3c:
                    r10 = r2 & 256(0x100, float:3.59E-43)
                    r11 = 1
                    if (r10 == 0) goto L43
                    r10 = 1
                    goto L45
                L43:
                    r10 = r22
                L45:
                    r12 = r2 & 512(0x200, float:7.175E-43)
                    if (r12 == 0) goto L4b
                    r12 = 1
                    goto L4d
                L4b:
                    r12 = r23
                L4d:
                    r2 = r2 & 1024(0x400, float:1.435E-42)
                    if (r2 == 0) goto L52
                    goto L54
                L52:
                    r11 = r24
                L54:
                    java.lang.String r2 = "fragment"
                    d0.z.d.m.checkNotNullParameter(r14, r2)
                    r2 = 0
                    r15 = r13
                    r16 = r3
                    r17 = r4
                    r18 = r5
                    r19 = r8
                    r20 = r9
                    r21 = r7
                    r22 = r10
                    r23 = r12
                    r24 = r11
                    r25 = r2
                    r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25)
                    r0.j = r1
                    r0.k = r3
                    r0.l = r4
                    r0.m = r5
                    r0.n = r6
                    r0.o = r8
                    r0.p = r9
                    r0.q = r7
                    r0.r = r10
                    r0.f2816s = r12
                    r0.t = r11
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.views.steps.StepsView.b.a.<init>(java.lang.Class, int, int, int, android.os.Bundle, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, boolean, boolean, boolean, int):void");
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof a)) {
                    return false;
                }
                a aVar = (a) obj;
                return m.areEqual(this.j, aVar.j) && this.k == aVar.k && this.l == aVar.l && this.m == aVar.m && m.areEqual(this.n, aVar.n) && m.areEqual(this.o, aVar.o) && m.areEqual(this.p, aVar.p) && m.areEqual(this.q, aVar.q) && this.r == aVar.r && this.f2816s == aVar.f2816s && this.t == aVar.t;
            }

            public int hashCode() {
                Class<? extends Fragment> cls = this.j;
                int i = 0;
                int hashCode = (((((((cls != null ? cls.hashCode() : 0) * 31) + this.k) * 31) + this.l) * 31) + this.m) * 31;
                Bundle bundle = this.n;
                int hashCode2 = (hashCode + (bundle != null ? bundle.hashCode() : 0)) * 31;
                Function1<View, Unit> function1 = this.o;
                int hashCode3 = (hashCode2 + (function1 != null ? function1.hashCode() : 0)) * 31;
                Function1<View, Unit> function12 = this.p;
                int hashCode4 = (hashCode3 + (function12 != null ? function12.hashCode() : 0)) * 31;
                Function1<View, Unit> function13 = this.q;
                if (function13 != null) {
                    i = function13.hashCode();
                }
                int i2 = (hashCode4 + i) * 31;
                boolean z2 = this.r;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.f2816s;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.t;
                if (!z4) {
                    i3 = z4 ? 1 : 0;
                }
                return i9 + i3;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("FragmentStep(fragment=");
                R.append(this.j);
                R.append(", nextText=");
                R.append(this.k);
                R.append(", cancelText=");
                R.append(this.l);
                R.append(", doneText=");
                R.append(this.m);
                R.append(", arguments=");
                R.append(this.n);
                R.append(", onNext=");
                R.append(this.o);
                R.append(", onCancel=");
                R.append(this.p);
                R.append(", onDone=");
                R.append(this.q);
                R.append(", canNext=");
                R.append(this.r);
                R.append(", canCancel=");
                R.append(this.f2816s);
                R.append(", allowScroll=");
                return b.d.b.a.a.M(R, this.t, ")");
            }
        }

        /* compiled from: StepsView.kt */
        /* renamed from: com.discord.views.steps.StepsView$b$b  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0232b extends b {
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof C0232b)) {
                    return false;
                }
                Objects.requireNonNull((C0232b) obj);
                return m.areEqual((Object) null, (Object) null) && m.areEqual((Object) null, (Object) null) && m.areEqual((Object) null, (Object) null);
            }

            public int hashCode() {
                return (((((((((((((((((0 * 31) + 0) * 31) + 0) * 31) + 0) * 31) + 0) * 31) + 0) * 31) + 0) * 31) + 0) * 31) + 0) * 31) + 0;
            }

            public String toString() {
                return "ViewStep(layoutId=0, nextText=0, cancelText=0, doneText=0, onNext=null, onCancel=null, onDone=null, canNext=false, canCancel=false, allowScroll=false)";
            }
        }

        public b(int i, int i2, int i3, Function1 function1, Function1 function12, Function1 function13, boolean z2, boolean z3, boolean z4, DefaultConstructorMarker defaultConstructorMarker) {
            this.a = i;
            this.f2815b = i2;
            this.c = i3;
            this.d = function1;
            this.e = function12;
            this.f = function13;
            this.g = z2;
            this.h = z3;
            this.i = z4;
        }
    }

    /* compiled from: StepsView.kt */
    /* loaded from: classes2.dex */
    public static final class c extends AppFragment {
        public c(@LayoutRes int i) {
            super(i);
        }
    }

    /* compiled from: StepsView.kt */
    /* loaded from: classes2.dex */
    public static final class d extends FragmentStateAdapter {
        public List<? extends b> a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(Fragment fragment, List<? extends b> list) {
            super(fragment);
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(list, "steps");
            this.a = list;
        }

        @Override // androidx.viewpager2.adapter.FragmentStateAdapter
        public Fragment createFragment(int i) {
            b bVar = this.a.get(i);
            if (bVar instanceof b.a) {
                b.a aVar = (b.a) bVar;
                Fragment newInstance = aVar.j.newInstance();
                Fragment fragment = newInstance;
                fragment.setArguments(aVar.n);
                m.checkNotNullExpressionValue(newInstance, "currentStep.fragment.new…tep.arguments\n          }");
                return fragment;
            } else if (!(bVar instanceof b.C0232b)) {
                return new Fragment();
            } else {
                Objects.requireNonNull((b.C0232b) bVar);
                return new c(0);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.a.size();
        }
    }

    /* compiled from: StepsView.kt */
    /* loaded from: classes2.dex */
    public static final class e extends ViewPager2.OnPageChangeCallback {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Function1 f2817b;
        public final /* synthetic */ Function0 c;

        public e(Function1 function1, Function0 function0) {
            this.f2817b = function1;
            this.c = function0;
        }

        @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
        public void onPageSelected(int i) {
            this.f2817b.invoke(Integer.valueOf(i));
            StepsView stepsView = StepsView.this;
            Function0<Unit> function0 = this.c;
            int i2 = StepsView.j;
            stepsView.c(i, function0);
            StepsView.this.k.g.setCurrentStepIndex(i);
        }
    }

    /* compiled from: StepsView.kt */
    /* loaded from: classes2.dex */
    public static final class f implements View.OnClickListener {
        public final /* synthetic */ Function0 j;

        public f(Function0 function0) {
            this.j = function0;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            this.j.invoke();
        }
    }

    /* compiled from: StepsView.kt */
    /* loaded from: classes2.dex */
    public static final class g implements View.OnClickListener {
        public final /* synthetic */ List k;
        public final /* synthetic */ int l;
        public final /* synthetic */ Function0 m;

        public g(List list, int i, Function0 function0) {
            this.k = list;
            this.l = i;
            this.m = function0;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Function1<View, Unit> function1 = ((b) this.k.get(this.l)).e;
            if (function1 != null) {
                m.checkNotNullExpressionValue(view, "it");
                function1.invoke(view);
                return;
            }
            ViewPager2 viewPager2 = StepsView.this.k.i;
            m.checkNotNullExpressionValue(viewPager2, "binding.stepsViewpager");
            if (viewPager2.getCurrentItem() == 0) {
                this.m.invoke();
                return;
            }
            ViewPager2 viewPager22 = StepsView.this.k.i;
            m.checkNotNullExpressionValue(viewPager22, "binding.stepsViewpager");
            ViewPager2 viewPager23 = StepsView.this.k.i;
            m.checkNotNullExpressionValue(viewPager23, "binding.stepsViewpager");
            viewPager22.setCurrentItem(viewPager23.getCurrentItem() - 1);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StepsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_steps, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.steps_button_container;
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.steps_button_container);
        if (linearLayout != null) {
            i = R.id.steps_cancel;
            TextView textView = (TextView) inflate.findViewById(R.id.steps_cancel);
            if (textView != null) {
                i = R.id.steps_close;
                ImageView imageView = (ImageView) inflate.findViewById(R.id.steps_close);
                if (imageView != null) {
                    i = R.id.steps_done;
                    LoadingButton loadingButton = (LoadingButton) inflate.findViewById(R.id.steps_done);
                    if (loadingButton != null) {
                        i = R.id.steps_next;
                        MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.steps_next);
                        if (materialButton != null) {
                            i = R.id.steps_progress_indicator;
                            StepsProgressIndicatorView stepsProgressIndicatorView = (StepsProgressIndicatorView) inflate.findViewById(R.id.steps_progress_indicator);
                            if (stepsProgressIndicatorView != null) {
                                i = R.id.steps_title;
                                TextView textView2 = (TextView) inflate.findViewById(R.id.steps_title);
                                if (textView2 != null) {
                                    i = R.id.steps_viewpager;
                                    ViewPager2 viewPager2 = (ViewPager2) inflate.findViewById(R.id.steps_viewpager);
                                    if (viewPager2 != null) {
                                        u3 u3Var = new u3((RelativeLayout) inflate, linearLayout, textView, imageView, loadingButton, materialButton, stepsProgressIndicatorView, textView2, viewPager2);
                                        m.checkNotNullExpressionValue(u3Var, "ViewStepsBinding.inflate…rom(context), this, true)");
                                        this.k = u3Var;
                                        setIsLoading(false);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void a(d dVar, Function0<Unit> function0, Function1<? super Integer, Unit> function1) {
        m.checkNotNullParameter(dVar, "adapter");
        m.checkNotNullParameter(function0, "onClose");
        m.checkNotNullParameter(function1, "onPageSelected");
        this.l = dVar;
        ViewPager2 viewPager2 = this.k.i;
        m.checkNotNullExpressionValue(viewPager2, "binding.stepsViewpager");
        viewPager2.setAdapter(this.l);
        ViewPager2.OnPageChangeCallback onPageChangeCallback = this.m;
        if (onPageChangeCallback != null) {
            this.k.i.unregisterOnPageChangeCallback(onPageChangeCallback);
        }
        ViewPager2 viewPager22 = this.k.i;
        e eVar = new e(function1, function0);
        this.m = eVar;
        viewPager22.registerOnPageChangeCallback(eVar);
        c(0, function0);
        this.k.g.setTotalStepCount(dVar.a.size());
    }

    public final void b(int i) {
        if (i >= 0) {
            d dVar = this.l;
            if (i < (dVar != null ? dVar.getItemCount() : 0)) {
                ViewPager2 viewPager2 = this.k.i;
                m.checkNotNullExpressionValue(viewPager2, "binding.stepsViewpager");
                viewPager2.setCurrentItem(i);
            }
        }
    }

    public final void c(int i, Function0<Unit> function0) {
        List<? extends b> list;
        this.k.d.setOnClickListener(new f(function0));
        d dVar = this.l;
        if (dVar != null && (list = dVar.a) != null) {
            ViewPager2 viewPager2 = this.k.i;
            m.checkNotNullExpressionValue(viewPager2, "binding.stepsViewpager");
            viewPager2.setUserInputEnabled(list.get(i).i);
            TextView textView = this.k.c;
            m.checkNotNullExpressionValue(textView, "binding.stepsCancel");
            textView.setText(getContext().getString(list.get(i).f2815b));
            this.k.e.setText(getContext().getString(list.get(i).c));
            MaterialButton materialButton = this.k.f;
            m.checkNotNullExpressionValue(materialButton, "binding.stepsNext");
            materialButton.setText(getContext().getString(list.get(i).a));
            int i2 = 0;
            this.k.f.setOnClickListener(new a(0, i, this, list));
            this.k.c.setOnClickListener(new g(list, i, function0));
            boolean z2 = true;
            this.k.e.setOnClickListener(new a(1, i, list, function0));
            d dVar2 = this.l;
            boolean z3 = dVar2 != null && i == dVar2.getItemCount() - 1;
            MaterialButton materialButton2 = this.k.f;
            m.checkNotNullExpressionValue(materialButton2, "binding.stepsNext");
            materialButton2.setVisibility(!z3 && list.get(i).g ? 0 : 8);
            TextView textView2 = this.k.c;
            m.checkNotNullExpressionValue(textView2, "binding.stepsCancel");
            textView2.setVisibility(!z3 && list.get(i).h ? 0 : 8);
            LoadingButton loadingButton = this.k.e;
            m.checkNotNullExpressionValue(loadingButton, "binding.stepsDone");
            loadingButton.setVisibility(z3 ? 0 : 8);
            LinearLayout linearLayout = this.k.f206b;
            m.checkNotNullExpressionValue(linearLayout, "binding.stepsButtonContainer");
            MaterialButton materialButton3 = this.k.f;
            m.checkNotNullExpressionValue(materialButton3, "binding.stepsNext");
            if (!(materialButton3.getVisibility() == 0)) {
                TextView textView3 = this.k.c;
                m.checkNotNullExpressionValue(textView3, "binding.stepsCancel");
                if (!(textView3.getVisibility() == 0)) {
                    LoadingButton loadingButton2 = this.k.e;
                    m.checkNotNullExpressionValue(loadingButton2, "binding.stepsDone");
                    if (!(loadingButton2.getVisibility() == 0)) {
                        z2 = false;
                    }
                }
            }
            if (!z2) {
                i2 = 8;
            }
            linearLayout.setVisibility(i2);
        }
    }

    public final void setIsDoneButtonEnabled(boolean z2) {
        LoadingButton loadingButton = this.k.e;
        m.checkNotNullExpressionValue(loadingButton, "binding.stepsDone");
        loadingButton.setEnabled(z2);
    }

    public final void setIsLoading(boolean z2) {
        this.k.e.setIsLoading(z2);
    }

    public final void setIsNextButtonEnabled(boolean z2) {
        MaterialButton materialButton = this.k.f;
        m.checkNotNullExpressionValue(materialButton, "binding.stepsNext");
        materialButton.setEnabled(z2);
    }

    public final void setIsTitleVisible(boolean z2) {
        TextView textView = this.k.h;
        m.checkNotNullExpressionValue(textView, "binding.stepsTitle");
        textView.setVisibility(z2 ? 0 : 8);
        if (z2) {
            StepsProgressIndicatorView stepsProgressIndicatorView = this.k.g;
            m.checkNotNullExpressionValue(stepsProgressIndicatorView, "binding.stepsProgressIndicator");
            stepsProgressIndicatorView.setVisibility(8);
        }
    }

    public final void setStepProgressIndicatorVisible(boolean z2) {
        StepsProgressIndicatorView stepsProgressIndicatorView = this.k.g;
        m.checkNotNullExpressionValue(stepsProgressIndicatorView, "binding.stepsProgressIndicator");
        stepsProgressIndicatorView.setVisibility(z2 ? 0 : 8);
        if (z2) {
            TextView textView = this.k.h;
            m.checkNotNullExpressionValue(textView, "binding.stepsTitle");
            textView.setVisibility(8);
        }
    }

    public final void setTitle(String str) {
        m.checkNotNullParameter(str, "title");
        TextView textView = this.k.h;
        m.checkNotNullExpressionValue(textView, "binding.stepsTitle");
        textView.setText(str);
    }
}
