package com.discord.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.viewbinding.ViewBinding;
import b.a.i.c2;
import b.a.i.d2;
import b.a.i.e2;
import b.a.y.b;
import b.a.y.c;
import b.a.y.d;
import b.a.y.e;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.flexbox.FlexboxLayout;
import com.swift.sandhook.annotation.MethodReflectParams;
import d0.g0.w;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: CodeVerificationView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\f\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0001>J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\u0007\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0015\u0010\bJ\u000f\u0010\u0016\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0016\u0010\bR\u0016\u0010\u001a\u001a\u00020\u00178\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0018\u0010\u001e\u001a\u0004\u0018\u00010\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u001c\u0010#\u001a\b\u0012\u0004\u0012\u00020 0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010'\u001a\u00020$8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010+\u001a\u00020(8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b)\u0010*R.\u00103\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00040,8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b-\u0010.\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u0018\u00105\u001a\u0004\u0018\u00010\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u0010\u001dR\u0016\u00109\u001a\u0002068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108R$\u0010\u0003\u001a\u00020\u00172\u0006\u0010:\u001a\u00020\u00178\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b;\u0010\u0019\u001a\u0004\b<\u0010=¨\u0006?"}, d2 = {"Lcom/discord/views/CodeVerificationView;", "Landroid/widget/LinearLayout;", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "", "setCode", "(Ljava/lang/CharSequence;)V", "b", "()V", "Landroid/view/inputmethod/EditorInfo;", "outAttrs", "Landroid/view/inputmethod/InputConnection;", "onCreateInputConnection", "(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;", "", "onCheckIsTextEditor", "()Z", "", MethodReflectParams.CHAR, "c", "(C)V", "d", "e", "", "p", "Ljava/lang/String;", "inputFormat", "Landroid/graphics/drawable/Drawable;", "m", "Landroid/graphics/drawable/Drawable;", "charBackgroundDrawableUnfocused", "", "Landroid/widget/TextView;", "l", "Ljava/util/List;", "characterViews", "Lcom/discord/views/CodeVerificationView$a;", "o", "Lcom/discord/views/CodeVerificationView$a;", "inputType", "", "getInputLength", "()I", "inputLength", "Lkotlin/Function1;", "r", "Lkotlin/jvm/functions/Function1;", "getOnCodeEntered", "()Lkotlin/jvm/functions/Function1;", "setOnCodeEntered", "(Lkotlin/jvm/functions/Function1;)V", "onCodeEntered", "n", "charBackgroundDrawableFocused", "Lb/a/i/c2;", "k", "Lb/a/i/c2;", "binding", "<set-?>", "q", "getCode", "()Ljava/lang/String;", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CodeVerificationView extends LinearLayout {
    public static final a j = a.NUMERIC;
    public final c2 k;
    public final List<TextView> l;
    public final Drawable m;
    public final Drawable n;
    public a o;
    public String p;
    public String q = "";
    public Function1<? super String, Unit> r = e.j;

    /* compiled from: CodeVerificationView.kt */
    /* loaded from: classes2.dex */
    public enum a {
        NUMERIC,
        ALPHANUMERIC;
        
        public static final C0229a n = new C0229a(null);
        public static final a[] m = values();

        /* compiled from: CodeVerificationView.kt */
        /* renamed from: com.discord.views.CodeVerificationView$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0229a {
            public C0229a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CodeVerificationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        ViewBinding viewBinding;
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_code_verification, (ViewGroup) this, false);
        addView(inflate);
        Objects.requireNonNull(inflate, "rootView");
        FlexboxLayout flexboxLayout = (FlexboxLayout) inflate;
        c2 c2Var = new c2(flexboxLayout, flexboxLayout);
        m.checkNotNullExpressionValue(c2Var, "ViewCodeVerificationBind…rom(context), this, true)");
        this.k = c2Var;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, com.discord.R.a.CodeVerificationView, 0, 0);
            m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…deVerificationView, 0, 0)");
            try {
                int i = obtainStyledAttributes.getInt(1, j.ordinal());
                a.C0229a aVar = a.n;
                this.o = a.m[i];
                String string = obtainStyledAttributes.getString(0);
                if (string == null) {
                    string = "xxx-xxx";
                }
                this.p = string;
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        setClickable(true);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setOnFocusChangeListener(new b(this));
        setOnClickListener(new c(this));
        setOnKeyListener(new d(this));
        String str = this.p;
        if (str == null) {
            m.throwUninitializedPropertyAccessException("inputFormat");
        }
        ArrayList arrayList = new ArrayList(str.length());
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            String valueOf = String.valueOf(charAt);
            int hashCode = valueOf.hashCode();
            if (hashCode != 45) {
                if (hashCode == 120 && valueOf.equals("x")) {
                    LayoutInflater from = LayoutInflater.from(context);
                    FlexboxLayout flexboxLayout2 = this.k.f90b;
                    View inflate2 = from.inflate(R.layout.view_code_verification_text, (ViewGroup) flexboxLayout2, false);
                    flexboxLayout2.addView(inflate2);
                    Objects.requireNonNull(inflate2, "rootView");
                    viewBinding = new e2((TextView) inflate2);
                    m.checkNotNullExpressionValue(viewBinding, "ViewCodeVerificationText…t), binding.layout, true)");
                    arrayList.add(viewBinding.getRoot());
                }
                throw new IllegalStateException("Invalid format for " + charAt);
            } else if (valueOf.equals("-")) {
                LayoutInflater from2 = LayoutInflater.from(context);
                FlexboxLayout flexboxLayout3 = this.k.f90b;
                View inflate3 = from2.inflate(R.layout.view_code_verification_space, (ViewGroup) flexboxLayout3, false);
                flexboxLayout3.addView(inflate3);
                Objects.requireNonNull(inflate3, "rootView");
                viewBinding = new d2((Space) inflate3);
                m.checkNotNullExpressionValue(viewBinding, "ViewCodeVerificationSpac…t), binding.layout, true)");
                arrayList.add(viewBinding.getRoot());
            } else {
                throw new IllegalStateException("Invalid format for " + charAt);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (next instanceof TextView) {
                arrayList2.add(next);
            }
        }
        this.l = arrayList2;
        ((TextView) arrayList2.get(0)).requestFocus();
        View view = (View) arrayList2.get(0);
        if (!ViewCompat.isLaidOut(view) || view.isLayoutRequested()) {
            view.addOnLayoutChangeListener(new b.a.y.a(this));
        } else {
            a(this);
        }
        this.m = ContextCompat.getDrawable(context, R.drawable.drawable_uikit_background_tertiary_button);
        this.n = ContextCompat.getDrawable(context, R.drawable.drawable_uikit_background_tertiary_button_outline);
        e();
    }

    public static final void a(CodeVerificationView codeVerificationView) {
        Object systemService = codeVerificationView.getContext().getSystemService("input_method");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
        ((InputMethodManager) systemService).showSoftInput(codeVerificationView, 2);
    }

    private final int getInputLength() {
        String str = this.p;
        if (str == null) {
            m.throwUninitializedPropertyAccessException("inputFormat");
        }
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (m.areEqual(String.valueOf(str.charAt(i2)), "x")) {
                i++;
            }
        }
        return i;
    }

    public final void b() {
        if (this.q.length() > 0) {
            this.q = "";
            d();
        }
    }

    public final void c(char c) {
        if (this.q.length() < getInputLength()) {
            String str = this.q;
            this.q = str + c;
            d();
        }
    }

    public final void d() {
        int i = 0;
        for (Object obj : this.l) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            TextView textView = (TextView) obj;
            if (i <= w.getLastIndex(this.q)) {
                textView.setText(String.valueOf(this.q.charAt(i)));
            } else {
                textView.setText("");
            }
        }
        if (this.q.length() == getInputLength()) {
            this.r.invoke(this.q);
        }
        e();
    }

    public final void e() {
        Drawable drawable;
        int min = Math.min(this.q.length(), n.getLastIndex(this.l));
        int i = 0;
        for (Object obj : this.l) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            TextView textView = (TextView) obj;
            if (i == min) {
                drawable = this.n;
            } else {
                drawable = this.m;
            }
            textView.setBackground(drawable);
        }
    }

    public final String getCode() {
        return this.q;
    }

    public final Function1<String, Unit> getOnCodeEntered() {
        return this.r;
    }

    @Override // android.view.View
    public boolean onCheckIsTextEditor() {
        return true;
    }

    @Override // android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        m.checkNotNullParameter(editorInfo, "outAttrs");
        a aVar = this.o;
        if (aVar == null) {
            m.throwUninitializedPropertyAccessException("inputType");
        }
        int ordinal = aVar.ordinal();
        if (ordinal == 0) {
            BaseInputConnection baseInputConnection = new BaseInputConnection(this, false);
            editorInfo.inputType = 18;
            return baseInputConnection;
        } else if (ordinal == 1) {
            BaseInputConnection baseInputConnection2 = new BaseInputConnection(this, false);
            editorInfo.inputType = 4224;
            return baseInputConnection2;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final void setCode(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, ModelAuditLogEntry.CHANGE_KEY_CODE);
        String str = this.q;
        String obj = charSequence.toString();
        this.q = obj;
        if (!m.areEqual(str, obj)) {
            d();
        }
    }

    public final void setOnCodeEntered(Function1<? super String, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.r = function1;
    }
}
