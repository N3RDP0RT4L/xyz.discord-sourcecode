package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.i.g4;
import b.a.y.g0;
import com.discord.utilities.color.ColorCompat;
import d0.a0.a;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: VoiceUserLimitView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010,\u001a\u00020+\u0012\n\b\u0002\u0010.\u001a\u0004\u0018\u00010-¢\u0006\u0004\b/\u00100J%\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\f\u001a\u00020\u00072\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0014¢\u0006\u0004\b\f\u0010\rR\u001f\u0010\u0013\u001a\u0004\u0018\u00010\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0016\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u001a\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001e\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\"\u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010&\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010(\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010\u0019R\u0016\u0010*\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010\u0019¨\u00061"}, d2 = {"Lcom/discord/views/VoiceUserLimitView;", "Landroid/widget/LinearLayout;", "", "current", "max", "", "isVideo", "", "a", "(IIZ)V", "Landroid/graphics/Canvas;", "canvas", "dispatchDraw", "(Landroid/graphics/Canvas;)V", "Landroid/graphics/drawable/Drawable;", "q", "Lkotlin/Lazy;", "getVideoCamDrawable", "()Landroid/graphics/drawable/Drawable;", "videoCamDrawable", "l", "I", "slantWidthPx", "Landroid/graphics/Point;", "n", "Landroid/graphics/Point;", "point1", "Landroid/graphics/Paint;", "k", "Landroid/graphics/Paint;", "paint", "Lb/a/i/g4;", "j", "Lb/a/i/g4;", "binding", "Landroid/graphics/Path;", "m", "Landroid/graphics/Path;", "path", "o", "point2", "p", "point3", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceUserLimitView extends LinearLayout {
    public final g4 j;
    public final Paint k;
    public final int l;
    public final Path m;
    public final Point n;
    public final Point o;
    public final Point p;
    public final Lazy q;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceUserLimitView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(getContext()).inflate(R.layout.voice_user_limit_view, this);
        int i = R.id.voice_user_limit_current;
        TextView textView = (TextView) findViewById(R.id.voice_user_limit_current);
        if (textView != null) {
            i = R.id.voice_user_limit_max;
            TextView textView2 = (TextView) findViewById(R.id.voice_user_limit_max);
            if (textView2 != null) {
                g4 g4Var = new g4(this, textView, textView2);
                m.checkNotNullExpressionValue(g4Var, "VoiceUserLimitViewBindin…ater.from(context), this)");
                this.j = g4Var;
                Paint paint = new Paint(1);
                this.k = paint;
                Path path = new Path();
                this.m = path;
                this.n = new Point();
                this.o = new Point();
                this.p = new Point();
                this.q = g.lazy(new g0(this));
                Resources resources = getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                this.l = a.roundToInt(TypedValue.applyDimension(1, 6.0f, resources.getDisplayMetrics()));
                paint.setColor(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorBackgroundMobilePrimary));
                paint.setStrokeWidth(2.0f);
                paint.setStyle(Paint.Style.FILL_AND_STROKE);
                paint.setAntiAlias(true);
                path.setFillType(Path.FillType.EVEN_ODD);
                return;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    private final Drawable getVideoCamDrawable() {
        return (Drawable) this.q.getValue();
    }

    public final void a(int i, int i2, boolean z2) {
        TextView textView = this.j.f117b;
        m.checkNotNullExpressionValue(textView, "binding.voiceUserLimitCurrent");
        textView.setText(i < 100 ? b.d.b.a.a.N(new Object[]{Integer.valueOf(i)}, 1, "%02d", "java.lang.String.format(format, *args)") : String.valueOf(i));
        TextView textView2 = this.j.c;
        m.checkNotNullExpressionValue(textView2, "binding.voiceUserLimitMax");
        textView2.setText(i2 < 100 ? b.d.b.a.a.N(new Object[]{Integer.valueOf(i2)}, 1, "%02d", "java.lang.String.format(format, *args)") : String.valueOf(i2));
        if (z2) {
            this.j.f117b.setCompoundDrawablesWithIntrinsicBounds(getVideoCamDrawable(), (Drawable) null, (Drawable) null, (Drawable) null);
        } else {
            this.j.f117b.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        m.checkNotNull(canvas);
        int height = getHeight();
        TextView textView = this.j.f117b;
        m.checkNotNullExpressionValue(textView, "binding.voiceUserLimitCurrent");
        int width = textView.getWidth();
        TextView textView2 = this.j.f117b;
        m.checkNotNullExpressionValue(textView2, "binding.voiceUserLimitCurrent");
        int width2 = textView2.getWidth() + this.l;
        this.n.set(width, height);
        this.o.set(width2, 0);
        this.p.set(width, 0);
        Path path = this.m;
        Point point = this.n;
        path.moveTo(point.x, point.y);
        Path path2 = this.m;
        Point point2 = this.o;
        path2.lineTo(point2.x, point2.y);
        Path path3 = this.m;
        Point point3 = this.p;
        path3.lineTo(point3.x, point3.y);
        Path path4 = this.m;
        Point point4 = this.n;
        path4.lineTo(point4.x, point4.y);
        this.m.close();
        canvas.drawPath(this.m, this.k);
        this.m.reset();
    }
}
