package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import b.a.i.v3;
import b.a.k.b;
import com.discord.stores.StoreApplicationStreamPreviews;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import xyz.discord.R;
/* compiled from: StreamPreviewView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\u000b\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\nJ'\u0010\u0012\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0007¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0017\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u001e"}, d2 = {"Lcom/discord/views/StreamPreviewView;", "Landroid/widget/FrameLayout;", "", "url", "", "setImage", "(Ljava/lang/String;)V", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setCaptionText", "(Ljava/lang/CharSequence;)V", "setOverlayCaptionText", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;", "preview", "Lcom/discord/utilities/streams/StreamContext$Joinability;", "joinability", "", "isAlreadyWatchingStream", "a", "(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Z)V", "Lb/a/i/v3;", "j", "Lb/a/i/v3;", "binding", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StreamPreviewView extends FrameLayout {
    public final v3 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StreamPreviewView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_stream_preview, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.stream_preview_image;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.stream_preview_image);
        if (simpleDraweeView != null) {
            i = R.id.stream_preview_overlay_text;
            TextView textView = (TextView) inflate.findViewById(R.id.stream_preview_overlay_text);
            if (textView != null) {
                i = R.id.stream_preview_placeholder_caption;
                TextView textView2 = (TextView) inflate.findViewById(R.id.stream_preview_placeholder_caption);
                if (textView2 != null) {
                    v3 v3Var = new v3((ConstraintLayout) inflate, simpleDraweeView, textView, textView2);
                    m.checkNotNullExpressionValue(v3Var, "ViewStreamPreviewBinding…rom(context), this, true)");
                    this.j = v3Var;
                    setClickable(true);
                    setFocusable(false);
                    setFocusableInTouchMode(false);
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    private final void setCaptionText(CharSequence charSequence) {
        TextView textView = this.j.d;
        m.checkNotNullExpressionValue(textView, "binding.streamPreviewPlaceholderCaption");
        ViewExtensions.setTextAndVisibilityBy(textView, charSequence);
    }

    private final void setImage(String str) {
        this.j.f212b.setImageURI(str);
    }

    private final void setOverlayCaptionText(CharSequence charSequence) {
        TextView textView = this.j.c;
        m.checkNotNullExpressionValue(textView, "binding.streamPreviewOverlayText");
        ViewExtensions.setTextAndVisibilityBy(textView, charSequence);
    }

    @MainThread
    public final void a(StoreApplicationStreamPreviews.StreamPreview streamPreview, StreamContext.Joinability joinability, boolean z2) {
        CharSequence d;
        CharSequence d2;
        CharSequence d3;
        CharSequence d4;
        m.checkNotNullParameter(streamPreview, "preview");
        m.checkNotNullParameter(joinability, "joinability");
        if (streamPreview instanceof StoreApplicationStreamPreviews.StreamPreview.Fetching) {
            setCaptionText(getContext().getString(R.string.stream_preview_loading));
        } else if (streamPreview instanceof StoreApplicationStreamPreviews.StreamPreview.Resolved) {
            StoreApplicationStreamPreviews.StreamPreview.Resolved resolved = (StoreApplicationStreamPreviews.StreamPreview.Resolved) streamPreview;
            if (resolved.getUrl() != null) {
                setImage(resolved.getUrl());
                setCaptionText(null);
            } else {
                setImage(null);
                if (joinability == StreamContext.Joinability.CAN_CONNECT) {
                    setCaptionText(getContext().getString(R.string.stream_no_preview));
                } else {
                    setCaptionText(null);
                }
            }
        } else {
            throw new NoWhenBranchMatchedException();
        }
        if (z2) {
            d4 = b.d(this, R.string.watch_stream_watching, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            setOverlayCaptionText(d4);
            return;
        }
        int ordinal = joinability.ordinal();
        if (ordinal == 0) {
            d = b.d(this, R.string.join_stream, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            setOverlayCaptionText(d);
        } else if (ordinal == 1) {
            d2 = b.d(this, R.string.unable_to_join_channel_full, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            setOverlayCaptionText(d2);
        } else if (ordinal == 2) {
            d3 = b.d(this, R.string.channel_locked_short, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            setOverlayCaptionText(d3);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }
}
