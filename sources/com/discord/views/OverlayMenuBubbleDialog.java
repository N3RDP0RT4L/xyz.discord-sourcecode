package com.discord.views;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.w0;
import b.a.i.x0;
import b.a.y.l;
import b.a.y.n;
import b.a.y.p;
import com.discord.app.AppComponent;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import d0.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import xyz.discord.R;
/* compiled from: OverlayMenuBubbleDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u001fB\u0011\b\u0016\u0012\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0019\u0010\u0006\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0014¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\u000b\u0010\fR\u0013\u0010\u0010\u001a\u00020\r8F@\u0006¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0014\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\"\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006 "}, d2 = {"Lcom/discord/views/OverlayMenuBubbleDialog;", "Lb/a/y/l;", "Lcom/discord/app/AppComponent;", "Landroid/content/res/Configuration;", "newConfig", "", "onConfigurationChanged", "(Landroid/content/res/Configuration;)V", "onAttachedToWindow", "()V", "Landroid/animation/Animator;", "getClosingAnimator", "()Landroid/animation/Animator;", "Landroid/view/View;", "getLinkedAnchorView", "()Landroid/view/View;", "linkedAnchorView", "Lb/a/i/w0;", "z", "Lb/a/i/w0;", "binding", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "Lcom/discord/views/OverlayMenuBubbleDialog$a;", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "adapter", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class OverlayMenuBubbleDialog extends l implements AppComponent {
    public final SimpleRecyclerAdapter<StoreVoiceParticipants.VoiceUser, a> A;

    /* renamed from: z  reason: collision with root package name */
    public final w0 f2804z;

    /* compiled from: OverlayMenuBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class a extends SimpleRecyclerAdapter.ViewHolder<StoreVoiceParticipants.VoiceUser> {
        public final x0 a;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(b.a.i.x0 r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                com.discord.views.VoiceUserView r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r0)
                r2.a = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.views.OverlayMenuBubbleDialog.a.<init>(b.a.i.x0):void");
        }

        @Override // com.discord.utilities.views.SimpleRecyclerAdapter.ViewHolder
        public void bind(StoreVoiceParticipants.VoiceUser voiceUser) {
            StoreVoiceParticipants.VoiceUser voiceUser2 = voiceUser;
            m.checkNotNullParameter(voiceUser2, "data");
            this.a.a.a(voiceUser2, R.dimen.avatar_size_extra_large);
        }
    }

    /* compiled from: OverlayMenuBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class b<T, R> implements j0.k.b<Long, Observable<? extends List<? extends StoreVoiceParticipants.VoiceUser>>> {
        public static final b j = new b();

        @Override // j0.k.b
        public Observable<? extends List<? extends StoreVoiceParticipants.VoiceUser>> call(Long l) {
            Long l2 = l;
            StoreChannels channels = StoreStream.Companion.getChannels();
            m.checkNotNullExpressionValue(l2, "channelId");
            return (Observable<R>) channels.observeChannel(l2.longValue()).Y(new p(l2));
        }
    }

    /* compiled from: OverlayMenuBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class c<T, R> implements j0.k.b<List<? extends StoreVoiceParticipants.VoiceUser>, Pair<? extends List<? extends StoreVoiceParticipants.VoiceUser>, ? extends Integer>> {
        public static final c j = new c();

        @Override // j0.k.b
        public Pair<? extends List<? extends StoreVoiceParticipants.VoiceUser>, ? extends Integer> call(List<? extends StoreVoiceParticipants.VoiceUser> list) {
            List<? extends StoreVoiceParticipants.VoiceUser> list2 = list;
            ArrayList Y = b.d.b.a.a.Y(list2, "voiceUsers");
            for (T t : list2) {
                if (!((StoreVoiceParticipants.VoiceUser) t).isMe()) {
                    Y.add(t);
                }
            }
            int size = Y.size();
            if (size >= 0 && 3 >= size) {
                return o.to(Y, 0);
            }
            return o.to(u.take(Y, 3), Integer.valueOf(Y.size() - 3));
        }
    }

    /* compiled from: OverlayMenuBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class d extends d0.z.d.o implements Function1<Pair<? extends List<? extends StoreVoiceParticipants.VoiceUser>, ? extends Integer>, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Pair<? extends List<? extends StoreVoiceParticipants.VoiceUser>, ? extends Integer> pair) {
            String str;
            Pair<? extends List<? extends StoreVoiceParticipants.VoiceUser>, ? extends Integer> pair2 = pair;
            int intValue = pair2.component2().intValue();
            OverlayMenuBubbleDialog overlayMenuBubbleDialog = OverlayMenuBubbleDialog.this;
            overlayMenuBubbleDialog.A.setData(pair2.component1());
            if (intValue == 0) {
                str = null;
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append('+');
                sb.append(intValue);
                str = sb.toString();
            }
            TextView textView = overlayMenuBubbleDialog.f2804z.d;
            m.checkNotNullExpressionValue(textView, "binding.overlayMembersOverflowTv");
            ViewExtensions.setTextAndVisibilityBy(textView, str);
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OverlayMenuBubbleDialog(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.overlay_bubble_menu, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.overlay_header;
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.overlay_header);
        if (linearLayout != null) {
            i = R.id.overlay_linked_anchor_view;
            View findViewById = inflate.findViewById(R.id.overlay_linked_anchor_view);
            if (findViewById != null) {
                i = R.id.overlay_members_overflow_tv;
                TextView textView = (TextView) inflate.findViewById(R.id.overlay_members_overflow_tv);
                if (textView != null) {
                    i = R.id.overlay_members_rv;
                    RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.overlay_members_rv);
                    if (recyclerView != null) {
                        i = R.id.overlay_menu;
                        OverlayMenuView overlayMenuView = (OverlayMenuView) inflate.findViewById(R.id.overlay_menu);
                        if (overlayMenuView != null) {
                            w0 w0Var = new w0((LinearLayout) inflate, linearLayout, findViewById, textView, recyclerView, overlayMenuView);
                            m.checkNotNullExpressionValue(w0Var, "OverlayBubbleMenuBinding…rom(context), this, true)");
                            this.f2804z = w0Var;
                            this.A = new SimpleRecyclerAdapter<>(null, n.j, 1, null);
                            overlayMenuView.setOnDismissRequested$app_productionGoogleRelease(new p(0, this));
                            setClipChildren(false);
                            return;
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // b.a.y.l
    public Animator getClosingAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setStartDelay(getResources().getInteger(17694720));
        OverlayMenuView overlayMenuView = this.f2804z.f;
        m.checkNotNullExpressionValue(overlayMenuView, "binding.overlayMenu");
        Animator loadAnimator = AnimatorInflater.loadAnimator(overlayMenuView.getContext(), R.animator.overlay_slide_down_fade_out);
        loadAnimator.setTarget(this.f2804z.f);
        Animator loadAnimator2 = AnimatorInflater.loadAnimator(getContext(), R.animator.overlay_slide_up_fade_out);
        loadAnimator2.setTarget(this.f2804z.f216b);
        animatorSet.playTogether(loadAnimator, loadAnimator2);
        return animatorSet;
    }

    public final View getLinkedAnchorView() {
        View view = this.f2804z.c;
        m.checkNotNullExpressionValue(view, "binding.overlayLinkedAnchorView");
        return view;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        OverlayMenuView overlayMenuView = this.f2804z.f;
        m.checkNotNullExpressionValue(overlayMenuView, "binding.overlayMenu");
        overlayMenuView.setAlpha(0.0f);
        LinearLayout linearLayout = this.f2804z.f216b;
        m.checkNotNullExpressionValue(linearLayout, "binding.overlayHeader");
        linearLayout.setAlpha(0.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        OverlayMenuView overlayMenuView2 = this.f2804z.f;
        m.checkNotNullExpressionValue(overlayMenuView2, "binding.overlayMenu");
        Animator loadAnimator = AnimatorInflater.loadAnimator(overlayMenuView2.getContext(), R.animator.overlay_slide_up_fade_in);
        loadAnimator.setTarget(this.f2804z.f);
        Animator loadAnimator2 = AnimatorInflater.loadAnimator(getContext(), R.animator.overlay_slide_down_fade_in);
        loadAnimator2.setTarget(this.f2804z.f216b);
        animatorSet.playTogether(loadAnimator, loadAnimator2);
        animatorSet.setStartDelay(getResources().getInteger(17694720));
        animatorSet.start();
        RecyclerView recyclerView = this.f2804z.e;
        m.checkNotNullExpressionValue(recyclerView, "binding.overlayMembersRv");
        recyclerView.setAdapter(this.A);
        Observable F = StoreStream.Companion.getVoiceChannelSelected().observeSelectedVoiceChannelId().Y(b.j).F(c.j);
        m.checkNotNullExpressionValue(F, "StoreStream\n        .get…- 3\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(F), this, null, 2, null), OverlayMenuBubbleDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d());
    }

    @Override // com.discord.overlay.views.OverlayDialog, com.discord.overlay.views.OverlayBubbleWrap, android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        getOnDialogClosed().invoke(this);
    }
}
