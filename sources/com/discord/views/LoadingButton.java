package com.discord.views;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.annotation.ColorInt;
import androidx.core.app.NotificationCompat;
import b.a.i.j2;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: LoadingButton.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0019\u0010\u0005\u001a\u00020\u00042\b\b\u0001\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000b¢\u0006\u0004\b\u0010\u0010\u000eJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u0012\u0010\u000eJ\u0019\u0010\u0014\u001a\u00020\u00042\b\b\u0001\u0010\u0013\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0014\u0010\u0006R\u0018\u0010\u0017\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0018\u0010\u001b\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001f\u001a\u00020\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010#\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006$"}, d2 = {"Lcom/discord/views/LoadingButton;", "Landroid/widget/FrameLayout;", "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "", "setProgressBarColor", "(I)V", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setText", "(Ljava/lang/CharSequence;)V", "", "isLoading", "setIsLoading", "(Z)V", "visible", "setIconVisibility", "enabled", "setEnabled", "colorInt", "setBackgroundColor", "l", "Ljava/lang/CharSequence;", "buttonText", "Landroid/graphics/drawable/Drawable;", "m", "Landroid/graphics/drawable/Drawable;", "buttonIcon", "", "k", "F", "disabledAlpha", "Lb/a/i/j2;", "j", "Lb/a/i/j2;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class LoadingButton extends FrameLayout {
    public final j2 j;
    public float k;
    public CharSequence l;
    public Drawable m;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Removed duplicated region for block: B:46:0x012f A[Catch: all -> 0x014c, TryCatch #0 {all -> 0x014c, blocks: (B:9:0x0052, B:11:0x0063, B:12:0x0066, B:15:0x006e, B:16:0x0074, B:18:0x0081, B:19:0x0084, B:21:0x00d2, B:22:0x00d8, B:24:0x00e1, B:25:0x00e7, B:28:0x00f2, B:29:0x00f6, B:31:0x00fa, B:34:0x0104, B:37:0x010e, B:40:0x0118, B:44:0x0122, B:46:0x012f, B:47:0x0132, B:50:0x0143, B:51:0x0145), top: B:59:0x0052 }] */
    /* JADX WARN: Removed duplicated region for block: B:49:0x0140  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x0143 A[Catch: all -> 0x014c, TryCatch #0 {all -> 0x014c, blocks: (B:9:0x0052, B:11:0x0063, B:12:0x0066, B:15:0x006e, B:16:0x0074, B:18:0x0081, B:19:0x0084, B:21:0x00d2, B:22:0x00d8, B:24:0x00e1, B:25:0x00e7, B:28:0x00f2, B:29:0x00f6, B:31:0x00fa, B:34:0x0104, B:37:0x010e, B:40:0x0118, B:44:0x0122, B:46:0x012f, B:47:0x0132, B:50:0x0143, B:51:0x0145), top: B:59:0x0052 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public LoadingButton(android.content.Context r10, android.util.AttributeSet r11) {
        /*
            Method dump skipped, instructions count: 376
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.LoadingButton.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    private final void setProgressBarColor(@ColorInt int i) {
        ProgressBar progressBar = this.j.c;
        m.checkNotNullExpressionValue(progressBar, "binding.loadingButtonProgress");
        progressBar.getIndeterminateDrawable().setColorFilter(i, PorterDuff.Mode.SRC_IN);
    }

    @Override // android.view.View
    public void setBackgroundColor(@ColorInt int i) {
        MaterialButton materialButton = this.j.f136b;
        m.checkNotNullExpressionValue(materialButton, "binding.loadingButtonButton");
        materialButton.setBackgroundTintList(ColorStateList.valueOf(i));
    }

    @Override // android.view.View
    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        MaterialButton materialButton = this.j.f136b;
        m.checkNotNullExpressionValue(materialButton, "binding.loadingButtonButton");
        materialButton.setEnabled(z2);
        setAlpha(isEnabled() ? 1.0f : this.k);
    }

    public final void setIconVisibility(boolean z2) {
        if (z2) {
            MaterialButton materialButton = this.j.f136b;
            m.checkNotNullExpressionValue(materialButton, "binding.loadingButtonButton");
            materialButton.setIcon(this.m);
            return;
        }
        MaterialButton materialButton2 = this.j.f136b;
        m.checkNotNullExpressionValue(materialButton2, "binding.loadingButtonButton");
        materialButton2.setIcon(null);
    }

    public final void setIsLoading(boolean z2) {
        if (z2) {
            setClickable(false);
            MaterialButton materialButton = this.j.f136b;
            m.checkNotNullExpressionValue(materialButton, "binding.loadingButtonButton");
            this.l = materialButton.getText().toString();
            MaterialButton materialButton2 = this.j.f136b;
            m.checkNotNullExpressionValue(materialButton2, "binding.loadingButtonButton");
            materialButton2.setText((CharSequence) null);
            MaterialButton materialButton3 = this.j.f136b;
            m.checkNotNullExpressionValue(materialButton3, "binding.loadingButtonButton");
            materialButton3.setIcon(null);
            ProgressBar progressBar = this.j.c;
            m.checkNotNullExpressionValue(progressBar, "binding.loadingButtonProgress");
            progressBar.setVisibility(0);
            return;
        }
        setClickable(true);
        MaterialButton materialButton4 = this.j.f136b;
        m.checkNotNullExpressionValue(materialButton4, "binding.loadingButtonButton");
        materialButton4.setText(this.l);
        MaterialButton materialButton5 = this.j.f136b;
        m.checkNotNullExpressionValue(materialButton5, "binding.loadingButtonButton");
        materialButton5.setIcon(this.m);
        ProgressBar progressBar2 = this.j.c;
        m.checkNotNullExpressionValue(progressBar2, "binding.loadingButtonProgress");
        progressBar2.setVisibility(8);
    }

    public final void setText(CharSequence charSequence) {
        this.l = charSequence;
        MaterialButton materialButton = this.j.f136b;
        m.checkNotNullExpressionValue(materialButton, "binding.loadingButtonButton");
        ProgressBar progressBar = this.j.c;
        m.checkNotNullExpressionValue(progressBar, "binding.loadingButtonProgress");
        if (progressBar.getVisibility() == 0) {
            charSequence = null;
        }
        materialButton.setText(charSequence);
    }
}
