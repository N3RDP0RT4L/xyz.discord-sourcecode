package com.discord.views.guilds;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.i.e3;
import b.a.k.b;
import com.discord.utilities.resources.StringResourceUtilsKt;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ServerMemberCount.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\f\u0010\nR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0011"}, d2 = {"Lcom/discord/views/guilds/ServerMemberCount;", "Landroid/widget/FrameLayout;", "", "charSequence", "", "setInvalidText", "(Ljava/lang/CharSequence;)V", "", "active", "setOnline", "(Ljava/lang/Integer;)V", "members", "setMembers", "Lb/a/i/e3;", "j", "Lb/a/i/e3;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerMemberCount extends FrameLayout {
    public final e3 j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerMemberCount(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_server_member_count, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.item_invite_online_dot;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.item_invite_online_dot);
        if (imageView != null) {
            i = R.id.item_invite_online_text;
            TextView textView = (TextView) inflate.findViewById(R.id.item_invite_online_text);
            if (textView != null) {
                i = R.id.item_invite_total_member_dot;
                ImageView imageView2 = (ImageView) inflate.findViewById(R.id.item_invite_total_member_dot);
                if (imageView2 != null) {
                    i = R.id.item_invite_total_member_text;
                    TextView textView2 = (TextView) inflate.findViewById(R.id.item_invite_total_member_text);
                    if (textView2 != null) {
                        e3 e3Var = new e3((LinearLayout) inflate, imageView, textView, imageView2, textView2);
                        m.checkNotNullExpressionValue(e3Var, "ViewServerMemberCountBin…rom(context), this, true)");
                        this.j = e3Var;
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void setInvalidText(CharSequence charSequence) {
        ImageView imageView = this.j.f104b;
        m.checkNotNullExpressionValue(imageView, "binding.itemInviteOnlineDot");
        imageView.setVisibility(8);
        ImageView imageView2 = this.j.d;
        m.checkNotNullExpressionValue(imageView2, "binding.itemInviteTotalMemberDot");
        imageView2.setVisibility(8);
        TextView textView = this.j.c;
        m.checkNotNullExpressionValue(textView, "binding.itemInviteOnlineText");
        b.a(textView, charSequence);
        TextView textView2 = this.j.e;
        m.checkNotNullExpressionValue(textView2, "binding.itemInviteTotalMemberText");
        b.a(textView2, null);
    }

    public final void setMembers(Integer num) {
        CharSequence charSequence;
        ImageView imageView = this.j.d;
        m.checkNotNullExpressionValue(imageView, "binding.itemInviteTotalMemberDot");
        imageView.setVisibility(num != null ? 0 : 8);
        TextView textView = this.j.e;
        m.checkNotNullExpressionValue(textView, "binding.itemInviteTotalMemberText");
        if (num != null) {
            num.intValue();
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            charSequence = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.members_header_members, num.intValue(), num);
        } else {
            charSequence = null;
        }
        b.a(textView, charSequence);
    }

    public final void setOnline(Integer num) {
        ImageView imageView = this.j.f104b;
        m.checkNotNullExpressionValue(imageView, "binding.itemInviteOnlineDot");
        imageView.setVisibility(num != null ? 0 : 8);
        TextView textView = this.j.c;
        m.checkNotNullExpressionValue(textView, "binding.itemInviteOnlineText");
        CharSequence charSequence = null;
        if (num != null) {
            num.intValue();
            charSequence = b.d(this, R.string.instant_invite_guild_members_online, new Object[]{num}, (r4 & 4) != 0 ? b.c.j : null);
        }
        b.a(textView, charSequence);
    }
}
