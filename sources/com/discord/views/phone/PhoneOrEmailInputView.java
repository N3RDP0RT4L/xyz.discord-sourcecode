package com.discord.views.phone;

import andhook.lib.HookHelper;
import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.i.q2;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.phone.PhoneCountryCode;
import com.discord.utilities.phone.PhoneUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.phone.WidgetPhoneCountryCodeBottomSheet;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: PhoneOrEmailInputView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001:\u00018J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0014\u001a\u00020\u00042\b\u0010\u0013\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0014\u0010\u0012J\u0015\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018J\r\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001a\u0010\u001bJ\r\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001d\u0010\u001eJ\r\u0010 \u001a\u00020\u001f¢\u0006\u0004\b \u0010!J#\u0010$\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00040\"¢\u0006\u0004\b$\u0010%J\u000f\u0010'\u001a\u00020&H\u0002¢\u0006\u0004\b'\u0010(J\u000f\u0010)\u001a\u00020\u0004H\u0002¢\u0006\u0004\b)\u0010*J\u000f\u0010+\u001a\u00020\u0004H\u0002¢\u0006\u0004\b+\u0010*J\u000f\u0010,\u001a\u00020\u0004H\u0002¢\u0006\u0004\b,\u0010*R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00102\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00105\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00107\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u00104¨\u00069"}, d2 = {"Lcom/discord/views/phone/PhoneOrEmailInputView;", "Landroid/widget/LinearLayout;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "", "setMainInputNextFocusForwardId", "(I)V", "Landroidx/fragment/app/Fragment;", "fragment", "b", "(Landroidx/fragment/app/Fragment;)V", "Lcom/discord/models/phone/PhoneCountryCode;", "countryCode", "setCountryCode", "(Lcom/discord/models/phone/PhoneCountryCode;)V", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setText", "(Ljava/lang/CharSequence;)V", "hint", "setHint", "Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;", "mode", "setMode", "(Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;)V", "Lcom/google/android/material/textfield/TextInputLayout;", "getMainTextInputLayout", "()Lcom/google/android/material/textfield/TextInputLayout;", "Lcom/google/android/material/textfield/TextInputEditText;", "getMainEditText", "()Lcom/google/android/material/textfield/TextInputEditText;", "", "getTextOrEmpty", "()Ljava/lang/String;", "Lkotlin/Function0;", "onAfterTextChanged", "a", "(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function0;)V", "", "e", "()Z", "f", "()V", "c", "d", "l", "Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;", "Lb/a/i/q2;", "k", "Lb/a/i/q2;", "binding", "m", "Ljava/lang/CharSequence;", NotificationCompat.CATEGORY_EMAIL, "n", "phone", "Mode", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PhoneOrEmailInputView extends LinearLayout {
    public static final /* synthetic */ int j = 0;
    public final q2 k;
    public Mode l;
    public CharSequence m;
    public CharSequence n;

    /* compiled from: PhoneOrEmailInputView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ADAPTIVE", "EMAIL", "PHONE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum Mode {
        ADAPTIVE,
        EMAIL,
        PHONE
    }

    /* compiled from: PhoneOrEmailInputView.kt */
    /* loaded from: classes2.dex */
    public static final class a extends o implements Function1<Editable, Unit> {
        public final /* synthetic */ Function0 $onAfterTextChanged;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Function0 function0) {
            super(1);
            this.$onAfterTextChanged = function0;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Editable editable) {
            m.checkNotNullParameter(editable, "it");
            this.$onAfterTextChanged.invoke();
            return Unit.a;
        }
    }

    /* compiled from: PhoneOrEmailInputView.kt */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function1<Editable, Unit> {
        public final /* synthetic */ Function0 $onAfterTextChanged;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(Function0 function0) {
            super(1);
            this.$onAfterTextChanged = function0;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Editable editable) {
            m.checkNotNullParameter(editable, "it");
            PhoneOrEmailInputView phoneOrEmailInputView = PhoneOrEmailInputView.this;
            int i = PhoneOrEmailInputView.j;
            phoneOrEmailInputView.c();
            this.$onAfterTextChanged.invoke();
            return Unit.a;
        }
    }

    /* compiled from: PhoneOrEmailInputView.kt */
    /* loaded from: classes2.dex */
    public static final class c extends o implements Function1<View, Unit> {
        public final /* synthetic */ Fragment $fragment;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(Fragment fragment) {
            super(1);
            this.$fragment = fragment;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(View view) {
            m.checkNotNullParameter(view, "it");
            WidgetPhoneCountryCodeBottomSheet.Companion companion = WidgetPhoneCountryCodeBottomSheet.Companion;
            FragmentManager parentFragmentManager = this.$fragment.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "fragment.parentFragmentManager");
            companion.show(parentFragmentManager);
            return Unit.a;
        }
    }

    /* compiled from: PhoneOrEmailInputView.kt */
    /* loaded from: classes2.dex */
    public static final class d extends o implements Function1<Editable, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Editable editable) {
            m.checkNotNullParameter(editable, "it");
            PhoneOrEmailInputView phoneOrEmailInputView = PhoneOrEmailInputView.this;
            int i = PhoneOrEmailInputView.j;
            phoneOrEmailInputView.f();
            PhoneOrEmailInputView.this.c();
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PhoneOrEmailInputView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.view_phone_or_email_input, this);
        int i = R.id.phone_or_email_country_code_wrap;
        TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.phone_or_email_country_code_wrap);
        if (textInputLayout != null) {
            i = R.id.phone_or_email_main_input;
            TextInputEditText textInputEditText = (TextInputEditText) findViewById(R.id.phone_or_email_main_input);
            if (textInputEditText != null) {
                i = R.id.phone_or_email_main_input_wrap;
                TextInputLayout textInputLayout2 = (TextInputLayout) findViewById(R.id.phone_or_email_main_input_wrap);
                if (textInputLayout2 != null) {
                    q2 q2Var = new q2(this, textInputLayout, textInputEditText, textInputLayout2);
                    m.checkNotNullExpressionValue(q2Var, "ViewPhoneOrEmailInputBin…ater.from(context), this)");
                    this.k = q2Var;
                    Mode mode = Mode.ADAPTIVE;
                    this.l = mode;
                    this.m = "";
                    this.n = "";
                    setLayoutTransition(new LayoutTransition());
                    if (attributeSet != null) {
                        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.PhoneOrEmailInputView, 0, 0);
                        m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…neOrEmailInputView, 0, 0)");
                        try {
                            setHint(obtainStyledAttributes.getString(1));
                            setMode(Mode.values()[obtainStyledAttributes.getInt(0, mode.ordinal())]);
                            int resourceId = obtainStyledAttributes.getResourceId(2, -1);
                            if (resourceId != -1) {
                                setMainInputNextFocusForwardId(resourceId);
                            }
                            return;
                        } finally {
                            obtainStyledAttributes.recycle();
                        }
                    } else {
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    private final void setMainInputNextFocusForwardId(int i) {
        TextInputLayout textInputLayout = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailMainInputWrap");
        textInputLayout.setNextFocusForwardId(i);
    }

    public final void a(Fragment fragment, Function0<Unit> function0) {
        m.checkNotNullParameter(fragment, "fragment");
        m.checkNotNullParameter(function0, "onAfterTextChanged");
        TextInputLayout textInputLayout = this.k.f181b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailCountryCodeWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout, fragment, new a(function0));
        TextInputLayout textInputLayout2 = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.phoneOrEmailMainInputWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, fragment, new b(function0));
    }

    public final void b(Fragment fragment) {
        m.checkNotNullParameter(fragment, "fragment");
        TextInputLayout textInputLayout = this.k.f181b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailCountryCodeWrap");
        ViewExtensions.setOnEditTextClickListener(textInputLayout, new c(fragment));
        TextInputLayout textInputLayout2 = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.phoneOrEmailMainInputWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, fragment, new d());
        c();
        d();
    }

    public final void c() {
        TextInputLayout textInputLayout = this.k.f181b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailCountryCodeWrap");
        textInputLayout.setVisibility(e() ? 0 : 8);
    }

    public final void d() {
        if (this.l.ordinal() != 2) {
            TextInputEditText textInputEditText = this.k.c;
            m.checkNotNullExpressionValue(textInputEditText, "binding.phoneOrEmailMainInput");
            textInputEditText.setInputType(32);
            if (Build.VERSION.SDK_INT >= 26) {
                this.k.c.setAutofillHints(new String[]{"emailAddress"});
            }
            TextInputLayout textInputLayout = this.k.d;
            m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailMainInputWrap");
            ViewExtensions.setText(textInputLayout, this.m);
            this.k.c.setSelection(this.m.length());
            return;
        }
        TextInputEditText textInputEditText2 = this.k.c;
        m.checkNotNullExpressionValue(textInputEditText2, "binding.phoneOrEmailMainInput");
        textInputEditText2.setInputType(3);
        if (Build.VERSION.SDK_INT >= 26) {
            this.k.c.setAutofillHints(new String[]{"phone"});
        }
        TextInputLayout textInputLayout2 = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.phoneOrEmailMainInputWrap");
        ViewExtensions.setText(textInputLayout2, this.n);
        this.k.c.setSelection(this.n.length());
    }

    public final boolean e() {
        int ordinal = this.l.ordinal();
        if (ordinal == 1) {
            return false;
        }
        if (ordinal == 2) {
            return true;
        }
        PhoneUtils phoneUtils = PhoneUtils.INSTANCE;
        TextInputLayout textInputLayout = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailMainInputWrap");
        return phoneUtils.isLikelyToContainPhoneNumber(ViewExtensions.getTextOrEmpty(textInputLayout));
    }

    public final void f() {
        TextInputLayout textInputLayout = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailMainInputWrap");
        String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
        if (this.l == Mode.PHONE || PhoneUtils.INSTANCE.isLikelyToContainPhoneNumber(textOrEmpty)) {
            this.n = textOrEmpty;
        } else {
            this.m = textOrEmpty;
        }
    }

    public final TextInputEditText getMainEditText() {
        TextInputEditText textInputEditText = this.k.c;
        m.checkNotNullExpressionValue(textInputEditText, "binding.phoneOrEmailMainInput");
        return textInputEditText;
    }

    public final TextInputLayout getMainTextInputLayout() {
        TextInputLayout textInputLayout = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailMainInputWrap");
        return textInputLayout;
    }

    public final String getTextOrEmpty() {
        if (e()) {
            StringBuilder sb = new StringBuilder();
            TextInputLayout textInputLayout = this.k.f181b;
            m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailCountryCodeWrap");
            sb.append(ViewExtensions.getTextOrEmpty(textInputLayout));
            TextInputLayout textInputLayout2 = this.k.d;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.phoneOrEmailMainInputWrap");
            sb.append(ViewExtensions.getTextOrEmpty(textInputLayout2));
            return sb.toString();
        }
        TextInputLayout textInputLayout3 = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.phoneOrEmailMainInputWrap");
        return ViewExtensions.getTextOrEmpty(textInputLayout3);
    }

    public final void setCountryCode(PhoneCountryCode phoneCountryCode) {
        m.checkNotNullParameter(phoneCountryCode, "countryCode");
        TextInputLayout textInputLayout = this.k.f181b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailCountryCodeWrap");
        ViewExtensions.setText(textInputLayout, phoneCountryCode.getCode());
    }

    public final void setHint(CharSequence charSequence) {
        TextInputLayout textInputLayout = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailMainInputWrap");
        textInputLayout.setHint(charSequence);
    }

    public final void setMode(Mode mode) {
        m.checkNotNullParameter(mode, "mode");
        this.l = mode;
        c();
        d();
    }

    public final void setText(CharSequence charSequence) {
        TextInputLayout textInputLayout = this.k.d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.phoneOrEmailMainInputWrap");
        ViewExtensions.setText(textInputLayout, charSequence);
        f();
    }
}
