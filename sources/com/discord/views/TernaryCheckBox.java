package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.app.NotificationCompat;
import b.a.d.m;
import b.a.i.w3;
import b.a.k.b;
import com.discord.utilities.view.text.LinkifiedTextView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: TernaryCheckBox.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 62\u00020\u0001:\u00027\rB\u001b\b\u0016\u0012\u0006\u00101\u001a\u000200\u0012\b\u00103\u001a\u0004\u0018\u000102¢\u0006\u0004\b4\u00105J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\r\u0010\b\u001a\u00020\u0004¢\u0006\u0004\b\b\u0010\tJ\r\u0010\n\u001a\u00020\u0004¢\u0006\u0004\b\n\u0010\tJ\r\u0010\u000b\u001a\u00020\u0004¢\u0006\u0004\b\u000b\u0010\tJ\r\u0010\r\u001a\u00020\f¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0005\u0010\u0011J\u0015\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0007\u0010\u0011J\r\u0010\u0012\u001a\u00020\u0004¢\u0006\u0004\b\u0012\u0010\tJ\u0017\u0010\u0015\u001a\u00020\u00042\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0017\u001a\u00020\u00042\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013¢\u0006\u0004\b\u0017\u0010\u0016R\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R$\u0010\"\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0016\u0010&\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0018\u0010)\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(R*\u0010*\u001a\u00020\u000f2\u0006\u0010*\u001a\u00020\u000f8\u0006@BX\u0086\u000e¢\u0006\u0012\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.\"\u0004\b/\u0010\u0011¨\u00068"}, d2 = {"Lcom/discord/views/TernaryCheckBox;", "Landroid/widget/RelativeLayout;", "", "message", "", "setOffDisabled", "(Ljava/lang/String;)V", "setDisabled", "f", "()V", "e", "d", "", "b", "()Z", "", "messageRes", "(I)V", "c", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setLabel", "(Ljava/lang/CharSequence;)V", "setSubtext", "l", "Ljava/lang/String;", "labelText", "Lcom/discord/views/TernaryCheckBox$b;", "n", "Lcom/discord/views/TernaryCheckBox$b;", "getOnSwitchStatusChangedListener", "()Lcom/discord/views/TernaryCheckBox$b;", "setOnSwitchStatusChangedListener", "(Lcom/discord/views/TernaryCheckBox$b;)V", "onSwitchStatusChangedListener", "Lb/a/i/w3;", "k", "Lb/a/i/w3;", "binding", "m", "Ljava/lang/CharSequence;", "subtextText", "switchStatus", "o", "I", "getSwitchStatus", "()I", "setSwitchStatus", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "j", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class TernaryCheckBox extends RelativeLayout {
    public static final a j = new a(null);
    public final w3 k;
    public String l;
    public CharSequence m;
    public b n;
    public int o;

    /* compiled from: TernaryCheckBox.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: TernaryCheckBox.kt */
    /* loaded from: classes2.dex */
    public interface b {
        void onSwitchStatusChanged(int i);
    }

    /* compiled from: TernaryCheckBox.kt */
    /* loaded from: classes2.dex */
    public static final class c implements View.OnClickListener {
        public final /* synthetic */ String k;

        public c(String str) {
            this.k = str;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            m.h(TernaryCheckBox.this.getContext(), this.k, 0, null, 12);
        }
    }

    /* compiled from: TernaryCheckBox.kt */
    /* loaded from: classes2.dex */
    public static final class d implements View.OnClickListener {
        public final /* synthetic */ String k;

        public d(String str) {
            this.k = str;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            m.h(TernaryCheckBox.this.getContext(), this.k, 0, null, 12);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TernaryCheckBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d0.z.d.m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_ternary_checkbox, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.checkable_off_container;
        FrameLayout frameLayout = (FrameLayout) inflate.findViewById(R.id.checkable_off_container);
        if (frameLayout != null) {
            i = R.id.checkboxes_container;
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.checkboxes_container);
            if (linearLayout != null) {
                i = R.id.off_disabled_overlay;
                View findViewById = inflate.findViewById(R.id.off_disabled_overlay);
                if (findViewById != null) {
                    i = R.id.setting_disabled_overlay;
                    View findViewById2 = inflate.findViewById(R.id.setting_disabled_overlay);
                    if (findViewById2 != null) {
                        i = R.id.setting_label;
                        TextView textView = (TextView) inflate.findViewById(R.id.setting_label);
                        if (textView != null) {
                            i = R.id.setting_subtext;
                            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) inflate.findViewById(R.id.setting_subtext);
                            if (linkifiedTextView != null) {
                                i = R.id.ternary_check_neutral;
                                CheckableImageView checkableImageView = (CheckableImageView) inflate.findViewById(R.id.ternary_check_neutral);
                                if (checkableImageView != null) {
                                    i = R.id.ternary_check_off;
                                    CheckableImageView checkableImageView2 = (CheckableImageView) inflate.findViewById(R.id.ternary_check_off);
                                    if (checkableImageView2 != null) {
                                        i = R.id.ternary_check_on;
                                        CheckableImageView checkableImageView3 = (CheckableImageView) inflate.findViewById(R.id.ternary_check_on);
                                        if (checkableImageView3 != null) {
                                            w3 w3Var = new w3((LinearLayout) inflate, frameLayout, linearLayout, findViewById, findViewById2, textView, linkifiedTextView, checkableImageView, checkableImageView2, checkableImageView3);
                                            d0.z.d.m.checkNotNullExpressionValue(w3Var, "ViewTernaryCheckboxBindi…rom(context), this, true)");
                                            this.k = w3Var;
                                            this.o = -1;
                                            if (attributeSet != null) {
                                                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.TernaryCheckBox, 0, 0);
                                                d0.z.d.m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…le.TernaryCheckBox, 0, 0)");
                                                try {
                                                    this.l = obtainStyledAttributes.getString(0);
                                                    String string = obtainStyledAttributes.getString(1);
                                                    this.m = string != null ? b.a.k.b.g(string, new Object[0], (r3 & 2) != 0 ? b.e.j : null) : null;
                                                } finally {
                                                    obtainStyledAttributes.recycle();
                                                }
                                            }
                                            d0.z.d.m.checkNotNullExpressionValue(textView, "binding.settingLabel");
                                            int i2 = 8;
                                            textView.setVisibility(this.l != null ? 0 : 8);
                                            d0.z.d.m.checkNotNullExpressionValue(textView, "binding.settingLabel");
                                            textView.setText(this.l);
                                            d0.z.d.m.checkNotNullExpressionValue(linkifiedTextView, "binding.settingSubtext");
                                            linkifiedTextView.setVisibility(this.m != null ? 0 : i2);
                                            d0.z.d.m.checkNotNullExpressionValue(linkifiedTextView, "binding.settingSubtext");
                                            linkifiedTextView.setText(this.m);
                                            checkableImageView3.setOnClickListener(new defpackage.c(0, this));
                                            checkableImageView2.setOnClickListener(new defpackage.c(1, this));
                                            checkableImageView.setOnClickListener(new defpackage.c(2, this));
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    private final void setDisabled(String str) {
        FrameLayout frameLayout = this.k.f219b;
        d0.z.d.m.checkNotNullExpressionValue(frameLayout, "binding.checkableOffContainer");
        frameLayout.setEnabled(true);
        View view = this.k.c;
        d0.z.d.m.checkNotNullExpressionValue(view, "binding.offDisabledOverlay");
        view.setVisibility(8);
        this.k.d.setOnClickListener(new c(str));
        View view2 = this.k.d;
        d0.z.d.m.checkNotNullExpressionValue(view2, "binding.settingDisabledOverlay");
        view2.setVisibility(0);
    }

    private final void setOffDisabled(String str) {
        CheckableImageView checkableImageView = this.k.h;
        d0.z.d.m.checkNotNullExpressionValue(checkableImageView, "binding.ternaryCheckOff");
        checkableImageView.setEnabled(false);
        View view = this.k.d;
        d0.z.d.m.checkNotNullExpressionValue(view, "binding.settingDisabledOverlay");
        view.setVisibility(8);
        this.k.c.setOnClickListener(new d(str));
        View view2 = this.k.c;
        d0.z.d.m.checkNotNullExpressionValue(view2, "binding.offDisabledOverlay");
        view2.setVisibility(0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setSwitchStatus(int i) {
        this.o = i;
        CheckableImageView checkableImageView = this.k.i;
        d0.z.d.m.checkNotNullExpressionValue(checkableImageView, "binding.ternaryCheckOn");
        boolean z2 = false;
        checkableImageView.setChecked(i == 1);
        CheckableImageView checkableImageView2 = this.k.h;
        d0.z.d.m.checkNotNullExpressionValue(checkableImageView2, "binding.ternaryCheckOff");
        checkableImageView2.setChecked(i == -1);
        CheckableImageView checkableImageView3 = this.k.g;
        d0.z.d.m.checkNotNullExpressionValue(checkableImageView3, "binding.ternaryCheckNeutral");
        if (i == 0) {
            z2 = true;
        }
        checkableImageView3.setChecked(z2);
        b bVar = this.n;
        if (bVar != null) {
            bVar.onSwitchStatusChanged(i);
        }
    }

    public final boolean b() {
        return this.o == 1;
    }

    public final void c() {
        CheckableImageView checkableImageView = this.k.h;
        d0.z.d.m.checkNotNullExpressionValue(checkableImageView, "binding.ternaryCheckOff");
        checkableImageView.setEnabled(true);
        View view = this.k.c;
        d0.z.d.m.checkNotNullExpressionValue(view, "binding.offDisabledOverlay");
        view.setVisibility(8);
        View view2 = this.k.d;
        d0.z.d.m.checkNotNullExpressionValue(view2, "binding.settingDisabledOverlay");
        view2.setVisibility(8);
    }

    public final void d() {
        setSwitchStatus(0);
    }

    public final void e() {
        setSwitchStatus(-1);
    }

    public final void f() {
        setSwitchStatus(1);
    }

    public final b getOnSwitchStatusChangedListener() {
        return this.n;
    }

    public final int getSwitchStatus() {
        return this.o;
    }

    public final void setLabel(CharSequence charSequence) {
        TextView textView = this.k.e;
        d0.z.d.m.checkNotNullExpressionValue(textView, "binding.settingLabel");
        textView.setText(charSequence);
    }

    public final void setOnSwitchStatusChangedListener(b bVar) {
        this.n = bVar;
    }

    public final void setSubtext(CharSequence charSequence) {
        LinkifiedTextView linkifiedTextView = this.k.f;
        d0.z.d.m.checkNotNullExpressionValue(linkifiedTextView, "binding.settingSubtext");
        linkifiedTextView.setText(charSequence);
    }

    public final void setDisabled(int i) {
        String string = getContext().getString(i);
        d0.z.d.m.checkNotNullExpressionValue(string, "context.getString(messageRes)");
        setDisabled(string);
    }

    public final void setOffDisabled(int i) {
        String string = getContext().getString(i);
        d0.z.d.m.checkNotNullExpressionValue(string, "context.getString(messageRes)");
        setOffDisabled(string);
    }
}
