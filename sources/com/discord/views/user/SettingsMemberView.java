package com.discord.views.user;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import androidx.annotation.ColorInt;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.k1;
import b.f.g.f.c;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.user.UserUtils;
import com.discord.views.UsernameView;
import com.discord.widgets.user.profile.DraweeSpanStringBuilderExtensionsKt;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import com.facebook.drawee.span.SimpleDraweeSpanTextView;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: SettingsMemberView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0017\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u0018"}, d2 = {"Lcom/discord/views/user/SettingsMemberView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Lcom/discord/models/user/User;", "user", "Lcom/discord/models/member/GuildMember;", "guildMember", "", "a", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)V", "", "backgroundColor", "setAvatarBackgroundColor", "(I)V", "Lb/a/i/k1;", "j", "Lb/a/i/k1;", "binding", "l", "Lcom/discord/models/user/User;", "m", "Lcom/discord/models/member/GuildMember;", "k", "I", "avatarBackgroundColor", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsMemberView extends ConstraintLayout {
    public final k1 j;
    public int k;
    public User l;
    public GuildMember m;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsMemberView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.settings_member_view, this);
        int i = R.id.large_avatar;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.large_avatar);
        if (simpleDraweeView != null) {
            i = R.id.member_subtitle;
            SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) findViewById(R.id.member_subtitle);
            if (simpleDraweeSpanTextView != null) {
                i = R.id.member_title;
                UsernameView usernameView = (UsernameView) findViewById(R.id.member_title);
                if (usernameView != null) {
                    k1 k1Var = new k1(this, simpleDraweeView, simpleDraweeSpanTextView, usernameView);
                    m.checkNotNullExpressionValue(k1Var, "SettingsMemberViewBindin…ater.from(context), this)");
                    this.j = k1Var;
                    this.k = ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundPrimary);
                    TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.SettingsMemberView);
                    m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…eable.SettingsMemberView)");
                    this.k = obtainStyledAttributes.getColor(0, ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundPrimary));
                    obtainStyledAttributes.recycle();
                    setAvatarBackgroundColor(this.k);
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    public final void a(User user, GuildMember guildMember) {
        m.checkNotNullParameter(user, "user");
        this.l = user;
        this.m = guildMember;
        String nick = guildMember != null ? guildMember.getNick() : null;
        int i = 0;
        boolean z2 = true;
        boolean z3 = !(nick == null || t.isBlank(nick));
        SimpleDraweeView simpleDraweeView = this.j.f142b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.largeAvatar");
        IconUtils.setIcon$default(simpleDraweeView, user, R.dimen.avatar_size_large, null, null, guildMember, 24, null);
        UsernameView usernameView = this.j.d;
        if (nick == null) {
            nick = user.getUsername();
        }
        UsernameView.c(usernameView, nick, null, false, null, null, 30);
        UsernameView usernameView2 = this.j.d;
        boolean isBot = user.isBot();
        int i2 = user.isSystemUser() ? R.string.system_dm_tag_system : R.string.bot_tag;
        UserUtils userUtils = UserUtils.INSTANCE;
        usernameView2.a(isBot, i2, userUtils.isVerifiedBot(user));
        boolean z4 = guildMember != null && guildMember.hasAvatar();
        DraweeSpanStringBuilder draweeSpanStringBuilder = new DraweeSpanStringBuilder();
        if (z4) {
            int dimension = (int) getResources().getDimension(R.dimen.avatar_size_profile_small);
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            DraweeSpanStringBuilderExtensionsKt.setAvatar$default(draweeSpanStringBuilder, context, IconUtils.getForUser$default(user, false, Integer.valueOf(dimension), 2, null), false, Integer.valueOf(dimension), Integer.valueOf(this.k), (char) 8194, 4, null);
        }
        draweeSpanStringBuilder.append(UserUtils.getUserNameWithDiscriminator$default(userUtils, user, null, null, 3, null));
        this.j.c.setDraweeSpanStringBuilder(draweeSpanStringBuilder);
        SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.j.c;
        m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.memberSubtitle");
        if (!z4 && !z3) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        simpleDraweeSpanTextView.setVisibility(i);
    }

    public final void setAvatarBackgroundColor(@ColorInt int i) {
        GuildMember guildMember;
        this.k = i;
        SimpleDraweeView simpleDraweeView = this.j.f142b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.largeAvatar");
        c a = c.a(simpleDraweeView.getWidth() / 2);
        m.checkNotNullExpressionValue(a, "roundingParams");
        a.f519b = true;
        a.d = i;
        a.a = 1;
        SimpleDraweeView simpleDraweeView2 = this.j.f142b;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.largeAvatar");
        GenericDraweeHierarchy hierarchy = simpleDraweeView2.getHierarchy();
        m.checkNotNullExpressionValue(hierarchy, "binding.largeAvatar.hierarchy");
        hierarchy.s(a);
        User user = this.l;
        if (user != null && (guildMember = this.m) != null) {
            a(user, guildMember);
        }
    }
}
