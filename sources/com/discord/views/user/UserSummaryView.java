package com.discord.views.user;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.b4;
import com.discord.R;
import com.discord.models.guild.UserGuildMember;
import com.discord.models.user.CoreUser;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.views.CutoutView;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.d0.f;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: UserSummaryView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0003\u0010\u0004J\u001b\u0010\b\u001a\u00020\u00022\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\r\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000f\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u0010\fR\u0016\u0010\u0013\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0015\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\fR\u001c\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u0018"}, d2 = {"Lcom/discord/views/user/UserSummaryView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "onFinishInflate", "()V", "", "Lcom/discord/models/guild/UserGuildMember;", "members", "setMembers", "(Ljava/util/List;)V", "", "l", "I", "overlapAmountPx", "k", "avatarSizePx", "Lcom/discord/views/CutoutView$a$c;", "j", "Lcom/discord/views/CutoutView$a$c;", "cutoutStyle", "n", "maxAvatars", "m", "Ljava/util/List;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserSummaryView extends ConstraintLayout {
    public int k;
    public int l;
    public int n;
    public final CutoutView.a.c j = new CutoutView.a.c(0);
    public List<UserGuildMember> m = n.emptyList();

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserSummaryView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        this.k = (int) (16 * resources.getDisplayMetrics().density);
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        this.l = (int) (4 * resources2.getDisplayMetrics().density);
        this.n = 3;
        int[] iArr = R.a.UserSummaryView;
        m.checkNotNullExpressionValue(iArr, "R.styleable.UserSummaryView");
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        this.k = obtainStyledAttributes.getDimensionPixelSize(1, this.k);
        this.l = obtainStyledAttributes.getDimensionPixelSize(0, this.l);
        this.n = obtainStyledAttributes.getInt(2, this.n);
        obtainStyledAttributes.recycle();
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode()) {
            setMembers(n.listOf((Object[]) new UserGuildMember[]{new UserGuildMember(new CoreUser(0L, "mreynolds", null, null, false, false, 0, null, 0, 0, null, null, 4092, null), null, 2, null), new UserGuildMember(new CoreUser(1L, "itskaylee", null, null, false, false, 0, null, 0, 0, null, null, 4092, null), null, 2, null)}));
        }
    }

    public final void setMembers(List<UserGuildMember> list) {
        int i;
        CutoutView.a aVar;
        SimpleDraweeView simpleDraweeView;
        m.checkNotNullParameter(list, "members");
        if (!m.areEqual(list, this.m)) {
            this.m = list;
            int coerceAtMost = f.coerceAtMost(list.size(), this.n);
            while (getChildCount() > coerceAtMost) {
                removeViewAt(getChildCount() - 1);
            }
            while (true) {
                i = 0;
                if (getChildCount() >= coerceAtMost) {
                    break;
                }
                int childCount = getChildCount();
                b4 a = b4.a(LayoutInflater.from(getContext()).inflate(xyz.discord.R.layout.view_user_summary_item, (ViewGroup) this, false));
                m.checkNotNullExpressionValue(a, "ViewUserSummaryItemBindi…ext), this, false\n      )");
                CutoutView cutoutView = a.a;
                cutoutView.setId(childCount + 10);
                int i2 = this.k;
                ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(i2, i2);
                layoutParams.topToTop = 0;
                layoutParams.bottomToBottom = 0;
                layoutParams.setMarginStart(childCount != 0 ? this.k - this.l : 0);
                if (childCount != 0) {
                    i = cutoutView.getId() - 1;
                }
                layoutParams.startToStart = i;
                cutoutView.setLayoutParams(layoutParams);
                m.checkNotNullExpressionValue(cutoutView, "ViewUserSummaryItemBindi…      }\n        }\n      }");
                addView(cutoutView, cutoutView.getLayoutParams());
            }
            int i3 = coerceAtMost - 1;
            while (i < coerceAtMost) {
                b4 a2 = b4.a(getChildAt(i));
                m.checkNotNullExpressionValue(a2, "ViewUserSummaryItemBinding.bind(getChildAt(i))");
                CutoutView cutoutView2 = a2.a;
                if (i == i3) {
                    aVar = CutoutView.a.d.a;
                } else {
                    aVar = this.j;
                }
                cutoutView2.setStyle(aVar);
                int mediaProxySize = IconUtils.getMediaProxySize(this.k);
                UserGuildMember userGuildMember = this.m.get(i);
                String forGuildMemberOrUser$default = IconUtils.getForGuildMemberOrUser$default(IconUtils.INSTANCE, userGuildMember.getUser(), userGuildMember.getGuildMember(), Integer.valueOf(mediaProxySize), false, 8, null);
                m.checkNotNullExpressionValue(a2.f85b, "cutout.avatar");
                if (!m.areEqual(forGuildMemberOrUser$default, simpleDraweeView.getTag())) {
                    SimpleDraweeView simpleDraweeView2 = a2.f85b;
                    m.checkNotNullExpressionValue(simpleDraweeView2, "cutout.avatar");
                    simpleDraweeView2.setTag(forGuildMemberOrUser$default);
                    SimpleDraweeView simpleDraweeView3 = a2.f85b;
                    m.checkNotNullExpressionValue(simpleDraweeView3, "cutout.avatar");
                    IconUtils.setIcon$default(simpleDraweeView3, forGuildMemberOrUser$default, (int) xyz.discord.R.dimen.avatar_size_reply, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
                }
                i++;
            }
        }
    }
}
