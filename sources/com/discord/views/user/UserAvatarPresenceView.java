package com.discord.views.user;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.ColorInt;
import b.a.i.t1;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.streams.StreamContext;
import com.discord.views.StatusView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: UserAvatarPresenceView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0005B\u0017\u0012\u0006\u0010#\u001a\u00020\"\u0012\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b&\u0010'J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\b\b\u0001\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ)\u0010\u000f\u001a\u00020\u00042\u001a\u0010\u000e\u001a\u0016\u0012\u0004\u0012\u00020\f\u0012\u0006\u0012\u0004\u0018\u00010\r\u0012\u0004\u0012\u00020\u00040\u000b¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u0007H\u0014¢\u0006\u0004\b\u0013\u0010\u0014R*\u0010\u000e\u001a\u0016\u0012\u0004\u0012\u00020\f\u0012\u0006\u0012\u0004\u0018\u00010\r\u0012\u0004\u0012\u00020\u00040\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u001a\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001e\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010!\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006("}, d2 = {"Lcom/discord/views/user/UserAvatarPresenceView;", "Landroid/widget/RelativeLayout;", "Lcom/discord/views/user/UserAvatarPresenceView$a;", "viewState", "", "a", "(Lcom/discord/views/user/UserAvatarPresenceView$a;)V", "", "backgroundColor", "setAvatarBackgroundColor", "(I)V", "Lkotlin/Function2;", "Landroid/graphics/Bitmap;", "", "onAvatarBitmapLoadedListener", "setOnAvatarBitmapLoadedListener", "(Lkotlin/jvm/functions/Function2;)V", "widthMeasureSpec", "heightMeasureSpec", "onMeasure", "(II)V", "m", "Lkotlin/jvm/functions/Function2;", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "k", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "imagesChangeDetector", "Lb/a/i/t1;", "j", "Lb/a/i/t1;", "binding", "l", "I", "cutoutSpacePx", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserAvatarPresenceView extends RelativeLayout {
    public final t1 j;
    public final MGImages.DistinctChangeDetector k;
    public int l;
    public Function2<? super Bitmap, ? super String, Unit> m;

    /* compiled from: UserAvatarPresenceView.kt */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function2<Bitmap, String, Unit> {
        public static final b j = new b();

        public b() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public Unit invoke(Bitmap bitmap, String str) {
            m.checkNotNullParameter(bitmap, "<anonymous parameter 0>");
            return Unit.a;
        }
    }

    /* compiled from: UserAvatarPresenceView.kt */
    /* loaded from: classes2.dex */
    public static final class c extends o implements Function1<ImageRequestBuilder, Unit> {
        public final /* synthetic */ String $iconUrl;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(String str) {
            super(1);
            this.$iconUrl = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(ImageRequestBuilder imageRequestBuilder) {
            ImageRequestBuilder imageRequestBuilder2 = imageRequestBuilder;
            m.checkNotNullParameter(imageRequestBuilder2, "imageRequestBuilder");
            imageRequestBuilder2.l = new b.a.y.t0.a(this);
            return Unit.a;
        }
    }

    /* compiled from: UserAvatarPresenceView.kt */
    /* loaded from: classes2.dex */
    public static final class d extends o implements Function1<ImageRequestBuilder, Unit> {
        public final /* synthetic */ String $iconUrl;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(String str) {
            super(1);
            this.$iconUrl = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(ImageRequestBuilder imageRequestBuilder) {
            ImageRequestBuilder imageRequestBuilder2 = imageRequestBuilder;
            m.checkNotNullParameter(imageRequestBuilder2, "imageRequestBuilder");
            imageRequestBuilder2.l = new b.a.y.t0.b(this);
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserAvatarPresenceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        View inflate = LayoutInflater.from(context).inflate(R.layout.user_avatar_presence_view, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.avatar;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.avatar);
        if (simpleDraweeView != null) {
            i = R.id.avatar_container;
            FrameLayout frameLayout = (FrameLayout) inflate.findViewById(R.id.avatar_container);
            if (frameLayout != null) {
                i = R.id.avatar_cutout;
                ImageView imageView = (ImageView) inflate.findViewById(R.id.avatar_cutout);
                if (imageView != null) {
                    i = R.id.static_avatar;
                    SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) inflate.findViewById(R.id.static_avatar);
                    if (simpleDraweeView2 != null) {
                        RelativeLayout relativeLayout = (RelativeLayout) inflate;
                        i = R.id.user_avatar_presence_status;
                        StatusView statusView = (StatusView) inflate.findViewById(R.id.user_avatar_presence_status);
                        if (statusView != null) {
                            t1 t1Var = new t1(relativeLayout, simpleDraweeView, frameLayout, imageView, simpleDraweeView2, relativeLayout, statusView);
                            m.checkNotNullExpressionValue(t1Var, "UserAvatarPresenceViewBi…rom(context), this, true)");
                            this.j = t1Var;
                            this.k = new MGImages.DistinctChangeDetector();
                            this.m = b.j;
                            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.UserAvatarPresenceView);
                            m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…e.UserAvatarPresenceView)");
                            int color = obtainStyledAttributes.getColor(0, ColorCompat.getThemedColor(this, (int) R.attr.primary_700));
                            this.l = obtainStyledAttributes.getDimensionPixelSize(1, 0);
                            obtainStyledAttributes.recycle();
                            setAvatarBackgroundColor(color);
                            m.checkNotNullExpressionValue(simpleDraweeView, "binding.avatar");
                            simpleDraweeView.setClipToOutline(true);
                            return;
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    /* JADX WARN: Removed duplicated region for block: B:43:0x00af  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x0100  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a(com.discord.views.user.UserAvatarPresenceView.a r21) {
        /*
            Method dump skipped, instructions count: 271
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.user.UserAvatarPresenceView.a(com.discord.views.user.UserAvatarPresenceView$a):void");
    }

    @Override // android.widget.RelativeLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int size = View.MeasureSpec.getSize(i);
        ImageView imageView = this.j.c;
        m.checkNotNullExpressionValue(imageView, "binding.avatarCutout");
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
        layoutParams.width = size;
        layoutParams.height = size;
        imageView.setLayoutParams(layoutParams);
        int i3 = size - (this.l * 2);
        SimpleDraweeView simpleDraweeView = this.j.f199b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.avatar");
        ViewGroup.LayoutParams layoutParams2 = simpleDraweeView.getLayoutParams();
        Objects.requireNonNull(layoutParams2, "null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
        layoutParams2.width = i3;
        layoutParams2.height = i3;
        simpleDraweeView.setLayoutParams(layoutParams2);
        float f = size;
        float f2 = 0.3375f * f;
        float f3 = 0.0375f * f;
        float f4 = f * 0.05f;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        float f5 = resources.getDisplayMetrics().density;
        float f6 = 12 * f5;
        if (f2 < f6) {
            float f7 = f6 / f2;
            f2 *= f7;
            f3 *= f7;
            f4 *= f7;
        }
        float f8 = f3;
        float max = Math.max(f3, f5 * 2);
        StatusView statusView = this.j.e;
        m.checkNotNullExpressionValue(statusView, "binding.userAvatarPresenceStatus");
        ViewGroup.LayoutParams layoutParams3 = statusView.getLayoutParams();
        layoutParams3.width = (int) f2;
        StatusView statusView2 = this.j.e;
        m.checkNotNullExpressionValue(statusView2, "binding.userAvatarPresenceStatus");
        statusView2.setLayoutParams(layoutParams3);
        StatusView statusView3 = this.j.e;
        m.checkNotNullExpressionValue(statusView3, "binding.userAvatarPresenceStatus");
        statusView3.setTranslationX(f8 - this.l);
        StatusView statusView4 = this.j.e;
        m.checkNotNullExpressionValue(statusView4, "binding.userAvatarPresenceStatus");
        statusView4.setTranslationY(f8 - this.l);
        this.j.e.setBorderWidth((int) max);
        this.j.e.setCornerRadius(f4);
    }

    public final void setAvatarBackgroundColor(@ColorInt int i) {
        this.j.e.setBackgroundColor(i);
        ImageView imageView = this.j.c;
        m.checkNotNullExpressionValue(imageView, "binding.avatarCutout");
        ColorCompatKt.tintWithColor(imageView, i);
    }

    public final void setOnAvatarBitmapLoadedListener(Function2<? super Bitmap, ? super String, Unit> function2) {
        m.checkNotNullParameter(function2, "onAvatarBitmapLoadedListener");
        this.m = function2;
    }

    /* compiled from: UserAvatarPresenceView.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final User a;

        /* renamed from: b  reason: collision with root package name */
        public final Presence f2818b;
        public final StreamContext c;
        public final boolean d;
        public final GuildMember e;

        public a(User user, Presence presence, StreamContext streamContext, boolean z2, GuildMember guildMember, int i) {
            int i2 = i & 16;
            m.checkNotNullParameter(user, "user");
            this.a = user;
            this.f2818b = presence;
            this.c = streamContext;
            this.d = z2;
            this.e = null;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return m.areEqual(this.a, aVar.a) && m.areEqual(this.f2818b, aVar.f2818b) && m.areEqual(this.c, aVar.c) && this.d == aVar.d && m.areEqual(this.e, aVar.e);
        }

        public int hashCode() {
            User user = this.a;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            Presence presence = this.f2818b;
            int hashCode2 = (hashCode + (presence != null ? presence.hashCode() : 0)) * 31;
            StreamContext streamContext = this.c;
            int hashCode3 = (hashCode2 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
            boolean z2 = this.d;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode3 + i2) * 31;
            GuildMember guildMember = this.e;
            if (guildMember != null) {
                i = guildMember.hashCode();
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ViewState(user=");
            R.append(this.a);
            R.append(", presence=");
            R.append(this.f2818b);
            R.append(", streamContext=");
            R.append(this.c);
            R.append(", showPresence=");
            R.append(this.d);
            R.append(", guildMember=");
            R.append(this.e);
            R.append(")");
            return R.toString();
        }

        public a(User user, Presence presence, StreamContext streamContext, boolean z2, GuildMember guildMember) {
            m.checkNotNullParameter(user, "user");
            this.a = user;
            this.f2818b = presence;
            this.c = streamContext;
            this.d = z2;
            this.e = guildMember;
        }
    }
}
