package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.AttrRes;
import androidx.annotation.DrawableRes;
import b.a.i.w1;
import com.discord.models.domain.ModelSubscription;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlaySku;
import com.discord.utilities.billing.GooglePlaySkuKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: ActiveSubscriptionView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0019J\u0083\u0001\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\n\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\u000b2\u0010\b\u0002\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\u0010\b\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\b\b\u0002\u0010\u0012\u001a\u00020\u0006¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/views/ActiveSubscriptionView;", "Landroid/widget/FrameLayout;", "Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;", "activeSubscriptionType", "Lcom/discord/models/domain/ModelSubscription$Status;", "status", "", "isTrialSubscription", "", "priceText", "isLoading", "", "guildBoostCount", "Lkotlin/Function0;", "", "topBtnCallback", "manageGuildBoostBtnCallback", "cancelBtnCallback", "isBasePlanMutatingToNonePlan", "a", "(Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;Lcom/discord/models/domain/ModelSubscription$Status;ZLjava/lang/CharSequence;ZILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Z)V", "Lb/a/i/w1;", "k", "Lb/a/i/w1;", "binding", "ActiveSubscriptionType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ActiveSubscriptionView extends FrameLayout {
    public static final /* synthetic */ int j = 0;
    public final w1 k;

    /* compiled from: ActiveSubscriptionView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u0017\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BO\b\u0002\u0012\b\b\u0001\u0010\u0011\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u000f\u001a\u00020\u0002\u0012\b\b\u0001\u0010\t\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u000b\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0002\u0012\b\b\u0001\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0004\u001a\u0004\b\n\u0010\u0006R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0004\u001a\u0004\b\f\u0010\u0006R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0004\u001a\u0004\b\u000e\u0010\u0006R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0004\u001a\u0004\b\u0010\u0010\u0006R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0004\u001a\u0004\b\u0012\u0010\u0006j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018¨\u0006\u0019"}, d2 = {"Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;", "", "", "headerImageError", "I", "getHeaderImageError", "()I", "headerBackgroundResub", "getHeaderBackgroundResub", "headerImageResub", "getHeaderImageResub", "headerBackground", "getHeaderBackground", "headerBackgroundError", "getHeaderBackgroundError", "headerImage", "getHeaderImage", "headerLogo", "getHeaderLogo", HookHelper.constructorName, "(Ljava/lang/String;IIIIIIII)V", "PREMIUM_CLASSIC", "PREMIUM", "PREMIUM_GUILD", "PREMIUM_AND_PREMIUM_GUILD", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ActiveSubscriptionType {
        PREMIUM_CLASSIC(R.attr.img_premium_classic_subscription_header_logo, R.drawable.img_premium_classic_subscription_header, R.drawable.img_premium_classic_subscription_header_resub, R.drawable.img_premium_classic_subscription_header_error, R.drawable.bg_premium_classic_subscription_header, R.drawable.bg_premium_subscription_header_resub, R.drawable.bg_premium_subscription_header_error),
        PREMIUM(R.attr.img_premium_subscription_header_logo, R.drawable.img_premium_subscription_header, R.drawable.img_premium_subscription_header_resub, R.drawable.img_premium_subscription_header_error, R.drawable.bg_premium_subscription_header, R.drawable.bg_premium_subscription_header_resub, R.drawable.bg_premium_subscription_header_error),
        PREMIUM_GUILD(R.attr.img_premium_guild_subscription_header_logo, R.drawable.img_premium_guild_subscription_header, R.drawable.img_premium_guild_subscription_header_resub, R.drawable.img_premium_guild_subscription_header_error, R.drawable.bg_premium_guild_subscription_header, R.drawable.bg_premium_guild_subscription_header_resub, R.drawable.bg_premium_guild_subscription_header_error),
        PREMIUM_AND_PREMIUM_GUILD(R.attr.img_logo_discord_nitro_and_boost_horizontal, R.drawable.img_premium_and_premium_guild_subscription_header, R.drawable.img_premium_and_premium_guild_subscription_header, R.drawable.img_premium_and_premium_guild_subscription_header, R.drawable.bg_premium_bundle_header, R.drawable.bg_premium_subscription_header_resub, R.drawable.bg_premium_subscription_header_error);
        
        private final int headerBackground;
        private final int headerBackgroundError;
        private final int headerBackgroundResub;
        private final int headerImage;
        private final int headerImageError;
        private final int headerImageResub;
        private final int headerLogo;

        ActiveSubscriptionType(@AttrRes int i, @DrawableRes int i2, @DrawableRes int i3, @DrawableRes int i4, @DrawableRes int i5, @DrawableRes int i6, @DrawableRes int i7) {
            this.headerLogo = i;
            this.headerImage = i2;
            this.headerImageResub = i3;
            this.headerImageError = i4;
            this.headerBackground = i5;
            this.headerBackgroundResub = i6;
            this.headerBackgroundError = i7;
        }

        public final int getHeaderBackground() {
            return this.headerBackground;
        }

        public final int getHeaderBackgroundError() {
            return this.headerBackgroundError;
        }

        public final int getHeaderBackgroundResub() {
            return this.headerBackgroundResub;
        }

        public final int getHeaderImage() {
            return this.headerImage;
        }

        public final int getHeaderImageError() {
            return this.headerImageError;
        }

        public final int getHeaderImageResub() {
            return this.headerImageResub;
        }

        public final int getHeaderLogo() {
            return this.headerLogo;
        }
    }

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                Function0 function0 = (Function0) this.k;
                if (function0 != null) {
                    Unit unit = (Unit) function0.invoke();
                }
            } else if (i == 1) {
                Function0 function02 = (Function0) this.k;
                if (function02 != null) {
                    Unit unit2 = (Unit) function02.invoke();
                }
            } else if (i == 2) {
                Function0 function03 = (Function0) this.k;
                if (function03 != null) {
                    Unit unit3 = (Unit) function03.invoke();
                }
            } else {
                throw null;
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ActiveSubscriptionView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_active_subscription, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.active_subscription_cancel_button;
        TextView textView = (TextView) inflate.findViewById(R.id.active_subscription_cancel_button);
        if (textView != null) {
            i = R.id.active_subscription_header_background;
            ImageView imageView = (ImageView) inflate.findViewById(R.id.active_subscription_header_background);
            if (imageView != null) {
                i = R.id.active_subscription_header_icon;
                ImageView imageView2 = (ImageView) inflate.findViewById(R.id.active_subscription_header_icon);
                if (imageView2 != null) {
                    i = R.id.active_subscription_header_logo;
                    ImageView imageView3 = (ImageView) inflate.findViewById(R.id.active_subscription_header_logo);
                    if (imageView3 != null) {
                        i = R.id.active_subscription_header_text;
                        TextView textView2 = (TextView) inflate.findViewById(R.id.active_subscription_header_text);
                        if (textView2 != null) {
                            i = R.id.active_subscription_manage_guild_boost_button;
                            MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.active_subscription_manage_guild_boost_button);
                            if (materialButton != null) {
                                i = R.id.active_subscription_progress;
                                ProgressBar progressBar = (ProgressBar) inflate.findViewById(R.id.active_subscription_progress);
                                if (progressBar != null) {
                                    i = R.id.active_subscription_top_button;
                                    MaterialButton materialButton2 = (MaterialButton) inflate.findViewById(R.id.active_subscription_top_button);
                                    if (materialButton2 != null) {
                                        w1 w1Var = new w1((FrameLayout) inflate, textView, imageView, imageView2, imageView3, textView2, materialButton, progressBar, materialButton2);
                                        m.checkNotNullExpressionValue(w1Var, "ViewActiveSubscriptionBi…rom(context), this, true)");
                                        this.k = w1Var;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public static final ActiveSubscriptionType b(ModelSubscription modelSubscription) {
        m.checkNotNullParameter(modelSubscription, Traits.Payment.Type.SUBSCRIPTION);
        if (modelSubscription.isGoogleSubscription()) {
            String paymentGatewayPlanId = modelSubscription.getPaymentGatewayPlanId();
            GooglePlaySku.Type type = null;
            GooglePlaySku fromSkuName = paymentGatewayPlanId != null ? GooglePlaySku.Companion.fromSkuName(paymentGatewayPlanId) : null;
            if (fromSkuName != null && GooglePlaySkuKt.isBundledSku(fromSkuName)) {
                return ActiveSubscriptionType.PREMIUM_AND_PREMIUM_GUILD;
            }
            if (fromSkuName != null) {
                type = fromSkuName.getType();
            }
            if (type == GooglePlaySku.Type.PREMIUM_GUILD) {
                return ActiveSubscriptionType.PREMIUM_GUILD;
            }
        }
        int ordinal = modelSubscription.getPlanType().ordinal();
        if (!(ordinal == 2 || ordinal == 3)) {
            switch (ordinal) {
                case 6:
                case 7:
                    break;
                case 8:
                case 9:
                    return ActiveSubscriptionType.PREMIUM_GUILD;
                default:
                    return ActiveSubscriptionType.PREMIUM_CLASSIC;
            }
        }
        return ActiveSubscriptionType.PREMIUM;
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0083  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0098  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00b3  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x0192  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x01d0  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x01d2  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01e2 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x01ef  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x01f1  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0201  */
    /* JADX WARN: Removed duplicated region for block: B:79:0x020b  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x0226  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x0228  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x022b  */
    /* JADX WARN: Removed duplicated region for block: B:86:0x022d  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x0249  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x024b  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x024e  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0250  */
    /* JADX WARN: Removed duplicated region for block: B:96:0x0279  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a(com.discord.views.ActiveSubscriptionView.ActiveSubscriptionType r17, com.discord.models.domain.ModelSubscription.Status r18, boolean r19, java.lang.CharSequence r20, boolean r21, int r22, kotlin.jvm.functions.Function0<kotlin.Unit> r23, kotlin.jvm.functions.Function0<kotlin.Unit> r24, kotlin.jvm.functions.Function0<kotlin.Unit> r25, boolean r26) {
        /*
            Method dump skipped, instructions count: 639
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.ActiveSubscriptionView.a(com.discord.views.ActiveSubscriptionView$ActiveSubscriptionType, com.discord.models.domain.ModelSubscription$Status, boolean, java.lang.CharSequence, boolean, int, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, boolean):void");
    }
}
