package com.discord.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.DimenRes;
import androidx.annotation.MainThread;
import androidx.annotation.Px;
import b.a.i.f4;
import b.a.y.j0;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.anim.RingAnimator;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: VoiceUserView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0005\u0010\tJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rJ!\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0003\u0010\u000f\u001a\u00020\u000eH\u0007¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u000eH\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0019\u0010\u0016\u001a\u00020\u00042\b\b\u0001\u0010\u0015\u001a\u00020\u000eH\u0007¢\u0006\u0004\b\u0016\u0010\u0014J\u0017\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\nH\u0007¢\u0006\u0004\b\u0018\u0010\rJ\u0017\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\nH\u0007¢\u0006\u0004\b\u001a\u0010\rJ)\u0010\u001f\u001a\u00020\u00042\u001a\u0010\u001e\u001a\u0016\u0012\u0004\u0012\u00020\u001c\u0012\u0006\u0012\u0004\u0018\u00010\u001d\u0012\u0004\u0012\u00020\u00040\u001b¢\u0006\u0004\b\u001f\u0010 J\u0015\u0010\"\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u000e¢\u0006\u0004\b\"\u0010\u0014R*\u0010\u001e\u001a\u0016\u0012\u0004\u0012\u00020\u001c\u0012\u0006\u0012\u0004\u0018\u00010\u001d\u0012\u0004\u0012\u00020\u00040\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\u0018\u0010'\u001a\u0004\u0018\u00010\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010\u0019\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010\u0017\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010+R\u0016\u0010/\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00103\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b4\u00105R\u0016\u00109\u001a\u0002068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108¨\u0006:"}, d2 = {"Lcom/discord/views/VoiceUserView;", "Landroid/widget/FrameLayout;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "voiceUser", "", "setVoiceState", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V", "Lcom/discord/views/VoiceUserView$a;", "displayVoiceState", "(Lcom/discord/views/VoiceUserView$a;)V", "", "selected", "setSelected", "(Z)V", "", "sizeDimenRes", "a", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;I)V", "avatarSize", "setAvatarSize", "(I)V", "size", "setRingMargin", "fadeWhenDisconnected", "setFadeWhenDisconnected", "animateAvatarWhenRinging", "setAnimateAvatarWhenRinging", "Lkotlin/Function2;", "Landroid/graphics/Bitmap;", "", "onBitmapLoadedListener", "setOnBitmapLoadedListener", "(Lkotlin/jvm/functions/Function2;)V", "resource", "setDefaultVoiceStateBackground", "p", "Lkotlin/jvm/functions/Function2;", "q", "Ljava/lang/String;", "prevAvatarUrl", "o", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "s", "Z", "r", "k", "I", "defaultVoiceStateBackground", "Lb/a/i/f4;", "l", "Lb/a/i/f4;", "binding", "m", "Lcom/discord/views/VoiceUserView$a;", "Lcom/discord/utilities/anim/RingAnimator;", "n", "Lcom/discord/utilities/anim/RingAnimator;", "ringAnimator", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceUserView extends FrameLayout {
    public static final /* synthetic */ int j = 0;
    public int k;
    public final f4 l;
    public a m;
    public final RingAnimator n;
    public StoreVoiceParticipants.VoiceUser o;
    public Function2<? super Bitmap, ? super String, Unit> p;
    public String q;
    public boolean r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f2808s;

    /* compiled from: VoiceUserView.kt */
    /* loaded from: classes2.dex */
    public enum a {
        CONNECTED,
        SPEAKING,
        MUTED,
        RINGING,
        DISCONNECTED
    }

    /* compiled from: VoiceUserView.kt */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function1<View, Unit> {
        public final /* synthetic */ int $avatarSize;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(int i) {
            super(1);
            this.$avatarSize = i;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(View view) {
            invoke2(view);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(View view) {
            m.checkNotNullParameter(view, "$this$resizeLayoutParams");
            if (view.getLayoutParams().width != this.$avatarSize || view.getLayoutParams().height != this.$avatarSize) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                int i = this.$avatarSize;
                layoutParams.width = i;
                layoutParams.height = i;
                view.setLayoutParams(layoutParams);
            }
        }
    }

    /* compiled from: VoiceUserView.kt */
    /* loaded from: classes2.dex */
    public static final class c extends o implements Function1<ImageRequestBuilder, Unit> {
        public final /* synthetic */ String $newAvatarUrl;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(String str) {
            super(1);
            this.$newAvatarUrl = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(ImageRequestBuilder imageRequestBuilder) {
            ImageRequestBuilder imageRequestBuilder2 = imageRequestBuilder;
            m.checkNotNullParameter(imageRequestBuilder2, "imageRequestBuilder");
            imageRequestBuilder2.l = new j0(this);
            return Unit.a;
        }
    }

    public VoiceUserView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public VoiceUserView(android.content.Context r3, android.util.AttributeSet r4, int r5, int r6) {
        /*
            r2 = this;
            r0 = r6 & 2
            if (r0 == 0) goto L5
            r4 = 0
        L5:
            r6 = r6 & 4
            r0 = 0
            if (r6 == 0) goto Lb
            r5 = 0
        Lb:
            java.lang.String r6 = "context"
            d0.z.d.m.checkNotNullParameter(r3, r6)
            r2.<init>(r3, r4, r5)
            r5 = 17170445(0x106000d, float:2.461195E-38)
            r2.k = r5
            android.view.LayoutInflater r5 = android.view.LayoutInflater.from(r3)
            r6 = 2131558879(0x7f0d01df, float:1.8743086E38)
            r5.inflate(r6, r2)
            r5 = 2131366082(0x7f0a10c2, float:1.8352047E38)
            android.view.View r6 = r2.findViewById(r5)
            com.facebook.drawee.view.SimpleDraweeView r6 = (com.facebook.drawee.view.SimpleDraweeView) r6
            if (r6 == 0) goto L80
            r5 = 2131366095(0x7f0a10cf, float:1.8352074E38)
            android.view.View r1 = r2.findViewById(r5)
            android.widget.TextView r1 = (android.widget.TextView) r1
            if (r1 == 0) goto L80
            b.a.i.f4 r5 = new b.a.i.f4
            r5.<init>(r2, r6, r1)
            java.lang.String r6 = "ViewVoiceUserBinding.inf…ater.from(context), this)"
            d0.z.d.m.checkNotNullExpressionValue(r5, r6)
            r2.l = r5
            com.discord.utilities.anim.RingAnimator r5 = new com.discord.utilities.anim.RingAnimator
            b.a.y.i0 r6 = new b.a.y.i0
            r6.<init>(r2)
            r5.<init>(r2, r6)
            r2.n = r5
            b.a.y.h0 r5 = b.a.y.h0.j
            r2.p = r5
            if (r4 == 0) goto L7f
            int[] r5 = com.discord.R.a.VoiceUserView
            android.content.res.TypedArray r3 = r3.obtainStyledAttributes(r4, r5, r0, r0)
            java.lang.String r4 = "context.obtainStyledAttr…able.VoiceUserView, 0, 0)"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
            r4 = 1
            r5 = 0
            float r4 = r3.getDimension(r4, r5)
            int r4 = (int) r4
            r5 = 2
            boolean r5 = r3.getBoolean(r5, r0)
            boolean r6 = r3.getBoolean(r0, r0)
            r3.recycle()
            if (r4 <= 0) goto L79
            r2.setAvatarSize(r4)
        L79:
            r2.setFadeWhenDisconnected(r5)
            r2.setAnimateAvatarWhenRinging(r6)
        L7f:
            return
        L80:
            android.content.res.Resources r3 = r2.getResources()
            java.lang.String r3 = r3.getResourceName(r5)
            java.lang.NullPointerException r4 = new java.lang.NullPointerException
            java.lang.String r5 = "Missing required view with ID: "
            java.lang.String r3 = r5.concat(r3)
            r4.<init>(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.views.VoiceUserView.<init>(android.content.Context, android.util.AttributeSet, int, int):void");
    }

    private final void setVoiceState(StoreVoiceParticipants.VoiceUser voiceUser) {
        a aVar;
        VoiceState voiceState = voiceUser.getVoiceState();
        boolean z2 = voiceState != null && (voiceState.h() || voiceState.e() || voiceState.l());
        if (voiceUser.isSpeaking()) {
            aVar = a.SPEAKING;
        } else if (voiceUser.isRinging()) {
            aVar = a.RINGING;
        } else if (!voiceUser.isConnected()) {
            aVar = a.DISCONNECTED;
        } else if (z2) {
            aVar = a.MUTED;
        } else {
            aVar = a.CONNECTED;
        }
        setVoiceState(aVar);
    }

    @MainThread
    public final void a(StoreVoiceParticipants.VoiceUser voiceUser, @DimenRes int i) {
        m.checkNotNullParameter(voiceUser, "voiceUser");
        if (!m.areEqual(this.o, voiceUser)) {
            this.o = voiceUser;
            int dimensionPixelSize = getResources().getDimensionPixelSize(i);
            User user = null;
            Integer valueOf = dimensionPixelSize > 0 ? Integer.valueOf(IconUtils.getMediaProxySize(dimensionPixelSize)) : null;
            GuildMember guildMember = voiceUser.getGuildMember();
            IconUtils iconUtils = IconUtils.INSTANCE;
            StoreVoiceParticipants.VoiceUser voiceUser2 = this.o;
            if (voiceUser2 != null) {
                user = voiceUser2.getUser();
            }
            String forGuildMemberOrUser$default = IconUtils.getForGuildMemberOrUser$default(iconUtils, user, guildMember, valueOf, false, 8, null);
            if (!m.areEqual(this.q, forGuildMemberOrUser$default)) {
                this.q = forGuildMemberOrUser$default;
                SimpleDraweeView simpleDraweeView = this.l.f111b;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.voiceUserAvatar");
                IconUtils.setIcon$default(simpleDraweeView, forGuildMemberOrUser$default, i, new c(forGuildMemberOrUser$default), (MGImages.ChangeDetector) null, 16, (Object) null);
            }
            TextView textView = this.l.c;
            m.checkNotNullExpressionValue(textView, "binding.voiceUserNameDisplay");
            textView.setText(voiceUser.getDisplayName());
            setVoiceState(voiceUser);
        }
    }

    @MainThread
    public final void setAnimateAvatarWhenRinging(boolean z2) {
        this.f2808s = z2;
    }

    @MainThread
    public final void setAvatarSize(int i) {
        b bVar = new b(i);
        SimpleDraweeView simpleDraweeView = this.l.f111b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.voiceUserAvatar");
        bVar.invoke2((View) simpleDraweeView);
        TextView textView = this.l.c;
        m.checkNotNullExpressionValue(textView, "binding.voiceUserNameDisplay");
        bVar.invoke2((View) textView);
        requestLayout();
    }

    public final void setDefaultVoiceStateBackground(int i) {
        this.k = i;
    }

    @MainThread
    public final void setFadeWhenDisconnected(boolean z2) {
        this.r = z2;
    }

    public final void setOnBitmapLoadedListener(Function2<? super Bitmap, ? super String, Unit> function2) {
        m.checkNotNullParameter(function2, "onBitmapLoadedListener");
        this.p = function2;
    }

    @MainThread
    public final void setRingMargin(@Px int i) {
        SimpleDraweeView simpleDraweeView = this.l.f111b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.voiceUserAvatar");
        SimpleDraweeView simpleDraweeView2 = this.l.f111b;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.voiceUserAvatar");
        ViewGroup.LayoutParams layoutParams = simpleDraweeView2.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
        layoutParams2.setMargins(i, i, i, i);
        simpleDraweeView.setLayoutParams(layoutParams2);
        requestLayout();
    }

    @Override // android.view.View
    public void setSelected(boolean z2) {
        boolean z3 = isSelected() != z2;
        super.setSelected(z2);
        if (z3) {
            ViewExtensions.fadeBy$default(this.l.c, isSelected(), 0L, 2, null);
        }
    }

    @MainThread
    public final void setVoiceUser(StoreVoiceParticipants.VoiceUser voiceUser) {
        a(voiceUser, R.dimen.avatar_size_unrestricted);
    }

    private final void setVoiceState(a aVar) {
        if (aVar != this.m) {
            this.m = aVar;
            setBackgroundResource(aVar.ordinal() != 1 ? this.k : R.drawable.drawable_voice_user_background_speaking);
            float f = (aVar != a.DISCONNECTED || !this.r) ? 1.0f : 0.3f;
            SimpleDraweeView simpleDraweeView = this.l.f111b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.voiceUserAvatar");
            simpleDraweeView.setAlpha(f);
            setAlpha(f);
            this.n.onUpdate();
        }
    }
}
