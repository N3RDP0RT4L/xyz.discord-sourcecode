package com.discord.views.typing;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.r1;
import b.a.y.s0.a;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: TypingDots.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\u0007\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\t\u0010\bR\u0016\u0010\f\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0014\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u0019"}, d2 = {"Lcom/discord/views/typing/TypingDots;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "isReplay", "", "a", "(Z)V", "b", "()V", "onDetachedFromWindow", "n", "Z", "isRunning", "", "m", "J", "dotsAnimationStaggerTimeMs", "Lb/a/i/r1;", "k", "Lb/a/i/r1;", "binding", "", "l", "I", "dotsAnimationTimeMs", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class TypingDots extends ConstraintLayout {
    public static final /* synthetic */ int j = 0;
    public final r1 k;
    public final int l;
    public final long m;
    public boolean n;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TypingDots(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.typing_dots_view, this);
        int i = R.id.view_typing_dots_1;
        TypingDot typingDot = (TypingDot) findViewById(R.id.view_typing_dots_1);
        if (typingDot != null) {
            i = R.id.view_typing_dots_2;
            TypingDot typingDot2 = (TypingDot) findViewById(R.id.view_typing_dots_2);
            if (typingDot2 != null) {
                i = R.id.view_typing_dots_3;
                TypingDot typingDot3 = (TypingDot) findViewById(R.id.view_typing_dots_3);
                if (typingDot3 != null) {
                    r1 r1Var = new r1(this, typingDot, typingDot2, typingDot3);
                    m.checkNotNullExpressionValue(r1Var, "TypingDotsViewBinding.in…ater.from(context), this)");
                    this.k = r1Var;
                    int integer = getResources().getInteger(R.integer.animation_time_standard);
                    this.l = integer;
                    this.m = (long) (integer / 1.5d);
                    typingDot3.setOnScaleDownCompleteListener(new a(this));
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    public final void a(boolean z2) {
        if (!(this.n && !z2)) {
            long j2 = z2 ? this.m : 0L;
            this.k.f185b.a(j2);
            this.k.c.a(this.m + j2);
            TypingDot typingDot = this.k.d;
            long j3 = this.m;
            typingDot.a(j2 + j3 + j3);
            this.n = true;
        }
    }

    public final void b() {
        r1 r1Var = this.k;
        for (TypingDot typingDot : n.listOf((Object[]) new TypingDot[]{r1Var.f185b, r1Var.c, r1Var.d})) {
            typingDot.j.cancel();
            typingDot.k.cancel();
        }
        this.n = false;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        b();
    }
}
