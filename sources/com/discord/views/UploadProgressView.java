package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import b.a.i.y3;
import b.d.b.a.a;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.view.extensions.ViewExtensions;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: UploadProgressView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ)\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\b2\b\b\u0001\u0010\u000b\u001a\u00020\u0004¢\u0006\u0004\b\f\u0010\rR\u0018\u0010\u0011\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0010R\u0016\u0010\u0017\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u001e"}, d2 = {"Lcom/discord/views/UploadProgressView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", "title", "", "progress", "", "subtitle", "", "a", "(Ljava/lang/CharSequence;ILjava/lang/String;)V", "drawableResId", "setIcon", "(I)V", "Landroid/graphics/drawable/Drawable;", "l", "Landroid/graphics/drawable/Drawable;", "drawableComplete", "m", "drawableInProgress", "Lb/a/i/y3;", "k", "Lb/a/i/y3;", "binding", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UploadProgressView extends ConstraintLayout {
    public static final /* synthetic */ int j = 0;
    public final y3 k;
    public final Drawable l;
    public final Drawable m;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UploadProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_upload_progress, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.progress_bar;
        ProgressBar progressBar = (ProgressBar) inflate.findViewById(R.id.progress_bar);
        if (progressBar != null) {
            i = R.id.progress_file_image;
            ImageView imageView = (ImageView) inflate.findViewById(R.id.progress_file_image);
            if (imageView != null) {
                i = R.id.progress_subtext;
                TextView textView = (TextView) inflate.findViewById(R.id.progress_subtext);
                if (textView != null) {
                    i = R.id.progress_text;
                    TextView textView2 = (TextView) inflate.findViewById(R.id.progress_text);
                    if (textView2 != null) {
                        y3 y3Var = new y3((ConstraintLayout) inflate, progressBar, imageView, textView, textView2);
                        m.checkNotNullExpressionValue(y3Var, "ViewUploadProgressBindin…rom(context), this, true)");
                        this.k = y3Var;
                        this.l = ContextCompat.getDrawable(getContext(), R.drawable.drawable_progress_green);
                        this.m = ContextCompat.getDrawable(getContext(), DrawableCompat.getThemedDrawableRes$default(this, (int) R.attr.progress_gradient, 0, 2, (Object) null));
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void a(CharSequence charSequence, int i, String str) {
        m.checkNotNullParameter(charSequence, "title");
        y3 y3Var = this.k;
        TextView textView = y3Var.e;
        m.checkNotNullExpressionValue(textView, "progressText");
        textView.setText(charSequence);
        boolean z2 = true;
        if (i >= 0) {
            ProgressBar progressBar = y3Var.f230b;
            m.checkNotNullExpressionValue(progressBar, "progressBar");
            progressBar.setIndeterminate(false);
            if (Build.VERSION.SDK_INT >= 24) {
                ProgressBar progressBar2 = y3Var.f230b;
                m.checkNotNullExpressionValue(progressBar2, "progressBar");
                if (i < progressBar2.getProgress()) {
                    z2 = false;
                }
                y3Var.f230b.setProgress(i, z2);
            } else {
                ProgressBar progressBar3 = y3Var.f230b;
                m.checkNotNullExpressionValue(progressBar3, "progressBar");
                progressBar3.setProgress(i);
            }
            ProgressBar progressBar4 = y3Var.f230b;
            m.checkNotNullExpressionValue(progressBar4, "progressBar");
            progressBar4.setProgressDrawable(i == 100 ? this.l : this.m);
            ProgressBar progressBar5 = y3Var.f230b;
            m.checkNotNullExpressionValue(progressBar5, "progressBar");
            progressBar5.setVisibility(0);
        } else if (i == -1) {
            ProgressBar progressBar6 = y3Var.f230b;
            m.checkNotNullExpressionValue(progressBar6, "progressBar");
            progressBar6.setVisibility(0);
            ProgressBar progressBar7 = y3Var.f230b;
            m.checkNotNullExpressionValue(progressBar7, "progressBar");
            progressBar7.setIndeterminate(true);
        } else if (i == -2) {
            ProgressBar progressBar8 = y3Var.f230b;
            m.checkNotNullExpressionValue(progressBar8, "progressBar");
            progressBar8.setVisibility(4);
        } else {
            throw new IllegalArgumentException(a.p("invalid argument supplied to progress: ", i));
        }
        TextView textView2 = y3Var.d;
        m.checkNotNullExpressionValue(textView2, "progressSubtext");
        ViewExtensions.setTextAndVisibilityBy(textView2, str);
    }

    public final void setIcon(@DrawableRes int i) {
        this.k.c.setImageResource(i);
    }
}
