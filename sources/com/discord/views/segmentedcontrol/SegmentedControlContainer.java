package com.discord.views.segmentedcontrol;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import androidx.core.view.ViewGroupKt;
import d0.t.n;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: SegmentedControlContainer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J%\u0010\n\u001a\u00020\u00042\u0016\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0007j\u0002`\b¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\f\u0010\u0006R*\u0010\u000f\u001a\u0016\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0007j\u0004\u0018\u0001`\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0016"}, d2 = {"Lcom/discord/views/segmentedcontrol/SegmentedControlContainer;", "Landroid/widget/LinearLayout;", "", "selectedIndex", "", "a", "(I)V", "Lkotlin/Function1;", "Lcom/discord/views/segmentedcontrol/SegmentSelectedChangeListener;", "listener", "setOnSegmentSelectedChangeListener", "(Lkotlin/jvm/functions/Function1;)V", "setSelectedIndex", "j", "Lkotlin/jvm/functions/Function1;", "segmentSelectedChangeListener", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SegmentedControlContainer extends LinearLayout {
    public Function1<? super Integer, Unit> j;

    /* compiled from: SegmentedControlContainer.kt */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ SegmentedControlContainer k;

        public a(int i, SegmentedControlContainer segmentedControlContainer) {
            this.j = i;
            this.k = segmentedControlContainer;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Function1<? super Integer, Unit> function1 = this.k.j;
            if (function1 != null) {
                function1.invoke(Integer.valueOf(this.j));
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SegmentedControlContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
    }

    public static /* synthetic */ void b(SegmentedControlContainer segmentedControlContainer, int i, int i2) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        segmentedControlContainer.a(i);
    }

    public final void a(int i) {
        int i2 = 0;
        for (View view : ViewGroupKt.getChildren(this)) {
            i2++;
            if (i2 < 0) {
                n.throwIndexOverflow();
            }
            View view2 = view;
            if (view2 instanceof b.a.y.o0.a) {
                view2.setOnClickListener(new a(i2, this));
            } else {
                throw new IllegalStateException("All children must be SegmentedControlSegments.".toString());
            }
        }
        setSelectedIndex(i);
    }

    public final void setOnSegmentSelectedChangeListener(Function1<? super Integer, Unit> function1) {
        m.checkNotNullParameter(function1, "listener");
        this.j = function1;
    }

    public final void setSelectedIndex(int i) {
        int i2 = 0;
        for (View view : ViewGroupKt.getChildren(this)) {
            i2++;
            if (i2 < 0) {
                n.throwIndexOverflow();
            }
            View view2 = view;
            if (view2 instanceof b.a.y.o0.a) {
                ((b.a.y.o0.a) view2).a(i2 == i);
            } else {
                throw new IllegalStateException("All children must be SegmentedControlSegments.".toString());
            }
        }
    }
}
