package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import b.a.i.o2;
import b.a.i.p2;
import b.a.y.r;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppComponent;
import com.discord.models.guild.Guild;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.voice.VoiceViewUtils;
import com.discord.widgets.voice.model.CallModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import xyz.discord.R;
/* compiled from: OverlayMenuView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002:\u0001 B\u001b\b\u0016\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010\u0004\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u0006\u0010\u0005R\u0016\u0010\n\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR(\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u000b8\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R(\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00140\u00138\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018¨\u0006!"}, d2 = {"Lcom/discord/views/OverlayMenuView;", "Landroid/widget/LinearLayout;", "Lcom/discord/app/AppComponent;", "", "onAttachedToWindow", "()V", "onDetachedFromWindow", "Lb/a/i/o2;", "k", "Lb/a/i/o2;", "binding", "Lkotlin/Function0;", "m", "Lkotlin/jvm/functions/Function0;", "getOnDismissRequested$app_productionGoogleRelease", "()Lkotlin/jvm/functions/Function0;", "setOnDismissRequested$app_productionGoogleRelease", "(Lkotlin/jvm/functions/Function0;)V", "onDismissRequested", "Lrx/subjects/Subject;", "Ljava/lang/Void;", "l", "Lrx/subjects/Subject;", "getUnsubscribeSignal", "()Lrx/subjects/Subject;", "unsubscribeSignal", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class OverlayMenuView extends LinearLayout implements AppComponent {
    public static final /* synthetic */ int j = 0;
    public final o2 k;
    public final Subject<Void, Void> l;
    public Function0<Unit> m;

    /* compiled from: OverlayMenuView.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Long a;

        /* renamed from: b  reason: collision with root package name */
        public final Guild f2805b;
        public final CallModel c;
        public final RtcConnection.Quality d;

        public a(Long l, Guild guild, CallModel callModel, RtcConnection.Quality quality) {
            m.checkNotNullParameter(quality, "rtcQuality");
            this.a = l;
            this.f2805b = guild;
            this.c = callModel;
            this.d = quality;
        }
    }

    /* compiled from: OverlayMenuView.kt */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function1<a, Unit> {
        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(a aVar) {
            ColorStateList colorStateList;
            a aVar2 = aVar;
            if (aVar2 != null) {
                OverlayMenuView overlayMenuView = OverlayMenuView.this;
                int i = OverlayMenuView.j;
                Objects.requireNonNull(overlayMenuView);
                if (aVar2.c != null) {
                    overlayMenuView.k.f167b.d.setOnClickListener(new f(0, overlayMenuView, aVar2));
                    TextView textView = overlayMenuView.k.f167b.d;
                    m.checkNotNullExpressionValue(textView, "binding.content.overlayInviteLink");
                    textView.setVisibility(PermissionUtils.can(1L, aVar2.a) ? 0 : 8);
                    overlayMenuView.k.f167b.g.setOnClickListener(new i(0, overlayMenuView));
                    overlayMenuView.k.f167b.f.setOnClickListener(new f(1, overlayMenuView, aVar2));
                    ImageView imageView = overlayMenuView.k.e;
                    m.checkNotNullExpressionValue(imageView, "binding.srcToggle");
                    if (aVar2.c.getAudioManagerState().getActiveAudioDevice() == DiscordAudioManager.DeviceTypes.SPEAKERPHONE) {
                        colorStateList = ColorStateList.valueOf(-1);
                    } else {
                        colorStateList = ColorStateList.valueOf(ColorCompat.getColor(overlayMenuView.getContext(), (int) R.color.primary_dark_400));
                    }
                    m.checkNotNullExpressionValue(colorStateList, "if (selectedOutputDevice…rimary_dark_400))\n      }");
                    imageView.setImageTintList(colorStateList);
                    overlayMenuView.k.e.setOnClickListener(new i(1, aVar2));
                    ImageView imageView2 = overlayMenuView.k.d;
                    m.checkNotNullExpressionValue(imageView2, "binding.muteToggle");
                    imageView2.setActivated(aVar2.c.isMeMutedByAnySource());
                    overlayMenuView.k.d.setOnClickListener(new i(2, aVar2));
                    overlayMenuView.k.c.setOnClickListener(new i(3, overlayMenuView));
                    overlayMenuView.k.f167b.e.setImageResource(VoiceViewUtils.INSTANCE.getQualityIndicator(aVar2.d));
                    TextView textView2 = overlayMenuView.k.f167b.c;
                    m.checkNotNullExpressionValue(textView2, "binding.content.overlayGuildName");
                    Guild guild = aVar2.f2805b;
                    String name = guild != null ? guild.getName() : null;
                    if (name == null) {
                        name = "";
                    }
                    textView2.setText(name);
                    TextView textView3 = overlayMenuView.k.f167b.f174b;
                    m.checkNotNullExpressionValue(textView3, "binding.content.overlayChannelName");
                    textView3.setText(ChannelUtils.c(aVar2.c.getChannel()));
                }
            }
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OverlayMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(getContext()).inflate(R.layout.view_overlay_menu, this);
        int i = R.id.content;
        View findViewById = findViewById(R.id.content);
        if (findViewById != null) {
            int i2 = R.id.overlay_channel_name;
            TextView textView = (TextView) findViewById.findViewById(R.id.overlay_channel_name);
            if (textView != null) {
                i2 = R.id.overlay_guild_name;
                TextView textView2 = (TextView) findViewById.findViewById(R.id.overlay_guild_name);
                if (textView2 != null) {
                    i2 = R.id.overlay_invite_link;
                    TextView textView3 = (TextView) findViewById.findViewById(R.id.overlay_invite_link);
                    if (textView3 != null) {
                        i2 = R.id.overlay_network_icon;
                        ImageView imageView = (ImageView) findViewById.findViewById(R.id.overlay_network_icon);
                        if (imageView != null) {
                            i2 = R.id.overlay_open_app;
                            TextView textView4 = (TextView) findViewById.findViewById(R.id.overlay_open_app);
                            if (textView4 != null) {
                                i2 = R.id.overlay_switch_channels;
                                TextView textView5 = (TextView) findViewById.findViewById(R.id.overlay_switch_channels);
                                if (textView5 != null) {
                                    p2 p2Var = new p2((CardView) findViewById, textView, textView2, textView3, imageView, textView4, textView5);
                                    i = R.id.disconnect_btn;
                                    ImageView imageView2 = (ImageView) findViewById(R.id.disconnect_btn);
                                    if (imageView2 != null) {
                                        i = R.id.mute_toggle;
                                        ImageView imageView3 = (ImageView) findViewById(R.id.mute_toggle);
                                        if (imageView3 != null) {
                                            i = R.id.src_toggle;
                                            ImageView imageView4 = (ImageView) findViewById(R.id.src_toggle);
                                            if (imageView4 != null) {
                                                o2 o2Var = new o2(this, p2Var, imageView2, imageView3, imageView4);
                                                m.checkNotNullExpressionValue(o2Var, "ViewOverlayMenuBinding.i…ater.from(context), this)");
                                                this.k = o2Var;
                                                PublishSubject k0 = PublishSubject.k0();
                                                m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
                                                this.l = k0;
                                                this.m = q.j;
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(i2)));
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    public final Function0<Unit> getOnDismissRequested$app_productionGoogleRelease() {
        return this.m;
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.l;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Observable<R> Y = StoreStream.Companion.getVoiceChannelSelected().observeSelectedChannel().Y(r.j);
        m.checkNotNullExpressionValue(Y, "StoreStream\n            …        }\n              }");
        Observable q = ObservableExtensionsKt.computationLatest(Y).q();
        m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), OverlayMenuView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new b());
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        getUnsubscribeSignal().onNext(null);
        super.onDetachedFromWindow();
    }

    public final void setOnDismissRequested$app_productionGoogleRelease(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.m = function0;
    }
}
