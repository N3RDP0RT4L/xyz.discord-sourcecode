package com.discord.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import androidx.core.content.ContextCompat;
import b.c.a.a0.d;
import com.discord.R;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.font.FontUtils;
import com.discord.views.CutoutView;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: NumericBadgingView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001*B\u001d\b\u0007\u0012\u0006\u0010%\u001a\u00020$\u0012\n\b\u0002\u0010'\u001a\u0004\u0018\u00010&¢\u0006\u0004\b(\u0010)J\u000f\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0014\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0016\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u000fR\u0016\u0010\u0019\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001d\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010!\u001a\u00020\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010#\u001a\u00020\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010 ¨\u0006+"}, d2 = {"Lcom/discord/views/NumericBadgingView;", "Lcom/discord/views/CutoutView;", "", "onFinishInflate", "()V", "Landroid/graphics/Canvas;", "canvas", "draw", "(Landroid/graphics/Canvas;)V", "", "number", "setBadgeNumber", "(I)V", "Landroid/graphics/Paint;", "v", "Landroid/graphics/Paint;", "badgePaint", "", "u", "Ljava/lang/String;", "badgeString", "w", "textPaint", "r", "I", "badgeTextPaddingVerticalPx", "Landroid/graphics/RectF;", "t", "Landroid/graphics/RectF;", "badgeRect", "", "q", "F", "badgeTextSizePx", "s", "badgeInsetSizePx", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NumericBadgingView extends CutoutView {
    public float q = DimenUtils.dpToPixels(12);
    public int r = DimenUtils.dpToPixels(8);

    /* renamed from: s  reason: collision with root package name */
    public float f2802s = DimenUtils.dpToPixels(4);
    public RectF t = new RectF();
    public String u = "";
    public final Paint v;
    public final Paint w;

    /* compiled from: NumericBadgingView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements CutoutView.a {
        public final RectF a;

        /* renamed from: b  reason: collision with root package name */
        public final float f2803b;

        public a(RectF rectF, float f) {
            m.checkNotNullParameter(rectF, "badgeRect");
            this.a = rectF;
            this.f2803b = f;
        }

        @Override // com.discord.views.CutoutView.a
        public Path a(Context context, int i, int i2) {
            m.checkNotNullParameter(context, "context");
            RectF rectF = new RectF(0.0f, 0.0f, this.a.width(), this.a.height());
            if (d.U0(context)) {
                rectF.offset(i - this.a.width(), 0.0f);
            }
            float f = this.f2803b;
            rectF.inset(-f, -f);
            Path path = new Path();
            float height = ((2 * this.f2803b) + this.a.height()) / 2.0f;
            path.addRoundRect(rectF, height, height, Path.Direction.CW);
            Path path2 = new Path();
            path2.addRect(0.0f, 0.0f, i, i2, Path.Direction.CW);
            Path path3 = new Path(path2);
            path3.op(path, Path.Op.DIFFERENCE);
            return path3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return m.areEqual(this.a, aVar.a) && Float.compare(this.f2803b, aVar.f2803b) == 0;
        }

        public int hashCode() {
            RectF rectF = this.a;
            return Float.floatToIntBits(this.f2803b) + ((rectF != null ? rectF.hashCode() : 0) * 31);
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("BadgeRectStyle(badgeRect=");
            R.append(this.a);
            R.append(", insetPx=");
            R.append(this.f2803b);
            R.append(")");
            return R.toString();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NumericBadgingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor((int) 4294901760L);
        this.v = paint;
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setTextAlign(Paint.Align.CENTER);
        paint2.setTextSize(this.q);
        paint2.setColor((int) 4294967295L);
        this.w = paint2;
        setWillNotDraw(false);
        int[] iArr = R.a.NumericBadgingView;
        m.checkNotNullExpressionValue(iArr, "R.styleable.NumericBadgingView");
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        paint.setColor(obtainStyledAttributes.getColor(0, 0));
        if (paint.getColor() == 0) {
            paint.setColor(ContextCompat.getColor(context, xyz.discord.R.color.status_red_500));
        }
        paint2.setColor(obtainStyledAttributes.getColor(1, 0));
        if (paint2.getColor() == 0) {
            paint2.setColor(ContextCompat.getColor(context, xyz.discord.R.color.white));
        }
        paint2.setTypeface(FontUtils.INSTANCE.getThemedFont(context, xyz.discord.R.attr.font_primary_semibold));
        obtainStyledAttributes.recycle();
    }

    @Override // com.discord.views.CutoutView, android.view.View
    public void draw(Canvas canvas) {
        float f;
        m.checkNotNullParameter(canvas, "canvas");
        super.draw(canvas);
        if (!(this.u.length() == 0)) {
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            if (d.U0(context)) {
                f = getMeasuredWidth() - (this.t.width() / 2.0f);
            } else {
                f = this.t.width() / 2.0f;
            }
            int save = canvas.save();
            canvas.translate(f, this.t.height() / 2.0f);
            try {
                float height = this.t.height() / 2.0f;
                canvas.drawRoundRect(this.t, height, height, this.v);
                canvas.drawText(this.u, 0.0f, -((this.w.descent() + this.w.ascent()) / 2.0f), this.w);
            } finally {
                canvas.restoreToCount(save);
            }
        }
    }

    @Override // com.discord.views.CutoutView, android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode()) {
            setBadgeNumber(42);
        }
    }

    public final void setBadgeNumber(int i) {
        if (i <= 0) {
            this.u = "";
            setStyle(CutoutView.a.d.a);
            return;
        }
        this.u = i >= 100 ? "99+" : String.valueOf(i);
        float textSize = this.w.getTextSize() + this.r;
        float f = textSize / 2.0f;
        float measureText = i >= 10 ? (this.w.measureText(this.u) + textSize) / 2.0f : f;
        this.t.set(-measureText, -f, measureText, f);
        setStyle(new a(this.t, this.f2802s));
        invalidate();
    }
}
