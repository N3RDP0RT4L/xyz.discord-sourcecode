package com.discord.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import b.a.i.y;
import b.a.k.b;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: IconRow.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u000f"}, d2 = {"Lcom/discord/views/IconRow;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "", "setText", "(Ljava/lang/String;)V", "", "imageRes", "setImageRes", "(Ljava/lang/Integer;)V", "Lb/a/i/y;", "j", "Lb/a/i/y;", "binding", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class IconRow extends ConstraintLayout {
    public final y j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public IconRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(R.layout.icon_row, this);
        int i = R.id.divider;
        View findViewById = findViewById(R.id.divider);
        if (findViewById != null) {
            i = R.id.image;
            ImageView imageView = (ImageView) findViewById(R.id.image);
            if (imageView != null) {
                i = R.id.text;
                TextView textView = (TextView) findViewById(R.id.text);
                if (textView != null) {
                    y yVar = new y(this, findViewById, imageView, textView);
                    m.checkNotNullExpressionValue(yVar, "IconRowBinding.inflate(L…ater.from(context), this)");
                    this.j = yVar;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i)));
    }

    public final void setImageRes(@DrawableRes Integer num) {
        if (num != null) {
            this.j.f226b.setImageResource(num.intValue());
        }
        ImageView imageView = this.j.f226b;
        m.checkNotNullExpressionValue(imageView, "binding.image");
        int i = 0;
        if (!(num == null || num.intValue() != 0)) {
            i = 8;
        }
        imageView.setVisibility(i);
    }

    public final void setText(String str) {
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        TextView textView = this.j.c;
        m.checkNotNullExpressionValue(textView, "binding.text");
        b.a(textView, str);
    }
}
