package com.discord.panels;

import andhook.lib.HookHelper;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PanelState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/panels/PanelState;", "", HookHelper.constructorName, "()V", "a", "b", "c", "d", "Lcom/discord/panels/PanelState$d;", "Lcom/discord/panels/PanelState$c;", "Lcom/discord/panels/PanelState$b;", "Lcom/discord/panels/PanelState$a;", "overlapping_panels_release"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public abstract class PanelState {

    /* compiled from: PanelState.kt */
    /* loaded from: classes.dex */
    public static final class a extends PanelState {
        public static final a a = new a();

        public a() {
            super(null);
        }
    }

    /* compiled from: PanelState.kt */
    /* loaded from: classes.dex */
    public static final class b extends PanelState {
        public static final b a = new b();

        public b() {
            super(null);
        }
    }

    /* compiled from: PanelState.kt */
    /* loaded from: classes.dex */
    public static final class c extends PanelState {
        public static final c a = new c();

        public c() {
            super(null);
        }
    }

    /* compiled from: PanelState.kt */
    /* loaded from: classes.dex */
    public static final class d extends PanelState {
        public static final d a = new d();

        public d() {
            super(null);
        }
    }

    public PanelState() {
    }

    public PanelState(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
