package com.discord.panels;

import andhook.lib.HookHelper;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import com.discord.panels.PanelState;
import d0.t.n;
import d0.z.d.h;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: OverlappingPanelsLayout.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\r\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\b\u0016\u0018\u0000 \u0098\u00012\u00020\u0001:\n\u0098\u0001\u0099\u0001\u009a\u0001\u009b\u0001\u009c\u0001B\u0015\b\u0016\u0012\b\u0010\u0092\u0001\u001a\u00030\u0091\u0001¢\u0006\u0006\b\u0093\u0001\u0010\u0094\u0001B!\b\u0016\u0012\b\u0010\u0092\u0001\u001a\u00030\u0091\u0001\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0006\b\u0093\u0001\u0010\u0095\u0001B,\b\u0016\u0012\b\u0010\u0092\u0001\u001a\u00030\u0091\u0001\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\t\b\u0002\u0010\u0096\u0001\u001a\u00020A¢\u0006\u0006\b\u0093\u0001\u0010\u0097\u0001J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\t\u0010\bJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u0010\u001a\u00020\u00042\b\b\u0002\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\u00042\b\b\u0002\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0012\u0010\u0011J\u0019\u0010\u0013\u001a\u00020\u00042\b\b\u0002\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0013\u0010\u0011J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\nH\u0002¢\u0006\u0004\b\u001a\u0010\u0019J\u000f\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001e\u0010\u001dJ\u0017\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u001f\u0010\u0017J\u0017\u0010\"\u001a\u00020 2\u0006\u0010!\u001a\u00020 H\u0002¢\u0006\u0004\b\"\u0010#J\u0017\u0010$\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b$\u0010%J\u0017\u0010&\u001a\u00020 2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b&\u0010'J\u001f\u0010)\u001a\u00020 2\u0006\u0010(\u001a\u00020 2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b)\u0010*J\u001f\u0010,\u001a\u00020 2\u0006\u0010+\u001a\u00020 2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b,\u0010*J+\u00100\u001a\u00020\u00042\u0006\u0010-\u001a\u00020 2\b\b\u0002\u0010\u000f\u001a\u00020\u000e2\b\b\u0002\u0010/\u001a\u00020.H\u0002¢\u0006\u0004\b0\u00101J\u0017\u00102\u001a\u00020\u00042\u0006\u0010-\u001a\u00020 H\u0002¢\u0006\u0004\b2\u00103J\u001f\u00105\u001a\u00020\u00042\u0006\u00104\u001a\u00020 2\u0006\u0010-\u001a\u00020 H\u0002¢\u0006\u0004\b5\u00106J\u001f\u00108\u001a\u0002072\u0006\u00104\u001a\u00020 2\u0006\u0010-\u001a\u00020 H\u0002¢\u0006\u0004\b8\u00109J\u001f\u0010:\u001a\u0002072\u0006\u00104\u001a\u00020 2\u0006\u0010-\u001a\u00020 H\u0002¢\u0006\u0004\b:\u00109J\u000f\u0010;\u001a\u00020\u0004H\u0002¢\u0006\u0004\b;\u0010\bJ\u0017\u0010<\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b<\u0010%J\u0017\u0010=\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b=\u0010%J\u000f\u0010>\u001a\u00020\u0004H\u0002¢\u0006\u0004\b>\u0010\bJ\u000f\u0010?\u001a\u00020\u0004H\u0002¢\u0006\u0004\b?\u0010\bJ7\u0010F\u001a\u00020\u00042\u0006\u0010@\u001a\u00020\u000e2\u0006\u0010B\u001a\u00020A2\u0006\u0010C\u001a\u00020A2\u0006\u0010D\u001a\u00020A2\u0006\u0010E\u001a\u00020AH\u0014¢\u0006\u0004\bF\u0010GJ\u0017\u0010H\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\bH\u0010%J\u0017\u0010I\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\bI\u0010%J\r\u0010\u0010\u001a\u00020\u0004¢\u0006\u0004\b\u0010\u0010\bJ\r\u0010\u0012\u001a\u00020\u0004¢\u0006\u0004\b\u0012\u0010\bJ\r\u0010\u0013\u001a\u00020\u0004¢\u0006\u0004\b\u0013\u0010\bJ\u0015\u0010K\u001a\u00020\u00042\u0006\u0010J\u001a\u00020\u001b¢\u0006\u0004\bK\u0010LJ\u0015\u0010M\u001a\u00020\u00042\u0006\u0010J\u001a\u00020\u001b¢\u0006\u0004\bM\u0010LJ\u0015\u0010O\u001a\u00020\u00042\u0006\u0010N\u001a\u00020\u000e¢\u0006\u0004\bO\u0010\u0011J\u001b\u0010S\u001a\u00020\u00042\f\u0010R\u001a\b\u0012\u0004\u0012\u00020Q0P¢\u0006\u0004\bS\u0010TJ!\u0010X\u001a\u00020\u00042\u0012\u0010W\u001a\n\u0012\u0006\b\u0001\u0012\u00020V0U\"\u00020V¢\u0006\u0004\bX\u0010YJ!\u0010Z\u001a\u00020\u00042\u0012\u0010W\u001a\n\u0012\u0006\b\u0001\u0012\u00020V0U\"\u00020V¢\u0006\u0004\bZ\u0010YJ\r\u0010[\u001a\u00020\n¢\u0006\u0004\b[\u0010\u0019J\u0015\u0010]\u001a\u00020\u00042\u0006\u0010\\\u001a\u000207¢\u0006\u0004\b]\u0010^J\u0015\u0010`\u001a\u00020\u00042\u0006\u0010_\u001a\u000207¢\u0006\u0004\b`\u0010^R\u0016\u0010a\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\ba\u0010bR\u0016\u0010c\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bc\u0010dR\u0016\u0010f\u001a\u00020e8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bf\u0010gR\u0016\u0010h\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bh\u0010bR&\u0010k\u001a\u0012\u0012\u0004\u0012\u00020V0ij\b\u0012\u0004\u0012\u00020V`j8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bk\u0010lR\u0016\u0010_\u001a\u0002078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b_\u0010mR\u0016\u0010n\u001a\u00020\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bn\u0010oR\u0016\u0010p\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bp\u0010dR\u0016\u0010q\u001a\u00020\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bq\u0010oR\u0018\u0010s\u001a\u0004\u0018\u00010r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bs\u0010tR\u0016\u0010u\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bu\u0010bR\u0016\u0010v\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bv\u0010dR\u0016\u0010w\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bw\u0010bR\u0016\u0010x\u001a\u00020e8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bx\u0010gR\u0016\u0010y\u001a\u00020A8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\by\u0010zR\u0016\u0010{\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b{\u0010dR\u0016\u0010|\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b|\u0010dR\u0016\u0010}\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b}\u0010dR\u0016\u0010\\\u001a\u0002078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\\\u0010mR\u0016\u0010~\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b~\u0010dR\u0016\u0010\u007f\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u007f\u0010dR\u0019\u0010\u0080\u0001\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0080\u0001\u0010\u0081\u0001R\u0018\u0010\u0082\u0001\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0082\u0001\u0010bR\u0018\u0010\u0083\u0001\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0083\u0001\u0010bR\"\u0010\u0085\u0001\u001a\u000b\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0084\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0085\u0001\u0010\u0086\u0001R\u0018\u0010\u0087\u0001\u001a\u00020e8\u0002@\u0002X\u0082.¢\u0006\u0007\n\u0005\b\u0087\u0001\u0010gR\u001c\u0010\u0089\u0001\u001a\u0005\u0018\u00010\u0088\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0089\u0001\u0010\u008a\u0001R\u0018\u0010\u008b\u0001\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u008b\u0001\u0010dR(\u0010\u008c\u0001\u001a\u0012\u0012\u0004\u0012\u00020V0ij\b\u0012\u0004\u0012\u00020V`j8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u008c\u0001\u0010lR\u001d\u0010R\u001a\b\u0012\u0004\u0012\u00020Q0P8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bR\u0010\u008d\u0001R\u001c\u0010\u008f\u0001\u001a\u0005\u0018\u00010\u008e\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008f\u0001\u0010\u0090\u0001¨\u0006\u009d\u0001"}, d2 = {"Lcom/discord/panels/OverlappingPanelsLayout;", "Landroid/widget/FrameLayout;", "Landroid/util/AttributeSet;", "attrs", "", "initialize", "(Landroid/util/AttributeSet;)V", "resetStartPanelWidth", "()V", "resetEndPanelWidth", "Lcom/discord/panels/OverlappingPanelsLayout$Panel;", "panel", "openPanel", "(Lcom/discord/panels/OverlappingPanelsLayout$Panel;)V", "", "isFling", "openStartPanel", "(Z)V", "openEndPanel", "closePanels", "Landroid/view/MotionEvent;", "event", "snapOpenOrClose", "(Landroid/view/MotionEvent;)V", "getLeftPanel", "()Lcom/discord/panels/OverlappingPanelsLayout$Panel;", "getRightPanel", "Lcom/discord/panels/OverlappingPanelsLayout$LockState;", "getLeftPanelLockState", "()Lcom/discord/panels/OverlappingPanelsLayout$LockState;", "getRightPanelLockState", "translateCenterPanel", "", "targetedX", "getNormalizedX", "(F)F", "shouldHandleActionMoveEvent", "(Landroid/view/MotionEvent;)Z", "getTargetedX", "(Landroid/view/MotionEvent;)F", "startX", "calculateDistanceX", "(FLandroid/view/MotionEvent;)F", "startY", "calculateDistanceY", "x", "", "animationDurationMs", "updateCenterPanelXWithAnimation", "(FZJ)V", "updateCenterPanelX", "(F)V", "previousX", "handleCenterPanelX", "(FF)V", "Lcom/discord/panels/PanelState;", "getStartPanelState", "(FF)Lcom/discord/panels/PanelState;", "getEndPanelState", "initPanels", "isTouchingCenterPanelWhileSidePanelOpen", "isTouchingChildGestureRegion", "handleStartPanelWidthUpdate", "handleEndPanelWidthUpdate", "changed", "", "left", "top", "right", "bottom", "onLayout", "(ZIIII)V", "onInterceptTouchEvent", "onTouchEvent", "lockState", "setStartPanelLockState", "(Lcom/discord/panels/OverlappingPanelsLayout$LockState;)V", "setEndPanelLockState", "useFullPortraitWidth", "setStartPanelUseFullPortraitWidth", "", "Landroid/graphics/Rect;", "childGestureRegions", "setChildGestureRegions", "(Ljava/util/List;)V", "", "Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;", "panelStateListenerArgs", "registerStartPanelStateListeners", "([Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;)V", "registerEndPanelStateListeners", "getSelectedPanel", "startPanelState", "handleStartPanelState", "(Lcom/discord/panels/PanelState;)V", "endPanelState", "handleEndPanelState", "isSystemGestureNavigationPossible", "Z", "minFlingPxPerSecond", "F", "Landroid/view/View;", "centerPanel", "Landroid/view/View;", "isScrollingHorizontally", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "startPanelStateListeners", "Ljava/util/ArrayList;", "Lcom/discord/panels/PanelState;", "endPanelLockState", "Lcom/discord/panels/OverlappingPanelsLayout$LockState;", "centerPanelAnimationEndX", "startPanelLockState", "Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;", "swipeDirection", "Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;", "useFullWidthForStartPanel", "homeGestureFromBottomThreshold", "isLeftToRight", "startPanel", "nonFullScreenSidePanelWidth", "I", "startPanelOpenedCenterPanelX", "scrollingSlopPx", "yFromInterceptActionDown", "centerPanelDiffX", "xFromInterceptActionDown", "selectedPanel", "Lcom/discord/panels/OverlappingPanelsLayout$Panel;", "wasActionDownOnClosedCenterPanel", "isHomeSystemGesture", "Lkotlin/Function0;", "pendingUpdate", "Lkotlin/jvm/functions/Function0;", "endPanel", "Landroid/animation/ValueAnimator;", "centerPanelXAnimator", "Landroid/animation/ValueAnimator;", "endPanelOpenedCenterPanelX", "endPanelStateListeners", "Ljava/util/List;", "Landroid/view/VelocityTracker;", "velocityTracker", "Landroid/view/VelocityTracker;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "Companion", "LockState", "Panel", "PanelStateListener", "SwipeDirection", "overlapping_panels_release"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public class OverlappingPanelsLayout extends FrameLayout {
    public static final Companion Companion = new Companion(null);
    private static final long SIDE_PANEL_CLOSE_DURATION_MS = 200;
    private static final long SIDE_PANEL_OPEN_DURATION_MS = 250;
    private HashMap _$_findViewCache;
    private View centerPanel;
    private float centerPanelAnimationEndX;
    private float centerPanelDiffX;
    private ValueAnimator centerPanelXAnimator;
    private List<Rect> childGestureRegions;
    private View endPanel;
    private LockState endPanelLockState;
    private float endPanelOpenedCenterPanelX;
    private PanelState endPanelState;
    private final ArrayList<PanelStateListener> endPanelStateListeners;
    private float homeGestureFromBottomThreshold;
    private boolean isHomeSystemGesture;
    private boolean isLeftToRight;
    private boolean isScrollingHorizontally;
    private final boolean isSystemGestureNavigationPossible;
    private float minFlingPxPerSecond;
    private int nonFullScreenSidePanelWidth;
    private Function0<Unit> pendingUpdate;
    private float scrollingSlopPx;
    private Panel selectedPanel;
    private View startPanel;
    private LockState startPanelLockState;
    private float startPanelOpenedCenterPanelX;
    private PanelState startPanelState;
    private final ArrayList<PanelStateListener> startPanelStateListeners;
    private SwipeDirection swipeDirection;
    private boolean useFullWidthForStartPanel;
    private VelocityTracker velocityTracker;
    private boolean wasActionDownOnClosedCenterPanel;
    private float xFromInterceptActionDown;
    private float yFromInterceptActionDown;

    /* compiled from: OverlappingPanelsLayout.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/panels/OverlappingPanelsLayout$Companion;", "", "", "SIDE_PANEL_CLOSE_DURATION_MS", "J", "SIDE_PANEL_OPEN_DURATION_MS", HookHelper.constructorName, "()V", "overlapping_panels_release"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: OverlappingPanelsLayout.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/panels/OverlappingPanelsLayout$LockState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "OPEN", "CLOSE", "UNLOCKED", "overlapping_panels_release"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public enum LockState {
        OPEN,
        CLOSE,
        UNLOCKED
    }

    /* compiled from: OverlappingPanelsLayout.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/panels/OverlappingPanelsLayout$Panel;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "START", "CENTER", "END", "overlapping_panels_release"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public enum Panel {
        START,
        CENTER,
        END
    }

    /* compiled from: OverlappingPanelsLayout.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;", "", "Lcom/discord/panels/PanelState;", "panelState", "", "onPanelStateChange", "(Lcom/discord/panels/PanelState;)V", "overlapping_panels_release"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public interface PanelStateListener {
        void onPanelStateChange(PanelState panelState);
    }

    /* compiled from: OverlappingPanelsLayout.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "LEFT", "RIGHT", "overlapping_panels_release"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public enum SwipeDirection {
        LEFT,
        RIGHT
    }

    /* compiled from: java-style lambda group */
    /* loaded from: classes.dex */
    public static final class a implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ int a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Object f2730b;

        public a(int i, Object obj) {
            this.a = i;
            this.f2730b = obj;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            int i = this.a;
            if (i == 0) {
                OverlappingPanelsLayout overlappingPanelsLayout = (OverlappingPanelsLayout) this.f2730b;
                m.checkExpressionValueIsNotNull(valueAnimator, "animator");
                Object animatedValue = valueAnimator.getAnimatedValue();
                if (animatedValue != null) {
                    overlappingPanelsLayout.updateCenterPanelX(((Float) animatedValue).floatValue());
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            } else if (i == 1) {
                OverlappingPanelsLayout overlappingPanelsLayout2 = (OverlappingPanelsLayout) this.f2730b;
                m.checkExpressionValueIsNotNull(valueAnimator, "animator");
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    overlappingPanelsLayout2.updateCenterPanelX(((Float) animatedValue2).floatValue());
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            } else {
                throw null;
            }
        }
    }

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class b implements View.OnLayoutChangeListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public b(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnLayoutChangeListener
        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            int i9 = this.j;
            if (i9 != 0) {
                if (i9 != 1) {
                    throw null;
                } else if (((OverlappingPanelsLayout) this.k).isLeftToRight && i != i5) {
                    ((OverlappingPanelsLayout) this.k).handleEndPanelWidthUpdate();
                } else if (!((OverlappingPanelsLayout) this.k).isLeftToRight && i3 != i7) {
                    ((OverlappingPanelsLayout) this.k).handleEndPanelWidthUpdate();
                }
            } else if (((OverlappingPanelsLayout) this.k).isLeftToRight && i3 != i7) {
                ((OverlappingPanelsLayout) this.k).handleStartPanelWidthUpdate();
            } else if (!((OverlappingPanelsLayout) this.k).isLeftToRight && i != i5) {
                ((OverlappingPanelsLayout) this.k).handleStartPanelWidthUpdate();
            }
        }
    }

    /* compiled from: OverlappingPanelsLayout.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function0<Unit> {
        public final /* synthetic */ boolean $isFling;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(boolean z2) {
            super(0);
            this.$isFling = z2;
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            OverlappingPanelsLayout.this.closePanels(this.$isFling);
            return Unit.a;
        }
    }

    /* compiled from: OverlappingPanelsLayout.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function0<Unit> {
        public final /* synthetic */ boolean $isFling;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(boolean z2) {
            super(0);
            this.$isFling = z2;
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            OverlappingPanelsLayout.this.openEndPanel(this.$isFling);
            return Unit.a;
        }
    }

    /* compiled from: OverlappingPanelsLayout.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function0<Unit> {
        public final /* synthetic */ boolean $isFling;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(boolean z2) {
            super(0);
            this.$isFling = z2;
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            OverlappingPanelsLayout.this.openStartPanel(this.$isFling);
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OverlappingPanelsLayout(Context context) {
        super(context);
        m.checkParameterIsNotNull(context, "context");
        boolean z2 = true;
        this.isLeftToRight = true;
        h hVar = h.a;
        this.startPanelOpenedCenterPanelX = hVar.getMIN_VALUE();
        this.endPanelOpenedCenterPanelX = hVar.getMAX_VALUE();
        this.startPanelStateListeners = new ArrayList<>();
        this.endPanelStateListeners = new ArrayList<>();
        this.selectedPanel = Panel.CENTER;
        LockState lockState = LockState.UNLOCKED;
        this.startPanelLockState = lockState;
        this.endPanelLockState = lockState;
        PanelState.a aVar = PanelState.a.a;
        this.startPanelState = aVar;
        this.endPanelState = aVar;
        this.childGestureRegions = n.emptyList();
        this.isSystemGestureNavigationPossible = Build.VERSION.SDK_INT < 29 ? false : z2;
    }

    public static final /* synthetic */ View access$getCenterPanel$p(OverlappingPanelsLayout overlappingPanelsLayout) {
        View view = overlappingPanelsLayout.centerPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        return view;
    }

    public static final /* synthetic */ View access$getStartPanel$p(OverlappingPanelsLayout overlappingPanelsLayout) {
        View view = overlappingPanelsLayout.startPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("startPanel");
        }
        return view;
    }

    private final float calculateDistanceX(float f, MotionEvent motionEvent) {
        return motionEvent.getX() - f;
    }

    private final float calculateDistanceY(float f, MotionEvent motionEvent) {
        return motionEvent.getY() - f;
    }

    public static /* synthetic */ void closePanels$default(OverlappingPanelsLayout overlappingPanelsLayout, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z2 = false;
            }
            overlappingPanelsLayout.closePanels(z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: closePanels");
    }

    private final PanelState getEndPanelState(float f, float f2) {
        boolean z2 = this.isLeftToRight;
        if (z2 && f2 >= 0.0f) {
            return PanelState.a.a;
        }
        if (!z2 && f2 <= 0.0f) {
            return PanelState.a.a;
        }
        if (f2 == this.endPanelOpenedCenterPanelX) {
            return PanelState.c.a;
        }
        if (z2 && f2 < f) {
            return PanelState.d.a;
        }
        if (z2 || f2 <= f) {
            return PanelState.b.a;
        }
        return PanelState.d.a;
    }

    private final Panel getLeftPanel() {
        return this.isLeftToRight ? Panel.START : Panel.END;
    }

    private final LockState getLeftPanelLockState() {
        return this.isLeftToRight ? this.startPanelLockState : this.endPanelLockState;
    }

    private final float getNormalizedX(float f) {
        LockState lockState = this.startPanelLockState;
        LockState lockState2 = LockState.OPEN;
        if (lockState == lockState2) {
            return this.startPanelOpenedCenterPanelX;
        }
        if (this.endPanelLockState == lockState2) {
            return this.endPanelOpenedCenterPanelX;
        }
        LockState leftPanelLockState = getLeftPanelLockState();
        LockState lockState3 = LockState.CLOSE;
        float f2 = 0.0f;
        float max = (leftPanelLockState == lockState3 || (this.selectedPanel == Panel.CENTER && this.swipeDirection == SwipeDirection.LEFT)) ? 0.0f : Math.max(this.startPanelOpenedCenterPanelX, this.endPanelOpenedCenterPanelX);
        if (!(getRightPanelLockState() == lockState3 || (this.selectedPanel == Panel.CENTER && this.swipeDirection == SwipeDirection.RIGHT))) {
            f2 = Math.min(this.startPanelOpenedCenterPanelX, this.endPanelOpenedCenterPanelX);
        }
        return f > max ? max : f < f2 ? f2 : f;
    }

    private final Panel getRightPanel() {
        return this.isLeftToRight ? Panel.END : Panel.START;
    }

    private final LockState getRightPanelLockState() {
        return this.isLeftToRight ? this.endPanelLockState : this.startPanelLockState;
    }

    private final PanelState getStartPanelState(float f, float f2) {
        boolean z2 = this.isLeftToRight;
        if (z2 && f2 <= 0.0f) {
            return PanelState.a.a;
        }
        if (!z2 && f2 >= 0.0f) {
            return PanelState.a.a;
        }
        if (f2 == this.startPanelOpenedCenterPanelX) {
            return PanelState.c.a;
        }
        if (z2 && f2 > f) {
            return PanelState.d.a;
        }
        if (z2 || f2 >= f) {
            return PanelState.b.a;
        }
        return PanelState.d.a;
    }

    private final float getTargetedX(MotionEvent motionEvent) {
        return motionEvent.getRawX() + this.centerPanelDiffX;
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0020, code lost:
        if (r1.getX() <= 0) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0034, code lost:
        if (r1.getX() < 0) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0036, code lost:
        r1 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x0057, code lost:
        if (r1.getX() >= 0) goto L31;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x006b, code lost:
        if (r1.getX() > 0) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x006d, code lost:
        r1 = 0;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void handleCenterPanelX(float r12, float r13) {
        /*
            Method dump skipped, instructions count: 289
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.panels.OverlappingPanelsLayout.handleCenterPanelX(float, float):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEndPanelWidthUpdate() {
        float f = this.endPanelOpenedCenterPanelX;
        float dimension = getResources().getDimension(R.a.overlapping_panels_margin_between_panels);
        View view = this.endPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("endPanel");
        }
        float f2 = -(view.getWidth() + dimension);
        this.endPanelOpenedCenterPanelX = f2;
        if (!this.isLeftToRight) {
            f2 = -f2;
        }
        this.endPanelOpenedCenterPanelX = f2;
        View view2 = this.centerPanel;
        if (view2 == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        if (view2.getX() == f || this.centerPanelAnimationEndX == f) {
            openEndPanel();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStartPanelWidthUpdate() {
        float f = this.startPanelOpenedCenterPanelX;
        float dimension = getResources().getDimension(R.a.overlapping_panels_margin_between_panels);
        View view = this.startPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("startPanel");
        }
        float width = view.getWidth() + dimension;
        this.startPanelOpenedCenterPanelX = width;
        if (!this.isLeftToRight) {
            width = -width;
        }
        this.startPanelOpenedCenterPanelX = width;
        View view2 = this.centerPanel;
        if (view2 == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        if (view2.getX() == f || this.centerPanelAnimationEndX == f) {
            openStartPanel();
        }
    }

    private final void initPanels() {
        View childAt = getChildAt(0);
        m.checkExpressionValueIsNotNull(childAt, "getChildAt(0)");
        this.startPanel = childAt;
        View childAt2 = getChildAt(1);
        m.checkExpressionValueIsNotNull(childAt2, "getChildAt(1)");
        this.centerPanel = childAt2;
        View childAt3 = getChildAt(2);
        m.checkExpressionValueIsNotNull(childAt3, "getChildAt(2)");
        this.endPanel = childAt3;
        View view = this.startPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("startPanel");
        }
        view.setVisibility(4);
        View view2 = this.startPanel;
        if (view2 == null) {
            m.throwUninitializedPropertyAccessException("startPanel");
        }
        view2.setElevation(0.0f);
        View view3 = this.centerPanel;
        if (view3 == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        view3.setVisibility(0);
        View view4 = this.centerPanel;
        if (view4 == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        view4.setElevation(0.0f);
        View view5 = this.endPanel;
        if (view5 == null) {
            m.throwUninitializedPropertyAccessException("endPanel");
        }
        view5.setVisibility(4);
        View view6 = this.endPanel;
        if (view6 == null) {
            m.throwUninitializedPropertyAccessException("endPanel");
        }
        view6.setElevation(0.0f);
        resetStartPanelWidth();
        resetEndPanelWidth();
        handleStartPanelWidthUpdate();
        handleEndPanelWidthUpdate();
        Function0<Unit> function0 = this.pendingUpdate;
        if (function0 != null) {
            function0.invoke();
        }
        this.pendingUpdate = null;
        View view7 = this.startPanel;
        if (view7 == null) {
            m.throwUninitializedPropertyAccessException("startPanel");
        }
        view7.addOnLayoutChangeListener(new b(0, this));
        View view8 = this.endPanel;
        if (view8 == null) {
            m.throwUninitializedPropertyAccessException("endPanel");
        }
        view8.addOnLayoutChangeListener(new b(1, this));
    }

    private final void initialize(AttributeSet attributeSet) {
        int i;
        b.a.o.a aVar = b.a.o.a.f247b;
        Context context = getContext();
        m.checkExpressionValueIsNotNull(context, "context");
        m.checkParameterIsNotNull(context, "context");
        this.isLeftToRight = TextUtils.getLayoutDirectionFromLocale(b.a.o.a.a.invoke(context)) == 0;
        this.scrollingSlopPx = getResources().getDimension(R.a.overlapping_panels_scroll_slop);
        this.homeGestureFromBottomThreshold = getResources().getDimension(R.a.overlapping_panels_home_gesture_from_bottom_threshold);
        this.minFlingPxPerSecond = getResources().getDimension(R.a.overlapping_panels_min_fling_dp_per_second);
        Resources resources = getResources();
        m.checkExpressionValueIsNotNull(resources, "resources");
        if (resources.getConfiguration().orientation == 1) {
            Resources resources2 = getResources();
            m.checkExpressionValueIsNotNull(resources2, "resources");
            i = resources2.getDisplayMetrics().widthPixels;
        } else {
            Resources resources3 = getResources();
            m.checkExpressionValueIsNotNull(resources3, "resources");
            i = resources3.getDisplayMetrics().heightPixels;
        }
        this.nonFullScreenSidePanelWidth = (int) ((i - getResources().getDimension(R.a.overlapping_panels_margin_between_panels)) - getResources().getDimension(R.a.overlapping_panels_closed_center_panel_visible_width));
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.b.OverlappingPanelsLayout, 0, 0);
        try {
            this.nonFullScreenSidePanelWidth = Math.min(this.nonFullScreenSidePanelWidth, (int) obtainStyledAttributes.getDimension(R.b.OverlappingPanelsLayout_maxSidePanelNonFullScreenWidth, Integer.MAX_VALUE));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    private final boolean isTouchingCenterPanelWhileSidePanelOpen(MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        View view = this.centerPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        float x3 = view.getX();
        float max = Math.max(this.startPanelOpenedCenterPanelX, this.endPanelOpenedCenterPanelX);
        float min = Math.min(this.startPanelOpenedCenterPanelX, this.endPanelOpenedCenterPanelX);
        View view2 = this.centerPanel;
        if (view2 == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        float width = view2.getWidth() + min;
        boolean z2 = x2 > max;
        boolean z3 = x2 < width;
        boolean z4 = x3 == max;
        boolean z5 = x3 == min;
        if (!z4 || !z2) {
            return z5 && z3;
        }
        return true;
    }

    private final boolean isTouchingChildGestureRegion(MotionEvent motionEvent) {
        boolean z2;
        float rawX = motionEvent.getRawX();
        float rawY = motionEvent.getRawY();
        Iterator<T> it = this.childGestureRegions.iterator();
        do {
            z2 = false;
            if (!it.hasNext()) {
                return false;
            }
            Rect rect = (Rect) it.next();
            boolean z3 = rawX >= ((float) rect.left) && rawX <= ((float) rect.right);
            boolean z4 = rawY <= ((float) rect.bottom) && rawY >= ((float) rect.top);
            if (z3 && z4) {
                z2 = true;
                continue;
            }
        } while (!z2);
        return true;
    }

    public static /* synthetic */ void openEndPanel$default(OverlappingPanelsLayout overlappingPanelsLayout, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z2 = false;
            }
            overlappingPanelsLayout.openEndPanel(z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: openEndPanel");
    }

    private final void openPanel(Panel panel) {
        int ordinal = panel.ordinal();
        if (ordinal == 0) {
            openStartPanel(false);
        } else if (ordinal == 1) {
            closePanels(false);
        } else if (ordinal == 2) {
            openEndPanel(false);
        }
    }

    public static /* synthetic */ void openStartPanel$default(OverlappingPanelsLayout overlappingPanelsLayout, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z2 = false;
            }
            overlappingPanelsLayout.openStartPanel(z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: openStartPanel");
    }

    private final void resetEndPanelWidth() {
        View view = this.endPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("endPanel");
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = this.nonFullScreenSidePanelWidth;
        View view2 = this.endPanel;
        if (view2 == null) {
            m.throwUninitializedPropertyAccessException("endPanel");
        }
        view2.setLayoutParams(layoutParams);
    }

    private final void resetStartPanelWidth() {
        View view = this.startPanel;
        if (view != null) {
            if (view == null) {
                m.throwUninitializedPropertyAccessException("startPanel");
            }
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = this.useFullWidthForStartPanel ? -1 : this.nonFullScreenSidePanelWidth;
            View view2 = this.startPanel;
            if (view2 == null) {
                m.throwUninitializedPropertyAccessException("startPanel");
            }
            view2.setLayoutParams(layoutParams);
        }
    }

    private final boolean shouldHandleActionMoveEvent(MotionEvent motionEvent) {
        float normalizedX = getNormalizedX(getTargetedX(motionEvent));
        View view = this.centerPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        float abs = Math.abs(normalizedX - view.getX());
        Resources resources = getResources();
        m.checkExpressionValueIsNotNull(resources, "resources");
        return normalizedX == 0.0f || normalizedX == this.startPanelOpenedCenterPanelX || normalizedX == this.endPanelOpenedCenterPanelX || ((abs > resources.getDisplayMetrics().density ? 1 : (abs == resources.getDisplayMetrics().density ? 0 : -1)) > 0);
    }

    private final void snapOpenOrClose(MotionEvent motionEvent) {
        float targetedX = getTargetedX(motionEvent);
        VelocityTracker velocityTracker = this.velocityTracker;
        if (velocityTracker != null) {
            velocityTracker.computeCurrentVelocity(1000);
        }
        VelocityTracker velocityTracker2 = this.velocityTracker;
        float xVelocity = velocityTracker2 != null ? velocityTracker2.getXVelocity() : h.a.getMIN_VALUE();
        boolean z2 = false;
        boolean z3 = Math.abs(xVelocity) > this.minFlingPxPerSecond;
        if (!this.isLeftToRight ? xVelocity < 0 : xVelocity > 0) {
            z2 = true;
        }
        if (z3) {
            if (z2) {
                Panel panel = this.selectedPanel;
                if (panel == Panel.END) {
                    closePanels(true);
                    return;
                } else if (panel == Panel.CENTER) {
                    openStartPanel(true);
                    return;
                }
            } else {
                Panel panel2 = this.selectedPanel;
                if (panel2 == Panel.START) {
                    closePanels(true);
                    return;
                } else if (panel2 == Panel.CENTER) {
                    openEndPanel(true);
                    return;
                }
            }
        }
        float max = Math.max(this.startPanelOpenedCenterPanelX, this.endPanelOpenedCenterPanelX);
        float min = Math.min(this.startPanelOpenedCenterPanelX, this.endPanelOpenedCenterPanelX);
        float f = 2;
        if (targetedX > max / f) {
            openPanel(getLeftPanel());
        } else if (targetedX < min / f) {
            openPanel(getRightPanel());
        } else {
            closePanels();
        }
    }

    private final void translateCenterPanel(MotionEvent motionEvent) {
        updateCenterPanelX(getNormalizedX(getTargetedX(motionEvent)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateCenterPanelX(float f) {
        View view = this.centerPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        float x2 = view.getX();
        View view2 = this.centerPanel;
        if (view2 == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        view2.setX(f);
        handleCenterPanelX(x2, f);
    }

    private final void updateCenterPanelXWithAnimation(float f, boolean z2, long j) {
        View view = this.centerPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        float x2 = view.getX();
        ValueAnimator valueAnimator = this.centerPanelXAnimator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        float normalizedX = getNormalizedX(f);
        this.centerPanelAnimationEndX = normalizedX;
        if (z2) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(x2, normalizedX);
            ofFloat.setInterpolator(new LinearOutSlowInInterpolator());
            ofFloat.setDuration(j);
            this.centerPanelXAnimator = ofFloat;
            ofFloat.addUpdateListener(new a(0, this));
        } else {
            ValueAnimator ofFloat2 = ValueAnimator.ofFloat(x2, normalizedX);
            ofFloat2.setInterpolator(new FastOutSlowInInterpolator());
            ofFloat2.setDuration(j);
            this.centerPanelXAnimator = ofFloat2;
            ofFloat2.addUpdateListener(new a(1, this));
        }
        ValueAnimator valueAnimator2 = this.centerPanelXAnimator;
        if (valueAnimator2 != null) {
            valueAnimator2.start();
        }
    }

    public static /* synthetic */ void updateCenterPanelXWithAnimation$default(OverlappingPanelsLayout overlappingPanelsLayout, float f, boolean z2, long j, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            if ((i & 4) != 0) {
                j = SIDE_PANEL_OPEN_DURATION_MS;
            }
            overlappingPanelsLayout.updateCenterPanelXWithAnimation(f, z2, j);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: updateCenterPanelXWithAnimation");
    }

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void closePanels() {
        closePanels(false);
    }

    public final Panel getSelectedPanel() {
        return this.selectedPanel;
    }

    public final void handleEndPanelState(PanelState panelState) {
        m.checkParameterIsNotNull(panelState, "endPanelState");
        PanelState panelState2 = this.endPanelState;
        PanelState.c cVar = PanelState.c.a;
        if (m.areEqual(panelState, cVar) && (!m.areEqual(panelState2, cVar))) {
            openEndPanel();
        } else if ((panelState instanceof PanelState.a) && m.areEqual(panelState2, cVar)) {
            closePanels();
        }
        this.endPanelState = panelState;
    }

    public final void handleStartPanelState(PanelState panelState) {
        m.checkParameterIsNotNull(panelState, "startPanelState");
        PanelState panelState2 = this.startPanelState;
        PanelState.c cVar = PanelState.c.a;
        if (m.areEqual(panelState, cVar) && (!m.areEqual(panelState2, cVar))) {
            openStartPanel();
        } else if (m.areEqual(panelState, PanelState.a.a) && m.areEqual(panelState2, cVar)) {
            closePanels();
        }
        this.startPanelState = panelState;
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        m.checkParameterIsNotNull(motionEvent, "event");
        int actionMasked = motionEvent.getActionMasked();
        boolean z2 = false;
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    if (!this.isScrollingHorizontally) {
                        float calculateDistanceX = calculateDistanceX(this.xFromInterceptActionDown, motionEvent);
                        float calculateDistanceY = calculateDistanceY(this.yFromInterceptActionDown, motionEvent);
                        boolean isTouchingChildGestureRegion = isTouchingChildGestureRegion(motionEvent);
                        if (Math.abs(calculateDistanceX) <= this.scrollingSlopPx || Math.abs(calculateDistanceX) <= Math.abs(calculateDistanceY) || isTouchingChildGestureRegion) {
                            return false;
                        }
                        this.isScrollingHorizontally = true;
                    }
                } else if (actionMasked != 3) {
                    return this.wasActionDownOnClosedCenterPanel;
                }
            }
            VelocityTracker velocityTracker = this.velocityTracker;
            if (velocityTracker != null) {
                velocityTracker.recycle();
            }
            this.velocityTracker = null;
            return this.isScrollingHorizontally || this.wasActionDownOnClosedCenterPanel;
        }
        this.isScrollingHorizontally = false;
        this.wasActionDownOnClosedCenterPanel = isTouchingCenterPanelWhileSidePanelOpen(motionEvent);
        View view = this.centerPanel;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("centerPanel");
        }
        this.centerPanelDiffX = view.getX() - motionEvent.getRawX();
        this.xFromInterceptActionDown = motionEvent.getX();
        float y2 = motionEvent.getY();
        this.yFromInterceptActionDown = y2;
        Resources resources = getResources();
        m.checkExpressionValueIsNotNull(resources, "resources");
        if (Math.abs(y2 - resources.getDisplayMetrics().heightPixels) < this.homeGestureFromBottomThreshold && this.isSystemGestureNavigationPossible) {
            z2 = true;
        }
        this.isHomeSystemGesture = z2;
        VelocityTracker velocityTracker2 = this.velocityTracker;
        if (velocityTracker2 == null) {
            VelocityTracker obtain = VelocityTracker.obtain();
            this.velocityTracker = obtain;
            if (obtain != null) {
                obtain.addMovement(motionEvent);
            }
        } else if (velocityTracker2 != null) {
            velocityTracker2.clear();
        }
        return this.wasActionDownOnClosedCenterPanel;
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        if (getChildCount() == 3 && this.centerPanel == null) {
            initPanels();
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        m.checkParameterIsNotNull(motionEvent, "event");
        if (this.isHomeSystemGesture) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                    if (actionMasked != 3) {
                        return false;
                    }
                } else if (isTouchingChildGestureRegion(motionEvent)) {
                    return false;
                } else {
                    float calculateDistanceX = calculateDistanceX(this.xFromInterceptActionDown, motionEvent);
                    if (Math.abs(calculateDistanceX) > this.scrollingSlopPx && this.swipeDirection == null) {
                        this.swipeDirection = calculateDistanceX > ((float) 0) ? SwipeDirection.RIGHT : SwipeDirection.LEFT;
                    }
                    VelocityTracker velocityTracker = this.velocityTracker;
                    if (velocityTracker != null) {
                        velocityTracker.addMovement(motionEvent);
                    }
                    if (shouldHandleActionMoveEvent(motionEvent)) {
                        translateCenterPanel(motionEvent);
                    }
                }
            }
            if (this.wasActionDownOnClosedCenterPanel && Math.abs(motionEvent.getX() - this.xFromInterceptActionDown) < this.scrollingSlopPx && !this.isScrollingHorizontally) {
                closePanels();
            } else {
                VelocityTracker velocityTracker2 = this.velocityTracker;
                if (velocityTracker2 != null) {
                    velocityTracker2.addMovement(motionEvent);
                }
                snapOpenOrClose(motionEvent);
            }
            this.wasActionDownOnClosedCenterPanel = false;
            this.isScrollingHorizontally = false;
            this.swipeDirection = null;
        }
        return true;
    }

    public final void openEndPanel() {
        openEndPanel(false);
    }

    public final void openStartPanel() {
        openStartPanel(false);
    }

    public final void registerEndPanelStateListeners(PanelStateListener... panelStateListenerArr) {
        m.checkParameterIsNotNull(panelStateListenerArr, "panelStateListenerArgs");
        for (PanelStateListener panelStateListener : panelStateListenerArr) {
            this.endPanelStateListeners.add(panelStateListener);
        }
    }

    public final void registerStartPanelStateListeners(PanelStateListener... panelStateListenerArr) {
        m.checkParameterIsNotNull(panelStateListenerArr, "panelStateListenerArgs");
        for (PanelStateListener panelStateListener : panelStateListenerArr) {
            this.startPanelStateListeners.add(panelStateListener);
        }
    }

    public final void setChildGestureRegions(List<Rect> list) {
        m.checkParameterIsNotNull(list, "childGestureRegions");
        this.childGestureRegions = list;
    }

    public final void setEndPanelLockState(LockState lockState) {
        m.checkParameterIsNotNull(lockState, "lockState");
        this.endPanelLockState = lockState;
        if (lockState == LockState.OPEN) {
            openEndPanel();
        }
    }

    public final void setStartPanelLockState(LockState lockState) {
        m.checkParameterIsNotNull(lockState, "lockState");
        this.startPanelLockState = lockState;
        if (lockState == LockState.OPEN) {
            openStartPanel();
        }
    }

    public final void setStartPanelUseFullPortraitWidth(boolean z2) {
        this.useFullWidthForStartPanel = z2;
        resetStartPanelWidth();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void closePanels(boolean z2) {
        if (this.centerPanel == null) {
            this.pendingUpdate = new c(z2);
        } else {
            updateCenterPanelXWithAnimation(0.0f, z2, SIDE_PANEL_CLOSE_DURATION_MS);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void openEndPanel(boolean z2) {
        if (this.centerPanel == null) {
            this.pendingUpdate = new d(z2);
        } else {
            updateCenterPanelXWithAnimation(this.endPanelOpenedCenterPanelX, z2, SIDE_PANEL_OPEN_DURATION_MS);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void openStartPanel(boolean z2) {
        if (this.centerPanel == null) {
            this.pendingUpdate = new e(z2);
        } else if (this.startPanelLockState == LockState.OPEN) {
            updateCenterPanelX(this.startPanelOpenedCenterPanelX);
        } else {
            updateCenterPanelXWithAnimation(this.startPanelOpenedCenterPanelX, z2, SIDE_PANEL_OPEN_DURATION_MS);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OverlappingPanelsLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkParameterIsNotNull(context, "context");
        boolean z2 = true;
        this.isLeftToRight = true;
        h hVar = h.a;
        this.startPanelOpenedCenterPanelX = hVar.getMIN_VALUE();
        this.endPanelOpenedCenterPanelX = hVar.getMAX_VALUE();
        this.startPanelStateListeners = new ArrayList<>();
        this.endPanelStateListeners = new ArrayList<>();
        this.selectedPanel = Panel.CENTER;
        LockState lockState = LockState.UNLOCKED;
        this.startPanelLockState = lockState;
        this.endPanelLockState = lockState;
        PanelState.a aVar = PanelState.a.a;
        this.startPanelState = aVar;
        this.endPanelState = aVar;
        this.childGestureRegions = n.emptyList();
        this.isSystemGestureNavigationPossible = Build.VERSION.SDK_INT < 29 ? false : z2;
        initialize(attributeSet);
    }

    public /* synthetic */ OverlappingPanelsLayout(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OverlappingPanelsLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkParameterIsNotNull(context, "context");
        boolean z2 = true;
        this.isLeftToRight = true;
        h hVar = h.a;
        this.startPanelOpenedCenterPanelX = hVar.getMIN_VALUE();
        this.endPanelOpenedCenterPanelX = hVar.getMAX_VALUE();
        this.startPanelStateListeners = new ArrayList<>();
        this.endPanelStateListeners = new ArrayList<>();
        this.selectedPanel = Panel.CENTER;
        LockState lockState = LockState.UNLOCKED;
        this.startPanelLockState = lockState;
        this.endPanelLockState = lockState;
        PanelState.a aVar = PanelState.a.a;
        this.startPanelState = aVar;
        this.endPanelState = aVar;
        this.childGestureRegions = n.emptyList();
        this.isSystemGestureNavigationPossible = Build.VERSION.SDK_INT < 29 ? false : z2;
        initialize(attributeSet);
    }

    public /* synthetic */ OverlappingPanelsLayout(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
