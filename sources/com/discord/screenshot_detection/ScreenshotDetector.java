package com.discord.screenshot_detection;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import b.a.s.b;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import rx.subjects.PublishSubject;
/* compiled from: ScreenshotDetector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001(B\u001f\u0012\u0006\u0010%\u001a\u00020$\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\f¢\u0006\u0004\b&\u0010'R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\u000b\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0019\u0010\u0011\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0015\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001c\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00170\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R*\u0010#\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b8\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"¨\u0006)"}, d2 = {"Lcom/discord/screenshot_detection/ScreenshotDetector;", "", "Lcom/discord/utilities/logging/Logger;", "f", "Lcom/discord/utilities/logging/Logger;", "getLogger", "()Lcom/discord/utilities/logging/Logger;", "logger", "Lb/a/s/a;", "d", "Lb/a/s/a;", "activityCallbacks", "Landroid/content/SharedPreferences;", "g", "Landroid/content/SharedPreferences;", "getCache", "()Landroid/content/SharedPreferences;", "cache", "Lb/a/s/b;", "c", "Lb/a/s/b;", "screenshotContentObserver", "Lrx/subjects/PublishSubject;", "Lcom/discord/screenshot_detection/ScreenshotDetector$Screenshot;", "b", "Lrx/subjects/PublishSubject;", "publishSubject", "", "value", "e", "Z", "getEnabled", "()Z", "a", "(Z)V", "enabled", "Landroid/app/Application;", "applicationContext", HookHelper.constructorName, "(Landroid/app/Application;Lcom/discord/utilities/logging/Logger;Landroid/content/SharedPreferences;)V", "Screenshot", "screenshot_detection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ScreenshotDetector {
    public static ScreenshotDetector a;

    /* renamed from: b  reason: collision with root package name */
    public final PublishSubject<Screenshot> f2776b;
    public final b c;
    public final b.a.s.a d;
    public boolean e;
    public final Logger f;
    public final SharedPreferences g;

    /* compiled from: ScreenshotDetector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0011\u001a\u00020\f\u0012\u0006\u0010\u0015\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/screenshot_detection/ScreenshotDetector$Screenshot;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Landroid/net/Uri;", "a", "Landroid/net/Uri;", "getUri", "()Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "b", "Ljava/lang/String;", "getFilename", "filename", HookHelper.constructorName, "(Landroid/net/Uri;Ljava/lang/String;)V", "screenshot_detection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Screenshot {
        public final Uri a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2777b;

        public Screenshot(Uri uri, String str) {
            m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            m.checkNotNullParameter(str, "filename");
            this.a = uri;
            this.f2777b = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Screenshot)) {
                return false;
            }
            Screenshot screenshot = (Screenshot) obj;
            return m.areEqual(this.a, screenshot.a) && m.areEqual(this.f2777b, screenshot.f2777b);
        }

        public int hashCode() {
            Uri uri = this.a;
            int i = 0;
            int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
            String str = this.f2777b;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Screenshot(uri=");
            R.append(this.a);
            R.append(", filename=");
            return b.d.b.a.a.H(R, this.f2777b, ")");
        }
    }

    /* compiled from: ScreenshotDetector.kt */
    /* loaded from: classes.dex */
    public static final class a extends o implements Function2<Uri, String, Unit> {
        public a() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public Unit invoke(Uri uri, String str) {
            Uri uri2 = uri;
            String str2 = str;
            m.checkNotNullParameter(uri2, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            m.checkNotNullParameter(str2, "filename");
            ScreenshotDetector screenshotDetector = ScreenshotDetector.this;
            if (screenshotDetector.e) {
                PublishSubject<Screenshot> publishSubject = screenshotDetector.f2776b;
                publishSubject.k.onNext(new Screenshot(uri2, str2));
            }
            return Unit.a;
        }
    }

    public ScreenshotDetector(Application application, Logger logger, SharedPreferences sharedPreferences) {
        m.checkNotNullParameter(application, "applicationContext");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(sharedPreferences, "cache");
        this.f = logger;
        this.g = sharedPreferences;
        PublishSubject<Screenshot> k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.f2776b = k0;
        ContentResolver contentResolver = application.getContentResolver();
        m.checkNotNullExpressionValue(contentResolver, "applicationContext.contentResolver");
        b bVar = new b(logger, contentResolver, null, new a(), 4);
        this.c = bVar;
        b.a.s.a aVar = new b.a.s.a(bVar);
        this.d = aVar;
        a(sharedPreferences.getBoolean("screenshot_detection_enabled", false));
        application.registerActivityLifecycleCallbacks(aVar);
        aVar.a(this.e);
    }

    public final void a(boolean z2) {
        this.e = z2;
        SharedPreferences.Editor edit = this.g.edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean("screenshot_detection_enabled", z2);
        edit.apply();
        this.d.a(z2);
    }
}
