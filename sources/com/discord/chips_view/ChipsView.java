package com.discord.chips_view;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.LeadingMarginSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.exifinterface.media.ExifInterface;
import b.a.f.a;
import b.a.f.d;
import b.a.f.e;
import b.a.f.f;
import b.a.f.g;
import com.discord.chips_view.ChipsView.a;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ChipsView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000*\u0004\b\u0000\u0010\u0001*\n\b\u0001\u0010\u0003*\u0004\u0018\u00010\u00022\u00020\u00042\u00020\u0005:\u0003$ \tB\u0019\b\u0016\u0012\u0006\u0010u\u001a\u00020t\u0012\u0006\u0010w\u001a\u00020v¢\u0006\u0004\bx\u0010yJ\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\r\u001a\u00020\b2\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\u0010\u001a\u00020\b2\u0010\u0010\u000f\u001a\f\u0012\u0002\b\u0003\u0012\u0002\b\u0003\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\u0010\u0010\u000eJ\u001f\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011H\u0014¢\u0006\u0004\b\u0014\u0010\u0015J!\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u00112\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017H\u0014¢\u0006\u0004\b\u0019\u0010\u001aJ1\u0010 \u001a\u00020\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001e\u001a\u00028\u00002\u0006\u0010\u001f\u001a\u00028\u0001¢\u0006\u0004\b \u0010!J\u0019\u0010$\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\"H\u0016¢\u0006\u0004\b$\u0010%J\u0019\u0010(\u001a\u00020\b2\n\u0010'\u001a\u0006\u0012\u0002\b\u00030&¢\u0006\u0004\b(\u0010)R\u0016\u0010,\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010.\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010+R\u0016\u00100\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u0010+R\u0016\u00102\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u0010+R\u0016\u00104\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u0010+R\u0016\u00106\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b5\u0010+R2\u0010?\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u000108\u0012\u0004\u0012\u00020\b\u0018\u0001078\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b9\u0010:\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>R\u0018\u0010C\u001a\u0004\u0018\u00010@8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010E\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010+R\u0016\u0010G\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bF\u0010+R\u0016\u0010I\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bH\u0010+R\u0016\u0010K\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bJ\u0010+R\u0016\u0010M\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bL\u0010+R\u0018\u0010Q\u001a\u0004\u0018\u00010N8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bO\u0010PR\u0016\u0010U\u001a\u00020R8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bS\u0010TR\u0016\u0010W\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bV\u0010+R\u0016\u0010Y\u001a\u00020R8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bX\u0010TR\u0016\u0010[\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bZ\u0010+R2\u0010_\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00018\u0001\u0012\u0004\u0012\u00020\b\u0018\u0001078\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\\\u0010:\u001a\u0004\b]\u0010<\"\u0004\b^\u0010>R.\u0010c\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000b0`8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\ba\u0010bR$\u0010d\u001a\u00020\u001b2\u0006\u0010d\u001a\u00020\u001b8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\be\u0010f\"\u0004\bg\u0010hR\u0016\u0010l\u001a\u00020i8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010kR2\u0010o\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00018\u0001\u0012\u0004\u0012\u00020\b\u0018\u0001078\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bT\u0010:\u001a\u0004\bm\u0010<\"\u0004\bn\u0010>R\u0016\u0010s\u001a\u00020p8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bq\u0010r¨\u0006z"}, d2 = {"Lcom/discord/chips_view/ChipsView;", "K", "Lcom/discord/chips_view/ChipsView$a;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "Landroid/widget/ScrollView;", "Lb/a/f/d$a;", "", "moveCursor", "", "c", "(Z)V", "Lb/a/f/a;", "chip", "e", "(Lb/a/f/a;)V", "rootChip", "f", "", "widthMeasureSpec", "heightMeasureSpec", "onMeasure", "(II)V", "direction", "Landroid/graphics/Rect;", "previouslyFocusedRect", "onRequestFocusInDescendants", "(ILandroid/graphics/Rect;)Z", "", "displayName", "imageContentDescription", "key", "data", "b", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/discord/chips_view/ChipsView$a;)V", "Landroid/view/inputmethod/InputConnection;", "target", "a", "(Landroid/view/inputmethod/InputConnection;)Landroid/view/inputmethod/InputConnection;", "", "pruneData", "d", "(Ljava/util/Collection;)V", "k", "I", "mChipsBgResId", "l", "mMaxHeight", "r", "mChipsBgColorClicked", "x", "mChipLayout", "m", "mVerticalSpacingPx", "s", "mChipsTextColor", "Lkotlin/Function1;", "", "G", "Lkotlin/jvm/functions/Function1;", "getTextChangedListener", "()Lkotlin/jvm/functions/Function1;", "setTextChangedListener", "(Lkotlin/jvm/functions/Function1;)V", "textChangedListener", "", "D", "Ljava/lang/Object;", "mCurrentEditTextSpan", "p", "mChipsColorClicked", "t", "mChipsTextColorClicked", "o", "mChipsColor", "n", "mChipHeightPx", "q", "mChipsBgColor", "Lb/a/f/e;", "B", "Lb/a/f/e;", "mRootChipsLayout", "", "v", "F", "mChipsSearchTextSize", "w", "mChipsHintRes", "y", "mDensity", "u", "mChipsSearchTextColor", ExifInterface.LONGITUDE_EAST, "getChipAddedListener", "setChipAddedListener", "chipAddedListener", "Ljava/util/LinkedHashMap;", "C", "Ljava/util/LinkedHashMap;", "mChipList", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "getText", "()Ljava/lang/String;", "setText", "(Ljava/lang/String;)V", "Lb/a/f/d;", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "Lb/a/f/d;", "mEditText", "getChipDeletedListener", "setChipDeletedListener", "chipDeletedListener", "Landroid/widget/RelativeLayout;", "z", "Landroid/widget/RelativeLayout;", "mChipsContainer", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "chips_view_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChipsView<K, V extends a> extends ScrollView implements d.a {
    public static final /* synthetic */ int j = 0;
    public final b.a.f.d A;
    public b.a.f.e B;
    public final LinkedHashMap<K, b.a.f.a<K, V>> C = new LinkedHashMap<>();
    public Object D;
    public Function1<? super V, Unit> E;
    public Function1<? super V, Unit> F;
    public Function1<? super CharSequence, Unit> G;
    public int k;
    public int l;
    public int m;
    public int n;
    public int o;
    public int p;
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public int f2073s;
    public int t;
    public int u;
    public float v;
    public int w;

    /* renamed from: x  reason: collision with root package name */
    public int f2074x;

    /* renamed from: y  reason: collision with root package name */
    public float f2075y;

    /* renamed from: z  reason: collision with root package name */
    public final RelativeLayout f2076z;

    /* compiled from: ChipsView.kt */
    /* loaded from: classes.dex */
    public interface a {
        String getDisplayString();
    }

    /* compiled from: ChipsView.kt */
    /* loaded from: classes.dex */
    public final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            m.checkNotNullParameter(editable, "s");
            Function1<CharSequence, Unit> textChangedListener = ChipsView.this.getTextChangedListener();
            if (textChangedListener != null) {
                textChangedListener.invoke(editable);
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            m.checkNotNullParameter(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            m.checkNotNullParameter(charSequence, "s");
        }
    }

    /* compiled from: ChipsView.kt */
    /* loaded from: classes.dex */
    public final class c extends InputConnectionWrapper {
        public c(InputConnection inputConnection) {
            super(inputConnection, true);
        }

        @Override // android.view.inputmethod.InputConnectionWrapper, android.view.inputmethod.InputConnection
        public boolean commitText(CharSequence charSequence, int i) {
            m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            return super.commitText(charSequence, i);
        }

        @Override // android.view.inputmethod.InputConnectionWrapper, android.view.inputmethod.InputConnection
        public boolean deleteSurroundingText(int i, int i2) {
            if (ChipsView.this.A.length() == 0 && i == 1 && i2 == 0) {
                return sendKeyEvent(new KeyEvent(0, 67)) && sendKeyEvent(new KeyEvent(1, 67));
            }
            return super.deleteSurroundingText(i, i2);
        }

        @Override // android.view.inputmethod.InputConnectionWrapper, android.view.inputmethod.InputConnection
        public boolean sendKeyEvent(KeyEvent keyEvent) {
            m.checkNotNullParameter(keyEvent, "event");
            if (!(ChipsView.this.A.length() == 0 && keyEvent.getAction() == 0 && keyEvent.getKeyCode() == 67)) {
                return super.sendKeyEvent(keyEvent);
            }
            ChipsView chipsView = ChipsView.this;
            if (chipsView.C.size() > 0) {
                try {
                    b.a.f.a<K, V> aVar = null;
                    for (Map.Entry<K, b.a.f.a<K, V>> entry : chipsView.C.entrySet()) {
                        aVar = entry.getValue();
                    }
                    if (aVar != null) {
                        m.checkNotNullParameter(aVar, "chip");
                        chipsView.f(aVar);
                        if (aVar.k) {
                            chipsView.e(aVar);
                        } else {
                            aVar.k = true;
                            chipsView.c(false);
                        }
                    }
                } catch (IndexOutOfBoundsException e) {
                    Log.e("ChipsView", "Out of bounds", e);
                }
            }
            return true;
        }
    }

    /* compiled from: ChipsView.kt */
    /* loaded from: classes.dex */
    public static final class d implements Runnable {
        public d() {
        }

        @Override // java.lang.Runnable
        public final void run() {
            ChipsView.this.fullScroll(130);
        }
    }

    /* compiled from: ChipsView.kt */
    /* loaded from: classes.dex */
    public static final class e implements Runnable {
        public final /* synthetic */ boolean k;

        public e(boolean z2) {
            this.k = z2;
        }

        @Override // java.lang.Runnable
        public final void run() {
            ChipsView chipsView = ChipsView.this;
            boolean z2 = this.k;
            int i = ChipsView.j;
            chipsView.c(z2);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Finally extract failed */
    public ChipsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        this.f2075y = resources.getDisplayMetrics().density;
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        this.f2076z = relativeLayout;
        addView(relativeLayout);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
        linearLayout.setFocusable(true);
        linearLayout.setFocusableInTouchMode(true);
        relativeLayout.addView(linearLayout);
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        b.a.f.d dVar = new b.a.f.d(context2, this);
        this.A = dVar;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.c.ChipsView, 0, 0);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "context.theme.obtainStyl…tyleable.ChipsView, 0, 0)");
        try {
            this.l = obtainStyledAttributes.getDimensionPixelSize(R.c.ChipsView_cv_max_height, -1);
            this.m = obtainStyledAttributes.getDimensionPixelSize(R.c.ChipsView_cv_vertical_spacing, (int) (1 * this.f2075y));
            this.n = obtainStyledAttributes.getDimensionPixelSize(R.c.ChipsView_cv_height, (int) (24 * this.f2075y));
            this.o = obtainStyledAttributes.getColor(R.c.ChipsView_cv_color, ContextCompat.getColor(context, 17170432));
            this.p = obtainStyledAttributes.getColor(R.c.ChipsView_cv_color_clicked, ContextCompat.getColor(context, 17170443));
            this.q = obtainStyledAttributes.getColor(R.c.ChipsView_cv_bg_color, ContextCompat.getColor(context, 17170443));
            this.k = obtainStyledAttributes.getResourceId(R.c.ChipsView_cv_chip_bg_res, 0);
            this.r = obtainStyledAttributes.getColor(R.c.ChipsView_cv_bg_color_clicked, ContextCompat.getColor(context, 17170451));
            this.f2073s = obtainStyledAttributes.getColor(R.c.ChipsView_cv_text_color, ViewCompat.MEASURED_STATE_MASK);
            this.t = obtainStyledAttributes.getColor(R.c.ChipsView_cv_text_color_clicked, -1);
            this.w = obtainStyledAttributes.getResourceId(R.c.ChipsView_cv_hint, 0);
            obtainStyledAttributes.getResourceId(R.c.ChipsView_cv_icon_placeholder, 0);
            this.u = obtainStyledAttributes.getColor(R.c.ChipsView_cv_search_text_color, ViewCompat.MEASURED_STATE_MASK);
            this.v = obtainStyledAttributes.getDimensionPixelSize(R.c.ChipsView_cv_search_text_size, 49);
            this.f2074x = obtainStyledAttributes.getResourceId(R.c.ChipsView_cv_chip_layout, R.b.view_chip_default);
            obtainStyledAttributes.recycle();
            int i = this.n + this.m;
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, i);
            float f = 4;
            layoutParams.leftMargin = (int) (this.f2075y * f);
            layoutParams.addRule(12, -1);
            dVar.setLayoutParams(layoutParams);
            dVar.setPadding(0, 0, 0, this.m);
            dVar.setBackgroundColor(Color.argb(0, 0, 0, 0));
            dVar.setImeOptions(268435456);
            dVar.setInputType(1);
            dVar.setTextColor(this.u);
            dVar.setTextSize(0, this.v);
            relativeLayout.addView(dVar);
            Context context3 = getContext();
            m.checkNotNullExpressionValue(context3, "context");
            b.a.f.e eVar = new b.a.f.e(context3, i);
            eVar.setOrientation(1);
            eVar.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            eVar.setPadding(0, (int) (f * this.f2075y), 0, 0);
            this.B = eVar;
            relativeLayout.addView(eVar);
            relativeLayout.setOnClickListener(new g(this));
            dVar.addTextChangedListener(new b());
            dVar.setOnFocusChangeListener(new f(this));
            c(false);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @Override // b.a.f.d.a
    public InputConnection a(InputConnection inputConnection) {
        return new c(inputConnection);
    }

    public final void b(String str, String str2, K k, V v) {
        if (!this.C.containsKey(k)) {
            this.A.setText("");
            b.a.f.a<K, V> aVar = new b.a.f.a<>(str, str2, k, v, new a.C0030a(this.r, this.f2075y, this.k, this.q, this.f2073s, this.t, this.p, this.o, this.n, this.f2074x), this);
            Editable text = this.A.getText();
            if (text != null) {
                Object obj = this.D;
                if (obj != null) {
                    text.removeSpan(obj);
                }
                text.setSpan(this.D, 0, 0, 17);
            }
            this.A.setText(text);
            this.C.put(k, aVar);
            Function1<? super V, Unit> function1 = this.E;
            if (function1 != null) {
                function1.invoke(aVar.o);
            }
            c(true);
            post(new d());
        }
    }

    public final void c(boolean z2) {
        TextView textView;
        RelativeLayout relativeLayout;
        Drawable background;
        TextView textView2;
        RelativeLayout relativeLayout2;
        Drawable background2;
        TextView textView3;
        TextView textView4;
        ImageView imageView;
        RelativeLayout relativeLayout3;
        RelativeLayout relativeLayout4;
        b.a.f.e eVar = this.B;
        e.a aVar = null;
        if (eVar != null) {
            Collection<b.a.f.a<K, V>> values = this.C.values();
            m.checkNotNullExpressionValue(values, "mChipList.values");
            m.checkNotNullParameter(values, "chips");
            for (LinearLayout linearLayout : eVar.j) {
                linearLayout.removeAllViews();
            }
            eVar.j.clear();
            eVar.removeAllViews();
            int width = eVar.getWidth();
            if (width != 0) {
                LinearLayout a2 = eVar.a();
                int i = 0;
                int i2 = 0;
                for (b.a.f.a<K, V> aVar2 : values) {
                    if (aVar2.j == null) {
                        View inflate = LayoutInflater.from(aVar2.q.getContext()).inflate(R.b.view_chip_default, (ViewGroup) null, false);
                        int i3 = R.a.chip_image;
                        ImageView imageView2 = (ImageView) inflate.findViewById(i3);
                        if (imageView2 != null) {
                            i3 = R.a.chip_text;
                            TextView textView5 = (TextView) inflate.findViewById(i3);
                            if (textView5 != null) {
                                aVar2.j = new b.a.f.h.a((RelativeLayout) inflate, imageView2, textView5);
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, aVar2.p.f);
                                b.a.f.h.a aVar3 = aVar2.j;
                                if (!(aVar3 == null || (relativeLayout4 = aVar3.a) == null)) {
                                    relativeLayout4.setLayoutParams(layoutParams);
                                }
                                b.a.f.h.a aVar4 = aVar2.j;
                                if (!(aVar4 == null || (relativeLayout3 = aVar4.a) == null)) {
                                    relativeLayout3.setBackgroundResource(aVar2.p.f66b);
                                    relativeLayout3.post(new b.a.f.b(relativeLayout3, aVar2));
                                    relativeLayout3.setOnClickListener(aVar2);
                                }
                                b.a.f.h.a aVar5 = aVar2.j;
                                if (!(aVar5 == null || (imageView = aVar5.f67b) == null)) {
                                    imageView.setOnClickListener(aVar2);
                                    m.checkNotNullExpressionValue(imageView, "it");
                                    imageView.setContentDescription(aVar2.m);
                                }
                                b.a.f.h.a aVar6 = aVar2.j;
                                if (!(aVar6 == null || (textView4 = aVar6.c) == null)) {
                                    textView4.setTextColor(aVar2.p.d);
                                }
                            }
                        }
                        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i3)));
                    }
                    b.a.f.h.a aVar7 = aVar2.j;
                    if (!(aVar7 == null || (textView3 = aVar7.c) == null)) {
                        textView3.setText(aVar2.l);
                    }
                    if (aVar2.k) {
                        b.a.f.h.a aVar8 = aVar2.j;
                        if (!(aVar8 == null || (relativeLayout2 = aVar8.a) == null || (background2 = relativeLayout2.getBackground()) == null)) {
                            background2.setColorFilter(aVar2.p.a, PorterDuff.Mode.SRC_ATOP);
                        }
                        b.a.f.h.a aVar9 = aVar2.j;
                        if (!(aVar9 == null || (textView2 = aVar9.c) == null)) {
                            textView2.setTextColor(aVar2.p.e);
                        }
                    } else {
                        b.a.f.h.a aVar10 = aVar2.j;
                        if (!(aVar10 == null || (relativeLayout = aVar10.a) == null || (background = relativeLayout.getBackground()) == null)) {
                            background.setColorFilter(aVar2.p.c, PorterDuff.Mode.SRC_ATOP);
                        }
                        b.a.f.h.a aVar11 = aVar2.j;
                        if (!(aVar11 == null || (textView = aVar11.c) == null)) {
                            textView.setTextColor(aVar2.p.d);
                        }
                    }
                    b.a.f.h.a aVar12 = aVar2.j;
                    RelativeLayout relativeLayout5 = aVar12 != null ? aVar12.a : null;
                    m.checkNotNull(relativeLayout5);
                    relativeLayout5.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                    if (relativeLayout5.getMeasuredWidth() + i > width) {
                        i2++;
                        a2 = eVar.a();
                        i = 0;
                    }
                    int measuredWidth = relativeLayout5.getMeasuredWidth();
                    ViewGroup.LayoutParams layoutParams2 = relativeLayout5.getLayoutParams();
                    Objects.requireNonNull(layoutParams2, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
                    i += measuredWidth + ((LinearLayout.LayoutParams) layoutParams2).rightMargin;
                    a2.addView(relativeLayout5);
                }
                if (width - i < width * 0.15f) {
                    i2++;
                    eVar.a();
                    i = 0;
                }
                aVar = new e.a(i2, i);
            }
        }
        if (this.C.isEmpty()) {
            int i4 = this.w;
            if (i4 != 0) {
                this.A.setHint(i4);
            }
        } else {
            this.A.setHint("");
        }
        if (aVar == null) {
            post(new e(z2));
            return;
        }
        int i5 = aVar.a;
        Editable text = this.A.getText();
        Object obj = this.D;
        if (!(obj == null || text == null)) {
            text.removeSpan(obj);
        }
        LeadingMarginSpan.Standard standard = new LeadingMarginSpan.Standard(i5, 0);
        this.D = standard;
        if (text != null) {
            text.setSpan(standard, 0, 0, 17);
        }
        this.A.setText(text);
        if (z2) {
            b.a.f.d dVar = this.A;
            dVar.setSelection(dVar.length());
        }
    }

    public final void d(Collection<?> collection) {
        m.checkNotNullParameter(collection, "pruneData");
        Iterator<Map.Entry<K, b.a.f.a<K, V>>> it = this.C.entrySet().iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            if (!u.contains(collection, it.next().getKey())) {
                it.remove();
                z2 = true;
            }
        }
        if (z2) {
            c(true);
        }
    }

    public final void e(b.a.f.a<K, V> aVar) {
        this.C.remove(aVar.n);
        Function1<? super V, Unit> function1 = this.F;
        if (function1 != null) {
            function1.invoke(aVar.o);
        }
        c(true);
    }

    public final void f(b.a.f.a<?, ?> aVar) {
        for (b.a.f.a<K, V> aVar2 : this.C.values()) {
            if (aVar2 != aVar) {
                aVar2.k = false;
            }
        }
        c(false);
    }

    public final Function1<V, Unit> getChipAddedListener() {
        return (Function1<? super V, Unit>) this.E;
    }

    public final Function1<V, Unit> getChipDeletedListener() {
        return (Function1<? super V, Unit>) this.F;
    }

    public final String getText() {
        return String.valueOf(this.A.getText());
    }

    public final Function1<CharSequence, Unit> getTextChangedListener() {
        return this.G;
    }

    @Override // android.widget.ScrollView, android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(this.l, Integer.MIN_VALUE));
    }

    @Override // android.widget.ScrollView, android.view.ViewGroup
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        return true;
    }

    public final void setChipAddedListener(Function1<? super V, Unit> function1) {
        this.E = function1;
    }

    public final void setChipDeletedListener(Function1<? super V, Unit> function1) {
        this.F = function1;
    }

    public final void setText(String str) {
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        this.A.setText(str);
    }

    public final void setTextChangedListener(Function1<? super CharSequence, Unit> function1) {
        this.G = function1;
    }
}
