package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackAnalyticsDataQueried.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R$\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010!\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u001b\u0010%\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0010\u001a\u0004\b&\u0010\u0012R\u001c\u0010'\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010\u0005R!\u0010+\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0010\u001a\u0004\b,\u0010\u0012R!\u0010-\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`*8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0010\u001a\u0004\b.\u0010\u0012R\u001b\u0010/\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0014\u001a\u0004\b0\u0010\u0016¨\u00061"}, d2 = {"Lcom/discord/analytics/generated/events/TrackAnalyticsDataQueried;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "responseHasData", "Ljava/lang/Boolean;", "getResponseHasData", "()Ljava/lang/Boolean;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "interval", "getInterval", "", "tableName", "Ljava/lang/CharSequence;", "getTableName", "()Ljava/lang/CharSequence;", "applicationId", "getApplicationId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "Lcom/discord/primitives/Timestamp;", "dateRangeEnd", "getDateRangeEnd", "dateRangeStart", "getDateRangeStart", "responseIsSuccessful", "getResponseIsSuccessful", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackAnalyticsDataQueried implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long applicationId = null;
    private final Long guildId = null;
    private final CharSequence tableName = null;
    private final Long dateRangeStart = null;
    private final Long dateRangeEnd = null;
    private final Long interval = null;
    private final Boolean responseIsSuccessful = null;
    private final Boolean responseHasData = null;
    private final transient String analyticsSchemaTypeName = "analytics_data_queried";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackAnalyticsDataQueried)) {
            return false;
        }
        TrackAnalyticsDataQueried trackAnalyticsDataQueried = (TrackAnalyticsDataQueried) obj;
        return m.areEqual(this.applicationId, trackAnalyticsDataQueried.applicationId) && m.areEqual(this.guildId, trackAnalyticsDataQueried.guildId) && m.areEqual(this.tableName, trackAnalyticsDataQueried.tableName) && m.areEqual(this.dateRangeStart, trackAnalyticsDataQueried.dateRangeStart) && m.areEqual(this.dateRangeEnd, trackAnalyticsDataQueried.dateRangeEnd) && m.areEqual(this.interval, trackAnalyticsDataQueried.interval) && m.areEqual(this.responseIsSuccessful, trackAnalyticsDataQueried.responseIsSuccessful) && m.areEqual(this.responseHasData, trackAnalyticsDataQueried.responseHasData);
    }

    public int hashCode() {
        Long l = this.applicationId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.guildId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.tableName;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l3 = this.dateRangeStart;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.dateRangeEnd;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.interval;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Boolean bool = this.responseIsSuccessful;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.responseHasData;
        if (bool2 != null) {
            i = bool2.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackAnalyticsDataQueried(applicationId=");
        R.append(this.applicationId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", tableName=");
        R.append(this.tableName);
        R.append(", dateRangeStart=");
        R.append(this.dateRangeStart);
        R.append(", dateRangeEnd=");
        R.append(this.dateRangeEnd);
        R.append(", interval=");
        R.append(this.interval);
        R.append(", responseIsSuccessful=");
        R.append(this.responseIsSuccessful);
        R.append(", responseHasData=");
        return a.C(R, this.responseHasData, ")");
    }
}
