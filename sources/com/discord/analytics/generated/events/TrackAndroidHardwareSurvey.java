package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackAndroidHardwareSurvey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b#\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R$\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u001b\u001a\u0004\b$\u0010\u001dR\u001b\u0010%\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u001b\u001a\u0004\b&\u0010\u001dR\u001b\u0010'\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u001b\u001a\u0004\b(\u0010\u001dR\u001b\u0010)\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010 \u001a\u0004\b*\u0010\"R\u001b\u0010+\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010 \u001a\u0004\b,\u0010\"R\u001b\u0010-\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u001b\u001a\u0004\b.\u0010\u001dR\u001b\u0010/\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u001b\u001a\u0004\b0\u0010\u001dR\u001b\u00101\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010 \u001a\u0004\b2\u0010\"R\u001b\u00103\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u000f\u001a\u0004\b3\u0010\u0011R\u001b\u00104\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u001b\u001a\u0004\b5\u0010\u001dR\u001b\u00106\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010 \u001a\u0004\b7\u0010\"R\u001b\u00108\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010 \u001a\u0004\b9\u0010\"R\u001b\u0010:\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u001b\u001a\u0004\b;\u0010\u001dR\u001c\u0010<\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010\u0005R\u001b\u0010?\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u001b\u001a\u0004\b@\u0010\u001d¨\u0006A"}, d2 = {"Lcom/discord/analytics/generated/events/TrackAndroidHardwareSurvey;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "deviceIs64Bit", "Ljava/lang/Boolean;", "getDeviceIs64Bit", "()Ljava/lang/Boolean;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "testDurationMs", "Ljava/lang/Long;", "getTestDurationMs", "()Ljava/lang/Long;", "", "deviceBrand", "Ljava/lang/CharSequence;", "getDeviceBrand", "()Ljava/lang/CharSequence;", "memoryClass", "getMemoryClass", "decoders720", "getDecoders720", "largeMemoryClass", "getLargeMemoryClass", "deviceSupportedAbis", "getDeviceSupportedAbis", "deviceModel", "getDeviceModel", "hardwareDecoders480", "getHardwareDecoders480", "decoders480", "getDecoders480", "deviceBoard", "getDeviceBoard", "isLowRamDevice", "hardwareDecoders720", "getHardwareDecoders720", "deviceManufacturer", "getDeviceManufacturer", "deviceProduct", "getDeviceProduct", "hardwareDecoders1080", "getHardwareDecoders1080", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "decoders1080", "getDecoders1080", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackAndroidHardwareSurvey implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence deviceBoard = null;
    private final CharSequence deviceBrand = null;
    private final CharSequence deviceManufacturer = null;
    private final CharSequence deviceModel = null;
    private final CharSequence deviceProduct = null;
    private final CharSequence deviceSupportedAbis = null;
    private final Boolean deviceIs64Bit = null;
    private final Boolean isLowRamDevice = null;
    private final Long memoryClass = null;
    private final Long largeMemoryClass = null;
    private final Long hardwareDecoders1080 = null;
    private final Long hardwareDecoders720 = null;
    private final Long hardwareDecoders480 = null;
    private final Long decoders1080 = null;
    private final Long decoders720 = null;
    private final Long decoders480 = null;
    private final Long testDurationMs = null;
    private final transient String analyticsSchemaTypeName = "android_hardware_survey";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackAndroidHardwareSurvey)) {
            return false;
        }
        TrackAndroidHardwareSurvey trackAndroidHardwareSurvey = (TrackAndroidHardwareSurvey) obj;
        return m.areEqual(this.deviceBoard, trackAndroidHardwareSurvey.deviceBoard) && m.areEqual(this.deviceBrand, trackAndroidHardwareSurvey.deviceBrand) && m.areEqual(this.deviceManufacturer, trackAndroidHardwareSurvey.deviceManufacturer) && m.areEqual(this.deviceModel, trackAndroidHardwareSurvey.deviceModel) && m.areEqual(this.deviceProduct, trackAndroidHardwareSurvey.deviceProduct) && m.areEqual(this.deviceSupportedAbis, trackAndroidHardwareSurvey.deviceSupportedAbis) && m.areEqual(this.deviceIs64Bit, trackAndroidHardwareSurvey.deviceIs64Bit) && m.areEqual(this.isLowRamDevice, trackAndroidHardwareSurvey.isLowRamDevice) && m.areEqual(this.memoryClass, trackAndroidHardwareSurvey.memoryClass) && m.areEqual(this.largeMemoryClass, trackAndroidHardwareSurvey.largeMemoryClass) && m.areEqual(this.hardwareDecoders1080, trackAndroidHardwareSurvey.hardwareDecoders1080) && m.areEqual(this.hardwareDecoders720, trackAndroidHardwareSurvey.hardwareDecoders720) && m.areEqual(this.hardwareDecoders480, trackAndroidHardwareSurvey.hardwareDecoders480) && m.areEqual(this.decoders1080, trackAndroidHardwareSurvey.decoders1080) && m.areEqual(this.decoders720, trackAndroidHardwareSurvey.decoders720) && m.areEqual(this.decoders480, trackAndroidHardwareSurvey.decoders480) && m.areEqual(this.testDurationMs, trackAndroidHardwareSurvey.testDurationMs);
    }

    public int hashCode() {
        CharSequence charSequence = this.deviceBoard;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.deviceBrand;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.deviceManufacturer;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.deviceModel;
        int hashCode4 = (hashCode3 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.deviceProduct;
        int hashCode5 = (hashCode4 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.deviceSupportedAbis;
        int hashCode6 = (hashCode5 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Boolean bool = this.deviceIs64Bit;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.isLowRamDevice;
        int hashCode8 = (hashCode7 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l = this.memoryClass;
        int hashCode9 = (hashCode8 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.largeMemoryClass;
        int hashCode10 = (hashCode9 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.hardwareDecoders1080;
        int hashCode11 = (hashCode10 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.hardwareDecoders720;
        int hashCode12 = (hashCode11 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.hardwareDecoders480;
        int hashCode13 = (hashCode12 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.decoders1080;
        int hashCode14 = (hashCode13 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.decoders720;
        int hashCode15 = (hashCode14 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.decoders480;
        int hashCode16 = (hashCode15 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.testDurationMs;
        if (l9 != null) {
            i = l9.hashCode();
        }
        return hashCode16 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackAndroidHardwareSurvey(deviceBoard=");
        R.append(this.deviceBoard);
        R.append(", deviceBrand=");
        R.append(this.deviceBrand);
        R.append(", deviceManufacturer=");
        R.append(this.deviceManufacturer);
        R.append(", deviceModel=");
        R.append(this.deviceModel);
        R.append(", deviceProduct=");
        R.append(this.deviceProduct);
        R.append(", deviceSupportedAbis=");
        R.append(this.deviceSupportedAbis);
        R.append(", deviceIs64Bit=");
        R.append(this.deviceIs64Bit);
        R.append(", isLowRamDevice=");
        R.append(this.isLowRamDevice);
        R.append(", memoryClass=");
        R.append(this.memoryClass);
        R.append(", largeMemoryClass=");
        R.append(this.largeMemoryClass);
        R.append(", hardwareDecoders1080=");
        R.append(this.hardwareDecoders1080);
        R.append(", hardwareDecoders720=");
        R.append(this.hardwareDecoders720);
        R.append(", hardwareDecoders480=");
        R.append(this.hardwareDecoders480);
        R.append(", decoders1080=");
        R.append(this.decoders1080);
        R.append(", decoders720=");
        R.append(this.decoders720);
        R.append(", decoders480=");
        R.append(this.decoders480);
        R.append(", testDurationMs=");
        return a.F(R, this.testDurationMs, ")");
    }
}
