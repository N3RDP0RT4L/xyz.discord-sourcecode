package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackPaymentRefunded.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0019\n\u0002\u0018\u0002\n\u0002\b&\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R$\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0010\u001a\u0004\b \u0010\u0012R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010#\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u001c\u001a\u0004\b$\u0010\u001eR\u001b\u0010%\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u001c\u001a\u0004\b&\u0010\u001eR\u001b\u0010'\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b'\u0010)R\u001b\u0010*\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0010\u001a\u0004\b+\u0010\u0012R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012R\u001b\u00100\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0010\u001a\u0004\b1\u0010\u0012R\u001b\u00102\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0010\u001a\u0004\b3\u0010\u0012R!\u00105\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`48\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0010\u001a\u0004\b6\u0010\u0012R\u001b\u00107\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0010\u001a\u0004\b8\u0010\u0012R\u001b\u00109\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0010\u001a\u0004\b:\u0010\u0012R\u001c\u0010;\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010\u0005R\u001b\u0010>\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0010\u001a\u0004\b?\u0010\u0012R\u001b\u0010@\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u001c\u001a\u0004\bA\u0010\u001eR\u001b\u0010B\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0010\u001a\u0004\bC\u0010\u0012R\u001b\u0010D\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010(\u001a\u0004\bE\u0010)R!\u0010F\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`48\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u0010\u001a\u0004\bG\u0010\u0012R\u001b\u0010H\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u001c\u001a\u0004\bI\u0010\u001eR\u001b\u0010J\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010\u0010\u001a\u0004\bK\u0010\u0012R!\u0010L\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`48\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010\u0010\u001a\u0004\bM\u0010\u0012R\u001b\u0010N\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010\u001c\u001a\u0004\bO\u0010\u001eR\u001b\u0010P\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010(\u001a\u0004\bQ\u0010)R\u001b\u0010R\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010\u0010\u001a\u0004\bS\u0010\u0012R\u001b\u0010T\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010\u0010\u001a\u0004\bU\u0010\u0012R!\u0010V\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`48\u0006@\u0006¢\u0006\f\n\u0004\bV\u0010\u0010\u001a\u0004\bW\u0010\u0012R\u001b\u0010X\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010(\u001a\u0004\bY\u0010)¨\u0006Z"}, d2 = {"Lcom/discord/analytics/generated/events/TrackPaymentRefunded;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "subscriptionPlanId", "Ljava/lang/Long;", "getSubscriptionPlanId", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "currency", "Ljava/lang/CharSequence;", "getCurrency", "()Ljava/lang/CharSequence;", "tax", "getTax", "paymentSourceId", "getPaymentSourceId", "failureMessage", "getFailureMessage", "paymentSourceCountry", "getPaymentSourceCountry", "isGift", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "skuSubscriptionPlanId", "getSkuSubscriptionPlanId", "amountRefunded", "getAmountRefunded", "refunderType", "getRefunderType", "paymentGateway", "getPaymentGateway", "subscriptionType", "getSubscriptionType", "Lcom/discord/primitives/Timestamp;", "subscriptionCurrentPeriodStart", "getSubscriptionCurrentPeriodStart", "refundedBy", "getRefundedBy", "skuId", "getSkuId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "paymentId", "getPaymentId", "paymentType", "getPaymentType", "skuType", "getSkuType", "taxInclusive", "getTaxInclusive", "createdAt", "getCreatedAt", "refundReason", "getRefundReason", "subscriptionId", "getSubscriptionId", "subscriptionCurrentPeriodEnd", "getSubscriptionCurrentPeriodEnd", "subscriptionPaymentGatewayPlanId", "getSubscriptionPaymentGatewayPlanId", "captured", "getCaptured", "price", "getPrice", "amount", "getAmount", "subscriptionCreatedAt", "getSubscriptionCreatedAt", "refundedOutOfPolicy", "getRefundedOutOfPolicy", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackPaymentRefunded implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long paymentId = null;
    private final Long createdAt = null;
    private final CharSequence paymentType = null;
    private final Long paymentGateway = null;
    private final Long price = null;
    private final CharSequence currency = null;
    private final Long amount = null;
    private final Long amountRefunded = null;
    private final Long tax = null;
    private final Boolean taxInclusive = null;
    private final Long skuId = null;
    private final Long skuType = null;
    private final Long skuSubscriptionPlanId = null;
    private final Long subscriptionId = null;
    private final Long subscriptionType = null;
    private final CharSequence subscriptionPaymentGatewayPlanId = null;
    private final Long subscriptionPlanId = null;
    private final Long subscriptionCreatedAt = null;
    private final Long subscriptionCurrentPeriodStart = null;
    private final Long subscriptionCurrentPeriodEnd = null;
    private final Long paymentSourceId = null;
    private final CharSequence paymentSourceCountry = null;
    private final CharSequence failureMessage = null;
    private final Boolean isGift = null;
    private final CharSequence refundReason = null;
    private final Boolean captured = null;
    private final Long refunderType = null;
    private final Long refundedBy = null;
    private final Boolean refundedOutOfPolicy = null;
    private final transient String analyticsSchemaTypeName = "payment_refunded";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackPaymentRefunded)) {
            return false;
        }
        TrackPaymentRefunded trackPaymentRefunded = (TrackPaymentRefunded) obj;
        return m.areEqual(this.paymentId, trackPaymentRefunded.paymentId) && m.areEqual(this.createdAt, trackPaymentRefunded.createdAt) && m.areEqual(this.paymentType, trackPaymentRefunded.paymentType) && m.areEqual(this.paymentGateway, trackPaymentRefunded.paymentGateway) && m.areEqual(this.price, trackPaymentRefunded.price) && m.areEqual(this.currency, trackPaymentRefunded.currency) && m.areEqual(this.amount, trackPaymentRefunded.amount) && m.areEqual(this.amountRefunded, trackPaymentRefunded.amountRefunded) && m.areEqual(this.tax, trackPaymentRefunded.tax) && m.areEqual(this.taxInclusive, trackPaymentRefunded.taxInclusive) && m.areEqual(this.skuId, trackPaymentRefunded.skuId) && m.areEqual(this.skuType, trackPaymentRefunded.skuType) && m.areEqual(this.skuSubscriptionPlanId, trackPaymentRefunded.skuSubscriptionPlanId) && m.areEqual(this.subscriptionId, trackPaymentRefunded.subscriptionId) && m.areEqual(this.subscriptionType, trackPaymentRefunded.subscriptionType) && m.areEqual(this.subscriptionPaymentGatewayPlanId, trackPaymentRefunded.subscriptionPaymentGatewayPlanId) && m.areEqual(this.subscriptionPlanId, trackPaymentRefunded.subscriptionPlanId) && m.areEqual(this.subscriptionCreatedAt, trackPaymentRefunded.subscriptionCreatedAt) && m.areEqual(this.subscriptionCurrentPeriodStart, trackPaymentRefunded.subscriptionCurrentPeriodStart) && m.areEqual(this.subscriptionCurrentPeriodEnd, trackPaymentRefunded.subscriptionCurrentPeriodEnd) && m.areEqual(this.paymentSourceId, trackPaymentRefunded.paymentSourceId) && m.areEqual(this.paymentSourceCountry, trackPaymentRefunded.paymentSourceCountry) && m.areEqual(this.failureMessage, trackPaymentRefunded.failureMessage) && m.areEqual(this.isGift, trackPaymentRefunded.isGift) && m.areEqual(this.refundReason, trackPaymentRefunded.refundReason) && m.areEqual(this.captured, trackPaymentRefunded.captured) && m.areEqual(this.refunderType, trackPaymentRefunded.refunderType) && m.areEqual(this.refundedBy, trackPaymentRefunded.refundedBy) && m.areEqual(this.refundedOutOfPolicy, trackPaymentRefunded.refundedOutOfPolicy);
    }

    public int hashCode() {
        Long l = this.paymentId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.createdAt;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.paymentType;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l3 = this.paymentGateway;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.price;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.currency;
        int hashCode6 = (hashCode5 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l5 = this.amount;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.amountRefunded;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.tax;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Boolean bool = this.taxInclusive;
        int hashCode10 = (hashCode9 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l8 = this.skuId;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.skuType;
        int hashCode12 = (hashCode11 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.skuSubscriptionPlanId;
        int hashCode13 = (hashCode12 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.subscriptionId;
        int hashCode14 = (hashCode13 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.subscriptionType;
        int hashCode15 = (hashCode14 + (l12 != null ? l12.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.subscriptionPaymentGatewayPlanId;
        int hashCode16 = (hashCode15 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l13 = this.subscriptionPlanId;
        int hashCode17 = (hashCode16 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.subscriptionCreatedAt;
        int hashCode18 = (hashCode17 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.subscriptionCurrentPeriodStart;
        int hashCode19 = (hashCode18 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.subscriptionCurrentPeriodEnd;
        int hashCode20 = (hashCode19 + (l16 != null ? l16.hashCode() : 0)) * 31;
        Long l17 = this.paymentSourceId;
        int hashCode21 = (hashCode20 + (l17 != null ? l17.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.paymentSourceCountry;
        int hashCode22 = (hashCode21 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.failureMessage;
        int hashCode23 = (hashCode22 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Boolean bool2 = this.isGift;
        int hashCode24 = (hashCode23 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.refundReason;
        int hashCode25 = (hashCode24 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Boolean bool3 = this.captured;
        int hashCode26 = (hashCode25 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l18 = this.refunderType;
        int hashCode27 = (hashCode26 + (l18 != null ? l18.hashCode() : 0)) * 31;
        Long l19 = this.refundedBy;
        int hashCode28 = (hashCode27 + (l19 != null ? l19.hashCode() : 0)) * 31;
        Boolean bool4 = this.refundedOutOfPolicy;
        if (bool4 != null) {
            i = bool4.hashCode();
        }
        return hashCode28 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackPaymentRefunded(paymentId=");
        R.append(this.paymentId);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", paymentType=");
        R.append(this.paymentType);
        R.append(", paymentGateway=");
        R.append(this.paymentGateway);
        R.append(", price=");
        R.append(this.price);
        R.append(", currency=");
        R.append(this.currency);
        R.append(", amount=");
        R.append(this.amount);
        R.append(", amountRefunded=");
        R.append(this.amountRefunded);
        R.append(", tax=");
        R.append(this.tax);
        R.append(", taxInclusive=");
        R.append(this.taxInclusive);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", skuType=");
        R.append(this.skuType);
        R.append(", skuSubscriptionPlanId=");
        R.append(this.skuSubscriptionPlanId);
        R.append(", subscriptionId=");
        R.append(this.subscriptionId);
        R.append(", subscriptionType=");
        R.append(this.subscriptionType);
        R.append(", subscriptionPaymentGatewayPlanId=");
        R.append(this.subscriptionPaymentGatewayPlanId);
        R.append(", subscriptionPlanId=");
        R.append(this.subscriptionPlanId);
        R.append(", subscriptionCreatedAt=");
        R.append(this.subscriptionCreatedAt);
        R.append(", subscriptionCurrentPeriodStart=");
        R.append(this.subscriptionCurrentPeriodStart);
        R.append(", subscriptionCurrentPeriodEnd=");
        R.append(this.subscriptionCurrentPeriodEnd);
        R.append(", paymentSourceId=");
        R.append(this.paymentSourceId);
        R.append(", paymentSourceCountry=");
        R.append(this.paymentSourceCountry);
        R.append(", failureMessage=");
        R.append(this.failureMessage);
        R.append(", isGift=");
        R.append(this.isGift);
        R.append(", refundReason=");
        R.append(this.refundReason);
        R.append(", captured=");
        R.append(this.captured);
        R.append(", refunderType=");
        R.append(this.refunderType);
        R.append(", refundedBy=");
        R.append(this.refundedBy);
        R.append(", refundedOutOfPolicy=");
        return a.C(R, this.refundedOutOfPolicy, ")");
    }
}
