package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackAcceptedInstantInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0012\n\u0002\u0010\r\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0014\u001a\u0004\b\u0018\u0010\u0016R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u001a\u0010\u0016R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0014\u001a\u0004\b\u001c\u0010\u0016R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0014\u001a\u0004\b\u001e\u0010\u0016R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0014\u001a\u0004\b \u0010\u0016R\u001b\u0010!\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0014\u001a\u0004\b\"\u0010\u0016R\u001b\u0010#\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0014\u001a\u0004\b$\u0010\u0016R\u001b\u0010&\u001a\u0004\u0018\u00010%8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u001b\u0010*\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0014\u001a\u0004\b+\u0010\u0016R\u001b\u0010,\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0014\u001a\u0004\b-\u0010\u0016R\u001b\u0010.\u001a\u0004\u0018\u00010%8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010'\u001a\u0004\b/\u0010)R\u001b\u00100\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0014\u001a\u0004\b1\u0010\u0016R\u001b\u00102\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u000f\u001a\u0004\b3\u0010\u0011R\u001b\u00104\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0014\u001a\u0004\b5\u0010\u0016R$\u00107\u001a\u0004\u0018\u0001068\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b7\u00108\u001a\u0004\b9\u0010:\"\u0004\b;\u0010<R\u001b\u0010=\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0014\u001a\u0004\b>\u0010\u0016R\u001b\u0010?\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0014\u001a\u0004\b@\u0010\u0016R\u001b\u0010A\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0014\u001a\u0004\bB\u0010\u0016R\u001b\u0010C\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0014\u001a\u0004\bD\u0010\u0016R\u001b\u0010E\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0014\u001a\u0004\bF\u0010\u0016R\u001b\u0010G\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0014\u001a\u0004\bH\u0010\u0016R\u001c\u0010I\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bI\u0010J\u001a\u0004\bK\u0010\u0005R\u001b\u0010L\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010\u0014\u001a\u0004\bM\u0010\u0016¨\u0006N"}, d2 = {"Lcom/discord/analytics/generated/events/TrackAcceptedInstantInvite;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "custom", "Ljava/lang/Boolean;", "getCustom", "()Ljava/lang/Boolean;", "", "locationChannelType", "Ljava/lang/Long;", "getLocationChannelType", "()Ljava/lang/Long;", "guildSizeTotal", "getGuildSizeTotal", "guildVerificationLevel", "getGuildVerificationLevel", "sizeTotal", "getSizeTotal", "userDay", "getUserDay", "channelType", "getChannelType", "userGuilds", "getUserGuilds", "locationChannelId", "getLocationChannelId", "", "invite", "Ljava/lang/CharSequence;", "getInvite", "()Ljava/lang/CharSequence;", "sizeOnline", "getSizeOnline", "guildSizeOnline", "getGuildSizeOnline", "inviteType", "getInviteType", "destinationUserId", "getDestinationUserId", "hasMutualGuild", "getHasMutualGuild", "locationMessageId", "getLocationMessageId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "ownerId", "getOwnerId", "locationGuildId", "getLocationGuildId", "inviteGuildScheduledEventId", "getInviteGuildScheduledEventId", "inviter", "getInviter", "guild", "getGuild", "channel", "getChannel", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "guildOwner", "getGuildOwner", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackAcceptedInstantInvite implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long channel = null;
    private final Long channelType = null;
    private final Boolean custom = null;
    private final Long guild = null;
    private final Long guildOwner = null;
    private final Long ownerId = null;
    private final CharSequence invite = null;
    private final Long inviter = null;
    private final Long userDay = null;
    private final Long userGuilds = null;
    private final Long locationGuildId = null;
    private final Long locationChannelId = null;
    private final Long locationChannelType = null;
    private final Long locationMessageId = null;
    private final Long guildSizeTotal = null;
    private final Long guildSizeOnline = null;
    private final Long sizeTotal = null;
    private final Long sizeOnline = null;
    private final CharSequence inviteType = null;
    private final Long destinationUserId = null;
    private final Long guildVerificationLevel = null;
    private final Boolean hasMutualGuild = null;
    private final Long inviteGuildScheduledEventId = null;
    private final transient String analyticsSchemaTypeName = "accepted_instant_invite";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackAcceptedInstantInvite)) {
            return false;
        }
        TrackAcceptedInstantInvite trackAcceptedInstantInvite = (TrackAcceptedInstantInvite) obj;
        return m.areEqual(this.channel, trackAcceptedInstantInvite.channel) && m.areEqual(this.channelType, trackAcceptedInstantInvite.channelType) && m.areEqual(this.custom, trackAcceptedInstantInvite.custom) && m.areEqual(this.guild, trackAcceptedInstantInvite.guild) && m.areEqual(this.guildOwner, trackAcceptedInstantInvite.guildOwner) && m.areEqual(this.ownerId, trackAcceptedInstantInvite.ownerId) && m.areEqual(this.invite, trackAcceptedInstantInvite.invite) && m.areEqual(this.inviter, trackAcceptedInstantInvite.inviter) && m.areEqual(this.userDay, trackAcceptedInstantInvite.userDay) && m.areEqual(this.userGuilds, trackAcceptedInstantInvite.userGuilds) && m.areEqual(this.locationGuildId, trackAcceptedInstantInvite.locationGuildId) && m.areEqual(this.locationChannelId, trackAcceptedInstantInvite.locationChannelId) && m.areEqual(this.locationChannelType, trackAcceptedInstantInvite.locationChannelType) && m.areEqual(this.locationMessageId, trackAcceptedInstantInvite.locationMessageId) && m.areEqual(this.guildSizeTotal, trackAcceptedInstantInvite.guildSizeTotal) && m.areEqual(this.guildSizeOnline, trackAcceptedInstantInvite.guildSizeOnline) && m.areEqual(this.sizeTotal, trackAcceptedInstantInvite.sizeTotal) && m.areEqual(this.sizeOnline, trackAcceptedInstantInvite.sizeOnline) && m.areEqual(this.inviteType, trackAcceptedInstantInvite.inviteType) && m.areEqual(this.destinationUserId, trackAcceptedInstantInvite.destinationUserId) && m.areEqual(this.guildVerificationLevel, trackAcceptedInstantInvite.guildVerificationLevel) && m.areEqual(this.hasMutualGuild, trackAcceptedInstantInvite.hasMutualGuild) && m.areEqual(this.inviteGuildScheduledEventId, trackAcceptedInstantInvite.inviteGuildScheduledEventId);
    }

    public int hashCode() {
        Long l = this.channel;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Boolean bool = this.custom;
        int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l3 = this.guild;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.guildOwner;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.ownerId;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        CharSequence charSequence = this.invite;
        int hashCode7 = (hashCode6 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l6 = this.inviter;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.userDay;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.userGuilds;
        int hashCode10 = (hashCode9 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.locationGuildId;
        int hashCode11 = (hashCode10 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.locationChannelId;
        int hashCode12 = (hashCode11 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.locationChannelType;
        int hashCode13 = (hashCode12 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.locationMessageId;
        int hashCode14 = (hashCode13 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.guildSizeTotal;
        int hashCode15 = (hashCode14 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.guildSizeOnline;
        int hashCode16 = (hashCode15 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.sizeTotal;
        int hashCode17 = (hashCode16 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.sizeOnline;
        int hashCode18 = (hashCode17 + (l16 != null ? l16.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.inviteType;
        int hashCode19 = (hashCode18 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l17 = this.destinationUserId;
        int hashCode20 = (hashCode19 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.guildVerificationLevel;
        int hashCode21 = (hashCode20 + (l18 != null ? l18.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasMutualGuild;
        int hashCode22 = (hashCode21 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l19 = this.inviteGuildScheduledEventId;
        if (l19 != null) {
            i = l19.hashCode();
        }
        return hashCode22 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackAcceptedInstantInvite(channel=");
        R.append(this.channel);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", custom=");
        R.append(this.custom);
        R.append(", guild=");
        R.append(this.guild);
        R.append(", guildOwner=");
        R.append(this.guildOwner);
        R.append(", ownerId=");
        R.append(this.ownerId);
        R.append(", invite=");
        R.append(this.invite);
        R.append(", inviter=");
        R.append(this.inviter);
        R.append(", userDay=");
        R.append(this.userDay);
        R.append(", userGuilds=");
        R.append(this.userGuilds);
        R.append(", locationGuildId=");
        R.append(this.locationGuildId);
        R.append(", locationChannelId=");
        R.append(this.locationChannelId);
        R.append(", locationChannelType=");
        R.append(this.locationChannelType);
        R.append(", locationMessageId=");
        R.append(this.locationMessageId);
        R.append(", guildSizeTotal=");
        R.append(this.guildSizeTotal);
        R.append(", guildSizeOnline=");
        R.append(this.guildSizeOnline);
        R.append(", sizeTotal=");
        R.append(this.sizeTotal);
        R.append(", sizeOnline=");
        R.append(this.sizeOnline);
        R.append(", inviteType=");
        R.append(this.inviteType);
        R.append(", destinationUserId=");
        R.append(this.destinationUserId);
        R.append(", guildVerificationLevel=");
        R.append(this.guildVerificationLevel);
        R.append(", hasMutualGuild=");
        R.append(this.hasMutualGuild);
        R.append(", inviteGuildScheduledEventId=");
        return a.F(R, this.inviteGuildScheduledEventId, ")");
    }
}
