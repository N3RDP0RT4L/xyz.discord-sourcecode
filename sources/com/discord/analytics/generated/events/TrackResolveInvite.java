package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackResolveInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u000f\u001a\u0004\b\u0013\u0010\u0011R\u001c\u0010\u0014\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0005R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010!\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0019\u001a\u0004\b\"\u0010\u001bR\u001b\u0010#\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u001e\u001a\u0004\b$\u0010 R\u001b\u0010%\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u000f\u001a\u0004\b&\u0010\u0011R\u001b\u0010'\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0019\u001a\u0004\b(\u0010\u001bR\u001b\u0010)\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0019\u001a\u0004\b*\u0010\u001bR\u001b\u0010+\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0019\u001a\u0004\b,\u0010\u001bR\u001b\u0010-\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0019\u001a\u0004\b.\u0010\u001bR\u001b\u0010/\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u001e\u001a\u0004\b0\u0010 R\u001b\u00101\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u001e\u001a\u0004\b2\u0010 R\u001b\u00103\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0019\u001a\u0004\b4\u0010\u001bR$\u00106\u001a\u0004\u0018\u0001058\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b6\u00107\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R\u001b\u0010<\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0019\u001a\u0004\b=\u0010\u001b¨\u0006>"}, d2 = {"Lcom/discord/analytics/generated/events/TrackResolveInvite;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "userBanned", "Ljava/lang/Boolean;", "getUserBanned", "()Ljava/lang/Boolean;", "authenticated", "getAuthenticated", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "channelId", "Ljava/lang/Long;", "getChannelId", "()Ljava/lang/Long;", "", "inviteType", "Ljava/lang/CharSequence;", "getInviteType", "()Ljava/lang/CharSequence;", "sizeTotal", "getSizeTotal", "inputValue", "getInputValue", "resolved", "getResolved", "channelType", "getChannelType", "sizeOnline", "getSizeOnline", "inviterId", "getInviterId", "guildId", "getGuildId", ModelAuditLogEntry.CHANGE_KEY_CODE, "getCode", "errorMessage", "getErrorMessage", "errorCode", "getErrorCode", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "destinationUserId", "getDestinationUserId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackResolveInvite implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Boolean resolved = null;
    private final CharSequence code = null;
    private final Boolean authenticated = null;
    private final Long guildId = null;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long inviterId = null;
    private final Long sizeTotal = null;
    private final Long sizeOnline = null;
    private final CharSequence inviteType = null;
    private final Long destinationUserId = null;
    private final Boolean userBanned = null;
    private final CharSequence inputValue = null;
    private final Long errorCode = null;
    private final CharSequence errorMessage = null;
    private final transient String analyticsSchemaTypeName = "resolve_invite";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackResolveInvite)) {
            return false;
        }
        TrackResolveInvite trackResolveInvite = (TrackResolveInvite) obj;
        return m.areEqual(this.resolved, trackResolveInvite.resolved) && m.areEqual(this.code, trackResolveInvite.code) && m.areEqual(this.authenticated, trackResolveInvite.authenticated) && m.areEqual(this.guildId, trackResolveInvite.guildId) && m.areEqual(this.channelId, trackResolveInvite.channelId) && m.areEqual(this.channelType, trackResolveInvite.channelType) && m.areEqual(this.inviterId, trackResolveInvite.inviterId) && m.areEqual(this.sizeTotal, trackResolveInvite.sizeTotal) && m.areEqual(this.sizeOnline, trackResolveInvite.sizeOnline) && m.areEqual(this.inviteType, trackResolveInvite.inviteType) && m.areEqual(this.destinationUserId, trackResolveInvite.destinationUserId) && m.areEqual(this.userBanned, trackResolveInvite.userBanned) && m.areEqual(this.inputValue, trackResolveInvite.inputValue) && m.areEqual(this.errorCode, trackResolveInvite.errorCode) && m.areEqual(this.errorMessage, trackResolveInvite.errorMessage);
    }

    public int hashCode() {
        Boolean bool = this.resolved;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        CharSequence charSequence = this.code;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool2 = this.authenticated;
        int hashCode3 = (hashCode2 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l = this.guildId;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channelType;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.inviterId;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.sizeTotal;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.sizeOnline;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.inviteType;
        int hashCode10 = (hashCode9 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l7 = this.destinationUserId;
        int hashCode11 = (hashCode10 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Boolean bool3 = this.userBanned;
        int hashCode12 = (hashCode11 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.inputValue;
        int hashCode13 = (hashCode12 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l8 = this.errorCode;
        int hashCode14 = (hashCode13 + (l8 != null ? l8.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.errorMessage;
        if (charSequence4 != null) {
            i = charSequence4.hashCode();
        }
        return hashCode14 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackResolveInvite(resolved=");
        R.append(this.resolved);
        R.append(", code=");
        R.append(this.code);
        R.append(", authenticated=");
        R.append(this.authenticated);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", inviterId=");
        R.append(this.inviterId);
        R.append(", sizeTotal=");
        R.append(this.sizeTotal);
        R.append(", sizeOnline=");
        R.append(this.sizeOnline);
        R.append(", inviteType=");
        R.append(this.inviteType);
        R.append(", destinationUserId=");
        R.append(this.destinationUserId);
        R.append(", userBanned=");
        R.append(this.userBanned);
        R.append(", inputValue=");
        R.append(this.inputValue);
        R.append(", errorCode=");
        R.append(this.errorCode);
        R.append(", errorMessage=");
        return a.D(R, this.errorMessage, ")");
    }
}
