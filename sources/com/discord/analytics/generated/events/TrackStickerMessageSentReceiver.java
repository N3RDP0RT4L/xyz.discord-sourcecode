package com.discord.analytics.generated.events;

import com.discord.api.science.AnalyticsSchema;
import kotlin.Metadata;
/* compiled from: TrackStickerMessageSent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001¨\u0006\u0002"}, d2 = {"Lcom/discord/analytics/generated/events/TrackStickerMessageSentReceiver;", "Lcom/discord/api/science/AnalyticsSchema;", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public interface TrackStickerMessageSentReceiver extends AnalyticsSchema {
}
