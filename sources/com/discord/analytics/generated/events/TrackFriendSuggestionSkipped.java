package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackFriendSuggestionSkipped.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\r\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u000f\u001a\u0004\b\u0012\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000f\u001a\u0004\b\u0014\u0010\u0011R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000f\u001a\u0004\b\u001a\u0010\u0011R\u001c\u0010\u001b\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0005R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0017\u001a\u0004\b\u001f\u0010\u0019R$\u0010!\u001a\u0004\u0018\u00010 8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\u001b\u0010'\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u000f\u001a\u0004\b(\u0010\u0011R\u001b\u0010)\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u000f\u001a\u0004\b*\u0010\u0011R\u001b\u0010+\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0017\u001a\u0004\b,\u0010\u0019R\u001b\u0010-\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0017\u001a\u0004\b.\u0010\u0019R\u001b\u00100\u001a\u0004\u0018\u00010/8\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103¨\u00064"}, d2 = {"Lcom/discord/analytics/generated/events/TrackFriendSuggestionSkipped;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "otherUserConsents", "Ljava/lang/Boolean;", "getOtherUserConsents", "()Ljava/lang/Boolean;", "isNonMutual", "userConsents", "getUserConsents", "", "userAllowedInSuggestions", "Ljava/lang/Long;", "getUserAllowedInSuggestions", "()Ljava/lang/Long;", "isReverseSuggestion", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "otherUserAllowedInSuggestions", "getOtherUserAllowedInSuggestions", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "userIsDiscoverable", "getUserIsDiscoverable", "otherUserDiscoverable", "getOtherUserDiscoverable", "existingRelationshipType", "getExistingRelationshipType", "suggestedUserId", "getSuggestedUserId", "", "platformType", "Ljava/lang/CharSequence;", "getPlatformType", "()Ljava/lang/CharSequence;", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackFriendSuggestionSkipped implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long suggestedUserId = null;
    private final CharSequence platformType = null;
    private final Boolean isNonMutual = null;
    private final Boolean isReverseSuggestion = null;
    private final Long existingRelationshipType = null;
    private final Long userAllowedInSuggestions = null;
    private final Boolean userConsents = null;
    private final Boolean userIsDiscoverable = null;
    private final Long otherUserAllowedInSuggestions = null;
    private final Boolean otherUserConsents = null;
    private final Boolean otherUserDiscoverable = null;
    private final transient String analyticsSchemaTypeName = "friend_suggestion_skipped";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackFriendSuggestionSkipped)) {
            return false;
        }
        TrackFriendSuggestionSkipped trackFriendSuggestionSkipped = (TrackFriendSuggestionSkipped) obj;
        return m.areEqual(this.suggestedUserId, trackFriendSuggestionSkipped.suggestedUserId) && m.areEqual(this.platformType, trackFriendSuggestionSkipped.platformType) && m.areEqual(this.isNonMutual, trackFriendSuggestionSkipped.isNonMutual) && m.areEqual(this.isReverseSuggestion, trackFriendSuggestionSkipped.isReverseSuggestion) && m.areEqual(this.existingRelationshipType, trackFriendSuggestionSkipped.existingRelationshipType) && m.areEqual(this.userAllowedInSuggestions, trackFriendSuggestionSkipped.userAllowedInSuggestions) && m.areEqual(this.userConsents, trackFriendSuggestionSkipped.userConsents) && m.areEqual(this.userIsDiscoverable, trackFriendSuggestionSkipped.userIsDiscoverable) && m.areEqual(this.otherUserAllowedInSuggestions, trackFriendSuggestionSkipped.otherUserAllowedInSuggestions) && m.areEqual(this.otherUserConsents, trackFriendSuggestionSkipped.otherUserConsents) && m.areEqual(this.otherUserDiscoverable, trackFriendSuggestionSkipped.otherUserDiscoverable);
    }

    public int hashCode() {
        Long l = this.suggestedUserId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.platformType;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool = this.isNonMutual;
        int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.isReverseSuggestion;
        int hashCode4 = (hashCode3 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l2 = this.existingRelationshipType;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.userAllowedInSuggestions;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool3 = this.userConsents;
        int hashCode7 = (hashCode6 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.userIsDiscoverable;
        int hashCode8 = (hashCode7 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l4 = this.otherUserAllowedInSuggestions;
        int hashCode9 = (hashCode8 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool5 = this.otherUserConsents;
        int hashCode10 = (hashCode9 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.otherUserDiscoverable;
        if (bool6 != null) {
            i = bool6.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackFriendSuggestionSkipped(suggestedUserId=");
        R.append(this.suggestedUserId);
        R.append(", platformType=");
        R.append(this.platformType);
        R.append(", isNonMutual=");
        R.append(this.isNonMutual);
        R.append(", isReverseSuggestion=");
        R.append(this.isReverseSuggestion);
        R.append(", existingRelationshipType=");
        R.append(this.existingRelationshipType);
        R.append(", userAllowedInSuggestions=");
        R.append(this.userAllowedInSuggestions);
        R.append(", userConsents=");
        R.append(this.userConsents);
        R.append(", userIsDiscoverable=");
        R.append(this.userIsDiscoverable);
        R.append(", otherUserAllowedInSuggestions=");
        R.append(this.otherUserAllowedInSuggestions);
        R.append(", otherUserConsents=");
        R.append(this.otherUserConsents);
        R.append(", otherUserDiscoverable=");
        return a.C(R, this.otherUserDiscoverable, ")");
    }
}
