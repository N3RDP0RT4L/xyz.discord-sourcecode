package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackVoiceQuality.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0007\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0010\u001a\u0004\b\u0017\u0010\u0012R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0010\u001a\u0004\b\u0019\u0010\u0012R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012R$\u0010&\u001a\u0004\u0018\u00010%8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012R\u001b\u0010/\u001a\u0004\u0018\u00010.8\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R\u001b\u00103\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0010\u001a\u0004\b4\u0010\u0012R\u001b\u00105\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0010\u001a\u0004\b6\u0010\u0012¨\u00067"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVoiceQuality;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "tier", "Ljava/lang/Long;", "getTier", "()Ljava/lang/Long;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "minimumPing", "getMinimumPing", "averagePing", "getAveragePing", "duration", "getDuration", "channelId", "getChannelId", "", "sessionId", "Ljava/lang/CharSequence;", "getSessionId", "()Ljava/lang/CharSequence;", "guildId", "getGuildId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "maximumPing", "getMaximumPing", "", "quality", "Ljava/lang/Float;", "getQuality", "()Ljava/lang/Float;", "speaker", "getSpeaker", "previousTier", "getPreviousTier", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVoiceQuality implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long averagePing = null;
    private final Long channelId = null;
    private final Long duration = null;
    private final Long guildId = null;
    private final Long maximumPing = null;
    private final Long minimumPing = null;
    private final Long previousTier = null;
    private final Float quality = null;
    private final CharSequence sessionId = null;
    private final Long speaker = null;
    private final Long tier = null;
    private final transient String analyticsSchemaTypeName = "voice_quality";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVoiceQuality)) {
            return false;
        }
        TrackVoiceQuality trackVoiceQuality = (TrackVoiceQuality) obj;
        return m.areEqual(this.averagePing, trackVoiceQuality.averagePing) && m.areEqual(this.channelId, trackVoiceQuality.channelId) && m.areEqual(this.duration, trackVoiceQuality.duration) && m.areEqual(this.guildId, trackVoiceQuality.guildId) && m.areEqual(this.maximumPing, trackVoiceQuality.maximumPing) && m.areEqual(this.minimumPing, trackVoiceQuality.minimumPing) && m.areEqual(this.previousTier, trackVoiceQuality.previousTier) && m.areEqual(this.quality, trackVoiceQuality.quality) && m.areEqual(this.sessionId, trackVoiceQuality.sessionId) && m.areEqual(this.speaker, trackVoiceQuality.speaker) && m.areEqual(this.tier, trackVoiceQuality.tier);
    }

    public int hashCode() {
        Long l = this.averagePing;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.duration;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.guildId;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.maximumPing;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.minimumPing;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.previousTier;
        int hashCode7 = (hashCode6 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Float f = this.quality;
        int hashCode8 = (hashCode7 + (f != null ? f.hashCode() : 0)) * 31;
        CharSequence charSequence = this.sessionId;
        int hashCode9 = (hashCode8 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l8 = this.speaker;
        int hashCode10 = (hashCode9 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.tier;
        if (l9 != null) {
            i = l9.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVoiceQuality(averagePing=");
        R.append(this.averagePing);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", duration=");
        R.append(this.duration);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", maximumPing=");
        R.append(this.maximumPing);
        R.append(", minimumPing=");
        R.append(this.minimumPing);
        R.append(", previousTier=");
        R.append(this.previousTier);
        R.append(", quality=");
        R.append(this.quality);
        R.append(", sessionId=");
        R.append(this.sessionId);
        R.append(", speaker=");
        R.append(this.speaker);
        R.append(", tier=");
        return a.F(R, this.tier, ")");
    }
}
