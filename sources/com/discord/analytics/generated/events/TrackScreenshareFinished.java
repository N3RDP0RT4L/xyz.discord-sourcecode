package com.discord.analytics.generated.events;

import androidx.appcompat.widget.ActivityChooserModel;
import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackScreenshareFinished.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0010\u0007\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0015\u001a\u0004\b\u0019\u0010\u0017R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0015\u001a\u0004\b\u001d\u0010\u0017R$\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001b\u0010%\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0015\u001a\u0004\b&\u0010\u0017R\u001b\u0010'\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0015\u001a\u0004\b(\u0010\u0017R\u001b\u0010)\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0010\u001a\u0004\b*\u0010\u0012R\u001b\u0010+\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0010\u001a\u0004\b,\u0010\u0012R\u001b\u0010-\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0010\u001a\u0004\b.\u0010\u0012R\u001b\u0010/\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0015\u001a\u0004\b0\u0010\u0017R\u001b\u00101\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0015\u001a\u0004\b2\u0010\u0017R\u001b\u00103\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0015\u001a\u0004\b4\u0010\u0017R\u001b\u00105\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0015\u001a\u0004\b6\u0010\u0017R\u001b\u00107\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0015\u001a\u0004\b8\u0010\u0017R\u001b\u00109\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0010\u001a\u0004\b:\u0010\u0012R\u001b\u0010;\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0010\u001a\u0004\b<\u0010\u0012R\u001b\u0010=\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0010\u001a\u0004\b>\u0010\u0012R\u001b\u0010@\u001a\u0004\u0018\u00010?8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010A\u001a\u0004\bB\u0010CR\u001c\u0010D\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bD\u0010E\u001a\u0004\bF\u0010\u0005R\u001b\u0010G\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0010\u001a\u0004\bH\u0010\u0012R\u001b\u0010I\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u0015\u001a\u0004\bJ\u0010\u0017R\u001b\u0010K\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\u0010\u001a\u0004\bL\u0010\u0012R\u001b\u0010M\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010\u0015\u001a\u0004\bN\u0010\u0017R\u001b\u0010O\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010\u0015\u001a\u0004\bP\u0010\u0017R\u001b\u0010Q\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u0010\u001a\u0004\bR\u0010\u0012R\u001b\u0010S\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010\u0015\u001a\u0004\bT\u0010\u0017¨\u0006U"}, d2 = {"Lcom/discord/analytics/generated/events/TrackScreenshareFinished;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "desktopCapturerType", "Ljava/lang/CharSequence;", "getDesktopCapturerType", "()Ljava/lang/CharSequence;", "", "gpuCount", "Ljava/lang/Long;", "getGpuCount", "()Ljava/lang/Long;", "guildId", "getGuildId", "cpu", "getCpu", "shareGameId", "getShareGameId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "screenshareFrames", "getScreenshareFrames", "hybridVideohookFrames", "getHybridVideohookFrames", "soundshareSession", "getSoundshareSession", "shareGameName", "getShareGameName", "cpuBrand", "getCpuBrand", "channelId", "getChannelId", "hybridDxgiFrames", "getHybridDxgiFrames", "quartzFrames", "getQuartzFrames", "videohookFrames", "getVideohookFrames", "screens", "getScreens", "rtcConnectionId", "getRtcConnectionId", "mediaSessionId", "getMediaSessionId", "cpuVendor", "getCpuVendor", "", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Ljava/lang/Float;", "getActivity", "()Ljava/lang/Float;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "context", "getContext", "gpuMemory", "getGpuMemory", "gpuBrand", "getGpuBrand", "hybridGdiFrames", "getHybridGdiFrames", "cpuMemory", "getCpuMemory", "gpu", "getGpu", "windows", "getWindows", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackScreenshareFinished implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long guildId = null;
    private final Long channelId = null;
    private final CharSequence context = null;
    private final Long screenshareFrames = null;
    private final Long videohookFrames = null;
    private final Long hybridDxgiFrames = null;
    private final Long hybridGdiFrames = null;
    private final Long hybridVideohookFrames = null;
    private final CharSequence soundshareSession = null;
    private final CharSequence shareGameName = null;
    private final Long shareGameId = null;
    private final CharSequence desktopCapturerType = null;
    private final Long screens = null;
    private final Long windows = null;
    private final CharSequence cpu = null;
    private final CharSequence gpu = null;
    private final CharSequence cpuBrand = null;
    private final CharSequence cpuVendor = null;
    private final Long cpuMemory = null;
    private final CharSequence gpuBrand = null;
    private final Long gpuCount = null;
    private final Long gpuMemory = null;
    private final CharSequence mediaSessionId = null;
    private final CharSequence rtcConnectionId = null;
    private final Long quartzFrames = null;
    private final Float activity = null;
    private final transient String analyticsSchemaTypeName = "screenshare_finished";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackScreenshareFinished)) {
            return false;
        }
        TrackScreenshareFinished trackScreenshareFinished = (TrackScreenshareFinished) obj;
        return m.areEqual(this.guildId, trackScreenshareFinished.guildId) && m.areEqual(this.channelId, trackScreenshareFinished.channelId) && m.areEqual(this.context, trackScreenshareFinished.context) && m.areEqual(this.screenshareFrames, trackScreenshareFinished.screenshareFrames) && m.areEqual(this.videohookFrames, trackScreenshareFinished.videohookFrames) && m.areEqual(this.hybridDxgiFrames, trackScreenshareFinished.hybridDxgiFrames) && m.areEqual(this.hybridGdiFrames, trackScreenshareFinished.hybridGdiFrames) && m.areEqual(this.hybridVideohookFrames, trackScreenshareFinished.hybridVideohookFrames) && m.areEqual(this.soundshareSession, trackScreenshareFinished.soundshareSession) && m.areEqual(this.shareGameName, trackScreenshareFinished.shareGameName) && m.areEqual(this.shareGameId, trackScreenshareFinished.shareGameId) && m.areEqual(this.desktopCapturerType, trackScreenshareFinished.desktopCapturerType) && m.areEqual(this.screens, trackScreenshareFinished.screens) && m.areEqual(this.windows, trackScreenshareFinished.windows) && m.areEqual(this.cpu, trackScreenshareFinished.cpu) && m.areEqual(this.gpu, trackScreenshareFinished.gpu) && m.areEqual(this.cpuBrand, trackScreenshareFinished.cpuBrand) && m.areEqual(this.cpuVendor, trackScreenshareFinished.cpuVendor) && m.areEqual(this.cpuMemory, trackScreenshareFinished.cpuMemory) && m.areEqual(this.gpuBrand, trackScreenshareFinished.gpuBrand) && m.areEqual(this.gpuCount, trackScreenshareFinished.gpuCount) && m.areEqual(this.gpuMemory, trackScreenshareFinished.gpuMemory) && m.areEqual(this.mediaSessionId, trackScreenshareFinished.mediaSessionId) && m.areEqual(this.rtcConnectionId, trackScreenshareFinished.rtcConnectionId) && m.areEqual(this.quartzFrames, trackScreenshareFinished.quartzFrames) && m.areEqual(this.activity, trackScreenshareFinished.activity);
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.context;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l3 = this.screenshareFrames;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.videohookFrames;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.hybridDxgiFrames;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.hybridGdiFrames;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.hybridVideohookFrames;
        int hashCode8 = (hashCode7 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.soundshareSession;
        int hashCode9 = (hashCode8 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.shareGameName;
        int hashCode10 = (hashCode9 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l8 = this.shareGameId;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.desktopCapturerType;
        int hashCode12 = (hashCode11 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l9 = this.screens;
        int hashCode13 = (hashCode12 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.windows;
        int hashCode14 = (hashCode13 + (l10 != null ? l10.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.cpu;
        int hashCode15 = (hashCode14 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.gpu;
        int hashCode16 = (hashCode15 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.cpuBrand;
        int hashCode17 = (hashCode16 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.cpuVendor;
        int hashCode18 = (hashCode17 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        Long l11 = this.cpuMemory;
        int hashCode19 = (hashCode18 + (l11 != null ? l11.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.gpuBrand;
        int hashCode20 = (hashCode19 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        Long l12 = this.gpuCount;
        int hashCode21 = (hashCode20 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.gpuMemory;
        int hashCode22 = (hashCode21 + (l13 != null ? l13.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.mediaSessionId;
        int hashCode23 = (hashCode22 + (charSequence10 != null ? charSequence10.hashCode() : 0)) * 31;
        CharSequence charSequence11 = this.rtcConnectionId;
        int hashCode24 = (hashCode23 + (charSequence11 != null ? charSequence11.hashCode() : 0)) * 31;
        Long l14 = this.quartzFrames;
        int hashCode25 = (hashCode24 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Float f = this.activity;
        if (f != null) {
            i = f.hashCode();
        }
        return hashCode25 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackScreenshareFinished(guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", context=");
        R.append(this.context);
        R.append(", screenshareFrames=");
        R.append(this.screenshareFrames);
        R.append(", videohookFrames=");
        R.append(this.videohookFrames);
        R.append(", hybridDxgiFrames=");
        R.append(this.hybridDxgiFrames);
        R.append(", hybridGdiFrames=");
        R.append(this.hybridGdiFrames);
        R.append(", hybridVideohookFrames=");
        R.append(this.hybridVideohookFrames);
        R.append(", soundshareSession=");
        R.append(this.soundshareSession);
        R.append(", shareGameName=");
        R.append(this.shareGameName);
        R.append(", shareGameId=");
        R.append(this.shareGameId);
        R.append(", desktopCapturerType=");
        R.append(this.desktopCapturerType);
        R.append(", screens=");
        R.append(this.screens);
        R.append(", windows=");
        R.append(this.windows);
        R.append(", cpu=");
        R.append(this.cpu);
        R.append(", gpu=");
        R.append(this.gpu);
        R.append(", cpuBrand=");
        R.append(this.cpuBrand);
        R.append(", cpuVendor=");
        R.append(this.cpuVendor);
        R.append(", cpuMemory=");
        R.append(this.cpuMemory);
        R.append(", gpuBrand=");
        R.append(this.gpuBrand);
        R.append(", gpuCount=");
        R.append(this.gpuCount);
        R.append(", gpuMemory=");
        R.append(this.gpuMemory);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", quartzFrames=");
        R.append(this.quartzFrames);
        R.append(", activity=");
        R.append(this.activity);
        R.append(")");
        return R.toString();
    }
}
