package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.analytics.generated.traits.TrackThread;
import com.discord.analytics.generated.traits.TrackThreadReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackThreadNotificationSettingsUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R!\u0010\u0017\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0018\u001a\u0004\b\u001c\u0010\u001aR\u001c\u0010\u001d\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u001b\u0010 \u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0012\u001a\u0004\b!\u0010\u0014R\u001b\u0010\"\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u001b\u0010&\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010#\u001a\u0004\b'\u0010%R$\u0010)\u001a\u0004\u0018\u00010(8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u001b\u0010/\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0018\u001a\u0004\b0\u0010\u001aR$\u00102\u001a\u0004\u0018\u0001018\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b2\u00103\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001b\u00108\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010#\u001a\u0004\b9\u0010%R\u001b\u0010:\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0018\u001a\u0004\b;\u0010\u001aR\u001b\u0010<\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0012\u001a\u0004\b=\u0010\u0014R\u001b\u0010>\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0018\u001a\u0004\b?\u0010\u001aR\u001b\u0010@\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010#\u001a\u0004\bA\u0010%R$\u0010C\u001a\u0004\u0018\u00010B8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bC\u0010D\u001a\u0004\bE\u0010F\"\u0004\bG\u0010HR!\u0010I\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u00168\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u0018\u001a\u0004\bJ\u0010\u001a¨\u0006K"}, d2 = {"Lcom/discord/analytics/generated/events/TrackThreadNotificationSettingsUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackThreadReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "parentNotificationSetting", "Ljava/lang/CharSequence;", "getParentNotificationSetting", "()Ljava/lang/CharSequence;", "", "Lcom/discord/primitives/Timestamp;", "oldThreadMutedUntil", "Ljava/lang/Long;", "getOldThreadMutedUntil", "()Ljava/lang/Long;", "channelType", "getChannelType", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "newThreadNotificationSetting", "getNewThreadNotificationSetting", "oldThreadIsMuted", "Ljava/lang/Boolean;", "getOldThreadIsMuted", "()Ljava/lang/Boolean;", "newThreadIsMuted", "getNewThreadIsMuted", "Lcom/discord/analytics/generated/traits/TrackThread;", "trackThread", "Lcom/discord/analytics/generated/traits/TrackThread;", "getTrackThread", "()Lcom/discord/analytics/generated/traits/TrackThread;", "setTrackThread", "(Lcom/discord/analytics/generated/traits/TrackThread;)V", "channelId", "getChannelId", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "hasInteractedWithThread", "getHasInteractedWithThread", "parentId", "getParentId", "oldThreadNotificationSetting", "getOldThreadNotificationSetting", "guildId", "getGuildId", "parentIsMuted", "getParentIsMuted", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "newThreadMutedUntil", "getNewThreadMutedUntil", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackThreadNotificationSettingsUpdated implements AnalyticsSchema, TrackBaseReceiver, TrackThreadReceiver, TrackLocationMetadataReceiver {
    private TrackBase trackBase;
    private TrackLocationMetadata trackLocationMetadata;
    private TrackThread trackThread;
    private final Long channelId = null;
    private final Long parentId = null;
    private final Long guildId = null;
    private final Long channelType = null;
    private final Boolean hasInteractedWithThread = null;
    private final Boolean parentIsMuted = null;
    private final CharSequence oldThreadNotificationSetting = null;
    private final CharSequence newThreadNotificationSetting = null;
    private final CharSequence parentNotificationSetting = null;
    private final Boolean oldThreadIsMuted = null;
    private final Boolean newThreadIsMuted = null;
    private final Long oldThreadMutedUntil = null;
    private final Long newThreadMutedUntil = null;
    private final transient String analyticsSchemaTypeName = "thread_notification_settings_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackThreadNotificationSettingsUpdated)) {
            return false;
        }
        TrackThreadNotificationSettingsUpdated trackThreadNotificationSettingsUpdated = (TrackThreadNotificationSettingsUpdated) obj;
        return m.areEqual(this.channelId, trackThreadNotificationSettingsUpdated.channelId) && m.areEqual(this.parentId, trackThreadNotificationSettingsUpdated.parentId) && m.areEqual(this.guildId, trackThreadNotificationSettingsUpdated.guildId) && m.areEqual(this.channelType, trackThreadNotificationSettingsUpdated.channelType) && m.areEqual(this.hasInteractedWithThread, trackThreadNotificationSettingsUpdated.hasInteractedWithThread) && m.areEqual(this.parentIsMuted, trackThreadNotificationSettingsUpdated.parentIsMuted) && m.areEqual(this.oldThreadNotificationSetting, trackThreadNotificationSettingsUpdated.oldThreadNotificationSetting) && m.areEqual(this.newThreadNotificationSetting, trackThreadNotificationSettingsUpdated.newThreadNotificationSetting) && m.areEqual(this.parentNotificationSetting, trackThreadNotificationSettingsUpdated.parentNotificationSetting) && m.areEqual(this.oldThreadIsMuted, trackThreadNotificationSettingsUpdated.oldThreadIsMuted) && m.areEqual(this.newThreadIsMuted, trackThreadNotificationSettingsUpdated.newThreadIsMuted) && m.areEqual(this.oldThreadMutedUntil, trackThreadNotificationSettingsUpdated.oldThreadMutedUntil) && m.areEqual(this.newThreadMutedUntil, trackThreadNotificationSettingsUpdated.newThreadMutedUntil);
    }

    public int hashCode() {
        Long l = this.channelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.parentId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.guildId;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.channelType;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool = this.hasInteractedWithThread;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.parentIsMuted;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.oldThreadNotificationSetting;
        int hashCode7 = (hashCode6 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.newThreadNotificationSetting;
        int hashCode8 = (hashCode7 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.parentNotificationSetting;
        int hashCode9 = (hashCode8 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Boolean bool3 = this.oldThreadIsMuted;
        int hashCode10 = (hashCode9 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.newThreadIsMuted;
        int hashCode11 = (hashCode10 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l5 = this.oldThreadMutedUntil;
        int hashCode12 = (hashCode11 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.newThreadMutedUntil;
        if (l6 != null) {
            i = l6.hashCode();
        }
        return hashCode12 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackThreadNotificationSettingsUpdated(channelId=");
        R.append(this.channelId);
        R.append(", parentId=");
        R.append(this.parentId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", hasInteractedWithThread=");
        R.append(this.hasInteractedWithThread);
        R.append(", parentIsMuted=");
        R.append(this.parentIsMuted);
        R.append(", oldThreadNotificationSetting=");
        R.append(this.oldThreadNotificationSetting);
        R.append(", newThreadNotificationSetting=");
        R.append(this.newThreadNotificationSetting);
        R.append(", parentNotificationSetting=");
        R.append(this.parentNotificationSetting);
        R.append(", oldThreadIsMuted=");
        R.append(this.oldThreadIsMuted);
        R.append(", newThreadIsMuted=");
        R.append(this.newThreadIsMuted);
        R.append(", oldThreadMutedUntil=");
        R.append(this.oldThreadMutedUntil);
        R.append(", newThreadMutedUntil=");
        return a.F(R, this.newThreadMutedUntil, ")");
    }
}
