package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackSubscriptionPlanUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\r\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR!\u0010\u0010\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0011\u001a\u0004\b\u0015\u0010\u0013R$\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR!\u0010\u001d\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0011\u001a\u0004\b\u001e\u0010\u0013R!\u0010\u001f\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0011\u001a\u0004\b \u0010\u0013R!\u0010!\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0011\u001a\u0004\b\"\u0010\u0013R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0011\u001a\u0004\b$\u0010\u0013R\u001c\u0010%\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010\u0005R\u001b\u0010(\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0011\u001a\u0004\b)\u0010\u0013R\u001b\u0010*\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0011\u001a\u0004\b+\u0010\u0013R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0011\u001a\u0004\b-\u0010\u0013R\u001b\u0010/\u001a\u0004\u0018\u00010.8\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R\u001b\u00103\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0011\u001a\u0004\b4\u0010\u0013R\u001b\u00105\u001a\u0004\u0018\u00010.8\u0006@\u0006¢\u0006\f\n\u0004\b5\u00100\u001a\u0004\b6\u00102R\u001b\u00107\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0011\u001a\u0004\b8\u0010\u0013R!\u00109\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0011\u001a\u0004\b:\u0010\u0013¨\u0006;"}, d2 = {"Lcom/discord/analytics/generated/events/TrackSubscriptionPlanUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/primitives/Timestamp;", "currentPeriodEnd", "Ljava/lang/Long;", "getCurrentPeriodEnd", "()Ljava/lang/Long;", "previousSubscriptionType", "getPreviousSubscriptionType", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "previousCurrentPeriodStart", "getPreviousCurrentPeriodStart", "currentPeriodStart", "getCurrentPeriodStart", "createdAt", "getCreatedAt", "planId", "getPlanId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "subscriptionType", "getSubscriptionType", "previousPlanId", "getPreviousPlanId", "paymentGateway", "getPaymentGateway", "", "previousPaymentGatewayPlanId", "Ljava/lang/CharSequence;", "getPreviousPaymentGatewayPlanId", "()Ljava/lang/CharSequence;", "subscriptionId", "getSubscriptionId", "paymentGatewayPlanId", "getPaymentGatewayPlanId", "previousSubscriptionId", "getPreviousSubscriptionId", "previousCurrentPeriodEnd", "getPreviousCurrentPeriodEnd", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSubscriptionPlanUpdated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long subscriptionId = null;
    private final Long subscriptionType = null;
    private final CharSequence paymentGatewayPlanId = null;
    private final Long planId = null;
    private final Long paymentGateway = null;
    private final Long createdAt = null;
    private final Long currentPeriodStart = null;
    private final Long currentPeriodEnd = null;
    private final Long previousSubscriptionId = null;
    private final Long previousSubscriptionType = null;
    private final CharSequence previousPaymentGatewayPlanId = null;
    private final Long previousPlanId = null;
    private final Long previousCurrentPeriodStart = null;
    private final Long previousCurrentPeriodEnd = null;
    private final transient String analyticsSchemaTypeName = "subscription_plan_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSubscriptionPlanUpdated)) {
            return false;
        }
        TrackSubscriptionPlanUpdated trackSubscriptionPlanUpdated = (TrackSubscriptionPlanUpdated) obj;
        return m.areEqual(this.subscriptionId, trackSubscriptionPlanUpdated.subscriptionId) && m.areEqual(this.subscriptionType, trackSubscriptionPlanUpdated.subscriptionType) && m.areEqual(this.paymentGatewayPlanId, trackSubscriptionPlanUpdated.paymentGatewayPlanId) && m.areEqual(this.planId, trackSubscriptionPlanUpdated.planId) && m.areEqual(this.paymentGateway, trackSubscriptionPlanUpdated.paymentGateway) && m.areEqual(this.createdAt, trackSubscriptionPlanUpdated.createdAt) && m.areEqual(this.currentPeriodStart, trackSubscriptionPlanUpdated.currentPeriodStart) && m.areEqual(this.currentPeriodEnd, trackSubscriptionPlanUpdated.currentPeriodEnd) && m.areEqual(this.previousSubscriptionId, trackSubscriptionPlanUpdated.previousSubscriptionId) && m.areEqual(this.previousSubscriptionType, trackSubscriptionPlanUpdated.previousSubscriptionType) && m.areEqual(this.previousPaymentGatewayPlanId, trackSubscriptionPlanUpdated.previousPaymentGatewayPlanId) && m.areEqual(this.previousPlanId, trackSubscriptionPlanUpdated.previousPlanId) && m.areEqual(this.previousCurrentPeriodStart, trackSubscriptionPlanUpdated.previousCurrentPeriodStart) && m.areEqual(this.previousCurrentPeriodEnd, trackSubscriptionPlanUpdated.previousCurrentPeriodEnd);
    }

    public int hashCode() {
        Long l = this.subscriptionId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.subscriptionType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.paymentGatewayPlanId;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l3 = this.planId;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.paymentGateway;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.createdAt;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.currentPeriodStart;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.currentPeriodEnd;
        int hashCode8 = (hashCode7 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.previousSubscriptionId;
        int hashCode9 = (hashCode8 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.previousSubscriptionType;
        int hashCode10 = (hashCode9 + (l9 != null ? l9.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.previousPaymentGatewayPlanId;
        int hashCode11 = (hashCode10 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l10 = this.previousPlanId;
        int hashCode12 = (hashCode11 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.previousCurrentPeriodStart;
        int hashCode13 = (hashCode12 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.previousCurrentPeriodEnd;
        if (l12 != null) {
            i = l12.hashCode();
        }
        return hashCode13 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSubscriptionPlanUpdated(subscriptionId=");
        R.append(this.subscriptionId);
        R.append(", subscriptionType=");
        R.append(this.subscriptionType);
        R.append(", paymentGatewayPlanId=");
        R.append(this.paymentGatewayPlanId);
        R.append(", planId=");
        R.append(this.planId);
        R.append(", paymentGateway=");
        R.append(this.paymentGateway);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", currentPeriodStart=");
        R.append(this.currentPeriodStart);
        R.append(", currentPeriodEnd=");
        R.append(this.currentPeriodEnd);
        R.append(", previousSubscriptionId=");
        R.append(this.previousSubscriptionId);
        R.append(", previousSubscriptionType=");
        R.append(this.previousSubscriptionType);
        R.append(", previousPaymentGatewayPlanId=");
        R.append(this.previousPaymentGatewayPlanId);
        R.append(", previousPlanId=");
        R.append(this.previousPlanId);
        R.append(", previousCurrentPeriodStart=");
        R.append(this.previousCurrentPeriodStart);
        R.append(", previousCurrentPeriodEnd=");
        return a.F(R, this.previousCurrentPeriodEnd, ")");
    }
}
