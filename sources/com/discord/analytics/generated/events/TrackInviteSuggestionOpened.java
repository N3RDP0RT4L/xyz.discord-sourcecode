package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackInviteSuggestionOpened.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R$\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001c\u0010\u001c\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u0005R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0010\u001a\u0004\b \u0010\u0012R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012¨\u0006%"}, d2 = {"Lcom/discord/analytics/generated/events/TrackInviteSuggestionOpened;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "numGroupDms", "getNumGroupDms", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "numDms", "getNumDms", "numFriends", "getNumFriends", "numSuggestions", "getNumSuggestions", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackInviteSuggestionOpened implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long numSuggestions = null;
    private final Long numFriends = null;
    private final Long numDms = null;
    private final Long numGroupDms = null;
    private final Long guildId = null;
    private final transient String analyticsSchemaTypeName = "invite_suggestion_opened";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackInviteSuggestionOpened)) {
            return false;
        }
        TrackInviteSuggestionOpened trackInviteSuggestionOpened = (TrackInviteSuggestionOpened) obj;
        return m.areEqual(this.numSuggestions, trackInviteSuggestionOpened.numSuggestions) && m.areEqual(this.numFriends, trackInviteSuggestionOpened.numFriends) && m.areEqual(this.numDms, trackInviteSuggestionOpened.numDms) && m.areEqual(this.numGroupDms, trackInviteSuggestionOpened.numGroupDms) && m.areEqual(this.guildId, trackInviteSuggestionOpened.guildId);
    }

    public int hashCode() {
        Long l = this.numSuggestions;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.numFriends;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.numDms;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.numGroupDms;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.guildId;
        if (l5 != null) {
            i = l5.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackInviteSuggestionOpened(numSuggestions=");
        R.append(this.numSuggestions);
        R.append(", numFriends=");
        R.append(this.numFriends);
        R.append(", numDms=");
        R.append(this.numDms);
        R.append(", numGroupDms=");
        R.append(this.numGroupDms);
        R.append(", guildId=");
        return a.F(R, this.guildId, ")");
    }
}
