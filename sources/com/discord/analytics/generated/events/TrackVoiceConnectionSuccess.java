package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackVoiceConnectionSuccess.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0017\u001a\u0004\b\u001b\u0010\u0019R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0017\u001a\u0004\b\u001d\u0010\u0019R\u001c\u0010\u001e\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010\u0005R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010#\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0017\u001a\u0004\b$\u0010\u0019R\u001b\u0010%\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0017\u001a\u0004\b&\u0010\u0019R\u001b\u0010'\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0017\u001a\u0004\b(\u0010\u0019R\u001b\u0010)\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0010\u001a\u0004\b*\u0010\u0012R\u001b\u0010+\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0017\u001a\u0004\b,\u0010\u0019R$\u0010.\u001a\u0004\u0018\u00010-8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b.\u0010/\u001a\u0004\b0\u00101\"\u0004\b2\u00103R\u001b\u00104\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0017\u001a\u0004\b5\u0010\u0019R\u001b\u00106\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0017\u001a\u0004\b7\u0010\u0019R\u001b\u00108\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0017\u001a\u0004\b9\u0010\u0019R\u001b\u0010:\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0010\u001a\u0004\b;\u0010\u0012R\u001b\u0010<\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0017\u001a\u0004\b=\u0010\u0019¨\u0006>"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVoiceConnectionSuccess;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "connectCount", "Ljava/lang/Long;", "getConnectCount", "()Ljava/lang/Long;", "channelId", "getChannelId", "", "hostname", "Ljava/lang/CharSequence;", "getHostname", "()Ljava/lang/CharSequence;", "voiceBackendVersion", "getVoiceBackendVersion", "audioLayer", "getAudioLayer", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "guildId", "getGuildId", "mediaSessionId", "getMediaSessionId", "rtcWorkerBackendVersion", "getRtcWorkerBackendVersion", "context", "getContext", "port", "getPort", "cloudflareBestRegion", "getCloudflareBestRegion", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "rtcConnectionId", "getRtcConnectionId", "sessionId", "getSessionId", "audioSubsystem", "getAudioSubsystem", "connectTime", "getConnectTime", "protocol", "getProtocol", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVoiceConnectionSuccess implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence hostname = null;
    private final CharSequence cloudflareBestRegion = null;
    private final Long port = null;
    private final CharSequence protocol = null;
    private final CharSequence sessionId = null;
    private final CharSequence mediaSessionId = null;
    private final Long connectTime = null;
    private final CharSequence rtcConnectionId = null;
    private final Long channelId = null;
    private final Long guildId = null;
    private final CharSequence context = null;
    private final Long connectCount = null;
    private final CharSequence audioSubsystem = null;
    private final CharSequence audioLayer = null;
    private final CharSequence voiceBackendVersion = null;
    private final CharSequence rtcWorkerBackendVersion = null;
    private final transient String analyticsSchemaTypeName = "voice_connection_success";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVoiceConnectionSuccess)) {
            return false;
        }
        TrackVoiceConnectionSuccess trackVoiceConnectionSuccess = (TrackVoiceConnectionSuccess) obj;
        return m.areEqual(this.hostname, trackVoiceConnectionSuccess.hostname) && m.areEqual(this.cloudflareBestRegion, trackVoiceConnectionSuccess.cloudflareBestRegion) && m.areEqual(this.port, trackVoiceConnectionSuccess.port) && m.areEqual(this.protocol, trackVoiceConnectionSuccess.protocol) && m.areEqual(this.sessionId, trackVoiceConnectionSuccess.sessionId) && m.areEqual(this.mediaSessionId, trackVoiceConnectionSuccess.mediaSessionId) && m.areEqual(this.connectTime, trackVoiceConnectionSuccess.connectTime) && m.areEqual(this.rtcConnectionId, trackVoiceConnectionSuccess.rtcConnectionId) && m.areEqual(this.channelId, trackVoiceConnectionSuccess.channelId) && m.areEqual(this.guildId, trackVoiceConnectionSuccess.guildId) && m.areEqual(this.context, trackVoiceConnectionSuccess.context) && m.areEqual(this.connectCount, trackVoiceConnectionSuccess.connectCount) && m.areEqual(this.audioSubsystem, trackVoiceConnectionSuccess.audioSubsystem) && m.areEqual(this.audioLayer, trackVoiceConnectionSuccess.audioLayer) && m.areEqual(this.voiceBackendVersion, trackVoiceConnectionSuccess.voiceBackendVersion) && m.areEqual(this.rtcWorkerBackendVersion, trackVoiceConnectionSuccess.rtcWorkerBackendVersion);
    }

    public int hashCode() {
        CharSequence charSequence = this.hostname;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.cloudflareBestRegion;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.port;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.protocol;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.sessionId;
        int hashCode5 = (hashCode4 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.mediaSessionId;
        int hashCode6 = (hashCode5 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l2 = this.connectTime;
        int hashCode7 = (hashCode6 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.rtcConnectionId;
        int hashCode8 = (hashCode7 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Long l3 = this.channelId;
        int hashCode9 = (hashCode8 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.guildId;
        int hashCode10 = (hashCode9 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.context;
        int hashCode11 = (hashCode10 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        Long l5 = this.connectCount;
        int hashCode12 = (hashCode11 + (l5 != null ? l5.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.audioSubsystem;
        int hashCode13 = (hashCode12 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.audioLayer;
        int hashCode14 = (hashCode13 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.voiceBackendVersion;
        int hashCode15 = (hashCode14 + (charSequence10 != null ? charSequence10.hashCode() : 0)) * 31;
        CharSequence charSequence11 = this.rtcWorkerBackendVersion;
        if (charSequence11 != null) {
            i = charSequence11.hashCode();
        }
        return hashCode15 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVoiceConnectionSuccess(hostname=");
        R.append(this.hostname);
        R.append(", cloudflareBestRegion=");
        R.append(this.cloudflareBestRegion);
        R.append(", port=");
        R.append(this.port);
        R.append(", protocol=");
        R.append(this.protocol);
        R.append(", sessionId=");
        R.append(this.sessionId);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", connectTime=");
        R.append(this.connectTime);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", context=");
        R.append(this.context);
        R.append(", connectCount=");
        R.append(this.connectCount);
        R.append(", audioSubsystem=");
        R.append(this.audioSubsystem);
        R.append(", audioLayer=");
        R.append(this.audioLayer);
        R.append(", voiceBackendVersion=");
        R.append(this.voiceBackendVersion);
        R.append(", rtcWorkerBackendVersion=");
        return a.D(R, this.rtcWorkerBackendVersion, ")");
    }
}
