package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackHardwareDetected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b1\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0017\u001a\u0004\b#\u0010\u0019R\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R\u001b\u0010&\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0010\u001a\u0004\b'\u0010\u0012R\u001b\u0010(\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0017\u001a\u0004\b)\u0010\u0019R\u001b\u0010*\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0010\u001a\u0004\b+\u0010\u0012R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012R\u001b\u00100\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R\u001b\u00104\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0010\u001a\u0004\b5\u0010\u0012R\u001c\u00106\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u0010\u0005R\u001b\u00109\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0010\u001a\u0004\b:\u0010\u0012R\u001b\u0010;\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0010\u001a\u0004\b<\u0010\u0012R\u001b\u0010=\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0017\u001a\u0004\b>\u0010\u0019R\u001b\u0010?\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0010\u001a\u0004\b@\u0010\u0012R\u001b\u0010A\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bA\u00101\u001a\u0004\bB\u00103R\u001b\u0010C\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0017\u001a\u0004\bD\u0010\u0019R\u001b\u0010E\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bE\u00101\u001a\u0004\bF\u00103R$\u0010H\u001a\u0004\u0018\u00010G8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bH\u0010I\u001a\u0004\bJ\u0010K\"\u0004\bL\u0010MR\u001b\u0010N\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bN\u00101\u001a\u0004\bO\u00103¨\u0006P"}, d2 = {"Lcom/discord/analytics/generated/events/TrackHardwareDetected;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "displayDesktopWidth", "Ljava/lang/Long;", "getDisplayDesktopWidth", "()Ljava/lang/Long;", "cameraCount", "getCameraCount", "", "cpuBrand", "Ljava/lang/CharSequence;", "getCpuBrand", "()Ljava/lang/CharSequence;", "displayPrimaryHeight", "getDisplayPrimaryHeight", "cpuMemory", "getCpuMemory", "gpuCount", "getGpuCount", "batteries", "getBatteries", "cpuVendor", "getCpuVendor", "displayPrimaryWidth", "getDisplayPrimaryWidth", "cameraDefaultProductId", "getCameraDefaultProductId", "cameraDefaultBus", "getCameraDefaultBus", "wave", "getWave", "cameraDefaultMaxWidth", "getCameraDefaultMaxWidth", "displayMonitors", "getDisplayMonitors", "cameraDefaultHasYuy2", "Ljava/lang/Boolean;", "getCameraDefaultHasYuy2", "()Ljava/lang/Boolean;", "cameraDefaultVendorId", "getCameraDefaultVendorId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "cameraDefaultMaxHeight", "getCameraDefaultMaxHeight", "displayDesktopHeight", "getDisplayDesktopHeight", "cameraDefaultName", "getCameraDefaultName", "gpuMemory", "getGpuMemory", "cameraDefaultHasI420", "getCameraDefaultHasI420", "gpuBrand", "getGpuBrand", "cameraDefaultHasMjpg", "getCameraDefaultHasMjpg", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "cameraDefaultHasNv12", "getCameraDefaultHasNv12", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackHardwareDetected implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long wave = null;
    private final CharSequence cpuBrand = null;
    private final CharSequence cpuVendor = null;
    private final Long cpuMemory = null;
    private final CharSequence gpuBrand = null;
    private final Long gpuCount = null;
    private final Long gpuMemory = null;
    private final Long batteries = null;
    private final Long displayMonitors = null;
    private final Long displayPrimaryWidth = null;
    private final Long displayPrimaryHeight = null;
    private final Long displayDesktopWidth = null;
    private final Long displayDesktopHeight = null;
    private final Long cameraCount = null;
    private final CharSequence cameraDefaultName = null;
    private final Long cameraDefaultMaxWidth = null;
    private final Long cameraDefaultMaxHeight = null;
    private final Boolean cameraDefaultHasI420 = null;
    private final Boolean cameraDefaultHasNv12 = null;
    private final Boolean cameraDefaultHasYuy2 = null;
    private final Boolean cameraDefaultHasMjpg = null;
    private final CharSequence cameraDefaultBus = null;
    private final Long cameraDefaultVendorId = null;
    private final Long cameraDefaultProductId = null;
    private final transient String analyticsSchemaTypeName = "hardware_detected";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackHardwareDetected)) {
            return false;
        }
        TrackHardwareDetected trackHardwareDetected = (TrackHardwareDetected) obj;
        return m.areEqual(this.wave, trackHardwareDetected.wave) && m.areEqual(this.cpuBrand, trackHardwareDetected.cpuBrand) && m.areEqual(this.cpuVendor, trackHardwareDetected.cpuVendor) && m.areEqual(this.cpuMemory, trackHardwareDetected.cpuMemory) && m.areEqual(this.gpuBrand, trackHardwareDetected.gpuBrand) && m.areEqual(this.gpuCount, trackHardwareDetected.gpuCount) && m.areEqual(this.gpuMemory, trackHardwareDetected.gpuMemory) && m.areEqual(this.batteries, trackHardwareDetected.batteries) && m.areEqual(this.displayMonitors, trackHardwareDetected.displayMonitors) && m.areEqual(this.displayPrimaryWidth, trackHardwareDetected.displayPrimaryWidth) && m.areEqual(this.displayPrimaryHeight, trackHardwareDetected.displayPrimaryHeight) && m.areEqual(this.displayDesktopWidth, trackHardwareDetected.displayDesktopWidth) && m.areEqual(this.displayDesktopHeight, trackHardwareDetected.displayDesktopHeight) && m.areEqual(this.cameraCount, trackHardwareDetected.cameraCount) && m.areEqual(this.cameraDefaultName, trackHardwareDetected.cameraDefaultName) && m.areEqual(this.cameraDefaultMaxWidth, trackHardwareDetected.cameraDefaultMaxWidth) && m.areEqual(this.cameraDefaultMaxHeight, trackHardwareDetected.cameraDefaultMaxHeight) && m.areEqual(this.cameraDefaultHasI420, trackHardwareDetected.cameraDefaultHasI420) && m.areEqual(this.cameraDefaultHasNv12, trackHardwareDetected.cameraDefaultHasNv12) && m.areEqual(this.cameraDefaultHasYuy2, trackHardwareDetected.cameraDefaultHasYuy2) && m.areEqual(this.cameraDefaultHasMjpg, trackHardwareDetected.cameraDefaultHasMjpg) && m.areEqual(this.cameraDefaultBus, trackHardwareDetected.cameraDefaultBus) && m.areEqual(this.cameraDefaultVendorId, trackHardwareDetected.cameraDefaultVendorId) && m.areEqual(this.cameraDefaultProductId, trackHardwareDetected.cameraDefaultProductId);
    }

    public int hashCode() {
        Long l = this.wave;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.cpuBrand;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.cpuVendor;
        int hashCode3 = (hashCode2 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l2 = this.cpuMemory;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.gpuBrand;
        int hashCode5 = (hashCode4 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l3 = this.gpuCount;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.gpuMemory;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.batteries;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.displayMonitors;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.displayPrimaryWidth;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.displayPrimaryHeight;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.displayDesktopWidth;
        int hashCode12 = (hashCode11 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.displayDesktopHeight;
        int hashCode13 = (hashCode12 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.cameraCount;
        int hashCode14 = (hashCode13 + (l11 != null ? l11.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.cameraDefaultName;
        int hashCode15 = (hashCode14 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l12 = this.cameraDefaultMaxWidth;
        int hashCode16 = (hashCode15 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.cameraDefaultMaxHeight;
        int hashCode17 = (hashCode16 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Boolean bool = this.cameraDefaultHasI420;
        int hashCode18 = (hashCode17 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.cameraDefaultHasNv12;
        int hashCode19 = (hashCode18 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.cameraDefaultHasYuy2;
        int hashCode20 = (hashCode19 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.cameraDefaultHasMjpg;
        int hashCode21 = (hashCode20 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.cameraDefaultBus;
        int hashCode22 = (hashCode21 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l14 = this.cameraDefaultVendorId;
        int hashCode23 = (hashCode22 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.cameraDefaultProductId;
        if (l15 != null) {
            i = l15.hashCode();
        }
        return hashCode23 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackHardwareDetected(wave=");
        R.append(this.wave);
        R.append(", cpuBrand=");
        R.append(this.cpuBrand);
        R.append(", cpuVendor=");
        R.append(this.cpuVendor);
        R.append(", cpuMemory=");
        R.append(this.cpuMemory);
        R.append(", gpuBrand=");
        R.append(this.gpuBrand);
        R.append(", gpuCount=");
        R.append(this.gpuCount);
        R.append(", gpuMemory=");
        R.append(this.gpuMemory);
        R.append(", batteries=");
        R.append(this.batteries);
        R.append(", displayMonitors=");
        R.append(this.displayMonitors);
        R.append(", displayPrimaryWidth=");
        R.append(this.displayPrimaryWidth);
        R.append(", displayPrimaryHeight=");
        R.append(this.displayPrimaryHeight);
        R.append(", displayDesktopWidth=");
        R.append(this.displayDesktopWidth);
        R.append(", displayDesktopHeight=");
        R.append(this.displayDesktopHeight);
        R.append(", cameraCount=");
        R.append(this.cameraCount);
        R.append(", cameraDefaultName=");
        R.append(this.cameraDefaultName);
        R.append(", cameraDefaultMaxWidth=");
        R.append(this.cameraDefaultMaxWidth);
        R.append(", cameraDefaultMaxHeight=");
        R.append(this.cameraDefaultMaxHeight);
        R.append(", cameraDefaultHasI420=");
        R.append(this.cameraDefaultHasI420);
        R.append(", cameraDefaultHasNv12=");
        R.append(this.cameraDefaultHasNv12);
        R.append(", cameraDefaultHasYuy2=");
        R.append(this.cameraDefaultHasYuy2);
        R.append(", cameraDefaultHasMjpg=");
        R.append(this.cameraDefaultHasMjpg);
        R.append(", cameraDefaultBus=");
        R.append(this.cameraDefaultBus);
        R.append(", cameraDefaultVendorId=");
        R.append(this.cameraDefaultVendorId);
        R.append(", cameraDefaultProductId=");
        return a.F(R, this.cameraDefaultProductId, ")");
    }
}
