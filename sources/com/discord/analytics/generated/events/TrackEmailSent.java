package com.discord.analytics.generated.events;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackEmail;
import com.discord.analytics.generated.traits.TrackEmailReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackEmailSent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0011\u001a\u0004\b\u0015\u0010\u0013R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0011\u001a\u0004\b\u0017\u0010\u0013R$\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001b\u0010 \u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001c\u0010$\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010\u0006R$\u0010(\u001a\u0004\u0018\u00010'8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001b\u0010.\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0011\u001a\u0004\b/\u0010\u0013R!\u00101\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`08\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0011\u001a\u0004\b2\u0010\u0013R\u001b\u00103\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010!\u001a\u0004\b4\u0010#¨\u00065"}, d2 = {"Lcom/discord/analytics/generated/events/TrackEmailSent;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackEmailReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "mmNumDmsMissed", "Ljava/lang/Long;", "getMmNumDmsMissed", "()Ljava/lang/Long;", "mmNumChannelsMissed", "getMmNumChannelsMissed", "mmNumGuildsMissed", "getMmNumGuildsMissed", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", NotificationCompat.CATEGORY_EMAIL, "Ljava/lang/CharSequence;", "getEmail", "()Ljava/lang/CharSequence;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "Lcom/discord/analytics/generated/traits/TrackEmail;", "trackEmail", "Lcom/discord/analytics/generated/traits/TrackEmail;", "getTrackEmail", "()Lcom/discord/analytics/generated/traits/TrackEmail;", "setTrackEmail", "(Lcom/discord/analytics/generated/traits/TrackEmail;)V", "mmNumMessagesMissed", "getMmNumMessagesMissed", "Lcom/discord/primitives/Timestamp;", "userLastActive", "getUserLastActive", "emailDomain", "getEmailDomain", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackEmailSent implements AnalyticsSchema, TrackBaseReceiver, TrackEmailReceiver {
    private TrackBase trackBase;
    private TrackEmail trackEmail;
    private final CharSequence email = null;
    private final CharSequence emailDomain = null;
    private final Long mmNumGuildsMissed = null;
    private final Long mmNumChannelsMissed = null;
    private final Long mmNumDmsMissed = null;
    private final Long mmNumMessagesMissed = null;
    private final Long userLastActive = null;
    private final transient String analyticsSchemaTypeName = "email_sent";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackEmailSent)) {
            return false;
        }
        TrackEmailSent trackEmailSent = (TrackEmailSent) obj;
        return m.areEqual(this.email, trackEmailSent.email) && m.areEqual(this.emailDomain, trackEmailSent.emailDomain) && m.areEqual(this.mmNumGuildsMissed, trackEmailSent.mmNumGuildsMissed) && m.areEqual(this.mmNumChannelsMissed, trackEmailSent.mmNumChannelsMissed) && m.areEqual(this.mmNumDmsMissed, trackEmailSent.mmNumDmsMissed) && m.areEqual(this.mmNumMessagesMissed, trackEmailSent.mmNumMessagesMissed) && m.areEqual(this.userLastActive, trackEmailSent.userLastActive);
    }

    public int hashCode() {
        CharSequence charSequence = this.email;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.emailDomain;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.mmNumGuildsMissed;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.mmNumChannelsMissed;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.mmNumDmsMissed;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.mmNumMessagesMissed;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.userLastActive;
        if (l5 != null) {
            i = l5.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackEmailSent(email=");
        R.append(this.email);
        R.append(", emailDomain=");
        R.append(this.emailDomain);
        R.append(", mmNumGuildsMissed=");
        R.append(this.mmNumGuildsMissed);
        R.append(", mmNumChannelsMissed=");
        R.append(this.mmNumChannelsMissed);
        R.append(", mmNumDmsMissed=");
        R.append(this.mmNumDmsMissed);
        R.append(", mmNumMessagesMissed=");
        R.append(this.mmNumMessagesMissed);
        R.append(", userLastActive=");
        return a.F(R, this.userLastActive, ")");
    }
}
