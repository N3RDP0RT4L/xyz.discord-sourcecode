package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackMessageEdited.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b1\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\r\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R!\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0019\u001a\u0004\b\u001d\u0010\u001bR!\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0015\u001a\u0004\b\u001f\u0010\u0017R\u001b\u0010 \u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u000f\u001a\u0004\b!\u0010\u0011R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0019\u001a\u0004\b#\u0010\u001bR!\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0015\u001a\u0004\b%\u0010\u0017R\u001b\u0010&\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0019\u001a\u0004\b'\u0010\u001bR\u001b\u0010(\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0019\u001a\u0004\b)\u0010\u001bR\u001b\u0010*\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0019\u001a\u0004\b+\u0010\u001bR\u001b\u0010,\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0019\u001a\u0004\b-\u0010\u001bR\u001b\u0010.\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0019\u001a\u0004\b/\u0010\u001bR\u001b\u00100\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0019\u001a\u0004\b1\u0010\u001bR\u001b\u00102\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0019\u001a\u0004\b3\u0010\u001bR\u001b\u00104\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0019\u001a\u0004\b5\u0010\u001bR\u001b\u00106\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0019\u001a\u0004\b7\u0010\u001bR\u001c\u00108\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010\u0005R\u001b\u0010;\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0019\u001a\u0004\b<\u0010\u001bR\u001b\u0010=\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0019\u001a\u0004\b>\u0010\u001bR\u001b\u0010?\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0019\u001a\u0004\b@\u0010\u001bR\u001b\u0010A\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0019\u001a\u0004\bB\u0010\u001bR\u001b\u0010C\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0019\u001a\u0004\bD\u0010\u001bR$\u0010F\u001a\u0004\u0018\u00010E8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bF\u0010G\u001a\u0004\bH\u0010I\"\u0004\bJ\u0010KR\u001b\u0010L\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010\u0019\u001a\u0004\bM\u0010\u001bR!\u0010N\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010\u0015\u001a\u0004\bO\u0010\u0017R\u001b\u0010P\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010\u0019\u001a\u0004\bQ\u0010\u001bR\u001b\u0010R\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010\u0019\u001a\u0004\bS\u0010\u001bR\u001b\u0010T\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010\u0019\u001a\u0004\bU\u0010\u001bR\u001b\u0010V\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bV\u0010\u000f\u001a\u0004\bW\u0010\u0011R\u001b\u0010X\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010\u0019\u001a\u0004\bY\u0010\u001bR\u001b\u0010[\u001a\u0004\u0018\u00010Z8\u0006@\u0006¢\u0006\f\n\u0004\b[\u0010\\\u001a\u0004\b]\u0010^R\u001b\u0010_\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b_\u0010\u0019\u001a\u0004\b`\u0010\u001bR\u001b\u0010a\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\ba\u0010\u000f\u001a\u0004\bb\u0010\u0011¨\u0006c"}, d2 = {"Lcom/discord/analytics/generated/events/TrackMessageEdited;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "private", "Ljava/lang/Boolean;", "getPrivate", "()Ljava/lang/Boolean;", "", "", "mentionIds", "Ljava/util/List;", "getMentionIds", "()Ljava/util/List;", "emojiManagedExternal", "Ljava/lang/Long;", "getEmojiManagedExternal", "()Ljava/lang/Long;", "clientApplicationId", "getClientApplicationId", "attachmentIds", "getAttachmentIds", "probablyHasMarkdown", "getProbablyHasMarkdown", "messageId", "getMessageId", "stickerIds", "getStickerIds", "emojiUnicode", "getEmojiUnicode", "replyAgeSeconds", "getReplyAgeSeconds", "emojiManaged", "getEmojiManaged", "server", "getServer", "emojiCustomExternal", "getEmojiCustomExternal", "referenceMessageGuild", "getReferenceMessageGuild", "channel", "getChannel", "maxAttachmentSize", "getMaxAttachmentSize", "applicationId", "getApplicationId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "numAttachments", "getNumAttachments", "length", "getLength", "wordCount", "getWordCount", "referenceMessageChannel", "getReferenceMessageChannel", "emojiCustom", "getEmojiCustom", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "messageType", "getMessageType", "recipientIds", "getRecipientIds", "referenceMessageId", "getReferenceMessageId", "numEmbeds", "getNumEmbeds", "activityAction", "getActivityAction", "emojiOnly", "getEmojiOnly", "emojiAnimated", "getEmojiAnimated", "", "activityPartyPlatform", "Ljava/lang/CharSequence;", "getActivityPartyPlatform", "()Ljava/lang/CharSequence;", "channelType", "getChannelType", "hasSpoiler", "getHasSpoiler", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackMessageEdited implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long messageId = null;
    private final Long messageType = null;
    private final Long channel = null;
    private final Long channelType = null;

    /* renamed from: private  reason: not valid java name */
    private final Boolean f0private = null;
    private final Long server = null;
    private final Long numAttachments = null;
    private final Long maxAttachmentSize = null;
    private final List<Long> recipientIds = null;
    private final List<Long> mentionIds = null;
    private final Long length = null;
    private final Long wordCount = null;
    private final Long emojiUnicode = null;
    private final Long emojiCustom = null;
    private final Long emojiCustomExternal = null;
    private final Long emojiManaged = null;
    private final Long emojiManagedExternal = null;
    private final Long emojiAnimated = null;
    private final Boolean emojiOnly = null;
    private final Long numEmbeds = null;
    private final Long clientApplicationId = null;
    private final Long applicationId = null;
    private final List<Long> attachmentIds = null;
    private final Long activityAction = null;
    private final CharSequence activityPartyPlatform = null;
    private final Boolean hasSpoiler = null;
    private final Boolean probablyHasMarkdown = null;
    private final Long referenceMessageId = null;
    private final Long referenceMessageChannel = null;
    private final Long referenceMessageGuild = null;
    private final Long replyAgeSeconds = null;
    private final List<Long> stickerIds = null;
    private final transient String analyticsSchemaTypeName = "message_edited";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackMessageEdited)) {
            return false;
        }
        TrackMessageEdited trackMessageEdited = (TrackMessageEdited) obj;
        return m.areEqual(this.messageId, trackMessageEdited.messageId) && m.areEqual(this.messageType, trackMessageEdited.messageType) && m.areEqual(this.channel, trackMessageEdited.channel) && m.areEqual(this.channelType, trackMessageEdited.channelType) && m.areEqual(this.f0private, trackMessageEdited.f0private) && m.areEqual(this.server, trackMessageEdited.server) && m.areEqual(this.numAttachments, trackMessageEdited.numAttachments) && m.areEqual(this.maxAttachmentSize, trackMessageEdited.maxAttachmentSize) && m.areEqual(this.recipientIds, trackMessageEdited.recipientIds) && m.areEqual(this.mentionIds, trackMessageEdited.mentionIds) && m.areEqual(this.length, trackMessageEdited.length) && m.areEqual(this.wordCount, trackMessageEdited.wordCount) && m.areEqual(this.emojiUnicode, trackMessageEdited.emojiUnicode) && m.areEqual(this.emojiCustom, trackMessageEdited.emojiCustom) && m.areEqual(this.emojiCustomExternal, trackMessageEdited.emojiCustomExternal) && m.areEqual(this.emojiManaged, trackMessageEdited.emojiManaged) && m.areEqual(this.emojiManagedExternal, trackMessageEdited.emojiManagedExternal) && m.areEqual(this.emojiAnimated, trackMessageEdited.emojiAnimated) && m.areEqual(this.emojiOnly, trackMessageEdited.emojiOnly) && m.areEqual(this.numEmbeds, trackMessageEdited.numEmbeds) && m.areEqual(this.clientApplicationId, trackMessageEdited.clientApplicationId) && m.areEqual(this.applicationId, trackMessageEdited.applicationId) && m.areEqual(this.attachmentIds, trackMessageEdited.attachmentIds) && m.areEqual(this.activityAction, trackMessageEdited.activityAction) && m.areEqual(this.activityPartyPlatform, trackMessageEdited.activityPartyPlatform) && m.areEqual(this.hasSpoiler, trackMessageEdited.hasSpoiler) && m.areEqual(this.probablyHasMarkdown, trackMessageEdited.probablyHasMarkdown) && m.areEqual(this.referenceMessageId, trackMessageEdited.referenceMessageId) && m.areEqual(this.referenceMessageChannel, trackMessageEdited.referenceMessageChannel) && m.areEqual(this.referenceMessageGuild, trackMessageEdited.referenceMessageGuild) && m.areEqual(this.replyAgeSeconds, trackMessageEdited.replyAgeSeconds) && m.areEqual(this.stickerIds, trackMessageEdited.stickerIds);
    }

    public int hashCode() {
        Long l = this.messageId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.messageType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channel;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.channelType;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool = this.f0private;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l5 = this.server;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.numAttachments;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.maxAttachmentSize;
        int hashCode8 = (hashCode7 + (l7 != null ? l7.hashCode() : 0)) * 31;
        List<Long> list = this.recipientIds;
        int hashCode9 = (hashCode8 + (list != null ? list.hashCode() : 0)) * 31;
        List<Long> list2 = this.mentionIds;
        int hashCode10 = (hashCode9 + (list2 != null ? list2.hashCode() : 0)) * 31;
        Long l8 = this.length;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.wordCount;
        int hashCode12 = (hashCode11 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.emojiUnicode;
        int hashCode13 = (hashCode12 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.emojiCustom;
        int hashCode14 = (hashCode13 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.emojiCustomExternal;
        int hashCode15 = (hashCode14 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.emojiManaged;
        int hashCode16 = (hashCode15 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.emojiManagedExternal;
        int hashCode17 = (hashCode16 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.emojiAnimated;
        int hashCode18 = (hashCode17 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Boolean bool2 = this.emojiOnly;
        int hashCode19 = (hashCode18 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l16 = this.numEmbeds;
        int hashCode20 = (hashCode19 + (l16 != null ? l16.hashCode() : 0)) * 31;
        Long l17 = this.clientApplicationId;
        int hashCode21 = (hashCode20 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.applicationId;
        int hashCode22 = (hashCode21 + (l18 != null ? l18.hashCode() : 0)) * 31;
        List<Long> list3 = this.attachmentIds;
        int hashCode23 = (hashCode22 + (list3 != null ? list3.hashCode() : 0)) * 31;
        Long l19 = this.activityAction;
        int hashCode24 = (hashCode23 + (l19 != null ? l19.hashCode() : 0)) * 31;
        CharSequence charSequence = this.activityPartyPlatform;
        int hashCode25 = (hashCode24 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool3 = this.hasSpoiler;
        int hashCode26 = (hashCode25 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.probablyHasMarkdown;
        int hashCode27 = (hashCode26 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l20 = this.referenceMessageId;
        int hashCode28 = (hashCode27 + (l20 != null ? l20.hashCode() : 0)) * 31;
        Long l21 = this.referenceMessageChannel;
        int hashCode29 = (hashCode28 + (l21 != null ? l21.hashCode() : 0)) * 31;
        Long l22 = this.referenceMessageGuild;
        int hashCode30 = (hashCode29 + (l22 != null ? l22.hashCode() : 0)) * 31;
        Long l23 = this.replyAgeSeconds;
        int hashCode31 = (hashCode30 + (l23 != null ? l23.hashCode() : 0)) * 31;
        List<Long> list4 = this.stickerIds;
        if (list4 != null) {
            i = list4.hashCode();
        }
        return hashCode31 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackMessageEdited(messageId=");
        R.append(this.messageId);
        R.append(", messageType=");
        R.append(this.messageType);
        R.append(", channel=");
        R.append(this.channel);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", private=");
        R.append(this.f0private);
        R.append(", server=");
        R.append(this.server);
        R.append(", numAttachments=");
        R.append(this.numAttachments);
        R.append(", maxAttachmentSize=");
        R.append(this.maxAttachmentSize);
        R.append(", recipientIds=");
        R.append(this.recipientIds);
        R.append(", mentionIds=");
        R.append(this.mentionIds);
        R.append(", length=");
        R.append(this.length);
        R.append(", wordCount=");
        R.append(this.wordCount);
        R.append(", emojiUnicode=");
        R.append(this.emojiUnicode);
        R.append(", emojiCustom=");
        R.append(this.emojiCustom);
        R.append(", emojiCustomExternal=");
        R.append(this.emojiCustomExternal);
        R.append(", emojiManaged=");
        R.append(this.emojiManaged);
        R.append(", emojiManagedExternal=");
        R.append(this.emojiManagedExternal);
        R.append(", emojiAnimated=");
        R.append(this.emojiAnimated);
        R.append(", emojiOnly=");
        R.append(this.emojiOnly);
        R.append(", numEmbeds=");
        R.append(this.numEmbeds);
        R.append(", clientApplicationId=");
        R.append(this.clientApplicationId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", attachmentIds=");
        R.append(this.attachmentIds);
        R.append(", activityAction=");
        R.append(this.activityAction);
        R.append(", activityPartyPlatform=");
        R.append(this.activityPartyPlatform);
        R.append(", hasSpoiler=");
        R.append(this.hasSpoiler);
        R.append(", probablyHasMarkdown=");
        R.append(this.probablyHasMarkdown);
        R.append(", referenceMessageId=");
        R.append(this.referenceMessageId);
        R.append(", referenceMessageChannel=");
        R.append(this.referenceMessageChannel);
        R.append(", referenceMessageGuild=");
        R.append(this.referenceMessageGuild);
        R.append(", replyAgeSeconds=");
        R.append(this.replyAgeSeconds);
        R.append(", stickerIds=");
        return a.K(R, this.stickerIds, ")");
    }
}
