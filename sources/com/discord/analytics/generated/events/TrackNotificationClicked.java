package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadata;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNotificationClicked.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0015\u001a\u0004\b\u0019\u0010\u0017R$\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001b\u0010\"\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u001b\u0010&\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010#\u001a\u0004\b'\u0010%R\u001b\u0010(\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010#\u001a\u0004\b)\u0010%R\u001b\u0010*\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0015\u001a\u0004\b+\u0010\u0017R\u001b\u0010,\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0015\u001a\u0004\b-\u0010\u0017R\u001b\u0010.\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0015\u001a\u0004\b/\u0010\u0017R\u001b\u00100\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010#\u001a\u0004\b1\u0010%R\u001b\u00102\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0015\u001a\u0004\b3\u0010\u0017R\u001b\u00104\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0015\u001a\u0004\b5\u0010\u0017R$\u00107\u001a\u0004\u0018\u0001068\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b7\u00108\u001a\u0004\b9\u0010:\"\u0004\b;\u0010<R\u001b\u0010=\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0010\u001a\u0004\b>\u0010\u0012R\u001c\u0010?\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010\u0006R\u001b\u0010B\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0015\u001a\u0004\bC\u0010\u0017R\u001b\u0010D\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0015\u001a\u0004\bE\u0010\u0017R\u001b\u0010F\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u0015\u001a\u0004\bG\u0010\u0017¨\u0006H"}, d2 = {"Lcom/discord/analytics/generated/events/TrackNotificationClicked;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "hasMessage", "Ljava/lang/Boolean;", "getHasMessage", "()Ljava/lang/Boolean;", "", "channelType", "Ljava/lang/Long;", "getChannelType", "()Ljava/lang/Long;", "channelId", "getChannelId", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "trackOverlayClientMetadata", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "getTrackOverlayClientMetadata", "()Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "setTrackOverlayClientMetadata", "(Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;)V", "", "activityName", "Ljava/lang/CharSequence;", "getActivityName", "()Ljava/lang/CharSequence;", "actionType", "getActionType", "notifType", "getNotifType", "messageType", "getMessageType", "activityType", "getActivityType", "notifUserId", "getNotifUserId", "platformType", "getPlatformType", "relType", "getRelType", "messageDataSize", "getMessageDataSize", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "notifInApp", "getNotifInApp", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "pushDataSize", "getPushDataSize", "guildId", "getGuildId", "messageId", "getMessageId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNotificationClicked implements AnalyticsSchema, TrackBaseReceiver, TrackOverlayClientMetadataReceiver {
    private TrackBase trackBase;
    private TrackOverlayClientMetadata trackOverlayClientMetadata;
    private final Boolean notifInApp = null;
    private final CharSequence notifType = null;
    private final Long notifUserId = null;
    private final Long messageId = null;
    private final Long messageType = null;
    private final Boolean hasMessage = null;
    private final Long guildId = null;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long relType = null;
    private final CharSequence platformType = null;
    private final Long activityType = null;
    private final CharSequence activityName = null;
    private final CharSequence actionType = null;
    private final Long pushDataSize = null;
    private final Long messageDataSize = null;
    private final transient String analyticsSchemaTypeName = "notification_clicked";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNotificationClicked)) {
            return false;
        }
        TrackNotificationClicked trackNotificationClicked = (TrackNotificationClicked) obj;
        return m.areEqual(this.notifInApp, trackNotificationClicked.notifInApp) && m.areEqual(this.notifType, trackNotificationClicked.notifType) && m.areEqual(this.notifUserId, trackNotificationClicked.notifUserId) && m.areEqual(this.messageId, trackNotificationClicked.messageId) && m.areEqual(this.messageType, trackNotificationClicked.messageType) && m.areEqual(this.hasMessage, trackNotificationClicked.hasMessage) && m.areEqual(this.guildId, trackNotificationClicked.guildId) && m.areEqual(this.channelId, trackNotificationClicked.channelId) && m.areEqual(this.channelType, trackNotificationClicked.channelType) && m.areEqual(this.relType, trackNotificationClicked.relType) && m.areEqual(this.platformType, trackNotificationClicked.platformType) && m.areEqual(this.activityType, trackNotificationClicked.activityType) && m.areEqual(this.activityName, trackNotificationClicked.activityName) && m.areEqual(this.actionType, trackNotificationClicked.actionType) && m.areEqual(this.pushDataSize, trackNotificationClicked.pushDataSize) && m.areEqual(this.messageDataSize, trackNotificationClicked.messageDataSize);
    }

    public int hashCode() {
        Boolean bool = this.notifInApp;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        CharSequence charSequence = this.notifType;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l = this.notifUserId;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.messageId;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.messageType;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasMessage;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l4 = this.guildId;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.channelId;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.channelType;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.relType;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.platformType;
        int hashCode11 = (hashCode10 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l8 = this.activityType;
        int hashCode12 = (hashCode11 + (l8 != null ? l8.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.activityName;
        int hashCode13 = (hashCode12 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.actionType;
        int hashCode14 = (hashCode13 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l9 = this.pushDataSize;
        int hashCode15 = (hashCode14 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.messageDataSize;
        if (l10 != null) {
            i = l10.hashCode();
        }
        return hashCode15 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNotificationClicked(notifInApp=");
        R.append(this.notifInApp);
        R.append(", notifType=");
        R.append(this.notifType);
        R.append(", notifUserId=");
        R.append(this.notifUserId);
        R.append(", messageId=");
        R.append(this.messageId);
        R.append(", messageType=");
        R.append(this.messageType);
        R.append(", hasMessage=");
        R.append(this.hasMessage);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", relType=");
        R.append(this.relType);
        R.append(", platformType=");
        R.append(this.platformType);
        R.append(", activityType=");
        R.append(this.activityType);
        R.append(", activityName=");
        R.append(this.activityName);
        R.append(", actionType=");
        R.append(this.actionType);
        R.append(", pushDataSize=");
        R.append(this.pushDataSize);
        R.append(", messageDataSize=");
        return a.F(R, this.messageDataSize, ")");
    }
}
