package com.discord.analytics.generated.events.network_action;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.analytics.generated.traits.TrackNetworkMetadata;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNetworkActionInviteResolve.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u001c\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B£\u0001\u0012\n\b\u0002\u0010H\u001a\u0004\u0018\u00010\r\u0012\n\b\u0002\u0010D\u001a\u0004\u0018\u00010\u001e\u0012\n\b\u0002\u0010F\u001a\u0004\u0018\u00010\r\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u00109\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010B\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010@\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u001e\u0012\n\b\u0002\u0010;\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\r\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u001e¢\u0006\u0004\bJ\u0010KJ\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR$\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0019\u001a\u0004\b\u001d\u0010\u001bR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010 \u001a\u0004\b$\u0010\"R\u001b\u0010%\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R$\u0010*\u001a\u0004\u0018\u00010)8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R$\u00101\u001a\u0004\u0018\u0001008\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b1\u00102\u001a\u0004\b3\u00104\"\u0004\b5\u00106R\u001b\u00107\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0019\u001a\u0004\b8\u0010\u001bR\u001b\u00109\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0019\u001a\u0004\b:\u0010\u001bR\u001b\u0010;\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0019\u001a\u0004\b<\u0010\u001bR\u001c\u0010=\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010\u0007R\u001b\u0010@\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0019\u001a\u0004\bA\u0010\u001bR\u001b\u0010B\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0019\u001a\u0004\bC\u0010\u001bR\u001b\u0010D\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010 \u001a\u0004\bE\u0010\"R\u001b\u0010F\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010&\u001a\u0004\bG\u0010(R\u001b\u0010H\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010&\u001a\u0004\bI\u0010(¨\u0006L"}, d2 = {"Lcom/discord/analytics/generated/events/network_action/TrackNetworkActionInviteResolve;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;", "trackNetworkMetadata", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;", "getTrackNetworkMetadata", "()Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;", "a", "(Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;)V", "", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "sizeOnline", "getSizeOnline", "", "inviteType", "Ljava/lang/CharSequence;", "getInviteType", "()Ljava/lang/CharSequence;", "inputValue", "getInputValue", "userBanned", "Ljava/lang/Boolean;", "getUserBanned", "()Ljava/lang/Boolean;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "sizeTotal", "getSizeTotal", "channelId", "getChannelId", "destinationUserId", "getDestinationUserId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "inviterId", "getInviterId", "channelType", "getChannelType", ModelAuditLogEntry.CHANGE_KEY_CODE, "getCode", "authenticated", "getAuthenticated", "resolved", "getResolved", HookHelper.constructorName, "(Ljava/lang/Boolean;Ljava/lang/CharSequence;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/CharSequence;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/CharSequence;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNetworkActionInviteResolve implements AnalyticsSchema, TrackBaseReceiver, TrackLocationMetadataReceiver, TrackNetworkMetadataReceiver {
    private final transient String analyticsSchemaTypeName;
    private final Boolean authenticated;
    private final Long channelId;
    private final Long channelType;
    private final CharSequence code;
    private final Long destinationUserId;
    private final Long guildId;
    private final CharSequence inputValue;
    private final CharSequence inviteType;
    private final Long inviterId;
    private final Boolean resolved;
    private final Long sizeOnline;
    private final Long sizeTotal;
    private TrackBase trackBase;
    private TrackLocationMetadata trackLocationMetadata;
    private TrackNetworkMetadata trackNetworkMetadata;
    private final Boolean userBanned;

    public TrackNetworkActionInviteResolve(Boolean bool, CharSequence charSequence, Boolean bool2, Long l, Long l2, Long l3, Long l4, Long l5, Long l6, CharSequence charSequence2, Long l7, Boolean bool3, CharSequence charSequence3) {
        this.resolved = bool;
        this.code = charSequence;
        this.authenticated = bool2;
        this.guildId = l;
        this.channelId = l2;
        this.channelType = l3;
        this.inviterId = l4;
        this.sizeTotal = l5;
        this.sizeOnline = l6;
        this.inviteType = charSequence2;
        this.destinationUserId = l7;
        this.userBanned = bool3;
        this.inputValue = charSequence3;
        this.analyticsSchemaTypeName = "network_action_invite_resolve";
    }

    @Override // com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver
    public void a(TrackNetworkMetadata trackNetworkMetadata) {
        this.trackNetworkMetadata = trackNetworkMetadata;
    }

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNetworkActionInviteResolve)) {
            return false;
        }
        TrackNetworkActionInviteResolve trackNetworkActionInviteResolve = (TrackNetworkActionInviteResolve) obj;
        return m.areEqual(this.resolved, trackNetworkActionInviteResolve.resolved) && m.areEqual(this.code, trackNetworkActionInviteResolve.code) && m.areEqual(this.authenticated, trackNetworkActionInviteResolve.authenticated) && m.areEqual(this.guildId, trackNetworkActionInviteResolve.guildId) && m.areEqual(this.channelId, trackNetworkActionInviteResolve.channelId) && m.areEqual(this.channelType, trackNetworkActionInviteResolve.channelType) && m.areEqual(this.inviterId, trackNetworkActionInviteResolve.inviterId) && m.areEqual(this.sizeTotal, trackNetworkActionInviteResolve.sizeTotal) && m.areEqual(this.sizeOnline, trackNetworkActionInviteResolve.sizeOnline) && m.areEqual(this.inviteType, trackNetworkActionInviteResolve.inviteType) && m.areEqual(this.destinationUserId, trackNetworkActionInviteResolve.destinationUserId) && m.areEqual(this.userBanned, trackNetworkActionInviteResolve.userBanned) && m.areEqual(this.inputValue, trackNetworkActionInviteResolve.inputValue);
    }

    public int hashCode() {
        Boolean bool = this.resolved;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        CharSequence charSequence = this.code;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool2 = this.authenticated;
        int hashCode3 = (hashCode2 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l = this.guildId;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channelType;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.inviterId;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.sizeTotal;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.sizeOnline;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.inviteType;
        int hashCode10 = (hashCode9 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l7 = this.destinationUserId;
        int hashCode11 = (hashCode10 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Boolean bool3 = this.userBanned;
        int hashCode12 = (hashCode11 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.inputValue;
        if (charSequence3 != null) {
            i = charSequence3.hashCode();
        }
        return hashCode12 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNetworkActionInviteResolve(resolved=");
        R.append(this.resolved);
        R.append(", code=");
        R.append(this.code);
        R.append(", authenticated=");
        R.append(this.authenticated);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", inviterId=");
        R.append(this.inviterId);
        R.append(", sizeTotal=");
        R.append(this.sizeTotal);
        R.append(", sizeOnline=");
        R.append(this.sizeOnline);
        R.append(", inviteType=");
        R.append(this.inviteType);
        R.append(", destinationUserId=");
        R.append(this.destinationUserId);
        R.append(", userBanned=");
        R.append(this.userBanned);
        R.append(", inputValue=");
        return a.D(R, this.inputValue, ")");
    }

    public TrackNetworkActionInviteResolve() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
}
