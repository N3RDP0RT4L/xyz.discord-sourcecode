package com.discord.analytics.generated.events;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackGiftCodeMetadata;
import com.discord.analytics.generated.traits.TrackGiftCodeMetadataReceiver;
import com.discord.analytics.generated.traits.TrackGuildTemplate;
import com.discord.analytics.generated.traits.TrackGuildTemplateReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackRegisterAttempted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\u0007\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR$\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010!\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0019\u001a\u0004\b\"\u0010\u001bR\u001b\u0010#\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0019\u001a\u0004\b$\u0010\u001bR\u001b\u0010&\u001a\u0004\u0018\u00010%8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u001b\u0010*\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u001b\u0010.\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0019\u001a\u0004\b/\u0010\u001bR\u001b\u00100\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u001e\u001a\u0004\b1\u0010 R\u001b\u00102\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u001e\u001a\u0004\b3\u0010 R$\u00105\u001a\u0004\u0018\u0001048\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b5\u00106\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R\u001b\u0010;\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u001e\u001a\u0004\b<\u0010 R\u001c\u0010=\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010\u0007R\u001b\u0010@\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010+\u001a\u0004\bA\u0010-R\u001b\u0010B\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0019\u001a\u0004\bC\u0010\u001bR\u001b\u0010D\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010+\u001a\u0004\bE\u0010-R\u001b\u0010F\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u001e\u001a\u0004\bG\u0010 R$\u0010I\u001a\u0004\u0018\u00010H8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bI\u0010J\u001a\u0004\bK\u0010L\"\u0004\bM\u0010NR\u001b\u0010O\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010+\u001a\u0004\bP\u0010-R\u001b\u0010Q\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u001e\u001a\u0004\bR\u0010 R\u001b\u0010S\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010\u001e\u001a\u0004\bT\u0010 R\u001b\u0010U\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u0019\u001a\u0004\bV\u0010\u001bR\u001b\u0010W\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010\u001e\u001a\u0004\bX\u0010 R\u001b\u0010Y\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\bY\u0010\u001e\u001a\u0004\bZ\u0010 ¨\u0006["}, d2 = {"Lcom/discord/analytics/generated/events/TrackRegisterAttempted;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildTemplateReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "inviteGuildId", "Ljava/lang/Long;", "getInviteGuildId", "()Ljava/lang/Long;", "", "inviteCode", "Ljava/lang/CharSequence;", "getInviteCode", "()Ljava/lang/CharSequence;", "inviteChannelId", "getInviteChannelId", "inviteChannelType", "getInviteChannelType", "", "discordAiSpamLikelihood", "Ljava/lang/Float;", "getDiscordAiSpamLikelihood", "()Ljava/lang/Float;", "full", "Ljava/lang/Boolean;", "getFull", "()Ljava/lang/Boolean;", "cfClientTrustScore", "getCfClientTrustScore", "phoneCountry", "getPhoneCountry", "identityType", "getIdentityType", "Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "trackGuildTemplate", "Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "getTrackGuildTemplate", "()Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "setTrackGuildTemplate", "(Lcom/discord/analytics/generated/traits/TrackGuildTemplate;)V", "phoneCarrierName", "getPhoneCarrierName", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "instantInvite", "getInstantInvite", "discriminatorAttempts", "getDiscriminatorAttempts", "hasInvalidFingerprint", "getHasInvalidFingerprint", "username", "getUsername", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "trackGiftCodeMetadata", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "getTrackGiftCodeMetadata", "()Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "setTrackGiftCodeMetadata", "(Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;)V", "ipBlacklisted", "getIpBlacklisted", "phone", "getPhone", "registrationSource", "getRegistrationSource", "inviteInviterId", "getInviteInviterId", NotificationCompat.CATEGORY_EMAIL, "getEmail", "discordAiModelVersion", "getDiscordAiModelVersion", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackRegisterAttempted implements AnalyticsSchema, TrackBaseReceiver, TrackGiftCodeMetadataReceiver, TrackGuildTemplateReceiver {
    private TrackBase trackBase;
    private TrackGiftCodeMetadata trackGiftCodeMetadata;
    private TrackGuildTemplate trackGuildTemplate;
    private final CharSequence registrationSource = null;
    private final Boolean full = null;
    private final Boolean instantInvite = null;
    private final CharSequence inviteCode = null;
    private final Long inviteGuildId = null;
    private final Long inviteChannelId = null;
    private final Long inviteChannelType = null;
    private final Long inviteInviterId = null;
    private final Boolean ipBlacklisted = null;
    private final Boolean hasInvalidFingerprint = null;
    private final Long discriminatorAttempts = null;
    private final Long cfClientTrustScore = null;
    private final CharSequence username = null;
    private final CharSequence email = null;
    private final CharSequence phone = null;
    private final CharSequence identityType = null;
    private final Float discordAiSpamLikelihood = null;
    private final CharSequence discordAiModelVersion = null;
    private final CharSequence phoneCarrierName = null;
    private final CharSequence phoneCountry = null;
    private final transient String analyticsSchemaTypeName = "register_attempted";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackRegisterAttempted)) {
            return false;
        }
        TrackRegisterAttempted trackRegisterAttempted = (TrackRegisterAttempted) obj;
        return m.areEqual(this.registrationSource, trackRegisterAttempted.registrationSource) && m.areEqual(this.full, trackRegisterAttempted.full) && m.areEqual(this.instantInvite, trackRegisterAttempted.instantInvite) && m.areEqual(this.inviteCode, trackRegisterAttempted.inviteCode) && m.areEqual(this.inviteGuildId, trackRegisterAttempted.inviteGuildId) && m.areEqual(this.inviteChannelId, trackRegisterAttempted.inviteChannelId) && m.areEqual(this.inviteChannelType, trackRegisterAttempted.inviteChannelType) && m.areEqual(this.inviteInviterId, trackRegisterAttempted.inviteInviterId) && m.areEqual(this.ipBlacklisted, trackRegisterAttempted.ipBlacklisted) && m.areEqual(this.hasInvalidFingerprint, trackRegisterAttempted.hasInvalidFingerprint) && m.areEqual(this.discriminatorAttempts, trackRegisterAttempted.discriminatorAttempts) && m.areEqual(this.cfClientTrustScore, trackRegisterAttempted.cfClientTrustScore) && m.areEqual(this.username, trackRegisterAttempted.username) && m.areEqual(this.email, trackRegisterAttempted.email) && m.areEqual(this.phone, trackRegisterAttempted.phone) && m.areEqual(this.identityType, trackRegisterAttempted.identityType) && m.areEqual(this.discordAiSpamLikelihood, trackRegisterAttempted.discordAiSpamLikelihood) && m.areEqual(this.discordAiModelVersion, trackRegisterAttempted.discordAiModelVersion) && m.areEqual(this.phoneCarrierName, trackRegisterAttempted.phoneCarrierName) && m.areEqual(this.phoneCountry, trackRegisterAttempted.phoneCountry);
    }

    public int hashCode() {
        CharSequence charSequence = this.registrationSource;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Boolean bool = this.full;
        int hashCode2 = (hashCode + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.instantInvite;
        int hashCode3 = (hashCode2 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.inviteCode;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.inviteGuildId;
        int hashCode5 = (hashCode4 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.inviteChannelId;
        int hashCode6 = (hashCode5 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.inviteChannelType;
        int hashCode7 = (hashCode6 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.inviteInviterId;
        int hashCode8 = (hashCode7 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool3 = this.ipBlacklisted;
        int hashCode9 = (hashCode8 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.hasInvalidFingerprint;
        int hashCode10 = (hashCode9 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l5 = this.discriminatorAttempts;
        int hashCode11 = (hashCode10 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.cfClientTrustScore;
        int hashCode12 = (hashCode11 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.username;
        int hashCode13 = (hashCode12 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.email;
        int hashCode14 = (hashCode13 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.phone;
        int hashCode15 = (hashCode14 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.identityType;
        int hashCode16 = (hashCode15 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Float f = this.discordAiSpamLikelihood;
        int hashCode17 = (hashCode16 + (f != null ? f.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.discordAiModelVersion;
        int hashCode18 = (hashCode17 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.phoneCarrierName;
        int hashCode19 = (hashCode18 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.phoneCountry;
        if (charSequence9 != null) {
            i = charSequence9.hashCode();
        }
        return hashCode19 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackRegisterAttempted(registrationSource=");
        R.append(this.registrationSource);
        R.append(", full=");
        R.append(this.full);
        R.append(", instantInvite=");
        R.append(this.instantInvite);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", inviteGuildId=");
        R.append(this.inviteGuildId);
        R.append(", inviteChannelId=");
        R.append(this.inviteChannelId);
        R.append(", inviteChannelType=");
        R.append(this.inviteChannelType);
        R.append(", inviteInviterId=");
        R.append(this.inviteInviterId);
        R.append(", ipBlacklisted=");
        R.append(this.ipBlacklisted);
        R.append(", hasInvalidFingerprint=");
        R.append(this.hasInvalidFingerprint);
        R.append(", discriminatorAttempts=");
        R.append(this.discriminatorAttempts);
        R.append(", cfClientTrustScore=");
        R.append(this.cfClientTrustScore);
        R.append(", username=");
        R.append(this.username);
        R.append(", email=");
        R.append(this.email);
        R.append(", phone=");
        R.append(this.phone);
        R.append(", identityType=");
        R.append(this.identityType);
        R.append(", discordAiSpamLikelihood=");
        R.append(this.discordAiSpamLikelihood);
        R.append(", discordAiModelVersion=");
        R.append(this.discordAiModelVersion);
        R.append(", phoneCarrierName=");
        R.append(this.phoneCarrierName);
        R.append(", phoneCountry=");
        return a.D(R, this.phoneCountry, ")");
    }
}
