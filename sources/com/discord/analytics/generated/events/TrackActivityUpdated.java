package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackActivityUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u000f\u001a\u0004\b\u0013\u0010\u0011R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0016\u001a\u0004\b\u001a\u0010\u0018R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001c\u0010 \u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010\u0005R\u001b\u0010#\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u001d\u001a\u0004\b$\u0010\u001fR\u001b\u0010%\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u001d\u001a\u0004\b&\u0010\u001fR\u001b\u0010'\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0016\u001a\u0004\b(\u0010\u0018R$\u0010*\u001a\u0004\u0018\u00010)8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u001b\u00100\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u000f\u001a\u0004\b1\u0010\u0011R\u001b\u00102\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0016\u001a\u0004\b3\u0010\u0018R\u001b\u00104\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u000f\u001a\u0004\b5\u0010\u0011R\u001b\u00106\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0016\u001a\u0004\b7\u0010\u0018¨\u00068"}, d2 = {"Lcom/discord/analytics/generated/events/TrackActivityUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "hasJoinSecret", "Ljava/lang/Boolean;", "getHasJoinSecret", "()Ljava/lang/Boolean;", "hasMatchSecret", "getHasMatchSecret", "", "platform", "Ljava/lang/CharSequence;", "getPlatform", "()Ljava/lang/CharSequence;", "trackId", "getTrackId", "", "type", "Ljava/lang/Long;", "getType", "()Ljava/lang/Long;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "partyMax", "getPartyMax", "applicationId", "getApplicationId", "partyPlatform", "getPartyPlatform", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "hasSpectateSecret", "getHasSpectateSecret", "partyId", "getPartyId", "hasImages", "getHasImages", "activitySessionId", "getActivitySessionId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackActivityUpdated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long applicationId = null;
    private final Boolean hasMatchSecret = null;
    private final Boolean hasSpectateSecret = null;
    private final Boolean hasJoinSecret = null;
    private final Boolean hasImages = null;
    private final Long partyMax = null;
    private final CharSequence partyId = null;
    private final CharSequence platform = null;
    private final CharSequence partyPlatform = null;
    private final CharSequence trackId = null;
    private final Long type = null;
    private final CharSequence activitySessionId = null;
    private final transient String analyticsSchemaTypeName = "activity_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackActivityUpdated)) {
            return false;
        }
        TrackActivityUpdated trackActivityUpdated = (TrackActivityUpdated) obj;
        return m.areEqual(this.applicationId, trackActivityUpdated.applicationId) && m.areEqual(this.hasMatchSecret, trackActivityUpdated.hasMatchSecret) && m.areEqual(this.hasSpectateSecret, trackActivityUpdated.hasSpectateSecret) && m.areEqual(this.hasJoinSecret, trackActivityUpdated.hasJoinSecret) && m.areEqual(this.hasImages, trackActivityUpdated.hasImages) && m.areEqual(this.partyMax, trackActivityUpdated.partyMax) && m.areEqual(this.partyId, trackActivityUpdated.partyId) && m.areEqual(this.platform, trackActivityUpdated.platform) && m.areEqual(this.partyPlatform, trackActivityUpdated.partyPlatform) && m.areEqual(this.trackId, trackActivityUpdated.trackId) && m.areEqual(this.type, trackActivityUpdated.type) && m.areEqual(this.activitySessionId, trackActivityUpdated.activitySessionId);
    }

    public int hashCode() {
        Long l = this.applicationId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Boolean bool = this.hasMatchSecret;
        int hashCode2 = (hashCode + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasSpectateSecret;
        int hashCode3 = (hashCode2 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.hasJoinSecret;
        int hashCode4 = (hashCode3 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.hasImages;
        int hashCode5 = (hashCode4 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l2 = this.partyMax;
        int hashCode6 = (hashCode5 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.partyId;
        int hashCode7 = (hashCode6 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.platform;
        int hashCode8 = (hashCode7 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.partyPlatform;
        int hashCode9 = (hashCode8 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.trackId;
        int hashCode10 = (hashCode9 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l3 = this.type;
        int hashCode11 = (hashCode10 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.activitySessionId;
        if (charSequence5 != null) {
            i = charSequence5.hashCode();
        }
        return hashCode11 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackActivityUpdated(applicationId=");
        R.append(this.applicationId);
        R.append(", hasMatchSecret=");
        R.append(this.hasMatchSecret);
        R.append(", hasSpectateSecret=");
        R.append(this.hasSpectateSecret);
        R.append(", hasJoinSecret=");
        R.append(this.hasJoinSecret);
        R.append(", hasImages=");
        R.append(this.hasImages);
        R.append(", partyMax=");
        R.append(this.partyMax);
        R.append(", partyId=");
        R.append(this.partyId);
        R.append(", platform=");
        R.append(this.platform);
        R.append(", partyPlatform=");
        R.append(this.partyPlatform);
        R.append(", trackId=");
        R.append(this.trackId);
        R.append(", type=");
        R.append(this.type);
        R.append(", activitySessionId=");
        return a.D(R, this.activitySessionId, ")");
    }
}
