package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackAppUiViewed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b*\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R$\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0015\u001a\u0004\b \u0010\u0017R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001c\u0010#\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010\u0005R\u001b\u0010&\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0015\u001a\u0004\b'\u0010\u0017R\u001b\u0010(\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0015\u001a\u0004\b)\u0010\u0017R\u001b\u0010*\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012R\u001b\u00100\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0015\u001a\u0004\b1\u0010\u0017R\u001b\u00102\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0015\u001a\u0004\b3\u0010\u0017R\u001b\u00104\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0015\u001a\u0004\b5\u0010\u0017R\u001b\u00106\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0015\u001a\u0004\b7\u0010\u0017R\u001b\u00108\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0010\u001a\u0004\b9\u0010\u0012R\u001b\u0010:\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0015\u001a\u0004\b;\u0010\u0017R\u001b\u0010<\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0015\u001a\u0004\b=\u0010\u0017R\u001b\u0010>\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0015\u001a\u0004\b?\u0010\u0017R\u001b\u0010@\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0015\u001a\u0004\bA\u0010\u0017¨\u0006B"}, d2 = {"Lcom/discord/analytics/generated/events/TrackAppUiViewed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "theme", "Ljava/lang/CharSequence;", "getTheme", "()Ljava/lang/CharSequence;", "", "totalCompressedByteSize", "Ljava/lang/Long;", "getTotalCompressedByteSize", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "durationMsSinceAppOpened", "getDurationMsSinceAppOpened", "loadId", "getLoadId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "durationMsSinceRequiredJsBundleLoaded", "getDurationMsSinceRequiredJsBundleLoaded", "durationMsSinceRequiredJsBundleParsed", "getDurationMsSinceRequiredJsBundleParsed", "hasCachedData", "Ljava/lang/Boolean;", "getHasCachedData", "()Ljava/lang/Boolean;", "manifest", "getManifest", "totalTransferByteSize", "getTotalTransferByteSize", "jsTransferByteSize", "getJsTransferByteSize", "cssUncompressedByteSize", "getCssUncompressedByteSize", "cssTransferByteSize", "getCssTransferByteSize", "screenName", "getScreenName", "jsCompressedByteSize", "getJsCompressedByteSize", "jsUncompressedByteSize", "getJsUncompressedByteSize", "totalUncompressedByteSize", "getTotalUncompressedByteSize", "cssCompressedByteSize", "getCssCompressedByteSize", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackAppUiViewed implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence loadId = null;
    private final CharSequence screenName = null;
    private final Long durationMsSinceAppOpened = null;
    private final Long durationMsSinceRequiredJsBundleLoaded = null;
    private final Long durationMsSinceRequiredJsBundleParsed = null;
    private final Boolean hasCachedData = null;
    private final CharSequence manifest = null;
    private final Long totalCompressedByteSize = null;
    private final Long totalUncompressedByteSize = null;
    private final Long totalTransferByteSize = null;
    private final Long jsCompressedByteSize = null;
    private final Long jsUncompressedByteSize = null;
    private final Long jsTransferByteSize = null;
    private final Long cssCompressedByteSize = null;
    private final Long cssUncompressedByteSize = null;
    private final Long cssTransferByteSize = null;
    private final CharSequence theme = null;
    private final transient String analyticsSchemaTypeName = "app_ui_viewed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackAppUiViewed)) {
            return false;
        }
        TrackAppUiViewed trackAppUiViewed = (TrackAppUiViewed) obj;
        return m.areEqual(this.loadId, trackAppUiViewed.loadId) && m.areEqual(this.screenName, trackAppUiViewed.screenName) && m.areEqual(this.durationMsSinceAppOpened, trackAppUiViewed.durationMsSinceAppOpened) && m.areEqual(this.durationMsSinceRequiredJsBundleLoaded, trackAppUiViewed.durationMsSinceRequiredJsBundleLoaded) && m.areEqual(this.durationMsSinceRequiredJsBundleParsed, trackAppUiViewed.durationMsSinceRequiredJsBundleParsed) && m.areEqual(this.hasCachedData, trackAppUiViewed.hasCachedData) && m.areEqual(this.manifest, trackAppUiViewed.manifest) && m.areEqual(this.totalCompressedByteSize, trackAppUiViewed.totalCompressedByteSize) && m.areEqual(this.totalUncompressedByteSize, trackAppUiViewed.totalUncompressedByteSize) && m.areEqual(this.totalTransferByteSize, trackAppUiViewed.totalTransferByteSize) && m.areEqual(this.jsCompressedByteSize, trackAppUiViewed.jsCompressedByteSize) && m.areEqual(this.jsUncompressedByteSize, trackAppUiViewed.jsUncompressedByteSize) && m.areEqual(this.jsTransferByteSize, trackAppUiViewed.jsTransferByteSize) && m.areEqual(this.cssCompressedByteSize, trackAppUiViewed.cssCompressedByteSize) && m.areEqual(this.cssUncompressedByteSize, trackAppUiViewed.cssUncompressedByteSize) && m.areEqual(this.cssTransferByteSize, trackAppUiViewed.cssTransferByteSize) && m.areEqual(this.theme, trackAppUiViewed.theme);
    }

    public int hashCode() {
        CharSequence charSequence = this.loadId;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.screenName;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.durationMsSinceAppOpened;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.durationMsSinceRequiredJsBundleLoaded;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.durationMsSinceRequiredJsBundleParsed;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool = this.hasCachedData;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.manifest;
        int hashCode7 = (hashCode6 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l4 = this.totalCompressedByteSize;
        int hashCode8 = (hashCode7 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.totalUncompressedByteSize;
        int hashCode9 = (hashCode8 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.totalTransferByteSize;
        int hashCode10 = (hashCode9 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.jsCompressedByteSize;
        int hashCode11 = (hashCode10 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.jsUncompressedByteSize;
        int hashCode12 = (hashCode11 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.jsTransferByteSize;
        int hashCode13 = (hashCode12 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.cssCompressedByteSize;
        int hashCode14 = (hashCode13 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.cssUncompressedByteSize;
        int hashCode15 = (hashCode14 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.cssTransferByteSize;
        int hashCode16 = (hashCode15 + (l12 != null ? l12.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.theme;
        if (charSequence4 != null) {
            i = charSequence4.hashCode();
        }
        return hashCode16 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackAppUiViewed(loadId=");
        R.append(this.loadId);
        R.append(", screenName=");
        R.append(this.screenName);
        R.append(", durationMsSinceAppOpened=");
        R.append(this.durationMsSinceAppOpened);
        R.append(", durationMsSinceRequiredJsBundleLoaded=");
        R.append(this.durationMsSinceRequiredJsBundleLoaded);
        R.append(", durationMsSinceRequiredJsBundleParsed=");
        R.append(this.durationMsSinceRequiredJsBundleParsed);
        R.append(", hasCachedData=");
        R.append(this.hasCachedData);
        R.append(", manifest=");
        R.append(this.manifest);
        R.append(", totalCompressedByteSize=");
        R.append(this.totalCompressedByteSize);
        R.append(", totalUncompressedByteSize=");
        R.append(this.totalUncompressedByteSize);
        R.append(", totalTransferByteSize=");
        R.append(this.totalTransferByteSize);
        R.append(", jsCompressedByteSize=");
        R.append(this.jsCompressedByteSize);
        R.append(", jsUncompressedByteSize=");
        R.append(this.jsUncompressedByteSize);
        R.append(", jsTransferByteSize=");
        R.append(this.jsTransferByteSize);
        R.append(", cssCompressedByteSize=");
        R.append(this.cssCompressedByteSize);
        R.append(", cssUncompressedByteSize=");
        R.append(this.cssUncompressedByteSize);
        R.append(", cssTransferByteSize=");
        R.append(this.cssTransferByteSize);
        R.append(", theme=");
        return a.D(R, this.theme, ")");
    }
}
