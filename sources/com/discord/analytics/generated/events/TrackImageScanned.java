package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackImageScanned.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\u0007\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0016\u0010\u0012R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012R\u001b\u0010%\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0010\u001a\u0004\b&\u0010\u0012R\u001c\u0010'\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010\u0005R\u001b\u0010*\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0010\u001a\u0004\b+\u0010\u0012R\u001b\u0010,\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0019\u001a\u0004\b-\u0010\u001bR$\u0010/\u001a\u0004\u0018\u00010.8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b/\u00100\u001a\u0004\b1\u00102\"\u0004\b3\u00104¨\u00065"}, d2 = {"Lcom/discord/analytics/generated/events/TrackImageScanned;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "safeSearchAdultLikelihood", "Ljava/lang/CharSequence;", "getSafeSearchAdultLikelihood", "()Ljava/lang/CharSequence;", "safeSearchRacyLikelihood", "getSafeSearchRacyLikelihood", "safeSearchSpoofLikelihood", "getSafeSearchSpoofLikelihood", "", "openNsfwSfw", "Ljava/lang/Float;", "getOpenNsfwSfw", "()Ljava/lang/Float;", "safeSearchViolenceLikelihood", "getSafeSearchViolenceLikelihood", "", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "scanUuid", "getScanUuid", "provider", "getProvider", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "safeSearchMedicalLikelihood", "getSafeSearchMedicalLikelihood", "openNsfwNsfw", "getOpenNsfwNsfw", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackImageScanned implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence scanUuid = null;
    private final CharSequence provider = null;
    private final CharSequence safeSearchAdultLikelihood = null;
    private final CharSequence safeSearchMedicalLikelihood = null;
    private final CharSequence safeSearchRacyLikelihood = null;
    private final CharSequence safeSearchSpoofLikelihood = null;
    private final CharSequence safeSearchViolenceLikelihood = null;
    private final Float openNsfwSfw = null;
    private final Float openNsfwNsfw = null;
    private final Long guildId = null;
    private final transient String analyticsSchemaTypeName = "image_scanned";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackImageScanned)) {
            return false;
        }
        TrackImageScanned trackImageScanned = (TrackImageScanned) obj;
        return m.areEqual(this.scanUuid, trackImageScanned.scanUuid) && m.areEqual(this.provider, trackImageScanned.provider) && m.areEqual(this.safeSearchAdultLikelihood, trackImageScanned.safeSearchAdultLikelihood) && m.areEqual(this.safeSearchMedicalLikelihood, trackImageScanned.safeSearchMedicalLikelihood) && m.areEqual(this.safeSearchRacyLikelihood, trackImageScanned.safeSearchRacyLikelihood) && m.areEqual(this.safeSearchSpoofLikelihood, trackImageScanned.safeSearchSpoofLikelihood) && m.areEqual(this.safeSearchViolenceLikelihood, trackImageScanned.safeSearchViolenceLikelihood) && m.areEqual(this.openNsfwSfw, trackImageScanned.openNsfwSfw) && m.areEqual(this.openNsfwNsfw, trackImageScanned.openNsfwNsfw) && m.areEqual(this.guildId, trackImageScanned.guildId);
    }

    public int hashCode() {
        CharSequence charSequence = this.scanUuid;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.provider;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.safeSearchAdultLikelihood;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.safeSearchMedicalLikelihood;
        int hashCode4 = (hashCode3 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.safeSearchRacyLikelihood;
        int hashCode5 = (hashCode4 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.safeSearchSpoofLikelihood;
        int hashCode6 = (hashCode5 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.safeSearchViolenceLikelihood;
        int hashCode7 = (hashCode6 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        Float f = this.openNsfwSfw;
        int hashCode8 = (hashCode7 + (f != null ? f.hashCode() : 0)) * 31;
        Float f2 = this.openNsfwNsfw;
        int hashCode9 = (hashCode8 + (f2 != null ? f2.hashCode() : 0)) * 31;
        Long l = this.guildId;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackImageScanned(scanUuid=");
        R.append(this.scanUuid);
        R.append(", provider=");
        R.append(this.provider);
        R.append(", safeSearchAdultLikelihood=");
        R.append(this.safeSearchAdultLikelihood);
        R.append(", safeSearchMedicalLikelihood=");
        R.append(this.safeSearchMedicalLikelihood);
        R.append(", safeSearchRacyLikelihood=");
        R.append(this.safeSearchRacyLikelihood);
        R.append(", safeSearchSpoofLikelihood=");
        R.append(this.safeSearchSpoofLikelihood);
        R.append(", safeSearchViolenceLikelihood=");
        R.append(this.safeSearchViolenceLikelihood);
        R.append(", openNsfwSfw=");
        R.append(this.openNsfwSfw);
        R.append(", openNsfwNsfw=");
        R.append(this.openNsfwNsfw);
        R.append(", guildId=");
        return a.F(R, this.guildId, ")");
    }
}
