package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.generated.traits.TrackGuildReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackSearchResultViewed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\r\n\u0002\b\u0019\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010 \n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0012\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0017\u001a\u0004\b\u001b\u0010\u0019R$\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001c\u0010#\u001a\u00020\u00068\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010\bR\u001b\u0010'\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001b\u0010+\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0017\u001a\u0004\b,\u0010\u0019R\u001b\u0010-\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0017\u001a\u0004\b.\u0010\u0019R\u001b\u0010/\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0017\u001a\u0004\b0\u0010\u0019R\u001b\u00101\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010(\u001a\u0004\b2\u0010*R\u001b\u00103\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0017\u001a\u0004\b4\u0010\u0019R\u001b\u00105\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0017\u001a\u0004\b6\u0010\u0019R\u001b\u00107\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b7\u00109R\u001b\u0010:\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0017\u001a\u0004\b;\u0010\u0019R\u001b\u0010<\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0017\u001a\u0004\b=\u0010\u0019R\u001b\u0010>\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010(\u001a\u0004\b?\u0010*R$\u0010A\u001a\u0004\u0018\u00010@8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bA\u0010B\u001a\u0004\bC\u0010D\"\u0004\bE\u0010FR$\u0010H\u001a\u0004\u0018\u00010G8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bH\u0010I\u001a\u0004\bJ\u0010K\"\u0004\bL\u0010MR\u001b\u0010N\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bN\u00108\u001a\u0004\bN\u00109R\u001b\u0010O\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010\u0017\u001a\u0004\bP\u0010\u0019R\u001b\u0010Q\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u0017\u001a\u0004\bR\u0010\u0019R\u001b\u0010S\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010\u0017\u001a\u0004\bT\u0010\u0019R\u001b\u0010U\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u0017\u001a\u0004\bV\u0010\u0019R!\u0010X\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010W8\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010Y\u001a\u0004\bZ\u0010[R\u001b\u0010\\\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b\\\u0010(\u001a\u0004\b]\u0010*R\u001b\u0010^\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b^\u0010(\u001a\u0004\b_\u0010*R\u001b\u0010`\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b`\u00108\u001a\u0004\ba\u00109R$\u0010c\u001a\u0004\u0018\u00010b8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bc\u0010d\u001a\u0004\be\u0010f\"\u0004\bg\u0010hR\u001b\u0010i\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bi\u00108\u001a\u0004\bi\u00109¨\u0006j"}, d2 = {"Lcom/discord/analytics/generated/events/TrackSearchResultViewed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "modifiers", "J", "getModifiers", "()J", "numResultsLocked", "Ljava/lang/Long;", "getNumResultsLocked", "()Ljava/lang/Long;", "loadDurationMs", "getLoadDurationMs", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "prevSearchId", "Ljava/lang/CharSequence;", "getPrevSearchId", "()Ljava/lang/CharSequence;", "pageNumLinks", "getPageNumLinks", "totalResults", "getTotalResults", "numModifiers", "getNumModifiers", "searchType", "getSearchType", "pageNumMessages", "getPageNumMessages", "pageNumAttach", "getPageNumAttach", "isSuggestion", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "offset", "getOffset", "pageResults", "getPageResults", "searchId", "getSearchId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "isIndexing", "limit", "getLimit", "pageNumEmbeds", "getPageNumEmbeds", "categoryId", "getCategoryId", "page", "getPage", "", "guildIds", "Ljava/util/List;", "getGuildIds", "()Ljava/util/List;", "query", "getQuery", "loadId", "getLoadId", "previewEnabled", "getPreviewEnabled", "Lcom/discord/analytics/generated/traits/TrackGuild;", "trackGuild", "Lcom/discord/analytics/generated/traits/TrackGuild;", "getTrackGuild", "()Lcom/discord/analytics/generated/traits/TrackGuild;", "setTrackGuild", "(Lcom/discord/analytics/generated/traits/TrackGuild;)V", "isError", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSearchResultViewed implements AnalyticsSchema, TrackBaseReceiver, TrackLocationMetadataReceiver, TrackGuildReceiver, TrackChannelReceiver {
    private final transient String analyticsSchemaTypeName;
    private final Long categoryId;
    private final List<Long> guildIds;
    private final Boolean isError;
    private final Boolean isIndexing;
    private final Boolean isSuggestion;
    private final Long limit;
    private final Long loadDurationMs;
    private final CharSequence loadId;
    private final long modifiers;
    private final Long numModifiers;
    private final Long numResultsLocked;
    private final Long offset;
    private final Long page;
    private final Long pageNumAttach;
    private final Long pageNumEmbeds;
    private final Long pageNumLinks;
    private final Long pageNumMessages;
    private final Long pageResults;
    private final CharSequence prevSearchId;
    private final Boolean previewEnabled;
    private final CharSequence query;
    private final CharSequence searchId;
    private final CharSequence searchType;
    private final Long totalResults;
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackGuild trackGuild;
    private TrackLocationMetadata trackLocationMetadata;

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSearchResultViewed)) {
            return false;
        }
        TrackSearchResultViewed trackSearchResultViewed = (TrackSearchResultViewed) obj;
        return m.areEqual(this.searchType, trackSearchResultViewed.searchType) && m.areEqual(this.loadId, trackSearchResultViewed.loadId) && m.areEqual(this.loadDurationMs, trackSearchResultViewed.loadDurationMs) && m.areEqual(this.searchId, trackSearchResultViewed.searchId) && m.areEqual(this.prevSearchId, trackSearchResultViewed.prevSearchId) && m.areEqual(this.isError, trackSearchResultViewed.isError) && m.areEqual(this.limit, trackSearchResultViewed.limit) && m.areEqual(this.offset, trackSearchResultViewed.offset) && m.areEqual(this.page, trackSearchResultViewed.page) && m.areEqual(this.totalResults, trackSearchResultViewed.totalResults) && m.areEqual(this.pageResults, trackSearchResultViewed.pageResults) && m.areEqual(this.isIndexing, trackSearchResultViewed.isIndexing) && m.areEqual(this.pageNumMessages, trackSearchResultViewed.pageNumMessages) && m.areEqual(this.pageNumLinks, trackSearchResultViewed.pageNumLinks) && m.areEqual(this.pageNumEmbeds, trackSearchResultViewed.pageNumEmbeds) && m.areEqual(this.pageNumAttach, trackSearchResultViewed.pageNumAttach) && this.modifiers == trackSearchResultViewed.modifiers && m.areEqual(this.numModifiers, trackSearchResultViewed.numModifiers) && m.areEqual(this.query, trackSearchResultViewed.query) && m.areEqual(this.guildIds, trackSearchResultViewed.guildIds) && m.areEqual(this.categoryId, trackSearchResultViewed.categoryId) && m.areEqual(this.previewEnabled, trackSearchResultViewed.previewEnabled) && m.areEqual(this.numResultsLocked, trackSearchResultViewed.numResultsLocked) && m.areEqual(this.isSuggestion, trackSearchResultViewed.isSuggestion);
    }

    public int hashCode() {
        CharSequence charSequence = this.searchType;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.loadId;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.loadDurationMs;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.searchId;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.prevSearchId;
        int hashCode5 = (hashCode4 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Boolean bool = this.isError;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l2 = this.limit;
        int hashCode7 = (hashCode6 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.offset;
        int hashCode8 = (hashCode7 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.page;
        int hashCode9 = (hashCode8 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.totalResults;
        int hashCode10 = (hashCode9 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.pageResults;
        int hashCode11 = (hashCode10 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Boolean bool2 = this.isIndexing;
        int hashCode12 = (hashCode11 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l7 = this.pageNumMessages;
        int hashCode13 = (hashCode12 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.pageNumLinks;
        int hashCode14 = (hashCode13 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.pageNumEmbeds;
        int hashCode15 = (hashCode14 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.pageNumAttach;
        int hashCode16 = l10 != null ? l10.hashCode() : 0;
        long j = this.modifiers;
        int i2 = (((hashCode15 + hashCode16) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        Long l11 = this.numModifiers;
        int hashCode17 = (i2 + (l11 != null ? l11.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.query;
        int hashCode18 = (hashCode17 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        List<Long> list = this.guildIds;
        int hashCode19 = (hashCode18 + (list != null ? list.hashCode() : 0)) * 31;
        Long l12 = this.categoryId;
        int hashCode20 = (hashCode19 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Boolean bool3 = this.previewEnabled;
        int hashCode21 = (hashCode20 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l13 = this.numResultsLocked;
        int hashCode22 = (hashCode21 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Boolean bool4 = this.isSuggestion;
        if (bool4 != null) {
            i = bool4.hashCode();
        }
        return hashCode22 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSearchResultViewed(searchType=");
        R.append(this.searchType);
        R.append(", loadId=");
        R.append(this.loadId);
        R.append(", loadDurationMs=");
        R.append(this.loadDurationMs);
        R.append(", searchId=");
        R.append(this.searchId);
        R.append(", prevSearchId=");
        R.append(this.prevSearchId);
        R.append(", isError=");
        R.append(this.isError);
        R.append(", limit=");
        R.append(this.limit);
        R.append(", offset=");
        R.append(this.offset);
        R.append(", page=");
        R.append(this.page);
        R.append(", totalResults=");
        R.append(this.totalResults);
        R.append(", pageResults=");
        R.append(this.pageResults);
        R.append(", isIndexing=");
        R.append(this.isIndexing);
        R.append(", pageNumMessages=");
        R.append(this.pageNumMessages);
        R.append(", pageNumLinks=");
        R.append(this.pageNumLinks);
        R.append(", pageNumEmbeds=");
        R.append(this.pageNumEmbeds);
        R.append(", pageNumAttach=");
        R.append(this.pageNumAttach);
        R.append(", modifiers=");
        R.append(this.modifiers);
        R.append(", numModifiers=");
        R.append(this.numModifiers);
        R.append(", query=");
        R.append(this.query);
        R.append(", guildIds=");
        R.append(this.guildIds);
        R.append(", categoryId=");
        R.append(this.categoryId);
        R.append(", previewEnabled=");
        R.append(this.previewEnabled);
        R.append(", numResultsLocked=");
        R.append(this.numResultsLocked);
        R.append(", isSuggestion=");
        return a.C(R, this.isSuggestion, ")");
    }
}
