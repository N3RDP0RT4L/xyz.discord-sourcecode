package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.generated.traits.TrackGuildReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackInviteSent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R$\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0013\u001a\u0004\b\u001e\u0010\u0015R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0013\u001a\u0004\b \u0010\u0015R\u001b\u0010\"\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u001b\u0010&\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0013\u001a\u0004\b'\u0010\u0015R$\u0010)\u001a\u0004\u0018\u00010(8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u001b\u0010/\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0013\u001a\u0004\b0\u0010\u0015R\u001b\u00101\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0013\u001a\u0004\b2\u0010\u0015R\u001b\u00103\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010#\u001a\u0004\b4\u0010%R\u001b\u00105\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0013\u001a\u0004\b6\u0010\u0015R\u001b\u00107\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010#\u001a\u0004\b8\u0010%R\u001b\u00109\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0013\u001a\u0004\b:\u0010\u0015R$\u0010<\u001a\u0004\u0018\u00010;8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b<\u0010=\u001a\u0004\b>\u0010?\"\u0004\b@\u0010AR\u001b\u0010B\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0013\u001a\u0004\bC\u0010\u0015R\u001b\u0010D\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0013\u001a\u0004\bE\u0010\u0015R\u001b\u0010F\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u0013\u001a\u0004\bG\u0010\u0015R\u001b\u0010H\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u0013\u001a\u0004\bI\u0010\u0015R\u001c\u0010J\u001a\u00020\u00068\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bJ\u0010K\u001a\u0004\bL\u0010\bR\u001b\u0010M\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010N\u001a\u0004\bM\u0010OR\u001b\u0010P\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010N\u001a\u0004\bP\u0010OR$\u0010R\u001a\u0004\u0018\u00010Q8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bR\u0010S\u001a\u0004\bT\u0010U\"\u0004\bV\u0010W¨\u0006X"}, d2 = {"Lcom/discord/analytics/generated/events/TrackInviteSent;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "inviteGuildId", "Ljava/lang/Long;", "getInviteGuildId", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "numSelected", "getNumSelected", "messageId", "getMessageId", "", "inviteCode", "Ljava/lang/CharSequence;", "getInviteCode", "()Ljava/lang/CharSequence;", "numAffinityConnections", "getNumAffinityConnections", "Lcom/discord/analytics/generated/traits/TrackGuild;", "trackGuild", "Lcom/discord/analytics/generated/traits/TrackGuild;", "getTrackGuild", "()Lcom/discord/analytics/generated/traits/TrackGuild;", "setTrackGuild", "(Lcom/discord/analytics/generated/traits/TrackGuild;)V", "inviteChannelId", "getInviteChannelId", "inviteInviterId", "getInviteInviterId", "inviteType", "getInviteType", "inviteChannelType", "getInviteChannelType", "sendType", "getSendType", "rowNum", "getRowNum", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "destinationUserId", "getDestinationUserId", "numTotal", "getNumTotal", "inviteGuildScheduledEventId", "getInviteGuildScheduledEventId", "applicationId", "getApplicationId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "isSuggested", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "isFiltered", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackInviteSent implements AnalyticsSchema, TrackBaseReceiver, TrackGuildReceiver, TrackChannelReceiver, TrackLocationMetadataReceiver {
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackGuild trackGuild;
    private TrackLocationMetadata trackLocationMetadata;
    private final CharSequence inviteType = null;
    private final CharSequence inviteCode = null;
    private final Long messageId = null;
    private final Long inviteGuildId = null;
    private final Long inviteChannelId = null;
    private final Long inviteChannelType = null;
    private final Long inviteInviterId = null;
    private final Long applicationId = null;
    private final Long destinationUserId = null;
    private final Boolean isSuggested = null;
    private final Long rowNum = null;
    private final Long numTotal = null;
    private final Long numAffinityConnections = null;
    private final Boolean isFiltered = null;
    private final Long numSelected = null;
    private final CharSequence sendType = null;
    private final Long inviteGuildScheduledEventId = null;
    private final transient String analyticsSchemaTypeName = "invite_sent";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackInviteSent)) {
            return false;
        }
        TrackInviteSent trackInviteSent = (TrackInviteSent) obj;
        return m.areEqual(this.inviteType, trackInviteSent.inviteType) && m.areEqual(this.inviteCode, trackInviteSent.inviteCode) && m.areEqual(this.messageId, trackInviteSent.messageId) && m.areEqual(this.inviteGuildId, trackInviteSent.inviteGuildId) && m.areEqual(this.inviteChannelId, trackInviteSent.inviteChannelId) && m.areEqual(this.inviteChannelType, trackInviteSent.inviteChannelType) && m.areEqual(this.inviteInviterId, trackInviteSent.inviteInviterId) && m.areEqual(this.applicationId, trackInviteSent.applicationId) && m.areEqual(this.destinationUserId, trackInviteSent.destinationUserId) && m.areEqual(this.isSuggested, trackInviteSent.isSuggested) && m.areEqual(this.rowNum, trackInviteSent.rowNum) && m.areEqual(this.numTotal, trackInviteSent.numTotal) && m.areEqual(this.numAffinityConnections, trackInviteSent.numAffinityConnections) && m.areEqual(this.isFiltered, trackInviteSent.isFiltered) && m.areEqual(this.numSelected, trackInviteSent.numSelected) && m.areEqual(this.sendType, trackInviteSent.sendType) && m.areEqual(this.inviteGuildScheduledEventId, trackInviteSent.inviteGuildScheduledEventId);
    }

    public int hashCode() {
        CharSequence charSequence = this.inviteType;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.inviteCode;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.messageId;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.inviteGuildId;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.inviteChannelId;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.inviteChannelType;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.inviteInviterId;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.applicationId;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.destinationUserId;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Boolean bool = this.isSuggested;
        int hashCode10 = (hashCode9 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l8 = this.rowNum;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.numTotal;
        int hashCode12 = (hashCode11 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.numAffinityConnections;
        int hashCode13 = (hashCode12 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Boolean bool2 = this.isFiltered;
        int hashCode14 = (hashCode13 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l11 = this.numSelected;
        int hashCode15 = (hashCode14 + (l11 != null ? l11.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.sendType;
        int hashCode16 = (hashCode15 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l12 = this.inviteGuildScheduledEventId;
        if (l12 != null) {
            i = l12.hashCode();
        }
        return hashCode16 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackInviteSent(inviteType=");
        R.append(this.inviteType);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", messageId=");
        R.append(this.messageId);
        R.append(", inviteGuildId=");
        R.append(this.inviteGuildId);
        R.append(", inviteChannelId=");
        R.append(this.inviteChannelId);
        R.append(", inviteChannelType=");
        R.append(this.inviteChannelType);
        R.append(", inviteInviterId=");
        R.append(this.inviteInviterId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", destinationUserId=");
        R.append(this.destinationUserId);
        R.append(", isSuggested=");
        R.append(this.isSuggested);
        R.append(", rowNum=");
        R.append(this.rowNum);
        R.append(", numTotal=");
        R.append(this.numTotal);
        R.append(", numAffinityConnections=");
        R.append(this.numAffinityConnections);
        R.append(", isFiltered=");
        R.append(this.isFiltered);
        R.append(", numSelected=");
        R.append(this.numSelected);
        R.append(", sendType=");
        R.append(this.sendType);
        R.append(", inviteGuildScheduledEventId=");
        return a.F(R, this.inviteGuildScheduledEventId, ")");
    }
}
