package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNetworkingSystemMetrics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b9\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0016\u0010\u0012R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0010\u001a\u0004\b\u0018\u0010\u0012R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0010\u001a\u0004\b\u001a\u0010\u0012R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0010\u001a\u0004\b\u001c\u0010\u0012R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0010\u001a\u0004\b\u001e\u0010\u0012R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0010\u001a\u0004\b \u0010\u0012R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012R\u001b\u0010%\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0010\u001a\u0004\b&\u0010\u0012R\u001b\u0010'\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0010\u001a\u0004\b(\u0010\u0012R\u001b\u0010)\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0010\u001a\u0004\b*\u0010\u0012R\u001b\u0010+\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0010\u001a\u0004\b,\u0010\u0012R\u001b\u0010-\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0010\u001a\u0004\b.\u0010\u0012R\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0010\u001a\u0004\b0\u0010\u0012R\u001b\u00101\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0010\u001a\u0004\b2\u0010\u0012R\u001b\u00103\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0010\u001a\u0004\b4\u0010\u0012R\u001b\u00105\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0010\u001a\u0004\b6\u0010\u0012R\u001b\u00107\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0010\u001a\u0004\b8\u0010\u0012R\u001b\u00109\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0010\u001a\u0004\b:\u0010\u0012R\u001b\u0010;\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0010\u001a\u0004\b<\u0010\u0012R\u001b\u0010=\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0010\u001a\u0004\b>\u0010\u0012R\u001b\u0010?\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0010\u001a\u0004\b@\u0010\u0012R\u001c\u0010A\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bA\u0010B\u001a\u0004\bC\u0010\u0005R\u001b\u0010D\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0010\u001a\u0004\bE\u0010\u0012R\u001b\u0010F\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u0010\u001a\u0004\bG\u0010\u0012R$\u0010I\u001a\u0004\u0018\u00010H8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bI\u0010J\u001a\u0004\bK\u0010L\"\u0004\bM\u0010NR\u001b\u0010O\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010\u0010\u001a\u0004\bP\u0010\u0012R\u001b\u0010Q\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u0010\u001a\u0004\bR\u0010\u0012R\u001b\u0010S\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010\u0010\u001a\u0004\bT\u0010\u0012R\u001b\u0010U\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u0010\u001a\u0004\bV\u0010\u0012¨\u0006W"}, d2 = {"Lcom/discord/analytics/generated/events/TrackNetworkingSystemMetrics;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "messagesDropped", "Ljava/lang/Long;", "getMessagesDropped", "()Ljava/lang/Long;", "messagesSent", "getMessagesSent", "dataSentPerPollMax", "getDataSentPerPollMax", "dataSentPerPollAvg", "getDataSentPerPollAvg", "dataReceived", "getDataReceived", "flushCalls", "getFlushCalls", "intervalNs", "getIntervalNs", "dataSentBytesAvg", "getDataSentBytesAvg", "pingRttStddev", "getPingRttStddev", "pingRttAvg", "getPingRttAvg", "pingsSent", "getPingsSent", "peerId", "getPeerId", "dataReceivedPerPollMax", "getDataReceivedPerPollMax", "dataReceivedBytesAvg", "getDataReceivedBytesAvg", "dataSentPerPollP95", "getDataSentPerPollP95", "applicationId", "getApplicationId", "pingRttMax", "getPingRttMax", "dataSent", "getDataSent", "dataReceivedPerPollP95", "getDataReceivedPerPollP95", "pollCalls", "getPollCalls", "routeChanges", "getRouteChanges", "messagesReceived", "getMessagesReceived", "dataSentBytesP95", "getDataSentBytesP95", "peerCount", "getPeerCount", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "pingRttP95", "getPingRttP95", "pongsReceived", "getPongsReceived", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "dataSentBytesMax", "getDataSentBytesMax", "dataReceivedPerPollAvg", "getDataReceivedPerPollAvg", "dataReceivedBytesP95", "getDataReceivedBytesP95", "dataReceivedBytesMax", "getDataReceivedBytesMax", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNetworkingSystemMetrics implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long applicationId = null;
    private final Long peerId = null;
    private final Long intervalNs = null;
    private final Long messagesSent = null;
    private final Long messagesDropped = null;
    private final Long messagesReceived = null;
    private final Long dataReceivedBytesAvg = null;
    private final Long dataReceivedBytesMax = null;
    private final Long dataReceivedBytesP95 = null;
    private final Long dataSentBytesAvg = null;
    private final Long dataSentBytesMax = null;
    private final Long dataSentBytesP95 = null;
    private final Long dataSentPerPollAvg = null;
    private final Long dataSentPerPollMax = null;
    private final Long dataSentPerPollP95 = null;
    private final Long dataReceivedPerPollAvg = null;
    private final Long dataReceivedPerPollMax = null;
    private final Long dataReceivedPerPollP95 = null;
    private final Long pingsSent = null;
    private final Long pongsReceived = null;
    private final Long dataSent = null;
    private final Long dataReceived = null;
    private final Long routeChanges = null;
    private final Long pingRttAvg = null;
    private final Long pingRttMax = null;
    private final Long pingRttP95 = null;
    private final Long pingRttStddev = null;
    private final Long pollCalls = null;
    private final Long flushCalls = null;
    private final Long peerCount = null;
    private final transient String analyticsSchemaTypeName = "networking_system_metrics";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNetworkingSystemMetrics)) {
            return false;
        }
        TrackNetworkingSystemMetrics trackNetworkingSystemMetrics = (TrackNetworkingSystemMetrics) obj;
        return m.areEqual(this.applicationId, trackNetworkingSystemMetrics.applicationId) && m.areEqual(this.peerId, trackNetworkingSystemMetrics.peerId) && m.areEqual(this.intervalNs, trackNetworkingSystemMetrics.intervalNs) && m.areEqual(this.messagesSent, trackNetworkingSystemMetrics.messagesSent) && m.areEqual(this.messagesDropped, trackNetworkingSystemMetrics.messagesDropped) && m.areEqual(this.messagesReceived, trackNetworkingSystemMetrics.messagesReceived) && m.areEqual(this.dataReceivedBytesAvg, trackNetworkingSystemMetrics.dataReceivedBytesAvg) && m.areEqual(this.dataReceivedBytesMax, trackNetworkingSystemMetrics.dataReceivedBytesMax) && m.areEqual(this.dataReceivedBytesP95, trackNetworkingSystemMetrics.dataReceivedBytesP95) && m.areEqual(this.dataSentBytesAvg, trackNetworkingSystemMetrics.dataSentBytesAvg) && m.areEqual(this.dataSentBytesMax, trackNetworkingSystemMetrics.dataSentBytesMax) && m.areEqual(this.dataSentBytesP95, trackNetworkingSystemMetrics.dataSentBytesP95) && m.areEqual(this.dataSentPerPollAvg, trackNetworkingSystemMetrics.dataSentPerPollAvg) && m.areEqual(this.dataSentPerPollMax, trackNetworkingSystemMetrics.dataSentPerPollMax) && m.areEqual(this.dataSentPerPollP95, trackNetworkingSystemMetrics.dataSentPerPollP95) && m.areEqual(this.dataReceivedPerPollAvg, trackNetworkingSystemMetrics.dataReceivedPerPollAvg) && m.areEqual(this.dataReceivedPerPollMax, trackNetworkingSystemMetrics.dataReceivedPerPollMax) && m.areEqual(this.dataReceivedPerPollP95, trackNetworkingSystemMetrics.dataReceivedPerPollP95) && m.areEqual(this.pingsSent, trackNetworkingSystemMetrics.pingsSent) && m.areEqual(this.pongsReceived, trackNetworkingSystemMetrics.pongsReceived) && m.areEqual(this.dataSent, trackNetworkingSystemMetrics.dataSent) && m.areEqual(this.dataReceived, trackNetworkingSystemMetrics.dataReceived) && m.areEqual(this.routeChanges, trackNetworkingSystemMetrics.routeChanges) && m.areEqual(this.pingRttAvg, trackNetworkingSystemMetrics.pingRttAvg) && m.areEqual(this.pingRttMax, trackNetworkingSystemMetrics.pingRttMax) && m.areEqual(this.pingRttP95, trackNetworkingSystemMetrics.pingRttP95) && m.areEqual(this.pingRttStddev, trackNetworkingSystemMetrics.pingRttStddev) && m.areEqual(this.pollCalls, trackNetworkingSystemMetrics.pollCalls) && m.areEqual(this.flushCalls, trackNetworkingSystemMetrics.flushCalls) && m.areEqual(this.peerCount, trackNetworkingSystemMetrics.peerCount);
    }

    public int hashCode() {
        Long l = this.applicationId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.peerId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.intervalNs;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.messagesSent;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.messagesDropped;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.messagesReceived;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.dataReceivedBytesAvg;
        int hashCode7 = (hashCode6 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.dataReceivedBytesMax;
        int hashCode8 = (hashCode7 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.dataReceivedBytesP95;
        int hashCode9 = (hashCode8 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.dataSentBytesAvg;
        int hashCode10 = (hashCode9 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.dataSentBytesMax;
        int hashCode11 = (hashCode10 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.dataSentBytesP95;
        int hashCode12 = (hashCode11 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.dataSentPerPollAvg;
        int hashCode13 = (hashCode12 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.dataSentPerPollMax;
        int hashCode14 = (hashCode13 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.dataSentPerPollP95;
        int hashCode15 = (hashCode14 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.dataReceivedPerPollAvg;
        int hashCode16 = (hashCode15 + (l16 != null ? l16.hashCode() : 0)) * 31;
        Long l17 = this.dataReceivedPerPollMax;
        int hashCode17 = (hashCode16 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.dataReceivedPerPollP95;
        int hashCode18 = (hashCode17 + (l18 != null ? l18.hashCode() : 0)) * 31;
        Long l19 = this.pingsSent;
        int hashCode19 = (hashCode18 + (l19 != null ? l19.hashCode() : 0)) * 31;
        Long l20 = this.pongsReceived;
        int hashCode20 = (hashCode19 + (l20 != null ? l20.hashCode() : 0)) * 31;
        Long l21 = this.dataSent;
        int hashCode21 = (hashCode20 + (l21 != null ? l21.hashCode() : 0)) * 31;
        Long l22 = this.dataReceived;
        int hashCode22 = (hashCode21 + (l22 != null ? l22.hashCode() : 0)) * 31;
        Long l23 = this.routeChanges;
        int hashCode23 = (hashCode22 + (l23 != null ? l23.hashCode() : 0)) * 31;
        Long l24 = this.pingRttAvg;
        int hashCode24 = (hashCode23 + (l24 != null ? l24.hashCode() : 0)) * 31;
        Long l25 = this.pingRttMax;
        int hashCode25 = (hashCode24 + (l25 != null ? l25.hashCode() : 0)) * 31;
        Long l26 = this.pingRttP95;
        int hashCode26 = (hashCode25 + (l26 != null ? l26.hashCode() : 0)) * 31;
        Long l27 = this.pingRttStddev;
        int hashCode27 = (hashCode26 + (l27 != null ? l27.hashCode() : 0)) * 31;
        Long l28 = this.pollCalls;
        int hashCode28 = (hashCode27 + (l28 != null ? l28.hashCode() : 0)) * 31;
        Long l29 = this.flushCalls;
        int hashCode29 = (hashCode28 + (l29 != null ? l29.hashCode() : 0)) * 31;
        Long l30 = this.peerCount;
        if (l30 != null) {
            i = l30.hashCode();
        }
        return hashCode29 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNetworkingSystemMetrics(applicationId=");
        R.append(this.applicationId);
        R.append(", peerId=");
        R.append(this.peerId);
        R.append(", intervalNs=");
        R.append(this.intervalNs);
        R.append(", messagesSent=");
        R.append(this.messagesSent);
        R.append(", messagesDropped=");
        R.append(this.messagesDropped);
        R.append(", messagesReceived=");
        R.append(this.messagesReceived);
        R.append(", dataReceivedBytesAvg=");
        R.append(this.dataReceivedBytesAvg);
        R.append(", dataReceivedBytesMax=");
        R.append(this.dataReceivedBytesMax);
        R.append(", dataReceivedBytesP95=");
        R.append(this.dataReceivedBytesP95);
        R.append(", dataSentBytesAvg=");
        R.append(this.dataSentBytesAvg);
        R.append(", dataSentBytesMax=");
        R.append(this.dataSentBytesMax);
        R.append(", dataSentBytesP95=");
        R.append(this.dataSentBytesP95);
        R.append(", dataSentPerPollAvg=");
        R.append(this.dataSentPerPollAvg);
        R.append(", dataSentPerPollMax=");
        R.append(this.dataSentPerPollMax);
        R.append(", dataSentPerPollP95=");
        R.append(this.dataSentPerPollP95);
        R.append(", dataReceivedPerPollAvg=");
        R.append(this.dataReceivedPerPollAvg);
        R.append(", dataReceivedPerPollMax=");
        R.append(this.dataReceivedPerPollMax);
        R.append(", dataReceivedPerPollP95=");
        R.append(this.dataReceivedPerPollP95);
        R.append(", pingsSent=");
        R.append(this.pingsSent);
        R.append(", pongsReceived=");
        R.append(this.pongsReceived);
        R.append(", dataSent=");
        R.append(this.dataSent);
        R.append(", dataReceived=");
        R.append(this.dataReceived);
        R.append(", routeChanges=");
        R.append(this.routeChanges);
        R.append(", pingRttAvg=");
        R.append(this.pingRttAvg);
        R.append(", pingRttMax=");
        R.append(this.pingRttMax);
        R.append(", pingRttP95=");
        R.append(this.pingRttP95);
        R.append(", pingRttStddev=");
        R.append(this.pingRttStddev);
        R.append(", pollCalls=");
        R.append(this.pollCalls);
        R.append(", flushCalls=");
        R.append(this.flushCalls);
        R.append(", peerCount=");
        return a.F(R, this.peerCount, ")");
    }
}
