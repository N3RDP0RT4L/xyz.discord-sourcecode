package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackApplicationSettingsUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u000b\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0014\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0006R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0018\u001a\u0004\b!\u0010\u001aR\u001b\u0010\"\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0011\u001a\u0004\b#\u0010\u0013R$\u0010%\u001a\u0004\u0018\u00010$8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R$\u0010,\u001a\u0004\u0018\u00010+8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001b\u00102\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u001d\u001a\u0004\b3\u0010\u001fR\u001b\u00104\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0018\u001a\u0004\b5\u0010\u001a¨\u00066"}, d2 = {"Lcom/discord/analytics/generated/events/TrackApplicationSettingsUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "launcherPlatform", "Ljava/lang/CharSequence;", "getLauncherPlatform", "()Ljava/lang/CharSequence;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "privateEnabled", "Ljava/lang/Boolean;", "getPrivateEnabled", "()Ljava/lang/Boolean;", "", "skuId", "Ljava/lang/Long;", "getSkuId", "()Ljava/lang/Long;", "hiddenEnabled", "getHiddenEnabled", "applicationName", "getApplicationName", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "applicationId", "getApplicationId", "overlayDisabled", "getOverlayDisabled", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackApplicationSettingsUpdated implements AnalyticsSchema, TrackBaseReceiver, TrackLocationMetadataReceiver {
    private TrackBase trackBase;
    private TrackLocationMetadata trackLocationMetadata;
    private final Boolean hiddenEnabled = null;
    private final Boolean overlayDisabled = null;
    private final Boolean privateEnabled = null;
    private final Long applicationId = null;
    private final CharSequence applicationName = null;
    private final Long skuId = null;
    private final CharSequence launcherPlatform = null;
    private final transient String analyticsSchemaTypeName = "application_settings_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackApplicationSettingsUpdated)) {
            return false;
        }
        TrackApplicationSettingsUpdated trackApplicationSettingsUpdated = (TrackApplicationSettingsUpdated) obj;
        return m.areEqual(this.hiddenEnabled, trackApplicationSettingsUpdated.hiddenEnabled) && m.areEqual(this.overlayDisabled, trackApplicationSettingsUpdated.overlayDisabled) && m.areEqual(this.privateEnabled, trackApplicationSettingsUpdated.privateEnabled) && m.areEqual(this.applicationId, trackApplicationSettingsUpdated.applicationId) && m.areEqual(this.applicationName, trackApplicationSettingsUpdated.applicationName) && m.areEqual(this.skuId, trackApplicationSettingsUpdated.skuId) && m.areEqual(this.launcherPlatform, trackApplicationSettingsUpdated.launcherPlatform);
    }

    public int hashCode() {
        Boolean bool = this.hiddenEnabled;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        Boolean bool2 = this.overlayDisabled;
        int hashCode2 = (hashCode + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.privateEnabled;
        int hashCode3 = (hashCode2 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l = this.applicationId;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence = this.applicationName;
        int hashCode5 = (hashCode4 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l2 = this.skuId;
        int hashCode6 = (hashCode5 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.launcherPlatform;
        if (charSequence2 != null) {
            i = charSequence2.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackApplicationSettingsUpdated(hiddenEnabled=");
        R.append(this.hiddenEnabled);
        R.append(", overlayDisabled=");
        R.append(this.overlayDisabled);
        R.append(", privateEnabled=");
        R.append(this.privateEnabled);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", applicationName=");
        R.append(this.applicationName);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", launcherPlatform=");
        return a.D(R, this.launcherPlatform, ")");
    }
}
