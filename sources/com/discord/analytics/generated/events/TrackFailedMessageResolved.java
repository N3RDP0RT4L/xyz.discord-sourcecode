package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.generated.traits.TrackGuildReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackFailedMessageResolved.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R!\u0010\u001a\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0012\u001a\u0004\b\u001b\u0010\u0014R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0016\u001a\u0004\b\u001d\u0010\u0018R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0012\u001a\u0004\b\u001f\u0010\u0014R$\u0010!\u001a\u0004\u0018\u00010 8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\u001b\u0010'\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0012\u001a\u0004\b(\u0010\u0014R\u001b\u0010)\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0012\u001a\u0004\b*\u0010\u0014R$\u0010,\u001a\u0004\u0018\u00010+8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001b\u00102\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0016\u001a\u0004\b3\u0010\u0018R\u001c\u00104\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u0010\u0007R\u001b\u00108\u001a\u0004\u0018\u0001078\u0006@\u0006¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010;R$\u0010=\u001a\u0004\u0018\u00010<8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@\"\u0004\bA\u0010B¨\u0006C"}, d2 = {"Lcom/discord/analytics/generated/events/TrackFailedMessageResolved;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "maxAttachmentSize", "Ljava/lang/Long;", "getMaxAttachmentSize", "()Ljava/lang/Long;", "hasVideo", "Ljava/lang/Boolean;", "getHasVideo", "()Ljava/lang/Boolean;", "Lcom/discord/primitives/Timestamp;", "initialAttemptTs", "getInitialAttemptTs", "previewEnabled", "getPreviewEnabled", "totalAttachmentSize", "getTotalAttachmentSize", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "numAttachments", "getNumAttachments", "numRetries", "getNumRetries", "Lcom/discord/analytics/generated/traits/TrackGuild;", "trackGuild", "Lcom/discord/analytics/generated/traits/TrackGuild;", "getTrackGuild", "()Lcom/discord/analytics/generated/traits/TrackGuild;", "setTrackGuild", "(Lcom/discord/analytics/generated/traits/TrackGuild;)V", "hasImage", "getHasImage", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "resolutionType", "Ljava/lang/CharSequence;", "getResolutionType", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackFailedMessageResolved implements AnalyticsSchema, TrackBaseReceiver, TrackGuildReceiver, TrackChannelReceiver {
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackGuild trackGuild;
    private final Long numAttachments = null;
    private final Long maxAttachmentSize = null;
    private final Long totalAttachmentSize = null;
    private final Boolean hasImage = null;
    private final Boolean hasVideo = null;
    private final CharSequence resolutionType = null;
    private final Long initialAttemptTs = null;
    private final Long numRetries = null;
    private final Boolean previewEnabled = null;
    private final transient String analyticsSchemaTypeName = "failed_message_resolved";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackFailedMessageResolved)) {
            return false;
        }
        TrackFailedMessageResolved trackFailedMessageResolved = (TrackFailedMessageResolved) obj;
        return m.areEqual(this.numAttachments, trackFailedMessageResolved.numAttachments) && m.areEqual(this.maxAttachmentSize, trackFailedMessageResolved.maxAttachmentSize) && m.areEqual(this.totalAttachmentSize, trackFailedMessageResolved.totalAttachmentSize) && m.areEqual(this.hasImage, trackFailedMessageResolved.hasImage) && m.areEqual(this.hasVideo, trackFailedMessageResolved.hasVideo) && m.areEqual(this.resolutionType, trackFailedMessageResolved.resolutionType) && m.areEqual(this.initialAttemptTs, trackFailedMessageResolved.initialAttemptTs) && m.areEqual(this.numRetries, trackFailedMessageResolved.numRetries) && m.areEqual(this.previewEnabled, trackFailedMessageResolved.previewEnabled);
    }

    public int hashCode() {
        Long l = this.numAttachments;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.maxAttachmentSize;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.totalAttachmentSize;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool = this.hasImage;
        int hashCode4 = (hashCode3 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasVideo;
        int hashCode5 = (hashCode4 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.resolutionType;
        int hashCode6 = (hashCode5 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l4 = this.initialAttemptTs;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.numRetries;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Boolean bool3 = this.previewEnabled;
        if (bool3 != null) {
            i = bool3.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackFailedMessageResolved(numAttachments=");
        R.append(this.numAttachments);
        R.append(", maxAttachmentSize=");
        R.append(this.maxAttachmentSize);
        R.append(", totalAttachmentSize=");
        R.append(this.totalAttachmentSize);
        R.append(", hasImage=");
        R.append(this.hasImage);
        R.append(", hasVideo=");
        R.append(this.hasVideo);
        R.append(", resolutionType=");
        R.append(this.resolutionType);
        R.append(", initialAttemptTs=");
        R.append(this.initialAttemptTs);
        R.append(", numRetries=");
        R.append(this.numRetries);
        R.append(", previewEnabled=");
        return a.C(R, this.previewEnabled, ")");
    }
}
