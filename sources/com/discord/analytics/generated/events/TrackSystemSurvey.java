package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackSystemSurvey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0016\u0010\u0012R$\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012R\u001b\u0010%\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0010\u001a\u0004\b&\u0010\u0012R\u001b\u0010'\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0010\u001a\u0004\b(\u0010\u0012R\u001b\u0010)\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0010\u001a\u0004\b*\u0010\u0012R\u001b\u0010+\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0010\u001a\u0004\b,\u0010\u0012R\u001c\u0010-\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u0010\u0005R\u001b\u00100\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010 \u001a\u0004\b1\u0010\"R\u001b\u00102\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010 \u001a\u0004\b3\u0010\"R\u001b\u00104\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0010\u001a\u0004\b5\u0010\u0012¨\u00066"}, d2 = {"Lcom/discord/analytics/generated/events/TrackSystemSurvey;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "displayMonitors", "Ljava/lang/Long;", "getDisplayMonitors", "()Ljava/lang/Long;", "displayDesktopWidth", "getDisplayDesktopWidth", "gpuCount", "getGpuCount", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "cpuBrand", "Ljava/lang/CharSequence;", "getCpuBrand", "()Ljava/lang/CharSequence;", "batteries", "getBatteries", "cpuMemory", "getCpuMemory", "displayDesktopHeight", "getDisplayDesktopHeight", "gpuMemory", "getGpuMemory", "displayPrimaryHeight", "getDisplayPrimaryHeight", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "cpuVendor", "getCpuVendor", "gpuBrand", "getGpuBrand", "displayPrimaryWidth", "getDisplayPrimaryWidth", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSystemSurvey implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence cpuBrand = null;
    private final CharSequence cpuVendor = null;
    private final Long cpuMemory = null;
    private final CharSequence gpuBrand = null;
    private final Long gpuCount = null;
    private final Long gpuMemory = null;
    private final Long batteries = null;
    private final Long displayMonitors = null;
    private final Long displayPrimaryWidth = null;
    private final Long displayPrimaryHeight = null;
    private final Long displayDesktopWidth = null;
    private final Long displayDesktopHeight = null;
    private final transient String analyticsSchemaTypeName = "system_survey";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSystemSurvey)) {
            return false;
        }
        TrackSystemSurvey trackSystemSurvey = (TrackSystemSurvey) obj;
        return m.areEqual(this.cpuBrand, trackSystemSurvey.cpuBrand) && m.areEqual(this.cpuVendor, trackSystemSurvey.cpuVendor) && m.areEqual(this.cpuMemory, trackSystemSurvey.cpuMemory) && m.areEqual(this.gpuBrand, trackSystemSurvey.gpuBrand) && m.areEqual(this.gpuCount, trackSystemSurvey.gpuCount) && m.areEqual(this.gpuMemory, trackSystemSurvey.gpuMemory) && m.areEqual(this.batteries, trackSystemSurvey.batteries) && m.areEqual(this.displayMonitors, trackSystemSurvey.displayMonitors) && m.areEqual(this.displayPrimaryWidth, trackSystemSurvey.displayPrimaryWidth) && m.areEqual(this.displayPrimaryHeight, trackSystemSurvey.displayPrimaryHeight) && m.areEqual(this.displayDesktopWidth, trackSystemSurvey.displayDesktopWidth) && m.areEqual(this.displayDesktopHeight, trackSystemSurvey.displayDesktopHeight);
    }

    public int hashCode() {
        CharSequence charSequence = this.cpuBrand;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.cpuVendor;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.cpuMemory;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.gpuBrand;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l2 = this.gpuCount;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.gpuMemory;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.batteries;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.displayMonitors;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.displayPrimaryWidth;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.displayPrimaryHeight;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.displayDesktopWidth;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.displayDesktopHeight;
        if (l9 != null) {
            i = l9.hashCode();
        }
        return hashCode11 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSystemSurvey(cpuBrand=");
        R.append(this.cpuBrand);
        R.append(", cpuVendor=");
        R.append(this.cpuVendor);
        R.append(", cpuMemory=");
        R.append(this.cpuMemory);
        R.append(", gpuBrand=");
        R.append(this.gpuBrand);
        R.append(", gpuCount=");
        R.append(this.gpuCount);
        R.append(", gpuMemory=");
        R.append(this.gpuMemory);
        R.append(", batteries=");
        R.append(this.batteries);
        R.append(", displayMonitors=");
        R.append(this.displayMonitors);
        R.append(", displayPrimaryWidth=");
        R.append(this.displayPrimaryWidth);
        R.append(", displayPrimaryHeight=");
        R.append(this.displayPrimaryHeight);
        R.append(", displayDesktopWidth=");
        R.append(this.displayDesktopWidth);
        R.append(", displayDesktopHeight=");
        return a.F(R, this.displayDesktopHeight, ")");
    }
}
