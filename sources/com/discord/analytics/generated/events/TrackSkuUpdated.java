package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackPriceByCurrency;
import com.discord.analytics.generated.traits.TrackPriceByCurrencyReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackSkuUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010 \n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R!\u0010\u0016\u001a\n\u0018\u00010\u0014j\u0004\u0018\u0001`\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0011\u001a\u0004\b\u001b\u0010\u0013R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0017\u001a\u0004\b\u001d\u0010\u0019R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0011\u001a\u0004\b\u001f\u0010\u0013R\u001b\u0010 \u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0017\u001a\u0004\b!\u0010\u0019R\u001b\u0010\"\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b\"\u0010$R\u001b\u0010%\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010#\u001a\u0004\b%\u0010$R$\u0010'\u001a\u0004\u0018\u00010&8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0011\u001a\u0004\b.\u0010\u0013R\u001b\u0010/\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0011\u001a\u0004\b0\u0010\u0013R!\u00102\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u0001018\u0006@\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105R!\u00106\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u0001018\u0006@\u0006¢\u0006\f\n\u0004\b6\u00103\u001a\u0004\b7\u00105R\u001b\u00108\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0017\u001a\u0004\b9\u0010\u0019R\u001b\u0010:\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010#\u001a\u0004\b;\u0010$R\u001c\u0010<\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010\u0006R\u001b\u0010?\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0011\u001a\u0004\b@\u0010\u0013R\u001b\u0010A\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0011\u001a\u0004\bB\u0010\u0013R\u001b\u0010C\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010#\u001a\u0004\bC\u0010$R!\u0010D\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u0001018\u0006@\u0006¢\u0006\f\n\u0004\bD\u00103\u001a\u0004\bE\u00105R$\u0010G\u001a\u0004\u0018\u00010F8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bG\u0010H\u001a\u0004\bI\u0010J\"\u0004\bK\u0010LR!\u0010M\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u0001018\u0006@\u0006¢\u0006\f\n\u0004\bM\u00103\u001a\u0004\bN\u00105R\u001b\u0010O\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010\u0017\u001a\u0004\bP\u0010\u0019¨\u0006Q"}, d2 = {"Lcom/discord/analytics/generated/events/TrackSkuUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackPriceByCurrencyReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "accessType", "Ljava/lang/CharSequence;", "getAccessType", "()Ljava/lang/CharSequence;", "", "Lcom/discord/primitives/Timestamp;", "releaseDate", "Ljava/lang/Long;", "getReleaseDate", "()Ljava/lang/Long;", "contentRatingPegi", "getContentRatingPegi", "priceTier", "getPriceTier", "storeTitle", "getStoreTitle", "dependentSkuId", "getDependentSkuId", "isDistribution", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "isExclusive", "Lcom/discord/analytics/generated/traits/TrackPriceByCurrency;", "trackPriceByCurrency", "Lcom/discord/analytics/generated/traits/TrackPriceByCurrency;", "getTrackPriceByCurrency", "()Lcom/discord/analytics/generated/traits/TrackPriceByCurrency;", "setTrackPriceByCurrency", "(Lcom/discord/analytics/generated/traits/TrackPriceByCurrency;)V", "metacriticId", "getMetacriticId", "updateType", "getUpdateType", "", "bundledSkuIds", "Ljava/util/List;", "getBundledSkuIds", "()Ljava/util/List;", "availableLocales", "getAvailableLocales", "regularPriceTier", "getRegularPriceTier", ModelAuditLogEntry.CHANGE_KEY_AVAILABLE, "getAvailable", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "contentRatingEsrb", "getContentRatingEsrb", "updateFrom", "getUpdateFrom", "isPremium", "features", "getFeatures", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "genres", "getGenres", "skuId", "getSkuId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSkuUpdated implements AnalyticsSchema, TrackBaseReceiver, TrackPriceByCurrencyReceiver {
    private TrackBase trackBase;
    private TrackPriceByCurrency trackPriceByCurrency;
    private final Long skuId = null;
    private final CharSequence updateFrom = null;
    private final CharSequence updateType = null;
    private final CharSequence storeTitle = null;
    private final Long dependentSkuId = null;
    private final List<Long> bundledSkuIds = null;
    private final CharSequence accessType = null;
    private final Long releaseDate = null;
    private final Boolean isPremium = null;
    private final Boolean isDistribution = null;
    private final Boolean available = null;
    private final List<CharSequence> availableLocales = null;
    private final CharSequence metacriticId = null;
    private final Boolean isExclusive = null;
    private final List<CharSequence> genres = null;
    private final List<CharSequence> features = null;
    private final CharSequence contentRatingPegi = null;
    private final CharSequence contentRatingEsrb = null;
    private final Long regularPriceTier = null;
    private final Long priceTier = null;
    private final transient String analyticsSchemaTypeName = "sku_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSkuUpdated)) {
            return false;
        }
        TrackSkuUpdated trackSkuUpdated = (TrackSkuUpdated) obj;
        return m.areEqual(this.skuId, trackSkuUpdated.skuId) && m.areEqual(this.updateFrom, trackSkuUpdated.updateFrom) && m.areEqual(this.updateType, trackSkuUpdated.updateType) && m.areEqual(this.storeTitle, trackSkuUpdated.storeTitle) && m.areEqual(this.dependentSkuId, trackSkuUpdated.dependentSkuId) && m.areEqual(this.bundledSkuIds, trackSkuUpdated.bundledSkuIds) && m.areEqual(this.accessType, trackSkuUpdated.accessType) && m.areEqual(this.releaseDate, trackSkuUpdated.releaseDate) && m.areEqual(this.isPremium, trackSkuUpdated.isPremium) && m.areEqual(this.isDistribution, trackSkuUpdated.isDistribution) && m.areEqual(this.available, trackSkuUpdated.available) && m.areEqual(this.availableLocales, trackSkuUpdated.availableLocales) && m.areEqual(this.metacriticId, trackSkuUpdated.metacriticId) && m.areEqual(this.isExclusive, trackSkuUpdated.isExclusive) && m.areEqual(this.genres, trackSkuUpdated.genres) && m.areEqual(this.features, trackSkuUpdated.features) && m.areEqual(this.contentRatingPegi, trackSkuUpdated.contentRatingPegi) && m.areEqual(this.contentRatingEsrb, trackSkuUpdated.contentRatingEsrb) && m.areEqual(this.regularPriceTier, trackSkuUpdated.regularPriceTier) && m.areEqual(this.priceTier, trackSkuUpdated.priceTier);
    }

    public int hashCode() {
        Long l = this.skuId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.updateFrom;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.updateType;
        int hashCode3 = (hashCode2 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.storeTitle;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l2 = this.dependentSkuId;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        List<Long> list = this.bundledSkuIds;
        int hashCode6 = (hashCode5 + (list != null ? list.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.accessType;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l3 = this.releaseDate;
        int hashCode8 = (hashCode7 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool = this.isPremium;
        int hashCode9 = (hashCode8 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.isDistribution;
        int hashCode10 = (hashCode9 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.available;
        int hashCode11 = (hashCode10 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        List<CharSequence> list2 = this.availableLocales;
        int hashCode12 = (hashCode11 + (list2 != null ? list2.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.metacriticId;
        int hashCode13 = (hashCode12 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Boolean bool4 = this.isExclusive;
        int hashCode14 = (hashCode13 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        List<CharSequence> list3 = this.genres;
        int hashCode15 = (hashCode14 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<CharSequence> list4 = this.features;
        int hashCode16 = (hashCode15 + (list4 != null ? list4.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.contentRatingPegi;
        int hashCode17 = (hashCode16 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.contentRatingEsrb;
        int hashCode18 = (hashCode17 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        Long l4 = this.regularPriceTier;
        int hashCode19 = (hashCode18 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.priceTier;
        if (l5 != null) {
            i = l5.hashCode();
        }
        return hashCode19 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSkuUpdated(skuId=");
        R.append(this.skuId);
        R.append(", updateFrom=");
        R.append(this.updateFrom);
        R.append(", updateType=");
        R.append(this.updateType);
        R.append(", storeTitle=");
        R.append(this.storeTitle);
        R.append(", dependentSkuId=");
        R.append(this.dependentSkuId);
        R.append(", bundledSkuIds=");
        R.append(this.bundledSkuIds);
        R.append(", accessType=");
        R.append(this.accessType);
        R.append(", releaseDate=");
        R.append(this.releaseDate);
        R.append(", isPremium=");
        R.append(this.isPremium);
        R.append(", isDistribution=");
        R.append(this.isDistribution);
        R.append(", available=");
        R.append(this.available);
        R.append(", availableLocales=");
        R.append(this.availableLocales);
        R.append(", metacriticId=");
        R.append(this.metacriticId);
        R.append(", isExclusive=");
        R.append(this.isExclusive);
        R.append(", genres=");
        R.append(this.genres);
        R.append(", features=");
        R.append(this.features);
        R.append(", contentRatingPegi=");
        R.append(this.contentRatingPegi);
        R.append(", contentRatingEsrb=");
        R.append(this.contentRatingEsrb);
        R.append(", regularPriceTier=");
        R.append(this.regularPriceTier);
        R.append(", priceTier=");
        return a.F(R, this.priceTier, ")");
    }
}
