package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNativeEchoCancellationConfigured.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u001b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u000e\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0005R$\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0019\u001a\u0004\b\u001d\u0010\u001bR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0019\u001a\u0004\b\u001f\u0010\u001bR\u001b\u0010 \u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0019\u001a\u0004\b!\u0010\u001bR\u001b\u0010\"\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0019\u001a\u0004\b#\u0010\u001bR\u001b\u0010$\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0019\u001a\u0004\b%\u0010\u001bR\u001b\u0010&\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0019\u001a\u0004\b'\u0010\u001bR\u001b\u0010(\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0019\u001a\u0004\b)\u0010\u001bR\u001b\u0010*\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0019\u001a\u0004\b+\u0010\u001b¨\u0006,"}, d2 = {"Lcom/discord/analytics/generated/events/TrackNativeEchoCancellationConfigured;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "aecEnabledInSettings", "Ljava/lang/Boolean;", "getAecEnabledInSettings", "()Ljava/lang/Boolean;", "builtinAecRequested", "getBuiltinAecRequested", "aecMobileModeByDefault", "getAecMobileModeByDefault", "aecMobileMode", "getAecMobileMode", "builtinAecSupportedJava", "getBuiltinAecSupportedJava", "builtinAecEnabled", "getBuiltinAecEnabled", "aecEnabledInNativeConfig", "getAecEnabledInNativeConfig", "builtinAecSupportedNative", "getBuiltinAecSupportedNative", "aecEnabledByDefault", "getAecEnabledByDefault", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNativeEchoCancellationConfigured implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Boolean builtinAecSupportedJava = null;
    private final Boolean builtinAecSupportedNative = null;
    private final Boolean builtinAecRequested = null;
    private final Boolean builtinAecEnabled = null;
    private final Boolean aecEnabledInSettings = null;
    private final Boolean aecEnabledInNativeConfig = null;
    private final Boolean aecMobileMode = null;
    private final Boolean aecEnabledByDefault = null;
    private final Boolean aecMobileModeByDefault = null;
    private final transient String analyticsSchemaTypeName = "native_echo_cancellation_configured";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNativeEchoCancellationConfigured)) {
            return false;
        }
        TrackNativeEchoCancellationConfigured trackNativeEchoCancellationConfigured = (TrackNativeEchoCancellationConfigured) obj;
        return m.areEqual(this.builtinAecSupportedJava, trackNativeEchoCancellationConfigured.builtinAecSupportedJava) && m.areEqual(this.builtinAecSupportedNative, trackNativeEchoCancellationConfigured.builtinAecSupportedNative) && m.areEqual(this.builtinAecRequested, trackNativeEchoCancellationConfigured.builtinAecRequested) && m.areEqual(this.builtinAecEnabled, trackNativeEchoCancellationConfigured.builtinAecEnabled) && m.areEqual(this.aecEnabledInSettings, trackNativeEchoCancellationConfigured.aecEnabledInSettings) && m.areEqual(this.aecEnabledInNativeConfig, trackNativeEchoCancellationConfigured.aecEnabledInNativeConfig) && m.areEqual(this.aecMobileMode, trackNativeEchoCancellationConfigured.aecMobileMode) && m.areEqual(this.aecEnabledByDefault, trackNativeEchoCancellationConfigured.aecEnabledByDefault) && m.areEqual(this.aecMobileModeByDefault, trackNativeEchoCancellationConfigured.aecMobileModeByDefault);
    }

    public int hashCode() {
        Boolean bool = this.builtinAecSupportedJava;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        Boolean bool2 = this.builtinAecSupportedNative;
        int hashCode2 = (hashCode + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.builtinAecRequested;
        int hashCode3 = (hashCode2 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.builtinAecEnabled;
        int hashCode4 = (hashCode3 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Boolean bool5 = this.aecEnabledInSettings;
        int hashCode5 = (hashCode4 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.aecEnabledInNativeConfig;
        int hashCode6 = (hashCode5 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        Boolean bool7 = this.aecMobileMode;
        int hashCode7 = (hashCode6 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        Boolean bool8 = this.aecEnabledByDefault;
        int hashCode8 = (hashCode7 + (bool8 != null ? bool8.hashCode() : 0)) * 31;
        Boolean bool9 = this.aecMobileModeByDefault;
        if (bool9 != null) {
            i = bool9.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNativeEchoCancellationConfigured(builtinAecSupportedJava=");
        R.append(this.builtinAecSupportedJava);
        R.append(", builtinAecSupportedNative=");
        R.append(this.builtinAecSupportedNative);
        R.append(", builtinAecRequested=");
        R.append(this.builtinAecRequested);
        R.append(", builtinAecEnabled=");
        R.append(this.builtinAecEnabled);
        R.append(", aecEnabledInSettings=");
        R.append(this.aecEnabledInSettings);
        R.append(", aecEnabledInNativeConfig=");
        R.append(this.aecEnabledInNativeConfig);
        R.append(", aecMobileMode=");
        R.append(this.aecMobileMode);
        R.append(", aecEnabledByDefault=");
        R.append(this.aecEnabledByDefault);
        R.append(", aecMobileModeByDefault=");
        return a.C(R, this.aecMobileModeByDefault, ")");
    }
}
