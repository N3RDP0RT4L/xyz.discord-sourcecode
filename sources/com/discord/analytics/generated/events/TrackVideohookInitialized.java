package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackVideohookInitialized.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\r\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0014\u001a\u0004\b\u0018\u0010\u0016R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u001a\u0010\u0016R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0014\u001a\u0004\b!\u0010\u0016R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u000f\u001a\u0004\b#\u0010\u0011R\u001c\u0010$\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010\u0005R\u001b\u0010'\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u001d\u001a\u0004\b(\u0010\u001fR\u001b\u0010)\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0014\u001a\u0004\b*\u0010\u0016R$\u0010,\u001a\u0004\u0018\u00010+8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/\"\u0004\b0\u00101¨\u00062"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVideohookInitialized;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "success", "Ljava/lang/Boolean;", "getSuccess", "()Ljava/lang/Boolean;", "", "sampleCount", "Ljava/lang/Long;", "getSampleCount", "()Ljava/lang/Long;", "framebufferFormat", "getFramebufferFormat", "backend", "getBackend", "", "soundshareSession", "Ljava/lang/CharSequence;", "getSoundshareSession", "()Ljava/lang/CharSequence;", "format", "getFormat", "reinitialization", "getReinitialization", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "shareGameName", "getShareGameName", "shareGameId", "getShareGameId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVideohookInitialized implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long backend = null;
    private final Long format = null;
    private final Long framebufferFormat = null;
    private final Long sampleCount = null;
    private final Boolean success = null;
    private final Boolean reinitialization = null;
    private final CharSequence soundshareSession = null;
    private final CharSequence shareGameName = null;
    private final Long shareGameId = null;
    private final transient String analyticsSchemaTypeName = "videohook_initialized";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVideohookInitialized)) {
            return false;
        }
        TrackVideohookInitialized trackVideohookInitialized = (TrackVideohookInitialized) obj;
        return m.areEqual(this.backend, trackVideohookInitialized.backend) && m.areEqual(this.format, trackVideohookInitialized.format) && m.areEqual(this.framebufferFormat, trackVideohookInitialized.framebufferFormat) && m.areEqual(this.sampleCount, trackVideohookInitialized.sampleCount) && m.areEqual(this.success, trackVideohookInitialized.success) && m.areEqual(this.reinitialization, trackVideohookInitialized.reinitialization) && m.areEqual(this.soundshareSession, trackVideohookInitialized.soundshareSession) && m.areEqual(this.shareGameName, trackVideohookInitialized.shareGameName) && m.areEqual(this.shareGameId, trackVideohookInitialized.shareGameId);
    }

    public int hashCode() {
        Long l = this.backend;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.format;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.framebufferFormat;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.sampleCount;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool = this.success;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.reinitialization;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.soundshareSession;
        int hashCode7 = (hashCode6 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.shareGameName;
        int hashCode8 = (hashCode7 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l5 = this.shareGameId;
        if (l5 != null) {
            i = l5.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVideohookInitialized(backend=");
        R.append(this.backend);
        R.append(", format=");
        R.append(this.format);
        R.append(", framebufferFormat=");
        R.append(this.framebufferFormat);
        R.append(", sampleCount=");
        R.append(this.sampleCount);
        R.append(", success=");
        R.append(this.success);
        R.append(", reinitialization=");
        R.append(this.reinitialization);
        R.append(", soundshareSession=");
        R.append(this.soundshareSession);
        R.append(", shareGameName=");
        R.append(this.shareGameName);
        R.append(", shareGameId=");
        return a.F(R, this.shareGameId, ")");
    }
}
