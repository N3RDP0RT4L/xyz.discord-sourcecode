package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackAfLoaded.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b'\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0011\u001a\u0004\b\u001a\u0010\u0013R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0011\u001a\u0004\b\u001c\u0010\u0013R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0011\u001a\u0004\b\u001e\u0010\u0013R$\u0010 \u001a\u0004\u0018\u00010\u001f8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R!\u0010'\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001b\u0010+\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0011\u001a\u0004\b,\u0010\u0013R\u001b\u0010-\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0011\u001a\u0004\b.\u0010\u0013R\u001c\u0010/\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u0006R$\u00103\u001a\u0004\u0018\u0001028\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b3\u00104\u001a\u0004\b5\u00106\"\u0004\b7\u00108R\u001b\u00109\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0011\u001a\u0004\b:\u0010\u0013R\u001b\u0010;\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0011\u001a\u0004\b<\u0010\u0013R!\u0010=\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010(\u001a\u0004\b>\u0010*R\u001b\u0010?\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0011\u001a\u0004\b@\u0010\u0013R\u001b\u0010A\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0011\u001a\u0004\bB\u0010\u0013R\u001b\u0010C\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0011\u001a\u0004\bD\u0010\u0013R\u001b\u0010E\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0011\u001a\u0004\bF\u0010\u0013R\u001b\u0010G\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0011\u001a\u0004\bH\u0010\u0013R\u001b\u0010I\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u0011\u001a\u0004\bJ\u0010\u0013R\u001b\u0010K\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\u0016\u001a\u0004\bL\u0010\u0018R!\u0010M\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010(\u001a\u0004\bN\u0010*R\u001b\u0010O\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010\u0011\u001a\u0004\bP\u0010\u0013R\u001b\u0010Q\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u0011\u001a\u0004\bR\u0010\u0013R\u001b\u0010S\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010\u0011\u001a\u0004\bT\u0010\u0013R\u001b\u0010U\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u0011\u001a\u0004\bV\u0010\u0013R!\u0010W\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010(\u001a\u0004\bX\u0010*¨\u0006Y"}, d2 = {"Lcom/discord/analytics/generated/events/TrackAfLoaded;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "numRecentlyPlayedCollapsed", "Ljava/lang/Long;", "getNumRecentlyPlayedCollapsed", "()Ljava/lang/Long;", "", "loadId", "Ljava/lang/CharSequence;", "getLoadId", "()Ljava/lang/CharSequence;", "numGameParties", "getNumGameParties", "numGamePartiesSolo", "getNumGamePartiesSolo", "numSoloCollapsed", "getNumSoloCollapsed", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "guildIdsViewed", "Ljava/util/List;", "getGuildIdsViewed", "()Ljava/util/List;", "numCards", "getNumCards", "numCardsGameNews", "getNumCardsGameNews", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "numGamePartiesRichPresence", "getNumGamePartiesRichPresence", "numGamePartiesVoice", "getNumGamePartiesVoice", "subscribedGames", "getSubscribedGames", "numLauncherApplications", "getNumLauncherApplications", "numCardsGamePlayable", "getNumCardsGamePlayable", "numCardsVisible", "getNumCardsVisible", "numUsersSubscribed", "getNumUsersSubscribed", "windowWidth", "getWindowWidth", "windowHeight", "getWindowHeight", "feedLayout", "getFeedLayout", "newsIdsViewed", "getNewsIdsViewed", "numGamePartiesRecentlyPlayed", "getNumGamePartiesRecentlyPlayed", "numItemsNowPlaying", "getNumItemsNowPlaying", "numItemsRecentlyPlayed", "getNumItemsRecentlyPlayed", "numGamePartiesCollapsed", "getNumGamePartiesCollapsed", "gameIds", "getGameIds", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackAfLoaded implements AnalyticsSchema, TrackBaseReceiver, TrackLocationMetadataReceiver {
    private TrackBase trackBase;
    private TrackLocationMetadata trackLocationMetadata;
    private final CharSequence loadId = null;
    private final List<Long> gameIds = null;
    private final Long numCards = null;
    private final Long numCardsVisible = null;
    private final Long numCardsGameNews = null;
    private final Long numCardsGamePlayable = null;
    private final Long numGameParties = null;
    private final Long numGamePartiesVoice = null;
    private final Long numGamePartiesSolo = null;
    private final Long numGamePartiesRecentlyPlayed = null;
    private final Long numGamePartiesRichPresence = null;
    private final Long numGamePartiesCollapsed = null;
    private final Long numSoloCollapsed = null;
    private final Long numRecentlyPlayedCollapsed = null;
    private final Long numUsersSubscribed = null;
    private final Long numLauncherApplications = null;
    private final Long windowWidth = null;
    private final Long windowHeight = null;
    private final CharSequence feedLayout = null;
    private final List<Long> subscribedGames = null;
    private final Long numItemsNowPlaying = null;
    private final Long numItemsRecentlyPlayed = null;
    private final List<Long> newsIdsViewed = null;
    private final List<Long> guildIdsViewed = null;
    private final transient String analyticsSchemaTypeName = "af_loaded";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackAfLoaded)) {
            return false;
        }
        TrackAfLoaded trackAfLoaded = (TrackAfLoaded) obj;
        return m.areEqual(this.loadId, trackAfLoaded.loadId) && m.areEqual(this.gameIds, trackAfLoaded.gameIds) && m.areEqual(this.numCards, trackAfLoaded.numCards) && m.areEqual(this.numCardsVisible, trackAfLoaded.numCardsVisible) && m.areEqual(this.numCardsGameNews, trackAfLoaded.numCardsGameNews) && m.areEqual(this.numCardsGamePlayable, trackAfLoaded.numCardsGamePlayable) && m.areEqual(this.numGameParties, trackAfLoaded.numGameParties) && m.areEqual(this.numGamePartiesVoice, trackAfLoaded.numGamePartiesVoice) && m.areEqual(this.numGamePartiesSolo, trackAfLoaded.numGamePartiesSolo) && m.areEqual(this.numGamePartiesRecentlyPlayed, trackAfLoaded.numGamePartiesRecentlyPlayed) && m.areEqual(this.numGamePartiesRichPresence, trackAfLoaded.numGamePartiesRichPresence) && m.areEqual(this.numGamePartiesCollapsed, trackAfLoaded.numGamePartiesCollapsed) && m.areEqual(this.numSoloCollapsed, trackAfLoaded.numSoloCollapsed) && m.areEqual(this.numRecentlyPlayedCollapsed, trackAfLoaded.numRecentlyPlayedCollapsed) && m.areEqual(this.numUsersSubscribed, trackAfLoaded.numUsersSubscribed) && m.areEqual(this.numLauncherApplications, trackAfLoaded.numLauncherApplications) && m.areEqual(this.windowWidth, trackAfLoaded.windowWidth) && m.areEqual(this.windowHeight, trackAfLoaded.windowHeight) && m.areEqual(this.feedLayout, trackAfLoaded.feedLayout) && m.areEqual(this.subscribedGames, trackAfLoaded.subscribedGames) && m.areEqual(this.numItemsNowPlaying, trackAfLoaded.numItemsNowPlaying) && m.areEqual(this.numItemsRecentlyPlayed, trackAfLoaded.numItemsRecentlyPlayed) && m.areEqual(this.newsIdsViewed, trackAfLoaded.newsIdsViewed) && m.areEqual(this.guildIdsViewed, trackAfLoaded.guildIdsViewed);
    }

    public int hashCode() {
        CharSequence charSequence = this.loadId;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        List<Long> list = this.gameIds;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        Long l = this.numCards;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.numCardsVisible;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.numCardsGameNews;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.numCardsGamePlayable;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.numGameParties;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.numGamePartiesVoice;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.numGamePartiesSolo;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.numGamePartiesRecentlyPlayed;
        int hashCode10 = (hashCode9 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.numGamePartiesRichPresence;
        int hashCode11 = (hashCode10 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.numGamePartiesCollapsed;
        int hashCode12 = (hashCode11 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.numSoloCollapsed;
        int hashCode13 = (hashCode12 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.numRecentlyPlayedCollapsed;
        int hashCode14 = (hashCode13 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.numUsersSubscribed;
        int hashCode15 = (hashCode14 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.numLauncherApplications;
        int hashCode16 = (hashCode15 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.windowWidth;
        int hashCode17 = (hashCode16 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.windowHeight;
        int hashCode18 = (hashCode17 + (l16 != null ? l16.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.feedLayout;
        int hashCode19 = (hashCode18 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        List<Long> list2 = this.subscribedGames;
        int hashCode20 = (hashCode19 + (list2 != null ? list2.hashCode() : 0)) * 31;
        Long l17 = this.numItemsNowPlaying;
        int hashCode21 = (hashCode20 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.numItemsRecentlyPlayed;
        int hashCode22 = (hashCode21 + (l18 != null ? l18.hashCode() : 0)) * 31;
        List<Long> list3 = this.newsIdsViewed;
        int hashCode23 = (hashCode22 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<Long> list4 = this.guildIdsViewed;
        if (list4 != null) {
            i = list4.hashCode();
        }
        return hashCode23 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackAfLoaded(loadId=");
        R.append(this.loadId);
        R.append(", gameIds=");
        R.append(this.gameIds);
        R.append(", numCards=");
        R.append(this.numCards);
        R.append(", numCardsVisible=");
        R.append(this.numCardsVisible);
        R.append(", numCardsGameNews=");
        R.append(this.numCardsGameNews);
        R.append(", numCardsGamePlayable=");
        R.append(this.numCardsGamePlayable);
        R.append(", numGameParties=");
        R.append(this.numGameParties);
        R.append(", numGamePartiesVoice=");
        R.append(this.numGamePartiesVoice);
        R.append(", numGamePartiesSolo=");
        R.append(this.numGamePartiesSolo);
        R.append(", numGamePartiesRecentlyPlayed=");
        R.append(this.numGamePartiesRecentlyPlayed);
        R.append(", numGamePartiesRichPresence=");
        R.append(this.numGamePartiesRichPresence);
        R.append(", numGamePartiesCollapsed=");
        R.append(this.numGamePartiesCollapsed);
        R.append(", numSoloCollapsed=");
        R.append(this.numSoloCollapsed);
        R.append(", numRecentlyPlayedCollapsed=");
        R.append(this.numRecentlyPlayedCollapsed);
        R.append(", numUsersSubscribed=");
        R.append(this.numUsersSubscribed);
        R.append(", numLauncherApplications=");
        R.append(this.numLauncherApplications);
        R.append(", windowWidth=");
        R.append(this.windowWidth);
        R.append(", windowHeight=");
        R.append(this.windowHeight);
        R.append(", feedLayout=");
        R.append(this.feedLayout);
        R.append(", subscribedGames=");
        R.append(this.subscribedGames);
        R.append(", numItemsNowPlaying=");
        R.append(this.numItemsNowPlaying);
        R.append(", numItemsRecentlyPlayed=");
        R.append(this.numItemsRecentlyPlayed);
        R.append(", newsIdsViewed=");
        R.append(this.newsIdsViewed);
        R.append(", guildIdsViewed=");
        return a.K(R, this.guildIdsViewed, ")");
    }
}
