package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNotificationRendered.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0010\u001a\u0004\b#\u0010\u0012R\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R$\u0010'\u001a\u0004\u0018\u00010&8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0010\u001a\u0004\b.\u0010\u0012R\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0010\u001a\u0004\b0\u0010\u0012R\u001b\u00102\u001a\u0004\u0018\u0001018\u0006@\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105R\u001b\u00106\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0010\u001a\u0004\b7\u0010\u0012R\u001b\u00108\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0010\u001a\u0004\b9\u0010\u0012R\u001b\u0010:\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0010\u001a\u0004\b;\u0010\u0012R\u001b\u0010<\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0017\u001a\u0004\b=\u0010\u0019R\u001b\u0010>\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0010\u001a\u0004\b?\u0010\u0012R\u001b\u0010@\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0010\u001a\u0004\bA\u0010\u0012R\u001b\u0010B\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0010\u001a\u0004\bC\u0010\u0012R\u001b\u0010D\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0010\u001a\u0004\bE\u0010\u0012¨\u0006F"}, d2 = {"Lcom/discord/analytics/generated/events/TrackNotificationRendered;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "connectionOpenDurationMs", "Ljava/lang/Long;", "getConnectionOpenDurationMs", "()Ljava/lang/Long;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "hasCache", "Ljava/lang/Boolean;", "getHasCache", "()Ljava/lang/Boolean;", "fullCacheLoadDurationMs", "getFullCacheLoadDurationMs", "onlineDurationMs", "getOnlineDurationMs", "appActiveDurationMs", "getAppActiveDurationMs", "cachedRenderDurationMs", "getCachedRenderDurationMs", "connectionPreOpenDurationMs", "getConnectionPreOpenDurationMs", "firstMessageFetchStartDurationMs", "getFirstMessageFetchStartDurationMs", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "coldStartDurationMs", "getColdStartDurationMs", "loadedMessagesRenderDurationMs", "getLoadedMessagesRenderDurationMs", "", "initialAppState", "Ljava/lang/CharSequence;", "getInitialAppState", "()Ljava/lang/CharSequence;", "appInactiveDurationMs", "getAppInactiveDurationMs", "appBackgroundedDurationMs", "getAppBackgroundedDurationMs", "cacheLoadDurationMs", "getCacheLoadDurationMs", "messageInPushPayload", "getMessageInPushPayload", "firstMessageFetchEndDurationMs", "getFirstMessageFetchEndDurationMs", "anyNewerMessagesRenderDurationMs", "getAnyNewerMessagesRenderDurationMs", "connectionResumeDurationMs", "getConnectionResumeDurationMs", "fullRenderDurationMs", "getFullRenderDurationMs", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNotificationRendered implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Boolean hasCache = null;
    private final Boolean messageInPushPayload = null;
    private final Long coldStartDurationMs = null;
    private final Long cachedRenderDurationMs = null;
    private final Long anyNewerMessagesRenderDurationMs = null;
    private final Long loadedMessagesRenderDurationMs = null;
    private final Long fullRenderDurationMs = null;
    private final Long cacheLoadDurationMs = null;
    private final Long fullCacheLoadDurationMs = null;
    private final Long connectionPreOpenDurationMs = null;
    private final Long connectionOpenDurationMs = null;
    private final Long connectionResumeDurationMs = null;
    private final Long onlineDurationMs = null;
    private final Long firstMessageFetchStartDurationMs = null;
    private final Long firstMessageFetchEndDurationMs = null;
    private final CharSequence initialAppState = null;
    private final Long appActiveDurationMs = null;
    private final Long appInactiveDurationMs = null;
    private final Long appBackgroundedDurationMs = null;
    private final transient String analyticsSchemaTypeName = "notification_rendered";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNotificationRendered)) {
            return false;
        }
        TrackNotificationRendered trackNotificationRendered = (TrackNotificationRendered) obj;
        return m.areEqual(this.hasCache, trackNotificationRendered.hasCache) && m.areEqual(this.messageInPushPayload, trackNotificationRendered.messageInPushPayload) && m.areEqual(this.coldStartDurationMs, trackNotificationRendered.coldStartDurationMs) && m.areEqual(this.cachedRenderDurationMs, trackNotificationRendered.cachedRenderDurationMs) && m.areEqual(this.anyNewerMessagesRenderDurationMs, trackNotificationRendered.anyNewerMessagesRenderDurationMs) && m.areEqual(this.loadedMessagesRenderDurationMs, trackNotificationRendered.loadedMessagesRenderDurationMs) && m.areEqual(this.fullRenderDurationMs, trackNotificationRendered.fullRenderDurationMs) && m.areEqual(this.cacheLoadDurationMs, trackNotificationRendered.cacheLoadDurationMs) && m.areEqual(this.fullCacheLoadDurationMs, trackNotificationRendered.fullCacheLoadDurationMs) && m.areEqual(this.connectionPreOpenDurationMs, trackNotificationRendered.connectionPreOpenDurationMs) && m.areEqual(this.connectionOpenDurationMs, trackNotificationRendered.connectionOpenDurationMs) && m.areEqual(this.connectionResumeDurationMs, trackNotificationRendered.connectionResumeDurationMs) && m.areEqual(this.onlineDurationMs, trackNotificationRendered.onlineDurationMs) && m.areEqual(this.firstMessageFetchStartDurationMs, trackNotificationRendered.firstMessageFetchStartDurationMs) && m.areEqual(this.firstMessageFetchEndDurationMs, trackNotificationRendered.firstMessageFetchEndDurationMs) && m.areEqual(this.initialAppState, trackNotificationRendered.initialAppState) && m.areEqual(this.appActiveDurationMs, trackNotificationRendered.appActiveDurationMs) && m.areEqual(this.appInactiveDurationMs, trackNotificationRendered.appInactiveDurationMs) && m.areEqual(this.appBackgroundedDurationMs, trackNotificationRendered.appBackgroundedDurationMs);
    }

    public int hashCode() {
        Boolean bool = this.hasCache;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        Boolean bool2 = this.messageInPushPayload;
        int hashCode2 = (hashCode + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l = this.coldStartDurationMs;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.cachedRenderDurationMs;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.anyNewerMessagesRenderDurationMs;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.loadedMessagesRenderDurationMs;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.fullRenderDurationMs;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.cacheLoadDurationMs;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.fullCacheLoadDurationMs;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.connectionPreOpenDurationMs;
        int hashCode10 = (hashCode9 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.connectionOpenDurationMs;
        int hashCode11 = (hashCode10 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.connectionResumeDurationMs;
        int hashCode12 = (hashCode11 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.onlineDurationMs;
        int hashCode13 = (hashCode12 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.firstMessageFetchStartDurationMs;
        int hashCode14 = (hashCode13 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.firstMessageFetchEndDurationMs;
        int hashCode15 = (hashCode14 + (l13 != null ? l13.hashCode() : 0)) * 31;
        CharSequence charSequence = this.initialAppState;
        int hashCode16 = (hashCode15 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l14 = this.appActiveDurationMs;
        int hashCode17 = (hashCode16 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.appInactiveDurationMs;
        int hashCode18 = (hashCode17 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.appBackgroundedDurationMs;
        if (l16 != null) {
            i = l16.hashCode();
        }
        return hashCode18 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNotificationRendered(hasCache=");
        R.append(this.hasCache);
        R.append(", messageInPushPayload=");
        R.append(this.messageInPushPayload);
        R.append(", coldStartDurationMs=");
        R.append(this.coldStartDurationMs);
        R.append(", cachedRenderDurationMs=");
        R.append(this.cachedRenderDurationMs);
        R.append(", anyNewerMessagesRenderDurationMs=");
        R.append(this.anyNewerMessagesRenderDurationMs);
        R.append(", loadedMessagesRenderDurationMs=");
        R.append(this.loadedMessagesRenderDurationMs);
        R.append(", fullRenderDurationMs=");
        R.append(this.fullRenderDurationMs);
        R.append(", cacheLoadDurationMs=");
        R.append(this.cacheLoadDurationMs);
        R.append(", fullCacheLoadDurationMs=");
        R.append(this.fullCacheLoadDurationMs);
        R.append(", connectionPreOpenDurationMs=");
        R.append(this.connectionPreOpenDurationMs);
        R.append(", connectionOpenDurationMs=");
        R.append(this.connectionOpenDurationMs);
        R.append(", connectionResumeDurationMs=");
        R.append(this.connectionResumeDurationMs);
        R.append(", onlineDurationMs=");
        R.append(this.onlineDurationMs);
        R.append(", firstMessageFetchStartDurationMs=");
        R.append(this.firstMessageFetchStartDurationMs);
        R.append(", firstMessageFetchEndDurationMs=");
        R.append(this.firstMessageFetchEndDurationMs);
        R.append(", initialAppState=");
        R.append(this.initialAppState);
        R.append(", appActiveDurationMs=");
        R.append(this.appActiveDurationMs);
        R.append(", appInactiveDurationMs=");
        R.append(this.appInactiveDurationMs);
        R.append(", appBackgroundedDurationMs=");
        return a.F(R, this.appBackgroundedDurationMs, ")");
    }
}
