package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.generated.traits.TrackGuildReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.analytics.generated.traits.TrackSourceMetadata;
import com.discord.analytics.generated.traits.TrackSourceMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackOpenModal.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\f\n\u0002\u0010\t\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0014\u001a\u0004\b\u0018\u0010\u0016R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u001a\u0010\u0016R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010 \u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u001c\u001a\u0004\b%\u0010\u001eR\u001b\u0010&\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010!\u001a\u0004\b'\u0010#R\u001b\u0010(\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0014\u001a\u0004\b)\u0010\u0016R\u001b\u0010*\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0014\u001a\u0004\b+\u0010\u0016R\u001b\u0010,\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0014\u001a\u0004\b-\u0010\u0016R\u001b\u0010.\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010!\u001a\u0004\b/\u0010#R$\u00101\u001a\u0004\u0018\u0001008\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b1\u00102\u001a\u0004\b3\u00104\"\u0004\b5\u00106R$\u00108\u001a\u0004\u0018\u0001078\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b8\u00109\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001b\u0010>\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0014\u001a\u0004\b?\u0010\u0016R\u001b\u0010@\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010!\u001a\u0004\bA\u0010#R\u001b\u0010B\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0014\u001a\u0004\bC\u0010\u0016R$\u0010E\u001a\u0004\u0018\u00010D8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bE\u0010F\u001a\u0004\bG\u0010H\"\u0004\bI\u0010JR\u001b\u0010K\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\u0014\u001a\u0004\bL\u0010\u0016R\u001b\u0010M\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010!\u001a\u0004\bN\u0010#R\u001b\u0010O\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010\u001c\u001a\u0004\bO\u0010\u001eR\u001b\u0010P\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010\u001c\u001a\u0004\bP\u0010\u001eR\u001c\u0010Q\u001a\u00020\u00078\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bQ\u0010R\u001a\u0004\bS\u0010\tR$\u0010U\u001a\u0004\u0018\u00010T8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bU\u0010V\u001a\u0004\bW\u0010X\"\u0004\bY\u0010ZR\u001b\u0010[\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b[\u0010\u0014\u001a\u0004\b\\\u0010\u0016R\u001b\u0010]\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b]\u0010!\u001a\u0004\b^\u0010#R$\u0010`\u001a\u0004\u0018\u00010_8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b`\u0010a\u001a\u0004\bb\u0010c\"\u0004\bd\u0010eR\u001b\u0010f\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bf\u0010!\u001a\u0004\bg\u0010#R\u001b\u0010h\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bh\u0010\u0014\u001a\u0004\bi\u0010\u0016R\u001b\u0010j\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bj\u0010!\u001a\u0004\bk\u0010#¨\u0006l"}, d2 = {"Lcom/discord/analytics/generated/events/TrackOpenModal;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackSourceMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "type", "Ljava/lang/CharSequence;", "getType", "()Ljava/lang/CharSequence;", "partyId", "getPartyId", "promotionId", "getPromotionId", "hasImages", "Ljava/lang/Boolean;", "getHasImages", "()Ljava/lang/Boolean;", "", "otherUserId", "Ljava/lang/Long;", "getOtherUserId", "()Ljava/lang/Long;", "profileHasNitroCustomization", "getProfileHasNitroCustomization", "messageContentLength", "getMessageContentLength", "deviceName", "getDeviceName", "loadId", "getLoadId", "profileUserStatus", "getProfileUserStatus", "applicationId", "getApplicationId", "Lcom/discord/analytics/generated/traits/TrackSourceMetadata;", "trackSourceMetadata", "Lcom/discord/analytics/generated/traits/TrackSourceMetadata;", "getTrackSourceMetadata", "()Lcom/discord/analytics/generated/traits/TrackSourceMetadata;", "setTrackSourceMetadata", "(Lcom/discord/analytics/generated/traits/TrackSourceMetadata;)V", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "gamePlatform", "getGamePlatform", "guildEventsCount", "getGuildEventsCount", "source", "getSource", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "applicationName", "getApplicationName", "partyMax", "getPartyMax", "isFriend", "isAdminUser", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "partyPlatform", "getPartyPlatform", "gameId", "getGameId", "Lcom/discord/analytics/generated/traits/TrackGuild;", "trackGuild", "Lcom/discord/analytics/generated/traits/TrackGuild;", "getTrackGuild", "()Lcom/discord/analytics/generated/traits/TrackGuild;", "c", "(Lcom/discord/analytics/generated/traits/TrackGuild;)V", "numGuildPermissions", "getNumGuildPermissions", "gameName", "getGameName", "skuId", "getSkuId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackOpenModal implements AnalyticsSchema, TrackBaseReceiver, TrackChannelReceiver, TrackGuildReceiver, TrackLocationMetadataReceiver, TrackSourceMetadataReceiver {
    private final transient String analyticsSchemaTypeName;
    private final Long applicationId;
    private final CharSequence applicationName;
    private final CharSequence deviceName;
    private final Long gameId;
    private final CharSequence gameName;
    private final CharSequence gamePlatform;
    private final Long guildEventsCount;
    private final Boolean hasImages;
    private final Boolean isAdminUser;
    private final Boolean isFriend;
    private final CharSequence loadId;
    private final Long messageContentLength;
    private final Long numGuildPermissions;
    private final Long otherUserId;
    private final CharSequence partyId;
    private final Long partyMax;
    private final CharSequence partyPlatform;
    private final Boolean profileHasNitroCustomization;
    private final CharSequence profileUserStatus;
    private final CharSequence promotionId;
    private final Long skuId;
    private final CharSequence source;
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackGuild trackGuild;
    private TrackLocationMetadata trackLocationMetadata;
    private TrackSourceMetadata trackSourceMetadata;
    private final CharSequence type;

    public TrackOpenModal() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 8388607);
    }

    public TrackOpenModal(CharSequence charSequence, CharSequence charSequence2, Long l, Long l2, CharSequence charSequence3, Boolean bool, Boolean bool2, Long l3, CharSequence charSequence4, CharSequence charSequence5, Long l4, CharSequence charSequence6, CharSequence charSequence7, Long l5, CharSequence charSequence8, CharSequence charSequence9, CharSequence charSequence10, Long l6, Long l7, CharSequence charSequence11, Boolean bool3, Long l8, Boolean bool4, int i) {
        int i2 = i & 1;
        CharSequence charSequence12 = (i & 2) != 0 ? null : charSequence2;
        int i3 = i & 4;
        int i4 = i & 8;
        int i5 = i & 16;
        int i6 = i & 32;
        int i7 = i & 64;
        int i8 = i & 128;
        int i9 = i & 256;
        int i10 = i & 512;
        int i11 = i & 1024;
        int i12 = i & 2048;
        int i13 = i & 4096;
        int i14 = i & 8192;
        int i15 = i & 16384;
        int i16 = 32768 & i;
        int i17 = 65536 & i;
        int i18 = 131072 & i;
        int i19 = 262144 & i;
        int i20 = 524288 & i;
        int i21 = 1048576 & i;
        Long l9 = (2097152 & i) != 0 ? null : l8;
        int i22 = i & 4194304;
        this.source = null;
        this.type = charSequence12;
        this.otherUserId = null;
        this.applicationId = null;
        this.applicationName = null;
        this.isFriend = null;
        this.hasImages = null;
        this.partyMax = null;
        this.partyId = null;
        this.partyPlatform = null;
        this.gameId = null;
        this.gameName = null;
        this.gamePlatform = null;
        this.skuId = null;
        this.deviceName = null;
        this.profileUserStatus = null;
        this.loadId = null;
        this.numGuildPermissions = null;
        this.messageContentLength = null;
        this.promotionId = null;
        this.profileHasNitroCustomization = null;
        this.guildEventsCount = l9;
        this.isAdminUser = null;
        this.analyticsSchemaTypeName = "open_modal";
    }

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public void c(TrackGuild trackGuild) {
        this.trackGuild = trackGuild;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackOpenModal)) {
            return false;
        }
        TrackOpenModal trackOpenModal = (TrackOpenModal) obj;
        return m.areEqual(this.source, trackOpenModal.source) && m.areEqual(this.type, trackOpenModal.type) && m.areEqual(this.otherUserId, trackOpenModal.otherUserId) && m.areEqual(this.applicationId, trackOpenModal.applicationId) && m.areEqual(this.applicationName, trackOpenModal.applicationName) && m.areEqual(this.isFriend, trackOpenModal.isFriend) && m.areEqual(this.hasImages, trackOpenModal.hasImages) && m.areEqual(this.partyMax, trackOpenModal.partyMax) && m.areEqual(this.partyId, trackOpenModal.partyId) && m.areEqual(this.partyPlatform, trackOpenModal.partyPlatform) && m.areEqual(this.gameId, trackOpenModal.gameId) && m.areEqual(this.gameName, trackOpenModal.gameName) && m.areEqual(this.gamePlatform, trackOpenModal.gamePlatform) && m.areEqual(this.skuId, trackOpenModal.skuId) && m.areEqual(this.deviceName, trackOpenModal.deviceName) && m.areEqual(this.profileUserStatus, trackOpenModal.profileUserStatus) && m.areEqual(this.loadId, trackOpenModal.loadId) && m.areEqual(this.numGuildPermissions, trackOpenModal.numGuildPermissions) && m.areEqual(this.messageContentLength, trackOpenModal.messageContentLength) && m.areEqual(this.promotionId, trackOpenModal.promotionId) && m.areEqual(this.profileHasNitroCustomization, trackOpenModal.profileHasNitroCustomization) && m.areEqual(this.guildEventsCount, trackOpenModal.guildEventsCount) && m.areEqual(this.isAdminUser, trackOpenModal.isAdminUser);
    }

    public int hashCode() {
        CharSequence charSequence = this.source;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.type;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.otherUserId;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.applicationId;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.applicationName;
        int hashCode5 = (hashCode4 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Boolean bool = this.isFriend;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasImages;
        int hashCode7 = (hashCode6 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l3 = this.partyMax;
        int hashCode8 = (hashCode7 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.partyId;
        int hashCode9 = (hashCode8 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.partyPlatform;
        int hashCode10 = (hashCode9 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l4 = this.gameId;
        int hashCode11 = (hashCode10 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.gameName;
        int hashCode12 = (hashCode11 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.gamePlatform;
        int hashCode13 = (hashCode12 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        Long l5 = this.skuId;
        int hashCode14 = (hashCode13 + (l5 != null ? l5.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.deviceName;
        int hashCode15 = (hashCode14 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.profileUserStatus;
        int hashCode16 = (hashCode15 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.loadId;
        int hashCode17 = (hashCode16 + (charSequence10 != null ? charSequence10.hashCode() : 0)) * 31;
        Long l6 = this.numGuildPermissions;
        int hashCode18 = (hashCode17 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.messageContentLength;
        int hashCode19 = (hashCode18 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence11 = this.promotionId;
        int hashCode20 = (hashCode19 + (charSequence11 != null ? charSequence11.hashCode() : 0)) * 31;
        Boolean bool3 = this.profileHasNitroCustomization;
        int hashCode21 = (hashCode20 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l8 = this.guildEventsCount;
        int hashCode22 = (hashCode21 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Boolean bool4 = this.isAdminUser;
        if (bool4 != null) {
            i = bool4.hashCode();
        }
        return hashCode22 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackOpenModal(source=");
        R.append(this.source);
        R.append(", type=");
        R.append(this.type);
        R.append(", otherUserId=");
        R.append(this.otherUserId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", applicationName=");
        R.append(this.applicationName);
        R.append(", isFriend=");
        R.append(this.isFriend);
        R.append(", hasImages=");
        R.append(this.hasImages);
        R.append(", partyMax=");
        R.append(this.partyMax);
        R.append(", partyId=");
        R.append(this.partyId);
        R.append(", partyPlatform=");
        R.append(this.partyPlatform);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", gameName=");
        R.append(this.gameName);
        R.append(", gamePlatform=");
        R.append(this.gamePlatform);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", deviceName=");
        R.append(this.deviceName);
        R.append(", profileUserStatus=");
        R.append(this.profileUserStatus);
        R.append(", loadId=");
        R.append(this.loadId);
        R.append(", numGuildPermissions=");
        R.append(this.numGuildPermissions);
        R.append(", messageContentLength=");
        R.append(this.messageContentLength);
        R.append(", promotionId=");
        R.append(this.promotionId);
        R.append(", profileHasNitroCustomization=");
        R.append(this.profileHasNitroCustomization);
        R.append(", guildEventsCount=");
        R.append(this.guildEventsCount);
        R.append(", isAdminUser=");
        return a.C(R, this.isAdminUser, ")");
    }
}
