package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackStreamWarningTriggered.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\r\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R$\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010$\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001b\u0010(\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010%\u001a\u0004\b)\u0010'R\u001b\u0010*\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010%\u001a\u0004\b+\u0010'R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012R\u001b\u0010.\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010%\u001a\u0004\b/\u0010'R\u001b\u00100\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010%\u001a\u0004\b1\u0010'R\u001b\u00102\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010%\u001a\u0004\b3\u0010'¨\u00064"}, d2 = {"Lcom/discord/analytics/generated/events/TrackStreamWarningTriggered;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "streamerUserId", "Ljava/lang/Long;", "getStreamerUserId", "()Ljava/lang/Long;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "videoEnabled", "Ljava/lang/Boolean;", "getVideoEnabled", "()Ljava/lang/Boolean;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "streamChannelId", "getStreamChannelId", "", "guildRegion", "Ljava/lang/CharSequence;", "getGuildRegion", "()Ljava/lang/CharSequence;", "streamRegion", "getStreamRegion", "applicationName", "getApplicationName", "applicationId", "getApplicationId", "videoLayout", "getVideoLayout", "noticeType", "getNoticeType", "mediaSessionId", "getMediaSessionId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackStreamWarningTriggered implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence noticeType = null;
    private final Long streamerUserId = null;
    private final Long streamChannelId = null;
    private final CharSequence streamRegion = null;
    private final CharSequence guildRegion = null;
    private final Boolean videoEnabled = null;
    private final CharSequence applicationName = null;
    private final Long applicationId = null;
    private final CharSequence videoLayout = null;
    private final CharSequence mediaSessionId = null;
    private final transient String analyticsSchemaTypeName = "stream_warning_triggered";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackStreamWarningTriggered)) {
            return false;
        }
        TrackStreamWarningTriggered trackStreamWarningTriggered = (TrackStreamWarningTriggered) obj;
        return m.areEqual(this.noticeType, trackStreamWarningTriggered.noticeType) && m.areEqual(this.streamerUserId, trackStreamWarningTriggered.streamerUserId) && m.areEqual(this.streamChannelId, trackStreamWarningTriggered.streamChannelId) && m.areEqual(this.streamRegion, trackStreamWarningTriggered.streamRegion) && m.areEqual(this.guildRegion, trackStreamWarningTriggered.guildRegion) && m.areEqual(this.videoEnabled, trackStreamWarningTriggered.videoEnabled) && m.areEqual(this.applicationName, trackStreamWarningTriggered.applicationName) && m.areEqual(this.applicationId, trackStreamWarningTriggered.applicationId) && m.areEqual(this.videoLayout, trackStreamWarningTriggered.videoLayout) && m.areEqual(this.mediaSessionId, trackStreamWarningTriggered.mediaSessionId);
    }

    public int hashCode() {
        CharSequence charSequence = this.noticeType;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.streamerUserId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.streamChannelId;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.streamRegion;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.guildRegion;
        int hashCode5 = (hashCode4 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Boolean bool = this.videoEnabled;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.applicationName;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l3 = this.applicationId;
        int hashCode8 = (hashCode7 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.videoLayout;
        int hashCode9 = (hashCode8 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.mediaSessionId;
        if (charSequence6 != null) {
            i = charSequence6.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackStreamWarningTriggered(noticeType=");
        R.append(this.noticeType);
        R.append(", streamerUserId=");
        R.append(this.streamerUserId);
        R.append(", streamChannelId=");
        R.append(this.streamChannelId);
        R.append(", streamRegion=");
        R.append(this.streamRegion);
        R.append(", guildRegion=");
        R.append(this.guildRegion);
        R.append(", videoEnabled=");
        R.append(this.videoEnabled);
        R.append(", applicationName=");
        R.append(this.applicationName);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", videoLayout=");
        R.append(this.videoLayout);
        R.append(", mediaSessionId=");
        return a.D(R, this.mediaSessionId, ")");
    }
}
