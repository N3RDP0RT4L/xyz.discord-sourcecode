package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackMarketingSite;
import com.discord.analytics.generated.traits.TrackMarketingSiteReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackMktgPageViewed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0006R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R$\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0014\u001a\u0004\b\u001f\u0010\u0016R\u001b\u0010 \u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0014\u001a\u0004\b!\u0010\u0016R\u001b\u0010\"\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R$\u0010'\u001a\u0004\u0018\u00010&8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0014\u001a\u0004\b.\u0010\u0016R\u001b\u0010/\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0014\u001a\u0004\b0\u0010\u0016¨\u00061"}, d2 = {"Lcom/discord/analytics/generated/events/TrackMktgPageViewed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackMarketingSiteReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "previousLinkLocation", "Ljava/lang/CharSequence;", "getPreviousLinkLocation", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "pageName", "getPageName", "queryReferrer", "getQueryReferrer", "hasSession", "Ljava/lang/Boolean;", "getHasSession", "()Ljava/lang/Boolean;", "Lcom/discord/analytics/generated/traits/TrackMarketingSite;", "trackMarketingSite", "Lcom/discord/analytics/generated/traits/TrackMarketingSite;", "getTrackMarketingSite", "()Lcom/discord/analytics/generated/traits/TrackMarketingSite;", "setTrackMarketingSite", "(Lcom/discord/analytics/generated/traits/TrackMarketingSite;)V", "staticExperimentUuid", "getStaticExperimentUuid", "previousPageName", "getPreviousPageName", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackMktgPageViewed implements AnalyticsSchema, TrackBaseReceiver, TrackMarketingSiteReceiver {
    private TrackBase trackBase;
    private TrackMarketingSite trackMarketingSite;
    private final CharSequence pageName = null;
    private final CharSequence previousPageName = null;
    private final CharSequence previousLinkLocation = null;
    private final CharSequence queryReferrer = null;
    private final Boolean hasSession = null;
    private final CharSequence staticExperimentUuid = null;
    private final transient String analyticsSchemaTypeName = "mktg_page_viewed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackMktgPageViewed)) {
            return false;
        }
        TrackMktgPageViewed trackMktgPageViewed = (TrackMktgPageViewed) obj;
        return m.areEqual(this.pageName, trackMktgPageViewed.pageName) && m.areEqual(this.previousPageName, trackMktgPageViewed.previousPageName) && m.areEqual(this.previousLinkLocation, trackMktgPageViewed.previousLinkLocation) && m.areEqual(this.queryReferrer, trackMktgPageViewed.queryReferrer) && m.areEqual(this.hasSession, trackMktgPageViewed.hasSession) && m.areEqual(this.staticExperimentUuid, trackMktgPageViewed.staticExperimentUuid);
    }

    public int hashCode() {
        CharSequence charSequence = this.pageName;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.previousPageName;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.previousLinkLocation;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.queryReferrer;
        int hashCode4 = (hashCode3 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Boolean bool = this.hasSession;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.staticExperimentUuid;
        if (charSequence5 != null) {
            i = charSequence5.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackMktgPageViewed(pageName=");
        R.append(this.pageName);
        R.append(", previousPageName=");
        R.append(this.previousPageName);
        R.append(", previousLinkLocation=");
        R.append(this.previousLinkLocation);
        R.append(", queryReferrer=");
        R.append(this.queryReferrer);
        R.append(", hasSession=");
        R.append(this.hasSession);
        R.append(", staticExperimentUuid=");
        return a.D(R, this.staticExperimentUuid, ")");
    }
}
