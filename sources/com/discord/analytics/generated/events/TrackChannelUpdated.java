package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackChannelUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b \b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0011\u001a\u0004\b\u001e\u0010\u0013R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0015\u001a\u0004\b \u0010\u0017R\u001b\u0010!\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0015\u001a\u0004\b\"\u0010\u0017R$\u0010$\u001a\u0004\u0018\u00010#8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u001b\u0010*\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0011\u001a\u0004\b+\u0010\u0013R\u001b\u0010,\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0011\u001a\u0004\b-\u0010\u0013R\u001b\u0010.\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u001a\u001a\u0004\b/\u0010\u001cR\u001b\u00100\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u001a\u001a\u0004\b1\u0010\u001cR\u001b\u00102\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0015\u001a\u0004\b3\u0010\u0017R\u001b\u00104\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u001a\u001a\u0004\b5\u0010\u001cR\u001b\u00106\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u001a\u001a\u0004\b7\u0010\u001cR$\u00109\u001a\u0004\u0018\u0001088\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b9\u0010:\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>R\u001b\u0010?\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u001a\u001a\u0004\b@\u0010\u001cR\u001b\u0010A\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0015\u001a\u0004\bB\u0010\u0017R\u001b\u0010C\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0015\u001a\u0004\bD\u0010\u0017R\u001b\u0010E\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0015\u001a\u0004\bF\u0010\u0017R\u001b\u0010G\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u001a\u001a\u0004\bH\u0010\u001cR\u001b\u0010I\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u001a\u001a\u0004\bJ\u0010\u001cR\u001b\u0010K\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\u001a\u001a\u0004\bL\u0010\u001cR\u001b\u0010M\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010\u0015\u001a\u0004\bN\u0010\u0017R\u001c\u0010O\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bO\u0010P\u001a\u0004\bQ\u0010\u0006R\u001b\u0010R\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010\u0011\u001a\u0004\bS\u0010\u0013R\u001b\u0010T\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010\u0011\u001a\u0004\bU\u0010\u0013R\u001b\u0010V\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bV\u0010\u0011\u001a\u0004\bW\u0010\u0013¨\u0006X"}, d2 = {"Lcom/discord/analytics/generated/events/TrackChannelUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "oldRtcRegion", "Ljava/lang/CharSequence;", "getOldRtcRegion", "()Ljava/lang/CharSequence;", "oldInvitable", "Ljava/lang/Boolean;", "getOldInvitable", "()Ljava/lang/Boolean;", "", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "guildName", "getGuildName", "newNsfw", "getNewNsfw", "oldArchived", "getOldArchived", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "newBannerHash", "getNewBannerHash", "oldName", "getOldName", "videoQualityMode", "getVideoQualityMode", "oldDefaultAutoArchiveDurationMinutes", "getOldDefaultAutoArchiveDurationMinutes", "oldLocked", "getOldLocked", "oldBitrate", "getOldBitrate", "oldAutoArchiveDurationMinutes", "getOldAutoArchiveDurationMinutes", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "oldVideoQualityMode", "getOldVideoQualityMode", "newInvitable", "getNewInvitable", "newLocked", "getNewLocked", "newArchived", "getNewArchived", "defaultAutoArchiveDurationMinutes", "getDefaultAutoArchiveDurationMinutes", "newAutoArchiveDurationMinutes", "getNewAutoArchiveDurationMinutes", ModelAuditLogEntry.CHANGE_KEY_BITRATE, "getBitrate", "oldNsfw", "getOldNsfw", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "rtcRegion", "getRtcRegion", "oldBannerHash", "getOldBannerHash", "newName", "getNewName", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackChannelUpdated implements AnalyticsSchema, TrackBaseReceiver, TrackChannelReceiver {
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private final Long guildId = null;
    private final CharSequence guildName = null;
    private final CharSequence oldName = null;
    private final CharSequence newName = null;
    private final Boolean oldNsfw = null;
    private final Boolean newNsfw = null;
    private final Long bitrate = null;
    private final Long oldBitrate = null;
    private final CharSequence rtcRegion = null;
    private final CharSequence oldRtcRegion = null;
    private final Long videoQualityMode = null;
    private final Long oldVideoQualityMode = null;
    private final Long defaultAutoArchiveDurationMinutes = null;
    private final Long oldDefaultAutoArchiveDurationMinutes = null;
    private final Boolean oldArchived = null;
    private final Boolean newArchived = null;
    private final Boolean oldLocked = null;
    private final Boolean newLocked = null;
    private final Boolean oldInvitable = null;
    private final Boolean newInvitable = null;
    private final Long oldAutoArchiveDurationMinutes = null;
    private final Long newAutoArchiveDurationMinutes = null;
    private final CharSequence oldBannerHash = null;
    private final CharSequence newBannerHash = null;
    private final transient String analyticsSchemaTypeName = "channel_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackChannelUpdated)) {
            return false;
        }
        TrackChannelUpdated trackChannelUpdated = (TrackChannelUpdated) obj;
        return m.areEqual(this.guildId, trackChannelUpdated.guildId) && m.areEqual(this.guildName, trackChannelUpdated.guildName) && m.areEqual(this.oldName, trackChannelUpdated.oldName) && m.areEqual(this.newName, trackChannelUpdated.newName) && m.areEqual(this.oldNsfw, trackChannelUpdated.oldNsfw) && m.areEqual(this.newNsfw, trackChannelUpdated.newNsfw) && m.areEqual(this.bitrate, trackChannelUpdated.bitrate) && m.areEqual(this.oldBitrate, trackChannelUpdated.oldBitrate) && m.areEqual(this.rtcRegion, trackChannelUpdated.rtcRegion) && m.areEqual(this.oldRtcRegion, trackChannelUpdated.oldRtcRegion) && m.areEqual(this.videoQualityMode, trackChannelUpdated.videoQualityMode) && m.areEqual(this.oldVideoQualityMode, trackChannelUpdated.oldVideoQualityMode) && m.areEqual(this.defaultAutoArchiveDurationMinutes, trackChannelUpdated.defaultAutoArchiveDurationMinutes) && m.areEqual(this.oldDefaultAutoArchiveDurationMinutes, trackChannelUpdated.oldDefaultAutoArchiveDurationMinutes) && m.areEqual(this.oldArchived, trackChannelUpdated.oldArchived) && m.areEqual(this.newArchived, trackChannelUpdated.newArchived) && m.areEqual(this.oldLocked, trackChannelUpdated.oldLocked) && m.areEqual(this.newLocked, trackChannelUpdated.newLocked) && m.areEqual(this.oldInvitable, trackChannelUpdated.oldInvitable) && m.areEqual(this.newInvitable, trackChannelUpdated.newInvitable) && m.areEqual(this.oldAutoArchiveDurationMinutes, trackChannelUpdated.oldAutoArchiveDurationMinutes) && m.areEqual(this.newAutoArchiveDurationMinutes, trackChannelUpdated.newAutoArchiveDurationMinutes) && m.areEqual(this.oldBannerHash, trackChannelUpdated.oldBannerHash) && m.areEqual(this.newBannerHash, trackChannelUpdated.newBannerHash);
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.guildName;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.oldName;
        int hashCode3 = (hashCode2 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.newName;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Boolean bool = this.oldNsfw;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.newNsfw;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l2 = this.bitrate;
        int hashCode7 = (hashCode6 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.oldBitrate;
        int hashCode8 = (hashCode7 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.rtcRegion;
        int hashCode9 = (hashCode8 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.oldRtcRegion;
        int hashCode10 = (hashCode9 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l4 = this.videoQualityMode;
        int hashCode11 = (hashCode10 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.oldVideoQualityMode;
        int hashCode12 = (hashCode11 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.defaultAutoArchiveDurationMinutes;
        int hashCode13 = (hashCode12 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.oldDefaultAutoArchiveDurationMinutes;
        int hashCode14 = (hashCode13 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Boolean bool3 = this.oldArchived;
        int hashCode15 = (hashCode14 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.newArchived;
        int hashCode16 = (hashCode15 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Boolean bool5 = this.oldLocked;
        int hashCode17 = (hashCode16 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.newLocked;
        int hashCode18 = (hashCode17 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        Boolean bool7 = this.oldInvitable;
        int hashCode19 = (hashCode18 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        Boolean bool8 = this.newInvitable;
        int hashCode20 = (hashCode19 + (bool8 != null ? bool8.hashCode() : 0)) * 31;
        Long l8 = this.oldAutoArchiveDurationMinutes;
        int hashCode21 = (hashCode20 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.newAutoArchiveDurationMinutes;
        int hashCode22 = (hashCode21 + (l9 != null ? l9.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.oldBannerHash;
        int hashCode23 = (hashCode22 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.newBannerHash;
        if (charSequence7 != null) {
            i = charSequence7.hashCode();
        }
        return hashCode23 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackChannelUpdated(guildId=");
        R.append(this.guildId);
        R.append(", guildName=");
        R.append(this.guildName);
        R.append(", oldName=");
        R.append(this.oldName);
        R.append(", newName=");
        R.append(this.newName);
        R.append(", oldNsfw=");
        R.append(this.oldNsfw);
        R.append(", newNsfw=");
        R.append(this.newNsfw);
        R.append(", bitrate=");
        R.append(this.bitrate);
        R.append(", oldBitrate=");
        R.append(this.oldBitrate);
        R.append(", rtcRegion=");
        R.append(this.rtcRegion);
        R.append(", oldRtcRegion=");
        R.append(this.oldRtcRegion);
        R.append(", videoQualityMode=");
        R.append(this.videoQualityMode);
        R.append(", oldVideoQualityMode=");
        R.append(this.oldVideoQualityMode);
        R.append(", defaultAutoArchiveDurationMinutes=");
        R.append(this.defaultAutoArchiveDurationMinutes);
        R.append(", oldDefaultAutoArchiveDurationMinutes=");
        R.append(this.oldDefaultAutoArchiveDurationMinutes);
        R.append(", oldArchived=");
        R.append(this.oldArchived);
        R.append(", newArchived=");
        R.append(this.newArchived);
        R.append(", oldLocked=");
        R.append(this.oldLocked);
        R.append(", newLocked=");
        R.append(this.newLocked);
        R.append(", oldInvitable=");
        R.append(this.oldInvitable);
        R.append(", newInvitable=");
        R.append(this.newInvitable);
        R.append(", oldAutoArchiveDurationMinutes=");
        R.append(this.oldAutoArchiveDurationMinutes);
        R.append(", newAutoArchiveDurationMinutes=");
        R.append(this.newAutoArchiveDurationMinutes);
        R.append(", oldBannerHash=");
        R.append(this.oldBannerHash);
        R.append(", newBannerHash=");
        return a.D(R, this.newBannerHash, ")");
    }
}
