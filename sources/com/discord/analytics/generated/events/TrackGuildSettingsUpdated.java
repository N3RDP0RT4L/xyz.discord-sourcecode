package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackGuildSettingsUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b)\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R$\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010!\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u001b\u0010%\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0010\u001a\u0004\b&\u0010\u0012R\u001b\u0010'\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0010\u001a\u0004\b(\u0010\u0012R\u001b\u0010)\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u001e\u001a\u0004\b*\u0010 R\u001c\u0010+\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010\u0005R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012R\u001b\u00100\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0010\u001a\u0004\b1\u0010\u0012R\u001b\u00102\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\"\u001a\u0004\b3\u0010$R\u001b\u00104\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0010\u001a\u0004\b5\u0010\u0012R\u001b\u00106\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\"\u001a\u0004\b6\u0010$R\u001b\u00107\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\"\u001a\u0004\b7\u0010$R\u001b\u00108\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0010\u001a\u0004\b9\u0010\u0012R\u001b\u0010:\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0010\u001a\u0004\b;\u0010\u0012R\u001b\u0010<\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\"\u001a\u0004\b<\u0010$R\u001b\u0010=\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0010\u001a\u0004\b>\u0010\u0012R\u001b\u0010?\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u001e\u001a\u0004\b@\u0010 R\u001b\u0010A\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0010\u001a\u0004\bB\u0010\u0012R\u001b\u0010C\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\"\u001a\u0004\bD\u0010$¨\u0006E"}, d2 = {"Lcom/discord/analytics/generated/events/TrackGuildSettingsUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "bannerEditType", "Ljava/lang/CharSequence;", "getBannerEditType", "()Ljava/lang/CharSequence;", "vanityUrlCode", "getVanityUrlCode", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "animatedBanner", "Ljava/lang/Boolean;", "getAnimatedBanner", "()Ljava/lang/Boolean;", "iconEditType", "getIconEditType", "splashEditType", "getSplashEditType", "publicUpdatesChannelId", "getPublicUpdatesChannelId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "oldGuildName", "getOldGuildName", "vanityUrlCodeEditType", "getVanityUrlCodeEditType", "animatedIcon", "getAnimatedIcon", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "getDescription", "isDiscoverable", "isFeaturable", "guildName", "getGuildName", "preferredLocale", "getPreferredLocale", "isPublic", "discoverySplashEditType", "getDiscoverySplashEditType", "rulesChannelId", "getRulesChannelId", "oldVanityUrlCode", "getOldVanityUrlCode", "premiumProgressBarEnabled", "getPremiumProgressBarEnabled", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackGuildSettingsUpdated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long guildId = null;
    private final CharSequence iconEditType = null;
    private final CharSequence guildName = null;
    private final CharSequence oldGuildName = null;
    private final Boolean animatedIcon = null;
    private final CharSequence bannerEditType = null;
    private final CharSequence splashEditType = null;
    private final CharSequence vanityUrlCodeEditType = null;
    private final CharSequence vanityUrlCode = null;
    private final CharSequence oldVanityUrlCode = null;
    private final CharSequence discoverySplashEditType = null;
    private final CharSequence description = null;
    private final CharSequence preferredLocale = null;
    private final Boolean isPublic = null;
    private final Boolean isDiscoverable = null;
    private final Boolean isFeaturable = null;
    private final Long rulesChannelId = null;
    private final Long publicUpdatesChannelId = null;
    private final Boolean premiumProgressBarEnabled = null;
    private final Boolean animatedBanner = null;
    private final transient String analyticsSchemaTypeName = "guild_settings_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackGuildSettingsUpdated)) {
            return false;
        }
        TrackGuildSettingsUpdated trackGuildSettingsUpdated = (TrackGuildSettingsUpdated) obj;
        return m.areEqual(this.guildId, trackGuildSettingsUpdated.guildId) && m.areEqual(this.iconEditType, trackGuildSettingsUpdated.iconEditType) && m.areEqual(this.guildName, trackGuildSettingsUpdated.guildName) && m.areEqual(this.oldGuildName, trackGuildSettingsUpdated.oldGuildName) && m.areEqual(this.animatedIcon, trackGuildSettingsUpdated.animatedIcon) && m.areEqual(this.bannerEditType, trackGuildSettingsUpdated.bannerEditType) && m.areEqual(this.splashEditType, trackGuildSettingsUpdated.splashEditType) && m.areEqual(this.vanityUrlCodeEditType, trackGuildSettingsUpdated.vanityUrlCodeEditType) && m.areEqual(this.vanityUrlCode, trackGuildSettingsUpdated.vanityUrlCode) && m.areEqual(this.oldVanityUrlCode, trackGuildSettingsUpdated.oldVanityUrlCode) && m.areEqual(this.discoverySplashEditType, trackGuildSettingsUpdated.discoverySplashEditType) && m.areEqual(this.description, trackGuildSettingsUpdated.description) && m.areEqual(this.preferredLocale, trackGuildSettingsUpdated.preferredLocale) && m.areEqual(this.isPublic, trackGuildSettingsUpdated.isPublic) && m.areEqual(this.isDiscoverable, trackGuildSettingsUpdated.isDiscoverable) && m.areEqual(this.isFeaturable, trackGuildSettingsUpdated.isFeaturable) && m.areEqual(this.rulesChannelId, trackGuildSettingsUpdated.rulesChannelId) && m.areEqual(this.publicUpdatesChannelId, trackGuildSettingsUpdated.publicUpdatesChannelId) && m.areEqual(this.premiumProgressBarEnabled, trackGuildSettingsUpdated.premiumProgressBarEnabled) && m.areEqual(this.animatedBanner, trackGuildSettingsUpdated.animatedBanner);
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.iconEditType;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.guildName;
        int hashCode3 = (hashCode2 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.oldGuildName;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Boolean bool = this.animatedIcon;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.bannerEditType;
        int hashCode6 = (hashCode5 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.splashEditType;
        int hashCode7 = (hashCode6 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.vanityUrlCodeEditType;
        int hashCode8 = (hashCode7 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.vanityUrlCode;
        int hashCode9 = (hashCode8 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.oldVanityUrlCode;
        int hashCode10 = (hashCode9 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.discoverySplashEditType;
        int hashCode11 = (hashCode10 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.description;
        int hashCode12 = (hashCode11 + (charSequence10 != null ? charSequence10.hashCode() : 0)) * 31;
        CharSequence charSequence11 = this.preferredLocale;
        int hashCode13 = (hashCode12 + (charSequence11 != null ? charSequence11.hashCode() : 0)) * 31;
        Boolean bool2 = this.isPublic;
        int hashCode14 = (hashCode13 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.isDiscoverable;
        int hashCode15 = (hashCode14 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.isFeaturable;
        int hashCode16 = (hashCode15 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l2 = this.rulesChannelId;
        int hashCode17 = (hashCode16 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.publicUpdatesChannelId;
        int hashCode18 = (hashCode17 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool5 = this.premiumProgressBarEnabled;
        int hashCode19 = (hashCode18 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.animatedBanner;
        if (bool6 != null) {
            i = bool6.hashCode();
        }
        return hashCode19 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackGuildSettingsUpdated(guildId=");
        R.append(this.guildId);
        R.append(", iconEditType=");
        R.append(this.iconEditType);
        R.append(", guildName=");
        R.append(this.guildName);
        R.append(", oldGuildName=");
        R.append(this.oldGuildName);
        R.append(", animatedIcon=");
        R.append(this.animatedIcon);
        R.append(", bannerEditType=");
        R.append(this.bannerEditType);
        R.append(", splashEditType=");
        R.append(this.splashEditType);
        R.append(", vanityUrlCodeEditType=");
        R.append(this.vanityUrlCodeEditType);
        R.append(", vanityUrlCode=");
        R.append(this.vanityUrlCode);
        R.append(", oldVanityUrlCode=");
        R.append(this.oldVanityUrlCode);
        R.append(", discoverySplashEditType=");
        R.append(this.discoverySplashEditType);
        R.append(", description=");
        R.append(this.description);
        R.append(", preferredLocale=");
        R.append(this.preferredLocale);
        R.append(", isPublic=");
        R.append(this.isPublic);
        R.append(", isDiscoverable=");
        R.append(this.isDiscoverable);
        R.append(", isFeaturable=");
        R.append(this.isFeaturable);
        R.append(", rulesChannelId=");
        R.append(this.rulesChannelId);
        R.append(", publicUpdatesChannelId=");
        R.append(this.publicUpdatesChannelId);
        R.append(", premiumProgressBarEnabled=");
        R.append(this.premiumProgressBarEnabled);
        R.append(", animatedBanner=");
        return a.C(R, this.animatedBanner, ")");
    }
}
