package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackQuickswitcherResultSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0011\n\u0002\u0010\u0007\n\u0002\b \n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u000e\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0005R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0013\u001a\u0004\b\u001c\u0010\u0015R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0013\u001a\u0004\b\u001e\u0010\u0015R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0013\u001a\u0004\b \u0010\u0015R\u001b\u0010!\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b!\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0018\u001a\u0004\b%\u0010\u001aR\u001b\u0010&\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0018\u001a\u0004\b'\u0010\u001aR\u001b\u0010)\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0013\u001a\u0004\b.\u0010\u0015R\u001b\u0010/\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0013\u001a\u0004\b0\u0010\u0015R\u001b\u00101\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0013\u001a\u0004\b2\u0010\u0015R\u001b\u00103\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\"\u001a\u0004\b3\u0010#R\u001b\u00104\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0013\u001a\u0004\b5\u0010\u0015R\u001b\u00106\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0013\u001a\u0004\b7\u0010\u0015R\u001b\u00108\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\"\u001a\u0004\b8\u0010#R\u001b\u00109\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0013\u001a\u0004\b:\u0010\u0015R\u001b\u0010;\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0013\u001a\u0004\b<\u0010\u0015R\u001b\u0010=\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010*\u001a\u0004\b>\u0010,R\u001b\u0010?\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0018\u001a\u0004\b@\u0010\u001aR\u001b\u0010A\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0013\u001a\u0004\bB\u0010\u0015R\u001b\u0010C\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0013\u001a\u0004\bD\u0010\u0015R\u001b\u0010E\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0013\u001a\u0004\bF\u0010\u0015R\u001b\u0010G\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0013\u001a\u0004\bH\u0010\u0015R$\u0010J\u001a\u0004\u0018\u00010I8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bJ\u0010K\u001a\u0004\bL\u0010M\"\u0004\bN\u0010O¨\u0006P"}, d2 = {"Lcom/discord/analytics/generated/events/TrackQuickswitcherResultSelected;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "currentChannelType", "Ljava/lang/Long;", "getCurrentChannelType", "()Ljava/lang/Long;", "", "queryMode", "Ljava/lang/CharSequence;", "getQueryMode", "()Ljava/lang/CharSequence;", "maxQueryLength", "getMaxQueryLength", "numResultsTotal", "getNumResultsTotal", "numResultsTextChannels", "getNumResultsTextChannels", "isUsernameLike", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "query", "getQuery", "selectedType", "getSelectedType", "", "selectedScore", "Ljava/lang/Float;", "getSelectedScore", "()Ljava/lang/Float;", "selectedGuildId", "getSelectedGuildId", "currentChannelId", "getCurrentChannelId", "selectedUserId", "getSelectedUserId", "isPhoneLike", "queryLength", "getQueryLength", "numResultsVoiceChannels", "getNumResultsVoiceChannels", "isEmailLike", "currentGuildId", "getCurrentGuildId", "selectedIndex", "getSelectedIndex", "topResultScore", "getTopResultScore", "topResultType", "getTopResultType", "selectedChannelId", "getSelectedChannelId", "numResultsGuilds", "getNumResultsGuilds", "numResultsUsers", "getNumResultsUsers", "numResultsGroupDms", "getNumResultsGroupDms", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackQuickswitcherResultSelected implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence selectedType = null;
    private final Float selectedScore = null;
    private final Long selectedIndex = null;
    private final Long selectedGuildId = null;
    private final Long selectedChannelId = null;
    private final Long selectedUserId = null;
    private final Long currentChannelId = null;
    private final Long currentChannelType = null;
    private final Long currentGuildId = null;
    private final CharSequence queryMode = null;
    private final Long queryLength = null;
    private final Long maxQueryLength = null;
    private final CharSequence topResultType = null;
    private final Float topResultScore = null;
    private final Long numResultsTotal = null;
    private final Long numResultsUsers = null;
    private final Long numResultsTextChannels = null;
    private final Long numResultsVoiceChannels = null;
    private final Long numResultsGuilds = null;
    private final Long numResultsGroupDms = null;
    private final Boolean isEmailLike = null;
    private final Boolean isPhoneLike = null;
    private final Boolean isUsernameLike = null;
    private final CharSequence query = null;
    private final transient String analyticsSchemaTypeName = "quickswitcher_result_selected";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackQuickswitcherResultSelected)) {
            return false;
        }
        TrackQuickswitcherResultSelected trackQuickswitcherResultSelected = (TrackQuickswitcherResultSelected) obj;
        return m.areEqual(this.selectedType, trackQuickswitcherResultSelected.selectedType) && m.areEqual(this.selectedScore, trackQuickswitcherResultSelected.selectedScore) && m.areEqual(this.selectedIndex, trackQuickswitcherResultSelected.selectedIndex) && m.areEqual(this.selectedGuildId, trackQuickswitcherResultSelected.selectedGuildId) && m.areEqual(this.selectedChannelId, trackQuickswitcherResultSelected.selectedChannelId) && m.areEqual(this.selectedUserId, trackQuickswitcherResultSelected.selectedUserId) && m.areEqual(this.currentChannelId, trackQuickswitcherResultSelected.currentChannelId) && m.areEqual(this.currentChannelType, trackQuickswitcherResultSelected.currentChannelType) && m.areEqual(this.currentGuildId, trackQuickswitcherResultSelected.currentGuildId) && m.areEqual(this.queryMode, trackQuickswitcherResultSelected.queryMode) && m.areEqual(this.queryLength, trackQuickswitcherResultSelected.queryLength) && m.areEqual(this.maxQueryLength, trackQuickswitcherResultSelected.maxQueryLength) && m.areEqual(this.topResultType, trackQuickswitcherResultSelected.topResultType) && m.areEqual(this.topResultScore, trackQuickswitcherResultSelected.topResultScore) && m.areEqual(this.numResultsTotal, trackQuickswitcherResultSelected.numResultsTotal) && m.areEqual(this.numResultsUsers, trackQuickswitcherResultSelected.numResultsUsers) && m.areEqual(this.numResultsTextChannels, trackQuickswitcherResultSelected.numResultsTextChannels) && m.areEqual(this.numResultsVoiceChannels, trackQuickswitcherResultSelected.numResultsVoiceChannels) && m.areEqual(this.numResultsGuilds, trackQuickswitcherResultSelected.numResultsGuilds) && m.areEqual(this.numResultsGroupDms, trackQuickswitcherResultSelected.numResultsGroupDms) && m.areEqual(this.isEmailLike, trackQuickswitcherResultSelected.isEmailLike) && m.areEqual(this.isPhoneLike, trackQuickswitcherResultSelected.isPhoneLike) && m.areEqual(this.isUsernameLike, trackQuickswitcherResultSelected.isUsernameLike) && m.areEqual(this.query, trackQuickswitcherResultSelected.query);
    }

    public int hashCode() {
        CharSequence charSequence = this.selectedType;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Float f = this.selectedScore;
        int hashCode2 = (hashCode + (f != null ? f.hashCode() : 0)) * 31;
        Long l = this.selectedIndex;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.selectedGuildId;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.selectedChannelId;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.selectedUserId;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.currentChannelId;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.currentChannelType;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.currentGuildId;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.queryMode;
        int hashCode10 = (hashCode9 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l8 = this.queryLength;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.maxQueryLength;
        int hashCode12 = (hashCode11 + (l9 != null ? l9.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.topResultType;
        int hashCode13 = (hashCode12 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Float f2 = this.topResultScore;
        int hashCode14 = (hashCode13 + (f2 != null ? f2.hashCode() : 0)) * 31;
        Long l10 = this.numResultsTotal;
        int hashCode15 = (hashCode14 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.numResultsUsers;
        int hashCode16 = (hashCode15 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.numResultsTextChannels;
        int hashCode17 = (hashCode16 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.numResultsVoiceChannels;
        int hashCode18 = (hashCode17 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.numResultsGuilds;
        int hashCode19 = (hashCode18 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.numResultsGroupDms;
        int hashCode20 = (hashCode19 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Boolean bool = this.isEmailLike;
        int hashCode21 = (hashCode20 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.isPhoneLike;
        int hashCode22 = (hashCode21 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.isUsernameLike;
        int hashCode23 = (hashCode22 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.query;
        if (charSequence4 != null) {
            i = charSequence4.hashCode();
        }
        return hashCode23 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackQuickswitcherResultSelected(selectedType=");
        R.append(this.selectedType);
        R.append(", selectedScore=");
        R.append(this.selectedScore);
        R.append(", selectedIndex=");
        R.append(this.selectedIndex);
        R.append(", selectedGuildId=");
        R.append(this.selectedGuildId);
        R.append(", selectedChannelId=");
        R.append(this.selectedChannelId);
        R.append(", selectedUserId=");
        R.append(this.selectedUserId);
        R.append(", currentChannelId=");
        R.append(this.currentChannelId);
        R.append(", currentChannelType=");
        R.append(this.currentChannelType);
        R.append(", currentGuildId=");
        R.append(this.currentGuildId);
        R.append(", queryMode=");
        R.append(this.queryMode);
        R.append(", queryLength=");
        R.append(this.queryLength);
        R.append(", maxQueryLength=");
        R.append(this.maxQueryLength);
        R.append(", topResultType=");
        R.append(this.topResultType);
        R.append(", topResultScore=");
        R.append(this.topResultScore);
        R.append(", numResultsTotal=");
        R.append(this.numResultsTotal);
        R.append(", numResultsUsers=");
        R.append(this.numResultsUsers);
        R.append(", numResultsTextChannels=");
        R.append(this.numResultsTextChannels);
        R.append(", numResultsVoiceChannels=");
        R.append(this.numResultsVoiceChannels);
        R.append(", numResultsGuilds=");
        R.append(this.numResultsGuilds);
        R.append(", numResultsGroupDms=");
        R.append(this.numResultsGroupDms);
        R.append(", isEmailLike=");
        R.append(this.isEmailLike);
        R.append(", isPhoneLike=");
        R.append(this.isPhoneLike);
        R.append(", isUsernameLike=");
        R.append(this.isUsernameLike);
        R.append(", query=");
        return a.D(R, this.query, ")");
    }
}
