package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.generated.traits.TrackGuildReceiver;
import com.discord.analytics.generated.traits.TrackGuildScheduledEvent;
import com.discord.analytics.generated.traits.TrackGuildScheduledEventReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackGuildScheduledEventUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\r\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0013\u001a\u0004\b\u0019\u0010\u0015R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0013\u001a\u0004\b \u0010\u0015R\u001c\u0010!\u001a\u00020\u00068\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\bR\u001b\u0010$\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0013\u001a\u0004\b%\u0010\u0015R\u001b\u0010&\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0013\u001a\u0004\b'\u0010\u0015R\u001b\u0010(\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0013\u001a\u0004\b)\u0010\u0015R!\u0010+\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0013\u001a\u0004\b,\u0010\u0015R\u001b\u0010-\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u001c\u001a\u0004\b.\u0010\u001eR\u001b\u0010/\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0013\u001a\u0004\b0\u0010\u0015R$\u00102\u001a\u0004\u0018\u0001018\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b2\u00103\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001b\u00108\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u001c\u001a\u0004\b9\u0010\u001eR!\u0010;\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010:8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>R!\u0010?\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`*8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0013\u001a\u0004\b@\u0010\u0015R$\u0010B\u001a\u0004\u0018\u00010A8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bB\u0010C\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR\u001b\u0010H\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u0013\u001a\u0004\bI\u0010\u0015R\u001b\u0010J\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010\u0013\u001a\u0004\bK\u0010\u0015R\u001b\u0010L\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010\u0013\u001a\u0004\bM\u0010\u0015R$\u0010O\u001a\u0004\u0018\u00010N8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bO\u0010P\u001a\u0004\bQ\u0010R\"\u0004\bS\u0010TR\u001b\u0010U\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u0013\u001a\u0004\bV\u0010\u0015R\u001b\u0010W\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010\u0013\u001a\u0004\bX\u0010\u0015R$\u0010Z\u001a\u0004\u0018\u00010Y8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bZ\u0010[\u001a\u0004\b\\\u0010]\"\u0004\b^\u0010_¨\u0006`"}, d2 = {"Lcom/discord/analytics/generated/events/TrackGuildScheduledEventUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildScheduledEventReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "numParticipants", "Ljava/lang/Long;", "getNumParticipants", "()Ljava/lang/Long;", "totalUniqueListeners", "getTotalUniqueListeners", "guildScheduledEventEntityType", "getGuildScheduledEventEntityType", "", "guildScheduledEventExternalLocation", "Ljava/lang/CharSequence;", "getGuildScheduledEventExternalLocation", "()Ljava/lang/CharSequence;", "guildScheduledEventInterestedCount", "getGuildScheduledEventInterestedCount", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "updaterId", "getUpdaterId", "maxConcurrentListeners", "getMaxConcurrentListeners", "maxConcurrentSpeakers", "getMaxConcurrentSpeakers", "Lcom/discord/primitives/Timestamp;", "startTime", "getStartTime", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "getDescription", "maxConcurrentParticipants", "getMaxConcurrentParticipants", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", ModelAuditLogEntry.CHANGE_KEY_NAME, "getName", "", "skuIds", "Ljava/util/List;", "getSkuIds", "()Ljava/util/List;", "endTime", "getEndTime", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "guildScheduledEventId", "getGuildScheduledEventId", "guildScheduledEventStatus", "getGuildScheduledEventStatus", "totalUniqueSpeakers", "getTotalUniqueSpeakers", "Lcom/discord/analytics/generated/traits/TrackGuild;", "trackGuild", "Lcom/discord/analytics/generated/traits/TrackGuild;", "getTrackGuild", "()Lcom/discord/analytics/generated/traits/TrackGuild;", "setTrackGuild", "(Lcom/discord/analytics/generated/traits/TrackGuild;)V", "totalUniqueParticipants", "getTotalUniqueParticipants", "privacyLevel", "getPrivacyLevel", "Lcom/discord/analytics/generated/traits/TrackGuildScheduledEvent;", "trackGuildScheduledEvent", "Lcom/discord/analytics/generated/traits/TrackGuildScheduledEvent;", "getTrackGuildScheduledEvent", "()Lcom/discord/analytics/generated/traits/TrackGuildScheduledEvent;", "setTrackGuildScheduledEvent", "(Lcom/discord/analytics/generated/traits/TrackGuildScheduledEvent;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackGuildScheduledEventUpdated implements AnalyticsSchema, TrackBaseReceiver, TrackGuildReceiver, TrackChannelReceiver, TrackGuildScheduledEventReceiver {
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackGuild trackGuild;
    private TrackGuildScheduledEvent trackGuildScheduledEvent;
    private final Long guildScheduledEventId = null;
    private final CharSequence name = null;
    private final Long startTime = null;
    private final Long endTime = null;
    private final List<Long> skuIds = null;
    private final Long guildScheduledEventStatus = null;
    private final Long privacyLevel = null;
    private final Long guildScheduledEventEntityType = null;
    private final CharSequence guildScheduledEventExternalLocation = null;
    private final Long guildScheduledEventInterestedCount = null;
    private final CharSequence description = null;
    private final Long updaterId = null;
    private final Long numParticipants = null;
    private final Long totalUniqueListeners = null;
    private final Long maxConcurrentListeners = null;
    private final Long totalUniqueSpeakers = null;
    private final Long maxConcurrentSpeakers = null;
    private final Long totalUniqueParticipants = null;
    private final Long maxConcurrentParticipants = null;
    private final transient String analyticsSchemaTypeName = "guild_scheduled_event_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackGuildScheduledEventUpdated)) {
            return false;
        }
        TrackGuildScheduledEventUpdated trackGuildScheduledEventUpdated = (TrackGuildScheduledEventUpdated) obj;
        return m.areEqual(this.guildScheduledEventId, trackGuildScheduledEventUpdated.guildScheduledEventId) && m.areEqual(this.name, trackGuildScheduledEventUpdated.name) && m.areEqual(this.startTime, trackGuildScheduledEventUpdated.startTime) && m.areEqual(this.endTime, trackGuildScheduledEventUpdated.endTime) && m.areEqual(this.skuIds, trackGuildScheduledEventUpdated.skuIds) && m.areEqual(this.guildScheduledEventStatus, trackGuildScheduledEventUpdated.guildScheduledEventStatus) && m.areEqual(this.privacyLevel, trackGuildScheduledEventUpdated.privacyLevel) && m.areEqual(this.guildScheduledEventEntityType, trackGuildScheduledEventUpdated.guildScheduledEventEntityType) && m.areEqual(this.guildScheduledEventExternalLocation, trackGuildScheduledEventUpdated.guildScheduledEventExternalLocation) && m.areEqual(this.guildScheduledEventInterestedCount, trackGuildScheduledEventUpdated.guildScheduledEventInterestedCount) && m.areEqual(this.description, trackGuildScheduledEventUpdated.description) && m.areEqual(this.updaterId, trackGuildScheduledEventUpdated.updaterId) && m.areEqual(this.numParticipants, trackGuildScheduledEventUpdated.numParticipants) && m.areEqual(this.totalUniqueListeners, trackGuildScheduledEventUpdated.totalUniqueListeners) && m.areEqual(this.maxConcurrentListeners, trackGuildScheduledEventUpdated.maxConcurrentListeners) && m.areEqual(this.totalUniqueSpeakers, trackGuildScheduledEventUpdated.totalUniqueSpeakers) && m.areEqual(this.maxConcurrentSpeakers, trackGuildScheduledEventUpdated.maxConcurrentSpeakers) && m.areEqual(this.totalUniqueParticipants, trackGuildScheduledEventUpdated.totalUniqueParticipants) && m.areEqual(this.maxConcurrentParticipants, trackGuildScheduledEventUpdated.maxConcurrentParticipants);
    }

    public int hashCode() {
        Long l = this.guildScheduledEventId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.name;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l2 = this.startTime;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.endTime;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        List<Long> list = this.skuIds;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        Long l4 = this.guildScheduledEventStatus;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.privacyLevel;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.guildScheduledEventEntityType;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.guildScheduledEventExternalLocation;
        int hashCode9 = (hashCode8 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l7 = this.guildScheduledEventInterestedCount;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.description;
        int hashCode11 = (hashCode10 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l8 = this.updaterId;
        int hashCode12 = (hashCode11 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.numParticipants;
        int hashCode13 = (hashCode12 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.totalUniqueListeners;
        int hashCode14 = (hashCode13 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.maxConcurrentListeners;
        int hashCode15 = (hashCode14 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.totalUniqueSpeakers;
        int hashCode16 = (hashCode15 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.maxConcurrentSpeakers;
        int hashCode17 = (hashCode16 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.totalUniqueParticipants;
        int hashCode18 = (hashCode17 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.maxConcurrentParticipants;
        if (l15 != null) {
            i = l15.hashCode();
        }
        return hashCode18 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackGuildScheduledEventUpdated(guildScheduledEventId=");
        R.append(this.guildScheduledEventId);
        R.append(", name=");
        R.append(this.name);
        R.append(", startTime=");
        R.append(this.startTime);
        R.append(", endTime=");
        R.append(this.endTime);
        R.append(", skuIds=");
        R.append(this.skuIds);
        R.append(", guildScheduledEventStatus=");
        R.append(this.guildScheduledEventStatus);
        R.append(", privacyLevel=");
        R.append(this.privacyLevel);
        R.append(", guildScheduledEventEntityType=");
        R.append(this.guildScheduledEventEntityType);
        R.append(", guildScheduledEventExternalLocation=");
        R.append(this.guildScheduledEventExternalLocation);
        R.append(", guildScheduledEventInterestedCount=");
        R.append(this.guildScheduledEventInterestedCount);
        R.append(", description=");
        R.append(this.description);
        R.append(", updaterId=");
        R.append(this.updaterId);
        R.append(", numParticipants=");
        R.append(this.numParticipants);
        R.append(", totalUniqueListeners=");
        R.append(this.totalUniqueListeners);
        R.append(", maxConcurrentListeners=");
        R.append(this.maxConcurrentListeners);
        R.append(", totalUniqueSpeakers=");
        R.append(this.totalUniqueSpeakers);
        R.append(", maxConcurrentSpeakers=");
        R.append(this.maxConcurrentSpeakers);
        R.append(", totalUniqueParticipants=");
        R.append(this.totalUniqueParticipants);
        R.append(", maxConcurrentParticipants=");
        return a.F(R, this.maxConcurrentParticipants, ")");
    }
}
