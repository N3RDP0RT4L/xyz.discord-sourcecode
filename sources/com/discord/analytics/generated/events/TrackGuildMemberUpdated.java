package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackGuildMemberUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u001e\n\u0002\u0010\u0007\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR!\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R!\u0010\u0016\u001a\n\u0018\u00010\u0014j\u0004\u0018\u0001`\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0017\u001a\u0004\b\u001b\u0010\u0019R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0017\u001a\u0004\b!\u0010\u0019R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u001b\u0010&\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u001d\u001a\u0004\b'\u0010\u001fR\u001b\u0010(\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010#\u001a\u0004\b)\u0010%R\u001b\u0010*\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u001d\u001a\u0004\b+\u0010\u001fR\u001b\u0010,\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u001d\u001a\u0004\b-\u0010\u001fR!\u0010.\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0011\u001a\u0004\b/\u0010\u0013R\u001b\u00100\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u001d\u001a\u0004\b1\u0010\u001fR\u001b\u00102\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u001d\u001a\u0004\b3\u0010\u001fR\u001b\u00105\u001a\u0004\u0018\u0001048\u0006@\u0006¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u00108R!\u00109\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0011\u001a\u0004\b:\u0010\u0013R\u001b\u0010;\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0017\u001a\u0004\b<\u0010\u0019R\u001b\u0010=\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u001d\u001a\u0004\b>\u0010\u001fR\u001b\u0010?\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0017\u001a\u0004\b@\u0010\u0019R$\u0010B\u001a\u0004\u0018\u00010A8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bB\u0010C\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR\u001b\u0010H\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u001d\u001a\u0004\bI\u0010\u001fR\u001c\u0010J\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bJ\u0010K\u001a\u0004\bL\u0010\u0005R\u001b\u0010M\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010\u001d\u001a\u0004\bN\u0010\u001f¨\u0006O"}, d2 = {"Lcom/discord/analytics/generated/events/TrackGuildMemberUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "", "fieldsUpdated", "Ljava/util/List;", "getFieldsUpdated", "()Ljava/util/List;", "", "Lcom/discord/primitives/Timestamp;", "communicationDisabledUntil", "Ljava/lang/Long;", "getCommunicationDisabledUntil", "()Ljava/lang/Long;", "targetUserId", "getTargetUserId", "newNickname", "Ljava/lang/CharSequence;", "getNewNickname", "()Ljava/lang/CharSequence;", "userPremiumTier", "getUserPremiumTier", ModelAuditLogEntry.CHANGE_KEY_MUTE, "Ljava/lang/Boolean;", "getMute", "()Ljava/lang/Boolean;", "bannerUpdateType", "getBannerUpdateType", ModelAuditLogEntry.CHANGE_KEY_DEAF, "getDeaf", "guildName", "getGuildName", "updateType", "getUpdateType", "newRoles", "getNewRoles", "oldNickname", "getOldNickname", ModelAuditLogEntry.CHANGE_KEY_REASON, "getReason", "", "duration", "Ljava/lang/Float;", "getDuration", "()Ljava/lang/Float;", "oldRoles", "getOldRoles", "guildId", "getGuildId", "nicknameUpdateType", "getNicknameUpdateType", "deleteMessageDays", "getDeleteMessageDays", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "bioUpdateType", "getBioUpdateType", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "avatarUpdateType", "getAvatarUpdateType", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackGuildMemberUpdated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long guildId = null;
    private final CharSequence guildName = null;
    private final Long targetUserId = null;
    private final CharSequence updateType = null;
    private final List<CharSequence> fieldsUpdated = null;
    private final CharSequence oldNickname = null;
    private final CharSequence newNickname = null;
    private final Boolean mute = null;
    private final Boolean deaf = null;
    private final List<Long> oldRoles = null;
    private final List<Long> newRoles = null;
    private final CharSequence reason = null;
    private final Long deleteMessageDays = null;
    private final CharSequence avatarUpdateType = null;
    private final Long userPremiumTier = null;
    private final CharSequence nicknameUpdateType = null;
    private final CharSequence bannerUpdateType = null;
    private final CharSequence bioUpdateType = null;
    private final Float duration = null;
    private final Long communicationDisabledUntil = null;
    private final transient String analyticsSchemaTypeName = "guild_member_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackGuildMemberUpdated)) {
            return false;
        }
        TrackGuildMemberUpdated trackGuildMemberUpdated = (TrackGuildMemberUpdated) obj;
        return m.areEqual(this.guildId, trackGuildMemberUpdated.guildId) && m.areEqual(this.guildName, trackGuildMemberUpdated.guildName) && m.areEqual(this.targetUserId, trackGuildMemberUpdated.targetUserId) && m.areEqual(this.updateType, trackGuildMemberUpdated.updateType) && m.areEqual(this.fieldsUpdated, trackGuildMemberUpdated.fieldsUpdated) && m.areEqual(this.oldNickname, trackGuildMemberUpdated.oldNickname) && m.areEqual(this.newNickname, trackGuildMemberUpdated.newNickname) && m.areEqual(this.mute, trackGuildMemberUpdated.mute) && m.areEqual(this.deaf, trackGuildMemberUpdated.deaf) && m.areEqual(this.oldRoles, trackGuildMemberUpdated.oldRoles) && m.areEqual(this.newRoles, trackGuildMemberUpdated.newRoles) && m.areEqual(this.reason, trackGuildMemberUpdated.reason) && m.areEqual(this.deleteMessageDays, trackGuildMemberUpdated.deleteMessageDays) && m.areEqual(this.avatarUpdateType, trackGuildMemberUpdated.avatarUpdateType) && m.areEqual(this.userPremiumTier, trackGuildMemberUpdated.userPremiumTier) && m.areEqual(this.nicknameUpdateType, trackGuildMemberUpdated.nicknameUpdateType) && m.areEqual(this.bannerUpdateType, trackGuildMemberUpdated.bannerUpdateType) && m.areEqual(this.bioUpdateType, trackGuildMemberUpdated.bioUpdateType) && m.areEqual(this.duration, trackGuildMemberUpdated.duration) && m.areEqual(this.communicationDisabledUntil, trackGuildMemberUpdated.communicationDisabledUntil);
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.guildName;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l2 = this.targetUserId;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.updateType;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        List<CharSequence> list = this.fieldsUpdated;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.oldNickname;
        int hashCode6 = (hashCode5 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.newNickname;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Boolean bool = this.mute;
        int hashCode8 = (hashCode7 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.deaf;
        int hashCode9 = (hashCode8 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        List<Long> list2 = this.oldRoles;
        int hashCode10 = (hashCode9 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Long> list3 = this.newRoles;
        int hashCode11 = (hashCode10 + (list3 != null ? list3.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.reason;
        int hashCode12 = (hashCode11 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l3 = this.deleteMessageDays;
        int hashCode13 = (hashCode12 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.avatarUpdateType;
        int hashCode14 = (hashCode13 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Long l4 = this.userPremiumTier;
        int hashCode15 = (hashCode14 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.nicknameUpdateType;
        int hashCode16 = (hashCode15 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.bannerUpdateType;
        int hashCode17 = (hashCode16 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.bioUpdateType;
        int hashCode18 = (hashCode17 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        Float f = this.duration;
        int hashCode19 = (hashCode18 + (f != null ? f.hashCode() : 0)) * 31;
        Long l5 = this.communicationDisabledUntil;
        if (l5 != null) {
            i = l5.hashCode();
        }
        return hashCode19 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackGuildMemberUpdated(guildId=");
        R.append(this.guildId);
        R.append(", guildName=");
        R.append(this.guildName);
        R.append(", targetUserId=");
        R.append(this.targetUserId);
        R.append(", updateType=");
        R.append(this.updateType);
        R.append(", fieldsUpdated=");
        R.append(this.fieldsUpdated);
        R.append(", oldNickname=");
        R.append(this.oldNickname);
        R.append(", newNickname=");
        R.append(this.newNickname);
        R.append(", mute=");
        R.append(this.mute);
        R.append(", deaf=");
        R.append(this.deaf);
        R.append(", oldRoles=");
        R.append(this.oldRoles);
        R.append(", newRoles=");
        R.append(this.newRoles);
        R.append(", reason=");
        R.append(this.reason);
        R.append(", deleteMessageDays=");
        R.append(this.deleteMessageDays);
        R.append(", avatarUpdateType=");
        R.append(this.avatarUpdateType);
        R.append(", userPremiumTier=");
        R.append(this.userPremiumTier);
        R.append(", nicknameUpdateType=");
        R.append(this.nicknameUpdateType);
        R.append(", bannerUpdateType=");
        R.append(this.bannerUpdateType);
        R.append(", bioUpdateType=");
        R.append(this.bioUpdateType);
        R.append(", duration=");
        R.append(this.duration);
        R.append(", communicationDisabledUntil=");
        return a.F(R, this.communicationDisabledUntil, ")");
    }
}
