package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackVideoLayoutToggled.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0010\u001a\u0004\b\u0019\u0010\u0012R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0015\u001a\u0004\b\u001b\u0010\u0017R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010 \u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0015\u001a\u0004\b!\u0010\u0017R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0015\u001a\u0004\b#\u0010\u0017R$\u0010%\u001a\u0004\u0018\u00010$8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R\u001b\u0010+\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0015\u001a\u0004\b,\u0010\u0017R\u001b\u0010-\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0015\u001a\u0004\b.\u0010\u0017R\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0010\u001a\u0004\b0\u0010\u0012R\u001c\u00101\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u0010\u0005R\u001b\u00104\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0015\u001a\u0004\b5\u0010\u0017R\u001b\u00106\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109¨\u0006:"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVideoLayoutToggled;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "videoStreamState", "Ljava/lang/CharSequence;", "getVideoStreamState", "()Ljava/lang/CharSequence;", "", "streamerUserId", "Ljava/lang/Long;", "getStreamerUserId", "()Ljava/lang/Long;", "gamePlatform", "getGamePlatform", "guildId", "getGuildId", "videoLayout", "getVideoLayout", "gameName", "getGameName", "channelId", "getChannelId", "channelType", "getChannelType", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "voiceStateCount", "getVoiceStateCount", "videoStreamCount", "getVideoStreamCount", "mediaSessionId", "getMediaSessionId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "gameId", "getGameId", "videoEnabled", "Ljava/lang/Boolean;", "getVideoEnabled", "()Ljava/lang/Boolean;", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVideoLayoutToggled implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long guildId = null;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long voiceStateCount = null;
    private final Long videoStreamCount = null;
    private final Boolean videoEnabled = null;
    private final CharSequence gameName = null;
    private final Long gameId = null;
    private final CharSequence videoLayout = null;
    private final CharSequence gamePlatform = null;
    private final Long streamerUserId = null;
    private final CharSequence videoStreamState = null;
    private final CharSequence mediaSessionId = null;
    private final transient String analyticsSchemaTypeName = "video_layout_toggled";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVideoLayoutToggled)) {
            return false;
        }
        TrackVideoLayoutToggled trackVideoLayoutToggled = (TrackVideoLayoutToggled) obj;
        return m.areEqual(this.guildId, trackVideoLayoutToggled.guildId) && m.areEqual(this.channelId, trackVideoLayoutToggled.channelId) && m.areEqual(this.channelType, trackVideoLayoutToggled.channelType) && m.areEqual(this.voiceStateCount, trackVideoLayoutToggled.voiceStateCount) && m.areEqual(this.videoStreamCount, trackVideoLayoutToggled.videoStreamCount) && m.areEqual(this.videoEnabled, trackVideoLayoutToggled.videoEnabled) && m.areEqual(this.gameName, trackVideoLayoutToggled.gameName) && m.areEqual(this.gameId, trackVideoLayoutToggled.gameId) && m.areEqual(this.videoLayout, trackVideoLayoutToggled.videoLayout) && m.areEqual(this.gamePlatform, trackVideoLayoutToggled.gamePlatform) && m.areEqual(this.streamerUserId, trackVideoLayoutToggled.streamerUserId) && m.areEqual(this.videoStreamState, trackVideoLayoutToggled.videoStreamState) && m.areEqual(this.mediaSessionId, trackVideoLayoutToggled.mediaSessionId);
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channelType;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.voiceStateCount;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.videoStreamCount;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Boolean bool = this.videoEnabled;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence = this.gameName;
        int hashCode7 = (hashCode6 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l6 = this.gameId;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.videoLayout;
        int hashCode9 = (hashCode8 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.gamePlatform;
        int hashCode10 = (hashCode9 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l7 = this.streamerUserId;
        int hashCode11 = (hashCode10 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.videoStreamState;
        int hashCode12 = (hashCode11 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.mediaSessionId;
        if (charSequence5 != null) {
            i = charSequence5.hashCode();
        }
        return hashCode12 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVideoLayoutToggled(guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", voiceStateCount=");
        R.append(this.voiceStateCount);
        R.append(", videoStreamCount=");
        R.append(this.videoStreamCount);
        R.append(", videoEnabled=");
        R.append(this.videoEnabled);
        R.append(", gameName=");
        R.append(this.gameName);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", videoLayout=");
        R.append(this.videoLayout);
        R.append(", gamePlatform=");
        R.append(this.gamePlatform);
        R.append(", streamerUserId=");
        R.append(this.streamerUserId);
        R.append(", videoStreamState=");
        R.append(this.videoStreamState);
        R.append(", mediaSessionId=");
        return a.D(R, this.mediaSessionId, ")");
    }
}
