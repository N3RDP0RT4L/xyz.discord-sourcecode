package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackCaptchaSolved.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0010\r\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0007\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R!\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R!\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0016\u001a\u0004\b\u001a\u0010\u0018R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u001c\u001a\u0004\b \u0010\u001eR\u001c\u0010!\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\u0005R\u001b\u0010$\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001b\u0010(\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u001c\u001a\u0004\b)\u0010\u001eR$\u0010+\u001a\u0004\u0018\u00010*8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u001b\u00101\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0010\u001a\u0004\b2\u0010\u0012R\u001b\u00104\u001a\u0004\u0018\u0001038\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u001b\u00108\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u001c\u001a\u0004\b9\u0010\u001eR\u001b\u0010:\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u001c\u001a\u0004\b;\u0010\u001eR\u001b\u0010<\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010%\u001a\u0004\b=\u0010'R\u001b\u0010>\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0010\u001a\u0004\b?\u0010\u0012R\u001b\u0010@\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u001c\u001a\u0004\bA\u0010\u001eR\u001b\u0010B\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010%\u001a\u0004\bC\u0010'R!\u0010E\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`D8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0010\u001a\u0004\bF\u0010\u0012R\u001b\u0010G\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0010\u001a\u0004\bH\u0010\u0012¨\u0006I"}, d2 = {"Lcom/discord/analytics/generated/events/TrackCaptchaSolved;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "locationMessageId", "Ljava/lang/Long;", "getLocationMessageId", "()Ljava/lang/Long;", "", "", "errorCodes", "Ljava/util/List;", "getErrorCodes", "()Ljava/util/List;", "scoreReason", "getScoreReason", "captchaService", "Ljava/lang/CharSequence;", "getCaptchaService", "()Ljava/lang/CharSequence;", "hostname", "getHostname", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "success", "Ljava/lang/Boolean;", "getSuccess", "()Ljava/lang/Boolean;", "sitekey", "getSitekey", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "locationGuildId", "getLocationGuildId", "", "score", "Ljava/lang/Float;", "getScore", "()Ljava/lang/Float;", "scopedUid1", "getScopedUid1", "scopedUid0", "getScopedUid0", "captchaRequired", "getCaptchaRequired", "locationChannelType", "getLocationChannelType", "userFlow", "getUserFlow", "forceBad", "getForceBad", "Lcom/discord/primitives/Timestamp;", "challengeTs", "getChallengeTs", "locationChannelId", "getLocationChannelId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackCaptchaSolved implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Boolean success = null;
    private final Boolean forceBad = null;
    private final Long challengeTs = null;
    private final CharSequence hostname = null;
    private final CharSequence scopedUid0 = null;
    private final CharSequence scopedUid1 = null;
    private final CharSequence sitekey = null;
    private final Float score = null;
    private final List<CharSequence> scoreReason = null;
    private final List<CharSequence> errorCodes = null;
    private final CharSequence userFlow = null;
    private final CharSequence captchaService = null;
    private final Boolean captchaRequired = null;
    private final Long locationGuildId = null;
    private final Long locationChannelId = null;
    private final Long locationChannelType = null;
    private final Long locationMessageId = null;
    private final transient String analyticsSchemaTypeName = "captcha_solved";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackCaptchaSolved)) {
            return false;
        }
        TrackCaptchaSolved trackCaptchaSolved = (TrackCaptchaSolved) obj;
        return m.areEqual(this.success, trackCaptchaSolved.success) && m.areEqual(this.forceBad, trackCaptchaSolved.forceBad) && m.areEqual(this.challengeTs, trackCaptchaSolved.challengeTs) && m.areEqual(this.hostname, trackCaptchaSolved.hostname) && m.areEqual(this.scopedUid0, trackCaptchaSolved.scopedUid0) && m.areEqual(this.scopedUid1, trackCaptchaSolved.scopedUid1) && m.areEqual(this.sitekey, trackCaptchaSolved.sitekey) && m.areEqual(this.score, trackCaptchaSolved.score) && m.areEqual(this.scoreReason, trackCaptchaSolved.scoreReason) && m.areEqual(this.errorCodes, trackCaptchaSolved.errorCodes) && m.areEqual(this.userFlow, trackCaptchaSolved.userFlow) && m.areEqual(this.captchaService, trackCaptchaSolved.captchaService) && m.areEqual(this.captchaRequired, trackCaptchaSolved.captchaRequired) && m.areEqual(this.locationGuildId, trackCaptchaSolved.locationGuildId) && m.areEqual(this.locationChannelId, trackCaptchaSolved.locationChannelId) && m.areEqual(this.locationChannelType, trackCaptchaSolved.locationChannelType) && m.areEqual(this.locationMessageId, trackCaptchaSolved.locationMessageId);
    }

    public int hashCode() {
        Boolean bool = this.success;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        Boolean bool2 = this.forceBad;
        int hashCode2 = (hashCode + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l = this.challengeTs;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence = this.hostname;
        int hashCode4 = (hashCode3 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.scopedUid0;
        int hashCode5 = (hashCode4 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.scopedUid1;
        int hashCode6 = (hashCode5 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.sitekey;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Float f = this.score;
        int hashCode8 = (hashCode7 + (f != null ? f.hashCode() : 0)) * 31;
        List<CharSequence> list = this.scoreReason;
        int hashCode9 = (hashCode8 + (list != null ? list.hashCode() : 0)) * 31;
        List<CharSequence> list2 = this.errorCodes;
        int hashCode10 = (hashCode9 + (list2 != null ? list2.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.userFlow;
        int hashCode11 = (hashCode10 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.captchaService;
        int hashCode12 = (hashCode11 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Boolean bool3 = this.captchaRequired;
        int hashCode13 = (hashCode12 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l2 = this.locationGuildId;
        int hashCode14 = (hashCode13 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.locationChannelId;
        int hashCode15 = (hashCode14 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.locationChannelType;
        int hashCode16 = (hashCode15 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.locationMessageId;
        if (l5 != null) {
            i = l5.hashCode();
        }
        return hashCode16 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackCaptchaSolved(success=");
        R.append(this.success);
        R.append(", forceBad=");
        R.append(this.forceBad);
        R.append(", challengeTs=");
        R.append(this.challengeTs);
        R.append(", hostname=");
        R.append(this.hostname);
        R.append(", scopedUid0=");
        R.append(this.scopedUid0);
        R.append(", scopedUid1=");
        R.append(this.scopedUid1);
        R.append(", sitekey=");
        R.append(this.sitekey);
        R.append(", score=");
        R.append(this.score);
        R.append(", scoreReason=");
        R.append(this.scoreReason);
        R.append(", errorCodes=");
        R.append(this.errorCodes);
        R.append(", userFlow=");
        R.append(this.userFlow);
        R.append(", captchaService=");
        R.append(this.captchaService);
        R.append(", captchaRequired=");
        R.append(this.captchaRequired);
        R.append(", locationGuildId=");
        R.append(this.locationGuildId);
        R.append(", locationChannelId=");
        R.append(this.locationChannelId);
        R.append(", locationChannelType=");
        R.append(this.locationChannelType);
        R.append(", locationMessageId=");
        return a.F(R, this.locationMessageId, ")");
    }
}
