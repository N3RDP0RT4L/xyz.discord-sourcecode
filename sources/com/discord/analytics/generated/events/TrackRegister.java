package com.discord.analytics.generated.events;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackGiftCodeMetadata;
import com.discord.analytics.generated.traits.TrackGiftCodeMetadataReceiver;
import com.discord.analytics.generated.traits.TrackGuildTemplate;
import com.discord.analytics.generated.traits.TrackGuildTemplateReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackRegister.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u0007\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0012\u001a\u0004\b\u0016\u0010\u0014R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0012\u001a\u0004\b!\u0010\u0014R\u001b\u0010\"\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u001d\u001a\u0004\b#\u0010\u001fR\u001b\u0010$\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0012\u001a\u0004\b%\u0010\u0014R\u001b\u0010&\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u001d\u001a\u0004\b'\u0010\u001fR$\u0010)\u001a\u0004\u0018\u00010(8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u001b\u0010/\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0012\u001a\u0004\b0\u0010\u0014R\u001b\u00101\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0012\u001a\u0004\b2\u0010\u0014R\u001b\u00103\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0012\u001a\u0004\b4\u0010\u0014R$\u00106\u001a\u0004\u0018\u0001058\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b6\u00107\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R\u001c\u0010<\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010\u0007R\u001b\u0010?\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u001d\u001a\u0004\b@\u0010\u001fR\u001b\u0010A\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u001d\u001a\u0004\bB\u0010\u001fR\u001b\u0010D\u001a\u0004\u0018\u00010C8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010E\u001a\u0004\bF\u0010GR\u001b\u0010H\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u001d\u001a\u0004\bI\u0010\u001fR\u001b\u0010J\u001a\u0004\u0018\u00010C8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010E\u001a\u0004\bK\u0010GR\u001b\u0010L\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010\u001d\u001a\u0004\bM\u0010\u001fR\u001b\u0010N\u001a\u0004\u0018\u00010C8\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010E\u001a\u0004\bO\u0010GR$\u0010Q\u001a\u0004\u0018\u00010P8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bQ\u0010R\u001a\u0004\bS\u0010T\"\u0004\bU\u0010VR\u001b\u0010W\u001a\u0004\u0018\u00010C8\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010E\u001a\u0004\bX\u0010G¨\u0006Y"}, d2 = {"Lcom/discord/analytics/generated/events/TrackRegister;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildTemplateReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "inviteCode", "Ljava/lang/CharSequence;", "getInviteCode", "()Ljava/lang/CharSequence;", "phoneCarrierName", "getPhoneCarrierName", "", "captchaScore", "Ljava/lang/Float;", "getCaptchaScore", "()Ljava/lang/Float;", "solvedCaptcha", "Ljava/lang/Boolean;", "getSolvedCaptcha", "()Ljava/lang/Boolean;", "phone", "getPhone", "requireVerifiedPhone", "getRequireVerifiedPhone", "registrationSource", "getRegistrationSource", "full", "getFull", "Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "trackGuildTemplate", "Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "getTrackGuildTemplate", "()Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "setTrackGuildTemplate", "(Lcom/discord/analytics/generated/traits/TrackGuildTemplate;)V", "identityType", "getIdentityType", "phoneCountry", "getPhoneCountry", NotificationCompat.CATEGORY_EMAIL, "getEmail", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "trackGiftCodeMetadata", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "getTrackGiftCodeMetadata", "()Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "setTrackGiftCodeMetadata", "(Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;)V", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "requireVerifiedPhoneThenEmail", "getRequireVerifiedPhoneThenEmail", "smiteCaptchaRequested", "getSmiteCaptchaRequested", "", "inviteChannelId", "Ljava/lang/Long;", "getInviteChannelId", "()Ljava/lang/Long;", "instantInvite", "getInstantInvite", "inviteInviterId", "getInviteInviterId", "requireVerifiedEmail", "getRequireVerifiedEmail", "inviteChannelType", "getInviteChannelType", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "inviteGuildId", "getInviteGuildId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackRegister implements AnalyticsSchema, TrackBaseReceiver, TrackGiftCodeMetadataReceiver, TrackGuildTemplateReceiver {
    private TrackBase trackBase;
    private TrackGiftCodeMetadata trackGiftCodeMetadata;
    private TrackGuildTemplate trackGuildTemplate;
    private final CharSequence registrationSource = null;
    private final Boolean full = null;
    private final Boolean instantInvite = null;
    private final CharSequence inviteCode = null;
    private final Long inviteGuildId = null;
    private final Long inviteChannelId = null;
    private final Long inviteChannelType = null;
    private final Long inviteInviterId = null;
    private final Boolean smiteCaptchaRequested = null;
    private final Boolean solvedCaptcha = null;
    private final Float captchaScore = null;
    private final Boolean requireVerifiedPhone = null;
    private final Boolean requireVerifiedEmail = null;
    private final Boolean requireVerifiedPhoneThenEmail = null;
    private final CharSequence email = null;
    private final CharSequence phone = null;
    private final CharSequence identityType = null;
    private final CharSequence phoneCarrierName = null;
    private final CharSequence phoneCountry = null;
    private final transient String analyticsSchemaTypeName = "register";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackRegister)) {
            return false;
        }
        TrackRegister trackRegister = (TrackRegister) obj;
        return m.areEqual(this.registrationSource, trackRegister.registrationSource) && m.areEqual(this.full, trackRegister.full) && m.areEqual(this.instantInvite, trackRegister.instantInvite) && m.areEqual(this.inviteCode, trackRegister.inviteCode) && m.areEqual(this.inviteGuildId, trackRegister.inviteGuildId) && m.areEqual(this.inviteChannelId, trackRegister.inviteChannelId) && m.areEqual(this.inviteChannelType, trackRegister.inviteChannelType) && m.areEqual(this.inviteInviterId, trackRegister.inviteInviterId) && m.areEqual(this.smiteCaptchaRequested, trackRegister.smiteCaptchaRequested) && m.areEqual(this.solvedCaptcha, trackRegister.solvedCaptcha) && m.areEqual(this.captchaScore, trackRegister.captchaScore) && m.areEqual(this.requireVerifiedPhone, trackRegister.requireVerifiedPhone) && m.areEqual(this.requireVerifiedEmail, trackRegister.requireVerifiedEmail) && m.areEqual(this.requireVerifiedPhoneThenEmail, trackRegister.requireVerifiedPhoneThenEmail) && m.areEqual(this.email, trackRegister.email) && m.areEqual(this.phone, trackRegister.phone) && m.areEqual(this.identityType, trackRegister.identityType) && m.areEqual(this.phoneCarrierName, trackRegister.phoneCarrierName) && m.areEqual(this.phoneCountry, trackRegister.phoneCountry);
    }

    public int hashCode() {
        CharSequence charSequence = this.registrationSource;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Boolean bool = this.full;
        int hashCode2 = (hashCode + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.instantInvite;
        int hashCode3 = (hashCode2 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.inviteCode;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.inviteGuildId;
        int hashCode5 = (hashCode4 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.inviteChannelId;
        int hashCode6 = (hashCode5 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.inviteChannelType;
        int hashCode7 = (hashCode6 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.inviteInviterId;
        int hashCode8 = (hashCode7 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool3 = this.smiteCaptchaRequested;
        int hashCode9 = (hashCode8 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.solvedCaptcha;
        int hashCode10 = (hashCode9 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Float f = this.captchaScore;
        int hashCode11 = (hashCode10 + (f != null ? f.hashCode() : 0)) * 31;
        Boolean bool5 = this.requireVerifiedPhone;
        int hashCode12 = (hashCode11 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.requireVerifiedEmail;
        int hashCode13 = (hashCode12 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        Boolean bool7 = this.requireVerifiedPhoneThenEmail;
        int hashCode14 = (hashCode13 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.email;
        int hashCode15 = (hashCode14 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.phone;
        int hashCode16 = (hashCode15 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.identityType;
        int hashCode17 = (hashCode16 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.phoneCarrierName;
        int hashCode18 = (hashCode17 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.phoneCountry;
        if (charSequence7 != null) {
            i = charSequence7.hashCode();
        }
        return hashCode18 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackRegister(registrationSource=");
        R.append(this.registrationSource);
        R.append(", full=");
        R.append(this.full);
        R.append(", instantInvite=");
        R.append(this.instantInvite);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", inviteGuildId=");
        R.append(this.inviteGuildId);
        R.append(", inviteChannelId=");
        R.append(this.inviteChannelId);
        R.append(", inviteChannelType=");
        R.append(this.inviteChannelType);
        R.append(", inviteInviterId=");
        R.append(this.inviteInviterId);
        R.append(", smiteCaptchaRequested=");
        R.append(this.smiteCaptchaRequested);
        R.append(", solvedCaptcha=");
        R.append(this.solvedCaptcha);
        R.append(", captchaScore=");
        R.append(this.captchaScore);
        R.append(", requireVerifiedPhone=");
        R.append(this.requireVerifiedPhone);
        R.append(", requireVerifiedEmail=");
        R.append(this.requireVerifiedEmail);
        R.append(", requireVerifiedPhoneThenEmail=");
        R.append(this.requireVerifiedPhoneThenEmail);
        R.append(", email=");
        R.append(this.email);
        R.append(", phone=");
        R.append(this.phone);
        R.append(", identityType=");
        R.append(this.identityType);
        R.append(", phoneCarrierName=");
        R.append(this.phoneCarrierName);
        R.append(", phoneCountry=");
        return a.D(R, this.phoneCountry, ")");
    }
}
