package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackVideoBackgroundFeedback.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b#\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R$\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u001b\u0010\"\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u001f\u001a\u0004\b#\u0010!R\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R\u001b\u0010&\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u001b\u0010*\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010'\u001a\u0004\b+\u0010)R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012R\u001b\u00100\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u001f\u001a\u0004\b1\u0010!R\u001b\u00102\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0010\u001a\u0004\b3\u0010\u0012R\u001b\u00104\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u001f\u001a\u0004\b5\u0010!R\u001b\u00106\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u001f\u001a\u0004\b7\u0010!R\u001b\u00108\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0010\u001a\u0004\b9\u0010\u0012R\u001b\u0010:\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u001f\u001a\u0004\b;\u0010!R\u001b\u0010<\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0010\u001a\u0004\b=\u0010\u0012R\u001b\u0010>\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0010\u001a\u0004\b?\u0010\u0012¨\u0006@"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVideoBackgroundFeedback;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "rtcConnectionId", "Ljava/lang/CharSequence;", "getRtcConnectionId", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "channelType", "Ljava/lang/Long;", "getChannelType", "()Ljava/lang/Long;", "voiceStateCount", "getVoiceStateCount", "videoEffectDetail", "getVideoEffectDetail", "videoEnabled", "Ljava/lang/Boolean;", "getVideoEnabled", "()Ljava/lang/Boolean;", "videoHardwareScalingEnabled", "getVideoHardwareScalingEnabled", "rating", "getRating", "videoEffectType", "getVideoEffectType", "channelId", "getChannelId", ModelAuditLogEntry.CHANGE_KEY_REASON, "getReason", "guildId", "getGuildId", "duration", "getDuration", "videoDeviceName", "getVideoDeviceName", "videoStreamCount", "getVideoStreamCount", "mediaSessionId", "getMediaSessionId", "feedback", "getFeedback", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVideoBackgroundFeedback implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence reason = null;
    private final CharSequence rating = null;
    private final CharSequence feedback = null;
    private final Long guildId = null;
    private final Long channelId = null;
    private final Long channelType = null;
    private final CharSequence mediaSessionId = null;
    private final CharSequence rtcConnectionId = null;
    private final Long duration = null;
    private final CharSequence videoDeviceName = null;
    private final Boolean videoEnabled = null;
    private final Boolean videoHardwareScalingEnabled = null;
    private final Long videoStreamCount = null;
    private final Long voiceStateCount = null;
    private final CharSequence videoEffectType = null;
    private final CharSequence videoEffectDetail = null;
    private final transient String analyticsSchemaTypeName = "video_background_feedback";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVideoBackgroundFeedback)) {
            return false;
        }
        TrackVideoBackgroundFeedback trackVideoBackgroundFeedback = (TrackVideoBackgroundFeedback) obj;
        return m.areEqual(this.reason, trackVideoBackgroundFeedback.reason) && m.areEqual(this.rating, trackVideoBackgroundFeedback.rating) && m.areEqual(this.feedback, trackVideoBackgroundFeedback.feedback) && m.areEqual(this.guildId, trackVideoBackgroundFeedback.guildId) && m.areEqual(this.channelId, trackVideoBackgroundFeedback.channelId) && m.areEqual(this.channelType, trackVideoBackgroundFeedback.channelType) && m.areEqual(this.mediaSessionId, trackVideoBackgroundFeedback.mediaSessionId) && m.areEqual(this.rtcConnectionId, trackVideoBackgroundFeedback.rtcConnectionId) && m.areEqual(this.duration, trackVideoBackgroundFeedback.duration) && m.areEqual(this.videoDeviceName, trackVideoBackgroundFeedback.videoDeviceName) && m.areEqual(this.videoEnabled, trackVideoBackgroundFeedback.videoEnabled) && m.areEqual(this.videoHardwareScalingEnabled, trackVideoBackgroundFeedback.videoHardwareScalingEnabled) && m.areEqual(this.videoStreamCount, trackVideoBackgroundFeedback.videoStreamCount) && m.areEqual(this.voiceStateCount, trackVideoBackgroundFeedback.voiceStateCount) && m.areEqual(this.videoEffectType, trackVideoBackgroundFeedback.videoEffectType) && m.areEqual(this.videoEffectDetail, trackVideoBackgroundFeedback.videoEffectDetail);
    }

    public int hashCode() {
        CharSequence charSequence = this.reason;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.rating;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.feedback;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l = this.guildId;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channelType;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.mediaSessionId;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.rtcConnectionId;
        int hashCode8 = (hashCode7 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l4 = this.duration;
        int hashCode9 = (hashCode8 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.videoDeviceName;
        int hashCode10 = (hashCode9 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Boolean bool = this.videoEnabled;
        int hashCode11 = (hashCode10 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.videoHardwareScalingEnabled;
        int hashCode12 = (hashCode11 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l5 = this.videoStreamCount;
        int hashCode13 = (hashCode12 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.voiceStateCount;
        int hashCode14 = (hashCode13 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.videoEffectType;
        int hashCode15 = (hashCode14 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.videoEffectDetail;
        if (charSequence8 != null) {
            i = charSequence8.hashCode();
        }
        return hashCode15 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVideoBackgroundFeedback(reason=");
        R.append(this.reason);
        R.append(", rating=");
        R.append(this.rating);
        R.append(", feedback=");
        R.append(this.feedback);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", duration=");
        R.append(this.duration);
        R.append(", videoDeviceName=");
        R.append(this.videoDeviceName);
        R.append(", videoEnabled=");
        R.append(this.videoEnabled);
        R.append(", videoHardwareScalingEnabled=");
        R.append(this.videoHardwareScalingEnabled);
        R.append(", videoStreamCount=");
        R.append(this.videoStreamCount);
        R.append(", voiceStateCount=");
        R.append(this.voiceStateCount);
        R.append(", videoEffectType=");
        R.append(this.videoEffectType);
        R.append(", videoEffectDetail=");
        return a.D(R, this.videoEffectDetail, ")");
    }
}
