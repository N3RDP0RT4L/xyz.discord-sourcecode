package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNetworkingPeerMetrics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u001b\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0016\u0010\u0012R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0010\u001a\u0004\b\u0018\u0010\u0012R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0010\u001a\u0004\b\u001a\u0010\u0012R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0010\u001a\u0004\b\u001c\u0010\u0012R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0010\u001a\u0004\b\u001e\u0010\u0012R\u001c\u0010\u001f\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\u0005R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0010\u001a\u0004\b#\u0010\u0012R\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R\u001b\u0010&\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0010\u001a\u0004\b'\u0010\u0012R\u001b\u0010(\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0010\u001a\u0004\b)\u0010\u0012R$\u0010+\u001a\u0004\u0018\u00010*8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.\"\u0004\b/\u00100¨\u00061"}, d2 = {"Lcom/discord/analytics/generated/events/TrackNetworkingPeerMetrics;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "pingsReceived", "Ljava/lang/Long;", "getPingsReceived", "()Ljava/lang/Long;", "intervalNs", "getIntervalNs", "pingsSent", "getPingsSent", "applicationId", "getApplicationId", "pongsSent", "getPongsSent", "pongsReceived", "getPongsReceived", "pingRttP95", "getPingRttP95", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "pingRttMax", "getPingRttMax", "peerId", "getPeerId", "pingRttAvg", "getPingRttAvg", "pingRttStddev", "getPingRttStddev", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNetworkingPeerMetrics implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long applicationId = null;
    private final Long peerId = null;
    private final Long intervalNs = null;
    private final Long pingsSent = null;
    private final Long pingsReceived = null;
    private final Long pongsSent = null;
    private final Long pongsReceived = null;
    private final Long pingRttAvg = null;
    private final Long pingRttMax = null;
    private final Long pingRttP95 = null;
    private final Long pingRttStddev = null;
    private final transient String analyticsSchemaTypeName = "networking_peer_metrics";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNetworkingPeerMetrics)) {
            return false;
        }
        TrackNetworkingPeerMetrics trackNetworkingPeerMetrics = (TrackNetworkingPeerMetrics) obj;
        return m.areEqual(this.applicationId, trackNetworkingPeerMetrics.applicationId) && m.areEqual(this.peerId, trackNetworkingPeerMetrics.peerId) && m.areEqual(this.intervalNs, trackNetworkingPeerMetrics.intervalNs) && m.areEqual(this.pingsSent, trackNetworkingPeerMetrics.pingsSent) && m.areEqual(this.pingsReceived, trackNetworkingPeerMetrics.pingsReceived) && m.areEqual(this.pongsSent, trackNetworkingPeerMetrics.pongsSent) && m.areEqual(this.pongsReceived, trackNetworkingPeerMetrics.pongsReceived) && m.areEqual(this.pingRttAvg, trackNetworkingPeerMetrics.pingRttAvg) && m.areEqual(this.pingRttMax, trackNetworkingPeerMetrics.pingRttMax) && m.areEqual(this.pingRttP95, trackNetworkingPeerMetrics.pingRttP95) && m.areEqual(this.pingRttStddev, trackNetworkingPeerMetrics.pingRttStddev);
    }

    public int hashCode() {
        Long l = this.applicationId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.peerId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.intervalNs;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.pingsSent;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.pingsReceived;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.pongsSent;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.pongsReceived;
        int hashCode7 = (hashCode6 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.pingRttAvg;
        int hashCode8 = (hashCode7 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.pingRttMax;
        int hashCode9 = (hashCode8 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.pingRttP95;
        int hashCode10 = (hashCode9 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.pingRttStddev;
        if (l11 != null) {
            i = l11.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNetworkingPeerMetrics(applicationId=");
        R.append(this.applicationId);
        R.append(", peerId=");
        R.append(this.peerId);
        R.append(", intervalNs=");
        R.append(this.intervalNs);
        R.append(", pingsSent=");
        R.append(this.pingsSent);
        R.append(", pingsReceived=");
        R.append(this.pingsReceived);
        R.append(", pongsSent=");
        R.append(this.pongsSent);
        R.append(", pongsReceived=");
        R.append(this.pongsReceived);
        R.append(", pingRttAvg=");
        R.append(this.pingRttAvg);
        R.append(", pingRttMax=");
        R.append(this.pingRttMax);
        R.append(", pingRttP95=");
        R.append(this.pingRttP95);
        R.append(", pingRttStddev=");
        return a.F(R, this.pingRttStddev, ")");
    }
}
